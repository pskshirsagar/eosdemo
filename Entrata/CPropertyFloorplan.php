<?php
use Psi\Eos\Entrata\CCompanyMediaFiles;
use Psi\Eos\Entrata\CUnitTypes;
use Psi\Eos\Entrata\CUnitSpaces;

class CPropertyFloorplan extends CBasePropertyFloorplan {

	const CATCH_ALL_FLOOR_PLAN = 'Catch All Floor Plan';

	protected $m_fltUnitMinRent;
	protected $m_fltUnitMaxRent;
	protected $m_fltMaxShareRent;
	protected $m_fltMinSharedRent;
	protected $m_fltDefaultMinRent;
	protected $m_fltDefaultMaxRent;
	protected $m_fltDefaultMinDeposit;
	protected $m_fltDefaultMaxDeposit;

	protected $m_boolHasSpecials;
	protected $m_boolShowInEntrata;
	protected $m_boolHasDisplaySpecials;
	protected $m_boolIsPropertyUnitSpecial;
	protected $m_boolIsBedroomMatch;
	protected $m_boolIsBathroomMatch;
	protected $m_boolIsAvailabilityMatch;
	protected $m_boolIsRentMatch;

	protected $m_intMinOccupant;
	protected $m_intMaxOccupant;
	protected $m_intLeaseTermId;
	protected $m_intUnitSpaceId;
	protected $m_intUnitTypesCount;
	protected $m_intPropertyUnitId;
	protected $m_intAvailableUnits;
	protected $m_intParentPropertyId;
	protected $m_intMinRentRateAmount;
	protected $m_intMaxRentRateAmount;
	protected $m_intPropertyFloorplanId;
	protected $m_intSpaceConfigurationId;
	protected $m_intMatchScore;

	protected $m_strFullsizeUri;
	protected $m_strBedBathCount;
	protected $m_strPropertyName;
	protected $m_strLeaseTermName;
	protected $m_strSpaceConfigurationName;

	protected $m_objProperty;
	protected $m_objFloorplanMarketingMediaAssociation;

	protected $m_arrobjSpecials;
	protected $m_arrobjUnitTypes;
	protected $m_arrobjUnitSpaces;
	protected $m_arrobjPropertyUnits;
	protected $m_arrobjCompanyMediaFiles;
	protected $m_arrobjFloorplanAmenities;

	protected $m_arrmixSpaceConfigurations;

	protected $m_arrintLeaseTerm;

    /**
	* Get Functions
	*
	*/

    public function getFloorplanAmenities() {
    	return $this->m_arrobjFloorplanAmenities;
    }

    public function getUnitTypes() {
        return $this->m_arrobjUnitTypes;
    }

	public function getLeaseTerms() {
		return $this->m_arrintLeaseTerm;
	}

    public function getMarketingMediaAssociation() {
        return $this->m_objFloorplanMarketingMediaAssociation;
    }

    public function getDefaultMinDeposit() {
        return $this->m_fltDefaultMinDeposit;
    }

    public function getDefaultMaxDeposit() {
        return $this->m_fltDefaultMaxDeposit;
    }

    public function getMinSharedRent() {
        return $this->m_fltMinSharedRent;
    }

    public function getMaxSharedRent() {
        return $this->m_fltMaxSharedRent;
    }

    public function getDefaultMinRent() {
        return $this->m_fltDefaultMinRent;
    }

    public function getDefaultMaxRent() {
        return $this->m_fltDefaultMaxRent;
    }

    public function getBedBathCount() {
    	return $this->m_strBedBathCount;
    }

    public function getSeoFloorplanName() {
    	return trim( \Psi\CStringService::singleton()->strtolower( \Psi\CStringService::singleton()->preg_replace( [ '/[^a-zA-Z0-9\s_\-]+/', '/(\s|-|_)+/' ], [ '', '-' ], $this->getFloorplanName() ) ) );
    }

    public function getUnitMinRent() {
        return $this->m_fltUnitMinRent;
    }

    public function getUnitMaxRent() {
        return $this->m_fltUnitMaxRent;
    }

    public function getUnitSpaces() {
        return $this->m_arrobjUnitSpaces;
    }

    public function getProperty() {
    	return $this->m_objProperty;
    }

    public function getSpecials() {
    	return $this->m_arrobjSpecials;
    }

    public function getIsPropertyUnitSpecial() {
    	return $this->m_boolIsPropertyUnitSpecial;
    }

  	public function getHasSpecials() {
    	return $this->m_boolHasSpecials;
    }

  	public function getHasDisplaySpecials() {
    	return $this->m_boolHasDisplaySpecials;
    }

    public function getShowInEntrata() {
        return $this->m_boolShowInEntrata;
    }

    public function getMinRentRateAmount() {
    	return $this->m_intMinRentRateAmount;
    }

    public function getMaxRentRateAmount() {
    	return $this->m_intMaxRentRateAmount;
    }

    public function getPropertyUnits() {
    	return $this->m_arrobjPropertyUnits;
    }

    public function getAvailableUnits() {
    	return $this->m_intAvailableUnits;
    }

    public function getFullsizeUri() {
    	return $this->m_strFullsizeUri;
    }

	public function getMinOccupant() {
    	return $this->m_intMinOccupant;
    }

    public function getMaxOccupant() {
    	return $this->m_intMaxOccupant;
    }

	public function getParentPropertyId() {
    	return $this->m_intParentPropertyId;
    }

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getLeaseTermName() {
	    return $this->m_strLeaseTermName;
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function getUnitTypesCount() {
		return $this->m_intUnitTypesCount;
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function getSpaceConfigurationId() {
		return $this->m_intSpaceConfigurationId;
	}

	public function getSpaceConfigurationName() {
		return $this->m_strSpaceConfigurationName;
	}

	public function getRentRange( $boolReturnHyphen = true ) {
		$strRentRange = '';
		if( 0 < $this->getMinRent() || 0 < $this->getMaxRent() ) {

			$strRentRange .= ( 0 < $this->getMinRent() ) ? __( '{%m,0}', [ $this->getMinRent() ] ) : NULL;
			$strRentRange .= ( 0 < $this->getMinRent() && 0 < $this->getMaxRent() && $this->getMaxRent() > $this->getMinRent() ) ? ' - ' : NULL;
			$strRentRange .= ( 0 < $this->getMaxRent() && $this->getMaxRent() > $this->getMinRent() ) ? __( '{%m,0}', [ $this->getMaxRent() ] ) : NULL;

			$this->m_fltMinRent = __( '{%f,0,p:0}', [ $this->getMinRent() ] );
			$this->m_fltMaxRent = __( '{%f,0,p:0}', [ $this->getMaxRent() ] );

		} elseif( true == $boolReturnHyphen ) {
			$strRentRange = '-';
		}
		return $strRentRange;
	}

	public function getDepositRange( $boolReturnHyphen = true ) {
		$strDepositRange = '';
		if( 0 < $this->getMinDeposit() || 0 < $this->getMaxDeposit() ) {
			$this->m_fltMinDeposit = __( '{%f,0,p:0}', [ $this->getMinDeposit() ] );
			$this->m_fltMaxDeposit = __( '{%f,0,p:0}', [ $this->getMaxDeposit() ] );
			$strDepositRange .= ( 0 < $this->getMinDeposit() ) ? __( '{%m,0}', [ $this->getMinDeposit() ] ) : NULL;
			$strDepositRange .= ( 0 < $this->getMinDeposit() && 0 < $this->getMaxDeposit() && $this->getMaxDeposit() > $this->getMinDeposit() ) ? ' - ' : NULL;
			$strDepositRange .= ( 0 < $this->getMaxDeposit() && $this->getMaxDeposit() > $this->getMinDeposit() ) ? __( '{%m,0}', [ $this->getMaxDeposit() ] ) : NULL;
		} elseif( true == $boolReturnHyphen ) {
			$strDepositRange = '-';
		}
		return $strDepositRange;
	}

	public function getSquareFeetRange( $boolReturnHyphen = true, $strUnitOfMeasure = NULL ) {
		if( false == valStr( $strUnitOfMeasure ) ) {
			$strUnitOfMeasure = __( 'Sq Ft' );
		}
		$strSquareFeetRange = '';
		if( 0 < $this->getMinSquareFeet() || 0 < $this->getMaxSquareFeet() ) {
			$strSquareFeetRange .= ( 0 < $this->getMinSquareFeet() ) ? ( $this->getMinSquareFeet() . ' ' . $strUnitOfMeasure ) : NULL;
			$strSquareFeetRange .= ( 0 < $this->getMinSquareFeet() && 0 < $this->getMaxSquareFeet() && $this->getMaxSquareFeet() > $this->getMinSquareFeet() ) ? ' - ' : NULL;
			$strSquareFeetRange .= ( 0 < $this->getMaxSquareFeet() && $this->getMaxSquareFeet() > $this->getMinSquareFeet() ) ? ( $this->getMaxSquareFeet() . ' ' . $strUnitOfMeasure ) : NULL;
		} elseif( true == $boolReturnHyphen ) {
			$strSquareFeetRange = '-';
		}
		return $strSquareFeetRange;
	}

	public function getRawPropertyFloorPlanData( $arrobjPropertyPreferences, $arrintLeaseTermOptions, $boolIsResidentWorks, $boolIsPropertySitePlanModule = false ) {
		$arrstrFloorplan2DMedias = [];
		$arrstrFloorplan3DMedias = [];

        if( true == valArr( $this->getMarketingMediaAssociations() ) ) {
			$intCount2D = 0;
			$intCount3D = 0;
			foreach( $this->getMarketingMediaAssociations() as $objMarketingMediaAssociation ) {
				if( CMarketingMediaSubType::FLOORPLAN_2D == $objMarketingMediaAssociation->getMediaTypeId() ) {
					if( false == $boolIsPropertySitePlanModule ) {
						$arrstrFloorplan2DMedias[$intCount2D]['image'] = $objMarketingMediaAssociation->getMarketingStorageTypeReference();
					} else {
						$arrstrFloorplan2DMedias[$intCount2D]['image'] = $objMarketingMediaAssociation->getMediaLibraryFullSizeUri();
					}
					$arrstrFloorplan2DMedias[$intCount2D]['scale'] = $objMarketingMediaAssociation->getImageScale();
					$intCount2D++;
				} else {
					if( false == $boolIsPropertySitePlanModule ) {
						$arrstrFloorplan3DMedias[$intCount3D++] = $objMarketingMediaAssociation->getMarketingStorageTypeReference();
					} else {
						$arrstrFloorplan3DMedias[$intCount3D++] = $objMarketingMediaAssociation->getMediaLibraryFullSizeUri();
					}
				}
			}
		}

        $arrstrPropertyFloorPlans 								= [];
    	$arrstrPropertyFloorPlans['id'] 						= $this->getId();
    	$arrstrPropertyFloorPlans['floorPlanName'] 				= $this->getFloorplanName();
    	$arrstrPropertyFloorPlans['numberOfBedrooms'] 			= $this->getNumberOfBedrooms();
    	$arrstrPropertyFloorPlans['numberOfBathrooms'] 			= $this->getNumberOfBathrooms();
    	$arrstrPropertyFloorPlans['minSquareFeet'] 				= $this->getMinSquareFeet();
    	$arrstrPropertyFloorPlans['maxSquareFeet'] 				= $this->getMaxSquareFeet();

		if( true == valArr( $arrobjPropertyPreferences ) ) {
			if( ( false == array_key_exists( 'HIDE_RENT_RANGE_REALTIME', $arrobjPropertyPreferences ) || ( true == array_key_exists( 'HIDE_RENT_RANGE_REALTIME', $arrobjPropertyPreferences ) && ( 1 != $arrobjPropertyPreferences['HIDE_RENT_RANGE_REALTIME']->getValue() && 3 != $arrobjPropertyPreferences['HIDE_RENT_RANGE_REALTIME']->getValue() ) ) ) ) {
				$arrstrPropertyFloorPlans['minRent']	= __( '{%f,0,p:2}', [ $this->getMinRent() ] );
				$arrstrPropertyFloorPlans['maxRent'] 	= __( '{%f,0,p:2}', [ $this->getMaxRent() ] );
			}
			if( ( false == array_key_exists( 'HIDE_DEPOSIT_RANGE_REALTIME', $arrobjPropertyPreferences ) || true == is_null( $arrobjPropertyPreferences['HIDE_DEPOSIT_RANGE_REALTIME']->getValue() ) ) ) {
				$arrstrPropertyFloorPlans['minDeposit']	= __( '{%f,0,p:2}', [ $this->getMinDeposit() ] );
				$arrstrPropertyFloorPlans['maxDeposit']	= __( '{%f,0,p:2}', [ $this->getMaxDeposit() ] );
			}
		}

		if( false == $boolIsResidentWorks ) {
    		$arrstrPropertyFloorPlans['disabledVirtualMoveIn']	= $this->getIsDisabledVirtualMoveIn();
    		$arrstrPropertyFloorPlans['numberOfAvailableUnits'] = $this->getAvailableUnits();
    		$arrstrPropertyFloorPlans['floorplan2DImage'] 		= $arrstrFloorplan2DMedias;
			$arrstrPropertyFloorPlans['floorplan3DImages'] 		= $arrstrFloorplan3DMedias;
			$arrstrPropertyFloorPlans['leaseTermOptions'] 		= $arrintLeaseTermOptions;
    	}
		return $arrstrPropertyFloorPlans;
    }

    public function getRawPropertyUnitData( $strLeaseStartDate, $arrobjPropertyBuildings = NULL, $intPropertyFloorId, $boolIsResidentWorks, $objDatabase ) {
    	$arrstrRawPropertyUnitData 	= [];
    	$arrstrPropertyUnits 		= [];
    	if( true == valArr( $this->getUnitSpaces() ) ) {
    	 	$arrobjUnitSpaces			= rekeyObjects( 'PropertyUnitId', $this->getUnitSpaces() );
    	 	foreach( $arrobjUnitSpaces as $objUnitSpace ) {
    	 		if( 0 < $intPropertyFloorId ) {
    	 			$objPropertyUnit   = $objUnitSpace->fetchPropertyUnitByPropertyFloorId( $intPropertyFloorId, $objDatabase );
    	 		} else {
    	 			$objPropertyUnit   = $objUnitSpace->fetchPropertyUnit( $objDatabase );
    	 		}

    	 		if( false == is_null( $objPropertyUnit ) && true == valObj( $objPropertyUnit, 'CPropertyUnit' ) ) {
    	 			$intUnitSpaceCount 		= $objPropertyUnit->fetchAvailableUnitSpaceCount( $objDatabase );
    	 			$objUnitType	= $objPropertyUnit->fetchUnitType( $objDatabase );

					if( 1 == $objPropertyUnit->getisFurnished() ) {
						$strUnitFurnished = 'Yes';
					} else {
						$strUnitFurnished = 'No';
					}
	    			$arrstrPropertyUnits['id'] 					= $objPropertyUnit->getId();
	    			if( NULL != $objPropertyUnit->getMarketingUnitNumber() ) {
	    				$arrstrPropertyUnits['unitName'] 		= $objPropertyUnit->getMarketingUnitNumber();
	    			} else {
	    				$arrstrPropertyUnits['unitName'] 		= $objPropertyUnit->getUnitNumber();
	    			}
	    			$arrstrPropertyUnits['buildingName']		= '';
	    			$arrstrPropertyUnits['minSquareFeet']		= $objPropertyUnit->getSquareFeet();
	    			$arrstrPropertyUnits['numberOfBedrooms'] 	= $objPropertyUnit->getNumberOfBedrooms();
	    			$arrstrPropertyUnits['numberOfBathrooms']	= $objPropertyUnit->getNumberOfBathrooms();
	    			$arrstrPropertyUnits['marketingUnitNumber']	= $objPropertyUnit->getMarketingUnitNumber();
	    			$arrstrPropertyUnits['unitNumber']			= $objPropertyUnit->getUnitNumber();
	    			$arrstrPropertyUnits['propertyId']			= $objPropertyUnit->getPropertyId();

 					$objPropertyBuilding	= ( false == is_null( $arrobjPropertyBuildings ) && true == valArr( $arrobjPropertyBuildings ) && true == array_key_exists( $objPropertyUnit->getPropertyBuildingId(), $arrobjPropertyBuildings ) ) ? $arrobjPropertyBuildings[$objPropertyUnit->getPropertyBuildingId()] : NULL;

					if( true == valObj( $objPropertyBuilding, 'CPropertyBuilding' ) ) {
						if( true == $boolIsResidentWorks ) {
							$arrstrPropertyUnits['buildingName'] 	= $objPropertyBuilding->getBuildingName();
						}
					}

					if( false == is_null( $intPropertyFloorId ) && true == isset( $intPropertyFloorId ) && 0 < $intPropertyFloorId ) {
						$arrstrPropertyUnits['xCoordinate']		= is_null( $objPropertyUnit->getFloorXPos() ) ? 0 : $objPropertyUnit->getFloorXPos();
						$arrstrPropertyUnits['yCoordinate']		= is_null( $objPropertyUnit->getFloorYPos() ) ? 0 : $objPropertyUnit->getFloorYPos();
					} else {
						$arrstrPropertyUnits['xCoordinate'] = is_null( $objPropertyUnit->getSitePlanXPos() ) ? 0 : $objPropertyUnit->getSitePlanXPos();
						$arrstrPropertyUnits['yCoordinate'] = is_null( $objPropertyUnit->getSitePlanYPos() ) ? 0 : $objPropertyUnit->getSitePlanYPos();
					}

					$arrstrPropertyUnits['xFloorCoordinate']	= is_null( $objPropertyUnit->getFloorXPos() ) ? 0 : $objPropertyUnit->getFloorXPos();
					$arrstrPropertyUnits['yFloorCoordinate']	= is_null( $objPropertyUnit->getFloorYPos() ) ? 0 : $objPropertyUnit->getFloorYPos();

					if( true == valObj( $objUnitType, 'CUnitType' ) ) {
						$arrstrPropertyUnits['floorPlanId'] 	= $objUnitType->getPropertyFloorplanId();
					}

					$arrstrPropertyUnits['floorId'] 		= $objPropertyUnit->getPropertyFloorId();
					$arrstrPropertyUnits['available'] 		= $intUnitSpaceCount;
					$arrstrPropertyUnits['furnished'] 		= $strUnitFurnished;

					if( false == $boolIsResidentWorks ) {
						$arrobjRates 	= [];
		    			$intBestPriceMonth 		= 0;
		    			$fltEffectiveRate		= 0;
		    			$arrobjRates 	= $objUnitSpace->loadPricingAndLeaseTerms( $objDatabase, $strLeaseStartDate );
						if( true == valArr( $arrobjRates ) ) {
							\Psi\CStringService::singleton()->asort( $arrobjRates );
							$intRateCounter = 1;
							foreach( $arrobjRates as $arrobjRate ) {
								if( 0 == $intBestPriceMonth ) {
									$intBestPriceMonth 	= $arrobjRate['term'];
									$fltEffectiveRate	= $arrobjRate['rent'];
								} elseif( $arrobjRate['rent'] < $fltEffectiveRate ) {
									$intBestPriceMonth 	= $arrobjRate['term'];
									$fltEffectiveRate	= $arrobjRate['rent'];
								}
							}

							$arrmixRates = [];
							$arrmixRates['orderNum']			= $intRateCounter;
							$arrmixRates['leaseTerm'] 			= $arrobjRate['term'];
							$arrmixRates['effectiveRent'] 		= $arrobjRate['rent'];
							$arrmixRates['isBestPrice']			= $arrobjRate['is_best_price'];
							$arrmixRentRates['isDefaultLeaseTerm']		= 0;
							if( true == isset( $arrobjRate['is_default_lease_terms'] ) ) {
								$arrmixRentRates['isDefaultLeaseTerm'] = $arrobjRate['is_default_lease_terms'];
							}
						}
					}
					$arrstrRawPropertyUnitData[] = $arrstrPropertyUnits;
				}
			}
			return $arrstrRawPropertyUnitData;
	   	}
	    return $arrstrRawPropertyUnitData;
	}

	public function getRawPropertyFloorData( $intPropertyFloorId, $objDatabase ) {
		$boolIsFloorImage = false;
		$arrstrPropertyFloors = [];

		if( true == valArr( $this->getUnitSpaces() ) ) {
	   		foreach( $this->getUnitSpaces() as $objUnitSpace ) {
	   			if( 0 < $intPropertyFloorId ) {
	   				$objPropertyUnit   = $objUnitSpace->fetchPropertyUnitByPropertyFloorId( $intPropertyFloorId, $objDatabase );
	   			} else {
	   				$objPropertyUnit   = $objUnitSpace->fetchPropertyUnit( $objDatabase );
	   			}

    	 		if( true == valObj( $objPropertyUnit, 'CPropertyUnit' ) ) {
	    	 		$objPropertyFloor = $this->m_objProperty->fetchPropertyFloorById( $intPropertyFloorId, $objDatabase );
					if( true == valObj( $objPropertyFloor, 'CPropertyFloor' ) ) {
						$objCompanyMediaFile = CCompanyMediaFiles::createService()->fetchCompanyMediaFileByIdByCid( $objPropertyFloor->getCompanyMediaFileId(), $this->getCid(), $objDatabase );
						if( false == valObj( $objCompanyMediaFile, 'CCompanyMediaFile' ) ) {
							continue;
						}
						$arrstrPropertyFloors['id'] 				= $objPropertyFloor->getId();
						$arrstrPropertyFloors['floorTitle'] 		= $objPropertyFloor->getFloorTitle();
						$arrstrPropertyFloors['floorNumber'] 		= $objPropertyFloor->getFloorNumber();
						$arrstrPropertyFloors['companyMediaFileId'] = $objCompanyMediaFile->getFullsizeUri();
						$boolIsFloorImage 							= true;
					} elseif( false == $boolIsFloorImage ) {
						$objCompanyMediaFile = $this->m_objProperty->fetchCompanyMediaFileByMediaTypeId( CMediaType::SITE_PLAN, $objDatabase );
						if( false == valObj( $objCompanyMediaFile, 'CCompanyMediaFile' ) ) {
							continue;
						}
						$arrstrPropertyFloors['id'] 				= 0;
						$arrstrPropertyFloors['floorTitle'] 		= '';
						$arrstrPropertyFloors['floorNumber'] 		= '';
						$arrstrPropertyFloors['companyMediaFileId'] = $objCompanyMediaFile->getFullsizeUri();
					}
    	 		}
			}
		}
		return $arrstrPropertyFloors;
   	}

   	public function getSpaceConfigurations() {
   		return $this->m_arrmixSpaceConfigurations;
   	}

   	public function getIsBedroomMatch() {
    	return $this->m_boolIsBedroomMatch;
    }

    public function getIsBathroomMatch() {
    	return $this->m_boolIsBathroomMatch;
    }

    public function getIsRentMatch() {
    	return $this->m_boolIsRentMatch;
    }

    public function getIsAvailabilityMatch() {
    	return $this->m_boolIsAvailabilityMatch;
    }

    public function getMatchScore() {
    	return $this->m_intMatchScore;
    }

	public function getMediaLibraryFullSizeUri() {
		return ( true == valStr( $this->getFullsizeUri() ) ? CConfig::get( 'media_library_path' ) . $this->getFullsizeUri() : NULL );
	}

    /**
	* Add Functions
	*
	*/
   	public function addFloorplanAmenity( $objFloorplanAmenity ) {
   		$this->m_arrobjFloorplanAmenities[$objFloorplanAmenity->getId()] = $objFloorplanAmenity;
   	}

    public function addUnitSpace( $objUnitSpace ) {
        $this->m_arrobjUnitSpaces[$objUnitSpace->getId()] = $objUnitSpace;
    }

    public function addMarketingMediaAssociation( $objFloorplanMarketingMediaAssociation ) {
        $this->m_arrobjCompanyMediaFiles[$objFloorplanMarketingMediaAssociation->getId()] = $objFloorplanMarketingMediaAssociation;
    }

    public function addPropertyUnit( $objPropertyUnit ) {
        $this->m_arrobjPropertyUnits[$objPropertyUnit->getId()] = $objPropertyUnit;
    }

    /**
	* Set Functions
	*
	*/
    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( true == isset( $arrmixValues['default_min_deposit'] ) )			$this->setDefaultMinDeposit( $arrmixValues['default_min_deposit'] );
        if( true == isset( $arrmixValues['default_max_deposit'] ) )			$this->setDefaultMaxDeposit( $arrmixValues['default_max_deposit'] );
        if( true == isset( $arrmixValues['default_min_rent'] ) )			$this->setDefaultMinRent( $arrmixValues['default_min_rent'] );
        if( true == isset( $arrmixValues['default_max_rent'] ) )			$this->setDefaultMaxRent( $arrmixValues['default_max_rent'] );
        if( true == isset( $arrmixValues['min_shared_rent'] ) )			    $this->setMinSharedRent( $arrmixValues['min_shared_rent'] );
        if( true == isset( $arrmixValues['max_shared_rent'] ) )			    $this->setMaxSharedRent( $arrmixValues['max_shared_rent'] );
        if( true == isset( $arrmixValues['specials'] ) )					$this->setSpecials( $arrmixValues['specials'] );
        if( true == isset( $arrmixValues['is_property_unit_special'] ) )	$this->setIsPropertyUnitSpecial( $arrmixValues['is_property_unit_special'] );
        if( true == isset( $arrmixValues['available_units'] ) )				$this->setAvailableUnits( $arrmixValues['available_units'] );
        if( true == isset( $arrmixValues['fullsize_uri'] ) )				$this->setFullsizeUri( $arrmixValues['fullsize_uri'] );
        if( true == isset( $arrmixValues['has_specials'] ) )				$this->setHasSpecials( $arrmixValues['has_specials'] );
        if( true == isset( $arrmixValues['has_display_specials'] ) )		$this->setHasDisplaySpecials( $arrmixValues['has_display_specials'] );
        if( true == isset( $arrmixValues['show_in_entrata'] ) )	           	$this->setShowInEntrata( $arrmixValues['show_in_entrata'] );
        if( true == isset( $arrmixValues['property_name'] ) )				$this->setPropertyName( $arrmixValues['property_name'] );
        if( true == isset( $arrmixValues['lease_term_name'] ) )				$this->setLeaseTermName( $arrmixValues['lease_term_name'] );
        if( true == isset( $arrmixValues['parent_property_id'] ) )			$this->setParentPropertyId( $arrmixValues['parent_property_id'] );
        if( true == isset( $arrmixValues['lease_term_id'] ) )				$this->setLeaseTermId( $arrmixValues['lease_term_id'] );
        if( true == isset( $arrmixValues['property_unit_id'] ) )			$this->setPropertyUnitId( $arrmixValues['property_unit_id'] );
        if( true == isset( $arrmixValues['unit_types_count'] ) )			$this->setUnitTypesCount( $arrmixValues['unit_types_count'] );
        if( true == isset( $arrmixValues['unit_space_id'] ) )				$this->setUnitSpaceId( $arrmixValues['unit_space_id'] );
        if( true == isset( $arrmixValues['space_configuration_id'] ) )		$this->setSpaceConfigurationId( $arrmixValues['space_configuration_id'] );
        if( true == isset( $arrmixValues['space_configuration_name'] ) )	$this->setSpaceConfigurationName( $arrmixValues['space_configuration_name'] );
        if( true == isset( $arrmixValues['bedroom_match'] ) )               $this->setIsBedroomMatch( $arrmixValues['bedroom_match'] );
	    if( true == isset( $arrmixValues['bathroom_match'] ) )              $this->setIsBathroomMatch( $arrmixValues['bathroom_match'] );
	    if( true == isset( $arrmixValues['rent_match'] ) )                  $this->setIsRentMatch( $arrmixValues['rent_match'] );
	    if( true == isset( $arrmixValues['availability_match'] ) )          $this->setIsAvailabilityMatch( $arrmixValues['availability_match'] );
	    if( true == isset( $arrmixValues['match_score'] ) )                 $this->setMatchScore( $arrmixValues['match_score'] );

		return parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

	public function setNumberOfBathrooms( $fltNumberOfBathrooms ) {
		if( 0 < strlen( $fltNumberOfBathrooms ) ) {
			// Filter out 1/4, 1/2, and 3/4
			$arrstrSearch 			= [ '1/4', '1/2', '3/4' ];
			$arrstrReplace 			= [ '.25', '.5', '.75' ];
			$strNumberOfBathrooms 	= str_replace( $arrstrSearch, $arrstrReplace, $fltNumberOfBathrooms );
			$fltNumberOfBathrooms 	= round( ( float ) str_replace( ' ', '', $strNumberOfBathrooms ), 2 );
		}
		parent::setNumberOfBathrooms( $fltNumberOfBathrooms );
	}

	public function setFloorplanAmenities( $arrobjFloorplanAmenities ) {
		$this->m_arrobjFloorplanAmenities = $arrobjFloorplanAmenities;
	}

	public function setUnitTypes( $arrobjUnitTypes ) {
        $this->m_arrobjUnitTypes = $arrobjUnitTypes;
        if( false == empty( $arrobjUnitTypes ) ) {
            $this->setUnitTypesCount( \Psi\Libraries\UtilFunctions\count( $arrobjUnitTypes ) );
        } else {
            $this->setUnitTypesCount( 0 );
        }
    }

	public function setLeaseTerms( $arrintLeaseTerm ) {
		$this->m_arrintLeaseTerm = $arrintLeaseTerm;
	}

    public function setMarketingMediaAssociation( $objFloorplanMarketingMediaAssociation ) {
        $this->m_objFloorplanMarketingMediaAssociation = $objFloorplanMarketingMediaAssociation;
    }

    public function setDefaultMinDeposit( $fltDefaultMinDeposit ) {
        $this->m_fltDefaultMinDeposit = CStrings::strToFloatDef( $fltDefaultMinDeposit, NULL, false, 65531 );
    }

    public function setDefaultMaxDeposit( $fltDefaultMaxDeposit ) {
        $this->m_fltDefaultMaxDeposit = CStrings::strToFloatDef( $fltDefaultMaxDeposit, NULL, false, 65531 );
    }

    public function setMinSharedRent( $fltMinSharedRent ) {
        $this->m_fltMinSharedRent = CStrings::strToFloatDef( $fltMinSharedRent, NULL, false, 65531 );
    }

    public function setMaxSharedRent( $fltMaxSharedRent ) {
        $this->m_fltMaxSharedRent = CStrings::strToFloatDef( $fltMaxSharedRent, NULL, false, 65531 );
    }

    public function setDefaultMinRent( $fltDefaultMinRent ) {
        $this->m_fltDefaultMinRent = CStrings::strToFloatDef( $fltDefaultMinRent, NULL, false, 65531 );
    }

    public function setDefaultMaxRent( $fltDefaultMaxRent ) {
        $this->m_fltDefaultMaxRent = CStrings::strToFloatDef( $fltDefaultMaxRent, NULL, false, 65531 );
    }

    public function setBedBathCount( $intNumberOfBedrooms, $intNumberOfBathrooms ) {
    	if( true == is_null( $intNumberOfBedrooms ) && true == is_null( $intNumberOfBathrooms ) ) {
    		$this->m_strBedBathCount = '- Bed, - Bath';
    	} elseif( true == is_null( $intNumberOfBedrooms ) ) {
    		$this->m_strBedBathCount = '- Bed, ' . ( int ) $intNumberOfBathrooms . 'Bath';
    	} elseif( true == is_null( $intNumberOfBathrooms ) ) {
    		$this->m_strBedBathCount = ( int ) $intNumberOfBedrooms . 'Bed, - Bath';
    	} else {
    		$this->m_strBedBathCount = ( int ) $intNumberOfBedrooms . 'Bed, ' . ( int ) $intNumberOfBathrooms . 'Bath';
    	}
    }

    public function setUnitMinRent( $fltUnitMinRent ) {
        $this->m_fltUnitMinRent = CStrings::strToFloatDef( $fltUnitMinRent, NULL, false, 65531 );
    }

    public function setUnitMaxRent( $fltUnitMaxRent ) {
        $this->m_fltUnitMaxRent = CStrings::strToFloatDef( $fltUnitMaxRent, NULL, false, 65531 );
    }

    public function setMinRent( $fltMinRent ) {
    	parent::setMinRent( $fltMinRent );
    	if( false == is_null( $fltMinRent ) ) {
			$this->m_fltMinRent = str_replace( '$', '', $fltMinRent );
		}
		if( 0 == ( float ) $this->m_fltMinRent ) $this->m_fltMinRent = NULL;
    }

    public function setMaxRent( $fltMaxRent ) {
    	parent::setMaxRent( $fltMaxRent );
    	if( false == is_null( $fltMaxRent ) ) {
			$this->m_fltMaxRent = str_replace( '$', '', $fltMaxRent );
		}
		if( 0 == ( float ) $this->m_fltMaxRent ) $this->m_fltMaxRent = NULL;
    }

    public function setMinDeposit( $fltMinDeposit ) {
    	parent::setMinDeposit( $fltMinDeposit );
    	if( false == is_null( $fltMinDeposit ) ) {
			$this->m_fltMinDeposit = str_replace( '$', '', $fltMinDeposit );
		}
		if( true == empty( $this->m_fltMinDeposit ) ) $this->m_fltMinDeposit = NULL;
    }

    public function setMaxDeposit( $fltMaxDeposit ) {
    	parent::setMaxDeposit( $fltMaxDeposit );
    	if( false == is_null( $fltMaxDeposit ) ) {
			$this->m_fltMaxDeposit = str_replace( '$', '', $fltMaxDeposit );
		}
		if( true == empty( $this->m_fltMaxDeposit ) ) $this->m_fltMaxDeposit = NULL;
    }

    public function setProperty( $objProperty ) {
    	$this->m_objProperty = $objProperty;
    }

   	public function setSpecials( $arrobjSpecials ) {
    	$this->m_arrobjSpecials = $arrobjSpecials;
    }

    public function setIsPropertyUnitSpecial( $boolIsPropertyUnitSpecial ) {
    	$this->m_boolIsPropertyUnitSpecial = $boolIsPropertyUnitSpecial;
    }

 	public function setHasSpecials( $boolHasSpecials ) {
    	$this->m_boolHasSpecials = $boolHasSpecials;
    }

 	public function setHasDisplaySpecials( $boolHasDisplaySpecials ) {
    	$this->m_boolHasDisplaySpecials = $boolHasDisplaySpecials;
    }

    public function setShowInEntrata( $boolShowInEntrata ) {
        $this->m_boolShowInEntrata = $boolShowInEntrata;
    }

    public function setMinRentRateAmount( $intMinRentRateAmount ) {
    	$this->m_intMinRentRateAmount = $intMinRentRateAmount;
    }

    public function setMaxRentRateAmount( $intMaxRentRateAmount ) {
    	$this->m_intMaxRentRateAmount = $intMaxRentRateAmount;
    }

    public function setUnitSpaces( $arrobjUnitSpaces ) {
        $this->m_arrobjUnitSpaces = $arrobjUnitSpaces;
    }

    public function setAvailableUnits( $intAvailableUnits ) {
    	$this->m_intAvailableUnits = $intAvailableUnits;
    }

    public function setFullsizeUri( $strFullsizeUri ) {
    	$this->m_strFullsizeUri = $strFullsizeUri;
    }

    public function setMinOccupant( $intMinOccupant ) {
    	$this->m_intMinOccupant = $intMinOccupant;
    }

    public function setMaxOccupant( $intMaxOccupant ) {
    	$this->m_intMaxOccupant = $intMaxOccupant;
    }

	public function setParentPropertyId( $intParentPropertyId ) {
    	$this->m_intParentPropertyId = $intParentPropertyId;
    }

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setLeaseTermName( $strLeaseTermName ) {
	    $this->m_strLeaseTermName = $strLeaseTermName;
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->m_intLeaseTermId = $intLeaseTermId;
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = $intPropertyUnitId;
	}

	public function setUnitTypesCount( $intUnitTypesCount ) {
		$this->m_intUnitTypesCount = $intUnitTypesCount;
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->m_intUnitSpaceId = $intUnitSpaceId;
	}

	public function setSpaceConfigurationId( $intSpaceConfigurationId ) {
		$this->m_intSpaceConfigurationId = $intSpaceConfigurationId;
	}

	public function setSpaceConfigurationName( $strSpaceConfigurationName ) {
		$this->m_strSpaceConfigurationName = $strSpaceConfigurationName;
	}

	public function setSpaceConfigurations( $arrmixSpaceConfigurations ) {
		$this->m_arrmixSpaceConfigurations = $arrmixSpaceConfigurations;
	}

	public function setIsBedroomMatch( $boolIsBedroomMatch ) {
    	$this->m_boolIsBedroomMatch = $boolIsBedroomMatch;
	}

	public function setIsBathroomMatch( $boolIsBathroomMatch ) {
		$this->m_boolIsBathroomMatch = $boolIsBathroomMatch;
	}

	public function setIsRentMatch( $boolIsRentMatch ) {
		$this->m_boolIsRentMatch = $boolIsRentMatch;
	}

	public function setIsAvailabilityMatch( $boolIsAvailabilityMatch ) {
		$this->m_boolIsAvailabilityMatch = $boolIsAvailabilityMatch;
	}

	public function setMatchScore( $intMatchScore ) {
		$this->m_intMatchScore = $intMatchScore;
	}

    /**
	* Create Functions
	*
	*/

	public function createFloorplanPreference() {
		$objFloorplanPreference = new CFloorplanPreference();
		$objFloorplanPreference->setCid( $this->m_intCid );
		$objFloorplanPreference->setPropertyFloorplanId( $this->m_intId );
		return $objFloorplanPreference;
	}

	public function createPropertyFloorplanGroup() {
		$objPropertyFloorplanGroup = new CPropertyFloorplanGroup();
		$objPropertyFloorplanGroup->setCid( $this->getCid() );
		$objPropertyFloorplanGroup->setPropertyId( $this->getPropertyId() );
		$objPropertyFloorplanGroup->setPropertyFloorplanId( $this->getId() );
		return $objPropertyFloorplanGroup;
	}

    public function createRate() {
        $objRate = new CRate();
		$objRate->setCid( $this->getCid() );
		$objRate->setPropertyId( $this->getPropertyId() );
		$objRate->setArCascadeId( CArCascade::FLOOR_PLAN );
		$objRate->setArCascadeReferenceId( $this->getId() );
		$objRate->setPropertyFloorplanId( $this->getId() );

		return $objRate;
    }

    public function createPropertyUnit() {
        $objRate = new CRate();
        $objRate->setCid( $this->m_intCid );
        $objRate->setRateTypeId( CRateType::PROPERTY_UNIT );
        $objRate->setReferenceId( $this->getId() );
        return $objRate;
    }

    public function createIntegrationClientKeyValue() {
        $objIntegrationClientKeyValue = new CIntegrationClientKeyValue();
        $objIntegrationClientKeyValue->setCid( $this->m_intCid );
        $objIntegrationClientKeyValue->setPropertyId( $this->m_intPropertyId );
        $objIntegrationClientKeyValue->setPropertyFloorplanId( $this->m_intId );
        $objIntegrationClientKeyValue->setKey( 'REMOTE_WAITING_LIST_UNIT_ID' );
        return $objIntegrationClientKeyValue;
    }

    public function createFloorplanMedia() {
    	$objFloorplanMedia = new CFloorplanMedia();
    	$objFloorplanMedia->setCid( $this->m_intCid );
    	$objFloorplanMedia->setPropertyFloorplanId( $this->m_intId );
    	return $objFloorplanMedia;
    }

    /**
	* Get Or Fetch Functions
	*
	*/

    public function getOrFetchProperty( $objDatabase ) {
    	if( false == isset( $this->m_objProperty ) || false == valObj( $this->m_objProperty, 'CProperty' ) ) {
    		$this->m_objProperty = $this->fetchProperty( $objDatabase );
    	}
    	return $this->m_objProperty;
    }

	public function getOrFetchUnitTypes( $objDatabase ) {
		if( false == valArr( $this->m_arrobjUnitTypes ) ) {
			$this->m_arrobjUnitTypes = CUnitTypes::createService()->fetchUnitTypesByPropertyFloorPlanIdByPropertyIdByCid( $this->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
		}
		return $this->m_arrobjUnitTypes;
	}

	public function getOrFetchUnitTypesWithPropertyIds( $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $this->m_arrobjUnitTypes ) ) {
			$this->m_arrobjUnitTypes = CUnitTypes::createService()->fetchUnitTypesByPropertyFloorPlanIdByPropertyIdsByCid( $this->getId(), $arrintPropertyIds, $this->getCid(), $objDatabase );
		}

		return $this->m_arrobjUnitTypes;
	}

    /**
	* Fetch Functions
	*
	*/

	public function fetchIntegratedAssociatedUnitTypes( $objDatabase ) {
		return CUnitTypes::createService()->fetchIntegratedAssociatedUnitTypesByPropertyIdByPropertyFloorPlanIdByCid( $this->getPropertyId(), $this->getId(), $this->getCid(), $objDatabase );
	}

    public function fetchFloorplanPreferences( $objDatabase ) {
		return \Psi\Eos\Entrata\CFloorplanPreferences::createService()->fetchFloorplanPreferencesByPropertyFloorplanIdKeyedByKeyByCid( $this->getId(), $this->getCid(), $objDatabase );
    }

	public function fetchClient( $objDatabase ) {
		return \Psi\Eos\Entrata\CClients::createService()->fetchClientById( $this->m_intCid, $objDatabase );
	}

	public function fetchIntegrationClientKeyValueByKey( $strKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValueByPropertyFloorplanIdByKeyByCid( $this->m_intId, $strKey, $this->getCid(), $objDatabase );
	}

	public function fetchApplications( $objDatabase ) {
		return \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationsByPropertyFloorplanIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchAvailableUnitSpacesByLeaseStartDate( $strLeaseStartDate, $arrobjPropertyPreferences, $intApplicationId, $objDatabase, $boolFetchAllUnits = false, $objFloorplanAvailabilityFilter = NULL ) {
		return CUnitSpaces::createService()->fetchAvailableUnitSpacesByPropertyIdByPropertyFloorplanIdByLeaseStartDateByCid( $this->getPropertyId(), $this->m_intId, $strLeaseStartDate, $arrobjPropertyPreferences, $intApplicationId, $this->getCid(), $objDatabase, $boolFetchAllUnits, $objFloorplanAvailabilityFilter );
	}

	public function fetchAvailableUnitSpacesByFloorplanAvailabilityFilterByPropertyPreferences( $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $objDatabase, $boolIsViewFloorplan = false ) {
		$objFloorplanAvailabilityFilter->setCid( $this->getCid() );
		$objFloorplanAvailabilityFilter->setPropertyFloorplanIds( [ $this->getId() ] );

		if( false == is_null( $objFloorplanAvailabilityFilter->getPropertyIds() ) ) {
			if( false == in_array( $this->getPropertyId(), $objFloorplanAvailabilityFilter->getPropertyIds() ) ) {
				$arrintPropertyIds = array_merge( [ $this->getPropertyId() ], $objFloorplanAvailabilityFilter->getPropertyIds() );
				$objFloorplanAvailabilityFilter->setPropertyIds( $arrintPropertyIds );
			}
		} else {
			$objFloorplanAvailabilityFilter->setPropertyIds( [ $this->getPropertyId() ] );
		}

		return $this->fetchCalculatedUnitSpacesByFloorplanAvailabilityFilterByPropertyPreferences( $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $objDatabase, $boolIsViewFloorplan );
	}

	public function fetchCompanyMediaFile( $objDatabase ) {
		return $this->m_objFloorplanMarketingMediaAssociation = CCompanyMediaFiles::createService()->fetchCompanyMediaFileByFloorplanIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFileById( $intId, $objDatabase ) {
		return $this->m_objFloorplanMarketingMediaAssociation = CCompanyMediaFiles::createService()->fetchCompanyMediaFileByIdByFloorplanIdByCid( $intId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFiles( $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByFloorplanIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitType( $objDatabase, $intPropertyId = NULL ) {
		if( true == is_null( $intPropertyId ) ) {
			$intPropertyId = $this->getPropertyId();
		}
		return CUnitTypes::createService()->fetchUnitTypeByPropertyFloorPlanIdByIdPropertyIdByCid( $this->getId(), $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchUnitTypes( $objDatabase ) {
		return CUnitTypes::createService()->fetchUnitTypesByPropertyFloorPlanIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpaces( $arrobjPropertyPreferences, $objDatabase ) {
		return CUnitSpaces::createService()->fetchAvailableUnitSpacesByPropertyIdByPropertyFloorplanIdByLeaseStartDateByCid( $this->getPropertyId(), $this->getId(), NULL, $arrobjPropertyPreferences, NULL, $this->getCid(), $objDatabase );
	}

	public function fetchApplicationDuplicates( $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicationDuplicates::createService()->fetchApplicationDuplicatesByPropertyFloorplanIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

    public function fetchProperty( $objDatabase ) {
    	return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
    }

    public function fetchPropertyFloorplanGroups( $objDatabase ) {
     	return \Psi\Eos\Entrata\CPropertyFloorplanGroups::createService()->fetchPropertyFloorplanGroupsByPropertyFloorplanIdByCid( $this->getId(), $this->getCid(), $objDatabase );
    }

   	public function fetchPropertyUnitsCount( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitsCountByPropertyFloorplanIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchUnitTypesCount( $objDatabase ) {
		return CUnitTypes::createService()->fetchUnitTypesCountByPropertyFloorplanIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnits( $objDatabase ) {
		$this->m_arrobjPropertyUnits = \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitsByPropertyFloorplanIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
		return $this->m_arrobjPropertyUnits;
	}

	public function fetchCalculatedUnitSpacesByFloorplanAvailabilityFilterByPropertyPreferences( $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $objDatabase, $boolIsViewFloorplan = false ) {
		$objFloorplanAvailabilityFilter->setCid( $this->getCid() );
		return CUnitSpaces::createService()->fetchCalculatedUnitSpacesByPropertyIdByPropertyFloorplanIdByFloorplanAvailabilityFilterByPropertyPreferences( $this->getPropertyId(), $this->getId(), $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $objDatabase, $intChildPropertyId = NULL, $boolConsiderAvailabilitySettings = false, $boolReturnSql = false, $boolFetchSpecialList = false, $boolFetchAmenitiesList = false, $boolIsViewFloorplan );
	}

	public function fetchActiveLeases( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchActiveLeasesByPropertyFloorplanIdByPropertyIdByLeaseStatusTypeIdsByCid( $this->m_intId, $this->getPropertyId(), [ CLeaseStatusType::FUTURE, CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ],  $this->getCid(), $objDatabase );
	}

    /**
	* Validation Functions
	*
	*/

    public function valFloorplanName() {
        $boolIsValid = true;
        if( false == isset( $this->m_strFloorplanName ) || ( 0 == strlen( $this->m_strFloorplanName ) ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'floorplan_name', __( 'Floorplan name is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valMinSquareFeet() {
        $boolIsValid = true;
		if( true == isset( $this->m_fltMinSquareFeet ) ) {
			if( false == is_numeric( $this->m_fltMinSquareFeet ) || 0 >= $this->m_fltMinSquareFeet ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_square_feet', __( 'Minimum square feet must be blank or greater than zero.' ) ) );
			}
		}
        return $boolIsValid;
    }

    public function valMaxSquareFeet() {
        $boolIsValid = true;
		if( true == isset( $this->m_fltMaxSquareFeet ) ) {
			if( false == is_numeric( $this->m_fltMaxSquareFeet ) || 0 >= $this->m_fltMaxSquareFeet ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_square_feet', __( 'Maximum square feet must be blank or greater than zero.' ) ) );
			}

			if( true == $boolIsValid && true == isset( $this->m_fltMinSquareFeet ) && true == is_numeric( $this->m_fltMinSquareFeet ) && $this->m_fltMinSquareFeet > $this->m_fltMaxSquareFeet ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_square_feet', __( 'Maximum square feet must be greater than minimum square feet.' ) ) );
			}
		}
        return $boolIsValid;
    }

    public function valNumberOfFloors() {
        $boolIsValid = true;
		if( true == isset( $this->m_intNumberOfFloors ) ) {
			if( false == is_int( $this->m_intNumberOfFloors ) || ( 0 >= $this->m_intNumberOfFloors ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_floors', __( 'Number of bedrooms must be blank or greater than zero.' ) ) );
			}
		}
        return $boolIsValid;
    }

    public function valNumberOfBedrooms() {
        $boolIsValid = true;
		if( true == isset( $this->m_intNumberOfBedrooms ) ) {
			if( false == is_int( $this->m_intNumberOfBedrooms ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bedrooms', __( 'Number of bedrooms must be integer.' ) ) );
			}
		} else {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bedrooms', __( 'Number of bedrooms is required.' ) ) );
    	}
        return $boolIsValid;
    }

    public function valNumberOfBathrooms() {
        $boolIsValid = true;
    	if( false == is_null( $this->m_fltNumberOfBathrooms ) ) {
    		if( true == is_numeric( $this->m_fltNumberOfBathrooms ) ) {
    			if( 0 > $this->m_fltNumberOfBathrooms ) {
    				$boolIsValid = false;
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms must be blank or greater than zero' ) ) );

    			} elseif( false == in_array( fmod( $this->m_fltNumberOfBathrooms, 1 ), [ 0, 0.25, 0.5, 0.75 ] ) ) {
    				$boolIsValid = false;
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
    			}
    		} else {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
    		}
    	} else {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms is required.' ) ) );
    	}
        return $boolIsValid;
    }

    public function valMinRent() {
        $boolIsValid = true;
		if( true == isset( $this->m_fltMinRent ) ) {
			if( false == is_numeric( $this->m_fltMinRent ) || 0 >= $this->m_fltMinRent ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_rent', __( 'Minimum rent must be blank or greater than zero.' ) ) );
			}
		}
        return $boolIsValid;
    }

    public function valMaxRent() {
        $boolIsValid = true;
		if( true == isset( $this->m_fltMaxRent ) ) {
			if( false == is_numeric( $this->m_fltMaxRent ) || 0 >= $this->m_fltMaxRent ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_rent', __( 'Maximum rent must be blank or greater than zero.' ) ) );
			}

			if( true == $boolIsValid && true == isset( $this->m_fltMinRent ) && true == is_numeric( $this->m_fltMinRent ) && $this->m_fltMinRent > $this->m_fltMaxRent ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_rent', __( 'Maximum rent must be greater than minimum rent.' ) ) );
			}
		}
        return $boolIsValid;
    }

    public function valMinDeposit() {
        $boolIsValid = true;
		if( true == isset( $this->m_fltMinDeposit ) ) {
			if( false == is_null( $this->m_fltMinDeposit ) && 0 > $this->m_fltMinDeposit ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_rent', __( 'Minimum deposit must greater than or equal to zero.' ) ) );
			}
		}
        return $boolIsValid;
    }

    public function valMaxDeposit() {
        $boolIsValid = true;
		if( true == isset( $this->m_fltMaxDeposit ) ) {
			if( false == is_null( $this->m_fltMaxDeposit ) && 0 > $this->m_fltMaxDeposit ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_deposit', __( 'Maximum deposit must be greater than or equal to zero.' ) ) );
			}

			if( true == $boolIsValid && true == isset( $this->m_fltMinDeposit ) && true == is_numeric( $this->m_fltMinDeposit ) && $this->m_fltMinDeposit > $this->m_fltMaxDeposit ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_deposit', __( 'Maximum deposit must be greater than minimum deposit.' ) ) );
			}
		}
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;
        switch( $strAction ) {
            case VALIDATE_UPDATE:
				// $boolIsValid &= $this->valId();
				$boolIsValid &= $this->valFloorplanName();
				$boolIsValid &= $this->valNumberOfFloors();

				if( Self::CATCH_ALL_FLOOR_PLAN != $this->m_strFloorplanName ) {
					$boolIsValid &= $this->valNumberOfBedrooms();
					$boolIsValid &= $this->valNumberOfBathrooms();
				}

				$boolIsValid &= $this->valMinSquareFeet();
				$boolIsValid &= $this->valMaxSquareFeet();
				$boolIsValid &= $this->valMinRent();
				$boolIsValid &= $this->valMaxRent();
				$boolIsValid &= $this->valMinDeposit();
				$boolIsValid &= $this->valMaxDeposit();
				break;

            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valFloorplanName();
				$boolIsValid &= $this->valNumberOfFloors();
				$boolIsValid &= $this->valNumberOfBedrooms();
				$boolIsValid &= $this->valNumberOfBathrooms();
				$boolIsValid &= $this->valMinSquareFeet();
				$boolIsValid &= $this->valMaxSquareFeet();
				$boolIsValid &= $this->valMinRent();
				$boolIsValid &= $this->valMaxRent();
				$boolIsValid &= $this->valMinDeposit();
				$boolIsValid &= $this->valMaxDeposit();
				break;

            case VALIDATE_DELETE:
				// $boolIsValid &= $this->valId();
                break;

            default:
               	$boolIsValid = true;
               	break;
        }

        return $boolIsValid;
    }

    /**
	* Other Functions
	*
	*/

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
    	if( true == $boolReturnSqlOnly ) {
    		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	}

		if( false == parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) return false;
		$arrstrData = fetchData( 'SELECT order_num FROM property_floorplans WHERE id = ' . $this->getId() . ' AND cid = ' . $this->getCid(), $objDatabase );
		if( true == isset( $arrstrData[0]['order_num'] ) ) {
			$this->setOrderNum( $arrstrData[0]['order_num'] );
		}

		return true;
    }

	public function importPropertyUnits( $objDatabase, $boolForceIntegration = false ) {
		$boolIsOptimizedCall	= false;
		$objProperty = $this->getOrFetchProperty( $objDatabase );
		if( false == valObj( $objProperty, 'CProperty' ) || true == is_null( $objProperty->getRemotePrimaryKey() ) ) return true;
		$objIntegrationDatabase 		= $objProperty->fetchIntegrationDatabase( $objDatabase );
		$boolIsRealPageBatchExecution 	= false;

		if( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && CIntegrationClientType::REAL_PAGE == $objIntegrationDatabase->getIntegrationClientTypeId() ) {
			$boolIsRealPageBatchExecution = true;
		}

		$arrobjIntegrationClients = \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientsByPropertyIdByIntegrationServiceIdsByCid( $objProperty->getId(), [ CIntegrationService::RETRIEVE_UNIT_TYPES_OPTIMIZED_PRICING, CIntegrationService::RETRIEVE_UNIT_TYPES_AVAILABILITY_AND_PRICING ], $objProperty->getCid(), $objDatabase );

		$arrobjIntegrationClients = rekeyObjects( 'IntegrationServiceId', $arrobjIntegrationClients );
		$objIntegrationClient     = getArrayElementByKey( CIntegrationService::RETRIEVE_UNIT_TYPES_OPTIMIZED_PRICING, $arrobjIntegrationClients );

		if( true == valObj( $objIntegrationClient, 'CIntegrationClient' ) && CIntegrationClientStatusType::ACTIVE == $objIntegrationClient->getIntegrationClientStatusTypeId() ) {
			$boolIsOptimizedCall = true;
		}

		if( false == $boolIsRealPageBatchExecution && false == $boolForceIntegration && false == $boolIsOptimizedCall ) {
			$intIntegrationClients = \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientsByPropertyIdByIntegrationServiceIdsByIntegrationPeriodIdsByCid( $objProperty->getId(), [ CIntegrationService::RETRIEVE_PROPERTY_UNITS ], [ CIntegrationPeriod::HOURLY ], $objProperty->getCid(), $objDatabase );

			if( CIntegrationService::RETRIEVE_PROPERTY_UNITS == $intIntegrationClients ) return true;
		}

		$arrobjIntegratedUnitTypes 	= [];

		if( false == $boolIsRealPageBatchExecution ) {
			$arrobjIntegratedUnitTypes = $this->fetchIntegratedAssociatedUnitTypes( $objDatabase );
			if( false == valArr( $arrobjIntegratedUnitTypes ) ) return true;
		}

	    $strInterval = '';
		if( true == $boolIsOptimizedCall ) {
		    $intIntegrationServiceId = CIntegrationService::RETRIEVE_UNIT_TYPES_OPTIMIZED_PRICING;
			$strInterval = '10 minutes';
		} else {
		    $intIntegrationServiceId = CIntegrationService::RETRIEVE_UNIT_TYPES_AVAILABILITY_AND_PRICING;
		    $strInterval = '2 minutes';
		}

		$boolIsExecute = $this->lastUpdatedData( $intIntegrationServiceId, $objDatabase, $strInterval );
		if( false == $boolIsExecute ) {
		    return true;
		}

		$objIntegrationClient = getArrayElementByKey( $intIntegrationServiceId, $arrobjIntegrationClients );

	    if( false == valObj( $objIntegrationClient, 'CIntegrationClient' ) ) return true;

		$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->getCid(), $intIntegrationServiceId, SYSTEM_USER_ID, $objDatabase );

		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setDatabase( $objDatabase );
		$objGenericWorker->setPropertyFloorplan( $this );
		$objGenericWorker->setCallTimeLimit( 3 );

		$objGenericWorker->setUnitTypes( $arrobjIntegratedUnitTypes );
		$objGenericWorker->process();

		return true;
	}

	public function lastUpdatedData( $intIntegrationServiceId, $objDatabase, $strInterval = '' ) {

	    if( true == empty( $strInterval ) ) {
	       return false;
	    }

		// Don't update pricing if updated in last 10 min
		$strSql = ' SELECT
						MAX( created_on ),
						CASE WHEN MAX( created_on ) IS NULL OR ( NOW() - INTERVAL \'' . $strInterval . '\' ) > MAX( created_on )
							THEN 1
							ELSE 0
						END as is_ready_to_re_process
					FROM
						integration_results
					WHERE
						integration_service_id = ' . ( int ) $intIntegrationServiceId . '
						AND cid = ' . ( int ) $this->getCid() . '
						AND property_id = ' . ( int ) $this->getPropertyId() . '
						AND reference_number = ' . ( int ) $this->getId() . '
						AND is_failed = 0;';

		$arrstrMixedData = fetchData( $strSql, $objDatabase );

		if( ( false == valArr( $arrstrMixedData ) || ( true == isset( $arrstrMixedData[0]['is_ready_to_re_process'] ) && 1 == $arrstrMixedData[0]['is_ready_to_re_process'] ) ) ) {
			return true;
		}

		return false;
	}

	public function disassociateData( $intCompanyUserId, $objDatabase ) {
		$arrobjPropertyUnits = ( array ) \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitsByPropertyFloorplanIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		foreach( $arrobjPropertyUnits as $objPropertyUnit ) {
			$objPropertyUnit->setPropertyFloorplanId( NULL );

			if( false == $objPropertyUnit->update( $intCompanyUserId, $objDatabase ) ) {
				trigger_error( 'Client: ' . $this->getCid() . ' : [Property ID : ' . $this->getPropertyId() . '] Property Unit ID[' . $objPropertyUnit->getId() . '] failed to update.', E_USER_WARNING );
				return false;
			}
		}

		$arrobjApplications = ( array ) $this->fetchApplications( $objDatabase );
		foreach( $arrobjApplications as $objApplication ) {
			if( 0 < $objApplication->getPropertyFloorplanId() ) {
				$objEventLibrary = new CEventLibrary();
				$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
				$objEventLibraryDataObject->setDatabase( $objDatabase );

				$objEvent = $objEventLibrary->createEvent( [ $objApplication ], CEventType::NOTES );

				$objEvent->setCid( $objApplication->getCid() );
				$objEvent->setEventDatetime( 'NOW()' );
				$objEvent->setIpAddress( $_SERVER['REMOTE_ADDR'] );
				$objEvent->setDataReferenceId( NULL );
				$objEvent->setDoNotExport( true );
				$objEventLibraryDataObject->setData( [ 'old_floorplan_name' => $this->getFloorplanName() ] );
				$objEventLibrary->buildEventDescription( $objApplication );
				$objEventLibrary->buildEventSystemNotes( $objApplication );
				$objEventLibrary->setIsExportEvent( false );

				$objApplication->setPropertyFloorplanId( NULL );

				if( false == $objApplication->update( $intCompanyUserId, $objDatabase ) ) {
					trigger_error( 'Application : ' . $objApplication->getId() . ' : failed to update.', E_USER_WARNING );
					return false;
				}

				if( false == $objEventLibrary->insertEvent( $intCompanyUserId, $objDatabase ) ) {
					trigger_error( 'Failed to create event', E_USER_WARNING );
					return false;
				}
			}
		}

		$arrobjApplicationDuplicates = ( array ) $this->fetchApplicationDuplicates( $objDatabase );
		foreach( $arrobjApplicationDuplicates as $objApplicationDuplicate ) {
			$objApplicationDuplicate->setPropertyFloorplanId( NULL );
			if( false == $objApplicationDuplicate->update( $intCompanyUserId, $objDatabase ) ) {
				trigger_error( 'Contact : ' . $objApplicationDuplicate->getId() . ' : failed to update.', E_USER_WARNING );
				return false;
			}
		}

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( ' NOW() ' );

		return $this->update( $intCurrentUserId, $objDatabase );
	}

	public function fetchActiveMarketingMediaAssociationByMediaSubTypeId( $intMarketingMediaSubType, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::FLOORPLAN_MEDIA, $intMarketingMediaSubType, $this->getCid(), $objDatabase );
	}

	public function fetchActiveMarketingMediaAssociationsByMediaSubTypeIds( $arrintMarketingMediaSubTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdsByMediaTypeIdByMediaSubTypeIdsByCid( [ $this->getId() ], CMarketingMediaType::FLOORPLAN_MEDIA, $arrintMarketingMediaSubTypeIds, $this->getCid(), $objDatabase, 'mma.order_num ASC, mma.id ASC' );
	}

	public function fetchDefaultFloorPlanMarketingMediaAssociation( $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdsByDetailsFieldByCid( $this->getId(), CMarketingMediaType::FLOORPLAN_MEDIA, [ CMarketingMediaSubType::FLOORPLAN_2D, CMarketingMediaSubType::FLOORPLAN_3D ], 'is_default', [ 1 ], $this->getCid(), $objDatabase );
	}

	public function fetchFloorplanActiveMarketingMediaAssociation( $objDatabase, $strOrderBy = 'mma.order_num ASC, mma.id ASC' ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdsByCid( $this->getId(), CMarketingMediaType::FLOORPLAN_MEDIA, [ CMarketingMediaSubType::FLOORPLAN_2D, CMarketingMediaSubType::FLOORPLAN_3D ], $this->getCid(), $objDatabase, $strOrderBy );
	}

	public function fetchFloorplanActiveMarketingMediaAssociations( $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdsByMediaTypeIdByMediaSubTypeIdsByCid( [ $this->getId() ], CMarketingMediaType::FLOORPLAN_MEDIA, [ CMarketingMediaSubType::FLOORPLAN_2D, CMarketingMediaSubType::FLOORPLAN_3D, CMarketingMediaSubType::FLOORPLAN_PDF ], $this->getCid(), $objDatabase, 'mma.order_num ASC, mma.id ASC' );
	}

}
?>