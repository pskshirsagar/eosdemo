<?php

class CTrafficCookie extends CBaseTrafficCookie {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCookie() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLastVisitedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
				break;

            default:
        }

        return $boolIsValid;
    }
}
?>