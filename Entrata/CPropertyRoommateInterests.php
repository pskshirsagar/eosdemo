<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyRoommateInterests
 * Do not add any new functions to this class.
 */

class CPropertyRoommateInterests extends CBasePropertyRoommateInterests {

	public static function fetchMaxOrderNumByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						MAX( order_num ) AS max_order_num
					FROM
						property_roommate_interests
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchColumn( $strSql, 'max_order_num', $objDatabase );
	}

	public static function fetchPropertyRoommateInterestByRoommateInterestIdByPropertyIdByCid( $intRoommateInterestId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intRoommateInterestId ) || false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_roommate_interests
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND	roommate_interest_id = ' . ( int ) $intRoommateInterestId;

		return self::fetchPropertyRoommateInterest( $strSql, $objDatabase );
	}

	public static function fetchPropertyRoommateInterestsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_roommate_interests
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
					ORDER BY
						order_num';

		return self::fetchPropertyRoommateInterests( $strSql, $objDatabase );
	}

	public static function fetchPropertyIdsWithMaxOrderNumByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = '	SELECT
						property_id,
						MAX( order_num ) AS max_order_num
					FROM
						property_roommate_interests
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN (' . implode( $arrintPropertyIds, ',' ) . ')
					GROUP BY
						property_id';

		$arrintPropertyIdsWithMaxOrderNum = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintPropertyIdsWithMaxOrderNum ) ) return NULL;

		$arrintFormattedPropertyIdsWithMaxOrderNum = [];

		foreach( $arrintPropertyIdsWithMaxOrderNum as $arrintPropertyIdWithMaxOrderNum ) {
			$arrintFormattedPropertyIdsWithMaxOrderNum[$arrintPropertyIdWithMaxOrderNum['property_id']] = $arrintPropertyIdWithMaxOrderNum['max_order_num'];
		}

		return $arrintFormattedPropertyIdsWithMaxOrderNum;
	}

	public static function fetchIsPublishedWithMatchingWeightsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						roommate_interest_id,
						is_published,
						matching_weight
				  	FROM
						property_roommate_interests
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
					ORDER BY
						order_num';

		$arrintRoommateInterestIdsWithIsPublished = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintRoommateInterestIdsWithIsPublished ) ) return NULL;

		$arrintFormattedRoommateInterestIdsWithIsPublished = [];

		foreach( $arrintRoommateInterestIdsWithIsPublished as $arrintRoommateInterestIdWithIsPublished ) {
			$arrintFormattedRoommateInterestIdsWithIsPublished[$arrintRoommateInterestIdWithIsPublished['roommate_interest_id']] = [ 'is_published' => $arrintRoommateInterestIdWithIsPublished['is_published'], 'matching_weight' => $arrintRoommateInterestIdWithIsPublished['matching_weight'] ];
		}
		return $arrintFormattedRoommateInterestIdsWithIsPublished;
	}

	public static function fetchPropertiesCountByRoommateInterestIdsByCid( $arrintRoommateInterestIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintRoommateInterestIds ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						COUNT( DISTINCT( pri.property_id ) ) as count,
						pri.roommate_interest_id as id
					FROM
						properties p
						JOIN property_products pp ON ( pp.cid = p.cid AND ( pp.property_id = p.id OR pp.property_id IS NULL ) )
						JOIN property_roommate_interests pri ON ( pri.cid = p.cid AND pri.property_id = p.id ) 
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids ) 
						AND pp.ps_product_id = ' . CPsProduct::ENTRATA . '
						AND pri.roommate_interest_id IN (  ' . implode( ', ', $arrintRoommateInterestIds ) . ')
					GROUP BY
						pri.roommate_interest_id';

		$arrintRoommateInterestsCount = fetchData( $strSql, $objDatabase );

		$arrintFormattedRoommateInterestsCount = [];

		if( true == valArr( $arrintRoommateInterestsCount ) ) {
			foreach( $arrintRoommateInterestsCount as $arrstrRoommmateInterestCount ) {
				$arrintFormattedRoommateInterestsCount[$arrstrRoommmateInterestCount['id']] = $arrstrRoommmateInterestCount['count'];
			}
		}

		return $arrintFormattedRoommateInterestsCount;
	}

	public static function fetchPropertyRoommateInterestsByInputTypeIdsByPropertyIdByCid( $arrintInputTypeIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintInputTypeIds ) || false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						pri.property_id,
						pri.roommate_interest_id,
						ri.input_type_id,
						ri.roommate_interest_category_id,
						ri.system_code,
						rio.system_code as option_system_code,
						ri.question,
						ric.name as category_name,
						rio.id as roommate_interest_option_id,
						rio.answer,
						rio.system_code as roommate_interest_option_system_code
					FROM
						property_roommate_interests pri
						JOIN roommate_interests ri ON ( ri.cid = pri.cid AND ri.id = pri.roommate_interest_id )
						JOIN roommate_interest_categories ric ON ( ric.id = ri.roommate_interest_category_id )
						LEFT JOIN roommate_interest_options rio ON ( rio.cid = pri.cid AND rio.roommate_interest_id = pri.roommate_interest_id )
					WHERE
						pri.cid = ' . ( int ) $intCid . '
						AND pri.property_id =  ' . ( int ) $intPropertyId . '
						AND pri.is_published = 1
						AND ri.input_type_id IN ( ' . implode( ',', $arrintInputTypeIds ) . ' )
					ORDER BY
						pri.order_num,
						rio.order_num';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAssociatedPropertyIdsByRoommateInterestIdByPropertyIdsByCid( $intRoommateInterestId, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						pri.property_id
					FROM
						property_roommate_interests pri
					WHERE
						pri.cid = ' . ( int ) $intCid . '
						AND pri.roommate_interest_id = ' . ( int ) $intRoommateInterestId . '
						AND pri.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';

		$arrintAssociatedPropertyIds = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintAssociatedPropertyIds ) ) return NULL;

		$arrintFormattedPropertyIds = [];

		foreach( $arrintAssociatedPropertyIds as $arrintAssociatedPropertyId ) {
			$arrintFormattedPropertyIds[] = $arrintAssociatedPropertyId['property_id'];
		}

		return $arrintFormattedPropertyIds;
	}

	public static function fetchIsPublishedWithMatchingWeightsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						p.id AS property_id,
					    p.occupancy_type_ids,
					    pri.roommate_interest_id,
					     CASE WHEN ( ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids ) AND pp.value = \'1\' ) THEN 1 ELSE 0 END AS is_student_semester_selection_enabled
				  	FROM
				  	    properties p
                        LEFT JOIN property_roommate_interests pri ON ( p.id = pri.property_id AND p.cid = pri.cid AND pri.is_published = 1 )
                        LEFT JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.key = \'ENABLE_SEMESTER_SELECTION\' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN( ' . implode( ',', $arrintPropertyIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

}
?>