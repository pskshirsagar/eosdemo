<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReviewResponseTemplates
 * Do not add any new functions to this class.
 */

class CReviewResponseTemplates extends CBaseReviewResponseTemplates {

	public static function fetchActiveReviewResponseTemplates( $objDatabase, $strReviewResponseSearchKeywords = NULL ) {
		$strFilterSql	= NULL;
		$strOrderBy		= ' ORDER BY order_num';

		if( true == valStr( $strReviewResponseSearchKeywords ) ) {
			$strFilterSql = ' AND LOWER( subject ) LIKE LOWER( \'%' . $strReviewResponseSearchKeywords . '%\' )';
		}

		$strSql = 'SELECT 
					*
					FROM
						review_response_templates
					WHERE
						deleted_by IS NULL
						AND deleted_on IS NULL ' . $strFilterSql . $strOrderBy;

		return self::fetchReviewResponseTemplates( $strSql, $objDatabase );
	}

	public static function fetchPreExistingReviewResponseTemplateBySubject( $strSubject, $objDatabase, $intReviewResponseTemplateId = NULL ) {
		$strWhereSql = NULL;

		if( true == valId( $intReviewResponseTemplateId ) ) {
			$strWhereSql = ' AND id <> ' . ( int ) $intReviewResponseTemplateId;
		}

		$strSql = 'SELECT 
					*
					FROM
						review_response_templates
					WHERE
						LOWER( subject ) = ' . '\'' . \Psi\CStringService::singleton()->strtolower( $strSubject ) . '\' 
						AND deleted_by IS NULL 
						AND deleted_on IS NULL ' . $strWhereSql;

		return self::fetchReviewResponseTemplate( $strSql, $objDatabase );
	}

}
?>