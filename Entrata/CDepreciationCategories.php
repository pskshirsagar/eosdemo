<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDepreciationCategories
 * Do not add any new functions to this class.
 */

class CDepreciationCategories extends CBaseDepreciationCategories {

	public static function fetchActiveDepreciationCategoriesByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( dc.id ) dc.id,
						dc.name,
						dc.useful_life_months,
						util_get_translated( \'name\', gat_accum.name, gat_accum.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS accum_account_name,
						gat_accum.formatted_account_number AS accum_formatted_account_number,
						util_get_translated( \'name\', gat_depreciation.name, gat_depreciation.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS depreciation_account_name,
						gat_depreciation.formatted_account_number AS depreciation_formatted_account_number,
						util_get_system_translated( \'name\', trp.name, trp.details ,\'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS tax_recovery_period_name,
						dc.is_depreciable
					FROM
						depreciation_categories dc
						LEFT JOIN gl_account_trees gat_accum ON ( dc.cid = gat_accum.cid AND dc.accum_depreciation_gl_account_id = gat_accum.gl_account_id AND gat_accum.is_default = 1 )
						LEFT JOIN gl_account_trees gat_depreciation ON (dc.cid = gat_depreciation.cid  AND dc.depreciation_gl_account_id = gat_depreciation.gl_account_id AND gat_depreciation.is_default = 1 )
						LEFT JOIN tax_recovery_periods trp ON ( dc.tax_recovery_period_id = trp.id )
					WHERE
						dc.cid = ' . ( int ) $intCid . '
						AND dc.deleted_by IS NULL
					ORDER BY
						dc.id DESC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAllDepreciationCategoriesByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						depreciation_categories
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_published = TRUE
						AND	deleted_by IS NULL
					ORDER BY
						name';

		return self::fetchDepreciationCategories( $strSql, $objClientDatabase );
	}

	public static function fetchDepreciationCategoryByIdByCid( $intId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						depreciation_categories
					WHERE
						id = ' . ( int ) $intId . '
						AND cid = ' . ( int ) $intCid . '
						AND is_published = TRUE
						AND	deleted_by IS NULL
					ORDER BY
						name';

		return self::fetchDepreciationCategory( $strSql, $objClientDatabase );
	}

	public static function fetchDepreciationCategoriesByNamesByCid( $arrstrNames, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrstrNames ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						depreciation_categories
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_published = TRUE
						AND	deleted_by IS NULL
						AND LOWER ( name ) IN ( \'' . implode( '\',\'', $arrstrNames ) . '\' )';

		return self::fetchDepreciationCategories( $strSql, $objClientDatabase );
	}

	public static function fetchDepreciationCategoriesByFieldNamesByCid( $arrstrFieldNames, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrstrFieldNames ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						' . implode( ',', $arrstrFieldNames ) . '
					FROM
						depreciation_categories
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND is_published = TRUE
					ORDER BY
						LOWER( name )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchDepreciationCategoriesByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {

		if( false == ( $arrintIds = getIntValuesFromArr( $arrintIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						depreciation_categories
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . sqlIntImplode( $arrintIds ) . ' )
						AND is_published IS TRUE
						AND	deleted_by IS NULL';

		return self::fetchDepreciationCategories( $strSql, $objClientDatabase );
	}

}

?>