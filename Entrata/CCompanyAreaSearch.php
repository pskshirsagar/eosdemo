<?php

class CCompanyAreaSearch extends CBaseCompanyAreaSearch {

    public function valCid() {
        $boolIsValid = true;

      if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is required' ) ) );
      }

        return $boolIsValid;
    }

    public function valCompanyAreaId() {
        $boolIsValid = true;

      if( true == is_null( $this->getCompanyAreaId() ) ) {
          $boolIsValid = false;
          $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_area_id', __( 'Company area id is required.' ) ) );
      }

        return $boolIsValid;
    }

    public function valName( $objDatabase = NULL ) {

        $boolIsValid = true;

       if( true == is_null( $this->getName() ) ) {
           $boolIsValid &= false;
           $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
       } else {
        	$intConflictingCompanyAreaSearchCount = \Psi\Eos\Entrata\CCompanyAreaSearches::createService()->fetchConflictingCompanyAreaSearchCountByCid( $this, $objDatabase );

        	if( 0 < $intConflictingCompanyAreaSearchCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is already in use.' ) ) );
				$boolIsValid = false;
        	}
	   }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getIsPublished() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
        // }

        return $boolIsValid;
    }

   public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valName( $objDatabase );
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
   }

}
?>