<?php

class CPropertySubsidyDetail extends CBasePropertySubsidyDetail {

	const FULL_TRANSFER    = 'Full';
	const PARTIAL_TRANSFER = 'Partial';

	protected $m_strParentCompanyDunsNumber;
	protected $m_strSubsidyTracsVersionName;
	protected $m_strSenderStreetAddress;
	protected $m_strParentCompanyTin;
	protected $m_strSenderCityName;
	protected $m_strOwnerDunsNumber;
	protected $m_strContractNumber;
	protected $m_strSenderZipCode;
	protected $m_strProjectImaxId;
	protected $m_strSenderState;
	protected $m_strSubsidyType;
	protected $m_strSenderName;
	protected $m_strOwnerTin;

	// FixMe: Entire additional TICs generation logic needs be get refactored.
	protected $m_intTexasStudentTaxCreditFileTypeId;
	private $m_intMissouriExhibitIUnitFileTypeId;
	protected $m_intNumberOfMat10;
	protected $m_intNumberOfMat40;
	protected $m_intNumberOfMat65;
	protected $m_intNumberOfMat70;

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );
		if( true == isset( $arrmixValues['parent_company_duns_number'] ) ) 			$this->setParentCompanyDunsNumber( $arrmixValues['parent_company_duns_number'] );
		if( true == isset( $arrmixValues['sender_street_address'] ) ) 				$this->setSenderStreetAddress( $arrmixValues['sender_street_address'] );
		if( true == isset( $arrmixValues['parent_company_tin'] ) )					$this->setParentCompanyTin( $arrmixValues['parent_company_tin'] );
		if( true == isset( $arrmixValues['sender_city_name'] ) ) 					$this->setSenderCityName( $arrmixValues['sender_city_name'] );
		if( true == isset( $arrmixValues['owner_duns_number'] ) ) 					$this->setOwnerDunsNumber( $arrmixValues['owner_duns_number'] );
		if( true == isset( $arrmixValues['contract_number'] ) ) 					$this->setContractNumber( $arrmixValues['contract_number'] );
		if( true == isset( $arrmixValues['sender_zip_code'] ) ) 					$this->setSenderZipCode( $arrmixValues['sender_zip_code'] );
		if( true == isset( $arrmixValues['project_imax_id'] ) ) 					$this->setProjectImaxId( $arrmixValues['project_imax_id'] );
		if( true == isset( $arrmixValues['sender_state'] ) ) 						$this->setSenderState( $arrmixValues['sender_state'] );
		if( true == isset( $arrmixValues['subsidy_type'] ) ) 						$this->setSubsidyType( $arrmixValues['subsidy_type'] );
		if( true == isset( $arrmixValues['sender_name'] ) ) 						$this->setSenderName( $arrmixValues['sender_name'] );
		if( true == isset( $arrmixValues['owner_tin'] ) ) 							$this->setOwnerTin( $arrmixValues['owner_tin'] );
		if( true == isset( $arrmixValues['subsidy_tracs_version_name'] ) ) 			$this->setSubsidyTracsVersionName( $arrmixValues['subsidy_tracs_version_name'] );
		// FixMe: Entire additional TICs generation logic needs be get refactored.
		if( true == isset( $arrmixValues['texas_student_certification_tic_file_type_id'] ) ) $this->setTexasStudentTaxCreditFileTypeId( $arrmixValues['texas_student_certification_tic_file_type_id'] );
		if( true == isset( $arrmixValues['missouri_exhibit_iunit_file_type_id'] ) ) $this->setMissouriExhibitIUnitFileTypeId( $arrmixValues['missouri_exhibit_iunit_file_type_id'] );

		return;
	}

	public function setParentCompanyDunsNumber( $strParentCompanyDunsNumber ) {
		$this->m_strParentCompanyDunsNumber = $strParentCompanyDunsNumber;
	}

	public function setSenderStreetAddress( $strSenderStreetAddress ) {
		$this->m_strSenderStreetAddress = $strSenderStreetAddress;
	}

	public function setParentCompanyTin( $strParentCompanyTin ) {
		$this->m_strParentCompanyTin = $strParentCompanyTin;
	}

	public function setSenderCityName( $strSenderCityName ) {
		$this->m_strSenderCityName = $strSenderCityName;
	}

	public function setOwnerDunsNumber( $strOwnerDunsNumber ) {
		$this->m_strOwnerDunsNumber = $strOwnerDunsNumber;
	}

	public function setContractNumber( $strContractNumber ) {
		$this->m_strContractNumber = $strContractNumber;
	}

	public function setSenderZipCode( $strSenderZipCode ) {
		$this->m_strSenderZipCode = $strSenderZipCode;
	}

	public function setProjectImaxId( $strProjectImaxId ) {
		$this->m_strProjectImaxId = $strProjectImaxId;
	}

	public function setSenderState( $strSenderState ) {
		$this->m_strSenderState = $strSenderState;
	}

	public function setSubsidyType( $strSubsidyType ) {
		$this->m_strSubsidyType = $strSubsidyType;
	}

	public function setSenderName( $strSenderName ) {
		$this->m_strSenderName = $strSenderName;
	}

	public function setOwnerTin( $strOwnerTin ) {
		$this->m_strOwnerTin = $strOwnerTin;
	}

	public function setSubsidyTracsVersionName( $strSubsidyTracsVersionName ) {
		$this->m_strSubsidyTracsVersionName = $strSubsidyTracsVersionName;
	}

	// FixMe: Entire additional TICs generation logic needs be get refactored.

	public function setTexasStudentTaxCreditFileTypeId( $intTexasStudentTaxCreditFileTypeId ) {
		$this->m_intTexasStudentTaxCreditFileTypeId = $intTexasStudentTaxCreditFileTypeId;
	}

	// FixMe: Entire additional TICs generation logic needs be get refactored.

	public function setMissouriExhibitIUnitFileTypeId( $intMissouriExhibitIUnitFileTypeId ) {
		$this->m_intMissouriExhibitIUnitFileTypeId = $intMissouriExhibitIUnitFileTypeId;
	}

	public function setNumberOfMat10( $intNumberOfMat10 ) {
		$this->m_intNumberOfMat10 = $intNumberOfMat10;
	}

	public function setNumberOfMat40( $intNumberOfMat40 ) {
		$this->m_intNumberOfMat40 = $intNumberOfMat40;
	}

	public function setNumberOfMat65( $intNumberOfMat65 ) {
		$this->m_intNumberOfMat65 = $intNumberOfMat65;
	}

	public function setNumberOfMat70( $intNumberOfMat70 ) {
		$this->m_intNumberOfMat70 = $intNumberOfMat70;
	}

	/**
	 * get Functions
	 *
	 */

	public function getParentCompanyDunsNumber() {
		return $this->m_strParentCompanyDunsNumber;
	}

	public function getSenderStreetAddress() {
		return $this->m_strSenderStreetAddress;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getParentCompanyTin() {
		return $this->m_strParentCompanyTin;
	}

	public function getSenderCityName() {
		return $this->m_strSenderCityName;
	}

	public function getOwnerDunsNumber() {
		return $this->m_strOwnerDunsNumber;
	}

	public function getContractNumber() {
		return $this->m_strContractNumber;
	}

	public function getSenderZipCode() {
		return $this->m_strSenderZipCode;
	}

	public function getProjectImaxId() {
		return $this->m_strProjectImaxId;
	}

	public function getSenderState() {
		return $this->m_strSenderState;
	}

	public function getSubsidyType() {
		return $this->m_strSubsidyType;
	}

	public function getSenderName() {
		return $this->m_strSenderName;
	}

	public function getOwnerTin() {
		return $this->m_strOwnerTin;
	}

	public function getSubsidyTracsVersionName() {
		return $this->m_strSubsidyTracsVersionName;
	}

	// FixMe: Entire additional TICs generation logic needs be get refactored.

	public function getTexasStudentTaxCreditFileTypeId() {
		return $this->m_intTexasStudentTaxCreditFileTypeId;
	}

	// FixMe: Entire additional TICs generation logic needs be get refactored.

	public function getMissouriExhibitIUnitFileTypeId() {
		return $this->m_intMissouriExhibitIUnitFileTypeId;
	}

	public function getNumberOfMat10() {
		return $this->m_intNumberOfMat10;
	}

	public function getNumberOfMat40() {
		return $this->m_intNumberOfMat40;
	}

	public function getNumberOfMat65() {
		return $this->m_intNumberOfMat65;
	}

	public function getNumberOfMat70() {
		return $this->m_intNumberOfMat70;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyRecertificationTypeId( $intSubsidyTypeId, $objDatabase ) {
		$boolIsValid = true;

		$intSubsidizedPropertyId = \Psi\Eos\Entrata\CPropertySubsidyTypes::createService()->fetchPropertyIdByPropertyIdBySubsidyTypeId( $this->getPropertyId(), $intSubsidyTypeId, $this->getCid(), $objDatabase );

		switch( $intSubsidyTypeId ) {

			case CSubsidyType::HUD:
				$boolIsValid &= $this->valHudSubsidyRecertificationTypeId( $intSubsidizedPropertyId );
				break;

			case CSubsidyType::TAX_CREDIT:
				$boolIsValid &= $this->valTaxCreditSubsidyRecertificationTypeId( $intSubsidizedPropertyId );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function valHudSubsidyRecertificationTypeId( $intHudPropertyId ) {
		$boolIsValid = true;

		if( true == valId( $intHudPropertyId ) && false == valStr( $this->getHudSubsidyRecertificationTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' Annual Recertification Timing is required for HUD subsidy type property.' ) );
		}

		if( true == valId( $this->getHudSubsidyRecertificationTypeId() ) && false == in_array( $this->getHudSubsidyRecertificationTypeId(), \CSubsidyRecertificationType::$c_arrintHudSubsidyRecertificationTypes ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' Valid Recertification Type is required for HUD subsidy type property.' ) );
		}

		return $boolIsValid;
	}

	public function valTaxCreditSubsidyRecertificationTypeId( $intTaxCreditPropertyId ) {
		$boolIsValid = true;

		if( true == valId( $intTaxCreditPropertyId ) && false == valStr( $this->getTaxCreditSubsidyRecertificationTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' Annual Recertification Timing is required for Tax Credit subsidy type property.' ) );
		}

		if( true == valId( $this->getTaxCreditSubsidyRecertificationTypeId() ) && false == in_array( $this->getTaxCreditSubsidyRecertificationTypeId(), \CSubsidyRecertificationType::$c_arrintTaxCreditSubsidyRecertificationTypes ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' Valid Recertification Type is required for Tax Credit subsidy type property.' ) );
		}

		return $boolIsValid;
	}

	public function valHometSubsidyRecertificationTypeId( $intHomePropertyId ) {
		$boolIsValid = true;

		if( true == valId( $intHomePropertyId ) && false == valStr( $this->getHomeSubsidyRecertificationTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' Annual Recertification Timing is required for Home subsidy type property.' ) );
		}

		if( true == valId( $this->getHomeSubsidyRecertificationTypeId() ) && false == in_array( $this->getHomeSubsidyRecertificationTypeId(), \CSubsidyRecertificationType::$c_arrintHudSubsidyRecertificationTypes ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' Valid Recertification Type is required for Home subsidy type property.' ) );
		}

		return $boolIsValid;
	}

	public function valTaxCreditAnnualRecertification() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHmfaCode() {
		$boolIsValid = true;
		if( 3 > strlen( $this->getHmfaCode() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hmfa code', ' Please select metro area first, under "Income Limits" tab.' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valHudProjectName() {
		$boolIsValid = true;

		if( true == valStr( $this->getHudProjectName() ) ) {
			if( 35 < strlen( trim( $this->getHudProjectName() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hud_project_name', ' Project name should be less than or equal to 35 characters.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valHudProjectNumber( $objDatabase ) {
		$boolIsValid = true;

		$intPropertyId = \Psi\Eos\Entrata\CSubsidyContracts::createService()->fetchPropertyIdByPropertyIdBySubsidyContractTypeIds( $this->getPropertyId(), CSubsidyContractType::$c_arrintProjectNumberSubsidyContractTypes, $this->getCid(), $objDatabase );

		if( true == valId( $intPropertyId ) && ( false == valStr( $this->getHudProjectNumber() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hud_project_number', ' Project Number is required.' ) );
		}

		if( true == valStr( $this->getHudProjectNumber() ) ) {
			if( false == ctype_alnum( $this->getHudProjectNumber() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hud_project_number', ' Project number should be alphanumeric value.' ) );
			} elseif( 8 != strlen( trim( $this->getHudProjectNumber() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hud_project_number', ' Project number must be 8 characters.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valHudProjectImaxIdEncrypted( $intHudPropertyId ) {
		$boolIsValid = true;
		if( true == valId( $intHudPropertyId ) && ( false == valStr( $this->getHudProjectImaxIdEncrypted() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' Project iMAX ID is required.' ) );
		}

		if( true == valStr( $this->getHudProjectImaxIdEncrypted() ) ) {
			if( false == ctype_alnum( $this->getHudProjectImaxIdEncrypted() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Project iMAX ID should be integer value.' ) );
			} elseif( 5 != strlen( $this->getHudProjectImaxIdEncrypted() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Project iMAX ID must be 5 digits.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valHudProjectImaxPasswordEncrypted() {
		$boolIsValid = true;

		if( false == valStr( $this->getHudProjectImaxPasswordEncrypted() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' Project iMAX password is required.' ) );
		}
		return $boolIsValid;
	}

	public function valHudRecipientImaxIdEncrypted( $intHudPropertyId ) {
		$boolIsValid = true;

		if( true == valId( $intHudPropertyId ) && ( false == valStr( $this->getHudRecipientImaxIdEncrypted() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' Recipient iMAX ID is required.' ) );
		}

		if( true == valStr( $this->getHudRecipientImaxIdEncrypted() ) ) {
			if( false == ctype_alnum( $this->getHudRecipientImaxIdEncrypted() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Recipient iMAX ID should be integer value.' ) );
			} elseif( 5 != strlen( $this->getHudRecipientImaxIdEncrypted() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Recipient iMAX ID must be 5 digits.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valHomeProgramStartAndEndDate() {
		$boolIsValid = true;

		if( false == valStr( $this->getHomeStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'HOME Program start date required.' ) );
		}

		if( false == valStr( $this->getHomeEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'HOME Program end date required.' ) );
		}

		if( valStr( $this->getHomeStartDate() ) && valStr( $this->getHomeEndDate() ) ) {
			if( strtotime( $this->getHomeStartDate() ) > strtotime( $this->getHomeEndDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' Home program start date must be less than end date.' ) );
			}
		}
		return $boolIsValid;
	}

	// FixMe: Entire additional TICs generation logic needs be get refactored.

	public function valTenantIncomeCertification( $intCompanyUserId, $objDatabase ) {
		$boolIsValid = true;

		if( true == valId( $this->getFileTypeId() ) && false == $this->isDocumentExists( $this->getFileTypeId(), $objDatabase ) ) {
			$boolIsValid &= $this->validateDocumentFunctionExists( $this->getFileTypeId(), $intCompanyUserId, $objDatabase );
		}

		if( true == valId( $this->getTexasStudentTaxCreditFileTypeId() ) && false == $this->isDocumentExists( $this->getTexasStudentTaxCreditFileTypeId(), $objDatabase ) ) {
			$boolIsValid &= $this->validateDocumentFunctionExists( $this->getTexasStudentTaxCreditFileTypeId(), $intCompanyUserId, $objDatabase );
		}

		if( true == valId( $this->getMissouriExhibitIUnitFileTypeId() ) && false == $this->isDocumentExists( $this->getMissouriExhibitIUnitFileTypeId(), $objDatabase ) ) {
			$boolIsValid &= $this->validateDocumentFunctionExists( $this->getMissouriExhibitIUnitFileTypeId(), $intCompanyUserId, $objDatabase );
		}

		return $boolIsValid;

	}

	private function validateDocumentFunctionExists( $intFileTypeId, $intCompanyUserId, $objDatabase ) {

		$strDocumentName = $this->generateDocumentName( $intFileTypeId, $objDatabase );

		if( true == valStr( $strDocumentName ) && false == $this->isFunctionExists( $strDocumentName, $objDatabase ) ) {
			$this->setFileTypeId( NULL );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_type_id', ' Function to create document with name ' . addslashes( $strDocumentName ) . '() does not exists.' ) );
			return false;
		}

		return $this->insertTicDocument( $strDocumentName, $intCompanyUserId, $objDatabase );
	}

	private function isFunctionExists( $strDocumentName, $objDatabase ) {

		$strSql = 'SELECT routine_name FROM information_schema.routines WHERE routine_schema = \'public\' AND routine_name = \'' . trim( $strDocumentName ) . '\';';

		return valArr( fetchData( $strSql, $objDatabase ) );

	}

	private function insertTicDocument( $strDocumentName, $intCompanyUserId, $objDatabase ) {

		$strSql = 'SELECT * FROM ' . $strDocumentName . '( ' . ( int ) $this->getCid() . ', ' . ( int ) $intCompanyUserId . ' )';

		return $this->executeDataSQL( $strSql, $objDatabase );

	}

	private function executeDataSQL( $strSql, $objDatabase ) {

		$objDataset = $objDatabase->createDataset();

		if( true == $objDataset->execute( $strSql ) ) {
			return true;
		}

		$objDataset->cleanup();
		return false;
	}

	public function validate( $strAction, $intCompanyUserId = NULL, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'tax_credit_property_info':
				$boolIsValid &= $this->valTenantIncomeCertification( $intCompanyUserId, $objDatabase );
				$boolIsValid &= $this->valHmfaCode();
				// @TODO: Put necessary validations here
				break;

			case 'hud_property_info':
				$intHudPropertyId = \Psi\Eos\Entrata\CPropertySubsidyTypes::createService()->fetchPropertyIdByPropertyIdBySubsidyTypeId( $this->getPropertyId(), CSubsidyType::HUD, $this->getCid(), $objDatabase );
				$boolIsValid &= $this->valHudProjectName();
				$boolIsValid &= $this->valHudProjectNumber( $objDatabase );
				$boolIsValid &= $this->valHudProjectImaxIdEncrypted( $intHudPropertyId );
				$boolIsValid &= $this->valHudProjectImaxPasswordEncrypted();
				$boolIsValid &= $this->valHudRecipientImaxIdEncrypted( $intHudPropertyId );
				break;

			case 'hud_certification':
				$boolIsValid &= $this->valSubsidyRecertificationTypeId( CSubsidyType::HUD, $objDatabase );
				break;

			case 'tax_credit_certification':
				$boolIsValid &= $this->valSubsidyRecertificationTypeId( CSubsidyType::TAX_CREDIT, $objDatabase );
				break;

			case 'home_property_info':
				$boolIsValid &= $this->valHomeProgramStartAndEndDate();
				break;

			case 'home_certification':
				$boolIsValid &= $this->valHometSubsidyRecertificationTypeId( CSubsidyType::HOME, $objDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	private function generateDocumentName( $intFileTypeId, $objDatabase ) {

		$arrmixFileTypes = CFileTypes::fetchFileTypesSystemCodeByIdsByCid( [ $intFileTypeId ], $this->getCid(), $objDatabase );

		if( false == valArr( $arrmixFileTypes ) ) return false;

		return \Psi\CStringService::singleton()->strtolower( ' document_' . current( $arrmixFileTypes )['name'] . '_insert' );

	}

	private function isDocumentExists( $intFileTypeId, $objDatabase ) {

		$intDocumentCount = CDocuments::fetchDocumentByFileTypeIdByOccupancyTypeIdByCid( $intFileTypeId, COccupancyType::AFFORDABLE, $this->getCid(), $objDatabase );

		return valId( $intDocumentCount );

	}

}
?>