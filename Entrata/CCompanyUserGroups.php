<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserGroups
 * Do not add any new functions to this class.
 */

class CCompanyUserGroups extends CBaseCompanyUserGroups {

	public static function fetchCompanyUserPreferencesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserPreferences( sprintf( 'SELECT * FROM company_user_preferences WHERE company_user_id = %d AND cid = %d ', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserPreferencesByKeyByCid( $strKey, $intCid, $objDatabase ) {
		return self::fetchCompanyUserPreferences( sprintf( 'SELECT * FROM company_user_preferences WHERE key = \'%s\' AND cid = %d ', $strKey, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserGroupsByCompanyUserIdsByCid( $arrintCompanyUserIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCompanyUserIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM company_user_groups WHERE cid = ' . ( int ) $intCid . ' AND company_user_id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )';

		return self::fetchCompanyUserGroups( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserGroupsByCompanyUserIdsByCidByFieldNames( $arrintCompanyUserIds, $intCid, $arrstrFieldNames, $objDatabase ) {

		if( false == valArr( $arrintCompanyUserIds ) ) return;

		$strSql = 'SELECT ' . implode( ',',  $arrstrFieldNames ) . ' FROM company_user_groups WHERE cid = ' . ( int ) $intCid . ' AND company_user_id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserGroupsByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT cug.*
						FROM
							company_groups cg,
							company_user_groups cug
						WHERE
							cg.id = cug.company_group_id
							AND cg.cid = cug.cid
							AND integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
							AND cug.cid = ' . ( int ) $intCid;
		return self::fetchCompanyUserGroups( $strSql, $objDatabase );

	}

	public static function fetchCompanyUserGroupsWithIntegrationDatabaseIdNullByCid( $intCid, $objDatabase ) {

			$strSql = 'SELECT cug.*
							FROM
								company_groups cg,
								company_user_groups cug
							WHERE
								cg.id = cug.company_group_id
								AND cg.cid = cug.cid
								AND integration_database_id IS NULL
								AND cug.cid = ' . ( int ) $intCid;

			return self::fetchCompanyUserGroups( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserCountByGroupIdByCid( $intGroupId, $intCid, $objDatabase ) {

		$strSql = ' WHERE cid = ' . ( int ) $intCid . ' AND company_group_id = ' . ( int ) $intGroupId;
		return self::fetchCompanyUserGroupCount( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserGroupsCountByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = 'SELECT count(id) FROM company_user_groups WHERE cid = ' . ( int ) $intCid . ' AND company_user_id = ' . ( int ) $intCompanyUserId;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchSortedCompanyUserGroupsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						company_user_groups
					WHERE company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cid = ' . ( int ) $intCid . ' ORDER BY id ';

		return self::fetchCompanyUserGroups( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserGroupsByIdsByCid( $arrintCompanyUserGroupIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						company_user_groups
					WHERE id IN ( ' . implode( ',', $arrintCompanyUserGroupIds ) . ' )
						AND cid = ' . ( int ) $intCid;

 		return self::fetchCompanyUserGroups( $strSql, $objDatabase );
	}

	public static function fetchEnabledCompanyUserCountByGroupIdByCid( $intGroupId, $intCid, $objDatabase ) {

		$strSql = 'SELECT 
						count( 1 ) AS count 
					FROM
						company_user_groups cug 
						JOIN company_users cu ON ( cug.company_user_id = cu.id AND cug.cid = cu.cid )
					WHERE
						cug.cid = ' . ( int ) $intCid . '
						AND cug.company_group_id = ' . ( int ) $intGroupId . ' 
						AND cu.is_disabled = 0';

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchCompanyUserIdsByCompanyGroupIdsByCid( $arrintCompanyGroupIds,  $intCid, $objDatabase ) {
		$strWhereSql = '';

		if( true == valArr( $arrintCompanyGroupIds ) ) {
			$strWhereSql = ' AND company_group_id IN ( ' . implode( ',', $arrintCompanyGroupIds ) . ' )';
		}

		$strSql = 'SELECT
						cu.id AS company_user_id
					FROM
						company_users cu
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND cu.is_administrator = 0
						AND cu.is_disabled = 0
						AND EXISTS (
									SELECT
										1
									FROM
										company_user_permissions cup
									WHERE
										cup.cid = cu.cid
										AND cup.company_user_id = cu.id
										AND ( cup.is_allowed = 1
										OR cup.is_inherited = 0 )
						)
						AND EXISTS (
									SELECT
										1
									FROM
										company_user_groups cug
									WHERE
										cug.cid = cu.cid
										AND cug.company_user_id = cu.id ' . $strWhereSql . '
						)';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserGroupsCompanyUsersByCompanyUserIdsByCid( $arrintCompanyUserIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCompanyUserIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						cug.id AS company_user_group_id,
						cu.id AS company_user_id,
						cu.company_employee_id,
						cg.id AS company_group_id,
						cu.company_user_type_id,
						cu.username AS company_user_name,
						cu.is_administrator,
						ce.name_first,
						ce.name_last,
						util_get_translated( \'name\', cg.name, cg.details ) AS company_group_name
					FROM
						company_user_groups cug
					LEFT JOIN company_groups cg ON ( cug.cid = cg.cid AND cug.company_group_id = cg.id )
					LEFT JOIN company_users cu ON ( cug.cid = cu.cid AND cug.company_user_id = cu.id )
					LEFT JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
					WHERE
						cug.cid = ' . ( int ) $intCid . '
						AND cug.company_user_id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )
					ORDER BY cug.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserGroupsByCompanyGroupIdByHelpResourceIdByCid( $intCompanyGroupId, $intHelpResourceId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						assigned_group_courses.*
					FROM
						(
							SELECT
								cug.company_user_id,
								cga.id,
								cga.days_alotted,
								ROW_NUMBER ( ) OVER ( PARTITION BY cug.company_user_id ORDER BY cug.created_on ) AS user_assigned_to_group_order
							FROM
								company_user_groups cug
								JOIN company_group_assessments cga ON ( cug.cid = cga.cid AND cug.company_group_id = cga.company_group_id AND cga.help_resource_id = ' . ( int ) $intHelpResourceId . ' AND cga.deleted_on IS NULL AND cga.deleted_by IS NULL AND cug.cid = ' . ( int ) $intCid . ' )
							WHERE
								cug.company_user_id IN (
															SELECT
																DISTINCT ( cug.company_user_id )
															FROM
																company_user_groups cug
															WHERE
																cug.company_group_id = ' . ( int ) $intCompanyGroupId . '
																AND cug.cid = ' . ( int ) $intCid . '
								)
								AND cug.company_group_id != ' . ( int ) $intCompanyGroupId . '
							GROUP BY
								cug.company_user_id,
								cga.id,
								cga.days_alotted,
								cug.created_on
						) assigned_group_courses
					WHERE
						assigned_group_courses.user_assigned_to_group_order = 1';

		return fetchData( $strSql, $objDatabase );
	}

}
?>