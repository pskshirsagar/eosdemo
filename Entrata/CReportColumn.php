<?php

class CReportColumn extends CBaseReportColumn {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportDatasetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valColumnKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGroupName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $strAction, $objDatabase ) {
		$boolIsValid = true;
		if( true == empty( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Column Name is required.</br>' ) );
		} else {
			$strWhere = '
					WHERE
						name = \'' . pg_escape_string( $this->m_strName ) . '\' 
						AND column_key = \'' . pg_escape_string( $this->m_strColumnKey ) . '\'';

			$strWhere .= ' AND report_dataset_id = ' . $this->m_intReportDatasetId . '
							AND deleted_by IS NULL';

			if( VALIDATE_UPDATE == $strAction ) {
				$strWhere .= ' AND id != ' . $this->m_intId;
			}

			if( false == is_null( $objDatabase ) ) {
				$intExistingReportColumnCount = \Psi\Eos\Entrata\CReportColumns::createService()->fetchReportColumnCount( $strWhere, $objDatabase );
				if( 0 < $intExistingReportColumnCount ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Column with ' . $this->m_strName . ' Name and ' . $this->m_strColumnKey . ' Key already exists.' ) );
				}

			}
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			$boolIsValid &= $this->valName( $strAction, $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>