<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueCalculators
 * Do not add any new functions to this class.
 */

class CRevenueCalculators extends CBaseRevenueCalculators {

	public static function fetchRevenueCalculatorsVersionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = $strSql = sprintf( 'SELECT 
							rc.id,
							rc.name
					FROM revenue_calculators rc
					WHERE 
					    rc.cid = %d
					    AND rc.property_id = %d
					    ORDER BY rc.name
					', $intCid, $intPropertyId );

		return fetchData( $strSql, $objDatabase );
	}

}
?>