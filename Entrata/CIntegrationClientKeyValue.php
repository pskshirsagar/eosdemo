<?php

class CIntegrationClientKeyValue extends CBaseIntegrationClientKeyValue {

	protected $m_strPropertyName;
	protected $m_strUnitTypeName;

    /**
     * Set Functions
     */

    public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

    	if( isset( $arrstrValues['property_name'] ) && $boolDirectSet ) $this->m_strPropertyName = trim( $arrstrValues['property_name'] );
    	elseif ( isset( $arrstrValues['property_name'] ) ) $this->setPropertyName( $arrstrValues['property_name'] );
    	if( isset( $arrstrValues['unit_type_name'] ) && $boolDirectSet ) $this->m_strUnitTypeName = trim( $arrstrValues['unit_type_name'] );
    	elseif ( isset( $arrstrValues['unit_type_name'] ) ) $this->setUnitTypeName( $arrstrValues['unit_type_name'] );
    }

    /**
     * Validation Functions
     */

    public function valKey() {
        $boolIsValid = true;

        if( true == is_null( $this->getKey() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Key is required.' ) );
        }

        return $boolIsValid;
    }

    public function valValue() {
        $boolIsValid = true;

        if( true == is_null( $this->getValue() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Value is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getPropertyId() ) && 'CHECK_SERVER_LOAD' != $this->getKey() ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Company Property is required.' ) );
    	}

    	return $boolIsValid;
    }

	public function valIntegrationDatabaseId() {
		$boolIsValid = true;

		if( true == is_null( $this->getIntegrationDatabaseId() ) && 'CHECK_SERVER_LOAD' == $this->getKey() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integration_database_id', 'Integration database is required.' ) );
		}

		return $boolIsValid;
	}

    public function setPropertyName( $strPropertyName ) {
    	$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, 64, NULL, true );
    }

    public function getPropertyName() {
    	return $this->m_strPropertyName;
    }

    public function setUnitTypeName( $strUnitTypeName ) {
    	$this->m_strUnitTypeName = CStrings::strTrimDef( $strUnitTypeName, 64, NULL, true );
    }

    public function getUnitTypeName() {
    	return $this->m_strUnitTypeName;
    }

    public function valDuplicates( $objDatabase ) {
        $boolIsValid = true;

        if( true == valObj( $objDatabase, 'CDatabase' ) ) {

        	if( false == is_null( $this->m_strKey ) && false == is_null( $this->m_strValue ) && false == is_null( $this->m_intPropertyId ) ) {
        		$strWhereSql = ' WHERE
        							cid = ' . $this->m_intCid . '
        							AND key = \'' . $this->m_strKey . '\'
        							AND value = \'' . $this->m_strValue . '\'
        							AND  property_id = ' . $this->m_intPropertyId . '
        						LIMIT 1';

        		$intIntegrationClientKeyValueCount = \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValueCount( $strWhereSql, $objDatabase );

        		if( 0 < $intIntegrationClientKeyValueCount ) {
        			$boolIsValid = false;
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'A record already exists with the same key, value and property for this company.' ) );
        		}
        	}
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valKey();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valIntegrationDatabaseId();
            	$boolIsValid &= $this->valValue();
            	$boolIsValid &= $this->valDuplicates( $objDatabase );
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valKey();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valIntegrationDatabaseId();
            	$boolIsValid &= $this->valValue();
            	$boolIsValid &= $this->valDuplicates( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>