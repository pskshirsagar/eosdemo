<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningMessageTypes
 * Do not add any new functions to this class.
 */

class CScreeningMessageTypes extends CBaseScreeningMessageTypes {

	public static function fetchScreeningMessageTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningMessageType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningMessageType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningMessageType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllPublishedScreeningMessageTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_message_types
					WHERE 
						is_published = TRUE';

		return  parent::fetchScreeningMessageTypes( $strSql, $objDatabase );
	}

}
?>