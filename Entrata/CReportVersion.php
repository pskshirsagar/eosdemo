<?php

class CReportVersion extends CBaseReportVersion {

	// region Run on Replica

	const NEVER = 'never';
	const HIGH_LATENCY = 'high_latency';
	const LOW_LATENCY = 'low_latency';

	// endregion

	private $m_arrmixDefinition;

	/*
	 * Getters
	 */

	public function getDefinitionField( $strFieldName ) {
		if( false == isset( $this->m_arrmixDefinition ) ) {
			$this->decodeDefinition();
		}

		return ( true == isset( $this->m_arrmixDefinition[$strFieldName] ) ? $this->m_arrmixDefinition[$strFieldName] : NULL );
	}

	public function getClass() {
		return $this->getDefinitionField( 'class' );
	}

	public function getRunOnReplicas() {
		return $this->getDefinitionField( 'run_on_replicas' ) ?: self::NEVER;
	}

	public function getUrl() {
		return $this->getDefinitionField( 'url' );
	}

	public function getReportXml() {
		return $this->getDefinitionField( 'report_xml' );
	}

	public function getDocumentId() {
		return $this->getDefinitionField( 'document_id' );
	}

	public function getCuid() {
		return $this->getDefinitionField( 'cuid' );
	}

	public function getValidationCallbacks() {
		return $this->getDefinitionField( 'validation_callbacks' ) ?: [];
	}

	public function getAllowOnlyAdmin() {
		return $this->getDefinitionField( 'allow_only_admin' );
	}

	public function getAllowOnlyPsiAdmin() {
		return $this->getDefinitionField( 'allow_only_psi_admin' );
	}

	public function getIsSchedulable() {
		return $this->getDefinitionField( 'is_schedulable' );
	}

	/*
	 * Setters
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		// We need to handle the definition before the parent method, so that escape sequences are not removed from the JSON and it is never direct set
		if( true == isset( $arrmixValues['definition'] ) ) {
			$this->setDefinition( $arrmixValues['definition'] );
			unset( $arrmixValues['definition'] );
		}

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

	public function setDefinition( $strDefinition ) {
		$this->m_strDefinition = $strDefinition;
		$this->decodeDefinition();
	}

	protected function setDefinitionField( $strFieldName, $mixValue ) {
		$this->m_arrmixDefinition[$strFieldName] = $mixValue;
		$this->encodeDefinition();
		return $this;
	}

	public function setDocumentId( $intDocumentId ) {
		$this->setDefinitionField( 'document_id', $intDocumentId );
		return $this;
	}

	public function setCuid( $intCuId ) {
		$this->setDefinitionField( 'cuid', $intCuId );
		return $this;
	}

	/*
	 * Validations
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultReportVersionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMajor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitleAddendum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefinition() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExpiration() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsLatest() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDefault() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/*
	 * Other Functions
	 */

	protected function decodeDefinition() {
		$this->m_arrmixDefinition = json_decode( $this->m_strDefinition, true );
	}

	protected function encodeDefinition() {
		$this->m_strDefinition = json_encode( $this->m_arrmixDefinition );
	}

}
