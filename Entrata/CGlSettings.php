<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlSettings
 * Do not add any new functions to this class.
 */

class CGlSettings extends CBaseGlSettings {

	public static function fetchGlSettingsByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM gl_settings WHERE cid = ' . ( int ) $intCid;
		return self::fetchGlSetting( $strSql, $objDatabase );
	}

	public static function fetchGlSettingsCountForGlAccountUsageTypeValidationByGlAccountIdByCid( $intGlAccountId, $intCid, $objClientDatabase ) {
		if( false == is_numeric( $intGlAccountId ) ) return 0;

		$strJoinFields = '	gs.ap_gl_account_id,
							gs.retained_earnings_gl_account_id,
							gs.market_rent_gl_account_id,
							gs.gain_to_lease_gl_account_id,
							gs.loss_to_lease_gl_account_id,
							gs.vacancy_loss_gl_account_id';

		$strCondition = '(
							gs.ap_gl_account_id = ' . ( int ) $intGlAccountId . '
							AND ga.gl_account_usage_type_id = ' . ( int ) CGlAccountUsageType::ACCOUNTS_PAYABLE . '
						) OR (
							gs.retained_earnings_gl_account_id = ' . ( int ) $intGlAccountId . '
							AND ga.gl_account_usage_type_id = ' . ( int ) CGlAccountUsageType::RETAINED_EARNINGS . '
						) OR (
							gs.market_rent_gl_account_id = ' . ( int ) $intGlAccountId . '
							AND ga.gl_account_usage_type_id = ' . ( int ) CGlAccountUsageType::RENT . '
						) OR (
							gs.gain_to_lease_gl_account_id = ' . ( int ) $intGlAccountId . '
							AND ga.gl_account_usage_type_id = ' . ( int ) CGlAccountUsageType::GAIN_LOSS_ON_LEAVE . '
						) OR (
							gs.loss_to_lease_gl_account_id = ' . ( int ) $intGlAccountId . '
							AND ga.gl_account_usage_type_id = ' . ( int ) CGlAccountUsageType::GAIN_LOSS_ON_LEAVE . '
						) OR (
							gs.vacancy_loss_gl_account_id = ' . ( int ) $intGlAccountId . '
							AND ga.gl_account_usage_type_id = ' . ( int ) CGlAccountUsageType::VACANCY_LOSS . '
						)';

		$strSql = 'SELECT
						count( gs.id )
					FROM
						gl_settings gs
						JOIN gl_accounts ga ON ( gs.cid = ga.cid AND ga.id IN ( ' . $strJoinFields . ' ) )
					WHERE
						gs.cid = ' . ( int ) $intCid . '
						AND ( ' . $strCondition . ' )';

		$arrstrData = fetchData( $strSql, $objClientDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchInterCompanyArGlAccountIdByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT inter_company_ar_gl_account_id FROM gl_settings WHERE cid = ' . ( int ) $intCid;

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['inter_company_ar_gl_account_id'];
	}

	public static function fetchRetentionPayableGlAccountIdByCid( $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT 
						gs.retention_payable_gl_account_id
					FROM gl_settings gs
						JOIN company_preferences cp ON (cp.cid = gs.cid AND cp.key = \'USE_RETENTION\' AND cp.value = 1::TEXT )
					WHERE
						gs.cid = ' . ( int ) $intCid;

		return parent::fetchColumn( $strSql, 'retention_payable_gl_account_id', $objDatabase );
	}

}
?>