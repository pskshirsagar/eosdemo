<?php

class CShippingVendor extends CBaseShippingVendor {

	protected $m_strFullsizeUri;

	public function setFullsizeUri( $strFullsizeUri ) {
		$this->m_strFullsizeUri = CStrings::strTrimDef( $strFullsizeUri, NULL, NULL, true );
	}

	public function getFullsizeUri() {
		return $this->m_strFullsizeUri;
	}

	public function valVendorName() {
		$boolIsValid = true;
		if( false == valStr( $this->m_strVendorName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_name', __( 'Please enter vendor name.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valIcon() {
		$boolIsValid = true;

		if( false == $this->m_boolIsVendorIcon && 1 == $this->m_intCompanyMediaFileId ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_media_file_id', __( 'Please choose vendor icon.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valColor() {
		$boolIsValid = true;
		if( false == valStr( $this->m_strColor ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'color', __( 'Please enter color.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valVendorName();
				$boolIsValid &= $this->valColor();
				$boolIsValid &= $this->valIcon();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['fullsize_uri'] ) && $boolDirectSet ) {
			$this->m_strFullsizeUri = trim( $arrmixValues['fullsize_uri'] );
		} elseif( isset( $arrmixValues['fullsize_uri'] ) ) {
			$this->setFullsizeUri( $arrmixValues['fullsize_uri'] );
		}
	}

}
?>