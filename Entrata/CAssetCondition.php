<?php

class CAssetCondition extends CBaseAssetCondition {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultAssetConditionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objClientDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
		}

		if( true == $boolIsValid ) {

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND LOWER( name ) = LOWER( \'' . addslashes( $this->getName() ) . '\' )
							AND id <> ' . ( int ) $this->getId() . '
							AND deleted_by IS NULL';

			if( 0 < \Psi\Eos\Entrata\CAssetConditions::createService()->fetchAssetConditionCount( $strWhere, $objClientDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "Name '{%s, 0}' already exists.", [ $this->getName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objClientDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objClientDatabase );
				$boolIsValid &= $this->isAssetConditionAssociatedWithAssets( $objClientDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->isAssetConditionAssociatedWithAssets( $objClientDatabase );
				break;

			default:
				$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function isAssetConditionAssociatedWithAssets( $objClientDatabase ) {

		$boolIsValid = true;

		if( true == $this->getIsDisabled() || false == is_null( $this->getDeletedBy() ) ) {
			$intAssetDetailsCount = ( int ) \Psi\Eos\Entrata\CAssetDetails::createService()->fetchAssetDetailsCountByAssetConditionIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );

			if( 0 != $intAssetDetailsCount ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'This asset condition is associated with one or more assets. Please reassign the asset(s) before deleting or disabling this asset condition.' ) ) );
			}
		}

		return $boolIsValid;
	}

}
?>