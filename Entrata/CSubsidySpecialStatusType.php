<?php

class CSubsidySpecialStatusType extends CBaseSubsidySpecialStatusType {

	const ELDERLY_HEAD_SPOUSE_OR_CO_HEAD		= 1;
	const FULL_TIME_STUDENT						= 2;
	const DISABLED								= 3;
	const US_MILITARY_VETERAN					= 4;
	const HOUSED_TEMPORARILY_DUE_TO_DISASTER	= 5;
	const FAMILY_SELF_SUFFICIENCY_PARTICIPANT	= 6;
	const PART_TIME_STUDENT						= 7;
	const POLICE_OR_SECURITY_OFFICER			= 8;

	const DISABLED_CODE							= 'H';

	public static $c_arrintHUDSpecificSubsidySpecialStatusTypeIds = [
		self::HOUSED_TEMPORARILY_DUE_TO_DISASTER => self::HOUSED_TEMPORARILY_DUE_TO_DISASTER,
		self::POLICE_OR_SECURITY_OFFICER => self::POLICE_OR_SECURITY_OFFICER
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>