<?php

class CPropertyDetail extends CBasePropertyDetail {

	protected $m_strRouteName;
	protected $m_strGlTreeName;
	protected $m_strPropertyName;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_strPhoneNumber;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;

	protected $m_intGlTreeId;

	/**
	 * Get Functions
	 */

	public function getTaxNumber() {
		if( false == valStr( $this->m_strTaxNumberEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strTaxNumberEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
	}

	public function getTaxNumberDecrypted() {
		if( false == valStr( $this->getTaxNumberEncrypted() ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getTaxNumberEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function getStreetLine3() {
		return $this->m_strStreetLine3;
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getRouteName() {
		return $this->m_strRouteName;
	}

	public function getGlTreeName() {
		return $this->m_strGlTreeName;
	}

	public function getGlTreeId() {
		return $this->m_intGlTreeId;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['tax_number'] ) ) $this->setTaxNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['tax_number'] ) : $arrmixValues['tax_number'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_name'] ) : $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['country_code'] ) : $arrmixValues['country_code'] );
		if( true == isset( $arrmixValues['street_line1'] ) ) $this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['street_line1'] ) : $arrmixValues['street_line1'] );
		if( true == isset( $arrmixValues['street_line2'] ) ) $this->setStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['street_line2'] ) : $arrmixValues['street_line2'] );
		if( true == isset( $arrmixValues['street_line3'] ) ) $this->setStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['street_line3'] ) : $arrmixValues['street_line3'] );
		if( true == isset( $arrmixValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['city'] ) : $arrmixValues['city'] );
		if( true == isset( $arrmixValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['state_code'] ) : $arrmixValues['state_code'] );
		if( true == isset( $arrmixValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['postal_code'] ) : $arrmixValues['postal_code'] );
		if( true == isset( $arrmixValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['phone_number'] ) : $arrmixValues['phone_number'] );
		if( true == isset( $arrmixValues['route_name'] ) ) $this->setRouteName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['route_name'] ) : $arrmixValues['route_name'] );
		if( true == isset( $arrmixValues['gl_tree_name'] ) ) $this->setGlTreeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['gl_tree_name'] ) : $arrmixValues['gl_tree_name'] );
		if( true == isset( $arrmixValues['gl_tree_id'] ) ) $this->setGlTreeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['gl_tree_id'] ) : $arrmixValues['gl_tree_id'] );

		return;
	}

	public function setTaxNumber( $strPlainTaxNumber ) {
		if( true == valStr( $strPlainTaxNumber ) ) {
			$this->setTaxNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainTaxNumber, CONFIG_SODIUM_KEY_TAX_NUMBER ) );
			$this->setTaxNumberMasked( CEncryption::maskText( $strPlainTaxNumber ) );
		}
	}

	public function setCommunityWebsiteUri( $strCommunityWebsiteUri ) {
		parent::setCommunityWebsiteUri( \Psi\CStringService::singleton()->strtolower( $strCommunityWebsiteUri ) );
	}

	public function setCorporateWebsiteUri( $strCorporateWebsiteUri ) {
		parent::setCorporateWebsiteUri( \Psi\CStringService::singleton()->strtolower( $strCorporateWebsiteUri ) );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setCountryCode( $strCountryCode ) {
		$this->m_strCountryCode = $strCountryCode;
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->m_strStreetLine1 = $strStreetLine1;
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->m_strStreetLine2 = $strStreetLine2;
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->m_strStreetLine3 = $strStreetLine3;
	}

	public function setCity( $strCity ) {
		$this->m_strCity = $strCity;
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = $strPostalCode;
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = $strPhoneNumber;
	}

	public function setRouteName( $strRouteName ) {
		$this->m_strRouteName = $strRouteName;
	}

	public function setGlTreeName( $strGlTreeName ) {
		return $this->m_strGlTreeName = $strGlTreeName;
	}

	public function setGlTreeId( $intGlTreeId ) {
		return $this->m_intGlTreeId = $intGlTreeId;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intId ) && 1 > $this->m_intId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'A property detail id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) && 1 > $this->m_intCid ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'A client id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyId ) && 1 > $this->m_intPropertyId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'A property id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMilitaryInstallationId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intMilitaryInstallationId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'military_installation_id', __( 'Please select installation.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCorporateWebsiteUri() {
		$boolIsValid = true;

		// Strip Out extra slashes
		if( true == preg_match( '/([^[:\/\/])(\/+)/i', $this->getCorporateWebsiteUri() ) ) {
			$this->setCorporateWebsiteUri( preg_replace( '/([^[:\/\/])(\/+)/i', '$1/', $this->getCorporateWebsiteUri() ) );
		}

		if( true == valStr( $this->getCorporateWebsiteUri() ) && false == CValidation::checkUrl( $this->getCorporateWebsiteUri(), false, true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'corporate_website_uri', __( 'A valid corporate website url is required. Url must start with \'http://\' , \'https://\' or \'/\'.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCommunityWebsiteUri() {
		$boolIsValid = true;

		// Strip Out extra slashes
		if( true == preg_match( '/([^[:\/\/])(\/+)/i', $this->getCommunityWebsiteUri() ) ) {
			$this->setCommunityWebsiteUri( preg_replace( '/([^[:\/\/])(\/+)/i', '$1/', $this->getCommunityWebsiteUri() ) );
		}

		if( true == valStr( $this->getCommunityWebsiteUri() ) && false == CValidation::checkUrl( $this->getCommunityWebsiteUri(), false, true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'community_website_uri', __( 'A valid property website url is required. Url must start with \'http://\' , \'https://\' or \'/\'.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode() {

		$boolIsValid = true;

		if( false == valStr( $this->getTaxPostalCode() ) ) return $boolIsValid;

		$intPostalCodeLength = strlen( str_replace( '-', '', $this->getTaxPostalCode() ) );

		if( CCountry::CODE_USA == $this->getTaxCountryCode()
		    && 0 < $intPostalCodeLength
		    && ( false == preg_match( '/^([[:alnum:]]){5,5}?$/', $this->getTaxPostalCode() )
		         && false == preg_match( '/^([[:alnum:]]){5,5}-([[:alnum:]]){4,4}?$/', $this->getTaxPostalCode() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_postal_code', __( 'Postal code must be 5 or 10 characters in XXXXX or XXXXX-XXXX format and should contain only alphanumeric characters.' ) ) );
		}

		if( CCountry::CODE_CANADA == $this->getTaxCountryCode()
		    && 0 < $intPostalCodeLength
		    && ( false == preg_match( '/^[a-zA-Z]{1}\d{1}[a-zA-Z]{1} \d{1}[a-zA-Z]{1}\d{1}$/', $this->getTaxPostalCode() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_postal_code', __( 'Postal code must be 6 characters in XXX XXX format and should contain only alphanumeric characters.' ) ) );
		}

		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Such validations should be done using CPhoneNumber::isValid() and in controller / library / outside-EOS.
	 *            Such format validation shouldn't be the part of EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumber() {

		$boolIsValid = true;

		if( false == valStr( $this->getTaxPhoneNumber() ) ) return $boolIsValid;

		$intPhoneNumberLength = strlen( $this->getTaxPhoneNumber() );

		if( false == preg_match( '/^[0-9\-\(\)\+\.\s]+$/', $this->getTaxPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_phone_number', __( 'Phone number is not valid.' ) ) );
		} elseif( 0 < $intPhoneNumberLength && 10 > $intPhoneNumberLength ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_phone_number', __( 'Phone number must be 10 digits.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFaxNumber() {

		$boolIsValid = true;

		if( false == valStr( $this->getTaxFax() ) ) return $boolIsValid;

		$intFaxLength = strlen( $this->getTaxFax() );

		if( false == preg_match( '/^[0-9\-\(\)\+\.\s]+$/', $this->getTaxFax() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_fax', __( 'Fax number is not valid.' ) ) );
		} elseif( 0 < $intFaxLength && 10 > $intFaxLength ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_fax', __( 'Fax number must be of 10 digits.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {

		$boolIsValid = true;

		if( false == valStr( $this->getTaxEmail() ) ) return $boolIsValid;

		if( false == CValidation::validateEmailAddresses( $this->getTaxEmail() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_email', __( 'Email address is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyDetailsFor1099EFile() {

		$boolIsValid = true;

		$arrstrRequiredFields[] = ( false == valStr( $this->getTaxNumberEncrypted() ) ) ? 'Tax ID Number' : '';
		$arrstrRequiredFields[] = ( false == valStr( $this->getPropertyName() ) ) ? 'Name' : '';
		$arrstrRequiredFields[] = ( false == valStr( $this->getStreetLine1() ) && false == valStr( $this->getStreetLine2() ) && false == valStr( $this->getStreetLine3() ) ) ? 'Address' : '';
		$arrstrRequiredFields[] = ( false == valStr( $this->getCity() ) ) ? 'City' : '';
		$arrstrRequiredFields[] = ( false == valStr( $this->getStateCode() ) ) ? 'State' : '';
		$arrstrRequiredFields[] = ( false == valStr( $this->getPostalCode() ) ) ? 'Zip Code' : '';
		$arrstrRequiredFields   = array_filter( $arrstrRequiredFields );

		if( true == valArr( $arrstrRequiredFields ) ) {
			$boolIsValid = false;

			$strValidationVerb = ( 1 == count( $arrstrRequiredFields ) ) ? 'is' : 'are all';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, '' . implode( ', ', $arrstrRequiredFields ) . ' ' . $strValidationVerb . ' required for ' . $this->getPropertyName() . '. Please <a href=\'?module=properties_setupxxx\' target="_blank">click here</a> to review the property\'s details. ' ) );
		}

		return $boolIsValid;
	}

	public function valTaxNumber( $objDatabase ) {

		$boolIsValid = true;

		$objPropertyAddress = $this->fetchPropertyAddress( $objDatabase );

		if( true == valStr( $this->getTaxNumber() ) && false == preg_match( '/^([\d]{2}-[\d]{7})$|([\d]{3}-[\d]{2}-[\d]{4})$/', $this->getTaxNumber() ) && CCountry::CODE_USA == $objPropertyAddress->getCountryCode() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_id', __( 'Tax ID Number should be in XX-XXXXXXX or XXX-XX-XXXX and should be numeric.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTaxCounty() {
		$boolIsValid = true;

		if( true == valStr( $this->getTaxCounty() ) && false == preg_match( '/^[a-zA-Z]+$/', $this->getTaxCounty() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_county', __( 'Tax county should be alphabetic characters.' ) ) );
		}

		return $boolIsValid;
	}

	public function valParcelNumbers() {
		$boolIsValid = true;

		if( true == valStr( $this->getParcelNumbers() ) && false == preg_match( '/^([a-z0-9]+,)*([a-z0-9]+){1}$/i', $this->getParcelNumbers() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'parcel_numbers', __( 'Parcel Numbers should be comma-separated alphanumeric.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {

		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valPostalCode();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valFaxNumber();
				$boolIsValid &= $this->valEmailAddress();

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valCorporateWebsiteUri();
				$boolIsValid &= $this->valCommunityWebsiteUri();
				$boolIsValid &= $this->valTaxNumber( $objDatabase );
				$boolIsValid &= $this->valTaxCounty();
				$boolIsValid &= $this->valParcelNumbers();
				break;

			case 'validate_military_installation':
				$boolIsValid &= $this->valMilitaryInstallationId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			case 'generate_1099_eFile':
				$boolIsValid &= $this->valPropertyDetailsFor1099EFile();
				break;

			case 'validate_marketing_description':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( true == $boolReturnSqlOnly ) {
			return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		if( false == parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		if( true == valObj( $this, 'CPropertyDetail' ) && true == valId( $this->getMilitaryInstallationId() ) && false == $this->syncMilitaryCustomerRelationships( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( true == $boolReturnSqlOnly ) {
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		if( false == parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		if( true == valObj( $this, 'CPropertyDetail' ) && true == valId( $this->getMilitaryInstallationId() ) && false == $this->syncMilitaryCustomerRelationships( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function syncMilitaryCustomerRelationships( $intCompanyUserId, $objDatabase ) {

		$arrmixMilitaryPropertyDetails = ( array ) fetchData( 'SELECT
																p.occupancy_type_ids,
																pd.military_installation_id,
															    pcrg.id AS property_customer_relation_group_id
															FROM
																properties p
																JOIN property_details pd ON ( pd.cid = p.cid AND pd.property_id = p.id )
																LEFT JOIN property_products pp ON ( pd.cid = pp.cid AND pd.property_id = pp.property_id AND pp.ps_product_id = ' . CPsProduct::ENTRATA_MILITARY . ' )
																LEFT JOIN property_customer_relationship_groups pcrg ON ( pd.cid = pcrg.cid AND pd.property_id = pcrg.property_id AND pcrg.occupancy_type_id = ' . COccupancyType::MILITARY . ' )
															WHERE p.cid = ' . ( int ) $this->getCid() . ' AND p.id = ' . ( int ) $this->getPropertyId(), $objDatabase );

		if( true == in_array( COccupancyType::MILITARY, CStrings::strToArrIntDef( $arrmixMilitaryPropertyDetails[0]['occupancy_type_ids'], NULL ) )
		    && true == valId( $arrmixMilitaryPropertyDetails[0]['military_installation_id'] )
		    && false == valId( $arrmixMilitaryPropertyDetails[0]['property_customer_relation_group_id'] ) ) {

			$arrstrResponse = fetchData( 'SELECT customer_relationship_group_association( ' . ( int ) $this->getCid() . '::INTEGER, ' . ( int ) $this->getPropertyId() . '::INTEGER, ' . ( int ) $intCompanyUserId . '::INTEGER, ' . COccupancyType::MILITARY . '::INTEGER );', $objDatabase );
			if( true == valArr( $arrstrResponse ) ) {
				return false;
			}
		}
		return true;
	}

	public function fetchPropertyAddress( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $this->getPropertyId(), CAddressType::PRIMARY, $this->getCid(), $objDatabase );
	}

}
?>
