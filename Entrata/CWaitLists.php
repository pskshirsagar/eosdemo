<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CWaitLists
 * Do not add any new functions to this class.
 */

class CWaitLists extends CBaseWaitLists {

	public static function fetchWaitListByPropertyGroupIdOccupancyTypeIdByCid( $intPropertyGroupId, $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchWaitList( sprintf( 'SELECT * FROM wait_lists WHERE property_group_id = %d AND occupancy_type_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intOccupancyTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchActiveWaitListByPropertyGroupIdOccupancyTypeIdByCid( $intPropertyGroupId, $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchWaitList( sprintf( 'SELECT * FROM wait_lists WHERE is_active IS TRUE AND property_group_id = %d AND occupancy_type_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intOccupancyTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchWaitListByPropertyGroupIdIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT 
						* 
				   FROM 
						wait_lists w 
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . ( int ) $intPropertyId . ']::INTEGER[] ) lp ON ( w.cid = lp.cid AND lp.property_id = w.property_group_id AND lp.is_disabled = 0 ) 
				   where 
						w.cid = ' . ( int ) $intCid . '
                   AND w.property_group_id = ' . ( int ) $intPropertyId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchWaitListDataByOccupancyTypeIdByPropertyGroupIdsByCid( $intOccupancyTypeId, $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
                        wl.id,
                        wl.application_stage_status_id,
                        wl.response_days,
                        (CASE WHEN wl.enable_availability_window_for_waitlist IS TRUE THEN 1 ELSE 0 END ) AS enable_availability_window_for_waitlist,
                        ass.application_stage_id,
                        ass.application_status_id
                    FROM
                        wait_lists wl
                        JOIN application_stage_statuses ass ON ( wl.application_stage_status_id = ass.id )
                        JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( $arrintPropertyIds, ',' ) . ' ]::INTEGER[] ) lp ON ( wl.cid = lp.cid AND lp.property_id = wl.property_group_id AND lp.is_disabled = 0 ) 
                    WHERE
                        wl.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . ' 
                        AND wl.property_group_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
                        AND wl.cid = ' . ( int ) $intCid . ' 
                        AND wl.is_active IS TRUE';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchWaitListsByOccupancyTypeIdsByPropertyGroupIdsByCid( $arrintOccupancyTypeIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT
                        *
                    FROM
                        wait_lists wl
                        JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( $arrintPropertyIds, ',' ) . ' ]::INTEGER[] ) lp ON ( wl.cid = lp.cid AND lp.property_id = wl.property_group_id AND lp.is_disabled = 0 )
                    WHERE
                        wl.occupancy_type_id IN ( ' . implode( ',', $arrintOccupancyTypeIds ) . ' )
                        AND wl.property_group_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
                        AND wl.cid = ' . ( int ) $intCid . ' 
                        AND wl.is_active IS TRUE';
		return self::fetchWaitLists( $strSql, $objDatabase );
	}

	public static function fetchWaitListByPropertyIdByOccupancyTypeIdByCid( $arrintPropertyIds, $intOccupancyTypeId, $intApplicationStageId, $intApplicationStatusId, $intLeaseIntervalTypeId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT 
				wl.*,
				CASE WHEN ass.id = ' . CApplicationStageStatus::PROSPECT_STAGE3_PARTIALLY_COMPLETED . '
					THEN ' . CApplicationStageStatus::PROSPECT_STAGE3_COMPLETED . '
					ELSE ass.id 
				END as original_application_stage_status_id
			FROM wait_lists wl
				JOIN application_stage_statuses ass ON ( ass.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . ' AND ass.application_stage_id = ' . ( int ) $intApplicationStageId . ' AND ass.application_status_id = ' . ( int ) $intApplicationStatusId . ' )
				JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( $arrintPropertyIds, ',' ) . ' ]::INTEGER[] ) lp ON ( wl.cid = lp.cid AND lp.property_id = wl.property_group_id AND lp.is_disabled = 0 ) 
			WHERE wl.cid = ' . ( int ) $intCid . '
				AND wl.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
				AND wl.property_group_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
				AND wl.is_active IS TRUE
			ORDER BY lp.property_id LIMIT 1';

		return self::fetchWaitList( $strSql, $objDatabase );
	}

	public static function fetchActiveWaitListsByPropertyGroupIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT 
						* 
				   FROM 
						wait_lists w 
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( $arrintPropertyIds, ',' ) . ' ]::INTEGER[] ) lp ON ( w.cid = lp.cid AND lp.property_id = w.property_group_id AND lp.is_disabled = 0 ) 
				   where 
						w.cid = ' . ( int ) $intCid . '
                        AND w.property_group_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) 
                        AND w.is_active IS TRUE
                   ORDER BY w.occupancy_type_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveWaitlistByOccupanyTypeIdsByPropertyIdByCid( $arrintOccupancyTypeIds, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintOccupancyTypeIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT 
						* 
				   FROM 
						wait_lists w 
				   where 
						w.cid = ' . ( int ) $intCid . '
						AND w.property_group_id = ' . ( int ) $intPropertyId . '
                        AND w.occupancy_type_id IN ( ' . implode( ',', $arrintOccupancyTypeIds ) . ' ) 
                        AND w.is_active IS TRUE';

		return self::fetchWaitLists( $strSql, $objDatabase );
	}

}

?>