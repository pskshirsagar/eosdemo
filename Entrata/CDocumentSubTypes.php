<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentSubTypes
 * Do not add any new functions to this class.
 */

class CDocumentSubTypes extends CBaseDocumentSubTypes {

	public static function fetchDocumentSubTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDocumentSubType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDocumentSubType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDocumentSubType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDocumentSubTypesByIds( $arrintDocumentSubTypeIds, $objDatabase ) {

		if( false == valArr( $arrintDocumentSubTypeIds ) ) return NULL;
		$strSql = ' SELECT
						*
					FROM
						document_sub_types
					WHERE
						id IN ( ' . implode( ',', $arrintDocumentSubTypeIds ) . ' )
					ORDER BY order_num';

		return self::fetchDocumentSubTypes( $strSql, $objDatabase );
	}

	public static function fetchDocumentSubTypesDataByIds( $arrintDocumentSubTypes, $objDatabase ) {

		if( false == valArr( $arrintDocumentSubTypes ) ) return NULL;
		$strSql = ' SELECT
						id,
						name
					FROM
						document_sub_types
					WHERE
						id IN ( ' . implode( ',', $arrintDocumentSubTypes ) . ' )
					ORDER BY order_num';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDocumentSubTypesByDocumentTypeIds( $arrintDocumentTypes, $objDatabase ) {

		if( false == valArr( $arrintDocumentTypes ) ) return NULL;
		$strSql = ' SELECT
						dst.id as id,
						util_get_system_translated( \'name\', dst.name, dst.details ) as name
					FROM
						document_sub_types dst
					WHERE
						dst.document_type_id IN ( ' . implode( ',', $arrintDocumentTypes ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

}
?>