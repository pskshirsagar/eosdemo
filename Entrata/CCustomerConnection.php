<?php

class CCustomerConnection extends CBaseCustomerConnection {

	use TEosPostalAddresses;

	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strNameLastMatronymic;
	protected $m_strPreferredName;
	protected $m_strCompanyName;
	protected $m_strPhoneNumber;
	protected $m_strFaxNumber;
	protected $m_strMobileNumber;
	protected $m_strEmailAddress;
	protected $m_strRole;
	protected $m_strReference;
	protected $m_intAddressTypeId;
	protected $m_intCustomerAddressId;
	protected $m_intCommercialClauseId;
	protected $m_objI18nPhoneNumber;
	protected $m_objCustomer;
	protected $m_objCustomerAddress;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strProvince;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_strOldEmailAddress;
	protected $m_strPasswordEncrypted;
	protected $m_strUsername;
	protected $m_strCustomerConnectionType;
	protected $m_arrstrPostalAddressFields;
	protected $m_intPrimaryPhoneNumberTypeId;
	protected $m_intContactNumberType;
	protected $m_strExtension;

	protected $m_intLeaseId;
	protected $m_intLeaseCustomerId;
	protected $m_intPropertyId;

	public function __construct() {
		parent::__construct();

		$this->m_arrstrPostalAddressFields = [
			'default' => [
				'm_strStreetLine1' => 'addressLine1',
				'm_strStreetLine2' => 'addressLine2',
				'm_strStreetLine3' => 'addressLine3',
				'm_strCity' => 'locality',
				'm_strStateCode' => 'administrativeArea',
				'm_strPostalCode' => 'postalCode',
				'm_strCountryCode' => 'country'
			]
		];

		return;
	}

	/**
	 * Get Functions
	 */

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getNameLastMatronymic() {
		return $this->m_strNameLastMatronymic;
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getExtension() {
		return $this->m_strExtension;
	}

	public function getPrimaryPhoneNumberTypeId() {
		return$this->m_intPrimaryPhoneNumberTypeId;
	}

	public function getContactNumberType() {
		return $this->m_intContactNumberType;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getFullName() {
		return implode( ' ', array_filter( [ $this->m_strNameFirst, $this->m_strNameLast ] ) );
	}

	public function getI18nPhoneNumber() {
		return $this->m_objI18nPhoneNumber;
	}

	public function getCustomer() {
		return $this->m_objCustomer;
	}

	public function getCustomerAddress() {
		return $this->m_objCustomerAddress;
	}

	public function getRole() {
		return $this->getDetailsField( 'role' );
	}

	public function getIsDedupeCustomer() {
		return $this->getDetailsField( 'is_dedupe_customer' );
	}

	public function getPermissions() {
		return $this->getDetailsField( 'permissions' );
	}

	public function getAddressTypeId() {
			return $this->m_intAddressTypeId;
	}

	public function getCustomerAddressId() {
		return $this->m_intCustomerAddressId;
	}

	public function getReference() {
		return $this->m_strReference;
	}

	public function getStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'default', 'm_strStreetLine1' );
	}

	public function getStreetLine2() {
		return $this->getPostalAddressField( 'addressLine2', 'default', 'm_strStreetLine2' );
	}

	public function getStreetLine3() {
		return $this->getPostalAddressField( 'addressLine3', 'default', 'm_strStreetLine3' );
	}

	public function getCity() {
		return $this->getPostalAddressField( 'locality', 'default', 'm_strCity' );
	}

	public function getStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'default', 'm_strStateCode' );
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function getPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'default', 'm_strPostalCode' );
	}

	public function getCommercialClauseId() {
		return $this->m_intCommercialClauseId;
	}

	public function getOldEmailAddress() {
		return $this->m_strOldEmailAddress;
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function getCustomerConnectionType() {
		return $this->m_strCustomerConnectionType;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getFaxNumber() {
		return $this->m_strFaxNumber;
	}

	/**
	 * Set Functions
	 */

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = CStrings::strTrimDef( $strNameFirst, 100, NULL, true );
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = CStrings::strTrimDef( $strNameLast, 100, NULL, true );
	}

	public function setNameLastMatronymic( $strNameLastMatronymic ) {
		$this->m_strNameLastMatronymic = CStrings::strTrimDef( $strNameLastMatronymic, 100, NULL, true );
	}

	public function setPreferredName( $strPreferredName ) {
		$this->m_strPreferredName = CStrings::strTrimDef( $strPreferredName, 100, NULL, true );
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = CStrings::strTrimDef( $strCompanyName, 100, NULL, true );
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = CStrings::strTrimDef( $strPhoneNumber, 15, NULL, true );
	}

	public function setExtension( $strExtension ) {
		$this->m_strExtension = CStrings::strTrimDef( $strExtension, 15, NULL, true );
	}

	public function setFaxNumber( $strFaxNumber ) {
		$this->m_strFaxNumber = CStrings::strTrimDef( $strFaxNumber, 15, NULL, true );
	}

	public function setMobileNumber( $strMobileNumber ) {
		$this->m_strMobileNumber = CStrings::strTrimDef( $strMobileNumber, 15, NULL, true );
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = CStrings::strTrimDef( $strEmailAddress, 100, NULL, true );
	}

	public function setI18nPhoneNumber( $objI18nPhoneNumber ) {
		$this->m_objI18nPhoneNumber = $objI18nPhoneNumber;
	}

	public function setCustomer( $objCustomer ) {
		$this->m_objCustomer = $objCustomer;
	}

	public function setCustomerAddress( $objCustomerAddress ) {
		$this->m_objCustomerAddress = $objCustomerAddress;
	}

	public function setRole( $strRole ) {
		$this->setDetailsField( 'role', $strRole );
	}

	public function setIsDedupeCustomer( $boolIsDedupe ) {
		$this->setDetailsField( 'is_dedupe_customer', $boolIsDedupe );
	}

	public function setPermissions( $arrmixPermissions ) {
		$this->setDetailsField( 'permissions', $arrmixPermissions );
	}

	public function setAddressTypeId( $intAddressTypeId ) {
		$this->m_intAddressTypeId = $intAddressTypeId;
	}

	public function setCustomerAddressId( $intCustomerAddressId ) {
		$this->m_intCustomerAddressId = $intCustomerAddressId;
	}

	public function setReference( $strReference ) {
		$this->m_strReference = $strReference;
	}

	public function getCountryCode() {
		return $this->getPostalAddressField( 'country', 'default', 'm_strCountryCode' );
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->setPostalAddressField( 'addressLine1', $strStreetLine1, $strAddressKey = 'default', 'm_strStreetLine1' );
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->setPostalAddressField( 'addressLine2', $strStreetLine2, $strAddressKey = 'default', 'm_strStreetLine2' );
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->setPostalAddressField( 'addressLine3', $strStreetLine3, $strAddressKey = 'default', 'm_strStreetLine3' );
	}

	public function setCity( $strCity ) {
		$this->setPostalAddressField( 'locality', $strCity, $strAddressKey = 'default', 'm_strCity' );
	}

	public function setStateCode( $strStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strStateCode, $strAddressKey = 'default', 'm_strStateCode' );
	}

	public function setProvince( $strProvince ) {
		$this->set( 'm_strProvince', CStrings::strTrimDef( $strProvince, 50, NULL, true ) );
	}

	public function setPostalCode( $strPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strPostalCode, $strAddressKey = 'default', 'm_strPostalCode' );
	}

	public function setCountryCode( $strCountryCode ) {
		$this->setPostalAddressField( 'country', $strCountryCode, $strAddressKey = 'default', 'm_strCountryCode' );
	}

	public function setCommercialClauseId( $intCommercialClauseId ) {
		return $this->m_intCommercialClauseId = $intCommercialClauseId;
	}

	public function setOldEmailAddress( $strOldEmailAddress ) {
		$this->set( 'm_strOldEmailAddress', CStrings::strTrimDef( $strOldEmailAddress, 240, NULL, true ) );
	}

	public function setPrimaryPhoneNumberTypeId( $intPrimaryPhoneNumberTypeId ) {
		$this->m_intPrimaryPhoneNumberTypeId = $intPrimaryPhoneNumberTypeId;
	}

	public function setContactNumberType( $intContactNumberType ) {
		$this->m_intContactNumberType = $intContactNumberType;
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 240, NULL, true ) );
	}

	public function setCustomerConnectionType( $strCustomerConnectionType ) {
		$this->set( 'm_strCustomerConnectionType', CStrings::strTrimDef( $strCustomerConnectionType, 240, NULL, true ) );
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = $intLeaseId;
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->m_intLeaseCustomerId = $intLeaseCustomerId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['name_first'] ) ) {
			$this->setNameFirst( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['name_first'] ) : $arrmixValues['name_first'] );
		}

		if( isset( $arrmixValues['name_last'] ) ) {
			$this->setNameLast( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['name_last'] ) : $arrmixValues['name_last'] );
		}

		if( isset( $arrmixValues['name_last_matronymic'] ) ) {
			$this->setNameLastMatronymic( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['name_last_matronymic'] ) : $arrmixValues['name_last_matronymic'] );
		}

		if( isset( $arrmixValues['preferred_name'] ) ) {
			$this->setPreferredName( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['preferred_name'] ) : $arrmixValues['preferred_name'] );
		}

		if( isset( $arrmixValues['company_name'] ) ) {
			$this->setCompanyName( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['company_name'] ) : $arrmixValues['company_name'] );
		}

		if( isset( $arrmixValues['phone_number'] ) ) {
			$this->setPhoneNumber( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['phone_number'] ) : $arrmixValues['phone_number'] );
		}

		if( isset( $arrmixValues['extension'] ) ) {
			$this->setExtension( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['extension'] ) : $arrmixValues['extension'] );
		}

		if( isset( $arrmixValues['fax_number'] ) ) {
			$this->setFaxNumber( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['fax_number'] ) : $arrmixValues['fax_number'] );
		}

		if( isset( $arrmixValues['mobile_number'] ) ) {
			$this->setMobileNumber( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['mobile_number'] ) : $arrmixValues['mobile_number'] );
		}

		if( isset( $arrmixValues['email_address'] ) ) {
			$this->setEmailAddress( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['email_address'] ) : $arrmixValues['email_address'] );
		}

		if( isset( $arrmixValues['role'] ) ) {
			$this->setRole( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['role'] ) : $arrmixValues['role'] );
		}

		if( isset( $arrmixValues['address_type_id'] ) ) {
			$this->setAddressTypeId( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['address_type_id'] ) : $arrmixValues['address_type_id'] );
		}

		if( isset( $arrmixValues['reference'] ) ) {
			$this->setReference( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['reference'] ) : $arrmixValues['reference'] );
		}

		if( isset( $arrmixValues['province'] ) ) {
			$this->setProvince( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['province'] ) : $arrmixValues['province'] );
		}

		if( isset( $arrmixValues['password_encrypted'] ) ) {
			$this->setPasswordEncrypted( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['password_encrypted'] ) : $arrmixValues['password_encrypted'] );
		}

		if( isset( $arrmixValues['customer_address_id'] ) ) {
			$this->setCustomerAddressId( ( int ) $arrmixValues['customer_address_id'] );
		}

		if( true == isset( $arrmixValues['commercial_clause_id'] ) ) {
			$this->setCommercialClauseId( $arrmixValues['commercial_clause_id'] );
		}

		if( isset( $arrmixValues['primary_phone_number_type_id'] ) ) {
			$this->setPrimaryPhoneNumberTypeId( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['primary_phone_number_type_id'] ) : $arrmixValues['primary_phone_number_type_id'] );
		}

		if( isset( $arrmixValues['contact_number_type'] ) ) {
			$this->setContactNumberType( ( true === $boolStripSlashes ) ? stripslashes( $arrmixValues['contact_number_type'] ) : $arrmixValues['contact_number_type'] );
		}

		if( isset( $arrmixValues['username'] ) ) {
			$this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['username'] ) : $arrmixValues['username'] );
		}

		if( isset( $arrmixValues['customer_connection_type'] ) ) {
			$this->setCustomerConnectionType( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['customer_connection_type'] ) : $arrmixValues['customer_connection_type'] );
		}

		if( true == isset( $arrmixValues['lease_id'] ) ) {
			$this->setLeaseId( $arrmixValues['lease_id'] );
		}

		if( true == isset( $arrmixValues['lease_customer_id'] ) ) {
			$this->setLeaseCustomerId( $arrmixValues['lease_customer_id'] );
		}

		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}

		$this->setPostalAddressValues( $arrmixValues, $this->m_arrstrPostalAddressFields );

	}

	public function valId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intId ) || 0 >= ( int ) $this->m_intId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Contact Request:  Id required - CCustomerContact::valId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || 0 >= ( int ) $this->m_intCid ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Contact Request:  Management Id required - CCustomerContact::valCid()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCustomerId ) || 0 >= ( int ) $this->m_intCustomerId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Contact Request:  Customer Id required - CCustomerContact::valCustomerId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valCustomerConnectionTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intCustomerConnectionTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_type_id', __( 'Contact Type Id is required' ) ), E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valConnectedCustomerId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intConnectedCustomerId ) || 0 >= ( int ) $this->m_intConnectedCustomerId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Contact Request:  Customer Id required - CCustomerContact::valCustomerId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valConfirmedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsAdmin() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameFirst( $boolAddErrorMessage = true, $boolIsLeadRepresentative = false ) {

		$boolIsValid = true;

		$strErrorMessage = '';
		if( true == $boolIsLeadRepresentative ) {
			$strErrorMessage = __( 'Representative' );
		}
		if( true == is_null( $this->m_strNameFirst ) ) {
			$boolIsValid = false;

			if( true == $boolAddErrorMessage ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( '{%s, 0}  First name is required.', [ $strErrorMessage ] ) ) );
			}

		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_strNameFirst, '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_strNameFirst, 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'Names cannot contain @ signs or email headers.' ) ) );

		} else {
			if( 1 > strlen( $this->m_strNameFirst ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( '{%s, 0}  First name must have at least one letter.', [ $strErrorMessage ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valNameLast( $boolAddErrorMessage = true, $boolIsLeadRepresentative = false ) {

		$boolIsValid = true;

		$strErrorMessage = '';
		if( true == $boolIsLeadRepresentative ) {
			$strErrorMessage = __( 'Representative' );
		}
		if( true == is_null( $this->m_strNameLast ) ) {
			$boolIsValid = false;

			if( true == $boolAddErrorMessage ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( '{%s, 0} Last name is required.', [ $strErrorMessage ] ) ) );
			}

		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_strNameLast, '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_strNameLast, 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Names cannot contain @ signs or email headers.' ) ) );

		}

		return $boolIsValid;
	}

	public function valEmailAddress() {

		$boolIsValid     = true;
		$strErrorMessage = '';

		if( true == is_null( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( '{%s, 0} Email address is required.', [ $strErrorMessage ] ), 504 ) );
		}

		if( false == is_null( $this->m_strEmailAddress ) && false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Please enter a valid {%s, 0} email address.', [ $strErrorMessage ] ) ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumber() {

		if( false == valStr( $this->getPhoneNumber() ) ) {
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is required.' ) );
			return false;
		}

		$objI18nPhoneNumber = new \i18n\CPhoneNumber( $this->getPhoneNumber() );

		if( true == valObj( $objI18nPhoneNumber, 'i18n\CPhoneNumber' ) ) {
			if( false == $objI18nPhoneNumber->isValid() ) {
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}). {%s, formatted_errors}', [ 'region' => $objI18nPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objI18nPhoneNumber->getCountryCode(), 'formatted_errors' => $objI18nPhoneNumber->getFormattedErrors() ] ) ) );

				return false;
			}
		} else {
			$intPrimaryNumberLength = \Psi\CStringService::singleton()->strlen( $this->getPhoneNumber() );
			if( 0 < $intPrimaryNumberLength && ( 10 > $intPrimaryNumberLength || 15 < $intPrimaryNumberLength ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( '{%s,0} number must be between 10 and 15 characters.' ), '' ) );

				return false;
			}
		}

		return true;
	}

	public function valAddressTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intAddressTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address_type_id', __( 'Address Type Id is required' ) ), E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( ' NOW() ' );

		return $this->update( $intCurrentUserId, $objDatabase );
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valCustomerConnectionTypeId();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				break;

			case 'validate_contact_form':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerConnectionTypeId();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber();
				break;

			case 'validate_tenant':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerConnectionTypeId();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valAddressTypeId();
				break;

			case 'validate_group':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerConnectionTypeId();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valEmailAddress();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>