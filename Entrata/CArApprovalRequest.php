<?php

class CArApprovalRequest extends CBaseArApprovalRequest {

	protected $m_objLease;

	protected $m_intLeaseIntervalId;

	protected $m_strRoutingInfo;

	/**
	 * Get Functions
	 */

	public function getLease() {
		return $this->m_objLease;
	}

	public function getLeaseIntervalId() {
		return $this->m_intLeaseIntervalId;
	}

	public function getRoutingInfo() {
		return $this->m_strRoutingInfo;
	}

	/**
	 * Set Functions
	 */

	public function setLease( $objLease ) {
		$this->m_objLease = $objLease;
	}

	public function setLeaseIntervalId( $intLeaseIntervalId ) {
		$this->m_intLeaseIntervalId = $intLeaseIntervalId;
	}

	public function setRoutingInfo( $strRoutingInfo ) {
		$this->m_strRoutingInfo = $strRoutingInfo;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRouteTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseAccommodationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getFormattedTransactionAmount() {

		$objStandardClass           = json_decode( $this->getRequestDetails() );
		$intTransactionAmount       = $objStandardClass->transaction_amount;

		if( 0 > $objStandardClass->transaction_amount ) {
			return '(' . __( '{%m, 0, nots}', [ abs( $intTransactionAmount ) ] ) . ')';
		} else {
			return __( '{%m, 0, nots}', [ abs( $intTransactionAmount ) ] );
		}
	}

	public function getFormattedPostMonth() {

		$objStandardClass           = json_decode( $this->getRequestDetails() );
		if( true == is_null( $objStandardClass->post_month ) ) return NULL;
		return \Psi\CStringService::singleton()->preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $objStandardClass->post_month );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['request_details'] ) && false == $boolDirectSet ) {
			$this->setRequestDetails( $arrmixValues['request_details'] );
		}

		if( isset( $arrmixValues['lease_interval_id'] ) && $boolDirectSet ) {
			$this->set( 'lease_interval_id', trim( $arrmixValues['lease_interval_id'] ) );
		} elseif( isset( $arrmixValues['lease_interval_id'] ) ) {
			$this->setLeaseIntervalId( $arrmixValues['lease_interval_id'] );
		}
	}

	public function archiveArApprovalRequest( CCompanyUser $objCurrentUser, CDatabase $objDatabase ) {
		$this->setIsArchived( true );
		return $this->update( $objCurrentUser, $objDatabase );
	}

}
?>