<?php

class CPrivacyDataRequest extends CBasePrivacyDataRequest {

	public function __construct() {
		parent::__construct();

		return;
	}

	public function createPrivacyDataRequestStatus() {

		$objPrivacyDataRequestStatus = new CPrivacyDataRequestStatus();
		$objPrivacyDataRequestStatus->setCid( $this->getCid() );
		$objPrivacyDataRequestStatus->setPrivacyDataRequestId( $this->getId() );

		return $objPrivacyDataRequestStatus;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStatus() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSourcePsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
