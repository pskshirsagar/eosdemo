<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImplementationChecklists
 * Do not add any new functions to this class.
 */

class CImplementationChecklists extends CBaseImplementationChecklists {

	public static function fetchModulesByPropertySettingsKeys( $objDatabase ) {
		$strSql = 'WITH RECURSIVE property_settings( module_id, title, parent_module_id, name, property_setting_key_id, depth ) AS (
						SELECT
							m.id,
							util_get_translated( \'title\', m.title, m.details ) AS title,
							m.parent_module_id,
							m.name,
							psk.id,
							1
						FROM
							property_setting_keys psk
							JOIN property_setting_groups psg ON ( psk.property_setting_group_id = psg.id )
							JOIN modules m ON ( psg.module_id = m.id )
						WHERE
							psk.is_implementation_required IS true
						UNION ALL
						SELECT
							m.id,
							util_get_translated( \'title\', m.title, m.details ) AS title,
							m.parent_module_id,
							m.name,
							ps.property_setting_key_id,
							depth+1
						FROM
							modules m,
							property_settings ps
						WHERE
							m.id = ps.parent_module_id
							AND m.id <> 1900
						)

					SELECT
						DISTINCT ON (module_id) *
					FROM
						property_settings;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCorporateImplementationChecklistsByCid( $intCid, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						implementation_checklists ic
					WHERE
						ic.property_id IS NULL
						AND ic.cid = ' . ( int ) $intCid;

		return self::fetchImplementationChecklists( $strSql, $objDatabase );
	}

	public static function fetchImplementationChecklistsByCidByPropertyIds( $intCid, $arrintPropertyIds, $objDatabase ) {
		if( true == empty( $arrintPropertyIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						implementation_checklists ic
					WHERE
						ic.cid = ' . ( int ) $intCid . '
						AND ic.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchImplementationChecklists( $strSql, $objDatabase );
	}

}
?>