<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChecklistItemTypeOptions
 * Do not add any new functions to this class.
 */

class CChecklistItemTypeOptions extends CBaseChecklistItemTypeOptions {

	public static function fetchChecklistItemTypeOptions( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CChecklistItemTypeOption', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchChecklistItemTypeOption( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CChecklistItemTypeOption', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCustomChecklistItemTypeOptionsByChecklistItemTypeIds( $arrintChecklistItemTypeIds, $intChecklistTypeId, $objDatabase ) {
		if( false == valArr( $arrintChecklistItemTypeIds ) || false == valId( $intChecklistTypeId ) ) return NULL;

		$strSql = 'SELECT util_set_locale(\'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getdefaultLocaleCode() . '\');
					SELECT
						cito.id,
						util_get_translated( \'name\', cito.name, cito.details ) AS name,
						cito.checklist_item_type_id,
						cito.checklist_trigger_ids
					FROM
						checklist_item_type_options cito
						JOIN checklist_item_types cit ON ( cit.id = cito.checklist_item_type_id )
						JOIN checklist_triggers ct On ( ct.id = ANY ( cito.checklist_trigger_ids ) AND ct.checklist_type_id = ' . ( int ) $intChecklistTypeId . '  )
					WHERE
						cito.checklist_item_type_id IN ( ' . implode( ', ', $arrintChecklistItemTypeIds ) . ' )
						AND cito.is_published = TRUE
					GROUP BY
						cito.id
					ORDER BY
						cito.order_num';

		return fetchData( $strSql, $objDatabase );
	}

}
?>
