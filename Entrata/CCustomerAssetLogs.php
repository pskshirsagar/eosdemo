<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAssetLogs
 * Do not add any new functions to this class.
 */

class CCustomerAssetLogs extends CBaseCustomerAssetLogs {

	public static function fetchRecentCustomerAssetLogByCustomerIdByIdByCid( $intCustomerId, $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cal.*
					FROM
						customer_asset_logs cal
					WHERE
						cal.cid = ' . ( int ) $intCid . '
						AND cal.customer_id = ' . ( int ) $intCustomerId . '
						AND cal.customer_asset_id = ' . ( int ) $intId . '
						AND cal.is_post_date_ignored = 0
						ORDER BY cal.log_datetime DESC LIMIT 1 ';

		return self::fetchCustomerAssetLog( $strSql, $objDatabase );
	}

}
?>