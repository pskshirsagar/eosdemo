<?php

class CLeaseLateFee extends CBaseLeaseLateFee {

    public function valId() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getLeaseId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valLateFeeTypeId() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getLateFeeTypeId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'late_fee_type_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valLateFeeFormulaId() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getLateFeeFormulaId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'late_fee_formula_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valGraceDays() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getGraceDays() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'grace_days', '' ) );
        // }

        return $boolIsValid;
    }

    public function valMinimumBalanceDue() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getMinimumBalanceDue() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_balance_due', '' ) );
        // }

        return $boolIsValid;
    }

    public function valMaximumMonthlyFeeAmount() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getMaximumMonthlyFeeAmount() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maximum_monthly_fee_amount', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCurrentChargesOnly() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getCurrentChargesOnly() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_charges_only', '' ) );
        // }

        return $boolIsValid;
    }

    public function valScheduledChargesOnly() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getScheduledChargesOnly() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_charges_only', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>