<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantAddresses
 * Do not add any new functions to this class.
 */

class CApplicantAddresses extends CBaseApplicantAddresses {

	public static function fetchApplicantAddressByApplicantIdByAddressTypeIdByCid( $intApplicantId, $intAddressTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM applicant_addresses WHERE applicant_id::int = ' . ( int ) $intApplicantId . ' AND cid = ' . ( int ) $intCid . ' AND address_type_id::int = ' . ( int ) $intAddressTypeId . ' ORDER BY id ASC LIMIT 1';
		return self::fetchApplicantAddress( $strSql, $objDatabase );

	}

	public static function fetchApplicantAddressesByApplicantIdsByAddressTypeIdsByCid( $arrintApplicantIds, $arrintAddressTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicantIds ) || false == valArr( $arrintAddressTypeIds ) )	return [];

		$strSql = 'SELECT * FROM applicant_addresses WHERE applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' ) AND cid = ' . ( int ) $intCid . ' AND address_type_id IN ( ' . implode( ',', $arrintAddressTypeIds ) . ' ) ORDER BY applicant_id, address_type_id DESC';

		return self::fetchApplicantAddresses( $strSql, $objDatabase );

	}

	public static function fetchApplicantAddressessByCidByApplicantIds( $intCid, $arrintApplicantIds, $objDatabase ) {
		if( false == valArr( $arrintApplicantIds ) ) return NULL;
		$strSql = 'SELECT * FROM applicant_addresses WHERE applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' ) AND cid = ' . ( int ) $intCid;
		return self::fetchApplicantAddresses( $strSql, $objDatabase );
	}

	public static function fetchApplicantAddressesByApplicantIdByAddressTypeIdsByCid( $intApplicantId, $arrintAddressTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintAddressTypeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						applicant_addresses
					WHERE
						applicant_id = ' . ( int ) $intApplicantId . '
						AND cid = ' . ( int ) $intCid . '
						AND address_type_id IN ( ' . implode( ',', $arrintAddressTypeIds ) . ' )
					ORDER BY
						address_type_id DESC';

		return self::fetchApplicantAddresses( $strSql, $objDatabase );
	}

	public static function fetchApplicantsAddressesByCustomerTypeIds( $intCid, $intApplicationId, $arrintCustomerTypeIds, $objDatabase ) {
		if( false == valArr( $arrintCustomerTypeIds ) )
			return NULL;

		$strSql = 'SELECT
					*
				  FROM applicant_addresses AS aad
				      JOIN applicant_applications aa ON ( aad.applicant_id = aa.applicant_id AND aad.cid = aa.cid )
				  WHERE
					  aa.application_id = ' . ( int ) $intApplicationId . '
				      AND aa.cid = ' . ( int ) $intCid . '
				      AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' );';

		return self::fetchApplicantAddresses( $strSql, $objDatabase );
	}

	public static function fetchApplicantsAddressesByCustomerIdsByAddressTypeIdsByCid( $arrintCustomerIds, $arrintAddressTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) || false == valArr( $arrintAddressTypeIds ) ) return NULL;

		$strSql = 'SELECT
						aa.*
					FROM
						applicant_addresses aa
						JOIN applicants a ON (a.cid = aa.cid AND a.id = aa.applicant_id)
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND a.customer_id IN(' . implode( ',', $arrintCustomerIds ) . ')
						AND aa.address_type_id IN(' . implode( ',', $arrintAddressTypeIds ) . ');';

		return self::fetchApplicantAddresses( $strSql, $objDatabase );
	}

}
?>