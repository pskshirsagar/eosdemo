<?php
use Psi\Eos\Entrata\CUnitSpaces;

class CRevenueOptimalRent extends CBaseRevenueOptimalRent {
	// CRevenueOptimalRent
	protected $m_boolIsRenewal;
	protected $m_objUnitSpace;
	protected $m_arrmixPricingMatrix;
	protected $m_strCustomOptimalRent;
	protected $m_arrobjRevenueOptimalDates;
	protected $m_intCustomLeaseMonth;
	protected $m_strCustomMoveInDate;

	public function getMatrix() {
		if( false == isset( $this->m_jsonMatrix ) ) {
			$this->m_jsonMatrix = parent::getMatrix();
		}

		if( false == valObj( $this->m_jsonMatrix, 'stdClass' ) ) {
			$this->m_arrobjRevenueOptimalDates = [];
			return $this->m_arrobjRevenueOptimalDates;
		}

		foreach( $this->m_jsonMatrix as $strDate => $jsonRevenueOptimalLeaseTerms ) {
			$this->m_arrobjRevenueOptimalDates[] = new CRevenueOptimalDate( $strDate, $jsonRevenueOptimalLeaseTerms );
		}

		return $this->m_jsonMatrix;
	}

	public function sqlMatrix() {
		if( true == valArr( $this->m_arrobjRevenueOptimalDates ) ) {
			$arrmixRevenueOptimalDates = [];
			foreach( $this->m_arrobjRevenueOptimalDates as $objRevenueOptimalDate ) {
				$arrmixRevenueOptimalDates += $objRevenueOptimalDate->toArray();
			}

			return	'\'' . addslashes( CStrings::jsonToStrDef( $arrmixRevenueOptimalDates ) ) . '\'';
		}

		return parent::sqlMatrix();
	}

	public function getRevenueOptimalDates() {
		if( false == isset( $this->m_arrobjRevenueOptimalDates ) ) {
			$this->getMatrix();
		}
		return $this->m_arrobjRevenueOptimalDates;
	}

	public function setRevenueOptimalDates( $arrobjRevenueOptimalDates ) {
		$this->m_arrobjRevenueOptimalDates = $arrobjRevenueOptimalDates;
	}

	public function setPricingMatrixArray( $arrmixPricingMatrix ) {

		$this->m_arrmixPricingMatrix = $arrmixPricingMatrix;
	}

	public function getPricingMatrixArray( $objDatabase = NULL, $objPropertyPreference = NULL, $objUnitSpace = NULL, $arrmixLeaseIntervalsData = [] ) {

		$arrmixPricingMatrixKeyed = [];

		if( true == is_null( $this->m_strMatrix ) ) {
			return NULL;
		}

		$arrmixPricingMatrix = CPricingLibrary::extractMatrixArray( $this->m_strMatrix );

		if( false == valArr( $arrmixPricingMatrix ) ) {
			return NULL;
		}

		usort( $arrmixPricingMatrix, function( $strLeftElement, $strRightElement ) {
			$intResult = strnatcmp( $strLeftElement['move_in_date_key'], $strRightElement['move_in_date_key'] );
			if( 0 == $intResult ) {
				if( true == isset( $strLeftElement['lease_term'] ) ) {
					$intResult = strnatcmp( $strLeftElement['lease_term'], $strRightElement['lease_term'] );
				} else {
					$intResult = strnatcmp( $strLeftElement['lease_term_key'], $strRightElement['lease_term_key'] );
				}
			}
			return $intResult;
		} );

		// get Rent Duration
		$intRentDuration = 3;
		if( true == is_null( $objPropertyPreference ) ) {
			$objPropertyPreference = $this->getOrFetchPropertyPreference( CPricingPostRentLibrary::PROPERTY_PREFERENCE_PRICING_CONSTRAINT_RENT_DURATION, $objDatabase );
		}
		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
		    $intRentDuration = $objPropertyPreference->getValue();
		}
		// get current available on adjustment
		$intIntervalAdjust = NULL;
		$intDiffDays = NULL;
		$intDontAdjustDate = 0;
		if( false == is_null( $objDatabase ) ) {
			if( true == is_null( $objUnitSpace ) ) {
				$objUnitSpace = $this->getOrFetchUnitSpace( $objDatabase );
			}
			$objDateMinMoveIn = new DateTime( $arrmixPricingMatrix[0]['move_in_date_key'] );
			if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
				$boolIsMtmLease = false;
				if( true == in_array( $objUnitSpace->getUnitSpaceStatusTypeId(), CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes ) ) {
					$objDateAvailableOn = new DateTime( date( 'Y-m-d', max( strtotime( $objUnitSpace->getAvailableOn() ), strtotime( date( 'Ymd' ) ) ) ) );

				} else {
					if( true == valArr( $arrmixLeaseIntervalsData ) ) {
						$strDate = $arrmixLeaseIntervalsData['lease_end_date'];
						if( CLeaseIntervalType::MONTH_TO_MONTH == $arrmixLeaseIntervalsData['lease_interval_type_id'] ) {
							$boolIsMtmLease = true;
						}
					} else {
						$arrmixLeaseIntervalAndProcess = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLatestActiveLeaseIntervalByUnitSpaceIdByPropertyIdByCid( $objUnitSpace->getId(), $objUnitSpace->getPropertyId(), $objUnitSpace->getCid(), $objDatabase );
						if( false == valArr( $arrmixLeaseIntervalAndProcess ) ) {
							$intDontAdjustDate = 1;
						}
						if( CLeaseIntervalType::MONTH_TO_MONTH == $arrmixLeaseIntervalAndProcess[0]['lease_interval_type_id'] ) {
							$boolIsMtmLease = true;
						}
						$strDate = $arrmixLeaseIntervalAndProcess[0]['lease_end_date'];
					}
					if( true == is_null( $strDate ) ) {
						$intDontAdjustDate = 1;
					}
					$objDateAvailableOn = new DateTime( date( 'Y-m-d', max( strtotime( $strDate . ' + 1 day' ), strtotime( date( 'Ymd' ) ) ) ) );
					if( true == $boolIsMtmLease ) {
						$objDateAvailableOn = new DateTime();
					}
				}
			} else {
				$objDateAvailableOn = new DateTime( date( 'Y-m-d', max( strtotime( $arrmixPricingMatrix[0]['move_in_date_key'] ), strtotime( date( 'Ymd' ) ) ) ) );
				$intDontAdjustDate = 1;
			}
			if( 0 == $intDontAdjustDate ) {
				$strIntervalAdjust = $objDateMinMoveIn->diff( $objDateAvailableOn );
				if( 1 == $strIntervalAdjust->invert ) {
					$strIntervalAdjust = '-' . $strIntervalAdjust->days;
				} else {
					$strIntervalAdjust = $strIntervalAdjust->days;
				}
			}
		}
		// create indexed matrix
		foreach( $arrmixPricingMatrix as $arrmixPrice ) {
			if( true == isset( $arrmixPrice['lease_term'] ) ) {
				$arrmixPrice['lease_term_key'] = $arrmixPrice['lease_term'];
			} else {
				$arrmixPrice['lease_term'] = $arrmixPrice['lease_term_key'];
			}
			// adjust dates
			$arrmixPrice['original_move_in_date'] = date( 'Y-m-d', strtotime( $arrmixPrice['move_in_date_key'] ) );
			if( false == is_null( $objDatabase ) && false == is_null( $strIntervalAdjust ) ) {
				if( false !== \Psi\CStringService::singleton()->strpos( $strIntervalAdjust, '-' ) ) {
					$arrmixPrice['move_in_date_key'] = date( 'Y-m-d', strtotime( $strIntervalAdjust . ' days', strtotime( $arrmixPrice['original_move_in_date'] ) ) );
				} else {
					$arrmixPrice['move_in_date_key'] = date( 'Y-m-d', strtotime( '+' . $strIntervalAdjust . ' days', strtotime( $arrmixPrice['original_move_in_date'] ) ) );
				}
			} else {
				$arrmixPrice['move_in_date_key'] = date( 'Y-m-d', strtotime( $arrmixPrice['original_move_in_date'] ) );
				if( true == $boolIsMtmLease ) {
					$arrmixPrice['move_in_date_key'] = date( 'Y-m-d' );
				}
			}
			$arrmixPrice['move_in_date'] = $arrmixPrice['move_in_date_key'];
			// add end date
			if( strtotime( $arrmixPrice['end_date'] ) < strtotime( $arrmixPrice['move_in_date_key'] ) ) {
				if( true == is_null( $intDiffDays ) ) {
					$objDateStart = new DateTime( $arrmixPrice['start_date'] );
					$objDateMoveIn = new DateTime( $arrmixPrice['move_in_date_key'] );
					$intDiffDays = $objDateStart->diff( $objDateMoveIn );
					$intDiffDays = $intDiffDays->days;
				}
				$arrmixPrice['end_date'] = date( 'Y-m-d', strtotime( '+' . $intDiffDays . ' days', strtotime( $arrmixPrice['end_date'] ) ) );
			} else {
				if( false == is_null( $strIntervalAdjust ) ) {
					if( false !== \Psi\CStringService::singleton()->strpos( $strIntervalAdjust, '-' ) ) {
						$arrmixPrice['end_date'] = date( 'Y-m-d', strtotime( $strIntervalAdjust . ' days', strtotime( $arrmixPrice['end_date'] ) ) );
					} else {
						$arrmixPrice['end_date'] = date( 'Y-m-d', strtotime( '+' . $strIntervalAdjust . ' days', strtotime( $arrmixPrice['end_date'] ) ) );
					}
				}
			}
			$arrmixPrice['start_date'] = $arrmixPrice['move_in_date'];
            // set indexed entry
			$arrmixPricingMatrixKeyed[$arrmixPrice['original_move_in_date']][$arrmixPrice['lease_term']] = $arrmixPrice;
		}
		if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
			if( true == in_array( $objUnitSpace->getUnitSpaceStatusTypeId(), CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes ) ) {
				// adjust first start_date to today.
				foreach( $arrmixPricingMatrixKeyed as $intOriginalMoveInDate => $arrmixPricingColumn ) {
					foreach( $arrmixPricingColumn as $intLeaseTerm => $arrmixPrice ) {
						$arrmixPricingMatrixKeyed[$intOriginalMoveInDate][$intLeaseTerm]['start_date'] = date( 'Y-m-d' );
					}
					break;
				}
			} else {
				// show only first move_in_date.
				$boolUnset = false;
				foreach( $arrmixPricingMatrixKeyed as $intOriginalMoveInDate => $arrmixPricingColumn ) {
					if( true == $boolUnset ) {
						unset( $arrmixPricingMatrixKeyed[$intOriginalMoveInDate] );
					} else {
						$boolUnset = true;
					}
				}
			}
		}
		$this->m_arrmixPricingMatrix = $arrmixPricingMatrixKeyed;
		return $arrmixPricingMatrixKeyed;
	}

	public function fetchUnitSpace( $objDatabase ) {

		if( false == valId( $this->m_intUnitSpaceId ) ) {
			return NULL;
		}

		return CUnitSpaces::createService()->fetchUnitSpaceByIdByCid( $this->getUnitSpaceId(), $this->getCid(), $objDatabase );
	}

	public function getOrFetchUnitSpace( $objDatabase ) {

		if( true == valObj( $this->m_objUnitSpace, 'CUnitSpace' ) ) {
			return $this->m_objUnitSpace;
		}

		$this->m_objUnitSpace = $this->fetchUnitSpace( $objDatabase );

		return $this->m_objUnitSpace;
	}

	public function fetchPropertyPreference( $strPropertyPreferenceKey, $objDatabase ) {

	    return \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( $strPropertyPreferenceKey, $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function getOrFetchPropertyPreference( $strPropertyPreferenceKey, $objDatabase ) {

	    if( true == valObj( $this->m_arrobjPropertyPreferences[$strPropertyPreferenceKey], 'CPropertyPreference' ) ) {
	        return $this->m_arrobjPropertyPreferences[$strPropertyPreferenceKey];
	    }

	    $this->m_arrobjPropertyPreferences[$strPropertyPreferenceKey] = $this->fetchPropertyPreference( $strPropertyPreferenceKey, $objDatabase );

	    return $this->m_arrobjPropertyPreferences[$strPropertyPreferenceKey];
	}

	public function getIsRenewal( $objDatabase ) {

		if( false == is_null( $this->m_boolIsRenewal ) ) {
			return $this->m_boolIsRenewal;
		}

		$objUnitSpace = $this->getOrFetchUnitSpace( $objDatabase );

		if( false == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
			$this->m_boolIsRenewal = NULL;
		} else {
		   $this->m_boolIsRenewal = ( CUnitSpaceStatusType::OCCUPIED_NO_NOTICE == $objUnitSpace->getUnitSpaceStatusTypeId() );
		}

		return $this->m_boolIsRenewal;
	}

	public function getCustomOptimalRent() {
		return $this->m_strCustomOptimalRent;
	}

	public function setCustomOptimalRent( $strCustomOptimalRent ) {
		$this->m_strCustomOptimalRent = $strCustomOptimalRent;
	}

	public function getCustomLeaseMonth() {
		return $this->m_intCustomLeaseMonth;
	}

	public function setCustomLeaseMonth( $intCustomLeaseMonth ) {
		$this->m_intCustomLeaseMonth = $intCustomLeaseMonth;
	}

	public function getCustomMoveInDate() {
		return $this->m_strCustomMoveInDate;
	}

	public function setCustomMoveInDate( $strCustomMoveInDate ) {
		$this->m_strCustomMoveInDate = $strCustomMoveInDate;
	}

	public function valId() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTypeId() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitId() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinExecutedRent() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxExecutedRent() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgExecutedRent() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastExecutedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinAdvertisedRent() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxAdvertisedRent() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgAdvertisedRent() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStdAdvertisedRent() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinOptimalRent() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxOptimalRent() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgOptimalRent() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStdOptimalRent() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinCompetitiveRent() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxCompetitiveRent() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgCompetitiveRent() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvailableUnitCount() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupiedUnitCount() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRentableUnitCount() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExposed30UnitCount() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExposed60UnitCount() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExposed90UnitCount() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDemandThirtyUnitCount() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDemandSixtyUnitCount() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDemandNinetyUnitCount() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWeeklyGrowthRate() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonthlyGrowthRate() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQuarterlyGrowthRate() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valYearlyGrowthRate() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFirstMoveInDate() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPricingMatrix() {

		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMatrix() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrmixValues['custom_optimal_rent'] ) && $boolDirectSet ) $this->m_strCustomOptimalRent = trim( stripcslashes( $arrmixValues['custom_optimal_rent'] ) );
		elseif( isset( $arrmixValues['custom_optimal_rent'] ) ) $this->setCustomOptimalRent( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['custom_optimal_rent'] ) : $arrmixValues['custom_optimal_rent'] );

		if( isset( $arrmixValues['custom_lease_month'] ) && $boolDirectSet ) $this->m_intCustomLeaseMonth = trim( stripcslashes( $arrmixValues['custom_lease_month'] ) );
		elseif( isset( $arrmixValues['custom_lease_month'] ) ) $this->setCustomLeaseMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['custom_lease_month'] ) : $arrmixValues['custom_lease_month'] );

		if( isset( $arrmixValues['custom_move_in_date'] ) && $boolDirectSet ) $this->strCustomMoveInDate = trim( stripcslashes( $arrmixValues['custom_move_in_date'] ) );
		elseif( isset( $arrmixValues['custom_move_in_date'] ) ) $this->setCustomMoveInDate( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['custom_move_in_date'] ) : $arrmixValues['custom_move_in_date'] );
	}

}
?>