<?php

use Applications\ResidentPortal\Library\Utils\CDateConverter;

class CCustomerGuest extends CBaseCustomerGuest {

	const NO_ACCESS			= 'no_access';
	const TODAYS_ACCESS		= 'todays_access';
	const CURRENT_ACCESS	= 'current_access';

	protected $m_strGuestType;
	protected $m_strDescriptiveText;
	protected $m_strGuestFrequencyType;
	protected $m_strCustomer;
	protected $m_strBldgUnit;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_arrstrFilteredGuestNames = [];
	protected $m_arrstrFilteredResidentConnection = [];
	protected $m_strCustomerGuestImageUri = '/images/_common/guest_image_blank.png';
	protected $m_intGuestActivityLogId;
	protected $m_strGuestCheckInTime;
	protected $m_strGuestCheckOutTime;
	protected $m_boolCheckInStatus = false;
	protected $m_strGuestFrequencyText;
	protected $m_boolIsGuestCheckedInValid = true;
	protected $m_strAccessStatus;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        if( NULL == $this->getCid() ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'A Client Id is required - CCustomerGuest' ) ) );
        	return false;
        }
        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;
        if( NULL == $this->getCustomerId() ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', __( 'A Customer Id is required - CCustomerGuest.' ) ) );
        	return false;
        }
        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;
        if( NULL == $this->getLeaseId() ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', __( 'A Lease Id is required - CCustomerGuest' ) ) );
        	return false;
        }
        return $boolIsValid;
    }

    public function valResident() {
        $boolIsValid = true;
        if( NULL == $this->getLeaseId() || NULL == $this->getCustomerId() ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_name', __( 'Please enter the resident name or unit number where the guest will be permitted.' ) ) );
        	return false;
        }
        return $boolIsValid;
    }

    public function valGuestTypeId() {
        $boolIsValid = true;
        if( true == is_null( $this->getGuestTypeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'guest_type_id', __( 'Guest Type is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valGuestFrequencyTypeId() {
       $boolIsValid = true;
       if( 0 == $this->getIsNoExpiration() && true == is_null( $this->getGuestFrequencyTypeId() ) ) {
       		$boolIsValid = false;
       		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'guest_frequency_type_id', __( 'Repeat is required.' ) ) );
       }
        return $boolIsValid;
    }

    public function valGuestName() {
        $boolIsValid = true;
        if( true == is_null( $this->getGuestName() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'guest_name', __( 'Guest Name is required.' ) ) );
        }
        return $boolIsValid;
    }

	public function valPreferredName() {
		$boolIsValid = true;
		if( true == is_null( $this->getPreferredName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'preferred_name', __( 'Preferred Name is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valGuestPhoneNumber( $boolIsPremium = false ) {
		if( true == $boolIsPremium ) {
			return true;
		}
		$boolIsValid = true;
		$intPrimaryNumberLength	= \Psi\CStringService::singleton()->strlen( $this->getPhoneNumber() );

		if( true == is_null( $this->getPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'guest_phone_number', __( 'Guest Phone Number is required.' ) ) );
		} elseif( 0 < $intPrimaryNumberLength && ( 10 > $intPrimaryNumberLength || 15 < $intPrimaryNumberLength ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'guest_phone_number', __( 'Guest Phone Number is invalid.' ) ) );
		}
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( true == is_null( $this->getEmailAddress() ) || 0 == strlen( trim( $this->getEmailAddress() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email is required.' ) );
			return $boolIsValid;

		} elseif( false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email does not appear to be valid.', 1 ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valGender() {
		$boolIsValid = true;

		if( false == isset( $this->m_strGender ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gender', 'Gender is required.' ) );
		}

		return $boolIsValid;
	}

    public function valWeekDays() {
        $boolIsValid = true;
        if( ( 0 == $this->getIsNoExpiration() && ( CGuestFrequencyType::WEEKLY == $this->getGuestFrequencyTypeId() || CGuestFrequencyType::WEEK_DAY == $this->getGuestFrequencyTypeId() ) && true == is_null( $this->getWeekDays() ) ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'week_days', __( 'Day of the week is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valStartTime() {
      	$boolIsValid = true;
        if( 0 == $this->getIsNoExpiration() && true == is_null( $this->getStartTime() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_time', __( 'Start time is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valEndTime() {
       $boolIsValid = true;

       if( 0 == $this->getIsNoExpiration() && true == is_null( $this->getEndTime() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_time', __( 'End time is required.' ) ) );
       } elseif( 0 == $this->getIsNoExpiration() && ( strtotime( $this->getStartDate() ) == strtotime( $this->getEndDate() ) || ( CGuestFrequencyType::NEVER == $this->getGuestFrequencyTypeId() && false == valStr( $this->getEndDate() ) ) ) && strtotime( $this->getStartTime() ) >= strtotime( $this->getEndTime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_time', __( 'End time should be greater than Start time.' ) ) );
       }
       return $boolIsValid;
    }

    public function valFrequency() {
        $boolIsValid = true;

        if( 0 == $this->getIsNoExpiration() && ( CGuestFrequencyType::NEVER != $this->getGuestFrequencyTypeId() && ( true == is_null( $this->getFrequency() ) || ( 0 >= $this->getFrequency() || 99 < $this->getFrequency() ) ) ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency', __( 'Please enter valid interval.' ) ) );
        }

        return $boolIsValid;
    }

    public function valMonthDays() {
        $boolIsValid = true;
        if( 0 == $this->getIsNoExpiration() && ( CGuestFrequencyType::MONTHLY == $this->getGuestFrequencyTypeId() ) && true == is_null( $this->getMonthDays() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'month_days', __( 'Day(s) of the month is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valStartDate() {
        $boolIsValid = false;

        switch( NULL ) {
        	default:
        		if( 0 == $this->getIsNoExpiration() && true == is_null( $this->getStartDate() ) ) {
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date is required.' ) ) );
        			break;
        		}

        		if( 0 == $this->getIsNoExpiration() && 1 !== CValidation::checkDate( $this->getStartDate() ) ) {
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_start', __( 'Start date must be in mm/dd/yyyy form and it must be correct.' ) ) );
        			break;
        		}

		        if( 0 == $this->getIsNoExpiration() && strtotime( '1/1/1970' ) > strtotime( $this->getStartDate() ) ) {
			        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_start', __( 'Start date should be greater than 1st January 1970.' ) ) );
			        break;
		        }

        		$boolIsValid = true;
        		break;
        }
        return $boolIsValid;
    }

    public function valEndDate() {
    	$boolIsValid = false;

    	switch( NULL ) {
    		default:
    			if( 0 == $this->getIsNoExpiration() && ( CGuestFrequencyType::NEVER != $this->getGuestFrequencyTypeId() && false == valStr( $this->getEndDate() ) ) ) {
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End Date is required.' ) ) );
    				break;
    			}

    			if( 0 == $this->getIsNoExpiration() && ( CGuestFrequencyType::NEVER != $this->getGuestFrequencyTypeId() && 1 !== CValidation::checkDate( $this->getEndDate() ) ) ) {
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date must be in mm/dd/yyyy form and it must be correct.' ) ) );
    				break;
    			}

    			if( 0 == $this->getIsNoExpiration() && ( CGuestFrequencyType::NEVER != $this->getGuestFrequencyTypeId() && true == valStr( $this->getStartDate() ) && strtotime( $this->getStartDate() ) > strtotime( $this->getEndDate() ) ) ) {
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date must be greater than or equal to start date.' ) ) );
    				break;
    			}

			    if( 0 == $this->getIsNoExpiration() && ( CGuestFrequencyType::NEVER != $this->getGuestFrequencyTypeId() && true == valStr( $this->getStartDate() ) && strtotime( date( 'Y-m-d H:i:s' ) ) > strtotime( $this->getEndDate() . ' ' . $this->getEndTime() ) ) ) {
				    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date and time must be greater than current date and time.' ) ) );
				    break;
			    }

    			$boolIsValid = true;
    			break;
    	}
    	return $boolIsValid;
    }

    public function validate( $strAction, $boolIsPremium = false, $arrobjPropertyPreferences = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {

        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		// DB required fields
        		$boolIsValid &= $this->valCustomerId();
        		$boolIsValid &= $this->valLeaseId();

        	case 'validate_guest_new_services':
        		$boolIsValid &= $this->valCid();
        		if( 'validate_guest_new_services' == $strAction ) {
        			$boolIsValid &= $this->valResident();
        		}
		        if( true == valArr( $arrobjPropertyPreferences ) && ( false == valObj( $arrobjPropertyPreferences['SHOW_GUEST_TYPE_FIELD'], 'CPropertyPreference' ) || ( true == valObj( $arrobjPropertyPreferences['SHOW_GUEST_TYPE_FIELD'], 'CPropertyPreference' ) && 2 == $arrobjPropertyPreferences['SHOW_GUEST_TYPE_FIELD']->getValue() ) ) ) {
			        $boolIsValid &= $this->valGuestTypeId();
		        }
		        $boolIsValid &= $this->valGuestName();
		        if( true == valArr( $arrobjPropertyPreferences ) && ( ( true == valObj( $arrobjPropertyPreferences['SHOW_PREFERRED_NAME_FIELD'], 'CPropertyPreference' ) && 2 == $arrobjPropertyPreferences['SHOW_PREFERRED_NAME_FIELD']->getValue() ) ) ) {
			        $boolIsValid &= $this->valPreferredName();
		        }
		        if( true == valArr( $arrobjPropertyPreferences ) && ( false == valObj( $arrobjPropertyPreferences['SHOW_PHONE_NUMBER_FIELD'], 'CPropertyPreference' ) || ( true == valObj( $arrobjPropertyPreferences['SHOW_PHONE_NUMBER_FIELD'], 'CPropertyPreference' ) && 2 == $arrobjPropertyPreferences['SHOW_PHONE_NUMBER_FIELD']->getValue() ) ) ) {
			        $boolIsValid &= $this->valGuestPhoneNumber( $boolIsPremium );
		        }
		        if( true == valArr( $arrobjPropertyPreferences ) && ( false == valObj( $arrobjPropertyPreferences['SHOW_EMAIL_ADDRESS_FIELD'], 'CPropertyPreference' ) || ( true == valObj( $arrobjPropertyPreferences['SHOW_EMAIL_ADDRESS_FIELD'], 'CPropertyPreference' ) && 2 == $arrobjPropertyPreferences['SHOW_EMAIL_ADDRESS_FIELD']->getValue() ) || ( true == valObj( $arrobjPropertyPreferences['SHOW_EMAIL_ADDRESS_FIELD'], 'CPropertyPreference' ) && 1 == $arrobjPropertyPreferences['SHOW_EMAIL_ADDRESS_FIELD']->getValue() && $this->getEmailAddress() != '' ) ) ) {
			        $boolIsValid &= $this->valEmailAddress();
		        }
		        if( true == valArr( $arrobjPropertyPreferences ) && ( ( true == valObj( $arrobjPropertyPreferences['SHOW_GENDER_FIELD'], 'CPropertyPreference' ) && 2 == $arrobjPropertyPreferences['SHOW_GENDER_FIELD']->getValue() ) ) ) {
			        $boolIsValid &= $this->valGender();
		        }
        		$boolIsValid &= $this->valGuestFrequencyTypeId();
          		$boolIsValid &= $this->valWeekDays();
        		$boolIsValid &= $this->valStartTime();
        		$boolIsValid &= $this->valEndTime();
        		$boolIsValid &= $this->valFrequency();
        		$boolIsValid &= $this->valMonthDays();
        		$boolIsValid &= $this->valStartDate();
        		$boolIsValid &= $this->valEndDate();
        		break;

	        case 'validate_customer_guest':
		        $boolIsValid &= $this->valCustomerId();
		        $boolIsValid &= $this->valLeaseId();
		        $boolIsValid &= $this->valCid();
		        $boolIsValid &= $this->valGuestTypeId();
		        $boolIsValid &= $this->valGuestName();
		        $boolIsValid &= $this->valFrequency();
		        $boolIsValid &= $this->valGuestFrequencyTypeId();
		        break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

	    if( true == isset( $arrmixValues['guest_type'] ) ) {
		    $this->setGuestType( $arrmixValues['guest_type'] );
	    }
	    if( true == isset( $arrmixValues['guest_frequency_type'] ) ) {
		    $this->setGuestFrequencyType( $arrmixValues['guest_frequency_type'] );
	    }
	    if( true == isset( $arrmixValues['bldg_unit'] ) ) {
		    $this->setBldgUnit( $arrmixValues['bldg_unit'] );
	    }
	    if( true == isset( $arrmixValues['customer'] ) ) {
		    $this->setCustomer( $arrmixValues['customer'] );
	    }
	    if( true == isset( $arrmixValues['property_id'] ) ) {
		    $this->setPropertyId( $arrmixValues['property_id'] );
	    }
	    if( true == isset( $arrmixValues['property_unit_id'] ) ) {
		    $this->setPropertyUnitId( $arrmixValues['property_unit_id'] );
	    }
	    if( true == isset( $arrmixValues['fullsize_uri'] ) ) {
		    $this->setCustomerGuestImageUri( $arrmixValues['fullsize_uri'] );
	    }
	    if( true == isset( $arrmixValues['check_in_time'] ) ) {
		    $this->setGuestCheckInTime( $arrmixValues['check_in_time'] );
	    }
	    if( true == isset( $arrmixValues['check_out_time'] ) ) {
		    $this->setGuestCheckOutTime( $arrmixValues['check_out_time'] );
	    }
	    if( true == isset( $arrmixValues['activity_log_id'] ) ) {
		    $this->setGuestActivityLogId( $arrmixValues['activity_log_id'] );
	    }
    	return;
    }

	public function getGuestType() {
		return $this->m_strGuestType;
	}

	public function setGuestType( $strGuestType ) {
		$this->m_strGuestType = $strGuestType;
	}

	public function getGuestFrequencyType() {
		return $this->m_strGuestFrequencyType;
	}

	public function setGuestFrequencyType( $strGuestFrequencyType ) {
		$this->m_strGuestFrequencyType = $strGuestFrequencyType;
	}

	public function setDescriptiveText( $strDescriptiveText ) {
		$this->m_strDescriptiveText = $strDescriptiveText;
	}

	public function setAccessStatus( $arrmixGuestReservationDateTime, $boolIsCheckKeyPermitted = true, $boolIsForceSet = false, $strFilterDate = NULL ) {

		if( true == $boolIsForceSet ) {
			$this->m_strAccessStatus = self::CURRENT_ACCESS;
		} else {
			$this->m_strAccessStatus = self::NO_ACCESS;

			if( true == $boolIsCheckKeyPermitted && false == $this->getIsKeyPermitted() ) {
				return;
			}

			$arrmixGuestReservationDateTime = rekeyArray( 'guest_reserved_date', $arrmixGuestReservationDateTime );

			$strSelectedDate = ( true == \valStr( $strFilterDate ) ) ? $strFilterDate : date( 'm/d/Y' );

			if( true == array_key_exists( $strSelectedDate, $arrmixGuestReservationDateTime ) ) {
				$strCurrentTimeStamp      = time();
				$boolIsFutureDateSelected = true;
				$strCurrentDate           = date( 'm/d/Y' );

				$this->m_strAccessStatus = self::TODAYS_ACCESS;

				if( strtotime( $strCurrentDate ) == strtotime( $strSelectedDate ) ) {
					$boolIsFutureDateSelected = false;
				}

				if( false == $boolIsFutureDateSelected
					&& strtotime( $arrmixGuestReservationDateTime[$strSelectedDate]['start_time'] ) <= $strCurrentTimeStamp
					&& $strCurrentTimeStamp <= strtotime( $arrmixGuestReservationDateTime[$strSelectedDate]['end_time'] ) ) {
						$this->m_strAccessStatus = self::CURRENT_ACCESS;
				}
			}
		}
	}

	public function getAccessStatus() {
		return $this->m_strAccessStatus;
	}

	public function loadDescriptiveText() {

		if( CGuestFrequencyType::NEVER == $this->getGuestFrequencyTypeId() ) {
			return '-';
		}

		if( CGuestFrequencyType::DAILY == $this->getGuestFrequencyTypeId() && 1 == $this->getFrequency() ) {
			return 'Daily Guest';
		}

		$arrstrDays = [];
		if( true == in_array( $this->getGuestFrequencyTypeId(), [ CGuestFrequencyType::WEEKLY, CGuestFrequencyType::WEEK_DAY ] ) ) {
			$arrstrDays = [ 'Su', 'M', 'T', 'W', 'Th', 'F', 'Sa' ];
		}

		$boolIsAddText = true;

		if( CGuestFrequencyType::WEEKLY == $this->getGuestFrequencyTypeId() && 1 == $this->getFrequency() ) {
			$strDescriptiveText = 'Weekly';
			$boolIsAddText = false;
		} else {
			$strDescriptiveText = 'On every ' . $this->getFrequency();

			if( 1 == $this->getFrequency() ) {
				$strDescriptiveText .= 'st';
			} elseif( 2 == $this->getFrequency() ) {
				$strDescriptiveText .= 'nd';
			} elseif( 3 == $this->getFrequency() ) {
				$strDescriptiveText .= 'rd';
			} else {
				$strDescriptiveText .= 'th';
			}

			switch( $this->getGuestFrequencyTypeId() ) {
				case CGuestFrequencyType::DAILY:
					$strDescriptiveText .= ' day';
					break;

				case CGuestFrequencyType::WEEKLY:
					$strDescriptiveText .= ' week';
					break;

				case CGuestFrequencyType::MONTHLY:
					$strDescriptiveText .= ' month';
					break;

				case CGuestFrequencyType::WEEK_DAY:
					$strDescriptiveText .= ' week of the month';
					break;

				default:
					$strDescriptiveText .= '';
					break;
			}
		}

		if( true == valStr( $this->getWeekDays() ) ) {
			if( true == $boolIsAddText ) {
				$strDescriptiveText .= ' for ';
			} else {
				$strDescriptiveText .= ' on ';
			}

			$arrintMonthDays = explode( ',',  $this->getWeekDays() );
			$intCount = 1;

			if( true == valArr( $arrstrDays ) ) {
				foreach( $arrintMonthDays as $intMonthDay ) {
					$strDescriptiveText .= $arrstrDays[$intMonthDay];
					if( $intCount++ < \Psi\Libraries\UtilFunctions\count( $arrintMonthDays ) ) {
						$strDescriptiveText .= ', ';
					}
				}
			}
		}

		return $strDescriptiveText;
	}

	public function getDescriptiveText() {
		return $this->m_strDescriptiveText;
	}

	public function getCustomer() {
		return $this->m_strCustomer;
	}

	public function setCustomer( $strCustomer ) {
		$this->m_strCustomer = $strCustomer;
	}

	public function getBldgUnit() {
		return $this->m_strBldgUnit;
	}

	public function setBldgUnit( $strBldgUnit ) {
		$this->m_strBldgUnit = $strBldgUnit;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = $intPropertyUnitId;
	}

	public function getCustomerGuestImageUri() {
		return $this->m_strCustomerGuestImageUri;
	}

	public function setCustomerGuestImageUri( $strCustomerGuestImageUri ) {
		$this->m_strCustomerGuestImageUri = ( true == valStr( $strCustomerGuestImageUri ) ? CConfig::get( 'media_library_path' ) . '/' . $strCustomerGuestImageUri : NULL );
	}

	public function getGuestActivityLogId() {
		return $this->m_intGuestActivityLogId;
	}

	public function setGuestActivityLogId( $intGuestActivityLogId ) {
		$this->m_intGuestActivityLogId = $intGuestActivityLogId;
	}

	public function getGuestCheckInTime() {
		return $this->m_strGuestCheckInTime;
	}

	public function setGuestCheckInTime( $strGuestCheckInTime ) {
		$this->m_strGuestCheckInTime = $strGuestCheckInTime;
	}

	public function getGuestCheckOutTime() {
		return $this->m_strGuestCheckOutTime;
	}

	public function setGuestCheckOutTime( $strGuestCheckOutTime ) {
		$this->m_strGuestCheckOutTime = $strGuestCheckOutTime;
	}

	public function getCheckInStatus() {
		return $this->m_boolCheckInStatus;
	}

	public function setCheckInStatus( $boolCheckInStatus ) {
		$this->m_boolCheckInStatus = $boolCheckInStatus;
	}

	public function getGuestFrequencyText() {
		return $this->m_strGuestFrequencyText;
	}

	public function setGuestFrequencyText( $strGuestFrequencyText ) {
		$this->m_strGuestFrequencyText = $strGuestFrequencyText;
	}

	public function getIsGuestCheckedInValid() {
		return $this->m_boolIsGuestCheckedInValid;
	}

	public function setIsGuestCheckedInValid( $boolIsGuestCheckedInValid ) {
		$this->m_boolIsGuestCheckedInValid = $boolIsGuestCheckedInValid;
	}

	public function getFilteredGuestNames() {
		return $this->m_arrstrFilteredGuestNames;
	}

	public function setFilteredGuestNames( $arrstrFilteredGuestNames ) {
		$this->m_arrstrFilteredGuestNames = $arrstrFilteredGuestNames;
	}

	public function getFilteredResidentConnection() {
		return $this->m_arrstrFilteredResidentConnection;
	}

	public function setFilteredResidentConnection( $arrstrFilteredResidentConnection ) {
		$this->m_arrstrFilteredResidentConnection = $arrstrFilteredResidentConnection;
	}

}
?>