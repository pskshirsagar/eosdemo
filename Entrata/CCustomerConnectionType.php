<?php

class CCustomerConnectionType extends CBaseCustomerConnectionType {

	const LANDLORD_REFERENCE 				= 1;
	const TYPE_2ND_LANDLORD_REFERENCE 		= 2;
	const TYPE_3RD_LANDLORD_REFERENCE   	= 3;
	const PERSONAL_REFERENCE 				= 4;
	const TYPE_2ND_PERSONAL_REFERENCE 		= 5;
	const TYPE_3RD_PERSONAL_REFERENCE 		= 6;
	const EMERGENCY_CONTACT 				= 7;
	const CREDITOR 							= 8;
	const TYPE_2ND_CREDITOR 				= 9;
	const TYPE_3RD_CREDITOR 				= 10;
	const BILLING 							= 11;
	const LEGAL								= 12;
	const PRIMARY 							= 13;
	const REPRESENTATIVE 					= 14;
	const MAINTENANCE 						= 15;

	public static $c_arrintTenantCustomerConnectionTypes = [ self::BILLING, self::PRIMARY, self::LEGAL, self::MAINTENANCE ];

	public static $c_arrintGroupCustomerConnectionTypes = [ self::BILLING, self::PRIMARY, self::LEGAL, self::MAINTENANCE ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getGroupCustomerConnectionTypes() {
		$arrmixGroupCustomerConnectionTypes = [
			self::BILLING 			=> __( 'Billing' ),
			self::PRIMARY 			=> __( 'Primary' ),
			self::LEGAL 			=> __( 'Legal' ),
			self::MAINTENANCE 		=> __( 'Maintenance' )
		];

		return $arrmixGroupCustomerConnectionTypes;
	}

}
?>