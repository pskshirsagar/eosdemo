<?php

class CLeaseIntervalInstallmentPlan extends CBaseLeaseIntervalInstallmentPlan {

	protected $m_intLedgerFilterId;

	public function valId() {
		$boolIsValid = true;
		if( false == valId( $this->m_intId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Id is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCidId ) ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valLeaseIntervalId() {
		$boolIsValid = true;
		if( false == isset( $this->m_intLeaseIntervalId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_interval_id', __( 'Lease interval id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valInstallmentPlanId() {
		$boolIsValid = true;
		if( false == valId( $this->m_intInstallmentPlanId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'installment_plan_id', __( 'Installment plan id is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valLeaseIntervalId();
				$boolIsValid &= $this->valInstallmentPlanId();
				break;

			case VALIDATE_DELETE:
				$this->valInstallmentPlanId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Get Methods
	 **/

	public function getLedgerFilterId() {
		return $this->m_intLedgerFilterId;
	}

	/**
	 * Set Methods
	 **/

	public function setLedgerFilterId( $intLedgerFilterid ) {
		$this->m_intLedgerFilterId = $intLedgerFilterid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( isset( $arrmixValues['ledger_filter_id'] ) && $boolDirectSet ) {
			$this->set( 'm_intLedgerFilterId', trim( $arrmixValues['ledger_filter_id'] ) );
		} elseif( isset( $arrmixValues['ledger_filter_id'] ) ) {
			$this->setLedgerFilterId( $arrmixValues['ledger_filter_id'] );
		}
	}

}
?>