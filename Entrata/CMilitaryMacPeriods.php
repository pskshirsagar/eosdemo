<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMilitaryMacPeriods
 * Do not add any new functions to this class.
 */

class CMilitaryMacPeriods extends CBaseMilitaryMacPeriods {

	public static function fetchMilitaryMacPeriodsListingsBySearchFieldsByCid( $intCid, $objDatabase, $intLimit = NULL, $intOffset = 0, $strSqlSearchWhere = NULL, $boolIsFromCompleted = false ) {

		if( true == $boolIsFromCompleted ) {
			$strCondition = 'AND mmp.mimo_accepted_on IS NOT NULL AND mmp.mimo_accepted_by IS NOT NULL
							AND mmp.promo_demo_processed_on IS NOT NULL AND mmp.promo_demo_processed_by IS NOT NULL
							AND mmp.payments_processed_on IS NOT NULL AND mmp.payments_processed_by IS NOT NULL';
		} else {
			$strCondition = 'AND ( mmp.mimo_accepted_on IS NULL
							OR mmp.promo_demo_processed_on IS NULL
							OR mmp.payments_processed_on IS NULL )';
		}

		$strSql = 'SELECT
						mmp.*,
						mi.name as installation_name,
						COALESCE ( ce1.name_first || \' \' || ce1.name_last, cu1.username ) AS mimo_generated_username,
						COALESCE ( ce2.name_first || \' \' || ce2.name_last, cu2.username ) AS promo_demo_processed_username,
						COALESCE ( ce3.name_first || \' \' || ce3.name_last, cu3.username ) AS payments_processed_username
					FROM
						military_mac_periods mmp
						JOIN military_installations mi ON ( mi.id = mmp.military_installation_id )
						LEFT JOIN company_users cu1 ON ( cu1.cid = mmp.cid AND cu1.id = mmp.mimo_generated_by )
						LEFT JOIN company_employees ce1 ON ( ce1.cid = cu1.cid AND ce1.id = cu1.company_employee_id )
						LEFT JOIN company_users cu2 ON ( cu2.cid = mmp.cid AND cu2.id = mmp.promo_demo_processed_by )
						LEFT JOIN company_employees ce2 ON ( ce2.cid = cu2.cid AND ce2.id = cu2.company_employee_id )
						LEFT JOIN company_users cu3 ON ( cu3.cid = mmp.cid AND cu3.id = mmp.payments_processed_by )
						LEFT JOIN company_employees ce3 ON ( ce3.cid = cu3.cid AND ce3.id = cu3.company_employee_id )
					WHERE
						mmp.cid = ' . ( int ) $intCid
						. $strCondition;

		if( true == valStr( $strSqlSearchWhere ) ) {
			$strSql .= $strSqlSearchWhere;
		}

		$strSql .= ' ORDER BY
                    mmp.post_month DESC, mmp.id DESC
                LIMIT ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMilitaryMacPeriodsCountBySearchFields( $intCid, $strWhere, $objDatabase, $boolIsFromCompleted = false ) {

		if( true == $boolIsFromCompleted ) {
			$strCondition = 'AND mmp.mimo_accepted_on IS NOT NULL AND mmp.mimo_accepted_by IS NOT NULL
							AND mmp.promo_demo_processed_on IS NOT NULL AND mmp.promo_demo_processed_by IS NOT NULL
							AND mmp.payments_processed_on IS NOT NULL AND mmp.payments_processed_by IS NOT NULL';
		} else {
			$strCondition = 'AND ( ( mmp.mimo_accepted_on IS NULL OR mmp.promo_demo_processed_on IS NULL OR mmp.payments_processed_on IS NULL )
			OR ( mmp.mimo_accepted_on IS NOT NULL AND mmp.promo_demo_processed_on IS NOT NULL AND mmp.payments_processed_on IS NOT NULL ) )';
		}

		$strSql = 'SELECT
						count( 1 ) count
					FROM
						military_mac_periods mmp
					WHERE
						mmp.cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						' . $strCondition;

		if( true == valStr( $strWhere ) ) {
			$strSql .= $strWhere;
		}

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchFileUploadedMilitaryMacPeriodCountByMacPeriodIdByCid( $intId, $boolIsFromPromoDemo, $boolIsFromPayments, $intCid, $objDatabase ) {

		$strCondition = ' AND deleted_by IS NULL AND deleted_on IS NULL';

		if( true == $boolIsFromPromoDemo ) {
			$strCondition .= ' AND promo_demo_processed_by IS NOT NULL AND promo_demo_processed_on IS NOT NULL';
		} elseif( true == $boolIsFromPayments ) {
			$strCondition .= ' AND payments_processed_by IS NOT NULL AND payments_processed_on IS NOT NULL';
		}

		$strWhereSql = ' WHERE id = ' . ( int ) $intId . ' AND cid = ' . ( int ) $intCid . $strCondition;
		return parent::fetchRowCount( $strWhereSql, 'military_mac_periods', $objDatabase );

	}

	public static function buildMoveInDataByMilitaryInstallationIdByDateRangeByCid( $intMilitaryInstallationId, $strStartDate, $strEndDate, $intCid, $objDatabase ) {

		if( false == valId( $intMilitaryInstallationId ) ) return NULL;

		$strSql = 'SELECT
						\'MI\' as move_in,
						c.tax_number_encrypted,
						lp.move_in_date,
						translate( c.name_first, \',\', \'\' ) as name_first,
						substr( c.name_middle, 1, 10 ) as name_middle,
						translate( c.name_last, \',\', \'\' ) as name_last,
						CASE cmd.military_component_id WHEN ' . CMilitaryComponent::ARMY . ' THEN \'AR\' WHEN ' . CMilitaryComponent::AIRFORCE . ' THEN \'AF\' WHEN ' . CMilitaryComponent::NAVY . ' THEN \'NY\' END as military_component_name,
						substr( ua.street_line1, 1, position( \' \' IN ua.street_line1 ) ) street_line1_upto_space,
						substr( substr( ua.street_line1, position( \' \' IN ua.street_line1 ) + 1, length( ua.street_line1 ) - position( \' \' IN ua.street_line1 ) + 1 ), 1, 50 ) street_line_after_space,
						\'\' as JColumn,
						ua.city,
						ua.state_code,
						ua.postal_code,
						substr( pu.military_neighborhood_code, 1, 50 ) as military_neighborhood_code,
						substr( cast( l.id as TEXT ), 1, 10 ) as le_id,
						cmd.duty_address_postal_code,
						mmst.code as military_marital_status_type,
						mdst.code as military_dependent_status_type,
						mdr.name as military_dependent_relationship,
						sub.charge_amount,
						mpg.pay_grade_mac_id,
						\'\' as VColumn,
						1 as WColumn,
						0 as XColumn,
						0 as YColumn,
						CASE WHEN sub1.military_concession_amount IS NOT NULL THEN 0 ELSE 1 END as is_full_bah,
						l.id as lease_id
					FROM
						property_details pd
						JOIN leases l ON ( pd.cid = l.cid AND pd.property_id = l.property_id )
						JOIN lease_intervals li ON ( li.cid = l.cid AND li.lease_id = l.id AND li.id = l.active_lease_interval_id )
						JOIN customers c ON ( c.cid = l.cid AND c.id = l.primary_customer_id )
						JOIN lease_processes lp ON ( lp.cid = l.cid AND lp.lease_id = l.id AND lp.customer_id IS NULL )
						LEFT JOIN (
							SELECT 
								SUM( sc.charge_amount ) as charge_amount,
								le.id as lease_id,
								le.property_id,
								le.cid
							FROM 
								scheduled_charges sc
								JOIN leases le on ( le.cid = sc.cid AND le.id = sc.lease_id )
								JOIN lease_processes lp1 ON ( lp1.cid = le.cid AND lp1.lease_id = le.id AND lp1.customer_id IS NULL )
								JOIN ar_codes arc ON ( arc.cid = sc.cid AND arc.id = sc.ar_code_id )
								JOIN ledger_filters lf ON ( lf.cid = arc.cid AND lf.id = arc.ledger_filter_id AND lf.default_ledger_filter_id = ' . CDefaultLedgerFilter::MILITARY . ' )
							WHERE
								sc.ar_trigger_id = ' . CArTrigger::MONTHLY . '
								AND sc.deleted_on IS NULL
								AND sc.is_unselected_quote = false
								AND sc.lease_interval_id = le.active_lease_interval_id
								AND CASE WHEN lp1.move_in_date = cast( date_trunc( \'month\', lp1.move_in_date ) as date )
									THEN lp1.move_in_date BETWEEN sc.charge_start_date AND COALESCE ( sc.charge_end_date, \'12/31/2099\' )
									ELSE cast( date_trunc(\'month\', lp1.move_in_date ) + interval \'1 month\' as date ) BETWEEN sc.charge_start_date AND COALESCE ( sc.charge_end_date, \'12/31/2099\' )
								END
							GROUP BY 
								le.id, le.property_id, le.cid
							) sub ON ( sub.cid = pd.cid AND sub.property_id = pd.property_id AND sub.lease_id = l.id )
						LEFT JOIN (
							SELECT 
								SUM( sc1.charge_amount ) as military_concession_amount,
								le1.id as lease_id,
								le1.property_id,
								le1.cid
							FROM 
								scheduled_charges sc1
								JOIN leases le1 on ( le1.cid = sc1.cid AND le1.id = sc1.lease_id )
								JOIN ar_codes arc1 ON ( arc1.cid = sc1.cid AND arc1.id = sc1.ar_code_id AND arc1.default_ar_code_id = ' . CDefaultArCode::MILITARY_CONCESSION . ' )
							WHERE
								sc1.ar_trigger_id = ' . CArTrigger::MONTHLY . ' 
								AND sc1.deleted_on IS NULL
								AND sc1.is_unselected_quote = false 
								AND sc1.lease_interval_id = le1.active_lease_interval_id 
							GROUP BY 
								le1.id, le1.property_id, le1.cid
							) sub1 ON ( sub1.cid = pd.cid AND sub1.property_id = pd.property_id AND sub1.lease_id = l.id )
						LEFT JOIN unit_addresses ua ON ( ua.cid = l.cid AND ua.property_id = l.property_id AND ua.property_unit_id = l.property_unit_id AND ua.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN unit_spaces us ON ( us.cid = l.cid AND us.property_id = l.property_id AND us.id = l.unit_space_id )
						LEFT JOIN customer_military_details cmd ON ( cmd.cid = l.cid AND cmd.customer_id = l.primary_customer_id ) /* Not sure customer will always have entry */
						LEFT JOIN property_units pu ON ( l.cid = pu.cid AND l.property_unit_id = pu.id )
						LEFT JOIN military_installations mi ON ( mi.id = pd.military_installation_id AND mi.is_published = true )
						LEFT JOIN military_marital_status_types mmst ON ( mmst.id = cmd.military_marital_status_type_id AND mmst.is_published = true )
						LEFT JOIN military_dependent_status_types mdst ON ( mdst.id = cmd.military_dependent_status_type_id AND mdst.is_published = true )
						LEFT JOIN military_dependent_relationships mdr ON ( mdr.id = cmd.military_dependent_relationship_id AND mdr.is_published = true )
						LEFT JOIN military_pay_grades mpg ON ( mpg.id = cmd.military_pay_grade_id AND mpg.is_published = true )
					WHERE
						l.cid = ' . ( int ) $intCid . '
						 AND 0 = CASE WHEN ( li.lease_interval_type_id = ' . \CLeaseIntervalType::TRANSFER . ' AND lp.is_transferring_in = 1 ) THEN 1
                          			  WHEN  ( li.lease_interval_type_id = ' . \CLeaseIntervalType::RENEWAL . ' AND lp.is_transferring_in = 0 ) THEN 1    
                            	 ELSE 0 
                            	 END
						AND l.occupancy_type_id = ' . COccupancyType::MILITARY . '
						AND li.lease_status_type_id IN ( ' . implode( ',', CLeaseStatusType::$c_arrintActivatedLeaseStatusTypes ) . ' )
						AND lp.move_in_date BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
						AND pd.military_installation_id = ' . ( int ) $intMilitaryInstallationId . '
						AND cmd.military_component_id IN ( ' . CMilitaryComponent::ARMY . ', ' . CMilitaryComponent::AIRFORCE . ', ' . CMilitaryComponent::NAVY . ' )
						';

		return fetchData( $strSql, $objDatabase );
	}

	public static function buildMoveOutDataByMilitaryInstallationIdByDateRangeByCid( $intMilitaryInstallationId, $strStartDate, $strEndDate, $intCid, $objDatabase ) {

		if( false == valId( $intMilitaryInstallationId ) ) return NULL;

		$strSql = 'SELECT
						\'MO\' AS move_out,
						c.tax_number_encrypted,
						lp.move_out_date,
						lp.notice_date,
						translate ( c.name_first, \',\', \'\' ) AS name_first,
						substr ( c.name_middle, 1, 10 ) AS name_middle,
						translate ( c.name_last, \',\', \'\' ) AS name_last,
						CASE cmd.military_component_id WHEN ' . CMilitaryComponent::ARMY . ' THEN \'AR\' WHEN ' . CMilitaryComponent::AIRFORCE . ' THEN \'AF\' WHEN ' . CMilitaryComponent::NAVY . ' THEN \'NY\' END as military_component_name,
						l.id as lease_id
					FROM
						property_details pd
						JOIN leases l ON ( pd.cid = l.cid AND pd.property_id = l.property_id )
						JOIN lease_intervals li ON ( li.cid = l.cid AND li.lease_id = l.id AND li.id = l.active_lease_interval_id )
						JOIN customers c ON ( c.cid = l.cid AND c.id = l.primary_customer_id )
						JOIN lease_processes lp ON ( lp.cid = l.cid AND lp.lease_id = l.id AND lp.customer_id IS NULL )
						JOIN customer_military_details cmd ON ( cmd.cid = l.cid AND cmd.customer_id = l.primary_customer_id )
					WHERE
						l.cid = ' . ( int ) $intCid . '
						AND ( lp.transfer_lease_id IS NULL AND lp.transfer_unit_space_id IS NULL )
						AND l.occupancy_type_id = ' . COccupancyType::MILITARY . '
						AND li.lease_status_type_id = ' . CLeaseStatusType::PAST . '
						AND lp.move_out_date BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
						AND pd.military_installation_id = ' . ( int ) $intMilitaryInstallationId . '
						AND cmd.military_component_id IN ( ' . CMilitaryComponent::ARMY . ', ' . CMilitaryComponent::AIRFORCE . ', ' . CMilitaryComponent::NAVY . ' )';

		return fetchData( $strSql, $objDatabase );
	}

}
?>
