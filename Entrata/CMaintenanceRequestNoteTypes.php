<?php

class CMaintenanceRequestNoteTypes extends CBaseMaintenanceRequestNoteTypes {

	public static function fetchMaintenanceRequestNoteTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMaintenanceRequestNoteType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchMaintenanceRequestNoteType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMaintenanceRequestNoteType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>