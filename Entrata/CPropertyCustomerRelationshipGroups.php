<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyCustomerRelationshipGroups
 * Do not add any new functions to this class.
 */

class CPropertyCustomerRelationshipGroups extends CBasePropertyCustomerRelationshipGroups {

	public static function fetchPropertyCustomerRelationshipGroupsByPropertyIdsByOccupancyTypeIdByCid( $arrintPropertyIds, $intOccupancyTypeId = COccupancyType::CONVENTIONAL, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return;

		$strSql = ' SELECT
						*
					FROM
						property_customer_relationship_groups pcrg
					WHERE
						pcrg.cid =' . ( int ) $intCid . '
						AND pcrg.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pcrg.occupancy_type_id = ' . ( int ) $intOccupancyTypeId;

		return parent::fetchPropertyCustomerRelationshipGroups( $strSql, $objDatabase );

	}

	public static function fetchPropertyCustomerRelationshipGroupByOccupancyTypeIdByPropertyIdByCid( $intOccupancyTypeId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
				*
			FROM
				property_customer_relationship_groups pcrg
			WHERE
				pcrg.cid = ' . ( int ) $intCid . '
				AND pcrg.property_id = ' . ( int ) $intPropertyId . '
				AND pcrg.occupancy_type_id = ' . ( int ) $intOccupancyTypeId;

		return parent::fetchPropertyCustomerRelationshipGroup( $strSql, $objDatabase );

	}
}
?>