<?php

class CIntegrationServiceLog extends CBaseIntegrationServiceLog {

	protected $m_strIntegrationDatabaseName;
	protected $m_strIntegrationClientTypeName;
	protected $m_strIntegrationServiceName;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

           	default:
           		$boolIsValid = true;
           		break;
        }

        return $boolIsValid;
    }

    /**
     * Set Functions
     */

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrValues['integration_database_name'] ) ) $this->setIntegrationDatabaseName( $arrValues['integration_database_name'] );
        if( true == isset( $arrValues['integration_client_type_name'] ) ) $this->setIntegrationClientTypeName( $arrValues['integration_client_type_name'] );
        if( true == isset( $arrValues['integration_service_name'] ) ) $this->setIntegrationServiceName( $arrValues['integration_service_name'] );
    }

	public function setIntegrationServiceName( $strIntegrationServiceName ) {
		$this->m_strIntegrationServiceName = $strIntegrationServiceName;
    }

	public function setIntegrationClientTypeName( $strIntegrationClientTypeName ) {
		$this->m_strIntegrationClientTypeName = $strIntegrationClientTypeName;
    }

	public function setIntegrationDatabaseName( $strIntegrationDatabaseName ) {
		$this->m_strIntegrationDatabaseName = $strIntegrationDatabaseName;
    }

    /**
     * Get Functions
     */

	public function getIntegrationServiceName() {
		return $this->m_strIntegrationServiceName;
    }

	public function getIntegrationClientTypeName() {
		return $this->m_strIntegrationClientTypeName;
    }

    public function getIntegrationDatabaseName() {
		return $this->m_strIntegrationDatabaseName;
    }
}
?>