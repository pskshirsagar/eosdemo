<?php

class CCustomerAuthentication extends CBaseCustomerAuthentication {

    /**
     * Get Functions
     */

	public function getPassword() {
		return CEncryption::decryptText( $this->getPasswordEncrypted(), CONFIG_KEY_LOGIN_PASSWORD );
	}

	/**
	 * Get Functions
	 */

    public function valCid() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCid())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ));
        // }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getPropertyId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCustomerId( )) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCustomerAuthenticationTypeId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCustomerAuthenticationTypeId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_authentication_type_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUsername() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getUsername() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPasswordEncrypted() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getPasswordEncrypted() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password_encrypted', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = false;
            	break;
        }

        return $boolIsValid;
    }
}
?>