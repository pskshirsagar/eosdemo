<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CComparableOptions
 * Do not add any new functions to this class.
 */

class CComparableOptions extends CBaseComparableOptions {

	public static function fetchComparableOptions( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CComparableOption', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchComparableOption( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CComparableOption', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchComparableOptionsByDefaultAmenityIds( $arrintDefaultAmenityId, $objDatabase ) {
		if( false == valArr( $arrintDefaultAmenityId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						comparable_options
					WHERE
						default_amenity_id IN ( ' . implode( ',', $arrintDefaultAmenityId ) . ' )
						AND is_published = true
					ORDER BY
						order_num asc';

		return parent::fetchComparableOptions( $strSql, $objDatabase );
	}

	public static function fetchComparableOptionsByUtilityTypeIds( $arrintUtilityTypeIds = NULL, $objDatabase ) {
		if( false == valArr( $arrintUtilityTypeIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						comparable_options
					WHERE
						is_published =  true::boolean
						AND utility_type_id IN ( ' . implode( ',', $arrintUtilityTypeIds ) . ' )
						AND is_published = true
					ORDER BY
						order_num asc';

		return self::fetchComparableOptions( $strSql, $objDatabase );
	}
}
?>