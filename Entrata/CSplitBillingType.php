<?php

class CSplitBillingType extends CBaseSplitBillingType {

	const FIXED_DATES 			= 1;
	const CONFIGURABLE_DATES	= 2;
	const CYCLICAL_WEEK_DAYS	= 3;

}
?>