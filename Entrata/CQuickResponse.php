<?php

class CQuickResponse extends CBaseQuickResponse {

	protected $m_strPropertyName;
	protected $m_arrintSelectedPropertyIds;
	protected $m_intPropertyQuickResponseId;

	/**
	 * Get Functions
	 */

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getSelectedPropertyIds() {
		return $this->m_arrintSelectedPropertyIds;
	}

	public function getPropertyQuickResponseId() {
		return $this->m_intPropertyQuickResponseId;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) )				$this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['property_quick_response_id'] ) )	$this->setPropertyQuickResponseId( $arrmixValues['property_quick_response_id'] );

		return;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setSelectedPropertyIds( $arrintSelectedPropertyIds ) {
		$this->m_arrintSelectedPropertyIds = $arrintSelectedPropertyIds;
	}

	public function setPropertyQuickResponseId( $intPropertyQuickResponseId ) {
		$this->m_intPropertyQuickResponseId = $intPropertyQuickResponseId;
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || 1 > ( $this->m_intCid = ( int ) $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

    public function valSettingsTemplateId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
    	$boolIsValid		= true;
    	$this->m_strName	= trim( $this->m_strName );

    	if( false == isset( $this->m_strName ) || 3 > \Psi\CStringService::singleton()->strlen( $this->m_strName ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'A name must contain at least 3 characters.' ) ) );
    		return false;
    	}

    	$strName = preg_replace( '/[^a-zA-Z0-9_()\-\s]/', '', $this->m_strName );

    	if( \Psi\CStringService::singleton()->strlen( $this->m_strName ) != \Psi\CStringService::singleton()->strlen( $strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "Only alphanumeric characters [0 to 9, A to Z ,'-' , '_', '(', ')' and a-z ] are allowed in name." ) ) );
    		return false;
    	}

    	$this->m_strName = preg_replace( '/(\s\s+)/', ' ', $this->m_strName );

    	return $boolIsValid;
    }

    public function valSelectProperty() {

    	$boolIsValid = true;

		if( false == valArr( $this->m_arrintSelectedPropertyIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_ids', __( 'Please select at least one property.' ) ) );
			$boolIsValid = false;
		}

    	return $boolIsValid;
    }

    public function valMessage() {
    	$boolIsValid = true;

    	if( true == is_null( trim( $this->m_strMessage ) ) || 0 == strlen( trim( $this->m_strMessage ) ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', __( 'Quick Response is required.' ) ) );
    	}

    	return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valName();
        		$boolIsValid &= $this->valMessage();
         		$boolIsValid &= $this->valSelectProperty();
        		break;

        	case VALIDATE_DELETE:
        		$boolIsValid &= $this->valCid();
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function archiveScreeningNotifications( $intCurrentUserId, $objDatabase ) {
		CScreeningNotifications::archiveScreeningNotificationsByQuickResponseIdByCid( $intCurrentUserId, $this->getId(), $this->getCid(), $objDatabase );
    }

	public function archiveScreeningNotificationsProperties( $intCurrentUserId, $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return;
		CScreeningNotifications::archiveScreeningNotificationsByQuickResponseIdByCid( $intCurrentUserId, $this->getId(), $this->getCid(), $objDatabase, $arrintPropertyIds );
	}

}
?>