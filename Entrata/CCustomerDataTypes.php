<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerDataTypes
 * Do not add any new functions to this class.
 */

class CCustomerDataTypes extends CBaseCustomerDataTypes {

	public static function fetchCustomerDataTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCustomerDataType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCustomerDataType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCustomerDataType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>