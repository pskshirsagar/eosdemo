<?php

class CArCodeGroup extends CBaseArCodeGroup {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == valId( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'cid', __( 'Valid client id is required.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valArCodeGroupTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeSummaryTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultArCodeGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );

		} elseif( true == valObj( $objDatabase, 'CDatabase' ) ) {

			$intDuplicateArCodeGroupCount = 0;

			$intDuplicateArCodeGroupCount = \Psi\Eos\Entrata\CArCodeGroups::createService()->fetchArCodeGroupCountByIdByNameByCid( $this->getId(), $this->getName(), $this->getCid(), $objDatabase );

			if( 0 < $intDuplicateArCodeGroupCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Duplicate charge code group name.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= ( true == $boolIsValid ) ? $this->valName( $objDatabase ) : $boolIsValid;
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;

	}

}

?>