<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyMergeFieldDocuments
 * Do not add any new functions to this class.
 */

class CCompanyMergeFieldDocuments extends CBaseCompanyMergeFieldDocuments {

    public function fetchCompanyMergeFieldDocumentsDataByCompanyMergeFieldIdByCid( $intCompanyMergeFieldId, $intCid, $objDatabase ) {

        if( false == valId( $intCompanyMergeFieldId ) ) return NULL;

        $strSql = 'SELECT * 
					FROM (
						SELECT
                        DISTINCT ON (d.name) d.*,
                        cmfd.*,
                        d.name as document_name,
                        d.is_bluemoon                                              
                   FROM
                        company_merge_field_documents cmfd
                        JOIN company_merge_fields cmf ON ( cmf.cid = cmfd.cid AND cmf.id = cmfd.company_merge_field_id )
                        JOIN documents d ON ( d.id = cmfd.document_id AND d.cid = cmfd.cid AND d.deleted_on IS NULL AND d.archived_on IS NULL )
                   WHERE
                        cmfd.cid = ' . ( int ) $intCid . '
                        AND cmfd.company_merge_field_id = ' . ( int ) $intCompanyMergeFieldId . '
                        AND cmfd.deleted_on IS NULL
                    ) AS d    
                   ORDER BY
                        ' . $objDatabase->getCollateSort( 'd.name', true );
        return fetchData( $strSql, $objDatabase );

    }

    public function fetchCompanyMergeFieldDocumentsByDocumentIdsByCid( $arrintDocumentIds, $intCid, $objDatabase ) {

    	if( false == valArr( $arrintDocumentIds ) ) return [];

    	$strSql = 'SELECT
    	                * 
    	            FROM 
    	                company_merge_field_documents
    	            WHERE
    	                cid = ' . ( int ) $intCid . '
    	                AND document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
    	                AND deleted_on IS NULL';

    	return self::fetchCompanyMergeFieldDocuments( $strSql, $objDatabase );
    }

}
?>