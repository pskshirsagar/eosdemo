<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeasingGoalAssociations
 * Do not add any new functions to this class.
 */

class CLeasingGoalAssociations extends CBaseLeasingGoalAssociations {

	public static function fetchLeasingGoalAssociationsByLeasingGoalIdByCid( $intLeasingGoalId, $intCid, $objDatabase, $strSelectFields = NULL ) {

		if( false == valId( $intLeasingGoalId ) ) return [];

		$strSql = 'SELECT
						' . ( ( true == valStr( $strSelectFields ) ) ? $strSelectFields : 'lga.*' ) . '
					FROM
						leasing_goal_associations lga
						JOIN leasing_goals lg On ( lga.cid = lg.cid AND lga.leasing_goal_id = lg.id )
					WHERE
						lg.id = ' . ( int ) $intLeasingGoalId . '
						AND lg.cid = ' . ( int ) $intCid . '
						AND lg.deleted_by IS NULL
						AND lg.deleted_on IS NULL
						AND lga.deleted_by IS NULL
						AND lga.deleted_on IS NULL';

		if( true == valStr( $strSelectFields ) ) {
			return fetchData( $strSql, $objDatabase );
		}

		return parent::fetchLeasingGoalAssociations( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermsByPropertyIdByGoalIdByCid( $intPropertyId, $intLeasingGoalId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intLeasingGoalId ) ) return NULL;

		$strSql = 'SELECT 
						DISTINCT lga.lease_start_window_id, 
						util_get_translated( \'name\', lt.name, lt.details ) AS name,
						CASE 
							WHEN lt.is_renewal = true AND lt.is_prospect = false THEN 
								lsw.renewal_start_date
							ELSE
								lsw.start_date
						END AS start_date,
						lsw.end_date
					FROM leasing_goal_associations lga
						JOIN leasing_goals lg ON lg.cid = lga.cid AND lg.id = lga.leasing_goal_id AND lg.deleted_by IS NULL
						JOIN lease_terms lt ON lga.cid = lt.cid AND lga.lease_term_id = lt.id AND lt.deleted_by IS NULL
						JOIN lease_start_windows lsw ON lga.cid = lsw.cid AND lg.property_id = lsw.property_id AND lga.lease_term_id = lsw.lease_term_id AND lga.lease_start_window_id = lsw.id AND lsw.deleted_by IS NULL
					WHERE 
						lg.property_id = ' . ( int ) $intPropertyId . ' 
						AND lg.id = ' . ( int ) $intLeasingGoalId . '
						AND lg.cid = ' . ( int ) $intCid . '
						AND lga.deleted_by IS NULL';

		$arrmixResult = fetchData( $strSql, $objDatabase );
		return $arrmixResult;
	}

}
?>