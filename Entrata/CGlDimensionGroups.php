<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlDimensionGroups
 * Do not add any new functions to this class.
 */

class CGlDimensionGroups extends CBaseGlDimensionGroups {

	public static function fetchSystemGlDimensionGroupByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_dimension_groups
					WHERE
						cid = ' . ( int ) $intCid;

		return self::fetchGlDimensionGroup( $strSql, $objClientDatabase );
	}
}
?>