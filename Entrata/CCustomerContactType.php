<?php

class CCustomerContactType extends CBaseCustomerContactType {

	const LANDLORD_REFERENCE 				= 1;
	const TYPE_2ND_LANDLORD_REFERENCE 		= 2;
	const TYPE_3RD_LANDLORD_REFERENCE   	= 3;
	const PERSONAL_REFERENCE 				= 4;
	const TYPE_2ND_PERSONAL_REFERENCE 		= 5;
	const TYPE_3RD_PERSONAL_REFERENCE 		= 6;
	const EMERGENCY_CONTACT 				= 7;
	const CREDITOR 							= 8;
	const TYPE_2ND_CREDITOR 				= 9;
	const TYPE_3RD_CREDITOR 				= 10;
	const BILLING 							= 11;
	const LEGAL								= 12;
	const PRIMARY 							= 13;
	const REPRESENTATIVE 					= 14;
	const MAINTENANCE 						= 15;

	public static $c_arrintTenantCustomerContactTypes = [ self::BILLING, self::PRIMARY, self::LEGAL, self::MAINTENANCE ];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getCommercialCustomerContactTypes() {
		$arrmixCommercialCustomerContactTypes = [
			self::BILLING 			=> __( 'Billing' ),
			self::PRIMARY 			=> __( 'Primary' ),
			self::LEGAL 			=> __( 'Legal' ),
			self::MAINTENANCE 		=> __( 'Maintenance' )
		];

		return $arrmixCommercialCustomerContactTypes;
	}

}
?>