<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenuePostRents
 * Do not add any new functions to this class.
 */

class CRevenuePostRents extends CBaseRevenuePostRents {

	public static function fetchRevenuePostRentsByPropertyIdsUnitTypeIdsProcessDateTimeByCid( $arrintPropertyIds, $arrintUnitTypeIds, $strProcessDateTime, $intCid, $objDatabase, $intRenewal = 0 ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintUnitTypeIds ) ) {
			return NULL;
		}
		$strRenewal = ( 0 === $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';

		$strSql		= 'SELECT
							*
						FROM
							revenue_post_rents
						WHERE
							property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
							AND cid = ' . ( int ) $intCid . '
							AND unit_space_id IS NULL
							AND is_override = 0
							AND is_approve = 0
							' . $strRenewal . '
							AND unit_type_id IN ( ' . implode( ', ', $arrintUnitTypeIds ) . ' )
							AND ( created_on > \'' . $strProcessDateTime . '\'  ) ';

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRentsByPropertyIdsUnitTypeIdsByProcessDateTimeByCid( $arrintPropertyIds, $arrintUnitTypeIds, $strProcessDateTime, $intCid, $objDatabase, $intRenewal = 0 ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintUnitTypeIds ) ) {
			return NULL;
		}
		$strRenewal = ( 0 === ( int ) $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';

		$strSql		= 'SELECT
							*
						FROM
							revenue_post_rents
						WHERE
							property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
							AND cid = ' . ( int ) $intCid . '
							AND is_override = 0
							AND is_approve = 0
							AND rejected_by IS NULL
							' . $strRenewal . '
							AND unit_type_id IN ( ' . implode( ', ', $arrintUnitTypeIds ) . ' )
							AND ( created_on > \'' . $strProcessDateTime . '\'  ) ';

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRentsByCidByPropertyIdsByUnitTypeIdsByProcessDateTime( $arrintPropertyIds, $arrintUnitTypeIds, $strProcessDateTime, $intCid, $objDatabase, $intRenewal = 0 ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintUnitTypeIds ) ) {
			return NULL;
		}
		$strRenewal = ( 0 === $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';

		$strSql		= 'SELECT
							*
						FROM
							revenue_post_rents
						WHERE
							property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
							AND cid = ' . ( int ) $intCid . '
							AND is_approve = 0
							' . $strRenewal . '
							AND unit_type_id IN ( ' . implode( ', ', $arrintUnitTypeIds ) . ' )
							AND ( created_on > \'' . $strProcessDateTime . '\'  ) ';

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRentsByCidByPropertyIdByUnitTypeIdByProcessDateTime( $intCid, $intPropertyId, $intUnitTypeId, $strProcessDateTime, $objDatabase, $intRenewal = 0 ) {
		$strRenewal = ( 0 === $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';

		$strSql	= 'SELECT
							*
						FROM
							revenue_post_rents
						WHERE
							cid = ' . ( int ) $intCid . '
							AND property_id = ' . ( int ) $intPropertyId . '
							AND unit_type_id = ' . ( int ) $intUnitTypeId . '
							AND unit_space_id IS NULL
							AND is_override = 0
							AND is_approve = 0
							' . $strRenewal . '
							AND ( created_on >= \'' . $strProcessDateTime . '\'  ) ';

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	public static function fetchCustomUnprocessedRevenuePostRents( $objDatabase, $intRenewal = 0 ) {
		if( 0 === $intRenewal ) {
			$strRenewal = ' AND rpr.is_renewal = 0';
		} elseif( 1 === $intRenewal ) {
			$strRenewal = ' AND rpr.is_renewal = 1';
		} else {
			$strRenewal = '';
		}

		$strSql = 'SELECT
						rpr.*
						,COALESCE( ce.name_first || \' \' || ce.name_last, cu.username ) AS posted_by
					FROM
						revenue_post_rents rpr
						JOIN property_preferences AS pp ON ( pp.cid = rpr.cid AND pp.property_id = rpr.property_id AND pp.key = \'' . CPricingPostRentLibrary::PROPERTY_PREFERENCE_PRICING_IS_POSTING . '\' AND pp.value = \'1\' )
						JOIN property_preferences AS pp1 ON ( pp1.cid = rpr.cid AND pp1.property_id = rpr.property_id AND pp1.key = \'' . CPropertySettingKey::PRICING_MODEL . '\' AND pp1.value = \'' . CPropertySettingKey::PRICING_MODEL_CONVENTIONAL . '\' )
						JOIN company_users cu ON( cu.cid = rpr.cid AND cu.id = rpr.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
    					LEFT JOIN company_employees ce ON( ce.cid = rpr.cid AND ce.id = cu.company_employee_id )
					WHERE
						rpr.cid IS NOT NULL
						AND rpr.process_datetime IS NULL
						AND rpr.is_override = 0
						AND rpr.is_approve = 0
						AND rpr.rejected_by IS NULL
						AND CASE 
							WHEN rpr.constrained_on IS NOT NULL 
								THEN rpr.accepted_by IS NOT NULL 
							ELSE TRUE
							END 
						' . $strRenewal . '
						AND rpr.updated_on::date = current_date
					ORDER BY
						rpr.created_on DESC';

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	public static function fetchCustomRevenuePostRents( $objDatabase, $intRenewal = 0, $intIsAutoPosting = 0 ) {
		if( 0 === $intRenewal ) {
			$strRenewal = ' AND rpr.is_renewal = 0';
		} elseif( 1 === $intRenewal ) {
			$strRenewal = ' AND rpr.is_renewal = 1';
		} else {
			$strRenewal = '';
		}

		if( 0 == $intIsAutoPosting ) {
			$strProcessDateTime = ' AND CAST( rpr.process_datetime AS DATE ) = \' ' . date( 'm / d / Y' ) . ' \' ';
			$strCreatedBy       = ' AND rpr.created_by != ' . CPricingLibrary::PRICING_USER_AUTO_POSTING;
		} else {
			$strProcessDateTime = ' AND rpr.process_datetime IS NULL';
			$strCreatedBy       = ' AND rpr.created_by = ' . CPricingLibrary::PRICING_USER_AUTO_POSTING;
		}

		$strSql = 'SELECT
						rpr.*
						,COALESCE( ce.name_first || \' \' || ce.name_last, cu.username ) AS posted_by
					FROM
						revenue_post_rents rpr
						JOIN property_preferences AS pp ON ( pp.cid = rpr.cid AND pp.property_id = rpr.property_id AND pp.key = \'' . CPricingPostRentLibrary::PROPERTY_PREFERENCE_PRICING_IS_POSTING . '\' AND pp.value = \'1\' )
						JOIN property_preferences AS pp1 ON ( pp1.cid = rpr.cid AND pp1.property_id = rpr.property_id AND pp1.key = \'' . CPropertySettingKey::PRICING_MODEL . '\' AND pp1.value = \'' . CPropertySettingKey::PRICING_MODEL_CONVENTIONAL . '\' )
						JOIN company_users cu ON( cu.cid = rpr.cid AND cu.id = rpr.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
    					LEFT JOIN company_employees ce ON( ce.cid = rpr.cid AND ce.id = cu.company_employee_id )
					WHERE
						rpr.cid IS NOT NULL
						' . $strProcessDateTime . '
						AND rpr.is_override = 0
						AND rpr.is_approve = 0
						' . $strRenewal . '
						' . $strCreatedBy . '
						AND rpr.updated_on::date = current_date
					ORDER BY
						rpr.created_on DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRentsApproveByPropertyIdProcessDateTime( $intCid, $intPropertyId, $strProcessDateTime, $objDatabase, $intRenewal = 0 ) {
		$strRenewal = ( 0 === $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';

		$strSql		= 'SELECT
							*
						FROM
							revenue_post_rents
						WHERE
				            cid = ' . ( int ) $intCid . '
							AND property_id = ' . ( int ) $intPropertyId . '
							AND unit_space_id IS NULL
							AND is_override = 0
							AND is_approve = 1
							' . $strRenewal . '
							AND ( created_on >= \'' . $strProcessDateTime . '\'  ) ';

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRentsApproveByPropertyIdsProcessDateTime( $intCid, $arrintPropertyIds, $strProcessDateTime, $objDatabase, $intRenewal = 0, $boolNeedsUserName = false, $intMaxExecutionTimeOut = 0, $intIsDashboardApproval = 0 ) {
		$strCondition = '';
		if( true == valArr( $arrintPropertyIds ) ) {
			$strCondition = ' AND rpr.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' ) ';
		}
		if( 0 === $intRenewal ) {
			$strRenewal = ' AND rpr.is_renewal = 0';
		} elseif( 1 === $intRenewal ) {
			$strRenewal = ' AND rpr.is_renewal = 1';
		} else {
			$strRenewal = '';
		}
		$strJoinSql = '';
		$strSelectSql = '';
		if( true == $boolNeedsUserName ) {
			$strJoinSql = 'JOIN properties p ON( p.cid = rpr.cid AND p.id = rpr.property_id )
							JOIN unit_types ut ON( ut.cid = rpr.cid AND ut.property_id = rpr.property_id AND ut.id = rpr.unit_type_id )
							JOIN company_users cu ON( cu.cid = rpr.cid AND cu.id = rpr.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
							LEFT JOIN company_employees ce ON( ce.cid = rpr.cid AND ce.id = cu.company_employee_id AND ce.cid = cu.cid )';
			$strSelectSql = ' ,p.property_name
							,COALESCE( ut.lookup_code, ut.name ) AS lookup_code
							,COALESCE( ce.name_first || \' \' || ce.name_last, cu.username ) AS username';
		}
		$strSpaceCondition = '';
		$strSelectSpace = '';
		if( 0 == $intIsDashboardApproval ) {
			$strSpaceCondition = ' AND rpr.unit_space_id IS NULL ';
		} else {
			$strSelectSpace = ' , us.unit_number_cache ';
			$strSpaceJoin = ' LEFT JOIN unit_spaces us ON( us.id = rpr.unit_space_id AND us.property_id = rpr.property_id AND us.cid = rpr.cid ) ';
		}

		$strSql 	= ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql		.= 'SELECT
							rpr.*
							' . $strSelectSql . $strSelectSpace . '
						FROM
							revenue_post_rents rpr
							' . $strJoinSql . $strSpaceJoin . '
						WHERE
							rpr.cid = ' . ( int ) $intCid . '
							' . $strCondition .
							$strSpaceCondition . '
							AND rpr.is_override = 0
							AND rpr.is_approve = 1
							' . $strRenewal . '
							AND ( rpr.created_on >= \'' . $strProcessDateTime . '\'  ) ';

		if( true == $boolNeedsUserName ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchRevenuePostRents( $strSql, $objDatabase );
		}
	}

	public static function fetchRevenuePostRentsByPropertyIdsForLastPosted( $intCid, $arrintPropertyIds, $strProcessDateTime, $objDatabase, $intRenewal = 0 ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		$strRenewal = ( 0 === $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';

		$strSql		= 'SELECT
							*
						FROM
							revenue_post_rents
						WHERE
				            cid = ' . ( int ) $intCid . '
							AND property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
							AND unit_space_id IS NULL
							AND process_datetime IS NOT NULL
							AND created_on >= \'' . $strProcessDateTime . '\'
							AND is_override = 0
							AND is_approve = 0
							' . $strRenewal . '
						ORDER BY
							property_id,
							id DESC';

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRentsByCreatedOnByPropertyIdsByCid( $intCid, $arrintPropertyIds, $strProcessDateTime, $objDatabase, $intRenewal = 0 ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		if( 0 === $intRenewal ) {
			$strUnitStatusCondition = ' AND us.unit_space_status_type_id IN ( ' . CUnitSpaceStatusType::VACANT_UNRENTED_READY . ', ' . CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY . ', ' . CUnitSpaceStatusType::NOTICE_UNRENTED . ' ) ';
			$strRenewal = ' AND rpr.is_renewal = 0';
		} else {
			$strUnitStatusCondition = ' AND us.unit_space_status_type_id IN ( ' . CUnitSpaceStatusType::OCCUPIED_NO_NOTICE . ' ) ';
			$strRenewal = ' AND rpr.is_renewal = 1';
		}

		$strSql		= 'SELECT
							rpr.*
						FROM
							revenue_post_rents rpr
							JOIN unit_spaces us ON ( us.id = rpr.unit_space_id AND us.unit_type_id = rpr.unit_type_id AND us.cid = rpr.cid AND us.deleted_on IS NULL' . $strUnitStatusCondition . ' )
						WHERE
				            rpr.cid = ' . ( int ) $intCid . '
							AND rpr.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
							AND rpr.process_datetime IS NOT NULL
							AND rpr.created_on >= \'' . $strProcessDateTime . '\'
							AND rpr.is_override = 0
							AND rpr.is_approve = 0
							' . $strRenewal . '
						ORDER BY
							rpr.property_id,
							rpr.id DESC';

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRentsByCidByPropertyIdByUnitTypeIdProcessDateTimeByCreatedOn( $intMnagamentCompanyId, $intPropertyId, $arrintUnitTypeIds, $strCurrentDate, $objDatabase, $intRenewal = 0 ) {
		$strRenewal = ( 0 === $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';
		$strUnitTypeCondition = ( true == valArr( $arrintUnitTypeIds ) ) ? 'AND unit_type_id IN( ' . implode( ', ', $arrintUnitTypeIds ) . ')' : 'AND unit_type_id = ' . ( int ) $arrintUnitTypeIds;

		$strSql = 'SELECT
						*
					FROM
						revenue_post_rents
					WHERE
	    		        cid = ' . ( int ) $intMnagamentCompanyId . '
						AND property_id		  = ' . ( int ) $intPropertyId . '
						' . $strUnitTypeCondition . '
						AND ( process_datetime IS NOT NULL OR is_approve = 1 )
						AND is_override 	  = 0
						' . $strRenewal . '
						AND created_on		  >= \'' . $strCurrentDate . '\'';
		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	// check for post rent message for all price group under specific property

	public static function fetchRevenuePostRentsByPropertyIdProcessDateTimeByCid( $intPropertyId, $strProcessDateTime, $intCid, $objDatabase, $intRenewal = 0 ) {
		$strRenewal = ( 0 === $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';

		$strSql	= 'SELECT
							*
						FROM
							revenue_post_rents
						WHERE
							property_id = ' . ( int ) $intPropertyId . '
						    AND cid = ' . ( int ) $intCid . '
						    AND unit_space_id IS NULL
							AND is_override = 0
							AND is_approve = 0
							' . $strRenewal . '
							AND ( created_on >= \'' . $strProcessDateTime . '\'  ) ';
		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRentsApproveByPropertyIdProcessDateTimeByCid( $intPropertyId, $strProcessDateTime, $intCid, $objDatabase, $intRenewal = 0 ) {
		$strRenewal = ( 0 === $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';

		$strSql		= 'SELECT
							*
						FROM
							revenue_post_rents
						WHERE
							property_id = ' . ( int ) $intPropertyId . '
							AND cid = ' . ( int ) $intCid . '
							AND unit_space_id IS NULL
							AND is_override = 0
							AND is_approve = 1
							' . $strRenewal . '
							AND ( created_on >= \'' . $strProcessDateTime . '\'  ) ';
		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRentsByPropertyIdsByCidOrderByProcessDatetimeByFilter( $arrintPropertyIds, $strPostingTypeFilter, $intIsPsiUser, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strPostingTypeFilterCondition = '';
		if( 0 == $intIsPsiUser ) {
			$strPostingTypeFilterCondition .= 'AND ( ( rpr.created_by = ' . CPricingLibrary::PRICING_USER_SYSTEM . ' AND rpr.unit_space_id IS NULL ) OR rpr.created_by <> ' . CPricingLibrary::PRICING_USER_SYSTEM . ' ) ';
		}

		if( isset ( $strPostingTypeFilter ) ) {

			switch( $strPostingTypeFilter ) {

				case 'normal':
					$strPostingTypeFilterCondition .= 'AND rpr.created_by <> ' . CPricingLibrary::PRICING_USER_SYSTEM;
					break;

				case 'auto':
					$strPostingTypeFilterCondition .= 'AND rpr.created_by = ' . CPricingLibrary::PRICING_USER_SYSTEM . ' AND rpr.unit_space_id IS NULL';
					break;

				case 'clear':
					$strPostingTypeFilterCondition .= 'AND rpr.created_by = ' . CPricingLibrary::PRICING_USER_SYSTEM . ' AND rpr.unit_space_id IS NOT NULL';
					break;

				default:
					$strPostingTypeFilterCondition .= '';
			}

		}

		$strSql     = 'SELECT
						     p.property_name,
						     ut.lookup_code,
							 us.unit_number_cache,
						     rpr.process_datetime,
							 rpr.is_renewal,
							 COALESCE( ce.name_first || \' \' || ce.name_last, cu.username ) AS username
						FROM
						     revenue_post_rents rpr
    						 JOIN properties p ON( p.cid = rpr.cid AND p.id = rpr.property_id )
    						 LEFT JOIN unit_types ut ON( rpr.unit_type_id = ut.id AND rpr.property_id = ut.property_id AND rpr.cid = ut.cid )
							 LEFT JOIN unit_spaces us ON( us.id = rpr.unit_space_id AND us.property_id = rpr.property_id AND us.cid = rpr.cid )
    						 JOIN company_users cu ON( cu.cid = rpr.cid AND cu.id = rpr.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
    						 LEFT JOIN company_employees ce ON( ce.cid = rpr.cid AND ce.id = cu.company_employee_id AND ce.cid = cu.cid )
						WHERE
						     rpr.cid = ' . ( int ) $intCid . '
						     AND rpr.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						     AND rpr.is_override = 0
						     AND rpr.is_approve = 0
							' . $strPostingTypeFilterCondition . '
						ORDER BY
						     rpr.process_datetime DESC,
						     rpr.property_id,
						     ut.lookup_code';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedRevenuePostRentsByPropertyIdsByCidOrderByProcessDatetimeByFilter( $intPageNo, $intPageSize, $arrintPropertyIds, $strPostingTypeFilter,  $intIsPsiUser, $intCid, $objDatabase ) {

		$strPostingTypeFilterCondition = '';
		if( 0 == $intIsPsiUser ) {
			$strPostingTypeFilterCondition .= 'AND ( ( rpr.created_by = ' . CPricingLibrary::PRICING_USER_SYSTEM . ' AND rpr.unit_space_id IS NULL ) OR rpr.created_by <> ' . CPricingLibrary::PRICING_USER_SYSTEM . ' ) ';
		}

		if( isset ( $strPostingTypeFilter ) ) {

			switch( $strPostingTypeFilter ) {

				case 'normal':
					$strPostingTypeFilterCondition .= 'AND rpr.created_by <> ' . CPricingLibrary::PRICING_USER_SYSTEM;
					break;

				case 'auto':
					$strPostingTypeFilterCondition .= 'AND rpr.created_by = ' . CPricingLibrary::PRICING_USER_SYSTEM . ' AND rpr.unit_space_id IS NULL';
					break;

				case 'clear':
					$strPostingTypeFilterCondition .= 'AND rpr.created_by = ' . CPricingLibrary::PRICING_USER_SYSTEM . ' AND rpr.unit_space_id IS NOT NULL';
					break;

				default:
					$strPostingTypeFilterCondition .= '';
			}

		}

		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		$strSql     = ' SELECT
						    *,
						    round ( (
						              SELECT
						                  rl.rate_amount
						              FROM
						                  rate_logs rl
						              WHERE
						                  rl.effective_through_date >= rl.effective_date
						                  AND rl.effective_date = TO_CHAR ( rpr.process_datetime::TIMESTAMP::DATE, \'MM/DD/YYYY\' )::DATE
						                  AND rl.cid = rpr.cid
						                  AND rl.property_id = rpr.property_id
						                  AND rl.unit_type_id = rpr.unit_type_id AND 
						                  CASE 
						                    WHEN rpr.unit_space_id IS NOT NULL
							                THEN rl.unit_space_id = rpr.unit_space_id
							                ELSE TRUE
							               END
						                  AND rl.lease_term_months = 12
						                  AND rl.rate_amount != 0
						                  AND rl.ar_phase_id = ' . CArPhase::ADVERTISED . '
						                  AND rl.is_allowed = TRUE
						                  AND rl.is_effective_datetime_ignored = FALSE
						                  AND rl.ar_origin_id = ' . CArOrigin::BASE . '
						              ORDER BY
						                  rl.window_start_date
						              LIMIT
						                  1
                            ) ) AS rent_posted
                            FROM 
                            (
								SELECT
									 rpr.cid,
									 rpr.id,
									 p.id AS property_id,
								     p.property_name,
									 ut.id AS unit_type_id,
									 COALESCE( ut.lookup_code, ut.name ) AS lookup_code,
									 us.id as unit_space_id,
									 us.unit_number_cache,
								     rpr.process_datetime,
									 rpr.is_renewal,
									 rpr.created_by,
								     COALESCE( ce.name_first || \' \' || ce.name_last, cu.username ) AS username
								FROM
						            revenue_post_rents rpr
    						        JOIN properties p ON( p.cid = rpr.cid AND p.id = rpr.property_id )
    						        LEFT JOIN unit_types ut ON( rpr.unit_type_id = ut.id AND rpr.property_id = ut.property_id AND rpr.cid = ut.cid )
							        LEFT JOIN unit_spaces us ON( us.id = rpr.unit_space_id AND us.property_id = rpr.property_id AND us.cid = rpr.cid )
    						        JOIN company_users cu ON( cu.cid = rpr.cid AND cu.id = rpr.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
    						        LEFT JOIN company_employees ce ON( ce.cid = rpr.cid AND ce.id = cu.company_employee_id )
								WHERE
						            rpr.cid = ' . ( int ) $intCid . '
						            AND rpr.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						            AND rpr.is_override = 0
						            AND rpr.is_approve = 0
						            ' . $strPostingTypeFilterCondition . '
								ORDER BY
								     rpr.process_datetime DESC,
								     rpr.property_id,
								     ut.lookup_code
								OFFSET ' . ( int ) $intOffset . '
								LIMIT ' . ( int ) $intPageSize . '
							) AS rpr';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomRevenuePostRentsUnprocessedByCids( $arrintCids, $objDatabase, $intRenewal = 0 ) {

		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}
		$strRenewal = ( 0 === $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';

		$strSql = 'SELECT
					    	rpr.*
					FROM
						revenue_post_rents rpr
						JOIN property_preferences AS pp ON ( pp.cid = rpr.cid AND pp.property_id = rpr.property_id AND pp.key = \'' . CPropertySettingKey::PRICING_MODEL . '\' AND pp.value = \'' . CPropertySettingKey::PRICING_MODEL_CONVENTIONAL . '\' )
					WHERE
						rpr.cid IN(' . implode( ', ', $arrintCids ) . ')
						AND process_datetime IS NULL
						AND is_override = 0
						AND is_approve = 0
						' . $strRenewal;

		return self::fetchRevenuePostRents( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchRevenuePostRentsByCidByProcessedDate( $strProcessDateTime, $intCid, $objDatabase ) {

		if( true == empty( $intCid ) || true == empty( $strProcessDateTime ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						SUM( CASE WHEN rpr.created_by <> ' . CPricingLibrary::PRICING_USER_SYSTEM . ' THEN 1 ELSE 0 END ) AS normal_posting_count,
						SUM( CASE WHEN rpr.created_by = ' . CPricingLibrary::PRICING_USER_SYSTEM . ' AND rpr.unit_space_id ISNULL  THEN 1 ELSE 0 END ) AS auto_posting_count,
						SUM( CASE WHEN rpr.created_by = ' . CPricingLibrary::PRICING_USER_SYSTEM . ' AND rpr.unit_space_id NOTNULL  THEN 1 ELSE 0 END ) AS clear_posting_count,
						to_char( rpr.process_datetime, \'YYYY/MM/DD\' ) AS processed_date
					FROM
						revenue_post_rents rpr
					WHERE
						rpr.cid = ' . ( int ) $intCid . '
						AND rpr.process_datetime > \'' . $strProcessDateTime . '\'
						AND rpr.is_override = 0
						AND rpr.is_approve = 0
					GROUP BY
						processed_date
					ORDER BY
						processed_date';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRentsByPropertyIdUnitTypeIdByUnitSpaceIdByCid( $intPropertyId, $arrintUnitTypeId, $strProcessDateTime, $intUnitSpaceId, $intCid, $objDatabase, $intRenewal = 0 ) {

		$strCondition = ( false == valArr( $intUnitSpaceId ) ) ? 'AND unit_space_id = ' . ( int ) $intUnitSpaceId : 'AND unit_space_id IN( ' . implode( ', ', $intUnitSpaceId ) . ')';
		$strRenewal = ( 0 === $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';

		$strSql		= 'SELECT
							*
						FROM
							revenue_post_rents
						WHERE
							property_id = ' . ( int ) $intPropertyId . '
							AND cid = ' . ( int ) $intCid . '
							' . $strCondition . '
							AND is_override = 0
							AND is_approve = 0
							' . $strRenewal . '
							AND unit_type_id IN ( ' . implode( ', ', $arrintUnitTypeId ) . ')
							AND process_datetime IS NOT NULL
							AND rejected_by IS NULL
							AND ( created_on > \'' . $strProcessDateTime . '\'  ) ';

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRentsByPropertyIdsUnitTypeIdsByUnitSpaceIdsByCid( $arrintPropertyIds, $arrintUnitTypeIds, $arrintUnitSpaceIds, $strProcessDateTime, $intCid, $objDatabase, $intRenewal = 0 ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintUnitTypeIds ) || false == valArr( $arrintUnitSpaceIds ) ) {
			return NULL;
		}
		$strRenewal = ( 0 === $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';

		$strSql		= 'SELECT
							*
						FROM
							revenue_post_rents
						WHERE
							cid = ' . ( int ) $intCid . '
							AND property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ')
							AND unit_type_id IN ( ' . implode( ', ', $arrintUnitTypeIds ) . ' )
							AND unit_space_id IN ( ' . implode( ', ', $arrintUnitSpaceIds ) . ' )
							AND is_override = 0
							AND is_approve = 0
							' . $strRenewal . '
							AND process_datetime IS NOT NULL
							AND ( created_on > \'' . $strProcessDateTime . '\'  ) ';

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRentsByPropertyIdByUnitTypeIdByCidByProcessDateTime( $intPropertyId, $intUnitTypeId, $intCid, $strInitialDate, $objDatabase, $intRenewal = 0, $intUnitSpaceId = NULL ) {
		$strRenewal = ( 0 === $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';
		$strUnitSpaceCondition = ( false == is_null( $intUnitSpaceId ) ) ? ' AND unit_space_id = ' . ( int ) $intUnitSpaceId : '';
		$strSql = 'SELECT
						*
					FROM
						revenue_post_rents
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND unit_type_id = ' . ( int ) $intUnitTypeId . '
						AND CAST( process_datetime AS DATE ) = \'' . $strInitialDate . '\'
						' . $strUnitSpaceCondition . '
						' . $strRenewal;

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	public static function fetchUnprocessedRevenuePostRentsByCidByPropertyIdByUnitTypeIdByUnitSpaceIds( $intPropertyId, $arrintUnitTypeIds, $arrintUnitSpaceIds, $intCid, $objDatabase, $boolIsReturnKeyedArray = false, $intRenewal = 0, $strCreatedOn = NULL ) {

		$strUnitSpaceCondition = ( true == valArr( $arrintUnitSpaceIds ) ) ? 'AND unit_space_id IN( ' . implode( ', ', $arrintUnitSpaceIds ) . ')' : 'AND unit_space_id IS NULL';
		$strUnitTypeCondition = ( true == valArr( $arrintUnitTypeIds ) ) ? 'AND unit_type_id IN( ' . implode( ', ', $arrintUnitTypeIds ) . ')' : 'AND unit_type_id = ' . ( int ) $arrintUnitTypeIds;
	    $strRenewal = ( 0 === $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';
	    $strCondition = ( NULL != $strCreatedOn ) ? ' AND created_on >= \'' . $strCreatedOn . '\'' : '';

		$strSql = 'SELECT
						*
					FROM
						revenue_post_rents
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						' . $strUnitTypeCondition . '
						' . $strUnitSpaceCondition . '
						AND process_datetime IS NULL
						AND rejected_by IS NULL
						AND is_approve = 0
						AND is_override = 0
						' . $strRenewal . $strCondition;

		return self::fetchRevenuePostRents( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchUnprocessedRevenuePostRentsByCidByPropertyIds( $arrintPropertyIds, $intCid, $objDatabase, $intRenewal = 0, $strCreatedOn = NULL ) {
		$strRenewal = ( 0 === $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';
		$strCondition = ( NULL != $strCreatedOn ) ? ' AND created_on >= \'' . $strCreatedOn . '\'' : '';

		$strSql = 'SELECT
						*
					FROM
						revenue_post_rents
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN( ' . implode( ', ', $arrintPropertyIds ) . ')
						AND process_datetime IS NULL
						AND rejected_by IS NULL
						AND is_approve = 0
						AND is_override = 0
						' . $strRenewal . $strCondition;

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	public static function overrideRevenuePostRentsByCidByPropertyIdByUnitTypeId( $intCid, $intPropertyId, $intUnitTypeId, $intUnitSpaceId = NULL, $objDatabase, $intIsApproved = 0 ) {
		$strWhere = $strCondition = '';
		if( false == empty( $intUnitSpaceId ) ) {
			$strWhere = ' AND unit_space_id = ' . ( int ) $intUnitSpaceId;
		} else {
			$strWhere = ' AND unit_space_id IS NULL';
		}
		if( 0 == $intIsApproved ) {
			$strCondition = ' AND process_datetime >= CURRENT_DATE';
		} else {
			$strCondition = ' AND updated_on >= CURRENT_DATE';
		}

		$strSql = 'UPDATE
						revenue_post_rents
					SET
						is_override = 1,
						process_datetime = NULL
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND unit_type_id = ' . ( int ) $intUnitTypeId . '
						AND process_datetime IS NOT NULL
						' . $strWhere . '
						' . $strCondition . ';';

		return fetchData( $strSql, $objDatabase );
	}

	public static function cancelRevenuePostRentsByUnitTypeIdByPropertyIdByCid( $intUnitTypeId, $intPropertyId, $intCid, $objDatabase, $intUnitSpaceId = NULL ) {
		if( false == empty( $intUnitSpaceId ) ) {
			$strWhere = ' AND unit_space_id = ' . ( int ) $intUnitSpaceId;
		} else {
			$strWhere = ' AND unit_space_id IS NULL';
		}

		$strSql = 'UPDATE
						revenue_post_rents
					SET
						is_override = 1
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND unit_type_id = ' . ( int ) $intUnitTypeId . '
						' . $strWhere;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRentsByCidByPropertyIds( $arrintPropertyIds, $strProcessDateTime, $intCid, $objDatabase, $intRenewal = 0 ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		$strRenewal = ( 0 === $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';

		$strSql		= 'SELECT
							*
						FROM
							revenue_post_rents
						WHERE
							cid = ' . ( int ) $intCid . '
							AND property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
							AND process_datetime IS NOT NULL
							AND is_override = 0
							AND is_approve = 0
							' . $strRenewal . '
							AND ( created_on > \'' . $strProcessDateTime . '\' )
						ORDER BY
							id DESC';

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	/**
	 *
	 * Update entries from revenue_post_rents table if any rent is Overriden or Reset
	 *
	 * @param string   $strCurrentDate
	 * @param int      $intUnitSpaceId
	 * @param int      $intUnitTypeId
	 * @param int      $intPropertyId
	 * @param int      $intCid
	 * @param resource $objDatabase
	 */
	public static function updateOverrideRevenuePostRents( $strCurrentDate, $intUnitSpaceId, $intUnitTypeId, $intPropertyId, $intCid, CDatabase $objDatabase, $intRenewal = 0 ) {

		$strPostRentCondition = '';

		if( false == empty( $intUnitSpaceId ) && -1 != $intUnitSpaceId ) {
			$strPostRentCondition = ' AND ( unit_space_id = ' . ( int ) $intUnitSpaceId . ' )  ';
		} else {
			if( false == empty( $intUnitTypeId ) && 0 < $intUnitTypeId ) {
				$strPostRentCondition = ' AND unit_type_id = ' . ( int ) $intUnitTypeId;
			}
		}

		$strRenewal = ( 0 === ( int ) $intRenewal ) ? ' AND is_renewal = 0' : ' AND is_renewal = 1';

		$strUpdateRevenuePostRent = 'UPDATE
											revenue_post_rents
										SET
											is_override = 1
										WHERE
											cid = ' . ( int ) $intCid . '
											AND property_id			= ' . ( int ) $intPropertyId . $strPostRentCondition . '
											AND ( process_datetime IS NOT NULL OR is_approve = 1 )
											AND is_override 	= 0
											' . $strRenewal . '
											AND created_on 		>= \'' . $strCurrentDate . '\'';

		fetchData( $strUpdateRevenuePostRent, $objDatabase );
	}

	public static function fetchUnprocessedRevenuePostRentsByUnitSpaceIdsByUnitTypeIdsByPropertyIdsByCid( $arrintUnitSpaceIds, $arrintUnitTypeIds, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintUnitTypeIds ) || false == valArr( $arrintUnitSpaceIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						revenue_post_rents
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id	IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND unit_type_id IN ( ' . implode( ', ', $arrintUnitTypeIds ) . ' )
						AND unit_space_id IN ( ' . implode( ', ', $arrintUnitSpaceIds ) . ' )
						AND process_datetime IS NULL
						AND is_approve = 0
						AND is_override = 0
						AND rejected_by IS NULL
						AND updated_on::date = current_date ';

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	/**
	 * @param $intUnitSpaceFlag
	 * @return array|bool
	 * Function for getting latest posted date for unit type and unit space from revenue_post_rents
	 */

	public static function insertRevenuePostRents( CDatabase $objDatabase, int $intCid, array $arrintPropertyIds, int $intCompanyUserId, bool $boolIsDisplaySql = false ) {

		$strInsertRevenuePostRentSql = 'SELECT * FROM public.insert_revenue_post_rents(
						' . ( int ) $intCid . ',
						ARRAY[ ' . implode( ',', $arrintPropertyIds ) . ' ]::INT[],
						' . ( int ) $intCompanyUserId . ',
						ARRAY[]::INT[],
						ARRAY[]::INT[],
						' . ( true == $boolIsDisplaySql ? 'true' : 'false' ) . '
			);';

		$objClientDataset = $objDatabase->createDataset();

		if( false == $objClientDataset->execute( $strInsertRevenuePostRentSql ) ) {
			return false;
		}
		return true;
	}

	public static function fetchRevenuePostRentsStatusByCidByPropertyIdsByUnitTypeIdsByUnitSpaceIds( $intCid, $objDatabase, $arrintPropertyIds = [], $arrintUnitTypeIds = [], $arrintUnitSpaceIds = [], $intIsRenewal = 0 ) {
		$strWhere = '';
		if( true == valArr( $arrintPropertyIds ) ) {
			$strWhere .= 'property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' ) AND ';
		}

		if( true == valArr( $arrintUnitTypeIds ) ) {
			$strWhere .= 'unit_type_id IN( ' . implode( ',', $arrintUnitTypeIds ) . ' ) AND ';
		}

		if( true == valArr( $arrintUnitSpaceIds ) ) {
			$strWhere .= 'unit_space_id IN( ' . implode( ',', $arrintUnitSpaceIds ) . ' ) AND ';
		}

		$strRenewal = ( 0 === $intIsRenewal ) ? '  is_renewal = 0 AND' : '  is_renewal = 1 AND';
		$strSql = 'SELECT cid,
						property_id,
						unit_type_id,
						unit_space_id,
						CASE
						 WHEN process_datetime IS NULL AND rejected_by IS NOT NULL AND rejected_on IS NOT NULL AND details ->> \'rejection_reason\' IS NOT NULL THEN details ->> \'rejection_reason\'
						 WHEN process_datetime IS NULL AND rejected_by IS NOT NULL AND rejected_on IS NOT NULL AND details ->> \'rejection_reason\' IS NULL THEN \'null\'
						END AS rejection_reason,
						CASE
						 WHEN process_datetime IS NULL AND constrained_on IS NOT NULL AND accepted_by IS NULL AND rejected_by IS NULL THEN \'Awaiting approval\'
						 WHEN process_datetime IS NOT NULL AND constrained_on IS NOT NULL AND accepted_by = 65 AND rejected_by IS NULL THEN \'Published with criteria not met\'
						 ELSE NULL
						END AS hint
					FROM revenue_post_rents
					WHERE 
						cid = ' . ( int ) $intCid . ' AND ' . $strWhere . '
						is_override = 0 AND
						' . $strRenewal . '
						updated_on >= CURRENT_DATE';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRentsForApprovalByPropertyIdsProcessDateTime( $intCid, $strProcessDateTime, $objDatabase, $intRenewal = 0, $boolNeedsUserName = false, $intMaxExecutionTimeOut = 0, $intIsDashboardApproval = 0, $arrintPropertyIds ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strCondition = '';
		if( 0 === $intRenewal ) {
			$strRenewal = ' AND rpr.is_renewal = 0';
		} elseif( 1 === $intRenewal ) {
			$strRenewal = ' AND rpr.is_renewal = 1';
		} else {
			$strRenewal = '';
		}
		$strJoinSql   = '';
		$strSelectSql = '';
		if( true == $boolNeedsUserName ) {
			$strJoinSql   = 'JOIN properties p ON( p.cid = rpr.cid AND p.id = rpr.property_id )
							JOIN unit_types ut ON( ut.cid = rpr.cid AND ut.property_id = rpr.property_id AND ut.id = rpr.unit_type_id )
							JOIN company_users cu ON( cu.cid = rpr.cid AND cu.id = rpr.requested_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
							LEFT JOIN company_employees ce ON( ce.cid = rpr.cid AND ce.id = cu.company_employee_id AND ce.cid = cu.cid )';
			$strSelectSql = ' ,p.property_name
							,COALESCE( ut.lookup_code, ut.name ) AS lookup_code
							,COALESCE( ce.name_first || \' \' || ce.name_last, cu.username ) AS username';
		}
		$strSelectSpace = ' , us.unit_number_cache ';
		$strSpaceJoin   = ' JOIN unit_spaces us ON( us.id = rpr.unit_space_id AND us.property_id = rpr.property_id AND us.cid = rpr.cid ) ';

		$strSql = ( 0 != $intMaxExecutionTimeOut ) ? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql .= 'SELECT
							rpr.*
							' . $strSelectSql . $strSelectSpace . '
						FROM
							revenue_post_rents rpr
							' . $strJoinSql . $strSpaceJoin . '
						WHERE
							rpr.cid = ' . ( int ) $intCid . '
							AND rpr.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
							AND rpr.is_override = 0
							AND rpr.requested_by IS NOT NULL 
							AND rpr.requested_on IS NOT NULL
							AND rpr.constrained_on IS NOT NULL
							AND rpr.accepted_by IS NULL
							AND rpr.accepted_on IS NULL
							AND rpr.rejected_by IS NULL
							AND rpr.rejected_on IS NULL
							AND rpr.process_datetime IS NULL
							' . $strRenewal . '
							AND ( rpr.created_on >= \'' . $strProcessDateTime . '\'  ) ';

		if( true == $boolNeedsUserName ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchRevenuePostRents( $strSql, $objDatabase );
		}
	}

	public static function getBatchIdNextValue( CDatabase $objDatabase ) {

		$strSql = "SELECT NEXTVAL('revenue_post_rents_batch_id_seq')";

		return fetchData( $strSql, $objDatabase )[0]['nextval'];
	}

	public static function fetchStudentPostRentsByCidByPropertyIdByUnitTypeIds( $intCid, $intPropertyId, $arrintUnitTypeIds, $objDatabase ) {
		if( false == valArr( $arrintUnitTypeIds ) ) {
			return NULL;
		}

		$strSql = sprintf( "SELECT
						            id,
						            cid,
						            property_id,
						            unit_type_id,
						            details ->> 'lease_term_id' AS lease_term_id, 
						            details ->> 'lease_start_window_id' AS lease_start_window_id,
						            details ->> 'unit_space_configuration_id' AS unit_space_configuration_id,
						            is_renewal,
						            is_override,
						            std_override_rent,
						            std_optimal_rent,
						            process_datetime,
						            created_on,
						            updated_on
						          FROM
						            revenue_post_rents
						          WHERE
						            cid = %d
						            AND property_id = %d
						            AND unit_type_id IN( %s )
						            AND is_override = 0
						            AND unit_space_id IS NULL
						            AND updated_on >= CURRENT_DATE
						            AND ( details->>'lease_term_id' ) IS NOT NULL 
						            AND ( details->>'lease_start_window_id' ) IS NOT NULL", $intCid, $intPropertyId, implode( ', ', $arrintUnitTypeIds ) );

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

	public static function fetchCustomUnprocessedStudentRevenuePostRents( $objDatabase ) {

		$strSql = sprintf( 'SELECT
						rpr.*,
						rpr.details->>\'lease_term_id\' AS lease_term_id,
						rpr.details->>\'lease_start_window_id\' AS lease_start_window_id,
                        rpr.details->>\'unit_space_configuration_id\' AS unit_space_configuration_id
					FROM
						revenue_post_rents rpr
						JOIN property_preferences AS pp ON ( pp.cid = rpr.cid AND pp.property_id = rpr.property_id AND pp.key = \'' . CPropertySettingKey::PRICING_MODEL . '\' AND pp.value = \'' . CPropertySettingKey::PRICING_MODEL_STUDENT . '\' )
						JOIN company_users cu ON( cu.cid = rpr.cid AND cu.id = rpr.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
    					LEFT JOIN company_employees ce ON( ce.cid = rpr.cid AND ce.id = cu.company_employee_id )
					WHERE
						rpr.cid IS NOT NULL
						AND rpr.unit_space_id IS NULL
						AND rpr.process_datetime IS NULL
						AND rpr.is_override = 0
						AND rpr.updated_on::DATE = CURRENT_DATE
					ORDER BY
						rpr.created_on DESC' );

		return self::fetchRevenuePostRents( $strSql, $objDatabase );
	}

}
?>