<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDashboardItems
 * Do not add any new functions to this class.
 */

class CDashboardItems extends CBaseDashboardItems {

	public static function fetchDashboardItem( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDashboardItem', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}
}
?>