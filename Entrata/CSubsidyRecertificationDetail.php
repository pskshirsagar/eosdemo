<?php

class CSubsidyRecertificationDetail extends CBaseSubsidyRecertificationDetail {

	protected $m_strBuildingName;

	const ANNUAL_RECERTIFICATION_TIMING_HOUSEHOLD_INDIVIDUAL_DATE 	= 1;
	const ANNUAL_RECERTIFICATION_TIMING_BUILDING_SPECIFIC_DATE 		= 2;
	const ANNUAL_RECERTIFICATION_TIMING_HOUSEHOLD_SPECIFIC_DATE 	= 3;

	/**
	 * Get Functions
	 *
	 */

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = false, $boolDirectSet = false ) {

		if( true == isset( $arrmixValues['building_name'] ) ) $this->setBuildingName( $arrmixValues['building_name'] );
		return parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

	public function setBuildingName( $strBuildingName ) {
		$this->m_strBuildingName = $strBuildingName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyBuildingId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonth() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'month', 'Month and Day is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDay() {
		$boolIsValid = true;
		if( false == is_numeric( $this->getDay() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'month', 'Month and Day is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'subsidy_recertification_details':
				$boolIsValid &= $this->valMonth();
				$boolIsValid &= $this->valDay();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>