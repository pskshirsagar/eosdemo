<?php

class CInspectionResponse extends CBaseInspectionResponse {

	protected $m_intLocationId;
	protected $m_intInspectionFormId;
	protected $m_intParentMaintenanceProblemId;
	protected $m_intParentMaintenanceLocationId;
	protected $m_strInspectionProblemUnitLocation;
	protected $m_strName;
	protected $m_intMaintenanceActionId;
	protected $m_intMaintenanceActionType;
	protected $m_strMaintenanceActionName;
    /**
     * Get Functions
     *
     */

    public function getLocationId() {
    	return $this->m_intLocationId;
    }

	public function getInspectionProblemUnitLocation() {
		return $this->m_strInspectionProblemUnitLocation;
	}

	public function getName() {
		return $this->m_strName;
	}

	public function getInspectionFormId() {
		return $this->m_intInspectionFormId;
	}

	public function getParentMaintenanceProblemId() {
		return $this->m_intParentMaintenanceProblemId;
	}

	public function getParentMaintenanceLocationId() {
		return $this->m_intParentMaintenanceLocationId;
	}

	public function getMaintenanceActionId() {
		return $this->m_intMaintenanceActionId;
	}

	public function getMaintenanceActionType() {
		return $this->m_intMaintenanceActionType;
	}

	public function getMaintenanceActionName() {
		return $this->m_strMaintenanceActionName;
	}
    /**
     * Set Functions
     *
     */

    public function setLocationId( $intLocationId ) {
    	return $this->m_intLocationId = $intLocationId;
    }

	public function setInspectionProblemUnitLocation( $strInspectionProblemUnitLocation ) {
		return $this->m_strInspectionProblemUnitLocation = $strInspectionProblemUnitLocation;
	}

	public function setName( $strName ) {
		return $this->m_strName = $strName;
	}

	public function setInspectionFormId( $intInspectionFormId ) {
		return $this->m_intInspectionFormId = $intInspectionFormId;
	}

	public function setParentMaintenanceProblemId( $intParentMaintenanceProblemId ) {
		return $this->m_intParentMaintenanceProblemId = $intParentMaintenanceProblemId;
	}

	public function setParentMaintenanceLocationId( $intParentMaintenanceLocationId ) {
		return $this->m_intParentMaintenanceLocationId = $intParentMaintenanceLocationId;
	}

	public function setMaintenanceActionId( $intMaintenanceActionId ) {
		return $this->m_intMaintenanceActionId = $intMaintenanceActionId;
	}

	public function setMaintenanceActionType( $intMaintenanceActionType ) {
		return $this->m_intMaintenanceActionType = $intMaintenanceActionType;
	}

	public function setMaintenanceActionName( $strMaintenanceActionName ) {
		return $this->m_strMaintenanceActionName = $strMaintenanceActionName;
	}

    public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

    	if( true == isset( $arrmixValues['location_id'] ) ) 						$this->setLocationId( $arrmixValues['location_id'] );
	    if( true == isset( $arrmixValues['inspection_problem_unit_location'] ) ) 	$this->setInspectionProblemUnitLocation( $arrmixValues['inspection_problem_unit_location'] );
	    if( true == isset( $arrmixValues['name'] ) ) {
		    $this->setName( $arrmixValues['name'] );
	    }
	    if( true == isset( $arrmixValues['inspection_form_id'] ) ) {
		    $this->setInspectionFormId( $arrmixValues['inspection_form_id'] );
	    }
	    if( true == isset( $arrmixValues['parent_maintenance_problem_id'] ) ) {
		    $this->setParentMaintenanceProblemId( $arrmixValues['parent_maintenance_problem_id'] );
	    }
	    if( true == isset( $arrmixValues['parent_maintenance_location_id'] ) ) {
		    $this->setParentMaintenanceLocationId( $arrmixValues['parent_maintenance_location_id'] );
	    }
		if( true == isset( $arrmixValues['maintenance_action_id'] ) ) {
			$this->setMaintenanceActionId( $arrmixValues['maintenance_action_id'] );
		}
	    if( true == isset( $arrmixValues['maintenance_action_type'] ) ) {
		    $this->setMaintenanceActionType( $arrmixValues['maintenance_action_type'] );
	    }
		if( true == isset( $arrmixValues['maintenance_action_name'] ) ) {
			$this->setMaintenanceActionName( $arrmixValues['maintenance_action_name'] );
		}
    }

    /**
     * Validation Functions
     *
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInspectionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInspectionFormLocationProblemId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaintenanceProblemId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApHeaderId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArTransactionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
           	case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            default:
            	// default case
            	break;
        }

        return $boolIsValid;
    }

}
?>