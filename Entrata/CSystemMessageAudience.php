<?php

class CSystemMessageAudience extends CBaseSystemMessageAudience {

	const RESIDENT           = 1;
	const LEAD               = 2;
	const PROPERTY           = 3;
	const CONTACT_SUBMISSION = 4;
	const VENDOR             = 5;
	const ENTRATA_EMPLOYEE   = 6;
	const INSURANCE_AGENT    = 7;
	const GENERAL            = 8;

	public static $c_arrstrSystemMessageAudiences = [
		self::RESIDENT 	=> 'Resident',
		self::LEAD 		=> 'Lead',
		self::PROPERTY 	=> 'Property',
		self::CONTACT_SUBMISSION => 'Contact Submission'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>