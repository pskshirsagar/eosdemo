<?php

class CCompanyIdentificationType extends CBaseCompanyIdentificationType {

	const CODE_DRIVERS_LICENSE                         = 'DR LIC';
	const CODE_GOVERMENT_ISSUED_PICTURE_ID             = 'GOV ID';
	const CODE_MILITARY_ID                             = 'MIL ID';
	const CODE_VISA                                    = 'VISA';
	const CODE_STATE_ISSUED_PICTURE_ID                 = 'STAT ID';
	const CODE_PASSPORT_NUMBER                         = 'PASS ID';
	const CODE_STUDENT_NUMBER                          = 'STUD ID';
	const SEARCH_IDENTIFICATION_TYPES                  = 'identification_types';
	const CODE_NATIONAL_INTERNATIONAL_RESIDENCE_PERMIT = 'RES PRMT';
	const CODE_INTERNATIONAL_CARD                      = 'INT CD';

	public static $c_arrintPoliceExportIdentificationTypes = [ self::CODE_DRIVERS_LICENSE, self::CODE_PASSPORT_NUMBER, self::CODE_NATIONAL_INTERNATIONAL_RESIDENCE_PERMIT, self::CODE_INTERNATIONAL_CARD ];

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSystemCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName( $objDatabase = NULL ) {

        $boolIsValid = true;

      /**
       * Validation example
       */

       if( false == isset( $this->m_strName ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Identification Name is required.' ) ) );

       } elseif( 3 > \Psi\CStringService::singleton()->strlen( $this->m_strName ) ) {
            $boolIsValid = false;
	       $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Identification Name must be at least {%d, 0} characters in length.', [ 3 ] ) ) );
       }

    	if( true == isset( $objDatabase ) ) {
			$objCompanyIdentificationType = CCompanyIdentificationTypes::fetchDuplicateCompanyIdentificationTypeByNameByCid( $this->getName(), $this->getId(), $this->getCid(), $objDatabase );
			if( true == is_numeric( $this->getId() ) ) {
				$objOriginalCompanyIdentificationType = CCompanyIdentificationTypes::fetchCompanyIdentificationTypeByIdByCid( $this->getId(), $this->getCid(), $objDatabase );
			}
    	}

		if( ( false == isset ( $objOriginalCompanyIdentificationType ) || $this->getName() != $objOriginalCompanyIdentificationType->getName() ) && true == valObj( $objCompanyIdentificationType, 'CCompanyIdentificationType' ) ) {
			$boolIsValid = false;
       		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Identification Name already in use.' ) ) );
		}

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

 	public function valDependantInformation( $objDatabase ) {

		$boolIsValid = true;

		$arrobjPropertyIdentificationTypes = CPropertyIdentificationTypes::fetchPropertyIdentificationTypesByCompanyIdentificationTypeIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjPropertyIdentificationTypes ) ) {
			$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_identification_type', __( 'Failed to delete the identification type, it is being associated with property.' ) ) );
		}

		return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valName( $objDatabase );
	           	$boolIsValid &= $this->valCid();
	           	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valDependantInformation( $objDatabase );
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>