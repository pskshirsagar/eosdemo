<?php

class CApplicationLeadSource extends CBaseApplicationLeadSource {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getCid() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valApplicationId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getApplicationId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_id', 'Application id is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valApplicantId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getApplicantId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applicant_id', 'Applicant id is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valLeadSourceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valApplicantId();
            	$boolIsValid &= $this->valApplicationId();
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valApplicationId();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = false;
            	break;
        }

        return $boolIsValid;
    }
}
?>