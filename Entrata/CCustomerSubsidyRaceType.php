<?php

class CCustomerSubsidyRaceType extends CBaseCustomerSubsidyRaceType {

	protected $m_boolIsDelete;

	public function getIsDelete() {
		return $this->m_boolIsDelete;
	}

	public function setIsDelete( $boolIsDelete ) {
		return $this->m_boolIsDelete = CStrings::strToBool( $boolIsDelete );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyRaceTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getSubsidyRaceTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_race_type_id', 'Subsidy race type is required.' ) ] );
		}

		return $boolIsValid;
	}

	public function valSubsidyRaceSubTypeId( $boolIsValidateRequired ) {
		$boolIsValid = true;

		if( ( CSubsidyRaceType::ASIAN == $this->getSubsidyRaceTypeId() || CSubsidyRaceType::NATIVE_HAWAIIAN_OR_OTHER_PACIFIC_ISLANDER == $this->getSubsidyRaceTypeId() ) && false == valId( $this->getSubsidyRaceSubTypeId() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_race_sub_type_id', 'Please select race type.', '', [ 'label' => 'Demographic Information', 'step_id' => CApplicationStep::BASIC_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsValidateRequired = true ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid = $this->valSubsidyRaceTypeId();
				break;

			case 'validate_subsidy_race_types':
				$boolIsValid = $this->valSubsidyRaceTypeId( $boolIsValidateRequired );
				$boolIsValid = $this->valSubsidyRaceSubTypeId( $boolIsValidateRequired );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>