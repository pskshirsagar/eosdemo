<?php

use Psi\Libraries\Cryptography\CCrypto;
use Psi\Eos\Admin\CAccounts;

class CMerchantAccount extends CBaseMerchantAccount {

	use TMerchantAccount;

	const MONEYGRAM_CONVENIENCE_FEES 	= 3.99;
	const CHECK21_CONVERSION_AMOUNT_LIMIT = 25000;
	const MA_BROADMOOR_VILLAGE = 38777;

	protected $m_objArPayment;
	protected $m_objIntermediaryAccount;
	protected $m_objReturnsAccount;

	protected $m_objClient;

	protected $m_arrobjArTransactions;

	protected $m_fltAmountDue;

	protected $m_strUsePreexisting;
	protected $m_strPropertyName;
	protected $m_intPropertyUnits;
	protected $m_intPropertyId;

	// This variable will get used while creating account
	protected $m_intEntityId;

	// This variable will be return when we are fetching all charge code merchant account types for a property
	protected $m_intArCodeId;
	protected $m_intBankAccountId;

	// This variable is used to store the order of the default customer payment objects.  We have
	// to load the value on to the merchant account and then assign it over to the customer payment
	// while created in $this->createArPayment.
	protected $m_intOrderNum;

	// This variable is used while mapping / creating customer payments during dynamic payment splitting process.
	protected $m_intCaptureDelayDays;

	// For use in Ach Customer Payments Script
	protected $m_arrobjPositiveArPayments;
	protected $m_arrobjNegativeArPayments;

	// For use in Ach Settlement Distributions Script
	protected $m_arrobjSettlingArPayments;
	protected $m_arrobjReturningArPayments;
	protected $m_arrobjReversingArPayments;
	protected $m_arrobjReturningReversedArPayments;

	// For use in Conveniece Fee Clearing Script
	protected $m_arrobjClearingTransactions;

	protected $m_objClientDatabase;
	protected $m_intPaymentMediumId;

	public function setClientDatabase( $objClientDatabase ) {
		$this->m_objClientDatabase = $objClientDatabase;
	}

	public function getClientDatabase() {
		return $this->m_objClientDatabase;
	}

	public function valClientDatabase() {
		return valObj( $this->m_objClientDatabase, CDatabase::class );
	}

	public function setPaymentMediumId( $intPaymentMediumId ) {
		$this->m_intPaymentMediumId = $intPaymentMediumId;

		if( false == valArr( $this->m_arrobjMerchantMethods ) ) {
			return;
		}

		foreach( $this->m_arrobjMerchantMethods as $intPaymentTypeId => $objMerchantMethod ) {
			// if it's null the method was created before setPaymentMediumId was called
			if( true == is_null( $objMerchantMethod->getPaymentMediumId() ) ) {
				$objMerchantMethod->setPaymentMediumId( $intPaymentMediumId );
			}

			// if it doesn't match the merchant account has been cloned and this is the wrong one
			if( $intPaymentMediumId != $objMerchantMethod->getPaymentMediumId() ) {
				unset( $this->m_arrobjMerchantMethods[$intPaymentTypeId] );
			}

		}
	}

	public function getPaymentMediumId() {
		if( true == is_null( $this->m_intPaymentMediumId ) ) {
			$this->setPaymentMediumId( CPaymentMedium::WEB );
		}

		return $this->m_intPaymentMediumId;
	}

	public function valPaymentMediumId() {
		return in_array( $this->getPaymentMediumId(), [ CPaymentMedium::WEB, CPaymentMedium::RECURRING, CPaymentMedium::TERMINAL ] );
	}

	// region Merchant Method Functions

	/**
	 * Magic function to replace all of the direct get/set that were
	 * part of the base class when we used the old view_company_merchant_accounts
	 * to load merchant accounts and methods in one giant, bloated object.
	 * We've tried to update everything to use $this->getOrFetchMethod but
	 * this is here just in case we missed any calling code that still uses
	 * the old getters and setters (e.g. getViMaxPaymentAmount)
	 *
	 * @param string $strMethod
	 * @param mixed[] $arrmixArguments
	 * @return mixed
	 */
	public function __call( $strMethod, $arrmixArguments ) {
		$arrmixMatches = [];
		if( false == preg_match( CPaymentType::GETTER_SETTER_REGEX, $strMethod, $arrmixMatches ) ) {
			return parent::__call( $strMethod, $arrmixArguments );
		}

		$intPaymentTypeId = CPaymentType::stringToId( $arrmixMatches[2] );
		$strMethodName = $arrmixMatches[1] . $arrmixMatches[3];

		if( 0 === strpos( $strMethod, 'get' ) || 0 === strpos( $strMethod, 'set' ) ) {
			$objMethod = $this->getMethodByAmount( $intPaymentTypeId );
			return $objMethod ? $objMethod->{$strMethodName}( ...$arrmixArguments ) : NULL;
		}

		return NULL;
	}

	/**
	 * @param $intPaymentTypeId
	 * @param double $fltPaymentAmount
	 * @return bool|CMerchantMethod
	 * @deprecated Use one of the methods in {@see TMerchantAccount} instead, such as
	 * {@see methodIsEnabled()}, {@see methodExists()},
	 * {@see getMethodByAmount()}, {@see getTopTierMethod()}
	 */
	public function getOrFetchMethod( $intPaymentTypeId, $fltPaymentAmount = 0 ) {
		if( false == in_array( $intPaymentTypeId, CPaymentType::$c_arrintMerchantAccountPaymentTypeIds ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Invalid payment type Id for merchant account.' ) );
			return NULL;
		}

		if( false == $this->valPaymentMediumId() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Invalid payment medium Id.' ) );
			return NULL;
		}

		// Load methods for this payment type if they aren't cached in memory
		if( !isset( $this->getMerchantMethods()[$this->getPaymentMediumId()] ) ) {
			$this->loadMethods();
		}

		// Find the method matching the payment type with the smallest max_payment_amount greater than $fltPaymentAmount
		/** @var CMerchantMethod[] $arrobjMerchantMethod */
		foreach( $this->getMerchantMethods()[$this->getPaymentMediumId()] ?? [] as $intTier => $arrobjMerchantMethods ) {
			$objMethod = $arrobjMerchantMethods[$intPaymentTypeId] ?? NULL;
			if( $objMethod && $objMethod->getMaxPaymentAmount() ?: PHP_INT_MAX >= $fltPaymentAmount ) {
				return $objMethod;
			}
		}

		// If we reach this point, there are no tiers with a max_payment_amount greater than the payment amount matching
		// the given medium and type
		return NULL;
	}

	public function loadMethods() {
		if( !$this->valClientDatabase() && valObj( $this->getDatabase(), CDatabase::class ) ) {
			$this->setClientDatabase( $this->getDatabase() );
		} elseif( !$this->valClientDatabase() && !valObj( $this->getDatabase(), CDatabase::class ) ) {
			$this->setClientDatabase( CDatabases::createDatabaseByCid( $this->getCid(), CDatabaseUserType::PS_PROPERTYMANAGER ) );
		}

		$this->setMerchantMethods( \Psi\Eos\Entrata\CMerchantMethods::createService()->fetchMerchantMethodTiers( $this->getId(), $this->getCid(), $this->getClientDatabase() ) );
	}

	public function createMethod() {
		return new CMerchantMethod();
	}

	public function addMethodErrors() {
		if( false == valArr( $this->m_arrobjMerchantMethods ) ) {
			return;
		}

		foreach( $this->m_arrobjMerchantMethods as $objMerchantMethod ) {
			$this->addErrorMsgs( $objMerchantMethod->getErrorMsgs() );
		}
	}

	// endregion

	public function setId( $intId ) {
		parent::setId( $intId );

		if( false == valArr( $this->m_arrobjMerchantMethods ) ) {
			return;
		}

		/** @var CMerchantMethod $objMerchantMethod */
		foreach( $this->m_arrobjMerchantMethods as $objMerchantMethod ) {
			$objMerchantMethod->setMerchantAccountId( $intId );
		}
	}

	// region Add Functions

	public function addPositiveArPayment( $objPositiveArPayment ) {
		$this->m_arrobjPositiveArPayments[$objPositiveArPayment->getId()] = $objPositiveArPayment;
	}

	public function addNegativeArPayment( $objNegativeArPayment ) {
		$this->m_arrobjNegativeArPayments[$objNegativeArPayment->getId()] = $objNegativeArPayment;
	}

	public function addSettlingArPayment( $objSettlingArPayment ) {
		$this->m_arrobjSettlingArPayments[$objSettlingArPayment->getId()] = $objSettlingArPayment;
	}

	public function addReturningArPayment( $objReturningArPayment ) {
		$this->m_arrobjReturningArPayments[$objReturningArPayment->getId()] = $objReturningArPayment;
	}

	public function addReversingArPayment( $objReversingArPayment ) {
		$this->m_arrobjReversingArPayments[$objReversingArPayment->getId()] = $objReversingArPayment;
	}

	public function addReturningReversedArPayment( $objReturningReversedArPayment ) {
		$this->m_arrobjReturningReversedArPayments[$objReturningReversedArPayment->getId()] = $objReturningReversedArPayment;
	}

	public function addClearingTransaction( $objClearingTransaction ) {
		$this->m_arrobjClearingTransactions[$objClearingTransaction->getId()] = $objClearingTransaction;
	}

	public function addArTransaction( $objArTransaction ) {
		$this->m_arrobjArTransactions[$objArTransaction->getId()] = $objArTransaction;
	}

	// endregion

	// region Get or Fetch Functions

	public function getOrFetchReturnsAccount( $objAdminDatabase ) {

		if( true == valObj( $this->m_objReturnsAccount, CAccount::class ) ) {
			return $this->m_objReturnsAccount;
		} else {
			return $this->fetchReturnsAccount( $objAdminDatabase );
		}
	}

	public function getOrFetchIntermediaryAccount( $objAdminDatabase ) {

		if( true == valObj( $this->m_objIntermediaryAccount, CAccount::class ) ) {
			return $this->m_objIntermediaryAccount;
		} else {
			return $this->fetchIntermediaryAccount( $objAdminDatabase );
		}
	}

	// endregion

	// region Create Functions

	public function createArPayment( $intPaymentTypeId = NULL ) {

		$objArPayment = new CArPayment();
		$objArPayment->setDefaults();
		$objArPayment->setCid( $this->getCid() );
		$objArPayment->setCompanyMerchantAccount( $this );
		$objArPayment->setMerchantAccount( $this );
		$objArPayment->setPaymentStatusTypeId( CPaymentStatusType::PENDING );
		$objArPayment->setArCodeId( $this->getArCodeId() );
		$objArPayment->setCompanyMerchantAccountId( $this->getId() );
		$objArPayment->setCurrencyCode( $this->getCurrencyCode() );

		if( true == $this->getIsDisabled() ) {
			$objArPayment->addErrorMsg( new CErrorMsg( NULL, '', 'The merchant account through which this payment would normally process is not active.  Please try back later.' ) );
			return $objArPayment;
		}

		$intMerchantGatewayId = NULL;

		if( false == is_null( $intPaymentTypeId ) ) {
			$intMerchantGatewayId = ( $this->methodIsEnabled( $intPaymentTypeId ) ) ? $this->getMethodByAmount( $intPaymentTypeId )->getMerchantGatewayId() : NULL;
		}

		if( false == is_null( $intMerchantGatewayId ) ) {
			$objArPayment->setMerchantGatewayId( $intMerchantGatewayId );
		}

		if( true == is_numeric( $this->m_intOrderNum ) ) {
			$objArPayment->setOrderNum( $this->m_intOrderNum );
		}

		if( true == is_numeric( $this->m_fltAmountDue ) ) {
			$objArPayment->setPaymentAmount( $this->m_fltAmountDue );
		}

		if( true == isset( $this->m_arrobjArTransactions ) ) {
			$objArPayment->setArTransactions( $this->m_arrobjArTransactions );
		}

		// Add logic to calculate capture_delay_days date.
		if( true == is_numeric( $this->getCaptureDelayDays() ) && 0 < $this->getCaptureDelayDays() ) {
			$objArPayment->setAutoCaptureOn( date( 'm/d/Y H:i:s', strtotime( '+' . ( string ) $this->getCaptureDelayDays() . ' days' ) ) );
		}

		return $objArPayment;
	}

	/**
	 * @param CArPayment $objArPayment
	 * @param bool $boolIsOverrideGateway
	 * @return bool|CFifthThirdGateway|CFirstRegionalGateway|CLitleGateway|CVantivGateway|CVitalGateway|CZionsGateway|CWorldpayWPGCcGateway
	 */
	public function createGateway( $objArPayment, $boolIsOverrideGateway = true ) {

		$objGateway = false;

		if( true == $boolIsOverrideGateway ) {
			$this->populateMerchantGatewayId( $objArPayment );
		}

		$strMerchantName = '';
		if( false == is_null( $this->getMerchantName() ) ) {
			$strMerchantName = $this->getMerchantName();
		} else {
			$strMerchantName = $this->getAccountName();
		}

		switch( $objArPayment->getMerchantGatewayId() ) {

			case CMerchantGateway::VANTIV_CREDIT_CARD:
				$objGateway = new CVantivGateway();
				$objGateway->setMerchantName( $strMerchantName );
				break;

			case CMerchantGateway::LITLE_CREDIT_CARD:
				$objGateway = new CLitleGateway();
				$objGateway->setMerchantName( $strMerchantName );
				break;

			case CMerchantGateway::VITAL:
				$objGateway = $this->generateVitalGateway();
				break;

			case CMerchantGateway::FIRST_REGIONAL_ACH:
			case CMerchantGateway::FIRST_REGIONAL_CHECK21:
				$objGateway = $this->generateFirstRegionalGateway();
				break;

			case CMerchantGateway::ZIONS_ACH:
			case CMerchantGateway::ZIONS_CHECK21:
				$objGateway = $this->generateZionsGateway();
				break;

			case CMerchantGateway::FIFTH_THIRD_ACH:
			case CMerchantGateway::FIFTH_THIRD_CHECK21:
				$objGateway = $this->generateFifthThirdGateway();
				break;

			case CMerchantGateway::FIRST_REGIONAL_WU_EMO:
				$objGateway = $this->generateWesternUnionGateway();
				break;

			case CMerchantGateway::WORLDPAY_INTL_CREDIT_CARD:
				$objGateway = new CWorldpayWPGCcGateway();
				$objGateway->setMerchantName( $strMerchantName );
				// make sure the currency object is loaded before populating the gateway
				$objArPayment->getOrFetchCurrency( $this->getClientDatabase() );
				break;

			case CMerchantGateway::MONEY_GRAM:
				$objGateway = $this->generateMoneyGramGateway();
				break;

			case CMerchantGateway::SEPA_DIRECT_DEBIT:
				$objGateway = \Psi\Core\Payment\Gateways\WorldpayWPG\CSepaDirectDebit::create();
				$objGateway->setMerchantAccount( $this );
				$objArPayment->getOrFetchCurrency( $this->getClientDatabase() );
				break;

			case CMerchantGateway::WORLDPAY_DIRECT_DEBIT:
				$objGateway = \Psi\Core\Payment\Gateways\WorldPay\CDirectDebit::create();
				break;

			default:
				trigger_error( 'Customer payment did not contain a valid merchant gateway id.', E_USER_ERROR );
				return false;
		}

		if( false == is_null( $objArPayment->getPaymentTypeId() ) ) {
			$objGateway = $this->determineMerchantGatewayCredentials( $objGateway, $objArPayment );
			$objGateway->setProcessingBankAccountId( $this->getMethodByAmount( $objArPayment->getPaymentTypeId() )->getProcessingBankAccountId() );
		}

		if( true == isset ( $objGateway ) ) {
			$objGateway->populateGateway( $objArPayment );
		}

		return $objGateway;
	}

	public function determineMerchantGatewayCredentials( $objGateway, $objArPayment ) {

		$objMethod = $this->getMethodByAmount( $objArPayment->getPaymentTypeId() );

		if( CMerchantGateway::WORLDPAY_INTL_CREDIT_CARD != $objArPayment->getMerchantGatewayId() ) {
			$objGateway->setGatewayUsername( ( !is_null( $objMethod ) ) ? $objMethod->getGatewayUsername() : NULL );
			$objGateway->setGatewayPassword( ( !is_null( $objMethod ) ) ? $objMethod->getGatewayPassword() : NULL );

			return $objGateway;
		}

		$boolIsEntrata = ( CPsProduct::ENTRATA == $objArPayment->getPsProductId() ? true : false );
		$boolIsScheduledPayment = ( false == is_null( $objArPayment->getScheduledPaymentId() ) ? true : false );

		if( true == $boolIsScheduledPayment ) {
			if( CMerchantGateway::SEPA_DIRECT_DEBIT == $objArPayment->getMerchantGatewayId() && CPaymentType::SEPA_DIRECT_DEBIT == $objArPayment->getPaymentTypeId() ) {
				$objGateway->setGatewayUsername( ( !is_null( $objMethod ) ) ? $objMethod->getWorldpayRecurringUsername() : NULL );
				$objGateway->setGatewayPassword( ( !is_null( $objMethod ) ) ? $objMethod->getWorldpayRecurringPassword() : NULL );
				$objGateway->setMerchantCode( ( !is_null( $objMethod ) ) ? $objMethod->getWorldpayRecurringMerchantCode() : NULL );

				return $objGateway;
			}

			$objGateway->setGatewayUsername( ( !is_null( $objMethod ) ) ? $objMethod->getWorldpayCcRecurringUsername() : NULL );
			$objGateway->setGatewayPassword( ( !is_null( $objMethod ) ) ? $objMethod->getWorldpayCcRecurringPassword() : NULL );
			$objGateway->setMerchantCode( ( !is_null( $objMethod ) ) ? $objMethod->getWorldpayCcRecurringMerchantCode() : NULL );
			$objGateway->setInteractionType( CWorldpayWPGCcGateway::RECR );

			return $objGateway;
		}

		if( true == $boolIsEntrata || true == $objArPayment->getForceWorldpayMoto() ) {
			$objGateway->setGatewayUsername( ( !is_null( $objMethod ) ) ? $objMethod->getWorldpayCcMotoUsername() : NULL );
			$objGateway->setGatewayPassword( ( !is_null( $objMethod ) ) ? $objMethod->getWorldpayCcMotoPassword() : NULL );
			$objGateway->setMerchantCode( ( !is_null( $objMethod ) ) ? $objMethod->getWorldpayCcMotoMerchantCode() : NULL );
			$objGateway->setInteractionType( CWorldpayWPGCcGateway::MOTO );

			return $objGateway;
		}

		if( CMerchantGateway::SEPA_DIRECT_DEBIT == $objArPayment->getMerchantGatewayId() && CPaymentType::SEPA_DIRECT_DEBIT == $objArPayment->getPaymentTypeId() ) {
			$objGateway->setGatewayUsername( ( !is_null( $objMethod ) ) ? $objMethod->getWorldpayEcomUsername() : NULL );
			$objGateway->setGatewayPassword( ( !is_null( $objMethod ) ) ? $objMethod->getWorldpayEcomPassword() : NULL );
			$objGateway->setMerchantCode( ( !is_null( $objMethod ) ) ? $objMethod->getWorldpayEcomMerchantCode() : NULL );

			return $objGateway;
		}

		$objGateway->setGatewayUsername( ( !is_null( $objMethod ) ) ? $objMethod->getWorldpayCcEcomUsername() : NULL );
		$objGateway->setGatewayPassword( ( !is_null( $objMethod ) ) ? $objMethod->getWorldpayCcEcomPassword() : NULL );
		$objGateway->setMerchantCode( ( !is_null( $objMethod ) ) ? $objMethod->getWorldpayCcEcomMerchantCode() : NULL );
		$objGateway->setInteractionType( CWorldpayWPGCcGateway::ECOM );
		$objGateway->set3dsFlexEnabled( $this->getWorldpay3dsFlexIsEnabled() );

		return $objGateway;
	}

	public function createArPaymentTransmission() {

		$objArPaymentTransmission = new CArPaymentTransmission();
		$objArPaymentTransmission->setCid( $this->getCid() );
		$objArPaymentTransmission->setCompanyMerchantAccountId( $this->getId() );
		$objArPaymentTransmission->setTransmissionDatetime( date( 'm/d/Y H:i:s' ) );

		return $objArPaymentTransmission;
	}

	// endregion

	// region Get Functions

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function getArPayment() {
		return $this->m_objArPayment;
	}

	public function getCaptureDelayDays() {
		return $this->m_intCaptureDelayDays;
	}

	public function getPositiveArPayments() {
		return $this->m_arrobjPositiveArPayments;
	}

	/**
	 * @return CArPayment[]
	 */
	public function getNegativeArPayments() {
		return $this->m_arrobjNegativeArPayments;
	}

	public function getSettlingArPayments() {
		return $this->m_arrobjSettlingArPayments;
	}

	public function getReturningArPayments() {
		return $this->m_arrobjReturningArPayments;
	}

	public function getReversingArPayments() {
		return $this->m_arrobjReversingArPayments;
	}

	public function getReturningReversedArPayments() {
		return $this->m_arrobjReturningReversedArPayments;
	}

	public function getClearingTransactions() {
		return $this->m_arrobjClearingTransactions;
	}

	public function getMaxMonthlyProcessAmount() {
		$intPaymentTypeId = ( $this->getCountryCode() == CCountry::CODE_CANADA ) ? CPaymentType::PAD : CPaymentType::ACH;
		return ( $this->methodIsEnabled( $intPaymentTypeId ) ) ? $this->getMethodByAmount( $intPaymentTypeId )->getMaxMonthlyProcessAmount() : 0.00;
	}

	public function getUsePreexisting() {
		return $this->m_strUsePreexisting;
	}

	public function getAmountDue() {
		return $this->m_fltAmountDue;
	}

	/**
	 * @return CClient
	 */
	public function getClient() {
		return $this->m_objClient;
	}

	public function getIsControlledDistribution() {
		$boolIsControlledDistribution = false;
		$arrobjMerchantAccountMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMerchantAccountMethods, function( CMerchantAccountMethod $objMethod ) use( &$boolIsControlledDistribution ) {
			$intPaymentTypeId = $this->getCountryCode() == CCountry::CODE_CANADA ? CPaymentType::PAD : CPaymentType::ACH;
			if( $intPaymentTypeId == $objMethod->getPaymentTypeId() ) {
				$boolIsControlledDistribution = $objMethod->getIsControlledDistribution();
			}
		} );
		return $boolIsControlledDistribution;
	}

	public function getCcDistributionDelayDays() {
		return ( $this->methodIsEnabled( CPaymentType::MASTERCARD ) ) ? $this->getMethodByAmount( CPaymentType::MASTERCARD )->getDistributionDelayDays() : 0;
	}

	public function getMaxPaymentAmount() {
		if( !in_array( $this->getCountryCode(), CCountry::$c_arrstrPaymentFacilitatorCountryCodes ) && CCurrency::CURRENCY_CODE_EUR == $this->getCurrencyCode() && $this->methodIsEnabled( CPaymentType::SEPA_DIRECT_DEBIT ) ) {
			return $this->getTopTierMethod( CPaymentType::SEPA_DIRECT_DEBIT )->getMaxPaymentAmount();
		}
		$intPaymentTypeId = ( $this->getCountryCode() == CCountry::CODE_CANADA ) ? CPaymentType::PAD : CPaymentType::ACH;
		return $this->getTopTierMethod( $intPaymentTypeId )->getMaxPaymentAmount();
	}

	public function getUndelayedSettlementCeiling() {
		$intPaymentTypeId = ( $this->getCountryCode() == CCountry::CODE_CANADA ) ? CPaymentType::PAD : CPaymentType::ACH;
		return ( $this->methodIsEnabled( $intPaymentTypeId ) ) ? $this->getMethodByAmount( $intPaymentTypeId )->getUndelayedSettlementCeiling() : NULL;
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function getRoutingNumber() {
		return $this->m_strRoutingNumber;
	}

	public function getAccountNumberEncrypted() {
		return $this->m_strAccountNumberEncrypted;
	}

	public function getTaxNumber() {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getTaxNumberEncrypted() ) ) {
			return NULL;
		}

		return CCrypto::createService()->decrypt( $this->getTaxNumberEncrypted(), CONFIG_SODIUM_KEY_CHECK_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CHECK_TAX_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getResidentReturnFee( $intPaymentTypeId ) {
		return ( $this->methodIsEnabled( $intPaymentTypeId ) ) ? $this->getMethodByAmount( $intPaymentTypeId )->getResidentReturnFee() : 0.00;
	}

	public function getMerchantGatewayId( $intPaymentTypeId ) {
		return ( $this->methodIsEnabled( $intPaymentTypeId ) ) ? $this->getMethodByAmount( $intPaymentTypeId )->getMerchantGatewayId() : 0;
	}

	public function getAcceptedPaymentTypeIds( $fltPaymentAmount = 0.0 ) {
		// Return empty result set if merchant account has been disabled.
		if( 1 == $this->getIsDisabled() ) {
			return NULL;
		}

		$arrintPaymentTypeIds = NULL;

		foreach( CPaymentType::$c_arrintMerchantAccountPaymentTypeIds as $intPaymentTypeId ) {
			$objMethod = $this->getMethodByAmount( $intPaymentTypeId, $fltPaymentAmount );
			if( valObj( $objMethod, CMerchantMethod::class, 'Id' ) ) {
				$arrintPaymentTypeIds[] = $intPaymentTypeId;
			}
		}

		return $arrintPaymentTypeIds;
	}

	public function getCid() {
		if( true == valObj( $this->m_objClient, CClient::class ) ) {
			return $this->m_objClient->getId();
		} else {
			return parent::getCid();
		}
	}

	public function getCcReversalDelayDays() {
		// TODO: This needs an update either in functionality or how it is used, as reversal delay days can vary by amount
		return ( $this->methodIsEnabled( CPaymentType::MASTERCARD ) ) ? $this->getMethodByAmount( CPaymentType::MASTERCARD )->getReversalDelayDays() : 0;
	}

	public function getDistributionAccount() {
		return $this->m_objDistributionAccount;
	}

	public function getIntermediaryAccount() {
		return $this->m_objIntermediaryAccount;
	}

	public function getReturnsAccount() {
		return $this->m_objReturnsAccount;
	}

	public function getEntityId() {
		return $this->m_intEntityId;
	}

	public function getIsOverrideFeeAmountValidation() {
		return $this->m_intIsOverrideFeeAmountValidation;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getPropertyUnits() {
		return $this->m_intPropertyUnits;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	// endregion

	// region Set Functions

	public function setBankAccountId( $intBankAccountId ) {
		$this->m_intBankAccountId = $intBankAccountId;
	}

	public function setArPayment( $objArPayment ) {
		$this->m_objArPayment = $objArPayment;
	}

	public function setFundMovementTypeId( $intFundMovementTypeId ) {
		$this->m_intFundMovementTypeId = $intFundMovementTypeId;
	}

	public function setCaptureDelayDays( $intCaptureDelayDays ) {
		$this->m_intCaptureDelayDays = $intCaptureDelayDays;
	}

	public function setArTransactions( $arrobjArTransactions ) {
		$this->m_arrobjArTransactions = $arrobjArTransactions;
	}

	public function setDistributionAccount( $objDistributionAccount ) {
		$this->m_objDistributionAccount = $objDistributionAccount;
	}

	public function setIntermediaryAccount( $objIntermediaryAccount ) {
		$this->m_objIntermediaryAccount = $objIntermediaryAccount;
	}

	public function setReturnsAccount( $objReturnsAccount ) {
		$this->m_objReturnsAccount = $objReturnsAccount;
	}

	public function setEntityId( $intEntityId ) {
		$this->m_intEntityId = $intEntityId;
	}

	public function setRoutingNumber( $strRoutingNumber ) {
		$this->m_strRoutingNumber = $strRoutingNumber;
	}

	public function setAccountNumberEncrypted( $strAccountNumberEncrypted ) {
		$this->m_strAccountNumberEncrypted = $strAccountNumberEncrypted;
	}

	public function setPositiveArPayments( $arrobjPositiveArPayments ) {
		$this->m_arrobjPositiveArPayments = $arrobjPositiveArPayments;
	}

	public function setNegativeArPayments( $arrobjNegativeArPayments ) {
		$this->m_arrobjNegativeArPayments = $arrobjNegativeArPayments;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['cc_reversal_delay_days'] ) )			$this->setCcReversalDelayDays( $arrmixValues['cc_reversal_delay_days'] );
		if( isset( $arrmixValues['cc_cut_off_time'] ) )					$this->setCcCutOffTime( $arrmixValues['cc_cut_off_time'] );
		if( isset( $arrmixValues['cc_distribution_delay_days'] ) )		$this->setCcDistributionDelayDays( $arrmixValues['cc_distribution_delay_days'] );

		if( isset( $arrmixValues['undelayed_settlement_ceiling'] ) )	$this->setUndelayedSettlementCeiling( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['undelayed_settlement_ceiling'] ): $arrmixValues['undelayed_settlement_ceiling'] );
		if( isset( $arrmixValues['max_monthly_process_amount'] ) )		$this->setMaxMonthlyProcessAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['max_monthly_process_amount'] ): $arrmixValues['max_monthly_process_amount'] );

		if( isset( $arrmixValues['use_preexisting'] ) )					$this->setUsePreexisting( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['use_preexisting'] ): $arrmixValues['use_preexisting'] );
		if( isset( $arrmixValues['amount_due'] ) )						$this->setAmountDue( $arrmixValues['amount_due'] );
		if( isset( $arrmixValues['ar_code_id'] ) )						$this->setArCodeId( $arrmixValues['ar_code_id'] );
		if( isset( $arrmixValues['is_controlled_distribution'] ) )		$this->setIsControlledDistribution( $arrmixValues['is_controlled_distribution'] );
		if( isset( $arrmixValues['override_fee_amount_validation'] ) )	$this->setIsOverrideFeeAmountValidation( $arrmixValues['override_fee_amount_validation'] );

		if( isset( $arrmixValues['routing_number'] ) )					$this->setRoutingNumber( $arrmixValues['routing_number'] );
		if( isset( $arrmixValues['account_number_encrypted'] ) )		$this->setAccountNumberEncrypted( $arrmixValues['account_number_encrypted'] );

		if( isset( $arrmixValues['bank_account_id'] ) )					$this->setBankAccountId( $arrmixValues['bank_account_id'] );

		if( isset( $arrmixValues['property_name'] ) )					$this->setPropertyName( $arrmixValues['property_name'] );
		if( isset( $arrmixValues['number_of_units'] ) )					$this->setPropertyUnits( $arrmixValues['number_of_units'] );
		if( isset( $arrmixValues['property_id'] ) )						$this->setPropertyId( $arrmixValues['property_id'] );

		// set values on merchant account methods
		$arrintUpdatedPaymentTypeIds = [];
		foreach( $arrmixValues as $strKey => $strValue ) {
			$strPaymentType = explode( '_', $strKey )[0];
			$intPaymentTypeId = CPaymentType::stringToId( ucfirst( $strPaymentType ) );
			if( false == is_null( $intPaymentTypeId ) && false == in_array( $intPaymentTypeId, $arrintUpdatedPaymentTypeIds ) ) {
				$arrintUpdatedPaymentTypeIds[] = $intPaymentTypeId;
			}
		}

		$arrobjMethods = $this->getMerchantMethods();

		/**
		 * Sets up dynamic gateway data based on gateway to payment type mapping
		 */
		foreach( CGatewayType::$c_arrintMerchantAccountGatewayTypes as $intGatewayTypeId ) {
			$arrintPaymentTypeIds = CPaymentType::$c_arrmixPaymentTypesByGatewayTypeId[$intGatewayTypeId];
			$strGatewayTypePrefix = CGatewayType::idToString( $intGatewayTypeId );

			array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( $arrintUpdatedPaymentTypeIds, $arrintPaymentTypeIds, $strGatewayTypePrefix, $arrstrValues, $boolStripSlashes ) {
				if( !in_array( $objMethod->getPaymentTypeId(), $arrintUpdatedPaymentTypeIds ) || !in_array( $objMethod->getPaymentTypeId(), $arrintPaymentTypeIds ) ) return;

				if( true == isset( $arrstrValues[$strGatewayTypePrefix . '_gateway_username'] ) ) 				$objMethod->setGatewayUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues[$strGatewayTypePrefix . '_gateway_username'] ) : $arrstrValues[$strGatewayTypePrefix . '_gateway_username'] );
				if( true == isset( $arrstrValues[$strGatewayTypePrefix . '_gateway_password'] ) ) 				$objMethod->setGatewayPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues[$strGatewayTypePrefix . '_gateway_password'] ) : $arrstrValues[$strGatewayTypePrefix . '_gateway_password'] );
				if( true == isset( $arrstrValues[$strGatewayTypePrefix . '_login_username'] ) ) 				$objMethod->setLoginUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues[$strGatewayTypePrefix . '_login_username'] ) : $arrstrValues[$strGatewayTypePrefix . '_login_username'] );
				if( true == isset( $arrstrValues[$strGatewayTypePrefix . '_login_password'] ) ) 				$objMethod->setLoginPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues[$strGatewayTypePrefix . '_login_password'] ) : $arrstrValues[$strGatewayTypePrefix . '_login_password'] );
				if( true == isset( $arrstrValues[$strGatewayTypePrefix . '_max_payment_amount'] ) ) 			$objMethod->setMaxPaymentAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues[$strGatewayTypePrefix . '_max_payment_amount'] ) : $arrstrValues[$strGatewayTypePrefix . '_max_payment_amount'] );
			} );
		}
	}

	public function setOrderNum( $intOrderNum ) {
		$this->m_intOrderNum = $intOrderNum;
	}

	public function setUsePreexisting( $strUsePreexisting ) {
		$this->m_strUsePreexisting = CStrings::strTrimDef( $strUsePreexisting, 20, NULL, true );
	}

	public function setAmountDue( $fltAmountDue ) {
		$this->m_fltAmountDue = CStrings::strToFloatDef( $fltAmountDue, NULL, true, 4 );
	}

	public function setArCodeId( $intArCodeId ) {
		$this->m_intArCodeId = $intArCodeId;
	}

	public function setClient( $objClient ) {
		if( false == valObj( $objClient, CClient::class ) ) {
			trigger_error( 'Incompatible types: \'CClient\' and \'' . get_class( $objClient ) . '\' - CCompanyMerchantAccount::setClient()', E_USER_ERROR );
			return false;
		}

		$this->m_objClient = $objClient;
		return true;
	}

	public function setIsControlledDistribution( $boolIsControlledDistribution ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function ( CMerchantMethod $objMethod ) use ( $boolIsControlledDistribution ) {
			if( in_array( $objMethod->getPaymentTypeId(), [ CPaymentType::ACH, CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX, CPaymentType::CHECK_21, CPaymentType::EMONEY_ORDER ] ) ) {
				$objMethod->setIsControlledDistribution( $boolIsControlledDistribution );
			}
		} );
	}

	public function setCcDistributionDelayDays( $intDistributionDelayDays ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function ( CMerchantMethod $objMethod ) use ( $intDistributionDelayDays ) {
			if( in_array( $objMethod->getPaymentTypeId(), [ CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX ] ) ) {
				$objMethod->setDistributionDelayDays( $intDistributionDelayDays );
			}
		} );
	}

	public function setNullNonCopiedFields() {
		$this->setId( NULL );
		$this->setDistributionAccountId( NULL );
		$this->setBillingAccountId( NULL );
		$this->setReturnsAccountId( NULL );
		$this->setAccountName( NULL );
		$this->setDescription( NULL );
		$this->setNotes( NULL );
		$this->setIsDisabled( NULL );
	}

	public function setCid( $intCid ) {
		parent::setCid( $intCid );
		// If we are setting the client id directly we unbind the
		// client object to avoid any concurrency issues
		$this->m_objClient = NULL;
	}

	public function setCcGatewayUsername( $strGatewayUsername ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function ( CMerchantMethod $objMethod ) use ( $strGatewayUsername ) {
			if( in_array( $objMethod->getPaymentTypeId(), [ CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX ] ) ) {
				$objMethod->setGatewayUsername( $strGatewayUsername );
			}
		} );
		// store the decrypted mid in details for reporting
		$this->setCcGatewayMid( $strGatewayUsername );
	}

	public function setMaxPaymentAmount( $fltMaxPaymentAmount ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantMethod $objMethod ) use( $fltMaxPaymentAmount ) {
			if( in_array( $objMethod->getPaymentTypeId(), [CPaymentType::ACH,CPaymentType::PAD,CPaymentType::MASTERCARD,CPaymentType::DISCOVER,CPaymentType::AMEX,CPaymentType::CHECK_21,CPaymentType::EMONEY_ORDER] ) ) {
				$objMethod->setMaxPaymentAmount( $fltMaxPaymentAmount );
			}
		} );
	}

	public function setUndelayedSettlementCeiling( $fltUndelayedSettlementCeiling ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantMethod $objMethod ) use( $fltUndelayedSettlementCeiling ) {
			if( in_array( $objMethod->getPaymentTypeId(), [CPaymentType::ACH,CPaymentType::PAD,CPaymentType::MASTERCARD,CPaymentType::DISCOVER,CPaymentType::AMEX,CPaymentType::CHECK_21,CPaymentType::EMONEY_ORDER] ) ) {
				$objMethod->setUndelayedSettlementCeiling( $fltUndelayedSettlementCeiling );
			}
		} );
	}

	public function setMaxMonthlyProcessAmount( $fltMaxMonthlyProcessAmount ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantMethod $objMethod ) use( $fltMaxMonthlyProcessAmount ) {
			if( in_array( $objMethod->getPaymentTypeId(), CPaymentType::$c_arrintMerchantAccountPaymentTypeIds ) ) {
				$objMethod->setMaxMonthlyProcessAmount( $fltMaxMonthlyProcessAmount );
			}
		} );
	}

	public function setCcReversalDelayDays( $intCcReversalDelayDays ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( $intCcReversalDelayDays ) {
			if( in_array( $objMethod->getPaymentTypeId(), [CPaymentType::VISA,CPaymentType::MASTERCARD,CPaymentType::DISCOVER,CPaymentType::AMEX] ) ) {
				$objMethod->setReversalDelayDays( $intCcReversalDelayDays );
			}
		} );
	}

	public function setCcCutOffTime( $fltCcCutOffTime ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( $fltCcCutOffTime ) {
			if( in_array( $objMethod->getPaymentTypeId(), [CPaymentType::VISA,CPaymentType::MASTERCARD,CPaymentType::DISCOVER,CPaymentType::AMEX] ) ) {
				$objMethod->setCutOffTime( $fltCcCutOffTime );
			}
		} );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setPropertyUnits( $strPropertyUnits ) {
		$this->m_intPropertyUnits = $strPropertyUnits;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}


	public function setIsOverrideFeeAmountValidation( $intIsOverrideFeeAmountValidation ) {
		$this->m_intIsOverrideFeeAmountValidation = $intIsOverrideFeeAmountValidation;
	}

	// endregion

	// region Validation Functions

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessingBankId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFundMovementTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDistributionAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIntermediaryAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillingAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnsAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClearingAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDonationAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRebateAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMerchantProcessingTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDistributionGroupTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllocationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotificationEmail() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMerchantName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPortalInputLabel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExternalLegalEntityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDistributionFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDistributionFailureFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFixedConvenienceFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTargetDiscountRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisableDistributionUntil() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEnableFeeRestructuring() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEnablePinlessDebitProcessing() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEnableSeparateDebitPricing() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNumAchReturnRetries() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowInternationalCards() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillInternationalCards() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequireSeparateSettlement() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConvertCheck21ToAch() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAggregated() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		$this->addMethodErrors();

		return $boolIsValid;
	}

	public function valCh21IsEnabled( $boolIsArPaymentValidation = false ) {
		$boolIsValid = true;

		if( true == $boolIsArPaymentValidation && 1 != $this->methodIsEnabled( CPaymentType::CHECK_21 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ch21_enabled', 'Configuration Error: Check 21 is not an active payment option.' ) );
		}

		return $boolIsValid;
	}

	public function validateTerminal( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case 'validate_terminal_check_21_payment_insert':
				$boolIsValid &= $this->valCh21IsEnabled( true );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	// endregion

	// region Fetch Functions

	public function fetchDistributionAccount( $objAdminDatabase ) {
		return CAccounts::createService()->fetchAccountByIdAndCid( $this->m_intDistributionAccountId, $this->m_intCid, $objAdminDatabase );
	}

	public function fetchIntermediaryAccount( $objAdminDatabase ) {
		$this->m_objIntermediaryAccount = CAccounts::createService()->fetchAccountByIdAndCid( $this->m_intIntermediaryAccountId, $this->m_intCid, $objAdminDatabase );
		return $this->m_objIntermediaryAccount;
	}

	public function fetchClearingAccount( $objAdminDatabase ) {
		return CAccounts::createService()->fetchAccountByIdAndCid( $this->getClearingAccountId(), $this->m_intCid, $objAdminDatabase );
	}

	public function fetchReturnsAccount( $objAdminDatabase ) {
		$this->m_objReturnsAccount = CAccounts::createService()->fetchAccountByIdAndCid( $this->getReturnsAccountId(), $this->m_intCid, $objAdminDatabase );
		return $this->m_objReturnsAccount;
	}

	public function fetchBillingAccount( $objAdminDatabase ) {
		return CAccounts::createService()->fetchAccountByIdAndCid( $this->m_intBillingAccountId, $this->m_intCid, $objAdminDatabase );
	}

	public function fetchClient( $objDatabase ) {
		$objClient = CClients::fetchClientById( $this->getCid(), $objDatabase );

		return $objClient;
	}

	public function fetchArCode( $objAdminDatabase ) {
		$objArCode = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByIdByCid( $this->getArCodeId(), $this->getCid(), $objAdminDatabase );

		return $objArCode;
	}

	public function fetchProperties( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByMerchantAccountIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchArPaymentTransmissionById( $intArPaymentTransmissionId, $objDatabase ) {
		return CArPaymentTransmissions::fetchArPaymentTransmissionsByIdByCompanyMerchantAccountIdByCid( $intArPaymentTransmissionId, $this->getId(), $this->getCid(), $objDatabase );
	}

	// endregion

	// region Other Functions

	public function increaseAmountDue( $fltIncreaseAmount ) {
		if( false == is_numeric( $this->m_fltAmountDue ) ) $this->m_fltAmountDue = 0;
		if( false == is_numeric( $fltIncreaseAmount ) ) return;

		$this->m_fltAmountDue += $fltIncreaseAmount;
	}

	public function generateFirstRegionalGateway() {

		$objGateway = new CFirstRegionalGateway();
		$objGateway->initialize();

		if( false == is_null( $this->getMerchantName() ) ) {
			$objGateway->setMerchantName( $this->getMerchantName() );
		} else {
			$objGateway->setMerchantName( $this->getAccountName() );
		}

		return $objGateway;
	}

	public function generateZionsGateway() {

		$objGateway = new CZionsGateway();
		$objGateway->initialize();

		if( false == is_null( $this->getMerchantName() ) ) {
			$objGateway->setMerchantName( $this->getMerchantName() );
		} else {
			$objGateway->setMerchantName( $this->getAccountName() );
		}

		return $objGateway;
	}

	public function generateFifthThirdGateway() {

		$objGateway = new CFifthThirdGateway();
		$objGateway->initialize();

		if( false == is_null( $this->getMerchantName() ) ) {
			$objGateway->setMerchantName( $this->getMerchantName() );
		} else {
			$objGateway->setMerchantName( $this->getAccountName() );
		}

		return $objGateway;
	}

	public function generateMoneyGramGateway() {

		$objGateway = new CMoneyGramGateway();
		$objGateway->initialize();

		if( false == is_null( $this->getMerchantName() ) ) {
			$objGateway->setMerchantName( $this->getMerchantName() );
		} else {
			$objGateway->setMerchantName( $this->getAccountName() );
		}

		return $objGateway;
	}

	public function generateWesternUnionGateway() {

		$objGateway = new CWesternUnionGateway();
		$objGateway->initialize();

		if( false == is_null( $this->getMerchantName() ) ) {
			$objGateway->setMerchantName( $this->getMerchantName() );
		} else {
			$objGateway->setMerchantName( $this->getAccountName() );
		}

		return $objGateway;
	}

	public function generateVitalGateway() {

		$objGateway = new CVitalGateway();
		$objGateway->initialize();

		if( false == is_null( $this->getMerchantName() ) ) {
			$objGateway->setMerchantName( $this->getMerchantName() );
		} else {
			$objGateway->setMerchantName( $this->getAccountName() );
		}

		return $objGateway;
	}

	public function generateMyEcheckGateway() {

		$objGateway = new CMyEcheckGateway();
		$objGateway->initialize();

		if( false == is_null( $this->getMerchantName() ) ) {
			$objGateway->setMerchantName( $this->getMerchantName() );
		} else {
			$objGateway->setMerchantName( $this->getAccountName() );
		}

		return $objGateway;
	}

	public function clearErrorMsgs() {
		$this->m_arrobjErrorMsgs = NULL;
	}

	public function releaseConflictingEndorsements( $intCurrentUserId, $objPaymentDatabase ) {

		$objOldCompanyMerchantAccount = \Psi\Eos\Payment\CCompanyMerchantAccounts::createService()->fetchCompanyMerchantAccountById( $this->getId(), $objPaymentDatabase );

		if( false == valObj( $objOldCompanyMerchantAccount, CCompanyMerchantAccount::class ) ) {
			return false;
		}

		if( $this->getProcessingBankId() == $objOldCompanyMerchantAccount->getProcessingBankId() ) {
			return true;
		}

		$arrobjArPaymentImages = CArPaymentImages::fetchUnBatchedEndorsedArPaymentImagesByCompanyMerchantAccountId( $this->getId(), $objPaymentDatabase );

		if( true == is_null( $arrobjArPaymentImages ) ) {
			return true;
		}

		foreach( $arrobjArPaymentImages as $objArPaymentImage ) {

			if( false === \Psi\CStringService::singleton()->stristr( $objArPaymentImage->getImageName(), 'endorsed_' ) ) {
				continue;
			}

			$objArPaymentImage->setImageName( str_replace( 'endorsed_', '', $objArPaymentImage->getImageName() ) );
			$objArPaymentImage->setEndorsedOn( NULL );

			if( false == $objArPaymentImage->update( $intCurrentUserId, $objPaymentDatabase ) ) {
				$this->addErrorMsgs( $objArPaymentImage->getErrorMsgs() );
				return false;
			}
		}

		return true;
	}

	// endregion

}
