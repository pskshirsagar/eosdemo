<?php

class CExternalMergeField extends CBaseExternalMergeField {

    protected $m_arrstrDefaultMergeFieldNames;
    protected $m_strDefaultBlockMergeFieldName;
    protected $m_intBlockIndex;
    protected $m_strFieldName;

    /**
     * Get Functions
     */

    public function getDefaultMergeFieldNames() {
        return json_decode( $this->m_arrstrDefaultMergeFieldNames );
    }

    public function getDefaultBlockMergeFieldName() {
        return $this->m_strDefaultBlockMergeFieldName;
    }

    public function getBlockIndex() {
        return $this->m_intBlockIndex;
    }

    public function getFieldName() {
        return $this->m_strFieldName;
    }

    /**
     * Set Functions
     */

    public function setDefaultMergeFieldNames( $arrstrDefaultMergeFieldNames ) {
        $this->m_arrstrDefaultMergeFieldNames = $arrstrDefaultMergeFieldNames;
    }

    public function setDefaultBlockMergeFieldName( $strDefaultBlockMergeFieldName ) {
        $this->m_strDefaultBlockMergeFieldName = $strDefaultBlockMergeFieldName;
    }

    public function setBlockIndex( $intBlockGroup ) {
        $this->m_intBlockIndex = $intBlockGroup;
        $this->setDetailsProperty( 'block_index', $intBlockGroup );
    }

    public function setDetailsProperty( string $strKey, $mixValue ) {
	    $this->setDetailsField( $strKey, $mixValue );
    }

    public function setFieldName( $strFieldName ) {
        $this->m_strFieldName = $strFieldName;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

        parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['default_merge_field_names'] ) ) {
            $this->setDefaultMergeFieldNames( $arrmixValues['default_merge_field_names'] );
        }

        if( true == isset( $arrmixValues['default_block_merge_field_name'] ) ) {
            $this->setDefaultBlockMergeFieldName( $arrmixValues['default_block_merge_field_name'] );
        }

        if( true == isset( $arrmixValues['block_index'] ) ) {
            $this->setBlockIndex( $arrmixValues['block_index'] );
        }

        if( true == isset( $arrmixValues['field_name'] ) ) {
            $this->setFieldName( $arrmixValues['field_name'] );
        }
    }

    /**
     * Validate Functions
     */

    public function valId() {
        $boolIsValid = true;
        if( true == is_null( $this->getId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'Id is required.' ) );
        }
        return $boolIsValid;
    }

    public function valTransmissionVendorId() {
        $boolIsValid = true;
        if( true == is_null( $this->getTransmissionVendorId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'Transmission vendor id is required.' ) );
        }
        return $boolIsValid;
    }

    public function valStateCode() {
        $boolIsValid = true;
        if( true == is_null( $this->getStateCode() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'State code is required.' ) );
        }
        return $boolIsValid;
    }

    public function valApartmentAssociation() {
        $boolIsValid = true;
        if( true == is_null( $this->getApartmentAssociation() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'Apartment association is required.' ) );
        }
        return $boolIsValid;
    }

    public function valExternalFormKeys() {
        $boolIsValid = true;
        if( true == is_null( $this->getExternalFormKeys() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'External form keys field is required.' ) );
        }
        return $boolIsValid;
    }

    public function valFormType() {
        $boolIsValid = true;
        if( true == is_null( $this->getFormType() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'Form type is required.' ) );
        }
        return $boolIsValid;
    }

    public function valExternalMergeFieldName() {
        $boolIsValid = true;
        if( true == is_null( $this->getExternalMergeFieldName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'External merge field name is required.' ) );
        }
        return $boolIsValid;
    }

    public function valDataType() {
        $boolIsValid = true;
        if( true == is_null( $this->getDataType() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'Data type is required.' ) );
        }
        return $boolIsValid;
    }

    public function valMaxLength() {
        $boolIsValid = true;
        if( true == is_null( $this->getMaxLength() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'Max length is required.' ) );
        }
        return $boolIsValid;
    }

    public function valDefaultMergeFieldIds() {
        $boolIsValid = true;
        if( true == is_null( $this->getDefaultMergeFieldIds() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'Default merge field ids field is required.' ) );
        }
        return $boolIsValid;
    }

    public function valBlockDefaultMergeFieldId() {
        $boolIsValid = true;
        if( true == is_null( $this->getBlockDefaultMergeFieldId() ) ) {
            $boolIsValid = false;
        }
        return $boolIsValid;
    }

    public function valMergeFieldGroupId() {
        $boolIsValid = true;
        if( true == is_null( $this->getMergeFieldGroupId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'Merge Field group is required.' ) );
        }
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        if( true == is_null( $this->getIsPublished() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'Is published field is required.' ) );
        }
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        if( true == is_null( $this->getDeletedBy() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'Deleted By field is required.' ) );
        }
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        if( true == is_null( $this->getDeletedOn() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'Deleted on Field is required.' ) );
        }
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
                $boolIsValid  = ( true == $this->valBlockDefaultMergeFieldId() )  ? ( $this->valDefaultMergeFieldIds() ) : true;
            case VALIDATE_DELETE:
                break;

            default:
                $boolIsValid = false;
                break;
        }

        return $boolIsValid;
    }

    public function updateWithConditions( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
        $strSql = $this->update( $intCurrentUserId, $objDatabase, true );
        if( false == valStr( $strSql ) ) {
            return false;
        }
        $strCondition = ' apartment_association =' . $this->sqlApartmentAssociation();
        if( CTransmissionVendor::TENANT_TECH == $this->getTransmissionVendorId() && NULL == $this->getApartmentAssociation() ) {
            $strCondition = ' true';
        }
        $strSql = explode( 'WHERE', $strSql )[0];
        $strSql .= 'WHERE
                        transmission_vendor_id =' . $this->sqlTransmissionVendorId() . '
                        AND ' . $strCondition . '
                        AND state_code =' . $this->sqlStateCode() . '
                        AND external_merge_field_name=' . $this->sqlExternalMergeFieldName();

        if( true == $boolReturnSqlOnly ) {
            return  $strSql;
        } else {
            return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

}
?>
