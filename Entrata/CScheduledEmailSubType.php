<?php

class CScheduledEmailSubType extends CBaseScheduledEmailSubType {

	const MISCELLANEOUS 		= 1;
	const LATE_RENT_REMINDER 	= 2;
	const BUILDING 				= 3;
}
?>