<?php

class CApplicantAssetTypes extends CBaseApplicantAssetTypes {

	public static function fetchApplicantAssetTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CApplicantAssetType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchApplicantAssetType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApplicantAssetType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>