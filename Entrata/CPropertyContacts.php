<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyContacts
 * Do not add any new functions to this class.
 */

class CPropertyContacts extends CBasePropertyContacts {

	public static function fetchPropertyContactByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						* 
					FROM 
						property_contacts
					WHERE 
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchPropertyContact( $strSql, $objDatabase );
	}

	public static function fetchPropertyContactsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if ( false == valArr( $arrintPropertyIds ) ) return [];

		return self::fetchPropertyContacts( sprintf( 'SELECT * FROM property_contacts WHERE property_id IN (' . implode( ',', $arrintPropertyIds ) . ') AND cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>