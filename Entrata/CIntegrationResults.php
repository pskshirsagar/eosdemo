<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationResults
 * Do not add any new functions to this class.
 */

class CIntegrationResults extends CBaseIntegrationResults {

	public static function fetchIntegrationResultByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ir.*,
						id.database_name,
						CASE WHEN irr.id IS NOT NULL
							THEN MIN(irr.reference_number ) OVER( PARTITION BY ir.id, ir.cid )
							ELSE ir.reference_number
						END as reference_number,
						string_agg( irr.reference_number::varchar, \',\') OVER (PARTITION BY ir.id, ir.cid ) AS reference_numbers,
						string_agg( p.property_name::varchar, \',\') OVER (PARTITION BY ir.id, ir.cid ) AS property_names
					FROM
						integration_results ir
						LEFT JOIN integration_clients ic ON ( ir.cid = ic.cid AND ir.integration_client_id = ic.id )
						LEFT JOIN integration_databases id ON ( id.cid = ic.cid AND id.id = ic.integration_database_id )
						LEFT JOIN integration_result_references irr ON( ir.cid = irr.cid AND ir.id = irr.integration_result_id )
						LEFT JOIN properties p ON( p.cid = ir.cid AND ( p.id = irr.property_id OR ir.property_id = p.id ) )
					WHERE
						ir.cid = ' . ( int ) $intCid . '
						AND ir.id = ' . ( int ) $intId . '';

		return parent::fetchIntegrationResult( $strSql, $objDatabase );
	}

	public static function fetchLastIntegrationResultByIntegrationServiceIdByReferenceNumberByCid( $objDatabase, $intIntegrationServiceId, $intReferenceNumber, $intCid ) {

		$strSql = 'SELECT
						ir.*,
						MIN(irr.reference_number ) OVER( PARTITION BY ir.id, ir.cid ) as reference_number,
						string_agg( irr.reference_number::varchar, \',\') OVER (PARTITION BY ir.id, ir.cid ) AS reference_numbers
					FROM
						integration_results ir
						JOIN integration_result_references irr ON( ir.cid=irr.cid AND ir.id = irr.integration_result_id )
					WHERE
						ir.integration_service_id = ' . ( int ) $intIntegrationServiceId . '
						AND irr.reference_number =' . ( int ) $intReferenceNumber . '
						AND ir.cid = ' . ( int ) $intCid . '
					ORDER BY ir.created_on DESC LIMIT 1;';

		return self::fetchIntegrationResult( $strSql, $objDatabase );
	}

	public static function fetchCustomPaginatedIntegrationResults( $intPageNo, $intPageSize, $boolConsiderTime, $objAdminDatabase, $objIntegrationResultsFilter = NULL, $boolIsDownload = false ) {

		$intOffset			= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit			= ( int ) $intPageSize;
		$strInnerSql		= NULL;
		$strWhereSql		= NULL;
		$strInnerTwoSql		= NULL;
		$strSqlCondition	= NULL;

		$strSql	= 'SELECT
						DISTINCT ON( ir.id, ir.cid )
						ir.id,
						ir.cid,
						CASE WHEN ir.property_id IS NOT NULL THEN ir.property_id ELSE irr.property_id END as property_id,
						ir.web_method_id,
						ir.service_id,
						ir.integration_client_id,
						ir.integration_service_id,
						ir.integration_queue_id,
						ir.error_message,
						CASE WHEN irr.reference_number IS NOT NULL THEN irr.reference_number ELSE ir.reference_number END as reference_number,
						irr.property_ids,
						irr.reference_numbers,
						CASE WHEN irr.property_names IS NOT NULL THEN irr.property_names ELSE p.property_name END AS property_names,
						ir.result_datetime,
						ir.integration_utility_version,
						ir.remote_processing_time,
						ir.processing_time,
						ir.is_failed,
						ir.use_amazon_file_storage,
						ir.updated_by,
						ir.updated_on,
						ir.created_by,
						ir.created_on,
						ir.extra_info
					FROM integration_results AS ir
						LEFT JOIN LATERAL( SELECT
							irr.cid,
							irr.property_id,
							irr.integration_result_id,
							irr.reference_number,
							string_agg( irr.property_id::varchar, \',\') OVER (PARTITION BY irr.cid, irr.integration_result_id ) AS property_ids,
							string_agg( irr.reference_number::varchar, \',\') OVER (PARTITION BY irr.cid, irr.integration_result_id ) AS reference_numbers,
							string_agg( p.property_name::varchar, \',\') OVER (PARTITION BY irr.cid, irr.integration_result_id ) AS property_names
						FROM
							integration_result_references irr
							LEFT JOIN properties p ON( p.cid = irr.cid AND p.id = irr.property_id )
						WHERE TRUE
							' . ( ( false == is_null( $objIntegrationResultsFilter->getCid() ) ) ? ' AND irr.cid = ' . $objIntegrationResultsFilter->getCid() : '' ) . '
							AND ir.id = irr.integration_result_id
							AND ir.cid = irr.cid
							' . ( ( false == is_null( $objIntegrationResultsFilter->getPropertyId() ) ) ? ' AND ( ir.property_id = ' . $objIntegrationResultsFilter->getPropertyId() . ' OR irr.property_id = ' . $objIntegrationResultsFilter->getPropertyId() . ' ) ' : '' ) . '
						) irr ON( ir.id = irr.integration_result_id AND ir.cid = irr.cid )
						LEFT JOIN properties p ON( p.cid = ir.cid AND p.id = ir.property_id )';

			if( true == valObj( $objIntegrationResultsFilter, 'CIntegrationResultsFilter' ) ) {

				$arrstrAndSearchParameters = self::fetchSearchCriteria( $boolConsiderTime, $objIntegrationResultsFilter );

				if( false == is_null( $objIntegrationResultsFilter->getIntegrationClientTypeIds() ) ) {
					$strInnerSql	.= ' LEFT OUTER JOIN integration_clients ic ON( ic.id = ir.integration_client_id AND ic.cid = ir.cid ) ';
				}

				if( false == is_null( $objIntegrationResultsFilter->getIntegrationDatabaseId() ) ) {
					if( false == valStr( $strInnerSql ) ) {
						$strInnerSql	.= ' LEFT OUTER JOIN integration_clients ic ON( ic.id = ir.integration_client_id AND ic.cid = ir.cid ) ';
					}

					$strWhereSql 	.= ' AND ic.integration_database_id = ' . $objIntegrationResultsFilter->getIntegrationDatabaseId();
				}

				if( false == is_null( $objIntegrationResultsFilter->getIntegrationSyncTypeIds() ) ) {
					if( false == is_null( $strInnerSql ) ) {
						$strInnerSql	.= ', integration_sync_types AS ist ';
					} else {
						$strInnerSql	.= ', integration_clients AS ic, integration_sync_types AS ist ';
					}

					$strWhereSql .= ' ic.integration_sync_type_id = ist.id ';
				}

				if( false == is_null( $objIntegrationResultsFilter->getIntegrationServiceIds() ) ) {
                    if( true == in_array( CIntegrationService::RETRIEVE_RENEWING_CUSTOMERS, explode( ',', $objIntegrationResultsFilter->getIntegrationServiceIds() ) ) ) {
                        $arrintServiceIds = array_merge( explode( ',', $objIntegrationResultsFilter->getIntegrationServiceIds() ), array( CIntegrationService::RETRIEVE_LEASE_OFFERS, CIntegrationService::RETRIEVE_RENEWAL_LEASE_CHARGES ) );
                        $objIntegrationResultsFilter->setIntegrationServiceIds( implode( ',', $arrintServiceIds ) );
                        $strInnerSql	.= ', integration_services AS wis  ';
                        $strWhereSql 	.= ' AND ir.integration_service_id = wis.id  ';
                    } else {
					    $strInnerSql	.= ', integration_services AS wis  ';
					    $strWhereSql 	.= ' AND ir.integration_service_id = wis.id  ';
                    }
				}

				if( false == is_null( $objIntegrationResultsFilter->getServicesIds() ) ) {
					$strInnerSql	.= ', services AS s  ';
					$strWhereSql 	.= ' AND ir.service_id = s.id  ';
				}

				if( false == is_null( $objIntegrationResultsFilter->getPropertyIds() ) ) {
					$strInnerSql	.= ', properties AS pr  ';
					$strWhereSql 	.= ' AND ir.property_id = pr.id  ';
				}

				if( false == is_null( $objIntegrationResultsFilter->getClientIds() ) ) {
					$strInnerSql	.= ', clients AS c  ';
					$strWhereSql 	.= ' AND ir.cid = c.id ';
				}

				if( false == is_null( $objIntegrationResultsFilter->getIntegrationClientTypeIds() ) )
					$strWhereSql .= ' AND ic.integration_client_type_id IS NOT NULL AND ic.integration_client_type_id IN( ' . $objIntegrationResultsFilter->getIntegrationClientTypeIds() . ')';
				if( false == is_null( $objIntegrationResultsFilter->getIntegrationSyncTypeIds() ) )
					$strWhereSql .= ' AND ist.id in ( ' . $objIntegrationResultsFilter->getIntegrationSyncTypeIds() . ')';
				if( false == is_null( $objIntegrationResultsFilter->getIntegrationServiceIds() ) )
                    $strWhereSql .= ' AND wis.id in ( ' . $objIntegrationResultsFilter->getIntegrationServiceIds() . ')';
				if( false == is_null( $objIntegrationResultsFilter->getServicesIds() ) )
					$strWhereSql .= ' AND ir.service_id in ( ' . $objIntegrationResultsFilter->getServicesIds() . ')';
				if( false == is_null( $objIntegrationResultsFilter->getPropertyIds() ) )
					$strWhereSql .= ' AND ir.property_id in ( ' . $objIntegrationResultsFilter->getPropertyIds() . ')';
				if( false == is_null( $objIntegrationResultsFilter->getClientIds() ) )
					$strWhereSql .= ' AND ir.cid in ( ' . $objIntegrationResultsFilter->getClientIds() . ')';

				if( false == is_null( $strInnerSql ) ) {
					$strSql	.= $strInnerSql . ' WHERE TRUE ' . $strWhereSql . ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );
				} else {
					$strSql	.= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' WHERE ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );
				}
			}

		$strInnerTwoSql	= 'SELECT
								DISTINCT ON( ir.id, ir.cid )
								ir.id,
								ir.cid,
								CASE WHEN ir.property_id IS NOT NULL THEN ir.property_id ELSE irr.property_id END as property_id,
								ir.web_method_id,
								ir.service_id,
								ir.integration_client_id,
								ir.integration_service_id,
								ir.integration_queue_id,
								ir.error_message,
								CASE WHEN irr.reference_number IS NOT NULL THEN irr.reference_number ELSE ir.reference_number END as reference_number,
								irr.property_ids,
								irr.reference_numbers,
								CASE WHEN irr.property_names IS NOT NULL THEN irr.property_names ELSE p.property_name END AS property_names,
								ir.result_datetime,
								ir.integration_utility_version,
								ir.remote_processing_time,
								ir.processing_time,
								ir.is_failed,
								ir.use_amazon_file_storage,
								ir.updated_by,
								ir.updated_on,
								ir.created_by,
								ir.created_on,
								ir.extra_info
							FROM integration_results AS ir
								LEFT JOIN LATERAL( SELECT
									irr.cid,
									irr.property_id,
									irr.integration_result_id,
									irr.reference_number,
									string_agg( irr.property_id::varchar, \',\') OVER (PARTITION BY irr.cid, irr.integration_result_id ) AS property_ids,
									string_agg( irr.reference_number::varchar, \',\') OVER (PARTITION BY irr.cid, irr.integration_result_id ) AS reference_numbers,
									string_agg( p.property_name::varchar, \',\') OVER (PARTITION BY irr.cid, irr.integration_result_id ) AS property_names
								FROM
									integration_result_references irr
									LEFT JOIN properties p ON( p.cid = irr.cid AND p.id = irr.property_id )
								WHERE TRUE
									' . ( ( false == is_null( $objIntegrationResultsFilter->getCid() ) ) ? ' AND irr.cid = ' . $objIntegrationResultsFilter->getCid() : '' ) . '
									AND ir.id = irr.integration_result_id
									AND ir.cid = irr.cid
									' . ( ( false == is_null( $objIntegrationResultsFilter->getPropertyId() ) ) ? ' AND ( ir.property_id = ' . $objIntegrationResultsFilter->getPropertyId() . ' OR irr.property_id = ' . $objIntegrationResultsFilter->getPropertyId() . ' ) ' : '' ) . '
								) irr ON( ir.id = irr.integration_result_id AND ir.cid = irr.cid )
								LEFT JOIN properties p ON( p.cid = ir.cid AND p.id = ir.property_id )';

			if( false == is_null( $strInnerSql ) ) {

					if( true == \Psi\CStringService::singleton()->strstr( $objIntegrationResultsFilter->getIntegrationClientTypeIds(), '11' ) && true == is_null( $objIntegrationResultsFilter->getWebMethodsIds() ) ) {

						$strInnerTwoSql 	.= 'WHERE ir.web_method_id IS NOT NULL';
						$strSqlCondition	= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );
						$strSql .= ' UNION  ' . $strInnerTwoSql . $strSqlCondition;

					} elseif( true == \Psi\CStringService::singleton()->strstr( $objIntegrationResultsFilter->getIntegrationClientTypeIds(), '11' ) && false == is_null( $objIntegrationResultsFilter->getWebMethodsIds() ) ) {

						$strInnerTwoSql		.= ' LEFT JOIN web_methods wm ON( wm.id = ir.web_method_id ) WHERE wm.id IN ( ' . $objIntegrationResultsFilter->getWebMethodsIds() . ' ) ';
						$strSqlCondition	= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );
						$strSql .= ' UNION  ' . $strInnerTwoSql . $strSqlCondition;

					} elseif( true == is_null( $objIntegrationResultsFilter->getIntegrationClientTypeIds() ) && false == is_null( $objIntegrationResultsFilter->getWebMethodsIds() ) ) {

						$strInnerTwoSql		.= ' LEFT JOIN web_methods wm ON( wm.id = ir.web_method_id ) WHERE wm.id IN ( ' . $objIntegrationResultsFilter->getWebMethodsIds() . ' ) ';
						$strSqlCondition	= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );
						$strSql .= ' UNION  ' . $strInnerTwoSql . $strSqlCondition;
					}

			} elseif( false == is_null( $objIntegrationResultsFilter->getWebMethodsIds() ) ) {

					$strInnerTwoSql		.= ' LEFT JOIN web_methods wm ON( wm.id = ir.web_method_id ) WHERE wm.id IN ( ' . $objIntegrationResultsFilter->getWebMethodsIds() . ' ) ';
					$strSqlCondition	= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );
					$strSql = $strInnerTwoSql . $strSqlCondition;
			}

			if( false == $boolIsDownload ) {
				$strSql .= ' ORDER BY id DESC ' . ' OFFSET ' . ( int ) $intOffset . '	LIMIT ' . ( int ) $intLimit;
			} else {
				$strSql .= ' ORDER BY id DESC ';
			}

		return self::fetchIntegrationResults( $strSql, $objAdminDatabase );
	}

	public static function fetchCustomPaginatedIntegrationResultsCount( $boolConsiderTime, $objAdminDatabase, $objIntegrationResultsFilter ) {

		$strSql	= 'SELECT
						count( DISTINCT ir.id )
					FROM
						integration_results AS ir
						LEFT JOIN integration_result_references irr ON( ir.id = irr.integration_result_id AND ir.cid = irr.cid )';

		$strInnerSql 	= NULL;
		$strWhereSql 	= NULL;
		$strInnerTwoSql	= NULL;

		if( true == valObj( $objIntegrationResultsFilter, 'CIntegrationResultsFilter' ) ) {

			$arrstrAndSearchParameters = self::fetchSearchCriteria( $boolConsiderTime, $objIntegrationResultsFilter );

			if( false == is_null( $objIntegrationResultsFilter->getIntegrationClientTypeIds() ) ) {
				$strInnerSql	.= ' LEFT OUTER JOIN integration_clients ic ON( ic.id = ir.integration_client_id AND ic.cid = ir.cid) ';
			}

			if( false == is_null( $objIntegrationResultsFilter->getIntegrationDatabaseId() ) ) {
				if( false == valStr( $strInnerSql ) ) {
					$strInnerSql	.= ' LEFT OUTER JOIN integration_clients ic ON( ic.id = ir.integration_client_id AND ic.cid = ir.cid) ';
				}
				$strWhereSql 	.= ' AND ic.integration_database_id = ' . $objIntegrationResultsFilter->getIntegrationDatabaseId();

			}

			if( false == is_null( $objIntegrationResultsFilter->getIntegrationSyncTypeIds() ) ) {
				if( false == is_null( $strInnerSql ) ) {
					$strInnerSql	.= ', integration_sync_types AS ist ';
				} else {
					$strInnerSql	.= ', integration_clients AS ic, integration_sync_types AS ist ';
				}

				$strWhereSql .= ' ic.integration_sync_type_id = ist.id';
			}

            if( false == is_null( $objIntegrationResultsFilter->getIntegrationServiceIds() ) ) {
                if( true == in_array( CIntegrationService::RETRIEVE_RENEWING_CUSTOMERS, explode( ',', $objIntegrationResultsFilter->getIntegrationServiceIds() ) ) ) {
                    $arrintServiceIds = array_merge( explode( ',', $objIntegrationResultsFilter->getIntegrationServiceIds() ), array( CIntegrationService::RETRIEVE_LEASE_OFFERS, CIntegrationService::RETRIEVE_RENEWAL_LEASE_CHARGES ) );
                    $objIntegrationResultsFilter->setIntegrationServiceIds( implode( ',', $arrintServiceIds ) );
                    $strInnerSql	.= ', integration_services AS wis  ';
                    $strWhereSql 	.= ' AND ir.integration_service_id = wis.id  ';
                } else {
                    $strInnerSql	.= ', integration_services AS wis  ';
                    $strWhereSql 	.= ' AND ir.integration_service_id = wis.id  ';
                }
            }

			if( false == is_null( $objIntegrationResultsFilter->getPropertyIds() ) ) {
				$strInnerSql	.= ', properties AS p ';
				$strWhereSql 	.= ' AND ir.property_id = p.id  ';
			}

			if( false == is_null( $objIntegrationResultsFilter->getClientIds() ) ) {
				$strInnerSql	.= ', clients AS c ';
				$strWhereSql 	.= ' AND ir.cid = c.id  ';
			}

			if( false == is_null( $objIntegrationResultsFilter->getServicesIds() ) ) {
				$strInnerSql	.= ', services AS s ';
				$strWhereSql 	.= ' AND ir.service_id = s.id  ';
			}

			if( false == is_null( $objIntegrationResultsFilter->getIntegrationClientTypeIds() ) ) 	$strWhereSql .= ' AND ic.integration_client_type_id IS NOT NULL AND ic.integration_client_type_id IN( ' . $objIntegrationResultsFilter->getIntegrationClientTypeIds() . ')';
			if( false == is_null( $objIntegrationResultsFilter->getIntegrationSyncTypeIds() ) ) 	$strWhereSql .= ' AND ist.id in ( ' . $objIntegrationResultsFilter->getIntegrationSyncTypeIds() . ')';
			if( false == is_null( $objIntegrationResultsFilter->getIntegrationServiceIds() ) ) 	$strWhereSql .= ' AND wis.id in ( ' . $objIntegrationResultsFilter->getIntegrationServiceIds() . ')';
			if( false == is_null( $objIntegrationResultsFilter->getPropertyIds() ) ) 	$strWhereSql .= ' AND p.id in ( ' . $objIntegrationResultsFilter->getPropertyIds() . ')';
			if( false == is_null( $objIntegrationResultsFilter->getClientIds() ) ) 	$strWhereSql .= ' AND c.id in ( ' . $objIntegrationResultsFilter->getClientIds() . ')';
			if( false == is_null( $objIntegrationResultsFilter->getServicesIds() ) ) 	$strWhereSql .= ' AND s.id in ( ' . $objIntegrationResultsFilter->getServicesIds() . ')';

			if( false == is_null( $strInnerSql ) ) {
				$strSql	.= $strInnerSql . ' WHERE TRUE ' . $strWhereSql . ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );
			} else {
				$strSql	.= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' WHERE ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );
			}
		}

		$strInnerTwoSql	= 'SELECT
								count( DISTINCT ir.id )
							FROM
								integration_results AS ir
								LEFT JOIN integration_result_references irr ON( ir.id = irr.integration_result_id AND ir.cid = irr.cid ) ';

		if( false == is_null( $strInnerSql ) ) {

				if( true == \Psi\CStringService::singleton()->strstr( $objIntegrationResultsFilter->getIntegrationClientTypeIds(), '11' ) && true == is_null( $objIntegrationResultsFilter->getWebMethodsIds() ) ) {

					$strInnerTwoSql .= 'WHERE ir.web_method_id IS NOT NULL';
					$strSqlCondition	= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );
					$strSql .= ' UNION  ' . $strInnerTwoSql . $strSqlCondition;

				} elseif( true == \Psi\CStringService::singleton()->strstr( $objIntegrationResultsFilter->getIntegrationClientTypeIds(), '11' ) && false == is_null( $objIntegrationResultsFilter->getWebMethodsIds() ) ) {

					$strInnerTwoSql	.= ' LEFT JOIN web_methods wm ON( wm.id = ir.web_method_id ) WHERE wm.id IN ( ' . $objIntegrationResultsFilter->getWebMethodsIds() . ' ) ';
					$strSqlCondition	= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );
					$strSql .= ' UNION  ' . $strInnerTwoSql . $strSqlCondition;

				} elseif( true == is_null( $objIntegrationResultsFilter->getIntegrationClientTypeIds() ) && false == is_null( $objIntegrationResultsFilter->getWebMethodsIds() ) ) {

					$strInnerTwoSql	.= ' LEFT JOIN web_methods wm ON( wm.id = ir.web_method_id ) WHERE wm.id IN ( ' . $objIntegrationResultsFilter->getWebMethodsIds() . ' ) ';
					$strSqlCondition	= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );
					$strSql .= ' UNION  ' . $strInnerTwoSql . $strSqlCondition;
				}

		} elseif( false == is_null( $objIntegrationResultsFilter->getWebMethodsIds() ) ) {

				$strInnerTwoSql	.= ' LEFT JOIN web_methods wm ON( wm.id = ir.web_method_id ) WHERE wm.id IN ( ' . $objIntegrationResultsFilter->getWebMethodsIds() . ' ) ';
				$strSqlCondition	= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );
				$strSql = $strInnerTwoSql . $strSqlCondition;
		}

		$arrstrData = fetchData( $strSql, $objAdminDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchSearchCriteria( $boolConsiderTime, $objIntegrationResultsFilter ) {

		$arrstrAndSearchParameters 	= [];
		$arrstrOutput				= [];
		$strApplicationCondition			= '';
		$strTransmissionCondition			= '';
		$strLeaseCustomerCondition			= '';
		$strEventReferenceCondition			= '';
		$strMaintenanceRequestCondition		= '';

		// Create SQL parameters.
		if( false == is_null( $objIntegrationResultsFilter->getCid() ) ) {
			$arrstrAndSearchParameters[] 		= ' ir.cid 				= ' . $objIntegrationResultsFilter->getCid() . ' ';
			$strApplicationCondition			= ' applications.cid 		= ' . $objIntegrationResultsFilter->getCid() . ' AND ';
			$strTransmissionCondition			= ' transmissions.cid 	= ' . $objIntegrationResultsFilter->getCid() . ' AND ';
			$strLeaseCustomerCondition 			= ' lc.cid 				= ' . $objIntegrationResultsFilter->getCid() . ' AND ';
			$strEventReferenceCondition			= ' event_references.cid 	= ' . $objIntegrationResultsFilter->getCid() . ' AND ';
			$strMaintenanceRequestCondition 	= ' mr.cid				= ' . $objIntegrationResultsFilter->getCid() . ' AND ';

			$strPathSearch	 = getNonBackupMountsPath( $objIntegrationResultsFilter->getCid(), PATH_MOUNTS_INTEGRATION_RESULTS );
		}

		if( false == is_null( $objIntegrationResultsFilter->getId() ) ) 						$arrstrAndSearchParameters[] = ' ir.id = ' . ( int ) $objIntegrationResultsFilter->getId() . '';
		if( false == is_null( $objIntegrationResultsFilter->getPropertyId() ) ) 				$arrstrAndSearchParameters[] = ' ( ir.property_id = ' . ( int ) $objIntegrationResultsFilter->getPropertyId() . ' OR irr.property_id = ' . ( int ) $objIntegrationResultsFilter->getPropertyId() . ' )';
		if( false == is_null( $objIntegrationResultsFilter->getUserId() ) ) 					$arrstrAndSearchParameters[] = ' ir.created_by = ' . ( int ) $objIntegrationResultsFilter->getUserId() . ' ';
		if( false == is_null( $objIntegrationResultsFilter->getReferenceNumber() ) ) 			$arrstrAndSearchParameters[] = ' ( ir.reference_number = ' . ( int ) $objIntegrationResultsFilter->getReferenceNumber() .
																																   ' OR irr.reference_number = ' . ( int ) $objIntegrationResultsFilter->getReferenceNumber() .
																																   ' OR ( irr.reference_number IN ( SELECT lease_id FROM applications WHERE ' . $strApplicationCondition . ' id = ' . ( int ) $objIntegrationResultsFilter->getReferenceNumber() . ' ) AND ir.integration_service_id != ' . CIntegrationService::RETRIEVE_AR_TRANSACTIONS . ' )
																																   	 OR irr.reference_number IN ( SELECT id FROM transmissions WHERE ' . $strTransmissionCondition . ' application_id = ' . ( int ) $objIntegrationResultsFilter->getReferenceNumber() . ' )
																																   	 OR ( irr.reference_number IN ( SELECT lc.customer_id FROM lease_customers lc INNER JOIN applications a ON ( lc.lease_id = a.lease_id AND lc.cid = a.cid ) WHERE ' . $strLeaseCustomerCondition . ' a.id = ' . ( int ) $objIntegrationResultsFilter->getReferenceNumber() . ' ) AND ir.integration_service_id = ' . CIntegrationService::UPDATE_CUSTOMER . ' )
																																   	 OR irr.reference_number IN ( SELECT mr.id FROM maintenance_requests mr WHERE ' . $strMaintenanceRequestCondition . ' mr.customer_id = ' . ( int ) $objIntegrationResultsFilter->getReferenceNumber() . ' )
																																	 OR ( irr.reference_number IN ( SELECT lc.lease_id FROM lease_customers lc WHERE ' . $strLeaseCustomerCondition . ' lc.customer_id = ' . ( int ) $objIntegrationResultsFilter->getReferenceNumber() . ' ) AND ir.integration_service_id = ' . CIntegrationService::SEND_CUSTOMERS_ADDL_DATA . ' )
																																	 OR irr.reference_number IN ( SELECT e.id FROM events e JOIN applications ON ( e.lease_interval_id = applications.lease_interval_id ) WHERE ' . $strApplicationCondition . ' applications.id = ' . ( int ) $objIntegrationResultsFilter->getReferenceNumber() . ' )
																																   	  )';

		$arrstrFromDateTime = $objIntegrationResultsFilter->getFromDateTime();
		$arrstrFromDateTime = explode( ' ', $arrstrFromDateTime );

		if( $boolConsiderTime && true == valStr( $objIntegrationResultsFilter->getFromDateTime() ) && '::00' != $arrstrFromDateTime[1] ) {
			if( false == is_null( $objIntegrationResultsFilter->getFromDateTime() ) ) {
				$arrstrAndSearchParameters[] = ' ir.result_datetime >= \'' . $objIntegrationResultsFilter->getFromDateTime() . ' \'';
			}
		} else {
			if( false == is_null( $objIntegrationResultsFilter->getFromDate() ) ) {
				$arrstrAndSearchParameters[] = ' ir.result_datetime >= \'' . date( 'Y-m-d', strtotime( $objIntegrationResultsFilter->getFromDate() ) ) . ' 00:00:00\'';
			}
		}

		$arrstrToDateTime = $objIntegrationResultsFilter->getToDateTime();
		$arrstrToDateTime = explode( ' ', $arrstrToDateTime );
		if( $boolConsiderTime && true == valStr( $objIntegrationResultsFilter->getToDateTime() ) && '::59' != $arrstrToDateTime[1] ) {
			if( false == is_null( $objIntegrationResultsFilter->getToDateTime() ) ) {
				$arrstrAndSearchParameters[] = ' ir.result_datetime <= \'' . $objIntegrationResultsFilter->getToDateTime() . ' \'';
			}
		} else {
			if( false == is_null( $objIntegrationResultsFilter->getToDate() ) ) {
				$arrstrAndSearchParameters[] = ' ir.result_datetime <= \'' . date( 'Y-m-d', strtotime( $objIntegrationResultsFilter->getToDate() ) ) . ' 23:59:59\'';
			}
		}

		if( false == is_null( $objIntegrationResultsFilter->getIsFailed() ) ) 					$arrstrAndSearchParameters[] = ' ir.is_failed=' . ( int ) $objIntegrationResultsFilter->getIsFailed();

		if( false == is_null( $objIntegrationResultsFilter->getErrorMessage() ) && 0 < strlen( trim( $objIntegrationResultsFilter->getErrorMessage() ) ) ) {
			$arrstrAndSearchParameters[] = ' ir.error_message ILIKE \'%' . addslashes( $objIntegrationResultsFilter->getErrorMessage() ) . '%\'';
		}

		if( false == is_null( $objIntegrationResultsFilter->getContentText() ) && 0 < strlen( trim( $objIntegrationResultsFilter->getContentText() ) ) ) {
			@exec( 'grep -r \'' . $objIntegrationResultsFilter->getContentText() . '\'' . $strPathSearch . '*', $arrstrOutput );

			$arrintAdditionalIds = [];

			if( true == valArr( $arrstrOutput ) ) {
				foreach( $arrstrOutput as $strOutput ) {
					$arrstrData = explode( ':', $strOutput );
					if( true == isset ( $arrstrData[0] ) ) {
						$arrstrPathInfo = pathinfo( $arrstrData[0] );
						$arrstrFileNameParts = explode( '_', $arrstrPathInfo['filename'] );
						if( true == isset ( $arrstrFileNameParts[0] ) && true == is_numeric( trim( $arrstrFileNameParts[0] ) ) ) {
							$arrintAdditionalIds[] = trim( $arrstrFileNameParts[0] );
						}
					}
				}

				if( true == valArr( $arrintAdditionalIds ) ) {
					$arrstrAndSearchParameters[] = ' ir.id IN ( ' . implode( ',', $arrintAdditionalIds ) . ' ) ';
				}
			}
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) {
			return $arrstrAndSearchParameters;
		} else {
			return NULL;
		}
	}

	public static function fetchIntegrationResultsByIntegrationServiceIdsByReferenceNumberByCid( $arrintIntegrationServiceIds, $intReferenceNumber, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ir.*,
						MIN( COALESCE( ir.reference_number, irr.reference_number ) ) OVER( PARTITION BY ir.id, ir.cid ) as reference_number
					FROM
						integration_results ir
						LEFT JOIN integration_result_references irr ON( ir.cid = irr.cid AND ir.id = irr.integration_result_id )
					WHERE
						ir.cid = ' . ( int ) $intCid . '
						AND ir.integration_service_id IN ( ' . implode( ',', $arrintIntegrationServiceIds ) . ' )
						AND ( ir.reference_number = ' . ( int ) $intReferenceNumber . ' OR irr.reference_number = ' . ( int ) $intReferenceNumber . ' )
					ORDER BY ir.created_on DESC, ir.id DESC';

		return self::fetchIntegrationResults( $strSql, $objDatabase );
	}

	public static function fetchLastSuccessIntegrationResultByPropertyIdByIntegrationServiceIdByResultdateByCid( $intPropertyId, $intIntegrationServiceId, $strResultDate, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ir.*,
						MIN(irr.reference_number ) OVER( PARTITION BY ir.id, ir.cid ) as reference_number,
						string_agg( irr.reference_number::varchar, \',\') OVER (PARTITION BY ir.id, ir.cid ) AS reference_numbers
					FROM
						integration_results ir
						LEFT JOIN integration_result_references irr ON( ir.id = irr.integration_result_id AND ir.cid = irr.cid )
					WHERE
						ir.cid = ' . ( int ) $intCid . '
						AND ir.property_id = ' . ( int ) $intPropertyId . '
						AND ir.integration_service_id = ' . ( int ) $intIntegrationServiceId . '
						AND date( ir.result_datetime ) = \'' . $strResultDate . '\'
						AND ( ir.is_failed IS NULL OR ir.is_failed = 0 )
					ORDER BY ir.created_on DESC
					LIMIT 1';

		return self::fetchIntegrationResult( $strSql, $objDatabase );
	}

	public static function fetchCustomIntegrationResultsCountsForServicesByDays( $strFromDate, $strToDate, $objDatabase ) {

		$strSql = 'SELECT
						COUNT ( id ) AS total_count,
							COUNT ( id ) - SUM ( is_failed ) AS success_count,
						SUM ( is_failed ) AS failed_count,
						date( created_on ) as days_interval
					FROM
						integration_results
					WHERE
						cid IS NOT NULL
						AND integration_service_id IS NOT NULL
						AND created_on >= \' ' . $strFromDate . '\'
					 	AND created_on <= \' ' . $strToDate . '\'
					GROUP BY
						date( created_on )';

		$arrintIntegrationResults = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintIntegrationResults ) ) ? $arrintIntegrationResults : NULL;
	}

	public static function fetchCustomIntegrationResultsCountsForServicesByDaysByCids( $strFromDate, $strToDate, $arrintCids, $objDatabase ) {

		$strSql = 'SELECT
						COUNT ( id ) AS total_count,
							COUNT ( id ) - SUM ( is_failed ) AS success_count,
						SUM ( is_failed ) AS failed_count,
						date( created_on ) as days_interval
					FROM
						integration_results
					WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND integration_service_id IS NOT NULL
						AND created_on::DATE >= \' ' . $strFromDate . '\'
					 	AND created_on::DATE <= \' ' . $strToDate . '\'
					GROUP BY
						date( created_on )';
		$arrintIntegrationResults = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintIntegrationResults ) ) ? $arrintIntegrationResults : NULL;
	}

	public static function fetchCustomTotalCountIntegrationResultByFromDateByToDate( $strFromDate, $strToDate, $objDatabase ) {

		if( false == valStr( $strFromDate ) || false == valStr( $strToDate ) ) return NULL;

		$strSql = 'SELECT
						count ( ir.id ),
						integration_service_id,
						iss.name
					FROM
						integration_results ir
						JOIN integration_services iss on (ir.integration_service_id = iss.id  )
					WHERE
						ir.created_on >= \' ' . $strFromDate . '\'
						AND ir.created_on <= \' ' . $strToDate . '\'
						AND ir.cid IS NOT NULL
						AND ir.integration_service_id IS NOT NULL
					GROUP BY
						ir.integration_service_id, iss.name
					HAVING
						ir.integration_service_id IS NOT NULL
					ORDER BY
						count ( ir.id ) desc';

		$arrintIntegrationResults = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintIntegrationResults ) ) ? $arrintIntegrationResults : NULL;
	}

	public static function fetchCustomTotalCountIntegrationResultByFromDateByToDateByCids( $strFromDate, $strToDate, $arrintCids, $objDatabase ) {

		if( false == valStr( $strFromDate ) || false == valStr( $strToDate ) ) return NULL;

		$strSql = 'SELECT
						count ( ir.id ),
						integration_service_id,
						iss.name,
						cid
					FROM
						integration_results ir
						JOIN integration_services iss on (ir.integration_service_id = iss.id  )
					WHERE
						ir.created_on::DATE >= \' ' . $strFromDate . '\'
						AND ir.created_on::DATE <= \' ' . $strToDate . '\'
						AND ir.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND ir.integration_service_id IS NOT NULL
					GROUP BY
						ir.integration_service_id, iss.name,ir.cid
					HAVING
						ir.integration_service_id IS NOT NULL
					ORDER BY
						count ( ir.id ) desc';

		$arrintIntegrationResults = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintIntegrationResults ) ) ? $arrintIntegrationResults : NULL;
	}

	public static function fetchCustomTotalCountIntegrationResultByFromDateByToDateDatewise( $strFromDate, $strToDate, $objDatabase ) {

		if( false == valStr( $strFromDate ) || false == valStr( $strToDate ) ) return NULL;

		$strSql = 'SELECT
						count ( ir.id ),
						date(result_datetime)
					FROM
						integration_results ir
					WHERE
						ir.created_on >= \' ' . $strFromDate . '\'
						AND ir.created_on <= \' ' . $strToDate . '\'
						AND ir.cid IS NOT NULL
						AND ir.integration_service_id IS NOT NULL
					GROUP BY
						date(ir.result_datetime)
					ORDER BY
						count ( ir.id ) desc';

		$arrintIntegrationResults = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintIntegrationResults ) ) ? $arrintIntegrationResults : NULL;
	}

	public static function fetchCustomTotalCountIntegrationResultByFromDateByToDateDatewiseByCids( $strFromDate, $strToDate, $arrintCids, $objDatabase ) {

		if( false == valStr( $strFromDate ) || false == valStr( $strToDate ) ) return NULL;

		$strSql = 'SELECT
						count ( ir.id ),
						date(result_datetime)
					FROM
						integration_results ir
					WHERE
						ir.created_on::DATE >= \' ' . $strFromDate . '\'
						AND ir.created_on::DATE <= \' ' . $strToDate . '\'
						AND ir.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND ir.integration_service_id IS NOT NULL
					GROUP BY
						date(ir.result_datetime)
					ORDER BY
						count ( ir.id ) desc';

		$arrintIntegrationResults = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintIntegrationResults ) ) ? $arrintIntegrationResults : NULL;
	}

	public static function fetchCustomMaxIntegrationResultRemoteProcessingByFromDateByToDateServicewise( $strFromDate, $strToDate, $objDatabase ) {

		if( false == valStr( $strFromDate ) || false == valStr( $strToDate ) ) return NULL;

		$strSql = 'SELECT
						sum ( ir.remote_processing_time ),
						ir.integration_service_id,
						iss.name,
						count( ir.id) as cnt_service,
						sum ( ir.remote_processing_time ) / count( ir.id) as avg_processtime
					FROM
						integration_results ir
						JOIN integration_services iss on (ir.integration_service_id = iss.id  )
					WHERE
						ir.created_on >= \' ' . $strFromDate . '\'
						AND ir.created_on <= \' ' . $strToDate . '\'
						AND ir.cid IS NOT NULL
						AND ir.remote_processing_time IS NOT NULL
					GROUP BY
						ir.integration_service_id, iss.name
					HAVING
						ir.integration_service_id IS NOT NULL
					ORDER BY
						max ( ir.remote_processing_time ) desc';

		$arrintIntegrationResults = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintIntegrationResults ) ) ? $arrintIntegrationResults : NULL;
	}

	public static function fetchCustomMaxIntegrationResultRemoteProcessingByFromDateByToDateServicewiseByCids( $strFromDate, $strToDate, $arrintCids, $objDatabase ) {

		if( false == valStr( $strFromDate ) || false == valStr( $strToDate ) ) return NULL;

		$strSql = 'SELECT
						sum ( ir.remote_processing_time ),
						ir.integration_service_id,
						iss.name,
						count( ir.id) as cnt_service,
						sum ( ir.remote_processing_time ) / count( ir.id) as avg_processtime
					FROM
						integration_results ir
						JOIN integration_services iss on (ir.integration_service_id = iss.id  )
					WHERE
						ir.created_on::DATE >= \' ' . $strFromDate . '\'
						AND ir.created_on::DATE <= \' ' . $strToDate . '\'
						AND ir.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND ir.remote_processing_time IS NOT NULL
					GROUP BY
						ir.integration_service_id, iss.name
					HAVING
						ir.integration_service_id IS NOT NULL
					ORDER BY
						max ( ir.remote_processing_time ) desc';

		$arrintIntegrationResults = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintIntegrationResults ) ) ? $arrintIntegrationResults : NULL;
	}

	public static function fetchSearchedRecords( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
						ir.id AS id,
						c.company_name AS client_name,
						c.id AS client_id,
						p.property_name AS property_name,
						irr.reference_number AS reference_number
					FROM
						integration_results ir
						LEFT JOIN clients c ON ( c.id = ir.cid )
						LEFT JOIN properties p ON ( p.id = ir.property_id )
						LEFT JOIN integration_result_references irr ON ( ir.id = irr.integration_result_id )
					WHERE
						to_char( ir.id, \'999999999\' ) LIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
						OR to_char( irr.reference_number, \'999999999\' ) LIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
					LIMIT 100';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchIntegrationResultIdsByIdsByCid( $arrintIntegrationResultIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintIntegrationResultIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ir.id
					FROM
						integration_results ir
					WHERE
						ir.id IN ( ' . implode( ',', $arrintIntegrationResultIds ) . ' )
						AND ir.cid = ' . ( int ) $intCid . '
						ORDER BY ir.created_on DESC, ir.id DESC';

		$arrstrResponseData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrResponseData ) ) {

			$arrintIntegrationResultIds = [];
			foreach( $arrstrResponseData as $arrstrResponse ) {
				array_push( $arrintIntegrationResultIds, $arrstrResponse['id'] );
			}

			return $arrintIntegrationResultIds;
		}

		return NULL;
	}

	public static function fetchIntegrationResultByIdsByCid( $arrintIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ir.*,
						id.database_name,
						CASE WHEN irr.id IS NOT NULL
							THEN MIN(irr.reference_number ) OVER( PARTITION BY ir.id, ir.cid )
							ELSE ir.reference_number
						END as reference_number,
						string_agg( irr.reference_number::varchar, \',\') OVER (PARTITION BY ir.id, ir.cid ) AS reference_numbers,
						string_agg( p.property_name::varchar, \',\') OVER (PARTITION BY ir.id, ir.cid ) AS property_names
					FROM
						integration_results ir
						LEFT JOIN integration_clients ic ON ( ir.cid = ic.cid AND ir.integration_client_id = ic.id )
						LEFT JOIN integration_databases id ON ( id.cid = ic.cid AND id.id = ic.integration_database_id )
						LEFT JOIN integration_result_references irr ON( ir.cid = irr.cid AND ir.id = irr.integration_result_id )
						LEFT JOIN properties p ON( p.cid = ir.cid AND ( p.id = irr.property_id OR ir.property_id = p.id ) )
					WHERE
						ir.cid = ' . ( int ) $intCid . '
						AND ir.id IN ( ' . implode( ',', $arrintIds ) . ' ) ';

		return parent::fetchIntegrationResults( $strSql, $objDatabase );
	}

	public static function fetchCustomLastIntegrationResultsByIntegrationServiceIdsByReferenceNumbersByCids( $objDatabase, $intIntegrationServiceIds, $intReferenceNumbers, $intCids ) {

		if( !valArr( $intIntegrationServiceIds ) || !valArr( $intReferenceNumbers ) || !valArr( $intCids ) ) {
			return false;
		}

		$strSql = 'SELECT
				          ordered_data.integration_result_id,
				          ordered_data.cid,
				          ordered_data.integration_service_id,
				          ordered_data.reference_number,
				          ordered_data.reference_numbers
					FROM    
					    (
					      SELECT
					          rank ( ) OVER ( PARTITION BY ir.integration_service_id, irr.reference_number ORDER BY ir.created_on DESC ) AS rank_num,
					          ir.id AS integration_result_id,
					          ir.cid,
					          ir.integration_service_id,
					          MIN ( irr.reference_number ) OVER ( PARTITION BY ir.id, ir.cid ) AS reference_number,
					          string_agg ( irr.reference_number::VARCHAR, \',\' ) OVER ( PARTITION BY ir.id, ir.cid ) AS reference_numbers
					      FROM
					          integration_results ir
					          JOIN integration_result_references irr ON ( ir.cid = irr.cid AND ir.id = irr.integration_result_id )
					      WHERE
					          ir.integration_service_id IN ( ' . implode( ',', $intIntegrationServiceIds ) . ' )
					          AND irr.reference_number IN ( ' . implode( ',', $intReferenceNumbers ) . ' )
					          AND ir.cid IN ( ' . implode( ',', $intCids ) . ' )
					      ORDER BY
					          ir.created_on DESC
					    ) AS ordered_data
					WHERE ordered_data.rank_num = 1';

		$arrmixData = fetchData( $strSql, $objDatabase );

		$arrmixRekeyedData = [];
		foreach( $arrmixData as $mixData ) {
			$arrmixRekeyedData[$mixData['reference_number']][$mixData['integration_service_id']] = $mixData;
		}

		return $arrmixRekeyedData;
	}

}
?>