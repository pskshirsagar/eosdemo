<?php

class CClTemplateOption extends CBaseClTemplateOption {

	const PROPERTY_NAME            = 1;
	const WEBSITE_URL              = 2;
	const AD_DESCRIPTION           = 3;
	const FLOOR_PLAN_DETAILS       = 4;
	const COMMUNITY_AMENITIES      = 5;
	const CONTACT_INFO             = 6;
	const ADA_LOGO                 = 7;
	const EHO_LOGO                 = 8;
	const PHOTO_REMOVING_ALLOWED   = 9;
	const INCLUDE_PROPERTY_IMAGE   = 10;
	const INCLUDE_FLOOR_PLAN_IMAGE = 11;
	const PROPERTY_ADDRESS         = 12;
	const PROPERTY_PHONE_NO        = 13;
	const PROPERTY_EMAIL_ADDRESS   = 14;
	const PET_POLICY               = 15;
	const OFFICE_HOURS             = 16;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>