<?php

class CDefaultPetType extends CBaseDefaultPetType {

	const DOG 					= 1;
	const CAT 					= 2;
	const BIRDS 				= 3;
	const REPTILES 				= 4;
	const FISH 					= 5;
	const SMALL_PET 			= 6;
	const OTHER 				= 7;

}
?>