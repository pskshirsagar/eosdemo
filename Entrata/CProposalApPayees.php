<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CProposalApPayees
 * Do not add any new functions to this class.
 */

class CProposalApPayees extends CBaseProposalApPayees {

	public static function fetchProposalApPayeesByCidByApPayeeContactId( $intCid, $intApPayeeContactId, $objClientDatabase ) {

		$strSql = 'SELECT
						pap.*, pd.name
					FROM
						proposals p
						JOIN proposal_details pd ON ( p.cid = pd.cid AND p.id = pd.proposal_id )
						LEFT JOIN proposal_ap_payees pap ON ( p.cid = pap.cid AND pap.proposal_id = p.id )
					WHERE
						pap.cid = ' . ( int ) $intCid . '
						AND pap.ap_payee_contact_id = ' . ( int ) $intApPayeeContactId;

		return self::fetchProposalApPayees( $strSql, $objClientDatabase );
	}

	public static function fetchProposalApPayeesByProposalIdByCid( $intProposalId, $intCid, $objDatabase ) {

		if( false == valId( $intProposalId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						pap.*,
						ap.company_name
					FROM
						proposal_ap_payees pap
						JOIN ap_payees ap ON ( pap.cid = ap.cid AND pap.ap_payee_id = ap.id )
					WHERE
						pap.cid = ' . ( int ) $intCid . '
						AND pap.proposal_id = ' . ( int ) $intProposalId . '
					ORDER BY 
						pap.id ASC';

		return self::fetchProposalApPayees( $strSql, $objDatabase );

	}

}
?>