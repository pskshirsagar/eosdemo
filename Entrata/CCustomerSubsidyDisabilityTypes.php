<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerSubsidyDisabilityTypes
 * Do not add any new functions to this class.
 */

class CCustomerSubsidyDisabilityTypes extends CBaseCustomerSubsidyDisabilityTypes {

	public static function fetchActiveCustomerSubsidyDisabilityTypesByCustomerIdByCId( $intCustomerId, $intCid, $objDatabase ) {
		if ( false == valId( $intCustomerId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						customer_subsidy_disability_types
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchCustomerSubsidyDisabilityTypes( $strSql, $objDatabase );
	}

	public static function fetchCustomerSubsidyDisabilityStatusByCustomerIdsByCId( $arrintCustomerIds, $intCid, $objDatabase ) {
		if ( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT
						max ( CASE WHEN csdt.subsidy_disability_type_id = ' . \CSubsidyDisabilityType::MOBILITY . ' THEN \'Y\' ELSE \'N\' END )AS mobility_disability,
						max ( CASE WHEN csdt.subsidy_disability_type_id = ' . \CSubsidyDisabilityType::HEARING . ' THEN \'Y\' ELSE \'N\' END )AS hearing_disability,
						max ( CASE WHEN csdt.subsidy_disability_type_id = ' . \CSubsidyDisabilityType::VISUAL . ' THEN \'Y\' ELSE \'N\' END )AS visual_disability
					FROM
						customer_subsidy_disability_types csdt
					WHERE
						csdt.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND csdt.cid = ' . ( int ) $intCid;

		$arrstrSubsidyDisabilityData = fetchData( $strSql, $objDatabase );

		return $arrstrSubsidyDisabilityData[0];
	}

}
?>