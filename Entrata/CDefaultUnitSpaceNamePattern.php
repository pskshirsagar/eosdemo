<?php

class CDefaultUnitSpaceNamePattern extends CBaseDefaultUnitSpaceNamePattern {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitNumberPattern() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpacePattern() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceNameExamples() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>