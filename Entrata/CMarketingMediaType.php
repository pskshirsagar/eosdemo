<?php

class CMarketingMediaType extends CBaseMarketingMediaType {

	const BLOG_MEDIA				= 1;
	const WEBSITE_MEDIA				= 2;
	const PROPERTY_MEDIA			= 3;
	const FLOORPLAN_MEDIA			= 4;
	const RATE_ASSOCIATION_MEDIA	= 5;
	const UNIT_FLOORPLAN_MEDIA		= 6;
	const PROPERTY_BUILDING_MEDIA	= 7;
	const COMPANY_PREFERENCES_DEFAULT_IMAGE	= 8;
	const WEBSITE_TEMPLATE_SLOT_MEDIA	= 9;
	const WEBSITE_CUSTOM_PAGE_MEDIA = 12;
	const BANK_ACCOUNT_MEDIA = 13;

}
?>