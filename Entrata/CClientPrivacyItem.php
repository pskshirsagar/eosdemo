<?php

class CClientPrivacyItem extends CBaseClientPrivacyItem {

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Client privacy item is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrivacyItemId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProductReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valToken( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getToken() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'token', 'Token is required. ' ) );
		} elseif( false == preg_match( '/^[a-zA-Z0-9.\\[\\]_]+$/', $this->getToken() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'token', 'Token contains invalid characters, only [a-zA-Z0-9._[ ] ] are allowed.' ) );
		}

		$intConflictingClientPrivacyItemCount 	= \Psi\Eos\Entrata\CClientPrivacyItems::createService()->fetchClientPrivacyItemsCountByNameByCid( $this->m_intId, $this->m_intCid, $this->sqlToken(), $objDatabase );
		$intConflictingPrivacyItemCount 		= \Psi\Eos\Entrata\CPrivacyItems::createService()->fetchPrivacyItemsCountByName( $this->m_intId, $this->sqlToken(), $objDatabase );

		if( true == valStr( $this->sqlToken() ) && ( 0 < $intConflictingClientPrivacyItemCount || 0 < $intConflictingPrivacyItemCount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Token', 'Token is already exists.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Cookie name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valProductId() {
		$boolIsValid = true;

		if( false == valStr( $this->getProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'product_id', 'Product is required.' ) );
		}
		return $boolIsValid;
	}

	public function valPrivacyItemTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrivacyItemGroupId() {
		$boolIsValid = true;

		if( false == valStr( $this->getPrivacyItemGroupId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'privacy_item_group_id', 'Category is required.' ) );
		}
		return $boolIsValid;
	}

	public function valIsDefaultState() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsStateEditable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDetailsEditable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valPrivacyItemGroupId();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valToken( $objDatabase );
				$boolIsValid &= $this->valProductId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
