<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CStructureTypes
 * Do not add any new functions to this class.
 */

class CStructureTypes extends CBaseStructureTypes {

	public static function fetchStructureTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CStructureType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchStructureType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CStructureType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>