<?php

class CPropertyTax extends CBasePropertyTax {

    protected $m_strTaxArCodeName;
    protected $m_strTaxedArCodeName;
    protected $m_arrintTaxedArCodeIds;
	protected $m_intTaxedArCodeId;
    protected $m_intScheduledChargeId;

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );

        if( true == isset( $arrmixValues['tax_ar_code_name'] ) ) $this->setTaxArCodeName( $arrmixValues['tax_ar_code_name'] );
        if( true == isset( $arrmixValues['taxed_ar_code_ids'] ) ) $this->setTaxedArCodeIds( $arrmixValues['taxed_ar_code_ids'] );
        if( true == isset( $arrmixValues['taxed_ar_code_id'] ) ) $this->setTaxedArCodeId( $arrmixValues['taxed_ar_code_id'] );
		if( true == isset( $arrmixValues['scheduled_charge_id'] ) ) $this->setScheduledChargeId( $arrmixValues['scheduled_charge_id'] );
        if( true == isset( $arrmixValues['taxed_ar_code_name'] ) ) $this->setTaxedArCodeName( $arrmixValues['taxed_ar_code_name'] );

        return;
    }

    /**
     * Get Functions
     */
    public function getTaxArCodeName() {
        return $this->m_strTaxArCodeName;
    }

    public function getTaxedArCodeIds() {
        return $this->m_arrintTaxedArCodeIds;
    }

	public function getTaxedArCodeId() {
        return $this->m_intTaxedArCodeId;
    }

    public function getScheduledChargeId() {
        return $this->m_intScheduledChargeId;
    }

    public function getTaxedArCodeName() {
    	return $this->m_strTaxedArCodeName;
    }

    public function getTaxAmount() {
    	return $this->m_fltTaxAmount;
    }

	public function getTaxPercentDisplay() {
		return $this->m_fltTaxPercent * 100;
	}

    /**
     * Set Functions
     */

    public function setTaxArCodeName( $strTaxArCodeName ) {
        $this->m_strTaxArCodeName = $strTaxArCodeName;
    }

    public function setTaxedArCodeIds( $arrintTaxedArCodeIds ) {
        $this->m_arrintTaxedArCodeIds = $arrintTaxedArCodeIds;
    }

	public function setTaxedArCodeId( $intTaxedArCodeId ) {
        $this->m_intTaxedArCodeId = $intTaxedArCodeId;
    }

    public function setScheduledChargeId( $intScheduledChargeId ) {
        $this->m_intScheduledChargeId = ( int ) $intScheduledChargeId;
    }

    public function setTaxedArCodeName( $strTaxedArCodeName ) {
    	$this->m_strTaxedArCodeName = $strTaxedArCodeName;
    }

    public function setTaxAmount( $fltTaxAmount ) {
    	$this->m_fltTaxAmount = $fltTaxAmount;
    }

    /**
     * Validate Functions
     */

    public function valName() {
    	$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );

    	}

    	return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( false == is_numeric( $this->getCid() ) || 0 >= $this->getCid() ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( false == is_numeric( $this->getPropertyId() ) || 0 >= $this->getPropertyId() ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valTaxArCodeId() {
        $boolIsValid = true;

        if( false == is_numeric( $this->getTaxArCodeId() ) || 0 >= $this->getTaxArCodeId() ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_ar_code_id', __( 'Tax Charge code is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valTaxedArCodeIds() {
    	$boolIsValid = true;

    	if( false == valArr( $this->getTaxedArCodeIds() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'taxed_ar_code_id', __( 'Select at least one charge code to be taxed.' ) ) );
    	}

    	if( true == valId( $this->getTaxArCodeId() ) && true == valArr( $this->getTaxedArCodeIds() ) && true == in_array( $this->getTaxArCodeId(), $this->getTaxedArCodeIds() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'taxed_ar_code_id', __( 'The Tax Charge Code cannot be used in Charge Codes to be Taxed.' ) ) );
    	}

    	return $boolIsValid;
    }

    public function valTaxPercent() {
        $boolIsValid = true;

        if( false == is_numeric( $this->getTaxPercent() ) || 0 == $this->getTaxPercent() ) {
        	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_percent', __( 'Tax Percent is invalid.' ) ) );
        }
        return $boolIsValid;
    }

    public function valStartDate() {
        $boolIsValid = true;
        if( true == is_null( $this->getStartDate() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start Date is required.' ) ) );
        } elseif( false == CValidation::validateISODate( $this->getStartDate() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start Date is invalid.' ) ) );
        }
        return $boolIsValid;
    }

    public function valEndDate() {
        $boolIsValid = true;
        if( false == is_null( $this->getEndDate() ) ) {
            if( false == CValidation::validateISODate( $this->getEndDate() ) ) {
		        $boolIsValid = false;
		        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End Date is invalid.' ) ) );
	        } elseif( strtotime( $this->getEndDate() ) < strtotime( $this->getStartDate() ) ) {
		        $boolIsValid = false;
		        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End Date should be on or after start date.' ) ) );
	        }
        }
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valTaxArCodeId();
				$boolIsValid &= $this->valTaxedArCodeIds();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valTaxPercent();
				break;

            case VALIDATE_DELETE:
                $boolIsValid &= $this->valCid();
                $boolIsValid &= $this->valPropertyId();
                $boolIsValid &= $this->valTaxArCodeId();
                break;

            default:
                $boolIsValid = false;
                break;
        }

        return $boolIsValid;
    }

    /**
     * Control Functions
     */

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = true ) {

        if( false == $boolIsSoftDelete ) {
            return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
        }

        $this->setDeletedBy( $intCurrentUserId );
        $this->setDeletedOn( ' NOW() ' );
        $this->setUpdatedBy( $intCurrentUserId );
        $this->setUpdatedOn( ' NOW() ' );

        return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    }

    /**
     * Util Functions
     */

    public function calTaxAmount( $fltTaxableAmount ) {
        $fltTaxAmount = 0;
        if( 0 >= $fltTaxableAmount || 0 >= $this->getTaxPercent() ) return $fltTaxAmount;
        $fltTaxAmount = ( ( $fltTaxableAmount * $this->getTaxPercent() ) / 100 );
        return CStrings::strToFloatDef( $fltTaxAmount, NULL, false, 6 );
    }

    /**
     * Other Object Level Fetch/ Create Functions
     */

    public function createPropertyTaxedArCode() {
    	$objPropertyTaxedArCode = new CPropertyTaxedArCode();
    	$objPropertyTaxedArCode->setCid( $this->getCid() );
    	$objPropertyTaxedArCode->setPropertyId( $this->getPropertyId() );
    	$objPropertyTaxedArCode->setPropertyTaxId( $this->getId() );
    	return $objPropertyTaxedArCode;
    }

    public function fetchPropertyTaxedArCodes( $objDatabase ) {
        return \Psi\Eos\Entrata\CPropertyTaxedArCodes::createService()->fetchPropertyTaxedArCodesByPropertyTaxIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase );
    }

}
?>