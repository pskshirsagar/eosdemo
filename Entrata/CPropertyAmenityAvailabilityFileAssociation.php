<?php

class CPropertyAmenityAvailabilityFileAssociation extends CBasePropertyAmenityAvailabilityFileAssociation {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyAmenityAvailabilityId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNote() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>