<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CProposals
 * Do not add any new functions to this class.
 */

class CProposals extends CBaseProposals {

	public  static function fetchProposalsCountByProposalFilterByCid( $objProposalFilter, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) return NULL;

		$strConditions = '';

		if( true == valObj( $objProposalFilter, 'CProposalFilter' ) ) {
			if( true == valStr( $objProposalFilter->getRequestName() ) ) {
				$strConditions .= ' AND prop.request_name ILIKE \'%' . $objProposalFilter->getRequestName() . '%\'';
			}

			if( true == valArr( $objProposalFilter->getProposalStatusTypeIds() ) ) {
				$strConditions .= ' AND pst.id IN ( ' . implode( ',', $objProposalFilter->getProposalStatusTypeIds() ) . ' )';
			}

			if( true == valStr( $objProposalFilter->getBidCloseDateFrom() ) ) {
				$strConditions .= ' AND prop.bid_close_date >= \'' . $objProposalFilter->getBidCloseDateFrom() . '\'::TIMESTAMP';
			}

			if( true == valStr( $objProposalFilter->getBidCloseDateTo() ) ) {
				$strConditions .= ' AND prop.bid_close_date <= \'' . $objProposalFilter->getBidCloseDateTo() . '\'::TIMESTAMP';
			}

			if( true == valArr( $objProposalFilter->getApPayeeIds() ) ) {
				$strConditions .= ' AND ap.id IN ( ' . implode( ',', $objProposalFilter->getApPayeeIds() ) . ' )';
			}

			if( true == valArr( $objProposalFilter->getJobIds() ) ) {
				$strConditions .= ' AND j.id IN ( ' . implode( ',', $objProposalFilter->getJobIds() ) . ' )';
			}
		}

		$strSql = 'SELECT
						COUNT( DISTINCT prop.id )
					FROM
						proposals prop
						JOIN proposal_status_types pst ON ( pst.id = prop.proposal_status_type_id )
						JOIN jobs j ON ( j.cid = prop.cid AND j.id = prop.job_id )
						JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( $objProposalFilter->getPropertyIds(), ',' ) . '] ) lp ON ( lp.is_disabled = 0 AND lp.property_id = prop.property_id )
						LEFT JOIN properties p ON ( p.cid = prop.cid AND p.id = lp.property_id )
						LEFT JOIN proposal_ap_payees pap ON ( pap.cid = prop.cid AND pap.proposal_id = prop.id )
						LEFT JOIN ap_payees ap ON ( ap.cid = pap.cid AND ap.id = pap.ap_payee_id )
					WHERE
						prop.cid = ' . ( int ) $intCid . $strConditions;

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchPaginatedProposalsByProposalFilterByCid( $objProposalFilter, $intCid, $objDatabase, $objPagination = NULL ) {

		if( false == valId( $intCid ) ) return NULL;

		$strConditions = '';
		$strLimitOffset	= '';

		if( true == valObj( $objProposalFilter, 'CProposalFilter' ) ) {
			if( true == valStr( $objProposalFilter->getRequestName() ) ) {
				$strConditions .= ' AND prop.request_name ILIKE \'%' . $objProposalFilter->getRequestName() . '%\'';
			}

			if( true == valArr( $objProposalFilter->getProposalStatusTypeIds() ) ) {
				$strConditions .= ' AND pst.id IN ( ' . implode( ',', $objProposalFilter->getProposalStatusTypeIds() ) . ' )';
			}

			if( true == valStr( $objProposalFilter->getBidCloseDateFrom() ) ) {
				$strConditions .= ' AND prop.bid_close_date >= \'' . $objProposalFilter->getBidCloseDateFrom() . '\'::TIMESTAMP';
			}

			if( true == valStr( $objProposalFilter->getBidCloseDateTo() ) ) {
				$strConditions .= ' AND prop.bid_close_date <= \'' . $objProposalFilter->getBidCloseDateTo() . '\'::TIMESTAMP';
			}

			if( true == valArr( $objProposalFilter->getApPayeeIds() ) ) {
				$strConditions .= ' AND ap.id IN ( ' . implode( ',', $objProposalFilter->getApPayeeIds() ) . ' )';
			}

			if( true == valArr( $objProposalFilter->getJobIds() ) ) {
				$strConditions .= ' AND j.id IN ( ' . implode( ',', $objProposalFilter->getJobIds() ) . ' )';
			}
		}

		$strOrder = ( false == $objProposalFilter->getOrderFlag() ) ? 'DESC' : 'ASC';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strLimitOffset = ' LIMIT ' . ( int ) $objPagination->getPageSize() . ' OFFSET ' . ( int ) $objPagination->getOffset();
		}

		switch( $objProposalFilter->getOrderBy() ) {

			case 'name':
				$strOrderBy = ' ORDER BY prop.request_name ' . $strOrder;
				break;

			case 'property':
				$strOrderBy = ' ORDER BY p.property_name ' . $strOrder;
				break;

			case 'job':
				$strOrderBy = ' ORDER BY j.name ' . $strOrder;
				break;

			case 'status':
				$strOrderBy = ' ORDER BY pst.order_num ' . $strOrder;
				break;

			case 'bid_close_date':
				$strOrderBy = ' ORDER BY prop.bid_close_date ' . $strOrder;
				break;

			default:
				$strOrderBy = ' ORDER BY pst.order_num ASC, prop.bid_close_date DESC, j.name ASC, prop.request_name ASC';
				break;
		}

		$strSql = 'SELECT
						prop.*,
						pst.name AS proposal_status_type_name,
						j.name AS job_name,
						p.property_name,
						array_agg( CASE WHEN ap.company_name IS NOT NULL THEN ap.company_name ELSE \'-\' END ) AS bidders
					FROM
						proposals prop
						JOIN proposal_status_types pst ON ( pst.id = prop.proposal_status_type_id )
						JOIN jobs j ON ( j.cid = prop.cid AND j.id = prop.job_id )
						JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( $objProposalFilter->getPropertyIds(), ',' ) . '] ) lp ON ( lp.is_disabled = 0 AND lp.property_id = prop.property_id )
						LEFT JOIN properties p ON ( p.cid = prop.cid AND p.id = lp.property_id )
						LEFT JOIN proposal_ap_payees pap ON ( pap.cid = prop.cid AND pap.proposal_id = prop.id )
						LEFT JOIN ap_payees ap ON ( ap.cid = pap.cid AND ap.id = pap.ap_payee_id )
					WHERE
						prop.cid = ' . ( int ) $intCid . $strConditions . '
					GROUP BY
						prop.id,
						prop.cid,
						pst.id,
						j.id,
						j.cid,
						p.id,
						p.cid' . $strOrderBy . $strLimitOffset;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchProposalByIdByCid( $intId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						prop.*,
						j.name AS job_name,
						p.property_name,
						ac.name AS contract_name
					FROM
						proposals prop
						JOIN jobs j ON ( j.cid = prop.cid AND j.id = prop.job_id )
						JOIN properties p ON ( p.cid = prop.cid AND p.id = prop.property_id )
						LEFT JOIN ap_contracts ac ON ( prop.cid = ac.cid AND ac.proposal_id = prop.id )
					WHERE
						prop.cid = ' . ( int ) $intCid . '
						AND prop.id = ' . ( int ) $intId;

		return self::fetchProposal( $strSql, $objDatabase );

	}

}
?>