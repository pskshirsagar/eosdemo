<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyLeadSurveyQuestions
 * Do not add any new functions to this class.
 */

class CPropertyLeadSurveyQuestions extends CBasePropertyLeadSurveyQuestions {

	public static function fetchPropertyLeadSurveyQuestionsByIdByPropertyIdByCid( $intId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}
		$strSql = ' SELECT
						*
					FROM
						property_lead_survey_questions
					WHERE
						id = ' . ( int ) $intId . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL
					LIMIT
						1';
		return self::fetchPropertyLeadSurveyQuestion( $strSql, $objDatabase );
	}

	public static function fetchPropertyLeadSurveyQuestionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsPublished = false ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}
		$strWhereSql = '';
		if( true == $boolIsPublished ) {
			$strWhereSql = ' AND is_published IS TRUE';
		}
		$strSql = ' SELECT
						*
					FROM
						property_lead_survey_questions
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL ' . $strWhereSql;

		return self::fetchPropertyLeadSurveyQuestions( $strSql, $objDatabase );
	}

	public static function fetchPropertyLeadSurveyQuestionsByIdsByPropertyIdByCid( $intIds, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valIntArr( $intIds ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						*
					FROM
						property_lead_survey_questions
					WHERE
						id IN ( ' . implode( ',', $intIds ) . ' )
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL';

		return self::fetchPropertyLeadSurveyQuestions( $strSql, $objDatabase );
	}

}
?>