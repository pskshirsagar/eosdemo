<?php

class CDocumentSubType extends CBaseDocumentSubType {

	// Constants for Auto Responders

	const MAINTENANCE_CONFIRMATION_ATTACHMENT				= 1;
	const PAYMENT_RECEIPT_ATTACHMENT 						= 2;
	const APPLICATION_CONFIRMATION_ATTACHMENT 				= 3;
	const GUEST_CARD_CONFIRMATION_ATTACHMENT 				= 4;

	// Constants for Forms

	const CUSTOM_FORM 										= 5;
	const ONLINE_RENTAL_APPLICATION_FORM					= 6;
	const RESIDENT_PORTAL_SURVEY_FORM						= 7;
	const LEASE_AGREEMENT									= 8;
	const APPLICATION_POLICY_DOCUMENT						= 17;
	const APPLICANT_MASS_EMAIL								= 18;
	const RESIDENT_MASS_EMAIL								= 19;
	const GUEST_CARD_MASS_EMAIL								= 20;
	const PROPERTY_MANAGER_MASS_EMAIL						= 21;
	const EMPLOYEE_MASS_EMAIL								= 22;
	const RESIDENT_PORTAL_MAINTENANCE_SURVEY				= 23;
	const RESIDENT_PORTAL_MOVE_IN_SURVEY					= 24;
	const RESIDENT_PORTAL_MOVE_OUT_SURVEY					= 25;
	const SOCIAL_EVENT										= 28;
	const DOCUMENT_TEMPLATE									= 29;
	const ELECTRONIC_SIGNATURE_ACT							= 30;
	const BLUE_MOON_LEASE_AGREEMENT							= 31;
	const FIRST_NOTICE										= 32;
	const REPEAT_NOTICE										= 33;
	const FINAL_NOTICE										= 34;
	const RESIDENT_VERIFY_SCREENING_POLICY_DOCUMENT			= 35;
	const DEMAND_NOTICE										= 36;
	const FIRST_PRE_COLLECTION_LETTER						= 37;
	const COLLECTION_NOTICE									= 38;
	const REPORTS											= 39;
	const SECOND_PRE_COLLECTION_LETTER						= 40;
	const FINAL_PRE_COLLECTION_LETTER						= 41;
	const COLLECTION_EMAIL									= 42;
	const NON_RENEWAL_NOTICE								= 43;
	const WAITLIST_EMAIL									= 44;
	const NSF_NOTICE										= 45;
	const REPAYMENT_AGREEMENT_LATE_NOTICE					= 46;
	const LEASE              								= 47;
	const APPLICATION              							= 48;

	public static $c_arrstrResidentPortalDocumentSubTypeStrings = [
		self::RESIDENT_PORTAL_MAINTENANCE_SURVEY => 'Maintenance Survey'
	];

	public static $c_arrintResidentPortalMoveInOutSurvey = [ CDocumentSubType::RESIDENT_PORTAL_MOVE_IN_SURVEY, CDocumentSubType::RESIDENT_PORTAL_MOVE_OUT_SURVEY ];

	public static function loadLateNoticeDocumentSubTypesArray() {
		$arrintLateNoticeDocumentSubTypes = [];

		$arrintLateNoticeDocumentSubTypes[1]	= self::FIRST_NOTICE;
		$arrintLateNoticeDocumentSubTypes[2]	= self::REPEAT_NOTICE;
		$arrintLateNoticeDocumentSubTypes[3]	= self::FINAL_NOTICE;
		$arrintLateNoticeDocumentSubTypes[4]	= self::DEMAND_NOTICE;

		return $arrintLateNoticeDocumentSubTypes;
	}

	public static function loadCollectionDocumentSubTypesArray() {
		$arrintCollectionDocumentSubTypes = [];

		$arrintCollectionDocumentSubTypes[1]	= self::FIRST_PRE_COLLECTION_LETTER;
		$arrintCollectionDocumentSubTypes[2]	= self::SECOND_PRE_COLLECTION_LETTER;
		$arrintCollectionDocumentSubTypes[3]	= self::FINAL_PRE_COLLECTION_LETTER;
		$arrintCollectionDocumentSubTypes[4]	= self::COLLECTION_NOTICE;

		return $arrintCollectionDocumentSubTypes;
	}

	public static function loadCollectionEmailSubTypesArray() {
		$arrintCollectionEmailSubTypes = [];

		$arrintCollectionEmailSubTypes[1]	= self::COLLECTION_EMAIL;

		return $arrintCollectionEmailSubTypes;
	}

	public static function loadNonRenewalNoticeDocumentSubTypesArray() {
		$arrintNonRenewaloticeDocumentSubTypes = [];

		$arrintNonRenewaloticeDocumentSubTypes[1] 	= self::NON_RENEWAL_NOTICE;

		return $arrintNonRenewaloticeDocumentSubTypes;
	}

	public static function loadWaitlistEmailSubTypesArray() {
		$arrintWaitlistEmailSubTypes = [];

		$arrintWaitlistEmailSubTypes[1]	= self::WAITLIST_EMAIL;

		return $arrintWaitlistEmailSubTypes;
	}

	public static function loadNSFNoticeSubTypesArray() {
		$arrintNSFNoticeSubTypes[1]	= self::NSF_NOTICE;
		return $arrintNSFNoticeSubTypes;
	}

	public static function loadRepaymentAgreementLateNoticeSubTypesArray() {
		$arrintRepaymentAgreementLateNoticeSubTypes[1] = self::REPAYMENT_AGREEMENT_LATE_NOTICE;
		return $arrintRepaymentAgreementLateNoticeSubTypes;
	}

}
