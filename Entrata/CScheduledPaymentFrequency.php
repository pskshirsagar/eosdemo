<?php

class CScheduledPaymentFrequency extends CBaseScheduledPaymentFrequency {

	const ONCE 			= 1;
	const DAILY 		= 2;
	const WEEKLY 		= 3;
	const MONTHLY 		= 4;
	const QUARTERLY 	= 5;
	const YEARLY 		= 6;

}
?>