<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSpecialGroupLeaseStartWindows
 * Do not add any new functions to this class.
 */

class CSpecialGroupLeaseStartWindows extends CBaseSpecialGroupLeaseStartWindows {

	public static function fetchAllSpecialGroupLeaseStartWindowsBySpecialGroupIdByCid( $intSpecialGroupId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						sglsw.*
					FROM
						special_group_lease_start_windows sglsw
						JOIN special_groups sg ON (sg.cid = sglsw.cid AND sg.id = sglsw.special_group_id)
					WHERE
						sglsw.special_group_id = ' . ( int ) $intSpecialGroupId . '
						AND sg.cid = ' . ( int ) $intCid;

		return self::fetchSpecialGroupLeaseStartWindows( $strSql, $objDatabase );
	}

}
?>