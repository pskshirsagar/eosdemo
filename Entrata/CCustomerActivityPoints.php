<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerActivityPoints
 * Do not add any new functions to this class.
 */

class CCustomerActivityPoints extends CBaseCustomerActivityPoints {

	public static function fetchCustomerActivityPointsByCompanyActivityPointIdByCustomerIdByCid( $intCompanyActivityPointId, $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerActivityPoints( sprintf( 'SELECT * FROM customer_activity_points WHERE company_activity_point_id = %d AND customer_id = %d AND cid = %d ', ( int ) $intCompanyActivityPointId, ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerActivityPointsByCidByPropertyIdByIntervalByLimit( $intCid, $objDatabase, $intPropertyId = NULL, $strInterval = NULL, $intLimit = NULL ) {

		$strIntervalCondition = '';
		switch( $strInterval ) {
			case 'month':
			case 'year':
				$strIntervalCondition = 'AND age(cap.reward_datetime) < \'1 ' . $strInterval . '\'';
				break;

			default:
				// default case
				break;
		}

		$strPropertyIdCondition = false == is_null( $intPropertyId ) ? 'AND l.property_id = ' . ( int ) $intPropertyId : '';

		$strSql = 'SELECT
						c.id as customer_id,
						c.cid,
						COALESCE( sum(cap.points_earned), 0 ) as points_earned
					FROM
						customers c
						JOIN lease_customers lc	ON ( c.id = lc.customer_id AND lc.cid = c.cid )
						JOIN leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid )
						LEFT JOIN customer_settings cs1	ON ( c.id = cs1.customer_id AND cs1.cid = c.cid AND cs1.key = \'opt_out_of_apartmates\' )
						LEFT JOIN customer_settings cs2	ON ( c.id = cs2.customer_id AND cs2.cid = c.cid AND cs2.key = \'opt_out_of_gamification\' )
						LEFT JOIN customer_activity_points cap ON ( c.id = cap.customer_id AND cap.cid = c.cid ' . $strIntervalCondition . ' )
					WHERE
						c.cid = ' . ( int ) $intCid . '
						' . $strPropertyIdCondition . '
						AND lc.lease_status_type_id IN ( ' . CLeaseStatusType::CURRENT . ', ' . CLeaseStatusType::NOTICE . ' )
						AND ( cs1.id IS NULL OR cs1.value <> \'1\' )
						AND ( cs2.id IS NULL OR cs2.value <> \'1\' )
						AND ( c.birth_date IS NULL OR age(c.birth_date) > \'18 years\' )
						AND l.occupancy_type_id <> ' . COccupancyType::OTHER_INCOME . '
					GROUP BY
						c.id,
						c.cid
					ORDER BY
						points_earned DESC';

		if( false == is_null( $intLimit ) ) {
		    $strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		return parent::fetchCustomerActivityPoints( $strSql, $objDatabase );
	}

	public static function fetchCustomerActivityPointsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCustomerActivityPointsByCidByPropertyIdByIntervalByLimit( $intCid, $objDatabase, $intPropertyId );
	}

	public static function fetchCustomerActivityPointTotalByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cap.customer_id,
						sum(cap.points_earned) points_earned
					FROM
						customer_activity_points cap
					WHERE
						cap.customer_id = ' . ( int ) $intCustomerId . '
						AND cap.cid = ' . ( int ) $intCid . '
					GROUP BY cap.customer_id';

		return parent::fetchCustomerActivityPoints( $strSql, $objDatabase );
	}

	public static function fetchCustomerActivityPointTotalByCustomerIdsByCid( $intCustomerIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cap.customer_id,
						sum(cap.points_earned) points_earned
					FROM
						customer_activity_points cap
					WHERE
						cap.customer_id IN ( ' . implode( ', ', $intCustomerIds ) . ' )
						AND cap.cid = ' . ( int ) $intCid . '
					GROUP BY cap.customer_id';

		$arrmixData = fetchData( $strSql, $objDatabase );

		$arrstrFinalData = [];

		if( true == valArr( $arrmixData ) ) {
			foreach( $arrmixData as $arrmixChunk ) {
				$arrstrFinalData[$arrmixChunk['customer_id']] = $arrmixChunk['points_earned'];
			}
		}

		return $arrstrFinalData;
	}

	public static function fetchCustomerActivityPointsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedUnits = false ) {

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

		$strSqlAndConditions = '';

		$strSqlAndConditions .= ' AND l.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )';

		$strSqlAndConditions .= ' AND lc.lease_status_type_id IN( ' . CLeaseStatusType::CURRENT . ',' . CLeaseStatusType::FUTURE . ',' . CLeaseStatusType::NOTICE . ' )';

		$strSql = 'SELECT
						lc.lease_id,
						l.property_id,
						lc.customer_id,
						p.property_name,
						p.lookup_code,
						COALESCE( c.name_last || \', \' || c.name_first, c.name_last, c.name_first  ) AS resident_name,
						COALESCE( SUM( cap.points_earned ), 0) AS all_time,
						COALESCE( SUM( CASE WHEN date_part(\'month\',cap.reward_datetime) = date_part(\'month\',CURRENT_TIMESTAMP-\'1 month\'::interval) THEN cap.points_earned ELSE 0 END ), 0 ) AS last_30,
						COALESCE( SUM( CASE WHEN date_trunc(\'month\', cap.reward_datetime) <> date_trunc(\'month\', CURRENT_TIMESTAMP) THEN 0 ELSE cap.points_earned END ), 0 ) AS month_to_date,
						COALESCE( SUM( CASE WHEN date_trunc(\'year\', cap.reward_datetime) <> date_trunc(\'year\', CURRENT_TIMESTAMP) THEN 0 ELSE cap.points_earned END ), 0 ) AS year_to_date
					FROM
						customers c
						JOIN lease_customers lc ON ( lc.cid = c.cid AND c.id = lc.customer_id )
						JOIN leases l ON ( l.cid = lc.cid AND l.id = lc.lease_id AND lc.customer_id = l.primary_customer_id   )
						JOIN lease_intervals li ON ( li.cid = l.cid AND li.lease_id = l.id AND li.id = l.active_lease_interval_id )
						JOIN lease_processes lp ON ( lp.cid = l.cid AND lp.lease_id = l.id AND lp.customer_id IS NULL )
						JOIN properties p ON ( p.cid = l.cid AND p.id = l.property_id )
						LEFT JOIN property_units pu ON ( l.cid = pu.cid AND l.property_unit_id = pu.id ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN customer_activity_points cap ON ( cap.cid = c.cid AND cap.customer_id = c.id )
					WHERE
						lc.cid = ' . ( int ) $intCid . '
						AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
						' . $strSqlAndConditions . '
					GROUP BY
						lc.lease_id, l.property_id, p.property_name, p.lookup_code, lc.customer_id, c.name_last,c.name_first
					ORDER BY
						p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

}
?>
