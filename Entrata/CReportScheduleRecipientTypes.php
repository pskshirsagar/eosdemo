<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportScheduleRecipientTypes
 * Do not add any new functions to this class.
 */

class CReportScheduleRecipientTypes extends CBaseReportScheduleRecipientTypes {

	public static function fetchReportScheduleRecipientTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CReportScheduleRecipientType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchReportScheduleRecipientType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CReportScheduleRecipientType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	/**
	 * @param $objDatabase
	 * @return CReportScheduleRecipientType[]
	 */
	public static function fetchAllReportScheduleRecipientTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM report_schedule_recipient_types';

		return self::fetchReportScheduleRecipientTypes( $strSql, $objDatabase );
	}

}
