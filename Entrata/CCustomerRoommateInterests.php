<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerRoommateInterests
 * Do not add any new functions to this class.
 */

class CCustomerRoommateInterests extends CBaseCustomerRoommateInterests {

	public static function fetchCustomerRoommateInterestsByCidByBulkUnitAssignmentFilter( $intCid, $objBulkUnitAssignmentFilter, $objDatabase, $arrintLeaseIds = NULL, $arrintPropertyUnitIds = NULL, $arrintCustomerIds = NULL, $boolIsRequiredMatch = false ) {
		if( false == valObj( $objBulkUnitAssignmentFilter, 'CBulkUnitAssignmentFilter' ) ) return NULL;

		$strSql = 'SELECT
						cri.id,
						cl.id AS lease_id,
						cl.property_unit_id,
						cri.roommate_interest_option_id,
						cri.roommate_interest_id,
						cri.customer_id,
						cri.written_response,
						rio.answer,
						rio.system_code AS roommate_interest_option_system_code,
						ri.question,
						ri.system_code AS roommate_interest_system_code,
						ric.name AS roommate_interest_category_name,
						ri.input_type_id AS roommate_interest_input_type_id,
						pri.matching_weight
					FROM
						customer_roommate_interests cri
						LEFT JOIN roommate_interest_options rio ON ( rio.cid = cri.cid AND rio.id = cri.roommate_interest_option_id AND rio.roommate_interest_id = cri.roommate_interest_id )
						LEFT JOIN roommate_interests ri ON ( ri.cid = cri.cid AND ri.id = cri.roommate_interest_id )
						LEFT JOIN roommate_interest_categories ric ON ( ric.id = ri.roommate_interest_category_id )
						JOIN cached_leases cl ON ( cl.cid = cri.cid AND cl.primary_customer_id = cri.customer_id AND cl.id = cri.lease_id )
						JOIN customers c ON ( cl.cid = c.cid AND cl.primary_customer_id = c.id )
						JOIN cached_applications ca ON ( ca.cid = cl.cid AND ca.lease_id = cl.id AND ca.property_id = cl.property_id )
						JOIN application_stage_statuses ass ON ( ca.lease_interval_type_id = ass.lease_interval_type_id and ca.application_stage_id = ass.application_stage_id and ca.application_status_id = ass.application_status_id )
						LEFT JOIN property_roommate_interests pri ON ( pri.roommate_interest_id = cri.roommate_interest_id AND pri.property_id = cl.property_id AND pri.cid = cl.cid )
					WHERE
						cri.cid = ' . ( int ) $intCid . '
						AND cl.property_id = ' . ( int ) $objBulkUnitAssignmentFilter->getPropertyId() . '
						AND ( rio.answer IS NOT NULL OR cri.written_response IS NOT NULL )
						AND ric.is_published = 1
						AND ca.lease_term_id = ' . ( int ) $objBulkUnitAssignmentFilter->getLeaseTermId() . '
						AND ca.lease_start_window_id = ' . ( int ) $objBulkUnitAssignmentFilter->getLeaseStartWindowId() . '
						AND ca.property_floorplan_id = ' . ( int ) $objBulkUnitAssignmentFilter->getPropertyFloorplanId() . '
						AND ass.id IN( ' . implode( ',', $objBulkUnitAssignmentFilter->getApplicationStageStatusIds() ) . ' )
						AND cl.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ' )';

		$strSql .= ( true == valArr( $arrintLeaseIds ) ) ? ' AND cl.id NOT IN ( ' . implode( ',', $arrintLeaseIds ) . ' )' : '';
		$strSql .= ( true == valArr( $arrintPropertyUnitIds ) ) ? ' AND cl.property_unit_id IN ( ' . implode( ',', $arrintPropertyUnitIds ) . ' )' : '';
		$strSql .= ( true == valArr( $arrintCustomerIds ) ) ? ' AND cri.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) AND ( cri.roommate_interest_option_id IS NOT NULL )' : '';
		$strSql .= ( true == $boolIsRequiredMatch ) ? ' AND pri.matching_weight = 3 ' : '';

		if( true == is_numeric( $objBulkUnitAssignmentFilter->getGenderId() ) ) {

			$strGender = ( 1 == ( int ) $objBulkUnitAssignmentFilter->getGenderId() ) ? 'M' : 'F';

			$strSql .= ' AND c.gender = \'' . $strGender . '\'';
		}

		$strSql .= ' ORDER BY ric.id, cri.roommate_interest_id ';

		return parent::fetchCustomerRoommateInterests( $strSql, $objDatabase );
	}

	public static function fetchCustomerRoommateInterestsByLeaseIdsOrCustomerIdsByPropertyIdByCid( $intCid, $intPropertyId, $objDatabase, $arrintLeaseIds = NULL, $arrintCustomerIds = NULL, $boolIsRequiredMatch = false ) {
		if( false == is_numeric( $intCid ) || false == is_numeric( $intPropertyId ) ) return NULL;

		$strSelectClause	= '';
		$strFromClause		= '';
		$strWhereClause		= '';

		if( true == valArr( $arrintLeaseIds ) ) {
			$strSelectClause	= ', l.id AS lease_id';
			$strFromClause		= ' JOIN leases l ON ( cri.lease_id = l.id AND cri.customer_id = l.primary_customer_id AND cri.cid = l.cid )';
			$strWhereClause		= ' AND l.id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';
		}

		if( true == valArr( $arrintCustomerIds ) )
			$strWhereClause		.= ' AND cri.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )';

		if( true == $boolIsRequiredMatch )
			$strWhereClause	   .= ' AND pri.matching_weight = 3 ';

		$strSql = 'SELECT
						cri.id,
						cri.roommate_interest_option_id,
						cri.roommate_interest_id,
						cri.written_response,
						ri.input_type_id AS roommate_interest_input_type_id,
						util_get_translated(\'question\', ri.question, ri.details ) as question,
						util_get_translated(\'name\', ric.name, ric.details ) AS roommate_interest_category_name,
						util_get_translated(\'answer\', rio.answer, rio.details ) as answer,
						ri.system_code AS roommate_interest_system_code,
						pri.matching_weight
						' . $strSelectClause . '
					FROM
						customer_roommate_interests cri
						LEFT JOIN roommate_interest_options rio ON ( cri.cid = rio.cid AND cri.roommate_interest_option_id = rio.id AND cri.roommate_interest_id = rio.roommate_interest_id )
						LEFT JOIN roommate_interests ri ON ( ri.cid = cri.cid AND ri.id = cri.roommate_interest_id )
						LEFT JOIN roommate_interest_categories ric ON ( ric.id = ri.roommate_interest_category_id )
						LEFT JOIN property_roommate_interests pri ON ( pri.cid = cri.cid AND pri.roommate_interest_id = cri.roommate_interest_id AND pri.property_id = ' . ( int ) $intPropertyId . ' )
						' . $strFromClause . '
					WHERE
						cri.cid = ' . ( int ) $intCid . '
						AND ( cri.roommate_interest_option_id IS NOT NULL OR cri.written_response IS NOT NULL )
						AND ric.is_published = 1
						' . $strWhereClause . '
					ORDER BY
						ric.id,
						cri.roommate_interest_id';

		return self::fetchCustomerRoommateInterests( $strSql, $objDatabase );
	}

	public static function fetchCustomerRoommateInterestsBySystemCodeByLeaseIdsByPropertyIdByCid( $intCid, $intPropertyId, $objDatabase, $arrintLeaseIds, $strSystemCode = NULL ) {
		if( false == is_numeric( $intCid ) || false == is_numeric( $intPropertyId ) ) return NULL;
		if( false == valArr( $arrintLeaseIds ) ) return;

		$strRoommateInterestJoin		= '';

		if( true == valStr( $strSystemCode ) )
			$strRoommateInterestJoin    .= " AND ri.system_code = '" . $strSystemCode . "' ";

		$strSql = 'SELECT
						l.id AS lease_id,
						l.primary_customer_id AS customer_id,
						CASE 
							WHEN ap.gender = \'M\' THEN ' . CGender::MALE . '
							WHEN ap.gender = \'F\' THEN ' . CGender::FEMALE . '
						END AS customer_gender,
						array_to_string ( array_agg ( CASE WHEN rio.answer = \'Male\' THEN ' . CGender::MALE . ' WHEN rio.answer = \'Female\' THEN ' . CGender::FEMALE . ' END ), \', \' ) AS answers
					FROM
						leases l
						JOIN applicants ap ON ( ap.cid = l.cid AND l.primary_customer_id = ap.customer_id )
						LEFT JOIN customer_roommate_interests cri ON ( cri.cid = ap.cid AND cri.customer_id = ap.customer_id AND l.id = cri.lease_id )
						LEFT JOIN roommate_interests ri ON ( ri.cid = cri.cid AND ri.id = cri.roommate_interest_id ' . $strRoommateInterestJoin . ' )    
						LEFT JOIN roommate_interest_options rio ON ( cri.cid = rio.cid AND cri.roommate_interest_option_id = rio.id AND cri.roommate_interest_id = rio.roommate_interest_id AND rio.roommate_interest_id = ri.id )
					WHERE
						l.property_id = ' . ( int ) $intPropertyId . '
						AND ap.gender IS NOT NULL
						AND l.cid = ' . ( int ) $intCid . '
						AND l.id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
					GROUP BY
						l.id,
						l.primary_customer_id,
						ap.gender';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerRoommateInterestsBySelectParametersByCustomerIdByLeaseIdByCid( $strSelectClause, $intCustomerId, $intLeaseId, $intCid, $objDatabase ) {
		if( false == valId( $intCustomerId ) || false == valId( $intLeaseId ) || false == valId( $intCid ) || false == valStr( $strSelectClause ) ) {
			return NULL;
		}
		$strSql = 'SELECT ' .
				$strSelectClause . '
					FROM
        				customer_roommate_interests cri
        			WHERE
						cri.cid = ' . ( int ) $intCid . '
						AND cri.roommate_interest_option_id IS NOT NULL
        				AND cri.customer_id = ' . ( int ) $intCustomerId . '
        				AND cri.lease_id = ' . ( int ) $intLeaseId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerRoommateInterestsByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $intCid, $objDatabase, $arrintRoommateInterestIds = [] ) {
		if( false == valId( $intCustomerId ) || false == valId( $intLeaseId ) || false == valId( $intCid ) ) {
			return NULL;
		}
		$strWhereClause = ' ';
		if( true == valArr( $arrintRoommateInterestIds ) ) {
			$strRoommateInterestIds             = implode( ',', $arrintRoommateInterestIds );
			$strWhereClause = ' AND cri.roommate_interest_id IN ( ' . $strRoommateInterestIds . ' ) ';
		}
		$strSql = 'SELECT *
					FROM
        				customer_roommate_interests cri
        			WHERE
						cri.cid = ' . ( int ) $intCid . '
        				AND cri.customer_id = ' . ( int ) $intCustomerId . '
        				AND cri.lease_id = ' . ( int ) $intLeaseId . $strWhereClause;

		return self::fetchCustomerRoommateInterests( $strSql, $objDatabase );
	}

	public static function fetchCustomerRoommateInterestsByCustomerIdsByLeaseIdsByCid( $arrintCustomerIds, $arrintLeaseIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) || false == valArr( $arrintLeaseIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT *
					FROM
        				customer_roommate_interests cri
        			WHERE
						cri.cid = ' . ( int ) $intCid . '
        				AND cri.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
        				AND cri.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) ';

		return self::fetchCustomerRoommateInterests( $strSql, $objDatabase );
	}

}
?>