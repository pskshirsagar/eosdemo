<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportFilters
 * Do not add any new functions to this class.
 */

class CReportFilters extends CBaseReportFilters {

	/**
	 * @param $intReportGroupId
	 * @param $intCid
	 * @param $objDatabase
	 * @return CReportFilter[]
	 */
	public static function fetchReportFiltersByReportGroupIdByCid( $intReportGroupId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				rf.*,
				ri.report_id,
				ri.report_group_id,
				ri.report_version_id,
				r.name AS report_name,
				r.report_type_id
			FROM
				report_instances ri
				JOIN reports r ON ( r.cid = ri.cid AND r.id = ri.report_id AND r.deleted_on IS NULL AND COALESCE( r.is_published, TRUE ) = TRUE )
				JOIN report_filters rf ON ( rf.cid = ri.cid AND rf.report_id = ri.report_id AND rf.id = ri.report_filter_id AND rf.deleted_on IS NULL )
				JOIN report_versions rv ON ( rv.cid = ri.cid AND rv.id = ri.report_version_id )
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND ri.report_group_id = ' . ( int ) $intReportGroupId . '
				AND ri.deleted_on IS NULL
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
				AND COALESCE( ( rv.definition->>\'is_published\' )::BOOLEAN, TRUE ) = TRUE
			ORDER BY
				ri.id';

		return self::fetchReportFilters( $strSql, $objDatabase, false );
	}

	/**
	 * @param $arrintIds
	 * @param $intCid
	 * @param $objDatabase
	 * @return CReportFilter[]
	 */
	public static function fetchReportFiltersByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return [];

		$strSql = ' SELECT
						*
					FROM
						report_filters
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintIds ) . ' )';

		return self::fetchReportFilters( $strSql, $objDatabase );
	}

	public static function fetchReportFiltersByReportIdByCompanyUserOrGroupIdByCid( $intReportId, $intCompanyUserOrGroupId, $intCid, $objDatabase, $boolHideAdminType, $boolIsForListing = false, $boolPublicFilterOnly = false ) {

		$strOrderBySql		= ( true == $boolIsForListing ) ? 'rf.name ASC' : 'rf.updated_on DESC';
		$strIsPublicSql		= ( $boolPublicFilterOnly == true ) ? ' AND rf.is_public = true ' : '';
		$strIsInternalSql	= 'AND NOT EXISTS (
										SELECT
											1
										FROM
											report_instances AS ri
											JOIN report_groups AS rg ON rg.cid = ri.cid AND rg.id = ri.report_group_id
										WHERE
											ri.cid = rf.cid
											AND ri.report_filter_id = rf.id
											AND rg.is_internal = TRUE
											AND ri.deleted_by IS NULL
									)';

		if( true == $boolHideAdminType ) {
			$strSql = '
				SELECT
					*
				FROM
					(
						SELECT
							rf.*,
							rf.company_user_id AS report_filter_company_user_id,
							r.name as report_name,
							sf.id AS shared_filter_id,
							sf.created_by AS shared_filter_created_by,
							sf.report_filter_id,
							sf.company_user_id,
							sf.company_group_id,
							rank ( ) OVER ( PARTITION BY rf.id ORDER BY sf.report_filter_id, sf.company_user_id, sf.company_group_id ) AS rank
						FROM
							report_filters rf
							JOIN reports r ON ( r.cid = rf.cid AND r.id = rf.report_id )
							LEFT JOIN shared_filters sf ON ( rf.cid = sf.cid AND sf.report_filter_id = rf.id AND sf.deleted_by IS NULL AND sf.deleted_on IS NULL )
						WHERE
							rf.cid = ' . ( int ) $intCid . '
							AND rf.report_id = ' . ( int ) $intReportId . '
							AND rf.deleted_by IS NULL
							AND rf.deleted_on IS NULL
							' . $strIsInternalSql . '
							' . $strIsPublicSql . '
						ORDER BY
							' . $strOrderBySql . '
					) AS report_filter_data
				WHERE
					report_filter_data.rank = 1 ';
		} else {
			$strSql = '
				SELECT
					*
				FROM
					(
						SELECT
							rf.*,
							r.name AS report_name,
							sf.id AS shared_filter_id,
							sf.created_by AS shared_filter_created_by,
							sf.report_filter_id,
							sf.company_user_id,
							sf.company_group_id,
							row_number ( ) OVER ( PARTITION BY rf.id ORDER BY sf.report_filter_id, sf.company_user_id, sf.company_group_id ) AS rank
						FROM
							report_filters rf
							JOIN reports r ON ( r.cid = rf.cid AND r.id = rf.report_id )
							LEFT JOIN shared_filters sf ON ( sf.cid = rf.cid AND sf.report_filter_id = rf.id AND sf.deleted_by IS NULL AND sf.deleted_on IS NULL )
							LEFT JOIN company_users cu ON ( cu.cid = sf.cid AND cu.id = sf.company_user_id AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
							LEFT JOIN company_user_groups cug ON ( cug.cid = sf.cid AND cug.company_group_id = sf.company_group_id )
						WHERE
							rf.cid = ' . ( int ) $intCid . '
							AND rf.report_id = ' . ( int ) $intReportId . '
							AND ( ( COALESCE ( sf.company_user_id, cug.company_user_id ) = ' . ( int ) $intCompanyUserOrGroupId . ' OR rf.is_public IS TRUE )
							OR rf.company_user_id = ' . ( int ) $intCompanyUserOrGroupId . ' )
							AND rf.deleted_by IS NULL
							AND rf.deleted_on IS NULL
							' . $strIsInternalSql . '
							' . $strIsPublicSql . '
						ORDER BY
							' . $strOrderBySql . '
					) AS report_filter_data
				WHERE
					report_filter_data.rank = 1 ';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublicOrSharedReportFiltersByCid( $intCid, $objDatabase, $arrintReportVersionData, $boolPacketAdd, $boolIsFailed, $boolIsIncludePrivate, $intCompanyUserIds ) {
		$strSqlFilterCondition = $strPacketDistinct = $strPacketGroupBy = $strPacketPublicSharedCondition = $strMigrationSql = '';

		if( $boolPacketAdd ) {
			$strPacketDistinct = ' , rf.name,rv.id';
			$strPacketGroupBy = ' , rv.id desc';
		} else {
			$strPacketPublicSharedCondition = 'AND ( 
				rf.is_public = TRUE OR 
				rf.id IN ( SELECT report_filter_id FROM pg_temp.shared_filters_temp )
			)';
			if( $boolIsIncludePrivate ) {
				$strPacketPublicSharedCondition = 'AND rf.is_public = FALSE AND rf.id NOT IN ( SELECT report_filter_id FROM pg_temp.shared_filters_temp )';
				if( valArr( $intCompanyUserIds ) ) {
					$strPacketPublicSharedCondition .= ' AND rf.company_user_id IN ( ' . sqlIntImplode( $intCompanyUserIds ) . ' ) ';
				}
			}
		}
		if( valArr( $arrintReportVersionData ) ) {
			$strSqlFilterCondition = 'AND ( rv.id, rf.id ) IN ( ' . sqlIntImplode( $arrintReportVersionData ) . ' ) ';
		} else {
			$strMigrationSql = ' AND COALESCE( ( rf.filters->>\'hide_from_migration\')::BOOLEAN, FALSE ) = FALSE
				AND COALESCE( ( rf.filters->>\'failed_to_migration\')::BOOLEAN, FALSE ) = FALSE';
			if( $boolIsFailed ) {
				$strMigrationSql = ' AND COALESCE( ( rf.filters->>\'hide_from_migration\')::BOOLEAN, FALSE ) = FALSE
				AND COALESCE( ( rf.filters->>\'failed_to_migration\')::BOOLEAN, FALSE ) = TRUE';
			}
		}

		$strSql = '
		DROP TABLE IF EXISTS pg_temp.history_data;
		CREATE TEMP TABLE pg_temp.history_data AS(
			SELECT
				history.cid,
				history.report_filter_id,
				history.last_run_on,
				COUNT( history.report_filter_id ) AS history_count
			FROM
				(
				SELECT
					DISTINCT rh.cid,
					rh.report_filter_id,
					rh.name,
					rh.queued_datetime,
					MAX( TO_CHAR( rh.created_on, \'MM/DD/YYYY HH24:MI:SS TZ\') ) OVER( PARTITION BY rh.CID, rh.report_filter_id ) AS last_run_on
				FROM
					report_histories AS rh
				WHERE
					rh.cid = ' . ( int ) $intCid . '
					AND rh.created_on > NOW() - INTERVAL \'30 DAY\'
				) AS history
			GROUP BY
				history.cid,
				history.report_filter_id,
				history.last_run_on );

		DROP INDEX IF EXISTS idx_history_data_report_filter_id;
		CREATE INDEX idx_history_data_report_filter_id ON pg_temp.history_data USING btree( report_filter_id );
		ANALYZE pg_temp.history_data;
		
		DROP TABLE IF EXISTS pg_temp.shared_filters_temp; 
		CREATE TEMP TABLE pg_temp.shared_filters_temp AS( 
			SELECT
				sf.report_filter_id
			FROM
				shared_filters AS sf
			WHERE
				sf.cid = ' . ( int ) $intCid . '
			AND sf.filter_type_id = ' . CFilterType::FILTER_TYPE_REPORT . '
		 );

		DROP INDEX IF EXISTS idx_shared_filters_report_filter_id; 
		CREATE INDEX idx_shared_filters_report_filter_id ON pg_temp.shared_filters_temp USING btree( report_filter_id );
		ANALYZE pg_temp.shared_filters_temp; 
		
		DROP TABLE IF EXISTS pg_temp.report_schedules_temp; 
		CREATE TEMP TABLE pg_temp.report_schedules_temp AS( 
		
			SELECT
				COUNT( rs.id ) AS schedule_count,
				rs.cid,
				rs.report_filter_id
			FROM
				report_schedules AS rs
			WHERE
				rs.cid = ' . ( int ) $intCid . '
				AND rs.deleted_by IS NULL
			GROUP BY
				rs.cid,
				rs.report_filter_id
		);

		DROP INDEX IF EXISTS idx_report_schedules_temp_report_filter_id; 
		CREATE INDEX idx_report_schedules_temp_report_filter_id ON pg_temp.report_schedules_temp USING btree( report_filter_id );
		ANALYZE pg_temp.report_schedules_temp; 

		SELECT
			DISTINCT ON ( r.title, rf.id ' . $strPacketDistinct . ' )
			rf.id,
			rf.id AS report_filter_id,
			rf.name,
			default_filter.report_filter_id AS default_filter_id,
			default_filter.report_version_id AS default_filter_version_id,
			rf.filters,
			rf.company_user_id,
			util_get_translated( \'title\', r.title, r.details ) AS title,
			ri.report_id,
			rv.id AS report_version_id,
			rv.major,
			rv.minor,
			r.report_type_id,
			ce.name_first,
			ce.name_last,
			COALESCE( r.description, rf.name ) as description,
			rv.id as report_version_id,
			COALESCE( rep_hist.last_run_on::TEXT, \'-\'::TEXT )::TEXT AS last_run_on,
			COALESCE( rep_hist.history_count::INT, \'0\'::INT ) AS last_30_days_usage_count,
			COUNT( CASE
				WHEN rg.report_group_type_id = ' . CReportGroupType::PACKET . ' AND ri.report_filter_id = rf.id THEN ri.id
				ELSE NULL
			END ) OVER ( PARTITION BY rf.id ) AS packet_count,
			m.title AS report_group,
			COALESCE( rep_sch.schedule_count, 0 ) AS schedule_count,
			rf.filters->>\'failed_reason\' AS failed_reason
		FROM
			report_filters rf
			JOIN reports r ON ( r.cid = rf.cid AND r.id = rf.report_id )
			JOIN default_reports AS dr ON dr.id = r.default_report_id
			JOIN default_report_groups AS drg ON drg.id = dr.default_report_group_id
			JOIN modules AS m ON m.id = drg.parent_module_id
			JOIN report_instances ri ON ( ri.cid = rf.cid AND ri.report_id = rf.report_id )
			JOIN report_groups rg ON ( ri.cid = rg.cid AND ri.report_group_id = rg.id )
			JOIN report_versions rv ON ( ri.cid = rv.cid AND ri.report_version_id = rv.id )
			JOIN company_users cu ON ( cu.cid = rf.cid AND cu.id = rf.company_user_id )
			LEFT JOIN report_instances AS default_filter ON default_filter.cid = rf.cid AND default_filter.report_filter_id = rf.id
			LEFT JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
			LEFT JOIN pg_temp.history_data AS rep_hist ON rep_hist.cid = rf.cid AND rep_hist.report_filter_id = rf.id
			LEFT JOIN pg_temp.report_schedules_temp AS rep_sch ON rep_sch.cid = rf.cid AND rep_sch.report_filter_id = rf.id
		WHERE
			rf.cid = ' . ( int ) $intCid . '
			' . $strPacketPublicSharedCondition . '
			AND rf.deleted_by IS NULL
			AND ri.deleted_by IS NULL
			' . $strSqlFilterCondition . '
			' . $strMigrationSql . '
		ORDER BY 
			r.title,
			rf.id,
			rf.name ' . $strPacketGroupBy . '
		';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportInstanceIdByReportFilterIdByReportIdByCid( $intReportFilterId, $intReportId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ri.report_id
					FROM
						report_instances ri
					JOIN report_groups rg ON ri.cid =  rg.cid AND rg.id = ri.report_group_id
					JOIN report_group_types rgt ON rgt.id =  rg.report_group_type_id
					WHERE
						ri.cid = ' . ( int ) $intCid . '
						AND ri.report_filter_id =  ' . ( int ) $intReportFilterId . '
						AND ri.report_id = ' . ( int ) $intReportId . '
						AND ri.deleted_by IS NULL
						AND ri.deleted_on IS NULL
						AND rg.report_group_type_id = ' . CReportGroupType::PACKET;

		return self::fetchReportFilters( $strSql, $objDatabase );
	}

	/**
	 * @param $intId
	 * @param $intCid
	 * @param $objDatabase
	 * @return CReportFilter|null
	 */
	public static function fetchReportFilterByIdByCid( $intId, $intCid, $objDatabase ) {
		if( false == isset( $intId ) ) return NULL;

		$strSql = '
			SELECT
				*
			FROM
				report_filters
			WHERE
				cid = ' . ( int ) $intCid . '
				AND id = ' . ( int ) $intId;

		return self::fetchReportFilter( $strSql, $objDatabase );
	}

	public static function fetchReportFiltersByReportGroupIdsByCid( $arrintReportGroupIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintReportGroupIds ) ) return [];

		$strSql = '
			SELECT
				rf.*
			FROM
				report_filters rf
				JOIN report_instances ri ON ( ri.cid = rf.cid AND ri.report_id = rf.report_id )
				JOIN report_groups rg ON ( rg.cid = rf.cid AND rg.id = ri.report_group_id AND rg.company_user_id = rf.company_user_id )
				
			WHERE
				rf.cid = ' . ( int ) $intCid . '
				AND rg.id IN ( ' . implode( ',', $arrintReportGroupIds ) . ' )';

		return self::fetchReportFilters( $strSql, $objDatabase );
	}

	public static function fetchReportFiltersByFilterKeyTypeByCid( $strFilterKey, $intCid, $objDatabase ) {
		if( !valStr( $strFilterKey ) ) return [];

		$strSql = '
			SELECT
				*
			FROM
				report_filters AS rf
			WHERE
				rf.cid = ' . ( int ) $intCid . '
				AND rf.filters -> \'report_filter\' -> \'' . $strFilterKey . '\' IS NOT NULL
				AND rf.deleted_by IS NULL
				AND rf.deleted_on IS NULL';

		return self::fetchReportFilters( $strSql, $objDatabase );
	}

	public static function fetchPublicOrSharedReportFiltersByCidByPeriod( $intCid, $objDatabase, $boolIsCountOnly, $strMigrateReportsAccessedIn, $boolIsIncludePrivate ) {

		$strWherePrivateCondition = 'AND ( rf.is_public = TRUE OR rf.id IN ( SELECT report_filter_id FROM pg_temp.shared_filters_temp ) )';
		if( $boolIsIncludePrivate ) {
			$strWherePrivateCondition = 'AND rf.is_public = FALSE AND rf.id NOT IN ( SELECT report_filter_id FROM pg_temp.shared_filters_temp )';
		}

		$strSql = '
		DROP TABLE IF EXISTS pg_temp.shared_filters_temp; 
		CREATE TEMP TABLE pg_temp.shared_filters_temp AS( 
			SELECT
				sf.report_filter_id
			FROM
				shared_filters AS sf
			WHERE
				sf.cid = ' . ( int ) $intCid . '
			AND sf.filter_type_id = ' . CFilterType::FILTER_TYPE_REPORT . '
		 );

		DROP INDEX IF EXISTS idx_shared_filters_report_filter_id; 
		CREATE INDEX idx_shared_filters_report_filter_id ON pg_temp.shared_filters_temp USING btree( report_filter_id );
		ANALYZE pg_temp.shared_filters_temp; 

		DROP TABLE IF EXISTS pg_temp.company_reports_temp;
		CREATE TEMP TABLE pg_temp.company_reports_temp AS(
			SELECT
				DISTINCT ON ( r.title, rf.id )
				rf.id,
				rf.id AS report_filter_id,
				rf.name,
				default_filter.report_filter_id AS default_filter_id,
				default_filter.report_version_id AS default_filter_version_id,
				rf.filters,
				rf.company_user_id,
				util_get_translated( \'title\', r.title, r.details ) AS title,
				ri.report_id,
				rv.id AS report_version_id,
				rv.major,
				rv.minor,
				r.report_type_id,
				ce.name_first,
				ce.name_last,
				COALESCE( r.description, rf.name ) as description,
				m.title AS report_group,
				CASE WHEN report_history.day_diff < 31 THEN 1 ELSE 0 END AS thirty,
				CASE WHEN report_history.day_diff < 61 THEN 1 ELSE 0 END AS sixty,
				CASE WHEN report_history.day_diff < 91 THEN 1 ELSE 0 END AS ninety,
				CASE WHEN report_history.day_diff < 366 THEN 1 ELSE 0 END AS one_year,
				1 AS all_records
			FROM
				report_filters AS rf
				JOIN reports AS r ON ( r.cid = rf.cid AND r.id = rf.report_id )
				JOIN default_reports AS dr ON dr.id = r.default_report_id
				JOIN default_report_groups AS drg ON drg.id = dr.default_report_group_id
				JOIN modules AS m ON m.id = drg.parent_module_id
				JOIN report_instances AS ri ON ( ri.cid = rf.cid AND ri.report_id = rf.report_id )
				JOIN report_groups AS rg ON ( ri.cid = rg.cid AND ri.report_group_id = rg.id )
				JOIN report_versions AS rv ON ( ri.cid = rv.cid AND ri.report_version_id = rv.id )
				JOIN company_users AS cu ON ( cu.cid = rf.cid AND cu.id = rf.company_user_id )
				LEFT JOIN report_instances AS default_filter ON default_filter.cid = rf.cid AND default_filter.report_filter_id = rf.id
				LEFT JOIN company_employees AS ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
				LEFT JOIN LATERAL(
					SELECT
						DATE_PART( \'DAY\', CURRENT_TIMESTAMP - ( MAX( rh.created_on ) OVER( partition by rh.cid, rh.report_filter_id ) )::TIMESTAMP ) AS day_diff
					FROM
						report_histories AS rh
					WHERE
						rh.cid = rf.cid
						AND rh.report_filter_id = rf.id
					ORDER BY
						rh.created_by DESC
					LIMIT 1
				) AS report_history on true
			WHERE
				rf.cid = ' . ( int ) $intCid . '
				' . $strWherePrivateCondition . '
				AND rf.deleted_by IS NULL
				AND ri.deleted_by IS NULL
				AND COALESCE( ( rf.filters->>\'hide_from_migration\')::BOOLEAN, FALSE ) = FALSE
				AND COALESCE( ( rf.filters->>\'failed_to_migration\')::BOOLEAN, FALSE ) = FALSE
			ORDER BY
				r.title,
				rf.id,
				rf.name
		);
		';

		$strSelect = 'crt.*';
		$strWhereCondition = '';
		if( $boolIsCountOnly ) {
			$strSelect = '
				COALESCE( SUM( crt.thirty ), 0 ) AS thirty,
				COALESCE( SUM( crt.sixty ), 0 ) AS sixty,
				COALESCE( SUM( crt.ninety ), 0 ) AS ninety,
				COALESCE( SUM( crt.one_year ), 0 ) AS one_year,
				COALESCE( SUM( crt.all_records ), 0 ) AS all_records';
		}
		if( valStr( $strMigrateReportsAccessedIn ) ) {
			$strWhereCondition = 'WHERE crt.' . $strMigrateReportsAccessedIn . ' = 1';
		}
		$strSql .= '
			SELECT
				' . $strSelect . '
			FROM
				pg_temp.company_reports_temp AS crt
			' . $strWhereCondition . '
			;';

		return fetchData( $strSql, $objDatabase );
	}

}
