<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChores
 * Do not add any new functions to this class.
 */

class CChores extends CBaseChores {

	public static function fetchChoreByIdByCid( $intChoreId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						c.*,
						ct.default_chore_type_id,
						p.post_month,
						p.property_id,
						p.id AS period_id
					FROM
						chores c
						JOIN chore_types ct ON ( c.cid = ct.cid AND c.chore_type_id = ct.id )
						JOIN chore_references cr ON ( c.cid = cr.cid AND c.id = cr.chore_id )
						JOIN periods p ON ( cr.cid = p.cid AND cr.reference_id = p.id AND cr.chore_reference_type_id = ' . CChoreReferenceType::PERIOD_CLOSES . ' )
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND c.id = ' . ( int ) $intChoreId;

		return self::fetchChore( $strSql, $objClientDatabase );
	}

	public static function fetchChoresByReferenceIdsByChoreReferenceTypeIdByChoreCategoryIdsByChoreStatusTypeIdByCid( $arrintReferenceIds, $intChoreRefererenceTypeId, $arrintChoreCategoryIds, $intChoreStatusTypeId, $intCid, $objClientDatabase, $boolIsPendingForAdvance = false ) {

		if( false == valArr( $arrintReferenceIds ) || false == valArr( $arrintChoreCategoryIds ) ) {
			return NULL;
		}

		$strCondition	= ' AND ct.is_required_to_lock_month = 1';
		$strFields		= 'CASE
								WHEN ( c.chore_category_id = ' . CChoreCategory::AR_CLOSE . ' AND pd.post_month < pgs.ar_post_month AND pd.ar_locked_on IS NULL )
									THEN p.property_name
								WHEN ( c.chore_category_id = ' . CChoreCategory::AP_CLOSE . ' AND pd.post_month < pgs.ap_post_month AND pd.ap_locked_on IS NULL )
									THEN p.property_name
								WHEN ( c.chore_category_id = ' . CChoreCategory::GL_CLOSE . ' AND pd.post_month < pgs.gl_post_month AND pd.gl_locked_on IS NULL )
									THEN p.property_name
								ELSE NULL
							END AS property_name';

		if( true == $boolIsPendingForAdvance ) {

			$strCondition	= ' AND ct.is_required_to_advance_month = 1';
			$strFields		= 'CASE
									WHEN ( c.chore_category_id = ' . CChoreCategory::AR_CLOSE . ' AND pd.is_ar_rollback = 0 AND pd.post_month = pgs.ar_post_month )
										THEN p.property_name
									WHEN ( c.chore_category_id = ' . CChoreCategory::AP_CLOSE . ' AND pd.is_ap_rollback = 0 AND pd.post_month = pgs.ap_post_month )
										THEN p.property_name
									WHEN ( c.chore_category_id = ' . CChoreCategory::GL_CLOSE . ' AND pd.is_gl_rollback = 0 AND pd.post_month = pgs.gl_post_month )
										THEN p.property_name
									ELSE NULL
								END AS property_name';
		}

		$strSql = 'SELECT util_set_locale( \'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						util_get_translated( \'name\', cc.name, cc.details ) AS category_name,' .
				  $strFields . '
					FROM
						chores c
						JOIN chore_categories cc ON ( c.chore_category_id = cc.id )
						JOIN chore_types ct ON ( c.cid = ct.cid AND c.chore_type_id = ct.id  )
						JOIN chore_references cr ON ( c.cid = cr.cid AND c.id = cr.chore_id )
						JOIN periods pd ON ( cr.cid = pd.cid AND cr.reference_id = pd.id )
						JOIN properties p ON ( pd.cid = p.cid AND pd.property_id = p.id )
						JOIN property_gl_settings pgs ON ( pgs.cid = pd.cid AND pgs.property_id = pd.property_id )
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND ct.disabled_by IS NULL
						AND cr.reference_id IN ( ' . implode( ',', $arrintReferenceIds ) . ' )
						AND cr.chore_reference_type_id = ' . ( int ) $intChoreRefererenceTypeId . '
						AND c.chore_category_id IN ( ' . implode( ',', $arrintChoreCategoryIds ) . ' )
						AND c.chore_status_type_id = ' . ( int ) $intChoreStatusTypeId .
				  $strCondition . '
					GROUP BY
						c.chore_category_id,
						cc.name,
						cc.details,
						property_name,
						pd.is_ar_rollback,
						pd.is_ap_rollback,
						pd.is_gl_rollback,
						pd.post_month,
						pgs.ar_post_month,
						pgs.ap_post_month,
						pgs.gl_post_month,
						pd.ar_locked_on,
						pd.ap_locked_on,
						pd.gl_locked_on
					ORDER BY
						p.property_name';

		$arrmixData		= fetchData( $strSql, $objClientDatabase );
		$arrmixResult	= NULL;

		foreach( $arrmixData as $arrmixDatum ) {
			if( true == valStr( $arrmixDatum['property_name'] ) ) {
				$arrmixResult[] = $arrmixDatum;
			}
		}

		return $arrmixResult;
	}

	public static function fetchChoresByPropertyIdsByPostMonthByChoreCategoryIdByChoreStatusTypeIdsByCid( $arrintPropertyIds, $strPostMonth, $intChoreCategoryId, $arrintChoreChoreStatusTypeIds, $intCid, $objClientDatabase, $boolIsAdvance = false ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintChoreChoreStatusTypeIds ) || false == valStr( $strPostMonth ) ) {
			return NULL;
		}

		$strCondition = NULL;

		if( $intChoreCategoryId == CChoreCategory::AR_CLOSE ) {
			$strCondition = ' AND p.post_month < pgs.ar_post_month AND p.ar_locked_on IS NULL';
		} elseif( $intChoreCategoryId == CChoreCategory::AP_CLOSE ) {
			$strCondition = ' AND p.post_month < pgs.ap_post_month AND p.ap_locked_on IS NULL';
		} elseif( $intChoreCategoryId == CChoreCategory::GL_CLOSE ) {
			$strCondition = ' AND p.post_month < pgs.gl_post_month AND p.gl_locked_on IS NULL';
		}

		if( true == $boolIsAdvance ) {

			if( $intChoreCategoryId == CChoreCategory::AR_CLOSE ) {
				$strCondition = 'AND p.post_month = pgs.ar_post_month';
			} elseif( $intChoreCategoryId == CChoreCategory::AP_CLOSE ) {
				$strCondition = 'AND p.post_month = pgs.ap_post_month';
			} elseif( $intChoreCategoryId == CChoreCategory::GL_CLOSE ) {
				$strCondition = 'AND p.post_month = pgs.gl_post_month';
			}
		}

		$strSql = 'SELECT
						c.*,
						p.id AS period_id,
						p.property_id,
						pr.property_name,
						ct.name,
						ct.is_required_to_lock_month,
						ct.is_required_to_advance_month,
						ct.default_chore_type_id
					FROM
						chores c
						JOIN chore_references cr ON ( c.cid = cr.cid AND c.id = cr.chore_id )
						JOIN periods p ON ( cr.cid = p.cid AND cr.reference_id = p.id )
						JOIN chore_types ct ON ( c.cid = ct.cid AND c.chore_type_id = ct.id )
						JOIN properties pr ON ( p.cid = pr.cid AND p.property_id = pr.id )
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.property_id )
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.post_month = \'' . $strPostMonth . '\'
						AND c.chore_category_id = ' . ( int ) $intChoreCategoryId . '
						AND c.chore_status_type_id IN ( ' . implode( ', ', $arrintChoreChoreStatusTypeIds ) . ' )
						AND cr.chore_reference_type_id = ' . CChoreReferenceType::PERIOD_CLOSES . '
						AND ct.disabled_by IS NULL
						AND ct.disabled_on IS NULL ' .
				  $strCondition . '
					ORDER BY
						property_name,
						ct.name';

		return self::fetchChores( $strSql, $objClientDatabase );
	}

	public static function fetchPendingLockChoresCountByPropertyIdByPostMonthByCategoriesByCid( $arrintPropertyIds, string $strPostMonth, $arrintChoreCategoryIds, int $intCid, CDatabase $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valStr( $strPostMonth ) ) {
			return NULL;
		}

		$strSql = '
			SELECT
				c.*
			FROM
				chores c
				JOIN chore_references cr ON c.cid = cr.cid AND c.id = cr.chore_id AND cr.chore_reference_type_id = ' . CChoreReferenceType::PERIOD_CLOSES . '
				JOIN periods p ON cr.cid = p.cid AND cr.reference_id = p.id AND p.post_month = \'' . $strPostMonth . '\'
				JOIN chore_types ct ON c.cid = ct.cid AND c.chore_type_id = ct.id AND ct.disabled_by IS NULL AND ct.disabled_on IS NULL AND ct.is_required_to_lock_month = 1
			WHERE
				c.cid = ' . ( int ) $intCid . '
				AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
				AND c.chore_status_type_id = ' . CChoreStatusType::UNUSED . '
				AND c.chore_category_id IN( ' . implode( ',', $arrintChoreCategoryIds ) . ' )';

		return self::fetchChores( $strSql, $objClientDatabase );
	}

	public static function fetchActiveChoreCountByPropertyIdsByPostMonthByChoreStatusTypeIdByCid( $arrintPropertyIds, $strPostMonth, $intChoreStatusTypeId, $intCid, $objClientDatabase, $boolIsAdvance = false ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strFields = 'SUM ( CASE
								WHEN ( c.chore_category_id = ' . CChoreCategory::AR_CLOSE . ' AND pd.post_month < pgs.ar_post_month AND pd.ar_locked_on IS NULL )
									THEN COALESCE( c.item_count, 0 )
								WHEN ( c.chore_category_id = ' . CChoreCategory::AP_CLOSE . ' AND pd.post_month < pgs.ap_post_month AND pd.ap_locked_on IS NULL )
									THEN COALESCE( c.item_count, 0 )
								WHEN ( c.chore_category_id = ' . CChoreCategory::GL_CLOSE . ' AND pd.post_month < pgs.gl_post_month AND pd.gl_locked_on IS NULL )
									THEN COALESCE( c.item_count, 0 )
								ELSE 0
							END ) AS category_chore_count';

		if( true == $boolIsAdvance ) {
			$strFields = 'SUM ( CASE
									WHEN ( c.chore_category_id = ' . CChoreCategory::AR_CLOSE . ' AND pd.is_ar_rollback = 0 AND pd.post_month = pgs.ar_post_month )
										THEN COALESCE( c.item_count, 0 )
									WHEN ( c.chore_category_id = ' . CChoreCategory::AP_CLOSE . ' AND pd.is_ap_rollback = 0 AND pd.post_month = pgs.ap_post_month )
										THEN COALESCE( c.item_count, 0 )
									WHEN ( c.chore_category_id = ' . CChoreCategory::GL_CLOSE . ' AND pd.is_gl_rollback = 0 AND pd.post_month = pgs.gl_post_month )
										THEN COALESCE( c.item_count, 0 )
									ELSE 0
								END ) AS category_chore_count';
		}

		$strSql = 'SELECT
					c.chore_category_id, ' .
				  $strFields . '
					FROM
						chores c
						JOIN chore_references cr ON ( cr.cid = c.cid AND c.id = cr.chore_id )
						JOIN periods pd ON ( pd.cid = cr.cid AND pd.id = cr.reference_id AND cr.chore_reference_type_id = ' . CChoreReferenceType::PERIOD_CLOSES . ' )
						JOIN property_gl_settings pgs ON ( pgs.cid = pd.cid AND pgs.property_id = pd.property_id )
						JOIN chore_types ct ON ( ct.cid = c.cid AND ct.id = c.chore_type_id )
					WHERE
						pd.cid = ' . ( int ) $intCid . '
						AND pd.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND DATE_TRUNC( \'month\', pd.post_month ) = \'' . $strPostMonth . '\'
						AND c.chore_status_type_id = ' . ( int ) $intChoreStatusTypeId . '
						AND ct.disabled_on IS NULL
					GROUP BY
						c.chore_category_id';

		$arrmixData		= fetchData( $strSql, $objClientDatabase );
		$arrintResults	= NULL;

		foreach( $arrmixData as $arrmixDatum ) {
			$arrintResults[$arrmixDatum['chore_category_id']] = $arrmixDatum['category_chore_count'];
		}

		return $arrintResults;
	}

	public static function fetchChoreItemCountsByPeriodClosingFilterByCompanyUserIdByCid( $objPeriodClosingFilter, $intCompanyUserId, $intCid, $objClientDatabase, $boolIsAdministrator = false, $arrintDefaultChoreTypeIds ) {

		if( false == valArr( $objPeriodClosingFilter->getPeriodIds() ) || false == valArr( $arrintDefaultChoreTypeIds ) ) {
			return NULL;
		}

		$arrintResults		= NULL;
		$arrintChoreTypeIds	= [
			CDefaultChoreType::MOVE_INS_PROCESSED,
			CDefaultChoreType::FMO_PROCESSED,
			CDefaultChoreType::PAYMENT_APPROVED,
			CDefaultChoreType::INVOICES_APPROVED,
			CDefaultChoreType::TRANSFERS_PROCESSED,
			CDefaultChoreType::MOVE_OUTS_PROCESSED,
			CDefaultChoreType::PURCHSE_ORDER_APPROVED,
			CDefaultChoreType::APPLICANT_RENT,
			CDefaultChoreType::REFUND_DEPOSIT_INTEREST
		];

		if( true == valArr( array_intersect( $arrintChoreTypeIds, $arrintDefaultChoreTypeIds ) ) ) {

			$strArEndDateCondition = 'LEAST ( p.ar_end_date, CURRENT_DATE )';
			$strApEndDateCondition = 'LEAST ( p.ap_end_date, CURRENT_DATE )';

			if( true == valStr( $objPeriodClosingFilter->getEndDate() ) ) {
				$strArEndDateCondition = $strApEndDateCondition = '\'' . $objPeriodClosingFilter->getEndDate() . '\'';
			}

			$strSqlCte   = '';
			$strSqlJoins = '';

			if( true == in_array( CDefaultChoreType::PURCHSE_ORDER_APPROVED, $arrintDefaultChoreTypeIds ) || true == in_array( CDefaultChoreType::INVOICES_APPROVED, $arrintDefaultChoreTypeIds ) ) {
				$strSqlCte .= 'company_user_properties AS ( ' . \Psi\Eos\Entrata\CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator ) . ' ),';
			}

			if( true == in_array( CDefaultChoreType::MOVE_INS_PROCESSED, $arrintDefaultChoreTypeIds ) || true == in_array( CDefaultChoreType::MOVE_OUTS_PROCESSED, $arrintDefaultChoreTypeIds ) || true == in_array( CDefaultChoreType::FMO_PROCESSED, $arrintDefaultChoreTypeIds ) ) {
				$strSqlCte   .= 'all_leases AS (
								SELECT
									COUNT( DISTINCT cl.id ) AS chore_task_count,
									cl.property_id,
									CASE
										WHEN cl.lease_status_type_id = ' . ( int ) CLeaseStatusType::FUTURE . '
										THEN ' . CDefaultChoreType::MOVE_INS_PROCESSED . '
										WHEN cl.lease_status_type_id = ' . ( int ) CLeaseStatusType::NOTICE . '
										THEN ' . CDefaultChoreType::MOVE_OUTS_PROCESSED . '
										WHEN cl.lease_status_type_id = ' . ( int ) CLeaseStatusType::PAST . '
										THEN ' . CDefaultChoreType::FMO_PROCESSED . '
										ELSE NULL
									END AS default_chore_type_id
								FROM
									cached_leases cl
									JOIN periods p ON ( cl.cid = p.cid AND cl.property_id = p.property_id )
								WHERE
									cl.cid = ' . ( int ) $intCid . '
									AND p.id IN ( ' . implode( ',', $objPeriodClosingFilter->getPeriodIds() ) . ' )
									AND ( ( cl.lease_status_type_id = ' . CLeaseStatusType::FUTURE . '
												AND cl.move_in_date <= ' . $strArEndDateCondition . '
											) OR ( cl.lease_status_type_id = ' . CLeaseStatusType::NOTICE . '
													AND cl.move_out_date <= ' . $strArEndDateCondition . '
													AND cl.property_unit_id IS NOT NULL
													AND cl.unit_space_id IS NOT NULL
											) OR ( cl.lease_status_type_id = ' . CLeaseStatusType::PAST . '
													AND cl.fmo_started_on IS NULL
													AND cl.move_out_date <= ' . $strArEndDateCondition . '
													AND cl.property_unit_id IS NOT NULL
													AND cl.unit_space_id IS NOT NULL )
										)
								GROUP BY
									cl.property_id,
									default_chore_type_id
							), ';
				$strSqlJoins .= 'SELECT * FROM all_leases UNION ALL ';
			}

			if( true == in_array( CDefaultChoreType::TRANSFERS_PROCESSED, $arrintDefaultChoreTypeIds ) ) {
				$strSqlCte   .= 'lease_transfers AS (
								SELECT
									COUNT ( DISTINCT cl.id ) AS chore_task_count,
									cl.property_id,'
								. CDefaultChoreType::TRANSFERS_PROCESSED . ' AS default_chore_type_id
								FROM
									cached_leases cl
									JOIN cached_leases clt ON ( cl.cid = clt.cid AND cl.transfer_lease_id = clt.id )
									JOIN cached_applications ca ON ( ca.cid = clt.cid AND ca.lease_id = clt.id AND ca.unit_space_id IS NOT NULL AND ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::TRANSFER . ', ' . CLeaseIntervalType::RENEWAL . ' ) AND ca.application_stage_id = ' . CApplicationStage::LEASE . ' AND ca.application_status_id = ' . CApplicationStatus::APPROVED . ' )
									JOIN periods p ON ( cl.cid = p.cid AND cl.property_id = p.property_id )
								WHERE
									cl.cid = ' . ( int ) $intCid . '
									AND p.id IN ( ' . implode( ',', $objPeriodClosingFilter->getPeriodIds() ) . ' )
									AND cl.lease_status_type_id = ' . CLeaseStatusType::NOTICE . '
									AND cl.transferred_on IS NULL
									AND cl.move_out_date <= ' . $strArEndDateCondition . '
								GROUP BY
									cl.property_id,
									default_chore_type_id
							), ';
				$strSqlJoins .= 'SELECT * FROM lease_transfers UNION ALL ';
			}

			if( true == in_array( CDefaultChoreType::PURCHSE_ORDER_APPROVED, $arrintDefaultChoreTypeIds ) ) {
				$strSqlCte   .= 'purchase_order_headers AS (
								SELECT
									COUNT( DISTINCT ah.id ) AS chore_task_count,
									ad.property_id,'
								. CDefaultChoreType::PURCHSE_ORDER_APPROVED . ' AS default_chore_type_id
								FROM
									ap_headers ah
									JOIN ap_payees ap ON ( ap.cid = ah.cid AND ap.id = ah.ap_payee_id )
									JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
									JOIN periods p ON ( p.cid = ad.cid AND p.property_id = ad.property_id )
								WHERE
									ah.cid = ' . ( int ) $intCid . '
									AND ah.ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
									AND ah.ap_financial_status_type_id = ' . CApFinancialStatusType::PENDING . '
									AND ah.is_template = FALSE
									AND ah.deleted_by IS NULL
									AND p.id IN ( ' . implode( ',', $objPeriodClosingFilter->getPeriodIds() ) . ' )
									AND ah.post_date <= ' . $strApEndDateCondition . '
									AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . '
									AND ah.id NOT IN ( SELECT
													DISTINCT ah.id
												FROM
													ap_headers ah
													JOIN ap_details ad  ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
													LEFT JOIN company_user_properties cup ON ( cup.cid = ad.cid AND cup.id = ad.property_id )
												WHERE
													ad.cid = ' . ( int ) $intCid . '
													AND ah.ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
													AND ( cup.is_disabled = 1 OR cup.id IS NULL )
											)
								GROUP BY
									ad.property_id,
									default_chore_type_id
							), ';
				$strSqlJoins .= 'SELECT * FROM purchase_order_headers UNION ALL ';
			}

			if( true == in_array( CDefaultChoreType::INVOICES_APPROVED, $arrintDefaultChoreTypeIds ) ) {
				$strSqlCte   .= 'ap_custom_headers AS (
								SELECT
									COUNT ( DISTINCT ah.id ) AS chore_task_count,
									ad.property_id,
									' . CDefaultChoreType::INVOICES_APPROVED . ' AS default_chore_type_id
								FROM
									ap_headers ah
									JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL )
									JOIN periods p ON ( ad.cid = p.cid AND ad.property_id = p.property_id AND ah.post_month <= p.post_month )
									LEFT JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
								WHERE
									ah.cid = ' . ( int ) $intCid . '
									AND p.id IN ( ' . implode( ',', $objPeriodClosingFilter->getPeriodIds() ) . ' )
									AND ah.is_batching = FALSE
									AND ah.is_template = FALSE
									AND ah.ap_payment_id IS NULL
									AND ah.reversal_ap_header_id IS NULL
									AND ah.is_posted = false
									AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
									AND COALESCE ( ah.ap_financial_status_type_id, 0 ) NOT IN ( ' . CApFinancialStatusType::REJECTED . ', ' . CApFinancialStatusType::DELETED . ' )
									AND ( ap.ap_payee_status_type_id <> ' . CApPayeeStatusType::INACTIVE . ' OR ah.lease_customer_id IS NOT NULL )
									AND ah.id NOT IN ( SELECT
													DISTINCT ah.id
												FROM
													ap_headers ah
													JOIN ap_details ad  ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
													LEFT JOIN company_user_properties cup ON ( cup.cid = ad.cid AND cup.id = ad.property_id )
												WHERE
													ad.cid = ' . ( int ) $intCid . '
													AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
													AND ( cup.is_disabled = 1 OR cup.id IS NULL )
											)
						GROUP BY
							ad.property_id,
							default_chore_type_id
					), ';
				$strSqlJoins .= 'SELECT * FROM ap_custom_headers UNION ALL ';
			}

			if( true == in_array( CDefaultChoreType::PAYMENT_APPROVED, $arrintDefaultChoreTypeIds ) ) {
				$strSqlCte   .= 'approved_payments AS (
								SELECT
									COUNT ( DISTINCT ad.ap_header_id ) AS chore_task_count,
									ad.property_id,
									' . CDefaultChoreType::PAYMENT_APPROVED . ' AS default_chore_type_id
								FROM
									ap_details ad
									JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
									JOIN gl_trees gt ON ( ah.cid = gt.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
									JOIN gl_account_trees gat ON ( gt.cid = gat.cid AND gat.gl_tree_id = gt.id )
									JOIN gl_accounts ga ON ( gat.cid = ga.cid AND ga.id = gat.gl_account_id AND ga.cid = ad.cid AND ga.id = ad.gl_account_id )
									JOIN bank_accounts ba ON ( ad.cid = ba.cid AND ad.bank_account_id = ba.id )
									LEFT JOIN ap_payees ap ON ( ap.cid = ad.cid AND ap.id = ah.ap_payee_id )
									JOIN periods p ON ( ad.cid = p.cid AND ad.property_id = p.property_id AND ad.post_month <= p.post_month )
									LEFT JOIN (
												SELECT
													ad.id,
													ad.cid,
													ad.pre_approval_amount - ABS( COALESCE ( SUM( aa.allocation_amount ), 0::NUMERIC ) ) AS approved_amount_due
												FROM
													ap_details ad
													JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
													LEFT JOIN ap_allocations aa ON ( aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id AND aa.is_deleted = false )
												WHERE
													ad.cid =  ' . ( int ) $intCid . '
													AND ad.transaction_amount > 0
													AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
													AND ad.deleted_by IS NULL
													AND ad.deleted_on IS NULL
												GROUP BY
													ad.id,
													ad.cid
												UNION
												SELECT
													ad.id,
													ad.cid,
													ad.pre_approval_amount + ABS( COALESCE ( SUM( aa.allocation_amount ), 0::NUMERIC ) ) AS approved_amount_due
												FROM
													ap_details ad
													JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
													LEFT JOIN ap_allocations aa ON ( aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id AND aa.is_deleted = false )
												WHERE
													ad.cid =  ' . ( int ) $intCid . '
													AND ad.transaction_amount <= 0
													AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
													AND ad.deleted_by IS NULL
													AND ad.deleted_on IS NULL
												GROUP BY
													ad.id,
													ad.cid
												) sub ON ( sub.cid = ad.cid AND sub.id = ad.id )
								WHERE
									ad.cid = ' . ( int ) $intCid . '
									AND p.id IN ( ' . implode( ',', $objPeriodClosingFilter->getPeriodIds() ) . ' )
									AND ad.payment_approved_on::DATE <= ' . $strApEndDateCondition . '
									AND ad.ap_header_id NOT IN (
																	SELECT
																		DISTINCT ad.ap_header_id
																	FROM
																		ap_details ad
																		JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
																		JOIN properties p ON ( ad.property_id = p.id AND ad.cid = p.cid )
																	WHERE
																		ad.cid = ' . ( int ) $intCid . '
																		AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
																		AND p.is_disabled = 1
																		AND ad.deleted_by IS NULL
																		AND ad.deleted_on IS NULL
																)
									AND ah.is_temporary = false
									AND ah.is_posted = true
									AND ah.ap_payment_id IS NULL
									AND ad.reversal_ap_detail_id IS NULL
									AND ad.payment_approved_by IS NOT NULL
									AND ad.payment_approved_on IS NOT NULL
									AND sub.approved_amount_due <> 0
									AND ad.transaction_amount_due <> 0
									AND ad.is_approved_for_payment = false
									AND ah.is_on_hold = false
									AND ( ap.ap_payee_status_type_id IS NULL OR ap.ap_payee_status_type_id <> ' . CApPayeeStatusType::LOCKED . ' )
									AND ba.is_credit_card_account = 0
								 GROUP BY
									ad.property_id,
									default_chore_type_id
							), ';
				$strSqlJoins .= 'SELECT * FROM approved_payments UNION ALL ';
			}

			if( true == in_array( CDefaultChoreType::APPLICANT_RENT, $arrintDefaultChoreTypeIds ) ) {
				$strSqlCte   .= 'applicant_rent AS (
								SELECT
									sum ( sub.count_of_trans_ids ) AS chore_task_count,
									sub.property_id,
									' . CDefaultChoreType::APPLICANT_RENT . ' AS default_chore_type_id
								FROM
								(
									SELECT
										at.cid,
										at.property_id,
										at.lease_id,
										count ( at.id ) AS count_of_trans_ids
									FROM
										ar_transactions at
										JOIN cached_lease_logs cll ON ( cll.cid = at.cid AND cll.lease_id = at.lease_id )
										LEFT JOIN ar_transactions at_reversal ON ( at_reversal.cid = at.cid AND at.ar_transaction_id = at_reversal.id AND at.id = at_reversal.ar_transaction_id AND at.post_month = at_reversal.post_month AND at.is_deleted = TRUE )
									WHERE
										at.ar_code_type_id = ' . CArCodeType::RENT . '
										AND at.period_id IN ( ' . sqlIntImplode( $objPeriodClosingFilter->getPeriodIds() ) . ' )
										AND at.cid = ' . ( int ) $intCid . '
										AND at.property_id IN ( ' . sqlIntImplode( $objPeriodClosingFilter->getPropertyIds() ) . ' )
										AND cll.unit_space_id IS NULL
										AND at.post_month BETWEEN cll.reporting_post_month
										AND cll.apply_through_post_month
										AND cll.is_post_month_ignored = 0
										AND cll.occupancy_type_id IN ( ' . sqlIntImplode( COccupancyType::$c_arrintIncludedOccupancyTypesInReports ) . ' )
										AND at_reversal.id IS NULL
									GROUP BY
										at.cid,
										at.property_id,
										at.lease_id
									HAVING
										sum ( at.transaction_amount ) <> 0
									) AS sub
								GROUP BY
								sub.cid,
								sub.property_id
							), ';
				$strSqlJoins .= 'SELECT * FROM applicant_rent UNION ALL ';
			}

			if( true == in_array( CDefaultChoreType::REFUND_DEPOSIT_INTEREST, $arrintDefaultChoreTypeIds ) ) {
				$strSqlCte .= 'refund_deposit_interest AS (
								SELECT
									COUNT ( DISTINCT ah.id ) AS chore_task_count,
									art.property_id,
									' . CDefaultChoreType::REFUND_DEPOSIT_INTEREST . ' AS default_chore_type_id
								FROM
									ap_headers ah
									JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month )
									JOIN periods p ON ( ad.cid = p.cid AND ad.property_id = p.property_id AND ad.post_month <= p.post_month )
									JOIN ar_allocations aa ON ( ad.cid = aa.cid AND ad.refund_ar_transaction_id = aa.credit_ar_transaction_id )
									JOIN ar_transactions art ON ( aa.cid = art.cid AND aa.credit_ar_transaction_id = art.id AND art.ar_trigger_id =' . CArTrigger::DEPOSIT_INTEREST . ' )
									JOIN property_interest_formulas pif ON ( pif.cid = art.cid AND pif.property_id = art.property_id AND pif.calculate_interest = true AND ( pif.effective_through_date IS NULL OR pif.effective_through_date >= CURRENT_DATE ) )
								WHERE
									art.cid = ' . ( int ) $intCid . '
									AND art.property_id IN ( ' . implode( ',', $objPeriodClosingFilter->getPropertyIds() ) . ' )
									AND p.id IN ( ' . implode( ',', $objPeriodClosingFilter->getPeriodIds() ) . ' )
									AND pif.interest_formula_type_id = ' . CInterestFormulaType::DEPOSIT_INTEREST . '
									AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
									AND ah.reversal_ap_header_id IS NULL
									AND COALESCE( ah.ap_financial_status_type_id, 0 ) <> ' . CApFinancialStatusType::DELETED . '
									AND ad.transaction_amount_due <> 0
									AND ad.deleted_by IS NULL
									AND ad.deleted_on IS NULL
								GROUP BY
									art.property_id,
									default_chore_type_id
							), ';
				$strSqlJoins .= 'SELECT * FROM refund_deposit_interest UNION ALL ';
			}

			$strSqlCte   = rtrim( $strSqlCte, ', ' );
			$strSqlJoins = rtrim( $strSqlJoins, 'UNION ALL ' );

			$strSql = 'WITH 
						' . $strSqlCte . '
						' . $strSqlJoins;

			$arrmixData    = fetchData( $strSql, $objClientDatabase );

			foreach( $arrmixData as $arrmixDatum ) {
				$arrintResults[$arrmixDatum['property_id']][$arrmixDatum['default_chore_type_id']] = $arrmixDatum['chore_task_count'];
			}
		}

		if( false == in_array( CDefaultChoreType::POST_DEPOSIT_INTEREST, $arrintDefaultChoreTypeIds ) ) {
			return $arrintResults;
		}

		// post_deposit_interest collect count of post detposit interest.
		$strPostWindowEndDate = NULL;

		if( true == valStr( $objPeriodClosingFilter->getEndDate() ) ) {
			$strPostWindowEndDate = $objPeriodClosingFilter->getEndDate();
		} else {
			$strPostWindowEndDate = date( 'm', strtotime( $objPeriodClosingFilter->getPostMonth() ) ) . '/' . date( 't', strtotime( $objPeriodClosingFilter->getPostMonth() ) ) . '/' . date( 'Y', strtotime( $objPeriodClosingFilter->getPostMonth() ) );
		}

		$arrmixLeasePostingDetails = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchDepositInterestsEligiblePropertiesByCid( $intCid, $objClientDatabase, NULL, NULL, false, $objPeriodClosingFilter->getPropertyIds(), $strPostWindowEndDate );

		foreach( $arrmixLeasePostingDetails as $arrmixLeasePostingDetail ) {
			$arrintResults[$arrmixLeasePostingDetail['property_id']][CDefaultChoreType::POST_DEPOSIT_INTEREST] = $arrmixLeasePostingDetail['lease_count'];
		}

		return $arrintResults;
	}

	public static function fetchCompletedChoresCountByReferenceIdsByChoreCategoryIdsByCid( $arrintReferenceIds, $arrintChoreCategoryIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintReferenceIds ) || false == valArr( $arrintChoreCategoryIds ) ) return NULL;

		$strSql = ' SELECT
						cr.reference_id,
						c.chore_category_id,
						SUM( CASE WHEN c.chore_status_type_id = ' . CChoreStatusType::COMPLETED . ' THEN 1 ELSE 0 END ) AS completed_chores,
						COUNT(c.id) AS total_chores
					FROM
						chores c
						JOIN chore_references cr ON ( cr.cid = c.cid AND cr.chore_id = c.id )
						JOIN chore_types ct ON ( ct.cid = c.cid AND c.chore_type_id = ct.id )
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND c.chore_category_id IN( ' . implode( ',', $arrintChoreCategoryIds ) . ' )
						AND cr.reference_id IN ( ' . implode( ',', $arrintReferenceIds ) . ' )
						AND cr.chore_reference_type_id = ' . CChoreReferenceType::PERIOD_CLOSES . '
						AND ct.disabled_on IS NULL
					GROUP BY
						cr.reference_id,
						c.chore_category_id';

		$arrmixChoreDetails = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixChoreDetails ) ) {
			$arrmixRekeyedDataRows = rekeyArray( 'reference_id', $arrmixChoreDetails, false, true );
			foreach( $arrmixRekeyedDataRows as $intReferenceId => $arrmixRekeyedDataRow ) {
				$arrmixRekeyedDataRow = rekeyArray( 'chore_category_id', $arrmixRekeyedDataRow );
				$arrmixData[$intReferenceId] = $arrmixRekeyedDataRow;
			}
			return $arrmixData;
		}

		return NULL;
	}

	public static function fetchChoresByReferenceIdByChoreCategoryIdByChoreStatusTypeIdsByCid( $intReferenceId, $intChoreCategoryId, $arrintChoreChoreStatusTypeIds, $intCid, $objClientDatabase ) {

		if( false == valId( $intReferenceId ) || false == valArr( $arrintChoreChoreStatusTypeIds ) ) return NULL;

		if( $intChoreCategoryId == CChoreCategory::AR_CLOSE ) {
			$strCondition = ' AND p.post_month <= pgs.ar_post_month';
		} elseif( $intChoreCategoryId == CChoreCategory::AP_CLOSE ) {
			$strCondition = ' AND p.post_month <= pgs.ap_post_month';
		} elseif( $intChoreCategoryId == CChoreCategory::GL_CLOSE ) {
			$strCondition = ' AND p.post_month <= pgs.gl_post_month';
		}

		$strSql = 'SELECT
						c.*,
						p.id AS period_id,
						p.property_id,
						pr.property_name,
						ct.name,
						ct.is_required_to_lock_month,
						ct.is_required_to_advance_month,
						ct.default_chore_type_id
					FROM
						chores c
						JOIN chore_references cr ON ( c.cid = cr.cid AND c.id = cr.chore_id )
						JOIN periods p ON ( cr.cid = p.cid AND cr.reference_id = p.id )
						JOIN chore_types ct ON ( c.cid = ct.cid AND c.chore_type_id = ct.id )
						JOIN properties pr ON ( p.cid = pr.cid AND p.property_id = pr.id )
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.property_id )
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND cr.reference_id = ' . ( int ) $intReferenceId . '
						AND c.chore_category_id = ' . ( int ) $intChoreCategoryId . '
						AND c.chore_status_type_id IN ( ' . implode( ', ', $arrintChoreChoreStatusTypeIds ) . ' )
						AND cr.chore_reference_type_id = ' . CChoreReferenceType::PERIOD_CLOSES . '
						AND ct.disabled_by IS NULL
						AND ct.disabled_on IS NULL ' .
				  $strCondition . '
					ORDER BY
						property_name,
						ct.name';

		return self::fetchChores( $strSql, $objClientDatabase );
	}

	public static function fetchChoresByPropertyIdByReferenceIdsByChoreReferenceTypeIdByChoreCategoryIdsByChoreStatusTypeIdByCid( $arrintReferenceIds, $intChoreRefererenceTypeId, $arrintChoreCategoryIds, $intChoreStatusTypeId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintReferenceIds ) || false == valArr( $arrintChoreCategoryIds ) || false == valId( $intChoreRefererenceTypeId ) || false == valId( $intChoreStatusTypeId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT util_set_locale( \'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						util_get_translated( \'name\', cc.name, cc.details ) AS category_name,
						CASE
							WHEN ( c.chore_category_id = ' . CChoreCategory::AR_CLOSE . ' AND pd.post_month < pgs.ar_post_month AND pd.ar_locked_on IS NULL )
								THEN pd.post_month
							WHEN ( c.chore_category_id = ' . CChoreCategory::AP_CLOSE . ' AND pd.post_month < pgs.ap_post_month AND pd.ap_locked_on IS NULL )
								THEN pd.post_month
							WHEN ( c.chore_category_id = ' . CChoreCategory::GL_CLOSE . ' AND pd.post_month < pgs.gl_post_month AND pd.gl_locked_on IS NULL )
								THEN pd.post_month
							ELSE NULL
						END AS post_month
					FROM
						chores c
						JOIN chore_categories cc ON ( c.chore_category_id = cc.id )
						JOIN chore_types ct ON ( c.cid = ct.cid AND c.chore_type_id = ct.id  )
						JOIN chore_references cr ON ( c.cid = cr.cid AND c.id = cr.chore_id )
						JOIN periods pd ON ( cr.cid = pd.cid AND cr.reference_id = pd.id )
						JOIN property_gl_settings pgs ON ( pgs.cid = pd.cid AND pgs.property_id = pd.property_id )
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND ct.disabled_on IS NULL
						AND cr.reference_id IN ( ' . implode( ',', $arrintReferenceIds ) . ' )
						AND cr.chore_reference_type_id = ' . ( int ) $intChoreRefererenceTypeId . '
						AND c.chore_category_id IN ( ' . implode( ',', $arrintChoreCategoryIds ) . ' )
						AND c.chore_status_type_id = ' . ( int ) $intChoreStatusTypeId . '
						AND ct.is_required_to_lock_month = 1
					GROUP BY
						c.chore_category_id,
						cc.name,
						cc.details,
						pd.post_month,
						pgs.ar_post_month,
						pgs.ap_post_month,
						pgs.gl_post_month,
						pd.ar_locked_on,
						pd.ap_locked_on,
						pd.gl_locked_on
					ORDER BY
						pd.post_month';

		$arrmixData		= fetchData( $strSql, $objClientDatabase );
		$arrmixResult	= NULL;

		foreach( $arrmixData as $arrmixDatum ) {
			if( true == valStr( $arrmixDatum['post_month'] ) ) {
				$arrmixResult[] = $arrmixDatum;
			}
		}

		return $arrmixResult;
	}

}
?>