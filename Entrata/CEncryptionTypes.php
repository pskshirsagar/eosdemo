<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEncryptionTypes
 * Do not add any new functions to this class.
 */

class CEncryptionTypes extends CBaseEncryptionTypes {

	public static function fetchEncryptionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEncryptionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchEncryptionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEncryptionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedEncryptionTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM encryption_types WHERE is_published = true';
		return self::fetchEncryptionTypes( $strSql, $objDatabase );
	}

}
?>