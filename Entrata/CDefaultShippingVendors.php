<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultShippingVendors
 * Do not add any new functions to this class.
 */

class CDefaultShippingVendors extends CBaseDefaultShippingVendors {

	public static function fetchDefaultShippingVendors( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CDefaultShippingVendor::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDefaultShippingVendor( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CDefaultShippingVendor::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDefaultShippingVendorsByPropertyCountryCode( $strPropertyCountryCode, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						default_shipping_vendors
					WHERE
						details->>\'assigned_country_codes\' = \'' . $strPropertyCountryCode . '\'
						OR details->>\'assigned_country_codes\' IS NULL
						OR details->\'assigned_country_codes\' @> \'["' . $strPropertyCountryCode . '"]\'
					ORDER BY
						( CASE WHEN details->>\'assigned_country_codes\' IS NOT NULL THEN 1 ELSE 2 END ), order_num';

		return self::fetchDefaultShippingVendors( $strSql, $objDatabase );
	}

	public static function fetchDefaultShippingVendorsByPropertyCountryCodeByIsPublishByIsDisabled( $strPropertyCountryCode, $arrintListOfDisabledVendors, $objDatabase ) {

		if( true == valStr( implode( ',', $arrintListOfDisabledVendors ) ) ) {
			$strSqlWhere = 'AND id NOT IN (' . implode( ',', $arrintListOfDisabledVendors ) . ')';
		} else {
			$strSqlWhere = '';
		}
		$strSql = 'SELECT
            *
          FROM
            default_shipping_vendors
          WHERE
            ( details->>\'assigned_country_codes\' = \'' . $strPropertyCountryCode . '\'
            OR details->>\'assigned_country_codes\' IS NULL 
			OR details->\'assigned_country_codes\' @> \'["' . $strPropertyCountryCode . '"]\' )
            ' . $strSqlWhere . '
            AND is_published = 1
          ORDER BY name ASC';

		return self::fetchDefaultShippingVendors( $strSql, $objDatabase );
	}

}
?>