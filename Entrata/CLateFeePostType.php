<?php

class CLateFeePostType extends CBaseLateFeePostType {

	const SUM_OF_BOTH			= 1;
	const WHICHEVER_IS_GREATER	= 2;
	const WHICHEVER_IS_LESS		= 3;

}
?>