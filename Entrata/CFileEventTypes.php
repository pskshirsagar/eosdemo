<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileEventTypes
 * Do not add any new functions to this class.
 */

class CFileEventTypes extends CBaseFileEventTypes {

    public static function fetchFileEventTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CFileEventType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchFileEventType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CFileEventType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchFileEventTypeById( $intId, $objDatabase ) {
	    $strSql = 'SELECT 
	                fet.id,
	                fet.name,
	                fet.description,
	                fet.is_published,
	                fet.order_num,
	                fet.details
	               FROM
	                file_event_types fet 
	               WHERE
	                fet.id = ' . ( int ) $intId . ' ';
	  	    return fetchData( $strSql, $objDatabase );
    }

}
?>