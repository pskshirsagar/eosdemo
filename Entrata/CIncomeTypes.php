<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIncomeTypes
 * Do not add any new functions to this class.
 */

class CIncomeTypes extends CBaseIncomeTypes {

	public static function fetchIncomeTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CIncomeType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchIncomeType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CIncomeType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllIncomeTypes( $objDatabase ) {
		return self::fetchIncomeTypes( 'SELECT * FROM income_types ORDER BY order_num, name', $objDatabase );
	}

	public static function fetchPublishedIncomeTypes( $objDatabase, $boolIsChangeSelfEmployerOrderBy = false ) {
		if( false == $boolIsChangeSelfEmployerOrderBy ) {
			$strOrderBy = ' order_num, name';
		} else {
			$strOrderBy = ' CASE id WHEN ' . CIncomeType::SELF_EMPLOYER . ' THEN 0 ELSE order_num END';
		}

		return self::fetchIncomeTypes( 'SELECT * FROM income_types WHERE is_published = 1 ORDER BY' . $strOrderBy, $objDatabase );
	}

	public static function fetchCustomIncomeTypes( $boolIsSubsidy, $objDatabase ) {

		$strWhereSql = ' AND itg.is_subsidy = ' . intval( $boolIsSubsidy ) . '::boolean';

		$strSql = 'SELECT
					    it.id,
					    it.name AS income_type_name,
					    itg.id as group_id,
					    itg.description AS income_type_group_name
					FROM
					    income_types AS it
					    JOIN income_type_groups AS itg ON it.income_type_group_id = itg.id
					WHERE
						itg.is_published = TRUE
					    ' . $strWhereSql . '
					ORDER BY
					    itg.order_num,
					    it.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllHudIncomeTypes( $objDatabase ) {
		return self::fetchIncomeTypes( 'SELECT * FROM income_types WHERE hud_code IS NOT NULL', $objDatabase );
	}

}
?>
