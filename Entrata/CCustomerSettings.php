<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerSettings
 * Do not add any new functions to this class.
 */

class CCustomerSettings extends CBaseCustomerSettings {

	public static function fetchCustomerSettingCountByKeyByCustomerIdByCid( $intCustomerId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
    					count ( 1 ) AS count
					FROM
						customer_settings cs
    					JOIN clients c ON c.id = cs.cid
					 WHERE
					 	cs.key = \'opt_out_of_ach_conversion\'
					 	AND cs.customer_id = ' . ( int ) $intCustomerId . '
					 	AND cs.cid = ' . ( int ) $intCid;

		$arrstrData = fetchData( $strSql, $objClientDatabase );

		if( true == isset ( $arrstrData[0]['count'] ) ) {
			return ( int ) $arrstrData[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchSimpleAllowedIosRentNotificationCustomersByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						cs.customer_id,
					    cs.value
					 FROM
						customer_settings cs
					    JOIN customer_settings css ON (css.customer_id = cs.customer_id
														 AND css.cid = cs.cid
				 										 AND ( css.key = \'PUSH_NOTIFICATION_RENT_REMINDER_ALLOWED\'
															   AND css.value = \'1\' ) )
					    JOIN clients c ON c.id = cs.cid
					 WHERE
						cs.key = \'PUSH_NOTIFICATION_IOS_DEVICE_TOKENS\'
						AND cs.value IS NOT NULL AND cs.value != \'\'
						AND cs.cid = ' . ( int ) $intCid . ' ;';

		$arrstrData = fetchData( $strSql, $objClientDatabase );

		return $arrstrData;
	}

	public static function fetchSimpleAllowedParcelAlertNotificationCustomersByCid( $intCid, $objClientDatabase ) {

		 $strSql = 'SELECT
						cs.customer_id,
					    cs.value
					 FROM
						customer_settings cs
					    JOIN customer_settings css ON (css.customer_id = cs.customer_id
														 AND css.cid = cs.cid
				 										 AND ( css.key = \'PUSH_NOTIFICATION_PARCEL_ALERT_ALLOWED\'
															   AND css.value = \'1\' ) )
					    JOIN clients c ON c.id = cs.cid
					 WHERE
						cs.key = \'PUSH_NOTIFICATION_IOS_DEVICE_TOKENS\'
						AND cs.value IS NOT NULL AND cs.value != \'\'
						AND cs.cid = ' . ( int ) $intCid . ' ;';

		$arrstrData = fetchData( $strSql, $objClientDatabase );

		return $arrstrData;
	}

	public static function fetchSimpleAllowedParcelAlertNotificationCustomersByCustomerIdsByCid( $arrintPackageCustomerIds, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						cs.customer_id,
					    cs.value
					 FROM
						customer_settings cs
					    JOIN customer_settings css ON (css.customer_id = cs.customer_id
														 AND css.cid = cs.cid
				 										 AND ( css.key = \'PUSH_NOTIFICATION_PARCEL_ALERT_ALLOWED\'
															   AND css.value = \'1\' ) )
					    JOIN clients c ON c.id = cs.cid
					 WHERE
						cs.key = \'PUSH_NOTIFICATION_IOS_DEVICE_TOKENS\'
						AND cs.value IS NOT NULL AND cs.value != \'\'
						AND cs.cid = ' . ( int ) $intCid . '
						AND cs.customer_id IN ( ' . implode( ',', $arrintPackageCustomerIds ) . ' ) ';

		$arrstrData = fetchData( $strSql, $objClientDatabase );

		return $arrstrData;
	}

	public static function fetchSimpleAllowedAndroidRentNotificationCustomersByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						cs.customer_id,
					    cs.value
					 FROM
						customer_settings cs
					 WHERE
						cs.key = \'PUSH_NOTIFICATION_RENT_REMINDER_ANDROID_DEVICE_TOKENS\'
						AND cs.value IS NOT NULL AND cs.value != \'\'
						AND cs.cid = ' . ( int ) $intCid . ' ;';

		$arrstrData = fetchData( $strSql, $objClientDatabase );

		return $arrstrData;
	}

	public static function fetchCustomCustomersSettingsByCustomerIdsByKeysByCids( $arrintCids, $arrintCustomerIds, $arrstrKeys, $objDatabase ) {

		if( false == valArr( $arrintCids ) || false == valArr( $arrintCustomerIds ) || false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						customer_id
					FROM
						customer_settings
					WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND key IN ( \'' . implode( '\',\'', array_filter( $arrstrKeys ) ) . '\' )
					GROUP BY
						customer_id
					HAVING
						count( key ) > 1 ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomCustomersSettingCountByCustomerIdByKeyByCid( $intCid = NULL, $intCustomerId = NULL, $strKey = NULL, $objDatabase ) {

		if( true == is_null( $intCid ) || true == is_null( $intCustomerId ) || false == valStr( $strKey ) ) return NULL;

		$strSql = 'SELECT
						count ( id ) AS count
					FROM
						customer_settings
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id = ' . ( int ) $intCustomerId . '
						AND key = \'' . $strKey . '\'';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrstrData[0]['count'] ) ) {
			return ( int ) $arrstrData[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchCustomerSettingsByCustomerIdByKeysByCid( $intCustomerId, $arrstrKeys, $intCid, $objDatabase ) {

		if( true == is_null( $intCid ) || true == is_null( $intCustomerId ) || false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						key, value
                    FROM
						customer_settings
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND key IN ( \'' . implode( '\',\'', array_filter( $arrstrKeys ) ) . '\' )
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerSettingByCustomerIdByKeyByCid( $intCustomerId, $strKey, $intCid, $objDatabase ) {

		if( true == is_null( $intCid ) || true == is_null( $intCustomerId ) || false == valStr( $strKey ) ) return NULL;

		$strSql = 'SELECT * FROM
						customer_settings
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND key = \'' . $strKey . '\'
						AND cid = ' . ( int ) $intCid;

		return self::fetchCustomerSetting( $strSql, $objDatabase );

	}

	public static function fetchCustomCustomerSettingsByCustomerIdByKeysByCid( $intCustomerId, $arrstrKeys, $intCid, $objDatabase ) {

		if( true == is_null( $intCid ) || true == is_null( $intCustomerId ) || false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT * FROM
						customer_settings
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchCustomerSettings( $strSql, $objDatabase );

	}

	public static function fetchCustomCustomerSettings( $arrstrWhereCondition, $objDatabase ) {

		if( false == valArr( $arrstrWhereCondition ) ) {
			return NULL;
		}

		$strSql = 'SELECT count( id ) AS count FROM customer_settings WHERE (customer_id, cid, key) IN ( ' . sqlIntImplode( $arrstrWhereCondition ) . ' );';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrmixData[0]['count'] ) ) {
			return ( int ) $arrmixData[0]['count'];
		}

		return 0;
	}

	public function fetchSimpleCustomerSettingsKeysByCustomerIdByKeysByCid( $intCustomerId, $arrstrKeys, $intCid, $objDatabase ) {
		if( true == is_null( $intCid ) || true == is_null( $intCustomerId ) || false == valArr( $arrstrKeys ) ) return NULL;

		$arrstrPreferredKeys = array();

		$strSql = 'SELECT
						key
					FROM
						customer_settings
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )
						AND cid = ' . ( int ) $intCid;

		$arrmixPreferredKeys = fetchData( $strSql, $objDatabase );

		foreach( $arrmixPreferredKeys as  $arrmixPreferredKey ) {
			array_push( $arrstrPreferredKeys, $arrmixPreferredKey['key'] );
		}

		return $arrstrPreferredKeys;
	}

	public static function fetchCustomCustomerSettingsByCustomerIdsByKeysByCids( $arrintCustomerIds, $arrstrKeys, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintCids ) || false == valArr( $arrintCustomerIds ) || false == valArr( $arrstrKeys ) ) {
			return [];
		}

		$strSql = 'SELECT 
						*
					FROM
						customer_settings
					WHERE
						customer_id IN ( ' . sqlIntImplode( $arrintCustomerIds ) . ' )
						AND cid IN ( ' . sqlIntImplode( $arrintCids ) . ' )
						AND key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )';

		return self::fetchCustomerSettings( $strSql, $objDatabase );

	}

}
?>