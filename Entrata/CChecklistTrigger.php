<?php

class CChecklistTrigger extends CBaseChecklistTrigger {

	const APPLICATION_STARTED 		= 1;
	const APPLICATION_APPROVED		= 2;
	const LEASE_STARTED 			= 3;
	const LEASE_APPROVED			= 4;
	const MOVE_IN 					= 5;
	const MOVE_OUT 					= 6;
	const GROUP_MOVE_IN             = 7;
	const GROUP_MOVE_OUT            = 8;

	public static $c_arrintMoveInChecklistTrigger = [
		self::MOVE_IN,
		self::GROUP_MOVE_IN
	];

	public static $c_arrintMoveOutChecklistTrigger = [
		self::MOVE_OUT,
		self::GROUP_MOVE_OUT
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>