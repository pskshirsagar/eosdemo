<?php

class CJobProjectContractUnit extends CBaseJobProjectContractUnit {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobProjectId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceRequestCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPoCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInvoiceCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInvoiceDrawCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>