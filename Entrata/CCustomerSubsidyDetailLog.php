<?php

class CCustomerSubsidyDetailLog extends CBaseCustomerSubsidyDetailLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerSubsidyDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyCitizenshipTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyEthnicityTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidySsnExceptionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyDependentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriorCustomerSubsidyDetailLogId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAlienRegistrationNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTracsIdEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostDateIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOpeningLog() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasDeclinedToSpecifyGender() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPartTimeStudent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPoliceOrSecurityOfficer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>