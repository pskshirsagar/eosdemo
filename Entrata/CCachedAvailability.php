<?php

class CCachedAvailability extends CBaseCachedAvailability {

	protected $m_intNumberOfBedrooms;
	protected $m_intSquareFeet;

	protected $m_strBuildingName;
	protected $m_strFloorplanName;

	protected $m_fltNumberOfBathrooms;

	public function getNumberOfBedrooms() {
		return $this->m_intNumberOfBedrooms;
	}

	public function getSquareFeet() {
		return $this->m_intSquareFeet;
	}

	public function getFloorplanName() {
		return $this->m_strFloorplanName;
	}

	public function getNumberOfBathrooms() {
		return $this->m_fltNumberOfBathrooms;
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function setNumberOfBedrooms( $intNumberOfBedrooms ) {
		$this->m_intNumberOfBedrooms = CStrings::strToIntDef( $intNumberOfBedrooms, NULL, false );
	}

	public function setSquareFeet( $intSquareFeet ) {
		$this->m_intSquareFeet = $intSquareFeet;
	}

	public function setFloorplanName( $strFloorplanName ) {
		$this->m_strFloorplanName = $strFloorplanName;
	}

	public function setNumberOfBathrooms( $fltNumberOfBathrooms ) {
		$this->m_fltNumberOfBathrooms = CStrings::strToFloatDef( $fltNumberOfBathrooms, NULL, false, 4 );
	}

	public function setBuildingName( $strBuildingName ) {
		$this->set( 'm_strBuildingName', CStrings::strTrimDef( $strBuildingName, 50, NULL, true ) );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['floorplan_name'] ) ) $this->setFloorplanName( $arrmixValues['floorplan_name'] );
		if( true == isset( $arrmixValues['number_of_bedrooms'] ) ) $this->setNumberOfBedrooms( $arrmixValues['number_of_bedrooms'] );
		if( true == isset( $arrmixValues['number_of_bathrooms'] ) ) $this->setNumberOfBathrooms( $arrmixValues['number_of_bathrooms'] );
		if( true == isset( $arrmixValues['square_feet'] ) ) $this->setSquareFeet( $arrmixValues['square_feet'] );
		if( true == isset( $arrmixValues['building_name'] ) ) $this->setBuildingName( $arrmixValues['building_name'] );

	}

	public function valAvailableDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyFloorplanId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGenderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasPets() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasSpecials() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAvailable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsMarketed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOccupied() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function updateCachedAvailabilities( $arrintUnitSpaceIds, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintUnitSpaceIds ) || false == valArr( $arrintPropertyIds ) || false == valId( $intCid ) ) {
			return NULL;
		}
		$strSql = 'UPDATE
					cached_availabilities
				SET
					is_dirty = TRUE
				WHERE
					cid = ' . ( int ) $intCid . ' AND
					property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' ) AND
					unit_space_id IN ( ' . sqlIntImplode( $arrintUnitSpaceIds ) . ' )';

		$arrmixResult = executeSql( $strSql, $objDatabase );

		if( 0 != $arrmixResult['failed'] ) {
			return false;
		} else {
			return true;
		}
	}

}
?>