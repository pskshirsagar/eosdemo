<?php

class CRuleConditionReference extends CBaseRuleConditionReference {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRuleConditionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRouteRuleReferenceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>