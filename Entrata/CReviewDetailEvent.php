<?php

class CReviewDetailEvent extends CBaseReviewDetailEvent {

	const DETAIL_LOG_DATE_FORMAT = 'Y-m-d H:i:s';

	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_boolIsParentEvent;
	protected $m_intRemotePrimaryKey;

	/**
	 * Get Functions
	 *
	 */

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getIsParentEvent() {
		return $this->m_boolIsParentEvent;
	}

	public function getRemotePrimaryKey() {
		return $this->m_intRemotePrimaryKey;
	}

	/**
	 * Set Function
	 *
	 */

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setIsParentEvent( $boolIsParentEvent ) {
		$this->m_boolIsParentEvent = $boolIsParentEvent;
	}

	public function setRemotePrimaryKey( $intRemotePrimaryKey ) {
		$this->m_intRemotePrimaryKey = $intRemotePrimaryKey;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name_first'] ) )	$this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) )	$this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['is_parent_event'] ) )	$this->setIsParentEvent( $arrmixValues['is_parent_event'] );
		if( true == isset( $arrmixValues['remote_primary_key'] ) )	$this->setRemotePrimaryKey( $arrmixValues['remote_primary_key'] );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEventTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOnlineResponse() {

		if( false == valStr( $this->getResponse() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', __( 'Online response note can not be empty.' ) ) );
			return false;
		}

		return true;
	}

	public function valEmail() {

		if( false == CValidation::validateEmailAddress( $this->getFromEmailAddress() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'from_email_address', __( 'Enter valid From Email address.' ) ) );
			return false;
		}

		if( false == CValidation::validateEmailAddress( $this->getToEmailAddress() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'to_email_address', __( 'Enter valid To Email address.' ) ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_online_response':
				$boolIsValid &= $this->valOnlineResponse();
				break;

			case 'validate_email':
				$boolIsValid &= $this->valEmail();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
