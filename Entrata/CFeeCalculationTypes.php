<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFeeCalculationTypes
 * Do not add any new functions to this class.
 */

class CFeeCalculationTypes extends CBaseFeeCalculationTypes {

	public static function fetchFeeCalculationTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CFeeCalculationType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchFeeCalculationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CFeeCalculationType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchFeeCalculationTypesByFeeTypeId( $intFeeTypeId, $objDatabase ) {

		if( false == valId( $intFeeTypeId ) ) return NULL;

		$strSql = '
					SELECT
						*
					FROM
						fee_calculation_types
					WHERE
						' . ( int ) $intFeeTypeId . ' = ANY( fee_type_ids )
					ORDER BY order_num ASC';

		return self::fetchFeeCalculationTypes( $strSql, $objDatabase );
	}

}
?>