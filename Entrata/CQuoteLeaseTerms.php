<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CQuoteLeaseTerms
 * Do not add any new functions to this class.
 */

class CQuoteLeaseTerms extends CBaseQuoteLeaseTerms {

	public static function fetchRenewalQuoteLeaseTermsByQuoteIdsByPropertyIdByCid( $arrintQuoteIds = [], $intPropertyId, $intCid, $objDatabase, $strMoveInDate, $boolAllowLeaseExpirationOverride = false, $boolIncludeCommercialLeaseTerms = false, $boolIncluceDeletedQuoteLeaseTerms = false ) {

		if( false === valArr( $arrintQuoteIds ) || false === valId( $intPropertyId ) || false === valObj( $objDatabase, 'CDatabase' ) || false === valId( $intCid ) ) {
			return NULL;
		}
		$strComercialCondition = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strIncludeDeleted = ' AND qlt.deleted_by IS NULL';
		if( true == $boolIncluceDeletedQuoteLeaseTerms )
			$strIncludeDeleted = NULL;

		$strLeaseExpirationLimitsJoinSql = NULL;
		$strLeaseExpirationLimitsWhereClauseSql = NULL;
		$strLeaseIntervalEndDate = NULL;

		if( true == valStr( $strMoveInDate ) ) {
			$strLeaseIntervalEndDate = date( 'Y-m-d', strtotime( $strMoveInDate . '-1 day' ) );
		}

		if( true == valStr( $strMoveInDate ) && false == $boolAllowLeaseExpirationOverride ) {
			$strLeaseExpirationLimitsWhereClauseSql = ' WHERE CASE
															WHEN qlt.pu_count IS NOT NULL AND qlt.lease_expiring_count >= qlt.pu_count 
															THEN FALSE
															ELSE TRUE
														END';
		}

		$strSql = 'SELECT util_set_locale(\'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getdefaultLocaleCode() . '\');
					With ple As ( SELECT
									pgs.cid,
									pgs.property_id,
									lem.id,
									lem.month,
									lem.expiration_tolerance,
									round ( count ( pu.id ) * lem.expiration_tolerance / 100 ) pu_count
								FROM
									property_gl_settings pgs 
									JOIN lease_expiration_months lem ON ( pgs.cid =lem.cid AND lem.lease_expiration_structure_id = pgs.lease_expiration_structure_id )
									LEFT JOIN property_units pu ON ( pu.cid = lem.cid AND pu.property_id = pgs.property_id )
								WHERE
									pgs.cid = ' . ( int ) $intCid . '
									AND pgs.property_id = ' . ( int ) $intPropertyId . ' 
								GROUP BY
									pgs.cid,
									pgs.property_id,
									lem.id,
									lem.month,
									lem.expiration_tolerance
									ORDER BY
									lem.month ASC )
					SELECT 
						qlt.*,
						CASE
							WHEN qlt.pu_count IS NOT NULL AND qlt.lease_expiring_count >= qlt.pu_count 
							THEN 1
							ELSE 0
						END AS exceeds_expiration_limit 
					FROM  
					( 
						SELECT
							qlt.*,
							CASE
								WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NULL
								THEN util_get_translated( \'name\', lt.name, lt.details ) ||\' (\'||to_char(lsw.start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\')\'
								WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NOT NULL
								THEN util_get_translated( \'name\', lt.name, lt.details ) ||\' (\'||to_char(lsw.renewal_start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\')\'
								ELSE util_get_translated( \'name\', lt.name, lt.details )
							END AS name,
							lt.term_month,
							lsw.end_date,
							lsw.renewal_start_date,
							(
								SELECT
									array_to_string( array_agg( initcap(s.name) ORDER BY s.name), \',\' ) As special_names
								FROM
									lease_associations la
									JOIN specials s ON ( la.cid = s.cid AND s.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND s.special_type_id = ' . CSpecialType::GIFT_INCENTIVE . ' )
								WHERE
									la.cid = qlt.cid
									AND la.quote_id = qlt.quote_id
									AND la.quote_lease_term_id = qlt.id
									AND la.deleted_on IS NULL
							) AS special_names,
							(
								SELECT
									COUNT( cli.id )
								FROM
									cached_leases cli
								WHERE cli.cid = ca.cid AND
									cli.property_id = ca.property_id AND
									cli.id != ca.id AND
									cli.lease_status_type_id IN ( ' . sqlIntImplode( CLeaseStatusType::$c_arrintFutureAndActiveLeaseStatusTypes ) . ' ) AND
									CASE
										WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' THEN
											DATE_TRUNC( \'month\',lsw.end_date ) = DATE_TRUNC( \'month\', cli.lease_end_date + 1 )
										ELSE
											DATE_TRUNC(\'MONTH\', \'' . $strMoveInDate . '\'::DATE + CAST ( lt.term_month || \'MONTHS -1DAY\' AS INTERVAL ) ) = DATE_TRUNC( \'month\', cli.lease_end_date )
									END
							) AS lease_expiring_count,
							ple.pu_count,
							DATE ( \'' . $strLeaseIntervalEndDate . '\'::DATE ) + interval \'1 month\' * lt.term_month as lease_term_expiry_date,
							sc.id AS base_rent_scheduled_charge_id,
							sc.charge_amount As new_rent_amount,
							CASE 
								WHEN sc.deleted_by IS NULL AND sc.id IS NOT NULL THEN 1  
									 ELSE 0 
								END As is_base_rent_selected,
							COALESCE( (
					            SELECT
					                sum ( sca.charge_amount )
					            FROM
					                scheduled_charges sca
					            WHERE
					                sca.cid = qlt.cid
					                AND sca.lease_interval_id = ca.lease_interval_id
					                AND sca.deleted_by IS NULL
					                AND sca.ar_trigger_id = ' . CArTrigger::MONTHLY . '
					                AND sca.ar_origin_id = ' . CArOrigin::AMENITY . '
					                AND sca.lease_term_id = qlt.lease_term_id
					                AND COALESCE ( sca.lease_start_window_id, 0 ) = COALESCE ( qlt.lease_start_window_id, 0 )
					          ), 0 ) AS amenity_rent_amount,
							COALESCE( rl.normalized_amount, 0 ) As calculated_rent_amount,
							sc.ar_code_id As rent_ar_code_id,
							ca.lease_id
						FROM
							quote_lease_terms qlt
							JOIN quotes q ON ( qlt.cid = q.cid AND qlt.quote_id = q.id )
							JOIN cached_applications ca ON ( ca.cid = q.cid AND q.application_id = ca.id ) 
							JOIN lease_terms lt ON ( qlt.cid = lt.cid AND qlt.lease_term_id = lt.id ' . $strComercialCondition . ' AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL ) )
							LEFT JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id AND pcs.property_id = ca.property_id )
							LEFT JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid  AND lsw.id = qlt.lease_start_window_id AND pcs.lease_start_structure_id = lsw.lease_start_structure_id AND lt.id = lsw.lease_term_id AND lsw.deleted_on IS NULL AND lsw.property_id = pcs.property_id AND lsw.is_active IS TRUE )
							LEFT JOIN ple ON ( ple.cid = ca.cid AND ple.property_id = ca.property_id AND ple.month = date_part ( \'month\', CASE
							                                                                                                   WHEN lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . ' THEN DATE ( \'' . $strMoveInDate . '\'::DATE ) + ( lt.term_month || \' MONTHS -1DAY\' )::INTERVAL
							                                                                                                   ELSE DATE ( date_trunc ( \'month\', lsw.end_date ) )
							                                                                                                 END ) )
							
							                                                                                                 
							LEFT JOIN scheduled_charges sc ON ( sc.cid = qlt.cid 
																AND sc.lease_id = ca.lease_id 
																AND sc.lease_interval_id = ca.lease_interval_id 
																AND sc.quote_id = qlt.quote_id 
																AND sc.lease_term_id = qlt.lease_term_id 
																AND COALESCE(qlt.lease_start_window_id,0) = COALESCE( sc.lease_start_window_id,0)
																AND sc.ar_origin_id = ' . CArOrigin::BASE . ' 
																AND COALESCE(sc.ar_origin_reference_id,0) = 0 
																AND sc.ar_code_type_id = ' . CArCodeType::RENT . ' )
							LEFT JOIN rate_logs rl ON ( rl.cid = sc.cid AND rl.id = sc.rate_log_id )
						WHERE
							qlt.cid = ' . ( int ) $intCid . '
							AND qlt.quote_id IN ( ' . sqlIntImplode( $arrintQuoteIds ) . ' )
							' . $strIncludeDeleted . '
						ORDER BY
							lsw.renewal_start_date,
							lt.term_month,
							qlt.quote_id 
						) As qlt 
					' . $strLeaseExpirationLimitsWhereClauseSql;

		return self::fetchQuoteLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchTransferQuoteLeaseTermsByQuoteIdsByPropertyIdByCid( $arrintQuoteIds = [], $intPropertyId, $intCid, $objDatabase, $strMoveInDate, $boolAllowLeaseExpirationOverride = false, $boolIncludeCommercialLeaseTerms = false ) {

		if( false == valArr( $arrintQuoteIds ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}
		$strComercialCondition = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		$strLeaseExpirationLimitsJoinSql = NULL;
		$strLeaseExpirationLimitsWhereClauseSql = NULL;
		if( true == valStr( $strMoveInDate ) && false == $boolAllowLeaseExpirationOverride ) {
			$strLeaseExpirationLimitsWhereClauseSql = ' WHERE CASE
															WHEN qlt.pu_count IS NOT NULL AND qlt.lease_expiring_count >= qlt.pu_count 
															THEN FALSE
															ELSE TRUE
														END';
		}

		$strSql = 'SELECT util_set_locale(\'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getdefaultLocaleCode() . '\');
					With ple As ( SELECT
									pgs.cid,
									pgs.property_id,
									lem.id,
									lem.month,
									lem.expiration_tolerance,
									round ( count ( pu.id ) * lem.expiration_tolerance / 100 ) pu_count
								FROM
									property_gl_settings pgs 
									JOIN lease_expiration_months lem ON ( pgs.cid =lem.cid AND lem.lease_expiration_structure_id = pgs.lease_expiration_structure_id )
									LEFT JOIN property_units pu ON ( pu.cid = lem.cid AND pu.property_id = pgs.property_id )
								WHERE
									pgs.cid = ' . ( int ) $intCid . '
									AND pgs.property_id = ' . ( int ) $intPropertyId . ' 
								GROUP BY
									pgs.cid,
									pgs.property_id,
									lem.id,
									lem.month,
									lem.expiration_tolerance
									ORDER BY
									lem.month ASC )
					SELECT 
						qlt.*,
						CASE
							WHEN qlt.pu_count IS NOT NULL AND qlt.lease_expiring_count >= qlt.pu_count 
							THEN 1
							ELSE 0
						END AS exceeds_expiration_limit 
					FROM  
					( 
						SELECT
							qlt.*,
							CASE
								WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NULL
								THEN util_get_translated( \'name\', lt.name, lt.details ) ||\' (\'||to_char(lsw.start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\')\'
								WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NOT NULL
								THEN util_get_translated( \'name\', lt.name, lt.details ) ||\' (\'||to_char(lsw.renewal_start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\')\'
								ELSE util_get_translated( \'name\', lt.name, lt.details )
							END AS name,
							(
								SELECT
									array_to_string( array_agg( initcap(s.name) ORDER BY s.name), \',\' ) As special_names
								FROM
									lease_associations la
									JOIN specials s ON ( la.cid = s.cid AND s.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND s.special_type_id = ' . CSpecialType::GIFT_INCENTIVE . ' )
								WHERE
									la.cid = qlt.cid
									AND la.quote_id = qlt.quote_id
									AND la.quote_lease_term_id = qlt.id
									AND la.deleted_on IS NULL
							) AS special_names,
							(
								SELECT
									COUNT( cli.id )
								FROM
									cached_leases cli
								WHERE cli.cid = ca.cid AND
									cli.property_id = ca.property_id AND
									cli.id != ca.id AND
									cli.lease_status_type_id IN ( ' . sqlIntImplode( CLeaseStatusType::$c_arrintFutureAndActiveLeaseStatusTypes ) . ' ) AND
									CASE
										WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' THEN
											DATE_TRUNC( \'month\',lsw.end_date ) = DATE_TRUNC( \'month\', cli.lease_end_date + 1 )
										ELSE
											DATE_TRUNC(\'MONTH\', \'' . $strMoveInDate . '\'::DATE + CAST ( lt.term_month || \'MONTHS -1DAY\' AS INTERVAL ) ) = DATE_TRUNC( \'month\', cli.lease_end_date )
									END
							) AS lease_expiring_count,
							ple.pu_count
						FROM
							quote_lease_terms qlt
							JOIN quotes q ON ( qlt.cid = q.cid AND qlt.quote_id = q.id )
							JOIN cached_applications ca ON ( ca.cid = q.cid AND q.application_id = ca.id ) 
							JOIN lease_terms lt ON ( qlt.cid = lt.cid AND qlt.lease_term_id = lt.id ' . $strComercialCondition . ')
							LEFT JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id AND pcs.property_id = ca.property_id )
							LEFT JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid  AND lsw.id = qlt.lease_start_window_id AND pcs.lease_start_structure_id = lsw.lease_start_structure_id AND lt.id = lsw.lease_term_id AND lsw.deleted_on IS NULL AND lsw.property_id = pcs.property_id AND lsw.is_active IS TRUE )
							LEFT JOIN ple ON ( ple.cid = ca.cid AND ple.property_id = ca.property_id AND ple.month = date_part ( \'month\', CASE
							                                                                                                   WHEN lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . ' THEN DATE ( date_trunc ( \'month\', \'' . $strMoveInDate . '\'::DATE ) ) + ( lt.term_month || \' MONTHS - 1 DAY\' )::INTERVAL
							                                                                                                   ELSE DATE ( date_trunc ( \'month\', lsw.end_date ) )
							                                                                                                 END ) )

						WHERE
							qlt.cid = ' . ( int ) $intCid . '
							AND qlt.quote_id IN ( ' . implode( ', ', $arrintQuoteIds ) . ' )
							AND qlt.deleted_by IS NULL
						ORDER BY
							qlt.quote_id,
							lt.term_month,
							lsw.start_date 
						) As qlt 
					' . $strLeaseExpirationLimitsWhereClauseSql;
		return self::fetchQuoteLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchCustomQuoteLeaseTermsByQuoteIds( $arrintQuoteIds, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false ) {

		if( false == valArr( $arrintQuoteIds ) || false == valId( $intCid ) ) {
			return NULL;
		}
		$strComercialCondition = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		$strSql = 'SELECT
						qlt.*
					FROM quote_lease_terms qlt
					JOIN quotes q ON( q.cid = qlt.cid AND q.id = qlt.quote_id )
					JOIN lease_terms lt ON( lt.cid = qlt.cid AND lt.id = qlt.lease_term_id )
					WHERE
						qlt.cid = ' . ( int ) $intCid . '
						AND qlt.deleted_on IS NULL
						AND qlt.quote_id IN (' . implode( ', ', $arrintQuoteIds ) . ')' . $strComercialCondition;

		return self::fetchQuoteLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchQuoteLeaseTermsByQuoteIdsByCid( $arrintQuoteIds, $intCid, $objDatabase, $boolFetchDeleted = false, $boolIncludeCommercialLeaseTerms = false ) {

		if( false == valArr( $arrintQuoteIds ) || false == valId( $intCid ) ) {
			return NULL;
		}
		$strComercialCondition = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strSqlFetchDeleted = ' AND qlt.deleted_on IS NULL';
		if( true == $boolFetchDeleted ) {
			$strSqlFetchDeleted = '';
		}

		$strSql = 'SELECT
						qlt.*,
						lt.term_month AS term_month,
						util_get_translated( \'name\', lt.name, lt.details ) AS name
					FROM
						quote_lease_terms qlt
						JOIN lease_terms lt ON( lt.cid = qlt.cid AND lt.id = qlt.lease_term_id )
					WHERE
						qlt.cid = ' . ( int ) $intCid . '
						AND qlt.quote_id IN (' . implode( ', ', $arrintQuoteIds ) . ')
						' . $strSqlFetchDeleted . $strComercialCondition . '
					ORDER BY
						lt.term_month asc';

		return self::fetchQuoteLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchQuoteLeaseTermsByQuoteIdByLeaseTermByCid( $intQuoteId, $intLeaseTermId, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false ) {

		$strComercialCondition = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		$strSql = 'SELECT
						qlt.*,
						lt.term_month AS term_month
					FROM
						quote_lease_terms qlt
						JOIN lease_terms lt ON( lt.cid = qlt.cid AND lt.id = qlt.lease_term_id )
					WHERE
						qlt.cid = ' . ( int ) $intCid . '
						AND qlt.lease_term_id = ' . ( int ) $intLeaseTermId . '
						AND qlt.quote_id = ' . ( int ) $intQuoteId . $strComercialCondition;

		return self::fetchQuoteLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchCustomQuoteLeaseTermByIdByPropertyIdByCid( $intId, $intPropertyId, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false ) {
		$strComercialCondition = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		$strSql = 'SELECT util_set_locale(\'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getdefaultLocaleCode() . '\');
					SELECT
						qlt.*,
						lt.term_month AS term_month,
						CASE
							WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NULL
							THEN util_get_translated( \'name\', lt.name, lt.details ) ||\' (\'||to_char(lsw.start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\')\'
							WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NOT NULL
							THEN util_get_translated( \'name\', lt.name, lt.details ) ||\' (\'||to_char(lsw.renewal_start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\')\'
							ELSE util_get_translated( \'name\', lt.name, lt.details )
						END AS name
					FROM
						quote_lease_terms qlt
						JOIN lease_terms lt ON ( qlt.cid = lt.cid AND qlt.lease_term_id = lt.id )
						JOIN property_gl_settings pgs ON ( qlt.cid = pgs.cid AND pgs.property_id = ' . ( int ) $intPropertyId . ' )
						LEFT JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id AND pcs.property_id = pgs.property_id )
						LEFT JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid  AND lsw.id = qlt.lease_start_window_id AND pcs.lease_start_structure_id = lsw.lease_start_structure_id AND lt.id = lsw.lease_term_id AND lsw.deleted_on IS NULL AND lsw.property_id = pcs.property_id AND lsw.is_active IS TRUE )
					WHERE
						qlt.cid = ' . ( int ) $intCid . '
						AND qlt.id = ( ' . ( int ) $intId . ' )
						AND qlt.deleted_by IS NULL ' . $strComercialCondition . '
					LIMIT 1';

		return self::fetchQuoteLeaseTerm( $strSql, $objDatabase );
	}

}
?>