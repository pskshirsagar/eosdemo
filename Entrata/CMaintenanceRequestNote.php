<?php

class CMaintenanceRequestNote extends CBaseMaintenanceRequestNote {

	const VALIDATE_INSERT_CLOSING_NOTE = 'VALIDATE_INSERT_CLOSING_NOTE';
	const VALIDATE_WEBSERVICE          = 'VALIDATE_WEBSERVICE';

	protected $m_boolSendRequestNoteEmail;

	/**
	 * Get Functions
	 */

	public function getSendRequestNoteEmail() {
		return $this->m_boolSendRequestNoteEmail;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );

		if( false == is_null( $this->getNote() ) ) {
			$this->setNote( replaceSpecialCharacters( $this->getNote() ) );
		}

		if( true == isset( $arrmixValues['send_request_note_email'] ) )
			$this->setSendRequestNoteEmail( $arrmixValues['send_request_note_email'] );
	}

	public function setSendRequestNoteEmail( $boolSendRequestNoteEmail ) {
		$this->m_boolSendRequestNoteEmail = $boolSendRequestNoteEmail;
	}

	public function valId() {
		$boolIsValid = true;

		// Validation example
		// if( true == is_null( $this->getId() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
		// }

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		// Validation example
		// if( true == is_null( $this->getCid() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
		// }

		return $boolIsValid;
	}

	public function valMaintenanceRequestId() {
		$boolIsValid = true;

		// Validation example
		if( true == is_null( $this->getMaintenanceRequestId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_request_id', __( 'Maintenance Request Id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;

		// Validation example
		// if( true == is_null( $this->getCompanyUserId() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_user_id', '' ) );
		// }

		return $boolIsValid;
	}

	public function valNote() {
		$boolIsValid = true;

		$this->getNote( getSanitizedFormField( $this->getNote(), true ) );
		// Validation example
		if( true == is_null( $this->getNote() ) || 'Add Note' == $this->getNote() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', __( 'Note is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNoteDatetime() {
		$boolIsValid = true;

		// Validation example
		// if( true == is_null( $this->getNoteDatetime() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note_datetime', '' ) );
		// }

		return $boolIsValid;
	}

	public function valIsResidentVisible() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsEntryNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsClosingNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		// Validation example
		// if( true == is_null( $this->getOrderNum() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ) );
		// }

		return $boolIsValid;
	}

	public function valUpdatedBy() {
		$boolIsValid = true;

		// Validation example
		// if( true == is_null( $this->getUpdatedBy() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ) );
		// }

		return $boolIsValid;
	}

	public function valUpdatedOn() {
		$boolIsValid = true;

		// Validation example
		// if( true == is_null( $this->getUpdatedOn() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ) );
		// }

		return $boolIsValid;
	}

	public function valCreatedBy() {
		$boolIsValid = true;

		// Validation example
		// if( true == is_null( $this->getCreatedBy() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
		// }

		return $boolIsValid;
	}

	public function valCreatedOn() {
		$boolIsValid = true;

		// Validation example
		// if( true == is_null( $this->getCreatedOn() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
		// }

		return $boolIsValid;
	}

	public function valInsertClosingNote( $objDatabase ) {
		return is_null( \Psi\Eos\Entrata\CMaintenanceRequestNotes::createService()->fetchMaintenanceRequestClosingNoteByMaintenanceRequestIdByCid( $this->getMaintenanceRequestId(), $this->getCid(), $objDatabase ) );
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valNote();
				break;

			case self::VALIDATE_INSERT_CLOSING_NOTE:
				$boolIsValid &= $this->valInsertClosingNote( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valNote();
				break;

			case self::VALIDATE_WEBSERVICE:
				$boolIsValid &= $this->valNote();
				$boolIsValid &= $this->valMaintenanceRequestId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>