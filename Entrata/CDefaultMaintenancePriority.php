<?php

class CDefaultMaintenancePriority extends CBaseDefaultMaintenancePriority {

	const VERY_LOW     = 'VL';
	const LOW          = 'LO';
	const MEDIUM       = 'MD';
	const HIGH         = 'HI';
	const VERY_HIGH    = 'VH';
	const EMERGENCY    = 'EM';

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCode() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getCode() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'code', '' ) );
        // }

        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getName() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getDescription() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getIsPublished() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
        // }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getOrderNum() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = false;
            	break;
        }

        return $boolIsValid;
    }

}
?>