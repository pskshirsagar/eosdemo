<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApHeaderExportBatchTypes
 * Do not add any new functions to this class.
 */

class CApHeaderExportBatchTypes extends CBaseApHeaderExportBatchTypes {

    public static function fetchApHeaderExportBatchTypes( $strSql, $objDatabase ) {
        // return self::fetchCachedObjects( $strSql, 'CApHeaderExportBatchType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false ); for time being commenting ,need to remove this
    	return parent::fetchObjects( $strSql, 'CApHeaderExportBatchType', $objDatabase );
    }

    public static function fetchApHeaderExportBatchType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CApHeaderExportBatchType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchAllApHeaderExportBatchTypes( $objDatabase, $strOrderBy = NULL ) {
    	$strOrderBy = ( true == is_null( $strOrderBy ) ) ? 'order_num, name' : $strOrderBy;
    	return self::fetchApHeaderExportBatchTypes( 'SELECT id, name,dont_allow_multiple_bank FROM ap_header_export_batch_types ORDER BY ' . $strOrderBy, $objDatabase );
    }

	public static function fetchApHeaderExportBatchTypesByApHeaderExportBatchIdByCid( $intApHeaderExportBatchId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
					DISTINCT ON ( ahebt.id ) ahebt.name
				FROM
					ap_header_export_batch_types ahebt
					JOIN bank_accounts ba ON ( ahebt.id = ba.ap_header_export_batch_type_id )
					JOIN ap_payments ap ON ( ba.id = ap.bank_account_id AND ba.cid = ap.cid )
					JOIN ap_headers ah ON ( ah.ap_payment_id = ap.id AND ah.cid = ap.cid )
				WHERE
					ah.ap_header_export_batch_id = ' . ( int ) $intApHeaderExportBatchId . '
					AND ah.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApHeaderExportBatchTypesAssociatedWithBankAccounts( $objDatabase, $intCid ) {

		$strSql = 'SELECT
						ahebt.*
					FROM
						ap_header_export_batch_types AS ahebt
					WHERE
						EXISTS (
							SELECT
								1
							FROM
								bank_accounts ba
							WHERE
								ap_header_export_batch_type_id = ahebt.id
								AND ba.deleted_on IS NULL
								AND ba.is_disabled = 0
								AND ba.cid = ' . ( int ) $intCid . '
							)
					ORDER BY
						ahebt.name';

		return self::fetchApHeaderExportBatchTypes( $strSql, $objDatabase );
	}

	public static function fetchCustomApHeaderExportBatchTypesByCid( $intCid, $objDatabase, $strOrderBy = NULL ) {
		$strOrderBy = ( true == is_null( $strOrderBy ) ) ? 'order_num, name' : $strOrderBy;
		$strSql = 'SELECT 
						 ahebt.*
					FROM 
						ap_header_export_batch_types ahebt
						JOIN bank_accounts ba ON ( ba.ap_header_export_batch_type_id = ahebt.id )
					WHERE 
						ahebt.is_published = 1
						AND ba.is_disabled = 0
						AND ba.is_credit_card_account = 0
						AND ba.cid = ' . ( int ) $intCid . '
					GROUP BY ahebt.id
					ORDER BY ' . $strOrderBy;

  		return self::fetchApHeaderExportBatchTypes( $strSql, $objDatabase );
	}

	public static function fetchPublishedApHeaderExportBatchTypes( $objDatabase, $strOrderBy = NULL ) {
			$strOrderBy = ( true == is_null( $strOrderBy ) ) ? 'order_num, name' : $strOrderBy;
			return self::fetchApHeaderExportBatchTypes( 'SELECT * FROM ap_header_export_batch_types WHERE is_published = 1 ORDER BY ' . $strOrderBy, $objDatabase );
	}

}
?>