<?php

class CUnitSpaceExclusion extends CBaseUnitSpaceExclusion {

	protected $m_strUnitExclusionReasonTypeName;
	protected $m_intGroupExcludedUnitsInGpr;
	protected $m_strName;

	/**
	 * Set Functions
	 */

	public function setUnitExclusionReasonTypeName( $strUnitExclusionReasonTypeName ) {
		$this->m_strUnitExclusionReasonTypeName = CStrings::strTrimDef( $strUnitExclusionReasonTypeName, 240, NULL, true );
	}

	public function setGroupExcludedUnitsInGpr( $intGroupExcludedUnitsInGpr ) {
		$this->m_intGroupExcludedUnitsInGpr = CStrings::strToIntDef( $intGroupExcludedUnitsInGpr, NULL, false );
	}

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['unit_exclusion_reason_type_name'] ) ) {
			$this->setUnitExclusionReasonTypeName( $arrValues['unit_exclusion_reason_type_name'] );
		}
		if( true == isset( $arrValues['group_excluded_units_in_gpr'] ) ) {
			$this->setGroupExcludedUnitsInGpr( $arrValues['group_excluded_units_in_gpr'] );
		}
		if( true == isset( $arrValues['name'] ) ) {
			$this->setName( $arrValues['name'] );
		}
	}

	/**
	 * Get Functions
	 */

	public function getUnitExclusionReasonTypeName() {
		return $this->m_strUnitExclusionReasonTypeName;
	}

	public function getGroupExcludedUnitsInGpr() {
		return $this->m_intGroupExcludedUnitsInGpr;
	}

	public function getName() {
		return $this->m_strName;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valPropertyId() {
		return true;
	}

	public function valUnitExclusionReasonTypeId() {
		return true;
	}

	public function valGlAccountId() {

		$boolIsValid = true;

		if( 0 != $this->getGroupExcludedUnitsInGpr() && false == is_numeric( $this->getGlAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( '{%s,0} Account is required.', [ $this->getUnitExclusionReasonTypeName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valGlAccountId();
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
		}

		return $boolIsValid;
	}

}
?>