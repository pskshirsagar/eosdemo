<?php

class CDelinquencyPolicyType extends CBaseDelinquencyPolicyType {

	const DELINQUENCY			= 1;
	const COLLECTIONS			= 2;

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public static function getDelinquencyPolicyTypeNameById( $intId ) {
		$strDelinquencyPolicyTypeName = NULL;

		switch( $intId ) {

			case CDelinquencyPolicyType::DELINQUENCY:
				$strDelinquencyPolicyTypeName = __( 'Delinquency' );
				break;

			case CDelinquencyPolicyType::COLLECTIONS:
				$strDelinquencyPolicyTypeName = __( 'Collections' );
				break;

			default:
				// default case
				break;
		}

		return $strDelinquencyPolicyTypeName;
	}

}
?>