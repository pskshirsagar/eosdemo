<?php

class CApPayee1099Adjustment extends CBaseApPayee1099Adjustment {

	protected $m_strVendorCode;

	/**
	 * Get Functions
	 */

	public function getAmount() {
		return $this->m_fltAmount;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getVendorCode() {
		return $this->m_strVendorCode;
	}

	/**
	 * Set Functions
	 */

	public function setAmount( $fltAmount ) {
		$this->m_fltAmount = $fltAmount;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, 50, NULL, true );
	}

	public function setVendorCode( $strVendorCode ) {
		$this->m_strVendorCode = CStrings::strTrimDef( $strVendorCode, 50, NULL, true );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['amount'] ) ) $this->setAmount( $arrmixValues['amount'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['vendor_code'] ) ) $this->setVendorCode( $arrmixValues['vendor_code'] );

		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApLegalEntityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {

		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', 'Property is required.' ) );
		}

		return $boolIsValid;
	}

	public function valAmount() {

		$boolIsValid = true;

		if( false == valStr( $this->getAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', 'Amount is required.' ) );
		} elseif( false == is_numeric( $this->getAmount() ) || 0 > $this->getAmount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', 'Amount should be positive.' ) );
		}

		return $boolIsValid;
	}

	public function valYear() {

		$boolIsValid = true;

		if( true == is_null( $this->getYear() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'year', 'Year is required.' ) );
		} elseif( false == CValidation::validateDate( $this->getYear() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'year', 'Year should be in YYYY format.' ) );
		}
		return $boolIsValid;
	}

	public function val1099Settings() {

		$boolIsValid = true;

		if( false == is_null( $this->getPropertyId() ) || true == valStr( $this->getAmount() ) || false == is_null( $this->getYear() ) ) {
			$boolIsValid &= $this->valPropertyId();
			$boolIsValid &= $this->valAmount();
			$boolIsValid &= $this->valYear();
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->val1099Settings();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>