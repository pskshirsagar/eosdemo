<?php

class CStudentLeasingCap extends CBaseStudentLeasingCap {

	const CAP_AMOUNT_RANGE = 2147483647;

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intPropertyId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Please select valid student property.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyFloorplanId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intPropertyFloorplanId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_floorplan_id', __( 'Invalid Property Floorplan' ) ) );
		}

		return $boolIsValid;
	}

	public function valLeaseTermAndWindow() {
		$boolIsValid = true;

		if( false == valId( $this->m_intLeaseTermId ) || false == valId( $this->m_intLeaseStartWindowId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_term', __( 'Please select valid lease term' ) ) );
		}

		return $boolIsValid;
	}

	public function valCapAmount() {
		$boolIsValid = true;

		if( false == valId( $this->m_intCapAmount ) && true == ( bool ) $this->m_boolIsActive ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cap_amount', __( 'Valid cap amount is required' ) ) );
		}

		if( true == $boolIsValid && ( $this::CAP_AMOUNT_RANGE <= $this->m_intCapAmount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cap_amount_range', __( 'Cap amount is required to be in the range of {%d, 0} to {%d, 1}', [ 0, 2147483647 ] ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyFloorplanId();
				$boolIsValid &= $this->valLeaseTermAndWindow();
				$boolIsValid &= $this->valCapAmount();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>