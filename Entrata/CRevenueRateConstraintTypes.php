<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueRateConstraintTypes
 * Do not add any new functions to this class.
 */

class CRevenueRateConstraintTypes extends CBaseRevenueRateConstraintTypes {

	public static function fetchRevenueRateConstraintTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CRevenueRateConstraintType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchRevenueRateConstraintType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CRevenueRateConstraintType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>