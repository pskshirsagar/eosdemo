<?php

class CDefaultMaintenanceBoard extends CBaseDefaultMaintenanceBoard {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGroupByProblems() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShowInspectionColumn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShowFloorplanOnUnitHover() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDontShowEndDatesAndDaysRemaining() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>