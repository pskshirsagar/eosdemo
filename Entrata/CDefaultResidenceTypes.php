<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultResidenceTypes
 * Do not add any new functions to this class.
 */

class CDefaultResidenceTypes extends CBaseDefaultResidenceTypes {

	public static function fetchDefaultResidenceTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDefaultResidenceType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDefaultResidenceType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultResidenceType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>