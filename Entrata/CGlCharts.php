<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlCharts
 * Do not add any new functions to this class.
 */

class CGlCharts extends CBaseGlCharts {

	public static function fetchGlChartBySystemCodeByCid( $strSystemCode, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT * FROM gl_charts WHERE system_code = ' . pg_escape_literal( $objClientDatabase->getHandle(), $strSystemCode ) . ' AND cid = ' . ( int ) $intCid;
		return self::fetchGlChart( $strSql, $objClientDatabase );
	}

}
?>