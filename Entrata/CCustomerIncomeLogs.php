<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerIncomeLogs
 * Do not add any new functions to this class.
 */

class CCustomerIncomeLogs extends CBaseCustomerIncomeLogs {

	public static function fetchApplicationCustomerIncomeLogsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase, $intApplicationId, $boolReturnSqlOnly = false ) {
		$strSql = 'SELECT
						cil.*
					FROM
						customer_income_logs cil
						JOIN application_associations assoc ON ( cil.cid = assoc.cid AND cil.customer_id = assoc.customer_id
																 AND assoc.customer_data_type_id = ' . CCustomerDataType::INCOME . '
																 AND assoc.customer_data_type_reference_id = cil.id )
					WHERE
						cil.cid = ' . ( int ) $intCid . '
						AND cil.customer_id = ' . ( int ) $intCustomerId . '
						AND assoc.application_id = ' . ( int ) $intApplicationId;

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		return self::fetchCustomerIncomeLog( $strSql, $objDatabase );
	}

	public static function fetchCustomerIncomeLogsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolReturnSqlOnly = false ) {
		$strSql = 'SELECT
						cil.*
					FROM
						 customer_income_logs cil
						 JOIN application_associations assoc ON ( assoc.cid = cil.cid AND assoc.customer_id = cil.customer_id
																  AND assoc.customer_data_type_id = ' . CCustomerDataType::INCOME . '
																  AND assoc.customer_data_type_reference_id = cil.id )

						 JOIN applicants a ON ( a.cid = cil.cid AND a.customer_id = cil.customer_id )
						 JOIN applicant_applications as aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid ' . $strCheckDeletedAASql . ' )
						 AND aa.application_id = ' . ( int ) $intApplicationId . ' AND aa.cid = ' . ( int ) $intCid . '
					ORDER BY cil.customer_id';

		if( false == $boolReturnSqlOnly ) {
			return $strSql;
		}

		return self::fetchCustomerIncomeLogs( $strSql, $objDatabase );
	}

	public static function fetchRecentCustomerIncomeLogByCustomerIdByIdByCid( $intCustomerId, $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cil.*
					FROM
						customer_income_logs cil
					WHERE
						cil.cid = ' . ( int ) $intCid . '
						AND cil.customer_id = ' . ( int ) $intCustomerId . '
						AND cil.customer_income_id = ' . ( int ) $intId . '
						AND cil.is_post_date_ignored = 0
					ORDER BY cil.log_datetime DESC
					LIMIT 1';

		return self::fetchCustomerIncomeLog( $strSql, $objDatabase );
	}

	public static function fetchCustomerIncomeLogsByCustomerIncomeIdsByCid( $intCid, $arrintCustomerIncomesIds, $objDatabase ) {
		$strSql	= 'SELECT *
					FROM
						customer_income_logs
					WHERE 
						cid = ' . ( int ) $intCid . ' 
						AND customer_income_id IN ( ' . implode( ',', $arrintCustomerIncomesIds ) . ' ) ';

		return self::fetchCustomerIncomeLogs( $strSql, $objDatabase );
	}

}
?>