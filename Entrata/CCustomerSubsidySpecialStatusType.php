<?php

class CCustomerSubsidySpecialStatusType extends CBaseCustomerSubsidySpecialStatusType {

	protected $m_boolIsDelete;

	public function getIsDelete() {
		return $this->m_boolIsDelete;
	}

	public function getRecentLog( $objDatabase ) {
		return NULL;
	}

	public function setIsDelete( $boolIsDelete ) {
		return $this->m_boolIsDelete = CStrings::strToBool( $boolIsDelete );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidySpecialStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>