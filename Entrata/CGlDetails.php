<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlDetails
 * Do not add any new functions to this class.
 */
class CGlDetails extends CBaseGlDetails {

	const GL_BOOK_TYPE_ACCRUAL      = 1;
	const GL_BOOK_TYPE_CASH         = 2;
	const GL_BOOK_TYPE_BOTH         = 3;

	public static function fetchApAllocationGlDetailsByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase, $boolIsBillbackManagementFeeCc = false ) {

		if( false == valIntArr( $arrintApHeaderIds ) ) return NULL;

		$strSubCondition = ( $boolIsBillbackManagementFeeCc )? 'OR aa.lump_ap_header_id = ad.ap_header_id ' : '';

		$strSql = 'SELECT
						gd.id,
						gd.property_id,
						gd.amount,
						gd.ap_detail_id,
						gd.memo,
						gat.account_number,
						gat.name as account_name,
						gh.gl_transaction_type_id,
						gh.reference,
						gh.memo as header_memo,
						gh.header_number,
						gh.post_month,
						gh.post_date,
						gd.gl_reconciliation_id,
						p.property_name
					FROM
						ap_details ad
						JOIN ap_allocations aa ON ( aa.cid = ad.cid AND ( aa.charge_ap_detail_id = ad.id OR aa.credit_ap_detail_id = ad.id )' . $strSubCondition . ' )
						JOIN gl_headers gh ON ( gh.cid = aa.cid AND gh.reference_id = aa.id AND aa.gl_transaction_type_id = gh.gl_transaction_type_id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApAllocationGlTransactionTypeIds ) . ' ) )
						JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id )
						LEFT JOIN properties p ON ( p.cid = gd.cid AND p.id = gd.property_id )
						LEFT JOIN gl_account_trees gat ON ( gd.cid = gat.cid AND gat.gl_account_id = gd.accrual_gl_account_id AND gat.is_default = 1 )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.ap_header_id IN (' . implode( ',', $arrintApHeaderIds ) . ')
						AND gh.offsetting_gl_header_id IS NULL
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchReconciliationStatusByApPaymentIdByCid( $intApPaymentId, $intCid, $objClientDatabase ) {

		$strSql = '	SELECT
						DISTINCT ap.id,
						apt.name AS ap_payment_type,
						ap.payment_number,
						ap.payment_status_type_id,
						pst.name,
						CASE
							WHEN ap.payment_status_type_id = ' . CPaymentStatusType::VOIDED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED] . '\'
							WHEN ap.payment_status_type_id IN ( ' . CPaymentStatusType::RECALL . ',' . CPaymentStatusType::RETURNING . ' ) THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RETURNED] . '\'
							ELSE CASE
								WHEN sub_ad.gl_reconciliation_id IS NOT NULL AND sub_ad.gl_reconciliation_status_type_id =' . CGlReconciliationStatusType::PAUSED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED] . '\'
								WHEN sub_ad.gl_reconciliation_id IS NULL THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED] . '\'
								ELSE \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED] . '\'
								END
							END AS payment_status_type_name,
								sub_ad.gl_reconciliation_status_type_id,
								sub_ad.is_system,
								first_value ( sub_ad.gl_reconciliation_id ) OVER ( PARTITION BY ap.id ORDER BY sub_ad.gl_reconciliation_id IS NULL, ap.id ) AS gl_reconciliation_id
							FROM
								ap_payments ap
								JOIN ap_headers ah_credit ON ( ap.cid = ah_credit.cid AND ap.id = ah_credit.ap_payment_id )
								LEFT JOIN bank_accounts ba ON ( ba.cid = ap.cid AND ba.id = ap.bank_account_id )
								LEFT JOIN ap_payment_types apt ON ( apt.id = ap.ap_payment_type_id )
								JOIN payment_status_types pst ON ( pst.id = ap.payment_status_type_id )
								JOIN LATERAL (
									SELECT
										gd.gl_reconciliation_id, 
										gr.gl_reconciliation_status_type_id,
										gr.is_system
									FROM
										ap_details ad_credit
										LEFT JOIN ap_allocations aa ON ( aa.cid = ad_credit.cid AND aa.credit_ap_detail_id = ad_credit.id )
										LEFT JOIN gl_headers gh ON ( gh.cid = aa.cid AND gh.reference_id = aa.id AND gh.gl_transaction_type_id = aa.gl_transaction_type_id AND gh.gl_transaction_type_id = ' . CGlTransactionType::AP_ALLOCATION . ' )
										LEFT JOIN gl_details gd ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id AND ( gd.gl_reconciliation_id IS NOT NULL OR ( ad_credit.inter_co_property_id IS NOT NULL AND ba.bank_account_type_id <> ' . \CBankAccountType::INTER_COMPANY . ' ) )
													AND gd.amount = CASE
													WHEN aa.origin_ap_allocation_id IS NOT NULL OR aa.lump_ap_header_id IS NOT NULL
													THEN aa.allocation_amount * - 1
														ELSE aa.allocation_amount
													END
													AND gd.property_id = CASE
													WHEN ba.reimbursed_property_id <> aa.property_id THEN ba.reimbursed_property_id
													WHEN ( ad_credit.inter_co_property_id IS NOT NULL AND ba.bank_account_type_id <> ' . \CBankAccountType::INTER_COMPANY . ' ) THEN ad_credit.inter_co_property_id
														ELSE aa.property_id
													END )
										LEFT JOIN gl_reconciliations gr ON ( gr.cid = gd.cid AND ( gr.id = gd.gl_reconciliation_id OR ( gd.gl_reconciliation_id IS NULL AND ad_credit.inter_co_property_id IS NOT NULL AND ba.bank_account_type_id <> ' . \CBankAccountType::INTER_COMPANY . ' ) ) )
									WHERE
										ah_credit.cid = ' . ( int ) $intCid . '
										AND ah_credit.id = ad_credit.ap_header_id
										AND ah_credit.gl_transaction_type_id = ad_credit.gl_transaction_type_id
										AND ah_credit.post_month = ad_credit.post_month
										AND ad_credit.deleted_on IS NULL
									GROUP BY
										1, 2, 3
								) AS sub_ad ON TRUE
							WHERE
								ap.cid = ' . ( int ) $intCid . '
								AND ap.id = ' . ( int ) $intApPaymentId . '
								AND ah_credit.deleted_on IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailsByCidByGlDetailsFilterForApi( $arrintPropertyIds, $intCid, $arrmixFilterData, $objClientDatabase, $boolReturnSqlOnly = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strGlAccountFieldName = ( 'cash' == $arrmixFilterData['transactionBasis'] ) ? 'gd.cash_gl_account_id' : 'gd.accrual_gl_account_id';

		// Added lease_customers join to get lease id and customer id
		// Added ap_allocations, ap_details, ap_headers join to fetch ap_payment_id of a particular GL Account.
		$strSql = 'SELECT
						gd.id,
						gd.property_id,
						gh.reference,
						' . $strGlAccountFieldName . ' AS gl_account_id,
						gd.amount,
						gat.formatted_account_number AS account_number,
						gat.name AS account_name,
						gat.secondary_number,
						gh.gl_transaction_type_id,
						gh.reference_id,
						gh.id as gh_header_id,
						CASE WHEN gh.gl_transaction_type_id = ' . CGlTransactionType::AP_CHARGE . '
						THEN ad.description
						ELSE gd.memo 
						END as memo,
						gh.memo as header_memo,
						gh.header_number,
						gh.post_month,
						gh.transaction_datetime,
						gh.post_date,
						art.lease_id,
						ah.ap_payment_id,
						art.ar_payment_id,
						gd.property_unit_id,
						gd.property_building_id,
						ah.ap_payee_id,
						ah.ap_payee_location_id,
						COALESCE (ara.charge_ar_transaction_id,art.id) art_id,
						gdm.id as gl_dimension_id,
						gdm.name as gl_dimension_name,
						gd.ap_detail_id,
						ac.id as contract_id,
						ac.name as contract_name,
						jp.id as job_cost_id,
						jp.name as job_cost,
						ac1.id as job_cost_code_id,
						ac1.name as job_cost_code,
						ah.refund_ar_transaction_id,
						ah.lease_customer_id,
						c.name_full
					FROM
						gl_details gd
						JOIN gl_headers gh ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id )
						JOIN gl_account_trees gat ON ( gat.cid = gd.cid AND ' . $strGlAccountFieldName . ' = gat.gl_account_id AND gat.is_default = 1 )
						LEFT JOIN ar_allocations ara ON ( ara.cid = gh.cid AND ara.id = gh.reference_id AND ara.gl_transaction_type_id = gh.gl_transaction_type_id )
						LEFT JOIN ar_transactions art ON ( gh.cid = art.cid AND ( gh.reference_id = art.id OR art.id = ara.credit_ar_transaction_id ) AND gh.gl_transaction_type_id = art.ar_code_type_id )
						LEFT JOIN ap_allocations aa ON ( gh.cid = aa.cid AND gh.reference_id = aa.id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApGlTransactionTypeIds ) . ' ) )
						LEFT JOIN ap_details ad ON ( COALESCE(aa.cid,gd.cid ) = ad.cid AND COALESCE( aa.charge_ap_detail_id, aa.credit_ap_detail_id, gd.ap_detail_id) = ad.id )
						LEFT JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month )
						LEFT JOIN lease_customers lc ON ( lc.cid = ah.cid AND lc.id = ah.lease_customer_id )
						LEFT JOIN customers c ON ( lc.cid = c.cid AND c.id = lc.customer_id )
						LEFT JOIN gl_dimensions gdm ON ( gdm.cid = gd.cid AND gdm.id = gd.gl_dimension_id )
						LEFT JOIN ap_contracts ac ON ( gd.cid = ac.cid AND gd.ap_contract_id = ac.id AND gh.gl_transaction_type_id = ' . \CGlTransactionType::GL_GJ . ' )
						LEFT JOIN job_phases jp ON ( jp.cid = gd.cid AND jp.id = gd.job_phase_id AND gh.gl_transaction_type_id = ' . \CGlTransactionType::GL_GJ . ' )
						LEFT JOIN ap_codes ac1 ON ( ac1.cid = gd.cid AND ac1.id = gd.ap_code_id AND gh.gl_transaction_type_id = ' . \CGlTransactionType::GL_GJ . ' ) 
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND gd.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND gh.is_template IS FALSE
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')';

		$arrintGlAccountIds 		= ( false == is_null( $arrmixFilterData['glAccountIds'] ) ) ? $arrmixFilterData['glAccountIds']: NULL;

		$strSql .= ( true == valArr( $arrintGlAccountIds ) ) ? ' AND ' . $strGlAccountFieldName . ' IN (' . implode( ', ', $arrintGlAccountIds ) . ' ) ' : NULL;
		$strSql .= ( false == is_null( $arrmixFilterData['postDateFrom'] ) ) ? ' AND gh.post_date >= \'' . $arrmixFilterData['postDateFrom'] . '\'' : NULL;
		$strSql .= ( false == is_null( $arrmixFilterData['postDateTo'] ) ) ? ' AND gh.post_date <= \'' . $arrmixFilterData['postDateTo'] . '\'' : NULL;
		if( false == is_null( $arrmixFilterData['postMonthFrom'] ) ) {
			$strOldStartPostMonthDate = explode( '/', $arrmixFilterData['postMonthFrom'] );
			$strSql .= ' AND gh.post_month >= \'' . date( 'm/d/Y', mktime( 0, 0, 0, $strOldStartPostMonthDate[0], 1, $strOldStartPostMonthDate[1] ) ) . '\'';
		}
		if( false == is_null( $arrmixFilterData['postMonthTo'] ) ) {
			$strOldEndPostMonthDate = explode( '/', $arrmixFilterData['postMonthTo'] );
			$strSql .= ' AND gh.post_month <= \'' . date( 'm/d/Y', mktime( 0, 0, 0, $strOldEndPostMonthDate[0], 1, $strOldEndPostMonthDate[1] ) ) . '\'';
		}
		if( false == is_null( $arrmixFilterData['glTreeId'] ) ) {
			$strSql .= ' AND gat.gl_tree_id = ' . $arrmixFilterData['glTreeId'];
		}
		if( false != valArr( $arrmixFilterData['glBookIds'] ) ) {
			$strSql .= ' AND gh.gl_book_id IN ( ' . implode( ', ', $arrmixFilterData['glBookIds'] ) . ' )';
		}

		$strSql .= ' AND gh.is_template = false';

		if( false == is_null( $arrmixFilterData['transactionDateFrom'] ) && false == is_null( $arrmixFilterData['transactionDateTo'] ) ) {
			$strSql .= ' AND gh.transaction_datetime >= \'' . date( 'Y-m-d', strtotime( $arrmixFilterData['transactionDateFrom'] ) ) . ' 00:00:00\'';
			$strSql .= ' AND gh.transaction_datetime <= \'' . date( 'Y-m-d', strtotime( $arrmixFilterData['transactionDateTo'] ) ) . ' 23:59:59\'';
		}
		$arrintExcludeGlTransactionTypes = [];
		if( true == $arrmixFilterData['excludeApTransactions'] ) {
			$arrintExcludeGlTransactionTypes = CGlTransactionType::$c_arrintApGlTransactionTypeIds;
		}
		if( true == $arrmixFilterData['excludeArTransactions'] ) {
			$arrintExcludeGlTransactionTypes = array_merge( $arrintExcludeGlTransactionTypes, CGlTransactionType::$c_arrintArGlTransactionTypeIds );
		}
		if( false != valArr( $arrintExcludeGlTransactionTypes ) ) {
			$strSql .= ' AND gh.gl_transaction_type_id NOT IN ( ' . implode( ',', $arrintExcludeGlTransactionTypes ) . ' )';
		}
		if( true == $arrmixFilterData['excludeExportedTransactions'] ) {
			$strSql .= '  AND NOT EXISTS(
								SELECT
									1
								FROM
									accounting_export_associations aea
									 JOIN accounting_export_batches aeb ON ( aea.accounting_export_batch_id = aeb.id AND aea.cid = aeb.cid )
								WHERE
									gd.id = aea.gl_detail_id
									AND gd.cid = aea.cid
									AND aeb.cid = ' . ( int ) $intCid . '
									AND aeb.transmission_vendor_id = ' . CTransmissionVendor::API_EXPORT . ' )';
		}
		$strSql .= ' ORDER BY
						gh.post_month,
						gh.post_date,
						gat.account_number,
						' . $strGlAccountFieldName . ',
						gd.id';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return fetchData( $strSql, $objClientDatabase );
		}

	}

	public static function fetchCustomGlDetailsByGlHeaderIdByCid( $intGlHeaderId, $intCid, $objClientDatabase, $boolIncludeDeletedUnits = false, $intOffset = NULL, $intPageLimit = NULL ) {

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						gd.*,
						p.property_name,
						pu.unit_number,
						cd.name AS company_department_name,
						gdim.name AS custom_tag_name,
						concat_ws( \' - \', j.name, jp.name ) AS job_phase_name,
						ac.name AS ap_contract_name,
						apc.name AS ap_code_name,
						aea.accounting_export_batch_id
					FROM
						gl_details gd
						LEFT JOIN properties p ON ( p.cid = gd.cid AND gd.property_id = p.id )
						LEFT JOIN property_units pu ON ( gd.cid = pu.cid AND gd.property_unit_id = pu.id ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN company_departments cd ON ( gd.cid = cd.cid AND gd.company_department_id = cd.id )
						LEFT JOIN gl_dimensions gdim ON ( gd.cid = gdim.cid AND gd.gl_dimension_id = gdim.id )
						LEFT JOIN job_phases jp ON ( gd.cid = jp.cid AND gd.job_phase_id = jp.id )
						LEFT JOIN jobs j ON ( j.cid = jp.cid AND j.id = jp.job_id )
						LEFT JOIN ap_contracts ac ON ( gd.cid = ac.cid AND gd.ap_contract_id = ac.id )
						LEFT JOIN ap_codes apc ON ( gd.cid = apc.cid AND gd.ap_code_id = apc.id )
						LEFT JOIN accounting_export_associations aea ON ( gd.cid = aea.cid AND gd.id = aea.gl_detail_id )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND gd.gl_header_id = ' . ( int ) $intGlHeaderId . '
					ORDER BY gd.id ASC';

		if( true === is_int( $intOffset ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset;
		}

		if( true === is_int( $intPageLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intPageLimit;
		}

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchUnBatchedSummarizedGlDetailsByGlDetailsFilter( $arrintGlDetailIds, $objGlDetailsFilter, $intGlExportBatchTypeId, $objClientDatabase ) {

		if( false == is_int( $objGlDetailsFilter->getGlChartId() ) ) return NULL;

		if( CTransmissionVendor::GL_EXPORT_JENARK == $intGlExportBatchTypeId ) {
			$strSql = self::buildSqlForJenarkFormat( $arrintGlDetailIds, $objGlDetailsFilter );
		} elseif( CTransmissionVendor::GL_EXPORT_ONESITE == $intGlExportBatchTypeId || CTransmissionVendor::GL_EXPORT_PEAK == $intGlExportBatchTypeId ) {
			$strSql = self::buildSqlForOnesiteFormat( $arrintGlDetailIds, $objGlDetailsFilter );
		} elseif( CTransmissionVendor::GL_EXPORT_MRI == $intGlExportBatchTypeId ) {
			$strSql = self::buildSqlForMriFormat( $arrintGlDetailIds, $objGlDetailsFilter );
		} elseif( CTransmissionVendor::GL_EXPORT_YARDI == $intGlExportBatchTypeId ) {
			$strSql = self::buildSqlForYardiFormat( $objGlDetailsFilter, $intGlExportBatchTypeId );
		} else {
			$strSql = self::buildSqlForOtherFormat( $arrintGlDetailIds, $objGlDetailsFilter, $intGlExportBatchTypeId );
		}
		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchUnBatchedSummarizedGlDetailsByGlDetailsFilterForPeopleSoft( $objGlDetailsFilter, $objClientDatabase ) {

		if( false == is_int( $objGlDetailsFilter->getGlChartId() ) ) return NULL;

		$strSql = ' SELECT
	                        sum(gd.amount) as amount,
	                   		 gd.cid,
	                   		 gd.property_id,
		                     gd.accrual_gl_account_id,
	                    	CASE
								WHEN ( DATE_TRUNC ( \'month\', gh.post_date ) ::TIMESTAMP < DATE_TRUNC ( \'month\', gh.post_month ) ::TIMESTAMP ) THEN gh.post_month
	      						WHEN ( DATE_TRUNC ( \'month\', gh.post_date ) ::TIMESTAMP > DATE_TRUNC ( \'month\', gh.post_month ) ::TIMESTAMP ) THEN date_trunc(\'month\', gh.post_month)+\'1month\'::interval-\'1day\'::interval
	      						WHEN ( DATE_TRUNC ( \'month\', gh.post_date ) ::TIMESTAMP = DATE_TRUNC ( \'month\', gh.post_month ) ::TIMESTAMP ) THEN gh.post_date
	      					 END AS post_date,
		                    gh.post_month,
	      					gat.name AS account_name,
	    					gat.secondary_number,
	    					gat.account_number,
							pp.value,
							CASE WHEN gh.reference_id IS NOT NULL THEN gh.reference_id ELSE 1 END AS ar_deposit_id,
							CASE
								WHEN gh.reference_id IS NOT NULL THEN ad.deposit_number::VARCHAR
          						ELSE gat.description
        					END AS description
	              		FROM
	                 		gl_details gd
	                  		JOIN gl_headers gh ON ( gd.cid = gh.cid
		                                              AND gd.gl_header_id = gh.id
		                                              AND gh.cid = ' . ( int ) $objGlDetailsFilter->getCid();

		$strSql .= ' AND gh.gl_transaction_type_id NOT IN ( ' . implode( ',', CGlTransactionType::$c_arrintApGlTransactionTypeIds ) . ' ) )
	                  		LEFT JOIN ar_deposits ad ON ( gh.cid = ad.cid AND ad.id = gh.reference_id AND gh.gl_transaction_type_id = ad.gl_transaction_type_id )
							JOIN property_preferences pp ON ( pp.cid = gd.cid AND pp.property_id = gd.property_id AND pp.key = \'PROPERTY_GL_BU\' AND pp.value IS NOT NULL )
	                  		JOIN gl_accounts ga ON ( gd.cid = ga.cid AND ga.id = gd.accrual_gl_account_id  )
		    				JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND gat.gl_account_id = ga.id )
		    				LEFT JOIN accounting_export_associations aea ON ( gd.cid = aea.cid AND gd.id = aea.gl_detail_id )
						WHERE
	                  		gd.cid = ' . ( int ) $objGlDetailsFilter->getCid() . '
	                  		AND gd.accrual_gl_account_id IS NOT NULL
	                  		AND aea.gl_detail_id IS NULL
	                  		AND ( gh.post_month >= \'' . date( 'm/1/Y', strtotime( $objGlDetailsFilter->getStartDate() ) ) . '\' AND gh.post_month <= \'' . date( 'm/t/Y', strtotime( $objGlDetailsFilter->getEndDate() ) ) . '\' )
	                  		AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')';

		if( true == valArr( $objGlDetailsFilter->getGlBookIds() ) ) {
			$strSql .= ' AND gh.gl_book_id IN ( ' . implode( ', ', $objGlDetailsFilter->getGlBookIds() ) . ' ) ';
		}

		if( true == valArr( $objGlDetailsFilter->getPropertyIds() ) ) {
			$strSql .= ' AND gd.property_id IN (' . implode( ', ', $objGlDetailsFilter->getPropertyIds() ) . ' )';
		}

		if( true == valStr( $objGlDetailsFilter->getPostStartDate() ) && true == valStr( $objGlDetailsFilter->getPostEndDate() ) ) {
			$strSql .= 'AND ( gh.post_date >= \'' . date( 'm/d/Y', strtotime( $objGlDetailsFilter->getPostStartDate() ) ) . '\' AND gh.post_date <= \'' . date( 'm/d/Y', strtotime( $objGlDetailsFilter->getPostEndDate() ) ) . '\' )';
		} elseIf( false != $objGlDetailsFilter->getIncludeAllUnexportedTransactions() && true == valStr( $objGlDetailsFilter->getTransactionStartDate() ) ) {
			$strSql .= ' AND gh.transaction_datetime >= \'' . date( 'Y-m-d', strtotime( $objGlDetailsFilter->getTransactionStartDate() ) ) . ' 00:00:00\'';
		}

		$strSql .= 'GROUP BY
	                  		gd.cid,
	                  		gd.property_id,
	                  		gh.post_month,
	                  		CASE
	       						WHEN ( DATE_TRUNC ( \'month\', gh.post_date ) ::TIMESTAMP < DATE_TRUNC ( \'month\', gh.post_month ) ::TIMESTAMP ) THEN gh.post_month
	      						WHEN ( DATE_TRUNC ( \'month\', gh.post_date ) ::TIMESTAMP > DATE_TRUNC ( \'month\', gh.post_month ) ::TIMESTAMP ) THEN date_trunc(\'month\', gh.post_month)+\'1month\'::interval-\'1day\'::interval
	      						WHEN ( DATE_TRUNC ( \'month\', gh.post_date ) ::TIMESTAMP = DATE_TRUNC ( \'month\', gh.post_month ) ::TIMESTAMP ) THEN gh.post_date
	      					 END,
	    					CASE
            					WHEN gh.reference_id IS NOT NULL THEN ad.deposit_number::VARCHAR
            					ELSE gat.description
          					END,
	                  		gd.accrual_gl_account_id,
	      					gat.name,
	    					gat.secondary_number,
	    					gat.account_number,
							pp.value,
	    					CASE WHEN gh.reference_id IS NOT NULL THEN gh.reference_id ELSE 1 END';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailsByGlReconciliationIdByGlReconciliationsFilterByCid( $intGlReconciliationId, $objGlReconciliationsFilter, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intGlReconciliationId ) || false == valObj( $objGlReconciliationsFilter, 'CGlReconciliationsFilter' ) ) return NULL;

		$strCondition					= NULL;
		$strConditionWithGlHeaderIds	= NULL;

		if( true == valArr( $objGlReconciliationsFilter->getGlAccountIds() ) ) {
			$strCondition .= ' ( gd.accrual_gl_account_id IN ( ' . implode( ',', $objGlReconciliationsFilter->getGlAccountIds() ) . ' ) OR
									gd.cash_gl_account_id IN ( ' . implode( ',', $objGlReconciliationsFilter->getGlAccountIds() ) . ' )
								) ';
		}

		if( true == valArr( $objGlReconciliationsFilter->getPropertyIds() ) ) {
			$strCondition .= ' AND gd.property_id IN ( ' . implode( ',', $objGlReconciliationsFilter->getPropertyIds() ) . ' ) ';
		}

		if( true == valArr( $objGlReconciliationsFilter->getGlHeaderIds() ) ) {
			$strConditionWithGlHeaderIds = ' AND gd.gl_header_id IN ( ' . implode( ',', $objGlReconciliationsFilter->getGlHeaderIds() ) . ' ) ';
		}

		$strSql = 'SELECT
						gd.*
					FROM
						gl_details gd
						JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id AND gh.offsetting_gl_header_id IS NULL )
					WHERE
						gd.cid = ' . ( int ) $intCid .
		          $strConditionWithGlHeaderIds . '
						AND (';

		if( false == is_null( $strCondition ) ) {
			$strSql .= ' (' . $strCondition . ') OR ';
		}

		$strSql .= '( gd.gl_reconciliation_id  = ' . ( int ) $intGlReconciliationId . ' ) )
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')
					ORDER BY
						gd.id ASC';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailsByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT * FROM gl_details WHERE id IN( ' . implode( ',', $arrintIds ) . ' ) AND cid = ' . ( int ) $intCid;
		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailsByPropertyIdByStartDateByEndDateByGlAccountIdsByCid( $intPropertyId, $strStartDate, $strEndDate, $arrintGlAccountIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintGlAccountIds ) ) return NULL;

		$strSql = 'SELECT
						CASE
							WHEN pgs.is_cash_basis <> 0 THEN gd.cash_gl_account_id
							ELSE gd.accrual_gl_account_id
						END AS gl_account_id,
						TO_CHAR ( gh.post_month, \'mm/01/YYYY\' ) AS formatted_post_month,
						SUM ( gd.amount ) AS amount,
						gd.gl_dimension_id
					FROM
						gl_details gd
						JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id )
						JOIN property_gl_settings pgs ON ( gh.cid = pgs.cid AND pgs.property_id = gd.property_id )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND gd.property_id = ' . ( int ) $intPropertyId . '
						AND gh.post_month >= \'' . $strStartDate . '\'
						AND gh.post_month <= \'' . $strEndDate . '\'
						AND gh.gl_header_status_type_id NOT IN( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
						AND ( CASE
								WHEN pgs.is_cash_basis <> 0 THEN gd.cash_gl_account_id 
								ELSE gd.accrual_gl_account_id 
							 END ) IN ( ' . sqlIntImplode( $arrintGlAccountIds ) . ' )
					GROUP BY
						1, 2, 4
					ORDER BY
						1, 2';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailByGlAccountIdByPropertyIdByPostMonthByCid( $intGlAccountId, $intPropertyId, $strPostMonth, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
			 			SUM( gd.amount ) AS actual_amount
					FROM
			 			gl_details gd
						JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id AND gh.offsetting_gl_header_id IS NULL )
					WHERE
			 			gd.cid = ' . ( int ) $intCid . '
						AND gd.property_id = ' . ( int ) $intPropertyId . '
			 			AND
						CASE
						WHEN gd.accrual_gl_account_id IS NOT NULL
						THEN
							gd.accrual_gl_account_id = ' . ( int ) $intGlAccountId . '
						ELSE
							gd.cash_gl_account_id = ' . ( int ) $intGlAccountId . '
						END
						AND to_char(gh.post_month, \'YYYY-MM\') = \'' . date( 'Y-m', strtotime( $strPostMonth ) ) . '\'
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ') ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlGetailsByArTransactionIdsByCid( $arrintArTransactionIds, $intCid, $objClientDatabase, $boolShowDeletedGlDetails = false ) {

		$strSql = 'SELECT util_set_locale( \'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
					    gd.*,
					    gh.reference_id as transaction_id,
					    gat.account_number AS account_number,
					    util_get_translated( \'name\', gat.name, gat.details ) AS account_name,
					    gh.transaction_datetime,
					    gh.post_month,
					    gh.post_date,
		                gh.gl_transaction_type_id
					FROM
					    gl_headers gh
					    JOIN gl_details gd ON ( gd.cid = gh.cid AND gd.property_id = gh.property_id AND gd.lease_id = gh.lease_id AND gd.gl_header_id = gh.id )
					    JOIN ar_transactions at ON ( at.cid = gh.cid AND at.property_id = gh.property_id AND at.lease_id = gh.lease_id AND at.id = gh.reference_id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintArTransactionGlTransactionTypeIds ) . ',' . CGlTransactionType::DATA_FIX_AR_TRANSACTIONS . ' ) )
					    JOIN gl_account_trees gat ON ( gat.cid = gd.cid AND gat.gl_account_id = gd.accrual_gl_account_id AND gat.is_default = 1 )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND at.id IN ( ' . implode( ',', $arrintArTransactionIds ) . ' ) '
		          . ( ( true == $boolShowDeletedGlDetails ) ? ' ' : ' AND at.is_deleted = false ' ) . '
						AND gh.offsetting_gl_header_id IS NULL
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')
					ORDER BY
						gd.gl_header_id,
						gd.amount DESC';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlGetailsByArAllocationIdsByCid( $arrintArAllocationIds, $intCid, $objClientDatabase ) {

		if ( false == valArr( $arrintArAllocationIds ) ) return NULL;

		$strSql = 'SELECT util_set_locale( \'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						gd.*,
						gh.reference_id,
						gat.account_number,
						util_get_translated( \'name\', gat.name, gat.details ) AS account_name,
						gh.transaction_datetime,
						gh.post_month,
						gh.post_date,
						gh.gl_transaction_type_id
					FROM
						gl_headers gh
						JOIN gl_details gd ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id )
						JOIN gl_account_trees gat ON ( gat.cid = gd.cid AND gat.gl_account_id = gd.accrual_gl_account_id AND gat.is_default = 1 )
						JOIN ar_allocations aa ON ( aa.cid = gh.cid AND aa.id = gh.reference_id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintArAllocationGlTransactionTypeIds ) . ',' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS . ',' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS2 . ',' . CGlTransactionType::DATA_FIX_AR_ALLOCATIONS3 . ' ) )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND gh.reference_id IN ( ' . implode( ',', $arrintArAllocationIds ) . ' )
						AND gh.offsetting_gl_header_id IS NULL
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')
					ORDER BY
						gd.gl_header_id,
						gd.amount DESC';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlGetailsByArPaymentIdByCid( $intArPaymentId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT util_set_locale( \'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						gh.post_month,
						gh.post_date,
						gh.transaction_datetime,
						gd.*,
						util_get_translated( \'name\', gat_accrual_gl_account.name, gat_accrual_gl_account.details ) AS account_name,
						gat_accrual_gl_account.formatted_account_number AS account_number
					FROM
						gl_details gd
						LEFT JOIN gl_trees gt ON ( gt.cid = gd.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						LEFT JOIN gl_account_trees gat_accrual_gl_account ON ( gat_accrual_gl_account.cid = gt.cid AND gat_accrual_gl_account.gl_tree_id = gt.id AND gd.cid = gat_accrual_gl_account.cid AND gd.accrual_gl_account_id = gat_accrual_gl_account.gl_account_id )
						LEFT JOIN gl_account_trees gat_cash_gl_account ON ( gat_cash_gl_account.cid = gt.cid AND gat_cash_gl_account.gl_tree_id = gt.id AND gd.cid = gat_cash_gl_account.cid AND gd.cash_gl_account_id = gat_cash_gl_account.gl_account_id )
						JOIN gl_headers gh ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.offsetting_gl_header_id IS NULL )
						JOIN ar_allocations aa ON ( aa.cid = gh.cid AND aa.id = gh.reference_id AND aa.is_posted = FALSE AND gh.gl_transaction_type_id = aa.gl_transaction_type_id )
						JOIN ar_transactions at ON ( at.cid = aa.cid AND at.id = aa.credit_ar_transaction_id )
						JOIN ar_payments ap ON ( ap.cid = at.cid AND ap.id = at.ar_payment_id )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND ap.id = ' . ( int ) $intArPaymentId . '
						AND gh.gl_transaction_type_id = ' . CGlTransactionType::AR_ALLOCATION . '
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')
				ORDER BY
						gd.gl_header_id,
						gd.amount DESC';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlGetailsByArDepositIdByCid( $intArDepositId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT util_set_locale( \'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						gd.*,
						gat.account_number,
						util_get_translated( \'name\', gat.name, gat.details ) AS account_name,
						gh.transaction_datetime,
						gh.post_month,
	 					gh.post_date,
						p.property_name
					FROM
						gl_details gd
						JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id AND gh.reference_id = ' . ( int ) $intArDepositId . ' AND gd.gl_transaction_type_id = gh.gl_transaction_type_id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintArDepositGlTransactionTypeIds ) . ' ) )
						JOIN gl_account_trees gat ON ( gd.cid = gat.cid AND gd.accrual_gl_account_id = gat.gl_account_id AND gat.is_default = 1 )
						JOIN properties p ON ( p.cid = gd.cid AND gd.property_id = p.id )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND gh.offsetting_gl_header_id IS NULL
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')
					ORDER BY
						gd.id';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailsByPropertyIdByBudgetIdByStartDateByEndDateByCid( $intPropertyId, $intBudgetId, $strStartDate, $strEndDate, $intCid, $objClientDatabase, $arrmixParameters = [] ) {

		$strGlAccountIds = ' SELECT
									gl_account_id
							FROM
									budget_gl_account_months
							WHERE
									cid = ' . ( int ) $intCid . '
							AND 
									budget_id = ' . ( int ) $intBudgetId . ' ';

		if( true == isset( $arrmixParameters['gl_account_ids'] ) && true == valArr( $arrmixParameters['gl_account_ids'] ) ) {
			$strGlAccountIds = ' ' . implode( ',', $arrmixParameters['gl_account_ids'] ) . ' ';
		}

		$strSql = 'WITH accrual_amount_total AS (
						SELECT
							gd.accrual_gl_account_id,
							NULL::INT AS cash_gl_account_id,
							TO_CHAR( gh.post_month, \'mm/01/YYYY\') AS formatted_post_month,
							SUM ( gd.amount ) AS amount,
							MAX ( gd.gl_dimension_id ) AS gl_dimension_id
						FROM
							gl_details gd
							JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id AND gh.offsetting_gl_header_id IS NULL )
							JOIN property_gl_settings pgs ON ( gh.cid = pgs.cid AND pgs.is_cash_basis = 0 AND pgs.property_id = ' . ( int ) $intPropertyId . ' )
						WHERE
							gd.cid = ' . ( int ) $intCid . '
							AND gd.property_id = ' . ( int ) $intPropertyId . '
							AND gd.accrual_gl_account_id IN ( ' . $strGlAccountIds . ' )
							AND gh.post_month >= \'' . $strStartDate . '\'
							AND gh.post_month <= \'' . $strEndDate . '\'
							AND gh.gl_header_status_type_id NOT IN( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
						GROUP BY
							gd.accrual_gl_account_id,
							formatted_post_month ),

					cash_amount_total AS (
						SELECT
							NULL::INT AS accrual_gl_account_id,
							gd.cash_gl_account_id,
							TO_CHAR( gh.post_month, \'mm/01/YYYY\') AS formatted_post_month,
							SUM ( gd.amount ) AS amount,
							MAX ( gd.gl_dimension_id ) AS gl_dimension_id
						FROM
							gl_details gd
							JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id AND gh.offsetting_gl_header_id IS NULL )
							JOIN property_gl_settings pgs ON ( gh.cid = pgs.cid AND pgs.is_cash_basis <> 0 AND pgs.property_id = ' . ( int ) $intPropertyId . ' )
						WHERE
							gd.cid = ' . ( int ) $intCid . '
							AND gd.property_id = ' . ( int ) $intPropertyId . '
							AND gd.cash_gl_account_id IN ( ' . $strGlAccountIds . ' )
							AND gh.post_month >= \'' . $strStartDate . '\'
							AND gh.post_month <= \'' . $strEndDate . '\'
							AND gh.gl_header_status_type_id NOT IN( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
						GROUP BY
							gd.cash_gl_account_id,
							formatted_post_month )

					SELECT * FROM accrual_amount_total
					UNION ALL
					SELECT * FROM cash_amount_total';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCustomGlDetailsByApDetailIdByCid( $intApDetailId, $intCid, $objClientDatabase ) {

		$strSql = '	SELECT
						gd.id,
						gd.property_id,
						gd.ap_detail_id,
						gd.amount,
						gat.account_number,
						gat.name as account_name,
						gh.id AS gl_header_id,
						gh.reference,
						gh.gl_transaction_type_id,
						gh.header_number,
						gh.post_month,
						gh.post_date
					FROM
						gl_headers gh
						LEFT JOIN ap_allocations aa ON ( gh.cid = aa.cid AND gh.reference_id = aa.id AND aa.gl_transaction_type_id = gh.gl_transaction_type_id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApAllocationGlTransactionTypeIds ) . ' ) )
						JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id )
						LEFT JOIN gl_account_trees gat ON ( gd.cid = gat.cid AND gat.gl_account_id = gd.accrual_gl_account_id AND gat.is_default = 1 )
					WHERE
						gd.ap_detail_id = ' . ( int ) $intApDetailId . '
						AND gd.cid = ' . ( int ) $intCid . '
						AND ( aa.is_deleted = false OR aa.is_deleted IS NULL )
						AND gh.offsetting_gl_header_id IS NULL
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailsByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApHeaderIds ) ) return NULL;

		$strSql = 'SELECT
						gd.*,
						gat.account_number,
						gat.name as account_name,
						gh.post_month,
						gh.post_date,
						p.property_name,
						aea.accounting_export_batch_id
					FROM
						gl_headers gh
						JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gd.reclass_gl_detail_id IS NULL )
						LEFT JOIN properties p ON ( p.cid = gd.cid AND p.id = gd.property_id )
						LEFT JOIN gl_account_trees gat ON ( gd.cid = gat.cid AND gd.accrual_gl_account_id = gat.gl_account_id AND gat.is_default = 1 )
						LEFT JOIN accounting_export_associations aea ON ( gd.cid = aea.cid AND gd.id = aea.gl_detail_id )
					WHERE
						gh.reference_id IN( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApHeaderGlTransactionTypeIds ) . ' )
						AND gh.cid = ' . ( int ) $intCid . '
						AND gh.offsetting_gl_header_id IS NULL
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')
					ORDER BY
						gd.id ASC';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchActiveGlDetailsByGlHeaderIdsByCid( $arrintGlHeaderIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintGlHeaderIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						gl_details
					WHERE
						cid = ' . ( int ) $intCid . '
						AND gl_header_id IN ( ' . implode( ',', $arrintGlHeaderIds ) . ' )
						AND offsetting_gl_detail_id IS NULL';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchActiveGlDetailsByApDetailIdsByCid( $arrintApDetailIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApDetailIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						*
					FROM
						gl_details
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_detail_id IN ( ' . implode( ',', $arrintApDetailIds ) . ' )
						AND offsetting_gl_detail_id IS NULL AND reclass_gl_detail_id IS NULL';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailsByPeriodIdByGlHeaderTypeIdsByCid( $intPeriodId, $arrintGlHeaderTypeIds, $intCid, $objClientDatabase, $boolIsOffsetted = true ) {

		if( false == valArr( $arrintGlHeaderTypeIds ) ) {
			return NULL;
		}

		$strCondition = NULL;

		if( false == $boolIsOffsetted ) {
			$strCondition = ' AND gh.offsetting_gl_header_id IS NULL';
		}

		$strSql = 'SELECT util_set_locale( \'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						gd.*,
						gat.account_number,
						util_get_translated( \'name\', gat.name, gat.details ) AS account_name,
						gh.post_month,
						gh.post_date
					FROM
						gl_headers gh
						JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id )
						JOIN gl_account_trees gat ON ( gd.cid = gat.cid AND gd.accrual_gl_account_id = gat.gl_account_id AND gat.is_default = 1 )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND gd.period_id = ' . ( int ) $intPeriodId . '
						AND gh.gl_header_type_id IN ( ' . implode( ', ', $arrintGlHeaderTypeIds ) . ' )
						AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')' .
		          $strCondition;

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchTotalAmountByPostMonthByPropertyIdByGlAccountIdsByAccountingMethodByCid( $strPostMonth, $intPropertyId, $arrintGlAccountIds, $boolCashAccountingMethod, $intCid, $objClientDatabase ) {

		if( false == valStr( $strPostMonth ) || false == valArr( $arrintGlAccountIds ) ) return NULL;

		$strAccountingMethod = ( true == $boolCashAccountingMethod ) ? 'gl_total.cash_gl_account_id' : 'gl_total.accrual_gl_account_id';

		$strSql = 'SELECT
						CASE WHEN gg.gl_account_type_id = ' . CGlAccountType::LIABILITIES . ' OR gg.gl_account_type_id = ' . CGlAccountType::EQUITY . ' OR gg.gl_account_type_id = ' . CGlAccountType::INCOME . ' THEN
	                        	SUM ( gl_total.amount * -1 )
							ELSE
								SUM ( gl_total.amount )
						END AS gl_total,
						ga.id
					FROM
						gl_accounts ga
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						JOIN gl_trees gt ON ( gat.cid = gt.cid AND gat.gl_tree_id = gt.id )
						JOIN gl_tree_types gtt ON ( gt.gl_tree_type_id = gtt.id )
						JOIN gl_account_types gat1 ON ( ga.gl_account_type_id = gat1.id /* AND gat1.is_income_type = 1*/ )
						LEFT JOIN gl_groups gg ON ( gg.cid = gat.cid AND gg.id = gat.gl_group_id )
						LEFT JOIN
						(
							SELECT
								gd.accrual_gl_account_id,
								gd.cash_gl_account_id,
								gd.cid,
								SUM ( gd.amount ) AS amount,
								COUNT ( gh.id ) AS row_count
							FROM
								gl_headers gh
								JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id )
							WHERE
								gd.cid = ' . ( int ) $intCid . '
								AND gd.property_id = ' . ( int ) $intPropertyId . '
								AND gh.post_month = \'' . $strPostMonth . '\'
								AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')
								AND gh.is_template = false
							GROUP BY
								gd.cid,
								gd.accrual_gl_account_id,
								gd.cash_gl_account_id
						) AS gl_total ON ( ga.cid = gl_total.cid AND ga.id = ' . $strAccountingMethod . ' )
					WHERE
						ga.cid = ' . ( int ) $intCid . '
						AND gt.gl_tree_type_id = ' . CGlTreeType::STANDARD . '
						AND ga.id IN (' . implode( ',', $arrintGlAccountIds ) . ')
						AND gt.system_code LIKE \'DEFAULT\'
						AND ( ga.disabled_by IS NULL OR gl_total.amount <> 0 )
					GROUP BY
				 		ga.id,
						gg.gl_account_type_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailsByApAllocationIdsByCid( $arrintApAllocationIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApAllocationIds ) ) return NULL;

		$strSql = 'SELECT
						gd.*,
						gat.account_number,
						gat.name as account_name,
						gh.post_month,
						gh.post_date,
						gh.reference_id AS ap_allocation_id,
						p.property_name
					FROM
						gl_headers gh
						JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id )
						LEFT JOIN properties p ON ( p.id = gd.property_id AND p.cid = gd.cid )
						LEFT JOIN gl_account_trees gat ON ( gd.cid = gat.cid AND gd.cash_gl_account_id = gat.gl_account_id AND gat.is_default = 1 )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND gh.reference_id IN ( ' . implode( ', ', $arrintApAllocationIds ) . ' )
						AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApAllocationGlTransactionTypeIds ) . ' )
						AND gh.offsetting_gl_header_id IS NULL
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')
					ORDER BY
						gd.id';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchTotalAmountByPostMonthByPropertyIdByGlGroupIdByAccountingMethodByCid( $strPostMonth, $intPropertyId, $intGlGroupId, $boolCashAccountingMethod, $intCid, $objClientDatabase ) {

		if( false == valStr( $strPostMonth ) ) {
			return NULL;
		}

		$strSqlAccountingMethod = ' gd.cash_gl_account_id = gat.gl_account_id ';

		if( false == $boolCashAccountingMethod ) {
			$strSqlAccountingMethod = ' gd.accrual_gl_account_id = gat.gl_account_id ';
		}

		$strSql = 'WITH RECURSIVE recursive_gl_groups( id, parent_gl_group_id, name, cid )
					AS (
						SELECT
							gg1.id,
							gg1.parent_gl_group_id,
							gg1.name,
							gg1.cid
						FROM
							gl_groups gg1
						WHERE
							gg1.id = ' . ( int ) $intGlGroupId . '
							AND gg1.cid = ' . ( int ) $intCid . '
						UNION ALL
							SELECT
								gg2.id,
								gg2.parent_gl_group_id,
								gg2.name,
								gg2.cid
							FROM
								gl_groups gg2,
								recursive_gl_groups rgg
							WHERE
								gg2.parent_gl_group_id = rgg.id
								AND gg2.cid = rgg.cid
								AND gg2.cid = ' . ( int ) $intCid . ' )
					SELECT
						CASE WHEN gg.gl_account_type_id = ' . CGlAccountType::LIABILITIES . ' OR gg.gl_account_type_id = ' . CGlAccountType::EQUITY . ' OR gg.gl_account_type_id = ' . CGlAccountType::INCOME . ' THEN
                                SUM ( gd.amount * -1 )
                            ELSE
                                SUM ( gd.amount )
                       END AS gl_total
					FROM
						recursive_gl_groups rgg
						LEFT JOIN gl_account_trees gat ON ( rgg.id = gat.gl_group_id AND rgg.cid = gat.cid )
						LEFT JOIN gl_details gd ON ( ' . $strSqlAccountingMethod . ' AND rgg.cid = gd.cid )
						LEFT JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id )
						LEFT JOIN gl_groups gg ON ( gd.cid = gg.cid AND gat.gl_group_id = gg.id )
						LEFT JOIN gl_trees gt ON ( gat.cid = gt.cid AND gat.gl_tree_id = gt.id )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND gh.post_month = \'' . $strPostMonth . '\'
						AND gd.property_id = ' . ( int ) $intPropertyId . '
						AND gt.is_system = ' . CGlTree::DEFAULT_IS_SYSTEM . '
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')
					GROUP BY
                          gg.gl_account_type_id';

		$arrmixData = fetchData( $strSql, $objClientDatabase );

		if( true == valArr( $arrmixData ) ) {
			return $arrmixData[0]['gl_total'];
		}

		return 0;
	}

	public static function fetchGlAccountGlDetailsByCidByRuleConditionIdByGLDetailIds( $intCid, $intRuleConditionId, $arrintGlDetailIds, $objClientDatabase ) {

		$strSql = 'SELECT
						gd.*
					FROM
						gl_details gd
						JOIN rule_condition_details rcd ON ( ( gd.cash_gl_account_id = rcd.reference_number OR gd.accrual_gl_account_id = rcd.reference_number ) AND gd.cid = rcd.cid )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND rcd.rule_condition_id = ' . ( int ) $intRuleConditionId . '
						AND gd.id = ANY( ARRAY[' . implode( ',', $arrintGlDetailIds ) . ']::int[] )';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchSpecificUsersGlDetailsByGlDetailIdsByRuleConditionIdByCid( $arrintGlDetailIds, $intRuleConditionId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlDetailIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gd.*
					FROM
						gl_details gd
						JOIN rule_condition_details rcd ON ( gd.cid = rcd.cid AND gd.created_by = rcd.reference_number )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND rcd.rule_condition_id = ' . ( int ) $intRuleConditionId . '
						AND gd.id = ANY( ARRAY[' . implode( ',', $arrintGlDetailIds ) . ']::int[] )';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailsForArDepositsByGlReconciliationFilter( $objGlReconciliationFilter, $intCid, $objClientDatabase ) {

		$strCondition = NULL;

		if( true == valObj( $objGlReconciliationFilter, 'CGlReconciliationsFilter' ) ) {

			if( true == valArr( $objGlReconciliationFilter->getGlAccountIds() ) ) {
				$strCondition .= ' AND ( gd.accrual_gl_account_id IN ( ' . implode( ',', $objGlReconciliationFilter->getGlAccountIds() ) . ' ) OR
										 gd.cash_gl_account_id IN ( ' . implode( ',', $objGlReconciliationFilter->getGlAccountIds() ) . ' )
										) ';
			}

			if( true == valArr( $objGlReconciliationFilter->getPropertyIds() ) ) {
				$strCondition .= ' AND gd.property_id IN ( ' . implode( ',', $objGlReconciliationFilter->getPropertyIds() ) . ' ) ';
			}

			if( true == $objGlReconciliationFilter->getIsBankRecOnPostMonth() && true == valStr( $objGlReconciliationFilter->getBeginningDate() ) ) {
				$strCondition .= ' AND gh.post_month < \'' . $objGlReconciliationFilter->getBeginningDate() . '\'';
			} elseif( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
				$strCondition .= ' AND gh.post_date <= \'' . $objGlReconciliationFilter->getStatementDate() . '\'';
			}
		}

		$strSql	= 'SELECT
						gd.*
					FROM
						gl_headers gh
						JOIN gl_details gd ON ( gh.id = gd.gl_header_id AND gh.cid = gd.cid AND gd.gl_reconciliation_id IS NULL )
						JOIN ar_deposits ad ON ( ad.id = gh.reference_id AND ad.gl_transaction_type_id = gh.gl_transaction_type_id AND ad.cid = gh.cid AND ad.is_deleted = 0 )
						JOIN gl_transaction_types gtt ON ( gtt.id = gh.gl_transaction_type_id )
					WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND ( gh.gl_transaction_type_id = ' . CGlTransactionType::AR_DEPOSIT . '
							OR gh.gl_transaction_type_id = ' . CGlTransactionType::AR_DEPOSIT_REVERSAL . ' ) ' . $strCondition . '
						AND ad.deposit_amount != 0
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')
					ORDER BY
						ad.id';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailsForJournalEntriesByGlReconciliationFilter( $objGlReconciliationFilter, $intCid, $objClientDatabase ) {

		$strCondition = NULL;
		if( true == valObj( $objGlReconciliationFilter, 'CGlReconciliationsFilter' ) ) {

			if( true == valArr( $objGlReconciliationFilter->getGlAccountIds() ) ) {
				$strCondition .= ' AND ( gd.accrual_gl_account_id IN ( ' . implode( ',', $objGlReconciliationFilter->getGlAccountIds() ) . ' ) OR
											gd.cash_gl_account_id IN ( ' . implode( ',', $objGlReconciliationFilter->getGlAccountIds() ) . ' )
										) ';
			}

			if( true == valArr( $objGlReconciliationFilter->getPropertyIds() ) ) {
				$strCondition .= ' AND gd.property_id IN ( ' . implode( ',', $objGlReconciliationFilter->getPropertyIds() ) . ' ) ';
			}

			if( true == $objGlReconciliationFilter->getIsBankRecOnPostMonth() && true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
				$strCondition .= ' AND gh.post_month <= \'' . $objGlReconciliationFilter->getStatementDate() . '\'';
			} elseif( true == valStr( $objGlReconciliationFilter->getStatementDate() ) ) {
				$strCondition .= ' AND gh.post_date <= \'' . $objGlReconciliationFilter->getStatementDate() . '\'';
			}
		}

		$strSql	= 'SELECT
						gd.*
					FROM
						gl_headers gh
						JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id )
						JOIN gl_transaction_types gtt ON ( gtt.id = gh.gl_transaction_type_id )
					WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND gh.is_template IS false
						AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . $strCondition . '
						AND gd.gl_reconciliation_id IS NULL
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')
				   ORDER BY
						gd.gl_header_id,
						gd.id';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailsByApDetailIdsByCid( $arrintApDetailIds, $intCid, $objClientDatabase ) {

		if( true == is_null( $intCid ) || false == valArr( $arrintApDetailIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gd.*,
						p.property_name,
						aea.accounting_export_batch_id
					FROM
						gl_details gd
						LEFT JOIN properties p ON ( p.id = gd.property_id AND p.cid = gd.cid )
						LEFT JOIN accounting_export_associations aea ON ( gd.cid = aea.cid AND gd.id = aea.gl_detail_id )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND gd.ap_detail_id IN ( ' . implode( ',', $arrintApDetailIds ) . ' )
						AND gd.offsetting_gl_detail_id IS NULL';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailsForUserValidationModeByArPaymentIdByCid( $intArPaymentId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						gd.id AS gl_detail_id,
						gd.gl_reconciliation_id,
						aea.accounting_export_batch_id,
						at.id ar_transaction_id,
						at.post_month
					FROM
						ar_payments ap
						JOIN ar_transactions at ON ( ap.cid = at.cid AND ap.id = at.ar_payment_id )
						LEFT JOIN ar_allocations aa ON ( aa.cid = at.cid AND ( aa.charge_ar_transaction_id = at.id OR aa.credit_ar_transaction_id = at.id ) )
						LEFT JOIN ar_deposit_transactions adt ON ( adt.cid = at.cid AND adt.ar_transaction_id = at.id )
						JOIN gl_headers gh ON ( gh.cid = at.cid AND ( ( gh.reference_id = adt.ar_deposit_id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintArDepositGlTransactionTypeIds ) . ' ) )
		                                                                  OR
		                                                              ( gh.reference_id = at.id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintArTransactionGlTransactionTypeIds ) . ' ) )
		                                                                  OR
		                                                              ( gh.reference_id = aa.id AND aa.gl_transaction_type_id = gh.gl_transaction_type_id ) ) )
						JOIN gl_details gd ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id)
						LEFT JOIN accounting_export_associations aea ON ( gd.cid = aea.cid AND gd.id = aea.gl_detail_id )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND ap.id = ' . ( int ) $intArPaymentId . '
						AND at.is_deleted = false';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailsForUserValidationModeByArTransactionIdByCid( $intArTransactionId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
					    at.post_month,
					    aea.accounting_export_batch_id
					FROM
					    ar_transactions at
					    LEFT JOIN ar_allocations aa ON ( aa.cid = at.cid AND ( aa.charge_ar_transaction_id = at.id OR aa.credit_ar_transaction_id = at.id ) )
					    JOIN gl_headers gh ON ( gh.cid = at.cid AND ( ( gh.reference_id = at.id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintArTransactionGlTransactionTypeIds ) . ' ) )
		                                                              OR
		                                                              ( gh.reference_id = aa.id AND aa.gl_transaction_type_id = gh.gl_transaction_type_id ) ) )
					    JOIN gl_details gd ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id )
					    LEFT JOIN accounting_export_associations aea ON ( gd.cid = aea.cid AND gd.id = aea.gl_detail_id )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND at.id = ' . ( int ) $intArTransactionId . '
						AND at.is_deleted = false';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function buildSqlForJenarkFormat( $arrintGlDetailIds, $objGlDetailsFilter ) {

		$strSql = ' WITH sub AS (
							 SELECT
		                         CASE
		                            WHEN ( gd.amount > 0 ) THEN sum ( gd.amount )
		                   	     END AS debit_amount,
		                   		 CASE
		                     		WHEN ( gd.amount < 0 ) THEN sum ( gd.amount )
		                   		 END AS credit_amount,
		                   		 gd.cid,
		                   		 gd.property_id,
			                     gd.accrual_gl_account_id,
			                     gd.cash_gl_account_id,
			                     gh.post_date,
			                     gh.post_month,
								 ad.deposit_number,
								 ac.name as ar_code_name,
								 gh.gl_transaction_type_id AS gl_transaction_type_id
							FROM
		                 	    gl_details gd
					            JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id AND gh.cid = ' . ( int ) $objGlDetailsFilter->getCid();

		$strSql .= ' )
								LEFT JOIN ar_deposits ad ON ( ad.cid = gh.cid AND ad.id = gh.reference_id AND ad.gl_transaction_type_id = gh.gl_transaction_type_id )
                  				LEFT JOIN ar_transactions art ON ( art.cid = gh.cid AND art.id = gh.reference_id AND gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintArTransactionGlTransactionTypeIds ) . ' ) )
                  				LEFT JOIN ar_codes ac ON ( ac.cid = art.cid AND ac.id = art.ar_code_id )
							WHERE
								gd.cid = ' . ( int ) $objGlDetailsFilter->getCid() . '
								AND gd.id IN ( ' . implode( ', ', $arrintGlDetailIds ) . ' ) ';

		$strSql .= ' GROUP BY
							gh.post_month,
							ar_code_name,
							gh.post_date,
							gd.cid,
							gd.property_id,
							gd.accrual_gl_account_id,
							gd.cash_gl_account_id,
							ad.deposit_number,
							gd.amount,
							gh.gl_transaction_type_id)
						SELECT
				            CASE
      						WHEN COALESCE ( ar_code_name, deposit_number::TEXT ) != \'\'
      						THEN COALESCE ( ar_code_name, deposit_number::TEXT )
      						ELSE
								CASE
									WHEN gl_transaction_type_id IN ( ' . CGlTransactionType::AR_ALLOCATION . ', ' . CGlTransactionType::AR_ALLOCATION_REVERSAL . ' ) THEN \'Allocation\'
								ELSE
									CASE
										WHEN gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . ' THEN \'Journal Entry\'
									END
					           END
    						END AS memo,
							sum ( debit_amount ) AS amount,
							cid,
							property_id,
							accrual_gl_account_id,
							cash_gl_account_id,
							post_date,
							post_month
						FROM
							sub
						WHERE
							debit_amount > 0
						GROUP BY
							post_month,
							memo,
							post_date,
							cid,
							property_id,
							accrual_gl_account_id,
							cash_gl_account_id
						UNION
						SELECT
							CASE
      						WHEN COALESCE ( ar_code_name, deposit_number::TEXT ) != \'\'
      						THEN COALESCE ( ar_code_name, deposit_number::TEXT )
      						ELSE
								CASE
									WHEN gl_transaction_type_id IN ( ' . CGlTransactionType::AR_ALLOCATION . ', ' . CGlTransactionType::AR_ALLOCATION_REVERSAL . ' ) THEN \'Allocation\'
								ELSE
									CASE
										WHEN gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . ' THEN \'Journal Entry\'
									END
					           END
    						END AS memo,
							sum ( credit_amount ) AS amount,
							cid,
							property_id,
							accrual_gl_account_id,
							cash_gl_account_id,
							post_date,
							post_month
						FROM
							sub
						WHERE
							credit_amount < 0
						GROUP BY
							post_month,
							memo,
							post_date,
							cid,
							property_id,
							accrual_gl_account_id,
							cash_gl_account_id ';

		return $strSql;

	}

	public static function buildSqlForYardiFormat( $objGlDetailsFilter, $intGlExportBatchTypeId = NULL ) {
		$strSql = 'WITH sub AS (
						SELECT
							CASE
								WHEN ( gd.amount > 0 ) THEN sum ( gd.amount )
							END AS debit_amount,
							CASE
								WHEN ( gd.amount < 0 ) THEN sum ( gd.amount )
							END AS credit_amount,
							gd.cid,
							gd.property_id,
							gd.accrual_gl_account_id,
							gd.cash_gl_account_id,
							gh.post_date,
							gh.post_month';

		$strSql .= ' FROM
							gl_details gd
							JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id AND gh.is_template IS FALSE AND gh.cid = ' . ( int ) $objGlDetailsFilter->getCid() . ')';

		$strSql .= ' WHERE
							gd.cid = ' . ( int ) $objGlDetailsFilter->getCid();

		if( true == valArr( $objGlDetailsFilter->getGlBookIds() ) ) {
			$strSql .= ' AND gh.gl_book_id IN ( ' . implode( ', ', $objGlDetailsFilter->getGlBookIds() ) . ' ) ';
		}

		if( false == $objGlDetailsFilter->getIncludeAllUnexportedTransactions() ) {
			$strSql .= ' AND ( gh.post_month >= \'' . date( 'm/1/Y', strtotime( $objGlDetailsFilter->getStartDate() ) ) . '\'';
			if( false != valStr( $objGlDetailsFilter->getEndDate() ) ) {
				$strSql .= ' AND gh.post_month <= \'' . date( 'm/t/Y', strtotime( $objGlDetailsFilter->getEndDate() ) ) . '\'';
			}
			$strSql .= ')';
		} elseif( false != valStr( $objGlDetailsFilter->getTransactionStartDate() ) ) {
			$strSql .= ' AND gh.transaction_datetime >= \'' . date( 'Y-m-d', strtotime( $objGlDetailsFilter->getTransactionStartDate() ) ) . ' 00:00:00\'';
		}

		if( true == valArr( $objGlDetailsFilter->getPropertyIds() ) ) {
			$strSql .= ' AND gd.property_id IN (' . implode( ', ', $objGlDetailsFilter->getPropertyIds() ) . ' )';
		}

		if( true == valStr( $objGlDetailsFilter->getPostStartDate() ) && true == valStr( $objGlDetailsFilter->getPostEndDate() ) ) {
			$strSql .= ' AND ( gh.post_date >= \'' . date( 'm/d/Y', strtotime( $objGlDetailsFilter->getPostStartDate() ) ) . '\' AND gh.post_date <= \'' . date( 'm/d/Y', strtotime( $objGlDetailsFilter->getPostEndDate() ) ) . '\' )';
		}

		$strSql .= ' AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')';

		// this condition is added to not get the exported transaction for same and different export type
		$strSql .= '  AND NOT EXISTS(
								SELECT
									1
								FROM
									accounting_export_associations aea
									 JOIN accounting_export_batches aeb ON ( aea.accounting_export_batch_id = aeb.id AND aea.cid = aeb.cid )
								WHERE
									gd.id = aea.gl_detail_id
									AND gd.cid = aea.cid
									AND aeb.cid = ' . ( int ) $objGlDetailsFilter->getCid() . ' 
									AND ( aeb.transmission_vendor_id = ' . ( int ) $intGlExportBatchTypeId . '
									OR aea.created_on < \'2019-05-08\' ) )';

		$strSql .= ' GROUP BY
						gd.cid,
						gd.property_id,
						gh.post_month,
						gh.post_date,
						gd.accrual_gl_account_id,
						gd.cash_gl_account_id,
						gd.amount';
		$strSql .= ' )
					SELECT
						sum ( debit_amount ) AS amount,
						cid,
						property_id,
						accrual_gl_account_id,
						cash_gl_account_id,
						post_date,
						post_month';

		$strSql .= ' FROM
						sub
					WHERE
						debit_amount > 0
					GROUP BY
						cid,
						property_id,
						post_month,
						post_date,
						accrual_gl_account_id,
						cash_gl_account_id';

		$strSql .= ' 
					UNION
					SELECT
						sum ( credit_amount ) AS amount,
						cid,
						property_id,
						accrual_gl_account_id,
						cash_gl_account_id,
						post_date,
						post_month';

		$strSql .= ' 
					FROM
						sub
					WHERE
						credit_amount < 0
					GROUP BY
						cid,
						property_id,
						post_month,
						post_date,
						accrual_gl_account_id,
						cash_gl_account_id';

		return $strSql;
	}

	public static function buildSqlForOtherFormat( $arrintGlDetailIds, $objGlDetailsFilter, $intGlExportBatchTypeId = NULL ) {

		$strSql = 'WITH sub AS (
						SELECT
							CASE
								WHEN ( gd.amount > 0 ) THEN sum ( gd.amount )
							END AS debit_amount,
							CASE
								WHEN ( gd.amount < 0 ) THEN sum ( gd.amount )
							END AS credit_amount,
							gd.cid,
							gd.property_id,
							gd.accrual_gl_account_id,
							gd.cash_gl_account_id,
							gh.post_date,
							gh.post_month';
		if( CTransmissionVendor::GL_EXPORT_GREAT_PLAINS == $intGlExportBatchTypeId ) {
			$strSql .= ' ,gh.transaction_datetime';
		}
		$strSql .= ' FROM
							gl_details gd
							JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id AND gh.cid = ' . ( int ) $objGlDetailsFilter->getCid() . ')';

		if( CTransmissionVendor::GL_EXPORT_TIMBERLINE_PM == $intGlExportBatchTypeId ) {
			$strSql .= ' JOIN properties p ON ( gd.cid = p.cid AND gd.property_id = p.id )
                			 LEFT JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.key IN ( \'TIMBERLINE_PM_EXCLUDE_AP_TRANSACTION\' ) )
                  			 JOIN gl_transaction_types gtt ON ( gd.gl_transaction_type_id = gtt.id )';
		}
		$strSql .= ' WHERE
							gd.cid = ' . ( int ) $objGlDetailsFilter->getCid() . '
							AND gd.id IN ( ' . implode( ',', $arrintGlDetailIds ) . ' )';
		if( CTransmissionVendor::GL_EXPORT_TIMBERLINE_PM == $intGlExportBatchTypeId ) {
			$strSql .= ' AND CASE
	                        WHEN pp.value = \'1\' THEN gtt.gl_ledger_type_id NOT IN (' . CGlLedgerType::AP . ')
	                        ELSE TRUE
	                      END';
		}
		$strSql .= ' GROUP BY
						gd.cid,
						gd.property_id,
						gh.post_month,
						gh.post_date,
						gd.accrual_gl_account_id,
						gd.cash_gl_account_id,
						gd.amount';
		if( CTransmissionVendor::GL_EXPORT_GREAT_PLAINS == $intGlExportBatchTypeId ) {
			$strSql .= ' ,gh.transaction_datetime ';
		}
		$strSql .= ' )
					SELECT
						sum ( debit_amount ) AS amount,
						cid,
						property_id,
						accrual_gl_account_id,
						cash_gl_account_id,
						post_date,
						post_month';
		if( CTransmissionVendor::GL_EXPORT_GREAT_PLAINS == $intGlExportBatchTypeId ) {
			$strSql .= ' ,transaction_datetime ';
		}
		$strSql .= ' FROM
						sub
					WHERE
						debit_amount > 0
					GROUP BY
						cid,
						property_id,
						post_month,
						post_date,
						accrual_gl_account_id,
						cash_gl_account_id';
		if( CTransmissionVendor::GL_EXPORT_GREAT_PLAINS == $intGlExportBatchTypeId ) {
			$strSql .= ' ,transaction_datetime ';
		}
		$strSql .= ' 
					UNION
					SELECT
						sum ( credit_amount ) AS amount,
						cid,
						property_id,
						accrual_gl_account_id,
						cash_gl_account_id,
						post_date,
						post_month';
		if( CTransmissionVendor::GL_EXPORT_GREAT_PLAINS == $intGlExportBatchTypeId ) {
			$strSql .= ' ,transaction_datetime ';
		}
		$strSql .= ' 
					FROM
						sub
					WHERE
						credit_amount < 0
					GROUP BY
						cid,
						property_id,
						post_month,
						post_date,
						accrual_gl_account_id,
						cash_gl_account_id';
		if( CTransmissionVendor::GL_EXPORT_GREAT_PLAINS == $intGlExportBatchTypeId ) {
			$strSql .= ' ,transaction_datetime ';
		}

		return $strSql;
	}

	public static function buildSqlForOnesiteFormat( $arrintGlDetailIds, $objGlDetailsFilter ) {

		$strSql = 'WITH sub AS (
						SELECT
							CASE
								WHEN ( gd.amount > 0 ) THEN sum ( gd.amount )
							END AS debit_amount,
							CASE
								WHEN ( gd.amount < 0 ) THEN sum ( gd.amount )
							END AS credit_amount,
							gd.cid,
							gd.property_id,
							gd.accrual_gl_account_id,
							gd.cash_gl_account_id,
							gh.post_date,
							gh.post_month,
							gh.reference AS memo
						FROM
							gl_details gd
							JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id AND gh.cid = ' . ( int ) $objGlDetailsFilter->getCid() . ' )
						WHERE
							gd.cid = ' . ( int ) $objGlDetailsFilter->getCid() . '
							AND gd.id IN ( ' . implode( ', ', $arrintGlDetailIds ) . ' ) ';

		$strSql .= ' GROUP BY
						gd.cid,
						gd.property_id,
						gh.post_month,
						gh.reference,
						gh.post_date,
						gd.accrual_gl_account_id,
						gd.cash_gl_account_id,
						gd.amount )
					SELECT
						sum ( debit_amount ) AS amount,
						cid,
						property_id,
						accrual_gl_account_id,
						cash_gl_account_id,
						post_date,
						post_month,
				        memo
					FROM
						sub
					WHERE
						debit_amount > 0
					GROUP BY
						cid,
						property_id,
						post_month,
						post_date,
						memo,
						accrual_gl_account_id,
						cash_gl_account_id
					UNION
					SELECT
						sum ( credit_amount ) AS amount,
						cid,
						property_id,
						accrual_gl_account_id,
						cash_gl_account_id,
						post_date,
						post_month,
						memo
					FROM
						sub
					WHERE
						credit_amount < 0
					GROUP BY
						cid,
						property_id,
						post_month,
						post_date,
						memo,
						accrual_gl_account_id,
						cash_gl_account_id';

		return $strSql;
	}

	public static function buildSqlForMriFormat( $arrintGlDetailIds, $objGlDetailsFilter ) {

		$strSql = 'WITH sub AS (
						SELECT
							CASE
								WHEN ( gd.amount > 0 ) THEN sum ( gd.amount )
							END AS debit_amount,
							CASE
								WHEN ( gd.amount < 0 ) THEN sum ( gd.amount )
							END AS credit_amount,
							gd.cid,
							gd.property_id,
							gd.accrual_gl_account_id,
							gd.cash_gl_account_id,
							gh.post_date,
							gh.post_month,
							min( gh.reference ) AS memo
						FROM
							gl_details gd
							JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id AND gh.cid = ' . ( int ) $objGlDetailsFilter->getCid() . ' ) 
						WHERE
							gd.cid = ' . ( int ) $objGlDetailsFilter->getCid() . '
							AND gd.id IN ( ' . implode( ',', $arrintGlDetailIds ) . ' )';
		$strSql .= ' GROUP BY
						gd.cid,
						gd.property_id,
						gh.post_month,
						gh.post_date,
						gd.accrual_gl_account_id,
						gd.cash_gl_account_id,
						gd.amount )
					SELECT
						sum ( debit_amount ) AS amount,
						cid,
						property_id,
						accrual_gl_account_id,
						cash_gl_account_id,
						post_date,
						post_month,
						min( memo ) AS memo
					FROM
						sub
					WHERE
						debit_amount > 0
					GROUP BY
						cid,
						property_id,
						post_month,
						post_date,
						accrual_gl_account_id,
						cash_gl_account_id
					UNION
					SELECT
						sum ( credit_amount ) AS amount,
						cid,
						property_id,
						accrual_gl_account_id,
						cash_gl_account_id,
						post_date,
						post_month,
						min( memo ) AS memo
					FROM
						sub
					WHERE
						credit_amount < 0
					GROUP BY
						cid,
						property_id,
						post_month,
						post_date,
						accrual_gl_account_id,
						cash_gl_account_id';

		return $strSql;
	}

	public static function fetchGlDetailsByGlHeaderScheduleIdByCid( $intGlHeaderScheduleId, $intCid, $objClientDatabase ) {

		if( false == valId( $intGlHeaderScheduleId ) ) return NULL;

		$strSql = 'SELECT
						ad.property_id
					FROM
						gl_details ad
						JOIN gl_headers gh ON ( ad.cid = gh.cid AND ad.gl_header_id = gh.id )
						JOIN gl_header_schedules ghs ON ( gh.cid = ghs.cid AND gh.gl_header_schedule_id = ghs.id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ghs.id = ' . ( int ) $intGlHeaderScheduleId;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailsByGlHeaderIdsByCids( $arrintGlHeaderIds, $arrintCids, $objClientDatabase ) {

		if( false == valIntArr( $arrintGlHeaderIds ) || false == valIntArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						gd.*,
						CASE
							WHEN
								gd.cash_gl_account_id IS NOT NULL
							THEN
								cash_gat.name
							ELSE
								accrual_gat.name
						END AS account_name,
						CASE
							WHEN
								gd.cash_gl_account_id IS NOT NULL
							THEN
								cash_gat.account_number
							ELSE
								accrual_gat.account_number
						END AS account_number,
						p.property_name AS property_name,
						cd.name as company_department_name,
						gdim.name as custom_tag_name
					FROM
						gl_details gd
						LEFT JOIN gl_account_trees accrual_gat ON ( gd.cid = accrual_gat.cid AND gd.accrual_gl_account_id = accrual_gat.gl_account_id AND accrual_gat.is_default = 1 )
						LEFT JOIN gl_account_trees cash_gat ON ( gd.cid = cash_gat.cid AND gd.cash_gl_account_id = cash_gat.gl_account_id AND cash_gat.is_default = 1)
						LEFT JOIN properties p ON ( gd.cid = p.cid AND gd.property_id = p.id )
						LEFT JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id AND gh.offsetting_gl_header_id IS NULL )
						LEFT JOIN company_departments cd ON ( gd.cid = cd.cid AND gd.company_department_id = cd.id )
						LEFT JOIN gl_dimensions gdim ON ( gd.cid = gdim.cid AND gd.gl_dimension_id = gdim.id )
					WHERE
						gd.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND gd.gl_header_id IN ( ' . implode( ',', $arrintGlHeaderIds ) . ' )
					ORDER BY
						gd.id ASC';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchArGlDetailsByReferenceIdsByGlTransactionTypeIdsByCid( $arrintReferenceIds, $arrintGlTransactionTypeIds, $intCid, $objClientDatabase, $boolExportedOnly = false ) {

		$strJoin = '';

		$strJoin .= ( true == $boolExportedOnly ) ? ' JOIN accounting_export_associations aea ON ( gd.cid = aea.cid AND gd.id = aea.gl_detail_id )' : '';

		$strSql	= 'SELECT
						*
					FROM
						gl_details gd
						JOIN gl_headers gh ON( gh.cid = gd.cid AND gh.id = gd.gl_header_id )';
		$strSql .= $strJoin;

		$strSql .= ' WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND gh.reference_id IN ( ' . implode( ',', $arrintReferenceIds ) . ' )' . '
						AND gh.gl_transaction_type_id IN ( ' . implode( ',', $arrintGlTransactionTypeIds ) . ' )';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailsCountByJobPhaseIdsByTransactionTypeIdByCid( $arrintJobPhaseIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintJobPhaseIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT( DISTINCT( gd.id ) )
					FROM
						gl_details gd
						JOIN gl_headers gh ON( gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.gl_header_type_id = ' . CGlHeaderType::STANDARD . ' )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . '
						AND gd.job_phase_id IN ( ' . implode( ',', $arrintJobPhaseIds ) . ' )';

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchGlDetailsByDrawRequestIdByCid( $intDrawRequestId, $intCid, $objDatabase ) {
		if( false == valId( $intDrawRequestId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'WITH draw_request_data AS (
						SELECT dr.id,
							dr.cid,
							dr.ar_transaction_id,
							adt.ar_transaction_id AS payment_ar_transaction_id, 
							aa.id AS ar_allocation_id,
							adt.ar_deposit_id
						FROM 
							draw_requests dr
							LEFT JOIN ar_deposit_transactions adt ON ( dr.ar_deposit_transaction_id = adt.id AND dr.cid = adt.cid )
							LEFT JOIN ar_allocations aa ON (dr.cid = aa.cid AND dr.ar_transaction_id = aa.charge_ar_transaction_id AND adt.ar_transaction_id = aa.credit_ar_transaction_id)
						WHERE 
							dr.id = ' . ( int ) $intDrawRequestId . '
							AND dr.cid = ' . ( int ) $intCid . '), 
				gl_detail_data AS (
					SELECT 
						gd.*,
						gh.post_month AS header_post_month,
						gh.post_date
					FROM 
						gl_details gd
						JOIN gl_headers gh ON( gh.cid = gd.cid AND gd.gl_header_id = gh.id) 
						JOIN draw_request_data drd ON( gd.cid = drd.cid )
					WHERE 
						gd.cid = drd.cid AND
						(
							(gd.gl_transaction_type_id = ' . CGlTransactionType::AR_CHARGE . ' AND gd.reference_id = drd.ar_transaction_id) OR
							(gd.gl_transaction_type_id = ' . CGlTransactionType::AR_PAYMENT . ' AND gd.reference_id = drd.payment_ar_transaction_id) OR
							(gd.gl_transaction_type_id = ' . CGlTransactionType::AR_ALLOCATION . ' AND gd.reference_id = drd.ar_allocation_id) OR
							(gd.gl_transaction_type_id = ' . CGlTransactionType::AR_DEPOSIT . ' AND gd.reference_id = drd.ar_deposit_id)
						)
				) 
			SELECT 
				gdd.header_post_month,
				gdd.post_date,
				gdd.amount,
				gat.formatted_account_number AS account_number,
				gat.name AS account_name,
				gdd.gl_transaction_type_id,
				gt.name
			FROM 
				gl_detail_data gdd
				LEFT JOIN gl_accounts ga ON ( ga.cid = gdd.cid AND gdd.accrual_gl_account_id = ga.id )
				LEFT JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1 )
				JOIN gl_transaction_types gt ON( gdd.gl_transaction_type_id = gt.id )
			ORDER BY
				gdd.gl_transaction_type_id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCustomGlDetailsByGlExportBatchId( $intGlExportBatchId, $intGlTreeId, $intCid, $objDatabase, $intGlExportBookTypeId = NULL ) {

		$strSql = '
					SELECT
					      sum( gd.amount) as batch_details
					FROM
					    gl_details AS gd
					    JOIN accounting_export_associations aea ON (aea.gl_detail_id = gd.id AND aea.cid = gd.cid)
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND aea.accounting_export_batch_id = ' . ( int ) $intGlExportBatchId . '
						AND gd.amount > 0
						AND EXISTS (
					                 SELECT
					                     1
					                 FROM
					                     gl_headers gh
					                 WHERE
					                     gh.id = gd.gl_header_id
					                     AND gd.cid = gh.cid
					                     AND gh.is_template = FALSE
					                     AND gh.gl_header_status_type_id NOT IN ( ' . implode( ',', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
					    )
					    AND EXISTS (
					                 SELECT
					                     1
					                 FROM
					                     gl_account_trees gat
					                 WHERE
					                     gd.cid = gat.cid';
		if( self::GL_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			$strSql .= ' AND ( gat.gl_account_id = gd.cash_gl_account_id OR gat.gl_account_id = gd.accrual_gl_account_id )';
		} elseif( self::GL_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
			$strSql .= ' AND gat.gl_account_id = gd.cash_gl_account_id';
		} else {
			$strSql .= ' AND gat.gl_account_id = gd.accrual_gl_account_id';
		}

		$strSql .= ' AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
						)
					UNION
					SELECT
					     count( gd.* ) as count
					FROM
					    gl_details AS gd
					    JOIN accounting_export_associations aea ON (aea.gl_detail_id = gd.id AND aea.cid = gd.cid)
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND aea.accounting_export_batch_id = ' . ( int ) $intGlExportBatchId . '
						AND gd.amount <> 0
						AND EXISTS (
					                 SELECT
					                     1
					                 FROM
					                     gl_headers gh
					                 WHERE
					                     gh.id = gd.gl_header_id
					                     AND gd.cid = gh.cid
					                     AND gh.is_template = FALSE
					                     AND gh.gl_header_status_type_id NOT IN ( ' . implode( ',', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
					    )
					    AND EXISTS (
					                 SELECT
					                     1
					                 FROM
					                     gl_account_trees gat
					                 WHERE
					                     gd.cid = gat.cid';

		if( self::GL_BOOK_TYPE_BOTH == $intGlExportBookTypeId ) {
			$strSql .= ' AND ( gat.gl_account_id = gd.cash_gl_account_id OR gat.gl_account_id = gd.accrual_gl_account_id )';
		} elseif( self::GL_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
			$strSql .= ' AND gat.gl_account_id = gd.cash_gl_account_id';
		} else {
			$strSql .= ' AND gat.gl_account_id = gd.accrual_gl_account_id';
		}
		$strSql .= ' AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
						)';

		$arrmixResults = fetchData( $strSql, $objDatabase );
		if( false == valArr( $arrmixResults ) ) {
			return false;
		}
		$arrmixResult['line_item_count'] = $arrmixResults[0]['batch_details'];
		$arrmixResult['batch_amount'] = $arrmixResults[1]['batch_details'];
		return $arrmixResult;

	}

	public static function fetchGlDetailsByReclassGlDetailIdsByGlTransactionTypeIdsByCid( $arrintReclassGlDetailIds, $arrintGlTransactionTypeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintReclassGlDetailIds ) || false == valArr( $arrintGlTransactionTypeIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						gl_details
					WHERE
						cid = ' . ( int ) $intCid . '
						AND reclass_gl_detail_id IN ( ' . implode( ',', $arrintReclassGlDetailIds ) . ' )
						AND gl_transaction_type_id IN ( ' . implode( ',', $arrintGlTransactionTypeIds ) . ' )';

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlDetailCountByOffsettingGlDetailIdsByCid( $arrintOffsettingGlDetailIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintOffsettingGlDetailIds ) ) {
			return 0;
		}

		$strSql = 'SELECT
						count( * )
					FROM
						gl_details
					WHERE
						offsetting_gl_detail_id IN ( ' . implode( ',', $arrintOffsettingGlDetailIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchGlDetailsForGlExports( $objGlDetailsFilter, $intTransmissionVendorId, $objDatabase ) {

		$strSql = '
			SELECT
						gd.id,
						gd.cid
					FROM
						gl_details gd
						JOIN gl_headers gh ON ( gd.gl_header_id = gh.id AND gd.cid = gh.cid AND gh.is_template IS FALSE )
						JOIN gl_account_trees gat ON ( gat.cid = gd.cid AND ( gat.gl_account_id = gd.accrual_gl_account_id OR gat.gl_account_id = gd.cash_gl_account_id ) ) ';

		if( CTransmissionVendor::GL_EXPORT_TIMBERLINE_PM == $intTransmissionVendorId ) {
			$strSql .= 'JOIN properties p ON ( gd.cid = p.cid AND gd.property_id = p.id )
          				LEFT JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.key IN ( \'TIMBERLINE_PM_EXCLUDE_AP_TRANSACTION\' ) )
          				JOIN gl_transaction_types gtt ON ( gd.gl_transaction_type_id = gtt.id ) ';
		}
		$strSql .= 'WHERE
						gh.cid = ' . ( int ) $objGlDetailsFilter->getCid() . '
										';
		if( true == valArr( $objGlDetailsFilter->getPropertyIds() ) ) {
			$strSql .= 'AND gd.property_id IN (' . implode( ', ', $objGlDetailsFilter->getPropertyIds() ) . ' ) ';
		}
		if( false == $objGlDetailsFilter->getIncludeAllUnexportedTransactions() ) {
			$strSql .= 'AND ( gh.post_month >= \'' . date( 'm/1/Y', strtotime( $objGlDetailsFilter->getEndDate() ) ) . '\' AND gh.post_month <= \'' . date( 'm/t/Y', strtotime( $objGlDetailsFilter->getEndDate() ) ) . '\' ) ';
		}

		if( true == valArr( $objGlDetailsFilter->getGlBookIds() ) ) {
			$strSql .= 'AND gh.gl_book_id IN ( ' . implode( ', ', $objGlDetailsFilter->getGlBookIds() ) . ' ) ';
		}

		$strSql .= 'AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')';

		$strSql .= ' AND NOT EXISTS (
                   SELECT 1
                    FROM accounting_export_associations aea
                         JOIN accounting_export_batches aeb ON (aea.accounting_export_batch_id = aeb.id AND aea.cid = aeb.cid)
                    WHERE gd.id = aea.gl_detail_id AND
                          gd.cid = aea.cid AND
                          aeb.cid = ' . ( int ) $objGlDetailsFilter->getCid() . ' AND
                          ( aeb.transmission_vendor_id = ' . ( int ) $intTransmissionVendorId . ' OR aea.created_on < \'2019-05-08\' ) )';

		if( true == valStr( $objGlDetailsFilter->getPostStartDate() ) && true == valStr( $objGlDetailsFilter->getPostEndDate() ) ) {

			$strSql .= 'AND ( gh.post_date >= \'' . date( 'm/d/Y', strtotime( $objGlDetailsFilter->getPostStartDate() ) ) . '\' AND gh.post_date <= \'' . date( 'm/d/Y', strtotime( $objGlDetailsFilter->getPostEndDate() ) ) . '\' )';
		}

		if( CTransmissionVendor::GL_EXPORT_TIMBERLINE_PM == $intTransmissionVendorId ) {
			if( true == valStr( $objGlDetailsFilter->getPostStartDate() ) && true == valStr( $objGlDetailsFilter->getPostEndDate() ) ) {

				$strSql .= 'AND ( gh.post_date >= \'' . date( 'm/d/Y', strtotime( $objGlDetailsFilter->getPostStartDate() ) ) . '\' AND gh.post_date <= \'' . date( 'm/d/Y', strtotime( $objGlDetailsFilter->getPostEndDate() ) ) . '\' )';
			}
			$strSql .= ' AND CASE
								WHEN pp.value = \'1\' THEN gtt.gl_ledger_type_id NOT IN (' . CGlLedgerType::AP . ')
								ELSE TRUE
							END';

		}

		$strSql .= ' GROUP BY gd.id,gd.cid';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllGlDetailsByGlHeaderIdsByCid( $arrintGlHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintGlHeaderIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT 
						* 
					FROM 
						gl_details 
					WHERE 
						gl_header_id IN ( ' . sqlIntImplode( $arrintGlHeaderIds ) . ' ) 
					AND cid = ' . ( int ) $intCid;

		return self::fetchGlDetails( $strSql, $objClientDatabase );
	}

	public static function fetchAssocciatedGlDetails( $objGlDetailsFilter, $objClientDatabase ) {

		$strSql = '
					SELECT
						gd.id,
						gd.cid
					FROM
						gl_details gd
						JOIN gl_headers gh ON ( gd.gl_header_id = gh.id AND gd.cid = gh.cid AND gh.is_template IS FALSE )
						LEFT JOIN gl_account_trees accrual_gl_account ON ( gd.cid = accrual_gl_account.cid AND gd.accrual_gl_account_id = accrual_gl_account.gl_account_id )
						LEFT JOIN gl_account_trees cash_gl_account ON ( gd.cid = cash_gl_account.cid AND gd.cash_gl_account_id = cash_gl_account.gl_account_id )
					WHERE
						gh.cid = ' . ( int ) $objGlDetailsFilter->getCid() . '
						AND NOT EXISTS(
							SELECT
								1
							FROM 
								accounting_export_associations
							WHERE 
								gd.id = gl_detail_id
								AND gd.cid = cid
							    AND cid = ' . ( int ) $objGlDetailsFilter->getCid() . '
							)';

		if( false == $objGlDetailsFilter->getIncludeAllUnexportedTransactions() ) {
			$strSql .= ' AND ( gh.post_month >= \'' . date( 'm/1/Y', strtotime( $objGlDetailsFilter->getStartDate() ) ) . '\'';
			if( false != valStr( $objGlDetailsFilter->getEndDate() ) ) {
				$strSql .= ' AND gh.post_month <= \'' . date( 'm/t/Y', strtotime( $objGlDetailsFilter->getEndDate() ) ) . '\'';
			}
			$strSql .= ')';
		}
		$strSql .= ' AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')
						AND ( accrual_gl_account.gl_tree_id = ' . ( int ) $objGlDetailsFilter->getGlTreeId() . ' OR cash_gl_account.gl_tree_id = ' . ( int ) $objGlDetailsFilter->getGlTreeId() . ' ) ';

		if( true == valArr( $objGlDetailsFilter->getPropertyIds() ) ) {
			$strSql .= ' AND gd.property_id IN (' . implode( ', ', $objGlDetailsFilter->getPropertyIds() ) . ' )';
		}

		$strSql .= ' GROUP BY gd.id, gd.cid';

		return fetchData( $strSql, $objClientDatabase );

	}

	public static function fetchGlTotalByPostMonthByPropertyIdsByGlAccountTypeIdByAccountingMethodByCid( $strPostMonth, $arrintPropertyIds, $intGlAccountTypeId, $boolCashAccountingMethod, $intCid, $objClientDatabase ) {

		if( false == valStr( $strPostMonth ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strAccountingMethod = ( true == $boolCashAccountingMethod ) ? 'cash' : 'accrual';

		$strSql = 'SELECT
						gd.property_id,
						SUM ( gd.amount ) AS ' . $strAccountingMethod . '_amount
					FROM
						gl_details gd
						JOIN gl_headers gh ON ( gd.cid = gh.cid AND gh.id = gd.gl_header_id AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ') AND gh.is_template = FALSE )
						JOIN gl_accounts ga ON ( gd.cid = ga.cid AND ga.id = gd.' . $strAccountingMethod . '_gl_account_id )
					WHERE
						gd.cid = ' . ( int ) $intCid . '
						AND gd.post_month = \'' . $strPostMonth . '\'
						AND gd.' . $strAccountingMethod . '_gl_account_id IS NOT NULL
						AND gd.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND EXISTS (
							SELECT 1
								FROM gl_account_trees gat 
								JOIN gl_groups gg ON ( gat.cid = gg.cid AND gat.gl_group_id = gg.id )
								JOIN gl_trees gt ON ( gt.cid = gat.cid AND gt.id = gat.gl_tree_id )
								WHERE 
								ga.cid = gat.cid AND gat.gl_account_id = ga.id
								AND gg.gl_account_type_id = ' . ( int ) $intGlAccountTypeId . '
								AND gt.gl_tree_type_id = ' . CGlTreeType::STANDARD . '
								AND gt.system_code = \'DEFAULT\' 
								)
					GROUP BY
						1';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCustomGlDetailsByExportTypeIdByCid( $intExportTypeId, $objGlDetailsFilter, $objClientDatabase ) {

		$strSql = 'SELECT 
						DISTINCT gd.id
					FROM 
						gl_details gd
						JOIN gl_headers gh ON( gh.id = gd.gl_header_id AND gh.cid = gd.cid AND gh.is_template IS FALSE )
					JOIN gl_account_trees gat ON ( gd.cid = gat.cid AND ( gat.gl_account_id = ';
		if( self::GL_BOOK_TYPE_BOTH == $objGlDetailsFilter->getGlExportBookTypeId() ) {
		$strSql .= ' gd.cash_gl_account_id OR gat.gl_account_id = gd.accrual_gl_account_id ) )';
		} else {
		$strSql .= ( self::GL_BOOK_TYPE_ACCRUAL == $objGlDetailsFilter->getGlExportBookTypeId() ) ? 'gd.accrual_gl_account_id ) )' : 'gd.cash_gl_account_id ) )';
		}
		$strSql .= ' WHERE 
						gd.cid = ' . ( int ) $objGlDetailsFilter->getCid();
		if( true == valArr( $objGlDetailsFilter->getGlBookIds() ) ) {
			$strSql .= ' AND gh.gl_book_id IN ( ' . implode( ', ', $objGlDetailsFilter->getGlBookIds() ) . ' ) ';
		}
		if( CTransmissionVendor::GL_EXPORT_YARDI_SIPP == $intExportTypeId ) {
			$strSql .= ' AND gat.secondary_number IS NOT NULL';
		}

		if( false == $objGlDetailsFilter->getIncludeAllUnexportedTransactions() ) {
			$strSql .= ' AND ( gh.post_month >= \'' . date( 'm/1/Y', strtotime( $objGlDetailsFilter->getStartDate() ) ) . '\'';
			if( false != valStr( $objGlDetailsFilter->getEndDate() ) ) {
				$strSql .= ' AND gh.post_month <= \'' . date( 'm/t/Y', strtotime( $objGlDetailsFilter->getEndDate() ) ) . '\'';
			}
			$strSql .= ')';
		} elseIf( true == valStr( $objGlDetailsFilter->getTransactionStartDate() ) ) {
			$strSql .= ' AND gh.transaction_datetime >= \'' . date( 'Y-m-d', strtotime( $objGlDetailsFilter->getTransactionStartDate() ) ) . ' 00:00:00\'';
		}

		if( true == valArr( $objGlDetailsFilter->getPropertyIds() ) ) {
			$strSql .= ' AND gd.property_id IN (' . implode( ', ', $objGlDetailsFilter->getPropertyIds() ) . ' )';
		}

		if( true == valStr( $objGlDetailsFilter->getPostStartDate() ) && true == valStr( $objGlDetailsFilter->getPostEndDate() ) ) {
			$strSql .= ' AND ( gh.post_date >= \'' . date( 'm/d/Y', strtotime( $objGlDetailsFilter->getPostStartDate() ) ) . '\' AND gh.post_date <= \'' . date( 'm/d/Y', strtotime( $objGlDetailsFilter->getPostEndDate() ) ) . '\' )';
		}

		$strSql .= ' AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')';

		// this condition is added to not get the exported transaction for same and different export type
		$strSql .= '  AND NOT EXISTS(
								SELECT
									1
								FROM
									accounting_export_associations aea
									 JOIN accounting_export_batches aeb ON ( aea.accounting_export_batch_id = aeb.id AND aea.cid = aeb.cid )
								WHERE
									gd.id = aea.gl_detail_id
									AND gd.cid = aea.cid
									AND aeb.cid = ' . ( int ) $objGlDetailsFilter->getCid() . ' 
									AND ( aeb.transmission_vendor_id = ' . ( int ) $intExportTypeId . '
									OR aea.created_on < \'2019-05-08\' ) )';
		if( false != valArr( $objGlDetailsFilter->getGlTransactionTypeIds() ) ) {
			$strSql .= ' AND gh.gl_transaction_type_id IN ( ' . implode( ',', $objGlDetailsFilter->getGlTransactionTypeIds() ) . ' )';
		}

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function buildSummarySqlForQuickBooksFormat( $intGlExportBatchId, $intCid, $objClientDatabase, $intGlExportBookTypeId, $intGlTreeId ) {
		$strSql = '
					SELECT
						CASE
							WHEN TO_CHAR( gh.post_month, \'mm/YYYY\' ) = TO_CHAR( gh.transaction_datetime, \'mm/YYYY\' ) THEN TO_CHAR( gh.transaction_datetime, \'mm/dd/YYYY\' )
							ELSE TO_CHAR( gh.post_month, \'mm/01/YYYY\' )
							END AS post_date,
						gd.amount as amount,
						ad.deposit_number,
						payments.payment_number,
						aeb.batch_datetime as transaction_datetime,
						invoices.header_number as invoice_header_number,
						payments.invoice_number as payment_invoice_number,
						app.company_name as company_name1,
						app2.company_name as company_name2,
						c.name_full as customer_name,
						gh.header_number as gl_headers_header_number,
						invoices.description as line_description,
						gd.cash_gl_account_id,
						gd.accrual_gl_account_id,
						payments.description as payment_description,
						gd.gl_transaction_type_id as gd_gl_transaction_type_id,
						payments.gl_transaction_type_id as payments_gl_transaction_type_id,
						gd.id as gl_detail_id
						';
		if( SELF::GL_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strSql .= ',gat.secondary_number,
							gat.account_number,
							gat.name as account_name';
		}
		$strJoinSql = ' FROM
						gl_headers gh
						JOIN gl_details gd ON (gh.id = gd.gl_header_id AND gd.cid = gh.cid)
						JOIN accounting_export_associations aea ON (aea.gl_detail_id = gd.id AND aea.cid = gd.cid)
						JOIN accounting_export_batches aeb ON (aea.accounting_export_batch_id = aeb.id AND aea.cid = aeb.cid)
						JOIN properties p ON (gd.property_id = p.id and gd.cid = p.cid)
						JOIN property_preferences pp ON ( pp.cid = gd.cid AND pp.property_id = gd.property_id AND pp.key = ' . pg_escape_literal( $objClientDatabase->getHandle(), 'GL_EXPORT_BATCH_TYPE_SELECT' ) . ' AND string_to_array(trim(pp.value, \'[]\'), \',\') @> string_to_array( \'' . CTransmissionVendor::GL_EXPORT_QUICK_BOOKS . '\', \',\') )';

		$strJoinSql .= ' LEFT JOIN ar_deposits ad ON ( gh.reference_id = ad.id AND ad.cid = gh.cid )
						LEFT JOIN
						(
							SELECT
							invApHeader.header_number,
							invAd.description,
							invApHeader.cid,
							invApheader.id,
							invapheader.ap_payee_id,
							invApHeader.lease_customer_id,
							invAd.id as ap_detail_id
						FROM
							ap_headers invApHeader
							JOIN ap_details invAd ON (invAd.cid = invApHeader.cid AND invApHeader.id = invAd.ap_header_id)
						WHERE
							invApHeader.cid = ' . ( int ) $intCid . '
						) invoices ON (invoices.cid = gd.cid AND invoices.id = gh.reference_id AND gh.gl_transaction_type_id = ' . CGlTransactionType::AP_CHARGE . ' AND invoices.ap_detail_id = gd.ap_detail_id )
						 LEFT JOIN
						(
							SELECT
								paymentAa.cid,
								paymentAa.id,
								invoice.header_number AS invoice_number,
								paymentApHeader.ap_payee_id,
								paymentApHeader.lease_customer_id,
								paymentApHeader.gl_transaction_type_id,
								ap.payment_number,
								paymentAd.description
							FROM
								ap_allocations paymentAa
								JOIN ap_details paymentAd ON (paymentAa.cid = paymentAd.cid AND paymentAa.credit_ap_detail_id = paymentAd.id)
								JOIN ap_headers paymentApHeader ON (paymentApHeader.cid = paymentAd.cid AND paymentApHeader.id = paymentAd.ap_header_id)
								JOIN ap_payments ap ON (ap.cid = paymentApHeader.cid AND ap.id = paymentApHeader.ap_payment_id)
								JOIN ap_details invoiceAd ON (paymentAa.cid = paymentAd.cid AND paymentAa.charge_ap_detail_id = invoiceAd.id)
								JOIN ap_headers invoice ON (invoice.cid = invoiceAd.cid AND invoice.id = invoiceAd.ap_header_id)
							WHERE paymentApHeader.cid = ' . ( int ) $intCid . '
						) payments ON (payments.cid = gh.cid AND payments.id = gh.reference_id)
						LEFT JOIN ap_payees app ON (invoices.cid = gh.cid AND app.id = invoices.ap_payee_id )
						LEFT JOIN ap_payees app2 ON (payments.cid = gh.cid AND app2.id = payments.ap_payee_id )
						LEFT JOIN lease_customers lc ON (lc.id = COALESCE(payments.lease_customer_id, invoices.lease_customer_id) AND lc.cid = gh.cid)
						LEFT JOIN customers c ON (c.id = lc.customer_id AND lc.cid = c.cid)';
		if( CAccountingExportBatch::GL_EXPORT_BOOK_TYPE_ACCRUAL == $intGlExportBookTypeId ) {
			$strJoinSql .= ' JOIN gl_account_trees gat ON ( gd.cid = gat.cid AND gat.gl_account_id = gd.accrual_gl_account_id )';
		}
		if( CAccountingExportBatch::GL_EXPORT_BOOK_TYPE_CASH == $intGlExportBookTypeId ) {
			$strJoinSql .= ' JOIN gl_account_trees gat ON ( gd.cid = gat.cid AND gat.gl_account_id = gd.cash_gl_account_id )';
		}
		$strWhereSql = ' WHERE
						gh.cid = ' . ( int ) $intCid . '
						AND gh.is_template = FALSE
						AND aea.accounting_export_batch_id = ' . ( int ) $intGlExportBatchId . '
						AND gh.gl_header_status_type_id NOT IN (' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ')';
		if( SELF::GL_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strWhereSql .= 'AND gat.gl_tree_id = ' . ( int ) $intGlTreeId;
		}

		$strGroupSql = ' GROUP BY
						post_date,
						deposit_number,
						payment_number,
						transaction_datetime,
						company_name1,
						company_name2,
						customer_name,
						CASE
						WHEN gd_gl_transaction_type_id = ' . CGlTransactionType:: AP_CHARGE . ' OR ( payments_gl_transaction_type_id = ' . CGlTransactionType::AP_CHARGE . ' AND gd_gl_transaction_type_id = ' . CGlTransactionType::AP_ALLOCATION . ' ) THEN gl_detail_id
						  ELSE 1
						END,
						invoice_header_number,
						gl_headers_header_number,
						line_description,
						cash_gl_account_id,
						accrual_gl_account_id,
						CASE
							WHEN cash_gl_account_id != accrual_gl_account_id THEN payment_description
							ELSE NULL
						END';

		$strSelectColumns = '
			post_date,
				sum(amount) as amount,
				deposit_number,
				payment_number,
				transaction_datetime,
				COALESCE(invoice_header_number, string_agg(payment_invoice_number, \',\')) as invoice_number,
				COALESCE(company_name1, company_name2) as vendor_name,
				customer_name,
				gl_headers_header_number as header_number,
				line_description,
				cash_gl_account_id,
				accrual_gl_account_id,
				CASE
					WHEN cash_gl_account_id != accrual_gl_account_id THEN payment_description
					ELSE NULL
				END as payment_line_description';

		if( SELF::GL_BOOK_TYPE_BOTH != $intGlExportBookTypeId ) {
			$strSelectColumns .= ',secondary_number,
				account_number,
				account_name';
			$strGroupSql .= ',account_number,
						secondary_number,
						account_name
						ORDER BY
							post_date,
							CASE
								WHEN cash_gl_account_id != accrual_gl_account_id THEN payment_description
								ELSE NULL
							END DESC';
		}
		$strSqlResult = 'WITH sub as (
				' . $strSql. $strJoinSql . $strWhereSql . ')
					SELECT
				' . $strSelectColumns . '
			FROM sub
				' . $strGroupSql;
		return fetchData( $strSqlResult, $objClientDatabase );

	}

}
?>
