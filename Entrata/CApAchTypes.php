<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApAchTypes
 * Do not add any new functions to this class.
 */

class CApAchTypes extends CBaseApAchTypes {

	public static function fetchApAchTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CApAchType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchApAchType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApAchType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>