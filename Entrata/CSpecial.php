<?php

use Psi\Libraries\UtilDates\CDates;

class CSpecial extends CBaseSpecial {

	protected $m_intRateIntervalOccurances;
	protected $m_intRateIntervalStart;
	protected $m_intArTriggerId;
	protected $m_intNoOfUnitTypeIds;
	protected $m_intNoOfUnitSpaceIds;
	protected $m_intPropertyBuildingId;
	protected $m_intNoOfPropertyFloorplanIds;
	protected $m_intTotalDiscountAmount;
	protected $m_intLeaseTermId;

	protected $m_fltRateAmount;
	protected $m_fltPercentage;
	protected $m_intArCodeId;
	protected $m_intArCodeTypeId;
	protected $m_intPropertyId;
	protected $m_intArCascadeId;
	protected $m_intArCascadeReferenceId;
	protected $m_intMaxOfferCount;

	protected $m_intUnitTypeId;
	protected $m_intPropertyFloorplanId;
	protected $m_intUnitSpaceId;

	protected $m_strSpecialNames;
	protected $m_strDeactivationDate;
	protected $m_strEffectiveThroughDate;
	protected $m_strSpecialRecipientName;
	protected $m_strSpecialTypeName;
	protected $m_strArCascadeReferenceName;
	protected $m_strSpecialStartDate;
	protected $m_strSpecialEndDate;
	protected $m_strLeaseIntervalStartDate;
	protected $m_strLeaseIntervalEndDate;

	protected $m_boolHideDescription;
	protected $m_boolRateAssociationIsPublished;

	protected $m_arrobjTierSpecials;
	protected $m_arrobjSpecialRateAssocaitions;
	protected $m_arrobjUnitTypes;
	protected $m_arrintUnitTypeIds;
	protected $m_arrmixSpecialRatesData;
	protected $m_arrstrRenewalSpecialQualifyingCriteria;
	protected $m_arrstrProspectSpecialQualifyingCriteria;

	protected $m_intIsPopupSpecial;
	protected $m_intIsInternetOnly;
	protected $m_intIsPropertyOptional;
	protected $m_intIsBrochureCoupon;
	protected $m_intSpecialGroupTypeId;
	protected $m_intApplicationStageId;
	protected $m_intApplicationStatusId;
	protected $m_intLeaseIntervalId;
	protected $m_intLeaseIntervalStatusTypeId;
	protected $m_intOfferId;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	/**
	 * Add Functions
	 */

	public function addTierSpecial( $objTierSpecial ) {
		$this->m_arrobjTierSpecials[$objTierSpecial->getId()] = $objTierSpecial;
	}

	public function addSpecialRateAssociation( $objSpecialRateAssociation ) {
		$this->m_arrobjSpecialRateAssocaitions[$objSpecialRateAssociation->getId()] = $objSpecialRateAssociation;
	}

	/**
	 * Custom getFunctions
	 */

	public function getTierSpecials() {
		return $this->m_arrobjTierSpecials;
	}

	public function getUnitTypes() {
		return $this->m_arrobjUnitTypes;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function getRateIntervalOccurances() {
		return $this->m_intRateIntervalOccurances;
	}

	public function getRateIntervalStart() {
		return $this->m_intRateIntervalStart;
	}

	public function getRateAmount() {
		return $this->m_fltRateAmount;
	}

	public function getPercentage() {
		return $this->m_fltPercentage;
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function getArCodeTypeId() {
		return $this->m_intArCodeTypeId;
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function getHideDescription() {
		return $this->m_boolHideDescription;
	}

	public function getRateAssociationIsPublished() {
		return $this->m_boolRateAssociationIsPublished;
	}

	public function getSpecialNames() {
		return $this->m_strSpecialNames;
	}

	public function getSpecialRecipientName() {
		return $this->m_strSpecialRecipientName;
	}

	public function getSpecialTypeName() {
		return $this->m_strSpecialTypeName;
	}

	public function getArCascadeReferenceName() {
		return $this->m_strArCascadeReferenceName;
	}

	public function getCompanySpecialType() {
		return $this->m_strCompanySpecialType;
	}

	public function getNoOfUnitTypes() {
		return $this->m_intNoOfUnitTypeIds;
	}

	public function getTotalDiscountAmount() {
		return $this->m_intTotalDiscountAmount;
	}

	public function getNoOfUnitSpaceIds() {
		return $this->m_intNoOfUnitSpaceIds;
	}

	public function getNoOfPropertyFloorplanIds() {
		return $this->m_intNoOfPropertyFloorplanIds;
	}

	public function getSpecialStartDate() {
		return $this->m_strSpecialStartDate;
	}

	public function getSpecialEndDate() {
		return $this->m_strSpecialEndDate;
	}

	public function getIsPopupSpecial() {
		return $this->m_intIsPopupSpecial;
	}

	public function getIsInternetOnly() {
		return $this->m_intIsInternetOnly;
	}

	public function getIsPropertyOptional() {
		return $this->m_intIsPropertyOptional;
	}

	public function getIsBrochureCoupon() {
		return $this->m_intIsBrochureCoupon;
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function getSpecialGroupTypeId() {
		return $this->m_intSpecialGroupTypeId;
	}

	public function getEffectiveThroughDate() {
		return $this->m_strEffectiveThroughDate;
	}

	public function getDeactivationDate() {
		return $this->m_strDeactivationDate;
	}

	public function getSpecialRatesData() {
		return $this->m_arrmixSpecialRatesData;
	}

	public function getLeaseIntervalId() {
		return $this->m_intLeaseIntervalId;
	}

	public function getLeaseIntervalStartDate() {
		return $this->m_strLeaseIntervalStartDate;
	}

	public function getLeaseIntervalEndDate() {
		return $this->m_strLeaseIntervalEndDate;
	}

	public function getLeaseIntervalStatusTypeId() {
		return $this->m_intLeaseIntervalStatusTypeId;
	}

	public function getOfferId() {
		return $this->m_intOfferId;
	}

	public function getRenewalSpecialQualifyingCriteria() {

		$this->m_arrstrRenewalSpecialQualifyingCriteria = [
			CSpecialsQualifyingCriterium::RENEWAL_LEASE_START_DATE => __( 'Renewal Lease Start Date' ),
			CSpecialsQualifyingCriterium::RENEWAL_OFFER_COMPLETED  => __( 'Renewal Offer Completed' )
		];

		return $this->m_arrstrRenewalSpecialQualifyingCriteria;
	}

	public function getProspectSpecialQualifyingCriteria() {

		$this->m_arrstrProspectSpecialQualifyingCriteria = [
			CSpecialsQualifyingCriterium::MOVE_IN               => __( 'Move In' ),
			CSpecialsQualifyingCriterium::APPLICATION_COMPLETED => __( 'Application Completed' )
		];

		return $this->m_arrstrProspectSpecialQualifyingCriteria;
	}

	/**
	 * @return int[]
	 */
	public function getUnitTypeIds() {
		return $this->m_arrintUnitTypeIds;
	}

	/**
	 * @return int
	 */
	public function getArCascadeId() {
		return $this->m_intArCascadeId;
	}

	/**
	 * @return int
	 */
	public function getArCascadeReferenceId() {
		return $this->m_intArCascadeReferenceId;
	}

	public function getMaxOfferCount() {
		return $this->m_intMaxOfferCount;
	}

	public function getApplicationStageId() {
		return $this->m_intApplicationStageId;
	}

	public function getApplicationStatusId() {
		return $this->m_intApplicationStatusId;
	}

	// Custom set

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->m_intPropertyFloorplanId = CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false );
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->m_intUnitSpaceId = CStrings::strToIntDef( $intUnitSpaceId, NULL, false );
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->m_intUnitTypeId = CStrings::strToIntDef( $intUnitTypeId, NULL, false );
	}

	public function setRateIntervalOccurances( $intRateIntervalOccurances ) {
		$this->m_intRateIntervalOccurances = CStrings::strToIntDef( $intRateIntervalOccurances, NULL, false );
	}

	public function setRateIntervalStart( $intRateIntervalStart ) {
		$this->m_intRateIntervalStart = CStrings::strToIntDef( $intRateIntervalStart, NULL, false );
	}

	public function setRateAmount( $fltRateAmount ) {
		$this->m_fltRateAmount = CStrings::strToFloatDef( $fltRateAmount, NULL, false, 2 );
	}

	public function setPercentage( $fltPercentage ) {
		$this->m_fltPercentage = CStrings::strToFloatDef( $fltPercentage, NULL, false, 2 );
	}

	public function setArCodeId( $intArCodeId ) {
		$this->m_intArCodeId = CStrings::strToIntDef( $intArCodeId, NULL, false );
	}

	public function setArCodeTypeId( $intArCodeTypeId ) {
		$this->m_intArCodeTypeId = CStrings::strToIntDef( $intArCodeTypeId, NULL, false );
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->m_intArTriggerId = CStrings::strToIntDef( $intArTriggerId, NULL, false );
	}

	public function setTierSpecials( $arrobjTierSpecials ) {
		$this->m_arrobjTierSpecials = $arrobjTierSpecials;
	}

	public function setUnitTypes( $arrobjUnitTypes ) {
		$this->m_arrobjUnitTypes = $arrobjUnitTypes;
	}

	public function setHideDescription( $boolHideDescription ) {
		$this->m_boolHideDescription = $boolHideDescription;
	}

	public function setRateAssociationIsPublished( $boolRateAssociationIsPublished ) {
		$this->m_boolRateAssoicationIsPublished = $boolRateAssociationIsPublished;
	}

	public function setSpecialNames( $strSpecialNames ) {
		$this->m_strSpecialNames = $strSpecialNames;
	}

	public function setSpecialRecipientName( $strSpecialRecipientName ) {
		return $this->m_strSpecialRecipientName = $strSpecialRecipientName;
	}

	public function setSpecialTypeName( $strSpecialTypeName ) {
		return $this->m_strSpecialTypeName = $strSpecialTypeName;
	}

	public function setArCascadeReferenceName( $strArCascadeReferenceName ) {
		$this->m_strArCascadeReferenceName = $strArCascadeReferenceName;
	}

	public function setCompanySpecialType( $strCompanySpecialType ) {
		$this->m_strCompanySpecialType = $strCompanySpecialType;
	}

	public function setNoOfUnitTypes( $intNoOfUnitTypeId ) {
		$this->m_intNoOfUnitTypeIds = $intNoOfUnitTypeId;
	}

	public function setTotalDiscountAmount( $intTotalDiscountAmount ) {
		return $this->m_intTotalDiscountAmount = $intTotalDiscountAmount;
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		return $this->m_intLeaseTermId = $intLeaseTermId;
	}

	public function setNoOfUnitSpaceIds( $arrintNoOfUnitSpaceIds ) {
		$this->m_intNoOfUnitSpaceIds = $arrintNoOfUnitSpaceIds;
	}

	public function setNoOfPropertyFloorplanIds( $arrintNoOfPropertyFloorplanIds ) {
		$this->m_intNoOfPropertyFloorplanIds = $arrintNoOfPropertyFloorplanIds;
	}

	public function setIsPopupSpecial( $intIsPopupSpecial ) {
		$this->m_intIsPopupSpecial = $intIsPopupSpecial;
	}

	public function setIsInternetOnly( $intIsInternetOnly ) {
		$this->m_intIsInternetOnly = $intIsInternetOnly;
	}

	public function setIsPropertyOptional( $intIsPropertyOptional ) {
		$this->m_intIsPropertyOptional = $intIsPropertyOptional;
	}

	public function setIsBrochureCoupon( $intIsBrochureCoupon ) {
		$this->m_intIsBrochureCoupon = $intIsBrochureCoupon;
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->m_intPropertyBuildingId = CStrings::strToIntDef( $intPropertyBuildingId, NULL, false );
	}

	public function setSpecialGroupTypeId( $intSpecialGroupTypeId ) {
		$this->m_intSpecialGroupTypeId = $intSpecialGroupTypeId;
	}

	public function setEffectiveThroughDate( $strEffectiveThroughDate ) {
		$this->m_strEffectiveThroughDate = $strEffectiveThroughDate;
	}

	public function setDeactivationDate( $strDeactivationDate ) {
		$this->m_strDeactivationDate = $strDeactivationDate;
	}

	public function setMaxOfferCount( $intMaxOfferCount ) {
		$this->m_intMaxOfferCount = $intMaxOfferCount;
	}

	public function setSpecialRatesData() {
		$this->m_arrmixSpecialRatesData['bool_rates_available']    = false;
		$this->m_arrmixSpecialRatesData['is_web_visible_rates']    = false;
		$this->m_arrmixSpecialRatesData['space_configuration_ids'] = [];
	}

	public function setLeaseIntervalId( $intLeaseIntervalId ) {
		$this->m_intLeaseIntervalId = $intLeaseIntervalId;
	}

	public function setLeaseIntervalStartDate( $strLeaseIntervalStartDate ) {
		$this->m_strLeaseIntervalStartDate = $strLeaseIntervalStartDate;
	}

	public function setLeaseIntervalEndDate( $strLeaseIntervalEndDate ) {
		$this->m_strLeaseIntervalEndDate = $strLeaseIntervalEndDate;
	}

	public function setLeaseIntervalStatusTypeId( $intLeaseIntervalStatusTypeId ) {
		$this->m_intLeaseIntervalStatusTypeId = $intLeaseIntervalStatusTypeId;
	}

	public function setOfferId( $intOfferId ) {
		$this->m_intOfferId = $intOfferId;
	}

	/**
	 * @param int $intArCascadeId
	 */
	public function setArCascadeId( $intArCascadeId ) {
		$this->m_intArCascadeId = $intArCascadeId;
	}

	/**
	 * @param int $intArCascadeReferenceId
	 */
	public function setArCascadeReferenceId( $intArCascadeReferenceId ) {
		$this->m_intArCascadeReferenceId = $intArCascadeReferenceId;
	}

	public function setApplicationStageId( $intApplicationStageId ) {
		$this->m_intApplicationStageId = $intApplicationStageId;
	}

	public function setApplicationStatusId( $intApplicationStatusId ) {
		$this->m_intApplicationStatusId = $intApplicationStatusId;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createScheduledCharge() {

		$intArTriggerId = NULL;

		switch( $this->getSpecialTypeId() ) {
			case CSpecialType::STANDARD:
				$intArTriggerId = CArTrigger::APPLICATION_COMPLETED;
				break;

			default:
				$intArTriggerId = CArTrigger::MONTHLY;
				break;
		}

		$objScheduledCharge = new CScheduledCharge();
		$objScheduledCharge->setCid( $this->getCid() );
		$objScheduledCharge->setArOriginReferenceId( $this->getId() );
		$objScheduledCharge->setSpecialTypeId( $this->getSpecialTypeId() );
		$objScheduledCharge->setArCodeId( $this->getArCodeId() );
		$objScheduledCharge->setArTriggerId( $intArTriggerId );
		$objScheduledCharge->setMemo( $this->getName() . ' ' . $this->getDescription() );
		$objScheduledCharge->setChargeAmount( number_format( $this->getConcessionAmount(), 2 ) );
		$objScheduledCharge->setRateIntervalOccurances( $this->getRateIntervalOccurances() );
		$objScheduledCharge->setRateIntervalStart( $this->getPostMonthStart() );

		return $objScheduledCharge;
	}

	public function createSpecialRateAssociation( $intPropertyId ) {

		$objSpecialRateAssociation = new CSpecialRateAssociation();
		$objSpecialRateAssociation->setCid( $this->m_intCid );
		$objSpecialRateAssociation->setPropertyId( $intPropertyId );
		$objSpecialRateAssociation->setArOriginId( CArOrigin::SPECIAL );
		$objSpecialRateAssociation->setArOriginReferenceId( $this->getId() );

		return $objSpecialRateAssociation;
	}

	public function createFieldValue() {

		$objFieldValue = new CFieldValue();
		$objFieldValue->setCid( $this->m_intCid );
		$objFieldValue->setFieldTypeId( CFieldType::SPECIALS );
		$objFieldValue->setReferenceId( $this->getId() );

		return $objFieldValue;

	}

	/**
	 * GET FUNCTIONS
	 */

	public function getSpecialRateAssociations( $objDatabase = NULL, $intSpecialId = NULL ) {
		if( true == is_null( $this->m_arrobjSpecialRateAssocaitions ) ) {

			if( false == valObj( $objDatabase, 'CDatabase' ) ) {
				$this->m_arrobjSpecialRateAssocaitions = [];
			} else if( false == is_null( $intSpecialId ) && 0 < $intSpecialId ) {
				$objSpecialRateAssociations = \Psi\Eos\Entrata\Custom\CSpecialRateAssociations::createService()->fetchSpecialRateAssociationsByArOriginReferenceIdByCid( $intSpecialId, $this->getCid(), $objDatabase );
				$this->setSpecialRateAssociations( $objSpecialRateAssociations );
			} else if( false == is_null( $this->getId() ) && 0 < $this->getId() ) {
				$objSpecialRateAssociations = \Psi\Eos\Entrata\Custom\CSpecialRateAssociations::createService()->fetchSpecialRateAssociationsByArOriginReferenceIdByCid( $this->getId(), $this->getCid(), $objDatabase );
				$this->setSpecialRateAssociations( $objSpecialRateAssociations );
			} else {
				$this->m_arrobjSpecialRateAssocaitions = [];
			}
		}

		return $this->m_arrobjSpecialRateAssocaitions;
	}

	/**
	 * SET FUNCTIONS
	 */

	public function setSpecialRateAssociations( $objSpecialRateAssociations ) {
		if( true == valArr( $objSpecialRateAssociations ) && false == empty( $objSpecialRateAssociations ) ) {
			$this->m_arrobjSpecialRateAssocaitions = $objSpecialRateAssociations;
		}
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrmixValues['lease_term_id'] );
		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrmixValues['unit_type_id'] );
		if( true == isset( $arrmixValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrmixValues['property_floorplan_id'] );
		if( true == isset( $arrmixValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrmixValues['unit_space_id'] );
		if( isset( $arrmixValues['property_building_id'] ) && $boolDirectSet ) $this->m_intPropertyBuildingId = trim( $arrmixValues['property_building_id'] );
		else if( isset( $arrmixValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrmixValues['property_building_id'] );
		if( true == isset( $arrmixValues['hide_description'] ) ) $this->setHideDescription( CStrings::strToBool( $arrmixValues['hide_description'] ) );
		if( true == isset( $arrmixValues['rate_association_is_published'] ) ) $this->setRateAssociationIsPublished( CStrings::strToBool( $arrmixValues['rate_association_is_published'] ) );
		if( true == isset( $arrmixValues['rate_interval_occurances'] ) ) $this->setRateIntervalOccurances( $arrmixValues['rate_interval_occurances'] );
		if( true == isset( $arrmixValues['rate_interval_start'] ) ) $this->setRateIntervalOccurances( $arrmixValues['rate_interval_start'] );
		if( true == isset( $arrmixValues['rate_amount'] ) ) $this->setRateAmount( $arrmixValues['rate_amount'] );
		if( true == isset( $arrmixValues['percentage'] ) ) $this->setPercentage( $arrmixValues['percentage'] );
		if( true == isset( $arrmixValues['ar_code_id'] ) ) $this->setArCodeId( $arrmixValues['ar_code_id'] );
		if( true == isset( $arrmixValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrmixValues['ar_code_type_id'] );
		if( true == isset( $arrmixValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrmixValues['ar_trigger_id'] );
		if( true == isset( $arrmixValues['special_group_type_id'] ) ) $this->setSpecialGroupTypeId( $arrmixValues['special_group_type_id'] );
		if( true == isset( $arrmixValues['is_popup_special'] ) ) $this->setIsPopupSpecial( $arrmixValues['is_popup_special'] );
		if( true == isset( $arrmixValues['is_internet_only'] ) ) $this->setIsInternetOnly( $arrmixValues['is_internet_only'] );
		if( true == isset( $arrmixValues['is_property_optional'] ) ) $this->setIsPropertyOptional( $arrmixValues['is_property_optional'] );
		if( true == isset( $arrmixValues['is_brochure_coupon'] ) ) $this->setIsBrochureCoupon( $arrmixValues['is_brochure_coupon'] );
		if( true == isset( $arrmixValues['max_offer_count'] ) ) $this->setMaxOfferCount( $arrmixValues['max_offer_count'] );
		if( true == isset( $arrmixValues['lease_interval_id'] ) ) $this->setLeaseIntervalId( $arrmixValues['lease_interval_id'] );
		if( true == isset( $arrmixValues['lease_interval_start_date'] ) ) $this->setLeaseIntervalStartDate( $arrmixValues['lease_interval_start_date'] );
		if( true == isset( $arrmixValues['lease_interval_end_date'] ) ) $this->setLeaseIntervalEndDate( $arrmixValues['lease_interval_end_date'] );
		if( true == isset( $arrmixValues['lease_status_type_id'] ) ) $this->setLeaseIntervalStatusTypeId( $arrmixValues['lease_status_type_id'] );
		if( true == isset( $arrmixValues['offer_id'] ) ) $this->setOfferId( $arrmixValues['offer_id'] );

		if( isset( $arrmixValues['special_names'] ) && $boolDirectSet ) {
			$this->m_strSpecialNames = trim( stripcslashes( $arrmixValues['special_names'] ) );
		} else if( isset( $arrmixValues['special_names'] ) ) {
			$this->setSpecialNames( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['special_names'] ) : $arrmixValues['special_names'] );
		}

		if( isset( $arrmixValues['special_recipient_name'] ) && $boolDirectSet ) {
			$this->m_strSpcialRecipientName = trim( stripcslashes( $arrmixValues['special_recipient_name'] ) );
		} else if( isset( $arrmixValues['special_recipient_name'] ) ) {
			$this->setSpecialRecipientName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['special_recipient_name'] ) : $arrmixValues['special_recipient_name'] );
		}

		if( isset( $arrmixValues['special_type_name'] ) && $boolDirectSet ) {
			$this->m_strSpecialTypeName = trim( stripcslashes( $arrmixValues['special_type_name'] ) );
		} else if( isset( $arrmixValues['special_type_name'] ) ) {
			$this->setSpecialTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['special_type_name'] ) : $arrmixValues['special_type_name'] );
		}

		if( isset( $arrmixValues['ar_cascade_reference_name'] ) && $boolDirectSet ) {
			$this->m_strArCascadeReferenceName = trim( stripcslashes( $arrmixValues['ar_cascade_reference_name'] ) );
		} else if( isset( $arrmixValues['ar_cascade_reference_name'] ) ) {
			$this->setArCascadeReferenceName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['ar_cascade_reference_name'] ) : $arrmixValues['ar_cascade_reference_name'] );
		}

		if( true == isset( $arrmixValues['property_floorplan_count'] ) && 0 < $arrmixValues['property_floorplan_count'] ) {
			$this->setNoOfPropertyFloorplanIds( $arrmixValues['property_floorplan_count'] );
			$this->setCompanySpecialType( 'Property Floorplan' );
		} else if( true == isset( $arrmixValues['unit_type_count'] ) && 0 < $arrmixValues['unit_type_count'] ) {
			$this->setNoOfUnitTypes( $arrmixValues['unit_type_count'] );
			$this->setCompanySpecialType( 'Unit Type' );
		} else if( true == isset( $arrmixValues['unit_space_count'] ) && 0 < $arrmixValues['unit_space_count'] ) {
			$this->setNoOfUnitSpaceIds( $arrmixValues['unit_space_count'] );
			$this->setCompanySpecialType( 'Unit' );
		} else {
			$this->setNoOfPropertyFloorplanIds( 'All' );
			$this->setNoOfUnitTypes( 'All' );
			$this->setNoOfUnitSpaceIds( 'All' );
			$this->setCompanySpecialType( 'Property' );
		}

		if( true == isset( $arrmixValues['ar_cascade_id'] ) ) {
			$this->setArCascadeId( $arrmixValues['ar_cascade_id'] );
		}

		if( true == isset( $arrmixValues['ar_cascade_reference_id'] ) ) {
			$this->setArCascadeReferenceId( $arrmixValues['ar_cascade_reference_id'] );
		}

		if( true == isset( $arrmixValues['application_stage_id'] ) ) {
			$this->setApplicationStageId( $arrmixValues['application_stage_id'] );
		}

		if( true == isset( $arrmixValues['application_status_id'] ) ) {
			$this->setApplicationStatusId( $arrmixValues['application_status_id'] );
		}

		$this->setSpecialRatesData();

		if( true == isset( $arrmixValues['all_rates_count'] ) && true == valId( $arrmixValues['all_rates_count'] ) ) {
			$this->m_arrmixSpecialRatesData['bool_rates_available'] = true;

			if( true == valId( $arrmixValues['web_visible_rates_count'] ) ) {
				$this->m_arrmixSpecialRatesData['is_web_visible_rates'] = true;
			}

			if( true == isset( $arrmixValues['space_configuration_ids'] ) && true == valStr( $arrmixValues['space_configuration_ids'] ) ) {
				$arrintSpaceConfigurationIds = explode( ',', $arrmixValues['space_configuration_ids'] );
				$arrstrWebVisibleRates       = explode( ',', $arrmixValues['web_visible_rates'] );

				foreach( $arrintSpaceConfigurationIds as $intKey => $intSpaceConfigurationId ) {
					if( true == array_key_exists( $intSpaceConfigurationId, $this->m_arrmixSpecialRatesData['space_configuration_ids'] ) ) {
						if( false == $this->m_arrmixSpecialRatesData['space_configuration_ids'][$intSpaceConfigurationId] && 't' == $arrstrWebVisibleRates[$intKey] ) {
							$this->m_arrmixSpecialRatesData['space_configuration_ids'][$intSpaceConfigurationId] = true;
						}
					} else {
						$this->m_arrmixSpecialRatesData['space_configuration_ids'][$intSpaceConfigurationId] = !( 'f' == $arrstrWebVisibleRates[$intKey] );
					}
				}
			}
		}
		return;
	}

	/**
	 * @param int[] $arrintUnitTypeIds
	 */
	public function setUnitTypeIds( $arrintUnitTypeIds ) {
		$this->m_arrintUnitTypeIds = $arrintUnitTypeIds;
	}

	/**
	 * VALIDATE FUNCTIONS
	 */

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		$objSpecialValidator = new CSpecialValidator();
		$objSpecialValidator->setSpecial( $this );
		$boolIsValid &= $objSpecialValidator->validate( $strAction, $objDatabase );
		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );

			return $this->update( $intCurrentUserId, $objDatabase, false, false );
		} else {
			return parent::delete( $intCurrentUserId, $objDatabase );
		}
	}

	public function deleteSpecial( $intArCascadeId, $intPropertyId, $intCurrentUserId, $objDatabase ) {
		if( 0 >= ( int ) $intArCascadeId || 0 >= ( int ) $intPropertyId ) return;

		/**
		 *	Comment: DELETE SPECIAL
		 *	1. Delete rates function
		 *	2. Hard delete rate associations
		 *	3. Soft delete special record
		 *	4. Hard delete field values
		 */

		// Rates Criteria

		$objRatesCriteria	= new CRatesCriteria();

		$objRatesCriteria->setCid( $this->getCid() );
		$objRatesCriteria->setPropertyId( $intPropertyId );
		$objRatesCriteria->setArCascadeId( $intArCascadeId );

		$objRatesCriteria->setArOriginId( CArOrigin::SPECIAL );
		$objRatesCriteria->setArOriginReferenceId( $this->getId() );

		if( true == valId( $this->getSpecialGroupId() ) ) {
			$objRatesCriteria->setRequireSpecialGroups( true );
			$objRatesCriteria->setSpecialGroupIds( [ $this->getSpecialGroupId() ] );
		}

		$objRatesLibrary	= new CRatesLibrary( $objRatesCriteria );

		// Rate Associations
		$arrobjSpecialRateAssociations = ( array ) \Psi\Eos\Entrata\CRateAssociations::createService()->fetchRateAssociationsByArCascadeIdByArOriginIdByArOriginReferenceIdByPropertyIdByCid( $intArCascadeId, CArOrigin::SPECIAL, $this->getId(), $intPropertyId, $this->getCid(), $objDatabase );

		// Field Values
		$arrobjFieldValues = ( array ) CFieldValues::fetchFieldValuesByReferenceIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		switch( NULL ) {
			default:
				$boolIsValid = true;

				if( false == $objRatesLibrary->deleteRates( $intCurrentUserId, $objDatabase, $boolIsFromSpecials = true ) ) {
					$boolIsValid &= false;
					break;
				}

				foreach( $arrobjSpecialRateAssociations as $objSpecialRateAssociation ) {

					if( false == $objSpecialRateAssociation->delete( $intCurrentUserId, $objDatabase ) ) {
						$boolIsValid &= false;
						break 2;
					}
				}

				foreach( $arrobjFieldValues as $objFieldValue ) {

					if( false == $objFieldValue->delete( $intCurrentUserId, $objDatabase ) ) {
						$boolIsValid &= false;
						break 2;
					}
				}

				if( false == self::delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true ) ) {
					$boolIsValid &= false;
					break;
				}
		}

		return $boolIsValid;
	}

	public function getOrFetchTierSpecials( $objDatabase ) {

		if( false == valArr( $this->m_arrobjTierSpecials ) ) {
			$arrobjTierSpecials = CSpecialTierSpecials::fetchCustomSpecialTierSpecialsByTierSpecialIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
			$this->setTierSpecials( $arrobjTierSpecials );
		}

		return $this->getTierSpecials();
	}

	public function setSpecialStartDateEndDate( $intSpecialExpirationDays, $strMoveInDate ) {

		$this->m_strSpecialStartDate 	= NULL;
		$this->m_strSpecialEndDate 		= NULL;

		$strMoveInDate = ( false == is_null( $strMoveInDate ) ) ? $strMoveInDate : date( 'm/d/Y' );

		if( false == is_null( $this->m_strStartDate ) && false == is_null( $this->m_strEndDate ) ) {

			$arrstrDateDiff 	= CDates::createService()->getDateDifference( $this->m_strStartDate, $this->m_strEndDate );
			$intDateDiffInDays 	= $arrstrDateDiff['days_total'];

			$intSpecialExpirationDaysToSubstract = $intSpecialExpirationDays - 1;

			$strSpecialStartDate 	= $this->m_strStartDate;
			$strSpecialEndDate 		= date( 'm/d/Y', strtotime( $intSpecialExpirationDaysToSubstract . ' days', strtotime( $strSpecialStartDate ) ) );

			for( $intTemp = 1; $intTemp < $intDateDiffInDays; $intTemp++ ) {

				if( strtotime( $strSpecialStartDate ) >= strtotime( $this->m_strEndDate ) || strtotime( $strSpecialEndDate ) >= strtotime( $this->m_strEndDate ) ) {
					break;
				}

				$arrstrDateDiffWithMoveInDate 	= CDates::createService()->getDateDifference( $strSpecialStartDate, $strMoveInDate );
				$intDateDiffWithMoveInDate 		= $arrstrDateDiffWithMoveInDate['days_total'];

				if( ( int ) $intSpecialExpirationDaysToSubstract >= ( int ) $intDateDiffWithMoveInDate ) {

					$this->m_strSpecialStartDate 	= $strSpecialStartDate;
					$this->m_strSpecialEndDate 		= $strSpecialEndDate;
					break;
				}

				$strSpecialStartDate = date( 'm/d/Y', strtotime( '1 day', strtotime( $strSpecialEndDate ) ) );
				$strSpecialEndDate   = date( 'm/d/Y', strtotime( $intSpecialExpirationDaysToSubstract . ' days', strtotime( $strSpecialStartDate ) ) );
			}
		}
	}

	public function validateDates( $objDatabase ) {

		if( CSpecialRecipient::PROSPECTS != $this->getSpecialRecipientId() || CSpecialType::RENEWAL_TIER != $this->getSpecialTypeId() ) {
			return true;
		}
		$arrobjSpecialGroupLeaseStartWindows = ( array ) CSpecialGroupLeaseStartWindows::fetchAllSpecialGroupLeaseStartWindowsBySpecialGroupIdByCid( $this->getSpecialGroupId(), $this->getCid(), $objDatabase );
		$intLeaseStartWindowIds = [];
		if( true === valArr( $arrobjSpecialGroupLeaseStartWindows ) ) {
			$intLeaseStartWindowIds = extractUniqueFieldValuesFromObjects( 'getLeaseStartWindowId', $arrobjSpecialGroupLeaseStartWindows );
		}

		$arrobjSpecials = ( array ) \Psi\Eos\Entrata\CSpecials::createService()->fetchProspectSpecialsByStartDateByEndDateByPropertyIdByCid( $this->getStartDate(), $this->getEndDate(), $this->getPropertyId(), $this->getCid(), $objDatabase, $this->getSpecialGroupId(), $this->getArCascadeId(), $this->getUnitTypeIds(), $intLeaseStartWindowIds );
		if( true === valArr( $arrobjSpecials ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Date of this Tier offer is conflicting with another' ) ) );
			return false;
		}

		return true;

	}

}
?>
