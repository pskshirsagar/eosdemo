<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceStatusTypes
 * Do not add any new functions to this class.
 */

class CUnitSpaceStatusTypes extends CBaseUnitSpaceStatusTypes {

	public static function fetchUnitSpaceStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CUnitSpaceStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchUnitSpaceStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CUnitSpaceStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllUnitSpaceStatusTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM unit_space_status_types ORDER BY order_num';
		return self::fetchUnitSpaceStatusTypes( $strSql, $objDatabase );
	}

	public static function fetchUnitSpaceStatusTypesByIds( $arrintIds, $objDatabase ) {
		$strSql = 'SELECT
						id,
						CASE WHEN id = ' . CUnitSpaceStatusType::VACANT_RENTED_NOT_READY . ' THEN \'Vacant Rented\'
							 WHEN id = ' . CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY . ' THEN \'Vacant Unrented\'
							 WHEN id = ' . CUnitSpaceStatusType::NOTICE_UNRENTED . ' THEN \'Units On Notice\'
							 WHEN id = ' . CUnitSpaceStatusType::OCCUPIED_NO_NOTICE . ' THEN \'Units On Notice/Lease\'
							 ELSE name
						END AS name
					FROM
						unit_space_status_types
					WHERE
						id IN ( ' . implode( ',', $arrintIds ) . ' )
					ORDER BY name DESC';
		return self::fetchUnitSpaceStatusTypes( $strSql, $objDatabase );
	}

}
?>