<?php

class CApExceptionQueueItem extends CBaseApExceptionQueueItem {

	protected $m_boolIsCcInvoice;
	protected $m_boolIsImportInvoice;
	protected $m_boolIsIntercompanyInvoice;

	protected $m_objApCode;
	protected $m_strApPayeeName;

	/**
	 * Get Functions
	 */

	public function getIsCcInvoice() {
		return $this->m_boolIsCcInvoice;
	}

	public function getIsImportInvoice() {
		return $this->m_boolIsImportInvoice;
	}

	public function getApCode() {
		return $this->m_objApCode;
	}

	public function getApPayeeName() {
		return $this->m_strApPayeeName;
	}

	public function getIsIntercompanyInvoice() {
		return $this->m_boolIsIntercompanyInvoice;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['ap_payee_name'] ) && $boolDirectSet ) {
			$this->set( 'm_strApPayeeName', trim( $arrmixValues['ap_payee_name'] ) );
		} elseif( isset( $arrmixValues['ap_payee_name'] ) ) {
			$this->setApPayeeName( $arrmixValues['ap_payee_name'] );
		}

	}

	/**
	 * Set Functions
	 */
	public function setIsCcInvoice( $boolIsCcInvoice ) {
		$this->m_boolIsCcInvoice = $boolIsCcInvoice;
	}

	public function setIsImportInvoice( $boolIsImportInvoice ) {
		$this->m_boolIsImportInvoice = $boolIsImportInvoice;
	}

	public function setApCode( $objApCode ) {
		$this->m_objApCode = $objApCode;
	}

	public function setApPayeeName( $strApPayeeName ) {
		$this->m_strApPayeeName = CStrings::strTrimDef( $strApPayeeName, 150, NULL, true );
	}

	public function setIsIntercompanyInvoice( $boolIsIntercompanyInvoice ) {
		$this->m_boolIsIntercompanyInvoice = ( bool ) $boolIsIntercompanyInvoice;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == valId( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUtilityBillId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillEscalationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillEscalateTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;

		if( false == is_object( $this->getDetails() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'details', 'Invalid json string for ap exception queue item.' ) );
		}

		return $boolIsValid;
	}

	public function valInvoice() {
		$boolIsValid = true;

		if( false == $this->getIsIntercompanyInvoice() && false == $this->getIsCcInvoice() && false == $this->getIsImportInvoice() && false == valId( $this->getOrderHeaderId() ) && false == valId( $this->getUtilityBillId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice', 'Utility bill id or vendor invoice id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valResolvedBy() {
		$boolIsValid = true;

		if( false == valId( $this->getResolvedBy() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resolved_by', 'Resolved by is required.' ) );
		}

		return $boolIsValid;
	}

	public function valResolvedOn() {
		$boolIsValid = true;

		if( false == valStr( $this->getResolvedOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resolved_on', 'Resolved on is required.' ) );
		}

		return $boolIsValid;
	}

	public function valProcessedBy() {
		$boolIsValid = true;

		if( false == valId( $this->getProcessedBy() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processed_by', 'Processed by is required.' ) );
		}

		return $boolIsValid;
	}

	public function valProcessedOn() {
		$boolIsValid = true;

		if( false == valStr( $this->getProcessedOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processed_on', 'Processed on is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCancelledBy() {
		$boolIsValid = true;

		if( false == valId( $this->getCancelledBy() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cancelled_by', 'Cancelled by is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCancelledOn() {
		$boolIsValid = true;

		if( false == valStr( $this->getCancelledOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cancelled_on', 'Cancelled on on is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCatalogItem() {
		$boolIsValid = true;

		// validate ap_code
		if( false == valObj( $this->getApCode(), 'CApCode' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code', __( 'Ap code is required.' ) ) );
		} else {
			// validate the apCatalogItem
			if( false == valArr( $this->getApCode()->getApCatalogItems() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_catalog_item', __( 'Ap catalog item is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDetails();
				$boolIsValid &= $this->valInvoice();
				break;

			case 'validate_purchse_order':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDetails();
				break;

			case 'resolve_invoice':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDetails();
				$boolIsValid &= $this->valInvoice();
				$boolIsValid &= $this->valResolvedBy();
				$boolIsValid &= $this->valResolvedOn();
				break;

			case 'process_invoice':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDetails();
				$boolIsValid &= $this->valInvoice();
				$boolIsValid &= $this->valProcessedBy();
				$boolIsValid &= $this->valProcessedOn();
				break;

			case 'cancel_invoice':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valInvoice();
				$boolIsValid &= $this->valCancelledBy();
				$boolIsValid &= $this->valCancelledOn();
				break;

			case 'validate_catalog_item':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCatalogItem();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>