<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseStartWindows
 * Do not add any new functions to this class.
 */

class CLeaseStartWindows extends CBaseLeaseStartWindows {

	public static function fetchLeaseStartWindowByPropertyIdByIdByCid( $intPropertyId, $intId, $intCid, $objDatabase, $boolSkipFlexibleLeaseTerm = true ) {

		if( false == valId( $intId ) || false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strWhereSql = ( true == $boolSkipFlexibleLeaseTerm ? ' AND COALESCE( lsw.lease_term_type_id, 0 ) <> ' . CLeaseTermType::FLEXIBLE : '' );

		$strSql = 'SELECT
						lsw.*
					FROM
						lease_start_windows lsw
					WHERE
						lsw.id = ' . ( int ) $intId . '
						AND lsw.property_id = ' . ( int ) $intPropertyId . '
						AND lsw.deleted_on IS NULL
						AND lsw.cid = ' . ( int ) $intCid . '
						' . $strWhereSql;

		return self::fetchLeaseStartWindow( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowByLeaseTermIdByPropertyIdByCid( $intLeaseTermId, $intPropertyId, $intCid, $objDatabase, $strWindowStartDate = NULL ) {

		if( false == valId( $intLeaseTermId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSqlCondition = '';
		if( true == valStr( $strWindowStartDate ) && CValidation::validateDate( $strWindowStartDate ) ) {
			$strSqlCondition .= ' AND DATE( lsw.end_date ) > DATE( \'' . $strWindowStartDate . '\' ) ';
		}

		$strSql = 'SELECT
						lsw.*,
						COALESCE( lsw.renewal_start_date, lsw.start_date ) AS renewal_start_date
					FROM
						lease_start_windows lsw
						JOIN property_charge_settings pcs ON ( pcs.cid = lsw.cid AND pcs.property_id = lsw.property_id AND pcs.lease_start_structure_id = lsw.lease_start_structure_id)
					WHERE
						lsw.cid = ' . ( int ) $intCid . '
						AND lsw.lease_term_id = ' . ( int ) $intLeaseTermId . '
						AND lsw.property_id = ' . ( int ) $intPropertyId . '
						AND lsw.deleted_on IS NULL
						AND lsw.is_active IS TRUE
						AND lsw.lease_term_type_id = ' . ( int ) CLeaseTermType::DATE_BASED . '
						' . $strSqlCondition . '
					ORDER BY lsw.start_date ASC
					LIMIT 1';

		return self::fetchLeaseStartWindow( $strSql, $objDatabase );
	}

	public static function fetchCustomLeaseStartWindowsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolSkipFlexibleLeaseTerm = true ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strWhereSql = ( true == $boolSkipFlexibleLeaseTerm ? ' AND COALESCE( lease_term_type_id, 0 ) <> ' . CLeaseTermType::FLEXIBLE : '' );

		$strSql = 'SELECT * FROM lease_start_windows WHERE property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid . ' AND deleted_on IS NULL ' . $strWhereSql;

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowsByPropertyIdByIdsByCid( $intPropertyId, $arrintIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintIds ) || false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT * FROM lease_start_windows WHERE property_id =' . ( int ) $intPropertyId . ' AND id IN( ' . implode( ',', $arrintIds ) . ') AND cid = ' . ( int ) $intCid;

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowsByLeaseIdsOrByApplicationIdsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $arrintLeaseIds = [], $arrintApplicationIds = [], $boolSkipFlexibleLeaseTerm = true ) {

		if( ( false == valArr( $arrintLeaseIds ) && false == valArr( $arrintApplicationIds ) ) || false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strWhereClause = '';

		if( true == valArr( $arrintLeaseIds ) ) {
			$strWhereClause = ' AND ca.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';
		}

		if( true == valArr( $arrintApplicationIds ) ) {
			$strWhereClause = ' AND ca.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )';
		}

		if( true == $boolSkipFlexibleLeaseTerm ) $strWhereClause .= ' AND COALESCE( lease_term_type_id, 0 ) <> ' . CLeaseTermType::FLEXIBLE;

		$strSql = '	SELECT
						ca.id,
						ca.lease_id,
						lsw.id AS lease_period_id
					FROM
						cached_applications ca
						JOIN lease_start_windows lsw ON ( lsw.cid = ca.cid AND lsw.property_id = ca.property_id AND lsw.id = ca.lease_start_window_id AND ca.created_on BETWEEN lsw.start_date AND lsw.end_date )
					WHERE
    					ca.cid = ' . ( int ) $intCid . '
					    AND ca.property_id = ' . ( int ) $intPropertyId . $strWhereClause;

		return fetchData( $strSql, $objDatabase );
	}

	public static function deleteOrphanedIntegratedLeaseStartWindowsByCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'DELETE FROM lease_start_windows AS lsw
					USING
						lease_start_windows AS s
						LEFT JOIN rates AS r ON (
							s.cid = r.cid
							AND s.id = r.lease_start_window_id
							AND r.ar_cascade_id = ' . CArCascade::SPACE . '
							AND r.ar_trigger_id = ' . CArTrigger::MONTHLY . ' )
					WHERE
						lsw.cid = s.cid
						AND lsw.id = s.id
						AND lsw.cid = ' . ( int ) $intCid . '
						AND lsw.property_id = ' . ( int ) $intPropertyId . '
						AND lsw.deleted_on IS NULL
						AND r.id IS NULL
						AND lsw.lease_term_id IS NULL;
					-- trick pg_query into not treating 0 deletions as an error.
					SELECT 1';

		return false == is_null( $objDatabase->execute( $strSql ) );

	}

	public static function fetchCustomLeaseStartWindowsByLeaseTermsFilter( $objLeaseTermsFilter, $objDatabase ) {
		/***
		 * To Do : We need to make this and fetchLeaseStartWindowsByLeaseTermsFilter  query common. Mean while if we are modifying anything then make sure to modify it there as well
		 */

		$strWhereCondition = '';
		$strJoinSql        = '';
		$strWhereSql       = ' AND lsw.occupancy_type_id = ' . ( int ) $objLeaseTermsFilter->getOccupancyTypeId() . ' AND lsw.lease_term_type_id = ' . ( int ) $objLeaseTermsFilter->getLeaseTermTypeId() . ' AND lsw.lease_term_id IN ( ' . implode( ',', $objLeaseTermsFilter->getLeaseTermIds() ) . ' )';

		if( true == valStr( $objLeaseTermsFilter->getYear() ) && 0 != $objLeaseTermsFilter->getYear() ) {
			$strWhereCondition .= ' AND to_char( lsw.start_date, \'YYYY\' ) = \'' . $objLeaseTermsFilter->getYear() . '\'';
		}

		if( true == valArr( $objLeaseTermsFilter->getPropertyIds() ) && 0 != $objLeaseTermsFilter->getPropertyIds()[0] ) {
			$strWhereCondition .= 'AND (  lsw.property_id IN ( ' . implode( ',', $objLeaseTermsFilter->getPropertyIds() ) . ' ) OR lsw.property_id IS NULL )';
		}

		if( ( true == valStr( $objLeaseTermsFilter->getYear() ) && 0 != $objLeaseTermsFilter->getYear() ) || ( true == valArr( $objLeaseTermsFilter->getPropertyIds() ) && 0 != $objLeaseTermsFilter->getPropertyIds()[0] ) ) {
			$strJoinSql = 'JOIN
					        (
					            SELECT
					                DISTINCT lsw.cid,
						            lsw.start_date,
						            lsw.end_date,
						            lsw.billing_end_date,
						            lsw.renewal_start_date,
                                    lsw.renewal_billing_start_date,
                                    lsw.lease_extension_date
					            FROM
					                lease_start_windows lsw
					            WHERE
					                lsw.cid = ' . ( int ) $objLeaseTermsFilter->getCid() . $strWhereCondition . '
					                AND lsw.deleted_by IS NULL
					                AND lsw.deleted_on IS NULL
					                ' . $strWhereSql . '
					            ORDER BY
					                lsw.start_date,
					                lsw.end_date
					        ) AS sub ON (
						                    sub.cid = lsw.cid
					                        AND sub.start_date = lsw.start_date
					                        AND sub.end_date = lsw.end_date
					                        AND sub.billing_end_date = lsw.billing_end_date
					                        AND sub.renewal_start_date = lsw.renewal_start_date
					                        AND sub.renewal_billing_start_date = lsw.renewal_billing_start_date
					                        AND ( sub.lease_extension_date IS NULL OR sub.lease_extension_date = lsw.lease_extension_date )
				                        )';
		}

		if( true == $objLeaseTermsFilter->getIsSkipDisabledProperties() ) {
			$strJoinSql .= ' JOIN properties p ON ( lsw.cid = p.cid AND lsw.property_id = p.id AND p.is_disabled = 0 )';
		}

		if( true == $objLeaseTermsFilter->getIsFetchAssociatedLeaseStartWindows() ) {
			$strSelect  = ' MAX(sub1.is_associated_lsw) as is_associated_lsw,
			               array_to_string ( array_agg ( sub1.is_associated_lsw ) , \',\' ) as is_associated_rates_leases, ';
			$strJoinSql .= ' LEFT JOIN(
								SELECT
									sub2.cid,
									sub2.lease_start_window_id,
									max ( sub2.is_associated_lsw ) AS is_associated_lsw
								FROM
								( SELECT
									DISTINCT ON ( lsw.id ) lsw.id AS lease_start_window_id,
									lsw.cid,
									CASE WHEN r.id IS NOT NULL THEN 1 ELSE 0 END AS is_associated_lsw
								FROM
									lease_start_windows lsw
									LEFT JOIN rates r ON ( lsw.cid = r.cid AND lsw.property_id = r.property_id AND lsw.id = r.lease_start_window_id AND r.rate_amount > 0 )
									WHERE
										lsw.cid =  ' . ( int ) $objLeaseTermsFilter->getCid() . '
										AND lsw.deleted_on IS NULL
										AND lsw.deleted_by IS NULL
										' . $strWhereSql . '
								UNION
								SELECT
									DISTINCT ON ( lsw.id ) lsw.id AS lease_start_window_id,
									lsw.cid,
									CASE WHEN li.id IS NOT NULL THEN 1 ELSE 0 END AS is_associated_lsw
								FROM
									lease_start_windows lsw
									LEFT JOIN lease_intervals li ON ( lsw.cid = li.cid AND lsw.property_id = li.property_id AND li.lease_start_window_id = lsw.id AND li.lease_status_type_id NOT IN (2,6) )
									LEFT JOIN applications a ON( a.cid = li.cid AND a.property_id = li.property_id AND a.lease_id = li.lease_id AND a.lease_interval_id = li.id AND a.is_deleted = FALSE AND a.application_status_id <> 6 )
									WHERE
										lsw.cid =  ' . ( int ) $objLeaseTermsFilter->getCid() . '
										AND lsw.deleted_on IS NULL
										AND lsw.deleted_by IS NULL
										' . $strWhereSql . '
										)sub2
									GROUP BY 
										sub2.lease_start_window_id,
										sub2.cid
									)sub1 ON ( sub1.cid = lsw.cid AND sub1.lease_start_window_id = lsw.id ) ';
		}

		$strSql = 'SELECT
						lsw.start_date,
						lsw.end_date,
						lsw.billing_end_date,
						lsw.renewal_start_date,
						lsw.lease_extension_date,
						' . $strSelect . '
						lsw.renewal_billing_start_date,
						lsw.min_days,
						lsw.max_days,
						lsw.lease_term_id,
						CASE WHEN ( \'t\' = lsw.show_on_website ) THEN 1 ELSE 0 END show_on_website,
					    CASE WHEN ( \'t\' = lsw.is_active ) THEN 1 ELSE 0 END is_active,
						--array_to_string ( array_agg ( lsw.property_id || \'_\' || lsw.id  ) , \',\' ) properties_with_lsw_ids,
						array_to_string ( array_agg ( lsw.id  ) , \',\' ) lsw_ids,
						array_to_string ( array_agg ( lsw.property_id  ), \',\' ) property_ids,
						COUNT( lsw.property_id  ) as properties_count
    				FROM
    					lease_start_windows lsw ' . $strJoinSql . ' 
    				WHERE
    					lsw.cid = ' . ( int ) $objLeaseTermsFilter->getCid() . '
	    				AND lsw.deleted_by IS NULL
    					AND lsw.deleted_on IS NULL
    					' . $strWhereSql . '
    				GROUP BY
    					lsw.start_date,
    					lsw.max_days,
    					lsw.end_date,
    					lsw.billing_end_date,
    					lsw.renewal_start_date,
    					lsw.renewal_billing_start_date,
    					lsw.min_days,
    					lsw.lease_term_id,
    					is_active,
    					show_on_website,
						lsw.lease_extension_date';

		if( true == valStr( $objLeaseTermsFilter->getOrderByColumn() ) ) {
			$strSortingDirection = ( true == ( valStr( $objLeaseTermsFilter->getSortingDirection() ) && ( true == in_array( $objLeaseTermsFilter->getSortingDirection(), [ 'asc', 'desc' ] ) ) ) ) ? $objLeaseTermsFilter->getSortingDirection() : 'asc';
			$strSql              .= ' ORDER BY lsw.' . $objLeaseTermsFilter->getOrderByColumn() . ' ' . $strSortingDirection;
		} else {
			$strSql .= ' ORDER BY lsw.end_date';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowsByLeaseTermsFilter( $objLeaseTermsFilter, $objDatabase ) {

		/**
		 * To Do JPA : We need to make this and fetchCustomLeaseStartWindowsByLeaseTermsFilter  query common. Mean while if we are modifying anything in this function then make sure to modify it there as well
		 */

		$strSubWhereSql = '';
		$strJoinSql = '';
		$strOrderBySql = '';

		$strMainWhereSql = ' AND lsw.occupancy_type_id = ' . ( int ) $objLeaseTermsFilter->getOccupancyTypeId() . ' AND lsw.lease_term_type_id = ' . ( int ) $objLeaseTermsFilter->getLeaseTermTypeId() . ' AND lsw.lease_term_id = ' . ( int ) $objLeaseTermsFilter->getLeaseTermId();

		if( true == valStr( $objLeaseTermsFilter->getYear() ) && 0 != $objLeaseTermsFilter->getYear() ) {
			$strSubWhereSql .= ' AND to_char( start_date, \'YYYY\' ) = \'' . $objLeaseTermsFilter->getYear() . '\'';
		}

		if( true == valArr( $objLeaseTermsFilter->getPropertyIds() ) ) {
			$strSubWhereSql .= 'AND (  lsw.property_id IN ( ' . implode( ',', $objLeaseTermsFilter->getPropertyIds() ) . ' ) OR lsw.property_id IS NULL )';
		}

		if( true == valId( $objLeaseTermsFilter->getPropertyId() ) ) {
			$strMainWhereSql .= ' AND lsw.property_id = ' . ( int ) $objLeaseTermsFilter->getPropertyId();
		}

		if( true == valStr( $objLeaseTermsFilter->getStartDate() ) ) {
			$strMainWhereSql .= ' AND lsw.start_date = \'' . $objLeaseTermsFilter->getStartDate() . '\'::DATE ';
		}

		if( true == valStr( $objLeaseTermsFilter->getEndDate() ) ) {
			$strMainWhereSql .= ' AND lsw.end_date = \'' . $objLeaseTermsFilter->getEndDate() . '\'::DATE ';
		}

		if( true == valStr( $objLeaseTermsFilter->getBillingEndDate() ) ) {
			$strMainWhereSql .= ' AND lsw.billing_end_date = \'' . $objLeaseTermsFilter->getBillingEndDate() . '\'::DATE ';
		}

		if( true == valStr( $objLeaseTermsFilter->getRenewalStartDate() ) ) {
			$strMainWhereSql .= ' AND lsw.renewal_start_date = \'' . $objLeaseTermsFilter->getRenewalStartDate() . '\'::DATE ';
		}

		if( true == valStr( $objLeaseTermsFilter->getRenewalBillingStartDate() ) ) {
			$strMainWhereSql .= ' AND lsw.renewal_billing_start_date = \'' . $objLeaseTermsFilter->getRenewalBillingStartDate() . '\'::DATE ';
		}

		if( true == valArr( $objLeaseTermsFilter->getLeaseTermIds() ) ) {
			$strMainWhereSql .= ' AND lsw.lease_term_id IN ( ' . implode( ',', $objLeaseTermsFilter->getLeaseTermIds() ) . ' ) ';
		}

		if( true == $objLeaseTermsFilter->getIsDeleted() ) {
			$strMainWhereSql .= ' AND lsw.deleted_by IS NOT NULL ';
			$strOrderBySql   .= ' ORDER BY lsw.deleted_on DESC LIMIT 1';
		} else {
			$strMainWhereSql .= ' AND lsw.deleted_by IS NULL AND lsw.deleted_on IS NULL';
		}

		if( ( true == valStr( $objLeaseTermsFilter->getYear() ) && 0 != $objLeaseTermsFilter->getYear() ) || ( true == valArr( $objLeaseTermsFilter->getPropertyIds() ) && 0 != $objLeaseTermsFilter->getPropertyIds()[0] ) ) {
		$strJoinSql = ' JOIN
				        (
				            SELECT
				                DISTINCT lsw.*
				            FROM
				              lease_start_windows lsw
				            WHERE
					            lsw.cid = ' . ( int ) $objLeaseTermsFilter->getCid() . '
					            AND lsw.deleted_by IS NULL
					            AND lsw.deleted_on IS NULL
					            ' . $strMainWhereSql . '
					            ' . $strSubWhereSql . '
				        ) AS sub ON ( sub.start_date = lsw.start_date AND sub.end_date = lsw.end_date AND sub.cid = lsw.cid AND sub.lease_term_id = lsw.lease_term_id 
				                            AND sub.billing_end_date = lsw.billing_end_date
					                        AND sub.renewal_start_date = lsw.renewal_start_date
					                        AND sub.renewal_billing_start_date = lsw.renewal_billing_start_date
					                        AND ( sub.lease_extension_date IS NULL OR sub.lease_extension_date = lsw.lease_extension_date ) )';

		}

		if( true == $objLeaseTermsFilter->getIsSkipDisabledProperties() ) {
			$strJoinSql .= ' JOIN properties p ON ( lsw.cid = p.cid AND lsw.property_id = p.id AND p.is_disabled = 0 )';
		}

		$strSql = 'SELECT
						lsw.*
    				FROM
    					lease_start_windows lsw ' . $strJoinSql . '
    				WHERE
    					lsw.cid = ' . ( int ) $objLeaseTermsFilter->getCid()
		          . $strMainWhereSql
		          . $strOrderBySql;

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchActiveLeaseStartWindowsByLeaseStartStructureIdByCid( $intLeaseStartStructureId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intLeaseStartStructureId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT * FROM lease_start_windows WHERE lease_start_structure_id = ' . ( int ) $intLeaseStartStructureId . ' AND cid = ' . ( int ) $intCid . ' AND deleted_on IS NULL ORDER BY offset_start_days ASC ';

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowsByLeaseStartStructureIdByCid( $intLeaseStartStructureId, $intCid, $objDatabase, $boolSkipFlexibleLeaseTerm = true ) {
		if( false === is_numeric( $intLeaseStartStructureId ) || false === is_numeric( $intCid ) ) return NULL;

		$strWhereSql = ( true == $boolSkipFlexibleLeaseTerm ? ' AND COALESCE( lsw.lease_term_type_id, 0 ) <> ' . CLeaseTermType::FLEXIBLE : '' );

		$strSql = 'SELECT
						lsw.*
					FROM
						lease_start_windows lsw
					WHERE
						lsw.cid = ' . ( int ) $intCid . '
						AND lsw.lease_start_structure_id = ' . ( int ) $intLeaseStartStructureId . '
						AND lsw.deleted_on IS NULL
						' . $strWhereSql . '
					ORDER BY
						lsw.offset_start_days ASC,
						lsw.offset_end_days ASC';

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchAssociatedLeaseStartWindowIdsByIdByPropertyIdByCid( $intId, $intPropertyId, $intCid, $objDatabase ) {

		$objLeaseStartWindow = self::fetchLeaseStartWindowsByPropertyIdByIdByCid( $intPropertyId, $intId, $intCid, $objDatabase );

		if( false == valObj( $objLeaseStartWindow, 'CLeaseStartWindow' ) ) return NULL;

		$strSql = 'SELECT
						lsw.id
					FROM
						lease_start_windows lsw
					WHERE
						lsw.end_date > CURRENT_DATE
						AND lsw.deleted_on IS NULL
						AND ( lsw.start_date BETWEEN \'' . $objLeaseStartWindow->getStartDate() . '\'	AND \'' . $objLeaseStartWindow->getEndDate() . '\'
						OR lsw.start_date <= \'' . $objLeaseStartWindow->getStartDate() . '\'
						AND lsw.end_date >= \'' . $objLeaseStartWindow->getStartDate() . '\' )
						AND lsw.cid = ' . ( int ) $intCid . '
						AND lsw.property_id = ' . ( int ) $intPropertyId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowsByPropertyIdByIdByCid( $intPropertyId, $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						lsw.*
					FROM
						lease_start_windows lsw
					WHERE
						lsw.id = ' . ( int ) $intId . '
						AND lsw.property_id = ' . ( int ) $intPropertyId . '
						AND lsw.deleted_on IS NULL
						AND lsw.cid = ' . ( int ) $intCid;

		return self::fetchLeaseStartWindow( $strSql, $objDatabase );
	}

	public static function fetchBillingEndDateByLeaseTermIdByPropertyIdByCid( $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
					    billing_end_date
					FROM
					    lease_start_windows lsw
					WHERE
						lsw.cid = ' . ( int ) $intCid . '
					    AND lsw.id = ' . ( int ) $intLeaseStartWindowId . '
					    AND lsw.property_id = ' . ( int ) $intPropertyId;

		return parent::fetchColumn( $strSql, 'billing_end_date', $objDatabase );
	}

	public static function fetchLeaseStartWindowsByLeaseTermIdsByCid( $arrintLeaseTermIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						lsw.*
					FROM
						lease_start_windows lsw
					WHERE
						lsw.lease_term_id IN ( ' . implode( ',', $arrintLeaseTermIds ) . ' )
						AND lsw.deleted_on IS NULL
						AND lsw.deleted_by IS NULL
						AND lsw.cid = ' . ( int ) $intCid;

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchPublishedRenewalLeaseStartWindowsByPropertyIdsByCId( $arrintPropertyId, $intCid, $objDatabase, $boolSkipFlexibleLeaseTerm = true ) {

		if( false == valArr( $arrintPropertyId ) ) return NULL;

		$strWhereSql = ( true == $boolSkipFlexibleLeaseTerm ? ' AND COALESCE( lsw.lease_term_type_id, 0 ) <> ' . CLeaseTermType::FLEXIBLE : '' );

		$strSql = 'SELECT
						lsw.*
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
						JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id AND pcs.lease_start_structure_id = lsw.lease_start_structure_id )
					WHERE
						lt.deleted_on IS NULL
						AND lt.term_month <> 0
						AND lt.is_renewal IS TRUE
						AND pcs.property_id IN ( ' . implode( ',', $arrintPropertyId ) . ' )
						AND lt.cid = ' . ( int ) $intCid . $strWhereSql . '
						ORDER BY lt.term_month';

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchRenewalDateBasedActiveLeaseStartWindowsByPropertyIdByCId( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						lsw.*, 
						util_get_translated( \'name\', lt.name, lt.details ) || \' ( \' ||  lsw.start_date || \' - \' || lsw.end_date || \') \' as lease_term_name
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
						JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id AND pcs.lease_start_structure_id = lsw.lease_start_structure_id AND lsw.property_id = pcs.property_id )
					WHERE
						lt.deleted_on IS NULL
						AND lt.term_month <> 0
						AND lt.is_renewal IS TRUE
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lsw.is_active = true
						AND lsw.lease_term_type_id = ' . ( int ) CLeaseTermType::DATE_BASED . '
						AND lsw.deleted_by IS NULL
						AND lsw.deleted_on IS NULL
						AND lt.cid = ' . ( int ) $intCid . ' ORDER BY lt.term_month';

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchDateBasedActiveLeaseStartWindowsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $arrintLeaseTermIds = [] ) {

		$strWhereSql = ( true == valArr( $arrintLeaseTermIds ) ) ? ' AND lsw.lease_term_id IN ( ' . implode( ',', $arrintLeaseTermIds ) . ' )' : '';

		$strSql = 'SELECT
						lsw.id,
						lsw.lease_term_id
					FROM
						lease_start_windows lsw
					WHERE
						lsw.property_id = ' . ( int ) $intPropertyId . '
						AND lsw.cid = ' . ( int ) $intCid . '
						AND lsw.deleted_on IS NULL
						AND lsw.deleted_by IS NULL
						AND lsw.is_active = true
						AND lsw.lease_term_type_id = ' . ( int ) CLeaseTermType::DATE_BASED . '
						AND lsw.end_date >= NOW()::DATE' . $strWhereSql;

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchDateBasedActiveLeaseStartWindowsByPropertyIdsByCid( $arrintPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyId ) ) return NULL;

        $strSql = 'SELECT
						lsw.id,
						lsw.lease_term_id,
						lsw.property_id as property_id
					FROM
						lease_start_windows lsw
					WHERE
						lsw.property_id in ( ' . implode( ',', $arrintPropertyId ) . ' )
						AND lsw.cid = ' . ( int ) $intCid . '
						AND lsw.deleted_on IS NULL
						AND lsw.deleted_by IS NULL
						AND lsw.is_active = true
						AND lsw.lease_term_type_id = ' . ( int ) CLeaseTermType::DATE_BASED . ' ';

		$arrobjDateBasedActiveLeaseStartWindows = self::fetchLeaseStartWindows( $strSql, $objDatabase );

        if( false == valArr( $arrobjDateBasedActiveLeaseStartWindows ) ) return NULL;

        $arrobjDateBasedActiveLeaseStartWindowsByPropertyId = [];

        foreach( $arrobjDateBasedActiveLeaseStartWindows as $intLeaseStartWindowIds => $arrobjDateBasedActiveLeaseStartWindow ) {
			$arrobjDateBasedActiveLeaseStartWindowsByPropertyId[$arrobjDateBasedActiveLeaseStartWindow->getPropertyId()]['LeaseTermIds'][] = $arrobjDateBasedActiveLeaseStartWindow->getLeaseTermId();
			$arrobjDateBasedActiveLeaseStartWindowsByPropertyId[$arrobjDateBasedActiveLeaseStartWindow->getPropertyId()]['LeaseStartWindowIds'][] = $intLeaseStartWindowIds;
		}

		return $arrobjDateBasedActiveLeaseStartWindowsByPropertyId;
	}

	public static function fetchDateBasedActiveLeaseStartWindowsByLeaseTermIdByPropertyIdByCid( $intLeaseTermId, $intPropertyId, $intCid, $objDatabase, $boolIsFetchCustomArray = false, $intId = NULL ) {

		if( false == valId( $intLeaseTermId ) ) return false;

		$strSelectSql = ( true == $boolIsFetchCustomArray ) ? ' lsw.id, lsw.start_date, lsw.end_date, lsw.renewal_billing_start_date, lsw.billing_end_date ' : ' lsw.*';
		$strWhere     = ( true == valId( $intId ) ) ? ' AND lsw.id = ' . ( int ) $intId : '';

		$strSql = 'SELECT
						' . $strSelectSql . '
					FROM
						lease_start_windows lsw
						JOIN property_charge_settings pcs ON ( pcs.cid = lsw.cid AND pcs.property_id = lsw.property_id AND pcs.lease_start_structure_id = lsw.lease_start_structure_id )
					WHERE
						lsw.property_id = ' . ( int ) $intPropertyId . '
						AND lsw.cid = ' . ( int ) $intCid . '
						AND lsw.lease_term_id = ' . ( int ) $intLeaseTermId . '
						AND lsw.is_active = TRUE
						AND lsw.end_date >= NOW()::DATE
						AND lsw.deleted_on IS NULL
						' . $strWhere . '
					ORDER BY
						lsw.start_date,
						lsw.end_date';

		if( true == $boolIsFetchCustomArray ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowsByLeaseTermIdByPropertyIdByCid( $intLeaseTermId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseTermId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						lsw.*
					FROM
						lease_start_windows lsw
						JOIN property_charge_settings pcs ON ( pcs.cid = lsw.cid AND pcs.property_id = lsw.property_id AND pcs.lease_start_structure_id = lsw.lease_start_structure_id )
					WHERE
						lsw.cid = ' . ( int ) $intCid . '
						AND lsw.lease_term_id = ' . ( int ) $intLeaseTermId . '
						AND lsw.property_id = ' . ( int ) $intPropertyId . '
						AND lsw.deleted_on IS NULL
						AND lsw.is_active IS TRUE
						AND lsw.lease_term_type_id = ' . ( int ) CLeaseTermType::DATE_BASED . '
						ORDER BY
						lsw.start_date ASC;';

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchAssociatedPropertyIdsByLeaseTermIdByCId( $intLeaseTermId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
			         DISTINCT property_id
			     FROM
			         lease_start_windows lsw
			     WHERE
					lsw.cid = ' . ( int ) $intCid . '
					AND lsw.lease_term_id = ' . ( int ) $intLeaseTermId . '
					AND lsw.deleted_on IS NULL
					AND lsw.deleted_by IS NULL
					AND lsw.lease_term_type_id = ' . ( int ) CLeaseTermType::DATE_BASED;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDuplicateLeaseStartWindowsCountByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
                        sub1.duplicate_windows_count
		             FROM
		                 (
		                   SELECT
		                        count ( lsw.id ) AS duplicate_windows_count
		                   FROM
		                       lease_start_windows lsw
		                   WHERE
		                        lsw.lease_term_id = ' . ( int ) $intLeaseTermId . '
		                        AND lsw.cid = ' . ( int ) $intCid . '
					            AND lsw.deleted_by IS NULL
		                   GROUP BY
		                       lsw.cid,
		                       lsw.lease_term_id,
		                       lsw.property_id,
		                       lsw.start_date,
		                       lsw.end_date,
		                       lsw.renewal_start_date
		                 ) AS sub1
		             WHERE
		                 duplicate_windows_count > 1';

		$arrmixResponse = fetchData( $strSql, $objDatabase );
		return $arrmixResponse;
	}

	public static function fetchOverlapExistingLeaseStartWindowsCountByOccupancyTypeIdByLeaseTermIdByCid( $intCid, $objDatabase, $arrmixLeaseStartWindow, $intOccupancyTypeId, $intLeaseTermTypeId ) {

		$strStartDate = $arrmixLeaseStartWindow['start_date'];
		$strEndDate = $arrmixLeaseStartWindow['end_date'];
		$arrintPropertyIds = json_decode( $arrmixLeaseStartWindow['property_ids'] );
		$strPropertyIds = implode( ',', ( array ) $arrintPropertyIds );

			$strSql = 'SELECT
						COUNT( lsw.id ) as cnt
					FROM
						PUBLIC.lease_start_windows lsw
					INNER JOIN PUBLIC.lease_terms lt 
					ON lt.id = lsw.lease_term_id 
					AND lt.lease_term_type_id = ' . ( int ) $intLeaseTermTypeId . '
					AND lt.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
					AND lt.cid = lsw.cid 
					WHERE
						lsw.cid =' . ( int ) $intCid . ' 
						AND lt.deleted_on IS NULL 
						AND lt.deleted_by IS NULL
						AND lsw.deleted_on IS NULL 
						AND lsw.deleted_by IS NULL
						AND  lsw.property_id IN ( ' . $strPropertyIds . ' )
						AND ( date( \'' . $strStartDate . '\' ), date( \'' . $strEndDate . '\' ) ) OVERLAPS ( lsw.start_date, lsw.end_date)
						GROUP BY lsw.property_id
						HAVING COUNT( lsw.id )>1';

			$arrmixResponse = fetchData( $strSql, $objDatabase );
			return $arrmixResponse;
	}

	public static function fetchLeaseStartWindowsByLeaseIdsByPropertyIdByCid( $arrintLeaseIds, $intPropertyId, $intCid, $objDatabase, $arrintLeaseIntervalIds = [] ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valArr( $arrintLeaseIds ) ) return NULL;

		$strWhereCondition = ' AND li.id = l.active_lease_interval_id';
		if( true == valArr( $arrintLeaseIntervalIds ) ) {
			$strWhereCondition = ' AND li.id IN (' . implode( ', ', $arrintLeaseIntervalIds ) . ')';
		}

		$strSql = 'SELECT lsw.id,
					      sub.property_unit_id
					FROM lease_terms lt
					     JOIN lease_start_windows lsw ON (lsw.cid = lt.cid AND lsw.lease_term_id = lt.id)
					     JOIN property_charge_settings pcs ON (pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id)
					     JOIN 
					     (
					       SELECT DISTINCT lsw.id,
					              lsw.start_date,
					              lsw.end_date,
					              l.property_unit_id
					       FROM lease_start_windows lsw
					            JOIN lease_intervals li ON (li.cid = lsw.cid AND li.property_id = lsw.property_id AND li.lease_start_window_id = lsw.id AND li.lease_status_type_id NOT IN (' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . '))
					            JOIN leases l ON ( li.cid = l.cid AND li.lease_id = l.id ' . $strWhereCondition . ' )
					       WHERE lsw.cid = ' . ( int ) $intCid . ' AND
					             lsw.property_id = ' . ( int ) $intPropertyId . ' AND
					             lsw.end_date >= CURRENT_DATE AND
					             li.lease_id IN (' . implode( ', ', $arrintLeaseIds ) . ')
					     ) AS sub on ( lsw.start_date BETWEEN sub.start_date AND sub.end_date OR lsw.start_date <= sub.start_date AND lsw.end_date >= sub.start_date )
					WHERE lsw.cid = ' . ( int ) $intCid . ' AND
					      lsw.property_id = ' . ( int ) $intPropertyId . ' AND
					      pcs.property_id = ' . ( int ) $intPropertyId . ' AND
					      lsw.end_date >= CURRENT_DATE AND
					      lt.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowsByPropertyIdByCidByOptionalFilters( $intPropertyId, $intCid, $objDatabase, $intId = NULL, $boolIgnoreDates = false, $boolSkipFlexibleLeaseTerm = true ) {

		$strCondition = NULL;
		if( true == valId( $intId ) ) {
			$strCondition = ' AND lsw.id = ' . ( int ) $intId;
		}

		if( true == $boolSkipFlexibleLeaseTerm ) {
			$strCondition .= ' AND COALESCE( lsw.lease_term_type_id, 0 ) <> ' . CLeaseTermType::FLEXIBLE;
		}

		$strDateCondition = NULL;
		if( false == $boolIgnoreDates ) {
			$strDateCondition = ' AND ( lsw.end_date IS NULL OR lsw.end_date >= NOW()::DATE )';
		}

		$strSql = 'SELECT
						lsw.*
					FROM
						lease_start_windows lsw
						JOIN property_charge_settings pcs ON pcs.cid = lsw.cid AND lsw.lease_start_structure_id IN( pcs.lease_start_structure_id, ' . CLeaseStartStructure::HIDDEN_SYSTEM_STUDENT_HOUSING_CATCH_ALL_STRUCTURE . ' ) AND pcs.property_id = ' . ( int ) $intPropertyId . '
					WHERE
						lsw.cid = ' . ( int ) $intCid . $strDateCondition . '
						AND lsw.deleted_on IS NULL
						AND lsw.is_active = TRUE
						AND COALESCE( lsw.property_id, ' . ( int ) $intPropertyId . ' ) = ' . ( int ) $intPropertyId . '
						' . $strCondition . '
					ORDER BY
						lsw.lease_term_type_id,
						lsw.id';

		return  self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowsByLeaseIdByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase, $intLeaseIntervalId ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intLeaseId ) ) return NULL;

		$strWhereCondition = ' AND li.id = l.active_lease_interval_id';
		if( true == valId( $intLeaseIntervalId ) ) {
			$strWhereCondition = ' AND li.id =' . ( int ) $intLeaseIntervalId;
		}

		$strSql = 'SELECT lsw.id,
				      sub.property_unit_id
				FROM lease_terms lt
				     JOIN lease_start_windows lsw ON (lsw.cid = lt.cid AND lsw.lease_term_id = lt.id)
				     JOIN property_charge_settings pcs ON (pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id)
				     JOIN
				     (
				       SELECT DISTINCT lsw.id,
				              lsw.start_date,
				              lsw.end_date,
				              l.property_unit_id
				       FROM lease_start_windows lsw
				            JOIN lease_intervals li ON (li.cid = lsw.cid AND li.property_id = lsw.property_id AND li.lease_start_window_id = lsw.id AND li.lease_status_type_id NOT IN (' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . '))
				            JOIN leases l ON ( li.cid = l.cid AND li.lease_id = l.id ' . $strWhereCondition . ' )
				       WHERE lsw.cid = ' . ( int ) $intCid . ' AND
				             lsw.property_id = ' . ( int ) $intPropertyId . ' AND
				             lsw.end_date >= CURRENT_DATE AND
				             li.lease_id = ' . ( int ) $intLeaseId . '
				     ) AS sub on ( lsw.start_date BETWEEN sub.start_date AND sub.end_date OR lsw.start_date <= sub.start_date AND lsw.end_date >= sub.start_date )
				WHERE lsw.cid = ' . ( int ) $intCid . ' AND
				      lsw.property_id = ' . ( int ) $intPropertyId . ' AND
				      pcs.property_id = ' . ( int ) $intPropertyId . ' AND
				      lsw.end_date >= CURRENT_DATE AND
				      lt.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowsByLeaseStartStructureIdByCidByStartEndDays( $intLeaseStartStructureId, $intCid, $intStartDate, $intEndDate, $objDatabase, $boolSkipFlexibleLeaseTerm = true ) {
		if( false === is_numeric( $intLeaseStartStructureId ) || false === is_numeric( $intCid ) ) {
			return NULL;
		}

		$strWhereSql = ( true == $boolSkipFlexibleLeaseTerm ? ' AND COALESCE( lsw.lease_term_type_id, 0 ) <> ' . CLeaseTermType::FLEXIBLE : '' );

		$strSql = 'SELECT
						lsw.id
					FROM
						lease_start_windows lsw
					WHERE
						lsw.cid = ' . ( int ) $intCid . '
						AND lsw.lease_start_structure_id = ' . ( int ) $intLeaseStartStructureId . '
					    AND lsw.offset_start_days = ' . ( int ) $intStartDate . '
					    AND lsw.offset_end_days = ' . ( int ) $intEndDate . $strWhereSql . '
						ORDER BY id DESC';
		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowsByLeaseIdsByPropertyIdsByCid( $arrintLeaseIds, $arrintPropertyIds, $intCid, $objDatabase, $arrintLeaseIntervalIds = [] ) {

		if( false == valId( $intCid ) || false == valArr( $arrintPropertyIds ) || false == valArr( $arrintLeaseIds ) ) return NULL;

		$strWhereCondition = ' AND li.id = l.active_lease_interval_id';
		if( true == valArr( $arrintLeaseIntervalIds ) ) {
			$strWhereCondition = ' AND li.id IN (' . implode( ', ', $arrintLeaseIntervalIds ) . ')';
		}

		$strSql = 'SELECT lsw.id,
					      sub.property_unit_id
					FROM lease_terms lt
					     JOIN lease_start_windows lsw ON (lsw.cid = lt.cid AND lsw.lease_term_id = lt.id)
					     JOIN property_charge_settings pcs ON (pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id)
					     JOIN
					     (
					       SELECT DISTINCT lsw.id,
					              lsw.start_date,
					              lsw.end_date,
					              l.property_unit_id
					       FROM lease_start_windows lsw
					            JOIN lease_intervals li ON (li.cid = lsw.cid AND li.property_id = lsw.property_id AND li.lease_start_window_id = lsw.id AND li.lease_status_type_id NOT IN (' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . '))
					            JOIN leases l ON ( li.cid = l.cid AND li.lease_id = l.id ' . $strWhereCondition . ' )
					       WHERE lsw.cid = ' . ( int ) $intCid . ' AND
					             lsw.property_id IN (' . implode( ', ', $arrintPropertyIds ) . ') AND
					             lsw.end_date >= CURRENT_DATE AND
					             li.lease_id IN (' . implode( ', ', $arrintLeaseIds ) . ')
					     ) AS sub on ( lsw.start_date BETWEEN sub.start_date AND sub.end_date OR lsw.start_date <= sub.start_date AND lsw.end_date >= sub.start_date )
					WHERE lsw.cid = ' . ( int ) $intCid . ' AND
					      lsw.property_id IN (' . implode( ', ', $arrintPropertyIds ) . ') AND
					      pcs.property_id IN (' . implode( ', ', $arrintPropertyIds ) . ') AND
					      lsw.end_date >= CURRENT_DATE AND
					      lt.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRenewalAndProspectDateBasedActiveLeaseStartWindowsByPropertyIdByCId( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						lsw.*,
						util_get_translated( \'name\', lt.name, lt.details ) || \' (\' || lsw.start_date || \' - \' || lsw.end_date || \') \' as lease_term_name,
						lt.is_renewal,
						lt.is_prospect,
						lsw.start_date,
						lsw.end_date
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
						JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id AND pcs.lease_start_structure_id = lsw.lease_start_structure_id AND lsw.property_id = pcs.property_id )
					WHERE
						lt.deleted_on IS NULL
						AND lt.term_month <> 0
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lsw.is_active = true
						AND lsw.lease_term_type_id = ' . ( int ) CLeaseTermType::DATE_BASED . '
						AND lsw.deleted_by IS NULL
						AND lsw.deleted_on IS NULL
						AND lt.cid = ' . ( int ) $intCid . ' 
						AND ( lt.is_renewal IS TRUE 
							OR lt.is_prospect IS TRUE )
						AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL )
						ORDER BY lt.term_month';

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolSkipFlexibleLeaseTerm = true ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strWhereSql = ( true == $boolSkipFlexibleLeaseTerm ? ' AND COALESCE( lsw.lease_term_type_id, 0 ) <> ' . CLeaseTermType::FLEXIBLE : '' );

		$strSql = ' SELECT
						lsw.*
					FROM
						lease_start_windows lsw
						JOIN property_charge_settings pcs ON pcs.cid = lsw.cid AND lsw.lease_start_structure_id IN( pcs.lease_start_structure_id, ' . CLeaseStartStructure::HIDDEN_SYSTEM_STUDENT_HOUSING_CATCH_ALL_STRUCTURE . ' ) 
					WHERE
						lsw.cid = ' . ( int ) $intCid . '
						AND ( lsw.end_date IS NULL OR lsw.end_date >= NOW()::DATE )
						AND lsw.deleted_on IS NULL
						AND lsw.is_active = TRUE
						AND lsw.property_id IN (' . implode( ', ', $arrintPropertyIds ) . ')
						' . $strWhereSql . '
					ORDER BY
						lsw.lease_term_type_id,
						lsw.id';

		return  self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowsHavingLeaseExtensionDateRelatedBadDataByCid( $intCid, $objDatabase ) {
		$strSql = ' SELECT
							lsw.*
						FROM
							lease_start_windows lsw 
						WHERE
							lsw.cid = ' . ( int ) $intCid . '
							AND lsw.lease_extension_date IS NOT NULL
							AND lsw.lease_extension_date <= lsw.end_date 
							AND lsw.deleted_on IS NULL';
		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
            lt.name || \' (\' || to_char(lsw.start_date, \'MM/DD/YYYY\') || \' - \' || to_char(lsw.end_date, \'MM/DD/YYYY\') || \')\'  AS destination_lease_term_name,
            lsw.id,
            lsw.lease_term_id,
            lt.name AS lease_term_name,
            lsw.start_date,
            lsw.end_date
          FROM 
            lease_terms lt
            JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id )
          WHERE 
            lsw.cid= ' . ( int ) $intCid . '
            AND lsw.property_id= ' . ( int ) $intPropertyId . '
            AND lt.deleted_on IS NULL
            AND lsw.deleted_on IS NULL order by ' . $objDatabase->getCollateSort( 'name' );

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchCommercialLeaseStartWindowByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {

		if( false === valId( $intLeaseIntervalId ) ) return NULL;

		$strSql = ' SELECT
						lsw.*
					FROM
						lease_start_windows lsw
						JOIN lease_intervals li ON ( li.cid = lsw.cid AND li.property_id = lsw.property_id AND li.lease_start_window_id = lsw.id AND li.lease_term_id = lsw.lease_term_id )
					WHERE
						li.id = ' . ( int ) $intLeaseIntervalId . '
						AND lsw.cid = ' . ( int ) $intCid . ' 
						AND lsw.deleted_on IS NULL';

		return self::fetchLeaseStartWindow( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {

		if( false === valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
			lsw.*,
            lt.name || \' ( \' ||  lsw.start_date || \' - \' || lsw.end_date || \') \' as lease_term_name
          FROM 
            lease_terms lt
            JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id )
          WHERE 
            lsw.id IN (' . implode( ', ', $arrintIds ) . ')
            AND lsw.cid= ' . ( int ) $intCid . '
            AND lt.deleted_on IS NULL
            AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL )
            AND lsw.deleted_on IS NULL order by name';

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchActiveDateBasedLeaseStartWindowsByPropertyId( $intPropertyId, $intCid, $objDatabase, $boolIsReturnSqlOnly = false ) {

		$strSql = sprintf( ' SELECT
			            lsw.id,
			            lsw.lease_term_id,
			            lt.name AS lease_term_name,
			            lsw.start_date,
			            lsw.end_date,
			            lt.term_month as lease_term_months
			          FROM 
			            lease_terms lt
			            JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id )
			          WHERE 
			            lsw.cid= %d
			            AND lsw.property_id= %d
			            AND lt.deleted_on IS NULL
			            AND lsw.deleted_on IS NULL
			            AND lt.is_disabled IS FALSE 
			            AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' 
			            AND lsw.end_date >= CURRENT_DATE 
			            AND lsw.is_active IS TRUE 
			            AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL . ' 
			            AND ( COALESCE( lt.default_lease_term_id, 0 ) <> 201 OR lt.default_lease_term_id IS NULL ) 
			            AND lt.is_prospect IS TRUE 
			            AND lt.show_on_website IS TRUE 
			          ORDER BY lt.term_month DESC', $intCid, $intPropertyId );
		if( true == $boolIsReturnSqlOnly ) {
			return $strSql;
		}

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchActiveFlexibleLeaseStartWindowsByPropertyId( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) {
			return;
		}

		$strSql = sprintf( ' SELECT
			           lsw.*
			          FROM 
			            lease_terms lt
			            JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id )
			          WHERE 
			            lsw.cid= %d
			            AND lsw.property_id= %d
			            AND lsw.occupancy_type_id=' . COccupancyType::HOSPITALITY . '
			            AND lt.lease_term_type_id = ' . CLeaseTermType::FLEXIBLE . ' 
			            AND lsw.end_date >= CURRENT_DATE 
			            AND lt.deleted_on IS NULL
			            AND lsw.deleted_on IS NULL
			            AND lt.is_disabled IS FALSE 
			            AND lsw.is_active IS TRUE 
			            AND lt.is_prospect IS TRUE 
			            AND lt.show_on_website IS TRUE 
			          ORDER BY lsw.start_date', $intCid, $intPropertyId );

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchActiveFlexibleLeaseStartWindowsCountByPropertyId( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) {
			return;
		}

		$strSql = sprintf( '
			SELECT COUNT(1)
	        FROM 
	            lease_terms lt
	            JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id )
	        WHERE 
	            lsw.cid= %d
	            AND lsw.property_id= %d
	            AND lsw.occupancy_type_id=' . COccupancyType::HOSPITALITY . '
	            AND lt.lease_term_type_id = ' . CLeaseTermType::FLEXIBLE . ' 
	            AND lsw.end_date >= CURRENT_DATE 
	            AND lt.deleted_on IS NULL
	            AND lsw.deleted_on IS NULL
	            AND lt.is_disabled IS FALSE 
	            AND lsw.is_active IS TRUE 
	            AND lt.is_prospect IS TRUE 
	            AND lt.show_on_website IS TRUE', $intCid, $intPropertyId );

		$arrintData = fetchData( $strSql, $objDatabase );
		return $arrintData[0]['count'] ?? 0;

	}

	public static function fetchActiveFlexibleLeaseStartWindowsByPropertyIdByLeaseStartDate( $intPropertyId, $intCid, $strLeaseStartDate, $strLeaseEndDate, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valStr( $strLeaseStartDate ) || false == valStr( $strLeaseEndDate ) ) {
			return;
		}

		$strSql = sprintf( ' SELECT
			           lsw.*,
			           lt.term_month
			          FROM 
			            lease_terms lt
			            JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id )
			          WHERE 
			            lsw.cid= %d
			            AND lsw.property_id= %d
			            AND lsw.occupancy_type_id=' . COccupancyType::HOSPITALITY . '
			            AND lt.lease_term_type_id = ' . CLeaseTermType::FLEXIBLE . ' 
						AND lsw.start_date <= \'' . $strLeaseStartDate . '\'
						AND lsw.end_date >= \'' . $strLeaseEndDate . '\' 
			            AND lsw.end_date >= CURRENT_DATE 
			            AND lt.deleted_on IS NULL
			            AND lsw.deleted_on IS NULL
			            AND lt.is_disabled IS FALSE 
			            AND lsw.is_active IS TRUE 
			            AND lt.is_prospect IS TRUE 
			            AND lt.show_on_website IS TRUE ', $intCid, $intPropertyId );

		return self::fetchLeaseStartWindow( $strSql, $objDatabase );
	}

	public static function fetchActiveFlexibleLeaseStartWindowsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) {
			return;
		}

		$strSql = sprintf( ' SELECT
                 lsw.*
                FROM 
                  lease_terms lt
                  JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id )
                WHERE 
                  lsw.cid= %d
                  AND lsw.property_id= %d
                  AND lsw.occupancy_type_id=' . COccupancyType::HOSPITALITY . '
                  AND lt.lease_term_type_id = ' . CLeaseTermType::FLEXIBLE . ' 
                  AND lsw.end_date >= CURRENT_DATE 
                  AND lt.deleted_on IS NULL
                  AND lsw.deleted_on IS NULL
                  AND lt.is_disabled IS FALSE 
                  AND lsw.is_active IS TRUE 
                  AND lt.is_prospect IS TRUE 
                  AND lt.show_on_website IS TRUE 
                ORDER BY lsw.start_date', $intCid, $intPropertyId );

		return self::fetchLeaseStartWindows( $strSql, $objDatabase );
	}

	public static function fetchActiveFlexibleLeaseStartWindowsByPropertyIdByCidByLeaseStartDate( $intPropertyId, $intCid, $strLeaseStartDate, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valStr( $strLeaseStartDate ) ) {
			return;
		}

		$strSql = sprintf( ' SELECT
                 lsw.*
                FROM 
                  lease_terms lt
                  JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id )
                WHERE 
                  lsw.cid= %d
                  AND lsw.property_id= %d
                  AND lsw.occupancy_type_id=' . COccupancyType::HOSPITALITY . '
                  AND lt.lease_term_type_id = ' . CLeaseTermType::FLEXIBLE . ' 
               AND lsw.start_date <= \'' . $strLeaseStartDate . '\'
               AND lsw.end_date >= \'' . $strLeaseStartDate . '\' 
                  AND lsw.end_date >= CURRENT_DATE 
                  AND lt.deleted_on IS NULL
                  AND lsw.deleted_on IS NULL
                  AND lt.is_disabled IS FALSE 
                  AND lsw.is_active IS TRUE 
                  AND lt.is_prospect IS TRUE 
                  AND lt.show_on_website IS TRUE ', $intCid, $intPropertyId );

		return self::fetchLeaseStartWindow( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartWindowDataByPropertyIdByIdByCid( $intPropertyId, $intId, $intCid, $objDatabase, $boolSkipFlexibleLeaseTerm = true ) {

		if( false == valId( $intId ) || false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strWhereSql = ( true == $boolSkipFlexibleLeaseTerm ? ' AND COALESCE( lsw.lease_term_type_id, 0 ) <> ' . CLeaseTermType::FLEXIBLE : '' );

		$strSql = 'SELECT
							lsw.id,lsw.cid,lsw.property_id,TO_CHAR(lsw.start_date:: DATE, \'Mon dd\') as start_date,TO_CHAR(lsw.end_date:: DATE, \'Mon dd,yyyy\') as end_date,
							oc.reservation_code
							FROM
							lease_start_windows lsw
							JOIN organization_contracts oc ON ( oc.lease_start_window_id = lsw.id AND oc.property_id = lsw.property_id ) 
							WHERE
							lsw.id = ' . ( int ) $intId . '
							AND lsw.property_id = ' . ( int ) $intPropertyId . '
							AND lsw.deleted_on IS NULL
							AND lsw.cid = ' . ( int ) $intCid . '
							' . $strWhereSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveFlexibleLeaseStartWindowsByPropertyIdByLeaseStartDateByLeaseEndDate( $intPropertyId, $intCid, $strLeaseStartDate, $strLeaseEndDate, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valStr( $strLeaseStartDate ) || false == valStr( $strLeaseEndDate ) || strtotime( $strLeaseStartDate ) > strtotime( $strLeaseEndDate ) ) {
			return;
		}

		$strSql = sprintf( ' SELECT
					lsw.*
					FROM 
					lease_terms lt
					JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id )
					WHERE 
					lsw.cid= %d
					AND lsw.property_id= %d
					AND lsw.occupancy_type_id=' . COccupancyType::HOSPITALITY . '
					AND lt.lease_term_type_id = ' . CLeaseTermType::FLEXIBLE . ' 
					AND lsw.start_date <= \'' . $strLeaseStartDate . '\'
					AND lsw.end_date >= \'' . $strLeaseEndDate . '\' 
					AND lsw.end_date >= CURRENT_DATE 
					AND lt.deleted_on IS NULL
					AND lsw.deleted_on IS NULL
					AND lt.is_disabled IS FALSE 
					AND lsw.is_active IS TRUE 
					AND lt.is_prospect IS TRUE 
					AND lt.show_on_website IS TRUE ', $intCid, $intPropertyId );
		return self::fetchLeaseStartWindow( $strSql, $objDatabase );
	}

}
?>
