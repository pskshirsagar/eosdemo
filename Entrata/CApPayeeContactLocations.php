<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeContactLocations
 * Do not add any new functions to this class.
 */

class CApPayeeContactLocations extends CBaseApPayeeContactLocations {

	public static function fetchApPayeeContactLocationsExceptByApPayeeContactIdByCid( $intApPayeeContactId, $objDatabase, $arrintLocationIds = NULL, $intCid, $boolIsPrimary = NULL ) {

		$strWhereCondition = '';

		if( false == is_null( $boolIsPrimary ) ) {
			$strWhereCondition .= ' AND is_primary = ' . $boolIsPrimary;
		}

		if( true == valIntArr( $arrintLocationIds ) ) {
			$strWhereCondition .= ' AND ap_payee_location_id IN ( ' . ( implode( ',', $arrintLocationIds ) ) . ')';
		}

		if( true == valId( $intApPayeeContactId ) ) {
			$strWhereCondition .= ' AND ap_payee_contact_id  <> ' . ( int ) $intApPayeeContactId;
		}

		$strSql = ' SELECT
						*
					FROM
						ap_payee_contact_locations
					WHERE
						cid =' . ( int ) $intCid . $strWhereCondition;

		return self::fetchApPayeeContactLocations( $strSql, $objDatabase );
	}

	public static function fetchPrimaryApPayeeContactLocationByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {

		if( false == valId( $intApPayeeLocationId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						apcl.*
					FROM
						ap_payee_contact_locations apcl
						JOIN ap_payee_contacts apc ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id )
					WHERE
						apcl.is_primary = true
						AND apcl.cid = ' . ( int ) $intCid . '
						AND apcl.ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND apc.name_first IS NOT NULL
						AND apc.name_last IS NOT NULL LIMIT 1';

		return self::fetchApPayeeContactLocation( $strSql, $objDatabase );
	}

	public static function fetchPrimaryApPayeeContactLocationByApPayeeContactIdByApPayeeLocationIdByCid( $intApPayeeContactId, $intApPayeeLocationId, $intCid, $objDatabase ) {

		if( false == valId( $intApPayeeLocationId ) || false == valId( $intCid ) || false == valId( $intApPayeeContactId ) ) return NULL;

		$strSql = 'SELECT
						apcl.*
					FROM
						ap_payee_contact_locations apcl
						JOIN ap_payee_contacts apc ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id )
					WHERE
						apcl.is_primary = true
						AND apcl.cid = ' . ( int ) $intCid . '
						AND apcl.ap_payee_contact_id = ' . ( int ) $intApPayeeContactId . '
						AND apcl.ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND apc.name_first IS NOT NULL
						AND apc.name_last IS NOT NULL LIMIT 1';

		return self::fetchApPayeeContactLocation( $strSql, $objDatabase );
	}

	public static function fetchAllPrimaryApPayeeContactLocationsByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {

		if( false == valId( $intApPayeeLocationId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						apcl.*
					FROM
						ap_payee_contact_locations apcl
						JOIN ap_payee_contacts apc ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id )
					WHERE
						apcl.is_primary = true
						AND apcl.cid = ' . ( int ) $intCid . '
						AND apcl.ap_payee_location_id = ' . ( int ) $intApPayeeLocationId;

		return self::fetchApPayeeContactLocation( $strSql, $objDatabase );
	}

	public static function fetchApPayeeContactLocationByApPayeeContactIdByApPayeeLocationIdByCid( $intApPayeeContactId, $intApPayeeLocationId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApPayeeContactId ) || false == valId( $intApPayeeLocationId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_contact_locations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_contact_id = ' . ( int ) $intApPayeeContactId . '
						AND ap_payee_location_id = ' . ( int ) $intApPayeeLocationId;

		return self::fetchApPayeeContactLocation( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactLocationsByApPayeeContactIdsByApPayeeLocationIdsByCid( $arrintApPayeeLocationIds, $arrintApPayeeContactIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeContactIds ) || false == valArr( $arrintApPayeeLocationIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_contact_locations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_location_id IN ( ' . ( implode( ',', $arrintApPayeeLocationIds ) ) . ')
						AND ap_payee_contact_id IN ( ' . ( implode( ',', $arrintApPayeeContactIds ) ) . ')';

		return self::fetchApPayeeContactLocations( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactLocationsByApPayeeIdByApPayeeLocationIdsByCid( $intApPayeeId, $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {

		if( false == valId( $intApPayeeId ) || false == valIntArr( $arrintApPayeeLocationIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						apcl.*
					FROM
						ap_payee_contact_locations apcl
						JOIN ap_payee_locations apl ON ( apl.cid = apcl.cid AND apl.id = apcl.ap_payee_location_id )
						JOIN ap_payee_contacts apc ON ( apl.cid = apc.cid AND apc.id = apcl.ap_payee_contact_id )
						JOIN ap_payees ap ON ( ap.cid = apc.cid AND ap.id = apc.ap_payee_id )
					WHERE
						apcl.cid = ' . ( int ) $intCid . '
						AND ap.ap_payee_type_id IN ( 1, 3 )
						AND ap.ap_payee_status_type_id IN ( 1 )
						AND apc.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apcl.ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )';

		return self::fetchApPayeeContactLocations( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactLocationsByApPayeeContactIdsByCId( $arrintApPayeeContactIds, $intCid, $objDatabase, $boolIsPrimary = false ) {

		if( false == valArr( $arrintApPayeeContactIds ) ) return NULL;

		$strCondition = '';
		if( true == $boolIsPrimary ) {
			$strCondition = ' AND is_primary IS TRUE';
		}

		$strSql = 'SELECT
						*
					FROM
						ap_payee_contact_locations
					WHERE
						ap_payee_contact_id IN( ' . sqlIntImplode( $arrintApPayeeContactIds ) . ' )
						AND cid = ' . ( int ) $intCid .
						$strCondition;

		return self::fetchApPayeeContactLocations( $strSql, $objDatabase );
	}

	public static function fetchAllApPayeeContactLocationsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase, $boolIsPrimary = false ) {

		$strCondition = '';
		if( true == $boolIsPrimary ) {
			$strCondition = ' AND apcl.is_primary IS TRUE';
		}

		$strSql = 'SELECT
						apcl.*
					FROM
						ap_payee_contact_locations apcl
						JOIN ap_payee_locations apl ON ( apl.cid = apcl.cid AND apl.id = apcl.ap_payee_location_id )
						JOIN ap_payee_contacts apc ON ( apl.cid = apc.cid AND apc.id = apcl.ap_payee_contact_id )
						JOIN ap_payees ap ON ( ap.cid = apc.cid AND ap.id = apc.ap_payee_id )
					WHERE
						apcl.cid = ' . ( int ) $intCid . '
						AND apc.ap_payee_id = ' . ( int ) $intApPayeeId .
						$strCondition;

		return self::fetchApPayeeContactLocations( $strSql, $objDatabase );

	}

	public static function fetchAllApPayeeContactLocationsByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objDatabase, $boolIsPrimary = false ) {

		if( false == valArr( $arrintApPayeeIds ) ) {
			return NULL;
		}

		$strWhere = '';
		if( true == $boolIsPrimary ) {
			$strWhere = ' AND apcl.is_primary IS TRUE';
		}

		$strSql = 'SELECT
						apcl.*,
						ap.id as ap_payee_id
					FROM
						ap_payee_contact_locations apcl
						JOIN ap_payee_locations apl ON ( apl.cid = apcl.cid AND apl.id = apcl.ap_payee_location_id )
						JOIN ap_payee_contacts apc ON ( apc.cid = apl.cid AND apc.id = apcl.ap_payee_contact_id )
						JOIN ap_payees ap ON ( ap.cid = apc.cid AND ap.id = apc.ap_payee_id )
					WHERE
						apcl.cid = ' . ( int ) $intCid . '
						AND apc.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )' .
						$strWhere;

		return self::fetchApPayeeContactLocations( $strSql, $objDatabase );

	}

}
?>