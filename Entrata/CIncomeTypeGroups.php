<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIncomeTypeGroups
 * Do not add any new functions to this class.
 */

class CIncomeTypeGroups extends CBaseIncomeTypeGroups {

	public static function fetchIncomeTypeGroups( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CIncomeTypeGroup', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchIncomeTypeGroup( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CIncomeTypeGroup', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>