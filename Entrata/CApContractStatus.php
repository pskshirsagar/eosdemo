<?php

class CApContractStatus extends CBaseApContractStatus {

	const ACTIVE		= 1;
	const COMPLETED		= 2;
	const CANCELLED		= 3;

	public static $c_arrintActiveContractStatuses = [
		self::ACTIVE		=> 'Active',
		self::COMPLETED	=> 'Completed',
		self::CANCELLED	=> 'Cancelled',

	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>