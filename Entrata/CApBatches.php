<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApBatches
 * Do not add any new functions to this class.
 */

class CApBatches extends CBaseApBatches {

	public static function fetchPaginatedPausedApBatchesByCid( $intCid, $objPagination, $objClientDatabase, $objApTransactionsFilter = NULL ) {

		$strConditions = '';

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) && true == valArr( $objApTransactionsFilter->getPropertyIds() ) ) {
			$strConditions = ' AND ( ( ab.property_id IN ( ' . implode( ',', $objApTransactionsFilter->getPropertyIds() ) . ' ) OR ab.property_id IS NULL ) AND ad.property_id IN ( ' . implode( ',', $objApTransactionsFilter->getPropertyIds() ) . ' ) OR ad.property_id IS NULL )';
		}

		$strSql = 'SELECT
						DISTINCT
						CASE
							WHEN (
									1 < (
											SELECT
												COUNT ( DISTINCT ad1.property_id )
											FROM
												ap_details ad1
												JOIN ap_headers ah ON ( ah.cid = ad1.cid AND ah.id = ad1.ap_header_id )
											WHERE
												ad1.cid = ' . ( int ) $intCid . '
												AND ab.cid = ah.cid
												AND ab.id = ah.ap_batch_id
										)
								)
							THEN
								\'Multiple\'
							ELSE
								p.property_name
						END AS property_name,
						(
							SELECT
								COUNT ( DISTINCT ad1.property_id )
							FROM
								ap_details ad1
								JOIN ap_headers ah ON ( ah.cid = ad1.cid AND ah.id = ad1.ap_header_id )
							WHERE
								ad1.cid = ' . ( int ) $intCid . '
								AND ab.cid = ah.cid
								AND ab.id = ah.ap_batch_id
						) AS property_count,
						ab.*,
						ce.name_first AS name_first,
						ce.name_last AS name_last
					FROM
						ap_batches ab
						LEFT JOIN ap_headers ah ON ( ab.cid = ah.cid AND ab.id = ah.ap_batch_id )
						LEFT JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL )
						JOIN company_users cu ON ( ab.cid = cu.cid AND ab.created_by = cu.id AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id AND cu.default_company_user_id IS NULL )
						LEFT JOIN properties p ON ( p.cid = ad.cid AND p.id = ad.property_id )
					WHERE
						ab.cid = ' . ( int ) $intCid . '
						AND ab.is_paused = 1' .
						$strConditions . '
					ORDER BY
						ab.created_on DESC';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPausedApBatchesCountByCid( $intCid, $objClientDatabase, $objApTransactionsFilter = NULL ) {

		$strConditions = '';

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) && true == valArr( $objApTransactionsFilter->getPropertyIds() ) ) {
			$strConditions = ' AND ( ( ab.property_id IN ( ' . implode( ',', $objApTransactionsFilter->getPropertyIds() ) . ' ) OR ab.property_id IS NULL ) OR ad.property_id IN ( ' . implode( ',', $objApTransactionsFilter->getPropertyIds() ) . ' ) )';
		}

		$strSql = 'SELECT
						COUNT( DISTINCT ab.id )
					FROM
						ap_batches ab
						LEFT JOIN ap_headers ah ON ( ab.cid = ah.cid AND ab.id = ah.ap_batch_id )
						LEFT JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL )
						JOIN company_users cu ON ( ab.cid = cu.cid AND ab.created_by = cu.id AND cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id AND cu.default_company_user_id IS NULL )
					WHERE
						ab.cid = ' . ( int ) $intCid . '
						AND ab.is_paused = 1 ' .
						$strConditions;

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchPausedApBatchPropertiesByIdByCid( $intId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						DISTINCT p.property_name,
						p.is_disabled
					FROM
						ap_batches ab
						LEFT JOIN ap_headers ah ON ( ab.cid = ah.cid AND ab.id = ah.ap_batch_id )
						LEFT JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL )
						LEFT JOIN properties p ON ( p.cid = ad.cid AND p.id = ad.property_id )
					WHERE
						ab.cid = ' . ( int ) $intCid . '
						AND ab.id = ' . ( int ) $intId;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPausedApBatchByIdByCid( $intApBatchId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApBatchId ) ) return false;

		return self::fetchApBatch( 'SELECT * FROM ap_batches WHERE id = ' . ( int ) $intApBatchId . ' AND cid = ' . ( int ) $intCid, $objClientDatabase );
	}

	public static function fetchPausedApBatchesByCid( $intCid, $objClientDatabase ) {
		return self::fetchApBatches( 'SELECT * FROM ap_batches WHERE cid = ' . ( int ) $intCid . ' AND is_paused = 1', $objClientDatabase );
	}

	public static function fetchApBatchesByIdsByCid( $arrintApBatchIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApBatchIds ) ) {
			return NULL;
		}

		return self::fetchApBatches( 'SELECT * FROM ap_batches WHERE id IN ( ' . implode( ',', $arrintApBatchIds ) . ' ) AND cid = ' . ( int ) $intCid, $objClientDatabase );
	}

}
?>