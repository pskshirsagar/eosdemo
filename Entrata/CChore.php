<?php

class CChore extends CBaseChore {

	protected $m_intPeriodId;
	protected $m_intPropertyId;
	protected $m_intDefaultChoreTypeId;
	protected $m_intIsRequiredToLockMonth;
	protected $m_intIsRequiredToAdvanceMonth;

	protected $m_strPostMonth;
	protected $m_strPropertyName;

	/**
	* Create Functions
	*
	*/

	public function createChoreReference() {

		$objChoreReference = new CChoreReference();
		$objChoreReference->setCid( $this->getCid() );

		return $objChoreReference;
	}

	/**
	* Get Functions
	*
	*/

	public function getIsRequiredToLockMonth() {
		return $this->m_intIsRequiredToLockMonth;
	}

	public function getIsRequiredToAdvanceMonth() {
		return $this->m_intIsRequiredToAdvanceMonth;
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getDefaultChoreTypeId() {
		return $this->m_intDefaultChoreTypeId;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	/**
	* Set Functions
	*
	*/

	public function setIsRequiredToLockMonth( $intIsRequiredToLockMonth ) {
		$this->m_intIsRequiredToLockMonth = CStrings::strToIntDef( $intIsRequiredToLockMonth, NULL, false );
	}

	public function setIsRequiredToAdvanceMonth( $intIsRequiredToAdvanceMonth ) {
		$this->m_intIsRequiredToAdvanceMonth = CStrings::strToIntDef( $intIsRequiredToAdvanceMonth, NULL, false );
	}

	public function setPeriodId( $intPeriodId ) {
		$this->m_intPeriodId = CStrings::strToIntDef( $intPeriodId, NULL, false );
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
	}

	public function setDefaultChoreTypeId( $intDefaultChoreTypeId ) {
		$this->m_intDefaultChoreTypeId = CStrings::strToIntDef( $intDefaultChoreTypeId, NULL, false );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, 50, NULL, true );
	}

	public function setPostMonth( $strPostMonth ) {
		$this->m_strPostMonth = CStrings::strTrimDef( $strPostMonth, -1, NULL, true );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['is_required_to_lock_month'] ) ) $this->setIsRequiredToLockMonth( $arrmixValues['is_required_to_lock_month'] );
		if( true == isset( $arrmixValues['is_required_to_advance_month'] ) ) $this->setIsRequiredToAdvanceMonth( $arrmixValues['is_required_to_advance_month'] );
		if( true == isset( $arrmixValues['period_id'] ) ) $this->setPeriodId( $arrmixValues['period_id'] );
		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['default_chore_type_id'] ) ) $this->setDefaultChoreTypeId( $arrmixValues['default_chore_type_id'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['post_month'] ) ) $this->setPostMonth( $arrmixValues['post_month'] );
		return;
	}

	/**
	* Validation Functions
	*
	*/

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChoreCategoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChoreTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChoreStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChorePriorityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChoreDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDueDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valItemCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPendingChores( $arrmixPendingChores ) {

		$boolIsValid = true;

		$arrstrPropertyNames	= [];
		$arrstrChoreCategories	= [];

		foreach( $arrmixPendingChores as $arrmixPendingChore ) {

			$arrstrCategoryNames							= explode( ' ', $arrmixPendingChore['category_name'] );
			$arrstrChoreCategories[$arrstrCategoryNames[0]]	= $arrstrCategoryNames[0];

			if( true == valStr( $arrmixPendingChore['property_name'] ) ) {
				$arrstrPropertyNames[$arrmixPendingChore['property_name']] = $arrmixPendingChore['property_name'];
			}
		}

		if( true == valArr( $arrstrChoreCategories ) && true == valArr( $arrstrPropertyNames ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Please complete all pending {%s, chore_categories} tasks for- {%s, property_names}.', [ 'chore_categories' => implode( ' and ', $arrstrChoreCategories ), 'property_names' => implode( ', ', $arrstrPropertyNames ) ] ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixPendingChores = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_pending_chores':
				$boolIsValid &= $this->valPendingChores( $arrmixPendingChores );
				break;

			case 'validate_bulk_pending_chores':
				$boolIsValid &= $this->valBulkPeriodPendingChores( $arrmixPendingChores );
				break;

			default:
				return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valBulkPeriodPendingChores( $arrmixPendingChores ) {

		$boolIsValid = true;

		$arrstrPostMonth	= [];

		foreach( $arrmixPendingChores as $arrmixPendingChore ) {
			 $arrstrPostMonths = explode( ' ', ( new DateTime( $arrmixPendingChore['post_month'] ) )->format( 'm/Y' ) );
			 $arrstrPostMonth[$arrstrPostMonths[0]] = $arrstrPostMonths[0];
		}

		if( true == valArr( $arrstrPostMonth ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'This property has pending closing tasks in {%s, 0} that are required to be completed before locking the period. Complete those closing tasks to perform this action.', [ implode( ', ', $arrstrPostMonth ) ] ) ) );
		}

		return $boolIsValid;
	}

	/**
	* Fetch Functions
	*
	*/

	public function fetchPeriodByChoreId( $objClientDatabase ) {
		return CPeriods::fetchPeriodByChoreIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

}