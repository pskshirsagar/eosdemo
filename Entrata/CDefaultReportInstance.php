<?php

class CDefaultReportInstance extends CBaseDefaultReportInstance {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultReportGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valModuleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentModuleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportFilterId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$intCurrentUserId;

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();
		if( CCluster::RAPID == $objDatabase->getClusterId() ) {
			$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, default_report_group_id, default_report_id, default_report_version_id, name, description, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlDefaultReportGroupId() . ', ' .
						$this->sqlDefaultReportId() . ', ' .
						$this->sqlDefaultReportVersionId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';
		} else {
			$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, default_report_group_id, default_report_id, default_report_version_id, name, description )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlDefaultReportGroupId() . ', ' .
						$this->sqlDefaultReportId() . ', ' .
						$this->sqlDefaultReportVersionId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ' ) ' . ' RETURNING id;';
		}
		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

}
?>