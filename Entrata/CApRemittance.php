<?php

use Psi\CStringService;
use Psi\Libraries\Cryptography\CCrypto;

class CApRemittance extends CBaseApRemittance {

	protected $m_boolIsVerified;
	protected $m_boolIsPrimary;

	protected $m_intApPayeeLocationId;
	protected $m_intCustomerId;
	protected $m_intDefaultApRemittanceId;
	protected $m_strAccountName;
	protected $m_strCompanyName;
	protected $m_strConfirmCheckAccountNumber;
	protected $m_strAccountNumber;
	protected $m_intAccountId;
	protected $m_arrmixApPayeeRemittanceEvent;
	protected $m_strLocationName;

	/** @var  \CApPayeeAccount[] */
	protected $m_arrobjApPayeeAccounts;

	public function getIsVerified() {
		return $this->m_boolIsVerified;
	}

	public function getIsPrimary() {
		return $this->m_boolIsPrimary;
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getCheckAccountNumberMasked() {
		$strCheckAccountNumber = $this->getCheckAccountNumber();
		$intStringLength = strlen( $strCheckAccountNumber );
		$strLastFour = CStringService::singleton()->substr( $strCheckAccountNumber, -4 );

		return CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getCheckAccountNumber() {
		if( false == \valStr( $this->getCheckAccountNumberEncrypted() ) ) {
			return NULL;
		}

		return CCrypto::createService()->decrypt( $this->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	public function getConfirmCheckAccountNumber() {
		return $this->m_strConfirmCheckAccountNumber;
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function getDefaultApRemittanceId() {
		return $this->m_intDefaultApRemittanceId;
	}

	public function getApPayeeAccounts() {
		return $this->m_arrobjApPayeeAccounts;
	}

	public function getApPayeeRemittanceEvent() {
		return $this->m_arrmixApPayeeRemittanceEvent;
	}

	public function getCcCardNumberMasked() {
		$strCcCardNumber = $this->getCcCardNumber();
		$intStringLength = strlen( $strCcCardNumber );
		$strLastFour = CStringService::singleton()->substr( $strCcCardNumber, -4 );

		return CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getCcCardNumber() {
		if( false == \valStr( $this->getCcCardNumberEncrypted() ) ) {
			return NULL;
		}

		return CCrypto::createService()->decrypt( $this->getCcCardNumberEncrypted(), CONFIG_SODIUM_KEY_CC_CARD_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CC_CARD_NUMBER ] );
	}

	public function getLocationName() {
		return $this->m_strLocationName;
	}

	public function setDefaultApRemittanceId( $intDefaultApRemittanceId ) {
		$this->m_intDefaultApRemittanceId = CStrings::strToIntDef( $intDefaultApRemittanceId, NULL, false );
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = CStrings::strToIntDef( $intCustomerId, NULL, false );
	}

	public function setIsVerified( $boolIsVerified ) {
		$this->m_boolIsVerified = CStrings::strToBool( $boolIsVerified );
	}

	public function setIsPrimary( $boolIsPrimary ) {
		$this->m_boolIsPrimary = CStrings::strToBool( $boolIsPrimary );
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->m_intApPayeeLocationId = CStrings::strToIntDef( $intApPayeeLocationId, NULL, false );
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

	public function setCcCardNumber( $strCcCardNumber ) {
		$strCcCardNumber = CStrings::strTrimDef( $strCcCardNumber, 20, NULL, true );
		// Ensure that a masked card number is always set on object
		if( false == is_null( $strCcCardNumber ) ) {
			$strCcCardNumber = str_repeat( 'X', strlen( $strCcCardNumber ) - 4 ) . CStringService::singleton()->substr( $strCcCardNumber, -4 );
		}

		if( true == \valStr( $strCcCardNumber ) ) {
			$this->setCcCardNumberEncrypted( CCrypto::createService()->encrypt( $strCcCardNumber, CONFIG_SODIUM_KEY_CC_CARD_NUMBER ) );
		}
	}

	public function setCheckAccountNumber( $strCheckAccountNumber ) {
		if( true == CStringService::singleton()->stristr( $strCheckAccountNumber, 'X' ) ) return;
		$strCheckAccountNumber = CStrings::strTrimDef( $strCheckAccountNumber, 20, NULL, true );

		if( true == \valStr( $strCheckAccountNumber ) ) {
			$this->setCheckAccountNumberEncrypted( CCrypto::createService()->encrypt( $strCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
		}
	}

	public function setConfirmCheckAccountNumber( $strConfirmCheckAccountNumber ) {
		return $this->m_strConfirmCheckAccountNumber = $strConfirmCheckAccountNumber;
	}

	public function setAccountNumber( $strAccountNumber ) {
		return $this->m_strAccountNumber = $strAccountNumber;
	}

	public function setAccountId( $strAccountId ) {
		return $this->m_intAccountId = $strAccountId;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setLocationName( $strLocationName ) {
		$this->m_strLocationName = $strLocationName;
	}

	public function addApPayeeAccount( $objApPayeeAccount ) {
		$this->m_arrobjApPayeeAccounts[] = $objApPayeeAccount;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['account_name'] ) ) $this->setAccountName( $arrmixValues['account_name'] );
		if( true == isset( $arrmixValues['ap_payee_location_id'] ) ) $this->setApPayeeLocationId( $arrmixValues['ap_payee_location_id'] );
		if( true == isset( $arrmixValues['is_primary'] ) ) $this->setIsPrimary( $arrmixValues['is_primary'] );
		if( true == isset( $arrmixValues['company_name'] ) ) $this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['customer_id'] ) ) $this->setCustomerId( $arrmixValues['customer_id'] );
		if( true == isset( $arrmixValues['check_account_number'] ) ) $this->setCheckAccountNumber( $arrmixValues['check_account_number'] );
		if( true == isset( $arrmixValues['confirm_check_account_number'] ) ) $this->setConfirmCheckAccountNumber( $arrmixValues['confirm_check_account_number'] );
		if( true == isset( $arrmixValues['account_number'] ) ) $this->setAccountNumber( $arrmixValues['account_number'] );
		if( true == isset( $arrmixValues['account_id'] ) ) $this->setAccountId( $arrmixValues['account_id'] );
		if( true == isset( $arrmixValues['default_ap_remittance_id'] ) ) $this->setDefaultApRemittanceId( $arrmixValues['default_ap_remittance_id'] );
		if( true == isset( $arrmixValues['postal_code'] ) ) $this->setPostalCode( CStringService::singleton()->strtoupper( $arrmixValues['postal_code'] ) );
		if( true == isset( $arrmixValues['location_name'] ) ) $this->setLocationName( $arrmixValues['location_name'] );
	}

	public function encryptCheckAccountNumber() {
		if( false == is_null( $this->getCheckAccountNumberEncrypted() ) ) {
			$this->setCheckAccountNumberEncrypted( CCrypto::createService()->encrypt( $this->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
	    }
	}

	public function decryptCheckAccountNumber() {
		if( false == is_null( $this->getCheckAccountNumberEncrypted() ) ) {
			$this->setCheckAccountNumberEncrypted( CCrypto::createService()->decrypt( $this->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] ) );
		}
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckAccountTypeId() {
		$boolIsValid = true;

		if( $this->getApPaymentTypeId() == CApPaymentType::ACH && true == is_null( $this->getCheckAccountTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', ' Account Type is required.' ) );
		}
		return $boolIsValid;
	}

	public function valCheckNameOnAccount() {
		$boolIsValid = true;

		if( true == in_array( $this->getApPaymentTypeId(), [ CApPaymentType::ACH, CApPaymentType::BILL_PAY_ACH ] ) && true == is_null( $this->getCheckNameOnAccount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', ' Name on account is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckBankName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ' Remittance Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckRoutingNumber( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == in_array( $this->getApPaymentTypeId(), [ CApPaymentType::ACH, CApPaymentType::BILL_PAY_ACH ] ) && true == is_null( $this->getCheckRoutingNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', ' Routing number is required.' ) );
		}

		if( false == is_null( $this->getCheckRoutingNumber() ) && 0 !== preg_match( '/[^0-9]/', $this->getCheckRoutingNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', ' Routing number must be a number.' ) );
			return false;
		}

		if( false == is_null( $this->getCheckRoutingNumber() ) && 9 != strlen( $this->getCheckRoutingNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', ' Routing number must be 9 digits long.' ) );
			return false;
		}

		if( false == is_null( $objDatabase ) ) {
			// If it is 9 numeric characters see if it is a fed ach participant
			$objFedAchParticipant = \Psi\Eos\Payment\CFedAchParticipants::createService()->fetchFedAchParticipantByRoutingNumber( $this->getCheckRoutingNumber(), $objDatabase );

			if( true == is_null( $objFedAchParticipant ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number is not valid.', 624 ) );
			}
		}

		return $boolIsValid;
	}

	public function valCheckAccountNumberEncrypted() {
		$boolIsValid = true;

		if( true == in_array( $this->getApPaymentTypeId(), [ CApPaymentType::ACH, CApPaymentType::BILL_PAY_ACH ] ) && true == is_null( $this->getCheckAccountNumberEncrypted() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', ' Account number is required.' ) );
		}

		if( true == in_array( $this->getApPaymentTypeId(), [ CApPaymentType::ACH, CApPaymentType::BILL_PAY_ACH ] ) && ( false == is_null( $this->getCheckAccountNumberEncrypted() ) && true == preg_match( '/^X/', $this->getCheckAccountNumber() ) ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', ' Account number has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valConfirmCheckAccountNumber() {
		$boolIsValid = true;
		$strDecryptedAccountNumber = $this->getCheckAccountNumber();

		if( 0 != strlen( $strDecryptedAccountNumber ) && 0 == strlen( $this->m_strConfirmCheckAccountNumber ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'confrim_check_account_number', 'Confirm account number is required.', 602 ) );
			$boolIsValid = false;
		} elseif( 0 != strlen( $strDecryptedAccountNumber ) && $strDecryptedAccountNumber != $this->m_strConfirmCheckAccountNumber ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'verify_check_account_number', 'Confirm account number and account number do not match.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCheckBlackListedBankAccount( $objClientDatabase ) {
		$boolIsValid = true;

		$objPaymentBlacklistEntry = new CPaymentBlacklistEntry();
		$objBlacklistedEntry = $objPaymentBlacklistEntry->checkBlacklistedBankAccount( $objClientDatabase, $this->getCheckRoutingNumber(), $this->getCheckAccountNumber() );

		if( NULL == $objBlacklistedEntry ) {
			return true;
		}

		switch( $objBlacklistedEntry->getPaymentBlacklistTypeId() ) {
			case CPaymentBlacklistType::NONE:
				return true;

			case CPaymentBlacklistType::ROUTING_NUMBER:
				$this->addErrorMsg( new CErrorMsg( 'ERROR_TYPE_VALIDATION', 'blacklist_bank_account', $objBlacklistedEntry->getBlacklistReason() ) );
				$boolIsValid = false;
				break;

			default:
				$this->addErrorMsg( new CErrorMsg( 'ERROR_TYPE_VALIDATION', 'blacklist_bank_account', $objBlacklistedEntry->getBlacklistReason() ) );
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function valStreetLine1() {

		$boolIsValid = true;

		$arrintApPaymentTypeIds = [ CApPaymentType::CHECK, CApPaymentType::BILL_PAY ];

		if( true == in_array( $this->getApPaymentTypeId(), $arrintApPaymentTypeIds ) && true == is_null( $this->getStreetLine1() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', ' Address Line 1 is required.' ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine2() {
		$boolIsValid = true;

		if( false == is_null( $this->getStreetLine2() ) && false == preg_match( '/^[a-zA-Z0-9\\\'\"\-\>\\/\,\\\ ]+$/', $this->getStreetLine2() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line2', ' Address Line 2 has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine3() {
		$boolIsValid = true;

		if( false == is_null( $this->getStreetLine3() ) && false == preg_match( '/^[a-zA-Z0-9\\\'\" ]+$/', $this->getStreetLine3() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line3', ' Contact Details:- Street Line 3 has invalid characters.' ) );
		}

		return $boolIsValid;
	}

	public function valCity( $boolIsRequired = false ) {
		$boolIsValid = true;

		$arrintApPaymentTypeIds = [ CApPaymentType::CHECK, CApPaymentType::BILL_PAY ];

		if( true == $boolIsRequired && true == in_array( $this->getApPaymentTypeId(), $arrintApPaymentTypeIds ) && true == is_null( $this->getCity() ) ) {
			$boolIsValid = false;
		}

		if( true == is_numeric( $this->getCity() ) ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valStateCode() {

		$boolIsValid = true;

		$arrintApPaymentTypeIds = [ CApPaymentType::CHECK, CApPaymentType::BILL_PAY ];

		if( true == in_array( $this->getApPaymentTypeId(), $arrintApPaymentTypeIds ) && true == is_null( $this->getStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', ' State is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode( $boolIsRequired = false ) {

		$boolIsValid = true;

		$intLength = strlen( str_replace( '-', '', $this->getPostalCode() ) );
		$arrintApPaymentTypeIds = [ CApPaymentType::CHECK, CApPaymentType::BILL_PAY ];

		if( true == $boolIsRequired && true == in_array( $this->getApPaymentTypeId(), $arrintApPaymentTypeIds ) && true == is_null( $this->getPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', ' Postal code is required.' ) );
		} elseif( false == is_null( $this->getPostalCode() ) && CCountry::CODE_USA == $this->getCountryCode() && 0 < strlen( str_replace( '-', '', $this->getPostalCode() ) )
					&& ( ( false == preg_match( '/^([[:alnum:]]){5,5}?$/', $this->getPostalCode() ) && false == preg_match( '/^([[:alnum:]]){5,5}-([[:alnum:]]){4,4}?$/', $this->getPostalCode() ) ) ) ) {

						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code must be 5 or 10 characters in XXXXX or XXXXX-XXXX format and should contain only alphanumeric characters.' ) );
		} elseif( false == is_null( $this->getPostalCode() ) && CCountry::CODE_CANADA == $this->getCountryCode() && 0 < strlen( str_replace( ' ', '', $this->getPostalCode() ) )
					&& ( ( false == preg_match( '/^([[:alnum:]]){3,3}?$/', $this->getPostalCode() ) && false == preg_match( '/^([[:alnum:]]){3,3} ([[:alnum:]]){3,3}?$/', $this->getPostalCode() ) ) ) ) {

						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code must be 6 characters in XXX XXX format and should contain only alphanumeric characters.' ) );
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillPayAchOrCheck() {

		$boolIsValid		= true;
		$boolIsValidAch		= true;
		$boolIsValidCheck	= true;

		$boolIsValidCheck	&= $this->valRemittanceAddress(); // true when either complete blank or complete filled
		$boolIsValidAch		&= $this->valBillPayAchAccountInfo(); // true when either complete blank or complete filled

		if( ( true == $boolIsValidCheck && false == valStr( $this->getStreetLine1() ) )
			&& ( true == $boolIsValidAch && false == valStr( $this->getCheckNameOnAccount() ) ) ) {
			// checks for completely blank remittance address
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_pay', 'Please enter remittance information for Bill Pay ACH or Check.' ) );
			$boolIsValid &= false;
		}

		if( false == $boolIsValidCheck || false == $boolIsValidAch ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valBillPayAchAccountInfo() {
		$boolIsValid	= true;

		if( true == valStr( $this->getCheckNameOnAccount() ) || true == valStr( $this->getCheckRoutingNumber() ) || true == valStr( $this->getCheckAccountNumberEncrypted() ) ) {
			$boolIsValid &= $this->valCheckNameOnAccount();
			$boolIsValid &= $this->valCheckRoutingNumber();
			$boolIsValid &= $this->valCheckAccountNumberEncrypted();
		}
		return $boolIsValid;
	}

	public function valRemittanceAddress() {
		$boolIsValid	= true;
		$arrintApPaymentTypeIds = [ CApPaymentType::CHECK, CApPaymentType::BILL_PAY_ACH, CApPaymentType::BILL_PAY, CApPaymentType::BILL_PAY_CHECK, CApPaymentType::WRITTEN_CHECK, CApPaymentType::AVID_PAY ];
		if( true == in_array( $this->getApPaymentTypeId(), $arrintApPaymentTypeIds ) ) {
			if( true == valStr( $this->getStreetLine1() ) || true == valStr( $this->getCity() ) || true == valStr( $this->getStateCode() ) || true == valStr( $this->getPostalCode() ) ) {

				if( false == valStr( $this->getStreetLine1() ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', ' Address Line 1 is missing.' ) );
					$boolIsValid = false;
				}

				if( false == valStr( $this->getCity() ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', ' City is missing.' ) );
					$boolIsValid = false;
				}

				if( false == valStr( $this->getStateCode() ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', ' State is missing.' ) );
					$boolIsValid = false;
				}

				if( false == valStr( $this->getPostalCode() ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', ' Zip is missing.' ) );
					$boolIsValid = false;
				}
			}
		}
		return $boolIsValid;
	}

	public function valCcExpirationDate() {
		$boolIsValid = true;

		$strExpirationDate = strtotime( $this->getCcExpDateYear() . '-' . $this->getCcExpDateMonth() );

		// Need to check for null values later
		if( strtotime( date( 'Y-m' ) ) > $strExpirationDate ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'exp_date', 'Property or Resident debit card information has expired.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsValidate = false, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'insert_vendor_remittance_contact_info':
				$boolIsValid &= $this->valName();

				if( CApPaymentType::BILL_PAY_ACH == $this->getApPaymentTypeId() ) {
					$boolIsValid &= $this->valBillPayAchOrCheck();
				} else {
					$boolIsValid &= $this->valCheckNameOnAccount();
					$boolIsValid &= $this->valCheckRoutingNumber();
					$boolIsValid &= $this->valCheckAccountNumberEncrypted();
				}
				break;

			case 'insert_vendor_remittance':
				$boolIsValid &= $this->valCheckNameOnAccount();
				$boolIsValid &= $this->valCheckRoutingNumber();
				$boolIsValid &= $this->valCheckAccountNumberEncrypted();
				break;

			case 'insert_refund_method':
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valCheckNameOnAccount();
				$boolIsValid &= $this->valCheckRoutingNumber();
				$boolIsValid &= $this->valCheckAccountNumberEncrypted();
				$boolIsValid &= $this->valCheckAccountTypeId();
				break;

			case 'rp_insert_refund_method':
				$boolIsValid &= $this->valCheckNameOnAccount();
				$boolIsValid &= $this->valCheckRoutingNumber( $objDatabase );
				$boolIsValid &= $this->valConfirmCheckAccountNumber();
				$boolIsValid &= $this->valCheckAccountNumberEncrypted();
				$boolIsValid &= $this->valCheckAccountTypeId();
				break;

			case 'validate_expiration_date':
				$boolIsValid &= $this->valCcExpirationDate();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function verifyAccount( $objDatabase ) {

		$objGiactLibrary = new CGiactLibrary( CPsProduct::BILL_PAY, $this->getCid(), $objDatabase );

		$arrstrResponses = $objGiactLibrary->verifyAccount( $this->getCheckAccountNumberEncrypted(), $this->getCheckRoutingNumber(), $this->getCheckAccountTypeId() );

		if( false === $arrstrResponses ) {
			return false;
		}

		$this->setGiactVerificationResponse( $objGiactLibrary->getCombinedResponse() );

		if( true == $objGiactLibrary->isPass() ) {
			$this->setVerifiedOn( 'NOW()' );
			$this->setIsVerified( true );
			return true;
		}

		$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Account information has been ' . CStringService::singleton()->strtolower( $objGiactLibrary->getVerificationResponse() ) ) );
		$this->addErrorMsgs( $objGiactLibrary->getErrorMsgs() );
		$this->setIsVerified( false );

		return false;
	}

	public function addApPayeeRemittanceEvent( $arrmixEvent ) {
		$this->m_arrmixApPayeeRemittanceEvent = $arrmixEvent;
	}

}
?>
