<?php

class CSmartDeviceBrand extends CBaseSmartDeviceBrand {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSmartDeviceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMake() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valModel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>