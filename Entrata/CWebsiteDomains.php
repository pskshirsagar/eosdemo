<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CWebsiteDomains
 * Do not add any new functions to this class.
 */
// We can remove this file, but still reference from psi/caching so keeping it, looks like we are not using this file any more Ref Task :2531265
// https://codereview.entratadev.com/D85457
// Will coordinate with respective developer or file owner.
class CWebsiteDomains extends CBaseWebsiteDomains {

	public static function fetchAllWebsiteDomainsByWebsiteIdByCid( $intWebsiteId, $intCid, $objDatabase ) {
		return self::fetchWebsiteDomains( sprintf( 'SELECT * FROM %s WHERE website_id = %d AND cid = %d AND property_id IS NULL ORDER BY id', 'website_domains', ( int ) $intWebsiteId, ( int ) $intCid ), $objDatabase );
	}

}
?>