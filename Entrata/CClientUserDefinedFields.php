<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CClientUserDefinedFields
 * Do not add any new functions to this class.
 */

class CClientUserDefinedFields extends CBaseClientUserDefinedFields {

	public static function getClientUserDefinedFieldByFiledNameByUdfTableByCid( string $strFieldName, string $strUdfTable, $intCid, \CDatabase $objDatabase ) {
		$strSql = 'SELECT 
						cudf.*
					FROM
						client_user_defined_fields cudf
						JOIN udf_tables udft ON cudf.cid = udft.cid AND cudf.udf_table_id = udft.id
					WHERE 
						cudf.deleted_by IS NULL 
						AND udft.name = \'' . $strUdfTable . '\' AND cudf.field_name = \'' . $strFieldName . '\' AND udft.cid = ' . ( int ) $intCid;

		return self::fetchClientUserDefinedField( $strSql, $objDatabase );
	}

}
?>