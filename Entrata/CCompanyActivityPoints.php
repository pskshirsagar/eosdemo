<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyActivityPoints
 * Do not add any new functions to this class.
 */

class CCompanyActivityPoints extends CBaseCompanyActivityPoints {

	public static function fetchCompanyActivityPointByCidByDefaultActivityPointId( $intCid, $intDefaultActivityPointId, $objDatabase ) {
		$strSql = 'SELECT
						cap.*
					FROM
						company_activity_points cap
					WHERE
						cap.cid = ' . ( int ) $intCid . '
						AND cap.default_activity_point_id = ' . ( int ) $intDefaultActivityPointId;

		return self::fetchCompanyActivityPoint( $strSql, $objDatabase );
	}

	public static function fetchCachedDefaultActivityPointsByCid( $intCid, $objDatabase ) {
		$arrmixData = CCache::fetchObject( 'DefaultActivityPoints_' . $intCid );

		if( false === $arrmixData || true == is_null( $arrmixData ) ) {
			$strSql = 'SELECT
							cap.default_activity_point_id,
							cap.no_badge_points
						FROM
							company_activity_points cap
						WHERE
							cap.cid = ' . ( int ) $intCid . '
							AND is_published = 1';

			$arrmixData = rekeyArray( 'default_activity_point_id', ( array ) fetchData( $strSql, $objDatabase ) );

			CCache::storeObject( 'DefaultActivityPoints_' . $intCid, $arrmixData, $intSpecificLifetime = 3600, $arrstrTags = [] );
		}

		return $arrmixData;
	}

	public static function fetchEnabledCompanyActivityPointsByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cap.*
					FROM
						company_activity_points cap
						JOIN default_activity_points dap ON ( cap.default_activity_point_id = dap.id )
					WHERE
						cap.cid = ' . ( int ) $intCid . '
						AND dap.is_published != 0';

		return self::fetchCompanyActivityPoints( $strSql, $objDatabase );
	}

}
?>