<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyApPaymentTypeAssociations
 * Do not add any new functions to this class.
 */

class CCompanyApPaymentTypeAssociations extends CBaseCompanyApPaymentTypeAssociations {

	public static function fetchNotifiedCompanyApPaymentTypeAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyApPaymentTypeAssociations( sprintf( 'SELECT * FROM company_ap_payment_type_associations WHERE cid = %d AND is_notify = true', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNotifiedCompanyApPaymentTypeIdsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ap_payment_type_id
					FROM
						company_ap_payment_type_associations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_notify = true';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEnabledBillPayCompanyApPaymentTypeAssociationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT p.id as property_id,
						p.property_name
					FROM
						company_ap_payment_type_associations capta
						JOIN properties AS p ON ( p.cid = capta.cid )
					WHERE
						capta.ap_payment_type_id = ' . CApPaymentType::BILL_PAY_ACH . '
						AND capta.is_enabled = true
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND capta.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEnabledCompanyApPaymentTypeAssociationsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						company_ap_payment_type_associations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_enabled = true';

		return parent::fetchCompanyApPaymentTypeAssociations( $strSql, $objDatabase );
	}

}
?>