<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlTreeTypes
 * Do not add any new functions to this class.
 */

class CGlTreeTypes extends CBaseGlTreeTypes {

	public static function fetchGlTreeTypes( $strSql, $objClientDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CGlTreeType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchGlTreeType( $strSql, $objClientDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGlTreeType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllGlTreeTypes( $objClientDatabase, $boolIsPublished = false ) {
		$strSql = 'SELECT * FROM gl_tree_types ORDER BY order_num';

		if( false != $boolIsPublished ) {
			$strSql = 'SELECT * FROM gl_tree_types WHERE is_published = 1 ORDER BY id';
		}

		return self::fetchGlTreeTypes( $strSql, $objClientDatabase );
	}
}
?>