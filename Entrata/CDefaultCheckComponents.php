<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultCheckComponents
 * Do not add any new functions to this class.
 */

class CDefaultCheckComponents extends CBaseDefaultCheckComponents {

	public static function fetchDefaultCheckComponents( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDefaultCheckComponent', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchDefaultCheckComponent( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultCheckComponent', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}
}
?>