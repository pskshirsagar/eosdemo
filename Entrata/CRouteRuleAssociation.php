<?php

class CRouteRuleAssociation extends CBaseRouteRuleAssociation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRouteRuleTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRouteTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>