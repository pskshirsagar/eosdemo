<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerPaymentAccounts
 * Do not add any new functions to this class.
 */

class CCustomerPaymentAccounts extends CBaseCustomerPaymentAccounts {

	public static function fetchCustomerPaymentAccountByIdByCustomerIdByCid( $intId, $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerPaymentAccount( sprintf( 'SELECT * FROM %s WHERE id = %d AND customer_id = %d AND cid = %d AND deleted_by IS NULL', 'customer_payment_accounts', ( int ) $intId, ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOrderedCustomerPaymentAccountsByCustomerIdByPaymentTypeIdsByCid( $intCustomerId, $arrintPaymentTypeIds, $intCid, $objDatabase ) {
	    if( false == valArr( $arrintPaymentTypeIds ) ) return NULL;
	    $strSql = sprintf( 'SELECT * FROM %s WHERE payment_type_id IN ( %s ) AND customer_id = %d AND cid = %d AND deleted_by IS NULL ORDER BY payment_type_id', 'customer_payment_accounts', implode( ',', $arrintPaymentTypeIds ), ( int ) $intCustomerId, ( int ) $intCid );
		return self::fetchCustomerPaymentAccounts( $strSql, $objDatabase );
	}

	public static function fetchOrderedCustomerPaymentAccountsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = sprintf( 'SELECT * FROM %s WHERE customer_id = %d AND cid = %d AND deleted_by IS NULL ORDER BY payment_type_id', 'customer_payment_accounts', ( int ) $intCustomerId, ( int ) $intCid );
		return self::fetchCustomerPaymentAccounts( $strSql, $objDatabase );
	}

	public static function fetchConflictingCustomerPaymentAccountCountByCustomerPaymentAccountByCid( $objCustomerPaymentAccount, $objDatabase ) {
		$strWhereSql = ' WHERE
							id <> ' . ( int ) $objCustomerPaymentAccount->getId() . '
							AND cid = ' . ( int ) $objCustomerPaymentAccount->getCid() . '
							AND customer_id = ' . ( int ) $objCustomerPaymentAccount->getCustomerId() . '
							AND deleted_by IS NULL ';

		if( true == CPaymentTypes::isCreditCardPayment( $objCustomerPaymentAccount->getPaymentTypeId() ) ) {
			$strWhereSql .= ' AND cc_card_number_encrypted = \'' . trim( $objCustomerPaymentAccount->getCcCardNumberEncrypted() ) . '\'';
			$strWhereSql .= ' AND cc_exp_date_month = \'' . trim( $objCustomerPaymentAccount->getCcExpDateMonth() ) . '\'';
			$strWhereSql .= ' AND cc_exp_date_year = \'' . trim( $objCustomerPaymentAccount->getCcExpDateYear() ) . '\'';
		} else {
			$strWhereSql .= ' AND check_account_number_encrypted = \'' . trim( $objCustomerPaymentAccount->getCheckAccountNumberEncrypted() ) . '\'';
			$strWhereSql .= ' AND check_routing_number = \'' . trim( $objCustomerPaymentAccount->getCheckRoutingNumber() ) . '\'';
		}

		$strWhereSql .= ' LIMIT 1';
		return self::fetchCustomerPaymentAccountCount( $strWhereSql, $objDatabase );
	}

	public static function fetchConflictingPaymentBankAccountCountByCustomerPaymentAccountByCidByIban( $objCustomerPaymentAccount, $objPaymentDatabase ) {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $objCustomerPaymentAccount->getIbanToken() ) ) {
			return NULL;
		}
		$strWhereSql = 'SELECT * FROM payment_bank_accounts WHERE
							details->>\'iban_token\' =  \'' . ( string ) $objCustomerPaymentAccount->getIbanToken() . '\'
							AND cid = ' . ( int ) $objCustomerPaymentAccount->getCid() . '
							AND deleted_by IS NULL';

		return \Psi\Eos\Payment\CPaymentBankAccounts::createService()->fetchPaymentBankAccounts( $strWhereSql, $objPaymentDatabase );

	}

	public static function fetchConflictingCustomerPaymentAccountCountByPaymentBankAccountIdByCidByPropertyIdByCustomerId( $arrintPaymentBankAccountIds, $objCustomerPaymentAccount, $objDatabase ) {
		if( false == \Psi\Libraries\UtilFunctions\valArr( $arrintPaymentBankAccountIds ) ) {
			return NULL;
		}
		$strWhereSql = ' WHERE
							payment_bank_account_id IN ( ' . sqlStrImplode( $arrintPaymentBankAccountIds ) . ' )
							AND cid = ' . ( int ) $objCustomerPaymentAccount->getCid() . '
							AND customer_id = ' . ( int ) $objCustomerPaymentAccount->getCustomerId() . '
							AND deleted_by IS NULL ';

		$strWhereSql .= ' LIMIT 1';
		return self::fetchCustomerPaymentAccountCount( $strWhereSql, $objDatabase );
	}

	public static function fetchElectronicCustomerPaymentAccountsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchCustomerPaymentAccounts( sprintf( 'SELECT * FROM customer_payment_accounts WHERE applicant_id = %d AND cid = %d AND payment_type_id <> %d AND deleted_by IS NULL', ( int ) $intApplicantId, ( int ) $intCid, CPaymentType::CHECK ), $objDatabase );
    }

	public static function fetchCustomerPaymentAccountsByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						customer_payment_accounts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )';

		return self::fetchCustomerPaymentAccounts( $strSql, $objDatabase );
	}

	public static function fetchSimpleCustomerPaymentAccountByIdByCid( $intId, $intCid, $objDatabase, $arrstrFieldsToFetch = [ '*' ], $boolIsFetchSimpleData = false ) {

		$strSql = 'SELECT
						' . implode( ',', $arrstrFieldsToFetch ) . '
					FROM
						customer_payment_accounts
					WHERE
						id = ' . ( int ) $intId . '
						AND cid = ' . ( int ) $intCid;

		if( true == $boolIsFetchSimpleData ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchCustomerPaymentAccount( $strSql, $objDatabase );
		}
	}

	public static function fetchCustomerPaymentAccountsByCustomerIdsByCidOrderByLastPaymentOn( $arrintCustomerIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						customer_payment_accounts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND last_used_on IS NOT NULL
					ORDER BY last_used_on DESC LIMIT 1';

		$arrmixData = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrmixData ) && true == valId( $arrmixData[0]['id'] ) ) {
			return $arrmixData[0]['id'];
		}
		return;
	}

	public static function fetchDuplicateCustomerPaymentAccountId( $objCustomerPaymentAccount, $objDatabase ) {
		$strWhereSql = ' WHERE
							id <> ' . ( int ) $objCustomerPaymentAccount->getId() . '
							AND cid = ' . ( int ) $objCustomerPaymentAccount->getCid() . '
							AND customer_id = ' . ( int ) $objCustomerPaymentAccount->getCustomerId() . '
							AND deleted_by IS NULL ';

		if( true == CPaymentTypes::isCreditCardPayment( $objCustomerPaymentAccount->getPaymentTypeId() ) ) {
			$strWhereSql .= ' AND cc_card_number_encrypted = \'' . trim( $objCustomerPaymentAccount->getCcCardNumberEncrypted() ) . '\'';
			$strWhereSql .= ' AND cc_exp_date_month = \'' . trim( $objCustomerPaymentAccount->getCcExpDateMonth() ) . '\'';
			$strWhereSql .= ' AND cc_exp_date_year = \'' . trim( $objCustomerPaymentAccount->getCcExpDateYear() ) . '\'';
		} else {
			$strWhereSql .= ' AND check_account_number_encrypted = \'' . trim( $objCustomerPaymentAccount->getCheckAccountNumberEncrypted() ) . '\'';
			$strWhereSql .= ' AND check_routing_number = \'' . trim( $objCustomerPaymentAccount->getCheckRoutingNumber() ) . '\'';
		}

		$strWhereSql .= ' LIMIT 1';

		$strSql = 'SELECT 
						id 
				   FROM 
				        customer_payment_accounts' . $strWhereSql;

		return self::fetchCustomerPaymentAccounts( $strSql, $objDatabase, false );
	}

	public static function fetchCustomerPaymentAccountsByCidByCustomerId( $intCid, $intCustomerId, $objDatabase ) {
		$strSql = 'SELECT
						c.id as customer_id,
						c.name_first,
						c.name_last,
						c.tax_number_masked,
						c.tax_number_encrypted,
						c.email_address,
						c.birth_date,
						cpa.*
					FROM
						customer_payment_accounts cpa
						JOIN customers c ON (cpa.cid = c.cid AND cpa.customer_id = c.id)
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND c.id = ' . ( int ) $intCustomerId;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomCustomerPaymentAccountNicknamesByIdsByCid( $arrintCustomerPaymentAccountIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerPaymentAccountIds ) ) {
			return false;
		}

		$strSql = 'SELECT
					 id,
					 account_nickname
				FROM
					customer_payment_accounts
				WHERE
					id IN (' . implode( ',', $arrintCustomerPaymentAccountIds ) . ')
					AND cid = ' . ( int ) $intCid . ' AND deleted_on IS NULL AND deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomCustomerPaymentAccountsByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM
						customer_payment_accounts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )  AND deleted_on IS NULL  AND deleted_by IS NULL';

		return self::fetchCustomerPaymentAccounts( $strSql, $objDatabase );
	}

	public static function fetchActiveCustomerPaymentAccountByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerPaymentAccount( sprintf( 'SELECT * FROM customer_payment_accounts WHERE id = %d AND cid = %d AND deleted_on IS NULL', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDebitACHCustomerPaymentAccountsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase, $boolAllowedACHPaymentMethod = true, $boolAllowedDebitPaymentMethod = true ) {
		if( false == valId( $intCustomerId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						customer_payment_accounts
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND cid = ' . ( int ) $intCid . '
						AND 
						( ( 1 = ' . ( int ) $boolAllowedACHPaymentMethod . ' AND payment_type_id = ' . CPaymentType::ACH . ' )
							OR ( 1 = ' . ( int ) $boolAllowedDebitPaymentMethod . ' AND ( payment_type_id IN( ' . implode( ',', CPaymentType::$c_arrintCreditCardPaymentTypes ) . ' ) AND is_debit_card = true ) ) )
						AND deleted_by IS NULL
					ORDER BY payment_type_id DESC';

		return self::fetchCustomerPaymentAccounts( $strSql, $objDatabase );
	}

	public static function fetchCustomerPaymentAccountDebitCardCountByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase, $boolAllowedDebitPaymentMethod = true ) {
		if( false == valId( $intCustomerId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						COUNT(*) AS count
					FROM
						customer_payment_accounts
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND 1 = ' . ( int ) $boolAllowedDebitPaymentMethod . '
						AND payment_type_id IN( ' . implode( ',', CPaymentType::$c_arrintCreditCardPaymentTypes ) . ' )
						AND is_debit_card = true
						AND deleted_by IS NULL';

		$arrmixCount = fetchData( $strSql, $objDatabase );

		return $arrmixCount[0]['count'];
	}

}
?>