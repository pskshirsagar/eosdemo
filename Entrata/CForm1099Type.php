<?php

class CForm1099Type extends CBaseForm1099Type {

	const FORM_1099_TYPE_1		= 1;
	const FORM_1099_TYPE_2		= 2;
	const FORM_1099_TYPE_3		= 3;
	const FORM_1099_TYPE_4 		= 4;
	const FORM_1099_TYPE_5		= 5;
	const FORM_1099_TYPE_6		= 6;
	const FORM_1099_TYPE_7		= 7;
	const FORM_1099_TYPE_8		= 8;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>