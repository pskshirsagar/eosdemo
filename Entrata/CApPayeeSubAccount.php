<?php

class CApPayeeSubAccount extends CBaseApPayeeSubAccount {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApPayeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApPayeeLocationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApPayeeAccountId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valGlAccountId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAccountNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>