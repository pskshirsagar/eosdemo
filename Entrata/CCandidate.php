<?php

class CCandidate extends CBaseCandidate {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCareerApplicationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCareerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDocumentId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCandidateStatusTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyEmployeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCareerSourceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSourceDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNamePrefix() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNameFirst() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNameMiddle() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNameLast() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNameSuffix() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPhoneNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valWorkNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMobileNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFaxNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valStreetLine1() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valStreetLine2() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCity() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valStateCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostalCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTaxNumberEncrypted() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valJobShiftType() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNotes() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSubmittedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSubmittedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApprovedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApprovedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRejectedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRejectedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// No Default case.
            	break;
        }

        return $boolIsValid;
    }
}
?>