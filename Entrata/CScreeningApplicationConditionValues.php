<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningApplicationConditionValues
 * Do not add any new functions to this class.
 */

class CScreeningApplicationConditionValues extends CBaseScreeningApplicationConditionValues {

	public static function fetchSatisfiedScreeningApplicationConditionValuesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						sacv.condition_subset_value_id
					FROM
						screening_application_condition_values sacv
						JOIN screening_application_condition_sets sacs ON( sacs.id = sacv.screening_application_condition_set_id AND sacs.cid = sacv.cid )
					WHERE
						sacs.application_id = ' . ( int ) $intApplicationId . '
						AND sacs.is_active = 1
						AND sacv.is_reverted = 0
						AND sacv.satisfied_on is NOT NULL
						AND sacs.cid = ' . ( int ) $intCid;

		$arrintResult = fetchData( $strSql, $objDatabase );

		return $arrintResult;
	}

	public static function fetchScreeningApplicationConditionValuesByCidByScreeningApplicationConditionSetIdByConditionTypeIds( $intCid, $intApplicationId, $arrintConditionTypeIds, $objDatabase, $boolFetchAllRecord = false ) {

		if( false == valArr( $arrintConditionTypeIds ) ) return NULL;
		$strConditionLimit = ( true == $boolFetchAllRecord ) ? '' : ' LIMIT 1  ';

		$strSql = 'SELECT
						sacv.*
					FROM
						screening_application_condition_values sacv
						JOIN screening_application_condition_sets sacs ON( sacs.id = sacv.screening_application_condition_set_id AND sacs.cid = sacv.cid )
					WHERE
						sacs.application_id = ' . ( int ) $intApplicationId . '
						AND sacs.cid = ' . ( int ) $intCid . '
						AND sacs.is_active = 1
						AND sacv.condition_type_id IN( ' . implode( ',', $arrintConditionTypeIds ) . ' )
					ORDER BY
						sacv.id DESC
		                ' . $strConditionLimit;

		return parent::fetchScreeningApplicationConditionValues( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValuesBySubsetValueIdsByApplicationIdByCid( $arrintSubsetValueIds, $intApplicationId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintSubsetValueIds ) ) return NULL;

		$strSql = '	SELECT
					    sacv.*
					FROM
					    screening_application_condition_sets sacs
					    JOIN screening_application_condition_values  sacv
					    ON sacs.id = sacv.screening_application_condition_set_id
					    AND sacs.cid = sacv.cid
					WHERE sacs.application_id = ' . ( int ) $intApplicationId . '
					AND sacs.cid = ' . ( int ) $intCid . '
					AND sacs.is_active = 1
					AND sacv.is_reverted = 0
					AND sacv.condition_subset_value_id IN( ' . implode( ',', $arrintSubsetValueIds ) . ' )';

		return parent::fetchScreeningApplicationConditionValues( $strSql, $objDatabase );

	}

	public static function updateProcessAutomatically( $arrintManuallyProcessedConditionSubsetValueIds, $arrintAutomaticProcessedConditionSubsetValueIds, $intCid, $objDatabase ) {
		if( false == $arrintManuallyProcessedConditionSubsetValueIds && false == $arrintAutomaticProcessedConditionSubsetValueIds ) return false;

		if( true == $arrintManuallyProcessedConditionSubsetValueIds ) {
			$strSql = 'UPDATE screening_application_condition_values SET process_automatically=false WHERE is_reverted = 0 AND condition_subset_value_id IN( ' . implode( ',', $arrintManuallyProcessedConditionSubsetValueIds ) . ') AND cid = ' . ( int ) $intCid . ';';
		}
		if( true == $arrintAutomaticProcessedConditionSubsetValueIds ) {
			$strSql .= 'UPDATE screening_application_condition_values SET process_automatically=true WHERE is_reverted = 0 AND condition_subset_value_id IN( ' . implode( ',', $arrintAutomaticProcessedConditionSubsetValueIds ) . ') AND cid = ' . ( int ) $intCid;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicationConditionNamesByCidByApplicationIds( $intCid, $arrintApplicationIds, $objDatabase ) {

		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						sacv.condition_name,
						sacs.application_id
					FROM
						screening_application_condition_values sacv
						LEFT JOIN screening_application_condition_sets sacs ON( sacs.id = sacv.screening_application_condition_set_id AND sacs.cid = sacv.cid )
					WHERE
						sacs.cid = ' . ( int ) $intCid . '
						AND sacs.is_active = 1
						AND sacs.application_id IN( ' . implode( ',', $arrintApplicationIds ) . ' )
						';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicationConditionNamesBySubsetValueIdsOrSatisfiedOnlyByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $arrintSubsetValueIds = [], $boolIsSatiesfiedOnly = false ) {

		$strSatisfiedCondition = ( true == $boolIsSatiesfiedOnly ) ? ' AND sacv.satisfied_on is NOT NULL ' : '';
		$strConditionSubsteValueIdsCondition = ( true == valArr( $arrintSubsetValueIds ) ) ? ' AND sacv.condition_subset_value_id IN( ' . implode( ',', $arrintSubsetValueIds ) . ' ) ' : '';

		$strSql = '	SELECT
					    array_to_string ( array_agg ( DISTINCT(sacv.condition_name) ), \', \' ) AS condition_names
					FROM
					    screening_application_condition_sets sacs
					    JOIN screening_application_condition_values  sacv
					    ON sacs.id = sacv.screening_application_condition_set_id
					    AND sacs.cid = sacv.cid
					WHERE sacs.application_id = ' . ( int ) $intApplicationId . '
					AND sacs.cid = ' . ( int ) $intCid . '
					AND sacs.is_active = 1
					AND sacv.is_reverted = 0
					' . $strSatisfiedCondition . $strConditionSubsteValueIdsCondition;

		return self::fetchColumn( $strSql, 'condition_names', $objDatabase );
	}

	public static function revertScreeningApplicationConditionValuesByConditionSetIdsByConditionTypeIdByCid( $arrintScreeningApplicationSetIds, $intCustomerTypeId, $intCid, $intCurrentUserId, $objDatabase ) {

		$strSql = 'UPDATE
                    screening_application_condition_values
                   SET
                    is_reverted = 0,
                    satisfied_by = NULL,
                    satisfied_on = NULL,
                    updated_by = ' . ( int ) $intCurrentUserId . ',
                    updated_on = NOW()
                   WHERE
                    screening_application_condition_set_id IN ( ' . implode( ',', $arrintScreeningApplicationSetIds ) . ' )
                    AND condition_type_id = ' . ( int ) $intCustomerTypeId . ' 
                    AND cid =  ' . ( int ) $intCid;

		if( false == fetchData( $strSql, $objDatabase ) ) return false;

		return true;
	}

}
?>