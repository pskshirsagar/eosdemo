<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueArCodeTypes
 * Do not add any new functions to this class.
 */

class CRevenueArCodeTypes extends CBaseRevenueArCodeTypes {

    public static function fetchRevenueArCodeTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CRevenueArCodeType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchRevenueArCodeType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CRevenueArCodeType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }
}
?>