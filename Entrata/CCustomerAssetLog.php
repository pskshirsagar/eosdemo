<?php

class CCustomerAssetLog extends CBaseCustomerAssetLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerAssetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerAssetTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameOnAccount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountNumberEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRoutingNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStreetLine1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStreetLine2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStreetLine3() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionProvince() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactFaxNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactEmail() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisposalAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCostToDispose() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCashValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInterestRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActualIncome() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDateEffective() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDateDivested() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>