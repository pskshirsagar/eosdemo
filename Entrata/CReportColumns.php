<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportColumns
 * Do not add any new functions to this class.
 */

class CReportColumns extends CBaseReportColumns {

	public static function fetchReportColumnsByReportDatasetIdWithOrderBy( $intReportDatasetId, $strOrderByType, $objDatabase ) {

		$strSql = 'SELECT 
						id,
						name,
						report_dataset_id,
						column_key,
						description,
						updated_on::DATE
					FROM 
						report_columns 
					WHERE 
						report_dataset_id =' . ( int ) $intReportDatasetId . '
						AND deleted_by IS NULL
					ORDER BY
						' . $strOrderByType;
		return self::fetchReportColumns( $strSql, $objDatabase );
	}

	public static function fetchMaxOrderNumberForReportDatasetColumn( $intReportDatasetId, $objDatabase ) {
		$strSql = 'SELECT 
					MAX( order_num ) + 1 AS id
				FROM 
					report_columns
				WHERE 
					report_dataset_id = ' . ( int ) $intReportDatasetId;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportDatasetColumnMaxId( $objDatabase ) {
		$strSql = '
			SELECT
				MAX( id ) + 1 AS id
			FROM
				report_columns';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportColumnsByIds( $arrintReportColumnIds, $objDatabase ) {
		if( !valArr( $arrintReportColumnIds ) ) return [];

		$strSql = 'SELECT 
						*
					FROM
						report_columns
					WHERE 
						id IN ( ' . implode( ',', $arrintReportColumnIds ) . ' )';

		return self::fetchReportColumns( $strSql, $objDatabase );
	}

	public static function fetchReportColumnsByColumnCount( CDatabase $objDatabase, bool $boolCompanyReport = false ) {
		$strWhereCondition = $strJoinCondition = '';
		if( $boolCompanyReport ) {
			$strJoinCondition = ' JOIN report_versions rv ON rv.default_report_version_id = drv.id JOIN report_new_instances rni ON rni.report_version_id = rv.id ';
			$strWhereCondition = ' WHERE rni.deleted_by is NULL ';
		}

		$strSql = '
			SELECT
				DISTINCT 
				column_list.column_name AS item_name,
				column_list.column_key AS item_key,
				column_list.column_count
			FROM
				(
				SELECT
					DISTINCT rc.column_key,
					MAX( util_get_translated( \'name\', rc.name, rc.details ) ) OVER ( PARTITION BY rc.column_key ) AS column_name,
					count( drv.default_report_id ) OVER ( PARTITION BY rc.column_key ) AS column_count
				FROM
					report_columns rc
					JOIN report_datasets rd ON rd.id = rc.report_dataset_id
					JOIN default_report_versions drv ON drv.id = rd.default_report_version_id ' . $strJoinCondition . $strWhereCondition . '
				) AS column_list
			ORDER BY
				column_count DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportColumnsByReportDatasetIds( $arrintReportDatasetIds, $objDatabase ) {
		if( !valArr( $arrintReportDatasetIds ) ) return [];

		$strSql = 'SELECT 
						*
					FROM
						report_columns
					WHERE 
						report_dataset_id IN ( ' . sqlIntImplode( $arrintReportDatasetIds ) . ' )';

		return self::fetchReportColumns( $strSql, $objDatabase );
	}

}
?>