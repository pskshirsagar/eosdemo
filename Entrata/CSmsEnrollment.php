<?php

class CSmsEnrollment extends CBaseSmsEnrollment {

	// Used while sending out initial enrollment confirmations in CSendSmsEnrollmentNotificationsScript
	protected $m_intPropertyId;

    public function setValues( $arrintValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrintValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrintValues['property_id'] ) ) $this->setPropertyId( $arrintValues['property_id'] );

        return;
    }

    public function setPropertyId( $intPropertyId ) {
    	$this->m_intPropertyId = $intPropertyId;
    }

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

    public function fetchProperty( $objDatabase ) {
    	return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
    }

    public function valMessageTypeId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getMessageTypeId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_type_id', '' ));
        // }

        return $boolIsValid;
    }

    public function validateSmsEnrollmentInsert( $objDatabase ) {
    	$objDuplicateSmsEnrollment = \Psi\Eos\Entrata\CSmsEnrollments::createService()->fetchSmsEnrollmentByCidByCustomerIdByMessageTypeId( $this->getCid(), $this->getCustomerId(), $this->getMessageTypeId(), $objDatabase );

    	if( true == valObj( $objDuplicateSmsEnrollment, 'CSmsEnrollment' ) ) return false;

    	return true;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
				$boolIsValid &= $this->validateSmsEnrollmentInsert( $objDatabase );
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>