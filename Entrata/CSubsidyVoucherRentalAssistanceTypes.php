<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyVoucherRentalAssistanceTypes
 * Do not add any new functions to this class.
 */

class CSubsidyVoucherRentalAssistanceTypes extends CBaseSubsidyVoucherRentalAssistanceTypes {

	public static function fetchPublishedSubsidyVoucherRentalAssistanceTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM subsidy_voucher_rental_assistance_types WHERE is_published = TRUE ORDER BY order_num';
		return self::fetchSubsidyVoucherRentalAssistanceTypes( $strSql, $objDatabase );
	}

}
?>