<?php

class CCandidateQuestion extends CBaseCandidateQuestion {

    public function valId() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCandidateId() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCandidateId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'candidate_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsLegallyAuthorized() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getIsLegallyAuthorized() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_legally_authorized', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHasConviction() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getHasConviction() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_conviction', '' ) );
        // }

        return $boolIsValid;
    }

    public function valConvictionDescription() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getConvictionDescription() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'conviction_description', '' ) );
        // }

        return $boolIsValid;
    }

    public function valWordsPerMinute() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getWordsPerMinute() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'words_per_minute', '' ) );
        // }

        return $boolIsValid;
    }

    public function valKnowsTenKey() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getKnowsTenKey() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'knows_ten_key', '' ) );
        // }

        return $boolIsValid;
    }

    public function valComputerSkills() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getComputerSkills() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'computer_skills', '' ) );
        // }

        return $boolIsValid;
    }

    public function valOtherSpecialSkills() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getOtherSpecialSkills() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'other_special_skills', '' ) );
        // }

        return $boolIsValid;
    }

    public function valMinimumSalary() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getMinimumSalary() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_salary', '' ) );
        // }

        return $boolIsValid;
    }

    public function valMinimumWeeklyHours() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getMinimumWeeklyHours() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_weekly_hours', '' ) );
        // }

        return $boolIsValid;
    }

    public function valMaximumWeeklyHours() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getMaximumWeeklyHours() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maximum_weekly_hours', '' ) );
        // }

        return $boolIsValid;
    }

    public function valAge() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getAge() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'age', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHasMilitaryExperience() {
        $boolIsValid = true;

        	/**
        	 * Validation example
        	 */

        // if( true == is_null( $this->getHasMilitaryExperience() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_military_experience', '' ) );
        // }

        return $boolIsValid;
    }

    public function valMilitaryBranch() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getMilitaryBranch() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'military_branch', '' ) );
        // }

        return $boolIsValid;
    }

    public function valMilitaryTraining() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getMilitaryTraining() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'military_training', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHasGed() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getHasGed() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_ged', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHasHsDiploma() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getHasHsDiploma() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_hs_diploma', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getUpdatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getUpdatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCreatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCreatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>