<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningNotificationTypes
 * Do not add any new functions to this class.
 */

class CScreeningNotificationTypes extends CBaseScreeningNotificationTypes {

	public static function fetchScreeningNotificationTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningNotificationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningNotificationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningNotificationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllPublishedScreeningNotificationTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_notification_types
					WHERE 
						is_published = TRUE';
		return  parent::fetchScreeningNotificationTypes( $strSql, $objDatabase );
	}

}
?>