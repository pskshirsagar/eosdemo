<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CNotificationCompanyGroups
 * Do not add any new functions to this class.
 */

class CNotificationCompanyGroups extends CBaseNotificationCompanyGroups {

    public static function fetchNotificationCompanyGroupDetailsByNotificationIdByCid( $intNotificationId, $intCid, $objDatabase ) {

    	$strSql = 'SELECT
    					cg.id,
    					util_get_translated( \'name\', cg.name, cg.details ) AS name
    				FROM
    					notification_company_groups ncg
    					JOIN company_groups cg ON ( ncg.cid = cg.cid AND cg.id = ncg.company_group_id OR ncg.company_group_id IS NULL)
    				WHERE
						ncg.cid = ' . ( int ) $intCid . ' AND ncg.notification_id = ' . ( int ) $intNotificationId . '
    				ORDER BY
						lower ( name )';

    	$arrstrData = fetchData( $strSql, $objDatabase );

    	$arrstrFinalData = [];

    	if( true == valArr( $arrstrData ) ) {
    		foreach( $arrstrData as $arrstrChunk ) {

    			$arrstrFinalData[] = [ 'id' => $arrstrChunk['id'], 'name' => $arrstrChunk['name'] ];
    		}
    	}

    	return $arrstrFinalData;
    }

}
?>