<?php

class CPropertyOnSiteContact extends CBasePropertyOnSiteContact {

    /**
     * Validation Functions
     */

	public function setNameFull( $strNameFull ) {
		parent::setNameFull( $strNameFull );
		parent::setOnSiteContactTypeId( CContactType::MANAGER );
	}

    public function valId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intId ) || ( 1 > $this->m_intId ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'On site contact id does not appear valid.' ) );
        }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'On site contact client does not appear valid.' ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intPropertyId ) || ( 1 > $this->m_intPropertyId ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'On site contact property id does not appear valid.' ) );
        }

        return $boolIsValid;
    }

    public function valOnSiteContactTypeId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intOnSiteContactTypeId ) || ( 1 > $this->m_intOnSiteContactTypeId ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'on_site_contact_type_id', 'Contact type does not appear to be valid.' ) );
        }

        return $boolIsValid;
    }

    public function valMarketingName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNameFirst() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNameLast() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNameFull() {
        $boolIsValid = true;

        if( false == isset( $this->m_strNameFull ) || ( 3 > strlen( $this->m_strNameFull ) ) ) {
            $boolIsValid = false;
			// we change the 'name_full' field to 'name_last' as name_full is not a displayed field
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'The manager full name must be at least 3 characters.' ) );
        }

        return $boolIsValid;
    }

    public function valTitle() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
            case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valOnSiteContactTypeId();
                break;

            case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
                break;

            default:
               	$boolIsValid = false;
               	break;
        }

        return $boolIsValid;
    }

}
?>