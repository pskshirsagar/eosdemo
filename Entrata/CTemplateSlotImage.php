<?php
class CTemplateSlotImage extends CBaseTemplateSlotImage {
	protected $m_arrobjCompanyMediaFiles;

	public function getCompanyMediaFiles() {
		return $this->m_arrobjCompanyMediaFiles;
	}

	public function addCompanyMediaFile( $objCompanyMediaFile ) {
		$this->m_arrobjCompanyMediaFiles[] = $objCompanyMediaFile;
	}

	public function valSlotImageLink() {
		if( false == is_null( $this->getLink() ) && false == CValidation::checkUrl( $this->getLink(), false, true ) ) {
			return false;
		}

		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valSlotImageLink();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

}
?>