<?php

class CCachedArCodePeriod extends CBaseCachedArCodePeriod {

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlAccountTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArOriginId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArTriggerTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArTriggerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDebitGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCreditGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDebitGlAccountName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCreditGlAccountName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDebitGlAccountNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCreditGlAccountNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWriteOffAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOutOfPeriodAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdjustedAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalCreditAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWriteOffAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOutOfPeriodAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdjustedAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalCurrentCreditAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalCreditAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalCurrentAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCurrentPaymentAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrePaymentAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentInKindAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRentAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOtherIncomeAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExpenseAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssetAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEquityAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOtherChargeAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDepositAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDepositCreditAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRefundAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOtherLiabilityAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReceivablesAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostToCashRentAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostToCashOtherIncomeAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostToCashExpenseAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostToCashAssetAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostToCashEquityAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostToCashOtherChargeAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalCashBasisAllocations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledChargeTotal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledChargeProratedTotal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllocationCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledChargeCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIncludeWithBaseRates() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostToCash() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPaymentInKind() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDepositCredit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>