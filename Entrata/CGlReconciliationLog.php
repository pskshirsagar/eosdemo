<?php

class CGlReconciliationLog extends CBaseGlReconciliationLog {

	const ACTION_PAUSED		= 'Paused';
	const ACTION_EDITED		= 'Edited';
	const ACTION_CREATED	= 'Created';
	const ACTION_COMPLETED	= 'Completed';

	public static function loadGlReconciliationLogActions() {
		$arrstrGlReconciliationLogActions = [];

		$arrstrGlReconciliationLogActions[self::ACTION_CREATED]	= self::ACTION_CREATED;
		$arrstrGlReconciliationLogActions[self::ACTION_EDITED]	= self::ACTION_EDITED;
		$arrstrGlReconciliationLogActions[self::ACTION_PAUSED]	= self::ACTION_PAUSED;
		$arrstrGlReconciliationLogActions[self::ACTION_COMPLETED]	= self::ACTION_COMPLETED;

		return $arrstrGlReconciliationLogActions;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlReconciliationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAction() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBeginningDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStatementDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBeginningAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndingAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>