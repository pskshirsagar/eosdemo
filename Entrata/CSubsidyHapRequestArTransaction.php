<?php

class CSubsidyHapRequestArTransaction extends CBaseSubsidyHapRequestArTransaction {

	protected $m_fltRequestedAmount;
	protected $m_intRepaymentArTransactionId;

	/**
	 * Setters
	 *
	 */

	public function setRequestedAmount( $fltRequestedAmount ) {
		$this->m_fltRequestedAmount = $fltRequestedAmount;
	}

	public function setRepaymentArTransactionId( $intRepaymentArTransactionId ) {
		$this->m_intRepaymentArTransactionId = $intRepaymentArTransactionId;
	}

	/**
	 * Getters
	 *
	 */

	public function getRequestedAmount() {
		return $this->m_fltRequestedAmount;
	}

	public function getRepaymentArTransactionId() {
		return $this->m_intRepaymentArTransactionId;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyHapRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyCertificationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAdjustment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['request_amount'] ) ) $this->setRequestedAmount( $arrmixValues['request_amount'] );
		if( true == isset( $arrmixValues['repayment_ar_transaction_id'] ) ) $this->setRepaymentArTransactionId( $arrmixValues['repayment_ar_transaction_id'] );

	}

}
?>