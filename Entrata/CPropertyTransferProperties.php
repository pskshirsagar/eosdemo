<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyTransferProperties
 * Do not add any new functions to this class.
 */

class CPropertyTransferProperties extends CBasePropertyTransferProperties {

	public static function fetchPropertyTransferPropertiesByPropertyTransferIdByCid( $intPropertyTransferId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM property_transfer_properties WHERE cid = ' . ( int ) $intCid . ' AND property_transfer_id = ' . ( int ) $intPropertyTransferId . ';';
		return self::fetchPropertyTransferProperties( $strSql, $objDatabase );
	}

	public static function fetchActivePropertyTransferPropertiesByPropertyTransferIdByCid( $intPropertyTransferId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM property_transfer_properties WHERE cid = ' . ( int ) $intCid . ' AND property_transfer_id = ' . ( int ) $intPropertyTransferId . ' AND approved_on IS NOT NULL AND transfered_on IS NULL;';
		return self::fetchPropertyTransferProperties( $strSql, $objDatabase );
	}
}
?>
