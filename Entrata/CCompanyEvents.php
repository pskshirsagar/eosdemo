<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEvents
 * Do not add any new functions to this class.
 */

class CCompanyEvents extends CBaseCompanyEvents {

	public static function fetchCompanyEventByIdByPropertyIdByCid( $intCompanyEventId, $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCompanyEvent( sprintf( 'SELECT ce.* FROM %s AS ce , %s AS pe WHERE pe.company_event_id = ce.id AND pe.cid = ce.cid AND ce.cid = %d AND pe.property_id = %d AND ce.id = %d ORDER BY ce.date_start', 'company_events', 'property_events', ( int ) $intCid, ( int ) $intPropertyId, ( int ) $intCompanyEventId ), $objDatabase );
	}

	public static function fetchCompanyEventByIdByPropertyIdByCustomerIdByCid( $intCompanyEventId, $intPropertyId, $arrintPropertyIds, $intCustomerId, $intCid, $objDatabase ) {
    	$strSql = 'SELECT
	    				ce.*,
	    				pe.property_id,
	    				p.property_name,
						CASE
							WHEN 1 = ( SELECT COUNT(id) FROM customer_events WHERE company_event_id = ce.id AND cid = ce.cid AND cid = ' . ( int ) $intCid . ' AND customer_id = ' . ( int ) $intCustomerId . ' )
							THEN 1
							ELSE 0
						END AS is_attending_event,
						CASE
							WHEN 1 = ( SELECT COUNT(id) FROM customer_events WHERE company_event_id = ce.id AND cid = ce.cid AND cid = ' . ( int ) $intCid . ' AND customer_id = ' . ( int ) $intCustomerId . ' )
							THEN ( SELECT id FROM customer_events WHERE company_event_id = ce.id AND cid = ce.cid AND cid = ' . ( int ) $intCid . ' AND customer_id = ' . ( int ) $intCustomerId . ' )
							ELSE NULL
						END AS customer_event_id,
						CASE
							WHEN ce.customer_id IS NULL
							THEN
								COUNT( attndcnt.custid ) OVER ( PARTITION BY ce.id )
							ELSE
								COUNT( attndcnt.custid ) OVER ( PARTITION BY ce.id ) + 1
	    				END AS attendees_count
					FROM
	                    company_events AS ce
						JOIN property_events AS pe ON ( pe.company_event_id = ce.id AND pe.cid = ce.cid )
	                    JOIN properties AS p ON ( p.id = pe.property_id AND p.cid = pe.cid )
	                    LEFT JOIN (
						             SELECT
	                             		 DISTINCT ON ( lc.customer_id ) lc.customer_id as custid,
						                 compevnt.id as company_event_id,
						                 compevnt.cid
						             FROM
						                 company_events AS compevnt
						                 JOIN customer_events AS custevnt ON ( custevnt.company_event_id = compevnt.id AND custevnt.cid = compevnt.cid )
						                 JOIN lease_customers AS lc ON ( lc.customer_id = custevnt.customer_id AND lc.cid = custevnt.cid )
	          							 JOIN leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid )
						             WHERE
						                 compevnt.id = ' . ( int ) $intCompanyEventId . '
						                 AND l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						                 AND compevnt.cid = ' . ( int ) $intCid . '

						         ) AS attndcnt ON  ( attndcnt.company_event_id = ce.id AND attndcnt.cid = ce.cid )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND pe.property_id = ' . ( int ) $intPropertyId . '
						AND ce.id = ' . ( int ) $intCompanyEventId . '
					ORDER BY
						ce.date_start';

		return self::fetchCompanyEvent( $strSql, $objDatabase );
   	}

	public static function fetchCompanyEventsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ce.*,
						CASE
						  	WHEN ce.customer_id IS NULL
						  	THEN ( SELECT SUM( er.counts ) FROM ( SELECT COALESCE( COUNT( cste.id ), 0 ) as counts FROM customer_events cste WHERE cste.company_event_id = ce.id AND cste.cid = ce.cid AND cste.cid = ' . ( int ) $intCid . ' UNION ALL SELECT COALESCE( COUNT( cer.id ), 0 ) as counts FROM company_event_responses cer JOIN property_events pe ON ( cer.property_event_id = pe.id AND pe.company_event_id = ce.id AND cer.cid = pe.cid ) WHERE cer.cid = ' . ( int ) $intCid . ' ) as er )
						  	ELSE ( SELECT SUM( er.counts ) FROM ( SELECT COALESCE( COUNT( cste.id ), 0 ) as counts FROM customer_events cste WHERE cste.company_event_id = ce.id AND cste.cid = ce.cid AND cste.cid = ' . ( int ) $intCid . ' UNION ALL SELECT COALESCE( COUNT( cer.id ), 0 ) as counts FROM company_event_responses cer JOIN property_events pe ON ( cer.property_event_id = pe.id AND pe.company_event_id = ce.id AND cer.cid = pe.cid ) WHERE cer.cid = ' . ( int ) $intCid . ' ) as er ) + 1
						END AS attendees_count
					FROM
						company_events AS ce
						JOIN property_events AS pe ON ( pe.company_event_id = ce.id AND ce.cid = pe.cid )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND pe.property_id = ' . ( int ) $intPropertyId . '
					ORDER BY
						ce.created_on DESC,
						pe.order_num';

		return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchCompanyEventsCountByPropertyIdsByCid( $intPropertyId, $intCid, $strEventsSearchFilter, $objDatabase ) {
		if( true == valArr( $strEventsSearchFilter ) ) {

			if( false == is_null( $strEventsSearchFilter['name'] ) ) {
				$strEventName = $strEventsSearchFilter['name'];
				$strAndWhere = 'AND ce.name ILIKE \'%' . trim( addslashes( $strEventName ) ) . '%\'';
			}

			if( 'Future' == $strEventsSearchFilter['status'] ) {
				$strAndWhere .= ' AND ( ce.date_start >= now()::date OR ce.date_end >= now()::date )';
			}

			if( 'Past' == $strEventsSearchFilter['status'] ) {
				$strAndWhere .= ' AND ce.date_end < now()::date';
			}

		} else {
			$strAndWhere = ( false == is_null( $strEventsSearchFilter ) ) ? 'AND ce.name ILIKE \'%' . trim( addslashes( $strEventsSearchFilter ) ) . '%\'' : '';
			$strAndWhere .= ' AND ( ce.date_start >= now()::date OR ce.date_end >= now()::date )';
		}

		$strSql = 'SELECT
						count(ce.id) as c
					FROM
						company_events AS ce
						JOIN property_events AS pe ON ( pe.company_event_id = ce.id AND ce.cid = pe.cid )
					WHERE
						ce.cid = ' . ( int ) $intCid .
						$strAndWhere . '
						AND ce.deleted_by IS NULL
						AND pe.property_id = ' . ( int ) $intPropertyId;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['c'] ) ) return $arrintResponse[0]['c'];

		return 0;
	}

	public static function fetchPaginatedCompanyEventsByPropertyIdByCid( $intPropertyId, $intCid, $strEventsSearchFilter, $objPagination, $arrintPropertyIds, $objDatabase ) {

		$strAndWhere = '';
		if( true == valArr( $strEventsSearchFilter ) ) {

			if( false == is_null( $strEventsSearchFilter['name'] ) ) {
				$strEventName = $strEventsSearchFilter['name'];
				$strAndWhere .= 'AND ce.name ILIKE \'%' . trim( addslashes( $strEventName ) ) . '%\'';
			}

			if( 'Future' == $strEventsSearchFilter['status'] ) {
				$strAndWhere .= ' AND ( ce.date_start >= now()::date OR ce.date_end >= now()::date )';
			}

			if( 'Past' == $strEventsSearchFilter['status'] ) {
				$strAndWhere .= ' AND ce.date_end < now()::date';
			}

		} else {
			$strAndWhere = ( false == is_null( $strEventsSearchFilter ) ) ? 'AND ce.name ILIKE \'%' . trim( addslashes( $strEventsSearchFilter ) ) . '%\'' : '';
			$strAndWhere .= ' AND ( ce.date_start >= now()::date OR ce.date_end >= now()::date )';
		}

        $strOffsetLimit = ( false == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) ? '' : 'OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();

		$strSelect = '';
        $strSqlJoin = '';

        if( true == valArr( $arrintPropertyIds ) ) {
	        if( true == in_array( $intPropertyId, $arrintPropertyIds ) ) {
	        	$strSelect = ', COUNT( attndcnt.custid ) OVER ( PARTITION BY ce.id ) +
	        											( SELECT
			                                                   COALESCE ( COUNT ( cer.id ), 0 ) AS counts
			                                               FROM
			                                                   company_event_responses cer
			                                                   JOIN property_events pe ON ( cer.property_event_id = pe.id AND pe.company_event_id = ce.id AND cer.cid = pe.cid ) AND cer.cid = ' . ( int ) $intCid . ' ) +
												      CASE WHEN ce.customer_id IS NULL
												           THEN
												            0
												           ELSE
												            1
												    END AS attendees_count ';
	        } else {
	        	$strSelect = ', COUNT( attndcnt.custid ) OVER ( PARTITION BY ce.id ) +
	        											( SELECT
			                                                   COALESCE ( COUNT ( cer.id ), 0 ) AS counts
			                                               FROM
			                                                   company_event_responses cer
			                                                   JOIN property_events pe ON ( cer.property_event_id = pe.id AND pe.company_event_id = ce.id AND cer.cid = pe.cid ) AND cer.cid = ' . ( int ) $intCid . ' )
	        											   AS attendees_count ';
	        }

	        $strSqlJoin = 'LEFT JOIN
							    (
							      SELECT
							          DISTINCT ON ( lc.customer_id, compevnt.id ) lc.customer_id AS custid,
							          compevnt.id AS company_event_id,
							          compevnt.cid
							      FROM
							          company_events AS compevnt
							          JOIN customer_events AS custevnt ON ( custevnt.company_event_id = compevnt.id AND custevnt.cid = compevnt.cid )
							          JOIN lease_customers AS lc ON ( lc.customer_id = custevnt.customer_id AND lc.cid = custevnt.cid )
							          JOIN leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid )
							      WHERE
							          l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							          AND custevnt.cid = ' . ( int ) $intCid . '
							    ) attndcnt ON ( attndcnt.company_event_id = ce.id AND attndcnt.cid = ce.cid )';
        }

        $strSql = 'SELECT
						DISTINCT ON ( ce.id ) ce.*,
        				cet.name as company_event_type,
					  	cmf.file_name as company_media_file_name,
        				c.name_first,
        				c.name_last,
        				cu.username
						' . $strSelect . '
					FROM
						company_events AS ce
					JOIN
						property_events AS pe ON ( pe.company_event_id = ce.id AND ce.cid = pe.cid )
					JOIN
						company_users cu ON ( ce.created_by = cu.id AND ce.cid = cu.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
					LEFT JOIN
						company_event_types cet ON ( cet.id = ce.company_event_type_id )
					LEFT JOIN
						company_media_files cmf ON ( cmf.cid = ce.cid and ce.company_media_file_id = cmf.id )
					LEFT JOIN
						customers c ON ( c.id = ce.customer_id AND c.cid = ce.cid )
						' . $strSqlJoin . '
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.deleted_by IS NULL
						AND pe.property_id = ' . ( int ) $intPropertyId . '
						' . $strAndWhere . '
					ORDER BY
						ce.id DESC, ce.created_on DESC, pe.order_num ' . $strOffsetLimit;

		return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchResidentCenterCompanyEventsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ce.*
					FROM
						company_events ce
						JOIN property_events pe ON ( pe.company_event_id = ce.id AND pe.cid = ce.cid )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND pe.property_id = ' . ( int ) $intPropertyId . '
						AND ce.is_published = 1
						AND ce.date_start IS NOT NULL
					ORDER BY
						pe.order_num';

		return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchCompanyEventsByPropertyIdsByCid( $arrintPropertyIds, $intCustomerId, $intCid, $objDatabase, $intExcludedCustomerId = NULL, $intLimit = NULL, $strSource = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strSqlIsRequiredColumnsData = ( true == valStr( $strSource ) && 'dashboard' === $strSource ) ? ' SELECT ce.id, ce.name, ce.date_start, pe.property_id ' : ' SELECT ce.*, pe.property_id ';
		$strSql = $strSqlIsRequiredColumnsData;
		$strSqlJoin = '';

		// if requested from dashboard - not required to fetch this data
		if( true == is_null( $strSource ) ) {
			$strSql .= ', CASE
							WHEN 1 = ( SELECT COUNT(id) FROM customer_events WHERE company_event_id = ce.id AND cid = ce.cid AND
				cid = ' . ( int ) $intCid . ' AND customer_id = ' . ( int ) $intCustomerId . ' )
							THEN 1
							ELSE 0
						END AS is_attending_event,
						CASE
							WHEN 1 = ( SELECT COUNT(id) FROM customer_events WHERE company_event_id = ce.id AND cid = ce.cid AND
				cid = ' . ( int ) $intCid . ' AND customer_id = ' . ( int ) $intCustomerId . ' )
							THEN ( SELECT id FROM customer_events WHERE company_event_id = ce.id AND cid = ce.cid AND
				cid = ' . ( int ) $intCid . ' AND customer_id = ' . ( int ) $intCustomerId . ' )
							ELSE NULL
						END AS customer_event_id,
						CASE
						  WHEN ce.customer_id IS NULL
							THEN
								COUNT( attndcnt.custid ) OVER ( PARTITION BY ce.id )
							ELSE
								COUNT( attndcnt.custid ) OVER ( PARTITION BY ce.id ) + 1
    						END AS attendees_count ';

			$strSqlJoin .= 'LEFT JOIN (
					             SELECT
					                 DISTINCT ON ( lc.customer_id, compevnt.id ) lc.customer_id as custid,
					                 compevnt.id as company_event_id,
					                 compevnt.cid
					             FROM
					                 company_events AS compevnt
					                 JOIN customer_events AS custevnt ON ( custevnt.company_event_id = compevnt.id AND custevnt.cid = compevnt.cid )
					                 JOIN lease_customers AS lc ON ( lc.customer_id = custevnt.customer_id AND lc.cid = custevnt.cid )
          							 JOIN leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid )
					             WHERE
					                 l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					                 AND compevnt.cid = ' . ( int ) $intCid . '
					         ) AS attndcnt ON  ( attndcnt.company_event_id = ce.id AND attndcnt.cid = ce.cid )';
		}

		$strSql .= 'FROM
						company_events AS ce
				 	JOIN
						property_events AS pe ON ( pe.company_event_id = ce.id AND pe.cid = ce.cid )
				    	' . $strSqlJoin . '
					WHERE
						ce.cid = ' . ( int ) $intCid . '
				  		AND pe.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
				  		AND ce.is_published = 1 ';

		// hides resident events if resident don't have permission to add event
		if( false == is_null( $intExcludedCustomerId ) ) {
			$strSql .= 'AND ( ce.customer_id != ' . ( int ) $intExcludedCustomerId . ' OR ce.customer_id IS NULL ) ';
		}

		$strSql .= 'AND CASE
	    					WHEN ce.frequency_id = ' . CFrequency::ONCE . ' OR ce.frequency_id IS NULL
	            			THEN ce.date_start IS NOT NULL AND ce.date_start >= now()::date
	            			WHEN ce.frequency_id IN( ' . CFrequency::DAILY . ',' . CFrequency::WEEKLY . ',' . CFrequency::MONTHLY . ' )
	            			THEN ( ce.date_start IS NOT NULL AND ( ce.date_start >= now()::date OR ce.date_end >= now()::date OR ce.date_end IS NULL ) )
	    				END
	 				ORDER BY
	            		ce.date_start, ce.time_start';

		if( false == is_null( $intLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchCreatedCompanyEventsByPropertyIdByCustomerIdByCid( $intPropertyId, $intCustomerId, $intCid, $objDatabase ) {
		$strSql = ' WITH
						event_data AS ( ';

		$strSql .= 'SELECT
						ce.*, pe.property_id,
						CASE
							WHEN ce.event_approval_data IS NOT NULL
							THEN 1
							ELSE 0
							END AS is_event_approval_value
					FROM
						company_events AS ce
						JOIN property_events AS pe ON ( pe.company_event_id = ce.id AND pe.cid = ce.cid )
					WHERE
						ce.date_start IS NOT NULL
						AND ce.cid = ' . ( int ) $intCid . '
						AND pe.property_id = ' . ( int ) $intPropertyId . '
						AND ce.customer_id = ' . ( int ) $intCustomerId . '
		                AND ce.deleted_by IS NULL AND ce.deleted_on IS NULL 
					ORDER BY
						ce.date_start,
						ce.time_start';

		$strSql .= ' ) ';

		$strSql .= ' SELECT
						*
					FROM
						event_data ed
					WHERE
						CASE
						WHEN ed.is_event_approval_value = 0
						THEN ed.is_published = 1
						ELSE ed.is_published = 0 END ';

		return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchAttendingCompanyEventsByPropertyIdsByCustomerIdByCid( $arrintPropertyIds, $intCustomerId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = ' WITH
						event_data AS ( ';

		$strSql .= 'SELECT
						ce.*, pe.property_id,
						CASE
							WHEN ce.event_approval_data IS NOT NULL
							THEN 1
							ELSE 0
				  		END AS is_event_approval_value
					FROM
						company_events AS ce
						JOIN property_events AS pe ON ( pe.company_event_id = ce.id AND pe.cid = ce.cid )
						JOIN customer_events AS cse ON ( cse.company_event_id = ce.id AND cse.cid = ce.cid AND cse.customer_id = ' . ( int ) $intCustomerId . '  )
					WHERE
						ce.date_start IS NOT NULL
						AND ce.cid = ' . ( int ) $intCid . '
						AND pe.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND CASE
				    		WHEN ce.frequency_id = ' . CFrequency::ONCE . ' OR ce.frequency_id IS NULL
				            	THEN ce.date_start IS NOT NULL AND ce.date_start >= now()::date
				            WHEN ce.frequency_id IN( ' . CFrequency::DAILY . ',' . CFrequency::WEEKLY . ',' . CFrequency::MONTHLY . ' )
				            	THEN ( ce.date_start IS NOT NULL AND ( ce.date_start >= now()::date OR ce.date_end >= now()::date OR ce.date_end IS NULL ) )
				    	END
					ORDER BY
						ce.date_start,
				        ce.time_start';

		$strSql .= ' ) ';

		$strSql .= ' SELECT
						*
					FROM
						event_data ed
					WHERE
						CASE
						WHEN ed.is_event_approval_value = 0
						THEN ed.is_published = 1
						ELSE ed.is_published = 0 END ';

		return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchPublishedActiveCompanyEventCountByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						count( ce.id )
					FROM
						company_events AS ce
					    JOIN property_events AS pe ON ( pe.company_event_id = ce.id AND pe.cid = ce.cid )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND pe.property_id = ' . ( int ) $intPropertyId . '
						AND ce.is_published = 1
						AND ( ce.date_end >= NOW()::date OR ce.date_end IS NULL )';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchPublishedActiveSortedCurrentMonthsCompanyEventsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
		 				ce.*
					FROM
						company_events AS ce
						JOIN property_events AS pe ON ( pe.company_event_id = ce.id AND pe.cid = ce.cid )
					WHERE
					    ce.cid = ' . ( int ) $intCid . '
					    AND pe.property_id = ' . ( int ) $intPropertyId . '
					    AND ce.is_published = 1
					    AND ce.date_start = ( SELECT
													ce.date_start
											  FROM
											  		company_events AS ce , property_events AS pe
											  WHERE pe.company_event_id = ce.id
					    							AND pe.cid = ce.cid
											  		AND ce.cid = ' . ( int ) $intCid . '
					    					  		AND pe.property_id = ' . ( int ) $intPropertyId . '
												    AND date_part(\'year\', ce.date_start) = date_part(\'year\', NOW())
												    AND date_part(\'month\', ce.date_start) = date_part(\'month\', NOW())
													AND date_part(\'day\', ce.date_start) >=  date_part(\'day\', NOW())
					    							AND ce.is_published = 1
												ORDER BY ce.date_start LIMIT 1 )
					ORDER BY
						ce.date_start,
					   	ce.time_start';

		 return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchPublishedActiveSortedCompanyEventsByPropertyIdByMonthByYearByCid( $intPropertyId, $intMonth, $intYear, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;
		if( false == isset( $intMonth ) || false == is_numeric( $intMonth ) ) return NULL;
		if( false == isset( $intYear ) || false == is_numeric( $intYear ) ) return NULL;

		$strSql = 'SELECT
		 				ce.*
					FROM
						company_events AS ce
						JOIN property_events AS pe ON ( pe.company_event_id = ce.id AND pe.cid = ce.cid )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
					    AND pe.property_id = ' . ( int ) $intPropertyId . '
					    AND ce.is_published = 1
					    AND date_part(\'year\', ce.date_start) = ' . ( int ) $intYear . '
					    AND date_part(\'month\', ce.date_start) = ' . ( int ) $intMonth . '
					ORDER BY
					    ce.date_start,
					   	ce.time_start';

		 return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchPublishedActiveSortedCompanyEventsByPropertyIdByDateByCid( $intPropertyId, $intDay, $intMonth, $intYear, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;
		if( false == isset( $intDay ) || false == is_numeric( $intDay ) ) return NULL;
		if( false == isset( $intMonth ) || false == is_numeric( $intMonth ) ) return NULL;
		if( false == isset( $intYear ) || false == is_numeric( $intYear ) ) return NULL;

		$strSql = 'SELECT
		 				ce.*
					FROM
						company_events AS ce
						JOIN property_events AS pe ON ( pe.company_event_id = ce.id AND pe.cid = ce.cid )
					WHERE
					    ce.cid = ' . ( int ) $intCid . '
					    AND pe.property_id = ' . ( int ) $intPropertyId . '
					    AND ce.is_published = 1
					    AND ce.date_start = ( SELECT
													ce.date_start
											  FROM
											  		company_events AS ce , property_events AS pe
											  WHERE pe.company_event_id = ce.id
					    							AND pe.cid = ce.cid
											  		AND ce.cid = ' . ( int ) $intCid . '
					    					  		AND pe.property_id = ' . ( int ) $intPropertyId . '
												    AND date_part(\'year\', ce.date_start) = ' . ( int ) $intYear . '
												    AND date_part(\'month\', ce.date_start) = ' . ( int ) $intMonth . '
													AND date_part(\'day\', ce.date_start) >=  ' . ( int ) $intDay . '
					    							AND ce.is_published = 1
												ORDER BY ce.date_start LIMIT 1 )
					ORDER BY
						ce.date_start,
						ce.time_start';

		 return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchSimpleCompanyEventsByCid( $intCid, $objDatabase,  $boolIsDeletedOnDeletedByEvents = true ) {

		$strSql = 'SELECT
						ce.*,
					  	p.property_name as property_name,
					  	cet.name as company_event_type,
					  	cmf.file_name as company_media_file_name
					FROM
					  	company_events ce
					  	JOIN property_events pe ON( pe.company_event_id = ce.id AND pe.cid = ce.cid )
					  	JOIN properties p ON( p.id = pe.property_id AND p.cid = pe.cid )
					  	LEFT JOIN company_event_types cet ON ( cet.id = ce.company_event_type_id )
					  	LEFT JOIN company_media_files cmf ON ( cmf.cid = ce.cid and ce.company_media_file_id = cmf.id )
					WHERE
					  	ce.is_global = 1
					  	AND  ce.cid = ' . ( int ) $intCid;

		if( false == $boolIsDeletedOnDeletedByEvents ) {
			$strSql .= 'AND ce.deleted_by IS NULL AND ce.deleted_on IS NULL';
		}
		 return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchPublishedFutureActiveSortedCompanyEventsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

       	$strSql = 'SELECT
						ce.*
					FROM
       					property_events pe
       					LEFT JOIN company_events ce ON ( pe.company_event_id = ce.id AND pe.cid = ce.cid AND pe.property_id = ' . ( int ) $intPropertyId . ' AND ce.cid = ' . ( int ) $intCid . ' )
					WHERE
						ce.is_published = 1
						AND pe.cid = ' . ( int ) $intCid . '
					    AND ce.date_start IS NOT NULL
						AND CASE
					    		WHEN ce.date_end IS NULL
					    			THEN ce.date_start >= \'' . date( 'm/d/Y' ) . '\'
					    		WHEN ce.date_end IS NOT NULL
					            	THEN ce.date_end >= \'' . date( 'm/d/Y' ) . '\'
					    	END
					ORDER BY
					   ce.date_start,
					   ce.time_start ';

		 return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchCompanyEventsByIdsByPropertyIdByClient( $arrintCompanyEventIds, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyEventIds ) ) return;

		$strSql = 'SELECT
						ce.*
					FROM
						company_events AS ce,
					    JOIN property_events AS pe ON ( pe.company_event_id = ce.id AND pe.cid = ce.cid )
					WHERE
					   	ce.cid = ' . ( int ) $intCid . '
					   	AND pe.property_id = ' . ( int ) $intPropertyId . '
					   	AND ce.id IN ( ' . implode( ',', $arrintCompanyEventIds ) . ' )';

		return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchUpcomingCompanyEventsByPropertyIdsByClubIdByCid( $arrintPropertyIds, $intClubId, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intClubId ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ce.*, pe.property_id
					FROM
		 				property_events pe
		 				LEFT JOIN company_events ce ON ( pe.company_event_id = ce.id AND pe.cid = ce.cid AND pe.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND ce.cid = ' . ( int ) $intCid . ' AND ce.club_id = ' . ( int ) $intClubId . ' )
					WHERE
						ce.is_published = 1
					    AND CASE
					    		WHEN ce.frequency_id = ' . CFrequency::ONCE . ' OR ce.frequency_id IS NULL
					            	THEN ce.date_start IS NOT NULL AND ce.date_start >= \'' . date( 'm/d/Y' ) . '\'
					            WHEN ce.frequency_id IN ( ' . CFrequency::DAILY . ',' . CFrequency::WEEKLY . ',' . CFrequency::MONTHLY . ' )
					            	THEN ( ce.date_start IS NOT NULL AND ( ce.date_end >= \'' . date( 'm/d/Y' ) . '\' OR ce.date_end IS NULL ) )
					    	END
					ORDER BY
					    ce.date_start,
					    ce.time_start';

		return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchPaginatedUnapprovedCompanyEventsByPropertyIdsByCid( $objPagination, $arrintPropertyIds, $intCid, $objDatabase, $boolIsRejectedEvents = false ) {
		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) return NULL;

		$strSqlRejected = ( true == $boolIsRejectedEvents ) ? ' AND deleted_on IS NOT NULL ' : ' AND deleted_on IS NULL ';

		$strSqlOffsetLimit = '';
		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {

			$intOffset = ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
			$intLimit = ( int ) $objPagination->getPageSize();
			$strSqlOffsetLimit = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		}

		$strSql = 'SELECT
						ce.*,
						c.name_first,
						c.name_last,
						p.property_name
					FROM
						company_events ce
					 	LEFT JOIN property_events pe ON ( ce.id = pe.company_event_id AND pe.cid = ce.cid )
					 	LEFT JOIN customers c ON ( c.id = ce.customer_id AND c.cid = ce.cid )
					 	LEFT JOIN properties p ON ( p.id = pe.property_id AND p.cid = pe.cid )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
					 	AND pe.property_id IN (' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strSqlRejected . '
					 	AND ce.customer_id IS NOT NULL
					 	AND ce.is_published != 1
					ORDER BY
					 	ce.updated_on DESC' . $strSqlOffsetLimit;

		return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchUnapprovedEventsCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsRejectedEvents = false ) {
		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) return NULL;

		$strSqlRejected = ( true == $boolIsRejectedEvents ) ? ' AND deleted_on IS NOT NULL ' : ' AND deleted_on IS NULL ';

		$strSql = 'SELECT
						count( ce.id ) as company_event_count
					FROM
						company_events ce
						LEFT JOIN property_events pe ON ( ce.id = pe.company_event_id AND pe.cid = ce.cid )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND pe.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND ce.customer_id IS NOT NULL
						AND ce.is_published != 1 ' . $strSqlRejected;

		return self::fetchColumn( $strSql, 'company_event_count', $objDatabase );
	}

	public static function fetchCompanyEventsByIdsByCid( $arrintCompanyEventIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyEventIds ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ce.*,
						c.name_first,
						c.name_last,
						p.property_name
					FROM
		 				company_events ce
						LEFT JOIN property_events pe ON ( ce.id = pe.company_event_id AND pe.cid = ce.cid )
						LEFT JOIN customers c ON ( c.id = ce.customer_id AND c.cid = ce.cid )
						LEFT JOIN properties p ON ( p.id = pe.property_id AND p.cid = pe.cid )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.id IN (' . implode( ',', $arrintCompanyEventIds ) . ')';

		return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchSimpleCompanyEventByIdByCid( $intCompanyEventId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ce.*,
						c.name_first,
						c.name_last,
						p.property_name
					FROM
						company_events ce
					 	LEFT JOIN property_events pe ON ( ce.id = pe.company_event_id AND pe.cid = ce.cid )
					 	LEFT JOIN customers c ON ( c.id = ce.customer_id AND c.cid = ce.cid )
					 	LEFT JOIN properties p ON ( p.id = pe.property_id AND p.cid = pe.cid )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
					 	AND ce.id = ' . ( int ) $intCompanyEventId;

		return self::fetchCompanyEvent( $strSql, $objDatabase );
	}

	public static function fetchPaginatedUnapprovedCompanyEventsByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intLimit, $strSortBy, $objDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							CASE WHEN TRIM( dp.resident_portal_events->>\'urgent_event_requested_since\' ) != \'\' AND ( ce.date_end - ce.date_start ) >= ( dp.resident_portal_events->>\'urgent_event_requested_since\' )::int THEN
								3
								WHEN TRIM( dp.resident_portal_events->>\'urgent_event_date_within\' ) != \'\' AND ce.date_start <= ( CURRENT_DATE + ( dp.resident_portal_events->>\'urgent_event_date_within\' )::int ) THEN
								3
								WHEN TRIM( dp.resident_portal_events->>\'important_reservation_date_within\' ) != \'\' AND ce.date_start <= ( CURRENT_DATE + ( dp.resident_portal_events->>\'important_reservation_date_within\' )::int ) THEN
								2
								WHEN TRIM( dp.resident_portal_events->>\'important_reservation_requested_since\' ) != \'\' AND ( ce.date_end - ce.date_start ) >= ( dp.resident_portal_events->>\'important_reservation_requested_since\' )::int THEN
								2
							ELSE
								1
							END AS priority,
							ce.id,
							cl.id AS lease_id,
							ce.customer_id,
							ce.name AS event,
							ce.date_start AS event_start_date,
							ce.date_end AS event_end_date,
							ce.time_start AS event_start_time,
							ce.time_end AS event_end_time,
							ce.event_approval_data as event_approval_data,
							ce.created_on AS requested,
							func_format_customer_name( c.name_first, c.name_last ) as customer_name,
							COALESCE( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache ) AS unit_number,
							cl.property_name AS property,
							MAX( cl.id ) OVER( PARTITION BY ce.id ORDER BY cl.id DESC ) AS max_lease_id,
 							tz.time_zone_name,
 							pe.property_id AS pe_property_id, lc.property_id AS lc_property_id
						FROM
						 	company_events ce
							JOIN property_events pe ON ( ce.id = pe.company_event_id AND pe.cid = ce.cid )
							JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) AS lp ON ( ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'lp.is_disabled = 0 AND' : '' ) . ' lp.property_id = pe.property_id )
							JOIN customers c ON ( c.id = ce.customer_id AND c.cid = ce.cid )
							JOIN lease_customers lc ON ( lc.customer_id = c.id AND lc.cid = ce.cid )
							JOIN properties p ON ( p.cid = lc.cid AND p.id = lc.property_id )
 							JOIN time_zones tz ON ( tz.id = p.time_zone_id )
							LEFT JOIN cached_leases cl ON ( cl.id = lc.lease_id AND cl.cid = ce.cid )
							LEFT JOIN dashboard_priorities dp ON ( dp.cid = ce.cid )
						WHERE
							ce.cid = ' . ( int ) $intCid . '
							AND ce.is_published != 1
							AND ce.customer_id IS NOT NULL
							AND ce.deleted_on IS NULL
							AND ce.is_archived != 1
						) subQ
					WHERE
						subQ.lease_id = subQ.max_lease_id
						' . $strPriorityWhere . '
					ORDER BY ' . $strSortBy . '
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnapprovedEventsCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql .= 'SELECT
						count( subQ ) AS ' .
						( false == $boolIsGroupByProperties ?
							'unapproved_events_count,
							MAX'
						:
							'resident_events,
							property_id,
							property_name,
						' ) .
						'( priority ) AS priority
					FROM (
						SELECT
							rank() OVER( PARTITION BY ce.id ORDER BY cl.id DESC ),
							CASE
								WHEN TRIM( dp.resident_portal_events->>\'urgent_event_requested_since\' ) != \'\' AND ( ce.date_end - ce.date_start ) >= ( dp.resident_portal_events->>\'urgent_event_requested_since\' )::int THEN
									3
								WHEN TRIM( dp.resident_portal_events->>\'urgent_event_date_within\' ) != \'\' AND ce.date_start <= ( CURRENT_DATE + ( dp.resident_portal_events->>\'urgent_event_date_within\' )::int ) THEN
									3
								WHEN TRIM( dp.resident_portal_events->>\'important_reservation_date_within\' ) != \'\' AND ce.date_start <= ( CURRENT_DATE + ( dp.resident_portal_events->>\'important_reservation_date_within\' )::int ) THEN
									2
								WHEN TRIM( dp.resident_portal_events->>\'important_reservation_requested_since\' ) != \'\' AND ( ce.date_end - ce.date_start ) >= ( dp.resident_portal_events->>\'important_reservation_requested_since\' )::int THEN
									2
								ELSE
									1
							END AS priority,
							cl.property_id,
							p.property_name
						FROM
					 		company_events ce
							JOIN property_events pe ON ( ce.id = pe.company_event_id AND pe.cid = ce.cid )
							JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) AS lp ON ( lp.property_id = pe.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
							JOIN properties p ON ( p.cid = pe.cid AND p.id = pe.property_id )
							JOIN customers c ON ( c.id = ce.customer_id AND c.cid = ce.cid )
							JOIN lease_customers lc ON ( lc.customer_id = c.id AND lc.cid = ce.cid )
							LEFT JOIN cached_leases cl ON ( cl.id = lc.lease_id AND cl.cid = ce.cid )
							LEFT JOIN dashboard_priorities dp ON ( dp.cid = ce.cid )
						WHERE
							ce.cid = ' . ( int ) $intCid . '
							AND ce.is_published != 1
							AND ce.customer_id IS NOT NULL
							AND ce.deleted_on IS NULL
							AND ce.is_archived != 1
						GROUP BY
							cl.id,
							ce.id,
							ce.date_end,
							ce.date_start,
							dp.resident_portal_events->>\'urgent_event_requested_since\',
							dp.resident_portal_events->>\'urgent_event_date_within\',
							dp.resident_portal_events->>\'important_reservation_date_within\',
							dp.resident_portal_events->>\'important_reservation_requested_since\',
							cl.property_id,
							p.property_name
					 ) subQ
					WHERE
						subQ.rank = 1
						' . $strPriorityWhere . ' ' .
					( true == $boolIsGroupByProperties ?
						' GROUP BY
							property_id,
							property_name,
							priority'
					: '' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllCompanyEventsByPropertyIdsByCid( $arrintPropertyIds, $intCustomerId, $intCid, $objDatabase, $intExcludedCustomerId = NULL, $strSource = '' ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = ' WITH
						event_data AS ( ';

		$strSql .= ' SELECT ce.*, pe.property_id ';

		$strSqlJoin = '';

			$strSql .= ', CASE
							WHEN 1 = ( SELECT COUNT(id) FROM customer_events WHERE company_event_id = ce.id AND cid = ce.cid AND
				cid = ' . ( int ) $intCid . ' AND customer_id = ' . ( int ) $intCustomerId . ' )
							THEN 1
							ELSE 0
						END AS is_attending_event,
						CASE
							WHEN 1 = ( SELECT COUNT(id) FROM customer_events WHERE company_event_id = ce.id AND cid = ce.cid AND
				cid = ' . ( int ) $intCid . ' AND customer_id = ' . ( int ) $intCustomerId . ' )
							THEN ( SELECT id FROM customer_events WHERE company_event_id = ce.id AND cid = ce.cid AND
				cid = ' . ( int ) $intCid . ' AND customer_id = ' . ( int ) $intCustomerId . ' )
							ELSE NULL
						END AS customer_event_id,
						CASE
						  WHEN ce.customer_id IS NULL
							THEN
								COUNT( attndcnt.custid ) OVER ( PARTITION BY ce.id )
							ELSE
								COUNT( attndcnt.custid ) OVER ( PARTITION BY ce.id ) + 1
    						END AS attendees_count,
						CASE
						   WHEN ce.event_approval_data IS NOT NULL
						     THEN 1
						     ELSE 0
				  		END AS is_event_approval_value ';

			$strSqlJoin .= 'LEFT JOIN (
					             SELECT
					                 DISTINCT ON ( lc.customer_id, compevnt.id ) lc.customer_id as custid,
					                 compevnt.id as company_event_id,
					                 compevnt.cid
					             FROM
					                 company_events AS compevnt
					                 JOIN customer_events AS custevnt ON ( custevnt.company_event_id = compevnt.id AND custevnt.cid = compevnt.cid )
					                 JOIN lease_customers AS lc ON ( lc.customer_id = custevnt.customer_id AND lc.cid = custevnt.cid )
          							 JOIN leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid )
					             WHERE
					                 l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					                 AND compevnt.cid = ' . ( int ) $intCid . '
					         ) AS attndcnt ON  ( attndcnt.company_event_id = ce.id AND attndcnt.cid = ce.cid )';

		$strSql .= 'FROM
						company_events AS ce
				 	JOIN
						property_events AS pe ON ( pe.company_event_id = ce.id AND pe.cid = ce.cid )
				    	' . $strSqlJoin . '
					WHERE
						ce.cid = ' . ( int ) $intCid . '
				  		AND pe.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		// hides resident events if resident don't have permission to add event
		if( false == is_null( $intExcludedCustomerId ) ) {
			$strSql .= 'AND ( ce.customer_id != ' . ( int ) $intExcludedCustomerId . ' OR ce.customer_id IS NULL ) ';
		}
		if( 'rp_responsive' == $strSource ) {
			$strSql .= 'AND ce.deleted_by IS NULL AND ce.deleted_on IS NULL ';
		}
		$strSql .= 'AND CASE
	    					WHEN ce.frequency_id = ' . CFrequency::ONCE . ' OR ce.frequency_id IS NULL
	            			THEN ce.date_start IS NOT NULL AND ce.date_start >= now()::date
	            			WHEN ce.frequency_id IN( ' . CFrequency::DAILY . ',' . CFrequency::WEEKLY . ',' . CFrequency::MONTHLY . ' )
	            			THEN ( ce.date_start IS NOT NULL AND ( ce.date_start >= now()::date OR ce.date_end >= now()::date OR ce.date_end IS NULL ) )
	    				END
	 				ORDER BY
	            		ce.date_start, ce.time_start';

		$strSql .= ' ) ';

		$strSql .= ' SELECT
						*
					FROM
						event_data ed
					WHERE
						CASE
						WHEN ed.is_event_approval_value = 0
						THEN ed.is_published = 1
						ELSE ed.is_published = 0 END ';

		return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

	public static function fetchSimpleUpdatedCompanyEventsByIdsByCid( $arrintCompanyEventIds, $intCid, $objDatabase ) {

		if( false == is_numeric( $intCid ) || false == valArr( $arrintCompanyEventIds ) ) return NULL;

		$strSql = 'SELECT event_approval_data->>\'event_company_media_file_id\' AS event_company_media_file_id
					   FROM company_events
				   WHERE cid = ' . ( int ) $intCid . '
					   AND id IN (' . implode( ',', $arrintCompanyEventIds ) . ')
					   AND event_approval_data IS NOT NULL
				   ORDER BY is_published ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedCompanyEventsByPropertyIdByCidByCustomerId( $intPropertyId, $intCid, $intCustomerId, $intPageNo, $intPageSize, $arrintPropertyIds, $objDatabase ) {

		$strOffsetLimit = '';
		if( true == valId( $intPageNo ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit = $intPageSize;
			$strOffsetLimit = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}
		$strSelect = '';
		$strSqlJoin = '';

		if( true == valArr( $arrintPropertyIds ) ) {
			if( true == in_array( $intPropertyId, $arrintPropertyIds ) ) {
				$strSelect = ', COUNT( attndcnt.custid ) OVER ( PARTITION BY ce.id ) +
	        											( SELECT
			                                                   COALESCE ( COUNT ( cer.id ), 0 ) AS counts
			                                               FROM
			                                                   company_event_responses cer
			                                                   JOIN property_events pe ON ( cer.property_event_id = pe.id AND pe.company_event_id = ce.id AND cer.cid = pe.cid ) AND cer.cid = ' . ( int ) $intCid . ' ) +
												      CASE WHEN ce.customer_id IS NULL
												           THEN
												            0
												           ELSE
												            1
												    END AS attendees_count ';
			} else {
				$strSelect = ', COUNT( attndcnt.custid ) OVER ( PARTITION BY ce.id ) +
	        											( SELECT
			                                                   COALESCE ( COUNT ( cer.id ), 0 ) AS counts
			                                               FROM
			                                                   company_event_responses cer
			                                                   JOIN property_events pe ON ( cer.property_event_id = pe.id AND pe.company_event_id = ce.id AND cer.cid = pe.cid ) AND cer.cid = ' . ( int ) $intCid . ' )
	        											   AS attendees_count ';
			}

			$strSqlJoin = 'LEFT JOIN
							    (
							      SELECT
							          DISTINCT ON ( lc.customer_id, compevnt.id ) lc.customer_id AS custid,
							          compevnt.id AS company_event_id,
							          compevnt.cid
							      FROM
							          company_events AS compevnt
							          JOIN customer_events AS custevnt ON ( custevnt.company_event_id = compevnt.id AND custevnt.cid = compevnt.cid )
							          JOIN lease_customers AS lc ON ( lc.customer_id = custevnt.customer_id AND lc.cid = custevnt.cid )
							          JOIN leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid )
							      WHERE
							          l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							          AND custevnt.cid = ' . ( int ) $intCid . '
							    ) attndcnt ON ( attndcnt.company_event_id = ce.id AND attndcnt.cid = ce.cid )';
		}

		$strSql = 'SELECT
						ce.*,
        				cet.name as company_event_type,
					  	cmf.file_name as company_media_file_name,
        				c.name_first,
        				c.name_last,
        				cu.username,
        				CASE
							WHEN 1 = ( SELECT COUNT(id) FROM customer_events WHERE company_event_id = ce.id AND cid = ce.cid AND
									cid = ' . ( int ) $intCid . ' AND customer_id = ' . ( int ) $intCustomerId . ' )
							THEN 1
							ELSE 0
						END AS is_attending_event
						' . $strSelect . '
					FROM
						company_events AS ce
					JOIN
						property_events AS pe ON ( pe.company_event_id = ce.id AND ce.cid = pe.cid )
					JOIN
						company_users cu ON ( ce.created_by = cu.id AND ce.cid = cu.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
					LEFT JOIN
						company_event_types cet ON ( cet.id = ce.company_event_type_id )
					LEFT JOIN
						company_media_files cmf ON ( cmf.cid = ce.cid and ce.company_media_file_id = cmf.id )
					LEFT JOIN
						customers c ON ( c.id = ce.customer_id AND c.cid = ce.cid )
						' . $strSqlJoin . '
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.deleted_by IS NULL AND ( ce.is_published = 1 OR ce.customer_id = ' . ( int ) $intCustomerId . ' )
						AND pe.property_id = ' . ( int ) $intPropertyId . '
						AND ( ce.date_start >= CURRENT_DATE - INTERVAL \'3 months\' ) 
					ORDER BY
						to_char(ce.date_start, \'YYYY-MM-DD\') DESC, pe.order_num ' . $strOffsetLimit;

		return self::fetchCompanyEvents( $strSql, $objDatabase );
	}

}

?>