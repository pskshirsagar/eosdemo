<?php

use Psi\Eos\Entrata\CMaintenancePriorities;

class CMaintenancePriority extends CBaseMaintenancePriority {

	const CODE_MAINTENANCE_PRIORITY_VERY_LOW 	= 'VL';
	const CODE_MAINTENANCE_PRIORITY_LOW 		= 'LO';
	const CODE_MAINTENANCE_PRIORITY_MEDIUM 	 	= 'MD';
	const CODE_MAINTENANCE_PRIORITY_HIGH 		= 'HI';
	const CODE_MAINTENANCE_PRIORITY_VERY_HIGH 	= 'VH';
	const CODE_MAINTENANCE_PRIORITY_EMERGENCY 	= 'EM';

	const VERY_LOW			= 1;
	const LOW				= 2;
	const MEDIUM			= 3;
	const HIGH				= 4;
	const EMERGENCY			= 5;
	const OTHER				= 6;

	protected $m_intIntegrationClientStatusTypeId;
	protected $m_strPropertyRemotePrimaryKey;
	protected $m_intPsProductId;
	protected $m_fltDueInterval;
	protected $m_intDueIntervalTypeId;

	protected $m_boolUseCalendarHours = false;
	protected $m_boolShowOnResidentPortalPropertyLevel = false;

	protected $m_strFilterPriority;

	public static $c_arrintIntegratedClientTypes = [
		CIntegrationClientType::YARDI,
		CIntegrationClientType::MRI,
		CIntegrationClientType::TIMBERLINE,
		CIntegrationClientType::TENANT_PRO,
		CIntegrationClientType::REAL_PAGE,
		CIntegrationClientType::JENARK,
		CIntegrationClientType::REAL_PAGE_API,
		CIntegrationClientType::YARDI_RPORTAL,
	];

	public static $c_arrintDefaultPriorities = [
		CMaintenancePriority::LOW,
		CMaintenancePriority::MEDIUM,
		CMaintenancePriority::HIGH
	];
    /**
     * Create Functions
     */

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getMaintenancePriorityName( $intMaintenancePriorityId, $intCid, $objDatabase ) {
		$objMaintenancePriority = CMaintenancePriorities::createService()->fetchMaintenancePriority( sprintf( 'SELECT name, details FROM maintenance_priorities WHERE id = %d AND cid = %d', ( int ) $intMaintenancePriorityId, ( int ) $intCid ), $objDatabase );
		$strMaintenancePriority = NULL;
		if( true == valObj( $objMaintenancePriority, CMaintenancePriority::class ) ) {
			$strMaintenancePriority = $objMaintenancePriority->getName();
		}
		return $strMaintenancePriority;
	}

    public function createPropertyMaintenancePriority() {

    	$objPropertyMaintenancePriority = new CPropertyMaintenancePriority();
    	$objPropertyMaintenancePriority->setCid( $this->m_intCid );
    	$objPropertyMaintenancePriority->setMaintenancePriorityId( $this->m_intId );

    	if( self::CODE_MAINTENANCE_PRIORITY_MEDIUM == $this->getCode() ) {
    		$objPropertyMaintenancePriority->setIsDefault( 1 );
    	}

    	return $objPropertyMaintenancePriority;
    }

    /**
     * Fetch Functions
     */

	public function fetchPropertyMaintenancePriorityByPropertyId( $intPropertyId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyMaintenancePriorities::createService()->fetchPropertyMaintenancePriorityByPropertyIdByIdByCid( $intPropertyId, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMaintenancePriorities( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyMaintenancePriorities::createService()->fetchPropertyMaintenancePrioritiesByMaintenancePriorityIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
        $boolIsValid = true;

        if( false == is_null( $this->getIntegrationDatabaseId() ) || false == is_null( $this->getRemotePrimaryKey() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'remote_primary_key', __( 'The Maintenance priority is integrated, editing will effect the snchronization with integrated application.' ) ) );
		}

        return $boolIsValid;
    }

    public function valCode( $objDatabase ) {

    	$boolIsValid = true;

    	if( true == valStr( $this->getCode() ) ) {

    		$objMaintenancePriority = CMaintenancePriorities::createService()->fetchMaintenancePriorityByCidByCode( $this->m_intCid, $this->getCode(), $objDatabase );

	    	if( true == valObj( $objMaintenancePriority, 'CMaintenancePriority' ) && $objMaintenancePriority->getId() != $this->getId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Code already exists.' ) ) );
			}
    	}

        return $boolIsValid;
    }

    public function valName( $objDatabase ) {

        $boolIsValid = true;

        if( true == valStr( $this->getName() ) ) {

	        $objMaintenancePriority  = CMaintenancePriorities::createService()->fetchCustomMaintenancePrioritiesWithPropertyRemotePrimaryKeyAndPsProductIdByCid( $this->m_intCid, $objDatabase, $this->getName() );
	        if( true == valObj( $objMaintenancePriority, 'CMaintenancePriority' ) && $objMaintenancePriority->getId() != $this->getId() ) {
				$boolIsValid = false;
	    		$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Name already exists.' ) ) );
			}

        } else {
        	$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valMaintenancePriorityTypeId() {

        $boolIsValid = true;

        if( false == valId( $this->getMaintenancePriorityTypeId() ) ) {
        	$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_priority_type_id', __( 'Priority Type is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valIsDefault() {
        $boolIsValid = true;

        if( 1 == $this->getIsDefault() ) {
        	$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_default', __( 'System Default Maintenance Priority can not be deleted.' ) ) );
        }

        return $boolIsValid;
    }

	public function valIsPublished( $objDatabase ) {
		$boolIsValid = true;
		$intCountMaintenanceDefaultPriority = CMaintenancePriorities::createService()->fetchCustomDefaultMaintenancePriorityByPriorityIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( 0 < $intCountMaintenanceDefaultPriority ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Default property priority \'{%s, 0}\' cannot be disabled.', [ $this->getName() ] ) ) );
		}
		return $boolIsValid;
	}

    public function valOrderNum() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valMaintenancePriorityMaintenanceRequests( $objDatabase ) {
    	$boolIsValid = true;

    	if( true == valId( $this->getId() ) ) {

    		$arrobjMaintenanceRequests = \Psi\Eos\Entrata\CMaintenanceRequests::createService()->fetchCustomMaintenanceRequestsByMaintenancePriorityIdByCid( $this->getId(), $this->getCid(), $objDatabase );

    		if( true == valArr( $arrobjMaintenanceRequests ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_priority_id', __( 'Maintenance request(s) exists for the maintenance priority.' ) ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function valMaintenancePriorityMaintenanceTemplates( $objDatabase ) {
    	$boolIsValid = true;

    	if( true == valId( $this->getId() ) ) {

    		$arrobjMaintenanceTemplates = \Psi\Eos\Entrata\CMaintenanceTemplates::createService()->fetchMaintenanceTemplatesByCidByMaintenancePriorityId( $this->getCid(), $this->getId(), $objDatabase );

    		if( true == valArr( $arrobjMaintenanceTemplates ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_priority_id', __( 'Maintenance template(s) exists for the maintenance priority.' ) ) );
    		}

    	}

    	return $boolIsValid;
    }

    public function valToggleMaintenancePriorityMaintenanceTemplates( $objDatabase ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getId() ) && 0 == $this->getIsPublished() ) {

    		$arrobjMaintenanceTemplates = \Psi\Eos\Entrata\CMaintenanceTemplates::createService()->fetchMaintenanceTemplatesByCidByMaintenancePriorityId( $this->getCid(), $this->getId(), $objDatabase );

    		if( true == valArr( $arrobjMaintenanceTemplates ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_priority_id', __( 'Maintenance template(s) exists for the maintenance priority.' ) ) );
    		}

    	}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCode( $objDatabase );
            	$boolIsValid &= $this->valName( $objDatabase );
            	$boolIsValid &= $this->valMaintenancePriorityTypeId();
            	$boolIsValid &= $this->valRemotePrimaryKey();
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valMaintenancePriorityMaintenanceRequests( $objDatabase );
            	$boolIsValid &= $this->valMaintenancePriorityMaintenanceTemplates( $objDatabase );
            	break;

            case 'toggle_publish_maintenance_priority':
            	$boolIsValid &= $this->valToggleMaintenancePriorityMaintenanceTemplates( $objDatabase );
            	break;

            case 'validate_update_new':
            	$boolIsValid &= $this->valCode( $objDatabase );
            	$boolIsValid &= $this->valName( $objDatabase );
            	$boolIsValid &= $this->valMaintenancePriorityTypeId();
            	// $boolIsValid &= $this->valRemotePrimaryKey();
            	$boolIsValid &= $this->valToggleMaintenancePriorityMaintenanceTemplates( $objDatabase );
            	if( false == valId( $this->getIsPublished() ) ) {
            		$boolIsValid &= $this->valIsPublished( $objDatabase );
            	}
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function setPropertyId( $intPropertyId ) {
    	$this->m_intPropertyId = $intPropertyId;
    }

    public function setPsProductId( $intPsProductId ) {
    	return $this->m_intPsProductId = $intPsProductId;
    }

	public function setPropertyRemotePrimaryKey( $strPropertyRemotePrimaryKey ) {
		return $this->m_strPropertyRemotePrimaryKey = $strPropertyRemotePrimaryKey;
	}

	public function setIntegrationClientStatusTypeId( $intIntegrationClientStatusTypeId ) {
		return $this->m_intIntegrationClientStatusTypeId = $intIntegrationClientStatusTypeId;
	}

	public function setFilterPriority( $strFilterPriority ) {
		return $this->m_strFilterPriority = $strFilterPriority;
	}

	public function setDueInterval( $fltDueInterval ) {
		$this->m_fltDueInterval = CStrings::strToFloatDef( $fltDueInterval, NULL, false, 2 );
	}

	public function setDueIntervalTypeId( $intDueIntervalTypeId ) {
		$this->m_intDueIntervalTypeId = CStrings::strToIntDef( $intDueIntervalTypeId, NULL, false );
	}

	public function setUseCalendarHours( $boolUseCalendarHours ) {
		$this->m_boolUseCalendarHours = CStrings::strToBool( $boolUseCalendarHours );
	}

	public function setShowOnResidentPortalPropertyLevel( $boolShowOnResidentPortalPropertyLevel ) {
		$this->m_boolShowOnResidentPortalPropertyLevel = $boolShowOnResidentPortalPropertyLevel;
	}

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

    public function getPsProductId() {
    	return $this->m_intPsProductId;
    }

	public function getPropertyRemotePrimaryKey() {
		return $this->m_strPropertyRemotePrimaryKey;
	}

	public function getIntegrationClientStatusTypeId() {
		return $this->m_intIntegrationClientStatusTypeId;
	}

	public function getFilterPriority() {
		return $this->m_strFilterPriority;
	}

	public function getDueInterval() {
		return $this->m_fltDueInterval;
	}

	public function getDueIntervalTypeId() {
		return $this->m_intDueIntervalTypeId;
	}

	public function getUseCalendarHours() {
		return $this->m_boolUseCalendarHours;
	}

	public function getShowOnResidentPortalPropertyLevel() {
		return $this->m_boolShowOnResidentPortalPropertyLevel;
	}

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['ps_product_id'] ) ) $this->setPsProductId( $arrmixValues['ps_product_id'] );
		if( true == isset( $arrmixValues['property_remote_primary_key'] ) ) $this->setPropertyRemotePrimaryKey( $arrmixValues['property_remote_primary_key'] );
		if( true == isset( $arrmixValues['integration_client_status_type_id'] ) ) $this->setIntegrationClientStatusTypeId( $arrmixValues['integration_client_status_type_id'] );
		if( true == isset( $arrmixValues['filter_priority'] ) ) $this->setFilterPriority( $arrmixValues['filter_priority'] );
	    if( true == isset( $arrmixValues['due_interval'] ) ) $this->setDueInterval( $arrmixValues['due_interval'] );
	    if( true == isset( $arrmixValues['due_interval_type_id'] ) ) $this->setDueIntervalTypeId( $arrmixValues['due_interval_type_id'] );
	    if( true == isset( $arrmixValues['use_calendar_hours'] ) ) $this->setUseCalendarHours( $arrmixValues['use_calendar_hours'] );
	    if( true == isset( $arrmixValues['show_on_resident_portal_property_level'] ) ) $this->setShowOnResidentPortalPropertyLevel( $arrmixValues['show_on_resident_portal_property_level'] );
    }

}
?>