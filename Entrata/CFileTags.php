<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileTags
 * Do not add any new functions to this class.
 */

class CFileTags extends CBaseFileTags {

    public static function fetchConflictingFileTagCount( $objConflictingFileTag, $objDatabase ) {

		$intId = ( false == is_numeric( $objConflictingFileTag->getId() ) ) ? 0 : $objConflictingFileTag->getId();

		$strWhereSql = ' WHERE
				   			cid = ' . ( int ) $objConflictingFileTag->getCid() . '
				   			AND id <> ' . ( int ) $intId .
					   	' AND keyword = \'' . ( string ) addslashes( $objConflictingFileTag->getKeyword() ) . '\'
				   		 LIMIT 1';

		return self::fetchFileTagCount( $strWhereSql, $objDatabase );
	}
}
?>