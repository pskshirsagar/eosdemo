<?php

class CDefaultSystemMessageTemplateCustomText extends CBaseDefaultSystemMessageTemplateCustomText {

	public function __construct() {
		parent::__construct();
		$this->setAllowDifferentialUpdate( true );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultSystemMessageId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemMessageTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getFilteredValue() {

		$strSearch = 'src="/media_library/' . $this->getCid() . '/';
		$strReplace = 'src="' . CConfig::get( 'media_library_path' ) . '/media_library/' . $this->getCid() . '/';

		return str_replace( $strSearch, $strReplace, $this->getValue() );
	}

}
?>