<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CModules
 * Do not add any new functions to this class.
 */

use Psi\Libraries\NewRelic\CNewRelic;

class CModules extends CBaseModules {

	public static function fetchModules( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CModule', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchModule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CModule', $objDatabase );
	}

	public static function fetchAllModules( $objDatabase ) {
		return self::fetchModules( 'SELECT * FROM modules WHERE id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . ' ORDER BY name', $objDatabase );
	}

	public static function fetchDefaultModules( $objDatabase, $strOrderBy = 'name' ) {
		return self::fetchModules( 'SELECT * FROM modules WHERE id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . ' ORDER BY ' . $strOrderBy, $objDatabase );
	}

	public static function fetchDefaultModuleByModuleName( $strModuleName, $objDatabase ) {
		return self::fetchModules( 'SELECT * FROM modules WHERE name = \'' . addslashes( $strModuleName ) . '\' ORDER BY name', $objDatabase );
	}

	public static function fetchDefaultModuleByIdByNameByActions( $intModuleId = NULL, $strModuleName, $strActions, $objDatabase ) {

		if( true == valStr( $strActions ) && 'NULL' != $strActions ) {
			$strSelectSql      = ' string_agg( ( SELECT array_agg(UNNEST)::TEXT FROM UNNEST ( modules.actions ) WHERE UNNEST = ANY ( ' . $strActions . '::TEXT [ ] ) ), \',\') as actions';
			$strWhereCondition = 'AND actions && ' . $strActions . '::text[]';
		} else {
			$strSelectSql      = ' 1 as actions';
			$strWhereCondition = 'AND actions IS NULL ';
		}

		if( true == valId( $intModuleId ) ) {
			$strWhereCondition .= 'AND id != ' . ( int ) $intModuleId;
		}

		$strSql = 'SELECT ' . $strSelectSql . ' FROM modules WHERE name = \'' . addslashes( $strModuleName ) . '\' ' . $strWhereCondition;

		return self::fetchColumn( $strSql, 'actions', $objDatabase );

	}

	public static function fetchAllPublishedModules( $objDatabase ) {
		return self::fetchModules( 'SELECT * FROM modules WHERE id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . ' AND is_published = 1 ORDER BY name, parent_module_id, order_num, id', $objDatabase );
	}

	public static function fetchAllPublishedVisibleModules( $objDatabase ) {
		return self::fetchModules( 'SELECT * FROM modules WHERE id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . ' AND is_published = 1 AND is_hidden = 0 ORDER BY parent_module_id, order_num, id', $objDatabase );
	}

	public static function fetchPublishedModulesByProductIdsByCompanyUserByCid( $arrintPsProductOptionIds, $objCompanyUser, $intCid, $objDatabase, $arrmixOptions = [] ) {

		$strProductIdsCondition = $strAdminCondition = $strProductOptionIdsCondition = '';
		$arrintPsProductIds         = $arrintPsProductOptionIds['product_ids'];
		$arrintPsProductOptionId    = $arrintPsProductOptionIds['options_ids'];
		$arrintTempPsProductId      = [];
		$strCountryCode = '';
		if( true == valArr( $arrmixOptions ) ) {
			$strCountryCode = ( is_null( $arrmixOptions['country_code'] ) ) ? 'US' : $arrmixOptions['country_code'];
			$strCountryCodeCondition = ' AND ( m.options->>\'country_codes\' IS NULL OR jsonb_array_length ( ( m.options ->> \'country_codes\' ) :: jsonb ) = 0 OR ( m.options->>\'country_codes\' )::jsonb ? \'' . $strCountryCode . '\' )';
		}

		if( true == valArr( $arrintPsProductOptionId ) ) {
			$arrintTempPsProductId      = array_keys( $arrintPsProductOptionIds['options_ids'] );
			$strProductOptionIdsCondition = ' OR ( pm.ps_product_id , pm.ps_product_option_id ) IN ( ' . sqlIntMultiImplode( $arrintPsProductOptionId ) . ' )';
		}

		if( true == valArr( $arrintPsProductIds ) ) {

			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintPsProductIds ) && true == in_array( CPsProduct::DOCUMENT_MANAGEMENT, $arrintPsProductIds ) && ( false == valArr( $arrintPsProductOptionId ) || ( 1 == \Psi\Libraries\UtilFunctions\count( $arrintTempPsProductId ) && true == in_array( CPsProduct::DOCUMENT_MANAGEMENT, $arrintTempPsProductId ) ) ) ) {
				$strProductIdsCondition = '( pm.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) AND pm.ps_product_option_id IS NULL ) ' . $strProductOptionIdsCondition;
			} else {
				$strProductIdsCondition = 'pm.ps_product_id IS NULL OR ( pm.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )  AND pm.ps_product_option_id IS NULL ) ' . $strProductOptionIdsCondition;
			}
		} else {
			$strProductIdsCondition = 'pm.ps_product_id IS NULL ';
		}

		if( true == $objCompanyUser->getIsAdministrator() && false == $objCompanyUser->getIsPSIUser() ) {
			$strAdminCondition = ' AND m.allow_only_psi_admin = 0';
		} elseif( false == $objCompanyUser->getIsPSIUser() ) {
			$strAdminCondition = ' AND m.allow_only_psi_admin = 0 AND m.allow_only_admin = 0';
		}

		$strSql = '
			SELECT
				m.*
			FROM(
				SELECT
					m.id,
					NULL::INTEGER AS cid,
					m.default_cid,
					m.parent_module_id,
					m.name,
					m.title,
					m.url,
					m.description,
					m.has_write_operation,
					m.is_public,
					m.allow_only_admin,
					m.allow_only_psi_admin,
					m.is_hidden,
					m.options::text,
					m.order_num,
					m.updated_by,
					m.updated_on,
					m.created_by,
					m.created_on,
					NULL::INTEGER AS report_instance_id,
					m.order_num AS order_num_final,
					m.has_write_operation
				FROM
					modules m
					LEFT JOIN product_modules pm ON pm.module_id = m.id
					LEFT JOIN report_groups rg ON rg.cid = ' . ( int ) $intCid . ' AND rg.module_id = m.id
					LEFT JOIN report_instances ri ON ri.cid = ' . ( int ) $intCid . ' AND ri.module_id = m.id
				WHERE
					m.is_published = 1
					AND m.id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . '
					AND ( ' . $strProductIdsCondition . ' )
					' . $strAdminCondition . '
					AND rg.id IS NULL
					AND ri.id IS NULL
					' . $strCountryCodeCondition . '

				UNION

				SELECT
					m.id,
					ri.cid AS cid,
					m.default_cid,
					ri.parent_module_id,
					m.name,
					util_get_translated( \'name\', ri.name, ri.details ) AS title,
					m.url,
					m.description,
					m.has_write_operation,
					m.is_public,
					m.allow_only_admin,
					m.allow_only_psi_admin,
					m.is_hidden,
					m.options::text,
					m.order_num,
					m.updated_by,
					m.updated_on,
					m.created_by,
					m.created_on,
					ri.id AS report_instance_id,
					m.order_num AS order_num_final,
					m.has_write_operation
				FROM
					report_instances ri
					JOIN report_groups rg ON rg.cid = ri.cid AND rg.id = ri.report_group_id AND rg.report_group_type_id <> ' . CReportGroupType::PACKET . '
					JOIN modules m ON m.id = ri.module_id
					LEFT JOIN product_modules pm ON pm.module_id = m.id
				WHERE
					m.is_published = 1
					AND ri.cid = ' . ( int ) $intCid . '
					AND ( ' . $strProductIdsCondition . ' )
					' . $strAdminCondition . '
					' . $strCountryCodeCondition . '

				UNION

				SELECT
					m.id,
					rg.cid,
					m.default_cid,
					rg.parent_module_id,
					m.name,
					util_get_translated( \'name\', rg.name, rg.details ) AS title,
					m.url,
					m.description,
					m.has_write_operation,
					m.is_public,
					m.allow_only_admin,
					m.allow_only_psi_admin,
					m.is_hidden,
					m.options::text,
					m.order_num,
					m.updated_by,
					m.updated_on,
					m.created_by,
					m.created_on,
					NULL::INTEGER AS report_instance_id,
					rg.position_number AS order_num_final,
					m.has_write_operation
				FROM
					report_groups rg
					JOIN modules m ON m.id = rg.module_id
					LEFT JOIN product_modules pm ON pm.module_id = m.id
				WHERE
					m.is_published = 1
					AND rg.cid = ' . ( int ) $intCid . '
					AND rg.report_group_type_id NOT IN ( ' . CReportGroupType::MY_REPORTS . ', ' . CReportGroupType::PACKET . ', ' . CReportGroupType::QUICK_LINK . ' )
					AND ( ' . $strProductIdsCondition . ' )
					' . $strAdminCondition . '
					' . $strCountryCodeCondition . '

				/* Include report instance modules added under 3.0 report framework. */
				UNION

				SELECT
					m.id,
					ri.cid AS cid,
					m.default_cid,
					ri.module_id,
					m.name,
					util_get_translated( \'name\', ri.name, ri.details ) AS title,
					m.url,
					m.description,
					m.has_write_operation,
					m.is_public,
					m.allow_only_admin,
					m.allow_only_psi_admin,
					m.is_hidden,
					m.options::text,
					m.order_num,
					m.updated_by,
					m.updated_on,
					m.created_by,
					m.created_on,
					ri.id AS report_instance_id,
					m.order_num AS order_num_final,
					m.has_write_operation
				FROM
					report_new_instances ri
					JOIN modules m ON m.id = ri.module_id
					LEFT JOIN product_modules pm ON pm.module_id = m.id
				WHERE
					ri.cid = ' . ( int ) $intCid . ' 
					AND ri.deleted_by IS NULL
					AND m.is_published = 1
					AND ( ' . $strProductIdsCondition . ' )
					' . $strAdminCondition . '
					' . $strCountryCodeCondition . '
			) m
			ORDER BY
				m.parent_module_id ASC NULLS FIRST,
				m.order_num_final,
				m.id';

		return self::fetchModules( $strSql, $objDatabase );
	}

	public static function fetchModuleByModuleName( $strModuleName, $objDatabase ) {
		if( true == is_null( $strModuleName ) ) return NULL;
		return self::fetchModule( 'SELECT * FROM modules WHERE name = \'' . addslashes( $strModuleName ) . '\'', $objDatabase );
	}

	public static function fetchNestedModules( $objDatabase, $arrintModuleIdsToExclude = NULL, $strWhereCondition = '' ) {
		$strSql = 'SELECT
						*
					FROM
						modules
					WHERE
						is_published = 1'
						. ( true == valArr( $arrintModuleIdsToExclude ) ? ' AND id NOT IN ( ' . implode( ',', $arrintModuleIdsToExclude ) . ' )' : '' ) . '
						AND ( is_public = 0 OR is_public IS NULL )
						AND id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . '
						' . $strWhereCondition . '
					ORDER BY
						order_num,
						id ASC';

		$arrobjModules = self::fetchModules( $strSql, $objDatabase );

		if( true == valArr( $arrobjModules ) ) {
			foreach( $arrobjModules as $objModule ) {
				if( true == is_numeric( $objModule->getParentModuleId() ) && true == isset( $arrobjModules[$objModule->getParentModuleId()] ) && true == valObj( $arrobjModules[$objModule->getParentModuleId()], 'CModule' ) ) {
					$arrobjModules[$objModule->getParentModuleId()]->addModule( $objModule );
				}
			}
			return $arrobjModules;
		} else {
			return false;
		}
	}

	public static function fetchCustomModulesByNames( $arrstrModuleNames, $objDatabase ) {
		if( false == valArr( $arrstrModuleNames ) ) return NULL;

		$strModuleNames = '';

		// @TODO: Instead of below loop & rtrim use this: ( \'' . implode( '\', \'', $arrstrModuleNames ) . '\' )'
		foreach( $arrstrModuleNames as $strModuleName ) {
			$strModuleNames .= '\'' . $strModuleName . '\',';
		}

		$strModuleNames = rtrim( $strModuleNames, ',' );

		return self::fetchModules( 'SELECT * FROM modules WHERE name IN ( ' . $strModuleNames . ' )', $objDatabase );
	}

	public static function fetchDefaultModulesByParentModuleId( $intParentModuleId, $objDatabase ) {
		$strSqlParentCondition = ( $intParentModuleId > 0 ) ? ' AND parent_module_id = ' . ( int ) $intParentModuleId : ' AND parent_module_id IS NULL';
		return self::fetchModules( 'SELECT * FROM modules WHERE id < ' . CModule::PLACEHOLDER_MODULE_THRESHOLD . $strSqlParentCondition . ' ORDER BY order_num', $objDatabase );
	}

	public static function fetchModulesByIds( $arrintModuleIds, $objDatabase ) {
		if( false == valArr( $arrintModuleIds ) ) return NULL;
		return self::fetchModules( 'SELECT * FROM modules WHERE id IN ( ' . implode( ',', $arrintModuleIds ) . ' ) ORDER BY order_num', $objDatabase );
	}

	public static function fetchParentModuleIdsByModuleIds( $arrintModuleIds, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT( parent_module_id )
					FROM
						modules
					WHERE
						parent_module_id IN( ' . implode( ',', $arrintModuleIds ) . ' )';

		$arrintParentModules = fetchData( $strSql, $objDatabase );
		return rekeyArray( 'parent_module_id', $arrintParentModules );
	}

	public static function fetchParentModuleIdByModuleId( $intModuleId, $objDatabase ) {
		$strSql = 'SELECT
						parent_module_id
					FROM
						modules
					WHERE
						id =' . ( int ) $intModuleId;

		$arrintParentModule = fetchData( $strSql, $objDatabase );
		return $arrintParentModule[0]['parent_module_id'];
	}

	public static function fetchDefaultModuleMaxOrderNumByParentModuleId( $intParentModuleId, $objDatabase ) {
		$strSqlParentCondition = ( $intParentModuleId > 0 ) ? 'parent_module_id = ' . ( int ) $intParentModuleId : 'parent_module_id IS NULL';

		$strSql = 'SELECT
						MAX( order_num ) AS order_num
					FROM
						modules
					WHERE
						' . $strSqlParentCondition;

		$arrintMaxOrderNum = fetchData( $strSql, $objDatabase );
		$intMaxOrderNum = ( true == valArr( $arrintMaxOrderNum ) ) ? $arrintMaxOrderNum[0]['order_num'] : 0;

		return $intMaxOrderNum;

	}

	public static function fetchDefaultModuleMaxId( $objDatabase ) {
		$strSql = 'SELECT
						MAX( id ) AS id
					FROM
						modules
					WHERE
						id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultPublishedModules( $objDatabase, $strOrderBy = 'name', $boolIsFromDefaultGroup = false ) {

		$strWhere = ( $boolIsFromDefaultGroup == true ) ? ' AND allow_only_psi_admin = 0 AND allow_only_admin = 0' : '';
		$strSql   = 'SELECT 
                          *
                     FROM 
                         modules 
                     WHERE 
                         id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . ' 
                         AND is_published = 1 ' . $strWhere . ' 
					 ORDER BY ' . $strOrderBy;

		return self::fetchModules( $strSql, $objDatabase );
	}

	public static function fetchTopLevelModulesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return [];

		$strSql = 'SELECT
						DISTINCT ON( m.id )
						m.*
					FROM
						modules m
						JOIN product_modules pm ON ( pm.module_id = m.id )
						JOIN property_products pp ON ( ( pp.cid = ' . ( int ) $intCid . ' AND pp.ps_product_id = pm.ps_product_id ) OR pp.ps_product_id IS NULL OR pm.ps_product_id IS NULL)
					WHERE
						m.is_published = 1
						AND m.id NOT IN ( ' . CModule::PROPERTY_SYSTEM . ' , ' . CModule::WEBSITE_SYSTEM . ' , ' . CModule::HELP_SYSTEM . ' )
						AND m.parent_module_id IS NULL
						AND ( pp.property_id IS NULL OR pp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
						AND m.is_hidden != 1
					ORDER BY
						m.id,
						order_num';

		return self::fetchModules( $strSql, $objDatabase );
	}

	public static function fetchSimpleReportModulesByModuleNamesByCid( $arrstrModuleNames, $intCid, $objDatabase, $boolSpecificLifetime = false, $boolGetCached = false ) {

		if( false == valArr( $arrstrModuleNames ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( r.id )
						m.id,
						m.name,
						util_get_system_translated( \'title\', m.title, m.details ) AS title,
						m.url,
						r.id AS report_id,
						r.report_type_id,
						ri.id as report_instance_id
					FROM
						modules m
						JOIN report_instances ri ON ( ri.module_id = m.id AND ri.cid = ' . ( int ) $intCid . ' )
						JOIN reports r ON ( r.cid = ri.cid AND r.id = ri.report_id AND r.cid = ' . ( int ) $intCid . ' )
					WHERE
						( r.cid = ' . ( int ) CClients::$c_intNullCid . ' OR r.cid = ' . ( int ) $intCid . ' )
						AND m.name IN ( \'' . implode( '\', \'', $arrstrModuleNames ) . '\' )
						AND ri.deleted_by IS NULL';

		$strCacheKey = $strSql . date( 'm/d/Y' );

		if( true == $boolGetCached ) {
			return fetchOrCacheData( $strSql, $boolSpecificLifetime, $strCacheKey . $objDatabase->getClusterId(), $objDatabase );
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchSimpleSiteTabletReportModulesByModuleNamesByTabNameByCid( $arrstrModuleNames, $strTabName, $intCid, $objDatabase, $boolSpecificLifetime = false, $boolGetCached = false ) {
		if( false == valArr( $arrstrModuleNames ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT m.id,
						m.name,
						util_get_system_translated( \'title\', m.title, m.details ) AS title,
						m.url,
						m.order_num,
						ri.parent_module_id AS report_group_module_id,
						ri.report_id AS report_id
					FROM
						modules m
						JOIN report_instances ri ON ( m.id = ri.module_id AND ri.cid = ' . ( int ) $intCid . ' )
						JOIN report_groups rg ON ( ri.parent_module_id = rg.module_id AND rg.cid = ' . ( int ) $intCid . ' )
					WHERE
						( COALESCE( ri.id, rg.id ) IS NOT NULL OR m.id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . ' )
						AND m.name IN ( \'' . implode( '\', \'', $arrstrModuleNames ) . '\' )
						AND rg.parent_module_id = ( SELECT id FROM modules WHERE name LIKE \'' . $strTabName . '\' )';

		$strCacheKey = $strSql . date( 'm/d/Y' );

		if( true == $boolGetCached ) {
			return fetchOrCacheData( $strSql, $boolSpecificLifetime, $strCacheKey . $objDatabase->getClusterId(), $objDatabase );
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchSimpleSystemModules( CDatabase $objDatabase ) {

		// WE are excluding all report groups and report instances from permissions as we are taking if from fetchSimpleModulesByCid
		// We still need inbox, history, saved filters, packet and scheduled report modules form this function.
		$strReportCategories = '\'' . CModule::ACCOUNTING_REPORT_TAB . '\', \'' . CModule::RESIDENTS_REPORT_TAB . '\', \'' . CModule::LEADS_REPORT_TAB . '\', \'' . CModule::MARKETING_REPORT_TAB . '\', \'' . CModule::MAINTENANCE_REPORT_TAB . '\', \'' . CModule::PRICING_REPORT_TAB . '\', \'' . CModule::PAYMENTS_REPORT_TAB . '\',
								\'' . CModule::UTILITIES_REPORT_TAB . '\', \'' . CModule::COMMUNICATIONS_REPORT_TAB . '\', \'' . CModule::SYSTEM_REPORT_TAB . '\', \'' . CModule::PROPERTIES_REPORT_TAB . '\', \'' . CModule::INTEGRATIONS_REPORT_TAB . '\', \'' . CModule::WEBSITES_REPORT_TAB . '\', \'' . CModule::TAX_CREDITS_REPORT_TAB . '\'';
		$strReportModuleIdsSql = '
			SELECT
				id
			FROM
				modules
			WHERE 
				parent_module_id IN (
					SELECT id
					FROM modules
					WHERE name IN ( ' . $strReportCategories . ' )
			)
		UNION ALL
			SELECT id
			FROM modules
			where parent_module_id IN (
				SELECT id
				FROM modules
				where parent_module_id IN (
					SELECT id
					FROM modules
					where name IN (  ' . $strReportCategories . ' )
				)
			)';

		$strSql = 'SELECT
						m.id,
						m.parent_module_id,
						m.name,
						m.has_write_operation,
						util_get_system_translated( \'title\', m.title, m.details ) as title, 
						m.url,
						m.is_public,
						m.is_hidden,
						m.allow_only_admin,
						m.allow_only_psi_admin,
						array_to_string( array_agg( DISTINCT pm.ps_product_id || \'-\' || COALESCE( pm.ps_product_option_id, 0 ) ), \',\' ) as ps_product_ids,
						m.options::text,
						m.is_action
					FROM
						modules m
						LEFT JOIN product_modules pm ON pm.module_id = m.id
					WHERE
						m.id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . '
						AND m.is_published = 1
						/* Exclude all report groups and report instance modules, Instead of CModule::REPORT_SYSTEM we can add only report tabs modules id */
						AND m.id NOT IN ( ' . $strReportModuleIdsSql . ' )
					GROUP BY
						m.id';

		$strCacheKey = 'SystemModulesCached-' . CLocaleContainer::createService()->getLocaleCode() . ( CLocaleContainer::createService()->getAcceptMachineTranslated() ? '-machine' : '' ) . '-' . $objDatabase->getClusterId();

		$arrmixModules = fetchOrCacheData( $strSql, $intCacheLifetime = 180, $strCacheKey, $objDatabase );

		if( true == empty( $arrmixModules ) ) {
			$arrmixModules = fetchData( $strSql, $objDatabase );
			CNewRelic::createService()->addCustomParameters( [ 'entrata.debug.emptyLoadModule' => 1 ] );
		}
		return $arrmixModules;
	}

	public static function fetchSimpleModulesByCid( $intCid, $objDatabase, $boolLoadEmptyReportGroups = false ) {
		unset( $boolLoadEmptyReportGroups );
		$strSql = '	SELECT
						m.id,
						COALESCE( ri.parent_module_id, rg.parent_module_id ) AS parent_module_id,
						m.name,
						m.has_write_operation,
						COALESCE( util_get_translated( \'name\', ri.name, ri.details ), util_get_translated( \'name\', rg.name, rg.details ) ) AS title,
						m.url,
						m.is_public,
						m.is_hidden,
						m.allow_only_admin,
						m.allow_only_psi_admin,
						NULL as ps_product_ids,
						m.options::TEXT,
						m.is_action
					FROM
						modules m
						LEFT JOIN report_groups rg ON rg.module_id = m.id AND rg.is_published = 1 AND rg.deleted_by IS NULL AND rg.cid = ' . ( int ) $intCid . '
						LEFT JOIN report_instances ri ON ri.module_id = m.id AND ri.deleted_by IS NULL AND ri.cid = ' . ( int ) $intCid . ' AND ri.id NOT IN (
							SELECT 
								inst.id 
							FROM 
								report_instances inst 
								JOIN report_groups grp ON ( inst.cid = grp.cid AND inst.report_group_id = grp.id ) 
							WHERE 
								inst.cid = ' . ( int ) $intCid . ' 
								AND grp.report_group_type_id IN ( ' . CReportGroupType::MY_REPORTS . ', ' . CReportGroupType::PACKET . ' )
						)
						LEFT JOIN report_versions rv ON rv.cid = ri.cid and rv.id = ri.report_version_id
					WHERE
						COALESCE( ri.id, rg.id ) IS NOT NULL
						AND COALESCE( ri.parent_module_id, rg.parent_module_id ) NOT IN ( ' . CModule::REPORT_PACKET_MODULE . ', ' . CModule::SCHEDULE_REPORT_MODULE . ' )
						AND CASE
							WHEN ((rv.definition ->> \'validation_callbacks\')::json ->> \'isAllowedClient\')::TEXT IS NOT NULL THEN rv.cid = ANY ((\'{ \' || trim(both \'[]\'
								FROM ((rv.definition ->> \'validation_callbacks\')::json ->> \'isAllowedClient\')::TEXT) || \' }\')::INT [ ])
							WHEN ((rv.definition ->> \'validation_callbacks\')::json ->> \'isAllowedCid\')::TEXT IS NOT NULL THEN rv.cid = ANY ((\'{ \' || trim(both \'[]\'
								FROM ((rv.definition ->> \'validation_callbacks\')::json ->> \'isAllowedCid\')::TEXT) || \' }\')::INT [ ])
							ELSE TRUE
						END
						AND COALESCE( ( rv.definition->>\'is_published\' )::BOOLEAN, TRUE ) = TRUE
						AND COALESCE ( rv.expiration, NOW ( ) ) >= NOW () ';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSystemModuleIds( $objDatabase ) {

		$strSql = 'SELECT
						m.id,
						m.name
					FROM
						modules m
					WHERE
						m.id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . '
						AND m.is_published = 1';

		$arrmixModules = ( array ) fetchOrCacheData( $strSql, $intCacheLifetime = 180, 'SystemModuleIdsCached' . $objDatabase->getClusterId(), $objDatabase );

		if( true == empty( $arrmixModules ) ) {
			$arrmixModules = ( array ) fetchData( $strSql, $objDatabase );
			CNewRelic::createService()->addCustomParameters( [ 'entrata.debug.emptyLoadModuleIdByModuleName' => 1 ] );
		}

		$arrmixReKeyedModules = [];

		foreach( $arrmixModules as $arrmixModule ) {
			$arrmixReKeyedModules[$arrmixModule['name']] = $arrmixModule['id'];
		}

		return $arrmixReKeyedModules;
	}

	public static function fetchReportModuleByReportInstanceId( $intReportInstanceId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				m.*,
				r.id AS report_id,
				ri.id AS report_instance_id
			FROM
				report_instances ri
				JOIN reports r ON r.cid = ri.cid AND r.id = ri.report_id
				JOIN modules m ON m.id = ri.module_id
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND ri.id = ' . ( int ) $intReportInstanceId;

		return self::fetchModule( $strSql, $objDatabase );
	}

	public static function fetchModuleIdByName( $strName, $objDatabase, $boolCheckIfPublished = false ) {

		$intModuleId = NULL;

		$strSql = 'SELECT
						id
					FROM
						modules
					WHERE
						name = \'' . $strName . '\'';

		// This is added temporarily to show/hide group checklist functionality depending on module is published or not
		// Once the module gets published on live we can remove this temporary condition
		if( true == $boolCheckIfPublished ) {
			$strSql .= ' AND is_published = 1';
		}

		$arrintModuleId = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintModuleId ) ) {
			$intModuleId = $arrintModuleId[0]['id'];
		}

		return $intModuleId;
	}

	public static function fetchModuleIdByNameByAction( $strName, $strAction = NULL, $objDatabase ) {
		$strWhereCondition = '';
		if( true == valStr( $strAction ) ) {
			$strWhereCondition = ' actions && \'{' . addslashes( $strAction ) . '}\'::TEXT[] OR ';
		}

		$strWhereCondition = '( ' . $strWhereCondition . ' actions IS NULL )';

		$strSql = 'SELECT
						id
					FROM
						modules
					WHERE
						name = \'' . addslashes( $strName ) . '\'
						AND ' . $strWhereCondition . '
						AND is_published = 1
					ORDER BY
						actions ASC NULLS LAST
					LIMIT 1';

		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchDefaultPublicOrHiddenModuleIds( $objDatabase ) {
		$strSql = 'SELECT
						id
					FROM
						modules m
					WHERE
						m.id < ' . CModule::PLACEHOLDER_MODULE_THRESHOLD . '
						AND (
								m.is_public = 1
								OR m.is_hidden = 1
							)
						AND m.is_published = 1';

		$arrintModulesData = fetchData( $strSql, $objDatabase );

		$arrintModuleIds = [];

		if( true == valArr( $arrintModulesData ) ) {
			foreach( $arrintModulesData as $arrintModuleData ) {
				$arrintModuleIds[$arrintModuleData['id']] = $arrintModuleData['id'];
			}
		}

		return $arrintModuleIds;
	}

	public static function fetchPublishedModules( $objDatabase ) {

		$strSql = ' SELECT
						 *
					FROM
						modules m
					WHERE
						m.is_hidden = 0
						AND m.allow_only_psi_admin = 0
						AND m.is_published = 1';

		return self::fetchModules( $strSql, $objDatabase );
	}

	public static function fetchPublishedModulesByCid( $intCid, $objDatabase ) {

		$strSql = ' SELECT
						COALESCE( title, description, name ) AS module_name,
						id,
						name
					FROM
						modules
					WHERE
						is_hidden = 0
						AND allow_only_psi_admin = 0
						AND is_published = 1
						AND id < ' . CModule::PLACEHOLDER_MODULE_THRESHOLD . '

					UNION ALL

					SELECT
						COALESCE( util_get_translated( \'name\', rg.name, rg.details ), util_get_translated( \'name\', ri.name, ri.details ) ) AS module_name,
						m.id,
						NULL
					FROM
						modules m
						LEFT JOIN report_groups rg ON( rg.module_id = m.id AND rg.cid = ' . ( int ) $intCid . ' )
						LEFT JOIN report_instances ri ON( ri.module_id = m.id AND ri.cid = ' . ( int ) $intCid . ' )
					WHERE
						m.is_hidden = 0 AND
						m.allow_only_psi_admin = 0 AND
						m.is_published = 1
						AND ( rg.id IS NOT NULL OR ri.id IS NOT NULL )
						AND m.id >= ' . CModule::PLACEHOLDER_MODULE_THRESHOLD;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublishedModulesByParentModuleIdByIsHidden( $intParentModuleId, $intIsHidden, $objDatabase, $boolFetchCachedData = false, $arrintSkipModulesFromResult = [] ) {

		$strSkipModuleConditionSql = '';
		if( valArr( $arrintSkipModulesFromResult ) ) {
			$strSkipModuleConditionSql = ' AND m.id NOT IN ( ' . sqlIntImplode( $arrintSkipModulesFromResult ) . ' ) ';
		}
		$strSql = ' SELECT
						m.id,
						m.name,
						util_get_system_translated( \'title\', m.title, m.details ) AS title,
						m.url
					FROM
						modules m
					WHERE
						parent_module_id = ' . ( int ) $intParentModuleId . '
						AND is_hidden = ' . ( int ) $intIsHidden . '
						' . $strSkipModuleConditionSql . '
						AND is_published = 1
					ORDER BY
						order_num';
		if( true == $boolFetchCachedData ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return ( array ) fetchOrCacheData( $strSql, $intCacheLifetime = 9000, 'PublishedModulesByVisibility' . $objDatabase->getClusterId(), $objDatabase );
		}
	}

	public static function fetchPublishedModulesByParentModuleIdsByIsHidden( $arrintParentModuleIds, $intIsHidden, $objDatabase, $boolFetchSystemModules = true ) {
		$strWhereClause = ( true === $boolFetchSystemModules )? 'AND id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD : '';

		$strSql = ' SELECT
						m.id,
						m.name,
						m.url,
						util_get_system_translated( \'title\', m.title, m.details ) AS title,
						m.parent_module_id
					FROM
						modules m
					WHERE
						parent_module_id IN ( ' . sqlIntImplode( $arrintParentModuleIds ) . ' )
						AND is_hidden = ' . ( int ) $intIsHidden . '
						AND is_published = 1 ' . $strWhereClause . '
					ORDER BY
						order_num,
						m.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRecursiveModulesWithFieldNamesByParentModuleId( $arrstrSelectFields, $intParentModuleId, $objDatabase, $strLevelPadding = '&nbsp;' ) {

		if( false == valId( $intParentModuleId ) ) return [];

		$arrstrSelectFields = array_merge( [ 'id', 'parent_module_id', 'name' ], $arrstrSelectFields );

		$strSelectFieldsSql = implode( ',', $arrstrSelectFields );
		$strRecursiveSelectFieldsSql = 'm.' . implode( ',m.', $arrstrSelectFields );

		$strSql = 'WITH RECURSIVE modules_tree( ' . $strSelectFieldsSql . ', level, sort ) AS (
						SELECT '
							. $strSelectFieldsSql . ',
							1 AS LEVEL,
							order_Num::text
						FROM
							modules
						WHERE
							id = ' . ( int ) $intParentModuleId . '
							AND id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . '

						UNION ALL

						SELECT '
							. $strRecursiveSelectFieldsSql . ',
							LEVEL + 1 AS LEVEL,
							CASE WHEN LEVEL < 1 THEN m.order_num::text
							  ELSE ( sort::text || \'|\' || m.order_num )
							END::text AS order_num
						FROM
							modules m,
							modules_tree mt
						WHERE
							m.parent_module_id = mt.id
							AND m.id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . '
							AND mt.id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . '
						)

						SELECT '
							. $strSelectFieldsSql . ',
							LPAD( \'' . str_repeat( $strLevelPadding, 2 ) . '\', LEVEL * 6 ) || NAME AS name,
							LEVEL AS depth
						FROM
							modules_tree
						ORDER BY
							(
							  SELECT
								  ARRAY_AGG( matches [ 1 ] ::numeric )
							  FROM
								  REGEXP_MATCHES( modules_tree.sort, $$(\d+)$$,  \'g\' ) AS matches /* Creating an array of all the module_ids, we then use it for sorting the sequence of modules */
							 )';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRecursiveModulesWithFieldNames( $arrmixComparisionFields, $objDatabase ) {

		$strComparisionFields = 'id';
		if( false != valArr( $arrmixComparisionFields ) ) {
			$strComparisionFields = implode( ',', $arrmixComparisionFields );
		}

		$strRecursiveSelectFieldsSql = 'm.' . implode( ',m.', $arrmixComparisionFields );
		$strRecursiveSelectFieldsForTreeSql = 'mt.' . implode( ',mt.', $arrmixComparisionFields );

		$strSql = 'WITH RECURSIVE modules_tree( ' . $strComparisionFields . ' , level, module_order, sort ) AS (
						SELECT
							' . $strComparisionFields . ',
							1 AS LEVEL,
							title::text AS module_order,
							order_Num::text
						FROM
							modules
						WHERE
							parent_module_id IS NULL
							AND id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . '

						UNION ALL

						SELECT
							' . $strRecursiveSelectFieldsSql . ',
							LEVEL + 1 AS LEVEL,
							CASE WHEN LEVEL > 1
								THEN module_order || \' > \' ||  mt.title
								ELSE module_order
							END AS module_order,
							CASE WHEN LEVEL < 1 THEN m.order_num::text
							  ELSE ( sort::text || \'|\' || m.order_num )
							END::text AS order_num
						FROM
							modules m,
							modules_tree mt
						WHERE
							m.parent_module_id = mt.id
							AND m.id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . '
							AND mt.id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . '
						)

						SELECT
							' . $strRecursiveSelectFieldsForTreeSql . ',
							df.validation_callback_function,
							module_order
						FROM
							modules_tree mt
							LEFT JOIN default_reports df ON ( df.module_id = mt.id )
						ORDER BY
							(
							SELECT
								ARRAY_AGG( matches [ 1 ] ::numeric )
							FROM
								REGEXP_MATCHES( mt.sort, $$(\d+)$$,  \'g\' ) AS matches /* Creating an array of all the module_ids, we then use it for sorting the sequence of modules */
							)';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchModuleIdByNames( $arrstrModuleNames, $objDatabase ) {

		if( false == valArr( $arrstrModuleNames ) ) return false;

		$strSql = 'SELECT
						id,
						name
					FROM
						modules
					WHERE
						name IN ( \'' . implode( '\', \'', $arrstrModuleNames ) . '\' )';

		$arrmixModules = fetchData( $strSql, $objDatabase );

		return $arrmixModules;
	}

	public static function fetchDefaultReportModulesByParentModuleId( $intParentModuleId, $objDatabase ) {

		$strSql = '
			SELECT
				DISTINCT m.name,
				util_get_system_translated( \'title\', m.title, m.details ) AS title,
				m.id,
				m.is_published,
				m.order_num
			FROM
				modules m
				JOIN default_report_groups drg ON ( m.id = drg.parent_module_id )
			WHERE
				m.id < ' . CModule::PLACEHOLDER_MODULE_THRESHOLD . '
				AND m.parent_module_id = ' . ( int ) $intParentModuleId . '
			ORDER BY
				m.order_num';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedLatestUpdatedModules( $objDatabase, $objPagination, $boolIsCount = false ) {

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strOrderByType = ( 0 == $objPagination->getOrderByType() ) ? ' ASC ' : ' DESC ';
			$intOffset = ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
			$intLimit = ( int ) $objPagination->getPageSize();
		}

		if( true == $boolIsCount ) {
			$strSelectSql = 'SELECT
						count(id) ';
		} else {
			$strSelectSql = 'SELECT
						*';
		}

		$strSql = 'WITH RECURSIVE module_list AS ( 
				                        SELECT
				                            id,
				                            name,
				                            util_get_system_translated( \'title\', title, details ) AS title,
				                            updated_on,
				                            concat ( title, \'\' ) AS PATH
				                        FROM
				                            modules
				                        WHERE
				                            id < ' . CModule::PLACEHOLDER_MODULE_THRESHOLD . '
				                            AND parent_module_id IS NULL
				                        UNION ALL
				                        SELECT
				                            m.id,
				                            m.name,
				                            util_get_system_translated( \'title\', m.title, m.details ) AS title,
				                            m.updated_on,
				                            concat ( module_list.path, \' >> \', util_get_system_translated( \'title\', m.title, m.details ) ) AS PATH
				                        FROM
				                            modules m
				                            JOIN module_list ON module_list.id = m.parent_module_id)
		
				' . $strSelectSql . ' 
				FROM
				    module_list m
				WHERE
				    m.updated_on >= ( CURRENT_DATE - INTERVAL \'2\' MONTH )';

		if( false == $boolIsCount && true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' ORDER BY 
							' . $objPagination->getOrderByField() . ' ' . $strOrderByType .
			           ' OFFSET ' . ( int ) $intOffset .
			           ' LIMIT ' . ( int ) $intLimit;
		}

		$arrstrResultSet = fetchData( $strSql, $objDatabase );
		return ( false == $boolIsCount ) ? $arrstrResultSet : $arrstrResultSet[0]['count'];
	}

	public static function migrateCompanyUserAndGroupPermissions( $intModuleId, $intParentModuleId, $objDatabase, $intCid = NULL, $boolReturnSqlOnly = false ) {

		if( true == is_null( $intCid ) ) {
			$intCid = CClients::$c_intNullCid;
		}

		$strSql = 'SELECT
						*
					FROM
						func_migrate_company_user_and_company_group_permissions( ' . ( int ) $intModuleId . ',' . ( int ) $intParentModuleId . ', ' . ( int ) $intCid . '); ';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		$objDataset = $objDatabase->createDataset();
		if( false == $objDataset->execute( $strSql ) ) {

// 			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert to permissions. The following error was reported.' ) );
// 			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objAdminDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public static function migratePsiUserAndGroupModulePermissions( $intModuleId, $intParentModuleId, $intUserId, $objAdminDatabase ) {

		$objDataset = $objAdminDatabase->createDataset();

		$strSql = 'SELECT
						*
					FROM
						func_migrate_psi_user_and_group_module_permissions( ' . ( int ) $intModuleId . ',' . ( int ) $intParentModuleId . ',' . ( int ) $intUserId . ') ';

		if( false == $objDataset->execute( $strSql ) ) {

// 			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert to permissions. The following error was reported.' ) );
// 			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objAdminDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}
		$objDataset->cleanup();

		return true;
	}

	public static function fetchModulesByPropertySettingGroupIds( $arrintPropertySettingGroupIds, $objDatabase ) {
		if( false == valArr( $arrintPropertySettingGroupIds ) ) return NULL;

		$strSql = ' WITH RECURSIVE property_setting_modules(
						module_id,
						parent_module_id,
						property_setting_group_id,
						module_name,
						ps_product_id,
						ps_product_option_id,
						order_num
					) AS (
						SELECT
							m.id AS module_id,
							m.parent_module_id,
							psg.id AS property_setting_group_id,
							m.name AS module_name,
							pm.ps_product_id,
							pm.ps_product_option_id,
							1 AS order_num
						FROM
							modules m
							JOIN property_setting_groups psg ON ( m.id = psg.module_id AND m.is_published = 1 )
							LEFT JOIN product_modules pm ON ( pm.module_id = m.id )
						WHERE
							psg.id IN ( ' . implode( ', ', $arrintPropertySettingGroupIds ) . ' )

						UNION ALL

						SELECT
							m.id AS module_id,
							m.parent_module_id,
							psm.property_setting_group_id,
							m.name AS module_name,
							pm.ps_product_id,
							pm.ps_product_option_id,
							psm.order_num + 1 AS order_num
						FROM
							property_setting_modules psm
							JOIN modules m on ( psm.parent_module_id = m.id )
							LEFT JOIN product_modules pm ON ( pm.module_id = m.id )
						WHERE
							m.id <> ' . CModule::SETUP_PROPERTIES_BETA . '
					)

					SELECT
						subq.module_id,
						subq.parent_module_id,
						subq.module_name,
						json_strip_nulls( json_object_agg( COALESCE( subq.ps_product_id, 0 ), subq.ps_product_ids ) ) AS ps_product_ids,
						MAX( subq.order_num ) AS order_num
					FROM
						(
						SELECT
							psm.module_id,
							psm.parent_module_id,
							psm.module_name,
							psm.ps_product_id,
							CASE WHEN psm.ps_product_id IS NOT NULL THEN array_agg( DISTINCT psm.ps_product_option_id ) ELSE NULL END AS ps_product_ids,
							MAX( psm.order_num ) as order_num
						FROM
							property_setting_modules psm
						GROUP BY
							psm.module_id,
							psm.parent_module_id,
							psm.module_name,
							psm.ps_product_id
						) AS subq
					GROUP BY
						subq.module_id,
						subq.parent_module_id,
						subq.module_name
					ORDER BY
						MAX( subq.order_num ) desc';

		$arrmixModuleDetails = fetchData( $strSql, $objDatabase );

		$arrmixValidationModuleDetails = [];
		if( false == empty( $arrmixModuleDetails ) ) {
			foreach( $arrmixModuleDetails as $arrmixModuleDetail ) {
				$arrmixValidationModuleDetails[$arrmixModuleDetail['module_id']] = $arrmixModuleDetail;
				$arrmixValidationModuleDetails[$arrmixModuleDetail['module_id']]['ps_product_ids'] 		= json_decode( $arrmixModuleDetail['ps_product_ids'], true );
			}
		}

		return $arrmixValidationModuleDetails;

	}

	public static function fetchPropertySettingKeysByKeywordOrModuleId( $objAdminDatabase, $objClientDatabase, $arrintParentModuleIds, $strSearchKeyword = NULL, $arrintModuleIds = NULL, $intQuestionTypeId ) {

		$intParentModuleId = ( int ) $arrintModuleIds['parent_module_id'];

		$strWhereCondition 				= ' WHERE ';
		$strModuleListWhereCondition 	= '';

		if( CIpsChecklistQuestionType::PROPERTY == $intQuestionTypeId ) {
			$intModuleId 					= CModule::SETUP_PROPERTIES_BETA;
			$strKeyTableName 				= 'property_setting_keys';
			$strGroupTableName 				= 'property_setting_groups';
			$strKeyTableAlias 				= 'psk';
			$strGroupTableAlias 			= 'psg';
			$strTableGroupId 				= 'property_setting_group_id';
			$strWhereCondition 				.= ' psk.is_implementation_required = true AND psk.is_disabled = FALSE AND ';
			$strModuleListWhereCondition 	= ' AND id NOT IN ( ' . CModule::SETUP_PROPERTIES_BETA_ADD_EDIT_PROPERTY . ',' . CModule::SETUP_PROPERTIES_BETA_SETUP . ',' . CModule::SETUP_PROPERTIES_BETA_SETTINGS_TEMPLATES . ' )';
		} elseif( CIpsChecklistQuestionType::WEBSITE == $intQuestionTypeId ) {
			$intModuleId 			= CModule::SETUP_WEBSITES_BETA;
			$strKeyTableName 		= 'website_setting_keys';
			$strKeyTableAlias 		= 'wsk';
			$strGroupTableName 		= 'website_setting_groups';
			$strGroupTableAlias 	= 'wsg';
			$strTableGroupId 		= 'website_setting_group_id';
		} elseif( CIpsChecklistQuestionType::COMPANY == $intQuestionTypeId ) {
			$intModuleId 			= CModule::COMPANY_SETUP;
			$strKeyTableName 		= 'company_setting_keys';
			$strKeyTableAlias 		= 'csk';
			$strGroupTableName 		= 'company_setting_groups';
			$strGroupTableAlias 	= 'csg';
			$strTableGroupId 		= 'company_setting_group_id';
		}
		if( true == valStr( $strSearchKeyword ) ) {
			$strWhereCondition .= '
			( CAST ( ' . $strKeyTableAlias . '.id AS TEXT ) LIKE \'%' . addslashes( trim( ( string ) $strSearchKeyword ) ) . '%\'
			OR ' . $strKeyTableAlias . '.key ILIKE\'%' . addslashes( trim( ( string ) $strSearchKeyword ) ) . '%\'
			OR ' . $strKeyTableAlias . '.label ILIKE\'%' . addslashes( trim( ( string ) $strSearchKeyword ) ) . '%\')';
		} elseif( true == valId( $intParentModuleId ) ) {
			$strWhereCondition .= ' module_list.parent_module_id = ' . $intParentModuleId;
		} elseif( true == valArr( $arrintParentModuleIds ) ) {
			$strWhereCondition .= ' module_list.parent_module_id IN(' . implode( ',', $arrintParentModuleIds ) . ')';
		} elseif( true == valArr( $arrintModuleIds ) ) {
			$strWhereCondition .= ' module_list.id IN( ' . implode( ',', $arrintModuleIds ) . ')';
		}

		$strSql = 'WITH RECURSIVE 
						module_list AS ( 
							SELECT 
								id,
								parent_module_id,
								util_get_system_translated( \'title\', title, details ) AS module_name,
								url,
								CAST (title As varchar (1000)) as module_path,
								\'Layer1\' as layer
							FROM 
								modules
							WHERE 
								parent_module_id = ' . ( int ) $intModuleId . $strModuleListWhereCondition . '
							UNION ALL
							SELECT 
								m.id,
								m.parent_module_id,
								util_get_system_translated( \'title\', m.title, m.details ) as module_name,
								m.url,
								CAST (ml.module_path || \' > \' || util_get_system_translated( \'title\', m.title, m.details ) As varchar (1000) ) as module_path,
								CASE
									WHEN ml.layer = \'Layer1\'
										THEN \'Layer2\'
									ELSE \'Layer3\'
								END as layer
							FROM 
								modules as m
								JOIN module_list as ml on (m.parent_module_id = ml.id)
					)
		SELECT 
			module_list.id,
			module_list.parent_module_id,
			module_name,
			module_list.module_path,
			module_list.layer,
			' . $strGroupTableAlias . '.name as group_name,
			' . $strKeyTableAlias . '.id as reference_key_id,
			' . $strKeyTableAlias . '.label as psk_label
 		FROM 
 			' . $strKeyTableName . ' AS ' . $strKeyTableAlias . '
			JOIN ' . $strGroupTableName . ' AS ' . $strGroupTableAlias . '  ON ( ' . $strGroupTableAlias . '.id = ' . $strKeyTableAlias . '.' . $strTableGroupId . ' )
			JOIN module_list ON ( ' . $strGroupTableAlias . '.module_id = module_list.id )' . $strWhereCondition . ' ORDER BY ' . $strKeyTableAlias . '.id ';

		$arrmixModuleDetails = rekeyArray( 'reference_key_id', fetchData( $strSql, $objClientDatabase ) );

		if( true == valArr( $arrmixModuleDetails ) ) {
			$arrintReferenceKeyIds	= array_keys( $arrmixModuleDetails );

			$strSql = 'SELECT
							icq.reference_key_id,
							icq.ips_checklist_question_type_id AS question_type_id 
						FROM
							ips_checklist_questions AS icq
							JOIN ips_checklist_question_types AS icqt ON ( icqt.id = icq.ips_checklist_question_type_id AND icqt.is_published = TRUE )
						WHERE
							icq.reference_key_id IN ( ' . sqlIntImplode( $arrintReferenceKeyIds ) . ' )
							AND icq.ips_checklist_question_type_id = ' . ( int ) $intQuestionTypeId . '
							AND	icq.deleted_by IS NULL';

			$arrintAssociatedReferenceKeyIds = array_keys( rekeyArray( 'reference_key_id', fetchData( $strSql, $objAdminDatabase ) ) );

			array_walk( $arrmixModuleDetails, function( &$arrstrValues, $intReferenceKeyId ) use ( $arrintAssociatedReferenceKeyIds ) {
				$arrstrValues['associated_to_question'] = ( true == in_array( $intReferenceKeyId, $arrintAssociatedReferenceKeyIds ) ) ? true : false;
			} );
		}

		return $arrmixModuleDetails;
	}

	public static function fetchPropertySettingsModules( $intParentModuleId, $objDatabase ) {

		$strWhereCondition = ' parent_module_id = ' . ( int ) $intParentModuleId;

		if( CModule::SETUP_PROPERTIES_BETA == $intParentModuleId ) {
			$strWhereCondition .= ' AND id NOT IN ( ' . CModule::SETUP_PROPERTIES_BETA_ADD_EDIT_PROPERTY . ',' . CModule::SETUP_PROPERTIES_BETA_SETUP . ',' . CModule::SETUP_PROPERTIES_BETA_SETTINGS_TEMPLATES . ' )';
		}

		$strSql = 'WITH RECURSIVE 
				module_list AS ( 
					SELECT 
						id,
						parent_module_id,
						util_get_system_translated( \'title\', title, details ) as module_name,
						url,
						CAST (title As varchar (1000)) as module_path,
						\'Layer1\' as layer
					FROM 
						modules
					WHERE ' . $strWhereCondition . '
						
					UNION ALL
					SELECT 
						m.id,
						m.parent_module_id,
						util_get_system_translated( \'title\', m.title, m.details ) as module_name,
						m.url,
						CAST (ml.module_path || \' > \' || util_get_system_translated( \'title\', m.title, m.details ) As varchar (1000) ) as module_path,
						CASE
							WHEN ml.layer = \'Layer1\'
								THEN \'Layer2\'
							ELSE \'Layer3\'
						END as layer
					FROM 
						modules as m
						JOIN module_list as ml on (m.parent_module_id = ml.id)
			)

		SELECT 
			id,
			parent_module_id,
			module_name,
			module_path,
			layer,
			url 
		FROM 
			module_list';

		$arrmixAllModuleList = [];
		$arrmixModuleDetails = fetchData( $strSql, $objDatabase );

		if( false == empty( $arrmixModuleDetails ) ) {
			foreach( $arrmixModuleDetails as $arrmixModuleDetail ) {
				$arrmixAllModuleList[$arrmixModuleDetail['layer']][$arrmixModuleDetail['id']] = $arrmixModuleDetail;
			}
		}

		return $arrmixAllModuleList;
	}

	public static function fetchChildModuleInfoByParentModuleId( $intParentModuleId, $objDatabase ) {

		$strSql = 'WITH RECURSIVE module_list AS ( 
                       SELECT
                           id,
                           unnest ( COALESCE( actions, ARRAY[\'\']::TEXT [ ] ) ) AS action,
                           name
                       FROM
                           modules
                       WHERE
                           id = ' . ( int ) $intParentModuleId . '
                           AND id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . '
                       UNION ALL
                       SELECT
                           m.id,
                           unnest ( COALESCE( m.actions, ARRAY[\'\']::TEXT [ ] ) ) AS action,
                           m.name
                       FROM
                           modules m
                           JOIN module_list ml ON ml.id = m.parent_module_id
                       WHERE
                           m.id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . '
                   )
					SELECT
					   *   
					FROM
	                    module_list';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		return $arrmixResult;
	}

	public static function updateChildModuleInfoByModuleId( $intUserId, $arrintModuleId, $intUnderMaintenance,$objDatabase ) {

		$strSql = 'UPDATE modules
					SET
					    options = jsonb_set ( options::jsonb, \'{under_maintenance_for_database_ids}\'::TEXT [ ], \'' . $intUnderMaintenance . '\'::jsonb ),
					    updated_on = NOW(),
					    updated_by = ' . ( int ) $intUserId . '
					WHERE
					    id IN (' . implode( ',', $arrintModuleId ) . ')';

		$arrmixResult = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrmixResult ) ) {
			return true;
		}
		return false;

	}

}
?>