<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInsurancePolicyCustomers
 * Do not add any new functions to this class.
 */

class CInsurancePolicyCustomers extends CBaseInsurancePolicyCustomers {

	public static function fetchInsurancePolicyCustomersByResidentInsurancePolicyIdsByCids( $arrintResidentInsurancePolicyIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintResidentInsurancePolicyIds ) ) return false;

		$strSql = ' SELECT
						ipc.id as insurance_policy_customer_id,
						ipc.resident_insurance_policy_id as id,
						c.name_first,
						c.name_last,
						c.phone_number,
						c.id as customer_id,
						ipc.lease_id,
						ipc.applicant_id
					FROM
						insurance_policy_customers ipc
						JOIN lease_customers lc ON( ipc.lease_id = lc.lease_id AND ipc.cid = lc.cid )
						JOIN customers c ON(c.id = lc.customer_id AND c.cid = lc.cid )
					WHERE
						ipc.resident_insurance_policy_id IN ( ' . implode( ',', $arrintResidentInsurancePolicyIds ) . ' )
						AND lc.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND ipc.cid IN ( ' . implode( ',', $arrintCids ) . ' )
					ORDER BY
						ipc.id';

		$arrstrData = fetchData( $strSql, $objDatabase );
		$arrstrFinalData = [];

		if( true == valArr( $arrstrData ) ) {
			foreach( $arrstrData as $arrstrChunk ) {
				$arrstrFinalData[$arrstrChunk['id']] = $arrstrChunk;
			}
		}
		return $arrstrFinalData;
	}

	public function fetchInsurancePolicyCustomerByResidentInsurancePolicyIdByCid( $intResidentInsurancePolicyId, $intCid, $objDatabase ) {
		if( is_null( $intResidentInsurancePolicyId ) ) return false;
		return self::fetchInsurancePolicyCustomer( sprintf( 'SELECT * FROM insurance_policy_customers WHERE resident_insurance_policy_id = %d AND cid = %d ORDER BY id DESC LIMIT 1', ( int ) $intResidentInsurancePolicyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInsurancePolicyCustomerByResidentInsurancePolicyIdByCustomerIdByCid( $intResidentInsurancePolicyId, $intCustomerId, $intCid, $objDatabase ) {
		if( is_null( $intResidentInsurancePolicyId ) || is_null( $intCustomerId ) ) {
			return false;
		}

		return self::fetchInsurancePolicyCustomer( sprintf( 'SELECT * FROM insurance_policy_customers WHERE resident_insurance_policy_id = %d AND customer_id = %d AND cid = %d', ( int ) $intResidentInsurancePolicyId, ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAllInsurancePolicyCustomerByResidentInsurancePolicyIdByCid( $intResidentInsurancePolicyId, $intCid, $objDatabase ) {
		if( is_null( $intResidentInsurancePolicyId ) ) return false;
		return self::fetchInsurancePolicyCustomers( sprintf( 'SELECT * FROM insurance_policy_customers WHERE resident_insurance_policy_id = %d AND cid = %d', ( int ) $intResidentInsurancePolicyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInsurancePolicyCustomersByResidentInsurancePolicyIdsByCid( $arrintResidentInsurancePolicyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintResidentInsurancePolicyIds ) ) return false;

		$strSql = 'SELECT
						*
					FROM
						insurance_policy_customers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND resident_insurance_policy_id IN( ' . implode( ',', $arrintResidentInsurancePolicyIds ) . ' )';
		return self::fetchInsurancePolicyCustomers( $strSql, $objDatabase );
	}

	public static function fetchInsurancePolicyCustomersByResidentInsurancePolicyIdByLeaseIdByPropertyIdByCid( $intResidentInsurancePolicyId, $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intResidentInsurancePolicyId ) || false == is_numeric( $intPropertyId ) || false == is_numeric( $intLeaseId ) || false == is_numeric( $intCid ) )  return NULL;

		$strSql = 'SELECT
						ipc.customer_id
					FROM
						insurance_policy_customers ipc
						JOIN resident_insurance_policies rip ON ( ipc.resident_insurance_policy_id = rip.id AND rip.cid = ipc.cid )
                        LEFT JOIN lease_customers lc ON ( lc.lease_id = ipc.lease_id AND ipc.customer_id = lc.customer_id )
                        LEFT JOIN customer_relationships cr ON( lc.cid = cr.cid AND lc.customer_relationship_id = cr.id AND lc.customer_type_id = cr.customer_type_id )
                        LEFT JOIN property_preferences pp ON ( pp.cid = ipc.cid AND pp.property_id = ipc.property_id AND pp.key = \'' . \CPropertyPreference::PAID_VERIFICATION . '\' AND pp.value = \'1\' )
					WHERE
				    	ipc.property_id = ' . ( int ) $intPropertyId . '
				    	AND ipc.cid = ' . ( int ) $intCid . '
				    	AND ipc.resident_insurance_policy_id = ' . ( int ) $intResidentInsurancePolicyId . '
				    	AND lc.customer_type_id != ' . CCustomerType::GUARANTOR . '
				    	AND cr.name NOT LIKE \'%Spouse%\' 
				    	AND ( pp.id IS NULL OR rip.confirmed_on IS NOT NULL )
						AND ipc.lease_id = ' . ( int ) $intLeaseId;

		$arrstrData = fetchData( $strSql, $objDatabase );

		return $arrstrData;
	}

	public static function fetchActiveInsurancePolicyCustomerByResidentInsurancePolicyIdByLeaseIdByCid( $intResidentInsurancePolicyId, $intLeaseId, $intCid, $objDatabase ) {
		if( true == is_null( $intResidentInsurancePolicyId ) || true == is_null( $intLeaseId ) ) return false;

		$strSql = 'SELECT
						*
					FROM
						insurance_policy_customers
					WHERE
						resident_insurance_policy_id = ' . ( int ) $intResidentInsurancePolicyId . '
						AND cid = ' . ( int ) $intCid . '
						AND lease_id = ' . ( int ) $intLeaseId . '
						AND is_invalid_match = 0';

		return self::fetchInsurancePolicyCustomer( $strSql, $objDatabase );
	}

	public static function fetchActiveResidentInsurancePolicyCustomersByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intLeaseId ) ) return NULL;

		$strSql = 'SELECT * FROM insurance_policy_customers WHERE lease_id = ' . ( int ) $intLeaseId . ' AND is_invalid_match = 0 AND cid = ' . ( int ) $intCid;

		return self::fetchInsurancePolicyCustomers( $strSql, $objDatabase );
	}

	public static function fetchInsurancePolicyCustomersByApplicationsIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {
    	if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						insurance_policy_customers
					WHERE
						application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ')
						AND	cid = ' . ( int ) $intCid;

		return self::fetchInsurancePolicyCustomers( $strSql, $objDatabase );
    }

    public static function fetchMatchInsurancePolicyCustomersByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {
    	if( false == valArr( $arrintApplicationIds ) ) return NULL;

    	$strSql = 'SELECT
						*
					FROM
						insurance_policy_customers
					WHERE
						application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND is_invalid_match <> 1 ';

    	return self::fetchInsurancePolicyCustomers( $strSql, $objDatabase );
    }

	public static function fetchInactiveInsurancePolicyCustomerByResidentInsurancePolicyIdByLeaseIdByCid( $intResidentInsurancePolicyId, $intLeaseId, $intCid, $objDatabase ) {
		if( true == is_null( $intResidentInsurancePolicyId ) || true == is_null( $intLeaseId ) ) return false;

		$strSql = 'SELECT
						*
					FROM
						insurance_policy_customers
					WHERE
						resident_insurance_policy_id = ' . ( int ) $intResidentInsurancePolicyId . '
						AND cid = ' . ( int ) $intCid . '
						AND lease_id = ' . ( int ) $intLeaseId . '
						AND is_invalid_match = 1';

		return self::fetchInsurancePolicyCustomer( $strSql, $objDatabase );
	}

	public static function fetchInsurancePolicyCustomerByLeaseIdsByPolicyStatusTypeIdsByCid( $arrintLeaseIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = 'SELECT
					    ipc.*
				   FROM
				   	    insurance_policy_customers ipc
				   WHERE
					    ipc.cid = ' . ( int ) $intCid . '
					    AND	ipc.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ')';

		return self::fetchInsurancePolicyCustomers( $strSql, $objDatabase );

	}

	public static function fetchInsurancePolicyCustomerByInsurancePolicyIdByPropertyIdByCid( $intInsurancePolicyId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intInsurancePolicyId ) || false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ipc.*
					FROM insurance_policy_customers ipc
						 JOIN resident_insurance_policies rip ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					WHERE
						rip.cid = ' . ( int ) $intCid . '
						AND rip.property_id = ' . ( int ) $intPropertyId . '
						AND rip.insurance_policy_id = ' . ( int ) $intInsurancePolicyId . '
						AND ipc.is_invalid_match = 0';

		return self::fetchInsurancePolicyCustomer( $strSql, $objDatabase );
	}

	public static function fetchCustomInsurancePolicyCustomerByCustomerIds( $arrintCustomerIds, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT
						ipc.*
					FROM
						insurance_policy_customers ipc
 						JOIN resident_insurance_policies rip ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
						JOIN lease_customers lc ON ( lc.lease_id = ipc.lease_id  AND lc.cid = ipc.cid )
						JOIN clients c ON ( rip.cid = c.id )
					WHERE
						ipc.customer_id IN( ' . implode( ',', $arrintCustomerIds ) . ')
						AND ipc.is_invalid_match = 0
						AND rip.end_date IS NULL
						AND rip.cancelled_on IS NULL
						AND lc.customer_type_id = 1
						AND rip.insurance_policy_status_type_id IN( ' . CInsurancePolicyStatusType::ACTIVE . ' , ' . CInsurancePolicyStatusType::CANCEL_PENDING . ')
						AND rip.is_integrated = 1';

		return self::fetchInsurancePolicyCustomers( $strSql, $objDatabase, false );
	}

	public static function fetchAllInsurancePolicyCustomersByResidentInsurancePolicyIdsByLeaseIdByCid( $arrintResidentInsurancePolicyIds, $intLeaseId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintResidentInsurancePolicyIds ) || false == is_numeric( $intLeaseId ) || false == is_numeric( $intCid ) ) return false;

		$strSql = ' SELECT
						ipc.id,
						ipc.resident_insurance_policy_id,
						ipc.customer_id,
						c.name_first,
						c.name_last
					FROM
						insurance_policy_customers ipc
						JOIN customers c ON(c.id = ipc.customer_id AND c.cid = ipc.cid )
					WHERE
						ipc.resident_insurance_policy_id IN ( ' . implode( ',', $arrintResidentInsurancePolicyIds ) . ' )
						AND ipc.cid = ' . ( int ) $intCid . '
						AND ipc.lease_id = ' . ( int ) $intLeaseId . '
					ORDER BY
						ipc.id';

		$arrstrData = fetchData( $strSql, $objDatabase );

		return $arrstrData;
	}

	public static function fetchActiveInsurancePolicyCustomersAboutToEndByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
                         ipc.lease_id,
                         ipc.cid,
                         ipc.property_id
                    FROM
					    insurance_policy_customers ipc
					    JOIN resident_insurance_policies rip ON ( rip.cid = ipc.cid AND rip.id = ipc.resident_insurance_policy_id )
					    JOIN cached_leases l ON ( ipc.cid = l.cid AND ipc.property_id = l.property_id AND ipc.lease_id = l.id )
						LEFT JOIN property_preferences pp ON ( pp.cid = rip.cid and pp.property_id = rip.property_id and pp.key = \'POLICY_ABOUT_TO_END_DAYS\' )
					WHERE
					    ipc.lease_id IS NOT NULL
					    AND rip.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
					    AND rip.cid IN ( ' . implode( ', ', $arrintCids ) . ' )
					    AND rip.lapsed_on IS NULL
					    AND ( ( rip.end_date IS NOT NULL AND rip.end_date > CURRENT_DATE + ( pp.value::INTEGER ) 
                        OR  ( rip.end_date IS NULL AND rip.effective_date <= l.lease_start_date ) 
						OR  ( rip.end_date IS NOT NULL AND rip.effective_date <> rip.end_date AND rip.end_date > l.lease_start_date AND rip.effective_date <= l.lease_start_date AND rip.end_date > CURRENT_DATE )))';

		return self::fetchInsurancePolicyCustomers( $strSql, $objDatabase );
	}

	public static function fetchInsurancePolicyCustomersByLeaseIdsByCid( $arrintLeaseId, $intCid, $objDatabase ) {
		if( false === valArr( $arrintLeaseId ) || false === valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ipc.*
					FROM
						insurance_policy_customers ipc
				   WHERE
						ipc.lease_id IN ( ' . sqlIntImplode( $arrintLeaseId ) . ' )
						AND ipc.cid = ' . ( int ) $intCid;

		return self::fetchInsurancePolicyCustomers( $strSql, $objDatabase );
	}

	public static function fetchActiveInsurancePolicyCustomersByResidentInsurancePolicyIdByLeaseIdByCid( $intResidentInsurancePolicyId, $intLeaseId, $intCid, $objDatabase, $intCustomerId = NULL ) {
		if( true == is_null( $intResidentInsurancePolicyId ) || true == is_null( $intLeaseId ) ) return false;

		$strCustomerIdConditionSql = ( true == valId( $intCustomerId ) ) ? ' AND customer_id  = ' . $intCustomerId . ' ' : '';

		$strSql = 'SELECT
						*
					FROM
						insurance_policy_customers
					WHERE
						resident_insurance_policy_id = ' . ( int ) $intResidentInsurancePolicyId . '
						AND cid = ' . ( int ) $intCid . '
						AND lease_id = ' . ( int ) $intLeaseId . '
						' . $strCustomerIdConditionSql . '
						AND is_invalid_match = 0';

		return self::fetchInsurancePolicyCustomers( $strSql, $objDatabase );
	}

}
?>