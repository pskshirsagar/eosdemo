<?php

class CSpecialType extends CBaseSpecialType {

	const STANDARD								= 1;
	const GIFT_INCENTIVE						= 2;
	const RECURRING_PAYMENT_SET_UP_INCENTIVE	= 3;
	const RENEWAL_TIER							= 4;
	const INSTALLMENT_PLAN						= 5;
	const VACANCY_SUSPENSION_SPECIAL			= 6;
	const FIXED_BAH_ADJUSTMENT                  = 7;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>