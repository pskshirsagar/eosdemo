<?php

class CScheduledApHeaderExportBatchProperty extends CBaseScheduledApHeaderExportBatchProperty {
	protected $m_strPropertyName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledApHeaderExportBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false );
		if( isset( $arrValues['property_name'] ) ) $this->setPropertyName( $arrValues['property_name'] );

	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>