<?php

class CIpsChecklistQuestionLogType extends CBaseIpsChecklistQuestionLogType {

	const ASSIGNED	    	= 1;
	const COMPLETED	    	= 2;
	const REJECTED	    	= 3;
	const UNASSIGNED        = 4;
	const ENTRATA_COMPLETED = 5;
	const ENTRATA_REJECTED  = 6;
	const INCOMPLETED       = 7;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>