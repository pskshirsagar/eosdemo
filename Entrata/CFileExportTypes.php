<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileExportTypes
 * Do not add any new functions to this class.
 */

class CFileExportTypes extends CBaseFileExportTypes {

    public static function fetchFileExportTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CFileExportType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchFileExportType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CFileExportType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>