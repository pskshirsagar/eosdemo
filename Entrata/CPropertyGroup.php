<?php

class CPropertyGroup extends CBasePropertyGroup {

	const SYSTEM_CODE_ALL = 'ALL';
	const ALL_PROPERTIES = 'All Properties';

	const LDAP_PARENT_PROPERTY_GROUP_NAME = 'property-groups';

	protected $m_boolIsInherited;
	protected $m_boolIsDisabled;
	protected $m_boolHasPropertyGroupAssociations;

	protected $m_strLookupCode;
	protected $m_strPropertyGroupTypeName;

	/**
	 * Get Functions
	 */

	public function getIsInherited() {
		return $this->m_boolIsInherited;
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function getLookUpCode() {
		return $this->m_strLookupCode;
	}

	public function getPropertyGroupTypeName() {
		return $this->m_strPropertyGroupTypeName;
	}

	/**
	 * Set Functions
	 */

	 public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
	 	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	 	if( isset( $arrmixValues['is_inherited'] ) && $boolDirectSet ) {
	 		$this->m_boolIsInherited = trim( $arrmixValues['is_inherited'] );
	 	} elseif( isset( $arrmixValues['is_inherited'] ) ) {
	 		$this->setIsInherited( $arrmixValues['is_inherited'] );
	 	}

		if( isset( $arrmixValues['is_disabled'] ) && $boolDirectSet ) {
			$this->m_boolIsDisabled = trim( $arrmixValues['is_disabled'] );
		} elseif( isset( $arrmixValues['is_disabled'] ) ) {
			$this->setIsDisabled( $arrmixValues['is_disabled'] );
		}

	 	if( isset( $arrmixValues['lookup_code'] ) && $boolDirectSet ) {
	 		$this->m_boolIsInherited = trim( $arrmixValues['lookup_code'] );
	 	} elseif( isset( $arrmixValues['lookup_code'] ) ) {
		    $this->setLookupCode( $arrmixValues['lookup_code'] );
	    }

		if( isset( $arrmixValues['property_group_type_name'] ) && $boolDirectSet ) {
			$this->m_strPropertyGroupTypeName = trim( $arrmixValues['property_group_type_name'] );
		} elseif( isset( $arrmixValues['property_group_type_name'] ) ) {
			$this->setPropertyGroupTypeName( $arrmixValues['property_group_type_name'] );
		}
	 }

	public function setIsInherited( $boolIsInherited ) {
		$this->m_boolIsInherited = $boolIsInherited;
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->m_boolIsDisabled = ( bool ) $boolIsDisabled;
	}

	public function setLookupCode( $strLookupcode ) {
		$this->m_strLookupCode = $strLookupcode;
	}

	public function setPropertyGroupTypeName( $strPropertyGroupTypeName ) {
		$this->m_strPropertyGroupTypeName = $strPropertyGroupTypeName;
	}

	/**
	 * Create Functions
	 */

	/**
	 * @return CPropertyGroupAssociation
	 */
	public function createPropertyGroupAssociation() {

		$objPropertyGroupAssociation = new CPropertyGroupAssociation();
		$objPropertyGroupAssociation->setCid( $this->getCid() );
		$objPropertyGroupAssociation->setPropertyGroupId( $this->getId() );

		return $objPropertyGroupAssociation;
	}

	/**
	 * @param $intid
	 * @param $objDatabase
	 * @return CPropertyGroupAssociation|mixed|null
	 */
	public function createOrFetchPropertyGroupAssociationByPropertyId( $intId, $objDatabase ) {

		$objPropertyGroupAssociation = self::fetchPropertyGroupAssociationByPropertyId( $intId, $objDatabase );

		if( false == valObj( $objPropertyGroupAssociation, 'CPropertyGroupAssociation' ) ) {

			$objPropertyGroupAssociation = new CPropertyGroupAssociation();
			$objPropertyGroupAssociation->setCid( $this->getCid() );
			$objPropertyGroupAssociation->setPropertyGroupId( $this->getId() );

		}
		return $objPropertyGroupAssociation;
	}

	 /**
	  * Fetch Functions
	  */

	public function fetchPropertyGroupAssociationByPropertyId( $intPropertyId, $objDatabase ) {
		return CPropertyGroupAssociations::fetchPropertyGroupAssociationByCidByPropertyGroupIdByPropertyId( $this->getCid(), $this->getId(), $intPropertyId, $objDatabase );
	}

	public function fetchPropertyGroupAssociations( $objDatabase ) {
		return CPropertyGroupAssociations::fetchPropertyGroupAssociationsByCidByPropertyGroupId( $this->getCid(), $this->getId(), $objDatabase );
	}

	public function fetchCountPropertyGroupAssociations( $objDatabase ) {
		return CPropertyGroupAssociations::fetchPropertyGroupAssociationCountByCidByPropertyGroupId( $this->getCid(), $this->getId(), $objDatabase );
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'cid does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyGroupTypeId( $objDatabase ) {
		$boolIsValid = true;

		// Checking if the Property Group Type ID provided exists
		if( ( false == isset( $this->m_intPropertyGroupTypeId ) || ( 1 > $this->m_intPropertyGroupTypeId ) ) || true == is_null( \Psi\Eos\Entrata\CPropertyGroupTypes::createService()->fetchPropertyGroupTypeByIdByCid( $this->getPropertyGroupTypeId(), $this->getCid(), $objDatabase ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_group_type_id', __( 'Please select category.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSystemCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Property Group Name is required.' ) ) );
		} else {

			if( \Psi\CStringService::singleton()->strlen( $this->m_strName ) < 4 ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Property Group Name should have at least 4 characters.' ) ) );
				return false;
			}

			$objPropertyGroupType = \Psi\Eos\Entrata\CPropertyGroups::createService()->fetchPropertyGroupByCidByPropertyGroupTypeIdByName( $this->getCid(), $this->getPropertyGroupTypeId(), $this->getName(), $objDatabase );

			if( true == isset ( $objPropertyGroupType ) && $objPropertyGroupType->getId() != $this->getId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "Property Group with the 'Name' provided already exists in the selected Property Group Category." ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUseCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valPropertyGroupTypeId( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( 'NOW()' );

		if( false == $this->update( $intUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

}
?>