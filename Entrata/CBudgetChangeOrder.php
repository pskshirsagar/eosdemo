<?php

class CBudgetChangeOrder extends CBaseBudgetChangeOrder {

	protected $m_strApprovalNote;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApContractId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */
	public function createBudgetChangeOrderLog() {
		$objBudgetChangeOrderLog = new CBudgetChangeOrderLog();
		$objBudgetChangeOrderLog->setCid( $this->getCid() );
		$objBudgetChangeOrderLog->setJobId( $this->getJobId() );
		$objBudgetChangeOrderLog->setApContractId( $this->getApContractId() );

		return $objBudgetChangeOrderLog;
	}

	/**
	 * Get Functions
	 */
	public function getApprovalNote() {
		return $this->m_strApprovalNote;
	}

	/**
	 * Set Functions
	 */
	public function setApprovalNote( $strApprovalNote ) {
		$this->m_strApprovalNote = $strApprovalNote;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['approval_note'] ) ) {
			$this->setApprovalNote( $arrmixValues['approval_note'] );
		}

		return;
	}

	public function fetchEventsByEventTypeIds( $arrintEventTypeIds, $objDatabase, $strOtherConditions = NULL, $intLimit = NULL ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByReferenceIdByReferenceTypeIdByEventTypeIdsByCid( $this->getId(), CEventReferenceType::BUDGET_CHANGE_ORDER, $arrintEventTypeIds, $this->getCid(), $objDatabase, $strOtherConditions, $intLimit );
	}

}
?>