<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerPetLogs
 * Do not add any new functions to this class.
 */

class CCustomerPetLogs extends CBaseCustomerPetLogs {

	public static function fetchRecentCustomerPetLogByCustomerIdByCustomerPetIdByCid( $intCustomerId, $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cpl.*
					FROM
						customer_pet_logs cpl
					WHERE
						cpl.cid = ' . ( int ) $intCid . '
						AND cpl.customer_id = ' . ( int ) $intCustomerId . '
						AND cpl.customer_pet_id = ' . ( int ) $intId . '
						AND cpl.is_post_date_ignored = false
						ORDER BY cpl.log_datetime DESC LIMIT 1 ';

		return self::fetchCustomerPetLog( $strSql, $objDatabase );
	}

	public static function fetchRecentCustomerPetLogsByCustomerIdByCustomerPetIdByCid( $intCustomerId, $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cpl.*
					FROM
						customer_pet_logs cpl
					WHERE
						cpl.cid = ' . ( int ) $intCid . '
						AND cpl.customer_id = ' . ( int ) $intCustomerId . '
						AND cpl.customer_pet_id = ' . ( int ) $intId . '
						AND cpl.is_post_date_ignored = false
						ORDER BY cpl.log_datetime DESC';

		return self::fetchCustomerPetLogs( $strSql, $objDatabase );
	}

	public static function fetchCustomerPetLogsByCustomerPetIdsByCid( $arrintCustomerPetIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerPetIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						cpl.*
				   FROM
						customer_pet_logs cpl
				   WHERE
						cpl.customer_pet_id IN (' . implode( ',', $arrintCustomerPetIds ) . ')
						ORDER BY cpl.log_datetime DESC';

		return self::fetchCustomerPetLogs( $strSql, $objDatabase );
	}

}
?>