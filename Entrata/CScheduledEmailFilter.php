<?php

use Psi\Eos\Entrata\CCompanyEmployees;

class CScheduledEmailFilter extends CBaseScheduledEmailFilter {

	const DELINQUENCIES_ZERO_TO_THIRTY		= 1;
	const DELINQUENCIES_THIRTY_TO_SIXTY		= 2;
	const DELINQUENCIES_SIXTY_TO_NINTY		= 3;
	const DELINQUENCIES_NINTY_PLUS			= 4;

	protected $m_arrobjScheduledEmails;
	protected $m_arrobjApplicants;
	protected $m_arrobjCustomers;
	protected $m_arrobjCompanyEmployees;
	protected $m_arrobjPropertyPreferences;
	protected $m_arrobjPropertyPrimaryEmailAddress;
	protected $m_objClient;
	protected $m_objPropertyPreference;
	protected $m_intRecipientCount;
	protected $m_intPageNo;
	protected $m_intPreviousPageNo;
	protected $m_intCountPerPage;
	protected $m_intIsRefreshFilter;
	protected $m_intScheduledEmailId;
	protected $m_intScheduledEmailEventTypeId;
	protected $m_intIsAfterEvent;
	protected $m_intDaysFromEvent;
	protected $m_arrstrBlockedEmailAddresses;
	protected $m_arrintPropertyIds;
	protected $m_arrintUnsubscribedRecipientIds;
	protected $m_arrintAllActiveStatusTypeIds;
	protected $m_arrintEnabledPropertyIds;
	protected $m_boolShowUnsubscribedRecipients;
	protected $m_boolShowBlockedRecipients;
	protected $m_boolShowEventRecipients;
	protected $m_boolShowBalanceFilter;
	protected $m_strSortBy;
	protected $m_strSortDirection;
	protected $m_strLastSentOn;
	protected $m_strConfirmedOn;
	protected $m_strUserName;
	protected $m_strEmailContent;
	protected $m_strDelinquencies;

	private $m_arrstrLeaseSubStatusTypes;

	const PAGE_NO = 1;
	const COUNT_PER_PAGE = 8;

	// Sub-Status of resident lease
	const IN_COLLECTIONS				= 1;
	const TRANSFER						= 2;
	const RENEWED						= 3;
	const ACTIVE_REPAYMENT_AGREEMENT	= 4;
	const MONTH_TO_MONTH				= 5;
	const EVICTION						= 6;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getLeaseSubStatusTypes() : array {
		if( true == valArr( $this->m_arrstrLeaseSubStatusTypes ) ) {
			return $this->m_arrstrLeaseSubStatusTypes;
		}

		$this->m_arrstrLeaseSubStatusTypes = [
			self::IN_COLLECTIONS             => __( 'In Collections' ),
			self::TRANSFER                   => __( 'Transfer' ),
			self::RENEWED                    => __( 'Renewed' ),
			self::ACTIVE_REPAYMENT_AGREEMENT => __( 'Active Repayment Agreement' ),
			self::MONTH_TO_MONTH             => __( 'Month to Month' ),
			self::EVICTION                   => __( 'Eviction' )
		];

		return $this->m_arrstrLeaseSubStatusTypes;

	}

	protected $_REQUEST_FORM_SCHEDULED_EMAIL_FILTER = [
		'name' 								=> NULL,
		'scheduled_email_type_id'			=> NULL,
		'blocked_recipients'				=> NULL,
		'properties' 						=> NULL,
		'property_buildings' 				=> NULL,
		'unit_number'						=> NULL,
		'property_floorplans'				=> NULL,
		'property_floors'					=> NULL,
		'unit_types'						=> NULL,
		'leasing_agents'					=> NULL,
		'lead_sources'						=> NULL,
		'lease_interval_types' 				=> NULL,
		'customer_types'					=> NULL,
		'products' 							=> NULL,
		'status_types'						=> NULL,
		'sub_status_types'					=> NULL,
		'desired_bathrooms'					=> NULL,
		'desired_bedrooms' 					=> NULL,
		'pet_types' 						=> NULL,
		'event_types' 						=> NULL,
		'move_in_date_min'					=> NULL,
		'move_in_date_max'					=> NULL,
		'min_move_out_date'					=> NULL,
		'max_move_out_date'					=> NULL,
		'lease_renewal_date_min'			=> NULL,
		'lease_renewal_date_max'			=> NULL,
		'lease_expiration_date_min'			=> NULL,
		'lease_expiration_date_max'			=> NULL,
		'application_created_on_date_min'	=> NULL,
		'application_created_on_date_max'	=> NULL,
		'balance_due_min'					=> NULL,
		'balance_due_max'					=> NULL,
		'credit_balance_min'				=> NULL,
		'credit_balance_max'				=> NULL,
		'desired_rent_min'					=> NULL,
		'desired_rent_max'					=> NULL,
		'application_types'					=> NULL,
		'customer_vehicle'					=> NULL,
		'delinquencies'						=> NULL,
		'days_created'						=> NULL,
		'company_user_types'				=> NULL,
		'company_groups'					=> NULL,
		'company_departments'				=> NULL,
		'page_no'							=> NULL,
		'count_per_page'					=> NULL,
		'sort_by'							=> NULL,
		'sort_direction'					=> NULL,
		'is_refresh_filter'					=> NULL,
		'blocked_email_addresses'			=> NULL,
		'is_shared'							=> 0,
		'is_resident_portal_username'		=> NULL,
		'is_renter_insurance'				=> NULL,
		'is_dedupe'							=> 0,
		'is_vehicle_permit_number'			=> NULL,
		'exclude_recipients_from_list'      => 0,
		'locale_code'                       => NULL,
		'group_contracts'                   => NULL,
		'is_new_list'                       => 1,
		'lease_customer_ids'				=> NULL,
		'is_published'						=> 1,
		'is_from_dashboard'					=> NULL,
		'text_message_blocked_recipients'   => NULL
	];

	protected $_REQUEST_FORM_SCHEDULED_EMAIL_ADDRESS_FILTER = [
		'email_address'		=> NULL,
		'is_recipient'		=> NULL
	];

	public function getDelinquencyValues() {
		if( isset( $this->m_strDelinquencies ) ) {
			return $this->m_strDelinquencies;
		}
		$this->m_strDelinquencies = [
			self::DELINQUENCIES_ZERO_TO_THIRTY => [
				'display_text' => __( '0-30 Days' ),
				'lower_limit' => 0,
				'upper_limit' => 30
			],
			self::DELINQUENCIES_THIRTY_TO_SIXTY => [
				'display_text' => __( '31-60 Days' ),
				'lower_limit' => 31,
				'upper_limit' => 60
			],
			self::DELINQUENCIES_SIXTY_TO_NINTY => [
				'display_text' => __( '61-90 Days' ),
				'lower_limit' => 61,
				'upper_limit' => 90
			],
			self::DELINQUENCIES_NINTY_PLUS => [
				'display_text' => __( '91+ Days' ),
				'lower_limit' => 91
			]
		];

		return $this->m_strDelinquencies;

	}

	public function setDefaults() {
		$this->setId( NULL );
		$this->setCid( NULL );
		$this->setScheduledEmailTypeId( NULL );
		$this->setBlockedRecipients( NULL );
		$this->setName( NULL );
		$this->setProperties( NULL );
		$this->setPropertyIds( NULL );
		$this->setUnitNumber( NULL );
		$this->setPropertyFloorplans( NULL );
		$this->setUnitTypes( NULL );
		$this->setLeasingAgents( NULL );
		$this->setLeadSources( NULL );
		$this->setLeaseIntervalTypes( NULL );
		$this->setProducts( NULL );
		$this->setStatusTypes( NULL );
		$this->setSubStatusTypes( NULL );
		$this->setCustomerTypes( NULL );
		$this->setDesiredBedrooms( NULL );
		$this->setDesiredBathrooms( NULL );
		$this->setPetTypes( NULL );
		$this->setEventTypes( NULL );
		$this->setPageNo( self::PAGE_NO );
		$this->setPreviousPageNo( self::PAGE_NO );
		$this->setIsRefreshFilter( NULL );
		$this->setCountPerPage( self::COUNT_PER_PAGE );
		$this->setMoveInDateMin( NULL );
		$this->setMoveInDateMax( NULL );
		$this->setLeaseRenewalDateMin( NULL );
		$this->setLeaseRenewalDateMax( NULL );
		$this->setLeaseExpirationDateMin( NULL );
		$this->setLeaseExpirationDateMax( NULL );
		$this->setApplicationCreatedOnDateMin( NULL );
		$this->setApplicationCreatedOnDateMax( NULL );
		$this->setBalanceDueMin( NULL );
		$this->setBalanceDueMax( NULL );
		$this->setCreditBalanceMax( NULL );
		$this->setCreditBalanceMin( NULL );
		$this->setDesiredRentMin( NULL );
		$this->setDesiredRentMax( NULL );
		$this->setApplicationTypes( NULL );
		$this->setCustomerVehicle( NULL );
		$this->setDelinquencies( NULL );
		$this->setDaysCreated( NULL );
		$this->setOrderNum( '0' );
		$this->setUnsubscribedRecipientIds( NULL );
		$this->setShowBlockedRecipients( false );
		$this->setShowUnsubscribedRecipients( false );
		$this->setShowEventRecipients( false );
		$this->setShowBalanceFilter( false );
		$this->setAllActiveStatusTypeIds();
		$this->setIsShared( 0 );
		$this->setIsDedupe( false );
		$this->setIsPublished( 1 );
		$this->setIsResidentPortalUsername( NULL );
		$this->setIsVehiclePermitNumber( NULL );
		$this->setIsNewList( 1 );
	}

	public function setDefaultValues() {
		$this->setBlockedRecipients( NULL );
		$this->setProperties( NULL );
		$this->setUnitNumber( NULL );
		$this->setPropertyFloorplans( NULL );
		$this->setUnitTypes( NULL );
		$this->setLeasingAgents( NULL );
		$this->setLeadSources( NULL );
		$this->setLeaseIntervalTypes( NULL );
		$this->setProducts( NULL );
		$this->setStatusTypes( NULL );
		$this->setCustomerTypes( NULL );
		$this->setPetTypes( NULL );
		$this->setEventTypes( NULL );
		$this->setDesiredBedrooms( NULL );
		$this->setDesiredBathrooms( NULL );
		$this->setMoveInDateMin( NULL );
		$this->setMoveInDateMax( NULL );
		$this->setLeaseRenewalDateMin( NULL );
		$this->setLeaseRenewalDateMax( NULL );
		$this->setLeaseExpirationDateMin( NULL );
		$this->setLeaseExpirationDateMax( NULL );
		$this->setApplicationCreatedOnDateMin( NULL );
		$this->setApplicationCreatedOnDateMax( NULL );
		$this->setBalanceDueMin( NULL );
		$this->setBalanceDueMax( NULL );
		$this->setCreditBalanceMax( NULL );
		$this->setCreditBalanceMin( NULL );
		$this->setDesiredRentMin( NULL );
		$this->setDesiredRentMax( NULL );
		$this->setApplicationTypes( NULL );
		$this->setCustomerVehicle( NULL );
		$this->setDelinquencies( NULL );
		$this->setAllActiveStatusTypeIds();
		$this->setDaysCreated( NULL );
		$this->setPageNo( self::PAGE_NO );
		$this->setPreviousPageNo( self::PAGE_NO );
		$this->setCountPerPage( self::COUNT_PER_PAGE );
		$this->setIsShared( 0 );
		$this->setIsDedupe( false );
		$this->setIsResidentPortalUsername( NULL );
		$this->setIsVehiclePermitNumber( NULL );
	}

	public function prepareRequestData( $arrmixRequestForm = [] ) {
		if( true == valArr( $arrmixRequestForm ) ) {
			$arrmixRequestForm['properties']				= ( true == isset( $arrmixRequestForm['property_ids'] ) && true == valArr( $arrmixRequestForm['property_ids'] ) )						? implode( ',', $arrmixRequestForm['property_ids'] ) : '';
			unset( $arrmixRequestForm['property_ids'] );
			$arrmixRequestForm['property_buildings']		= ( true == isset( $arrmixRequestForm['property_building_ids'] ) && true == valArr( $arrmixRequestForm['property_building_ids'] ) )		? implode( ',', $arrmixRequestForm['property_building_ids'] ) : '';
			unset( $arrmixRequestForm['property_building_ids'] );
			$arrmixRequestForm['unit_number'] 				= ( true == isset( $arrmixRequestForm['unit_numbers'] ) ) 																				? $arrmixRequestForm['unit_numbers'] : '';
			unset( $arrmixRequestForm['unit_numbers'] );
			$arrmixRequestForm['property_floorplans']		= ( true == isset( $arrmixRequestForm['property_floor_plan_ids'] ) && true == valArr( $arrmixRequestForm['property_floor_plan_ids'] ) )	? implode( ',', $arrmixRequestForm['property_floor_plan_ids'] ) : '';
			unset( $arrmixRequestForm['property_floor_plan_ids'] );
			$arrmixRequestForm['property_floors']			= ( true == isset( $arrmixRequestForm['property_floor_ids'] ) && true == valArr( $arrmixRequestForm['property_floor_ids'] ) )			? implode( ',', $arrmixRequestForm['property_floor_ids'] ) : '';
			unset( $arrmixRequestForm['property_floor_ids'] );
			$arrmixRequestForm['unit_types']				= ( true == isset( $arrmixRequestForm['unit_type_ids'] ) && true == valArr( $arrmixRequestForm['unit_type_ids'] ) )						? implode( ',', $arrmixRequestForm['unit_type_ids'] ) : '';
			unset( $arrmixRequestForm['unit_type_ids'] );
			$arrmixRequestForm['leasing_agents']			= ( true == isset( $arrmixRequestForm['leasing_agent_ids'] ) && true == valArr( $arrmixRequestForm['leasing_agent_ids'] ) )				? implode( ',', $arrmixRequestForm['leasing_agent_ids'] ) : '';
			unset( $arrmixRequestForm['leasing_agent_ids'] );
			$arrmixRequestForm['lead_sources']				= ( true == isset( $arrmixRequestForm['lead_source_ids'] ) && true == valArr( $arrmixRequestForm['lead_source_ids'] ) )					? implode( ',', $arrmixRequestForm['lead_source_ids'] ) : '';
			unset( $arrmixRequestForm['lead_source_ids'] );
			$arrmixRequestForm['lease_interval_types']		= ( true == isset( $arrmixRequestForm['lease_interval_type_ids'] ) && true == valArr( $arrmixRequestForm['lease_interval_type_ids'] ) )	? implode( ',', $arrmixRequestForm['lease_interval_type_ids'] ) : '';
			unset( $arrmixRequestForm['lease_interval_type_ids'] );
			$arrmixRequestForm['products']					= ( true == isset( $arrmixRequestForm['product_ids'] ) && true == valArr( $arrmixRequestForm['product_ids'] ) )							? implode( ',', $arrmixRequestForm['product_ids'] ) : '';
			unset( $arrmixRequestForm['product_ids'] );
			$arrmixRequestForm['status_types']				= ( true == isset( $arrmixRequestForm['status_type_ids'] ) && true == valArr( $arrmixRequestForm['status_type_ids'] ) )					? implode( ',', $arrmixRequestForm['status_type_ids'] ) : '';
			unset( $arrmixRequestForm['status_type_ids'] );
			$arrmixRequestForm['desired_bedrooms']			= ( true == isset( $arrmixRequestForm['desired_bedroom_ids'] ) && true == valArr( $arrmixRequestForm['desired_bedroom_ids'] ) )			? implode( ',', $arrmixRequestForm['desired_bedroom_ids'] ) : '';
			unset( $arrmixRequestForm['desired_bedroom_ids'] );
			$arrmixRequestForm['desired_bathrooms']			= ( true == isset( $arrmixRequestForm['desired_bathroom_ids'] ) && true == valArr( $arrmixRequestForm['desired_bathroom_ids'] ) )		? implode( ',', $arrmixRequestForm['desired_bathroom_ids'] ) : '';
			unset( $arrmixRequestForm['desired_bathroom_ids'] );
			$arrmixRequestForm['pet_types']					= ( true == isset( $arrmixRequestForm['pet_type_ids'] ) && true == valArr( $arrmixRequestForm['pet_type_ids'] ) )						? implode( ',', $arrmixRequestForm['pet_type_ids'] ) : '';
			unset( $arrmixRequestForm['pet_type_ids'] );
			$arrmixRequestForm['customer_types']			= ( true == isset( $arrmixRequestForm['customer_type_ids'] ) && true == valArr( $arrmixRequestForm['customer_type_ids'] ) )				? implode( ',', $arrmixRequestForm['customer_type_ids'] ) : '';
			unset( $arrmixRequestForm['customer_type_ids'] );
			$arrmixRequestForm['group_contracts']           = ( true == isset( $arrmixRequestForm['group_contract_ids'] ) && true == valArr( $arrmixRequestForm['group_contract_ids'] ) )				? implode( ',', array_filter( $arrmixRequestForm['group_contract_ids'] ) ) : '';
			unset( $arrmixRequestForm['group_contract_ids'] );
			$arrmixRequestForm['application_types']			= ( true == isset( $arrmixRequestForm['application_type_ids'] ) && true == valArr( $arrmixRequestForm['application_type_ids'] ) )		? implode( ',', $arrmixRequestForm['application_type_ids'] ) : '';
			unset( $arrmixRequestForm['application_type_ids'] );
			$arrmixRequestForm['event_types']				= ( true == isset( $arrmixRequestForm['event_type_ids'] ) && true == valArr( $arrmixRequestForm['event_type_ids'] ) )					? implode( ',', $arrmixRequestForm['event_type_ids'] ) : '';
			unset( $arrmixRequestForm['event_type_ids'] );
			$arrmixRequestForm['sub_status_types']	= ( true == isset( $arrmixRequestForm['sub_status_types'] ) && true == valArr( $arrmixRequestForm['sub_status_types'] ) )		? implode( ',', $arrmixRequestForm['sub_status_types'] ) : '';
			$arrmixRequestForm['company_user_types']				= ( true == isset( $arrmixRequestForm['company_user_type_keys'] ) && true == valArr( $arrmixRequestForm['company_user_type_keys'] ) )					? implode( ',', $arrmixRequestForm['company_user_type_keys'] ) : '';
			unset( $arrmixRequestForm['company_user_type_keys'] );
			$arrmixRequestForm['company_groups']				= ( true == isset( $arrmixRequestForm['company_group_ids'] ) && true == valArr( $arrmixRequestForm['company_group_ids'] ) )					? implode( ',', $arrmixRequestForm['company_group_ids'] ) : '';
			unset( $arrmixRequestForm['company_group_ids'] );
			$arrmixRequestForm['company_departments']				= ( true == isset( $arrmixRequestForm['company_department_ids'] ) && true == valArr( $arrmixRequestForm['company_department_ids'] ) )					? implode( ',', $arrmixRequestForm['company_department_ids'] ) : '';
			unset( $arrmixRequestForm['company_department_ids'] );
			$arrmixRequestForm['locale_code']				= ( true == isset( $arrmixRequestForm['locale_code'] ) && true == valStr( $arrmixRequestForm['locale_code'] ) )					? $arrmixRequestForm['locale_code'] : '';
			$arrmixRequestForm['move_in_date_min']	= ( true == isset( $arrmixRequestForm['move_in_date_min'] ) && true == valStr( $arrmixRequestForm['move_in_date_min'] ) )		? CValidation::checkISODateFormat( $arrmixRequestForm['move_in_date_min'] ): '';
			$arrmixRequestForm['move_in_date_max']	= ( true == isset( $arrmixRequestForm['move_in_date_max'] ) && true == valStr( $arrmixRequestForm['move_in_date_max'] ) )		? CValidation::checkISODateFormat( $arrmixRequestForm['move_in_date_max'] ): '';
			$arrmixRequestForm['min_move_out_date']	= ( true == isset( $arrmixRequestForm['min_move_out_date'] ) && true == valStr( $arrmixRequestForm['min_move_out_date'] ) )		? CValidation::checkISODateFormat( $arrmixRequestForm['min_move_out_date'] ): '';
			$arrmixRequestForm['max_move_out_date']	= ( true == isset( $arrmixRequestForm['max_move_out_date'] ) && true == valStr( $arrmixRequestForm['max_move_out_date'] ) )		? CValidation::checkISODateFormat( $arrmixRequestForm['max_move_out_date'] ): '';
			$arrmixRequestForm['lease_renewal_date_min']	= ( true == isset( $arrmixRequestForm['lease_renewal_date_min'] ) && true == valStr( $arrmixRequestForm['lease_renewal_date_min'] ) )		? CValidation::checkISODateFormat( $arrmixRequestForm['lease_renewal_date_min'] ): '';
			$arrmixRequestForm['lease_renewal_date_max']	= ( true == isset( $arrmixRequestForm['lease_renewal_date_max'] ) && true == valStr( $arrmixRequestForm['lease_renewal_date_max'] ) )		? CValidation::checkISODateFormat( $arrmixRequestForm['lease_renewal_date_max'] ): '';
			$arrmixRequestForm['lease_expiration_date_min']	= ( true == isset( $arrmixRequestForm['lease_expiration_date_min'] ) && true == valStr( $arrmixRequestForm['lease_expiration_date_min'] ) )		? CValidation::checkISODateFormat( $arrmixRequestForm['lease_expiration_date_min'] ): '';
			$arrmixRequestForm['lease_expiration_date_max']	= ( true == isset( $arrmixRequestForm['lease_expiration_date_max'] ) && true == valStr( $arrmixRequestForm['lease_expiration_date_max'] ) )		? CValidation::checkISODateFormat( $arrmixRequestForm['lease_expiration_date_max'] ): '';
			$arrmixRequestForm['application_created_on_date_min']	= ( true == isset( $arrmixRequestForm['application_created_on_date_min'] ) && true == valStr( $arrmixRequestForm['application_created_on_date_min'] ) )		? CValidation::checkISODateFormat( $arrmixRequestForm['application_created_on_date_min'] ): '';
			$arrmixRequestForm['application_created_on_date_max']	= ( true == isset( $arrmixRequestForm['application_created_on_date_max'] ) && true == valStr( $arrmixRequestForm['application_created_on_date_max'] ) )		? CValidation::checkISODateFormat( $arrmixRequestForm['application_created_on_date_max'] ): '';

			$arrmixIsResidentPortalUsername = $arrmixRequestForm['is_resident_portal_username'];
			$arrmixRequestForm['is_resident_portal_username'] = '';
			if( true == valArr( $arrmixIsResidentPortalUsername ) ) {
				$intResidentPortalCount = 0;
				foreach( $arrmixIsResidentPortalUsername as $intValue ) {
					$intResidentPortalCount++;
				}
				if( 1 == $intResidentPortalCount ) {
					$intResidentPortalCount = $intValue;
				}
				$arrmixRequestForm['is_resident_portal_username'] = $intResidentPortalCount;
			}

			$arrmixCustomerVehicle = $arrmixRequestForm['customer_vehicle'];
			$strCustomerVehicle = $arrmixRequestForm['old_customer_vehicle'];
			$arrmixRequestForm['customer_vehicle'] = '';
			if( true == valArr( $arrmixCustomerVehicle ) ) {
				$intCustomerVehicleSettingCount = 0;
				foreach( $arrmixCustomerVehicle as $intValue ) {
					$intCustomerVehicleSettingCount++;
				}
				if( 1 == $intCustomerVehicleSettingCount ) {
					$intCustomerVehicleSettingCount = $intValue;
				}
				$arrmixRequestForm['customer_vehicle'] = $intCustomerVehicleSettingCount;
			}

			if( true == valStr( $strCustomerVehicle ) ) {
				$arrmixRequestForm['customer_vehicle'] = $strCustomerVehicle;
			}

			$arrmixIsVehiclePermitNumber = $arrmixRequestForm['is_vehicle_permit_number'];
			$arrmixRequestForm['is_vehicle_permit_number'] = '';
			if( true == valArr( $arrmixIsVehiclePermitNumber ) ) {
				$intIsVehiclePermitNumberCount = 0;

				foreach( $arrmixIsVehiclePermitNumber as $intValue ) {
					$intIsVehiclePermitNumberCount++;
				}

				if( 1 == $intIsVehiclePermitNumberCount ) {
					$intIsVehiclePermitNumberCount = $intValue;
				}

				$arrmixRequestForm['is_vehicle_permit_number'] = $intIsVehiclePermitNumberCount;
			}

			$arrmixIsRenterInsurance = $arrmixRequestForm['is_renter_insurance'];
			$arrmixRequestForm['is_renter_insurance'] = '';
			if( true == valArr( $arrmixIsRenterInsurance ) ) {
				$intIsRenterInsuranceCount = 0;

				foreach( $arrmixIsRenterInsurance as $intValue ) {
					$intIsRenterInsuranceCount++;
				}

				if( 1 == $intIsRenterInsuranceCount ) {
					$intIsRenterInsuranceCount = $intValue;
				}

				$arrmixRequestForm['is_renter_insurance'] = $intIsRenterInsuranceCount;
			}

			$arrmixRequestForm = mergeIntersectArray( $this->_REQUEST_FORM_SCHEDULED_EMAIL_FILTER, $arrmixRequestForm );
		}

		$this->setValues( $arrmixRequestForm, false );
	}

	/**
	 * ******************************************************************
	 * ******************** Setter Functions ******************************
	 * ******************************************************************
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['recipient_count'] ) ) {
			$this->setRecipientCount( $arrmixValues['recipient_count'] );
		}
		if( true == isset( $arrmixValues['text_message_blocked_recipients'] ) ) {
			$this->setDetailsField( ['text_message_blocked_recipients'], $arrmixValues['text_message_blocked_recipients'] );
		}
		if( true == isset( $arrmixValues['page_no'] ) ) {
			$this->setPageNo( $arrmixValues['page_no'] );
		}
		if( true == isset( $arrmixValues['page_size'] ) ) {
			$this->setPreviousPageNo( $arrmixValues['page_size'] );
		}
		if( true == isset( $arrmixValues['page_size'] ) ) {
			$this->setCountPerPage( $arrmixValues['page_size'] );
		}
		if( true == isset( $arrmixValues['is_refresh_filter'] ) ) {
			$this->setIsRefreshFilter( $arrmixValues['is_refresh_filter'] );
		}
		if( true == isset( $arrmixValues['sort_by'] ) ) {
			$this->setSortBy( $arrmixValues['sort_by'] );
		}
		if( true == isset( $arrmixValues['sort_direction'] ) ) {
			$this->setSortDirection( $arrmixValues['sort_direction'] );
		}
		if( true == isset( $arrmixValues['show_unsubscribed_recipients'] ) ) {
			$this->setShowUnsubscribedRecipients( $arrmixValues['show_unsubscribed_recipients'] );
		}
		if( true == isset( $arrmixValues['show_blocked_recipients'] ) ) {
			$this->setShowBlockedRecipients( $arrmixValues['show_blocked_recipients'] );
		}
		if( true == isset( $arrmixValues['show_event_recipients'] ) ) {
			$this->setShowEventRecipients( $arrmixValues['show_event_recipients'] );
		}
		if( true == isset( $arrmixValues['username'] ) ) {
			$this->setUserName( $arrmixValues['username'] );
		}
		if( false == valStr( $this->getStatusTypes() ) ) {
			$this->setAllActiveStatusTypeIds();
		}
		if( true == isset( $arrmixValues['company_user_types'] ) ) {
			$this->setCompanyUserTypes( $arrmixValues['company_user_types'] );
		}
		if( true == isset( $arrmixValues['company_groups'] ) ) {
			$this->setCompanyGroups( $arrmixValues['company_groups'] );
		}
		if( true == isset( $arrmixValues['company_departments'] ) ) {
			$this->setCompanyDepartments( $arrmixValues['company_departments'] );
		}
		if( true == isset( $arrmixValues['exclude_recipients_from_list'] ) ) {
			$this->setIsExcludeRecipient( $arrmixValues['exclude_recipients_from_list'] );
		}
		if( true == isset( $arrmixValues['locale_code'] ) ) {
			$this->setLocaleCode( $arrmixValues['locale_code'] );
		}
		if( true == isset( $arrmixValues['group_contracts'] ) ) {
			$this->setGroupContracts( $arrmixValues['group_contracts'] );
		}
		if( true == isset( $arrmixValues['lease_customer_ids'] ) ) {
			if( true == valArr( $arrmixValues['lease_customer_ids'] ) ) {
				$arrmixValues['lease_customer_ids'] = implode( ',', $arrmixValues['lease_customer_ids'] );
			}
			$this->setLeaseCustomerIds( $arrmixValues['lease_customer_ids'] );
		}
		if( true == isset( $arrmixValues['is_from_dashboard'] ) ) {
			$this->setIsFromDashboard( $arrmixValues['is_from_dashboard'] );
		}
	}

	public function addScheduledEmail( $objScheduledEmail ) {
		$this->m_arrobjScheduledEmails[$objScheduledEmail->getId()] = $objScheduledEmail;
	}

	public function setLastSentOn( $strLastSentOn ) {
		$this->m_strLastSentOn = $strLastSentOn;
	}

	public function setIsAfterEvent( $intIsAfterEvent ) {
		$this->m_intIsAfterEvent = $intIsAfterEvent;
	}

	public function setConfirmedOn( $strConfirmedOn ) {
		$this->m_strConfirmedOn = $strConfirmedOn;
	}

	public function setDaysFromEvent( $intDaysFromEvent ) {
		$this->m_intDaysFromEvent = $intDaysFromEvent;
	}

	public function setScheduledEmailEventTypeId( $intScheduledEmailEventTypeId ) {
		$this->m_intScheduledEmailEventTypeId = $intScheduledEmailEventTypeId;
	}

	public function setMoveInDateMin( $strMoveInDateMin ) {
		$this->m_strMoveInDateMin = ( true == valStr( $strMoveInDateMin ) && 'Min' != $strMoveInDateMin ) ? $strMoveInDateMin : NULL;
	}

	public function setMoveInDateMax( $strMoveInDateMax ) {
		$this->m_strMoveInDateMax = ( true == valStr( $strMoveInDateMax ) && 'Max' != $strMoveInDateMax ) ? $strMoveInDateMax : NULL;
	}

	public function setLeaseRenewalDateMin( $strLeaseRenewalDateMin ) {
		$this->m_strLeaseRenewalDateMin = ( true == valStr( $strLeaseRenewalDateMin ) && 'Min' != $strLeaseRenewalDateMin ) ? $strLeaseRenewalDateMin : NULL;
	}

	public function setLeaseRenewalDateMax( $strLeaseRenewalDateMax ) {
		$this->m_strLeaseRenewalDateMax = ( true == valStr( $strLeaseRenewalDateMax ) && 'Max' != $strLeaseRenewalDateMax ) ? $strLeaseRenewalDateMax : NULL;
	}

	public function setLeaseExpirationDateMin( $strLeaseExpirationDateMin ) {
		$this->m_strLeaseExpirationDateMin = ( true == valStr( $strLeaseExpirationDateMin ) && 'Min' != $strLeaseExpirationDateMin ) ? $strLeaseExpirationDateMin : NULL;
	}

	public function setLeaseExpirationDateMax( $strLeaseExpirationDateMax ) {
		$this->m_strLeaseExpirationDateMax = ( true == valStr( $strLeaseExpirationDateMax ) && 'Max' != $strLeaseExpirationDateMax ) ? $strLeaseExpirationDateMax : NULL;
	}

	public function setApplicationCreatedOnDateMin( $strApplicationCreatedOnDateMin ) {
		$this->m_strApplicationCreatedOnDateMin = ( true == valStr( $strApplicationCreatedOnDateMin ) && 'Min' != $strApplicationCreatedOnDateMin ) ? $strApplicationCreatedOnDateMin : NULL;
	}

	public function setApplicationCreatedOnDateMax( $strApplicationCreatedOnDateMax ) {
		$this->m_strApplicationCreatedOnDateMax = ( true == valStr( $strApplicationCreatedOnDateMax ) && 'Max' != $strApplicationCreatedOnDateMax ) ? $strApplicationCreatedOnDateMax : NULL;
	}

	public function setPageNo( $intPageNo ) {
		$this->m_intPageNo = ( false == is_null( $intPageNo ) ) ? $intPageNo : 1;
	}

	public function setPreviousPageNo( $intPreviousPageNo ) {
		$this->m_intPreviousPageNo = $intPreviousPageNo;
	}

	public function setCountPerPage( $intCountPerPage ) {
		$this->m_intCountPerPage = ( false == is_null( $intCountPerPage ) && 0 < $intCountPerPage ) ? $intCountPerPage : self::COUNT_PER_PAGE;
	}

	public function setRecipientCount( $intRecipientCount ) {
		$this->m_intRecipientCount = CStrings::strToIntDef( $intRecipientCount, NULL, false );
	}

	public function setIsRefreshFilter( $intIsRefreshFilter ) {
		$this->m_intIsRefreshFilter = $intIsRefreshFilter;
	}

	public function setSortBy( $strSortBy ) {
		$this->m_strSortBy = $strSortBy;
	}

	public function setSortDirection( $strSortDirection ) {
		$this->m_strSortDirection = $strSortDirection;
	}

	public function setPropertyIds( $arrintPropertyIds ) {
		$this->m_arrintPropertyIds = $arrintPropertyIds;
	}

	public function setUnsubscribedRecipientIds( $arrintUnsubscribedRecipientIds ) {
		$this->m_arrintUnsubscribedRecipientIds = $arrintUnsubscribedRecipientIds;
	}

	public function setName( $strName ) {
		$this->m_strName = CStrings::strTrimDef( ( 'Type List Name Here' == $strName ) ? NULL : $strName, 50, NULL, true );
	}

	public function setShowUnsubscribedRecipients( $boolShowUnsubscribedRecipients ) {
		$this->m_boolShowUnsubscribedRecipients = $boolShowUnsubscribedRecipients;
	}

	public function setShowBlockedRecipients( $boolShowBlockedRecipients ) {
		$this->m_boolShowBlockedRecipients = $boolShowBlockedRecipients;
	}

	public function setShowEventRecipients( $boolShowEventRecipients ) {
		$this->m_boolShowEventRecipients = $boolShowEventRecipients;
	}

	public function setScheduledEmailId( $intScheduledEmailId ) {
		$this->m_intScheduledEmailId = $intScheduledEmailId;
	}

	public function setAllActiveStatusTypeIds() {

		$this->m_arrintAllActiveStatusTypeIds = [];

		if( false == is_null( $this->getScheduledEmailTypeId() ) ) {

			switch( $this->getScheduledEmailTypeId() ) {
				case CScheduledEmailType::RESIDENTS:
					$this->m_arrintAllActiveStatusTypeIds[] = CLeaseStatusType::FUTURE;
					$this->m_arrintAllActiveStatusTypeIds[] = CLeaseStatusType::CURRENT;
					$this->m_arrintAllActiveStatusTypeIds[] = CLeaseStatusType::NOTICE;
					break;

				case CScheduledEmailType::PROSPECTS:
					$this->m_arrintAllActiveStatusTypeIds[] = CApplicationStageStatus::PROSPECT_STAGE1_COMPLETED;
					$this->m_arrintAllActiveStatusTypeIds[] = CApplicationStageStatus::PROSPECT_STAGE2_STARTED;
					$this->m_arrintAllActiveStatusTypeIds[] = CApplicationStageStatus::PROSPECT_STAGE2_COMPLETED;
					$this->m_arrintAllActiveStatusTypeIds[] = CApplicationStageStatus::PROSPECT_STAGE2_APPROVED;
					$this->m_arrintAllActiveStatusTypeIds[] = CApplicationStageStatus::PROSPECT_STAGE3_STARTED;
					$this->m_arrintAllActiveStatusTypeIds[] = CApplicationStageStatus::PROSPECT_STAGE3_PARTIALLY_COMPLETED;
					$this->m_arrintAllActiveStatusTypeIds[] = CApplicationStageStatus::PROSPECT_STAGE3_COMPLETED;
					$this->m_arrintAllActiveStatusTypeIds[] = CApplicationStageStatus::PROSPECT_STAGE3_APPROVED;
					$this->m_arrintAllActiveStatusTypeIds[] = CApplicationStageStatus::PROSPECT_STAGE4_STARTED;
					$this->m_arrintAllActiveStatusTypeIds[] = CApplicationStageStatus::PROSPECT_STAGE4_PARTIALLY_COMPLETED;
					$this->m_arrintAllActiveStatusTypeIds[] = CApplicationStageStatus::PROSPECT_STAGE4_COMPLETED;
					break;

				default:
					// Default case
					break;
			}
		}
	}

	public function setOnFlyListName( $objCompanyUser = NULL ) {
		if( false == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			return NULL;
		}

		$this->setName( $objCompanyUser->getId() . '-' . date( 'Y-m-d H:i:s' ) );
		$this->setIsPublished( 0 );
	}

	public function setUserName( $strUserName ) {
		$this->m_strUserName = $strUserName;
	}

	public function setShowBalanceFilter( $boolShowBalanceFilter ) {
		$this->m_boolShowBalanceFilter = $boolShowBalanceFilter;
	}

	public function setEmailContent( $strEmailContent ) {
		$this->m_strEmailContent = $strEmailContent;
	}

	public function setIsExcludeRecipient( $intIsExcludeRecipient ) {
		$this->setDetailsField( 'exclude_recipients_from_list', $intIsExcludeRecipient );
	}

	/**
	 * *****************************************************************
	 * *******************************Getter Functions *******************
	 * *****************************************************************
	 */

	public function getLastSentOn() {
		return $this->m_strLastSentOn;
	}

	public function getConfirmedOn() {
		return $this->m_strConfirmedOn;
	}

	public function getPropertyPreference() {
		return $this->m_objPropertyPreference;
	}

	public function getScheduledEmails() {
		return $this->m_arrobjScheduledEmails;
	}

	public function getBlockedRecipientIds() : array {
		$arrintBlockedRecipientIds = [];
		if( false == is_null( $this->m_strBlockedRecipients ) && true == valStr( $this->m_strBlockedRecipients ) ) {
			$arrintBlockedRecipientIds = explode( ',', $this->m_strBlockedRecipients );
		}
		return $arrintBlockedRecipientIds;
	}

	public function getUnsubscribedRecipientIds() {
		return $this->m_arrintUnsubscribedRecipientIds;
	}

	public function getBlockedEmailAddresses() {
		return $this->m_arrstrBlockedEmailAddresses;
	}

	public function getPageNo() : int {
		return $this->m_intPageNo = ( false == is_null( $this->m_intPageNo ) ) ? $this->m_intPageNo : 1;
	}

	public function getPreviousPageNo() {
		return $this->m_intPreviousPageNo;
	}

	public function getCountPerPage() : int {
		return $this->m_intCountPerPage = ( false == is_null( $this->m_intCountPerPage ) && 0 < $this->m_intCountPerPage ) ? $this->m_intCountPerPage : self::COUNT_PER_PAGE;
	}

	public function getRecipientCount() {
		return $this->m_intRecipientCount;
	}

	public function getIsRefreshFilter() {
		return $this->m_intIsRefreshFilter;
	}

	public function getSortBy() {
		return $this->m_strSortBy;
	}

	public function getSortDirection() {
		return $this->m_strSortDirection;
	}

	public function getPropertyIds() {
		return $this->m_arrintPropertyIds;
	}

	public function getScheduledEmailEventTypeId() {
		return $this->m_intScheduledEmailEventTypeId;
	}

	public function getIsAfterEvent() {
		return $this->m_intIsAfterEvent;
	}

	public function getDaysFromEvent() {
		return $this->m_intDaysFromEvent;
	}

	public function getShowBlockedRecipients() {
		return $this->m_boolShowBlockedRecipients;
	}

	public function getShowUnsubscribedRecipients() {
		return $this->m_boolShowUnsubscribedRecipients;
	}

	public function getShowEventRecipients() {
		return $this->m_boolShowEventRecipients;
	}

	public function getScheduledEmailId() {
		return $this->m_intScheduledEmailId;
	}

	public function getAllActiveStatusTypeIds() {
		return $this->m_arrintAllActiveStatusTypeIds;
	}

	public function getUserName() {
		return $this->m_strUserName;
	}

	public function getShowBalanceFilter() {
		return $this->m_boolShowBalanceFilter;
	}

	public function getEmailContent() {
		return $this->m_strEmailContent;
	}

	public function getIsExcludeRecipient() {
		return $this->getExcludeRecipientsFromList();
	}

	/**
	 * *****************************************************************
	 * ************************** Get Or Fetch Functions *****************
	 * *****************************************************************
	 */
	public function getOrFetchPropertyIds( $objDatabase ) {
		if( false == valArr( $this->m_arrintPropertyIds ) ) {
			$this->m_arrintPropertyIds = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyIdsByCidByCompanyUserIdBySessionData( $this->getCompanyUserId(), $this->getCid(), NULL, $objDatabase );
		}

		return $this->m_arrintPropertyIds;
	}

	public function getOrFetchUnsubscribedRecipientIds( $objDatabase ) {
		if( false == valArr( $this->m_arrintUnsubscribedRecipientIds ) ) {
			$this->m_arrintUnsubscribedRecipientIds = $this->fetchUnsubscribedRecipientIdsByCid( $this->getCid(), $objDatabase );
		}

		return $this->m_arrintUnsubscribedRecipientIds;
	}

	public function getOrFetchClient( $objAdminDatabase ) {
		if( false == valObj( $this->m_objClient, 'CClient' ) ) {
			$this->m_objClient = $this->fetchClient( $objAdminDatabase );
		}

		return $this->m_objClient;
	}

	public function getOrFetchPropertyPreferences( $objDatabase ) {
		if( false == valArr( $this->m_arrobjPropertyPreferences ) ) {
			$arrintPropertyIds = ( false == is_null( $this->getProperties() ) && true == valStr( $this->getProperties() ) ) ? explode( ',', trim( $this->getProperties(), ',' ) ) : $this->m_arrintPropertyIds;

			$arrstrKeys = [
				'FROM_MESSAGE_CENTER_DEFAULT_EMAIL',
				'BCC_MESSAGE_CENTER',
				'BCC_MESSAGE_CENTER_NOTIFICATION',
				'SOCIAL_MEDIA_BLOG_URL',
				'SOCIAL_MEDIA_FACEBOOK_URL',
				'SOCIAL_MEDIA_FLICKR_URL',
				'SOCIAL_MEDIA_FOURSQUARE_URL',
				'SOCIAL_MEDIA_GOOGLE_PLUS_URL',
				'SOCIAL_MEDIA_GOOGLE_PLACES_URL',
				'SOCIAL_MEDIA_INSTAGRAM_URL',
				'SOCIAL_MEDIA_LINKEDIN_USER',
				'SOCIAL_MEDIA_MYSPACE_URL',
				'SOCIAL_MEDIA_PINTEREST_URL',
				'SOCIAL_MEDIA_SATISFACTS_ID',
				'SOCIAL_MEDIA_TUMBLR_URL',
				'SOCIAL_MEDIA_TWITTER_URL',
				'SOCIAL_MEDIA_YELP_URL',
				'SOCIAL_MEDIA_YOUTUBE_URL'
			];
			$this->m_arrobjPropertyPreferences = CPropertyPreferences::fetchPropertyPreferencesByPropertyIdsByKeysByCid( $arrintPropertyIds, $arrstrKeys, $this->getCid(), $objDatabase, true );
		}

		return $this->m_arrobjPropertyPreferences;
	}

	public function getOrFetchPropertyPrimaryEmailAddress( $objDatabase ) {

		if( false == valArr( $this->m_arrobjPropertyPrimaryEmailAddress ) ) {
			$arrintPropertyIds = ( false == is_null( $this->getProperties() ) && true == valStr( $this->getProperties() ) ) ? explode( ',', trim( $this->getProperties(), ',' ) ) : $this->m_arrintPropertyIds;
			$this->m_arrobjPropertyPrimaryEmailAddress = CPropertyEmailAddresses::fetchPropertyEmailAddressesByPropertyIdsByEmailAddressTypeIdByCid( $arrintPropertyIds, CEmailAddressType::PRIMARY, $this->getCid(), $objDatabase );
		}
		return $this->m_arrobjPropertyPrimaryEmailAddress;
	}

	public function getRequestFormFields() : array {
		return ( array ) $this->_REQUEST_FORM_SCHEDULED_EMAIL_FILTER;
	}

	/**
	 * *****************************************************************
	 * *******************************Validate Functions *****************
	 * *****************************************************************
	 */
	public function valScheduledEmailTypeId() : bool {
		$boolIsValid = true;

		if( true == is_null( $this->getScheduledEmailTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_email_type_id', __( 'Email Group is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName() : bool {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCustomerTypes() : bool {
		return true;
	}

	public function valMoveInDateMin() : bool {
		$boolIsValid = true;

		if( true == is_null( $this->getMoveInMinDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date_min', __( 'Move In Minimum date is empty.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMoveInDateMax() : bool {
		$boolIsValid = true;

		if( true == is_null( $this->getMoveInMaxDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date_max', __( 'Move in maximum date is empty.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDeletedBy() : bool {
		$boolIsValid = true;

		if( true == is_null( $this->getDeletedBy() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deleted_by', '' ) );
		}

		return $boolIsValid;
	}

	public function valDeletedOn() : bool {
		$boolIsValid = true;

		if( true == is_null( $this->getDeletedOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deleted_on', '' ) );
		}

		return $boolIsValid;
	}

	public function valConflictScheduledEmailFilterName( $objCompanyUser, $objDatabase ) : bool {
		$boolIsValid = true;

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( __( 'Application Error: Valid database object not provided.' ), E_USER_ERROR );
		}

		if( false == valStr( $this->getName() ) || ( 0 == \Psi\CStringService::singleton()->strcasecmp( 'Name This List', $this->getName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'List name is required.' ) ) );
		} elseif( false == is_null( $this->getName() ) ) {
			if( 2 > \Psi\CStringService::singleton()->strlen( $this->getName() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'List name must have at least 2 characters.' ) ) );
			} elseif( 50 < \Psi\CStringService::singleton()->strlen( $this->getName() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'List name cannot be more than 50 characters long.' ) ) );
			} elseif( true == preg_match( '/"/', $this->getName() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Please do not use double quotes (") in the list name.' ) ) );
			} elseif( 0 == \Psi\CStringService::singleton()->strcasecmp( 'Temporary List', $this->getName() ) && 1 == $this->getIsPublished() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Temporary List is not a valid list name.' ) ) );
			} elseif( 0 != strcmp( $this->getName(), strip_tags( $this->getName() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Please do not use HTML tag in the list name.' ) ) );
			} elseif( 0 != strcmp( $this->getName(), str_replace( '\\', '', $this->getName() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Please do not use slash (\) in the list name.' ) ) );
			} else {

				$intConflictScheduledEmailFilterCount = $this->fetchScheduledEmailFilterCountByNameByCid( $objCompanyUser, $objDatabase );

				if( 0 < $intConflictScheduledEmailFilterCount ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'This list name is already in use !  Please choose another list name.' ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objCompanyUser = NULL, $objDatabase = NULL ) : bool {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:

				$boolIsValid &= $this->valScheduledEmailTypeId();
				$boolIsValid &= $this->valConflictScheduledEmailFilterName( $objCompanyUser, $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDeletedBy();
				$boolIsValid &= $this->valDeletedOn();
				break;

			case 'validate_move_in_dates':
				$boolIsValid &= $this->valMoveInDateMin();
				$boolIsValid &= $this->valMoveInDateMax();
				break;

			default:
				// Default case
				break;
		}

		return $boolIsValid;
	}

	/**
	 * *****************************************************************
	 * ***************************** Fetch Functions *********************
	 * *****************************************************************
	 */
	public function fetchPropertyPreference( $objDatabase ) {
		if( false == is_null( $this->getProperties() ) && false == \Psi\CStringService::singleton()->strpos( $this->getProperties(), ',' ) ) {
			$this->m_objPropertyPreference = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'FROM_MESSAGE_CENTER_DEFAULT_EMAIL', $this->getProperties(), $this->getCid(), $objDatabase );
		}
		return $this->m_objPropertyPreference;
	}

	public function fetchScheduledEmailFilterCountByNameByCid( $objCompanyUser, $objDatabase ) {
		if( false == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			return NULL;
		}

		$strWhereSql = ' WHERE
						deleted_on IS NULL
					AND is_published = 1
					AND lower( name) = \'' . \Psi\CStringService::singleton()->strtolower( trim( addslashes( $this->getName() ) ) ) . '\'
					AND cid = ' . ( int ) $this->getCid();

		if( false == is_null( $this->getId() ) ) {
			$strWhereSql .= ' AND id != ' . ( int ) $this->getId();
			$strWhereSql .= ' AND company_user_id = ( select
													sef.company_user_id
												FROM
													scheduled_email_filters AS sef
												WHERE
													sef.id = ' . ( int ) $this->getId() . ' AND
													sef.cid = ' . ( int ) $this->getCid() . ')';
		} else {
			$strWhereSql .= ' AND company_user_id = ' . ( int ) $objCompanyUser->getId();
		}

		return \Psi\Eos\Entrata\CScheduledEmailFilters::createService()->fetchScheduledEmailFilterCount( $strWhereSql, $objDatabase );
	}

	public function fetchScheduledEmails( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CScheduledEmails::createService()->fetchScheduledEmailsByScheduledEmailFilterIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchUnsubscribedRecipientIdsByCid( $intCid, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledEmailAddressFilters::createService()->fetchScheduledEmailAddressFiltersByScheduledEmailTypeIdByCid( $this->getScheduledEmailTypeId(), $intCid, $objDatabase );
	}

	public function fetchScheduledEmailType( $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledEmailTypes::createService()->fetchPublishedScheduledEmailTypeById( $this->getScheduledEmailTypeId(), $objDatabase );
	}

	public function fetchCustomers( $objDatabase ) {
		$arrobjCustomers = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByPropertyIdsByCidByScheduledEmailFilter( $this->getPropertyIds(), $this->getCid(), $this, $objDatabase );
		return $arrobjCustomers;
	}

	public function fetchTotalCustomersCount( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersCountByPropertyIdsByScheduledEmailFilterByCid( $this->getPropertyIds(), $this, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseCustomers( $objDatabase ) {
		//@TODO Test it thoroughly in second phase refactoring
		$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByPropertyIdsByScheduledEmailFilterByCid( $this->getPropertyIds(), $this, $this->getCid(), $objDatabase, $boolShowAllRecords = true, false, false, true );
		return $arrobjLeaseCustomers;
	}

	public function fetchApplications( $objDatabase ) {
		$arrobjApplicants = CApplications::fetchApplicationsByPropertyIdsByScheduledEmailFilterByCid( $this->getPropertyIds(), $this, $this->getCid(), $objDatabase, $boolShowAllRecords = true, false, false, true );
		return $arrobjApplicants;
	}

	public function fetchCompanyEmployees( $objDatabase ) {
		$arrobjCompanyEmployees = CCompanyEmployees::createService()->fetchCompanyEmployeesByPropertyIdsByScheduledEmailFilterByCid( $this->getPropertyIds(), $this, $this->getCid(), $objDatabase, true );
		return $arrobjCompanyEmployees;
	}

	public function fetchApPayees( $objDatabase ) {
		$arrobjApPayees = CApPayees::fetchApPayeesByScheduledEmailFilterByCid( $this, $this->getCid(), $objDatabase, true );
		return $arrobjApPayees;
	}

	public function fetchTotalCompanyEmployeesCount( $objDatabase ) {
		return CCompanyEmployees::createService()->fetchCompanyEmployeesCountByPropertyIdsByScheduledEmailFilterByCid( $this->getPropertyIds(), $this, $this->getCid(), $objDatabase );
	}

	public function fetchClient( $objAdminDatabase ) {
		return \Psi\Eos\Admin\CClients::createService()->fetchSimpleClientById( $this->getCid(), $objAdminDatabase );
	}

	public function fetchProperties( $objDatabase ) {
		if( false == is_null( $this->getProperties() ) ) {
			$arrintPropertyIds = explode( ',', trim( $this->getProperties(), ',' ) );
		} else {
			$arrintPropertyIds = $this->getOrFetchPropertyIds( $objDatabase );
		}

		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function processActiveProperties( $arrintEnabledPropertyIds ) {
		if( false == valArr( $arrintEnabledPropertyIds ) ) {
			return NULL;
		}

		$strPropertyIds = $this->getProperties();

		if( false == is_null( $strPropertyIds ) && true == valStr( $strPropertyIds ) ) {
			$arrintActivePropertyIds = array_intersect( explode( ',', $strPropertyIds ), $arrintEnabledPropertyIds );
			$strPropertyIds = ( true == valArr( $arrintActivePropertyIds, 0 ) ) ? implode( ',', $arrintActivePropertyIds ) : $strPropertyIds;
			$this->setProperties( $strPropertyIds );
		}
	}

	public function fetchCompanyUser( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchEmployeeCompanyUserByIdByCidByIsDisabled( $this->getCompanyUserId(), $this->getCid(), $objDatabase, $boolIncludeDeletedAA = false );
	}

	public function loadFilterStatistics( $arrintPropertyIds, $objDatabase, $boolIncludeDeletedAA = false ) : \CMessageCenterStatistics {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$objMessageCenterStatistics = new CMessageCenterStatistics();
		$arrstrConditions = [];
		$strSql = '';

		switch( $this->getScheduledEmailTypeId() ) {
			case CScheduledEmailType::PROSPECTS:
				$arrstrConditions[] = 'COUNT( DISTINCT( appt.id ) ) AS total_recipients';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN appt.username IS NOT NULL OR appt.mobile_number IS NOT NULL THEN appt.id END ) ) AS total_recipients_reachable';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN appt.username IS NULL THEN appt.id END ) ) AS no_email_address';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN appt.username IS NOT NULL THEN appt.id END ) ) AS email_address';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN appt.mobile_number IS NULL THEN appt.id END ) ) AS no_mobile_number';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN appt.mobile_number IS NOT NULL THEN appt.id END ) ) AS mobile_number';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN appt.work_number IS NULL THEN appt.id END ) ) AS no_work_number';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN appt.phone_number IS NULL THEN appt.id END ) ) AS no_home_number';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN appt.username IS NULL AND appt.phone_number IS NULL THEN appt.id END ) ) AS no_contacts';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN seaf.scheduled_email_id IS NOT NULL AND seaf.is_unsubscribed = 1 THEN seaf.applicant_id END ) ) AS unsubscribed';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN ( appt.username IS NULL ) OR ( seaf.id IS NOT NULL ) THEN appt.id END ) ) AS unsubscribed_or_no_email_address';

				if( true == is_numeric( $this->getScheduledEmailId() ) ) {
					$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN seaf.scheduled_email_id = ' . $this->getScheduledEmailId() . ' AND seaf.cid = ' . $this->getCid() . ' THEN seaf.applicant_id END ) ) AS opt_out_recipients';
				}

				$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

				// need to ask about above if condition
				$strSql = 'SELECT
								' . implode( ', ', $arrstrConditions ) . '
							FROM
								applications AS apps
								JOIN applicant_applications AS aa ON ( aa.application_id = apps.id AND aa.cid = apps.cid ' . $strCheckDeletedAASql . ' )
								JOIN applicants As appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
								LEFT JOIN leases AS l ON ( l.id = apps.lease_id AND l.cid = apps.cid )
								LEFT JOIN property_floorplans AS pfp ON ( pfp.id = apps.property_floorplan_id AND pfp.cid = apps.cid AND pfp.deleted_on IS NULL )
								LEFT JOIN property_units AS pu ON ( pu.id = apps.property_unit_id AND pu.cid = apps.cid AND pu.deleted_on IS NULL )
								LEFT JOIN unit_spaces AS us ON ( l.unit_space_id = us.id AND us.property_unit_id = pu.id AND l.cid = us.cid AND us.cid = pu.cid AND us.deleted_on IS NULL )
								LEFT JOIN customer_pets AS cp ON ( cp.lease_id = apps.lease_id AND cp.customer_id = aapt.customer_id AND cp.cid = aa.cid )
								LEFT JOIN scheduled_email_address_filters AS seaf ON ( appt.id = seaf.applicant_id AND appt.cid = seaf.cid )
								WHERE
									apps.cid = ' . $this->getCid() . CApplications::loadScheduledEmailFilterSearchSql( $this, $arrintPropertyIds );
				break;

			case CScheduledEmailType::RESIDENTS:
				$arrstrConditions[] = 'COUNT( DISTINCT( c.id ) ) AS total_recipients';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN cps.username IS NOT NULL OR c.mobile_number IS NOT NULL THEN c.id END ) ) AS total_recipients_reachable';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN cps.username IS NULL THEN c.id END ) ) AS no_email_address';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN cps.username IS NOT NULL THEN c.id END ) ) AS email_address';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN c.mobile_number IS NULL THEN c.id END ) ) AS no_mobile_number';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN c.mobile_number IS NOT NULL THEN c.id END ) ) AS mobile_number';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN c.work_number IS NULL THEN c.id END ) ) AS no_work_number';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN c.phone_number IS NULL THEN c.id END ) ) AS no_home_number';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN cps.username IS NULL AND c.phone_number IS NULL THEN c.id END ) ) AS no_contacts';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN seaf.scheduled_email_id IS NOT NULL AND seaf.is_unsubscribed = 1 THEN seaf.customer_id END ) ) AS unsubscribed';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN ( cps.username IS NULL ) OR ( seaf.id IS NOT NULL ) THEN c.id END ) ) AS unsubscribed_or_no_email_address';

				if( true == is_numeric( $this->getScheduledEmailId() ) ) {
					$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN ( seaf.scheduled_email_id = ' . $this->getScheduledEmailId() . ' AND seaf.cid = ' . $this->getCid() . ') THEN seaf.customer_id END ) ) AS opt_out_recipients';
				}
				// need to ask about above if condition
				     $strSql = 'SELECT ' . implode( ', ', $arrstrConditions ) . '

							FROM
								customers AS c
								JOIN customer_portal_settings cps ON ( c.id = cps.customer_id AND c.cid = cps.cid )
								JOIN lease_customers lc ON ( c.id = lc.customer_id AND c.cid = lc.cid )
								JOIN leases l ON ( lc.lease_id = l.id AND lc.cid = l.cid )
								JOIN lease_processes lp ON ( lp.lease_id = l.id AND lp.cid = l.cid AND lp.customer_id IS NULL )
								JOIN lease_intervals li ON ( li.id = l.active_lease_interval_id AND li.cid = l.cid )
								JOIN properties AS p ON ( p.id = l.property_id AND p.cid = l.cid )
								LEFT JOIN unit_spaces us ON ( l.unit_space_id = us.id AND l.cid = us.cid AND us.deleted_on IS NULL )
								LEFT JOIN property_units pu ON ( us.property_unit_id = pu.id AND us.cid = pu.cid AND pu.deleted_on IS NULL )
								LEFT JOIN property_floorplans AS pfp ON ( pfp.id = pu.property_floorplan_id AND pfp.cid = pu.cid AND pfp.deleted_on IS NULL )
								LEFT JOIN property_buildings pb ON ( pu.property_building_id = pb.id AND pu.cid = pb.cid AND pb.deleted_on IS NULL )
								LEFT JOIN rate_associations AS ra ON ( ra.property_id = p.id AND ra.cascade_id = ' . CArCascade::SPACE . ' AND ra.ar_cascade_reference_id = us.id AND ra.cid = p.cid AND ra.cid = us.cid )
								LEFT JOIN unit_types AS ut ON ( ut.id = pu.unit_type_id AND ut.cid = pu.cid )
								LEFT JOIN scheduled_email_address_filters AS seaf ON ( seaf.customer_id = c.id AND seaf.cid = c.cid )
							WHERE
								li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
								AND c.cid = ' . ( int ) $this->getCid() . \Psi\Eos\Entrata\CCustomers::createService()->loadScheduledEmailFilterSearchSql( $this, $arrintPropertyIds );
				break;

			// doubt in following case
			case CScheduledEmailType::EMPLOYEES:
				$arrstrConditions[] = 'COUNT( DISTINCT( ce.id ) ) AS total_recipients';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN ( ce.email_address IS NOT NULL) OR ( ( ce.phone_number1 IS NOT NULL AND ce.phone_number1_type_id = ' . CPhoneNumberType::MOBILE . ' OR ce.phone_number2 IS NOT NULL AND ce.phone_number2_type_id = ' . CPhoneNumberType::MOBILE . ' ) ) THEN ce.id END ) ) AS total_recipients_reachable';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN ce.email_address IS NULL THEN ce.id END ) ) AS no_email_address';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN ce.email_address IS NOT NULL THEN ce.id END ) ) AS email_address';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN ( ce.phone_number1 IS NULL AND ce.phone_number1_type_id = ' . CPhoneNumberType::MOBILE . ' OR ce.phone_number2 IS NULL
			    							AND ce.phone_number2_type_id = ' . CPhoneNumberType::MOBILE . ' )
									THEN ce.id END ) ) AS no_mobile_number';

				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN ( ce.phone_number1 IS NULL AND ce.phone_number1_type_id = ' . CPhoneNumberType::MOBILE . ' OR ce.phone_number2 IS NULL
			    							AND ce.phone_number2_type_id = ' . CPhoneNumberType::MOBILE . ' )
									THEN ce.id END ) ) AS mobile_number';

				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN ( ce.phone_number1 IS NULL AND ce.phone_number1_type_id = ' . CPhoneNumberType::OFFICE . ' OR ce.phone_number2 IS NULL
			    							AND ce.phone_number2_type_id = ' . CPhoneNumberType::OFFICE . ' )
									THEN ce.id END ) ) AS no_work_number';

				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN ( ce.phone_number1 IS NULL AND ce.phone_number1_type_id = ' . CPhoneNumberType::HOME . ' OR ce.phone_number2 IS NULL
			    							AND ce.phone_number2_type_id = ' . CPhoneNumberType::HOME . ' )
									THEN ce.id END ) ) AS no_home_number';

				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN ce.email_address IS NULL AND ce.phone_number1 IS NULL THEN ce.id END ) ) AS no_contacts';
				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN seaf.scheduled_email_id IS NOT NULL AND seaf.is_unsubscribed = 1 THEN seaf.company_employee_id END ) ) AS unsubscribed';

				if( true == is_numeric( $this->getScheduledEmailId() ) ) {
					$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN seaf.scheduled_email_id = ' . $this->getScheduledEmailId() . ' THEN seaf.company_employee_id END ) ) AS opt_out_recipients';
				}
				// need to check above if condition

				$arrstrConditions[] = 'COUNT( DISTINCT( CASE WHEN ( ce.email_address IS NULL ) OR ( seaf.id IS NOT NULL ) THEN ce.id END ) ) AS unsubscribed_or_no_email_address';

			  $strSql = 'SELECT ' . implode( ', ', $arrstrConditions ) . '
							FROM
								company_employees AS ce
								LEFT JOIN company_users AS cu ON ( cu.company_employee_id = ce.id AND ( cu.cid = ce.cid AND cu.default_company_user_id IS NULL ) ) AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
								LEFT JOIN scheduled_email_address_filters AS seaf ON ( seaf.company_employee_id = ce.id AND seaf.cid = ce.cid)
							WHERE
								ce.cid = ' . ( int ) $this->getCid() . CCompanyEmployees::createService()->loadScheduledEmailFilterSql( $this, $arrintPropertyIds );
				break;

			default:
				// Default case
				break;
		}

		$arrmixResponse = fetchData( $strSql, $objDatabase );

		$objMessageCenterStatistics->setValues( ( true == valArr( $arrmixResponse ) ) ? $arrmixResponse[0] : [] );

		return $objMessageCenterStatistics;
	}

	public function fetchCustomersCountByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersCountByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $this, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyEmployeesCountByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CCompanyEmployees::createService()->fetchCompanyEmployeesCountByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $this, $this->getCid(), $objDatabase );
	}

	public function processRetrieveCustomersWithBalance( $objDatabase, $boolManualUpdate = false ) {

		if( false == $boolManualUpdate && true == valStr( $this->getEmailContent() ) && false === \Psi\CStringService::singleton()->strpos( $this->getEmailContent(), '*BALANCE_DUE*' ) ) {
			return false;
		}

		$arrobjProperties = $this->fetchProperties( $objDatabase );

		// do not process if more then one properties are selected
		if( false == valArr( $arrobjProperties ) ) {
			return false;
		}

		if( \Psi\Libraries\UtilFunctions\count( $arrobjProperties ) > 1 ) {
			return false;
		}

		$objProperty = reset( $arrobjProperties );

		// dont process for INTEGRATION_CLIENT_TYPE_REAL_PAGE
		$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objDatabase );
		if( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && CIntegrationClientType::REAL_PAGE == $objIntegrationDatabase->getIntegrationClientTypeId() ) {
			return true;
		}

		if( true == valStr( $this->getStatusTypes() ) ) {
			if( 99 == ( int ) trim( $this->getStatusTypes() ) ) {
				$arrobjStatusTypes = ( array ) \Psi\Eos\Entrata\CLeaseStatusTypes::createService()->fetchPublishedLeaseStatusTypes( $objDatabase );
				$arrintStatusTypeIds = array_keys( $arrobjStatusTypes );
			} else {
				$arrintStatusTypeIds = explode( ',', trim( $this->getStatusTypes(), ',' ) );
			}
		} else {
			$arrintStatusTypeIds = $this->getAllActiveStatusTypeIds();
		}

		$objUtilitiesDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::UTILITIES, CDatabaseUserType::PS_PROPERTYMANAGER );

		$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->getCid(), CIntegrationService::RETRIEVE_CUSTOMERS_WITH_BALANCE_DUE, $this->getCompanyUserId(), $objDatabase );
		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setLeaseStatusTypeIds( $arrintStatusTypeIds );
		$objGenericWorker->setDontUpdateLastSyncOn( true );
		$objGenericWorker->setPullLedger( false );
		$objGenericWorker->setDatabase( $objDatabase );
		$objGenericWorker->setUtilityDatabase( $objUtilitiesDatabase );

		if( true == valObj( $objGenericWorker, 'CGenericClientWorker' ) ) {
			return false;
		}

		return $objGenericWorker->process();
	}

	public function getFirstRecipientFromFilterList( $arrintEnabledPropertyIds, $objDatabase ) {
		$arrintPropertyIds	= NULL;
		$arrobjRecipients	= NULL;

		$arrintPropertyIds			= ( false == is_null( $this->getProperties() ) ) ? explode( ',', trim( $this->getProperties(), ',' ) ) : $arrintEnabledPropertyIds;

		switch( $this->getScheduledEmailTypeId() ) {
			case CScheduledEmailType::RESIDENTS:
				//@TODO Test it thoroughly in second phase refactoring
				$arrobjRecipients	= \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $this, $this->getCid(), $objDatabase );
				break;

			case CScheduledEmailType::PROSPECTS:
				$arrobjRecipients	= CApplications::fetchApplicationsByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $this, $this->getCid(), $objDatabase, $boolShowAllRecords = true );
				break;

			case CScheduledEmailType::EMPLOYEES:
				$arrobjRecipients	= CCompanyEmployees::createService()->fetchPaginatedCompanyEmployeesByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $this, $this->getCid(), $objDatabase );
				break;

			default:
		}

		if( true == valArr( $arrobjRecipients ) ) {
			return current( $arrobjRecipients );
		}

		return NULL;
	}

	public function processMonayGramAcountsByCustomers( &$arrobjLeaseCustomers, $arrintCustomerIds, $objMessageCenterEmailContentController, $objPaymentDatabase ) : bool {
		if( false == \Psi\CStringService::singleton()->strpos( $objMessageCenterEmailContentController->getScheduledEmailContent(), '*MONEYGRAM_ACCOUNT_NUMBER*' ) ) {
			return false;
		}

		if( false == valArr( $arrintCustomerIds ) ) {
			return false;
		}

		$arrobjMoneyGramAccountsByCustomers = [];

		$arrobjMoneyGramAccounts = \Psi\Eos\Payment\CMoneyGramAccounts::createService()->fetchMoneyGramAccountByCustomerIdsByCid( $arrintCustomerIds, $this->getCid(), $objPaymentDatabase );

		if( false == valArr( $arrobjMoneyGramAccounts ) ) {
			return false;
		}

		foreach( $arrobjMoneyGramAccounts as $objMoneyGramAccount ) {
			$arrobjMoneyGramAccountsByCustomers[$objMoneyGramAccount->getCustomerId()][$objMoneyGramAccount->getLeaseId()] = $objMoneyGramAccount;
		}

		foreach( $arrobjLeaseCustomers as $objLeaseCustomer ) {
			if( true == valArr( $arrobjMoneyGramAccountsByCustomers )
				&& true == array_key_exists( $objLeaseCustomer->getCustomerId(), $arrobjMoneyGramAccountsByCustomers )
				&& true == array_key_exists( $objLeaseCustomer->getLeaseId(), $arrobjMoneyGramAccountsByCustomers[$objLeaseCustomer->getCustomerId()] ) ) {

				$objMoneyGramAccount = $arrobjMoneyGramAccountsByCustomers[$objLeaseCustomer->getCustomerId()][$objLeaseCustomer->getLeaseId()];
				if( true == valObj( $objMoneyGramAccount, 'CMoneyGramAccount' ) && NULL != $objMoneyGramAccount->getId() ) {
					$objLeaseCustomer->setMoneyGramAccountId( $objMoneyGramAccount->getId() );
				}
			}
		}
		return true;
	}

	public function getRecipientsByScheduledEmailTypeId() : array {

		switch( $this->getScheduledEmailTypeId() ) {
			case CScheduledEmailType::RESIDENTS:
				$arrobjRecipients = ( array ) $this->fetchLeaseCustomers( $this->m_objDatabase );
				break;

			case CScheduledEmailType::PROSPECTS:
				$arrobjRecipients = ( array ) $this->fetchApplications( $this->m_objDatabase );
				break;

			case CScheduledEmailType::EMPLOYEES:
				$arrobjRecipients = ( array ) $this->fetchCompanyEmployees( $this->m_objDatabase );
				break;

			case CScheduledEmailType::VENDORS:
				$arrobjRecipients = ( array ) $this->fetchApPayees( $this->m_objDatabase );
				break;

			default:
				$arrobjRecipients = [];
		}

		return $arrobjRecipients;
	}

}
?>
