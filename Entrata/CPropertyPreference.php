<?php

class CPropertyPreference extends CBasePropertyPreference {

	protected $m_arrstrAllowedScriptTagKeys;

	const DEFAULT_LEDGER_DISPLAY_DAYS						= 60;
	const PAST_RESIDENT_LOGIN_TOLERANCE_DAYS				= 60;

	const SHOW_PRIMARY_ON_FMO_STATEMENT						= 1;
	const SHOW_PRIMARY_AND_GUARANTOR_ON_FMO_STATEMENT		= 3;
	const SHOW_ALL_OCCUPANTS_ON_FMO_STATEMENT				= 4;
	const SHOW_ALL_RESPONSIBLE_OCCUPANTS_ON_FMO_STATEMENT	= 5;

	const TRANSFER_BALANCE_TO_NEW_LEASE_ALWAYS				= 1;
	const TRANSFER_BALANCE_TO_NEW_LEASE_NEVER				= 2;
	const TRANSFER_BALANCE_TO_NEW_LEASE_OPTIONAL			= 3;

	const TRANSFER_BALANCE_AS_THEY_ARE						= 1;
	const TRANSFER_BALANCE_AS_BEGINNING_BALANCE				= 2;

	const SEND_LATE_NOTICE_TO_PRIMARY						= 1;
	const SEND_LATE_NOTICE_TO_PRIMARY_AND_GUARANTOR			= 2;
	const SEND_LATE_NOTICE_TO_ALL_RESPONSIBLE				= 3;
	const SEND_LATE_NOTICE_TO_ALL_RESPONSIBLE_OCCUPANTS		= 4;

	const MOVE_OUT											= 1;
	const NOTICE_TO_VACATE									= 2;

	const TRACK_INVENTORY_QUANTITIES						= 'TRACK_INVENTORY_QUANTITIES';
	// Asset Depreciation constants

	const IS_ASSET_POST_DEPRECIATION                 = 'IS_ASSET_POST_DEPRECIATION';
	const ASSET_DEPRECIATION_POST_CONVENTION_TYPE_ID = 'ASSET_DEPRECIATION_POST_CONVENTION_TYPE_ID';
	const ASSET_POST_DEPRECIATION_TO_GL_BOOK_ID      = 'ASSET_POST_DEPRECIATION_TO_GL_BOOK_ID';
	const ASSET_START_POSTING_DEPRECIATION_DATE      = 'ASSET_START_POSTING_DEPRECIATION_DATE';
	const ASSET_DEPRECIATION_ACCOUNT_ID              = 'ASSET_DEPRECIATION_ACCOUNT_ID';

	const DELINQUENCY_LEVEL									= 'DELINQUENCY_LEVEL';

	const REQUIRE_FORCE_PLACED_POLICIES						= 'REQUIRE_FORCE_PLACED_POLICIES';
	const FORCE_PLACED_FEE									= 'FORCE_PLACED_FEE';
	const POLICY_AR_CHARGE_CODE								= 'POLICY_AR_CHARGE_CODE';
	const MASTER_POLICY_AGREEMENT							= 'MASTER_POLICY_AGREEMENT';
	const IS_AFFORDABLE										= 'IS_AFFORDABLE';
	const PRORATE_BY_DAY 									= 'PRORATE_BY_DAY';
	const REQUIRE_BLANKET_POLICIES							= 'REQUIRE_BLANKET_POLICIES';
	const ENROLLMENT_TYPE							        = 'ENROLLMENT_TYPE';
	const ENFORCEMENT_STRATEGY							    = 'ENFORCEMENT_STRATEGY';
	const GRACE_PERIOD_DAYS							        = 'GRACE_PERIOD_DAYS';
	const SEND_FORCE_PLACED_EMAILS                          = 'SEND_FORCE_PLACED_EMAILS';

	const REQUIRE_EACH_INDIVIDUAL_RESIDENT_HAVE_ACTIVE_COVERAGE = 'REQUIRE_EACH_INDIVIDUAL_RESIDENT_HAVE_ACTIVE_COVERAGE';
	const REQUIRE_INSURANCE_FOR_ALL_OCCUPIED_UNITS			= 'REQUIRE_INSURANCE_FOR_ALL_OCCUPIED_UNITS';
	const REQUIRE_INSURANCE_FOR_ALL_OCCUPIED_UNITS_EFFECTIVE_DATE = 'REQUIRE_INSURANCE_FOR_ALL_OCCUPIED_UNITS_EFFECTIVE_DATE';
	const MINIMUM_LIABILITY                                 = 'MINIMUM_LIABILITY';
	const REQUIRE_ADDITIONAL_INTEREST                       = 'REQUIRE_ADDITIONAL_INTEREST';
	const ADDITIONAL_INTEREST_NAME                          = 'ADDITIONAL_INTEREST_NAME';
	const ADDITIONAL_INTEREST_ADDRESS                       = 'ADDITIONAL_INTEREST_ADDRESS';

	const PAID_VERIFICATION                                     = 'PAID_VERIFICATION';

	const SOCIAL_MEDIA_POSTING_FALURE 						= 'SOCIAL_MEDIA_POSTING_FALURE';
	const SOCIAL_MEDIA_POST_PENDING_APPROVAL 				= 'SOCIAL_MEDIA_POST_PENDING_APPROVAL';
	const SOCIAL_MEDIA_POST_REQUIRES_REVISIONS 				= 'SOCIAL_MEDIA_POST_REQUIRES_REVISIONS';
	const ILS_PORTAL_PROPERTY_WEBSITE 				        = 'ILS_PORTAL_PROPERTY_WEBSITE';
	const ILS_PORTAL_CHECK_AVAILABILITY_DOMAIN 				= 'ILS_PORTAL_CHECK_AVAILABILITY_DOMAIN';

	const SQUARE_FEET                                       = 'Square Feet';
	const SQUARE_METER                                      = 'Square Meter';

	const POUNDS                                       = 'Pounds (lbs)';
	const KILOGRAMS                                    = 'Kilograms (kg)';

	const MILES                                       = 'Miles (mi)';
	const KILOMETERS                                    = 'Kilometers (km)';

	const KEY_PRE_BILL_NOTIFICATION_EMAIL                   = 'PRE_BILL_NOTIFICATION_EMAIL';
	const KEY_DEFAULT_ASSET_LOCATION_ID                     = 'DEFAULT_ASSET_LOCATION_ID';

	const LEASE_TERM_SORTING_ORDER_ASC						= 1;
	const LEASE_TERM_SORTING_ORDER_DESC						= 2;

	const HAND_DELIVER										= 1;
	const EMAIL												= 2;
	const EMAIL_AND_HAND_DELIVER							= 3;

	const MAX_ALLOW_INTEGRATION_OF_DUPLICATE_GUEST_CARDS_AFTER_DAYS = 9999;
	const MAX_FILE_UPLOAD_VALUE = 8;

	const SIMPLEMENTATION_PORTAL_ACTIVATED		= 'SIMPLEMENTATION_PORTAL_ACTIVATED';
	const SIMPLEMENTATION_PORTAL_COMPLETED		= 'SIMPLEMENTATION_PORTAL_COMPLETED';
	const ACCEPT_INTERNATIONAL_PHONE			= 'ACCEPT_INTERNATIONAL_PHONE';

	const CHARGE_CODE_MAPPING                   = 'CHARGE_CODE_MAPPING';
	const LEAD_SOURCE_MAPPING                   = 'LEAD_SOURCE_MAPPING';
	const LEASE_TERM_MAPPING                    = 'LEASE_TERM_MAPPING';
	const LEASING_AGENT_MAPPING                 = 'LEASING_AGENT_MAPPING';
	const PET_TYPE_MAPPING                      = 'PET_TYPE_MAPPING';
	const MOVE_OUT_REASON_MAPPING               = 'MOVE_OUT_REASON_MAPPING';
	const MAINTENANCE_PROBLEMS_MAPPING          = 'MAINTENANCE_PROBLEMS_MAPPING';
	const MAINTENANCE_LOCATIONS_MAPPING          = 'MAINTENANCE_LOCATIONS_MAPPING';
	const MAINTENANCE_PRIORITIES_MAPPING		= 'MAINTENANCE_PRIORITIES_MAPPING';
	const MAINTENANCE_STATUSES_MAPPING			= 'MAINTENANCE_STATUSES_MAPPING';
	const MAINTENANCE_PROBLEMS_LOCATIONS_MAPPING = 'MAINTENANCE_PROBLEMS_LOCATIONS_MAPPING';

	const INSURANCE_POLICY_STATUS_TYPE_MAPPING  = 'INSURANCE_POLICY_STATUS_TYPE_MAPPING';

	const REPLY_FORWARDING_EMAIL_CREATOR 		= 1;
	const REPLY_FORWARDING_PROPERTY_EMAIL 		= 2;
	const REPLY_FORWARDING_ENTRATA_USER_EMAIL 	= 3;
	const REPLY_FORWARDING_CUSTOM_EMAIL 		= 4;

	const EMAIL_PREFERENCE_NEVER                = 0;
	const EMAIL_PREFERENCE_ALWAYS               = 1;
	const EMAIL_PREFERENCE_DEFAULT_NO           = 3;
	const EMAIL_PREFERENCE_DEFAULT_YES          = 2;

	const SCREENING_REQUIRED 		= 2;
	const SCREENING_OPTIONAL 		= 1;
	const NO_SCREENING 		        = 0;

	const PRICING_BENCHMARK_PRE_LEASE_PAST_YEAR     = 'Target_Property_Past_One_Year';
	const PRICING_BENCHMARK_PRE_LEASE_PAST_TWO_YEAR = 'Target_Property_Past_Two_Year';
	const PRICING_BENCHMARK_COMPETITORS             = 'Market_Insights_Selected_Competitors';
	const PRICING_BENCHMARK_MARKET                  = 'Market_Insights_Market';

	const PRICING_ANCHOR_CONSTRAINT_EXECUTED          = 'EXECUTED';
	const PRICING_ANCHOR_CONSTRAINT_BUDGETED          = 'BUDGETED';
	const PRICING_ANCHOR_CONSTRAINT_EXECUTED_BUDGETED = 'EXECUTED_BUDGETED';

	const SMS_OPT_IN_TYPE_SINGLE = 0;
	const SMS_OPT_IN_TYPE_DOUBLE = 1;

	const STUDENT_AVAILABILITY_LEASE_BY_UNIT_SPACE = 2;

	public static $c_arrstrAutoGenerateLeaseKeys			= [ 'COMBINE_RENEWAL_OFFERS_LEASE_VERSION' ];
	public static $c_arrintFMOStatementPropertyPreferences	= [
		self::SHOW_PRIMARY_ON_FMO_STATEMENT						=> [ CCustomerType::PRIMARY ],
		self::SHOW_PRIMARY_AND_GUARANTOR_ON_FMO_STATEMENT		=> [ CCustomerType::PRIMARY, CCustomerType::GUARANTOR ],
		self::SHOW_ALL_OCCUPANTS_ON_FMO_STATEMENT				=> [ CCustomerType::PRIMARY, CCustomerType::RESPONSIBLE, CCustomerType::NOT_RESPONSIBLE, CCustomerType::GUARANTOR, CCustomerType::NON_LEASING_OCCUPANT ],
		self::SHOW_ALL_RESPONSIBLE_OCCUPANTS_ON_FMO_STATEMENT	=> [ CCustomerType::PRIMARY, CCustomerType::RESPONSIBLE, CCustomerType::GUARANTOR ]
	];

	public static $c_arrstrMaintenanceRequestPropertyPreferences = [
		'DO_NOT_SEND_EMAIL_UPDATES_FOR_STATUS_OPEN',
		'DO_NOT_SEND_EMAIL_UPDATES_FOR_STATUS_SUSPENDED',
		'DO_NOT_SEND_EMAIL_UPDATES_FOR_STATUS_CLOSED',
		'DO_NOT_SEND_EMAIL_UPDATES_FOR_STATUS_CANCELLED',
		'DO_NOT_SEND_WORK_ORDER_EMAILS_TO_RESIDENTS',
		'DO_NOT_SEND_WORK_ORDER_EMAILS_TO_ANY_PROPERTY_EMPLOYEES',
		'EXPORT_FLOATING_WO',
		'SEND_EMAIL_UPDATES_ONLY_ON_RESIDENT_SUBMITTED_WORK_ORDERS'
	];

	public static $c_arrstrLeasingCenterPropertyPreferenceKeys = [
		'LEASING_CENTER_SETTING_HIDE_RENT_RANGE_REALTIME',
		'APPLY_PROSPECT_PORTAL_SETTING_FOR_LEASING_CENTER',
		'HIDE_FLOORPLAN_RENT_RANGE_WHEN_NO_AVAILABLE_UNITS',
		'HIDE_UNIT_AVAILABILITY',
		'SPECIFIC_UNITS_OVERRIDE_AVAILABLE_UNITS',
		'UNITS_DISPLAY_ORDER',
		'CATEGORIZE_FLOOR_PLANS_BY_BEDROOM_COUNT',
		'SHOW_FLOOR_NUMBER_IN_FLOOR_PLAN_FILTER',
		'HIDE_UNITS_WITH_NO_RENT',
		'ENTRATA_ONLY_SHOW_UNITS_WITH_NO_RENT_IN_UNIT_SELECTOR',
		'GENERATE_FLOORPLAN_RENT_USING_VISIBLE_UNITS',
		'FLOORPLAN_UNIT_DISPLAY_LIMIT'
	];

	public static $c_arrstrMoveOutSmartPropertyPreferenceKeys 	= [
		'SMART_DEVICES_MOVE_OUT_LIGHTS_SWITCH',
		'SMART_DEVICES_MOVE_OUT_THERMOSTAT',
		'SMART_DEVICES_MOVE_OUT_WATER_SHUTOFF_VALVES',
		'SMART_DEVICES_MOVE_OUT_HIGH_VOLTAGE_SHUT_OFF_SWITCH',
		'SMART_DEVICES_MOVE_OUT_OUTLETS',
		'SMART_DEVICES_MOVE_OUT_FAN',
		'ALEXA_FOR_RESIDENTIAL'
	];

	public static $c_arrstrMoveInSmartPropertyPreferenceKeys 	= [
		'SMART_DEVICES_MOVE_IN_LIGHTS_SWITCH',
		'SMART_DEVICES_MOVE_IN_OUTLETS',
		'SMART_DEVICES_MOVE_IN_THERMOSTAT',
		'SMART_DEVICES_MOVE_IN_WATER_SHUTOFF_VALVES',
		'SMART_DEVICES_MOVE_IN_HIGH_VOLTAGE_SHUT_OFF_SWITCH',
		'SMART_DEVICES_MOVE_IN_FAN',
		'ALEXA_FOR_RESIDENTIAL'
	];
	public static $c_arrstrMigrationClientLevelMappings = [
		self::CHARGE_CODE_MAPPING              => 'CHARGE_CODE_MAPPING',
		self::INSURANCE_POLICY_STATUS_TYPE_MAPPING              => 'INSURANCE_POLICY_STATUS_TYPE_MAPPING'
	];

	public static $c_arrstrMigrationPropertyMappings = [
		self::LEAD_SOURCE_MAPPING              => 'LEAD_SOURCE_MAPPING',
		self::LEASE_TERM_MAPPING               => 'LEASE_TERM_MAPPING',
		self::LEASING_AGENT_MAPPING            => 'LEASING_AGENT_MAPPING',
		self::PET_TYPE_MAPPING                 => 'PET_TYPE_MAPPING',
		self::MOVE_OUT_REASON_MAPPING          => 'MOVE_OUT_REASON_MAPPING',
		self::MAINTENANCE_PROBLEMS_MAPPING     => 'MAINTENANCE_PROBLEMS_MAPPING',
		self::MAINTENANCE_LOCATIONS_MAPPING     => 'MAINTENANCE_LOCATIONS_MAPPING',
		self::MAINTENANCE_PROBLEMS_LOCATIONS_MAPPING => 'MAINTENANCE_PROBLEMS_LOCATIONS_MAPPING'
	];

	public static $c_arrstrInsuranceAdditionalInterestPreferenceKeys = [ 'REQUIRE_ADDITIONAL_INTEREST', 'ADDITIONAL_INTEREST_NAME', 'ADDITIONAL_INTEREST_ADDRESS' ];

	public static $c_arrstrMaintenanceForSamsungPreferenceKeys = [
		'DEFAULT_LOCATION_FOR_SAMSUNG_SETTINGS',
		'DEFAULT_PROBLEM_FOR_SAMSUNG_SETTINGS',
		'DEFAULT_PRIORITY_FOR_SAMSUNG_SETTINGS'
	];

	public function __construct() {
		parent::__construct();

		$this->m_arrstrAllowedScriptTagKeys = [ 'CHAT_LIVE_MONITOR_TAG', 'CHECK_AVAILABILITY_INSTRUCTIONS', 'AVAILABILITY_ALERT_CUSTOM_CONFIRMATION_TEXT', 'DEFAULT_EMAIL_RELAY_REPLY_FORWARDING_ADDRESS','EMAIL_RELAY_ENTRATA_USER_EMAIL_REPLY_FORWARDING_ADDRESS' ];
		return;
	}

	/**
	 * Set Functions
	 */

	public function setValue( $strValue, $strLocaleCode = NULL ) {

		if( false == is_null( $this->getKey() ) && true == in_array( $this->getKey(), $this->m_arrstrAllowedScriptTagKeys ) ) {
			$this->m_strValue = CStrings::strTrimDef( $strValue, -1, NULL, true, true );
		} else {
			parent::setValue( $strValue, $strLocaleCode );
		}
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intId ) || ( 1 > $this->m_intId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Property preference id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyId ) || ( 1 > $this->m_intPropertyId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertySettingKeyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey( $objDatabase = NULL ) {
		$boolIsValid = true;
		$objPropertyPreference 	= NULL;
		$objLateNoticeType	= NULL;

		if( false == isset( $this->m_strKey ) || ( false == $this->m_strKey ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', __( 'Property preference key must be provided.' ) ) );
		}

		if( true == isset( $this->m_strValue ) && 'MAXIMUM_OCCUPANTS_ALLOWED' == $this->m_strKey ) {
			if( false == is_numeric( $this->m_strValue ) || 0 >= $this->m_strValue ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_occupants', __( 'Max occupants allowed must be blank or greater than zero.' ) ) );
			}
		}

		if( 'SEND_AFTER_DELINQUENCY' == $this->m_strKey ) {
			if( false == isset( $this->m_strValue ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'SEND_AFTER_DELINQUENCY', __( 'Number of days being delinquent is required.' ) ) );
			} elseif( false == is_numeric( $this->m_strValue ) && 0 >= $this->m_strValue ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'SEND_AFTER_DELINQUENCY', __( 'Number of days being delinquent should be numeric .' ) ) );
			} elseif( 0 >= $this->m_strValue ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'SEND_AFTER_DELINQUENCY', __( 'Number of days being delinquent should be greater than zero .' ) ) );
			} elseif( !( ( string ) round( $this->m_strValue ) == $this->m_strValue && false == \Psi\CStringService::singleton()->strpos( $this->m_strValue, '.' ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'SEND_AFTER_DELINQUENCY', __( 'Number of days being delinquent should not be decimal.' ) ) );
			}
		}

		if( 'AUTOMATICALLY_RESEND_EVERY_DAYS' == $this->m_strKey ) {
			if( false == isset( $this->m_strValue ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'AUTOMATICALLY_RESEND_EVERY_DAYS', __( 'Number of days for automatically resend late notice is required.' ) ) );
			} elseif( false == is_numeric( $this->m_strValue ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'AUTOMATICALLY_RESEND_EVERY_DAYS', __( 'Number of days for automatically resend late notice should be numeric .' ) ) );
			} elseif( 0 >= $this->m_strValue ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'AUTOMATICALLY_RESEND_EVERY_DAYS', __( 'Number of days for automatically resend late notice should be greater than zero .' ) ) );
			} elseif( !( ( string ) round( $this->m_strValue ) == $this->m_strValue && false == \Psi\CStringService::singleton()->strpos( $this->m_strValue, '.' ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'AUTOMATICALLY_RESEND_EVERY_DAYS', __( 'Number of days for automatically resend late notice should not be decimal.' ) ) );
			}
		}

		if( true == isset( $this->m_strValue ) && 'DEMAND_NOTICE_THRESHOLD' == $this->m_strKey ) {
			$objPropertyPreference 	= \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'AUTOMATICALLY_SEND_LATE_NOTICES', $this->m_intPropertyId, $this->m_intCid, $objDatabase );
			$objLateNoticeType		= \Psi\Eos\Entrata\CLateNoticeTypes::createService()->fetchLateNoticeTypeByPropertyIdByDocumentSubTypeIdByCid( $this->m_intPropertyId, CDocumentSubType::DEMAND_NOTICE, $this->m_intCid, $objDatabase );

			if( false == is_numeric( $this->m_strValue ) || 0 > $this->m_strValue ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'DEMAND_NOTICE_THRESHOLD', __( 'Demand notice threshold amount should be positive.' ) ) );
			} elseif( '0.00' == $this->m_strValue ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'DEMAND_NOTICE_THRESHOLD', __( 'Demand notice threshold amount should be greater than zero.' ) ) );
			}

			if( ( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && 1 == $objPropertyPreference->getValue() && false == valObj( $objLateNoticeType, 'CLateNoticeType' ) )
			    || ( true == valObj( $objLateNoticeType, 'CLateNoticeType' ) && false == $objLateNoticeType->getIsPublished() )
			    || false == valObj( $objLateNoticeType, 'CLateNoticeType' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'DEMAND_NOTICE_THRESHOLD', __( 'Demand threshold cannot be set. To set a demand threshold please publish and associate a demand notice.' ) ) );
				return $boolIsValid;
			}
		}

		if( true == isset( $this->m_strValue ) && 'MINIMUM_BALANCE_DUE_THRESHOLD' == $this->m_strKey ) {
			if( false == is_numeric( $this->m_strValue ) || 0 > $this->m_strValue ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'MINIMUM_BALANCE_DUE_THRESHOLD', __( 'Minimum balance due threshold amount should be positive.' ) ) );
			} elseif( '0.00' == $this->m_strValue ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'MINIMUM_BALANCE_DUE_THRESHOLD', __( 'Minimum balance due threshold amount should be greater than zero.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valAffordablePreferencesVal( $objDatabase, $arrmixDependentPropertyPreferences ) {

		$boolIsValid = true;

		$arrstrNineDigitValidationKeys = [
			'OWNER_DUNS_NUMBER'					=> __( 'Owner DUNS number' ),
			'PARENT_COMPANY_DUNS_NUMBER'		=> __( 'Parent company DUNS number' ),
			'EMPLOYER_IDENTIFICATION_NUMBER'	=> __( 'Employer identification number' ),
			'OWNER_TIN'							=> __( 'Owner taxpayer identification number ' ),
			'PARENT_COMPANY_TIN'				=> __( 'Parent company taxpayer identification number' )
		];

		$arrstrHudPropertyValidationKeys = [
			'IMAX_ID_SENDER'					=> __( 'IMAX ID sender' ),
			'IMAX_ID_RECIPIENT'					=> __( 'IMAX ID recipient' ),
			'TRACS_VERSION'						=> __( 'TRACS version' ),
			'EMPLOYER_IDENTIFICATION_NUMBER'	=> __( 'Employer identification number' )
		];

		$arrstrUniqueValidationKeys = [ 'IMAX_ID_SENDER' ];

		$arrstrAlphaNumericValidationKeys = [
			'EMPLOYER_IDENTIFICATION_NUMBER'	=> __( 'Employer identification number' ),
			'OWNER_TIN'							=> __( 'Owner taxpayer identification number ' ),
			'PARENT_COMPANY_TIN'				=> __( 'Parent company taxpayer identification number' ),
			'OWNER_DUNS_NUMBER'					=> __( 'Owner DUNS number' ),
			'PARENT_COMPANY_DUNS_NUMBER'		=> __( 'Parent company DUNS number' ),
			'PROJECT_NUMBER'					=> __( 'Project number' ),
			'PROJECT_NAME'						=> __( 'Project name' )
		];

		$arrstrIntegerValidationKeys = [
			'IMAX_ID_SENDER'	=> __( 'IMAX ID sender' ),
			'IMAX_ID_RECIPIENT'	=> __( 'IMAX ID recipient' )
		];

		$arrstrPerticularSubsidyContractValidationKeys = [
			'OWNER_DUNS_NUMBER'				=> __( 'Owner DUNS number' ),
			'PARENT_COMPANY_DUNS_NUMBER'	=> __( 'Parent company DUNS number' )
		];

		$arrintSubsidyContractTypes = [ CSubsidyContractType::SUBSIDY_CONTRACT_TYPE_SECTION_236, CSubsidyContractType::SUBSIDY_CONTRACT_TYPE_BMIR, CSubsidyContractType::SUBSIDY_CONTRACT_TYPE_RENT_SUPPLEMENT, CSubsidyContractType::SUBSIDY_CONTRACT_TYPE_RAP, CSubsidyContractType::SUBSIDY_CONTRACT_TYPE_SECTION_202_162_PAC, CSubsidyContractType::SUBSIDY_CONTRACT_TYPE_SECTION_202_PRAC, CSubsidyContractType::SUBSIDY_CONTRACT_TYPE_SECTION_811_PRAC, CSubsidyContractType::SUBSIDY_CONTRACT_TYPE_SECTION_8 ];
		$arrintPerticularSubsidyContractTypes = [ CSubsidyContractType::SUBSIDY_CONTRACT_TYPE_RENT_SUPPLEMENT, CSubsidyContractType::SUBSIDY_CONTRACT_TYPE_RAP,  CSubsidyContractType::SUBSIDY_CONTRACT_TYPE_SECTION_202_PRAC, CSubsidyContractType::SUBSIDY_CONTRACT_TYPE_SECTION_811_PRAC, CSubsidyContractType::SUBSIDY_CONTRACT_TYPE_SECTION_8 ];

		if( 'PROJECT_NUMBER' == $this->m_strKey ) {
			$intSubsidyContracts = CSubsidyContracts::fetchSubsidyContractCountByCidByPropertyId( $this->m_intCid, $this->m_intPropertyId, $arrintSubsidyContractTypes, $objDatabase );
			if( 0 < $intSubsidyContracts && ( true == empty( $this->m_strValue ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', __( 'Project number is required.' ) ) );
			}

			if( 8 < \Psi\CStringService::singleton()->strlen( trim( $this->m_strValue ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', __( 'Project number accepts less than or equal to {%d, 0} characters.', [ 8 ] ) ) );
			}
		}
		if( true == array_key_exists( $this->m_strKey, $arrstrPerticularSubsidyContractValidationKeys ) ) {
			$intPerticularSubsidyContracts = CSubsidyContracts::fetchSubsidyContractCountByCidByPropertyId( $this->m_intCid, $this->m_intPropertyId, $arrintPerticularSubsidyContractTypes, $objDatabase );
			if( 0 < $intPerticularSubsidyContracts && ( true == empty( $this->m_strValue ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', __( '{%s, 0} is required.', [ $arrstrPerticularSubsidyContractValidationKeys[$this->m_strKey] ] ) ) );
			}
		}

		if( true == array_key_exists( $this->m_strKey, $arrstrHudPropertyValidationKeys ) ) {
			$intPropertyId 	= CSubsidyContracts::fetchHudPropertyIdByCidByPropertyId( $this->m_intCid, $this->m_intPropertyId, $objDatabase );
			if( true == valStr( $intPropertyId ) ) {
				if( true == empty( $this->m_strValue ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', __( '{%s, 0} required for HUD property.', [ $arrstrHudPropertyValidationKeys[$this->m_strKey] ] ) ) );
				}
			}
		}

		if( true == array_key_exists( $this->m_strKey, $arrstrAlphaNumericValidationKeys ) ) {
			if( false == empty( $this->m_strValue ) ) {
				if( false == ctype_alnum( $this->m_strValue ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value',  __( '{%s, 0} requires alphanumeric value.', [ $arrstrAlphaNumericValidationKeys[$this->m_strKey] ] ) ) );
				}
			}
		}
		if( true == array_key_exists( $this->m_strKey, $arrstrNineDigitValidationKeys ) ) {
			if( false == empty( $this->m_strValue ) && 9 < \Psi\CStringService::singleton()->strlen( trim( $this->m_strValue ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', __( '{%s, 0} accepts less than or equal to {%d, 1} characters.', [ $arrstrNineDigitValidationKeys[$this->m_strKey], 9 ] ) ) );
			}
		}

		if( 'PROJECT_NAME' == $this->m_strKey ) {
			if( false == empty( $this->m_strValue ) && 35 < \Psi\CStringService::singleton()->strlen( trim( $this->m_strValue ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', ' ' . __( 'Project name accepts less than or equal to {%d, 0} characters.', [ 35 ] ) ) );
			}
		}
		if( true == array_key_exists( $this->m_strKey, $arrstrIntegerValidationKeys ) ) {
			if( false == empty( $this->m_strValue ) ) {
				if( false == ctype_digit( $this->m_strValue ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', __( '{%s, 0} requires an integer value. ', [ $arrstrIntegerValidationKeys ][$this->m_strKey] ) ) );

				} elseif( 5 != \Psi\CStringService::singleton()->strlen( $this->m_strValue ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', __( '{%s, 0} must be of {%d, 1} digits.', [ $arrstrIntegerValidationKeys[$this->m_strKey], 5 ] ) ) );

				} elseif( true == in_array( $this->m_strKey, $arrstrUniqueValidationKeys ) ) {
					if( 'IMAX_ID_SENDER' == $this->m_strKey ) {
						$intPropertyId = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByValueByCid( $this->m_strKey, 'TRACM' . $this->m_strValue, $this->m_intCid, $objDatabase );
					}
					if( true == valStr( $intPropertyId ) && $this->m_intPropertyId != $intPropertyId ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', __( 'IMAX ID sender already exist for other property' ) ) );
					}
				}
			}
		}
		if( false == is_null( $arrmixDependentPropertyPreferences ) ) {

			if( 'OWNER_DUNS_NUMBER' == $this->m_strKey && false == empty( $arrmixDependentPropertyPreferences['OWNER_DUNS_NUMBER'] ) && true == empty( $arrmixDependentPropertyPreferences['OWNER_TIN'] ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', __( 'Owner Taxpayer Identification Number is required.' ) ) );
			}

			if( 'PARENT_COMPANY_DUNS_NUMBER' == $this->m_strKey && false == empty( $arrmixDependentPropertyPreferences['PARENT_COMPANY_DUNS_NUMBER'] ) && true == empty( $arrmixDependentPropertyPreferences['PARENT_COMPANY_TIN'] ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', __( 'Parent Company Taxpayer Identification Number is required.' ) ) );
			}

			if( 'ANNUAL_RECERTIFICATION_PROCESS' == $this->m_strKey && ( 'All households on a specific date' == $arrmixDependentPropertyPreferences['ANNUAL_RECERTIFICATION_PROCESS'] || 'All buildings on a specific date' == $arrmixDependentPropertyPreferences['ANNUAL_RECERTIFICATION_PROCESS'] ) && true == empty( $arrmixDependentPropertyPreferences['ANNUAL_RECERTIFICATION_PROCESS_DATE'] ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', __( 'Annual Recertification Process Date is required.' ) ) );
			}

		}
		return $boolIsValid;

	}

	public function valTransferFeeAmount() {
		$boolIsValid = true;

		if( 'TRANSFER_FEE_AMOUNT' == $this->getKey() && false == valStr( $this->getValue() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', __( 'Transfer fee amount is required.' ) ) );
		}

		if( 'TRANSFER_FEE_AMOUNT' == $this->getKey() && true == valStr( $this->getValue() ) && 0 >= $this->getValue() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', __( 'Transfer fee must be greater than zero.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPsCategoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceCategoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public static function validateAssets( $arrmixPropertyPreferences, $intPropertyId, $intCid, $objClientDatabase ) {

		$arrobjErrorMsgs			= [];
		$arrmixPropertyGroupIds		= [];

		$arrobjCompanyPreferences	= rekeyObjects( 'Key', ( array ) \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferenceByKeysByCid( [ 'USE_ITEM_CATALOG', 'USE_ASSET_TRACKING' ], $intCid, $objClientDatabase ) );

		$arrmixPropertyGroupIds		= rekeyArray( 'property_group_id', ( array ) \Psi\Eos\Entrata\CPropertyGroupAssociations::createService()->fetchPropertyGroupAssociationsByCidByPropertyIds( $intCid, [ $intPropertyId ], $objClientDatabase ) );
		$arrmixGlAccounts			= rekeyArray( 'id', ( array ) \Psi\Eos\Entrata\CGlAccounts::createService()->fetchAssociatedGlAccountsByPropertyGroupIdsByCid( array_keys( $arrmixPropertyGroupIds ), $intCid, $objClientDatabase, [ CGlAccountType::LIABILITIES ] ) );

		if( true == isset( $arrmixPropertyPreferences['PURCHASE_CLEARING_ACCOUNT']['value'] ) ) {

			$intGlAccountId = $arrmixPropertyPreferences['PURCHASE_CLEARING_ACCOUNT']['value'];

			if( true == valArr( $arrmixGlAccounts ) && false == array_key_exists( $intGlAccountId, $arrmixGlAccounts ) ) {
				$arrobjErrorMsgs[] = new CErrorMsg( ERROR_TYPE_VALIDATION, 'PURCHASE_CLEARING_ACCOUNT', __( 'GL account is not associated with Property.' ) );
			} elseif( true == isset( $arrmixGlAccounts[$intGlAccountId]['gl_account_usage_type_id'] ) && CGlAccountUsageType::ACCOUNTS_PAYABLE == $arrmixGlAccounts[$intGlAccountId]['gl_account_usage_type_id'] ) {
				$arrobjErrorMsgs[] = new CErrorMsg( ERROR_TYPE_VALIDATION, 'PURCHASE_CLEARING_ACCOUNT', __( 'GL account cannot be of type Accounts Payable.' ) );
			}
		}

		if( true == array_key_exists( 'TRACK_INVENTORY_QUANTITIES', $arrmixPropertyPreferences )
		    && 1 == $arrmixPropertyPreferences['TRACK_INVENTORY_QUANTITIES']['value']
		    && false == array_key_exists( 'USE_ITEM_CATALOG', $arrobjCompanyPreferences ) ) {

			$arrobjErrorMsgs[] = new CErrorMsg( ERROR_TYPE_VALIDATION, 'USE_ITEM_CATALOG', __( 'Use Item Catalog must be enabled in Corporate Settings. Click <a href="?module=catalog_items_setupxxx" target="_blank">here</a> to go there now.' ) );
		}

		return $arrobjErrorMsgs;
	}

	public function validate( $strAction, $objDatabase = NULL, $arrmixDependentPropertyPreferences = NULL ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valKey( $objDatabase );
				break;

			case 'VALIDATE_BILLING_PREFERENCES':
				$boolIsValid &= $this->valPostForNextMonth( $arrmixDependentPropertyPreferences );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			case 'validate_affordable_preferences':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valKey();
				$boolIsValid &= $this->valAffordablePreferencesVal( $objDatabase, $arrmixDependentPropertyPreferences );
				break;

			case 'validate_transfer_fee_preferences':
				$boolIsValid &= $this->valTransferFeeAmount();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getConsolidatedErrorMsg() {
		$strErrorMessage = NULL;
		if( true == valArr( $this->getErrorMsgs() ) ) {
			foreach( $this->getErrorMsgs() as $objErrorMsg ) {
				$strErrorMessage .= $objErrorMsg->getMessage() . "\n";
			}
		}
		return $strErrorMessage;
	}

	public function createPropertyPreference( $intPropertySettingKeyId, $strPropertySettingKey, $strValue, $intPropertyId, $intCid ) {
		$objPropertyPreference = new CPropertyPreference();
		$objPropertyPreference->setCid( $intCid );
		$objPropertyPreference->setPropertyId( $intPropertyId );
		$objPropertyPreference->setKey( $strPropertySettingKey );
		$objPropertyPreference->setPropertySettingKeyId( $intPropertySettingKeyId );
		$objPropertyPreference->setValue( $strValue );

		return $objPropertyPreference;
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getUnitOfMeasureForArea() {
		return [
			self::SQUARE_FEET 		=> __( 'Square Feet' ),
			self::SQUARE_METER 		=> __( 'Square Meter' ),
		];
	}

}
?>