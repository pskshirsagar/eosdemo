<?php

class CApHeaderLeaseCustomer extends CBaseApHeaderLeaseCustomer {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApHeaderId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseCustomerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>