<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultContactSubmissionTypes
 * Do not add any new functions to this class.
 */

class CDefaultContactSubmissionTypes extends CBaseDefaultContactSubmissionTypes {

	public static function fetchDefaultContactSubmissionTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CDefaultContactSubmissionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>