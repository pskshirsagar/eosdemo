<?php

class CPropertyGroupType extends CBasePropertyGroupType {

	// @TODO: 3rd & 4th constant below should start with DEFAULT_PROPERTY_GROUP_TYPE
	const DEFAULT_PROPERTY_GROUP_TYPE_STATE_SYSTEM_CODE		= 'STATE'; 		// Grouping by States
	const DEFAULT_PROPERTY_GROUP_TYPE_PROPTYPE_SYSTEM_CODE 	= 'PROPTYPE'; 	// Grouping by Property Types
	const PROPERTY_LIST_SYSTEM_CODE							= 'PROPERTY'; 	// This will contain all individual property.
	const ALL_PROPERTY_SYSTEM_CODE							= 'ALL'; 		// This will contain all properties.
	const CENTRAL_LEASING_OFFICE_SYSTEM_CODE				= 'CENTRAL'; 	// This will contain all central leasing office properties.
	const HISTORICAL_ACCESS_SYSTEM_CODE						= 'HISTORIC'; 	// This will contain all disabled & properties with historical access product.

	/**
	 * @param $strSystemCode
	 * @param $objDatabase
	 * @return CPropertyGroup|mixed|null
	 */
	public function createOrFetchPropertyGroupBySystemCode( $strSystemCode, $objDatabase ) {

		$objPropertyGroup = self::fetchPropertyGroupBySystemCodeByCid( $strSystemCode, $objDatabase );

		if( false == valObj( $objPropertyGroup, 'CPropertyGroup' ) ) {

			$objPropertyGroup = new CPropertyGroup();
			$objPropertyGroup->setDefaults();
			$objPropertyGroup->setCid( $this->getCid() );
			$objPropertyGroup->setPropertyGroupTypeId( $this->getId() );
			$objPropertyGroup->setSystemCode( $strSystemCode );
			$objPropertyGroup->setIsSystem( 1 );

			switch( $this->getSystemCode() ) {
				case self::DEFAULT_PROPERTY_GROUP_TYPE_STATE_SYSTEM_CODE:
					$objPropertyGroup->setName( \Psi\Eos\Entrata\CStates::createService()->fetchStateByCode( $strSystemCode, $objDatabase )->getName() );
					break;

				case self::DEFAULT_PROPERTY_GROUP_TYPE_PROPTYPE_SYSTEM_CODE:
					$objPropertyGroup->setName( CPropertyTypes::fetchPropertyTypeById( $strSystemCode, $objDatabase )->getName() );
					break;

				default:
					trigger_error( 'Invalid system code for Property Group' );

			}
		}

		return $objPropertyGroup;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPropertyGroups( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyGroups::createService()->fetchPropertyGroupsByPropertyGroupTypeIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyGroupBySystemCodeByCid( $strSystemCode, $objDatabase ) {

		$strSql = ' SELECT
						pg.*
					FROM
						property_groups pg
						JOIN property_group_types pgt ON ( pgt.cid = ' . $this->getCid() . '
															AND pg.property_group_type_id = pgt.id
															AND pg.cid = pgt.cid
															AND pgt.system_code = \'' . $this->getSystemCode() . '\'
															AND pg.is_system = 1 
															AND pgt.deleted_by IS NULL
															AND pgt.deleted_on IS NULL )
					WHERE
						pg.cid = ' . $this->getCid() . '
						AND pg.system_code = \'' . $strSystemCode . '\'
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL;';

		return \Psi\Eos\Entrata\CPropertyGroups::createService()->fetchPropertyGroup( $strSql, $objDatabase );
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Property Group Type client does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSystemCode() {
		$boolIsValid = true;
		if( 1 == $this->getIsSystem() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_code', 'Default Property Groups cannot be Edited.' ) );
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Category Name is required.' ) ) );
		} elseif( false == is_null( $this->m_strName ) ) {

			if( \Psi\CStringService::singleton()->strlen( $this->m_strName ) < 4 ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Category Name should have at least 4 characters.' ) ) );
				return false;
			}

			// Checking if the Custom Property Group Type 'name' entered by the user is a Smart Group Name
			if( false == $this->getIsSystem() ) {
				$objSmartGroup = \Psi\Eos\Entrata\CDefaultPropertyGroupTypes::createService()->fetchDefaultPropertyGroupTypeByName( $this->getName(), $objDatabase );

				if( true == valObj( $objSmartGroup, 'CDefaultPropertyGroupType' ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "Property Group Type with the 'Name' provided cannot be created/edited, as its named as a Smart Group Type." ) ) );
					return false;
				}
			}

			// Checking for pre-existing groups with the same name
			$strWhereSql = ' WHERE
								cid = ' . ( int ) $this->getCid() . '
								AND LOWER( name ) =  \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->m_strName ) ) . '\'' .
								( ( 0 < $this->m_intId ) ? ' AND id <> ' . $this->m_intId : '' );

			if( 0 < \Psi\Eos\Entrata\CPropertyGroupTypes::createService()->fetchPropertyGroupTypeCount( $strWhereSql, $objDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Category Name provided already exists.' ) ) );
			}

		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strDescription ) || 0 == strlen( $this->m_strDescription ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Description is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUseCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyGroupBeforePropertyGroupTypeDelete( $objDatabase ) {
		$boolIsValid = true;
		$intPropertyGroupCount = \Psi\Eos\Entrata\CPropertyGroups::createService()->fetchPropertyGroupCountByPropertyGroupTypeIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( 0 != $intPropertyGroupCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_group_type_id', __( "Property Group Type '{%s,0}' cannot be deleted. It has Property Groups assigned to it.", [ $this->getName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDescription( $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valPropertyGroupBeforePropertyGroupTypeDelete( $objDatabase );
				break;

			default:
				// default case
				$boolIsValid = false;
				break;
		}
	return $boolIsValid;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( 'NOW()' );

		if( false == $this->update( $intUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

}
?>