<?php

class CScheduledTaskExecution extends CBaseScheduledTaskExecution {

	const SCHEDULED_TASK_ACTION_ALL             = 'All';
	const SCHEDULED_TASK_ACTION_EMAIL           = 'Email';
	const SCHEDULED_TASK_ACTION_SMS             = 'Sms';
	const SCHEDULED_TASK_ACTION_MANUAL_CONTACT  = 'Create Manual Contact';
	const SCHEDULED_TASK_ACTION_NOT_PROGESSING  = 'Flag Not Progressing';
	const SCHEDULED_TASK_ACTION_CANCEL_ARCHIVE  = 'Cancel/Archive Application';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledTaskAction() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEventId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExecutedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExecutedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>