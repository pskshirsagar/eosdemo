<?php

class CUtility extends CBaseUtility {

    /**
     * Create Functions
     */

    public function createPropertyUtility() {

    	$objPropertyUtility = new CPropertyUtility();
    	$objPropertyUtility->setCid( $this->getCid() );
    	$objPropertyUtility->setUtilityId( $this->getId() );
    	$objPropertyUtility->setUtilityFormulaId( CUtilityFormula::NOT_SPECIFIED );

    	return $objPropertyUtility;
    }

    /**
     * Validate Functions
     */

	public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
           $boolIsValid = false;
           $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valName( $objDatabase ) {
        $boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Utility Name is required.' ) ) );

        } elseif( true == valObj( $objDatabase, 'CDatabase' ) ) {

        	$strSqlWhere = 'WHERE
								cid = ' . ( int ) $this->getCid() . '
								AND lower( name ) = \'' . trim( \Psi\CStringService::singleton()->strtolower( addslashes( $this->getName() ) ) ) . '\'';

			if( 0 < $this->getId() ) {
				$strSqlWhere .= 'AND id <>' . ( int ) $this->getId();
			}

			if( 0 < CUtilities::fetchUtilityCount( $strSqlWhere, $objDatabase ) ) {
				$boolIsValid = false;
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Utility Name already exists.' ) ) );
			}
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valCid();
        		$boolIsValid &= $this->valName( $objDatabase );
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>