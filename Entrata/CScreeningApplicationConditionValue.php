<?php

class CScreeningApplicationConditionValue extends CBaseScreeningApplicationConditionValue {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScreeningApplicationConditionSetId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valExternalConditionSubsetId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valConditionName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valConditionTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valConditionAmountTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBaseChargeCodeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplyToChargeCodeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valConditionValue() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFinalValue() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsActive() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSatisfiedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function revertScreeningApplicationConditionValues( $arrintScreeningApplicationSetId, $intCid, $intCurrentUserId, $objDatabase ) {
		if( false == valArr( $arrintScreeningApplicationSetId ) ) return false;

        $strSql = 'UPDATE
                    screening_application_condition_values
                   SET
                    is_reverted = 0,
                    satisfied_by = NULL,
                    satisfied_on = NULL,
                    updated_by = ' . ( int ) $intCurrentUserId . ',
                    updated_on = NOW()
                   WHERE
                    screening_application_condition_set_id IN ( ' . implode( ',', $arrintScreeningApplicationSetId ) . ' )
                    AND cid =  ' . ( int ) $intCid;

        if( false == fetchData( $strSql, $objDatabase ) ) return false;

        return true;
    }

	public function setRates( $objApplication, $objDatabase ) {

		$objRateLog = $this->getRate( $objApplication, $objDatabase );

		if( true == valObj( $objRateLog, 'CRateLog' ) || true == valObj( $objRateLog, 'CRate' ) ) {
			$fltAmount = $objRateLog->getRateAmount();

			if( CScreeningPackageConditionType::REPLACE_SECURITY_DEPOSIT == $this->getConditionTypeId() ) {
				$fltAmount = $this->getCalculatedReplaceSecurityDeposit( $objApplication, $objRateLog, $objDatabase );
			}

			$this->setFinalValue( $fltAmount );
			$this->setApplyToChargeCodeId( $objRateLog->getArCodeId() );
		}
	}

	public function getRate( $objApplication, $objDatabase ) {

    	if( false == in_array( $this->getConditionTypeId(), CScreeningPackageConditionType::$c_arrintFinancialScreeningConditionTypeIds ) ) return;

    	if( CScreeningPackageConditionType::INCREASE_RENT == $this->getConditionTypeId() ) {
    		$intArTriggerId = CArTrigger::MONTHLY;
    	} else {
    		$intArTriggerId = CArTrigger::MOVE_IN;
    	}

		$objProperty                        = $objApplication->getOrFetchProperty( $objDatabase );
		$intPackageSubsetValueId            = $this->getConditionSubsetValueId();
		$intScreeningAvailableConditionId   = $this->getScreeningAvailableConditionId();

		$arrobjRate = \Psi\Eos\Entrata\CRates::createService()->fetchActiveRatesByArOriginReferenceIdByArOriginIdByPropertyIdByCid( $intScreeningAvailableConditionId, CArOrigin::RISK_PREMIUM, $objProperty->getId(), $this->getCid(), $objDatabase );

		if( false == valArr( $arrobjRate ) ) {
			$arrobjRate = \Psi\Eos\Entrata\CRates::createService()->fetchActiveRatesByArOriginReferenceIdByArOriginIdByPropertyIdByCid( $intPackageSubsetValueId, CArOrigin::RISK_PREMIUM, $objProperty->getId(), $this->getCid(), $objDatabase );
			$intArOriginReferenceId = $intPackageSubsetValueId;
		} else {
			$intArOriginReferenceId = $intScreeningAvailableConditionId;
		}

    	if( true == valArr( $arrobjRate ) ) {
    		$arrobjRate = array_values( $arrobjRate );
    		$intArForumlaId = $arrobjRate[0]->getArFormulaId();
    		if( CArFormula::FIXED_AMOUNT != $intArForumlaId ) {
    			$intArTriggerId = 0;
    		} else {
			    $intArTriggerId = $arrobjRate[0]->getArTriggerId();
		    }
    	} else {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, __( 'No rates are setup for the selected conditions with reference id: ' ) . ( int ) $intArOriginReferenceId ) );
    		return false;
    	}

		$arrobjPropertyPreferences = ( array ) CPropertyPreferences::fetchCustomPropertyPreferencesByKeysByPropertyIdByCid( [ 'ENABLE_SEMESTER_SELECTION', 'APPLY_ALL_RATES', 'RV_USE_SCHEDULED_CHARGE_FOR_RATES', 'EXCLUDE_ADD_ONS' ], $objProperty->getId(), $this->getCid(), $objDatabase );

		$objRateLogsFilter 			= new CRateLogsFilter();
		$boolUseScheduledChargeForRates       = ( ( false == array_key_exists( 'RV_USE_SCHEDULED_CHARGE_FOR_RATES', $arrobjPropertyPreferences ) ) || ( true == array_key_exists( 'RV_USE_SCHEDULED_CHARGE_FOR_RATES', $arrobjPropertyPreferences ) && 1 == $arrobjPropertyPreferences['RV_USE_SCHEDULED_CHARGE_FOR_RATES']->getValue() ) ) ? true : false;

		$objRateLogsFilter->setCids( [ $this->getCid() ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $objApplication->getPropertyId() ] );

		if( true == $boolUseScheduledChargeForRates ) {
			$objRate = $arrobjRate[0];

			if( CArFormula::FIXED_AMOUNT == $arrobjRate[0]->getArFormulaId() ) {
				$fltRateAmount = $objRate->getRateAmount();
			} else {

				$fltRateAmount = CScreeningUtils::getRateFromScheduledCharges( $arrobjRate[0], $objApplication, $objDatabase );
			}

			if( $fltRateAmount <= 0 ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, __( 'Failed to load scheduled charge rate for reference id: {%d, 0}. Please contact support.', [ ( int ) $intArOriginReferenceId ] ) ) );
				return false;
			}

			$objRate->setRateAmount( $fltRateAmount );
			return $objRate;
		}

    	if( 0 < $intArTriggerId ) {
		    $objRateLogsFilter->setArTriggerIds( [ $intArTriggerId ] );
		    $objRateLogsFilter->setArOriginReferenceIds( [ $intArOriginReferenceId ] );
		    $objRateLogsFilter->setArOriginIds( [ CArOrigin::RISK_PREMIUM ] );
    	} else {
		    $objRateLogsFilter->setArTriggerIds( [] );
		    $objRateLogsFilter->setArOriginReferenceIds( [] );
		    $objRateLogsFilter->setArOriginIds( [] );
    	}

    	$objRateLogsFilter->setLoadOptionalRates( true );

    	// Condition For Student Property

		$arrintSpaceConfigurationIds = [];

    	if( true == valObj( $objProperty, 'CProperty', 'PropertyTypeId' ) && true == in_array( COccupancyType::STUDENT, $objProperty->getOccupancyTypeIds() ) ) {
		    $boolEnableSemesterSelection = ( true == array_key_exists( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) && 1 == $arrobjPropertyPreferences['ENABLE_SEMESTER_SELECTION']->getValue() ) ? true : false;

		    if( true == $boolEnableSemesterSelection ) {
    			$objRateLogsFilter->setIsStudentProperty( true );
			    $objRateLogsFilter->setLeaseStartWindowId( $objApplication->getLeaseStartWindowId() );
			    $objRateLogsFilter->getIsEntrata( true );

			    $arrintSpaceConfigurationIds = ( true == valId( $objApplication->getSpaceConfigurationId() ) ) ? [ $objApplication->getSpaceConfigurationId() ] : [];
    		}
    	}

    	if( false == is_null( $objApplication->getUnitSpaceId() ) ) {
			$objRateLogsFilter->setUnitSpaceIds( [ $objApplication->getUnitSpaceId() ] );
    	} elseif( false == is_null( $objApplication->getUnitTypeId() ) ) {
			$objRateLogsFilter->setUnitTypeIds( [ $objApplication->getUnitTypeId() ] );
    	}

		$boolUseAllRates = false;
		if( true == array_key_exists( 'APPLY_ALL_RATES', $arrobjPropertyPreferences ) && true == $arrobjPropertyPreferences['APPLY_ALL_RATES']->getValue() ) {
			$arrobjLeaseAssociations = \Psi\Eos\Entrata\CLeaseAssociations::createService()->fetchLeaseAssociationByArOriginIdsByArOriginReferenceIdByLeaseIntervalIdByCid( [ CArOrigin::RISK_PREMIUM ], $intArOriginReferenceId, $objApplication->getLeaseIntervalId(), $objApplication->getCid(), $objDatabase );
			if( true == valArr( $arrobjLeaseAssociations ) ) {
				$boolUseAllRates = true;
				$objRateLogsFilter->setLeaseIntervalIds( [ $objApplication->getLeaseIntervalId() ] );
			}
		}

		$boolExcludeAddOns = ( true == array_key_exists( 'EXCLUDE_ADD_ONS', $arrobjPropertyPreferences ) && true == $arrobjPropertyPreferences['EXCLUDE_ADD_ONS']->getValue() ) ? true : false;

		// If the property setting is ON do not pass addons for rate log filter
		if( true == $boolUseAllRates && true == $boolExcludeAddOns ) {
			$objRateLogsFilter->setArOriginIds( CArOrigin::$c_arrintArOriginIdsWithoutAddOns );
		}

    	if( false !== \Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase ) ) {
		    $arrintLeaseTermIds = ( true == valId( $objApplication->getLeaseTermId() ) ) ? [ $objApplication->getLeaseTermId() ] : [];
		    $arrobjRateLogs = ( array ) \Psi\Eos\Entrata\CRateLogs::createService()->fetchTempProspectRates( $objDatabase, false, false, $arrintLeaseTermIds, NULL, NULL, NULL, $arrintSpaceConfigurationIds );

    		if( false == valArr( $arrobjRateLogs ) ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, __( 'Failed to load rates for reference id : {%d, 0} And cid : ', [ ( int ) $intArOriginReferenceId ] ) . $this->getCid() ) );
    			return;
    		}

		    $objRate = NULL;
		    $objLeaseTermRate = NULL;
		    $fltRateAmount = 0;
		    $arrmixLeaseTermRateAmounts = [];

		    if( 1 < \Psi\Libraries\UtilFunctions\count( $arrobjRateLogs ) ) {
			    $intTotalRateLogs = \Psi\Libraries\UtilFunctions\count( $arrobjRateLogs );
			    foreach( $arrobjRateLogs as $objRateLog ) {
				    if( $intArOriginReferenceId == $objRateLog->getArOriginReferenceId() ) {
					    if( true == is_null( $objRate ) ) $objRate = $objRateLog;

					    if( $objRateLog->getRateAmount() > 0 ) {
						    $arrmixLeaseTermRateAmounts[$objRateLog->getLeaseTermId()] = $objRateLog->getRateAmount();
					    }

					    if( true == valId( $objApplication->getLeaseTermId() ) && $objApplication->getLeaseTermId() == $objRateLog->getLeaseTermId() ) {
						    $objLeaseTermRate = $objRateLog;

						    if( false == $boolUseAllRates ) {
							    break;
						    }
					    }
				    }
			    }

			    if( true == $boolUseAllRates ) {
				    $intLeaseTermCounts = \Psi\Libraries\UtilFunctions\count( $arrmixLeaseTermRateAmounts );
				    foreach( $arrmixLeaseTermRateAmounts as $fltLeaseTermRateAmount ) {
					    $fltRateAmount += $fltLeaseTermRateAmount;
				    }

				    if( $fltRateAmount <= 0 ) {
					    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, __( ' Selected condition sets have a $0 value and cannot be calculated. Lease Terms: {%d, 0} Rate Counts : {%d, 1}, Please review lease term setup', [ ( int ) $intLeaseTermCounts, ( int ) $intTotalRateLogs ] ) ) );
					    return false;
				    }
			    }

		    } else {
			    $arrobjRateLogs = array_values( $arrobjRateLogs );
			    $objRateLog = $arrobjRateLogs[0];
			    if( $intArOriginReferenceId == $objRateLog->getArOriginReferenceId() ) {
				    $fltRateAmount = $objRateLog->getRateAmount();
				    $objRate = $objRateLog;
			    }
		    }
	    }

		if( true == valObj( $objLeaseTermRate, 'CRateLog' ) ) {
			$objRate = $objLeaseTermRate;
		}

    	if( false == valObj( $objRate, 'CRateLog' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, __( 'Failed to load rates for reference id : {%d, 0} And cid : {%d, 1}', [ ( int ) $intArOriginReferenceId, $this->getCid() ] ) ) );
			return false;
		}

		if( true == $boolUseAllRates ) {
			$objRate->setRateAmount( $fltRateAmount );
		}

		if( $objRate->getRateAmount() <= 0 ) {
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, __( 'Selected condition sets have a $0 value and cannot be calculated. Please review screening condition setup' ) ) );
		    return false;
	    }

    	return $objRate;
    }

    public function getCalculatedReplaceSecurityDeposit( $objApplication, $objRateLog, $objDatabase ) {

    	$fltChargeAmount		 = $objRateLog->getRateAmount();
    	$fltCurrentDepositAmount = 0;

	    $arrobjDepositScheduledCharges = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchActiveScheduledChargesByLeaseIdByLeaseIntervalIdsByArTriggerIdsByArCodeGroupIdByArCodeTypeIdByCid( [ $objApplication->getLeaseIntervalId() ], $objApplication->getCid(), $objDatabase, [], NULL, NULL, $objRateLog->getArCodeId() );

    	if( true == valArr( $arrobjDepositScheduledCharges ) ) {
    		foreach( $arrobjDepositScheduledCharges as $objDepositScheduledCharge ) {
    			if( 1 == $objDepositScheduledCharge->getArOriginId() ) {
    				// Consider only the base charge amounts
    				$fltCurrentDepositAmount += $objDepositScheduledCharge->getChargeAmount();
    			}
    		}

    		// Find the difference of New Value and Current Value for Replacement Deposit
    		$fltChargeAmount = $objRateLog->getRateAmount() - $fltCurrentDepositAmount;

    		if( 0 >= $fltChargeAmount ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, __( 'Unable to replace Security Deposit as the calculated amount is less than the current Security Deposit.' ) ) );
    		}
    	}

    	return $fltChargeAmount;
    }

	public function getErrorMessages() {
		$strErrorMessage = '';
		foreach( $this->getErrorMsgs() as $objErrorMsg ) {
			$strErrorMessage .= $objErrorMsg->getMessage() . '~.~';
		}

		return $strErrorMessage;
	}

}
?>