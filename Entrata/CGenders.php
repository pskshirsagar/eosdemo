<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGenders
 * Do not add any new functions to this class.
 */

class CGenders extends CBaseGenders {

	public static function fetchAllGenders( $objDatabase ) {
		$strSql = 'SELECT * FROM genders';
		return self::fetchGenders( $strSql, $objDatabase );
	}

	public static function fetchPublishedGenders( $objDatabase ) {
		$strSql = 'SELECT * FROM
						genders
					WHERE
						is_published = 1
					ORDER BY
						order_num,
						id';

		return self::fetchGenders( $strSql, $objDatabase );
	}
}
?>