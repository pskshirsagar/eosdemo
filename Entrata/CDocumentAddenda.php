<?php

class CDocumentAddenda extends CBaseDocumentAddenda {

	protected $m_strFileType;
	protected $m_strTempFileName;
	protected $m_strFileError;

	protected $m_boolLimitCountersign;

	protected $m_objOldDocumentAddenda;

	protected $m_arrstrAllowedFileTypes = [ 'application/msword' ];
	protected $m_arrstrExternalFormFields;

	protected $m_intIsApplyToGuarantor;
	protected $m_intIsApplyToApplicant;

	protected $m_intIsDynamic;
	protected $m_intIsDefault;

	/**
	 * Create Functions
	 */

	public function createDocumentAddenda() {
		$objDocumentAddenda = new CDocumentAddenda();
		$objDocumentAddenda->setDocumentAddendaId( $this->getId() );
		$objDocumentAddenda->setDocumentId( $this->getDocumentId() );
		$objDocumentAddenda->setCid( $this->getCid() );
		$objDocumentAddenda->setDocumentAddendaTypeId( $this->getDocumentAddendaTypeId() );

		return $objDocumentAddenda;
	}

	public function createDocumentMergeField() {

		$objDocumentMergeField = new CDocumentMergeField();

		$objDocumentMergeField->setCid( $this->m_intCid );
		$objDocumentMergeField->setDocumentId( $this->getDocumentId() );
		$objDocumentMergeField->setDocumentAddendaId( $this->m_intId );

		$objDocumentMergeField->setAllowUserUpdate( 1 );
		$objDocumentMergeField->setAllowAdminUpdate( 1 );
		$objDocumentMergeField->setForceOriginUpdate( 1 );
		$objDocumentMergeField->setShowInMergeDisplay( 1 );
		$objDocumentMergeField->setIsRequired( 0 );
		$objDocumentMergeField->setDataType( CCompanyMergeField::DATA_TYPE_TEXT );

		return $objDocumentMergeField;
	}

	/**
	 * Get Functions
	 */

	public function getFileType() {
		return $this->m_strFileType;
	}

	public function getTempFileName() {
		return $this->m_strTempFileName;
	}

	public function getFileError() {
		return $this->m_strFileError;
	}

	public function getOldDocumentAddenda() {
		return $this->m_objOldDocumentAddenda;
	}

	public function getIsApplyToGuarantor() {
		return $this->m_intIsApplyToGuarantor;
	}

	public function getIsApplyToApplicant() {
		return $this->m_intIsApplyToApplicant;
	}

	public function getIsDynamic() {
		return $this->m_intIsDynamic;
	}

	public function getIsDefault() {
		return $this->m_intIsDefault;
	}

	public function getLimitCountersign() {
		return $this->m_boolLimitCountersign;
	}

	public function getExternalFormFields() {
		return $this->m_arrstrExternalFormFields;
	}

	/**
	 * Set Functions
	 */

	public function setFileType( $strFileType ) {
		$this->m_strFileType = $strFileType;
	}

	public function setTempFileName( $strTempFileName ) {
		$this->m_strTempFileName = $strTempFileName;
	}

	public function setFileError( $strFileError ) {
		$this->m_strFileError = $strFileError;
	}

	public function setIsApplyToGuarantor( $intIsApplyToGuarantor ) {
		$this->m_intIsApplyToGuarantor = $intIsApplyToGuarantor;
	}

	public function setIsApplyToApplicant( $intIsApplyToApplicant ) {
		$this->m_intIsApplyToApplicant = $intIsApplyToApplicant;
	}

	public function setIsDynamic( $intIsDynamic ) {
		$this->m_intIsDynamic = $intIsDynamic;
	}

	public function setIsDefault( $intIsDefault ) {
		$this->m_intIsDefault = $intIsDefault;
	}

	public function setFileData( $arrstrFileData ) {

		if( true == isset( $arrstrFileData['tmp_name'] ) ) $this->setTempFileName( $arrstrFileData['tmp_name'] );
		if( true == isset( $arrstrFileData['error'] ) ) $this->setFileError( $arrstrFileData['error'] );
		if( true == isset( $arrstrFileData['name'] ) ) $this->setFileName( $arrstrFileData['name'] );
		if( true == isset( $arrstrFileData['type'] ) ) $this->setFileType( $arrstrFileData['type'] );

		$this->setFilePath( $this->buildDocumentAddendaFilePath() );
	}

	public function setOldDocumentAddenda( $objOldDocumentAddenda ) {
		$this->m_objOldDocumentAddenda = $objOldDocumentAddenda;
	}

	public function getAppliedToLable() {
		if( 1 == $this->getIsForCoSigner() && ( 1 == $this->getIsForPrimaryApplicant() || 1 == $this->getIsForCoApplicant() ) ) {
			return 'Applicants and Guarantor';
		} else if( 1 == $this->getIsForCoSigner() ) {
			return 'Guarantor Only';
		} else if( 1 == $this->getIsForPrimaryApplicant() || 1 == $this->getIsForCoApplicant() ) {
			return 'Applicants Only';
		}
	}

	public function setLimitCountersign( $boolLimitCountersign ) {
		$this->m_boolLimitCountersign = $boolLimitCountersign;
	}

	public function setExternalFormFields( $arrstrExternalFormFields ) {
		$this->m_arrstrExternalFormFields = $arrstrExternalFormFields;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchDocumentAddendaById( $intDocumentAddendaId, $objDatabase ) {

		return CDocumentAddendas::fetchDocumentAddendaByDocumentAddendaIdByIdByCid( $this->getId(), $intDocumentAddendaId, $this->getCid(), $objDatabase );
	}

	public function fetchDocumentPreferences( $objDatabase ) {

		return CDocumentPreferences::fetchDocumentPreferenceByDocumentIdByCid( $this->getDocumentId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Validate Functions
	 */

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDocumentId() {
		$boolIsValid = true;

		if( true == is_null( $this->getDocumentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_id', 'Main document is required.' ) );
		}

		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;

		if( true == is_null( $this->getFileName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'Please select a document to upload.' ) );
		}

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		if( true == is_null( $this->getOrderNum() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', 'Order num is required.' ) );
			return false;
		}

		if( false == is_int( $this->getOrderNum() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', 'Order num should be an integer.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyApplicationId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyApplicationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Company application id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valFileError() {

		$boolIsValid = true;

		if( UPLOAD_ERR_NO_FILE != $this->getFileError() && false == in_array( $this->getFileType(), $this->m_arrstrAllowedFileTypes ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unrecognized file type. Please choose .doc files only.' ) );
			return $boolIsValid;
		}

		if( UPLOAD_ERR_INI_SIZE == $this->getFileError() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to upload properly. Files cannot be larger than ' . PHP_MAX_POST . 'B.' ) );
			return $boolIsValid;
		} elseif( UPLOAD_ERR_NO_TMP_DIR == $this->getFileError() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' No file was uploaded , Missing a temporary folder.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;
		$objDatabase = NULL;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'insert_update_policy_document':
				$boolIsValid &= $this->valCompanyApplicationId();
				break;

			case 'upload_document':
				break;

			case 'insert_document_addenda_type':
				break;

			case 'insert_master_guarantor_document':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentId();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * other Functions
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = ' SELECT document_sub_type_id,name FROM documents WHERE cid = ' . ( int ) $this->getCid() . ' AND id = ' . ( int ) $this->getDocumentId();
		$arrintDocumentData = fetchData( $strSql, $objDatabase );
		$intCounter = 0;

		if( true == valArr( $arrintDocumentData ) && true == isset( $arrintDocumentData[0]['document_sub_type_id'] ) && CDocumentSubType::RESIDENT_VERIFY_SCREENING_POLICY_DOCUMENT == ( int ) $arrintDocumentData[0]['document_sub_type_id'] && 'Screening Authorization and Rights' == $arrintDocumentData[0]['name'] ) {
			$strSql = ' SELECT COUNT( da.id ) AS counter
					FROM document_addendas da
						JOIN documents d ON ( d.cid = da.cid AND da.document_id = d.id )
					WHERE da.cid = ' . ( int ) $this->getCid() . '
						AND da.company_application_id = ' . ( int ) $this->getCompanyApplicationId() . '
						AND da.is_published = 1
						AND d.document_sub_type_id = ' . CDocumentSubType::RESIDENT_VERIFY_SCREENING_POLICY_DOCUMENT . '
						AND da.deleted_on IS NULL
						AND da.archived_on IS NULL
						AND d.name = \'Screening Authorization and Rights\'';

			$arrintData = fetchData( $strSql, $objDatabase );
			$intCounter = ( true == valArr( $arrintData ) && true == isset ( $arrintData[0]['counter'] ) && 0 < $arrintData[0]['counter'] ) ? $arrintData[0]['counter'] : 0;
		}

		if( 0 == $intCounter ) {
			return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			trigger_error( 'Trying to add similar documents. Company Application Id: ' . ( int ) $this->getCompanyApplicationId() . ' Document Id: ' . ( int ) $this->getDocumentId() . ' # Cid : ' . ( int ) $this->getCid(), E_USER_WARNING );
		}

	}

	public function delete( $intCurrentUserId, $objClientDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		return $this->update( $intCurrentUserId, $objClientDatabase, $boolReturnSqlOnly );
	}

	public function buildFileData( $objDatabase ) {

		if( true == is_null( $this->getId() ) || false == is_numeric( $this->getId() ) ) {
			$this->setId( $this->fetchNextId( $objDatabase ) );
		}

		$arrstrFileInfo = pathinfo( $_FILES['document_addenda_file']['name'] );
		$strExtension 	= ( true == isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;
		$strFileName 	= date( 'Y' ) . date( 'm' ) . date( 'd' ) . '_' . $this->getCid() . '_' . $this->getDocumentId() . '_' . $this->getId() . '.' . $strExtension;

		// Getting the correct mime-type
		$strMimeType 	= '';
		$objFileExtension 	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		$arrstrFileData = [
			'tmp_name' => $_FILES['document_addenda_file']['tmp_name'],
			'error'    => $_FILES['document_addenda_file']['error'],
			'name'     => $strFileName,
			'type'     => $strMimeType
		];

		$this->setFileData( $arrstrFileData );
	}

	public function buildDocumentAddendaFilePath() {
		return 'leases/' . $this->getDocumentId() . '/document_addendas/';
	}

	public function uploadFile() {

		$boolIsValid = true;

		$strUploadPath = getMountsPath( $this->getCid(), PATH_MOUNTS_DOCUMENTS ) . $this->getFilePath();

		if( false == is_dir( $strUploadPath ) ) {
			if( false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Failed to create directory path.' ) );
				$boolIsValid = false;
			}
		}

		if( false == move_uploaded_file( $this->getTempFileName(), $strUploadPath . $this->getFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' No file was uploaded.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function downloadFile( $objClientDatabase ) {

		$strFullpath = getMountsPath( $this->getCid(), PATH_MOUNTS_DOCUMENTS ) . $this->getFilePath() . $this->getFileName();

		if( false == file_exists( $strFullpath ) ) {
			trigger_error( 'File is not present or has been deleted.', E_USER_ERROR );
			exit;
		}

		$arrstrFileParts = pathinfo( $this->getFileName() );

		if( true == valArr( $arrstrFileParts ) && false == is_null( $arrstrFileParts['extension'] ) ) {
			$objFileExtension = \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileParts['extension'], $objClientDatabase );
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		header( 'Pragma: public' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control: public' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-Length: ' . filesize( $strFullpath ) );
		header( 'Content-type: ' . $strMimeType );
		header( 'Content-Disposition: attachment; filename= ' . $this->getFileName() );
		header( 'Content-Transfer-Encoding:binary' );

		echo file_get_contents( $strFullpath );
		exit();
	}

	public function copyFile() {
		if( false == valObj( $this->m_objOldDocumentAddenda, 'CDocumentAddenda' ) ) {
			return false;
		}

		$strSource 		= getMountsPath( $this->getCid(), PATH_MOUNTS_DOCUMENTS ) . $this->m_objOldDocumentAddenda->getFilePath() . $this->m_objOldDocumentAddenda->getFileName();
		$strDestination = getMountsPath( $this->getCid(), PATH_MOUNTS_DOCUMENTS ) . $this->getFilePath() . $this->getFileName();

		if( !CFileIo::copyFile( $strSource, $strDestination ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Failed to copy file.' ) );
			return false;
		}
		return true;
	}

	public function mapDocumentData( $objDocument ) {
		$this->setRequireConfirmation( $objDocument->getRequireConfirmation() );
		$this->setIsForPrimaryApplicant( $objDocument->getIsForPrimaryApplicant() );
		$this->setIsForCoApplicant( $objDocument->getIsForCoApplicant() );
		$this->setIsForCoSigner( $objDocument->getIsForCoSigner() );
		$this->setAttachToEmail( $objDocument->getAttachToEmail() );
		$this->setCompanyApplicationId( $objDocument->getCompanyApplicationId() );
	}

}
?>