<?php

class CLeaseTermType extends CBaseLeaseTermType {

	const CONVENTIONAL = 1;
	const DATE_BASED    = 2;
	const FLEXIBLE     = 3;

	public static $c_arrintConventionalLeaseTermOccupancyTypes = [
		COccupancyType::CONVENTIONAL,
		COccupancyType::COMMERCIAL,
		COccupancyType::AFFORDABLE,
		COccupancyType::MILITARY
	];

	public static $c_arrintFlexibleLeaseTermOccupancyTypes = [
		COccupancyType::HOSPITALITY
	];

	public static $c_arrintStudentLeaseTermOccupancyTypes = [
		COccupancyType::STUDENT
	];

}
?>