<?php

class CCalendarEvent extends CBaseCalendarEvent {

	protected $m_boolIsLeasingCenter;

	protected $m_intAttendeeId;
	protected $m_intCalendarId;
	protected $m_intPropertyId;
	protected $m_intTourUnitCounts;
	protected $m_intDefaultCalendarEventType;

	protected $m_strEventType;
	protected $m_strForegroundColor;
	protected $m_strBackgroundColor;
	protected $m_strAssociatedPerson;


	protected $m_strUnitNumber;
	protected $m_strEventNotes;
	protected $m_objApplication;
	protected $m_strPhoneNumber;
	protected $m_strAmenityName;
	protected $m_strEmailAddress;
	protected $m_strRecurrenceStartDatetime;

	protected $m_objSystemDateTimeZone;

	// This data is stored in event_details
	protected $m_boolIsAllDay;
	protected $m_intLeaseCustomerId;
	protected $m_intCustomerId;

	protected $m_strEventStartTime;
	protected $m_strEventEndTime;

	const THIS_WEEK_EDIT_RECURRING       = 1;
	const FOLLOWING_WEEKS_EDIT_RECURRING = 2;

	public function __construct() {
		parent::__construct();

		$this->m_objSystemDateTimeZone = new DateTimeZone( 'America/Denver' );

		return;
	}

	public function getRecurrenceStartDatetime() {
		return $this->m_strRecurrenceStartDatetime;
	}

	public function getCalendarId() {
		return $this->m_intCalendarId;
	}

	public function getAssociatedPerson() {
		return $this->m_strAssociatedPerson;
	}

	public function getEventTypeLabel() {
		return $this->m_strEventType;
	}

	public function getAttendeeId() {
		return $this->m_intAttendeeId;
	}

	public function getIsAllDay() {
		return $this->m_boolIsAllDay;
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->m_intLeaseCustomerId = $intLeaseCustomerId;
	}
	public function setEventStartTime( $strEventStartTime ) {
		$this->m_strEventStartTime = $strEventStartTime;
	}
	public function setEventEndTime( $strEventEndTime ) {
		$this->m_strEventEndTime = $strEventEndTime;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		// We need to handle filters before the parent method, so that escape sequences are not removed from the JSON
		if( isset( $arrmixValues['event_details'] ) ) {
			if( true == $boolDirectSet ) {
				$this->m_strEventDetails = trim( $arrmixValues['event_details'] );
			} else {
				$this->setEventDetails( $arrmixValues['event_details'] );
			}
			unset( $arrmixValues['event_details'] );
		}

		if( isset( $arrmixValues['schedule_details'] ) ) {
			if( true == $boolDirectSet ) {
				$this->m_strScheduleDetails = trim( $arrmixValues['schedule_details'] );
			} else {
				$this->setScheduleDetails( $arrmixValues['schedule_details'] );
			}
			unset( $arrmixValues['schedule_details'] );
		}

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['calendar_id'] ) ) {
			$this->setCalendarId( $arrmixValues['calendar_id'] );
		}
		if( true == isset( $arrmixValues['associated_person'] ) ) {
			$this->setAssociatedPerson( $arrmixValues['associated_person'] );
		}
		if( true == isset( $arrmixValues['event_type'] ) ) {
			$this->setEventTypeLabel( $arrmixValues['event_type'] );
		}
		if( true == isset( $arrmixValues['attendee_id'] ) ) {
			$this->setAttendeeId( $arrmixValues['attendee_id'] );
		}
		if( true == isset( $arrmixValues['foreground_color'] ) ) {
			$this->setForegroundColor( $arrmixValues['foreground_color'] );
		}
		if( true == isset( $arrmixValues['background_color'] ) ) {
			$this->setBackgroundColor( $arrmixValues['background_color'] );
		}
		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
		if( true == isset( $arrmixValues['default_calendar_event_type_id'] ) ) {
			$this->setDefaultCalendarEventTypeId( $arrmixValues['default_calendar_event_type_id'] );
		}
		if( true == isset( $arrmixValues['phone_number'] ) ) {
			$this->setPhoneNumber( $arrmixValues['phone_number'] );
		}
		if( true == isset( $arrmixValues['email_address'] ) ) {
			$this->setEmailAddress( $arrmixValues['email_address'] );
		}
		if( true == isset( $arrmixValues['event_notes'] ) ) {
			$this->setEventNotes( $arrmixValues['event_notes'] );
		}
		if( true == isset( $arrmixValues['amenity_name'] ) ) {
			$this->setAmenityName( $arrmixValues['amenity_name'] );
		}
		if( true == isset( $arrmixValues['unit_number'] ) ) {
			$this->setUnitNumber( $arrmixValues['unit_number'] );
		}

		if( true == isset( $arrmixValues['tour_unit_counts'] ) ) {
			$this->setTourUnitCounts( $arrmixValues['tour_unit_counts'] );
		}
		if( true == isset( $arrmixValues['customer_id'] ) ) {
			$this->setCustomerId( $arrmixValues['customer_id'] );
		}

		if( true == isset( $arrmixValues['event_start_time'] ) ) {
			$this->setEventStartTime( $arrmixValues['event_start_time'] );
		}
		if( true == isset( $arrmixValues['event_end_time'] ) ) {
			$this->setEventEndTime( $arrmixValues['event_end_time'] );
		}
		$arrmixEventDetails = json_decode( $this->getEventDetails(), true );
		if( true == isset( $arrmixEventDetails['event_all_day'] ) ) {
			$this->setIsAllDay( $arrmixEventDetails['event_all_day'] ? true : false );
		}
	}

	public function setCalendarId( $intCalendarId ) {
		$this->m_intCalendarId = $intCalendarId;
	}

	public function setAssociatedPerson( $strAssociatedPerson ) {
		$this->m_strAssociatedPerson = $strAssociatedPerson;
	}

	public function setEventTypeLabel( $strEventType ) {
		$this->m_strEventType = $strEventType;
	}

	public function setAttendeeId( $intAttendeeId ) {
		$this->m_intAttendeeId = $intAttendeeId;
	}

	public function setForegroundColor( $strForegroundColor ) {
		$this->m_strForegroundColor = $strForegroundColor;
	}

	public function setBackgroundColor( $strBackgroundColor ) {
		$this->m_strBackgroundColor = $strBackgroundColor;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setDefaultCalendarEventTypeId( $intDefaultCalendarEventTypeId ) {
		$this->m_intDefaultCalendarEventType = $intDefaultCalendarEventTypeId;
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = $strPhoneNumber;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setEventNotes( $strEventNotes ) {
		$this->m_strEventNotes = $strEventNotes;
	}

	public function setAmenityName( $strAmenityName ) {
		$this->m_strAmenityName = $strAmenityName;
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = $strUnitNumber;
	}

	public function setIsAllDay( $boolIsAllDay ) {
		$this->m_boolIsAllDay = $boolIsAllDay;
	}

	public function getForegroundColor() {
		return $this->m_strForegroundColor;
	}

	public function getBackgroundColor() {
		return $this->m_strBackgroundColor;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getEventNotes() {
		return $this->m_strEventNotes;
	}

	public function getAmenityName() {
		return $this->m_strAmenityName;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getDefaultCalendarEventTypeId() {
		return $this->m_intDefaultCalendarEventType;
	}

	public function getEventStartTime() {
		return $this->m_strEventStartTime;
	}
	public function getEventEndTime() {
		return $this->m_strEventEndTime;
	}
	/**
	 * @return mixed
	 */
	public function getTourUnitCounts() {
		return $this->m_intTourUnitCounts;
	}

	/**
	 * @param mixed $intTourUnitCounts
	 */

	public function setTourUnitCounts( $intTourUnitCounts ) {
		$this->m_intTourUnitCounts = $intTourUnitCounts;
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = $intCustomerId;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function setIsLeasingCenter( $boolIsLeasingCenter ) {
		$this->m_boolIsLeasingCenter = $boolIsLeasingCenter;
	}

	public function setRecurrenceStartDatetime( $strRecurrenceStartDatetime ) {
		$this->m_strRecurrenceStartDatetime = $strRecurrenceStartDatetime;
	}

	public function setCalendarEventDatetimes( $arrmixCalendarEventData, $strEventStartDate, $strEventStartTime, $strEventEndTime, $strTimezoneOffset, $strRecurringEndDate = '', $strEventEndDate = NULL ) {
		// Validate Start & End Dates
		$objSystemDateTimeZone = new DateTimeZone( 'America/Denver' );

		$objGreenwichTimeZone                = new DateTimeZone( 'Greenwich' );
		$objGreenwichDateTime                = new DateTime( 'now', $objGreenwichTimeZone );
		$intTimezoneOffSet                   = $objSystemDateTimeZone->getOffset( $objGreenwichDateTime ) / 60;
		$objGreenwichDateTimeNoRecurringDate = new DateTime( '12/31/2099', $objGreenwichTimeZone );
		$intTimezoneOffSetNoRecurringDate    = $objSystemDateTimeZone->getOffset( $objGreenwichDateTimeNoRecurringDate ) / 60;
		$objGreenwichDateTimeRecurring       = new DateTime( $strRecurringEndDate, $objGreenwichTimeZone );
		$intTimezoneOffSetRecurring          = $objSystemDateTimeZone->getOffset( $objGreenwichDateTimeRecurring ) / 60;
		$strStartDate                        = $strEventStartDate;

		if( false == valStr( $strEventStartTime ) ) {
			$strEventStartTime = date( 'H:i:s', time() );
		}
		$strStartTimestamp                   = strtotime( $strStartDate . ' ' . $strEventStartTime . ' ' . $strTimezoneOffset );
		$strEndDate                          = ( CCalendarEventCategory::GENERAL == $arrmixCalendarEventData['calendar_event_category_id'] ? $strEventEndDate : $strEventStartDate );

		$boolIsInspectionEvent = false;
		if( CCalendarEventCategory::GENERAL == $arrmixCalendarEventData['calendar_event_category_id'] ) {
			if( 1 == $arrmixCalendarEventData['event_details']['event_all_day'] ) {
				$objAllDayDateTime = new DateTime( 'now', $this->m_objSystemDateTimeZone );
				$strEndDate        = $strStartDate;
				$strStartTimestamp = strtotime( $strStartDate . ' 00:00:00 ' . $strTimezoneOffset );
				$strEndTimestamp   = strtotime( $strEndDate . ' 23:59:59 ' . $strTimezoneOffset );

			} else {
				if( 1 == $arrmixCalendarEventData['event_details']['event_recurring'] && true == valStr( $strRecurringEndDate ) ) {
					$strEndTimestamp = strtotime( $strRecurringEndDate . ' ' . $strEventEndTime . ' ' . $strTimezoneOffset );
				} else {
					if( 1 == $arrmixCalendarEventData['event_details']['event_recurring'] ) {
						$strEndTimestamp = strtotime( '12/31/2037' . ' ' . $strEventEndTime . ' ' . $strTimezoneOffset );
					} else {
						$strEndTimestamp = strtotime( $strEventEndDate . ' ' . $strEventEndTime . ' ' . $strTimezoneOffset );
					}
				}
			}
		} elseif( CCalendarEventCategory::MAINTENANCE == $arrmixCalendarEventData['calendar_event_category_id'] && CDefaultCalendarEventType::INSPECTION == $arrmixCalendarEventData['default_calendar_event_type_id'] ) {
			if( true == valStr( $strEventEndTime ) ) {
				$strEndTimestamp = strtotime( $strStartDate . ' ' . $strEventEndTime . ' ' . $strTimezoneOffset );
			}
			$boolIsInspectionEvent = true;
		} else {
			$strEndTimestamp = strtotime( $strStartDate . ' ' . $strEventEndTime . ' ' . $strTimezoneOffset );
		}

		// Validation

		if( false == valStr( $strStartDate ) ) {
			return [ 'field' => 'add-event-end-datetime', 'message' => __( 'The event start date is required' ) ];
		}

		if( false == $boolIsInspectionEvent && ( false == valStr( $strEventStartTime ) || false == valStr( $strEventEndTime ) ) ) {
			return [ 'field' => 'add-event-end-datetime', 'message' => __( 'The event start and end time is required.' ) ];
		}

		if( CCalendarEventCategory::MAINTENANCE == $arrmixCalendarEventData['calendar_event_category_id'] && ( 0 > $strStartTimestamp || false == valId( $strStartTimestamp ) ) ) {
			return [ 'field' => 'add-event-end-datetime', 'message' => __( 'The event date is invalid.' ) ];
		}

		if( true == $boolIsInspectionEvent ) {
			if( true == valStr( $strEventStartTime ) && true == valStr( $strEventEndTime ) && $strEndTimestamp <= $strStartTimestamp ) {
				return [ 'field' => 'add-event-end-datetime', 'message' => __( 'The event start time must be earlier than the end time.' ) ];
			}

			if( false == is_null( $strStartTimestamp ) ) {
				$this->setStartDatetime( date( 'c', $strStartTimestamp ) );
			} else {
				$this->setStartDatetime( NULL );
			}

			if( false == is_null( $strEndTimestamp ) ) {
				$this->setEndDatetime( date( 'c', $strEndTimestamp ) );
			} else {
				$this->setEndDatetime( NULL );
			}

			return NULL;
		} elseif( $strEndTimestamp <= $strStartTimestamp ) {
			return [ 'field' => 'add-event-end-datetime', 'message' => __( 'The event start time must be earlier than the end time.' ) ];
		}

		$this->setStartDatetime( date( 'c', $strStartTimestamp ) );
		if( true == is_null( $this->getStartDatetime() ) ) {
			return [ 'field' => '', 'message' => __( 'Failed to insert event. Date issue.' ) ];
		}
		$this->setEndDatetime( date( 'c', $strEndTimestamp ) );

		return NULL;
	}

	public function prepareInsertUpdateScheduleAppointmentEvent( $strStartDate, $strStartTime, $strEndTime, $strDescription, $intPropertyId, $objCompanyUser, $objClient, $objDatabase, $arrmixFloorplanDetails = NULL, $boolIsUpdateGuestCard = false, $strEventDatetime = NULL, $intEventResultId = NULL, $arrintUnitSpaceIds = NULL ) {

		if( false == valObj( $objClient, 'CClient' ) || false == valObj( $objDatabase, 'CDatabase' ) ) {
			return [];
		}

		$boolIsRescheduled		= false;
		$boolIsUnitAssigned		= false;

		$objOldAppointmentEvent	= NULL;

		$objEvent              = \Psi\Eos\Entrata\CEvents::createService()->fetchEventByIdByCid( $this->getEventId(), $objClient->getId(), $objDatabase );
		$objEventStartDateTime = new DateTime( $strStartDate . ' ' . $strStartTime );
		$objEventEndDateTime   = new DateTime( $strStartDate . ' ' . $strEndTime );
		$this->setApplication( $objDatabase );

		$objEventLibrary           = new CEventLibrary();
		$objEventLibraryDataObject = new CEventLibraryDataObject();

		$arrmixData = [
			'is_calendar_event' => true,
			'event_title'		=> $this->getTitle(),
			'user_name'			=> $objCompanyUser->getUsername( true ),
			'is_leasing_center' => $this->getIsLeasingCenter()
		];

		$objCompanyEmployee = $objClient->fetchCompanyEmployeeByCompanyUserId( $objCompanyUser->getId(), $objDatabase );

		if( true == valObj( $objEvent, 'CEvent' )
			&& CEventType::SELF_GUIDED_TOUR == $objEvent->getEventTypeId()
			&& ( ( strtotime( $objEvent->getScheduledDatetime() ) != strtotime( $this->getStartDatetime() ) || strtotime( $objEvent->getScheduledEndDatetime() ) != strtotime( $this->getEndDatetime() ) ) ) ) {
			$objOldAppointmentEvent = clone $objEvent;
			$objRescheduledEventResult = \Psi\Eos\Entrata\CEventResults::createService()->fetchEventResultByDefaultEventResultIdByCid( CDefaultEventResult::RESCHEDULED, $objClient->getId(), $objDatabase );
			$objOldAppointmentEvent->setEventResultId( $objRescheduledEventResult->getId() );
			$objOldAppointmentEvent->setDefaultEventResultId( $objRescheduledEventResult->getDefaultEventResultId() );
			$objEvent = NULL;
			$boolIsRescheduled = true;
			$boolIsEventUpdate = false;
		} else {
			$boolIsEventUpdate = true;
		}

		if( true == valObj( $objEvent, 'CEvent' ) ) {

			if( $objEvent->getEventTypeId() != CEventType::SCHEDULED_APPOINTMENT
				&& false == in_array( $objEvent->getEventTypeId(), CEventType::$c_arrintOnsiteVisitAndTourEventTypeIds ) ) {
				return [];
			}

			$arrmixData['is_event_updated'] = true;

			$objEvent->setNotes( $strDescription );
			$objEvent->setScheduledDatetime( $this->getStartDatetime() );
			$objEvent->setScheduledEndDatetime( $this->getEndDatetime() );

			$objSystemEmail    = NULL;
			$objEvent->getOrFetchUnitSpaceIds( $objDatabase );
			$arrintOldUnitSpaceIds = $objEvent->getUnitSpaceIds() ?? [];
			if( ( false == is_null( $objEvent->getUnitSpaceId() ) || true == valStr( $objEvent->getUnitSpaceId() ) ) && false == valArr( $arrintUnitSpaceIds ) ) {
				$boolIsUnitAssigned = true;
			} elseif( true == valArr( $arrintUnitSpaceIds ) && $arrintOldUnitSpaceIds != $arrintUnitSpaceIds ) {
				$boolIsUnitAssigned = true;
			} else {
				$boolIsUnitAssigned = false;
			}
		} else {
			if( true == valArr( $arrintUnitSpaceIds ) ) $boolIsUnitAssigned = true;
			if( $this->getDefaultCalendarEventTypeId() == CDefaultCalendarEventType::SELF_GUIDED_TOUR ) {
				if( true == $boolIsRescheduled ) {
					$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::SELF_GUIDED_TOUR, CEventSubType::RESCHEDULED );
					$objOldAppointmentEvent->getOrFetchUnitSpaceIds( $objDatabase );
					if( true == valArr( $objOldAppointmentEvent->getUnitSpaceIds() ) ) {
						$objEvent->setUnitSpaceId( $objOldAppointmentEvent->getUnitSpaceId() );
						$objEvent->setUnitSpaceIds( $objOldAppointmentEvent->getUnitSpaceIds() );
					}
					$objEvent->setAssociatedEventId( $objOldAppointmentEvent->getId() );

					if( ( false == is_null( $objEvent->getUnitSpaceId() ) || true == valStr( $objEvent->getUnitSpaceId() ) ) && false == valArr( $arrintUnitSpaceIds ) ) {
						$boolIsUnitAssigned = true;
					} elseif( true == valArr( $arrintUnitSpaceIds ) && $objOldAppointmentEvent->getUnitSpaceIds() != $arrintUnitSpaceIds ) {
						$boolIsUnitAssigned = true;
					} else {
						$boolIsUnitAssigned = false;
					}

				} else {
					$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::SELF_GUIDED_TOUR, CEventSubType::APPOINTMENT );
				}
			} else {
				$intEventSubTypeId = CDefaultCalendarEventType::$c_arrintLeasingAppointmentEventSubTypesIds[$this->getDefaultCalendarEventTypeId()] ?? CEventSubType::LEASING_APPOINTMENTS;
				$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::SCHEDULED_APPOINTMENT, $intEventSubTypeId );
			}

			$arrmixData['is_event_updated'] = false;

			$objEvent->setNotes( $strDescription );
			$objEvent->setScheduledDatetime( $this->getStartDatetime() );
			$objEvent->setScheduledEndDatetime( $this->getEndDatetime() );

			if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) && true == valObj( $this->getApplication(), 'CApplication' ) ) {

				$objCompanyEmployeeDepartment = \Psi\Eos\Entrata\CCompanyEmployeeDepartments::createService()->fetchCompanyEmployeeDepartmentByCidByCompanyEmployeeIdByDefaultCompanyDepartmentId( $objClient->getId(), $objCompanyEmployee->getId(), CCompanyDepartment::DEFAULT_COMPANY_DEPARTMENT_LEASING_ID, $objDatabase );
				$objEvent->setCompanyEmployeeId( $objCompanyEmployee->getId() );
				$objEvent->setCompanyEmployee( $objCompanyEmployee );

				if( NULL == $this->getApplication()->getLeasingAgentId() && true == valObj( $objCompanyEmployeeDepartment, 'CCompanyDepartment' ) && 0 == $objCompanyUser->getIsAdministrator() ) {
					$this->getApplication()->setLeasingAgentId( $objCompanyEmployee->getId() );
				}
			}
			// If we choose to link out to the calendar event from the activity log.

			$objSystemEmail = NULL;
			if( true == $this->getIsLeasingCenter() ) {
				$objSystemEmail = $this->processEventNotificationEmail( $objEventStartDateTime, $objEventEndDateTime, $intPropertyId, $objClient, $objDatabase, $arrmixFloorplanDetails, $boolIsUpdateGuestCard );
			}

			$boolIsEventUpdate = false;
		}

		$arrmixSGTParameters = [];

		if( CEventType::SELF_GUIDED_TOUR == $objEvent->getEventTypeId() && true == $boolIsUnitAssigned ) {

			$arrmixSGTParameters['event_type_id'] = CEventType::SELF_GUIDED_TOUR;
			$arrmixSGTParameters['event_sub_type_id'] = CEventSubType::UNIT_ASSIGNED;
			$arrmixSGTParameters['event_datetime'] = 'NOW()';
			$arrmixSGTParameters['scheduled_datetime'] = $this->getStartDatetime();
			$arrmixSGTParameters['scheduled_end_datetime'] = $this->getEndDatetime();
			$arrmixSGTParameters['application'] = $this->getApplication();

			$objApplicant              = \Psi\Eos\Entrata\CApplicants::createService()->fetchApplicantByIdByApplicationIdByCid( $this->getApplication()->getPrimaryApplicantId(), $this->getApplication()->getId(), $objClient->getId(), $objDatabase );
			$arrmixSGTParameters['applicant'] = $objApplicant;

			if( true == valArr( $arrintUnitSpaceIds ) ) {
				$arrobjUnitSpaces = ( array ) \Psi\Eos\Entrata\CUnitSpaces::createService()->fetchUnitSpacesByIdsByCid( $arrintUnitSpaceIds, $objClient->getId(), $objDatabase );
				$objUnitSpace     = reset( $arrobjUnitSpaces );

				if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
					$objEvent->setUnitSpaceId( $objUnitSpace->getId() );
					$objEvent->setUnitSpaceIds( $arrintUnitSpaceIds );
					$arrmixSGTParameters['unit_space_id'] = $objUnitSpace->getId();
					$arrmixSGTParameters['unit_space_ids'] = $arrintUnitSpaceIds;
					$arrmixSGTParameters['unit_number'] = $objUnitSpace->getUnitNumber();
				}
			} else {
				$arrmixSGTParameters['unit_space_id'] = NULL;
				$arrmixSGTParameters['unit_space_ids'] = NULL;
				$arrmixSGTParameters['unit_number'] = NULL;

				$objEvent->setUnitSpaceId( NULL );
				$objEvent->setUnitSpaceIds( NULL );
			}

		}

		if( false != valId( $intEventResultId ) ) {
			$objEvent->setEventResultId( $intEventResultId );
		}
		if( false != valStr( $strEventDatetime ) ) {
			$objEvent->setEventDatetime( $strEventDatetime );
		}

		$objEventLibraryDataObject->setEvent( $objEvent );
		$objEventLibraryDataObject->setDatabase( $objDatabase );
		$objEventLibraryDataObject->setData( $arrmixData );
		$objEventLibrary->setEventLibraryDataObject( $objEventLibraryDataObject );
		$objEventLibrary->setIsExportEvent( true );
		$objEventLibrary->buildEventDescription( $this->getApplication() );

		$objOldAppointmentEventLibrary = NULL;

		if( true == valObj( $objOldAppointmentEvent, 'CEvent' ) ) {
			$objOldAppointmentEventLibrary           = new CEventLibrary();
			$objOldAppointmentEventLibraryDataObject = new CEventLibraryDataObject();
			$objOldAppointmentEventLibraryDataObject->setEvent( $objOldAppointmentEvent );
			$objOldAppointmentEventLibraryDataObject->setDatabase( $objDatabase );
			$objOldAppointmentEventLibrary->setEventLibraryDataObject( $objOldAppointmentEventLibraryDataObject );
			$objOldAppointmentEventLibrary->setIsExportEvent( true );
			$objOldAppointmentEventLibrary->buildEventDescription( $this->getApplication() );
		}

		return [ $objEventLibrary, $objSystemEmail, $boolIsEventUpdate, $arrmixSGTParameters, $boolIsRescheduled, $objOldAppointmentEventLibrary ];
	}

	public function setApplication( $objDatabase ) {
		if( true == valId( $this->getApplicationId() ) ) {
			$this->m_objApplication = CApplications::fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
		} else {
			$this->m_objApplication = CApplications::fetchApplicationByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );
		}

	}

	public function getIsLeasingCenter() {
		return $this->m_boolIsLeasingCenter;
	}

	public function getApplication() {
		return $this->m_objApplication;
	}

	public function processEventNotificationEmail( $objStartDateTime, $objEndDateTime, $intPropertyId, $objClient, $objDatabase, $arrmixFloorplanDetails = NULL, $boolIsUpdateGuestCard = false ) {
		$strToEmailAddress         = '';
		$objApplicant              = CApplicants::fetchApplicantByIdByApplicationIdByCid( $this->getApplication()->getPrimaryApplicantId(), $this->getApplication()->getId(), $objClient->getId(), $objDatabase );
		$strLeadSourceName         = \Psi\Eos\Entrata\CLeadSources::createService()->fetchLeadSourceNameByIdByCid( $this->getApplication()->getLeadSourceId(), $objClient->getId(), $objDatabase );
		$objProperty               = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $intPropertyId, $objClient->getId(), $objDatabase );
		$arrobjPropertyPreferences = $objProperty->getOrFetchPropertyPreferences( $objDatabase );

		if( true == array_key_exists( 'CALL_CENTER_DASHBOARD_EVENT_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) ) {
			$strToEmailAddress = $arrobjPropertyPreferences['CALL_CENTER_DASHBOARD_EVENT_NOTIFICATION_EMAIL']->getValue();
		}

		if( true == CValidation::validateEmailAddresses( $strToEmailAddress ) ) {
			$objSystemEmail = $objClient->createSystemEmail( CSystemEmailType::LEASING_CENTER_CONTACT );

			$strSubject     = 'Appointment Set for ' . $this->getApplication()->getPropertyName();
			$strHtmlContent = '';
			if( false == $boolIsUpdateGuestCard ) {
				$strHtmlContent .= 'Guest Card Submitted by Leasing Center: ' . $this->getApplication()->getPropertyName() . '<br />';
			} else {
				$strHtmlContent .= 'Guest Card Updated by Leasing Center: ' . $this->getApplication()->getPropertyName() . '<br />';
			}
			if( true == valStr( $this->getApplication()->getUpdatedOn() ) ) {
				$strHtmlContent .= 'Date Submitted: ' . date( 'd/m/Y H:i:s', strtotime( $this->getApplication()->getUpdatedOn() ) ) . '<br />';
			} else {
				$strHtmlContent .= 'Date Submitted: ' . date( 'd/m/Y H:i:s' ) . '<br />';
			}

			$strHtmlContent .= 'General Information' . '<br />';
			$strHtmlContent .= '-------------------------------------------------------------------------------------------' . '<br />';
			$strHtmlContent .= 'Lead Id: ' . $this->getApplication()->getId() . '<br />';
			$strHtmlContent .= 'Prospect Name: ' . $objApplicant->getNameFirst() . ', ' . $objApplicant->getNameLast() . '<br />';
			$strHtmlContent .= 'Property: ' . $this->getApplication()->getPropertyName() . '<br />';
			$strHtmlContent .= 'Prospect Primary Phone: ' . $objApplicant->getPrimaryPhoneNumber() . '<br />';
			$strHtmlContent .= 'Prospect Email: ' . $objApplicant->getUsername() . '<br />';

			if( true == valStr( $arrmixFloorplanDetails['move_in_date'] ) ) {
				$strHtmlContent .= 'Desired Move-In Date: ' . $arrmixFloorplanDetails['move_in_date'] . '<br />';
			} else {
				$strHtmlContent .= 'Desired Move-In Date: Not Specified' . '<br />';
			}

			if( true == valStr( $arrmixFloorplanDetails['lease_length'] ) ) {
				$strHtmlContent .= 'Desired Lease Term: ' . $arrmixFloorplanDetails['lease_length'] . '<br />';
			} else {
				$strHtmlContent .= 'Desired Lease Term: Not Specified' . '<br />';
			}

			if( true == valStr( $arrmixFloorplanDetails['rent_range'] ) ) {
				$strHtmlContent .= 'Desired Monthly Rent: ' . $arrmixFloorplanDetails['rent_range'] . '<br />';
			} else {
				$strHtmlContent .= 'Desired Monthly Rent: Not Specified' . '<br />';
			}

			$strHtmlContent .= 'Lead Source: ' . $strLeadSourceName . '<br />';
			$strHtmlContent .= 'Guest Card Questions or Comments: ' . '<br />';
			$strHtmlContent .= $objApplicant->getNotes() . '<br />';
			$strHtmlContent .= 'Start Date: ' . $objStartDateTime->format( 'm/d/y g:ia' ) . '<br />';
			$strHtmlContent .= 'End Date: ' . $objEndDateTime->format( 'm/d/y g:ia' ) . '<br />';

			$objSystemEmail->setPropertyId( $objProperty->getId() );
			$objSystemEmail->setSubject( $strSubject );
			$objSystemEmail->setToEmailAddress( $strToEmailAddress );
			$objSystemEmail->setHtmlContent( $strHtmlContent );

			return $objSystemEmail;
		}

		return NULL;
	}

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCalendarEventCategoryId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCalendarEventTypeId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valEventTypeId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valEventId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valStartDatetime() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valEndDatetime() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valScheduleDetails() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valEventDetails() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valScheduling() {
		$boolIsValid = true;
		$arrmixEventDetails = json_decode( $this->getEventDetails(), true );
		if( false == valId( $arrmixEventDetails['frequency_id'] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_id', __( 'Frequency and scheduling is required. ' ) ) );
		} else if( true == valId( $arrmixEventDetails['frequency_id'] ) && \CFrequency::DAILY == $arrmixEventDetails['frequency_id'] ) {
			if( \CFrequency::WEEKLY == $arrmixEventDetails['repeat_frequency_id'] ) {
				if( false == valArr( $arrmixEventDetails['days_of_week'] ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_of_week', __( 'Please select the scheduled day(s) of service.' ) ) );
				}
				if( false == valId( $arrmixEventDetails['occurrence_id'] ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occurrence_id', __( 'Occurrences in a week is required. ' ) ) );
				}
				if( true == valArr( $arrmixEventDetails['days_of_week'] ) && ( \Psi\Libraries\UtilFunctions\count( $arrmixEventDetails['days_of_week'] ) > $arrmixEventDetails['occurrence_id'] ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occurrence_id', __( 'Please select the {%d, 0} day(s) of the week this service will occur.', [ $arrmixEventDetails['occurrence_id'] ] ) ) );
				}
			}

			if( \CFrequency::WEEKDAY_OF_MONTH == $arrmixEventDetails['repeat_frequency_id'] ) {
				if( false == valArr( $arrmixEventDetails['days_of_week'] ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_of_week', __( 'Please select the scheduled day(s) of service.' ) ) );
				}
				if( true == valArr( $arrmixEventDetails['days_of_week'] ) && ( \Psi\Libraries\UtilFunctions\count( $arrmixEventDetails['days_of_week'] ) > 1 ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occurrence_id', __( 'Please select the {%d, 0} day(s) of the week this service will occur.', [ 1 ] ) ) );
				}
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_event_details':
				$boolIsValid &= $this->valScheduling();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getServiceDaysSelected() {

		if( false == valId( $this->getId() ) ) {
			return NULL;
		}

		$arrmixEventDetails = json_decode( $this->getEventDetails(), true );

		if( true == valArr( $arrmixEventDetails['days_of_week'] ) ) {
			$arrstrDaysOfWeek = [];
			sort( $arrmixEventDetails['days_of_week'] );
			foreach( $arrmixEventDetails['days_of_week'] as $intDayOfWeek ) {
				$arrstrDaysOfWeek[] = \CAddOn::createService()->getWeekDaysAbbr()[$intDayOfWeek];
			}

			if( true == valArr( $arrstrDaysOfWeek ) ) {
				return implode( ', ', $arrstrDaysOfWeek );
			}

		}
	}

}

?>