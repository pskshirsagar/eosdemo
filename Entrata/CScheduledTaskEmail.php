<?php

use Psi\Eos\Entrata\CSystemMessageTemplateCustomTexts;
use Psi\Eos\Entrata\CSystemMessageTemplateSlotImages;
use Psi\Eos\Entrata\CSystemMessageTemplateSlots;
use Psi\Eos\Entrata\CCompanyMediaFiles;

class CScheduledTaskEmail extends CBaseScheduledTaskEmail {

	protected $m_arrobjFiles;

	protected $m_arrstrSystemMessageTemplateCustomTexts = [
		'CONTENT_TOP_LEFT'		=> '',
		'CAPTION_MAIN'			=> '',
		'MAIN_BODY_CONTENT'		=> '',
		'SUB_CAPTION_LEFT'		=> '',
		'SUB_CAPTION_CENTER'	=> '',
		'SUB_CAPTION_RIGHT'		=> '',
		'CONTENT_MIDDLE_LEFT'	=> '',
		'CONTENT_MIDDLE_RIGHT'	=> '',
		'CONTENT_MIDDLE_MIDDLE'	=> '',
		'FOOTER'				=> '',
		'SUB_CAPTION'			=> '',
		'SUB_FOOTER'			=> ''
	];

	protected $m_arrstrSystemMessageTemplateSlotImages = [
		'header_main_image' => [ 'company_media_file_id' => NULL, 'fullsize_uri' => '', 'link' => NULL, 'link_target' => NULL ],
		'left_image' 		=> [ 'company_media_file_id' => NULL, 'fullsize_uri' => '', 'link' => NULL, 'link_target' => NULL ],
		'center_image' 		=> [ 'company_media_file_id' => NULL, 'fullsize_uri' => '', 'link' => NULL, 'link_target' => NULL ],
		'right_image' 		=> [ 'company_media_file_id' => NULL, 'fullsize_uri' => '', 'link' => NULL, 'link_target' => NULL ],
	];

	protected $m_arrstrEmailFileAttachments = [
		'file1' => [ 'id' => NULL, 'title' => '' ],
		'file2' => [ 'id' => NULL, 'title' => '' ],
		'file3' => [ 'id' => NULL, 'title' => '' ],
	];

	public function fetchFiles( $objDatabase ) {
		return CFiles::fetchFilesByScheduledEmailIdByFileTypeSystemCodeByCid( $this->getId(), CFileType::SYSTEM_CODE_MESSAGE_CENTER, $this->getCid(), $objDatabase );
	}

	public function getEmailContent( $objDatabase ) {
		$objScheduledTaskEmailContent = new CScheduledTaskEmailContentLibrary();
		$strHtmlContent = $objScheduledTaskEmailContent->getEmailContent( $this->getId(), $this->getCid(), $objDatabase );
		return $strHtmlContent;
	}

	public function setFiles( $arrobjFiles ) {
		$this->m_arrobjFiles = $arrobjFiles;
	}

	public function getFiles() {
		return $this->m_arrobjFiles;
	}

	public function getEmailContentByPropertyId( $objDatabase ) {
		$objScheduledTaskEmailContent = new CScheduledTaskEmailContentLibrary();
		$strHtmlContent = $objScheduledTaskEmailContent->getEmailContent( $this->getId(), $this->getCid(), $objDatabase );
		$this->setFiles( $objScheduledTaskEmailContent->getFiles() );
		return $strHtmlContent;
	}

	public function loadLeaseCustomerMergeFields( $arrobjLeaseCustomers, $intCid, $strHtmlContent, $intCompanyUserId, $objDatabase ) {
		$objMergeFieldLibrary = new CMergeFieldLibrary( $intCid, $intCompanyUserId, $objDatabase );
		$objMergeFieldLibrary->loadLeaseCustomersMergeFields( $arrobjLeaseCustomers, $strHtmlContent, true, CSystemEmailType::EVENT_SCHEDULER_EMAIL );
	}

	public function loadEmailSubjectMergeFields( $strHtmlSubject, $intCid, $objDatabase, $arrmixSubjectMergeFields, $objScheduledTaskEmail ) {
		$objMergeFieldLibrary = new CMergeFieldLibrary( $intCid, $intCompanyUserId, $objDatabase );
		$objMergeFieldLibrary->loadEmailSubjectMergeFields( $strHtmlSubject, $arrmixSubjectMergeFields, $objScheduledTaskEmail );
	}

	public function loadApplicationsMergeFields( $arrobjApplications, $intCid, $strHtmlContent, $intCompanyUserId, $objDatabase ) {
		$objMergeFieldLibrary = new CMergeFieldLibrary( $intCid, $intCompanyUserId, $objDatabase );
		$objMergeFieldLibrary->loadApplicationsMergeFields( $arrobjApplications, $strHtmlContent, CSystemEmailType::EVENT_SCHEDULER_EMAIL );
	}

	public function getRecipientMergeFields( $objRecipient ) {
		$objScheduledTaskEmailContent = new CScheduledTaskEmailContentLibrary();
		$arrmixMergeFields = $objScheduledTaskEmailContent->getRecipientMergeFields( $objRecipient );
		return $arrmixMergeFields;
	}

    public function getSystemMessageTemplateCustomTexts( $objClientDatabase ) {
    	$arrstrSystemMessageTemplateCustomTexts = ( array ) CSystemMessageTemplateCustomTexts::createService()->fetchSimpleSystemMessageTemplateCustomTextsByScheduledTaskEmailIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
    	return $arrstrSystemMessageTemplateCustomTexts;
    }

	public function getSystemMessageTemplateSlotImages( $objDatabase ) {
		if( false == valStr( $this->getId() ) ) return false;

		$arrstrSystemMessageTemplateSlotImages = [];

		$arrstrSystemMessageTemplateSlotName    = ( array ) CSystemMessageTemplateSlots::createService()->fetchSimpleSystemMessageTemplateSlots( $objDatabase );
		$arrmixSystemMessageTemplateSlotImages  = ( array ) CSystemMessageTemplateSlotImages::createService()->fetchSimpleSystemMessageTemplateSlotImagesByScheduledTaskEmailIdByCid( $this->getId(), $this->getCid(),  $objDatabase );

		foreach( $arrmixSystemMessageTemplateSlotImages as $arrmixSystemMessageTemplateSlotImage ) {
			if( true == array_key_exists( $arrmixSystemMessageTemplateSlotImage['system_message_template_slot_id'], $arrstrSystemMessageTemplateSlotName ) ) {
				$strKey = $arrstrSystemMessageTemplateSlotName[$arrmixSystemMessageTemplateSlotImage['system_message_template_slot_id']];
				$strFullUri = '';

				if( true == valStr( $arrmixSystemMessageTemplateSlotImage['fullsize_uri'] ) ) {
					$strFullUri = CConfig::get( 'media_library_path' ) . $arrmixSystemMessageTemplateSlotImage['fullsize_uri'];
				}

				$arrstrSystemMessageTemplateSlotImages[$strKey] = [
					'company_media_file_id' => $arrmixSystemMessageTemplateSlotImage['company_media_file_id'],
					'fullsize_uri'			=> $strFullUri,
					'link'					=> $arrmixSystemMessageTemplateSlotImage['link'],
					'link_target'			=> $arrmixSystemMessageTemplateSlotImage['link_target']
				];
			}
		}

		$arrstrSystemMessageTemplateSlotImages = array_merge( $this->m_arrstrSystemMessageTemplateSlotImages, $arrstrSystemMessageTemplateSlotImages );

		return $arrstrSystemMessageTemplateSlotImages;
	}

    public function getEmailAttachments( $objClientDatabase ) {
    	if( false == valStr( $this->getId() ) ) return false;
    	$arrmixFileAttachments = [];
    	$this->m_arrobjFiles = ( array ) CFiles::fetchFilesByScheduledTaskEmailIdByFileTypeSystemCodeByCid( $this->getId(), CFileType::SYSTEM_CODE_MESSAGE_CENTER, $this->getCid(), $objClientDatabase );

    	$intCount = 1;
    	foreach( $this->m_arrobjFiles as $objFile ) {
    		$strKey = 'file' . $intCount;
    		$arrmixFileAttachments[$strKey] = [ 'id' => $objFile->getId(), 'title' => $objFile->getTitle() ];
    		$intCount++;
    	}

    	$arrmixFileAttachments = array_merge( $this->m_arrstrEmailFileAttachments, $arrmixFileAttachments );
    	return $arrmixFileAttachments;
    }

    public function updateSystemMessageTemplateCustomText( $arrstrCustomText, $objCompanyUser, $objClientDatabase ) {
    	// fetch and delete old custom text
    	if( false == valArr( $arrstrCustomText ) ) return false;

    	$arrobjSystemMessageTemplateCustomText = ( array ) CSystemMessageTemplateCustomTexts::createService()->fetchSystemMessageTemplateCustomTextsByScheduledTaskEmailIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );

    	foreach( $arrobjSystemMessageTemplateCustomText as $objSystemMessageTemplateCustomText ) {
    		if( false == $objSystemMessageTemplateCustomText->delete( $objCompanyUser->getId(), $objClientDatabase ) ) {
    			$this->addErrorMsg( 'Error saving email templated custom text' );
    			return false;
    		}
    	}

    	foreach( $arrstrCustomText as $strKey => $strValue ) {
    		$objSystemMessageTemplateCustomText = new CSystemMessageTemplateCustomText();
    		$objSystemMessageTemplateCustomText->setScheduledTaskEmailId( $this->getId() );
    		$objSystemMessageTemplateCustomText->setSystemMessageTemplateId( $this->getSystemMessageTemplateId() );
            $objSystemMessageTemplateCustomText->setCid( $this->getCid() );
            $objSystemMessageTemplateCustomText->setKey( $strKey );
            $objSystemMessageTemplateCustomText->setValue( $strValue );
   			if( false == $objSystemMessageTemplateCustomText->insert( $objCompanyUser->getId(), $objClientDatabase ) ) {
   				$this->addErrorMsg( 'Error saving email templated custom text' );
   				return false;
   			}
   		}
   		return true;
    }

    public function updateSystemMessageTemplateSlotImages( $arrmixSlotImages, $objCompanyUser, $objClientDatabase ) {

    	$arrmixSystemMessageTemplateSlots       = ( array ) CSystemMessageTemplateSlots::createService()->fetchSimpleSystemMessageTemplateSlotsBySystemMessageTemplateId( $this->getSystemMessageTemplateId(), $objClientDatabase );
    	$arrobjSystemMessageTemplateSlotImages  = ( array ) CSystemMessageTemplateSlotImages::createService()->fetchSystemMessageTemplateSlotImagesByScheduledTaskEmailIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	    $arrintOldCompanyMediaFileIds           = [];
	    $arrintNewCompanyMediaFileIds           = [];
	    $strSavingContentError                  = __( 'Error while saving content' );

    	foreach( $arrobjSystemMessageTemplateSlotImages as $objSystemMessageTemplateSlotImage ) {
    		if( false == $objSystemMessageTemplateSlotImage->delete( $objCompanyUser->getId(), $objClientDatabase ) ) {
    			$this->addErrorMsg( $strSavingContentError );
    			return false;
    		}
		    $arrintOldCompanyMediaFileIds[] = $objSystemMessageTemplateSlotImage->getCompanyMediaFileId();
    	}

    	if( true == valArr( $arrmixSlotImages ) ) {
    		foreach( $arrmixSlotImages as $strImageKey => $arrmixSlotImage ) {
    			if( false == valArr( $arrmixSlotImage ) || false == valStr( $arrmixSlotImage['company_media_file_id'] ) ) continue;

    			$intSystemMessageTemplateSlotId = array_search( $strImageKey, $arrmixSystemMessageTemplateSlots );

    			if( false == is_numeric( $intSystemMessageTemplateSlotId ) ) continue;

    			$objCompanyMediaFile = CCompanyMediaFiles::createService()->fetchCompanyMediaFileByIdByCid( $arrmixSlotImage['company_media_file_id'], $this->getCid(), $objClientDatabase );
			    if( false == valObj( $objCompanyMediaFile, 'CCompanyMediaFile' ) ) {
				    continue;
			    }

    			$objSystemMessageTemplateSlotImage = new CSystemMessageTemplateSlotImage();
                $objSystemMessageTemplateSlotImage->setScheduledTaskEmailId( $this->getId() );
                $objSystemMessageTemplateSlotImage->setSystemMessageTemplateId( $this->getSystemMessageTemplateId() );
                $objSystemMessageTemplateSlotImage->setCid( $this->getCid() );

                $objSystemMessageTemplateSlotImage->setSystemMessageTemplateSlotId( $intSystemMessageTemplateSlotId );
                $objSystemMessageTemplateSlotImage->setCompanyMediaFileId( $arrmixSlotImage['company_media_file_id'] );
				$objSystemMessageTemplateSlotImage->setLink( $arrmixSlotImage['link'] );
				$objSystemMessageTemplateSlotImage->setLinkTarget( $arrmixSlotImage['link_target'] );

    			if( false == $objSystemMessageTemplateSlotImage->insert( $objCompanyUser->getId(), $objClientDatabase ) ) {
    				$this->addErrorMsg( $strSavingContentError );
    				return false;
    			}
			    $arrintNewCompanyMediaFileIds[] = $arrmixSlotImage['company_media_file_id'];
    		}
    	}

	    foreach( $arrintOldCompanyMediaFileIds as $intOldComapanyMediaFileId ) {
		    if( false == in_array( $intOldComapanyMediaFileId, $arrintNewCompanyMediaFileIds ) ) {
			    $objCompanyMediaFile = CCompanyMediaFiles::createService()->fetchCompanyMediaFileByIdByCid( $intOldComapanyMediaFileId, $this->getCid(), $objClientDatabase );
			    $arrobjSystemMessageTemplateSlotImages = ( array ) CSystemMessageTemplateSlotImages::createService()->fetchSystemMessageTemplateSlotImagesByCompanyMediaFileIdsByCid( [ $intOldComapanyMediaFileId ],  $this->getCid(), $objClientDatabase );

			    if( true == valArr( $arrobjSystemMessageTemplateSlotImages ) ) {
				    if( false == CSystemMessageTemplateSlotImages::createService()->bulkDelete( $arrobjSystemMessageTemplateSlotImages, $objClientDatabase ) ) {
					    $this->addErrorMsg( $strSavingContentError );
					    return false;
				    }
			    }

		        if( true == valObj( $objCompanyMediaFile, 'CCompanyMediaFile' ) ) {
				    if( false == $objCompanyMediaFile->delete( $objCompanyUser->getId(), $objClientDatabase ) ) {
					    $this->addErrorMsg( $strSavingContentError );
					    return false;
				    }
			    }
		    }
	    }

    	return true;
    }

    public function updateFileAttachments( $arrstrEmailFileAttachments, $objCompanyUser, $objClientDatabase, $objStorageGateway ) {

    	if( false == valArr( $arrstrEmailFileAttachments ) ) {
    		$this->addErrorMsg( 'Error while saving email attachments' );
    		return false;
    	}

    	foreach( $arrstrEmailFileAttachments as $arrmixFileAttachment ) {
    		if( true == is_numeric( $arrmixFileAttachment['id'] ) ) {

    			$objFile 			= CFiles::fetchFileByIdByCid( $arrmixFileAttachment['id'], $this->getCid(), $objClientDatabase );
    			$objFileAssociation = \Psi\Eos\Entrata\CFileAssociations::createService()->fetchFileAssociationByFileIdByScheduledTaskEmailIdByCid( $objFile->getId(), $this->getId(), $this->getCid(), $objClientDatabase );

    			if( false == valObj( $objFileAssociation, 'CFileAssociation' ) ) {
    				$objFileAssociation = new CFileAssociation();
    				$objFileAssociation->setCid( $this->getCid() );
    				$objFileAssociation->setScheduledTaskEmailId( $this->getId() );
    			}

    			$objFileAssociation->setFileId( $objFile->getId() );

    			if( false == $objFile->insertOrUpdate( $objCompanyUser->getId(), $objClientDatabase ) ) {
    				$objFile->deleteObject( $objStorageGateway );
    				$this->m_objDatabase->rollback();
    				$this->addErrorMsg( 'Error while saving email attachments' );
    				return false;
    			}

    			if( false == $objFileAssociation->insertOrUpdate( $objCompanyUser->getId(), $objClientDatabase ) ) {
    				$this->m_objDatabase->rollback();
    				$this->addErrorMsg( 'Error while saving email attachments' );
    				return false;
    			}
    		}
    	}

    	return true;
    }

	public function valCcEmailAddress() {
		$boolIsValid = true;

		switch( NULL ) {
			default:
				if( true == valStr( $this->getCcEmailAddress() ) ) {
					$arrstrEmailCcAddresses = explode( ',', $this->getCcEmailAddress() );

					if( 5 < \Psi\Libraries\UtilFunctions\count( $arrstrEmailCcAddresses ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_email_address', '5 or less cc email addresses are permitted.' ) );
						$boolIsValid = false;
						break 1;
					}

					if( true == valArr( $arrstrEmailCcAddresses ) ) {
						foreach( $arrstrEmailCcAddresses as $strCcEmailAddress ) {
							if( false == CValidation::validateEmailAddresses( trim( $strCcEmailAddress ) ) ) {
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_email_address', 'cc field has an incorrect email address format.' ) );
								$boolIsValid = false;
								break 1;
							}
						}
					}
				}
		}

    	return $boolIsValid;
    }

    public function valBccEmailAddress() {
    	$boolIsValid = true;

    	switch( NULL ) {
    		default:
    			if( true == valStr( $this->getBccEmailAddress() ) ) {
    				$arrstrEmailBccAddresses = explode( ',', $this->getBccEmailAddress() );

    				if( 5 < \Psi\Libraries\UtilFunctions\count( $arrstrEmailBccAddresses ) ) {
    					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bcc_email_address', '5 or less Bccc email addresses are permitted.' ) );
    					$boolIsValid = false;
    					break 1;
    				}

    				if( true == valArr( $arrstrEmailBccAddresses ) ) {
    					foreach( $arrstrEmailBccAddresses as $strBccEmailAddress ) {
    						if( false == CValidation::validateEmailAddresses( trim( $strBccEmailAddress ) ) ) {
    							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bcc_email_address', 'Bcc field has an incorrect email address format.' ) );
    							$boolIsValid = false;
    							break 1;
    						}
    					}
    				}
    			}
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction ) {
    	$boolIsValid = true;

    	switch( $strAction ) {
    		case VALIDATE_INSERT:
    		case VALIDATE_UPDATE:
    			$boolIsValid &= $this->valBccEmailAddress();
    			$boolIsValid &= $this->valCcEmailAddress();
    			break;

    		case VALIDATE_DELETE:
    			break;

    		default:
    			$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

}
?>