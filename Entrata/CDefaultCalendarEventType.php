<?php

class CDefaultCalendarEventType extends CBaseDefaultCalendarEventType {

	const GENERAL				= 1;
	const COMMUNITY				= 2;
	const UNAVAILABLE			= 3;
	const MOVE_IN				= 4;
	const MOVE_OUT				= 5;
	const TRANSFER				= 6;
	const RESIDENT_APPOINTMENT	= 7;
	const TOUR					= 8;
	const WORK_ORDER			= 9;
	const LEASING_APPOINTMENT	= 10;
	const INSPECTION			= 11;
	const AMENITY_RESERVATION	= 12;
	const MOVE_IN_SCHEDULER     = 13;
	const SELF_GUIDED_TOUR      = 14;
	const VIRTUAL_TOUR          = 15;
	const SERVICES_RESERVATION  = 16;

	protected $m_strCalendarEventCategoryName;

	public static $c_arrintSelfGuidedAndVirtualTourIds = [
		self::SELF_GUIDED_TOUR,
		self::VIRTUAL_TOUR
	];

	public static $c_arrintEventTypeTourIds = [
		self::SELF_GUIDED_TOUR 		=> CEventType::SELF_GUIDED_TOUR,
		self::VIRTUAL_TOUR			=> CEventType::VIRTUAL_TOUR,
		self::TOUR					=> CEventType::TOUR
	];

	public static $c_arrintLeasingAppointmentEventSubTypesIds = [
		self::VIRTUAL_TOUR			=> CEventSubType::VIRTUAL_TOUR,
		self::TOUR					=> CEventSubType::TOUR,
		self::LEASING_APPOINTMENT 	=> CEventSubType::LEASING_APPOINTMENTS
	];

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['calendar_event_category_name'] ) ) $this->setCalendarEventCategoryName( $arrmixValues['calendar_event_category_name'] );

	}

	public function getCalendarEventCategoryName() {
		return $this->m_strCalendarEventCategoryName;
	}

	public function setCalendarEventCategoryName( $strCalendarEventCategoryName ) {
		$this->m_strCalendarEventCategoryName = $strCalendarEventCategoryName;
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCalendarEventCategoryId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEventTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>