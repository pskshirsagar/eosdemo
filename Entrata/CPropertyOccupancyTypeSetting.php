<?php

class CPropertyOccupancyTypeSetting extends CBasePropertyOccupancyTypeSetting {

	protected $m_strOccupancyTypeName;
	protected $m_strArTriggerName;

	public function valId() {
		$boolIsValid = true;
		if( false == valId( $this->getId() ) ) $boolIsValid = false;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( false == valId( $this->getPropertyId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, __( 'Property Id is not valid.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valOccupancyTypeId() {
		$boolIsValid = true;
		if( false == valId( $this->getOccupancyTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, __( 'Occupancy type is not valid.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valArTriggerId() {
		$boolIsValid = true;
		if( false == valId( $this->getArTriggerId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, __( 'Please select valid advertised frequency.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valDisplayAdvertisedFrequency() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInvoicingStartDate() {
		$boolIsValid = true;

		if( true == $this->getEnableInvoicing() && false == valStr( $this->getInvoicingStartDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Invoice Transactions Posted On / After is required.' ) ) );
			$boolIsValid = false;
		} elseif( true == $this->getEnableInvoicing() && false == CValidation::validateISODate( $this->getInvoicingStartDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Invoice Transactions Posted On / After is not valid.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valInvoiceTemplate() {
		if( true == $this->getEnableInvoicing() && false == valId( $this->getInvoiceDocumentId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Invoice Template is required.' ) ) );
			return false;
		}

		return true;
	}

	public function valCreditNoteInvoiceDocumentId() {
		if( true == $this->getEnableInvoicing() && false == valId( $this->getCreditNoteInvoiceDocumentId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Credit Note Template is required.' ) ) );
			return false;
		}

		return true;
	}

    public function valCorrectiveInvoiceDocumentId() {
        if( true == $this->getEnableInvoicing() && false == valId( $this->getCorrectiveInvoiceDocumentId() ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Correction Invoice Template is required.' ) ) );
            return false;
        }

        return true;
    }

	public function valDownPaymentInvoiceDocumentId() {
		if( true == $this->getEnableInvoicing() && false == valId( $this->getDownPaymentInvoiceDocumentId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Down Payment Invoice Template is required.' ) ) );
			return false;
		}

		return true;
	}

	public function valDownPaymentInvoicePrefix() {
		if( true == $this->getEnableInvoicing() && false == valStr( $this->getDownPaymentInvoicePrefix() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Down Payment Invoice Prefix is required.' ) ) );
			return false;
		}

		return true;
	}

	public function valAutoInvoiceDay() {
		if( true == $this->getEnableInvoicing() && true == $this->getAutoGenerateInvoices() && false == valStr( $this->getAutoInvoiceDay() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Auto Generate Invoices On is required when Auto Generation is enabled.' ) ) );
			return false;
		}
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valOccupancyTypeId();
				$boolIsValid &= $this->valArTriggerId();
				break;

			case 'validate_invoice_settings':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valOccupancyTypeId();
				$boolIsValid &= $this->valInvoicingStartDate();
				$boolIsValid &= $this->valInvoiceTemplate();
				$boolIsValid &= $this->valCreditNoteInvoiceDocumentId();
                $boolIsValid &= $this->valCorrectiveInvoiceDocumentId();
				$boolIsValid &= $this->valDownPaymentInvoiceDocumentId();
				$boolIsValid &= $this->valDownPaymentInvoicePrefix();
				$boolIsValid &= $this->valAutoInvoiceDay();
				break;

			case 'validate_auto_allocate_payments':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valOccupancyTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Get Functions
	 */

	public function getArTriggerName() {
		return $this->m_strArTriggerName;
	}

	public function getOccupancyTypeName() {
		return $this->m_strOccupancyTypeName;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['occupancy_type_name'] ) ) $this->setOccupancyTypeName( $arrmixValues['occupancy_type_name'] );
		if( true == isset( $arrmixValues['ar_trigger_name'] ) ) $this->setArTriggerName( $arrmixValues['ar_trigger_name'] );
		return;
	}

	public function setArTriggerName( $strArTriggerName ) {
		$this->m_strArTriggerName = $strArTriggerName;
	}

	public function setOccupancyTypeName( $strOccupancyTypeName ) {
		$this->m_strOccupancyTypeName = $strOccupancyTypeName;
	}

	public function setInvoiceDefaultDueDays( $intInvoiceDefaultDueDays ) {
		$this->set( 'm_intInvoiceDefaultDueDays', CStrings::strTrimDef( $intInvoiceDefaultDueDays, -1, NULL, true ) );
	}

}
?>