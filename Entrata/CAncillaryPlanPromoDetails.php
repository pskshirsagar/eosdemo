<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAncillaryPlanPromoDetails
 * Do not add any new functions to this class.
 */

class CAncillaryPlanPromoDetails extends CBaseAncillaryPlanPromoDetails {

    public static function fetchPaginatedAncillaryPlanDetails( $objPagination, $objClientDatabase ) {

		$strSql = 'SELECT
						appd.*,
						av.name as vendor_name,
						(
							CASE
								WHEN apr.postal_codes IS NOT NULL THEN apr.postal_codes
								WHEN apr.state_code IS NOT NULL THEN apr.state_code
								ELSE apr.country_code
							END
						) AS location
					FROM
						ancillary_plan_promo_details appd
						JOIN ancillary_vendors av ON appd.ancillary_vendor_id = av.id
						LEFT JOIN ancillary_promo_regions apr ON appd.id = apr.ancillary_plan_promo_details_id
					ORDER BY
						appd.created_on DESC,
						appd.ancillary_vendor_id
					OFFSET
						' . ( int ) ( ( $objPagination->getPageNo() - 1 ) * $objPagination->getPageSize() ) . '
					LIMIT
						' . ( int ) $objPagination->getPageSize();

		return self::fetchAncillaryPlanPromoDetails( $strSql, $objClientDatabase );
	}

	public static function fetchAncillaryPlanPromoDetailsIdsByAncillaryVendorId( $intVendorId, $objDatabase ) {
		if( false == valId( $intVendorId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id
					FROM
						ancillary_plan_promo_details
					WHERE
						ancillary_vendor_id = ' . ( int ) $intVendorId;

		$arrintData = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrintData ) ) ? implode( ',', array_keys( rekeyArray( 'id', $arrintData ) ) ) : NULL;
	}

	public static function fetchAncillaryPlanPromoDetailsByRegionByVendorIdByExternalPlanIds( $intVendorId, $arrstrExternalPlanIds, $objPropertyAddress, $objDatabase ) {
	    if( false == valArr( $arrstrExternalPlanIds ) ) return NULL;

	    $strSql = 'SELECT
	                   appd.*,
	                   av.promotional_description,
	                   av.name as vendor_name
	               FROM
	                   ancillary_plan_promo_details appd
	                   JOIN ancillary_promo_regions apr ON appd.id = apr.ancillary_plan_promo_details_id AND ( apr.postal_codes = \'' . $objPropertyAddress->getPostalCode() . '\' OR apr.state_code = \'' . $objPropertyAddress->getStateCode() . '\' OR apr.country_code = \'' . $objPropertyAddress->getCountryCode() . '\' )
	                   LEFT JOIN ancillary_vendors av ON appd.ancillary_vendor_id = av.id
	               WHERE
	                   appd.external_plan_id IN ( \'' . implode( '\',\'', $arrstrExternalPlanIds ) . '\')
	                   AND av.id = ' . ( int ) $intVendorId;

	    return fetchData( $strSql, $objDatabase );
	}

	public static function deleteAncillaryPlanPromoDetailsByIds( $arrintIds, $objDatabase ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'DELETE
					FROM
						ancillary_plan_promo_details
					WHERE
						id IN ( ' . implode( ',', $arrintIds ) . ' ) ';

		if( false == $objDatabase->execute( $strSql ) ) {
			return false;
		}

		return true;
	}

}
?>