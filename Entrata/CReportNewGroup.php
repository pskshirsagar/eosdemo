<?php

class CReportNewGroup extends CBaseReportNewGroup {

	/** @var CReportNewInstance[] */
	private $m_arrobjReportInstances = [];

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['report_instances'] ) && true == valArr( $arrmixReportInstances = json_decode( $arrmixValues['report_instances'], true ) ) ) {
			$this->setReportInstances( array_filter( array_map( function( $arrmixReportInstance ) {
				if( false == isset( $arrmixReportInstance['report_version_id'] ) ) return NULL;

				$objReportInstance = new CReportNewInstance();
				$objReportInstance->setValues( $arrmixReportInstance );
				// Validate report object.
				$objReportValidation = \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( CReportsValidation::class );
				if( false == $objReportValidation->validateReport( $objReportInstance->getReport() ) ) {
					return NULL;
				}
				return $objReportInstance;
			}, $arrmixReportInstances ) ) );
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( date( 'm/d/Y H:i:s' ) );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function getReportInstances() {
		return $this->m_arrobjReportInstances;
	}

	public function setReportInstances( $arrobjReportInstances ) {
		$this->m_arrobjReportInstances = $arrobjReportInstances;
		return $this;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportGroupTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;
		if( !valstr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Report Group name is required.' ) ) );
		}
		// Name must be unique vs other groups with same type (and company_user_id if type=2)
		if( true == valArr( \Psi\Eos\Entrata\CReportNewGroups::createService()->fetchDuplicateReportGroups( $this, $objDatabase ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'A report group with the name "{%s, 0}" already exists', [ $this->getName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valIsInternal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		switch( $strAction ) {
			case VALIDATE_INSERT:
				return $this->valName( $objDatabase );

			case VALIDATE_UPDATE:
				return ( $this->valId() && $this->valCid() && $this->valName( $objDatabase ) );

			case VALIDATE_DELETE:
				return ( $this->valId() && $this->valCid() );

			default:
				return false;
		}
	}

}
