<?php

class CLeaseAccommodation extends CBaseLeaseAccommodation {

	protected $m_intPropertyId;
	protected $m_intArApprovalRequestId;
	protected $m_intApprovalStatusTypeId;

	protected $m_strAccommodationName;

	protected $m_boolArApprovalRequestIsArchived;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentLeaseAccommodationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccommodationId() {
		$boolIsValid = true;

		if( false == valId( $this->getAccommodationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'accommodation_id', __( 'Category is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLeaseAccommodationStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;

		if( false == valStr( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Title is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {
		if( false == valStr( $this->getStartDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start Date is required.' ), NULL ) );
			return false;
		} elseif( false == is_numeric( strtotime( $this->getStartDate() ) ) || false == CValidation::checkDateFormat( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getStartDate() ] ), $boolFormat = true ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start Date is not valid.' ), NULL ) );
			return false;
		}

		return true;
	}

	public function valEndDate( $arrmixDataToValidate ) {

		if( true == getArrayElementByKey( 'does_not_end', $arrmixDataToValidate ) ) return true;

		if( false == valStr( $this->getEndDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End Date is required.' ), NULL ) );
			return false;
		} elseif( false == is_numeric( strtotime( $this->getEndDate() ) ) || false == CValidation::checkDateFormat( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getEndDate() ] ), $boolFormat = true ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End Date is not valid.' ), NULL ) );
			return false;
		} elseif( strtotime( $this->getStartDate() ) > strtotime( $this->getEndDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End Date should be greater than or equal to Start date.' ), NULL ) );
			return false;
		}

		return true;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixDataToValidate = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valAccommodationId();
				$boolIsValid &= $this->valEndDate( $arrmixDataToValidate );
				break;

			case VALIDATE_UPDATE:
				break;

			case 'validate_end_lease_accommodation':
				$boolIsValid &= $this->valEndDate( $arrmixDataToValidate );
				break;

			case 'update_lease_accommodation':
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate( $arrmixDataToValidate );
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valAccommodationId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['accommodation_name'] ) ) $this->setAccommodationName( $arrmixValues['accommodation_name'] );
		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['ar_approval_request_id'] ) ) $this->setArApprovalRequestId( $arrmixValues['ar_approval_request_id'] );
		if( true == isset( $arrmixValues['approval_status_type_id'] ) ) $this->setApprovalStatusTypeId( $arrmixValues['approval_status_type_id'] );
		if( true == isset( $arrmixValues['ar_approval_request_is_archived'] ) ) $this->setArApprovalRequestIsArchived( $arrmixValues['ar_approval_request_is_archived'] );
	}

	public function setAccommodationName( $strAccommodationName ) {
		$this->m_strAccommodationName = $strAccommodationName;
	}

	public function getAccommodationName() {
		return $this->m_strAccommodationName;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getArApprovalRequestId() {
		return $this->m_intArApprovalRequestId;
	}

	public function getApprovalStatusTypeId() {
		return $this->m_intApprovalStatusTypeId;
	}

	public function getArApprovalRequestIsArchived() {
		return $this->m_boolArApprovalRequestIsArchived;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setArApprovalRequestId( $intArApprovalRequestId ) {
		$this->m_intArApprovalRequestId = $intArApprovalRequestId;
	}

	public function setApprovalStatusTypeId( $intApprovalStatusTypeId ) {
		$this->m_intApprovalStatusTypeId = $intApprovalStatusTypeId;
	}

	public function setArApprovalRequestIsArchived( $boolArApprovalRequestIsArchived ) {
		$this->m_boolArApprovalRequestIsArchived = $boolArApprovalRequestIsArchived;
	}

}
?>