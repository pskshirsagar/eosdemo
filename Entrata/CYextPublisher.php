<?php
use Psi\Libraries\UtilConfig\CConfig;

class CYextPublisher extends CBaseYextPublisher {

	use TEosStoredObject;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemoteId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogoUri() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function buildSqlWhere( CGenericIlsRaFilter $objFilter ) : array {
		$arrstrWhere = [];

		if( true == $objFilter->has( 'ids' ) ) {
			$arrstrWhere[] = 'yps.id IN ( ' . \sqlIntImplode( $objFilter->get( 'ids' ) ) . ')';
		}

		if( true == $objFilter->has( 'property_ids' ) ) {
			$arrstrWhere[] = 'yps.property_id IN ( ' . \sqlIntImplode( $objFilter->get( 'property_ids' ) ) . ')';
		}

		if( true == $objFilter->has( 'yext_publisher_ids' ) ) {
			$arrstrWhere[] = 'yps.yext_publisher_id IN ( ' . \sqlIntImplode( $objFilter->get( 'yext_publisher_ids' ) ) . ')';
		}

		if( true == $objFilter->has( 'active_publisher' ) ) {
			$arrstrWhere[] = 'yp.deleted_on IS NULL AND yp.deleted_by IS NULL';
		}

		if( true == $objFilter->has( 'cid' ) ) {
			$arrstrWhere[] = 'yps.cid = ' . ( int ) $objFilter->get( 'cid' );
		}

		return $arrstrWhere;
	}

	protected function calcStorageContainer( $strVendor = NULL ) : string {
		switch( $strVendor ) {
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return CMarketingHubLibrary::MEDIA_LIBRARY_PATH;
			default:
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3:
				return CONFIG_OSG_BUCKET_MEDIA_LIBRARY;
		}
	}

	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) : string {
		return CMarketingHubLibrary::YEXT_LOGO_PATH . basename( $_FILES['logo_uri']['name'] );
	}

	public function getLogoUri() {
		return $this->getCdnUrl( $this->m_strLogoUri);
	}

	protected function getCdnUrl( $strUrl ) {
		$strCdnUrl = '';
		if( true == valStr( $strUrl ) ) {
			return ( false === strpos( $strUrl, 'media_library' ) ) ? CConfig::createService()->get( 'is_secure_url' ) ? 'https://' : 'http://' . CConfig::createService()->get( 'osg_distribution_media_library' ) . '/'. $strUrl : CONFIG_MEDIA_LIBRARY_PATH . $strUrl;
		}

		return $strCdnUrl;
	}

}
?>