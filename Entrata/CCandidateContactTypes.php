<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCandidateContactTypes
 * Do not add any new functions to this class.
 */

class CCandidateContactTypes extends CBaseCandidateContactTypes {

	public static function fetchCandidateContactTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCandidateContactType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCandidateContactType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCandidateContactType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>