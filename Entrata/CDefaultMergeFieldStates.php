<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultMergeFieldStates
 * Do not add any new functions to this class.
 */

class CDefaultMergeFieldStates extends CBaseDefaultMergeFieldStates {

	public static function fetchAllDefaultMergeFieldStates( $objDatabase ) {
		return self::fetchDefaultMergeFieldStates( 'SELECT * FROM default_merge_field_states', $objDatabase );
	}

	public static function  fetchDefaultMergeFieldStatesByStateCode( $strStateCode, $objDatabase ) {
		return self::fetchDefaultMergeFieldStates( sprintf( 'SELECT * FROM default_merge_field_states WHERE state_code = \'%s\'', $strStateCode ), $objDatabase );
	}
}
?>