<?php

class CEventReference extends CBaseEventReference {

	// Added for alert_messages
	public $m_objEvent;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEventReferenceTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReferenceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEventId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	$boolIsValid  = true;

    	if( true == $boolReturnSqlOnly ) {
    		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	} else {
    		$boolIsValid  &= parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	}

    	if( true == $boolIsValid ) {
	    	// Call for LeadAlert.
			$objAlertMessage = new CAlertMessage();
			$objAlertMessage->syncAlertMessages( $intCurrentUserId, $objDatabase, $this->getEventId() );
    	}

    	return $boolIsValid;
    }

    /**
     * Set Functions
     */

	public function setEvent( $objEvent ) {
		$this->m_objEvent = $objEvent;
	}

	/**
	 * Get Functions
	 */

	public function getEvent() {
		return $this->m_objEvent;
	}

    /**
     * Other Functions
     */

    public function getConsolidatedErrorMsg() {
    	$strErrorMessage = NULL;

    	if( true == valArr( $this->m_arrobjErrorMsgs ) ) {
    		foreach( $this->m_arrobjErrorMsgs as $objErrorMsg ) {
    			$strErrorMessage .= $objErrorMsg->getMessage() . "\n";
    		}
    	}
    	return $strErrorMessage;
    }

}
?>