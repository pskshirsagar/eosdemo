<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileEvents
 * Do not add any new functions to this class.
 */

class CFileEvents extends CBaseFileEvents {

	public static function fetchRecentFileEventsByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						e.id,
						e.event_description,
						util_get_translated( \'name\', et.name, et.details ) as event_type_name,
						e.created_by,
						e.created_on,
						e.details,
						COALESCE( ce.name_first || \' \' || ce.name_last, cu.username ) as company_user_name
					FROM
						file_events AS e
						JOIN file_event_types et ON ( et.id = e.file_event_type_id )
						LEFT JOIN company_users cu ON ( cu.cid = e.cid AND cu.id = e.company_user_id AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE	e.file_id = ' . ( int ) $intFileId . '
					AND		e.cid =	' . ( int ) $intCid . '
					ORDER BY
						e.id DESC
					LIMIT 5';

		return self::fetchFileEvents( $strSql, $objDatabase );
	}

	public static function fetchCountRecentFileEventsByFileIdByCid( $intFileId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count(e.id)
					FROM
						file_events AS e
						JOIN file_event_types et ON ( et.id = e.file_event_type_id )
						LEFT JOIN company_users cu ON ( cu.cid = e.cid AND cu.id = e.company_user_id AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE	e.file_id = ' . ( int ) $intFileId . '
					AND		e.cid =	' . ( int ) $intCid;

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		return ( false == is_null( $arrmixResponseData[0]['count'] ) ) ? $arrmixResponseData[0]['count'] : 0;
	}

	public static function fetchFileEventsByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						e.id,
						e.cid,
						e.event_description,
						et.name as event_type_name,
						e.created_by,
						e.created_on,
						e.details,
						COALESCE( ce.name_first || \' \' || ce.name_last, cu.username ) as company_user_name
					FROM
						file_events AS e
						JOIN file_event_types et ON ( et.id = e.file_event_type_id )
						LEFT JOIN company_users cu ON ( cu.cid = e.cid AND cu.id = e.company_user_id AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE	e.file_id = ' . ( int ) $intFileId . '
					AND		e.cid =	' . ( int ) $intCid . '
					ORDER BY
						e.id DESC';

		return self::fetchFileEvents( $strSql, $objDatabase );
	}

	public static function fetchFileEventsByFileIdsByCid( $arrintFileIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintFileIds ) ) return NULL;

		$strSql = 'SELECT
						fe.*
					FROM
						file_events fe
					WHERE
						fe.file_id IN ( ' . implode( ',', $arrintFileIds ) . ' )
						AND fe.cid = ' . ( int ) $intCid;

		return self::fetchFileEvents( $strSql, $objDatabase );
	}

}
?>