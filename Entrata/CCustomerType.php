<?php

class CCustomerType extends CBaseCustomerType {

	const PRIMARY					= 1;
	const RESPONSIBLE				= 2;
	const NOT_RESPONSIBLE			= 3;
	const GUARANTOR					= 4;
	const NON_LEASING_OCCUPANT		= 5;

	public static $c_arrintResponsibleCustomerTypeIds 			= [ self::PRIMARY, self::RESPONSIBLE, self::GUARANTOR ];
	public static $c_arrintApplicationRequiredCustomerTypeIds 	= [ self::PRIMARY, self::RESPONSIBLE, self::GUARANTOR, self::NON_LEASING_OCCUPANT ];
	public static $c_arrintSecondaryCustomerTypeIds 			= [ self::RESPONSIBLE, self::GUARANTOR, self::NOT_RESPONSIBLE, self::NON_LEASING_OCCUPANT ];
	public static $c_arrintAllCustomerTypeIds 					= [ self::PRIMARY, self::RESPONSIBLE, self::NOT_RESPONSIBLE, self::GUARANTOR, self::NON_LEASING_OCCUPANT ];
	public static $c_arrintSubsidizedSecondaryCustomerTypeIds 	= [ self::RESPONSIBLE, self::NOT_RESPONSIBLE ];
	public static $c_arrintAllMilitaryCustomerTypeIds 			= [ self::PRIMARY, self::RESPONSIBLE, self::NOT_RESPONSIBLE, self::NON_LEASING_OCCUPANT ];
	public static $c_arrintInsuranceResponsibleCustomerTypeIds 	= [ self::PRIMARY, self::RESPONSIBLE ];
	public static $c_arrintParentConsentRequiredCustomerTypeIds = [ self::PRIMARY, self::RESPONSIBLE ];
	public static $c_arrintResponsibleAndNonLeasingCustomerTypeIds = [ self::PRIMARY, self::RESPONSIBLE, self::GUARANTOR, self::NON_LEASING_OCCUPANT ];
	public static $c_arrintGuarantorAndNonLeasingCustomerTypeIds = [ self::GUARANTOR, self::NON_LEASING_OCCUPANT ];
	public static $c_arrintRenewalOfferExpirationReminderNoticeSentToCustomerTypeIds = [ self::PRIMARY, self::RESPONSIBLE ];
	public static $c_arrintCorporateScreeningCustomerTypeIds 	= [ self::RESPONSIBLE, self::GUARANTOR, self::NON_LEASING_OCCUPANT ];
	public static $c_arrintResponsibleApplicantsCustomerTypeIds = [ self::PRIMARY, self::RESPONSIBLE, self::GUARANTOR ];
	public static $c_arrintResponsibleOccupantsCustomerTypeIds = [ self::PRIMARY, self::RESPONSIBLE ];
	public static $c_arrintNonResponsibleOccupantsCustomerTypeIds = [ self::NOT_RESPONSIBLE, self::NON_LEASING_OCCUPANT ];
	public static $c_arrintAllOccupantsCustomerTypeIds = [ self::PRIMARY, self::RESPONSIBLE, self::NOT_RESPONSIBLE, self::NON_LEASING_OCCUPANT ];
	public static $c_arrintResponsibleAndGuarantorCustomerTypeIds = [ self::GUARANTOR, self::RESPONSIBLE ];

	public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

    public function getCustomerTypeIdToStrArray() {
    	return [
            self::PRIMARY 					=> __( 'Primary' ),
            self::RESPONSIBLE 				=> __( 'Co-Applicant' ),
            self::GUARANTOR 				=> __( 'Guarantor' ),
            self::NOT_RESPONSIBLE 			=> __( 'Occupant' ),
            self::NON_LEASING_OCCUPANT		=> __( 'Non-Leasing Occupant' )
	    ];
    }

    public function customerTypeIdToStr( $intCustomerTypeId ) {
    	switch( $intCustomerTypeId ) {
    		case self::PRIMARY:
    			return __( 'Primary' );
    		case self::RESPONSIBLE:
    			return __( 'Co-Applicant' );
    		case self::GUARANTOR:
    			return __( 'Guarantor' );
    		case self::NOT_RESPONSIBLE:
    			return __( 'Occupant' );
    		case self::NON_LEASING_OCCUPANT:
    			return __( 'Non-Leasing Occupant' );
    		default:
    			return NULL;
    	}
    }

    public function getResponsibleCustomerTypeIdToStrArray() {
    	return [
            self::PRIMARY 		=> __( 'Primary' ),
            self::RESPONSIBLE 	=> __( 'Co-Applicant' ),
            self::GUARANTOR	 	=> __( 'Guarantor' )
	    ];
    }

    public static function assignSmartyConstants( $objSmarty ) {
    	$objSmarty->assign( 'CUSTOMER_TYPE_PRIMARY', 					self::PRIMARY );
    	$objSmarty->assign( 'CUSTOMER_TYPE_RESPONSIBLE', 				self::RESPONSIBLE );
    	$objSmarty->assign( 'CUSTOMER_TYPE_GUARANTOR', 					self::GUARANTOR );
    	$objSmarty->assign( 'CUSTOMER_TYPE_NOT_RESPONSIBLE', 			self::NOT_RESPONSIBLE );
    	$objSmarty->assign( 'CUSTOMER_TYPE_NON_LEASING_OCCUPANT',	self::NON_LEASING_OCCUPANT );
    }

	public static function assignTemplateConstants( $arrmixTemplateParameters ) {
		$arrmixTemplateParameters['CUSTOMER_TYPE_PRIMARY'] 					= self::PRIMARY;
		$arrmixTemplateParameters['CUSTOMER_TYPE_RESPONSIBLE'] 				= self::RESPONSIBLE;
		$arrmixTemplateParameters['CUSTOMER_TYPE_GUARANTOR'] 				= self::GUARANTOR;
		$arrmixTemplateParameters['CUSTOMER_TYPE_NOT_RESPONSIBLE'] 			= self::NOT_RESPONSIBLE;
		$arrmixTemplateParameters['CUSTOMER_TYPE_NON_LEASING_OCCUPANT'] 	= self::NON_LEASING_OCCUPANT;

		return $arrmixTemplateParameters;
	}

}
?>