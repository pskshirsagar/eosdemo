<?php

class CDefaultScheduledTask extends CBaseDefaultScheduledTask {

	const LEAD_CONSIDER_NOT_PROGRESSING	 		    = 18;
	const CANCEL_OR_ARCHIVE_LEAD_AFTER	 		    = 19;
	const APPLICANT_CONSIDER_NOT_PROGRESSING	    = 38;
	const CANCEL_OR_ARCHIVE_APPLICANT_AFTER		    = 39;
	const GUEST_CARD_FOLLOWUP_AFTER		            = 40;
	const BULK_UNIT_ASSIGNMENT_NOTIFICATION		    = 72;
	const RETURN_TO_NOT_PROGRESSING_LEAD	 	    = 81;
	const RETURN_TO_NOT_PROGRESSING_APPLICANT	    = 82;
	const LEAD_AUTO_CANCEL_OR_ARCHIVE_REMINDER      = 83;
	const APPLICANT_AUTO_CANCEL_OR_ARCHIVE_REMINDER = 84;
	const LEAD_APPOINTMENT_REMINDER	 		 		= 10;
	const APPLICANT_APPOINTMENT_REMINDER	 		= 31;
	const LEAD_SELF_GUIDED_TOUR_REMINDER            = 228;
	const APPLICANT_SELF_GUIDED_TOUR_REMINDER       = 229;

	const MOVE_IN									= 45;
	const MOVE_OUT									= 48;

	const COMMERCIAL_MOVE_IN						= 147;
	const COMMERCIAL_MOVE_OUT						= 148;
	const COMMERCIAL_PRE_LEASE_EXPIRATION_REMINDER  = 149;
	const COMMERCIAL_UNINSURED_TENANTS				= 213;

	const APPLICANT_AUTO_CANCEL_OR_ARCHIVE_AFTER_MOVE_IN    = 89;
	const LEAD_AUTO_CANCEL_OR_ARCHIVE_AFTER_MOVE_IN         = 90;

	const CANCEL_OR_ARCHIVE_LEAD_APPLICANT_AFTER_DEFAULT_DAYS               = 180;
	const CANCEL_OR_ARCHIVE_LEAD_APPLICANT_AFTER_DAYS_FOR_STUDENT_PROPERTY  = 365;

	const LEAD_INCOMING_CALL_ANSWERED_BY_LEASING_CENTER = 120;
	const APPLICANT_INCOMING_CALL_ANSWERED_BY_LEASING_CENTER = 121;

	const LEAD_APPOINTMENT_SCHEDULED_BY_LEASING_CENTER = 122;
	const APPLICANT_APPOINTMENT_SCHEDULED_BY_LEASING_CENTER = 123;
	const PRE_QUALIFICATION_APPROVED = 137;
	const ROOMMATE_INVITATION_SENT = 154;
	const ROOMMATE_INVITATION_ACCEPTED = 155;
	const AGENT_ACCESS_INVITATION_EMAIL = 171;
	const AGENT_ACCESS_CONFIRMATION_EMAIL = 172;
	const AGENT_ACCESS_RESEND_INVITATION_EMAIL = 173;
	const PARENTAL_GUARDIAN_CONSENT_BACKGROUND_CHECK = 179;
	const DEPOSITS_DETAILS_PDF = 184;
	const PAYMENT_SUBMITTED_FOR_NAME = 189;
	const RESIDENT_SUPPORT_CALL = 190;

	const INCOMPLETE_MOVE_IN_CHECKLIST_ITEMS = 188;

	public static $c_arrintIncomingCallAnsweredByLeasingCenterDefaultScheduledTaskIds = [
		self::LEAD_INCOMING_CALL_ANSWERED_BY_LEASING_CENTER,
		self::APPLICANT_INCOMING_CALL_ANSWERED_BY_LEASING_CENTER
	];

	public static $c_arrintAppointmentScheduledIds = [
		self::LEAD_APPOINTMENT_SCHEDULED_BY_LEASING_CENTER,
		self::APPLICANT_APPOINTMENT_SCHEDULED_BY_LEASING_CENTER
	];

	public static $c_arrintAppointmentReminderIds = [
		self::LEAD_APPOINTMENT_REMINDER,
		self::APPLICANT_APPOINTMENT_REMINDER
	];

	public static $c_arrintHiddenDefaultScheduledTaskIdsFromUi = [
		self::AGENT_ACCESS_INVITATION_EMAIL,
		self::AGENT_ACCESS_CONFIRMATION_EMAIL,
		self::AGENT_ACCESS_RESEND_INVITATION_EMAIL,
		self::PARENTAL_GUARDIAN_CONSENT_BACKGROUND_CHECK,
		self::DEPOSITS_DETAILS_PDF,
		self::PAYMENT_SUBMITTED_FOR_NAME,
		self::RESIDENT_SUPPORT_CALL
	];

	public static $c_arrintCancelOrArchiveReminderDefaultScheduledTaskIds = [
		self::LEAD_AUTO_CANCEL_OR_ARCHIVE_REMINDER,
		self::APPLICANT_AUTO_CANCEL_OR_ARCHIVE_REMINDER,
		self::CANCEL_OR_ARCHIVE_LEAD_AFTER,
		self::CANCEL_OR_ARCHIVE_APPLICANT_AFTER
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledTaskTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduleTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultScheduledTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledTaskGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEventTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEventSubTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduleDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
