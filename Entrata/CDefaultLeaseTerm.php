<?php

class CDefaultLeaseTerm extends CBaseDefaultLeaseTerm {

	const GROUP = 201;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultLeaseTermStructureId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTermMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowProspectEdit() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowRenewalEdit() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDateBased() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsProspect() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsRenewal() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDisabled() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDefault() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsUnset() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valShowOnWebsite() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>