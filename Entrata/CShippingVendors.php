<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CShippingVendors
 * Do not add any new functions to this class.
 */

class CShippingVendors extends CBaseShippingVendors {

	public static function fetchShippingVendorByVendorNameByPropertyIdByCid( $strVendorName, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valStr( $strVendorName ) ) return NULL;

		$strSql = 'SELECT
						sv.*
					FROM
						shipping_vendors sv
					WHERE
						sv.cid = ' . ( int ) $intCid . '
						AND sv.property_id = ' . ( int ) $intPropertyId . '
						AND sv.vendor_name = \'' . ( string ) $strVendorName . '\'
						AND sv.is_custom = false';

		return self::fetchShippingVendor( $strSql, $objDatabase );
	}

	public static function fetchCustomShippingVendorsWithIconByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $isBoolShowDefaultPackagType = false ) {

	if( false == $isBoolShowDefaultPackagType ) {
		$strSqlWhereCluase = ' AND sv.property_id IS NOT NULL AND sv.property_id = ' . ( int ) $intPropertyId;
	} elseif( false == valId( $intPropertyId ) ) {
		$strSqlWhereCluase = ' AND sv.is_enabled = true AND ( sv.property_id IS NULL OR sv.property_id = ' . ( int ) $intPropertyId . ' ) ';
	} else {
		$strSqlWhereCluase = ' AND sv.is_enabled = true AND (sv.property_id = ' . ( int ) $intPropertyId . ' ) ';
	}

		if( true == $isBoolShowDefaultPackagType ) {
			$strSqlOrderByClause = ' ORDER BY vendor_name ASC';
		} else {
			$strSqlOrderByClause = ' ORDER BY (CASE WHEN is_custom is false then 1 else 2 end) ';
		}

		$strSql = 'SELECT
						sv.*,
						cmf.fullsize_uri
					FROM
						shipping_vendors sv
						LEFT JOIN company_media_files cmf ON cmf.id = sv.company_media_file_id AND cmf.cid = sv.cid
					WHERE
						sv.cid = ' . ( int ) $intCid . $strSqlWhereCluase . $strSqlOrderByClause;
		return self::fetchShippingVendors( $strSql, $objDatabase );
	}

	public static function fetchShippingVendorsWithIconByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sv.*,
						cmf.fullsize_uri
					FROM
						shipping_vendors sv
						LEFT JOIN company_media_files cmf ON cmf.id = sv.company_media_file_id AND cmf.cid = sv.cid
					WHERE
						sv.cid = ' . ( int ) $intCid . '  AND sv.property_id IS NOT NULL';
		return self::fetchShippingVendors( $strSql, $objDatabase );
	}

	public static function fetchShippingVendorsByShippingVendorName( $strVendorName, $objDatabase ) {
		$strSql = 'SELECT
						sv.id, sv.cid
					FROM
						shipping_vendors sv
					WHERE
						lower( sv.vendor_name ) like \'' . ( string ) $strVendorName . '\'
						AND sv.property_id IS NOT NULL
						AND is_enabled = true';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchShippingVendorsWithIconByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

				$strSql = 'SELECT
						sv.*,
                        cmf.fullsize_uri
					FROM
						shipping_vendors sv
						LEFT JOIN company_media_files cmf ON( cmf.id = sv.company_media_file_id AND cmf.cid = sv.cid )
					WHERE
						sv.cid = ' . ( int ) $intCid . '
						AND ( sv.property_id = ' . ( int ) $intPropertyId . ' )
					ORDER BY (CASE WHEN is_custom is false then 1 else 2 end)';
				return self::fetchShippingVendors( $strSql, $objDatabase );
	}

	public static function fetchShippingVendorIdByVendorNameByCid( $strVendorName, $intCid, $objDatabase ) {
		if( false == valStr( $strVendorName ) ) return NULL;

		$strSql = 'SELECT
						sv.id
					FROM
						shipping_vendors sv
					WHERE
						sv.cid = ' . ( int ) $intCid . '
						AND lower( sv.vendor_name ) like \'' . ( string ) $strVendorName . '\'
						AND sv.is_custom = false';

		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchDisabledCustomShippingVendorsByCid( $strVendorName, $objDatabase ) {

		$strSql = 'SELECT
						sv.id, sv.cid, sv.property_id 
					FROM
						shipping_vendors sv
					WHERE
						lower( sv.vendor_name ) like \'' . ( string ) $strVendorName . '\'
						AND sv.property_id IS NOT NULL
						AND is_enabled = false
		                AND is_custom = true';
		return fetchData( $strSql, $objDatabase );
	}

	public function fetchDuplicateShippingVendors( $strVendorName, $intCid, $intPropertyId, $objDatabase, $intVendorId = NULL ) {

		if( false == valStr( $strVendorName ) || true == is_null( $intCid ) || true == is_null( $intPropertyId ) ) return NULL;

		$strVendorName = trim( $strVendorName );

		$strSql = 'SELECT COUNT(id)
							FROM
								shipping_vendors as sv
							WHERE
								sv.cid = ' . ( int ) $intCid . '
							AND
								sv.property_id = ' . ( int ) $intPropertyId . '
							AND
								lower( sv.vendor_name ) like lower( \'' . ( string ) pg_escape_string( $strVendorName ) . '\')';
		if( $intVendorId ) {
			$strSql .= ' AND sv.id != ' . ( int ) $intVendorId;
		}
		$arrDuplicateCount = fetchData( $strSql, $objDatabase );
		return $arrDuplicateCount['0']['count'];
	}

	public function fetchPropertyShippingVendorsIdsByIdByPropertyIds( $strVendorId, $arrPropertyIds, $objDatabase ) {

		$arrPropertyIds = implode( ',', $arrPropertyIds );

		$strSql = 'SELECT id
              FROM
                shipping_vendors as sv
              WHERE
                vendor_name = ( SELECT vendor_name from shipping_vendors
                  WHERE
                    id = ' . ( int ) $strVendorId . '
                  LIMIT 1 )
              AND
                sv.property_id IN( ' . $arrPropertyIds . ' ) ';

		$arrintShippingVendorsIds = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrintShippingVendorsIds ) ) {
			$strShippingVendorsIds = implode( ',', ( array ) array_column( $arrintShippingVendorsIds, 'id' ) );
			$strShippingVendorsIds .= ',' . $strVendorId;
		} else {
			$strShippingVendorsIds = $strVendorId;
		}

		return $strShippingVendorsIds;
	}

	public static function fetchNonDefaultShippingVendorsWithIconByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sv.*,
                        cmf.fullsize_uri
					FROM
						shipping_vendors sv
						LEFT JOIN company_media_files cmf ON( cmf.id = sv.company_media_file_id AND cmf.cid = sv.cid )
					WHERE
						sv.cid = ' . ( int ) $intCid . '
						AND sv.property_id = ' . ( int ) $intPropertyId . '
						AND sv.is_custom = true';

		return self::fetchShippingVendors( $strSql, $objDatabase );
	}

	public static function fetchEnabledNonDefaultShippingVendorsWithIconByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sv.*,
                        cmf.fullsize_uri
					FROM
						shipping_vendors sv
						LEFT JOIN company_media_files cmf ON( cmf.id = sv.company_media_file_id AND cmf.cid = sv.cid )
					WHERE
						sv.cid = ' . ( int ) $intCid . '
						AND sv.property_id = ' . ( int ) $intPropertyId . '
						AND sv.is_custom = true
						AND sv.is_enabled = true';

		return self::fetchShippingVendors( $strSql, $objDatabase );
	}

}
?>