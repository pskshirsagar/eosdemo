<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeTerms
 * Do not add any new functions to this class.
 */

class CApPayeeTerms extends CBaseApPayeeTerms {

	public static function fetchAllApPayeeTermsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_payee_terms
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					ORDER BY
						order_num,
						id';
		return self::fetchApPayeeTerms( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeTermByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_payee_terms apt
						JOIN ap_payees ap ON ( apt.cid = ap.cid AND apt.id = ap.ap_payee_term_id )
					WHERE
						apt.cid =' . ( int ) $intCid . '
						AND ap.id =' . ( int ) $intApPayeeId . '
						AND apt.deleted_by IS NULL
						AND apt.deleted_on IS NULL
					ORDER BY
						apt.order_num,
						apt.id';

		return self::fetchApPayeeTerm( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeTermBySystemCodeByCid( $strSystemCode, $intCid, $objClientDatabase ) {

		if( false == valStr( $strSystemCode ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_payee_terms
					WHERE
						cid = ' . ( int ) $intCid . '
						AND system_code = \'' . ( string ) $strSystemCode . '\'
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					ORDER BY
						order_num,
						id';

		return self::fetchApPayeeTerm( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeTermByDaysByCid( $intDays, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						id
					FROM
						ap_payee_terms
					WHERE
						cid = ' . ( int ) $intCid . '
						AND days = ' . ( int ) $intDays . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchApPayeeTerm( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeTermBySystemCodeByCids( $strSystemCode, $arrintCids, $objClientDatabase ) {

		if( false == valStr( $strSystemCode ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_payee_terms
					WHERE
						cid IN ( ' . sqlIntImplode( $arrintCids ) . ' )
						AND system_code = \'' . ( string ) $strSystemCode . '\'
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					ORDER BY
						order_num,
						id';

		return self::fetchApPayeeTerms( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPayeeTermsByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApHeaderIds ) ) return NULL;

		$strSql = 'SELECT
						apt.name,
						ah.id AS ap_header_id
					FROM
						ap_payee_terms apt
						JOIN ap_payees ap ON ( apt.cid = ap.cid AND apt.id = ap.ap_payee_term_id )
						JOIN ap_headers ah ON ( ap.cid = ah.cid AND ap.id = ah.ap_payee_id )
					WHERE
						apt.cid =' . ( int ) $intCid . '
						AND ah.id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND apt.deleted_by IS NULL
						AND apt.deleted_on IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeTermsByIdsByCid( $arrintApPayeeTermIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeTermIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_terms
					WHERE
						cid =' . ( int ) $intCid . '
						AND id IN ( ' . sqlIntImplode( $arrintApPayeeTermIds ) . ' )';

		return self::fetchApPayeeTerms( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeTermsByApPayeeIdsByCid( $intApPayeeIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $intApPayeeIds ) ) return NULL;

		$strSql = 'SELECT
						apt.*
					FROM
						ap_payee_terms apt
						JOIN ap_payees ap ON ( apt.cid = ap.cid AND apt.id = ap.ap_payee_term_id )
					WHERE
						apt.cid =' . ( int ) $intCid . '
						AND ap.id IN( ' . implode( ',', $intApPayeeIds ) . ' )
						AND apt.deleted_by IS NULL
						AND apt.deleted_on IS NULL
					ORDER BY
						apt.order_num,
						apt.id';
		return self::fetchApPayeeTerms( $strSql, $objClientDatabase );
	}

}
?>