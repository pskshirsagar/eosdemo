<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerSubsidyRaceTypes
 * Do not add any new functions to this class.
 */

class CCustomerSubsidyRaceTypes extends CBaseCustomerSubsidyRaceTypes {

	public static function fetchActiveCustomerSubsidyRaceTypesByCustomerIdByCId( $intCustomerId, $intCid, $objDatabase ) {

		if ( false == valId( $intCustomerId ) )
			return NULL;

		$strSql = 'SELECT
						*
					FROM
						customer_subsidy_race_types
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchCustomerSubsidyRaceTypes( $strSql, $objDatabase );
	}

	public static function fetchCustomerSubsidyRaceTypeByCustomerIdBySubsidyRaceTypeIdByCId( $intCustomerId, $intSubsidyRaceTypeId, $intCid, $objDatabase ) {

		if( false == valId( $intCustomerId ) || false == valId( $intSubsidyRaceTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						customer_subsidy_race_types
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id = ' . ( int ) $intCustomerId . '
						AND subsidy_race_type_id = ' . ( int ) $intSubsidyRaceTypeId . ' LIMIT 1;';

		return self::fetchCustomerSubsidyRaceType( $strSql, $objDatabase );
	}

	public static function fetchCustomerSubsidyRaceTypesByCustomerIdsByCId( $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						customer_subsidy_race_types
					WHERE
						cid = ' . ( int ) $intCid . ' 
						AND customer_id IN ( ' . sqlIntImplode( $arrintCustomerIds ) . ' )';

		return self::fetchCustomerSubsidyRaceTypes( $strSql, $objDatabase );
	}

}
?>