<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledApHeaderExportBatches
 * Do not add any new functions to this class.
 */

class CScheduledApHeaderExportBatches extends CBaseScheduledApHeaderExportBatches {

	public static function fetchPaginatedActiveScheduledApHeaderExportBatchesByCidByFilter( $intCid, $objDatabase, $intPageNo, $intPageSize, $arrmixFilterData ) {

		$intOffset	= 0;
		$intLimit	= 0;

		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit  = ( int ) $intPageSize;
		}

		$strSql = 'SELECT DISTINCT ON ( saheb.id ) saheb.*,
						sahebp.property_id
					FROM 
						scheduled_ap_header_export_batches saheb
						LEFT JOIN scheduled_ap_header_export_batch_properties sahebp ON ( saheb.id = sahebp.scheduled_ap_header_export_batch_id AND saheb.cid = sahebp.cid )
					WHERE
                        saheb.cid = ' . ( int ) $intCid . '
                        AND saheb.deleted_by IS NULL
                        AND saheb.deleted_on IS NULL';
		if( false != valArr( $arrmixFilterData['property_ids'] ) ) {
				$strSql .= ' AND sahebp.property_id IN ( ' . implode( ',', $arrmixFilterData['property_ids'] ) . '  )';
		}
		if( false != valStr( $arrmixFilterData['start_date'] ) ) {
			$strSql .= ' AND saheb.scheduled_start_on >= \'' . $arrmixFilterData['start_date'] . '\'';
		}
		if( false != valStr( $arrmixFilterData['end_date'] ) ) {
			$strSql .= ' AND saheb.scheduled_start_on <= \'' . $arrmixFilterData['end_date'] . '\'';
		}
		$strSql .= ' order by saheb.id DESC, saheb.created_on DESC';
		if( 0 < $intOffset ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset;
		}

		if( false == is_null( $intLimit ) && false == is_null( $intPageSize ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchScheduledApHeaderExportBatches( $strSql, $objDatabase );

	}

	public static function fetchTotalCountScheduledApHeaderExportBatches( $intCid, $objDatabase, $arrmixFilterData ) {

		$strSql = 'SELECT count( DISTINCT ( saheb ) )
					FROM 
						scheduled_ap_header_export_batches saheb
						JOIN scheduled_ap_header_export_batch_properties sahebp ON ( saheb.id = sahebp.scheduled_ap_header_export_batch_id AND saheb.cid = sahebp.cid )
					WHERE
                        saheb.cid = ' . ( int ) $intCid . '
                        AND saheb.deleted_by IS NULL
                        AND saheb.deleted_on IS NULL';
		if( false != valArr( $arrmixFilterData['property_ids'] ) ) {
			$strSql .= ' AND sahebp.property_id IN ( ' . implode( ',', $arrmixFilterData['property_ids'] ) . '  )';
		}
		if( false != valStr( $arrmixFilterData['start_date'] ) ) {
			$strSql .= ' AND saheb.scheduled_start_on >= \'' . $arrmixFilterData['start_date'] . '\'';
		}
		if( false != valStr( $arrmixFilterData['end_date'] ) ) {
			$strSql .= ' AND saheb.scheduled_start_on <= \'' . $arrmixFilterData['end_date'] . '\'';
		}
		$arrintResponse = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
		return 0;
	}

	public static function fetchCustomScheduledApHeaderExportBatchByIdByCid( $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
		                saheb.*,
		                ctv.request_url,
		                ctv.port,
						ctv.username_encrypted,
		                ctv.password_encrypted,
		                ctv.client_number,
                        tv.name as export_type_name,
                        ahefft.name as file_format_name,
                        aene.email_addresses
					FROM
						scheduled_ap_header_export_batches saheb
						JOIN transmission_vendors tv ON ( saheb.ap_header_export_batch_type_id = tv.id )
						LEFT JOIN ap_header_export_file_format_types ahefft ON ( tv.id = ahefft.ap_header_export_batch_type_id AND ahefft.id = saheb.ap_header_export_file_format_type_id )
						LEFT JOIN company_transmission_vendors ctv ON ( ctv.cid = saheb.cid AND ctv.id = saheb.company_transmission_vendor_id )
						LEFT JOIN accounting_export_notification_emails aene ON ( saheb.cid = aene.cid AND saheb.id = aene.scheduler_id AND aene.accounting_export_type_id = ' . CTransmissionType::POSITIVE_PAY . ' )
					WHERE
						saheb.cid = ' . ( int ) $intCid . '
						AND saheb.id = ' . ( int ) $intId . '
						AND tv.transmission_type_id = ' . CTransmissionType::POSITIVE_PAY . '
						AND saheb.deleted_by IS NULL
						AND saheb.deleted_on IS NULL';

		return self::fetchScheduledApHeaderExportBatch( $strSql, $objDatabase );

	}

	public static function fetchScheduledApHeaderExportBatchesByNextPostDate( $strDate, $objDatabase ) {
		return self::fetchScheduledApHeaderExportBatches( 'SELECT * FROM scheduled_ap_header_export_batches saheb WHERE  saheb.deleted_on IS NULL AND saheb.deleted_by IS NULL AND next_post_date = \'' . $strDate . '\' AND ( scheduled_end_on >= CURRENT_DATE OR scheduled_end_on IS NULL AND array_length(saheb.bank_account_ids,1 ) IS NOT NULL )', $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchesByIdsByCid( $arrintScheduledApHeaderExportBatchIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScheduledApHeaderExportBatchIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
                        saheb.*
                    FROM
                        scheduled_ap_header_export_batches saheb
                    WHERE
                         saheb.id IN ( ' . implode( ',', $arrintScheduledApHeaderExportBatchIds ) . ' )
                         AND saheb.cid =  ' . ( int ) $intCid . '
                         AND saheb.deleted_on IS NULL 
                         AND saheb.deleted_by IS NULL
                         AND array_length(saheb.bank_account_ids,1 ) IS NOT NULL';

		return self::fetchScheduledApHeaderExportBatches( $strSql, $objDatabase );
	}

	public static function fetchCustomScheduledApHeaderExportBatchesByCompanyTransmissionVendorId( $intCompanyTransmissionVendorId, $intCid, $objDatabase ) {
		$strSql = 'SELECT 
						count ( * )
					FROM
					    scheduled_ap_header_export_batches
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_transmission_vendor_id = ' . ( int ) $intCompanyTransmissionVendorId;
		$arrintResponse = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
		return 0;
	}

	public static function fetchCustomScheduledApHeaderExportBatchesByKeyedSchedulerIds( $arrintKeyedSchedulerIds, $objDatabase ) {
		if( false == valArr( $arrintKeyedSchedulerIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
                        *
                    FROM
                        scheduled_ap_header_export_batches saheb
                    WHERE
                         ( cid, id ) IN ( ' . sqlIntMultiImplode( $arrintKeyedSchedulerIds ) . ' )
                         AND deleted_on IS NULL
                         AND deleted_by IS NULL';
		return self::fetchScheduledApHeaderExportBatches( $strSql, $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchesByBankAccountIdByExportTypeIdByCid( $intBankAccountId, $intExportTypeId, $intCid, $objDatabase, $arrintPropertyIds ) {
		$strSql = ' SELECT 
					spheb.id as scheduled_ap_header_export_batch_id,
					sphebp.property_id
				FROM 
					scheduled_ap_header_export_batches spheb
				JOIN 
					scheduled_ap_header_export_batch_properties sphebp ON ( sphebp.cid = spheb.cid AND sphebp.scheduled_ap_header_export_batch_id = spheb.id )
				WHERE 
					spheb.cid = ' . ( int ) $intCid . '
					AND ' . ( int ) $intBankAccountId . ' = ANY( spheb.bank_account_ids )
					AND spheb.ap_header_export_batch_type_id = ' . ( int ) $intExportTypeId . '
					AND spheb.deleted_by IS NULL
					AND spheb.deleted_on IS NULL';
		if( false != valArr( $arrintPropertyIds ) ) {
			$strSql .= ' AND sphebp.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchesByBankAccountIdsByPropertyIds( $arrintBankAccountIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintBankAccountIds ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT sq.id
						FROM (
							SELECT
								saheb.id as id,
								array_agg(sahebp.property_id) over(partition by saheb.ap_header_export_batch_type_id, saheb.id) as property_ids,
								saheb.bank_account_ids
							FROM
								scheduled_ap_header_export_batches AS saheb
							JOIN
								scheduled_ap_header_export_batch_properties AS sahebp ON (saheb.id = sahebp.scheduled_ap_header_export_batch_id AND saheb.cid = sahebp.cid)
							WHERE
								saheb.cid = ' . ( int ) $intCid . '
								AND saheb.deleted_by IS NULL
								AND ( saheb.scheduled_end_on >= CURRENT_DATE OR saheb.scheduled_end_on IS NULL AND array_length(saheb.bank_account_ids,1 ) IS NOT NULL ) ) AS sq
						WHERE
							sq.property_ids = ARRAY [ ' . implode( ',', $arrintPropertyIds ) . ' ]
							AND sq.bank_account_ids = ARRAY [ ' . implode( ',', $arrintBankAccountIds ) . ' ]
						GROUP BY
							sq.id';
		return fetchData( $strSql, $objDatabase );
	}

}
?>