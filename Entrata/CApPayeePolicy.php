<?php

class CApPayeePolicy extends CBaseApPayeePolicy {

	protected $m_strPolicyType;
	protected $m_fltRequiredLiabilityAmount;

	/**
	 * Get Functions
	 */

	public function getPolicyType() {
		return $this->m_strPolicyType;
	}

	public function getRequiredLiabilityAmount() {
		return $this->m_fltRequiredLiabilityAmount;
	}

	/**
	 * Set Functions
	 */

	public function setPolicyType( $strPolicyType ) {
		return $this->m_strPolicyType = $strPolicyType;
	}

	public function setRequiredLiabilityAmount( $fltRequiredLiabilityAmount ) {
		$this->m_fltRequiredLiabilityAmount = CStrings::strToFloatDef( $fltRequiredLiabilityAmount, NULL, false, 2 );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['policy_type'] ) ) $this->setPolicyType( $arrmixValues['policy_type'] );
		if( true == isset( $arrmixValues['required_liability_amount'] ) ) $this->setRequiredLiabilityAmount( $arrmixValues['required_liability_amount'] );

		return;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchApPayeePolicyTypeById( $objClientDatabase ) {
		return CApPayeePolicyTypes::fetchApPayeePolicyTypeByIdByCid( $this->getApPayeePolicyTypeId(), $this->getCid(), $objClientDatabase );
	}

	/**
	 * Validate Functions
	 */

	public function valInsuranceProvider( $boolIsMandatory = false ) {
		$boolIsValid = true;

		if( true == $boolIsMandatory && true == is_null( $this->getInsuranceProvider() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'insurance_provider', 'Insurance Provider is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPolicyNumber( $boolIsMandatory = false ) {
		$boolIsValid = true;

		if( true == $boolIsMandatory && true == is_null( $this->getPolicyNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'policy_number', 'Policy Number is required' ) );
		}

		return $boolIsValid;
	}

	public function valApPayeePolicyTypeId( $boolIsMandatory = false ) {
		$boolIsValid = true;

		if( true == $boolIsMandatory && true == is_null( $this->getApPayeePolicyTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_policy_type_id', 'Type is required' ) );
		}
		return $boolIsValid;
	}

	public function valVpPolicyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLiabilityLimit( $boolIsMandatory = false, $objClientDatabase ) {

		if( true == $boolIsMandatory && true == is_null( $this->getLiabilityLimit() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'liability_limit', 'Liability amount is required' ) );
			return false;
		}

		$objApPayeePolicyType = $this->fetchApPayeePolicyTypeById( $objClientDatabase );

		if( true == valObj( $objApPayeePolicyType, 'CApPayeePolicyType' ) && ( 1 == $objApPayeePolicyType->getIsValidateInsuranceDate() || 1 == $objApPayeePolicyType->getIsRequired() )
				&& true == is_numeric( $this->getLiabilityLimit() ) && $this->getLiabilityLimit() < $objApPayeePolicyType->getRequiredLiabilityAmount() ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'liability_limit', 'Liability amount should not less than required liability amount ( ' . $objApPayeePolicyType->getRequiredLiabilityAmount() . ' ).' ) );
			return false;
		}

		return true;
	}

	public function valInsuranceEndDate( $objClientDatabase ) {

		if( false == preg_match( '#^(0[1-9]|1[0-2])/(0[1-9]|[1-2][0-9]|3[0-1])/[0-9]{4}$#', $this->getInsuranceEndDate() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'insurance_end_date', 'Expiration date is invalid.' ) );
			return false;
		}

		if( false == is_null( $this->getInsuranceEndDate() ) ) {

			$intMonth	= ( int ) \Psi\CStringService::singleton()->substr( $this->getInsuranceEndDate(), 0, 2 );
			$intDay		= ( int ) \Psi\CStringService::singleton()->substr( $this->getInsuranceEndDate(), 3, 2 );
			$intYear	= ( int ) \Psi\CStringService::singleton()->substr( $this->getInsuranceEndDate(), 6, 4 );

			if( false == checkdate( $intMonth, $intDay, $intYear ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'insurance_end_date', 'Expiration date is invalid.' ) );
				return false;
			}
		}

		$objApPayeePolicyType = $this->fetchApPayeePolicyTypeById( $objClientDatabase );

		if( true == valObj( $objApPayeePolicyType, 'CApPayeePolicyType' ) && ( 1 == $objApPayeePolicyType->getIsValidateInsuranceDate() || 1 == $objApPayeePolicyType->getIsRequired() )
				&& ( int ) strtotime( $this->getInsuranceEndDate() ) < ( int ) strtotime( date( 'Y-m-d' ) ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'insurance_end_date', 'Expiration date should not less than today\'s date.' ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valInsuranceProvider( true );
				$boolIsValid &= $this->valPolicyNumber( true );
				$boolIsValid &= $this->valLiabilityLimit( true, $objClientDatabase );
				$boolIsValid &= $this->valApPayeePolicyTypeId( true );
				$boolIsValid &= $this->valInsuranceEndDate( $objClientDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valInsuranceProvider( true );
				$boolIsValid &= $this->valPolicyNumber( true );
				$boolIsValid &= $this->valLiabilityLimit( true, $objClientDatabase );
				$boolIsValid &= $this->valApPayeePolicyTypeId( true );
				$boolIsValid &= $this->valInsuranceEndDate( $objClientDatabase );
				break;

			case 'vendor_insert':
				$boolIsValid &= $this->valInsuranceProvider( false );
				$boolIsValid &= $this->valPolicyNumber( false );
				$boolIsValid &= $this->valLiabilityLimit( false );
				$boolIsValid &= $this->valApPayeePolicyTypeId( false );
				$boolIsValid &= $this->valInsuranceEndDate( $objClientDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>