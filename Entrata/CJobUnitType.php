<?php

class CJobUnitType extends CBaseJobUnitType {

	protected $m_intTotalUnitCount;
	protected $m_intTotalAssignedUnits;

	protected $m_strUnitTypeName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Get Functions
	 */

	public function getUnitTypeName() {
		return $this->m_strUnitTypeName;
	}

	public function getTotalUnitCout() {
		return $this->m_intTotalUnitCount;
	}

	public function getTotalAssignedUnits() {
		return $this->m_intTotalAssignedUnits;
	}

	/**
	 * Set Functions
	 */

	public function setUnitTypeName( $strUnitTypeName ) {
		$this->m_strUnitTypeName = CStrings::strTrimDef( $strUnitTypeName, 50, NULL, true );
	}

	public function setTotalUnitCout( $intTotalUnitCount ) {
		$this->m_intTotalUnitCount = $intTotalUnitCount;
	}

	public function setTotalAssignedUnits( $intTotalAssignedUnits ) {
		$this->m_intTotalAssignedUnits = $intTotalAssignedUnits;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['unit_type_name'] ) ) $this->setUnitTypeName( $arrmixValues['unit_type_name'] );
		if( true == isset( $arrmixValues['total_unit_count'] ) ) $this->setTotalUnitCout( $arrmixValues['total_unit_count'] );
		if( true == isset( $arrmixValues['total_assigned_units'] ) ) $this->setTotalAssignedUnits( $arrmixValues['total_assigned_units'] );
	}

}
?>