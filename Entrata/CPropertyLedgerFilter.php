<?php

class CPropertyLedgerFilter extends CBasePropertyLedgerFilter {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLedgerFilterId( $objClientDatabase ) {
		$boolIsValid = true;
		if( true == is_null( $this->getLedgerFilterId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Please select ledger type.' ) ) );
		} else {
			$intPropertyLedgerFilterCount = \Psi\Eos\Entrata\CPropertyLedgerFilters::createService()->fetchPropertyLedgerFilterCountByLedgerFilterIdByPropertyIdByCid( $this->getId(), $this->getLedgerFilterId(), $this->getPropertyId(), $this->getCid(), $objClientDatabase );
			if( 0 < $intPropertyLedgerFilterCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Ledger type already associated.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valAssociation( $arrmixArCodes = [], $boolValAssociation = true ) {

		$strTransactionChargeCodeError	= '';
		$strOtherChargeCodeErrMsg		= '';
		$strReplaceChangeOrEnable		= ( true == $boolValAssociation ) ? __( 'changed' ) : __( 'disabled' );

		$arrstrOtherChargeCodes = [];

		foreach( $arrmixArCodes as $arrmixArCode ) {
			if( $arrmixArCode['row_count'] > 0 && $arrmixArCode['single_reference'] == 'transaction' ) {
				$strReferenceTransactionReplace	= ( $arrmixArCode['row_count'] > 1 ) ? __( 'transactions' ) : __( 'transaction' );
				$strTransactionChargeCodeError	= __( 'This Ledger Filter cannot be {%s, 0}. It is being used by {%s, 1} at this property<br/><br/>', [ $strReplaceChangeOrEnable, $strReferenceTransactionReplace ] );
			}
			if( $arrmixArCode['row_count'] > 0 && $arrmixArCode['single_reference'] != 'transaction' ) {
				$arrstrOtherChargeCodes[] = ( $arrmixArCode['row_count'] > 1 ) ? $arrmixArCode['multiple_reference'] : $arrmixArCode['single_reference'];
			}
		}

		if( true == valArr( $arrstrOtherChargeCodes ) && false == valStr( $strTransactionChargeCodeError ) ) {
			$strOtherChargeCodeErrMsg = __( 'This Ledger Filter cannot be {%s, 0} because it is referenced in the following areas for this property. Update these areas to different charge codes to {%s, 1} the ledger: {%s, 2}', [ $strReplaceChangeOrEnable, $strReplaceChangeOrEnable, implode( ', ', $arrstrOtherChargeCodes ) ] );
		}

		if( false == $this->getIsPublished() && ( true == valStr( $strTransactionChargeCodeError ) || true == valStr( $strOtherChargeCodeErrMsg ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', $strTransactionChargeCodeError . $strOtherChargeCodeErrMsg ) );
			return false;
		}

		if( true == $this->getIsPublished() && true == $boolValAssociation && ( true == valStr( $strTransactionChargeCodeError ) || true == valStr( $strOtherChargeCodeErrMsg ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', $strTransactionChargeCodeError . $strOtherChargeCodeErrMsg ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction, $objClientDatabase, $arrmixArCodes, $boolValAssociation = true ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valLedgerFilterId( $objClientDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valLedgerFilterId( $objClientDatabase );
				$boolIsValid &= $this->valAssociation( $arrmixArCodes, $boolValAssociation );
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>