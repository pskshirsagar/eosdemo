<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyNotifications
 * Do not add any new functions to this class.
 */

class CCompanyNotifications extends CBaseCompanyNotifications {

	public static function fetchCompanyNotificationsByCidByPropertyNotificationGroupIds( $intCid, $arrintPropertyNotificationGroupIds, $objClientDatabase ) {
		if( false == valId( $intCid ) || false == valArr( $arrintPropertyNotificationGroupIds ) ) return NULL;

		$strSql = 'SELECT 
						* 
					FROM 
						company_notifications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_notification_group_id IN ( ' . implode( ',', $arrintPropertyNotificationGroupIds ) . ' )
					ORDER BY
						id';

		return self::fetchCompanyNotifications( $strSql, $objClientDatabase );
	}

	public static function fetchActiveCompanyNotificationsByCidByByPropertyNotificationGroupIds( $intCid, $arrintPropertyNotificationGroupIds, $objClientDatabase ) {
		if( false == valId( $intCid ) || false == valArr( $arrintPropertyNotificationGroupIds ) ) return NULL;

		$strSql = 'SELECT 
						* 
					FROM 
						company_notifications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_notification_group_id IN ( ' . implode( ',', $arrintPropertyNotificationGroupIds ) . ' )
						AND ( start_datetime IS NULL OR date_trunc( \'day\', start_datetime ) <= CURRENT_DATE )
						AND ( end_datetime IS NULL OR date_trunc( \'day\', end_datetime ) >= CURRENT_DATE )
					ORDER BY
						id';

		return self::fetchCompanyNotifications( $strSql, $objClientDatabase );
	}

}
?>