<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportGroups
 * Do not add any new functions to this class.
 */

class CReportGroups extends CBaseReportGroups {

	public static function fetchSimpleReportGroupsByParentModuleIdByCid( $intParentModuleId, $boolIgnoreUserReportGroup, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						rg.id,
						util_get_translated( \'name\', rg.name, rg.details ) AS name,
						util_get_translated( \'description\', rg.description, rg.details ) AS description,
						rg.default_report_group_id,
						rg.position_number,
						rg.module_id
					FROM
						report_groups rg
					WHERE
						rg.cid = ' . ( int ) $intCid . '
						AND rg.deleted_on IS NULL
						AND rg.parent_module_id = ' . ( int ) $intParentModuleId . '
						' . ( ( $boolIgnoreUserReportGroup ) ? ' AND rg.company_user_id IS NULL ' : '' ) . '
					ORDER BY
						rg.position_number';

		return rekeyArray( 'id', fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchReportGroupByParentModuleIdByCompanyUserIdByCid( $intParentModuleId, $intCompanyUserId, $intCid, $objDatabase, $arrintModuleIds = [] ) {

		$strModuleIdCondition = ( true == is_null( $intCompanyUserId ) && true == valArr( $arrintModuleIds ) ) ? ' AND rg.module_id IN ( ' . implode( $arrintModuleIds, ',' ) . ' )' : '';

		$strSql = 'SELECT
						rg.id,
						util_get_translated( \'name\', rg.name, rg.details ) AS name
					FROM
						report_groups rg
					WHERE
						rg.cid = ' . ( int ) $intCid . '
						AND rg.parent_module_id = ' . ( int ) $intParentModuleId . '
						' . ( false == is_null( $intCompanyUserId ) ? 'AND rg.company_user_id = ' . ( int ) $intCompanyUserId : '' ) . '
						AND rg.is_published = 1
						AND rg.deleted_by IS NULL
						' . $strModuleIdCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportGroupsByTabNameByCompanyUserIdByCid( $strTabName, $intCompanyUserId, $intCid, $objDatabase ) {

		if( CModule::FAVORITE_REPORTS == $strTabName ) {
			$strTabCondition = 'AND rg.company_user_id = ' . ( int ) $intCompanyUserId;
		} else {
			$strTabCondition = 'AND m.name = \'' . $strTabName . '\'';
		}

		$strSql = '
			SELECT
				rg.*
			FROM
				report_groups rg
				JOIN modules m ON m.id = rg.parent_module_id
			WHERE
				rg.cid = ' . ( int ) $intCid . '
				AND rg.is_published = 1
				' . $strTabCondition . '
			ORDER BY
				rg.name';

		return self::fetchReportGroups( $strSql, $objDatabase );
	}

	public static function fetchSimpleReportGroupByIdByCid( $intReportGroupId, $intCid, $objDatabase ) {
		return self::fetchReportGroup( sprintf( 'SELECT * FROM report_groups WHERE cid = %d AND id = %d', ( int ) $intCid, ( int ) $intReportGroupId ), $objDatabase );
	}

	public static function fetchReportGroupsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						rg.id,
						util_get_translated( \'name\', rg.name, rg.details ) AS report_group_name,
						rg.company_user_id AS user_id,
						rg.module_id,
						rg.parent_module_id,
						ri.id AS report_instance_id,
						util_get_translated( \'name\', ri.name, ri.details ) AS name,
						r.id AS report_id,
						ri.report_filter_id,
						rv.definition ->> \'report_adhoc_type_id\',
						rv.definition ->> \'report_xml\',
						r.name AS report_name,
						m.url,
						util_get_translated( \'title\', m.title, m.details ) AS title, 
						rv.id AS report_version_id,
						rv.major,
						rv.minor
					FROM
						report_groups rg
						JOIN report_instances ri ON ( ri.cid = rg.cid AND ri.report_group_id = rg.id )
						JOIN report_versions rv ON ( rv.cid = ri.cid AND rv.id = ri.report_version_id )
						JOIN reports r ON ( r.cid = ri.cid AND r.id = ri.report_id )
						JOIN modules m ON ( rg.cid = ' . ( int ) $intCid . ' AND rg.parent_module_id = m.id AND m.is_published = 1 )
					WHERE
						rg.cid = ' . ( int ) $intCid . '
						AND rg.report_group_type_id = ' . CReportGroupType::MY_REPORTS . '
						AND rg.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND m.name = \'' . CModule::FAVORITE_REPORTS . '\'
						AND rg.deleted_on IS NULL
						AND ri.deleted_on IS NULL
						AND COALESCE((rv.definition ->> \'is_published\')::BOOLEAN, TRUE) = TRUE
						AND COALESCE( r.is_published, TRUE ) = TRUE
						AND COALESCE( rv.expiration, now() ) >= now()
						AND COALESCE( util.util_to_bool( ri.details->>\'is_quick_link\' ), FALSE ) = TRUE
					ORDER BY
						ri.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportCountByReportNameByReportGroupIdByCid( $strReportName, $intReportGroupId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						count( m.id ) AS report_count
					FROM
						report_groups rg
						JOIN report_instances ri ON ( rg.cid = ri.cid AND rg.id = ri.container_id )
						JOIN modules m ON ( m.cid = ri.cid AND ri.module_id = m.id )
					WHERE
						rg.cid = ' . ( int ) $intCid . '
						AND rg.id = ' . ( int ) $intReportGroupId . '
						AND LOWER( m.title ) = \'' . \Psi\CStringService::singleton()->strtolower( $strReportName ) . '\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNextAvailablePositionNumberByReportTabModuleIdByManamgentCompanyId( $intParentModuleId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						max(rg.position_number) + 1 as position_number
					FROM
						report_groups rg
					WHERE
						rg.cid = ' . ( int ) $intCid . '
						AND rg.parent_module_id = ' . ( int ) $intParentModuleId;

		$arrintData = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrintData ) ) ? ( int ) $arrintData[0]['position_number'] : 1;
	}

	public static function fetchCustomUserReportGroupByUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						id,
						module_id,
						util_get_translated( \'name\', rg.name, rg.details ) AS name
					FROM
						report_groups rg
					WHERE
						rg.cid = ' . ( int ) $intCid . '
						AND rg.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND rg.report_group_type_id = ' . CReportGroupType::MY_REPORTS . '
						AND deleted_on IS NULL
					ORDER BY
						id,
						name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportGroupByReportGroupNameByParentModuleIdByUserIdByCid( $strReportGroupName, $intParentModuleId, $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						id,
						name,
						module_id,
						cid,
						parent_module_id,
						details
					FROM
						report_groups rg
					WHERE
						rg.cid = ' . ( int ) $intCid . '
						AND rg.name = \'' . $strReportGroupName . '\'
						AND rg.parent_Module_id = ' . ( int ) $intParentModuleId . '
						AND deleted_on IS NULL
						' . ( false == is_null( $intCompanyUserId ) ? 'AND rg.company_user_id = ' . ( int ) $intCompanyUserId : 'AND rg.company_user_id IS NULL' ) . '
					ORDER BY
						id,
						name';

		return self::fetchReportGroup( $strSql, $objDatabase );
	}

	/**
	 * @param $arrintIds
	 * @param $intCid
	 * @param $objDatabase
	 * @return CReportGroup[]|null
	 */
	public static function fetchReportGroupsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				*
			FROM
				report_groups
			WHERE
				cid = ' . ( int ) $intCid . '
				AND id IN ( ' . implode( $arrintIds, ',' ) . ' )';

		return self::fetchReportGroups( $strSql, $objDatabase );
	}

	public static function fetchReportPacketsByParentModuleIdByCid( $intParentModuleId, $intCid, $objDatabase, $boolIsPSIUser = false ) {

		$strSql = '	SELECT
						util_get_translated( \'name\', rg.name, rg.details ) AS packet_name,
						rg.id AS packet_id,
						rg.module_id AS packet_module_id,
						rg.is_internal,
						ri.report_id,
						m.name AS module_name,
						r.name,
						rf.id AS report_filter_id,
						rs.id AS report_schedule_id,
						rs.download_options,
						ri.id AS report_instance_id,
						m.url,
						CASE
							WHEN rst.name IS NOT NULL THEN util_get_translated( \'name\', rst.name, rst.details )
							ELSE \'' . __( 'Manual' ) . '\'
						END AS packet_type,
						CASE
							WHEN rf.name IS NOT NULL THEN util_get_translated( \'name\', ri.name, ri.details ) || \'-\' || rf.name
							ELSE util_get_translated( \'name\', ri.name, ri.details )
						END AS report_name,
						rf.filters AS filter_set,
						rst.id AS packet_type_id,
						rg.created_by
					FROM
						report_groups rg
						JOIN report_instances ri ON ( rg.cid = ri.cid AND rg.id = ri.report_group_id )
						JOIN reports r ON ( r.cid = ri.cid AND r.id = ri.report_id )
						JOIN modules m ON ( m.id = ri.module_id )
						LEFT JOIN report_filters rf ON ( ri.cid = rf.cid AND ri.report_id  = rf.report_id AND rf.id = ri.report_filter_id AND rf.deleted_by IS NULL )
						LEFT JOIN report_schedules rs ON ( rg.cid = rs.cid AND rg.id = rs.report_group_id AND rs.deleted_by IS NULL )
						LEFT JOIN report_schedule_types rst ON ( rs.report_schedule_type_id = rst.id )
						JOIN default_reports dr ON ( dr.id = r.default_report_id )
					WHERE
						ri.cid = ' . ( int ) $intCid . '
						AND rg.report_group_type_id = ' . ( int ) CReportGroupType::PACKET . '
						AND rg.parent_module_id = ' . ( int ) $intParentModuleId . '
						AND rg.deleted_by IS NULL
						AND rg.deleted_on IS NULL
						AND ri.deleted_by IS NULL
						AND ri.deleted_on IS NULL ';

		if( false == $boolIsPSIUser ) {
			$strSql .= 'AND rg.is_internal = false ';
		}

		$strSql .= 'ORDER BY
						rg.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportPacketWithScheduleDetailsByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						ri.id AS report_instance_id,
						ri.report_id,
						rs.id AS report_schedule_id,
						rf.id AS report_filter_id,
						rg.id AS packet_id,
						rg.is_internal,
						rv.id AS report_version_id,
						rv.major::TEXT || \'.\' || rv.minor::TEXT AS version,
						util_get_translated( \'title\', r.title, r.details ) AS report_title,
						util_get_translated( \'name\', ri.name, ri.details ) AS report_name,
						util_get_translated( \'name\', rg.name, rg.details ) AS packet_name,
						rf.name AS report_filter_name,
						CASE
							WHEN
								rs.id IS NULL
							THEN
								' . CReportScheduleType::MANUAL . '
							ELSE
								rs.report_schedule_type_id
						END as packet_type_id,
						rs.download_options,
						st.start_on
					FROM
						report_groups rg
						JOIN report_instances ri ON ( ri.cid = rg.cid AND ri.report_group_id = rg.id AND rg.report_group_type_id = ' . CReportGroupType::PACKET . ' )
						JOIN reports r ON ( r.cid = ri.cid AND r.id = ri.report_id )
						JOIN report_versions rv ON ( rv.cid = ri.cid AND rv.id = ri.report_version_id )
						JOIN report_filters rf ON ( rf.cid = ri.cid AND rf.report_id = ri.report_id AND rf.id = ri.report_filter_id AND rf.deleted_by IS NULL )
						LEFT JOIN report_schedules rs ON ( rs.cid = ri.cid AND rs.report_group_id = rg.id AND rs.deleted_by IS NULL )
						LEFT JOIN scheduled_tasks st ON ( st.cid = rs.cid AND st.id = rs.scheduled_task_id )
					WHERE
						ri.cid = ' . ( int ) $intCid . '
						AND rg.id = ' . ( int ) $intId . '
						AND ( strpos ( rs.download_options::TEXT, \'"is_internal": true\' )::INTEGER = 0 )
						AND ri.deleted_by IS NULL
						AND st.deleted_by IS NULL
						AND ri.deleted_on IS NULL
						AND st.deleted_on IS NULL 
					ORDER BY
						ri.id';
		// Adding order by clause so that this listing should show instances in order in which user added those.
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportGroupsByCompanyUserIdsByGroupTypeIdByCid( $arrintCompanyUserIds, $intCid, $objDatabase, $boolIsForCount = false ) {
		if( false == valArr( $arrintCompanyUserIds ) ) return [];

		if( true == $boolIsForCount ) {
			$strSelectFieldSql = ', COUNT( id ) AS group_count ';
			$strGroupByFieldSql = '';
		} else {
			$strGroupByFieldSql = $strSelectFieldSql = ', id, name ';
		}

		$strSql = ' SELECT 
						company_user_id
						' . $strSelectFieldSql . '
					FROM
						report_groups
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_user_id IN ( ' . implode( ', ', $arrintCompanyUserIds ) . ' )
						AND report_group_type_id = ' . ( int ) CReportGroupType::PACKET . '
						AND deleted_by IS NULL
					GROUP BY
						company_user_id'
						. $strGroupByFieldSql;

		return fetchData( $strSql, $objDatabase );
	}

}

?>