<?php

class CSubsidyEthnicityType extends CBaseSubsidyEthnicityType {
	const DECLINED_TO_REPORT 	= 1;
	const HISPANIC 				= 2;
	const NON_HISPANIC 			= 3;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getSubsidizeEthnicityTypeIdByName( $strSubsidyEthnicityType ) {

		switch( \Psi\CStringService::singleton()->strtolower( $strSubsidyEthnicityType ) ) {
			case 'declined to report':
				$intSubsidyEthnicityTypeId = self::DECLINED_TO_REPORT;
				break;

			case 'hispanic':
				$intSubsidyEthnicityTypeId = self::HISPANIC;
				break;

			case 'non-hispanic':
			case 'non hispanic':
				$intSubsidyEthnicityTypeId = self::NON_HISPANIC;
				break;

			default:
				$intSubsidyEthnicityTypeId = NULL;
				break;
		}

		return $intSubsidyEthnicityTypeId;
	}

}
?>