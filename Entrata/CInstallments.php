<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInstallments
 * Do not add any new functions to this class.
 */

class CInstallments extends CBaseInstallments {

	public static function fetchInstallmentsByParametersListByInstallmentPlanIdByCid( $strSelectParameters, $intInstallmentPlanId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
	   					' . $strSelectParameters . '
				   FROM
					    installments i
			       WHERE
				    	i.cid = ' . ( int ) $intCid . '
				    	AND i.installment_plan_id = ' . ( int ) $intInstallmentPlanId . '
				    	AND i.deleted_by IS NULL 
				   ORDER BY
				 	  	i.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchInstallmentsByInstallmentPlanIdsByCid( $arrintInstallmentPlanIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintInstallmentPlanIds ) ) return NULL;

		$strSql = 'SELECT
	   					i.*
				   FROM
					    installments i
			       WHERE
				    	i.cid = ' . ( int ) $intCid . '
				    	AND i.installment_plan_id IN (' . implode( ', ', $arrintInstallmentPlanIds ) . ')
				    	AND i.deleted_by IS NULL 
				   ORDER BY
						i.id';

		return self::fetchInstallments( $strSql, $objDatabase );
	}

	public static function fetchInstallmentsByIdsByCid( $arrintInstallmentIds, $intCId, $objDatabase, $strOrderBy = 'i.id' ) {

		if( false == valArr( $arrintInstallmentIds ) ) return NULL;

		$strSql = 'SELECT
	   					*
				   FROM
					    installments i
			       WHERE
				    	i.cid = ' . ( int ) $intCId . '
				    	AND i.id IN (' . implode( ', ', $arrintInstallmentIds ) . ')
				    	AND i.deleted_by IS NULL 
				   ORDER BY ' . $strOrderBy;

		return self::fetchInstallments( $strSql, $objDatabase );
	}

	public static function fetchMinimumInstallmentStartDateByInstallmentPlanIdByCId( $intInstallmentPlanId, $intCId, $objDatabase ) {

		if( false == is_numeric( $intInstallmentPlanId ) ) return NULL;

		$strSql = 'SELECT
	   					MIN( i.charge_start_date ) as charge_start_date
				   FROM
					    installments i
						JOIN installment_plans ip ON ( i.cid = ip.cid AND i.installment_plan_id = ip.id )
			       WHERE
				    	ip.cid = ' . ( int ) $intCId . '
				    	AND ip.id = ' . ( int ) $intInstallmentPlanId . '
				    	AND i.deleted_by IS NULL 
				   GROUP BY
				 	  	i.installment_plan_id';

		return self::fetchColumn( $strSql, 'charge_start_date', $objDatabase );
	}

	public static function fetchLastInstallmentEndDateByLeaseIntervalIdByCId( $intLeaseIntervalId, $intCId, $objDatabase ) {

		if( false == is_numeric( $intLeaseIntervalId ) ) return NULL;

		$strSql = 'SELECT
	   					MAX( i.charge_end_date ) as charge_end_date
				   FROM
					    installments i
					    JOIN lease_interval_installment_plans liip ON ( i.cid = liip.cid AND i.installment_plan_id = liip.installment_plan_id )
			       WHERE
				    	liip.cid = ' . ( int ) $intCId . '
				    	AND liip.lease_interval_id = ' . ( int ) $intLeaseIntervalId;

		return self::fetchColumn( $strSql, 'charge_end_date', $objDatabase );
	}

	public static function fetchInstallmentsByScheduledChargeIdsByPorpertyIdByCid( $arrintScheduledChargeIds, $intPropertyId, $intCId, $objDatabase ) {

		if( false == valArr( $arrintScheduledChargeIds ) ) return NULL;

		$strSql = 'SELECT
   					 	sc.id as scheduled_charge_id,
    					i.id,
    					i.name
				   FROM
    					installments i
    					JOIN scheduled_charges sc ON ( i.cid = sc.cid AND i.id = sc.installment_id )
				   WHERE
    					i.cid = ' . ( int ) $intCId . '
    					AND sc.id IN ( ' . implode( $arrintScheduledChargeIds, ',' ) . ' )
    					AND sc.property_id = ' . ( int ) $intPropertyId . '
						AND i.post_all_at_once = TRUE';

		$arrmixTempData = fetchData( $strSql, $objDatabase );

		$arrmixData = [];

		if( true == valArr( $arrmixTempData ) ) {

			foreach( $arrmixTempData as $intData => $arrmixValue ) {
				$arrmixData[$arrmixValue['scheduled_charge_id']]['id'] = $arrmixValue['id'];
				$arrmixData[$arrmixValue['scheduled_charge_id']]['name'] = $arrmixValue['name'];
			}
		}

		return $arrmixData;
	}

	public static function fetchCurrentInstallmentsByInstallmentPlanIdByCid( $intInstallmentPlanId, $intCid, $intLeaseId, $objDatabase ) {

		if( false == is_numeric( $intInstallmentPlanId ) ) return NULL;

		$strSql = ' SELECT
					    i.*
					FROM
					    installments i
					    LEFT JOIN scheduled_charges sc ON ( sc.installment_plan_id = i.installment_plan_id AND sc.cid = i.cid )
					WHERE
					    i.cid = ' . ( int ) $intCid . '
					    AND i.installment_plan_id = ' . ( int ) $intInstallmentPlanId . '
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
					ORDER BY
					    i.id';

		return self::fetchInstallments( $strSql, $objDatabase );
	}

	public static function fetchLastInstallmentsByLeaseIntervalIdByCId( $intLeaseIntervalId, $intCId, $objDatabase ) {

		if( false == is_numeric( $intLeaseIntervalId ) ) return NULL;

		$strSql = 'SELECT
	   					i.*
				   FROM
					    installments i
					    JOIN lease_interval_installment_plans liip ON ( i.cid = liip.cid AND i.installment_plan_id = liip.installment_plan_id )
			       WHERE
				    	liip.cid = ' . ( int ) $intCId . '
				    	AND liip.lease_interval_id = ' . ( int ) $intLeaseIntervalId;

		return parent::fetchObjects( $strSql, CInstallment::class, $objDatabase );
	}

}
?>
