<?php

class CLeasingGoal extends CBaseLeasingGoal {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Goal Name is required' ), NULL ) );
			$boolIsValid &= false;
		} elseif( true == valId( CLeasingGoals::fetchLeasingGoalByNameByPropertyIdByCid( $this->getName(), $this->getPropertyId(), $this->getCid(), $this->m_objDatabase, $this->getId() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Entered Goal name \'{%s, 0}\' is already exists.', [ $this->getName() ] ), NULL ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valPreLeaseStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getPreLeaseStartDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pre_lease_start_date', __( 'Pre-Lease Start date is required.' ), NULL ) );
			$boolIsValid &= false;
		} elseif( false == is_numeric( strtotime( $this->getPreLeaseStartDate() ) ) || false == CValidation::checkDateFormat( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getPreLeaseStartDate() ] ), $boolFormat = true ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pre_lease_start_date', __( 'Valid Pre-Lease Start date is required.' ), NULL ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valPreLeaseEndDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getPreLeaseEndDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pre_lease_end_date', __( 'Pre-Lease End date is required.' ), NULL ) );
			$boolIsValid &= false;
		} elseif( false == is_numeric( strtotime( $this->getPreLeaseEndDate() ) ) || false == CValidation::checkDateFormat( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getPreLeaseEndDate() ] ), $boolFormat = true ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pre_lease_end_date', __( 'Valid Pre-Lease End date is required.' ), NULL ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valOccupiedUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validateDates() {
		$boolIsValid = true;

		if( strtotime( $this->getPreLeaseEndDate() ) <= strtotime( $this->getPreLeaseStartDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Pre-Lease start date should be less than Pre-Lease end date.' ), NULL ) );
			return false;
		}

		return $boolIsValid;
	}

	public function createLeasingGoalDistribution( $strDate = NULL, $intCount = 0, $boolIsRenewal = false ) {
		$objLeasingGoalDistribution = new CLeasingGoalDistribution();
		$objLeasingGoalDistribution->setCid( $this->getCid() );
		$objLeasingGoalDistribution->setLeasingGoalId( $this->getId() );
		$objLeasingGoalDistribution->setGoalDate( $strDate );
		$objLeasingGoalDistribution->setGoalCount( $intCount );
		$objLeasingGoalDistribution->setIsRenewal( $boolIsRenewal );

		return $objLeasingGoalDistribution;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valPreLeaseStartDate();
				$boolIsValid &= $this->valPreLeaseEndDate();
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->validateDates();
				}
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>