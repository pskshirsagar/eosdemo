<?php

class CCampaignTarget extends CBaseCampaignTarget {

	protected $m_strFromEmailAddress;
	protected $m_strToEmailAddress;
	protected $m_strCcEmailAddress;
	protected $m_strEmailSubject;
	protected $m_strEmailContent;
	protected $m_strPropertyName;
	protected $m_intLeadSourceId;
	protected $m_strLeadSourceName;

	/**
	 * Get Functions
	 */

	public function getFromEmailAddress() {
		return $this->m_strFromEmailAddress;
	}

	public function getToEmailAddress() {
		return $this->m_strToEmailAddress;
	}

	public function getCcEmailAddress() {
		return $this->m_strCcEmailAddress;
	}

	public function getEmailSubject() {
		return $this->m_strEmailSubject;
	}

	public function getEmailContent() {
		return $this->m_strEmailContent;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getLeadSourceId() {
		return $this->m_intLeadSourceId;
	}

	public function getLeadSourceName() {
		return $this->m_strLeadSourceName;
	}

	/**
	 * Set Functions
	 */

	public function setFromEmailAddress( $strFromEmailAddress ) {
		$this->m_strFromEmailAddress = $strFromEmailAddress;
	}

	public function setToEmailAddress( $strToEmailAddress ) {
		$this->m_strToEmailAddress = $strToEmailAddress;
	}

	public function setCcEmailAddress( $strCcEmailAddress ) {
		$this->m_strCcEmailAddress = $strCcEmailAddress;
	}

	public function setEmailSubject( $strEmailSubject ) {
		$this->m_strEmailSubject = $strEmailSubject;
	}

	public function setEmailContent( $strEmailContent ) {
		$this->m_strEmailContent = $strEmailContent;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setLeadSourceId( $intLeadSourceId ) {
		$this->m_intLeadSourceId = $intLeadSourceId;
	}

	public function setLeadSourceName( $strLeadSourceName ) {
		$this->m_strLeadSourceName = $strLeadSourceName;
	}

	public function setGender( $strGender ) {
		$this->m_strGender = \Psi\CStringService::singleton()->strtoupper( CStrings::strTrimDef( $strGender, 1, NULL, true ) );
	}

	/**
	 * Other Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) )
			$this->setPropertyName( $arrmixValues['property_name'] );

		if( true == isset( $arrmixValues['lead_source_id'] ) )
			$this->setLeadSourceId( $arrmixValues['lead_source_id'] );

		if( true == isset( $arrmixValues['lead_source'] ) )
			$this->setLeadSourceName( $arrmixValues['lead_source'] );
	}

	public function encodeResponseEmail() {

		$arrmixResponsiveEmail = [];

		$arrmixResponsiveEmail['from_email_address'] 	= isset( $this->m_strFromEmailAddress ) ? $this->m_strFromEmailAddress:'';
		$arrmixResponsiveEmail['to_email_address'] 		= isset( $this->m_strToEmailAddress ) ? $this->m_strToEmailAddress:'';
		$arrmixResponsiveEmail['cc_email_address'] 		= isset( $this->m_strCcEmailAddress ) ? $this->m_strCcEmailAddress:'';

		$this->m_strEmailContent = preg_replace( '/<img[^>]+\>/i', '', $this->m_strEmailContent );

		$arrmixResponsiveEmail['email_subject'] 		= isset( $this->m_strEmailSubject ) ? base64_encode( serialize( $this->m_strEmailSubject ) ) : '';
		$arrmixResponsiveEmail['email_content'] 		= isset( $this->m_strEmailContent ) ? base64_encode( serialize( $this->m_strEmailContent ) ): '';

		$strJsonResponsiveEmail = json_encode( $arrmixResponsiveEmail );

		return $strJsonResponsiveEmail;
	}

	public function decodeResponseEmail() {

		$arrmixResponsiveEmail = json_decode( $this->getResponseEmail(), true );

		if( true == valStr( $arrmixResponsiveEmail['from_email_address'] ) ) $this->setFromEmailAddress( $arrmixResponsiveEmail['from_email_address'] );
		if( true == valStr( $arrmixResponsiveEmail['to_email_address'] ) ) $this->setToEmailAddress( $arrmixResponsiveEmail['to_email_address'] );
		if( true == valStr( $arrmixResponsiveEmail['cc_email_address'] ) ) $this->setCcEmailAddress( $arrmixResponsiveEmail['cc_email_address'] );
		if( true == valStr( $arrmixResponsiveEmail['email_subject'] ) ) $this->setEmailSubject( stripslashes( unserialize( base64_decode( $arrmixResponsiveEmail['email_subject'] ) ) ) );
		if( true == valStr( $arrmixResponsiveEmail['email_content'] ) ) $this->setEmailContent( stripslashes( unserialize( base64_decode( $arrmixResponsiveEmail['email_content'] ) ) ) );
	}

	/**
	 * Validate Functions
	 */
public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCampaignId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNamePrefix() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameMiddle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameSuffix() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameMaiden() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumber( $boolIsMandatory = false ) {
		$boolIsValid = true;

		$objPhoneNumber = $this->createPhoneNumberObject( 'getPhoneNumber' );
		if( true == valObj( $objPhoneNumber, \i18n\CPhoneNumber::class ) ) {
			$this->setPhoneNumber( $objPhoneNumber->getValue() );
		}

		if( true == $boolIsMandatory && true == is_null( $this->getPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is required.' ) ) );
		} elseif( false == is_null( $this->getPhoneNumber() ) && ( false == valObj( $objPhoneNumber, \i18n\CPhoneNumber::class ) || false == $objPhoneNumber->isValid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMobileNumber( $boolIsMandatory = false ) {
		$boolIsValid = true;
		$objPhoneNumber = $this->createPhoneNumberObject( 'getMobileNumber' );

		if( true == valObj( $objPhoneNumber, \i18n\CPhoneNumber::class ) ) {
			$this->setMobileNumber( $objPhoneNumber->getValue() );
		}

		if( true == $boolIsMandatory && true == is_null( $this->getMobileNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_number', __( 'Mobile number is required.' ) ) );
		} elseif( false == is_null( $this->getPhoneNumber() ) && ( false == valObj( $objPhoneNumber, \i18n\CPhoneNumber::class ) || false == $objPhoneNumber->isValid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_number', __( 'Mobile number is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valWorkNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFaxNumber( $boolIsMandatory = false ) {
		$boolIsValid = true;
		$objPhoneNumber = $this->createPhoneNumberObject( 'getFaxNumber' );

		if( true == valObj( $objPhoneNumber, \i18n\CPhoneNumber::class ) ) {
			$this->setFaxNumber( $objPhoneNumber->getValue() );
		}

		if( true == $boolIsMandatory && true == is_null( $this->getFaxNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', __( 'Fax number is required.' ) ) );
		} elseif( false == is_null( $this->getFaxNumber() ) && ( ( 10 > \Psi\CStringService::singleton()->strlen( $this->getFaxNumber() ) || 15 < \Psi\CStringService::singleton()->strlen( $this->getFaxNumber() ) ) || false == valObj( $objPhoneNumber, \i18n\CPhoneNumber::class ) || false == $objPhoneNumber->isValid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', __( 'Fax number is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( false == CValidation::validateEmailAddresses( trim( $this->getEmailAddress() ) ) ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valBirthDate() {
		$boolIsValid = true;

		if( false == CValidation::validateDate( trim( $this->getBirthDate() ) ) ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valGender() {
		$boolIsValid 		= true;
		$arrintGenderTypes	= [ 'male', 'female', 'm', 'f' ];

		if( false == in_array( trim( \Psi\CStringService::singleton()->strtolower( $this->getGender() ) ), $arrintGenderTypes ) ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valHeight() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWeight() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEyeColor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHairColor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyIdentificationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIdentificationValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIdentificationExpiration() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResponseEmail() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArchivedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArchivedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_uploaded_data_list':
				if( true == valStr( $this->getBirthDate() ) && false == $this->valBirthDate() ) {
					$boolIsValid &= false;
					$this->setBirthDate( NULL );
				}

				if( true == valStr( $this->getFaxNumber() ) && false == $this->valFaxNumber( false ) ) {
					$boolIsValid &= false;
					$this->setFaxNumber( NULL );
				}

				if( true == valStr( $this->getMobileNumber() ) && false == $this->valMobileNumber( false ) ) {
					$boolIsValid &= false;
					$this->setMobileNumber( NULL );
				}

				if( true == valStr( $this->getPhoneNumber() ) && false == $this->valPhoneNumber( false ) ) {
					$boolIsValid &= false;
					$this->setPhoneNumber( NULL );
				}

				if( true == valStr( $this->getGender() ) && false == $this->valGender() ) {
					$boolIsValid &= false;
					$this->setGender( NULL );
				}
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function prepareMergeFields( $strUnsubscribeLink = NULL, $strViewInBrowserUrl = NULL ) {
		$arrmixMergeFields = [];

		$arrmixMergeFields['__UNSUBSCRIBE__'] = $strUnsubscribeLink;

		$strViewInBrowserLink = '<span id=view_in_browser_link><a id="anchor" target="_blank" href="' . $strViewInBrowserUrl . '">' . __( 'View it in your browser' ) . '</a></span>';
		$arrmixMergeFields['__VIEW_IN_BROWSER_LINK__'] = $strViewInBrowserLink;

		return array_merge( $arrmixMergeFields, $this->toMergeFieldsArray() );
	}

	public function toMergeFieldsArray() {
		$arrstrGender    = [ 'M' => 'Male', 'F' => 'Female' ];

		return [
			'__EMAIL_ADDRESS__'	=> ( true == valStr( $this->getEmailAddress() ) ? $this->getEmailAddress() : NULL ),
			'__NAME_FIRST__'	=> ( true == valStr( $this->getNameFirst() ) ? $this->getNameFirst() : NULL ),
			'__NAME_MIDDLE__'	=> ( true == valStr( $this->getNameMiddle() ) ? $this->getNameMiddle() : NULL ),
			'__NAME_LAST__'		=> ( true == valStr( $this->getNameLast() ) ? $this->getNameLast() : NULL ),
			'__PHONE_NUMBER__'	=> ( true == valStr( $this->getPhoneNumber() ) ? __( '{%h,0}', [ [ 'phone_number' => $this->getPhoneNumber() ] ] ) : NULL ),
			'__MOBILE_NUMBER__'	=> ( true == valStr( $this->getMobileNumber() ) ? __( '{%h,0}', [ [ 'phone_number' => $this->getMobileNumber() ] ] ) : NULL ),
			'__FAX_NUMBER__'	=> ( true == valStr( $this->getFaxNumber() ) ? __( '{%h,0}', [ [ 'phone_number' => $this->getFaxNumber() ] ] ) : NULL ),
			'__BIRTH_DATE__'	=> ( true == valStr( $this->getBirthDate() ) ? $this->getBirthDate() : NULL ),
			'__GENDER__'		=> ( true == valStr( $this->getGender() ) ? $arrstrGender[$this->getGender()] : NULL )
		];
	}

	public function formatViewInBrowserLink( $strViewInBrowserUrl ) {
		$strViewInBrowserLink = '';

		$strBaseUrl = CONFIG_HOST_PREFIX . 'www.' . CConfig::get( 'entrata_suffix' );

		$strViewInBrowser = $strBaseUrl . $strViewInBrowserUrl . '&campaign_email_details[campaign_id]=' . $this->getCampaignId() . '&campaign_email_details[cid]=' . $this->getCid() . '&campaign_email_details[campaign_target_id]=' . $this->getId();
		$strViewInBrowserLink = '<span id=viewInBrowser><a target="_blank" href="' . $strViewInBrowser . '">' . __( 'View it in your browser' ) . '</a></span>';

		return $strViewInBrowserLink;
	}

	public function createPhoneNumberObject( $strFunctionName ) {

		if( false == method_exists( $this, $strFunctionName ) ) {
			return NULL;
		}

		$strNumber = $this->$strFunctionName();

		if( true == is_null( $strNumber ) ) {
			return NULL;
		}

		$arrmixOptions              = [];
		$objPhoneNumber = new \i18n\CPhoneNumber( $strNumber, $arrmixOptions );
		return $objPhoneNumber;
	}

}
?>