<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportGroupTypes
 * Do not add any new functions to this class.
 */

class CReportGroupTypes extends CBaseReportGroupTypes {

	public static function fetchReportGroupTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CReportGroupType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchReportGroupType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CReportGroupType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>