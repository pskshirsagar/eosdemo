<?php

class CUtilityFormula extends CBaseUtilityFormula {

	const NOT_SPECIFIED 					= 1;
	const BILLED_BY_PROVIDER 				= 2;
	const BILLED_BY_PROPERTY_SUBMETER 		= 3;
	const BILLED_BY_PROPERTY_RATIO_FORMULA 	= 4;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>