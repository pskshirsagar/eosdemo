<?php

class CReview extends CBaseReview {

	use TEosDetails;

	const MAX_LISTING = 15;
	const PROPERTY_SETTING_RECOMMENDED = 'recommended';
	const PROPERTY_SETTING_NOT_RECOMMENDED = 'notrecommended';

	protected $m_intRating;

	protected $m_fltAvgAllTimeRatting;
	protected $m_fltAvgLastThreeMonthsRatting;
	protected $m_fltAvgLastSixMonthsRatting;
	protected $m_fltAvgLastYearRatting;

	protected $m_strStateName;
	protected $m_strCompanyName;
	protected $m_strPropertyName;
	protected $m_strName;
	protected $m_strReviewerName;
	protected $m_strTitle;
	protected $m_strReviewUrl;
	protected $m_strReview;
	protected $m_strEmailAddress;
	protected $m_strRwxDomain;
	protected $m_strSource;
	protected $m_strDisplayName;
	protected $m_boolIsDirectReply;
	protected $m_strReputationSource;
	protected $m_strReviewTypeName;
	protected $m_strReviewNotificationEmailAddress;
	protected $m_strRemoteReviewKey;
	protected $m_strTimeZoneName;
	protected $m_boolIsProspectPortalPublished;
	protected $m_boolIsCompleted;
	protected $m_intOverallRating;
	protected $m_strReviewDatetime;
	protected $m_strProspectPortalPublishedOn;
	protected $m_strCompletedOn;
	protected $m_intTransmissionVendorId;
	protected $m_intRemotePageKey;
	protected $m_boolIsRecommended;
	protected $m_intViewedBy;
	protected $m_boolIsRespondedTo;
	protected $m_boolIsResponseApproved;
	protected $m_strFinalResponse;
	protected $m_boolIsLcResponseCompleted;
	protected $m_strReviewResponderAction;
	protected $m_intReviewRecommendationTypeId;
	protected $m_boolHasAttributes;
	protected $m_intReviewDetailStatusId;

	protected $m_objCompanyMediaFile;
	protected $m_objReviewRating;

	protected $m_arrobjReviewDetails;
	protected $m_arrobjReviewRatings;
	protected $m_arrobjReviewNotes;
	protected $m_arrobjReviewDetailEvents;
	protected $m_arrobjReviewDetailAttributes;
	protected $m_boolIsHideReviewUpdates;
	protected $m_boolIsHideReplyOnSourceLink;

	const REVIEW_REQUIRE_ATTRIBUTES					= 'Attributes';
	const REVIEW_REQUIRE_RESPONSE					= 'Response';
	const REVIEW_REQUIRE_RESPONSE_AND_ATTRIBUTES	= 'Response & Attributes';

	/**
	 * Get Functions
	 *
	 */

	public function getName() {
		return $this->m_strName;
	}

	public function getReviewerName() {
		return $this->m_strReviewerName;
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function getReviewUrl() {
		return $this->m_strReviewUrl;
	}

	public function getReview() {
		return $this->m_strReview;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getStateName() {
		return $this->m_strStateName;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getRating() {
		return $this->m_intRating;
	}

	public function getIsRecommended() {
		return $this->m_boolIsRecommended;
	}

	public function getCompanyMediaFile() {
		return $this->m_objCompanyMediaFile;
	}

	public function getReviewRating() {
		return $this->m_objReviewRating;
	}

	public function getReviewRatings() {
		return $this->m_arrobjReviewRatings;
	}

	public function getRwxDomain() {
		return $this->m_strRwxDomain;
	}

	public function getAvgAllTimeRatting() {
		return $this->m_fltAvgAllTimeRatting;
	}

	public function getAvgLastThreeMonthsRatting() {
		return $this->m_fltAvgLastThreeMonthsRatting;
	}

	public function getAvgLastSixMonthsRatting() {
		return $this->m_fltAvgLastSixMonthsRatting;
	}

	public function getAvgLastYearRatting() {
		return $this->m_fltAvgLastYearRatting;
	}

	public function getSource() {
		return $this->m_strSource;
	}

	public function getDisplayName() {
		return $this->m_strDisplayName;
	}

	public function getIsDirectReply() {
		return $this->m_boolIsDirectReply;
	}

	public function getReputationSource() {
		return $this->m_strReputationSource;
	}

	public function getReviewTypeName() {
		return $this->m_strReviewTypeName;
	}

	public function getReviewNotes() {
		return $this->m_arrobjReviewNotes;
	}

	public function getReviewNotificationEmailAddress() {
		return $this->m_strReviewNotificationEmailAddress;
	}

	public function getRemoteReviewKey() {
		return $this->m_strRemoteReviewKey;
	}

	public function getTransmissionVendorId() {
		return $this->m_intTransmissionVendorId;
	}

	public function getRemotePageKey() {
		return $this->m_intRemotePageKey;
	}

	public function getTimeZoneName() {
		return $this->m_strTimeZoneName;
	}

	public function getReviewDatetime() {
		return $this->m_strReviewDatetime;
	}

	public function getIsProspectPortalPublished() {
		return $this->m_boolIsProspectPortalPublished;
	}

	public function getIsCompleted() {
		return $this->m_boolIsCompleted;
	}

	public function getOverallRating() {
		return $this->m_intOverallRating;
	}

	public function getProspectPortalPublishedOn() {
		return $this->m_strProspectPortalPublishedOn;
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function getReviewDetails() {
		return $this->m_arrobjReviewDetails;
	}

	public function getReviewDetailEvents() {
		return $this->m_arrobjReviewDetailEvents;
	}

	public function getReviewDetailAttributes() {
		return $this->m_arrobjReviewDetailAttributes;
	}

	public function getIsRespondedTo() {
		return $this->m_boolIsRespondedTo;
	}

	public function getReviewRecommendationTypeId() {
		return $this->m_intReviewRecommendationTypeId;
	}

	public function getIsResponseApproved() {
		return $this->m_boolIsResponseApproved;
	}

	public function getHasAttributes() {
		return $this->m_boolHasAttributes;
	}

	public function getReviewDetailStatusId() {
		return $this->m_intReviewDetailStatusId;
	}

	public function getFinalResponse() {
		return $this->m_strFinalResponse;
	}

	public function getIsLcResponseCompleted() {
		return $this->m_boolIsLcResponseCompleted;
	}

	public function getReviewResponderAction() {
		return $this->m_strReviewResponderAction;
	}

	public function getIsHideReviewUpdates() {
		return $this->m_boolIsHideReviewUpdates;
	}

	public function getIsHideReplyOnSourceLink() {
		return $this->m_boolIsHideReplyOnSourceLink;
	}

	/**
	 * Set Function
	 *
	 */

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	public function setReviewerName( $strReviewerName ) {
		$this->m_strReviewerName = $strReviewerName;
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = $strTitle;
	}

	public function setReviewUrl( $strReviewUrl ) {
		$this->m_strReviewUrl = $strReviewUrl;
	}

	public function setReview( $strReview ) {
		$this->m_strReview = $strReview;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setStateName( $strStateName ) {
		$this->m_strStateName = $strStateName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setRating( $intRating ) {
		$this->m_intRating = $intRating;
	}

	public function setIsRecommended( $strIsRecommended ) {
		$this->m_boolIsRecommended = CStrings::strToBool( $strIsRecommended );
	}

	public function setCompanyMediaFile( $objCompanyMediaFile ) {
		$this->m_objCompanyMediaFile = $objCompanyMediaFile;
	}

	public function setReviewRating( $objReviewRating ) {
		$this->m_objReviewRating = $objReviewRating;
	}

	public function setRwxDomain( $strRwxDomain ) {
		$this->m_strRwxDomain = $strRwxDomain;
	}

	public function setReviewRatings( $arrobjReviewRatings ) {
		$this->m_arrobjReviewRatings = $arrobjReviewRatings;
	}

	public function setAvgAllTimeRatting( $fltAvgAllTimeRatting ) {
		$this->m_fltAvgAllTimeRatting = $fltAvgAllTimeRatting;
	}

	public function setAvgLastThreeMonthsRatting( $fltAvgLastThreeMonthsRatting ) {
		$this->m_fltAvgLastThreeMonthsRatting = $fltAvgLastThreeMonthsRatting;
	}

	public function setAvgLastSixMonthsRatting( $fltAvgLastSixMonthsRatting ) {
		$this->m_fltAvgLastSixMonthsRatting = $fltAvgLastSixMonthsRatting;
	}

	public function setAvgLastYearRatting( $fltAvgLastYearRatting ) {
		$this->m_fltAvgLastYearRatting = $fltAvgLastYearRatting;
	}

	public function setSource( $strSource ) {
		$this->m_strSource = $strSource;
	}

	public function setReputationSource( $strReputationSource ) {
		$this->m_strReputationSource = $strReputationSource;
	}

	public function setDisplayName( $strDisplayName ) {
		$this->m_strDisplayName = $strDisplayName;
	}

	public function setIsDirectReply( $boolIsDirectReply ) {
		$this->m_boolIsDirectReply = CStrings::strToBool( $boolIsDirectReply );
	}

	public function setReviewTypeName( $strReviewTypeName ) {
		$this->m_strReviewTypeName = $strReviewTypeName;
	}

	public function setReviewNotes( $arrobjReviewNotes ) {
		$this->m_arrobjReviewNotes = $arrobjReviewNotes;
	}

	public function setReviewNotificationEmailAddress( $strReviewNotificationEmailAddress ) {
		$this->m_strReviewNotificationEmailAddress = $strReviewNotificationEmailAddress;
	}

	public function setRemoteReviewKey( $strRemoteReviewKey ) {
		$this->m_strRemoteReviewKey = $strRemoteReviewKey;
	}

	public function setTransmissionVendorId( $intTransmissionVendorId ) {
		$this->m_intTransmissionVendorId = $intTransmissionVendorId;
	}

	public function setRemotePageKey( $intRemotePageKey ) {
		$this->m_intRemotePageKey = $intRemotePageKey;
	}

	public function setTimeZoneName( $strTimeZoneName ) {
		$this->m_strTimeZoneName = $strTimeZoneName;
	}

	public function setReviewDatetime( $strReviewDatetime ) {
		$this->m_strReviewDatetime = CStrings::strTrimDef( $strReviewDatetime, -1, NULL, true );
	}

	public function setViewedBy( $intViewedBy ) {
		$this->m_intViewedBy = $intViewedBy;
	}

	public function setIsProspectPortalPublished( $strIsProspectPortalPublished ) {
		$this->m_boolIsProspectPortalPublished = CStrings::strToBool( $strIsProspectPortalPublished );
	}

	public function setIsCompleted( $strIsCompleted ) {
		$this->m_boolIsCompleted = CStrings::strToBool( $strIsCompleted );
	}

	public function setOverallRating( $intOverallRating ) {
		$this->m_intOverallRating = $intOverallRating;
	}

	public function setProspectPortalPublishedOn( $strProspectPortalPublishedOn ) {
		$this->m_strProspectPortalPublishedOn = CStrings::strTrimDef( $strProspectPortalPublishedOn, -1, NULL, true );
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->m_strCompletedOn  = CStrings::strTrimDef( $strCompletedOn, -1, NULL, true );
	}

	public function setReviewDetails( $arrobjReviewDetails ) {
		$this->m_arrobjReviewDetails = $arrobjReviewDetails;
	}

	public function setReviewDetailEvents( $arrobjReviewDetailEvents ) {
		$this->m_arrobjReviewDetailEvents = $arrobjReviewDetailEvents;
	}

	public function setReviewDetailAttributes( $arrobjReviewDetailAttributes ) {
		$this->m_arrobjReviewDetailAttributes = $arrobjReviewDetailAttributes;
	}

	public function setIsRespondedTo( $boolIsRespondedTo ) {
		$this->m_boolIsRespondedTo = CStrings::strToBool( $boolIsRespondedTo );
	}

	public function setReviewRecommendationTypeId( $intReviewRecommendationTypeId ) {
		$this->m_intReviewRecommendationTypeId = $intReviewRecommendationTypeId;
	}

	public function setIsResponseApproved( $boolIsResponseApproved ) {
		$this->m_boolIsResponseApproved = CStrings::strToBool( $boolIsResponseApproved );
	}

	public function setHasAttributes( $boolHasAttributes ) {
		$this->m_boolHasAttributes = CStrings::strToBool( $boolHasAttributes );
	}

	public function setReviewDetailStatusId( $intReviewDetailStatusId ) {
		$this->m_intReviewDetailStatusId = $intReviewDetailStatusId;
	}

	public function setFinalResponse( $strFinalResponse ) {
		$this->set( 'm_strFinalResponse', CStrings::strTrimDef( $strFinalResponse, -1, NULL, true ) );
	}

	public function setIsLcResponseCompleted( $boolIsLcResponseCompleted ) {
		$this->m_boolIsLcResponseCompleted = CStrings::strToBool( $boolIsLcResponseCompleted );
	}

	public function setReviewResponderAction( $strReviewResponderAction ) {
		$this->m_strReviewResponderAction = $strReviewResponderAction;
	}

	public function setIsHideReviewUpdates( $boolIsHideReviewUpdates ) {
		$this->m_boolIsHideReviewUpdates = CStrings::strToBool( $boolIsHideReviewUpdates );
	}

	public function setIsHideReplyOnSourceLink( $boolIsHideReplyOnSourceLink ) {
		$this->m_boolIsHideReplyOnSourceLink = CStrings::strToBool( $boolIsHideReplyOnSourceLink );
	}

	/**
	 * Create Functions
	 *
	 */

	public function createReviewDetail() {

		$objReviewDetail = new CReviewDetail();
		$objReviewDetail->setCid( $this->getCid() );
		$objReviewDetail->setPropertyId( $this->getPropertyId() );
		$objReviewDetail->setReviewId( $this->getId() );

		return $objReviewDetail;
	}

	public function createReviewNote() {

		$objReviewNote = new CReviewNote();
		$objReviewNote->setCid( $this->getCid() );
		$objReviewNote->setPropertyId( $this->getPropertyId() );
		$objReviewNote->setReviewId( $this->getId() );

		return $objReviewNote;
	}

	public function createReviewDetailEvent( $intReviewEventTypeId = NULL ) {

		$objReviewDetailEvent = new CReviewDetailEvent;
		$objReviewDetailEvent->setCid( $this->getCid() );
		$objReviewDetailEvent->setPropertyId( $this->getPropertyId() );
		$objReviewDetailEvent->setReviewDetailId( $this->getReviewDetailId() );

		$objReviewDetailEvent->setReviewEventTypeId( ( true == valId( $intReviewEventTypeId ) ) ? $intReviewEventTypeId : NULL );

		return $objReviewDetailEvent;
	}

	public function createReviewDetailAttribute() {

		$objReviewDetailAttribute = new CReviewDetailAttribute();

		$objReviewDetailAttribute->setCid( $this->getCid() );
		$objReviewDetailAttribute->setPropertyId( $this->getPropertyId() );
		$objReviewDetailAttribute->setReviewDetailId( $this->getReviewDetailId() );

		return $objReviewDetailAttribute;
	}

	public function createReviewTask( $intReviewTaskTypeId ) {

		$objReviewTask = new CReviewTask();
		$objReviewTask->setCid( $this->getCid() );
		$objReviewTask->setPropertyId( $this->getPropertyId() );
		$objReviewTask->setReviewId( $this->getId() );
		$objReviewTask->setReviewTaskTypeId( $intReviewTaskTypeId );

		return $objReviewTask;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchReviewDetail( $objDatabase ) {
		return \Psi\Eos\Entrata\CReviewDetails::createService()->fetchReviewDetailByIdByCid( $this->getReviewDetailId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name'] ) ) {
			$this->setName( $arrmixValues['name'] );
		}
		if( true == isset( $arrmixValues['reviewer_name'] ) ) {
			$this->setReviewerName( $arrmixValues['reviewer_name'] );
		}
		if( true == isset( $arrmixValues['title'] ) ) {
			$this->setTitle( $arrmixValues['title'] );
		}
		if( true == isset( $arrmixValues['review'] ) ) {
			$this->setReview( $arrmixValues['review'] );
		}
		if( true == isset( $arrmixValues['review_url'] ) ) {
			$this->setReviewUrl( $arrmixValues['review_url'] );
		}
		if( true == isset( $arrmixValues['email_address'] ) ) {
			$this->setEmailAddress( $arrmixValues['email_address'] );
		}
		if( true == isset( $arrmixValues['state_name'] ) ) {
			$this->setStateName( $arrmixValues['state_name'] );
		}
		if( true == isset( $arrmixValues['company_name'] ) ) {
			$this->setCompanyName( $arrmixValues['company_name'] );
		}
		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}
		if( true == isset( $arrmixValues['rating'] ) ) {
			$this->setRating( $arrmixValues['rating'] );
		}
		if( true == isset( $arrmixValues['is_recommended'] ) ) {
			$this->setIsRecommended( $arrmixValues['is_recommended'] );
		}
		if( true == isset( $arrmixValues['avg_all_time'] ) ) {
			$this->setAvgAllTimeRatting( $arrmixValues['avg_all_time'] );
		}
		if( true == isset( $arrmixValues['avg_last_three_months'] ) ) {
			$this->setAvgLastThreeMonthsRatting( $arrmixValues['avg_last_three_months'] );
		}
		if( true == isset( $arrmixValues['avg_last_six_months'] ) ) {
			$this->setAvgLastSixMonthsRatting( $arrmixValues['avg_last_six_months'] );
		}
		if( true == isset( $arrmixValues['avg_last_year'] ) ) {
			$this->setAvgLastYearRatting( $arrmixValues['avg_last_year'] );
		}
		if( true == isset( $arrmixValues['source'] ) ) {
			$this->setSource( $arrmixValues['source'] );
		}
		if( true == isset( $arrmixValues['display_name'] ) ) {
			$this->setDisplayName( $arrmixValues['display_name'] );
		}
		if( true == isset( $arrmixValues['is_direct_reply'] ) ) {
			$this->setIsDirectReply( $arrmixValues['is_direct_reply'] );
		}
		if( true == isset( $arrmixValues['reputation_source'] ) ) {
			$this->setReputationSource( $arrmixValues['reputation_source'] );
		}
		if( true == isset( $arrmixValues['review_type_name'] ) ) {
			$this->setReviewTypeName( $arrmixValues['review_type_name'] );
		}
		if( true == isset( $arrmixValues['review_notification_email_address'] ) ) {
			$this->setReviewNotificationEmailAddress( $arrmixValues['review_notification_email_address'] );
		}
		if( true == isset( $arrmixValues['remote_review_key'] ) ) {
			$this->setRemoteReviewKey( $arrmixValues['remote_review_key'] );
		}
		if( true == isset( $arrmixValues['transmission_vendor_id'] ) ) {
			$this->setTransmissionVendorId( $arrmixValues['transmission_vendor_id'] );
		}
		if( true == isset( $arrmixValues['remote_page_key'] ) ) {
			$this->setRemotePageKey( $arrmixValues['remote_page_key'] );
		}
		if( true == isset( $arrmixValues['time_zone_name'] ) ) {
			$this->setTimeZoneName( $arrmixValues['time_zone_name'] );
		}
		if( true == isset( $arrmixValues['review_datetime'] ) ) {
			$this->setReviewDatetime( $arrmixValues['review_datetime'] );
		}
		if( true == isset( $arrmixValues['is_prospect_portal_published'] ) ) {
			$this->setIsProspectPortalPublished( $arrmixValues['is_prospect_portal_published'] );
		}
		if( true == isset( $arrmixValues['is_completed'] ) ) {
			$this->setIsCompleted( $arrmixValues['is_completed'] );
		}
		if( true == isset( $arrmixValues['overall_rating'] ) ) {
			$this->setOverallRating( $arrmixValues['overall_rating'] );
		}
		if( true == isset( $arrmixValues['prospect_portal_published_on'] ) ) {
			$this->setProspectPortalPublishedOn( $arrmixValues['prospect_portal_published_on'] );
		}
		if( true == isset( $arrmixValues['completed_on'] ) ) {
			$this->setCompletedOn( $arrmixValues['completed_on'] );
		}
		if( true == isset( $arrmixValues['is_responded_to'] ) ) {
			$this->setIsRespondedTo( $arrmixValues['is_responded_to'] );
		}
		if( true == isset( $arrmixValues['review_recommendation_type_id'] ) ) {
			$this->setReviewRecommendationTypeId( $arrmixValues['review_recommendation_type_id'] );
		}
		if( true == isset( $arrmixValues['is_response_approved'] ) ) {
			$this->setIsResponseApproved( $arrmixValues['is_response_approved'] );
		}
		if( true == isset( $arrmixValues['has_attributes'] ) ) {
			$this->setHasAttributes( $arrmixValues['has_attributes'] );
		}
		if( true == isset( $arrmixValues['review_detail_status_id'] ) ) {
			$this->setReviewDetailStatusId( $arrmixValues['review_detail_status_id'] );
		}
		if( true == isset( $arrmixValues['review_note'] ) ) {
			$this->setReviewNotes( $arrmixValues['review_note'] );
		}
		if( true == isset( $arrmixValues['details'] ) ) {
			$this->setDetails( $arrmixValues['details'] );
		}
		if( true == isset( $arrmixValues['final_response'] ) ) {
			$this->setFinalResponse( $arrmixValues['final_response'] );
		}
		if( true == isset( $arrmixValues['is_lc_response_completed'] ) ) {
			$this->setIsLcResponseCompleted( $arrmixValues['is_lc_response_completed'] );
		}
		if( true == isset( $arrmixValues['is_hide_review_updates'] ) ) {
			$this->setIsHideReviewUpdates( $arrmixValues['is_hide_review_updates'] );
		}
		if( true == isset( $arrmixValues['is_hide_reply_on_source_link'] ) ) {
			$this->setIsHideReplyOnSourceLink( $arrmixValues['is_hide_reply_on_source_link'] );
		}
	}

	public function valTitle() {
		$boolIsValid = true;

		if( CReviewType::RESIDENT == $this->m_intReviewTypeId ) {
			return true;
		}

		$this->setTitle( $this->valSanitizedReviewFormField( $this->getTitle() ) );

		if( false == valStr( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'testimonial', 'Review Title is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTestimonial() {
		$boolIsValid = true;

		if( false == valStr( $this->getReview() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'feedback', 'Feedback is required.' ) );
		}
		return $boolIsValid;
	}

	public function valReview() {
		$boolIsValid = true;

		$this->setReview( $this->valSanitizedReviewFormField( $this->getReview() ) );

		if( false == valStr( $this->getReview() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'testimonial', 'Review is required.' ) );
		}
		return $boolIsValid;
	}

	public function valRemotePrimaryKey( $objDatabase ) {

		$boolIsValid = true;

		$objReviews = \Psi\Eos\Entrata\CReviews::createService()->fetchReviewsByRemotePrimaryKeyByReviewTypeId( $this->m_strRemotePrimaryKey, $this->m_intReviewTypeId, $objDatabase );

		if( false == is_null( $objReviews ) ) {
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( false == is_null( $this->getEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address is not a valid one.' ) );
		}
		return $boolIsValid;
	}

	public function valShowUntil() {

		$boolIsValid = true;
		if( true == isset( $this->m_strShowUntil ) ) {
			if( true == isset( $this->m_strShowUntil ) && 1 !== CValidation::checkDate( $this->m_strShowUntil ) ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'show_until', 'Feedback show until date must be in mm/dd/yyyy form.' ) );
			} else if( ( strtotime( $this->getShowUntil() ) ) <= ( strtotime( date( 'm/d/Y' ) ) ) ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'show_until', 'Feedback show until date should be greater than todays date.' ) );
			}
		}

		return $boolIsValid;
	}

	// After having discussion with security team, adding custom reg-ex function to validate Review from Ratings and Reviews

	public function valSanitizedReviewFormField( $strFormField ) {
		return \Psi\CStringService::singleton()->preg_replace( '/(^\'|[\s\']\'[\']*|[\/~`\!@#\$%\^&\*\(\)\+=\{\}\[\]\|;:"\<\>\?\\\][\']*)/', ' ', strip_tags( $strFormField ) );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyMediaFileId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFromDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExternalReviewId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentApprovedOn() {
		return true;
	}

	public function valShowOnCorporateSites() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSyndicationAllowed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRespondedTo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasAttributes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCompleted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShowOnDashboard() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerLinkedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerLinkedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentUpdatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function buildReputationFilterWhereSql( $objReputationFilter ) {
		$strSql = NULL;

		if( true == $objReputationFilter->getCid() ) {
			$strSql .= ' AND ( r.cid = ' . $objReputationFilter->getCid() . ' )';
		}

		if( true == $objReputationFilter->getReviewSearchText() && true == valStr( $objReputationFilter->getReviewSearchText() ) ) {
			$strSql .= ' AND ( rd.title ILIKE \'%' . addslashes( $objReputationFilter->getReviewSearchText() ) . '%\' OR rd.name ILIKE \'%' . addslashes( $objReputationFilter->getReviewSearchText() ) . '%\' OR rd.review ILIKE \'%' . addslashes( $objReputationFilter->getReviewSearchText() ) . '%\' OR rd.email_address ILIKE \'%' . addslashes( $objReputationFilter->getReviewSearchText() ) . '%\' ) ';
		}

		if( true == $objReputationFilter->getFromDate() ) {
			$strSql .= ' AND ( rd.review_datetime AT TIME ZONE tz.time_zone_name )::DATE >= \'' . date( 'Y-m-d', strtotime( $objReputationFilter->getFromDate() ) ) . '\'';
		}

		if( true == $objReputationFilter->getToDate() ) {
			$strSql .= ' AND ( rd.review_datetime AT TIME ZONE tz.time_zone_name )::DATE <= \'' . date( 'Y-m-d', strtotime( $objReputationFilter->getToDate() ) ) . '\'';
		}

		if( false == $objReputationFilter->getIsShowDeletedReviews() ) {
			$strSql .= ' AND r.deleted_on IS NULL AND
						r.deleted_by IS NULL ';
		} else {
			$strSql .= ' AND r.deleted_on IS NOT NULL
							AND r.deleted_by IS NOT NULL';
		}

		// Filter related to review statuses
		if( true == valArr( $objReputationFilter->getReviewStatuses() ) ) {
			$strStatusCon = '';
			foreach( $objReputationFilter->getReviewStatuses() as $strStatus ) {
				switch( $strStatus ) {
					case 'incomplete':
						if( valStr( $strStatusCon ) ) {
							$strStatusCon .= ' OR rd.is_completed = FALSE';
						} else {
							$strStatusCon .= ' AND ( rd.is_completed = FALSE';
						}
						break;

					case 'complete':
						if( valStr( $strStatusCon ) ) {
							$strStatusCon .= ' OR rd.is_completed = TRUE';
						} else {
							$strStatusCon .= ' AND ( rd.is_completed = TRUE';
						}
						break;

					case 'unpublished':
						if( valStr( $strStatusCon ) ) {
							$strStatusCon .= ' OR ( rd.is_prospect_portal_published = FALSE AND r.review_type_id IN ( ' . CReviewType::RESIDENT_RATINGS_AND_REVIEW . ',' . CReviewType::MODERN_MESSAGE . ') )';
						} else {
							$strStatusCon .= ' AND ( ( rd.is_prospect_portal_published = FALSE AND r.review_type_id IN ( ' . CReviewType::RESIDENT_RATINGS_AND_REVIEW . ',' . CReviewType::MODERN_MESSAGE . ') )';
						}
						break;

					case 'published':
						if( valStr( $strStatusCon ) ) {
							$strStatusCon .= ' OR ( rd.is_prospect_portal_published = TRUE AND r.review_type_id = ' . CReviewType::RESIDENT_RATINGS_AND_REVIEW . ')';
						} else {
							$strStatusCon .= ' AND ( ( rd.is_prospect_portal_published = TRUE AND r.review_type_id = ' . CReviewType::RESIDENT_RATINGS_AND_REVIEW . ')';
						}
						break;

					case 'no_attributes':
						if( valStr( $strStatusCon ) ) {
							$strStatusCon .= ' OR rd.has_attributes = FALSE';
						} else {
							$strStatusCon .= ' AND ( rd.has_attributes = FALSE';
						}
						break;

					case 'attributes':
						if( valStr( $strStatusCon ) ) {
							$strStatusCon .= ' OR rd.has_attributes = TRUE';
						} else {
							$strStatusCon .= ' AND ( rd.has_attributes = TRUE';
						}
						break;

					case 'unresponded':
						if( valStr( $strStatusCon ) ) {
							$strStatusCon .= ' OR rd.is_responded_to = FALSE';
						} else {
							$strStatusCon .= ' AND ( rd.is_responded_to = FALSE';
						}
						break;

					case 'responded':
						if( valStr( $strStatusCon ) ) {
							$strStatusCon .= ' AND rd.is_responded_to = TRUE';
						} else {
							$strStatusCon .= ' AND ( rd.is_responded_to = TRUE';
						}
						break;

					default:
						$strStatusCon .= '';
						break;
				}
			}
			if( valStr( $strStatusCon ) ) {
				$strSql .= $strStatusCon . ' )';
			}
		}

		return $strSql;
	}

	public static function buildReviewsForReportsCTE( $objReputationFilter ) {
		return $strSql = ' WITH tn_notes AS(
						SELECT
							rn.cid,
							rn.property_id,
							rn.review_id,
							rn.created_by,
							rn.note,
							rn.note AS notes,
							ROW_NUMBER() OVER(PARTITION BY rn.property_id,rn.review_id ORDER BY rn.created_on DESC ) AS latest_note
						FROM
							review_notes rn
						WHERE
							rn.cid = ' . ( int ) $objReputationFilter->getCid() . '
							AND rn.property_id  IN ( ' . implode( ',', $objReputationFilter->getPropertyIds() ) . ' )
					)';
	}

	public static function buildReviewsSelectForReports( $objReputationFilter ) {

		return $strSelectSql = '
				,( rd.review_datetime AT TIME ZONE tz.time_zone_name )::DATE AS date,
				CASE 
					WHEN ( rde.created_on AT TIME ZONE tz.time_zone_name )::DATE BETWEEN \'' . date( 'Y-m-d', strtotime( $objReputationFilter->getFromDate() ) ) . '\' AND \'' . date( 'Y-m-d', strtotime( $objReputationFilter->getToDate() ) ) . '\' 
					AND rd.is_responded_to IS TRUE
					THEN COALESCE( rde.details -> \'response\', rde.details -> \'response_text\' ) 
				END AS reply,
				r.id AS review_id,
				rn.notes,
				attribute_names.positive AS positive,
				attribute_names.negative AS negative,
				
				CASE
					WHEN 0 > rd.overall_rating OR rd.overall_rating IS NULL THEN \'' . __( 'No Rating' ) . '\'
					ELSE CONCAT_WS( \' \', rd.overall_rating::TEXT, CASE WHEN rd.overall_rating < 1 THEN \'' . __( 'Stars' ) . '\' ELSE \'' . __( 'Star' ) . '\' END )
				END AS rating,
				CASE WHEN rd.is_completed IS TRUE THEN \'' . __( 'Yes' ) . '\' ELSE \'' . __( 'No' ) . '\' END AS complete,
				CASE WHEN rd.is_responded_to IS TRUE THEN \'' . __( 'Yes' ) . '\' ELSE \'' . __( 'No' ) . '\' END AS replied,
				CASE WHEN r.ps_product_id <> ' . CPsProduct::REPUTATION_ADVISOR . ' THEN CASE WHEN rd.is_prospect_portal_published IS TRUE THEN \'' . __( 'Yes' ) . '\' ELSE \'' . __( 'No' ) . '\' END ELSE \' \' END AS published,
				CASE WHEN rde.id IS NOT NULL THEN ROW_NUMBER() OVER( PARTITION BY rde.property_id, rde.review_detail_id ORDER BY rde.created_on ASC ) END AS latest_reply
				';

	}

	public static function buildReviewsJoinForReports( $objReputationFilter ) {

		return $strJoinSql = '
			LEFT JOIN tn_notes rn ON rn.cid = r.cid AND rn.property_id = r.property_id AND rn.review_id = r.id AND rn.latest_note = 1
			LEFT JOIN review_detail_events rde ON rde.cid = rd.cid AND rde.review_detail_id = rd.id AND rde.review_event_type_id IN 
			( ' . CReviewEventType::ONLINE_RESPONSE . ', ' . CReviewEventType::WROTE_RESPONSE . ', ' . CReviewEventType::RESPONSE_INSERTED . ', ' . CReviewEventType::RESPONSE_EDITED . ' )
			LEFT JOIN LATERAL (
						SELECT
							rs.id,
							STRING_AGG( CASE WHEN rda.value = \'1\' THEN attribute_name  END, E\',\r\n\' ) AS positive ,
							STRING_AGG( CASE WHEN rda.value = \'-1\' THEN attribute_name  END, E\',\r\n\' ) AS negative
						FROM
							 reviews rs
								JOIN review_details rd ON  rd.cid = rs.cid AND rd.property_id = rs.property_id AND rd.id = rs.review_detail_id 
								LEFT JOIN review_detail_attributes rda ON rda.cid = rd.cid AND rda.review_detail_id = rd.id
								LEFT JOIN property_review_attributes pra ON pra.cid = rda.cid AND pra.id = rda.property_review_attribute_id
								LEFT JOIN company_review_attributes cra ON cra.cid = pra.cid AND cra.id = pra.company_review_attribute_id
						WHERE
							rs.cid = ' . ( int ) $objReputationFilter->getCid() . '
							AND rs.id = r.id
							AND ( rd.review_datetime )::DATE >= \'' . date( 'Y-m-d', strtotime( $objReputationFilter->getFromDate() ) ) . '\'
							AND ( rd.review_datetime )::DATE <= \'' . date( 'Y-m-d', strtotime( $objReputationFilter->getToDate() ) ) . '\'
						GROUP BY
							rs.cid,
							rs.id,
							 p.property_name,
							 rd.review_datetime
					) AS attribute_names ON TRUE
			';
	}

	public static function buildGenerateSeriesSql( $objReputationFilter, $strShowChartBy = 'month' ) {

		$strToDate = $objReputationFilter->getToDate();

		if( $strShowChartBy == 'day' ) {

			$strSql = 'WITH date_ranges AS ( SELECT
												to_char( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\' ) AS start_time,
												to_char( GENERATE_SERIES, \'YYYY-MM-DD 23:59:59\') AS end_time
											 FROM
												GENERATE_SERIES( \'' . $objReputationFilter->getFromDate() . '\'::DATE, \'' . $objReputationFilter->getToDate() . '\'::DATE, \'1 day\' )
											)';
		} elseif( $strShowChartBy == 'week' ) {

			$strSql = 'WITH date_ranges AS ( SELECT
											CASE
												WHEN \'' . $objReputationFilter->getFromDate() . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE THEN
													TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
											    ELSE TO_CHAR(date_trunc( \'WEEK\', GENERATE_SERIES), \'YYYY-MM-DD HH24:MI:SS\')
											END AS start_time,
											CASE
												WHEN \'' . $objReputationFilter->getToDate() . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE THEN
													TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
												    ELSE TO_CHAR( ( DATE_TRUNC( \'WEEK\', GENERATE_SERIES ) + INTERVAL \'1 WEEK - 1 day\' ), \'YYYY-MM-DD 23:59:59\' )
											END AS end_time
										FROM (
											SELECT GENERATE_SERIES( \'' . $objReputationFilter->getFromDate() . '\'::DATE, \'' . $strToDate . '\'::DATE, \'1 WEEK\' )
												UNION	
											    SELECT \'' . $objReputationFilter->getToDate() . '\'::DATE
											) AS display_period
										)';

		} elseif( $strShowChartBy == 'year' ) {

			if( date( 'm', strtotime( $objReputationFilter->getToDate() ) ) > date( 'm', strtotime( $objReputationFilter->getFromDate() ) ) ) {
				$strToDate = date( 'm/d/Y', strtotime( '-' . ( date( 'm', strtotime( $objReputationFilter->getToDate() ) ) - date( 'm', strtotime( $objReputationFilter->getFromDate() ) ) + 1 ) . ' months', strtotime( $objReputationFilter->getToDate() ) ) );
			}

			$strSql = 'WITH date_ranges AS ( SELECT
											CASE
												WHEN \'' . $objReputationFilter->getFromDate() . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE THEN
													TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
											    ELSE TO_CHAR(date_trunc( \'YEAR\', GENERATE_SERIES), \'YYYY-MM-DD HH24:MI:SS\')
											END AS start_time,
											CASE
												WHEN \'' . $objReputationFilter->getToDate() . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE THEN
													TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
												    ELSE TO_CHAR( ( DATE_TRUNC( \'YEAR\', GENERATE_SERIES ) + INTERVAL \'1 YEAR - 1 day\' ), \'YYYY-MM-DD 23:59:59\' )
											END AS end_time
										FROM (
											SELECT GENERATE_SERIES( \'' . $objReputationFilter->getFromDate() . '\'::DATE, \'' . $strToDate . '\'::DATE, \'1 YEAR\' )
												UNION	
											    SELECT \'' . $objReputationFilter->getToDate() . '\'::DATE
											) AS display_period
										)';

		} else {

			if( date( 'd', strtotime( $objReputationFilter->getToDate() ) ) > date( 'd', strtotime( $objReputationFilter->getFromDate() ) ) ) {
				$strToDate = date( 'm/d/Y', strtotime( '-' . ( date( 'd', strtotime( $objReputationFilter->getToDate() ) ) - date( 'd', strtotime( $objReputationFilter->getFromDate() ) ) + 1 ) . ' day', strtotime( $objReputationFilter->getToDate() ) ) );
			}

			$strSql = 'WITH date_ranges AS ( SELECT
											CASE
												WHEN \'' . $objReputationFilter->getFromDate() . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE 
													THEN TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
											        ELSE TO_CHAR(date_trunc( \'MONTH\', GENERATE_SERIES), \'YYYY-MM-DD HH24:MI:SS\')
											END AS start_time,
											CASE
												WHEN \'' . $objReputationFilter->getToDate() . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE 
													THEN TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
													ELSE TO_CHAR( ( DATE_TRUNC( \'MONTH\', GENERATE_SERIES ) + INTERVAL \'1 MONTH - 1 day\' ), \'YYYY-MM-DD 23:59:59\' )
											END AS end_time
										FROM (
												SELECT GENERATE_SERIES( \'' . $objReputationFilter->getFromDate() . '\'::DATE, \'' . $strToDate . '\'::DATE, \'1 MONTH\' )
												UNION	
											    SELECT \'' . $objReputationFilter->getToDate() . '\'::DATE
											) AS display_period
										)';

		}

		return $strSql;
	}

	public static function buildWhereSqlForContractedPropertyReviewTypes( $objReputationFilter ) {

		if( false == valObj( $objReputationFilter, 'CReputationFilter' ) ) return '';
		$arrintSelectedReviewTypes = array_intersect( $objReputationFilter->getReviewTypeIds(), explode( ',', implode( ',', $objReputationFilter->getSelectedEntrataReviewTypes() ) ) );
		$strWhereSql = '';
		$arrintSelectedContractedPropertyReviewTypes = [];
		foreach( $objReputationFilter->getSelectedContractedPropertyReviewTypes() as $strReviewTypeIds => $intPropertyId ) {
			$arrintSelectedContractedPropertyReviewTypes = array_unique( array_merge( $arrintSelectedContractedPropertyReviewTypes, explode( ',', $strReviewTypeIds ) ) );
		}

		if( true == valArr( $arrintSelectedContractedPropertyReviewTypes ) ) {
			$arrintSelectedReviewTypes = array_unique( array_merge( $arrintSelectedReviewTypes, array_intersect( $objReputationFilter->getReviewTypeIds(), $arrintSelectedContractedPropertyReviewTypes ) ) );
		}

		if( false == valArr( $arrintSelectedReviewTypes ) ) return '';

		$strWhereSql = ' AND r.review_type_id IN ( ' . implode( ',', $arrintSelectedReviewTypes ) . ' )';

		unset( $arrintSelectedReviewTypes );

		return $strWhereSql;
	}

	public static function buildGenerateSeriesSqlForSVG( $objReputationFilter, $strShowChartBy = 'month', $intInterval ) {

		$strInterval = $intInterval . $strShowChartBy;

		$strToDate = $objReputationFilter->getToDate();

		if( $strShowChartBy == 'day' ) {
			$strSql = 'WITH date_ranges AS ( SELECT
												to_char( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\' ) AS start_time,
												to_char( GENERATE_SERIES, \'YYYY-MM-DD 23:59:59\') AS end_time
											 FROM
												GENERATE_SERIES( \'' . $objReputationFilter->getFromDate() . '\'::DATE, \'' . $objReputationFilter->getToDate() . '\'::DATE, \'1 day\' )
											)';
		} elseif( $strShowChartBy == 'week' ) {
			$strSql = 'WITH date_ranges AS ( SELECT
											CASE
												WHEN \'' . $objReputationFilter->getFromDate() . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE THEN
													TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
												ELSE TO_CHAR(date_trunc( \'WEEK\', GENERATE_SERIES), \'YYYY-MM-DD HH24:MI:SS\')
											END AS start_time,
											CASE
												WHEN \'' . $objReputationFilter->getToDate() . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE THEN
													TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
													ELSE TO_CHAR( ( DATE_TRUNC( \'WEEK\', GENERATE_SERIES ) + INTERVAL \'1 WEEK - 1 day\' ), \'YYYY-MM-DD 23:59:59\' )
											END AS end_time
										FROM (
											SELECT GENERATE_SERIES( \'' . $objReputationFilter->getFromDate() . '\'::DATE, \'' . $strToDate . '\'::DATE, \'1 WEEK\' )
												UNION
												SELECT \'' . $objReputationFilter->getToDate() . '\'::DATE
											) AS display_period
										)';

		} elseif( $strShowChartBy == 'year' ) {
			$strSql = 'WITH date_ranges AS ( SELECT
											CASE
												WHEN \'' . $objReputationFilter->getFromDate() . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE THEN
													TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
												ELSE TO_CHAR(date_trunc( \'YEAR\', GENERATE_SERIES), \'YYYY-MM-DD HH24:MI:SS\')
											END AS start_time,
											CASE
												WHEN \'' . $objReputationFilter->getToDate() . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE THEN
													TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
													ELSE 
														CASE 
															WHEN TO_CHAR( ( DATE_TRUNC( \'YEAR\', GENERATE_SERIES ) + INTERVAL \' ' . $strInterval . ' - 1 day\' ), \'YYYY-MM-DD 23:59:59\' ) > \'' . date( 'Y-m-d H:i:s', strtotime( $objReputationFilter->getToDate() ) ) . '\' 
															THEN \'' . date( 'Y-m-d H:i:s', strtotime( $objReputationFilter->getToDate() ) ) . '\' 
															ELSE TO_CHAR( ( DATE_TRUNC( \'YEAR\', GENERATE_SERIES ) + INTERVAL \' ' . $strInterval . ' - 1 day\' ), \'YYYY-MM-DD 23:59:59\' ) 
														END
											END AS end_time
										FROM (
											SELECT GENERATE_SERIES( \'' . $objReputationFilter->getFromDate() . '\'::DATE, \'' . $strToDate . '\'::DATE, \' ' . $strInterval . ' \' )
												UNION
												SELECT \'' . $objReputationFilter->getToDate() . '\'::DATE
											) AS display_period
										)';

		} else {
			$strSql = 'WITH date_ranges AS ( SELECT
											CASE
												WHEN \'' . $objReputationFilter->getFromDate() . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE 
													THEN TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
													ELSE TO_CHAR(date_trunc( \'MONTH\', GENERATE_SERIES), \'YYYY-MM-DD HH24:MI:SS\')
											END AS start_time,
											CASE
												WHEN \'' . $objReputationFilter->getToDate() . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE 
													THEN TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
													ELSE TO_CHAR( ( DATE_TRUNC( \'MONTH\', GENERATE_SERIES ) + INTERVAL \'' . $strInterval . ' - 1 day\' ), \'YYYY-MM-DD 23:59:59\' )
											END AS end_time
										FROM (
												SELECT GENERATE_SERIES( \'' . $objReputationFilter->getFromDate() . '\'::DATE, \'' . $strToDate . '\'::DATE, \' ' . $strInterval . '\' )
												UNION	
												SELECT \'' . $objReputationFilter->getToDate() . '\'::DATE
											) AS display_period
										)';

		}

		return $strSql;
	}

	public static function buildGenerateDateSeriesSql( $strFromDateSeries, $strToDateSeries, $strShowChartBy = 'month', $intInterval ) {

		$strInterval = $intInterval . $strShowChartBy;

		$strToDate = $strToDateSeries;

		if( $strShowChartBy == 'day' ) {
			$strSql = 'WITH date_ranges AS ( SELECT
												to_char( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\' ) AS start_time,
												to_char( GENERATE_SERIES, \'YYYY-MM-DD 23:59:59\') AS end_time
											 FROM
												GENERATE_SERIES( \'' . $strFromDateSeries . '\'::DATE, \'' . $strToDateSeries . '\'::DATE, \'1 day\' )
											)';
		} elseif( $strShowChartBy == 'week' ) {
			$strSql = 'WITH date_ranges AS ( SELECT
											CASE
												WHEN \'' . $strFromDateSeries . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE THEN
													TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
												ELSE TO_CHAR(date_trunc( \'WEEK\', GENERATE_SERIES), \'YYYY-MM-DD HH24:MI:SS\')
											END AS start_time,
											CASE
												WHEN \'' . $strToDateSeries . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE THEN
													TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
													ELSE TO_CHAR( ( DATE_TRUNC( \'WEEK\', GENERATE_SERIES ) + INTERVAL \'1 WEEK - 1 day\' ), \'YYYY-MM-DD 23:59:59\' )
											END AS end_time
										FROM (
											SELECT GENERATE_SERIES( \'' . $strFromDateSeries . '\'::DATE, \'' . $strToDate . '\'::DATE, \'1 WEEK\' )
												UNION
												SELECT \'' . $strToDateSeries . '\'::DATE
											) AS display_period
										)';

		} elseif( $strShowChartBy == 'year' ) {
			$strSql = 'WITH date_ranges AS ( SELECT
											CASE
												WHEN \'' . $strFromDateSeries . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE THEN
													TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
												ELSE TO_CHAR(date_trunc( \'YEAR\', GENERATE_SERIES), \'YYYY-MM-DD HH24:MI:SS\')
											END AS start_time,
											CASE
												WHEN \'' . $strToDateSeries . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE THEN
													TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
													ELSE 
														CASE 
															WHEN TO_CHAR( ( DATE_TRUNC( \'YEAR\', GENERATE_SERIES ) + INTERVAL \' ' . $strInterval . ' - 1 day\' ), \'YYYY-MM-DD 23:59:59\' ) > \'' . date( 'Y-m-d H:i:s', strtotime( $strToDateSeries ) ) . '\' 
															THEN \'' . date( 'Y-m-d H:i:s', strtotime( $strToDateSeries ) ) . '\' 
															ELSE TO_CHAR( ( DATE_TRUNC( \'YEAR\', GENERATE_SERIES ) + INTERVAL \' ' . $strInterval . ' - 1 day\' ), \'YYYY-MM-DD 23:59:59\' ) 
														END
											END AS end_time
										FROM (
											SELECT GENERATE_SERIES( \'' . $strFromDateSeries . '\'::DATE, \'' . $strToDate . '\'::DATE, \' ' . $strInterval . ' \' )
												UNION
												SELECT \'' . $strToDateSeries . '\'::DATE
											) AS display_period
										)';

		} else {
			$strSql = 'WITH date_ranges AS ( SELECT
											CASE
												WHEN \'' . $strFromDateSeries . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE 
													THEN TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
													ELSE TO_CHAR(date_trunc( \'MONTH\', GENERATE_SERIES), \'YYYY-MM-DD HH24:MI:SS\')
											END AS start_time,
											CASE
												WHEN \'' . $strToDateSeries . '\' = TO_CHAR( GENERATE_SERIES, \'MM/DD/YYYY HH24:MI:SS\')::DATE 
													THEN TO_CHAR( GENERATE_SERIES, \'YYYY-MM-DD HH24:MI:SS\')
													ELSE TO_CHAR( ( DATE_TRUNC( \'MONTH\', GENERATE_SERIES ) + INTERVAL \'' . $strInterval . ' - 1 day\' ), \'YYYY-MM-DD 23:59:59\' )
											END AS end_time
										FROM (
												SELECT GENERATE_SERIES( \'' . $strFromDateSeries . '\'::DATE, \'' . $strToDate . '\'::DATE, \' ' . $strInterval . '\' )
												UNION	
												SELECT \'' . $strToDateSeries . '\'::DATE
											) AS display_period
										)';

		}

		return $strSql;
	}

	public static function getReviewRecommendationTypeNameById( $intReviewRecommendationTypeId ) {
		switch( $intReviewRecommendationTypeId ) {

			case CReviewRecommendationType::RECOMMENDED:
				return self::PROPERTY_SETTING_RECOMMENDED;

			case CReviewRecommendationType::NOT_RECOMMENDED:
				return self::PROPERTY_SETTING_NOT_RECOMMENDED;

			default:
				// No default action
		}

		return NULL;
	}

}
?>