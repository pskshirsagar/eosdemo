<?php

class CDepositInterestFormula extends CBaseDepositInterestFormula {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInterestStartDays() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInterestQualificationDays() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInterestGraceDays() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInterestPeriodForfeitDays() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDenyInterestForSkips() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valGenerateRefundCheck() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>