<?php

class CRateType extends CBaseRateType {

	const CLIENT 								= 1;
	const PROPERTY 								= 2;
	const UNIT_TYPE 							= 3;
	const PROPERTY_BUILDING 					= 4;
	const PROPERTY_UNIT 						= 5;
	const UNIT_SPACE 							= 6;
	const PROPERTY_AMENITY 						= 7;
	const UNIT_AMENITY							= 8;
	const PROPERTY_APPLICATION_FEE_TYPE			= 9;
	const PROPERTY_PET_TYPE						= 10;
	const PROPERTY_CHOICE_SPECIFIC				= 11;
	const COMPANY_SPECIALS						= 12;
	const ADD_ON_GROUP							= 13;
	const ADD_ON								= 14;
	const INSPECTION_RESPONSE					= 15;
	const MAINTENANCE_PROBLEMS					= 16;
	const UNIT_SPACE_AMENITY					= 17;

}
?>