<?php

class CSubsidyCitizenshipType extends CBaseSubsidyCitizenshipType {

	const CITIZEN_OR_NATIONAL		= 1;
	const IMMIGRATION_STATUS		= 2;
	const INELIGIBLE_CHILD			= 3;
	const INELIGIBLE_NON_CITIZEN	= 4;
	const INELIGIBLE_PARENT			= 5;
	const NO_DOCUMENTATION			= 6;
	const PENDING_VERIFICATION		= 7;
	const NOT_FAMILY_MEMBER			= 8;

	public static $c_arrintSubsidyEligibileCitizenshipTypes = [
		self::CITIZEN_OR_NATIONAL,
		self::IMMIGRATION_STATUS,
		self::PENDING_VERIFICATION
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getSubsidyCitizenshipTypeIdByName( $strCitizenship ) {

		$intSubsidyCitizenshipTypeId = NULL;

		switch( \Psi\CStringService::singleton()->strtolower( $strCitizenship ) ) {

			case 'citizen':
				$intSubsidyCitizenshipTypeId = self::CITIZEN_OR_NATIONAL;
				break;

			case 'noncitizen with eligible immigration status':
			case 'non-citizen with eligible immigration status':
				$intSubsidyCitizenshipTypeId = self::IMMIGRATION_STATUS;
				break;

			case 'ineligible noncitizen child':
				$intSubsidyCitizenshipTypeId = self::INELIGIBLE_CHILD;
				break;

			case 'ineligible parent':
				$intSubsidyCitizenshipTypeId = self::INELIGIBLE_PARENT;
				break;

			case 'no documentation':
				$intSubsidyCitizenshipTypeId = self::NO_DOCUMENTATION;
				break;

			case 'pending verification':
				$intSubsidyCitizenshipTypeId = self::PENDING_VERIFICATION;
				break;

			default:
				$intSubsidyCitizenshipTypeId = NULL;
				break;
		}

		return $intSubsidyCitizenshipTypeId;
	}

}
?>