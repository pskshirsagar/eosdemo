<?php
class CListItem extends CBaseListItem {

	protected $m_intPropertyId;
	protected $m_intRemotePrimaryKey;

	protected $m_strDefaultCancellationReason;
	protected $m_strListTypeName;

	protected $m_boolIsDefaultListItemId;

	protected $m_arrstrCancellationReasonNames;
	protected $m_strDefaultListItemName;

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['is_default_list_item'] ) )				$this->setIsDefaultListItemId( $arrmixValues['is_default_list_item'] );
		if( true == isset( $arrmixValues['default_cancellation_reason_name'] ) )	$this->setDefaultCancellationReason( $arrmixValues['default_cancellation_reason_name'] );
    	if( true == isset( $arrmixValues['cancellation_reason_names'] ) )			$this->setCancellationReasonNames( $arrmixValues['cancellation_reason_names'] );
    	if( true == isset( $arrmixValues['property_id'] ) )							$this->setPropertyId( $arrmixValues['property_id'] );
    	if( true == isset( $arrmixValues['remote_primary_key'] ) )					$this->setPropertyId( $arrmixValues['remote_primary_key'] );
    	if( true == isset( $arrmixValues['list_type_name'] ) )						$this->setListTypeName( $arrmixValues['list_type_name'] );
    	if( true == isset( $arrmixValues['default_list_item_name'] ) )				$this->setDefaultListItemName( $arrmixValues['default_list_item_name'] );
		return;
	}

	public function setIsDefaultListItemId( $boolIsDefaultListItemId ) {
		$this->m_boolIsDefaultListItemId = $boolIsDefaultListItemId;
	}

	public function setDefaultCancellationReason( $strDefaultCancellationReason ) {
		$this->m_strDefaultCancellationReason = $strDefaultCancellationReason;
	}

	public function setCancellationReasonNames( $arrstrCancellationReasonNames ) {
		$this->m_arrstrCancellationReasonNames = $arrstrCancellationReasonNames;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setRemotePrimaryKey( $intRemotePrimaryKey ) {
		$this->m_intRemotePrimaryKey = $intRemotePrimaryKey;
	}

	public function setListTypeName( $strListTypeName ) {
		$this->m_strListTypeName = $strListTypeName;
	}

	public function setDefaultListItemName( $strDefaultListItemName ) {
		$this->m_strDefaultListItemName = $strDefaultListItemName;
	}

	/**
	 * Get Functions
	 */

	public function getIsDefaultListItemId() {
		return $this->m_boolIsDefaultListItemId;
	}

	public function getDefaultCancellationReason() {
		return $this->m_strDefaultCancellationReason;
	}

	public function getCancellationReasonNames() {
		return $this->m_arrstrCancellationReasonNames;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getRemotePrimaryKey() {
		return $this->m_intRemotePrimaryKey;
	}

	public function getListTypeName() {
		return $this->m_strListTypeName;
	}

	public function getDefaultListItemName() {
		return $this->m_strDefaultListItemName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valListTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultListItemId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase = NULL ) {

		$boolIsValid = true;
		if( false == isset( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Please enter Name of Reason.' ) ) );

		} elseif( 3 > \Psi\CStringService::singleton()->strlen( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name of Reason must be at least {%d,0} characters in length.', [ 3 ] ) ) );
		}

		if( true == isset( $objDatabase ) ) {

			$objListItem = \Psi\Eos\Entrata\CListItems::createService()->fetchDuplicateListItemByNameByCidByListTypeId( $this->getName(), $this->getId(),  $this->getCid(), $objDatabase, $this->getListTypeId() );

			if( true == is_numeric( $this->getId() ) ) {
				$objOriginalListItem = \Psi\Eos\Entrata\CListItems::createService()->fetchListItemByIdByCid( $this->getId(), $this->getCid(), $objDatabase );
			}
		}

		if( ( false == isset ( $objOriginalListItem ) || $this->getName() != $objOriginalListItem->getName() ) && true == valObj( $objListItem, 'CListItem' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name of Reason already in use.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validateCustomCancellationReasons() {

		$boolIsValid = true;
		$boolIsValid &= $this->valCid();

		if( false == valStr( $this->getName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Cancellation Reason Name should not be empty.' ), NULL ) );
			return $boolIsValid &= false;
		} elseif( 100 < \Psi\CStringService::singleton()->strlen( $this->getName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Cancellation Reason Name should be upto {%d, 0} characters.', [ 100 ] ), NULL ) );
			return $boolIsValid &= false;
		}

		if( true == in_array( $this->getName(), $this->getCancellationReasonNames() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Cancellation Reason Name already exists for list type.' ), NULL ) );
			$boolIsValid = false;
			return $boolIsValid;
		}

		if( false == is_numeric( $this->getListTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select List Type.' ), NULL ) );
			return $boolIsValid &= false;
		}

		if( false == is_numeric( $this->getDefaultListItemId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select Most Similar To.' ), NULL ) );
			return $boolIsValid &= false;
		}

		return $boolIsValid;
	}

    public function valDependantInformation( $objDatabase ) {

    	$boolIsValid = true;

    	$arrobjPropertyListItems = \Psi\Eos\Entrata\CPropertyListItems::createService()->fetchPropertyListItemsByListItemIdByCid( $this->getId(), $this->getCid(), $objDatabase );

    	if( true == valArr( $arrobjPropertyListItems ) ) {
    		$boolIsValid = false;
    		foreach( $arrobjPropertyListItems as $objPropertyListItem ) {
    			if( true == in_array( $objPropertyListItem->getListTypeId(), [ CListType::MOVE_OUT, CListType::SKIP, CListType::EVICTION ] ) ) {
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property move out reason', __( 'This move-out reason is associated to properties and / or leases and cannot be deleted.' ) ) );
    				break;
    			} elseif( CListType::TRANSFER == $objPropertyListItem->getListTypeId() ) {
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property move out reason', __( 'Could not delete the company transfer reason, it is being associated with property.' ) ) );
    				break;
    			}
    		}
    	}

    	return $boolIsValid;

    }

    public function validate( $strAction, $objDatabase = NULL ) {
    	$boolIsValid = true;

    	switch( $strAction ) {
    		case VALIDATE_INSERT:
    		case VALIDATE_UPDATE:
    			$boolIsValid &= $this->valName( $objDatabase );
    			break;

    		case VALIDATE_DELETE:
    			$boolIsValid &= $this->valDependantInformation( $objDatabase );
    			break;

    		default:
    			$boolIsValid = true;
    			break;
    	}

    	return $boolIsValid;
    }

}
?>