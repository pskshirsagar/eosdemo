<?php

class CSiteTabletStat extends CBaseSiteTabletStat {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPlatform() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valVersion() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBuild() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScreen() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEvent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>