<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerDetails
 * Do not add any new functions to this class.
 */

class CCustomerDetails extends CBaseCustomerDetails {

	public static function fetchCustomerDetailByCustomerIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerDetail( sprintf( 'SELECT * FROM customer_details WHERE customer_id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerDetailsByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT * FROM customer_details WHERE customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) AND cid = ' . ( int ) $intCid;
		return self::fetchCustomerDetails( $strSql, $objDatabase );
	}

}
?>