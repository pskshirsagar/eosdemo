<?php

trait TEosStoredObject {

	protected $m_objObjectStorageGatewayResponse;

	public function getObjectStorageGatewayResponse() {
		return $this->m_objObjectStorageGatewayResponse;
	}

	public function setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse ) {
		$this->m_objObjectStorageGatewayResponse = $objObjectStorageGatewayResponse;
		return $this;
	}

	abstract protected function calcStorageContainer( $strVendor = NULL );

	abstract protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL );

	public function createPutGatewayRequest( $arrmixArgs = [], $strReferenceTag = NULL ) {

		$arrmixArgs = $arrmixArgs ?? [];

		$strKey = $this->calcStorageKey( $strReferenceTag );

		if( !empty( $arrmixArgs['inputFile'] ) ) {
			$strKey = $this->coerceFileExtension( $strKey, $arrmixArgs['inputFile'] );
		}

		$arrmixRequest = [
			'cid'            => $this->getCid(),
			'container'      => $this->calcStorageContainer(),
			'key'            => $strKey
		];

		foreach( $arrmixArgs as $strKey => $strVal ) {
			$arrmixRequest[$strKey] = $strVal;
		}

		if( 1 == $this->getCid() ) unset( $arrmixRequest['cid'] );

		return $arrmixRequest;
	}

	public function fetchStoredObject( $objDatabase, $strReferenceTag = NULL, $boolForceNewObject = false ) {

		$objStoredObject = $boolForceNewObject ? NULL : \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );

		// If no object found, create one using $this data to facilitate get requests
		if( !valObj( $objStoredObject, 'CStoredObject' ) ) {

			$objStoredObject = new \CStoredObject();
			$objStoredObject->setCid( $this->getCid() );
			$objStoredObject->setReferenceTable( static::TABLE_NAME );
			$objStoredObject->setReferenceId( $this->getId() );
			$objStoredObject->setReferenceTag( $strReferenceTag );
			$objStoredObject->setVendor( \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM );
			$objStoredObject->setContainer( $this->calcStorageContainer( \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM ) );
			$objStoredObject->setKey( $this->calcStorageKey( $strReferenceTag, \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM ) );
			$objStoredObject->setDetailsField( [ 'legacy_persistence', 'storageGateway' ], \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 );
			$objStoredObject->setDetailsField( [ 'legacy_persistence', 'container' ],  $this->calcStorageContainer( \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 ) );
			$objStoredObject->setDetailsField( [ 'legacy_persistence', 'key' ], $this->calcStorageKey( $strReferenceTag, \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 ) );
		}

		return $objStoredObject;
	}

	public function insertOrUpdateStoredObject( $intCurrentUserId, $objDatabase, $objStoredObject = NULL, $strReferenceTag = NULL ) {

		$objStoredObject = $objStoredObject ?? $this->fetchStoredObject( $objDatabase, $strReferenceTag );

		if( true == valId( $objStoredObject->getId() ) ) {
			return $this->updateStoredObject( $objStoredObject, $intCurrentUserId, $objDatabase );
		} else {
			return $this->insertStoredObject( $intCurrentUserId, $objDatabase, $strReferenceTag );
		}
	}

	protected function insertStoredObject( $intCurrentUserId, $objDatabase, $strReferenceTag = NULL ) {
		if( false == valObj( $this->getObjectStorageGatewayResponse(), \Psi\Libraries\ObjectStorage\DataTransferObjects\CObjectStorageGatewayResponse::class ) ) return true;

		$objStoredObject = new \CStoredObject();
		$objStoredObject->setCid( $this->getCid() );
		$objStoredObject->setReferenceTable( static::TABLE_NAME );
		$objStoredObject->setReferenceId( $this->getId() );
		$objStoredObject->setReferenceTag( $strReferenceTag );
		$objStoredObject->setVendor( $this->getObjectStorageGatewayResponse()['request']['storageGateway'] );
		$objStoredObject->setContainer( $this->getObjectStorageGatewayResponse()['request']['container'] );
		$objStoredObject->setKey( $this->getObjectStorageGatewayResponse()['request']['key'] );

		$objStoredObject->setDetailsField( [ 'object_url' ], $this->getObjectStorageGatewayResponse()['objectURL'] );
		$objStoredObject->setDetailsField( [ 'last_modified' ], date( 'Y-m-d H:i:s' ) );
		$objStoredObject->setDetailsField( [ 'content_type' ], $this->getObjectStorageGatewayResponse()['request']['contentType'] );
		$objStoredObject->setCreatedBy( $intCurrentUserId );
		$objStoredObject->setCreatedOn( 'now()' );
		$objStoredObject->setUpdatedBy( $intCurrentUserId );
		$objStoredObject->setUpdatedOn( 'now()' );
		$objStoredObject->setContentLength( $this->getObjectStorageGatewayResponse()['contentLength'] );
		$objStoredObject->setTitle( $this->getTitle() ?? $this->getName() ?? $this->getBatchName() );
		$objStoredObject->setEtag( $this->getObjectStorageGatewayResponse()['eTag'] );

		if( false == $objStoredObject->insert( $intCurrentUserId, $objDatabase ) ) {
			$objDatabase->rollback();
			return false;
		}

		return true;
	}

	protected function updateStoredObject( $objStoredObject, $intCurrentUserId, $objDatabase ) {
		if( false == valObj( $this->getObjectStorageGatewayResponse(), \Psi\Libraries\ObjectStorage\DataTransferObjects\CObjectStorageGatewayResponse::class ) ) return true;

		$objStoredObject->setKey( $this->calcStorageKey( $objStoredObject->getReferenceTag() ) );
		$objStoredObject->setVendor( $this->getObjectStorageGatewayResponse()['request']['storageGateway'] );
		$objStoredObject->setContainer( $this->getObjectStorageGatewayResponse()['request']['container'] );
		$objStoredObject->setDetailsField( [ 'object_url' ], $this->getObjectStorageGatewayResponse()['objectURL'] );
		$objStoredObject->setDetailsField( [ 'last_modified' ], date( 'Y-m-d H:i:s' ) );
		$objStoredObject->setDetailsField( [ 'content_type' ], $this->getObjectStorageGatewayResponse()['contentType'] );
		$objStoredObject->setUpdatedBy( $intCurrentUserId );
		$objStoredObject->setUpdatedOn( 'now()' );
		$objStoredObject->setContentLength( $this->getObjectStorageGatewayResponse()['contentLength'] );
		$objStoredObject->setTitle( $this->getTitle() ?? $this->getName() ?? $this->getBatchName() );
		$objStoredObject->setEtag( $this->getObjectStorageGatewayResponse()['eTag'] );

		if( false == $objStoredObject->update( $intCurrentUserId, $objDatabase ) ) {
			$objDatabase->rollback();
			return false;
		}

		return true;
	}

	public function deleteStoredObject( $intCurrentUserId, $objDatabase, $objStoredObject = NULL, $strReferenceTag = NULL ) {

		$objStoredObject = $objStoredObject ?? $this->fetchStoredObject( $objDatabase, $strReferenceTag );

		if( true == valId( $objStoredObject->getId() ) ) {
			$objStoredObject->setDeletedBy( $intCurrentUserId );
			$objStoredObject->setDeletedOn( 'now()' );
			return $objStoredObject->update( $intCurrentUserId, $objDatabase );
		} else {
			return false;
		}
	}

	/*
	 *  Coerces $strSubject to have the same file extension as $strSource (e.g. pdf, jpg)
	 */

	protected function coerceFileExtension( $strSubject, $strSource ) {
		$strExtension = pathinfo( $strSource )['extension'];
		return \Psi\CStringService::singleton()->preg_replace( '#.[^.]+$#', '.' . $strExtension, $strSubject );
	}

}