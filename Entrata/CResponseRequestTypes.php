<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CResponseRequestTypes
 * Do not add any new functions to this class.
 */

class CResponseRequestTypes extends CBaseResponseRequestTypes {

	public static function fetchResponseRequestTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CResponseRequestType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchResponseRequestType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CResponseRequestType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>