<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerPortalSettings
 * Do not add any new functions to this class.
 */

class CCustomerPortalSettings extends CBaseCustomerPortalSettings {

	public static function fetchPreExistingCustomerCountByUsernameByCid( $strUsername, $intCid, $intCustomerId = NULL, $objDatabase, $boolIsFromSelfAccountCreation = false ) {
		$strCondition = '';

		if( false == is_null( $intCustomerId ) && true == is_numeric( $intCustomerId ) ) {
			$strCondition = ' AND cps.customer_id != ' . ( int ) $intCustomerId;
		}
		$strWhereCondition = '';
		if( true == $boolIsFromSelfAccountCreation ) {
			$strWhereCondition .= ' AND lc.lease_status_type_id <>' . CLeaseStatusType::CANCELLED;
		}
		$strFrom = 'customer_portal_settings cps
					JOIN lease_customers lc ON ( cps.cid = lc.cid AND cps.customer_id = lc.customer_id )
					JOIN properties p ON ( p.cid = lc.cid AND lc.property_id = p.id ) ';

		$strWhereSql = ' WHERE cps.cid = ' . ( int ) $intCid . '
							AND lower(username) = \'' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '\' '
		               . $strCondition . ' ' . $strWhereCondition . '
							AND p.is_disabled <> 1  LIMIT 1';

		return self::fetchCustomerPortalSettingCount( $strWhereSql, $objDatabase, $strFrom );
	}

	public static function fetchPreExistingCustomerByUsernamesByCid( $arrstrUserNames, $intCid, $objDatabase ) {

		$strEscapeUserNames = pg_escape_string( implode( ',', $arrstrUserNames ) );

		$strSql = 'SELECT customer_id, username FROM customer_portal_settings WHERE cid = ' . ( int ) $intCid . ' AND lower( username ) IN( \'' . implode( '\',\'', explode( ',', $strEscapeUserNames ) ) . '\' ) LIMIT 1 ';
		return self::fetchCustomerPortalSetting( $strSql, $objDatabase );
	}

	public static function fetchCustomerPortalSettingByUsernameByCid( $strUsername, $intCid, $objDatabase ) {
		return self::fetchCustomerPortalSetting( 'SELECT * FROM customer_portal_settings WHERE cid = ' . ( int ) $intCid . ' AND lower(username) = \'' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '\' LIMIT 1', $objDatabase );
	}

	public static function fetchLatestCustomerPortalSettingByUsernameByCid( $strUsername, $intCid, $objDatabase ) {
		return self::fetchCustomerPortalSetting( 'SELECT * FROM customer_portal_settings WHERE cid = ' . ( int ) $intCid . ' AND lower(username) = \'' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '\' ORDER BY updated_on desc LIMIT 1', $objDatabase );
	}

	public static function fetchCustomerPortalSettingCustomerIdByUsernameByCid( $strUsername, $intCid, $objDatabase ) {
		return self::fetchCustomerPortalSetting( 'SELECT customer_id FROM customer_portal_settings WHERE cid = ' . ( int ) $intCid . ' AND lower(username) = \'' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '\' LIMIT 1', $objDatabase );
	}

	public static function fetchCustomerPortalSettingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT DISTINCT ON( cps.id ) cps.*
					FROM customer_portal_settings cps, lease_customers lc
					WHERE cps.cid = ' . ( int ) $intCid . '
					AND cps.customer_id = lc.customer_id AND cps.cid = lc.cid
					AND lc.lease_status_type_id != ' . CLeaseStatusType::CANCELLED . '
					AND lc.lease_id IN ( SELECT id FROM leases l WHERE l.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' ) AND l.cid = ' . ( int ) $intCid . ' )
					AND cps.username IS NOT NULL
					AND cps.customer_id IS NOT NULL';

		return self::fetchCustomerPortalSettings( $strSql, $objDatabase );
	}

	public static function fetchCustomCustomerPortalSettingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT DISTINCT ON( cps.id ) cps.*
					FROM customer_portal_settings cps, lease_customers lc
					WHERE cps.cid = ' . ( int ) $intCid . '
					AND cps.customer_id = lc.customer_id AND cps.cid = lc.cid
					AND lc.lease_status_type_id != ' . CLeaseStatusType::CANCELLED . '
					AND lc.lease_id IN ( SELECT id FROM leases l WHERE l.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' ) AND l.cid = ' . ( int ) $intCid . ' )
					AND cps.customer_id IS NOT NULL';

		return self::fetchCustomerPortalSettings( $strSql, $objDatabase );
	}

	public static function fetchCustomCustomerPortalSettingByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM customer_portal_settings WHERE customer_id ::integer = ' . ( int ) $intCustomerId . ' AND cid ::integer = ' . ( int ) $intCid;
		return self::fetchCustomerPortalSetting( $strSql, $objDatabase );
	}

	public static function fetchCustomerPortalSettingByFacebookUserId( $strFacebookUser, $objDatabase ) {
		$strSql = 'SELECT id FROM customer_portal_settings WHERE facebook_user = \'' . $strFacebookUser . '\' LIMIT 1';
		return parent::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchCustomerPortalSettingByPropertyIdByFacebookUserByCid( $intPropertyId, $intFacebookUser, $intCid, $objDatabase ) {
		// $intFacebookUser = 'a' . $intFacebookUser;
		// $intFacebookUser = $intFacebookUser.'a';
		// $intFacebookUser = '1000000asd22375198';
		// removed type casting ( int ) code & checking for digit value only for $intFacebookUser to handle request with user id greater than 2147483647 integer length.
		if( false == preg_match( '/^\d+$/', $intFacebookUser ) ) {
			return false;
		}

		$strSql = 'SELECT cps.id
						FROM
						customer_portal_settings cps
						LEFT JOIN lease_customers clc ON ( cps.customer_id = clc.customer_id AND cps.cid = clc.cid )
						LEFT JOIN leases cl ON ( cl.id = clc.lease_id AND cl.cid = clc.cid )
					WHERE cl.property_id::integer = ' . ( int ) $intPropertyId . '::integer
						AND cps.facebook_user = \'' . $intFacebookUser . '\'
						AND cps.cid ::integer = ' . ( int ) $intCid . ' LIMIT 1';

		return self::fetchCustomerPortalSetting( $strSql, $objDatabase );
	}

	public static function fetchCustomerPortalSettingByFacebookUserIncludingChildPropertiesByPropertyIdByCid( $intFacebookUser, $intPropertyId, $intCid, $objDatabase ) {
		if( false == preg_match( '/^\d+$/', $intFacebookUser ) ) return false;

		$strSql = 'SELECT
						cps.customer_id
					FROM
						customer_portal_settings cps
						LEFT JOIN lease_customers lc ON ( cps.customer_id = lc.customer_id AND cps.cid = lc.cid )
						LEFT JOIN leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid )
					WHERE
						(
							l.property_id = ' . ( int ) $intPropertyId . '
							OR l.property_id IN (
								SELECT
									p.id
								FROM
									properties p
								WHERE
									p.cid = ' . ( int ) $intCid . '
									AND p.property_id IN ( ' . ( int ) $intPropertyId . ' )
							)
						)
						AND cps.facebook_user = \'' . $intFacebookUser . '\'
						AND cps.cid::integer = ' . ( int ) $intCid . '
					LIMIT
						1';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['customer_id'];

	}

	public static function fetchNetworkingEnrolledCustomersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( 0 == ( int ) $intPropertyId ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM (
					SELECT DISTINCT ON(cps.facebook_user) cps.id, cps.facebook_user, cps.username
						FROM
						customer_portal_settings cps
						LEFT JOIN lease_customers lc ON ( cps.cid = lc.cid AND cps.customer_id = lc.customer_id )
						LEFT JOIN leases l ON ( lc.cid = l.cid AND l.id = lc.lease_id )
					WHERE
						l.cid::integer = ' . ( int ) $intCid . '::integer
						AND l.property_id::integer = ' . ( int ) $intPropertyId . '::integer
						AND cps.is_networking_enrolled = 1
						AND cps.facebook_user IS NOT NULL
					) as sub_query1';

		return self::fetchCustomerPortalSettings( $strSql, $objDatabase );
	}

	public static function fetchCustomCustomerPortalSettingByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM customer_portal_settings WHERE customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) AND cid ::integer = ' . ( int ) $intCid;
		return self::fetchCustomerPortalSettings( $strSql, $objDatabase );
	}

	public static function fetchCustomCustomerPortalSettingCountByUsernameByCid( $strUsername, $intCid, $objDatabase ) {

		$strFrom = ' 	customer_portal_settings cps
						JOIN lease_customers lc ON( cps.customer_id = lc.customer_id AND cps.cid = lc.cid )
						JOIN leases l ON( l.id = lc.lease_id AND lc.cid = l.cid )
						JOIN properties p ON( l.property_id = p.id AND l.cid = p.cid )';

		$strWhere = ' WHERE
						lower( cps.username ) = \'' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '\'
						AND cps.cid = ' . ( int ) $intCid . '
					    AND p.is_disabled = 0
						AND p.disabled_on IS NULL
					LIMIT 1';

		return self::fetchCustomerPortalSettingCount( $strWhere, $objDatabase, $strFrom );
	}

	public static function fetchCustomerPortalSettingCount( $strWhere, $objDatabase, $strFrom = 'customer_portal_settings' ) {
		return parent::fetchRowCount( $strWhere, $strFrom, $objDatabase );
	}

	public static function fetchCustomerPortalSettingsByUsernameByCid( $strUsername, $intCid, $objDatabase ) {
		$strSql = 'SELECT
					cps.customer_id,
					cps.username,
					cps.password_encrypted,
					cps.cid,
					l.property_id,
					wp.website_id
				FROM
					customer_portal_settings cps
					JOIN lease_customers lc ON cps.customer_id = lc.customer_id AND cps.cid = lc.cid
					JOIN leases l ON lc.lease_id = l.id AND lc.cid = l.cid
					LEFT JOIN website_properties wp ON l.property_id = wp.property_id AND l.cid = wp.cid
				WHERE
					cps.cid = ' . ( int ) $intCid . '
					AND lower( cps.username ) = \'' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '\'
					AND lc.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
				ORDER BY
					CASE
                    	WHEN
                        	' . CLeaseStatusType::CURRENT . ' = lc.lease_status_type_id
                        THEN
                        	1
                        WHEN
                        	' . CLeaseStatusType::NOTICE . ' = lc.lease_status_type_id
                        THEN
                        	2
                        WHEN
                        	' . CLeaseStatusType::FUTURE . ' = lc.lease_status_type_id
                        THEN
                        	3
                        WHEN
                        	' . CLeaseStatusType::APPLICANT . ' = lc.lease_status_type_id
                        THEN
                        	4
                        WHEN
                        	' . CLeaseStatusType::PAST . ' = lc.lease_status_type_id
                        THEN
                        	5
                        ELSE
                        	6
                    END';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerPortalSettingsForGlobalUsers( $objDatabase, $intLimit = 5000, $intOffset = 0, $intDaySpan = 1, $boolUpdatedOnly = false ) {
		$strWhereClause = '';
		if( true == $boolUpdatedOnly ) {
			$strWhereClause = 'AND cps.updated_on BETWEEN NOW() - INTERVAL \'' . ( int ) $intDaySpan . ' day\' AND NOW() ';
		}

		$strSql = 'SELECT DISTINCT ON ( cps.created_on, cps.cid, l.property_id, cps.username )
					cps.cid,
					cps.customer_id,
					l.property_id,
					cps.username,
					cps.password_encrypted,
					cps.login_attempt_count,
					cps.last_login_attempt_on,
					cps.dont_allow_login,
					cps.updated_by,
					cps.created_by,
					cps.created_on
				FROM
					customer_portal_settings cps
					JOIN lease_customers lc ON cps.customer_id = lc.customer_id AND cps.cid = lc.cid
					JOIN leases l ON lc.lease_id = l.id AND lc.cid = l.cid
				WHERE
					cps.username IS NOT NULL
					AND cps.username <> \'\'
					AND lc.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CANCELLED . ' )
					' . $strWhereClause . '
				ORDER BY
					cps.created_on, cps.cid, l.property_id, cps.username
				LIMIT ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset;

		/*
		 *
		 * (
						SELECT
							  *
						  FROM
							(
							SELECT
								lct.cid, lct.property_id, lct.customer_id, lct.lease_status_type_id,
								rank() OVER (
									PARTITION BY lct.cid, lct.customer_id
									ORDER BY
										CASE lease_status_type_id
											WHEN ' . CLeaseStatusType::CURRENT   . ' THEN 1
											WHEN ' . CLeaseStatusType::NOTICE    . ' THEN 2
											WHEN ' . CLeaseStatusType::FUTURE    . ' THEN 3
											WHEN ' . CLeaseStatusType::APPLICANT . ' THEN 4
											WHEN ' . CLeaseStatusType::PAST      . ' THEN 5
										END,
										lct.id DESC
								) AS status_rank
							FROM lease_customers lct
							WHERE lease_status_type_id NOT IN ( ' . CLeaseStatusType::CANCELLED . ' )
						  ) as lc_sub where status_rank = 1

					)
		 */

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCancelledCustomerPortalSettingsForGlobalUsers( $objDatabase, $intLimit = 5000, $intOffset = 0, $intDaySpan = 1, $boolUpdatedOnly = false ) {
		$strWhereClause = '';
		if( true == $boolUpdatedOnly ) {
			$strWhereClause = 'AND lc.updated_on BETWEEN NOW() - INTERVAL \'' . ( int ) $intDaySpan . ' day\' AND NOW() ';
		}
		$strSql = 'SELECT DISTINCT ON ( cps.created_on, cps.cid, lc.property_id, cps.username )
					cps.cid || \',\' || cps.customer_id || \',\' || lc.property_id || \',\' || cps.username AS user_data
				FROM
					customer_portal_settings cps
					JOIN lease_customers lc ON cps.customer_id = lc.customer_id AND cps.cid = lc.cid
				WHERE
					cps.username IS NOT NULL
					AND cps.username <> \'\'
					' . $strWhereClause . '
					AND lc.lease_status_type_id IN ( ' . CLeaseStatusType::CANCELLED . ' )
					AND NOT EXISTS (
						select * from lease_customers lc2
						where lc2.customer_id = lc.customer_id and lc2.cid = lc.cid
						AND lc2.property_id = lc.property_id
						AND lc2.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CANCELLED . ' )
					)
				LIMIT ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMissingMultiPropCustomerPortalSettings( $arrstrMultiPropertyGlobalUsers, $objDatabase ) {
		$arrstrMultiPropGlobalUsersToCheck = [];
		foreach( $arrstrMultiPropertyGlobalUsers as $arrstrMultiPropertyGlobalUser ) {
			array_push( $arrstrMultiPropGlobalUsersToCheck, pg_escape_string( $arrstrMultiPropertyGlobalUser['user_data'] ) );
		}

		$strSql = '
				WITH multi_prop_customers AS (
					SELECT
						(string_to_array(myarray.t, \',\'))[1]::integer AS cid,
						(string_to_array(myarray.t, \',\'))[2]::integer AS customer_id,
						(string_to_array(myarray.t, \',\'))[3]::integer AS property_id,
						(string_to_array(myarray.t, \',\'))[4] AS username
					FROM (
						SELECT unnest(
							ARRAY[' .
								'\'' . implode( '\',\'', $arrstrMultiPropGlobalUsersToCheck ) . '\'
							]
						) AS t
					) AS myarray
				)
				SELECT
					mpc.cid || \',\' ||
					mpc.customer_id || \',\' ||
					mpc.property_id || \',\' ||
					mpc.username AS user_data
				FROM multi_prop_customers mpc
				LEFT JOIN customer_portal_settings cps ON mpc.cid = cps.cid AND mpc.customer_id = cps.customer_id AND mpc.username = cps.username
				LEFT JOIN lease_customers lc ON lc.cid = cps.cid and lc.customer_id = cps.customer_id AND lc.property_id = mpc.property_id
				WHERE lc.id IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCountCustomerPortalSettingByUsernameByCid( $strUsername, $intCid, $objDatabase ) {
		$strSql = 'SELECT
					count( cps.id ) AS total_count
				FROM
					customer_portal_settings cps
					JOIN lease_customers lc ON cps.customer_id = lc.customer_id AND cps.cid = lc.cid
				WHERE
					cps.cid = ' . ( int ) $intCid . '
					AND lower( cps.username ) = \'' . \Psi\CStringService::singleton()->strtolower( trim( addslashes( $strUsername ) ) ) . '\'
					AND lc.lease_status_type_id != ' . CLeaseStatusType::CANCELLED . '
					AND cps.password_encrypted IS NOT NULL LIMIT 1';

		return self::fetchColumn( $strSql, 'total_count', $objDatabase );
	}

	public static function fetchCustomCustomerPortalSettingDataByUsernameByCidByPropertyId( $strUsername, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cps.customer_id,
						lc.lease_id
					FROM  customer_portal_settings cps
						JOIN lease_customers lc ON( cps.customer_id = lc.customer_id AND cps.cid = lc.cid )
						JOIN leases l ON( l.id = lc.lease_id AND lc.cid = l.cid )
					WHERE
						lower( cps.username ) = \'' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '\'
						AND cps.cid = ' . ( int ) $intCid . '
						AND lc.lease_status_type_id IN (' . CLeaseStatusType::CURRENT . ' , ' . CLeaseStatusType::NOTICE . ')
					LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

}
?>