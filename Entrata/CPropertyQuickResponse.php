<?php

class CPropertyQuickResponse extends CBasePropertyQuickResponse {

	protected $m_strName;
	protected $m_strMessage;
	protected $m_strSubject;

    /**
     * Custom get and set functions
     */

	public function getName() {
		return $this->m_strName;
	}

	public function getMessage() {
		return $this->m_strMessage;
	}

	public function getAssociationText() {
		return $this->m_strAssociationText;
	}

	public function getSubject() {
		return $this->m_strSubject;
	}

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	public function setMessage( $strMessage ) {
		$this->m_strMessage = $strMessage;
	}

	public function setAssociationText( $strAssociationText ) {
		$this->m_strAssociationText = $strAssociationText;
	}

	public function setSubject( $strSubject ) {
		$this->m_strSubject = $strSubject;
	}

	/**
	 * Validation functions
	 */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valQuickResponseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    /**
     * Set functions
     */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name'] ) )	$this->setName( $arrmixValues['name'] );
		if( true == isset( $arrmixValues['message'] ) )	$this->setMessage( $arrmixValues['message'] );
		if( true == isset( $arrmixValues['subject'] ) )	$this->setSubject( $arrmixValues['subject'] );

		return;
	}
}
?>