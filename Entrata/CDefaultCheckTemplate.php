<?php

class CDefaultCheckTemplate extends CBaseDefaultCheckTemplate {

    public function valId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valSystemCode() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getSystemCode() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_code', '' ) );
        // }

        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getName() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getDescription() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', '' ) );
        // }

        return $boolIsValid;
    }

    public function valTopMargin() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getTopMargin() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'top_margin', '' ) );
        // }

        return $boolIsValid;
    }

    public function valLeftMargin() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getLeftMargin() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'left_margin', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPrintStubCheckNumber() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getPrintStubCheckNumber() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'print_stub_check_number', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPrintStubBankInfo() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getPrintStubBankInfo() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'print_stub_bank_info', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCenterCompanyInfo() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getCenterCompanyInfo() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'center_company_info', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCenterBankInfo() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getCenterBankInfo() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'center_bank_info', '' ) );
        // }

        return $boolIsValid;
    }

    public function valThreeChecksPerPage() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getThreeChecksPerPage() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'three_checks_per_page', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsAboveStubs() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getIsAboveStubs() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_above_stubs', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsPrePrinted() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getIsPrePrinted() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_pre_printed', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsDefault() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getIsDefault() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_default', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getIsSystem() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_system', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsDisabled() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getIsDisabled() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_disabled', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>