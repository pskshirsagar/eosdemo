<?php

class CCustomerSubsidyDetail extends CBaseCustomerSubsidyDetail {

	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strMiddleInitial;
	protected $m_strBirthDate;
	protected $m_strCustomerRelationshipCode;
	protected $m_strGender;
	protected $m_strIdentificationCode;
	protected $m_strSpecialStatusCode;
	protected $m_strMembercitizenshipCode;
	protected $m_strAlienRegistrationNumber;
	protected $m_strAbleToWorkCareCode;
	protected $m_strEthnicity;
	protected $m_strStudentStatus;
	protected $m_strSsnException;
	protected $m_strPuertoRican;
	protected $m_strCuban;
	protected $m_strMexican;
	protected $m_strAnotherHispanic;
	protected $m_strRaceAmerican;
	protected $m_strRaceAsian;
	protected $m_strRaceBlack;
	protected $m_strRaceNative;
	protected $m_strRaceWhite;
	protected $m_strRaceOther;
	protected $m_strRaceDeclined;
	protected $m_strAsianIndia;
	protected $m_strJapanese;
	protected $m_strChinese;
	protected $m_strKorean;
	protected $m_strFilipino;
	protected $m_strVietnamese;
	protected $m_strOtherAsian;
	protected $m_strNativeHawaiian;
	protected $m_strSamoan;
	protected $m_strGuamanian;
	protected $m_strOtherPacificIslander;
	protected $m_strSubsidyCertificationEffectiveDate;
	protected $m_strHeadOfHouseholdIdCode;
	protected $m_strCorrectionType;
	protected $m_strEivIndicator;
	protected $m_strSsnNumber;
	protected $m_strDescription;
	protected $m_strDateOfDeath;
	protected $m_strMoveOutCode;
	protected $m_strTerminationCode;
	protected $m_strTransactionType;
	protected $m_strAnticipatedVoucherDate;

	protected $m_intUnitNumber;
	protected $m_intMemberNumber;

	protected $m_boolIsHouseholdReceiveWelfareRent;

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == valStr( $arrmixValues['head_of_household_id_code'] ) ) {
			$objCustomer = new CCustomer();
			$objCustomer->setTaxNumberEncrypted( $arrmixValues['head_of_household_id_code'] );
		}

		if( true == isset( $arrmixValues['member_number'] ) )						$this->setMemberNumber( $arrmixValues['member_number'] );
		if( true == isset( $arrmixValues['name_first'] ) )							$this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) )							$this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['middle_initial'] ) ) 						$this->setMiddleInitial( $arrmixValues['middle_initial'] );
		if( true == isset( $arrmixValues['customer_relationship_code'] ) ) 		    $this->setCustomerRelationshipCode( $arrmixValues['customer_relationship_code'] );
		if( true == isset( $arrmixValues['birth_date'] ) )							$this->setBirthDate( $arrmixValues['birth_date'] );
		if( true == isset( $arrmixValues['gender'] ) ) 								$this->setGender( $arrmixValues['gender'] );
		if( true == isset( $arrmixValues['identification_code'] ) ) 				$this->setIdentificationCode( $arrmixValues['identification_code'] );
		if( true == isset( $arrmixValues['special_status_code'] ) ) 				$this->setSpecialStatusCode( $arrmixValues['special_status_code'] );
		if( true == isset( $arrmixValues['member_citizenship_code'] ) ) 		    $this->setMembercitizenshipCode( $arrmixValues['member_citizenship_code'] );
		if( true == isset( $arrmixValues['alien_registration_number'] ) ) 			$this->setAlienRegistrationNumber( $arrmixValues['alien_registration_number'] );
		if( true == isset( $arrmixValues['able_to_work_care_code'] ) ) 				$this->setAbleToWorkCareCode( $arrmixValues['able_to_work_care_code'] );
		if( true == isset( $arrmixValues['ethnicity'] ) ) 							$this->setEthnicity( $arrmixValues['ethnicity'] );
		if( true == isset( $arrmixValues['student_status'] ) ) 						$this->setStudentStatus( $arrmixValues['student_status'] );
		if( true == isset( $arrmixValues['ssn_exception'] ) ) 						$this->setSsnException( $arrmixValues['ssn_exception'] );
		if( true == isset( $arrmixValues['puerto_rican'] ) ) 						$this->setPuertoRican( $arrmixValues['puerto_rican'] );
		if( true == isset( $arrmixValues['cuban'] ) ) 								$this->setCuban( $arrmixValues['cuban'] );
		if( true == isset( $arrmixValues['mexican'] ) ) 							$this->setMexican( $arrmixValues['mexican'] );
		if( true == isset( $arrmixValues['another_hispanic'] ) ) 					$this->setAnotherHispanic( $arrmixValues['another_hispanic'] );
		if( true == isset( $arrmixValues['race_american'] ) ) 						$this->setRaceAmerican( $arrmixValues['race_american'] );
		if( true == isset( $arrmixValues['race_asian'] ) ) 							$this->setRaceAsian( $arrmixValues['race_asian'] );
		if( true == isset( $arrmixValues['race_black'] ) ) 							$this->setRaceBlack( $arrmixValues['race_black'] );
		if( true == isset( $arrmixValues['race_native'] ) ) 						$this->setRaceNative( $arrmixValues['race_native'] );
		if( true == isset( $arrmixValues['race_white'] ) ) 							$this->setRaceWhite( $arrmixValues['race_white'] );
		if( true == isset( $arrmixValues['race_other'] ) ) 							$this->setRaceOther( $arrmixValues['race_other'] );
		if( true == isset( $arrmixValues['race_declined'] ) ) 						$this->setRaceDeclined( $arrmixValues['race_declined'] );
		if( true == isset( $arrmixValues['asian_india'] ) ) 						$this->setAsianIndia( $arrmixValues['asian_india'] );
		if( true == isset( $arrmixValues['japanese'] ) ) 							$this->setJapanese( $arrmixValues['japanese'] );
		if( true == isset( $arrmixValues['chinese'] ) ) 							$this->setChinese( $arrmixValues['chinese'] );
		if( true == isset( $arrmixValues['korean'] ) ) 								$this->setKorean( $arrmixValues['korean'] );
		if( true == isset( $arrmixValues['filipino'] ) ) 							$this->setFilipino( $arrmixValues['filipino'] );
		if( true == isset( $arrmixValues['vietnamese'] ) ) 							$this->setVietnamese( $arrmixValues['vietnamese'] );
		if( true == isset( $arrmixValues['other_asian'] ) ) 						$this->setOtherAsian( $arrmixValues['other_asian'] );
		if( true == isset( $arrmixValues['native_hawaiian'] ) ) 					$this->setNativeHawaiian( $arrmixValues['native_hawaiian'] );
		if( true == isset( $arrmixValues['samoan'] ) ) 								$this->setSamoan( $arrmixValues['samoan'] );
		if( true == isset( $arrmixValues['guamanian'] ) ) 							$this->setGuamanian( $arrmixValues['guamanian'] );
		if( true == isset( $arrmixValues['other_pacific_islander'] ) ) 				$this->setOtherPacificIslander( $arrmixValues['other_pacific_islander'] );
		if( true == isset( $arrmixValues['is_household_receive_welfare_rent'] ) ) 	$this->setIsHouseholdReceiveWelfareRent( $arrmixValues['is_household_receive_welfare_rent'] );
		if( true == isset( $arrmixValues['unit_number'] ) ) 						$this->setUnitNumber( $arrmixValues['unit_number'] );
		if( true == isset( $arrmixValues['eiv_indicator'] ) ) 						$this->setEivIndicator( $arrmixValues['eiv_indicator'] );
		if( true == isset( $arrmixValues['correction_type'] ) ) 					$this->setCorrectionType( $arrmixValues['correction_type'] );
		if( true == isset( $arrmixValues['head_of_household_id_code'] ) )           $this->setHeadOfHouseholdIdCode( $objCustomer->getTaxNumber() );
		if( true == isset( $arrmixValues['subsidy_certification_effective_date'] ) ) $this->setSubsidyCertificationEffectiveDate( $arrmixValues['subsidy_certification_effective_date'] );
		if( true == isset( $arrmixValues['ssn_number'] ) ) 			                $this->setSsnNumber( $arrmixValues['ssn_number'] );
		if( true == isset( $arrmixValues['description'] ) ) 			            $this->setDescription( $arrmixValues['description'] );
		if( true == isset( $arrmixValues['date_of_death'] ) ) 			            $this->setDateOfDeath( $arrmixValues['date_of_death'] );
		if( true == isset( $arrmixValues['move_out_code'] ) ) 			            $this->setMoveOutCode( $arrmixValues['move_out_code'] );
		if( true == isset( $arrmixValues['termination_code'] ) ) 			        $this->setTerminationCode( $arrmixValues['termination_code'] );
		if( true == isset( $arrmixValues['transaction_type'] ) ) 			        $this->setTransactionType( $arrmixValues['transaction_type'] );
		if( true == isset( $arrmixValues['anticipated_voucher_date'] ) ) 			$this->setAnticipatedVoucherDate( $arrmixValues['anticipated_voucher_date'] );

		return;
	}

	public function setMemberNumber( $intMemberNumber ) {
		$this->m_intMemberNumber = $intMemberNumber;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setMiddleInitial( $strMiddleInitial ) {
		$this->m_strMiddleInitial = $strMiddleInitial;
	}

	public function setBirthDate( $strBirthDate ) {
		$this->m_strBirthDate = $strBirthDate;
	}

	public function setCustomerRelationshipCode( $strCustomerRelationshipCode ) {
		$this->m_strCustomerRelationshipCode = $strCustomerRelationshipCode;
	}

	public function setGender( $strGender ) {
		$this->m_strGender = $strGender;
	}

	public function setIdentificationCode( $strIdentificationCode ) {
		$this->m_strIdentificationCode = $strIdentificationCode;
	}

	public function setSpecialStatusCode( $strSpecialStatusCode ) {
		$this->m_strSpecialStatusCode = $strSpecialStatusCode;
	}

	public function setMembercitizenshipCode( $strMembercitizenshipCode ) {
		$this->m_strMembercitizenshipCode = $strMembercitizenshipCode;
	}

	public function setAlienRegistrationNumber( $strAlienRegistrationNumber ) {
		$this->m_strAlienRegistrationNumber = $strAlienRegistrationNumber;
	}

	public function setAbleToWorkCareCode( $strAbleToWorkCareCode ) {
		$this->m_strAbleToWorkCareCode = $strAbleToWorkCareCode;
	}

	public function setEthnicity( $strEthnicity ) {
		$this->m_strEthnicity = $strEthnicity;
	}

	public function setStudentStatus( $strStudentStatus ) {
		$this->m_strStudentStatus = $strStudentStatus;
	}

	public function setSsnException( $strSsnException ) {
		$this->m_strSsnException = $strSsnException;
	}

	public function setPuertoRican( $strPuertoRican ) {
		$this->m_strPuertoRican = $strPuertoRican;
	}

	public function setCuban( $strCuban ) {
		$this->m_strCuban = $strCuban;
	}

	public function setMexican( $strMexican ) {
		$this->m_strMexican = $strMexican;
	}

	public function setAnotherHispanic( $strAnotherHispanic ) {
		$this->m_strAnotherHispanic = $strAnotherHispanic;
	}

	public function setRaceAmerican( $strRaceAmerican ) {
		$this->m_strRaceAmerican = $strRaceAmerican;
	}

	public function setRaceAsian( $strRaceAsian ) {
		$this->m_strRaceAsian = $strRaceAsian;
	}

	public function setRaceBlack( $strRaceBlack ) {
		$this->m_strRaceBlack = $strRaceBlack;
	}

	public function setRaceNative( $strRaceNative ) {
		$this->m_strRaceNative = $strRaceNative;
	}

	public function setRaceWhite( $strRaceWhite ) {
		$this->m_strRaceWhite = $strRaceWhite;
	}

	public function setRaceOther( $strRaceOther ) {
		$this->m_strRaceOther = $strRaceOther;
	}

	public function setRaceDeclined( $strRaceDeclined ) {
		$this->m_strRaceDeclined = $strRaceDeclined;
	}

	public function setAsianIndia( $strAsianIndia ) {
		$this->m_strAsianIndia = $strAsianIndia;
	}

	public function setJapanese( $strJapanese ) {
		$this->m_strJapanese = $strJapanese;
	}

	public function setChinese( $strChinese ) {
		$this->m_strChinese = $strChinese;
	}

	public function setKorean( $strKorean ) {
		$this->m_strKorean = $strKorean;
	}

	public function setFilipino( $strFilipino ) {
		$this->m_strFilipino = $strFilipino;
	}

	public function setVietnamese( $strVietnamese ) {
		$this->m_strVietnamese = $strVietnamese;
	}

	public function setOtherAsian( $strOtherAsian ) {
		$this->m_strOtherAsian = $strOtherAsian;
	}

	public function setNativeHawaiian( $strNativeHawaiian ) {
		$this->m_strNativeHawaiian = $strNativeHawaiian;
	}

	public function setSamoan( $strSamoan ) {
		$this->m_strSamoan = $strSamoan;
	}

	public function setGuamanian( $strGuamanian ) {
		$this->m_strGuamanian = $strGuamanian;
	}

	public function setOtherPacificIslander( $strOtherPacificIslander ) {
		$this->m_strOtherPacificIslander = $strOtherPacificIslander;
	}

	public function setIsHouseholdReceiveWelfareRent( $boolIsHouseholdReceiveWelfareRent ) {
		$this->m_boolIsHouseholdReceiveWelfareRent = $boolIsHouseholdReceiveWelfareRent;
	}

	public function setUnitNumber( $intUnitNumber ) {
		$this->m_intUnitNumber = $intUnitNumber;
	}

	public function setEivIndicator( $strEivIndicator ) {
		$this->m_strEivIndicator = $strEivIndicator;
	}

	public function setCorrectionType( $strCorrectionType ) {
		$this->m_strCorrectionType = $strCorrectionType;
	}

	public function setHeadOfHouseholdIdCode( $strHeadOfHouseholdIdCode ) {
		$this->m_strHeadOfHouseholdIdCode = $strHeadOfHouseholdIdCode;
	}

	public function setSubsidyCertificationEffectiveDate( $strSubsidyCertificationEffectiveDate ) {
		$this->m_strSubsidyCertificationEffectiveDate = $strSubsidyCertificationEffectiveDate;
	}

	public function setSsnNumber( $strSsnNumber ) {
		$this->m_strSsnNumber = $strSsnNumber;
	}

	public function setDescription( $strDescription ) {
		$this->m_strDescription = $strDescription;
	}

	public function setDateOfDeath( $strDateOfDeath ) {
		$this->m_strDateOfDeath = $strDateOfDeath;
	}

	public function setMoveOutCode( $strMoveOutCode ) {
		$this->m_strMoveOutCode = $strMoveOutCode;
	}

	public function setTerminationCode( $strTerminationCode ) {
		$this->m_strTerminationCode = $strTerminationCode;
	}

	public function setTransactionType( $strTransactionType ) {
		$this->m_strTransactionType = $strTransactionType;
	}

	public function setAnticipatedVoucherDate( $strAnticipatedVoucherDate ) {
		$this->m_strAnticipatedVoucherDate = $strAnticipatedVoucherDate;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getMemberNumber() {
		return $this->m_intMemberNumber;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getMiddleInitial() {
		return $this->m_strMiddleInitial;
	}

	public function getBirthDate() {
		return $this->m_strBirthDate;
	}

	public function getCustomerRelationshipCode() {
		return $this->m_strCustomerRelationshipCode;
	}

	public function getGender() {
		return $this->m_strGender;
	}

	public function getIdentificationCode() {
		return $this->m_strIdentificationCode;
	}

	public function getSpecialStatusCode() {
		return $this->m_strSpecialStatusCode;
	}

	public function getMembercitizenshipCode() {
		return $this->m_strMembercitizenshipCode;
	}

	public function getAlienRegistrationNumber() {
		return $this->m_strAlienRegistrationNumber;
	}

	public function getAbleToWorkCareCode() {
		return $this->m_strAbleToWorkCareCode;
	}

	public function getEthnicity() {
		return $this->m_strEthnicity;
	}

	public function getStudentStatus() {
		return $this->m_strStudentStatus;
	}

	public function getSsnException() {
		return $this->m_strSsnException;
	}

	public function getPuertoRican() {
		return $this->m_strPuertoRican;
	}

	public function getCuban() {
		return $this->m_strCuban;
	}

	public function getMexican() {
		return $this->m_strMexican;
	}

	public function getAnotherHispanic() {
		return $this->m_strAnotherHispanic;
	}

	public function getRaceAmerican() {
		return $this->m_strRaceAmerican;
	}

	public function getRaceAsian() {
		return $this->m_strRaceAsian;
	}

	public function getRaceBlack() {
		return $this->m_strRaceBlack;
	}

	public function getRaceNative() {
		return $this->m_strRaceNative;
	}

	public function getRaceWhite() {
		return $this->m_strRaceWhite;
	}

	public function getRaceOther() {
		return $this->m_strRaceOther;
	}

	public function getRaceDeclined() {
		return $this->m_strRaceDeclined;
	}

	public function getAsianIndia() {
		return $this->m_strAsianIndia;
	}

	public function getJapanese() {
		return $this->m_strJapanese;
	}

	public function getChinese() {
		return $this->m_strChinese;
	}

	public function getKorean() {
		return $this->m_strKorean;
	}

	public function getFilipino() {
		return $this->m_strFilipino;
	}

	public function getVietnamese() {
		return $this->m_strVietnamese;
	}

	public function getOtherAsian() {
		return $this->m_strOtherAsian;
	}

	public function getNativeHawaiian() {
		return $this->m_strNativeHawaiian;
	}

	public function getSamoan() {
		return $this->m_strSamoan;
	}

	public function getGuamanian() {
		return $this->m_strGuamanian;
	}

	public function getOtherPacificIslander() {
		return $this->m_strOtherPacificIslander;
	}

	public function getUnitNumber() {
		return $this->m_intUnitNumber;
	}

	public function getEivIndicator() {
		return $this->m_strEivIndicator;
	}

	public function getCorrectionType() {
		return $this->m_strCorrectionType;
	}

	public function getHeadOfHouseholdIdCode() {
		return $this->m_strHeadOfHouseholdIdCode;
	}

	public function getSubsidyCertificationEffectiveDate() {
		return $this->m_strSubsidyCertificationEffectiveDate;
	}

	public function getRecentLog( $objDatabase ) {
		$objCustomerSubsidyDetailLog = CCustomerSubsidyDetailLogs::fetchRecentCustomerSubsidyDetailLogByCustomerIdByIdByCid( $this->getCustomerId(), $this->getId(), $this->getCid(), $objDatabase );
		if( true == valObj( $objCustomerSubsidyDetailLog, 'CCustomerSubsidyDetailLog' ) ) {
			return $objCustomerSubsidyDetailLog;
		}

		return NULL;
	}

	public function getIsHouseholdReceiveWelfareRent() {
		return $this->m_boolIsHouseholdReceiveWelfareRent;
	}

	public function getSsnNumber() {
		return $this->m_strSsnNumber;
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function getDateOfDeath() {
		return $this->m_strDateOfDeath;
	}

	public function getMoveOutCode() {
		return $this->m_strMoveOutCode;
	}

	public function getTerminationCode() {
		return $this->m_strTerminationCode;
	}

	public function getTransactionType() {
		return $this->m_strTransactionType;
	}

	public function getAnticipatedVoucherDate() {
		return $this->m_strAnticipatedVoucherDate;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyCitizenshipTypeId( $boolIsValidateRequired ) {
		$boolIsValid = true;
		if( false == valStr( $this->getSubsidyCitizenshipTypeId() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_citizenship_type_id', 'Citizenship is required.', '', [ 'label' => 'Citizenship', 'step_id' => CApplicationStep::BASIC_INFO ] ) ] );
		}
		return $boolIsValid;
	}

	public function valSubsidyEthnicityTypeId( $boolIsValidateRequired ) {
		$boolIsValid = true;
		if( false == valStr( $this->getSubsidyEthnicityTypeId() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_ethnicity_type_id', 'Ethnicity is required.', '', [ 'label' => 'Demographic Information', 'step_id' => CApplicationStep::BASIC_INFO ] ) ] );
		}

		return $boolIsValid;
	}

	public function valSubsidyEthnicitySubTypeId( $boolIsValidateRequired ) {
		$boolIsValid = true;
		if( \CSubsidyEthnicityType::HISPANIC == $this->getSubsidyEthnicityTypeId() ) {
			if( false == valStr( $this->getSubsidyEthnicitySubTypeId() ) ) {
				if( true == $boolIsValidateRequired ) {
					$boolIsValid = false;
				}
				$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_ethnicity_sub_type_id', CSubsidyEthnicityType::getTypeNameByTypeId( $this->getSubsidyEthnicityTypeId() ) . ' Sub-type is required.', '', [ 'label' => 'Demographic Information', 'step_id' => CApplicationStep::BASIC_INFO ] ) ] );
			}
		}

		return $boolIsValid;
	}

	public function valSubsidySsnExceptionTypeId( $boolIsOccupantSSN, $boolIsValidateRequired ) {
		$boolIsValid = true;
		if( true == $boolIsOccupantSSN ) {
			if( false == valStr( $this->getSubsidySsnExceptionTypeId() ) ) {
				if( true == $boolIsValidateRequired ) {
					$boolIsValid = false;
				}
				$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_ssn_exception_type_id', 'SSN exception type is required.', '',  [ 'label' => 'SSN Exception Type Id', 'step_id' => CApplicationStep::BASIC_INFO ] ) ] );
			}
		}
		return $boolIsValid;
	}

	public function valSubsidyDependentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAlienRegistrationNumber() {
		$boolIsValid = true;
		if( true == valStr( $this->getAlienRegistrationNumber() ) ) {
			if( false == ctype_alnum( $this->getAlienRegistrationNumber() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'alien_registration_number', ' Alien registration number should be alphanumeric value.' ) );
			} elseif( 10 != strlen( trim( $this->getAlienRegistrationNumber() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'alien_registration_number', ' Alien registration number must be 10 characters.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valTracsId( $boolIsOccupantSSN, $boolIsValidateRequired ) {
		$boolIsValid = true;
		if( true == $boolIsOccupantSSN ) {
			if( false == valStr( $this->getTracsIdEncrypted() ) ) {
				if( true == $boolIsValidateRequired ) {
					$boolIsValid = false;
				}
				$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'tracs_id', 'TRACS ID is required.', '',  [ 'label' => 'Tracs id', 'step_id' => CApplicationStep::BASIC_INFO ] ) ] );
			} elseif( false == ctype_alnum( $this->getTracsIdEncrypted() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tracs_id', ' TRACS ID should be alphanumeric value.' ) );
			} elseif( 9 != strlen( trim( $this->getTracsIdEncrypted() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tracs_id', ' TRACS ID must be 9 characters.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valHudSpecifyGender() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPartTimeStudent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPoliceOrSecurityOfficer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStudentException( $boolIsValidateRequired, $objDatabase, $boolIsStudentExceptionRequired ) {

		$arrobjCustomerSubsidySpecialStatusTypes = \Psi\Eos\Entrata\CCustomerSubsidySpecialStatusTypes::createService()->fetchActiveCustomerSubsidySpecialStatusTypesByCustomerIdsBySubsidySpecialStatusTypeIdByCId( [ $this->getCustomerId() ], \CSubsidySpecialStatusType::FULL_TIME_STUDENT, $this->getCid(), $objDatabase );
		$boolIsValid = true;

		// Student exception only required if the student is full-time.
		if( valArr( $arrobjCustomerSubsidySpecialStatusTypes ) ) {

			$arrobjCustomerSubsidySpecialStatusTypes = rekeyObjects( 'SubsidySpecialStatusTypeId', $arrobjCustomerSubsidySpecialStatusTypes );
			$objCustomerSubsidySpecialStatusType = $arrobjCustomerSubsidySpecialStatusTypes[\CSubsidySpecialStatusType::FULL_TIME_STUDENT];
			if( true == valObj( $objCustomerSubsidySpecialStatusType, 'CCustomerSubsidySpecialStatusType' ) ) {

				if( true == valStr( $this->getStudentException() ) || true == $this->getIsPartTimeStudent() ) {

					return $boolIsValid;
				} else {
					if( true == $boolIsValidateRequired && true == $boolIsStudentExceptionRequired ) {
						$boolIsValid = false;
					}
					if( true == $boolIsStudentExceptionRequired ) {
						$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'student_exception', 'Citizenship is required.', '', [ 'label' => 'Student Exception', 'step_id' => CApplicationStep::BASIC_INFO ] ) ] );
					}

				}
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsOccupantSSN = false, $boolIsValidateRequired = true, $objDatabase = NULL, $boolIsStudentExceptionRequired = true ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valSubsidyEthnicityTypeId( $boolIsValidateRequired );
				$boolIsValid &= $this->valSubsidyEthnicitySubTypeId( $boolIsValidateRequired );
				break;

			case 'validate_secure_info_details':
				$boolIsValid &= $this->valAlienRegistrationNumber();
				$boolIsValid &= $this->valTracsId( $boolIsOccupantSSN, $boolIsValidateRequired );
				break;

			case 'validate_citizenship_information':
				$boolIsValid &= $this->valAlienRegistrationNumber();
				$boolIsValid &= $this->valTracsId( $boolIsOccupantSSN, $boolIsValidateRequired );
				$boolIsValid &= $this->valSubsidyCitizenshipTypeId( $boolIsValidateRequired );
				$boolIsValid &= $this->valSubsidySsnExceptionTypeId( $boolIsOccupantSSN, $boolIsValidateRequired );
				$boolIsValid &= $this->valSubsidyEthnicityTypeId( $boolIsValidateRequired );
				$boolIsValid &= $this->valSubsidyEthnicitySubTypeId( $boolIsValidateRequired );
				break;

			case 'validate_student_exception':
				if( valObj( $objDatabase, 'CDatabase' ) ) $boolIsValid &= $this->valStudentException( $boolIsValidateRequired, $objDatabase, $boolIsStudentExceptionRequired );
				$boolIsValid &= $this->valSubsidyEthnicityTypeId( $boolIsValidateRequired );
				$boolIsValid &= $this->valSubsidyEthnicitySubTypeId( $boolIsValidateRequired );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
