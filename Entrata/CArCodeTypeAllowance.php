<?php

class CArCodeTypeAllowance extends CBaseArCodeTypeAllowance {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArCodeTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReferenceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsAllowed() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsGlAccountUsageType() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsGlAccountType() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsCreditAllowance() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>