<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyPreferenceTypes
 * Do not add any new functions to this class.
 */

class CPropertyPreferenceTypes extends CBasePropertyPreferenceTypes {

	public static function fetchPropertyPreferenceTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPropertyPreferenceType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPropertyPreferenceType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPropertyPreferenceType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>