<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlAccountUsageTypes
 * Do not add any new functions to this class.
 */

class CGlAccountUsageTypes extends CBaseGlAccountUsageTypes {

	public static function fetchGlAccountUsageTypes( $strSql, $objClientDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CGlAccountUsageType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchGlAccountUsageType( $strSql, $objClientDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGlAccountUsageType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedGlAccountUsageTypesByGlAccountTypeId( $intGlAccountTypeId, $objClientDatabase, $arrintExcludedGlAccountUsageTypeIds = [] ) {
		if( false == is_numeric( $intGlAccountTypeId ) ) return NULL;

		$strWhereSql = '';
		if( true === valArr( $arrintExcludedGlAccountUsageTypeIds ) ) {
			$strWhereSql = ' AND id NOT IN (' . implode( ',', $arrintExcludedGlAccountUsageTypeIds ) . ') ';
		}

		$strSql = 'SELECT
						*
					FROM
						gl_account_usage_types
					WHERE
						gl_account_type_id = ' . ( int ) $intGlAccountTypeId . '
						AND is_published = true
						' . $strWhereSql . '
					ORDER BY LOWER( name )';

		return self::fetchGlAccountUsageTypes( $strSql, $objClientDatabase );
	}

	public static function fetchPublishedGlAccountUsageTypes( $objClientDatabase ) {
		return self::fetchGlAccountUsageTypes( 'SELECT * FROM gl_account_usage_types WHERE is_published = true ORDER BY LOWER( name )', $objClientDatabase );
	}

}
?>