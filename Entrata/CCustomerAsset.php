<?php

class CCustomerAsset extends CBaseCustomerAsset {

	const FUTURE			= 1;
	const CURRENT			= 2;
	const PAST				= 3;

	protected $m_boolIsDisposed;
	protected $m_boolIsDelete;
	protected $m_boolCustomerAssetTypeSelected;
	protected $m_boolIsPureAffordableLease;

	protected $m_intMemberNumber;
	protected $m_intApplicationId;

	protected $m_strAssetStatus;
	protected $m_strRequiredQuestion;
	protected $m_strStatus;
	protected $m_strAction;

	protected $m_arrstrAllAssetStatusTypes;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getAllAssetStatusTypes() {

		if( true == valArr( $this->m_arrstrAllAssetStatusTypes ) )
			return $this->m_arrstrAllAssetStatusTypes;

		$this->m_arrstrAllAssetStatusTypes = [
			self::FUTURE	=> __( 'Future' ),
			self::CURRENT	=> __( 'Current' ),
			self::PAST		=> __( 'Past' )
		];

		return $this->m_arrstrAllAssetStatusTypes;
	}

	/**
	 * get Functions
	 */

	public function getIsDisposed() {
		return $this->m_boolIsDisposed;
	}

	public function getAccountNumber() {
		if( false == valStr( $this->m_strAccountNumberEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	public function getAccountNumberMasked() {
		$strAccountNumber 	= ( true == valStr( $this->m_strAccountNumberEncrypted ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] ) : NULL );
		$intStringLength 	= strlen( $strAccountNumber );
		$strLastFour 		= \Psi\CStringService::singleton()->substr( $strAccountNumber, -4 );
		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, '*', STR_PAD_LEFT );
	}

	public function getIsDelete() {
		return $this->m_boolIsDelete;
	}

	public function getCustomerAssetTypeSelected() {
		return $this->m_boolCustomerAssetTypeSelected;
	}

	public function getAssetStatus() {
		return $this->m_strAssetStatus;
	}

	public function getRecentLog( $objDatabase ) {
		$objCustomerAssetLog = CCustomerAssetLogs::fetchRecentCustomerAssetLogByCustomerIdByIdByCid( $this->getCustomerId(), $this->getId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objCustomerAssetLog, 'CCustomerAssetLog' ) ) {
			return $objCustomerAssetLog;
		}

		return NULL;
	}

	public function getRequiredQuestion( $boolIsSecondaryApplicant = false, $strCustomerFirstName = NULL, $strLabelCustomerAssetType = NULL ) {
		if( true == valStr( $this->m_strRequiredQuestion ) ) return $this->m_strRequiredQuestion;

		switch( $strLabelCustomerAssetType ) {
			case 'real estate sale':
				$this->m_strRequiredQuestion = ( true == $boolIsSecondaryApplicant ) ? __( 'Does {%s, customer_first_name } have a real estate sale ?', [ 'customer_first_name' => $strCustomerFirstName] ) : __( 'Do you have a real estate sale ?' );
				break;

			case 'ira or other retirement accounts':
				$this->m_strRequiredQuestion = ( true == $boolIsSecondaryApplicant ) ? __( 'Does {%s, customer_first_name } have IRA or other retirement accounts ?', [ 'customer_first_name' => $strCustomerFirstName] ) : __( 'Do you have IRA or other retirement accounts ?' );
				break;

			default:
				$this->m_strRequiredQuestion = ( true == $boolIsSecondaryApplicant ) ? __( 'Does {%s, customer_first_name } have {%s,asset_type_label} ?', [ 'customer_first_name' => $strCustomerFirstName, 'asset_type_label' => $strLabelCustomerAssetType ] ) : __( 'Do you have {%s, asset_type_label} ?', ['asset_type_label' => $strLabelCustomerAssetType] );

		}

		return $this->m_strRequiredQuestion;
	}

	public function getMemberNumber() {
		return $this->m_intMemberNumber;
	}

	public function getDefaultSectionName() {
		return 'Asset\'s';
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function getIsPureAffordableLease() {
		return $this->m_boolIsPureAffordableLease;
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	/**
	 * set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['is_delete'] ) )       $this->setIsDelete( $arrmixValues['is_delete'] );
		if( true == isset( $arrmixValues['is_disposed'] ) )     $this->setIsDisposed( $arrmixValues['is_disposed'] );
		if( true == isset( $arrmixValues['account_number'] ) )  $this->setAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_number'] ) : $arrmixValues['account_number'] );
		if( true == isset( $arrmixValues['asset_select'] ) )    $this->setCustomerAssetTypeSelected( $arrmixValues['asset_select'] );
		if( true == isset( $arrmixValues['asset_status'] ) )    $this->setAssetStatus( $arrmixValues['asset_status'] );
		if( true == isset( $arrmixValues['status'] ) )          $this->setStatus( $arrmixValues['status'] );
		if( true == isset( $arrmixValues['member_number'] ) ) 	$this->setMemberNumber( $arrmixValues['member_number'] );
	}

	public function setIsDisposed( $boolIsDisposed ) {
		$this->m_boolIsDisposed = $boolIsDisposed;
	}

	public function setIsDelete( $boolIsDelete ) {
		$this->m_boolIsDelete = $boolIsDelete;
	}

	public function setCustomerAssetTypeSelected( $boolCustomerAssetTypeSelected ) {
		$this->m_boolCustomerAssetTypeSelected = $boolCustomerAssetTypeSelected;
	}

	public function setAssetStatus( $strAssetStatus ) {
		$this->m_strAssetStatus = $strAssetStatus;
	}

	public function setAccountNumber( $strPlainAccountNumber ) {
		if ( true == \Psi\CStringService::singleton()->stristr( $strPlainAccountNumber, '*' ) ) return;
		$strPlainAccountNumber = CStrings::strTrimDef( $strPlainAccountNumber, 20, NULL, true );
		if( true == valStr( $strPlainAccountNumber ) ) {
			$this->setAccountNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
		}
	}

	public function setMemberNumber( $intMemberNumber ) {
		$this->m_intMemberNumber = $intMemberNumber;
	}

	public function setStatus( $strStatus ) {
		$this->m_strStatus = $strStatus;
	}

	public function setIsPureAffordableLease( $boolIsPureAffordableLease ) {
		$this->m_boolIsPureAffordableLease = $boolIsPureAffordableLease;
	}

	public function setAction( $strAction ) {
		$this->m_strAction = $strAction;
	}

	public function setApplicationId( $intApplicationId ) {
		$this->m_intApplicationId = $intApplicationId;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameOnAccount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountNumberEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRoutingNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStreetLine1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStreetLine2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStreetLine3() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionProvince() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactPhoneExtension() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactFaxNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCashValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActualIncome() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $objPreviousCustomerAsset = NULL ) {

		$boolIsValid = true;
		if( true == $this->getIsPureAffordableLease() && true == valStr( $this->getAction() ) ) {
			$boolIsValid = $this->insertEventsAndApplicantApplicationChangesForAffordable( $intCurrentUserId, $objDatabase, $objPreviousCustomerAsset );
		}

		if( true == $boolIsValid ) {
			if( true == is_null( $this->getId() ) ) {
				return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			} else {
				return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			}
		}

		return $boolIsValid;

	}

	/**
	 * Validate Function
	 */

	public function validate( $strAction, $boolValidateEndDate = false, $objCustomerAssetType = NULL, $arrobjPropertyApplicationPreferences = [], $arrstrCustomerAssetTypeCustomFields = [], $boolValidateRequiredFields= true ) {
		$boolIsValid = true;

		$objCustomerAssetValidator = new CCustomerAssetValidator();
		$objCustomerAssetValidator->setCustomerAsset( $this );
		$objCustomerAssetValidator->setValidateRequiredFields( $boolValidateRequiredFields );
		$boolIsValid &= $objCustomerAssetValidator->validate( $strAction, $boolValidateEndDate, $objCustomerAssetType, $arrobjPropertyApplicationPreferences, $arrstrCustomerAssetTypeCustomFields );

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsHardDelete = false, $boolReturnSqlOnly = false ) {

		$boolIsValid = true;
		if( true == $this->getIsPureAffordableLease() && true == valStr( $this->getAction() ) ) {
			$boolIsValid = $this->insertEventsAndApplicantApplicationChangesForAffordable( $intCurrentUserId, $objDatabase );
		}

		if( true == $boolIsValid ) {
			if( true == $boolIsHardDelete ) {
				return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			} else {
				$this->setDeletedBy( $intCurrentUserId );
				$this->setDeletedOn( 'NOW()' );
				return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			}
		}

		return $boolIsValid;

	}

	public function insertEventsAndApplicantApplicationChangesForAffordable( $intCurrentUserId, $objDatabase, $objPreviousCustomerAsset = NULL ) {

		$arrobjEvents = [];
		$arrstrActions = $this->checkAssetActionType( $objPreviousCustomerAsset );
		if( false == valArr( $arrstrActions ) ) {
			return true;
		}
		$objAffordableLibrary = $this->prepareAffordableLibrary( $intCurrentUserId, $objDatabase );
		foreach( $arrstrActions as $strAction ) {
			$arrobjEvents[] = $objAffordableLibrary->createEventAndApplicantApplicationChanges( CEventType::ASSET_CHANGE, $objPreviousCustomerAsset, $strAction );
		}
		return $objAffordableLibrary->insertEventsAndApplicantApplicationChanges( $arrobjEvents );
	}

	public function checkAssetActionType( $objPreviousCustomerAsset = NULL ) {

		$arrstrActions = [];
		if( 'add_asset' == $this->m_strAction ) {
			$this->m_strAction = ( false == valId( $this->getId() ) ) ? 'add_asset' : 'edit_asset';
		}

		if( true == in_array( $this->m_strAction, [ 'delete_asset', 'end_asset', 'add_asset' ] ) ) {
			$arrstrActions[]        = $this->m_strAction;
			if( 'add_asset' == $this->m_strAction && true == valStr( $this->getDateDivested() ) ) {
				$arrstrActions[]    = 'add_disposed';
			}
		} else {
			if( $objPreviousCustomerAsset->getActualIncome() != $this->getActualIncome() ) {
				$arrstrActions[] = 'edit_asset_amount';
			}
			if( $objPreviousCustomerAsset->getCustomerAssetTypeId() != $this->getCustomerAssetTypeId() ) {
				$arrstrActions[] = 'edit_asset_type';
			}
			if( false == valStr( $this->getDateDivested() ) && true == valStr( $objPreviousCustomerAsset->getDateDivested() ) ) {
				$arrstrActions[] = 'edit_asset_indisposed';
			}
			if( true == valStr( $this->getDateDivested() ) && false == valStr( $objPreviousCustomerAsset->getDateDivested() ) ) {
				$arrstrActions[] = 'edit_asset_disposed';
			}
			if( date( 'm/d/y', strtotime( $objPreviousCustomerAsset->getAssetEffectiveDate() ) ) != date( 'm/d/y', strtotime( $this->getAssetEffectiveDate() ) ) ) {
				$arrstrActions[] = 'edit_asset_effective_date';
			}
			if( 1 > \Psi\Libraries\UtilFunctions\count( $arrstrActions ) && false == in_array( $arrstrActions, [ 'edit_asset_amount', 'edit_asset_type', 'edit_asset_indisposed', 'edit_asset_disposed', 'edit_asset_effective_date' ] ) ) {
				return false;
			}
		}
		return $arrstrActions;
	}

	public function prepareAffordableLibrary( $intCurrentUserId, $objDatabase ) {

		$objApplication = CApplications::fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intCurrentUserId, $this->getCid(), $objDatabase );
		$objApplicant   = CApplicants::fetchApplicantByLeaseIdByCustomerIdByCid( $objApplication->getLeaseId(), $this->getCustomerId(), $this->getCid(), $objDatabase );
		$objLease       = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $objApplication->getLeaseId(), $this->getCid(), $objDatabase );

		require_once( PATH_LIBRARY_AFFORDABLE . 'CAffordableLibrary.class.php' );
		$objAffordableLibrary = new CAffordableLibrary();
		$objAffordableLibrary->setDatabase( $objDatabase );
		$objAffordableLibrary->setApplicantId( $objApplicant->getId() );
		$objAffordableLibrary->setApplication( $objApplication );
		$objAffordableLibrary->setCompanyUser( $objCompanyUser );
		$objAffordableLibrary->setCustomerAsset( $this );
		$objAffordableLibrary->setLease( $objLease );

		return $objAffordableLibrary;
	}

}
?>
