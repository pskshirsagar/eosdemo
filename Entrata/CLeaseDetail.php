<?php

class CLeaseDetail extends CBaseLeaseDetail {

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Company Lease Request:  client id required - CLeaseDetail::valCid()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;

    	if( true == is_null( $this->getLeaseId() ) || 0 >= ( int ) $this->getLeaseId() ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Company Lease Request:  Lease id required - CLeaseDetail::valLeaseId()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

	public function valLateFeePostedBy() {
        $boolIsValid = true;

        if( true == is_null( $this->getLateFeePostedBy() ) ) {
            $boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'late_fee_posted_by', __( 'Last fee posted by is required.' ) ) );
        }

        return $boolIsValid;
    }

	public function valLateFeePostedOn() {
        $boolIsValid = true;

		if( true == valStr( $this->getLateFeePostedOn() ) ) {
			$mixValidatedLateFeePostedOn = CValidation::checkISODateFormat( $this->getLateFeePostedOn(), $boolFormat = true );
			if( false === $mixValidatedLateFeePostedOn ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'late_fee_posted_on', __( 'Late fee posted on date must be in mm/dd/yyyy format.' ) ) );
			} else {
				$this->setLateFeePostedOn( date( 'm/d/Y', strtotime( $mixValidatedLateFeePostedOn ) ) );
			}
		}

        return $boolIsValid;
    }

    public function valNumberOfUnitKeys( $boolIsKeyMandatory = false ) {

    	$boolIsValid = true;

    	if( true == $boolIsKeyMandatory && false == valStr( $this->getNumberOfUnitKeys() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_unit_keys', __( 'The unit keys are required.' ) ) );
    		$boolIsValid &= false;
    	}

    	if( true == valStr( $this->getNumberOfUnitKeys() ) ) {
    		if( false == is_numeric( $this->getNumberOfUnitKeys() ) ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_unit_keys', __( 'The unit keys number provided by you is invalid.' ) ) );
    			$boolIsValid &= false;
    		} elseif( 0 > $this->getNumberOfUnitKeys() ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_unit_keys', __( 'The unit keys number must be greater than or equal to 0.' ) ) );
    			$boolIsValid &= false;
    		}
    	}

    	return $boolIsValid;
    }

    public function valNumberOfMailKeys( $boolIsKeyMandatory = false ) {

    	$boolIsValid = true;

    	if( true == $boolIsKeyMandatory && false == valStr( $this->getNumberOfMailKeys() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_mail_keys', __( 'The mailbox keys are required.' ) ) );
    		$boolIsValid &= false;
    	}

    	if( true == valStr( $this->getNumberOfMailKeys() ) ) {
    		if( false == is_numeric( $this->getNumberOfMailKeys() ) ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_mail_keys', __( 'The mailbox keys number provided by you is invalid.' ) ) );
    			$boolIsValid &= false;
    		} elseif( 0 > $this->getNumberOfMailKeys() ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_mail_keys', __( 'The mailbox keys number must be greater than or equal to 0.' ) ) );
    			$boolIsValid &= false;
    		}
    	}

    	return $boolIsValid;
    }

    public function valNumberOfOtherKeys() {

    	$boolIsValid = true;

    	if( true == $this->getNumberOfOtherKeys() ) {
    		if( false == is_numeric( $this->getNumberOfOtherKeys() ) ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_other_keys', __( 'The other keys number provided by you is invalid.' ) ) );
    			$boolIsValid &= false;
    		} elseif( 0 > $this->getNumberOfOtherKeys() ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_other_keys', __( 'The other keys number must be greater than or equal to 0.' ) ) );
    			$boolIsValid &= false;
    		}
    	}

    	return $boolIsValid;
    }

	public function valLatePaymentCount() {

		$boolIsValid = true;

		if( true == is_null( $this->getLatePaymentCount() ) || 0 > $this->getLatePaymentCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'late_payment_count', __( 'The Late payment count must be greater than or equal to 0.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valReturnedPaymentsCount() {

		$boolIsValid = true;

		if( true == is_null( $this->getReturnedPaymentsCount() ) || 0 > $this->getReturnedPaymentsCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'returned_payments_count', __( 'The Returned payment count must be greater than or equal to 0.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            case 'late_fee_post_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valLateFeePostedBy();
				$boolIsValid &= $this->valLateFeePostedOn();
				break;

            case 'validate_keys':
            	$boolIsValid &= $this->valNumberOfUnitKeys();
            	$boolIsValid &= $this->valNumberOfMailKeys();
            	$boolIsValid &= $this->valNumberOfOtherKeys();
            	break;

            case 'validate_keys_mandatory':
            	$boolIsValid &= $this->valNumberOfUnitKeys( true );
            	$boolIsValid &= $this->valNumberOfMailKeys( true );
            	$boolIsValid &= $this->valNumberOfOtherKeys();
            	break;

	        case 'validate_tenant_payments':
		        $boolIsValid &= $this->valId();
		        $boolIsValid &= $this->valLatePaymentCount();
		        $boolIsValid &= $this->valReturnedPaymentsCount();
		        break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>