<?php

class CSocialPostStatus extends CBaseSocialPostStatus {

	const DRAFT					= 1;
	const PENDING_APPROVAL		= 2;
	const APPROVED				= 3;
	const REVISION_REQUIRED		= 4;
	const POSTED				= 5;
	const FAILED				= 6;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getSocialPostStatusNameById( $intSocialPostStatusId ) {
		if( self::PENDING_APPROVAL == $intSocialPostStatusId ) {
			return __( 'Pending Approval' );
		} elseif( self::APPROVED == $intSocialPostStatusId ) {
			return __( 'Approved' );
		} elseif( self::REVISION_REQUIRED == $intSocialPostStatusId ) {
			return __( 'Revision Required' );
		} elseif( self::POSTED == $intSocialPostStatusId ) {
			return  __( 'Posted' );
		} elseif( self::FAILED == $intSocialPostStatusId ) {
			return  __( 'Failed' );
		} else {
			return __( 'NA' );
		}
	}

}
?>