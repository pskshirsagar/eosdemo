<?php

class CAddOnType extends CBaseAddOnType {

	const ASSIGNABLE_ITEMS			= 1;
	const RENTABLE_ITEMS			= 2;
	const SERVICES 					= 3;
	const FEATURE_UPGRADES 			= 4;
	const RESERVATIONS 				= 5;

	public static $c_arrintScheduledChargeAddOnTypeIds = [ self::ASSIGNABLE_ITEMS, self::RENTABLE_ITEMS, self::SERVICES ];
	public static $c_arrintAllAddOnTypeIds = [ self::ASSIGNABLE_ITEMS, self::SERVICES, self::FEATURE_UPGRADES ];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function loadCustomizedAddOnTypes() {
		$arrstrCustomizedAddOnTypes = [];
		// $arrstrCustomizedAddOnTypes[self::ASSIGNABLE_ITEMS]	= 'Assignable Items';
		$arrstrCustomizedAddOnTypes[self::RENTABLE_ITEMS]	= __( 'Rentable Items' );
		$arrstrCustomizedAddOnTypes[self::SERVICES]			= __( 'Services' );

		return $arrstrCustomizedAddOnTypes;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function fetchAddOnCategories( $objDatabase ) {
		return \Psi\Eos\Entrata\CAddOnCategories::createService()->fetchAddOnCategoriesByCidByAddOnTypeId( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function getAddOnTypeIdToStrArray() {
		return [
			self::RENTABLE_ITEMS => __( 'Rentable item(s)' ),
			self::SERVICES => __( 'Service(s)' ),
			self::ASSIGNABLE_ITEMS	=> __( 'Assignable item(s)' )
		];
	}

}
?>