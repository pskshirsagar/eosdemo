<?php

class CStoredObject extends CBaseStoredObject {

	protected $m_strTempFileName;
	protected $m_strFileError;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceTable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceTag() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVendor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContainer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContentLength() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEtag() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setTempFileName( $strTempFileName ) {
		$this->m_strTempFileName = $strTempFileName;
	}

	public function setFileError( $strFileError ) {
		$this->m_strFileError = $strFileError;
	}

	public function getTempFileName() {
		return $this->m_strTempFileName;
	}

	public function getFileError() {
		return $this->m_strFileError;
	}

	public function buildFileData( $strInputTypeFileName, $objDatabase, $intFileIndex = NULL ) {

		if( true == empty( $_FILES ) && empty( $_POST ) && isset( $_SERVER['REQUEST_METHOD'] ) && 'post' == \Psi\CStringService::singleton()->strtolower( $_SERVER['REQUEST_METHOD'] ) ) {
			$strPostMaxSize = ini_get( 'post_max_size' );
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to upload. File size should not exceed more than ' . $strPostMaxSize . 'B.' ) );
			return false;
		}

		if( false == is_numeric( $intFileIndex ) ) {
			$this->setTitle( $_FILES[$strInputTypeFileName]['name'] );
			$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'] );
		} else {
			$this->setTitle( $_FILES[$strInputTypeFileName]['name'][$intFileIndex] );
			$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'][$intFileIndex] );
		}

		$strExtension 	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;

		$strMimeType 	= '';
		$objFileExtension 	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_extention', ' Please select a file to upload. ' ) );
			return false;
		}

		$strMimeType = $objFileExtension->getMimeType();

		if( false == is_numeric( $intFileIndex ) ) {
			if( true == isset( $_FILES[$strInputTypeFileName]['tmp_name'] ) ) 	 $this->setTempFileName( $_FILES[$strInputTypeFileName]['tmp_name'] );
			if( true == isset( $_FILES[$strInputTypeFileName]['error'] ) ) 		 $this->setFileError( $_FILES[$strInputTypeFileName]['error'] );
		} else {
			if( true == isset( $_FILES[$strInputTypeFileName]['tmp_name'] ) ) 	 $this->setTempFileName( $_FILES[$strInputTypeFileName]['tmp_name'][$intFileIndex] );
			if( true == isset( $_FILES[$strInputTypeFileName]['error'] ) ) 		 $this->setFileError( $_FILES[$strInputTypeFileName]['error'][$intFileIndex] );
		}

		$strFileName = uniqid( mt_rand(), true ) . '_' . date( 'Ymdhis' ) . '.' . $strExtension;

		$this->setKey( '/template_library_attachments/' . $strFileName );

		return true;
	}

	public function uploadObject( $objObjectStorageGateway, $strFolderName = PATH_MOUNTS_FILES, $boolEncrypt = true ) {

		if( false == valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			trigger_error( 'Failed to get IObjectStorage', E_USER_WARNING );
			return false;
		}

		if( false == is_null( $this->getKey() ) && false == is_null( $this->getTitle() ) ) {

			$objResponse = $objObjectStorageGateway->putObject( [
				'cid'             => $this->getCid(),
				'objectId'        => $this->getId(),
				'container'       => $strFolderName,
				'key'             => $this->getKey(),
				'data'            => CFileIo::fileGetContents( $this->getTempFileName() ),
				'noEncrypt'       => !$boolEncrypt
			] );

			if( true == $objResponse->hasErrors() ) {
				trigger_error( 'Failed to upload file', E_USER_WARNING );
				return false;
			}
			return true;
		}
		return false;
	}

	public function downloadObject( $objObjectStorageGateway, $objDatabase, $strFolderName = PATH_MOUNTS_FILES ) {

		// This was ported from CFile::downloadFile - Jad

		$strPath     = $this->getKey();

		$intObjectId = ( true == valId( $this->getId() ) ) ? $this->getId() : -1;

		$objObjectStorageGatewayResponse = $objObjectStorageGateway->getObject( [
			'cid'           => $this->getCid(),
			'objectId'      => $intObjectId,
			'container'     => $strFolderName,
			'key'           => $strPath,
			'outputFile'    => 'temp'
		] );

		$strTempFullPath = $objObjectStorageGatewayResponse['outputFile'];

		if( false == CFileIo::fileExists( $strTempFullPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'File is not present or has been deleted.' ) );
			return false;
		}

		$arrstrFileParts    = pathinfo( $strTempFullPath );
		$strFileName        = $this->getTitle();
		$strPdfFileName     = '';
		$strPdfFilePath     = NULL;

		if( false !== \Psi\CStringService::singleton()->strpos( $strFileName, '.html' ) ) {
			$strPdfFileName = str_replace( '.html', '.pdf', $strFileName );
			$strPdfFilePath = preg_replace( '#\/[^/]*$#', '', $strPath );
		}

		if( true == valArr( $arrstrFileParts ) && false == is_null( $arrstrFileParts['extension'] ) ) {
			$objFileExtension = \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileParts['extension'], $objDatabase );
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		header( 'Pragma: public' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control: public' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-Length:' . CFileIo::getFileSize( $strTempFullPath ) );
		header( 'Content-Type:' . $strMimeType );
		header( 'Content-Disposition: attachment; filename= "' . $strFileName . '"' );
		header( 'Content-Transfer-Encoding:binary' );

		if( 'text/html' == $strMimeType )
			echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';

		echo CFileIo::fileGetContents( $strTempFullPath );
		CFileIo::deleteFile( $strTempFullPath );
		exit;

	}

	public function deleteObject( $objObjectStorageGateway, $strFolderPath = PATH_MOUNTS_FILES ) {
		if( true == is_null( $this->getKey() ) || true == is_null( $this->getTitle() ) ) return false;

		if( false == valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			trigger_error( 'Failed to get IObjectStorage', E_USER_WARNING );

			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Couldn\'t delete file(' . $this->getKey() . ' ). ' ) );
			return false;
		}

		$arrstrFileExtensionsToDelete   = [ 'tif', 'png', 'doc', 'docx', 'jpg', 'jpeg', 'bmp', 'pdf','BMP', 'GIF', 'JPE', 'ICO', 'TXT', 'TIFF', 'PSD', 'PDF' ];
		$arrstrFilePathInfo             = pathinfo( $this->getTitle() );

		if( false == in_array( $arrstrFilePathInfo['extension'], $arrstrFileExtensionsToDelete ) ) return false;

		$objDeleteObjectResponse = $objObjectStorageGateway->deleteObject( [
			'cid'             => $this->getCid(),
			'objectId'        => $this->getId(),
			'container'       => $strFolderPath,
			'key'             => $this->getKey()
		] );

		if( true == valObj( $objDeleteObjectResponse, 'CRouterObjectStorageGateway ' ) && true == $objDeleteObjectResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to remove the file.' ) );
			return false;
		}

		return true;

	}

	public function createGatewayRequest( $arrmixArgs = [] ) {
		$arrmixRequest = [
			'cid'                   => $this->getCid(),
			'container'             => $this->getContainer(),
			'objectId'              => $this->getReferenceId(),
			'key'                   => $this->getKey(),
			'storageGateway'        => $this->getVendor(),
			'legacyStorageGateway'  => $this->getDetailsField( [ 'legacy_persistence', 'storageGateway' ] )
		];

		if( true == valStr( $this->getDetailsField( [ 'legacy_persistence', 'container' ] ) ) ) {
			$arrmixRequest['legacyContainer'] = $this->getDetailsField( [ 'legacy_persistence', 'container' ] );
		}

		if( true == valStr( $this->getDetailsField( [ 'legacy_persistence', 'key' ] ) ) ) {
			$arrmixRequest['legacyKey'] = $this->getDetailsField( [ 'legacy_persistence', 'key' ] );
		}

		foreach( $arrmixArgs as $strKey => $strVal ) {
			$arrmixRequest[$strKey] = $strVal;
		}

		if( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_MULTI_SOURCED == $arrmixRequest['storageGateway'] ) {
			unset( $arrmixRequest['storageGateway'] );
		}

		if( 1 == $this->getCid() ) unset( $arrmixRequest['cid'] );

		return $arrmixRequest;
	}

}
?>