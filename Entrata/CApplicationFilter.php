<?php

class CApplicationFilter extends CBaseApplicationFilter {

	protected $m_strLeadIds;
	protected $m_strUnitNumbers;
	protected $m_strLeads;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strPropertyGroups;

	protected $m_boolLeasingAgentIsNull;
	protected $m_boolConsiderLeasingAgent;

	protected $m_intPageNo;
	protected $m_intPreviousPageNo;
	protected $m_intCountPerPage;
	protected $m_intRowLimit;
	protected $m_intIsPreviousLeaseRenewalTab;

	protected $m_intIsCurrentLeadsPageSelected;
	protected $m_intIsAllLeadsPageSelected;
	protected $m_intIsAllLeadsDeselected;
	protected $m_intCurrentSelectedLeadsPageNo;

	protected $m_arrintCompanyAssociatedPsProductIds;
	protected $m_arrintApplicationIds;
	protected $m_intApplicationId;

	protected $m_boolIsFollowup;
	protected $m_boolISPartiallyGeneratedLease;
	protected $m_boolIsCompletedOnIsNotNull;
	protected $m_arrintCompanyApplicationIds;
	protected $m_boolIsExcludeNewLeads;
	protected $m_arrintExcludingApplicationIds;

	protected $m_boolIsExcludeRenewals;

	const PAGE_NO		 = 1;
	const COUNT_PER_PAGE = 20;

	const INTEGRATED	 = 1;
	const NON_INTEGRATED = 0;

	const LEADS_SELECTION_TYPE_SELECT_THIS_PAGE 	= 1;
	const LEADS_SELECTION_TYPE_SELECT_ALL_PAGES 	= 2;
	const LEADS_SELECTION_TYPE_DESELECT_ALL_PAGE 	= 3;

	protected $_REQUEST_FORM_APPLICATION_SEARCH	= [
		'id'							=> NULL,
		'cid'  							=> NULL,
		'company_user_id'  				=> NULL,
		'application_id'  				=> NULL,
		'properties'  					=> NULL,
		'property_groups'  				=> NULL,
		'property_floorplans' 			=> NULL,
		'application_stage_statuses'		=> NULL,
		'lead_sources'					=> NULL,
		'ps_products'					=> NULL,
		'company_employees'				=> NULL,
		'lease_interval_types'			=> NULL,
		'screening_response_types'		=> NULL,
		'last_contact_min_days'			=> NULL,
		'last_contact_max_days'			=> NULL,
		'ad_delay_days'					=> NULL,
		'name_first'					=> NULL,
		'name_last'						=> NULL,
		'filter_name'					=> NULL,
		'desired_bedrooms'				=> NULL,
		'desired_bathrooms'				=> NULL,
		'desired_pets'					=> NULL,
		'desired_rent_min'				=> NULL,
		'desired_rent_max'				=> NULL,
		'desired_occupants'				=> NULL,
		'desired_floors'				=> NULL,
		'unit_kinds'					=> NULL,
		'application_start_date'		=> NULL,
		'application_end_date'			=> NULL,
		'unit_numbers'					=> NULL,
		'move_in_date_min'				=> NULL,
		'move_in_date_max'				=> NULL,
		'sort_by'						=> NULL,
		'sort_direction'				=> NULL,
		'leads'							=> NULL,
		'page_no'						=> NULL,
		'count_per_page'				=> NULL,
		'lead_ids' 						=> NULL,
		'is_integrated'					=> NULL,
		'quick_search'					=> NULL,
		'is_current_leads_page_selected'	=> 0,
		'is_all_leads_page_selected'		=> 0,
		'is_all_leads_deselected'			=> 1,
		'current_selected_leads_page_no'	=> NULL,
		'application_ids'					=> NULL,
		'company_application_ids'			=> NULL,
		'leasing_agent_is_null'				=> false,
		'occupancy_type_id'					=> NULL
	];

	public function setIsExcludeRenewals( $boolIsExcludeRenewals ) {
		  $this->m_boolIsExcludeRenewals = CStrings::strToIntDef( $boolIsExcludeRenewals, NULL, false );
	}

	public function getIsExcludeRenewals() {
		return $this->m_boolIsExcludeRenewals;
	}

	public function valApplicationStartDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getApplicationStartDate() ) && 'Min' != $this->getApplicationStartDate() && false == CValidation::validateDate( $this->getApplicationStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_start_date', 'Min date created of format mm/dd/yyyy is required.' ) );
		} elseif( false == is_null( $this->getApplicationStartDate() ) && 'Min' != $this->getApplicationStartDate() ) {
			// check the dates to make sure the min date created isn't after the max date created
			// if( true == is_null( $this->getApplicationEndDate())) return $boolIsValid;

			if( true == is_null( $this->getApplicationEndDate() ) || 'Max' == $this->getApplicationEndDate() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_end_date', 'Max date created must be greater than min date created.' ) );
			} else {

				if( strtotime( $this->getApplicationStartDate() ) > strtotime( $this->getApplicationEndDate() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_end_date', 'Max date created must be greater than min date created.' ) );
				}
			}
		}
		$strApplicationStartDate = trim( $this->getApplicationStartDate() );
		$this->setApplicationStartDate( ( 'Min' == $this->getApplicationStartDate() || true == empty( $strApplicationStartDate ) )? NULL : date( 'Y-m-d', strtotime( $this->getApplicationStartDate() ) ) );

		return $boolIsValid;
	}

	public function valApplicationEndDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getApplicationEndDate() ) && 'Max' != $this->getApplicationEndDate() && false == CValidation::validateDate( $this->getApplicationEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_end_date', 'Max date created of format mm/dd/yyyy is required.' ) );
		} elseif( false == is_null( $this->getApplicationEndDate() ) && 'Max' != $this->getApplicationEndDate() && ( 'Min' == $this->getApplicationStartDate() || true == is_null( $this->getApplicationStartDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_start_date', 'Min date created of format mm/dd/yyyy is required.' ) );
		}
		$strApplicationEndDate = trim( $this->getApplicationEndDate() );
		$this->setApplicationEndDate( ( 'Max' == $this->getApplicationEndDate() || true == empty( $strApplicationEndDate ) ) ? NULL : date( 'm/d/Y', strtotime( $this->getApplicationEndDate() ) ) );

		return $boolIsValid;
	}

	public function valMoveInDateMin() {
		$boolIsValid = true;

		if( false == is_null( $this->getMoveInDateMin() ) && 'Min' != $this->getMoveInDateMin() && false == CValidation::validateDate( $this->getMoveInDateMin() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date_min', 'Desired Min Move-in date in format mm/dd/yyyy is required.' ) );

		} elseif( false == is_null( $this->getMoveInDateMin() ) && 'Min' != $this->getMoveInDateMin() ) {

			if( true == is_null( $this->getMoveInDateMax() ) || 'Max' == $this->getMoveInDateMax() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date_max', 'Desired Max Move-in date in format mm/dd/yyyy is required.' ) );

			} elseif( strtotime( $this->getMoveInDateMin() ) > strtotime( $this->getMoveInDateMax() ) ) {
   				$boolIsValid = false;
   				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date_max', 'Desired Max Move-in date must be greater than Desired Min Move-in date.' ) );
			}
		}
		$strMoveInDateMin = trim( $this->getMoveInDateMin() );
   		$this->setMoveInDateMin( ( 'Min' == $this->getMoveInDateMin() || true == empty( $strMoveInDateMin ) ) ? NULL : date( 'm/d/Y', strtotime( $this->getMoveInDateMin() ) ) );

		return $boolIsValid;
	}

	public function valMoveInDateMax() {
		$boolIsValid = true;

		if( false == is_null( $this->getMoveInDateMax() ) && 'Max' != $this->getMoveInDateMax() && false == CValidation::validateDate( $this->getMoveInDateMax() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date_max', 'Desired Max Move-in date in format mm/dd/yyyy is required.' ) );

		} elseif( false == is_null( $this->getMoveInDateMax() ) && 'Max' != $this->getMoveInDateMax() && ( 'Min' == $this->getMoveInDateMin() || true == is_null( $this->getMoveInDateMin() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date_min', 'Desired Min Move-in date in format mm/dd/yyyy is required.' ) );
		}
		$strMoveInDateMax = trim( $this->getMoveInDateMax() );
   		$this->setMoveInDateMax( ( 'Max' == $this->getMoveInDateMax() || true == empty( $strMoveInDateMax ) ) ? NULL : date( 'm/d/Y', strtotime( $this->getMoveInDateMax() ) ) );

		return $boolIsValid;
	}

	public function valConflictingCompanyUserApplicationFilterName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getFilterName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'filter_name', 'Filter name is required.' ) );
		}

		$intConflictingApplicationFilterNameCount = \Psi\Eos\Entrata\CApplicationFilters::createService()->fetchConflictingCompanyUserApplicationFilterNameCountByCid( $this->getCompanyUserId(), $this->getFilterName(), $this->m_intCid, $objDatabase );

	if( 0 < $intConflictingApplicationFilterNameCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'filter_name', 'Filter name \'' . $this->getFilterName() . '\' already in use.' ) );
	}

		return $boolIsValid;
	}

	public function valMinMaxLastContactDays() {
		$boolIsValid = true;
		if( false == is_null( $this->getLastContactMinDays() ) && false == is_null( $this->getLastContactMaxDays() ) ) {
			if( $this->getLastContactMaxDays() < 0 || $this->getLastContactMinDays() < 0 ) {
				 $boolIsValid = false;
			 $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_last_contact_days', 'Please enter Valid values for Days since last contact.' ) );
			} elseif( $this->getLastContactMaxDays() < $this->getLastContactMinDays() ) {
				 $boolIsValid = false;
			 $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_last_contact_days', 'Max contact days must be greater than Min Contact Days.' ) );
			}
		}

		 return $boolIsValid;
	}

	public function valDesiredMinMaxRent() {
		$boolIsValid = true;

		if( true == is_numeric( $this->getDesiredRentMin() ) && true == is_numeric( $this->getDesiredRentMax() ) && ( ( float ) $this->getDesiredRentMin() > ( float ) $this->getDesiredRentMax() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Maximum Desired Rent must be greater than Minimum Desired Rent.' ) );
		}

		return $boolIsValid;
	}

	/**
	 * Set Functions
	 */

	public function setDefaults() {

		$this->setId( NULL );
		$this->setCid( NULL );
		$this->setCompanyUserId( NULL );
		$this->setApplicationId( NULL );
		$this->setProperties( NULL );
		$this->setPropertyFloorplans( NULL );
		$this->setApplicationStageStatuses( NULL );
		$this->setLeadSources( NULL );
		$this->setCompanyAssociatedPsProductIds( NULL );

		$this->setPsProducts( NULL );
		$this->setCompanyEmployees( NULL );
		$this->setLeaseIntervalTypes( NULL );
		$this->setScreeningResponseTypes( NULL );
		$this->setLastContactMinDays( NULL );
		$this->setLastContactMaxDays( NULL );
		$this->setAdDelayDays( NULL );
		$this->setNameFirst( NULL );
		$this->setNameLast( NULL );
		$this->setFilterName( NULL );
		$this->setDesiredBedrooms( NULL );
		$this->setDesiredBathrooms( NULL );
		$this->setDesiredPets( NULL );
		$this->setDesiredRentMin( NULL );

		$this->setDesiredRentMax( NULL );
		$this->setDesiredOccupants( NULL );
		$this->setDesiredFloors( NULL );
		$this->setUnitKinds( NULL );
		$this->setUnitNumbers( NULL );

		$this->setApplicationStartDate( NULL );
		$this->setApplicationEndDate( NULL );

		$this->setMoveInDateMin( NULL );
		$this->setMoveInDateMax( NULL );

	//	 	$this->setSortBy( NULL );
	//	 	$this->setSortDirection( 'DESC' );
		$this->setLeads( NULL );

		$this->setPageNo( self::PAGE_NO );
		$this->setPreviousPageNo( self::PAGE_NO );
		$this->setCountPerPage( self::COUNT_PER_PAGE );
		$this->setLeadIds( NULL );

		$this->setIsIntegrated( NULL );
		$this->setQuickSearch( NULL );

		$this->setIsCurrentLeadsPageSelected( 0 );
		$this->setIsAllLeadsPageSelected( 0 );
		$this->setIsAllLeadsDeselected( 1 );
		$this->setCurrentSelectedLeadsPageNo( NULL );
		$this->setApplicationIds( NULL );
		$this->setIsFollowup( 0 );
		$this->setIsPartiallyGeneratedLease( false );
		$this->setcompanyApplicationIds( NULL );
		$this->setIsExcludeNewLeads( false );
		$this->setLeasingAgentIsNull( false );

		$this->setConsiderLeasingAgent( true );
		return;
	}

	public function validate( $strAction = '', $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valConflictingCompanyUserApplicationFilterName( $objDatabase );
				$boolIsValid &= $this->valApplicationStartDate();
				$boolIsValid &= $this->valApplicationEndDate();
				$boolIsValid &= $this->valMinMaxLastContactDays();
				$boolIsValid &= $this->valMoveInDateMin();
				$boolIsValid &= $this->valMoveInDateMax();
				$boolIsValid &= $this->valDesiredMinMaxRent();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valApplicationStartDate();
				$boolIsValid &= $this->valApplicationEndDate();
				$boolIsValid &= $this->valMinMaxLastContactDays();
				$boolIsValid &= $this->valMoveInDateMin();
				$boolIsValid &= $this->valMoveInDateMax();
				$boolIsValid &= $this->valDesiredMinMaxRent();
			 	break;

			case VALIDATE_DELETE:
				break;

			case 'validate_dates':
				$boolIsValid &= $this->valApplicationStartDate();
				$boolIsValid &= $this->valApplicationEndDate();
				break;

			default:
				$boolIsValid &= $this->valApplicationStartDate();
				$boolIsValid &= $this->valApplicationEndDate();
				$boolIsValid &= $this->valMinMaxLastContactDays();
				$boolIsValid &= $this->valMoveInDateMin();
				$boolIsValid &= $this->valMoveInDateMax();
				$boolIsValid &= $this->valDesiredMinMaxRent();
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Set Functions
	 */

	public function setcompanyApplicationIds( $arrintCompanyApplicationIds ) {
		$this->m_arrintCompanyApplicationIds = $arrintCompanyApplicationIds;
	}

	public function setIsExcludeNewLeads( $boolIsExcludeNewLeads ) {
		$this->m_boolIsExcludeNewLeads = $boolIsExcludeNewLeads;
	}

	public function setProperties( $strProperties ) {
		$this->m_strProperties = NULL;
		if( true == valArr( $strProperties ) ) {
			$this->m_strProperties = implode( ',', $strProperties );
		} elseif( true == valStr( $strProperties ) ) {
			$this->m_strProperties = $strProperties;
		}
	}

	public function setPropertyGroups( $strPropertyGroups ) {
		$this->m_strPropertyGroups = NULL;
		if( true == valArr( $strPropertyGroups ) ) {
			$this->m_strPropertyGroups = implode( ',', $strPropertyGroups );
		} elseif( true == valStr( $strPropertyGroups ) ) {
			$this->m_strPropertyGroups = $strPropertyGroups;
		}
	}

	public function setApplicationId( $intApplicationId ) {
		$this->m_intApplicationId = $intApplicationId;
	}

	public function setCompanyAssociatedPsProductIds( $arrintCompanyAssociatedPsProductIds ) {
		$this->m_arrintCompanyAssociatedPsProductIds = $arrintCompanyAssociatedPsProductIds;
	}

	public function setApplicationIds( $arrintApplicationIds ) {
		$this->m_arrintApplicationIds = $arrintApplicationIds;
	}

	public function setIsFollowup( $boolIsFollowup ) {
		$this->m_boolIsFollowup = $boolIsFollowup;
	}

	public function setIsPartiallyGeneratedLease( $boolIsPartiallyGeneratedLease ) {
	  	$this->m_boolIsPartiallyGeneratedLease = $boolIsPartiallyGeneratedLease;
	}

	public function setLeads( $strLeads ) {
		$this->m_strLeads = $strLeads;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

 	public function setLeasingAgentIsNull( $boolLeasingAgentIsNull ) {
		$this->m_boolLeasingAgentIsNull = $boolLeasingAgentIsNull;
	}

	public function setConsiderLeasingAgent( $boolConsiderLeasingAgent ) {
		$this->m_boolConsiderLeasingAgent = $boolConsiderLeasingAgent;
	}

	public function setUnitNumbers( $strUnitNumbers ) {
		$this->m_strUnitNumbers = $strUnitNumbers;
	}

	public function setPageNo( $intPageNo ) {
		$this->m_intPageNo = $intPageNo;
	}

	public function setPreviousPageNo( $intPreviousPageNo ) {
		$this->m_intPreviousPageNo = $intPreviousPageNo;
	}

	public function setCountPerPage( $intCountPerPage ) {
		$this->m_intCountPerPage = $intCountPerPage;
	}

	public function setRowLimit( $intRowLimit ) {
		$this->m_intRowLimit = $intRowLimit;
	}

	public function setLeadIds( $strLeadIds ) {
		$this->m_strLeadIds = $strLeadIds;
	}

	public function setIsPreviousLeaseRenewalTab( $intIsPreviousLeaseRenewalTab ) {
		$this->m_intIsPreviousLeaseRenewalTab = $intIsPreviousLeaseRenewalTab;
	}

	public function setIsCurrentLeadsPageSelected( $intIsCurrentLeadsPageSelected ) {
		$this->m_intIsCurrentLeadsPageSelected = $intIsCurrentLeadsPageSelected;
	}

	public function setIsAllLeadsPageSelected( $intIsAllLeadsPageSelected ) {
		$this->m_intIsAllLeadsPageSelected = $intIsAllLeadsPageSelected;
	}

	public function setIsAllLeadsDeselected( $intIsAllLeadsDeselected ) {
		$this->m_intIsAllLeadsDeselected = $intIsAllLeadsDeselected;
	}

	public function setCurrentSelectedLeadsPageNo( $intCurrentSelectedLeadsPageNo ) {
		$this->m_intCurrentSelectedLeadsPageNo = $intCurrentSelectedLeadsPageNo;
	}

	public function setIsCompletedOnIsNotNull( $boolIsCompletedOnIsNotNull ) {
		$this->m_boolIsCompletedOnIsNotNull = $boolIsCompletedOnIsNotNull;
	}

	public function setExcludingApplicationIds( $arrintExcludingApplicationIds ) {
		$this->m_arrintExcludingApplicationIds = $arrintExcludingApplicationIds;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );

		if( true == isset( $arrmixValues['leads'] ) ) 			$this->setLeads( ( true == $boolStripSlashes )		? stripslashes( $arrmixValues['leads'] ) 		: $arrmixValues['leads'] );
		if( true == isset( $arrmixValues['unit_numbers'] ) ) 	$this->setUnitNumbers( ( true == $boolStripSlashes )	? stripslashes( $arrmixValues['unit_numbers'] ): $arrmixValues['unit_numbers'] );

		if( true == isset( $arrmixValues['is_current_leads_page_selected'] ) ) 	$this->setIsCurrentLeadsPageSelected( ( true == $boolStripSlashes )	? stripslashes( $arrmixValues['is_current_leads_page_selected'] ): $arrmixValues['is_current_leads_page_selected'] );
		if( true == isset( $arrmixValues['is_all_leads_page_selected'] ) ) 		$this->setIsAllLeadsPageSelected( ( true == $boolStripSlashes )		? stripslashes( $arrmixValues['is_all_leads_page_selected'] ): $arrmixValues['is_all_leads_page_selected'] );
		if( true == isset( $arrmixValues['is_all_leads_deselected'] ) ) 		$this->setIsAllLeadsDeselected( ( true == $boolStripSlashes )		? stripslashes( $arrmixValues['is_all_leads_deselected'] ): $arrmixValues['is_all_leads_deselected'] );
		if( true == isset( $arrmixValues['current_selected_leads_page_no'] ) ) 	$this->setCurrentSelectedLeadsPageNo( ( true == $boolStripSlashes )	? stripslashes( $arrmixValues['current_selected_leads_page_no'] ): $arrmixValues['current_selected_leads_page_no'] );
		if( true == isset( $arrmixValues['leasing_agent_is_null'] ) ) 			$this->setLeasingAgentIsNull( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['leasing_agent_is_null'] ): $arrmixValues['leasing_agent_is_null'] );
		if( true == isset( $arrmixValues['property_groups'] ) ) 				$this->setPropertyGroups( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_groups'] ) : $arrmixValues['property_groups'] );

 		return;
	}

	/**
	 * Get Functions
	 */

	public function getcompanyApplicationIds() {
		return $this->m_arrintCompanyApplicationIds;
	}

	public function getIsExcludeNewLeads() {
		return $this->m_boolIsExcludeNewLeads;
	}

	public function getCompanyAssociatedPsProductIds() {
		return $this->m_arrintCompanyAssociatedPsProductIds;
	}

	public function getApplicationIds() {
		return $this->m_arrintApplicationIds;
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function getIsFollowup() {
		return $this->m_boolIsFollowup;
	}

	public function getIsPartiallyGeneratedLease() {
		return $this->m_boolIsPartiallyGeneratedLease;
	}

	public function getLeads() {
		return $this->m_strLeads;
	}

	public function getLeasingAgentIsNull() {
		return $this->m_boolLeasingAgentIsNull;
	}

	public function getConsiderLeasingAgent() {
		return $this->m_boolConsiderLeasingAgent;
	}

	public function getUnitNumbers() {
		return $this->m_strUnitNumbers;
	}

	public function getPageNo() {
		return $this->m_intPageNo;
	}

	public function getPreviousPageNo() {
		return $this->m_intPreviousPageNo;
	}

	public function getCountPerPage() {
		return $this->m_intCountPerPage;
	}

	public function getRowLimit() {
		return $this->m_intRowLimit;
	}

	public function getLeadIds() {
		return $this->m_strLeadIds;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getIsPreviousLeaseRenewalTab() {
		return $this->m_intIsPreviousLeaseRenewalTab;
	}

	public function getIsCurrentLeadsPageSelected() {
		return $this->m_intIsCurrentLeadsPageSelected;
	}

	public function getIsAllLeadsPageSelected() {
		return $this->m_intIsAllLeadsPageSelected;
	}

	public function getIsAllLeadsDeselected() {
		return $this->m_intIsAllLeadsDeselected;
	}

	public function getCurrentSelectedLeadsPageNo() {
		return $this->m_intCurrentSelectedLeadsPageNo;
	}

	public function getIsCompletedOnIsNotNull() {
		return $this->m_boolIsCompletedOnIsNotNull;
	}

	public function getExcludingApplicationIds() {
		return $this->m_arrintExcludingApplicationIds;
	}

	public function getPropertyGroups() {
		return $this->m_strPropertyGroups;
	}

	/**
	 * Other Functions
	 */

	public function setRequestData( $arrmixRequestForm ) {

		$arrmixRequestForm['properties'] 					= ( true == isset( $arrmixRequestForm['property_ids'] ) && true == valArr( $arrmixRequestForm['property_ids'] ) ) 											? implode( ',', $arrmixRequestForm['property_ids'] ) : '';
			unset( $arrmixRequestForm['property_ids'] );
		$arrmixRequestForm['property_groups'] 				= ( true == isset( $arrmixRequestForm['property_group_ids'] ) && true == valArr( $arrmixRequestForm['property_group_ids'] ) ) 								? implode( ',', $arrmixRequestForm['property_group_ids'] ) : '';
			unset( $arrmixRequestForm['property_group_ids'] );
		$arrmixRequestForm['application_stage_statuses'] 	= ( true == isset( $arrmixRequestForm['application_stage_status_ids'] ) && true == valArr( $arrmixRequestForm['application_stage_status_ids'] ) ) 				? implode( ',', $arrmixRequestForm['application_stage_status_ids'] ) : '';
			unset( $arrmixRequestForm['application_stage_status_ids'] );
		$arrmixRequestForm['lead_sources'] 				= ( true == isset( $arrmixRequestForm['lead_source_ids'] ) && true == valArr( $arrmixRequestForm['lead_source_ids'] ) ) 										? implode( ',', $arrmixRequestForm['lead_source_ids'] ) : '';
			unset( $arrmixRequestForm['lead_source_ids'] );
		$arrmixRequestForm['ps_products'] 					= ( true == isset( $arrmixRequestForm['ps_product_ids'] ) && true == valArr( $arrmixRequestForm['ps_product_ids'] ) ) 										? implode( ',', $arrmixRequestForm['ps_product_ids'] ) : '';
			unset( $arrmixRequestForm['ps_product_ids'] );
		$arrmixRequestForm['last_contact_min_days'] 		= ( true == isset( $arrmixRequestForm['min_last_contact_days'] ) )																							? $arrmixRequestForm['min_last_contact_days'] : '';
		unset( $arrmixRequestForm['min_last_contact_days'] );
		$arrmixRequestForm['last_contact_max_days'] 		= ( true == isset( $arrmixRequestForm['max_last_contact_days'] ) ) 																						? $arrmixRequestForm['max_last_contact_days'] : '';
			unset( $arrmixRequestForm['max_last_contact_days'] );
		$arrmixRequestForm['company_employees'] 			= ( true == isset( $arrmixRequestForm['company_employee_ids'] ) && true == valArr( $arrmixRequestForm['company_employee_ids'] ) ) 							? implode( ',', $arrmixRequestForm['company_employee_ids'] ) : '';
			unset( $arrmixRequestForm['company_employee_ids'] );
		$arrmixRequestForm['desired_bedrooms'] 			= ( true == isset( $arrmixRequestForm['no_of_bed_rooms'] ) && true == valArr( $arrmixRequestForm['no_of_bed_rooms'] ) ) 										? implode( ',', $arrmixRequestForm['no_of_bed_rooms'] ) : '';
			unset( $arrmixRequestForm['no_of_bed_rooms'] );
		$arrmixRequestForm['desired_bathrooms'] 			= ( true == isset( $arrmixRequestForm['no_of_bath_rooms'] ) && true == valArr( $arrmixRequestForm['no_of_bath_rooms'] ) ) 									? implode( ',', $arrmixRequestForm['no_of_bath_rooms'] ) : '';
			unset( $arrmixRequestForm['no_of_bath_rooms'] );
		$arrmixRequestForm['desired_rent_min'] 			= ( true == isset( $arrmixRequestForm['min_desired_rent'] ) )  																							? $arrmixRequestForm['min_desired_rent'] : '';
			unset( $arrmixRequestForm['min_desired_rent'] );
		$arrmixRequestForm['desired_rent_max'] 			= ( true == isset( $arrmixRequestForm['max_desired_rent'] ) )																								? $arrmixRequestForm['max_desired_rent'] : '';
			unset( $arrmixRequestForm['max_desired_rent'] );
		$arrmixRequestForm['desired_occupants'] 			= ( true == isset( $arrmixRequestForm['no_of_occupant'] ) ) 																								? $arrmixRequestForm['no_of_occupant'] : '';
			unset( $arrmixRequestForm['no_of_occupant'] );
		$arrmixRequestForm['property_floorplans'] 			= ( true == isset( $arrmixRequestForm['property_floor_plan_ids'] ) && true == valArr( $arrmixRequestForm['property_floor_plan_ids'] ) ) 						? implode( ',', $arrmixRequestForm['property_floor_plan_ids'] ) : '';
			unset( $arrmixRequestForm['property_floor_plan_ids'] );
		$arrmixRequestForm['unit_kinds'] 					= ( true == isset( $arrmixRequestForm['unit_kind'] ) )																										? $arrmixRequestForm['unit_kind'] : '';
			unset( $arrmixRequestForm['unit_kind'] );
		$arrmixRequestForm['unit_numbers'] 				= ( true == isset( $arrmixRequestForm['unit_number'] ) ) 																									? $arrmixRequestForm['unit_number'] : '';
			unset( $arrmixRequestForm['unit_number'] );
		$arrmixRequestForm['leads'] 						= ( true == isset( $arrmixRequestForm['all_leads'] ) ) 																									? $arrmixRequestForm['all_leads'] : '';
			unset( $arrmixRequestForm['all_leads'] );
		$arrmixRequestForm['quick_search'] 				= ( true == isset( $arrmixRequestForm['advanced_search_quick_search'] ) && false == is_null( trim( $arrmixRequestForm['advanced_search_quick_search'] ) ) ) 	? $arrmixRequestForm['advanced_search_quick_search'] : $arrmixRequestForm['leads'];
			unset( $arrmixRequestForm['advanced_search_quick_search'] );
		$arrmixRequestForm['is_integrated'] 				= ( true == isset( $arrmixRequestForm['is_integrated_status_ids'] ) && true == valArr( $arrmixRequestForm['is_integrated_status_ids'] ) ) 					? implode( ',', $arrmixRequestForm['is_integrated_status_ids'] ) : '';
			unset( $arrmixRequestForm['is_integrated_status_ids'] );
		$arrmixRequestForm['lease_interval_types'] 					= ( true == isset( $arrmixRequestForm['lease_interval_type_ids'] ) && true == valArr( $arrmixRequestForm['lease_interval_type_ids'] ) ) 											? implode( ',', $arrmixRequestForm['lease_interval_type_ids'] ) : '';
			unset( $arrmixRequestForm['lease_interval_type_ids'] );
		$arrmixRequestForm['company_application_ids'] 		= ( true == isset( $arrmixRequestForm['company_application_ids'] ) && true == valArr( $arrmixRequestForm['company_application_ids'] ) )						? implode( ',', $arrmixRequestForm['company_application_ids'] ) : '';
			unset( $arrmixRequestForm['company_application_ids'] );

		return $arrmixRequestForm;
	}

	public function prepareRequestData( $arrmixRequestForm = [] ) {

		if( true == isset( $arrmixRequestForm['all_leads'] ) && 'Find a Lead ...' != $arrmixRequestForm['all_leads'] ) {
			if( 1 == $this->getPageNo() ) {
				$this->setDefaults();
			}

			$this->setIsCurrentLeadsPageSelected( $arrmixRequestForm['is_current_leads_page_selected'] );
			$this->setIsAllLeadsPageSelected( $arrmixRequestForm['is_all_leads_page_selected'] );
			$this->setIsAllLeadsDeselected( $arrmixRequestForm['is_all_leads_deselected'] );
			$this->setCurrentSelectedLeadsPageNo( $arrmixRequestForm['current_selected_leads_page_no'] );

			if( true == isset( $arrmixRequestForm['sort_by'] ) && true == isset( $arrmixRequestForm['sort_direction'] ) ) {
				$this->setSortBy( $arrmixRequestForm['sort_by'] );
				$this->setSortDirection( $arrmixRequestForm['sort_direction'] );
			}

			$this->setQuickSearch( ( false == empty( $arrmixRequestForm['all_leads'] ) ) ? $arrmixRequestForm['all_leads'] : $arrmixRequestForm['advanced_search_quick_search'] );
			$this->setLeads( $arrmixRequestForm['all_leads'] );

		} else {
			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrmixRequestForm ) ) {
				$arrmixRequestForm = $this->setRequestData( $arrmixRequestForm );
			}
			if( true == valArr( $this->_REQUEST_FORM_APPLICATION_SEARCH ) ) {
				$arrmixRequestForm = mergeIntersectArray( $this->_REQUEST_FORM_APPLICATION_SEARCH, $arrmixRequestForm );
			}

			$this->setValues( $arrmixRequestForm, false );

		}
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function changeDateFormat() {
		$this->setApplicationStartDate( false == is_null( $this->getApplicationStartDate() ) ? date( 'm/d/Y', strtotime( $this->getApplicationStartDate() ) ) : 'Min' );
		$this->setApplicationEndDate( false == is_null( $this->getApplicationEndDate() )? date( 'm/d/Y', strtotime( $this->getApplicationEndDate() ) ) : 'Max' );
	}

	public function changeDateMoveInFormat() {
		$this->setMoveInDateMin( ( false == is_null( $this->getMoveInDateMin() ) ) ? date( 'm/d/Y', strtotime( $this->getMoveInDateMin() ) ) : 'Min' );
		$this->setMoveInDateMax( ( false == is_null( $this->getMoveInDateMax() ) ) ? date( 'm/d/Y', strtotime( $this->getMoveInDateMax() ) ) : 'Max' );
	}

	public function getValues() {

		if( true == isset( $this->m_intId ) )								$arrmixValues['id'] = $this->m_intId;
		if( true == isset( $this->m_intCid ) )								$arrmixValues['cid'] = $this->m_intCid;

		if( true == isset( $this->m_intCompanyUserId ) )					$arrmixValues['company_user_id'] = $this->m_intCompanyUserId;
		if( true == isset( $this->m_strProperties ) )						$arrmixValues['properties'] = $this->m_strProperties;
		if( true == isset( $this->m_strPropertyGroups ) )					$arrmixValues['property_groups'] = $this->m_strPropertyGroups;
		if( true == isset( $this->m_strPropertyFloorplans ) )				$arrmixValues['property_floorplans'] = $this->m_strPropertyFloorplans;
		if( true == isset( $this->m_strUnitKinds ) )						$arrmixValues['unit_kinds'] = $this->m_strUnitKinds;
		if( true == isset( $this->m_strApplicationStageStatuses ) )			$arrmixValues['application_stage_statuses'] = $this->m_strApplicationStageStatuses;
		if( true == isset( $this->m_strScreeningResponseTypes ) )			$arrmixValues['screening_response_types'] = $this->m_strScreeningResponseTypes;

		if( true == isset( $this->m_strLeaseIntervalTypes ) )				$arrmixValues['lease_interval_types'] = $this->m_strLeaseIntervalTypes;
		if( true == isset( $this->m_strLeadSources ) )						$arrmixValues['lead_sources'] = $this->m_strLeadSources;
		if( true == isset( $this->m_strPsProducts ) )						$arrmixValues['ps_products'] = $this->m_strPsProducts;
		if( true == isset( $this->m_strCompanyEmployees ) )					$arrmixValues['company_employees'] = $this->m_strCompanyEmployees;
		if( true == isset( $this->m_strApplications ) )						$arrmixValues['applications'] = $this->m_strApplications;
		if( true == isset( $this->m_strFilterName ) )						$arrmixValues['filter_name'] = $this->m_strFilterName;
		if( true == isset( $this->m_strQuickSearch ) )						$arrmixValues['quick_search'] = $this->m_strQuickSearch;

		if( true == isset( $this->m_intLastContactMinDays ) )				$arrmixValues['last_contact_min_days'] = $this->m_intLastContactMinDays;
		if( true == isset( $this->m_intLastContactMaxDays ) )				$arrmixValues['last_contact_max_days'] = $this->m_intLastContactMaxDays;
		if( true == isset( $this->m_strDesiredBedrooms ) )					$arrmixValues['desired_bedrooms'] = $this->m_strDesiredBedrooms;
		if( true == isset( $this->m_strDesiredBathrooms ) )					$arrmixValues['desired_bathrooms'] = $this->m_strDesiredBathrooms;
		if( true == isset( $this->m_strDesiredPets ) )						$arrmixValues['desired_pets'] = $this->m_strDesiredPets;
		if( true == isset( $this->m_fltDesiredRentMin ) )					$arrmixValues['desired_rent_min'] = $this->m_fltDesiredRentMin;

		if( true == isset( $this->m_fltDesiredRentMax ) )					$arrmixValues['desired_rent_max'] = $this->m_fltDesiredRentMax;
		if( true == isset( $this->m_strDesiredOccupants ) )					$arrmixValues['desired_occupants'] = $this->m_strDesiredOccupants;
		if( true == isset( $this->m_strDesiredFloors ) )					$arrmixValues['desired_floors'] = $this->m_strDesiredFloors;
		if( true == isset( $this->m_strApplicationStartDate ) )				$arrmixValues['application_start_date'] = $this->m_strApplicationStartDate;
		if( true == isset( $this->m_strApplicationEndDate ) )				$arrmixValues['application_end_date'] = $this->m_strApplicationEndDate;
		if( true == isset( $this->m_strMoveInDateMin ) )					$arrmixValues['move_in_date_min'] = $this->m_strMoveInDateMin;
		if( true == isset( $this->m_strMoveInDateMax ) )					$arrmixValues['move_in_date_max'] = $this->m_strMoveInDateMax;

		if( true == isset( $this->m_strSortBy ) )							$arrmixValues['sort_by'] = $this->m_strSortBy;
		if( true == isset( $this->m_strSortDirection ) )					$arrmixValues['sort_direction'] = $this->m_strSortDirection;
		if( true == isset( $this->m_intIsIntegrated ) )						$arrmixValues['is_integrated'] = $this->m_intIsIntegrated;
 		if( true == isset( $this->m_intDeletedBy ) )						$arrmixValues['deleted_by'] = $this->m_intDeletedBy;

 		if( true == isset( $this->m_strDeletedOn ) )						$arrmixValues['deleted_on'] = $this->m_strDeletedOn;
 		if( true == isset( $this->m_intUpdatedBy ) )						$arrmixValues['updated_by'] = $this->m_intUpdatedBy;
 		if( true == isset( $this->m_strUpdatedOn ) )						$arrmixValues['updated_on'] = $this->m_strUpdatedOn;
 		if( true == isset( $this->m_intCreatedBy ) )						$arrmixValues['created_by'] = $this->m_intCreatedBy;
 		if( true == isset( $this->m_strCreatedOn ) )						$arrmixValues['created_on'] = $this->m_strCreatedOn;

		if( true == isset( $this->m_strLeads ) )							$arrmixValues['leads'] = $this->m_strLeads;
		if( true == isset( $this->m_strUnitNumbers ) )						$arrmixValues['unit_numbers'] = $this->m_strUnitNumbers;

		if( true == isset( $this->m_intIsCurrentLeadsPageSelected ) )		$arrmixValues['is_current_leads_page_selected'] = $this->m_intIsCurrentLeadsPageSelected;
		if( true == isset( $this->m_intIsAllLeadsPageSelected ) ) 			$arrmixValues['is_all_leads_page_selected'] = $this->m_intIsAllLeadsPageSelected;
		if( true == isset( $this->m_intIsAllLeadsDeselected ) ) 			$arrmixValues['is_all_leads_deselected'] = $this->m_intIsAllLeadsDeselected;
		if( true == isset( $this->m_intCurrentSelectedLeadsPageNo ) ) 		$arrmixValues['current_selected_leads_page_no'] = $this->m_intCurrentSelectedLeadsPageNo;
		if( true == isset( $this->m_boolLeasingAgentIsNull ) )				$arrmixValues['leasing_agent_is_null'] = $this->m_boolLeasingAgentIsNull;

		return $arrmixValues;
	}

	public function getApplicationOccupancyTypeId() {
		return $this->m_intApplicationOccupancyTypeId;
	}

	public function setApplicationOccupancyTypeId( $intApplicationOccupancyTypeId ) {
		$this->m_intApplicationOccupancyTypeId = $intApplicationOccupancyTypeId;
	}

}
?>