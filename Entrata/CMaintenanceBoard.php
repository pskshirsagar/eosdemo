<?php

class CMaintenanceBoard extends CBaseMaintenanceBoard {

	const CATEGORY	= 0;
	const PROBLEM	= 1;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

        if( false == valStr( $this->getName() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( ' Make Ready Board name is required. ' ) ) );
        }

		$intCountMaintenanceBoards = \Psi\Eos\Entrata\CMaintenanceBoards::createService()->fetchCustomMaintenanceBoardIdByNameByCid( $this->getId(), $this->getName(), $this->getCid(), $objDatabase );

		if( 0 < $intCountMaintenanceBoards ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( '\'{%s, 0}\'  already exists. Please enter a different name.', [ $this->getName() ] ) ) );
		}

        return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGroupByProblems() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShowInspectionColumn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShowFloorplanOnUnitHover() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDontShowEndDatesAndDaysRemaining() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled( $objDatabase, $arrobjProperties = NULL ) {
		$boolIsValid = true;
		$arrmixMaintenanceBoardAssociatedProperties = \Psi\Eos\Entrata\CMaintenanceBoards::createService()->fetchCustomAssociatedPropertiesCountByMaintenanceBoardIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase );
		if( NULL !== $arrobjProperties && true == valArr( $arrmixMaintenanceBoardAssociatedProperties ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'enabled', __( ' Make Ready Board already in use.' ) ) );
			$boolIsValid = false;
		}

		if( NULL !== $arrobjProperties && true == $boolIsValid ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( ' Cannot associate property to the board in disable mode.' ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $arrobjProperties = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
				if( ( true == valId( $this->getId() ) || NULL !== $arrobjProperties ) && true == $this->getIsDisabled() ) {
					$boolIsValid &= $this->valIsDisabled( $objDatabase, $arrobjProperties );
				}
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>