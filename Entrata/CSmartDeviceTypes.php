<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSmartDeviceTypes
 * Do not add any new functions to this class.
 */

class CSmartDeviceTypes extends CBaseSmartDeviceTypes {

	public static function fetchSmartDeviceTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSmartDeviceType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSmartDeviceType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSmartDeviceType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedSmartDeviceTypes( $objDatabase ) {

		$strSql = 'SELECT
				        sdt.*
					FROM
						 smart_device_types sdt
					WHERE
						sdt.is_published = true
					ORDER BY
						' . $objDatabase->getCollateSort( 'sdt.name' ) . ' ASC';

		return self::fetchSmartDeviceTypes( $strSql, $objDatabase );
	}

}
?>