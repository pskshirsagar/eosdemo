<?php

class CRevenueOptimalRentLog extends CBaseRevenueOptimalRentLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRevenueOptimalRentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportingPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectivePeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriorRevenueOptimalRentLogId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportingPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportingPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinExecutedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxExecutedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgExecutedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastExecutedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinAdvertisedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxAdvertisedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgAdvertisedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStdAdvertisedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinOptimalRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxOptimalRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgOptimalRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStdOptimalRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinCompetitiveRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxCompetitiveRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgCompetitiveRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOptimalBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNoticePeriodAdjustment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStalenessPeriodAdjustment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinimumLeasedAdjustment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSustainableOccupancyAdjustment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOptimalOccupancyRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOptimalOccupancyRentConstrained() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOptimalAdvertisedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvailableUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupiedUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRentableUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExposed30UnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExposed60UnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExposed90UnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDemandThirtyUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDemandSixtyUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDemandNinetyUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWeeklyGrowthRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonthlyGrowthRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQuarterlyGrowthRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valYearlyGrowthRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFirstMoveInDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMatrix() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostMonthIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostDateIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOpeningLog() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStdVacancyLossAdjustment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStdTurnCostAdjustment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStdExpirationAdjustment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOptimalAdvertisedRentUnconstrained() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>