<?php

class CFormAnswer extends CBaseFormAnswer {

    public function valId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valQuestion() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getQuestion() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'question', '' ) );
        // }

        return $boolIsValid;
    }

    public function valAnswer() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getAnswer() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'answer', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Other Functions
     */

    public function encryptAnswer() {
        $this->setAnswer( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $this->getAnswer(), CONFIG_SODIUM_KEY_FORM_ANSWERS ) );
		return;
    }

    public function decryptAnswer() {
        $this->setAnswer( \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getAnswer(), CONFIG_SODIUM_KEY_FORM_ANSWERS, [ 'legacy_secret_key' => CONFIG_KEY_FORM_ANSWERS ] ) );
		return;
    }

}
?>