<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningDocumentExportRequestStatusTypes
 * Do not add any new functions to this class.
 */

class CScreeningDocumentExportRequestStatusTypes extends CBaseScreeningDocumentExportRequestStatusTypes {

	public static function fetchScreeningDocumentExportRequestStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CScreeningDocumentExportRequestStatusType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningDocumentExportRequestStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CScreeningDocumentExportRequestStatusType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllScreeningDocumentExportRequestStatusTypes( $objDatabase ) {
		return fetchData( 'select id, description from screening_document_export_request_status_types where is_published = true', $objDatabase );
	}

}
?>