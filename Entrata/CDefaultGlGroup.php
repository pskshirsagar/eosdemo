<?php

class CDefaultGlGroup extends CBaseDefaultGlGroup {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlAccountTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultGlTreeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentDefaultGlGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginDefaultGlGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlGroupTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalLabel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHideOnReports() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsTotaled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsUnderlined() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsBold() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>