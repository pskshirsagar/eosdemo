<?php

class CSystemMessageTemplateSlot extends CBaseSystemMessageTemplateSlot {

	protected $m_intMediaWidth;
	protected $m_intMediaHeight;
	protected $m_strAlt;

	/**
		* SetValue Functions
	*/

	public function setValues( $arrintValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrintValues, $boolStripSlashes, $boolDirectSet );

		if( false == is_null( $arrintValues['media_width'] ) ) $this->setMediaWidth( $arrintValues['media_width'] );
		if( false == is_null( $arrintValues['media_height'] ) ) $this->setMediaHeight( $arrintValues['media_height'] );

 		return true;
	}

	/**
		* Set Functions
	*/

	public function setMediaWidth( $intMediaWidth ) {
		$this->m_intMediaWidth = $intMediaWidth;
	}

	public function setMediaHeight( $intMediaHeight ) {
		$this->m_intMediaHeight = $intMediaHeight;
	}

	public function setAlt( $strAlt ) {
		$this->m_strAlt = $strAlt;
	}

	/**
		* Get Functions
	*/

	public function getMediaWidth() {
		return $this->m_intMediaWidth;
	}

	public function getMediaHeight() {
		return $this->m_intMediaHeight;
	}

	public function getAlt() {
		return $this->m_strAlt;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemMessageTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultImageName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBgcolor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMediaWidth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMediaHeight() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>