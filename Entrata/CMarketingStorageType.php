<?php

class CMarketingStorageType extends CBaseMarketingStorageType {

	const MEDIA_LIBRARY		= 1;
	const OBJECT_STORAGE	= 2;
	const AMAZON_S3			= 3;
	const EXTERNAL_MEDIA	= 4;

}
?>