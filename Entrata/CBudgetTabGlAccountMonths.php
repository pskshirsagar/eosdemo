<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetTabGlAccountMonths
 * Do not add any new functions to this class.
 */

class CBudgetTabGlAccountMonths extends CBaseBudgetTabGlAccountMonths {

	public static function fetchBudgetTabGlAccountMonthsByBudgetDataSourceTypeIdsByBudgetIdByCid( $arrintBudgetDataSourceTypeIds, $intBudgetId, $intCid, $objDatabase ) {
		if( false == valIntArr( $arrintBudgetDataSourceTypeIds ) || false == valId( $intBudgetId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						btgam.*,
						btga.budget_data_source_type_id,
						btga.formula
					FROM
						budget_tab_gl_account_months btgam
						JOIN budget_tab_gl_accounts btga ON ( btga.cid = btgam.cid AND btga.id = btgam.budget_tab_gl_account_id )
					WHERE
						btgam.cid = ' . ( int ) $intCid . '
						AND btgam.budget_id = ' . ( int ) $intBudgetId . '
						AND btga.budget_data_source_type_id IN ( ' . sqlIntImplode( $arrintBudgetDataSourceTypeIds ) . ' )';

		return self::fetchBudgetTabGlAccountMonths( $strSql, $objDatabase );
	}

	public static function fetchBudgetTabGlAccountMonthsBudgetDetailsByBudgetIdByCid( $intBudgetId, $intCid, $objDatabase, $intBudgetTabId = NULL ) {

		if( false == valId( $intBudgetId ) || false == valId( $intCid ) ) return NULL;

		$strSelectFields  = '';
		$strJoinCondition = '';
		if( true == valId( $intBudgetTabId ) ) {
			$strSelectFields  = ',  btgam.id,
									btgam.budget_tab_gl_account_id,
									btga.budget_data_source_type_id, 
									COALESCE( btgam.original_amount, 0 ) AS original_amount,
									count( bgaa.comment )  OVER ( PARTITION BY btgam.cid, btgam.budget_id, btgam.budget_tab_gl_account_id, btgam.post_month ) AS comment_count
			';
			$strJoinCondition = ' LEFT JOIN budget_gl_account_activities bgaa ON ( bgaa.cid = btgam.cid AND bgaa.budget_id = btgam.budget_id AND bgaa.budget_tab_gl_account_id = btgam.budget_tab_gl_account_id AND bgaa.post_month = btgam.post_month AND bgaa.budget_activity_type_id = ' . CBudgetActivityType::COMMENT . ' AND bgaa.deleted_by IS NULL )';
		}

		$strSql = 'SELECT
						DISTINCT gat.gl_account_id ,
						gat.formatted_account_number AS gl_account_number,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS gl_account_name,
						SUM( COALESCE( btgam.amount, 0 ) ) OVER( PARTITION BY  btgam.cid, btgam.budget_id,  btga.gl_account_id ) AS total,
						SUM( COALESCE( btgam.amount, 0 ) ) OVER( PARTITION BY  btgam.cid, btgam.budget_id,  btga.gl_account_id, btgam.post_month )  AS amount,
						btgam.post_month
						' . $strSelectFields . '
					FROM
						budget_tab_gl_account_months btgam 
						JOIN budget_tab_gl_accounts btga ON ( btga.cid = btgam.cid AND btga.id = btgam.budget_tab_gl_account_id )
						JOIN gl_account_trees gat ON ( gat.cid = btga.cid AND gat.gl_account_id = btga.gl_account_id )
						JOIN gl_trees gt ON ( gt.cid = gat.cid AND gt.id = gat.gl_tree_id AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						' . $strJoinCondition . '
					WHERE
						btgam.cid = ' . ( int ) $intCid . '
						AND btgam.budget_id = ' . ( int ) $intBudgetId . ( ( true == valId( $intBudgetTabId ) ) ? ' AND btga.budget_tab_id =' . ( int ) $intBudgetTabId : NULL ) . '
					ORDER BY
						gl_account_number,
						gl_account_name, 
						btgam.post_month';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchBudgetTabGlAccountMonthsByBudgetIdByBudgetAssumptionMonthIdsByCid( $intBudgetId, $arrintBudgetAssumptionMonthIds, $intCid, $objDatabase ) {

		if( false == valId( $intBudgetId ) || false == valIntArr( $arrintBudgetAssumptionMonthIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					DISTINCT ON ( btgam.id, btgam.cid ) btgam.*,
					btga.formula,
					btga.budget_tab_id
				FROM
					budget_tab_gl_account_months btgam
					JOIN budget_tab_gl_accounts btga ON ( btga.cid = btgam.cid AND btga.id = btgam.budget_tab_gl_account_id AND btga.budget_id = btgam.budget_id )
					JOIN budget_assumption_months bam ON ( bam.cid = btga.cid AND bam.post_month = btgam.post_month AND bam.budget_id = btga.budget_id )
					JOIN budget_assumptions ba ON ( ba.cid = bam.cid AND ba.id = bam.budget_assumption_id AND bam.budget_id = ba.budget_id )
				WHERE
					btgam.cid = ' . ( int ) $intCid . '
					AND btgam.budget_id = ' . ( int ) $intBudgetId . '
					AND btga.budget_data_source_type_id = ' . CBudgetDataSourceType::CUSTOM_FORMULAS . '
					AND bam.id IN ( ' . sqlIntImplode( $arrintBudgetAssumptionMonthIds ) . ' )
					AND btga.formula LIKE \'%\' || ba.key || \'%\'';

		return self::fetchBudgetTabGlAccountMonths( $strSql, $objDatabase );

	}

	public static function fetchBudgetTabGlAccountMonthsByIdsByBudgetIdByCid( $arrintIds, $intBudgetId, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintIds ) || false == valId( $intBudgetId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						btgam.*,
						btga.budget_tab_id
					FROM
						budget_tab_gl_account_months btgam
						JOIN budget_tab_gl_accounts btga ON ( btga.cid = btgam.cid AND btga.id = btgam.budget_tab_gl_account_id AND btga.budget_id = btgam.budget_id )
					WHERE
						btgam.cid = ' . ( int ) $intCid . '
						AND btgam.budget_id = ' . ( int ) $intBudgetId . '
						AND btgam.id IN ( ' . sqlIntImplode( $arrintIds ) . ' )';

		return self::fetchBudgetTabGlAccountMonths( $strSql, $objDatabase );
	}

	public static function fetchBudgetTabGlAccountMonthsByBudgetIdByBudgetTabIdByCid( $intBudgetId, $intBudgetTabId, $intCid, $objDatabase ) {

		if( false == valId( $intBudgetId ) || false == valId( $intBudgetTabId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						btgam.*
					FROM
						budget_tab_gl_account_months btgam
						JOIN budget_tab_gl_accounts btga ON ( btga.cid = btgam.cid AND btga.id = btgam.budget_tab_gl_account_id AND btga.budget_id = btgam.budget_id )
					WHERE
						btgam.cid = ' . ( int ) $intCid . '
						AND btgam.budget_id = ' . ( int ) $intBudgetId . '
						AND btga.budget_tab_id = ' . ( int ) $intBudgetTabId;

		return self::fetchBudgetTabGlAccountMonths( $strSql, $objDatabase );
	}

}
?>