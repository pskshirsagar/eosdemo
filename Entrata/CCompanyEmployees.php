<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployees
 * Do not add any new functions to this class.
 */
class CCompanyEmployees extends CBaseCompanyEmployees {
	/**
	* Fetch Functions
	*
	*/

	public static function fetchCachedCompanyEmployees( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchCachedObjects( $strSql, 'CCompanyEmployee', $objDatabase, DATA_CACHE_MEMORY, 300, true, $boolIsReturnKeyedArray );
	}

	// Renaming this function as Cached because this table is dynamic and we are unnecessary storing data in cache by using this function in other functions. If we need to store data in cache then please use fetchCachedCompanyEmployee instead of fetchCompanyEmployee.[DevOps]

	public static function fetchCachedCompanyEmployee( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CCompanyEmployee', $objDatabase, DATA_CACHE_MEMORY, 300, true );
    }

	public static function fetchCompanyEmployeeByRemotePrimaryKeyByCid( $strRemotePrimaryKey, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployee( sprintf( 'SELECT * FROM company_employees WHERE cid::integer = %d AND lower( remote_primary_key ) = \'%s\' ', ( int ) $intCid, addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) trim( $strRemotePrimaryKey ) ) ) ), $objDatabase );
	}

	public static function fetchCompanyEmployeeByNameFirstByNameLastByCid( $strNameFirst, $strNameLast, $intCid, $objDatabase ) {
		$strSql = 'SELECT *
						FROM
							 company_employees
						WHERE
							lower( name_first ) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strNameFirst ) ), 50, NULL, true ) . '\'
							AND lower(name_last) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strNameLast ) ), 50, NULL, true ) . '\'
							AND cid = ' . ( int ) $intCid . '
						LIMIT 1';

		return self::fetchCompanyEmployee( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesByNameByClient( $strNameFirst, $strNameLast, $intCid, $objDatabase ) {
		$strSql = 'SELECT ce.*
						FROM
							 company_employees ce
						WHERE
							lower( trim( FROM ce.name_first ) ) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strNameFirst ) ), 50, NULL, true ) . '\'
							AND ce.cid = ' . ( int ) $intCid;
		$strSql .= ( true == valStr( $strNameLast ) ) ? ' AND lower( trim( FROM ce.name_last ) ) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strNameLast ) ), 50, NULL, true ) . '\'' : '';

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeByNameFirstByNameLastByEmailAddressByCid( $strNameFirst, $strNameLast, $strEmailAddress, $intCid, $objDatabase ) {
		$strSql = 'SELECT *
						FROM
							 company_employees
						WHERE
							lower(name_first) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strNameFirst ) ), 50, NULL, true ) . '\'
							AND lower(name_last) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strNameLast ) ), 50, NULL, true ) . '\'
							AND lower(email_address) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strEmailAddress ) ), 50, NULL, true ) . '\'
							AND cid = ' . ( int ) $intCid . '
						LIMIT 1';

		return self::fetchCompanyEmployee( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeByNameFirstWithNameLastNullByCid( $strNameFirst, $intCid, $objDatabase ) {
		$strSql = 'SELECT *
						FROM
							 company_employees
						WHERE
							lower(name_first) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strNameFirst ) ), 50, NULL, true ) . '\'
							AND name_last IS NULL
							AND cid = ' . ( int ) $intCid . '
						LIMIT 1';

		return self::fetchCompanyEmployee( $strSql, $objDatabase );
	}

	public static function fetchUnassociatedCompanyEmployeesByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ce.*,
						cu.username
					FROM
						company_employees ce
						LEFT JOIN company_users cu ON cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.id NOT IN ( SELECT cu.company_employee_id FROM company_users cu WHERE cu.cid = ' . ( int ) $intCid . ' AND cu.default_company_user_id IS NULL AND cu.company_employee_id IS NOT NULL AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						ORDER BY LOWER( CASE WHEN ce.name_last IS NOT NULL THEN ce.name_last || ce.name_first ELSE ce.name_first END ) ASC';

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesByIdsByCid( $arrintCompanyEmployeeIds, $intCid, $objDatabase, $boolIsSortOrder = false, $boolIsReturnArray = false, $boolIdFromNewWorkOrder = false ) {
		if( false == valArr( array_filter( ( array ) $arrintCompanyEmployeeIds ) ) )
			return NULL;

		$strSql = 'SELECT *';

		if( false != $boolIdFromNewWorkOrder ) {
			$strSql .= ', name_first||\' \'||name_last as namefull ';
		}

		$strSql .= ' FROM
						company_employees
					WHERE
						id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' )
						AND cid = ' . ( int ) $intCid;

			if( true == $boolIsSortOrder ) {
				$strSql .= ' ORDER BY lower(name_first), lower(name_last)';
			}

		if( false != $boolIsReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeUsersByEmployeeIdsByCid( $arrintCompanyEmployeeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyEmployeeIds ) )
			return NULL;

		$strSql = 'SELECT
						ce.*,
						cu.username
					FROM
						company_employees ce
						JOIN company_users cu ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
					WHERE
						ce.id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' )
						AND ce.cid = ' . ( int ) $intCid;

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchAssociatedCompanyEmployeesByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT 	ce.*,
							cu.is_administrator,
							cu.is_disabled,
							cu.username as username
					 FROM
					 		company_employees ce
					 JOIN 	company_users cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
					WHERE
							ce.cid = ' . ( int ) $intCid . '
					  AND	cu.is_disabled = 0
				 ORDER BY
				 			LOWER(ce.name_last), LOWER(ce.name_first)';

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchSimpleCompanyEmployeeByIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		$strSelectFields = 'ce.*';
		$strCompanyDepartmentsJoinSql = '';

		$strSql = 'SELECT
						' . $strSelectFields . '
					FROM
						company_employees ce
						' . $strCompanyDepartmentsJoinSql . '
					WHERE
						ce.id = ' . ( int ) $intCompanyEmployeeId . '
						AND ce.cid = ' . ( int ) $intCid;

		return parent::fetchCompanyEmployee( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeCountByIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						COUNT( ce.id )
					FROM
						company_employees ce
					WHERE
						ce.id = ' . ( int ) $intCompanyEmployeeId . '
						AND ce.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPreExistingCompanyEmployeeByEmailAddressByCid( $strEmailAddress, $intCompanyEmployeeId, $intCid, $objDatabase, $boolIsDisable = false, $intCompanyUserTypeId = CCompanyUserType::ENTRATA ) {
		$strCondition = '';

		if( false == is_null( $intCompanyEmployeeId ) && 0 < $intCompanyEmployeeId ) {
			$strCondition = ' AND ce.id != ' . ( int ) $intCompanyEmployeeId;
		}
		if( false == $boolIsDisable ) {
			$strCondition .= ' AND cu.is_disabled = 0';
		} else {
			$strCondition .= ' AND cu.is_disabled = 1';
		}
		$strSql = 'SELECT
						ce.*
					FROM
						company_employees ce
						JOIN company_users cu ON (ce.id = cu.company_employee_id AND ce.cid = cu.cid)
					WHERE
						ce.cid::INTEGER = ' . ( int ) $intCid . '
						AND lower ( ce.email_address ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strEmailAddress ) ) . '\'
						AND cu.company_user_type_id = ' . ( int ) $intCompanyUserTypeId . $strCondition . ' LIMIT 1';

		return self::fetchCompanyEmployee( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesOrderByNameByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
							*
					FROM
						company_employees
					WHERE
						cid =  ' . ( int ) $intCid . '
					ORDER BY lower(name_first), lower(name_last)';

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeByEmailAddressByCid( $strEmailAddress, $intCid, $objDatabase ) {
		if( false == valStr( $strEmailAddress ) )
			return NULL;

		$strSql = 'SELECT * FROM company_employees WHERE cid = ' . ( int ) $intCid . ' AND lower(email_address) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( trim( $strEmailAddress ) ) ) . '\' LIMIT 1';
		return self::fetchCompanyEmployee( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase, $boolIsDefaultCompanyUserId = true ) {
		$strSql = 'SELECT
						ce.*, cu.username,cu.company_user_type_id
					FROM
						company_employees ce,
						company_users cu
					WHERE
						cu.company_employee_id = ce.id
						AND cu.cid = ce.cid
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND	cu.cid = ' . ( int ) $intCid;

		if( true == $boolIsDefaultCompanyUserId ) {
			$strSql .= ' AND cu.default_company_user_id IS NULL';
		}

		$strSql .= ' AND cu.id = ' . ( int ) $intCompanyUserId . '
 					LIMIT 1';

		return self::fetchCompanyEmployee( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesByCompanyUserIdsByCid( $arrintCompanyUserIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyUserIds ) )
			return;

		$strSql = 'SELECT
						ce.*,
						cu.id AS company_user_id
					FROM
						company_employees ce
						JOIN company_users cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
					WHERE
						cu.id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )
						AND ce.cid = ' . ( int ) $intCid;

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchAllCompanyEmployeesByCompanyUserIdsByCid( $arrintCompanyUserIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyUserIds ) )
			return;

		$strSql = 'SELECT
						COALESCE(  NULLIF( concat_ws( \' \', ce.name_first::text, ce.name_last::text ), \'\' ), cu.username  ) as name,
						cu.id AS company_user_id
					FROM
						company_users cu
						LEFT JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						cu.id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )
						AND cu.cid = ' . ( int ) $intCid;

		$arrmixResponseData = ( array ) fetchData( $strSql, $objDatabase );

		foreach( $arrmixResponseData as $arrmixResponse ) {
			$arrmixCompanyEmployeeNames[$arrmixResponse['company_user_id']] = $arrmixResponse['name_first'] . ' ' . $arrmixResponse['name'];
		}

		return $arrmixCompanyEmployeeNames;
	}

	public static function fetchCompanyEmployeeNamesByCompanyUserIdsByCid( $arrintCompanyUserIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyUserIds ) )
			return;

		$arrmixCompanyEmployeeNames = [];

		$strSql = 'SELECT
						ce.name_first,
						ce.name_last,
						cu.id AS company_user_id
					FROM
						company_employees ce
						JOIN company_users cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
					WHERE
						cu.id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )
						AND ce.cid = ' . ( int ) $intCid;

		$arrmixResponseData = ( array ) fetchData( $strSql, $objDatabase );

		foreach( $arrmixResponseData as $arrmixResponse ) {
			$arrmixCompanyEmployeeNames[$arrmixResponse['company_user_id']] = $arrmixResponse['name_first'] . ' ' . $arrmixResponse['name_last'];
		}

		return $arrmixCompanyEmployeeNames;
	}

	public static function fetchCompanyEmployeesByPropertyIdByCid( $intPropertyId, $intCid, $boolIsDisabled = NULL, $intEmployeeStatusTypeId = NULL, $intCompanyUserId = NULL, $objDatabase ) {
		$strSubSql = '';

		$strSql = 'SELECT
						ce.*
					FROM
						company_employees ce
						LEFT JOIN company_users cu ON cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						LEFT JOIN view_company_user_properties cup ON cup.company_user_id = cu.id AND cup.cid = cu.cid
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.default_company_user_id IS NULL
						AND cu.username != \'PS Admin\' ';

		if( false == is_null( $boolIsDisabled ) && false == is_null( $intEmployeeStatusTypeId ) ) {
			$strSubSql .= ' AND
						(
							( cup.property_id = ' . ( int ) $intPropertyId . ' OR cu.is_administrator = 1 )
							AND ce.employee_status_type_id = 1
							AND cu.is_disabled = 0
						)';
		} elseif( false == is_null( $intCompanyUserId ) ) {
			$strSubSql .= ' AND
						(
							( cup.property_id = ' . ( int ) $intPropertyId . ' OR cu.is_administrator = 1 )
							AND ( ce.employee_status_type_id = 1 OR ce.id = ' . ( int ) $intCompanyUserId . ' )
							AND ( cu.is_disabled = 0 OR ce.id = ' . ( int ) $intCompanyUserId . ' )
						)';
		} else {
			$strSubSql .= ' AND (
								cup.property_id = ' . ( int ) $intPropertyId . ' OR cu.is_administrator = 1
							)';
		}

		$strSql .= $strSubSql . ' ORDER BY ' . $objDatabase->getCollateSort( 'lower(ce.name_first)' ) . ',' . $objDatabase->getCollateSort( 'lower(ce.name_last)' );

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchLeasingAgents( $intCid, $objClientDatabase, $strSortByField = NULL ) {
		$strSql = 'SELECT
						*
					FROM
						company_employees
					WHERE
							( name_first IS NOT NULL OR	name_last IS NOT NULL )
						AND cid = ' . ( int ) $intCid;

		if( true == is_null( $strSortByField ) ) {
			$strSql .= ' ORDER BY id';
		} else {
			$strSql .= ' ORDER BY ' . addslashes( $strSortByField );
		}

		return self::fetchCompanyEmployees( $strSql, $objClientDatabase );
	}

	public static function fetchEnabledCompanyEmployeesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ce.*
					FROM
						company_employees ce
						LEFT JOIN company_users cu ON cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						LEFT JOIN view_company_user_properties cup ON cup.company_user_id = cu.id AND cup.cid = cu.cid
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.default_company_user_id IS NULL
						AND ( cup.property_id = ' . ( int ) $intPropertyId . ' OR cu.is_administrator = 1 )
						AND cu.username != \'PS Admin\'
						AND cu.is_disabled=0
						AND cu.deleted_on IS NULL';

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesByPropertyIdByScheduledEmailFilterByCid( $intPropertyId, $objScheduledEmailFilter, $intCid, $objDatabase ) {

		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) ) return NULL;

		$arrstrWhere = [];

		if( true == valStr( $objScheduledEmailFilter->getCompanyUserTypes() ) ) {
			$strUserTypeWhere = [];

			if( false !== \Psi\CStringService::singleton()->strpos( $objScheduledEmailFilter->getCompanyUserTypes(), 'is_non_employee' ) ) {
				$strUserTypeWhere[] = '( ce.is_employee = 0 AND cu.is_administrator = 0 )';
			}
			if( false !== \Psi\CStringService::singleton()->strpos( $objScheduledEmailFilter->getCompanyUserTypes(), 'is_employee' ) ) {
				$strUserTypeWhere[] = '( ce.is_employee = 1 AND cu.is_administrator = 0 )';
			}
			if( false !== \Psi\CStringService::singleton()->strpos( $objScheduledEmailFilter->getCompanyUserTypes(), 'is_administrator' ) ) {
				$strUserTypeWhere[] = 'cu.is_administrator = 1';
			}

			$arrstrWhere[] = '(' . implode( ' OR ', $strUserTypeWhere ) . ')';
		}

		if( true == valStr( $objScheduledEmailFilter->getCompanyGroups() ) ) {
			$arrstrWhere[] = 'cg.id IN (' . $objScheduledEmailFilter->getCompanyGroups() . ')';
		}

		if( true == valStr( $objScheduledEmailFilter->getCompanyDepartments() ) ) {
			$arrstrWhere[]  = 'cd.id IN (' . $objScheduledEmailFilter->getCompanyDepartments() . ')';
		}

		$strSql = 'SELECT 
						DISTINCT ON (subquery.recipient_id, subquery.name_last)
						subquery.recipient_id,
						subquery.name_first,
						subquery.name_last,
						vcup.property_id,
						subquery.company_department_name,
						subquery.company_group_name,
						subquery.is_administrator,
						subquery.is_employee
					FROM
						(	
						SELECT
							ce.id AS recipient_id,
							ce.name_first,
							ce.name_last,
							( array_to_string ( array_agg ( DISTINCT ( util_get_translated( \'name\', cd.name, cd.details )  ) ), \', \' ) ) as company_department_name,
							( array_to_string ( array_agg ( DISTINCT ( util_get_translated( \'name\', cg.name, cg.details ) ) ), \', \' ) ) as company_group_name,
							cu.is_administrator,
							ce.is_employee,
							cu.id AS company_user_id,
	                        ce.cid
						FROM
							company_employees AS ce
							LEFT JOIN company_users AS cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
							LEFT OUTER JOIN scheduled_email_address_filters AS seaf ON ( seaf.company_employee_id = ce.id AND seaf.cid = ce.cid )
							LEFT JOIN company_employee_departments ced ON (ce.cid = ced.cid AND ce.id = ced.company_employee_id)
							LEFT JOIN company_departments cd ON ( ced.cid = cd.cid AND ced.company_department_id = cd.id )
							LEFT JOIN company_user_groups cug ON ( cug.cid = cu.cid AND cug.company_user_id = cu.id )
							LEFT JOIN company_groups cg ON ( cg.cid = cug.cid AND cg.id = cug.company_group_id )
						WHERE
							ce.cid = ' . ( int ) $intCid . '
							AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
							AND seaf.company_employee_id IS NULL
							AND cu.is_disabled = 0
							AND ce.email_address IS NOT NULL ' . self::loadScheduledEmailFilterSql( $objScheduledEmailFilter, [ $intPropertyId ] );

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere );
		}

		$strSql .= ' GROUP BY ce.id,
						ce.name_first,
						ce.name_last,
						cu.is_administrator,
						ce.is_employee,
						cu.id,
						ce.cid';

		if( 0 < $objScheduledEmailFilter->getRecipientCount() ) {
			$strSql .= ' LIMIT ' . $objScheduledEmailFilter->getRecipientCount();
		}

		$strSql .= '
					) as subquery
						LEFT JOIN view_company_user_properties vcup ON ( vcup.company_user_id = subquery.company_user_id AND vcup.cid = subquery.cid )
						LEFT JOIN properties pp ON ( pp.id = vcup.property_id AND pp.cid = subquery.cid )
					';
		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedCompanyEmployeesByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $objScheduledEmailFilter, $intCid, $objDatabase, $boolIsRequestToDownload = false ) {
		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) )
			return NULL;

		$arrstrWhere = [];

		if( false == is_null( $objScheduledEmailFilter->getPageNo() ) && false == is_null( $objScheduledEmailFilter->getCountPerPage() ) ) {
			$intOffset = ( 0 < $objScheduledEmailFilter->getPageNo() ) ? $objScheduledEmailFilter->getCountPerPage() * ( $objScheduledEmailFilter->getPageNo() - 1 ) : 0;
			$intLimit = ( int ) $objScheduledEmailFilter->getCountPerPage();
		}

		$strProperties = trim( $objScheduledEmailFilter->getProperties() );

		if( true == valArr( $arrintPropertyIds ) ) {
			$strProperties = implode( ',', $arrintPropertyIds );
		}

		if( true == $objScheduledEmailFilter->getShowUnsubscribedRecipients() && true == valArr( $objScheduledEmailFilter->getUnsubscribedRecipientIds() ) ) {
			$arrstrWhere[] = 'ce.id NOT IN ( ' . implode( ',', $objScheduledEmailFilter->getUnsubscribedRecipientIds() ) . ' )';
		}

		if( true == valId( $objScheduledEmailFilter->getIsExcludeRecipient() ) && 1 == $objScheduledEmailFilter->getIsExcludeRecipient() ) {
			$arrstrWhere[] = ' ce.email_address IS NOT NULL ';
		}

		if( false == is_null( $objScheduledEmailFilter->getStatusTypes() ) ) {
			$arrstrWhere[] = 'ce.employee_status_type_id IN (' . $objScheduledEmailFilter->getStatusTypes() . ')';
		}

		if( true == valStr( $objScheduledEmailFilter->getCompanyUserTypes() ) ) {
			$strUserTypeWhere = [];

			if( false !== \Psi\CStringService::singleton()->strpos( $objScheduledEmailFilter->getCompanyUserTypes(), 'is_non_employee' ) ) {
				$strUserTypeWhere[] = '(ce.is_employee = 0 AND cu.is_administrator = 0)';
			}
			if( false !== \Psi\CStringService::singleton()->strpos( $objScheduledEmailFilter->getCompanyUserTypes(), 'is_employee' ) ) {
				$strUserTypeWhere[] = '(ce.is_employee = 1 AND cu.is_administrator = 0)';
			}
			if( false !== \Psi\CStringService::singleton()->strpos( $objScheduledEmailFilter->getCompanyUserTypes(), 'is_administrator' ) ) {
				$strUserTypeWhere[] = 'cu.is_administrator = 1';
			}

			$arrstrWhere[] = '(' . implode( ' OR ', $strUserTypeWhere ) . ')';
		}

		if( true == valStr( $objScheduledEmailFilter->getCompanyGroups() ) ) {
			$arrstrWhere[] = 'cg.id IN (' . $objScheduledEmailFilter->getCompanyGroups() . ')';
		}

		if( true == valStr( $objScheduledEmailFilter->getCompanyDepartments() ) ) {
			$arrstrWhere[]  = 'cd.id IN (' . $objScheduledEmailFilter->getCompanyDepartments() . ')';
		}

		$arrstrOrderBy = [
			'customer_name_last'		=> 'ce.name_last',
			'customer_email_address'	=> 'ce.email_address',
			'created_on'				=> 'ce.created_on'
		];

		$strSortBy			= ( true == isset( $arrstrOrderBy[$objScheduledEmailFilter->getSortBy()] ) ? addslashes( $arrstrOrderBy[$objScheduledEmailFilter->getSortBy()] ) : $objDatabase->getCollateSort( 'ce.name_last' ) );
		$strSortDirection	= ( false == is_null( $objScheduledEmailFilter->getSortDirection() ) ) ? $objScheduledEmailFilter->getSortDirection() : 'ASC';

		$strSql = 'SELECT
						DISTINCT ON ( ' . $strSortBy . ', ce.id, ce.name_last )
						ce.id,
						ce.name_first,
						ce.name_last,
						ce.employee_status_type_id,
						ce.email_address,
						ce.date_started,
						ce.created_on,
						( CASE
							WHEN ce.phone_number1_type_id = ' . CPhoneNumberType::MOBILE . ' THEN phone_number1
							WHEN ce.phone_number2_type_id = ' . CPhoneNumberType::MOBILE . ' THEN phone_number2
						END ) AS phone_number,
						
						( array_to_string ( array_agg ( DISTINCT ( cd.name ) ), \', \' ) ) as company_department_names,
						( array_to_string ( array_agg ( DISTINCT ( cg.name  ) ), \', \' ) ) as company_group_name,
						cu.is_administrator,
						ce.is_employee
					FROM
						company_employees AS ce
						JOIN company_users AS cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						LEFT JOIN company_employee_departments ced ON (ce.cid = ced.cid AND ce.id = ced.company_employee_id)
						LEFT JOIN company_departments cd ON ( ced.cid = cd.cid AND ced.company_department_id = cd.id )
						LEFT JOIN company_user_groups cug ON ( cug.cid = cu.cid AND cug.company_user_id = cu.id )
						LEFT JOIN company_groups cg ON ( cg.cid = cug.cid AND cg.id = cug.company_group_id )
						LEFT JOIN scheduled_email_address_filters seaf ON ( seaf.cid = ce.cid AND seaf.company_employee_id = ce.id )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND cu.is_disabled = 0
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND seaf.id IS NULL ';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere );
		}

		$strSql .= 'AND ( cu.is_administrator = 1
                    OR EXISTS (
                        SELECT
                            1
                        FROM
                             view_company_user_properties cup
                        WHERE
                             cup.company_user_id = cu.id
                             AND cup.cid = cu.cid
                             AND cup.property_id IN ( ' . $strProperties . ' )
                     ) )';

		$strSql .= ' GROUP BY ce.id,
						ce.name_first,
						ce.name_last,
						ce.employee_status_type_id,
						ce.email_address,
						ce.date_started,
						ce.created_on,
						ce.phone_number1_type_id,
						ce.phone_number1,
						ce.phone_number2_type_id,
						ce.phone_number2,
						cu.is_administrator,
						ce.is_employee';

		if( false == is_null( $strSortBy ) && false == is_null( $strSortDirection ) ) {
			$strSql .= ' ORDER BY ' . $strSortBy . ' ' . addslashes( $strSortDirection );
		}

		if( false == $boolIsRequestToDownload ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchCompanyEmployees( $strSql, $objDatabase, false );
	}

	public static function fetchCompanyEmployeesCountByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $objScheduledEmailFilter, $intCid, $objDatabase ) {
		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) )
			return NULL;

		$arrstrWhere = [];
		$strProperties = trim( $objScheduledEmailFilter->getProperties() );

		if( true == valArr( $arrintPropertyIds ) ) {
			$arrstrWhere[] = '( cu.is_administrator = 1 OR cup.property_id IN (' . implode( ',', $arrintPropertyIds ) . ') )';
		} elseif( true == valStr( $strProperties ) ) {
			$arrstrWhere[] = '( cu.is_administrator = 1 OR cup.property_id IN (' . trim( $strProperties, ',' ) . ' ) )';
		}

		$strStatusTypes = trim( $objScheduledEmailFilter->getStatusTypes() );
		if( false == empty( $strStatusTypes ) ) {
			$arrstrWhere[] = 'ce.employee_status_type_id IN (' . $objScheduledEmailFilter->getStatusTypes() . ')';
		}

		$strSql = 'SELECT
						COUNT( DISTINCT ce.id )
					FROM
							company_employees AS ce
						LEFT JOIN company_users AS cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						LEFT JOIN view_company_user_properties AS cup ON ( cup.company_user_id = cu.id AND cup.cid = cu.cid )
						LEFT OUTER JOIN scheduled_email_address_filters AS seaf ON ( seaf.company_employee_id = ce.id AND seaf.cid = ce.cid )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND cu.is_disabled = 0
						AND seaf.company_employee_id IS NULL';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere );
		}

		$arrintCompanyEmployee = ( array ) fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintCompanyEmployee[0]['count'] ) )
			return $arrintCompanyEmployee[0]['count'];

		return 0;
	}

	public static function fetchSimpleRecipientsByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $objScheduledEmailFilter, $objDatabase, $strSelectedFields ) {
		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) || false == valStr( $strSelectedFields ) ) return NULL;

		$strSelectColumns = '';
		switch( $strSelectedFields ) {
			case 'recipients':
				$strSelectColumns = 'DISTINCT ce.id AS recipients';
				break;

			case 'count':
				$strSelectColumns = 'COUNT( DISTINCT ce.id )';
				break;

			default:
				return NULL;
		}

		if( false == valStr( $strSelectColumns ) ) return NULL;

		$arrstrWhere = [];

		$strProperties = trim( $objScheduledEmailFilter->getProperties() );

		if( true == valArr( $arrintPropertyIds ) ) {
			$strProperties = implode( ',', $arrintPropertyIds );
		}

		if( true == $objScheduledEmailFilter->getShowUnsubscribedRecipients() && true == valArr( $objScheduledEmailFilter->getUnsubscribedRecipientIds() ) ) {
			$arrstrWhere[] = 'ce.id NOT IN ( ' . implode( ',', $objScheduledEmailFilter->getUnsubscribedRecipientIds() ) . ' )';
		}

		if( true == valId( $objScheduledEmailFilter->getIsExcludeRecipient() ) && 1 == $objScheduledEmailFilter->getIsExcludeRecipient() ) {
			$arrstrWhere[] = ' ce.email_address IS NOT NULL ';
		}

		if( false == is_null( $objScheduledEmailFilter->getStatusTypes() ) ) {
			$arrstrWhere[] = 'ce.employee_status_type_id IN (' . $objScheduledEmailFilter->getStatusTypes() . ')';
		}

		if( true == valStr( $objScheduledEmailFilter->getCompanyUserTypes() ) ) {
			$strUserTypeWhere = [];

			if( false !== \Psi\CStringService::singleton()->strpos( $objScheduledEmailFilter->getCompanyUserTypes(), 'is_non_employee' ) ) {
				$strUserTypeWhere[] = 'ce.is_employee = 0 AND cu.is_administrator = 0';
			}
			if( false !== \Psi\CStringService::singleton()->strpos( $objScheduledEmailFilter->getCompanyUserTypes(), 'is_employee' ) ) {
				$strUserTypeWhere[] = '( ce.is_employee = 1 AND cu.is_administrator = 0 )';
			}
			if( false !== \Psi\CStringService::singleton()->strpos( $objScheduledEmailFilter->getCompanyUserTypes(), 'is_administrator' ) ) {
				$strUserTypeWhere[] = 'cu.is_administrator = 1';
			}

			$arrstrWhere[] = '(' . implode( ' OR ', $strUserTypeWhere ) . ')';
		}

		if( true == valStr( $objScheduledEmailFilter->getCompanyGroups() ) ) {
			$arrstrWhere[] = 'cg.id IN (' . $objScheduledEmailFilter->getCompanyGroups() . ')';
		}

		if( true == valStr( $objScheduledEmailFilter->getCompanyDepartments() ) ) {
			$arrstrWhere[] = 'cd.id IN (' . $objScheduledEmailFilter->getCompanyDepartments() . ')';
		}

		$strSql = 'SELECT ' . $strSelectColumns . '
					FROM
						company_employees AS ce
						LEFT JOIN company_users AS cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						LEFT JOIN company_employee_departments ced ON (ce.cid = ced.cid AND ce.id = ced.company_employee_id)
						LEFT JOIN company_departments cd ON ( ced.cid = cd.cid AND ced.company_department_id = cd.id )
						LEFT JOIN company_user_groups cug ON ( cug.cid = cu.cid AND cug.company_user_id = cu.id )
						LEFT JOIN company_groups cg ON ( cg.cid = cug.cid AND cg.id = cug.company_group_id )
						LEFT JOIN scheduled_email_address_filters seaf ON ( seaf.cid = ce.cid AND ce.id = seaf.company_employee_id )
					WHERE
						ce.cid = ' . ( int ) $objScheduledEmailFilter->getCid() . '
						AND cu.is_disabled = 0
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' 
						AND seaf.id IS NULL ';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere );
		}

		$strSql .= 'AND ( cu.is_administrator = 1
                    OR EXISTS (
                        SELECT
                            1
                        FROM
                             view_company_user_properties cup
                        WHERE
                             cup.company_user_id = cu.id
                             AND cup.cid = cu.cid
                             AND cup.property_id IN ( ' . $strProperties . ' )
                     ) ) ';

		$strSql .= ' GROUP BY ce.id, ce.name_first, ce.name_last';

		$arrmixResponseData = ( array ) fetchData( $strSql, $objDatabase );

		switch( $strSelectedFields ) {
			case 'recipients':
				$arrmixAllRecipients = [];
				foreach( $arrmixResponseData as $arrmixReponse ) {
					$arrmixAllRecipients[] = $arrmixReponse['recipients'];
				}
				return $arrmixAllRecipients;

			case 'count':
				if( true == isset( $arrmixResponseData[0]['count'] ) )
					return $arrmixResponseData[0]['count'];

				return 0;

			default:
				return NULL;
		}
	}

	public static function loadScheduledEmailFilterSql( $objScheduledEmailFilter, $arrintPropertyIds ) {
		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) ) return NULL;

		$arrstrWhere = [];
		$strSqlCondition = '';

		$strProperties = $objScheduledEmailFilter->getProperties();
		if( true == valArr( $arrintPropertyIds ) ) {
			$strProperties = implode( ',', $arrintPropertyIds );
		}

		if( true == valStr( $strProperties ) ) {
			$arrstrWhere[] = '( cu.is_administrator = 1
                              OR EXISTS (
                                          SELECT
                                            1
                                          FROM
                                            view_company_user_properties cup
                                          WHERE
				                            cup.company_user_id = cu.id
				                            AND cup.cid = cu.cid
				                            AND cup.property_id IN ( ' . $strProperties . ' )
                                          ) 
                            )';
		}

		if( true == $objScheduledEmailFilter->getShowBlockedRecipients() && true == valArr( $objScheduledEmailFilter->getBlockedRecipientIds() ) ) {
			$arrstrWhere[] = 'ce.id NOT IN ( ' . implode( ',', $objScheduledEmailFilter->getBlockedRecipientIds() ) . ' )';
		}

		if( true == $objScheduledEmailFilter->getShowUnsubscribedRecipients() && true == valArr( $objScheduledEmailFilter->getUnsubscribedRecipientIds() ) ) {
			$arrstrWhere[] = 'ce.id NOT IN ( ' . implode( ',', $objScheduledEmailFilter->getUnsubscribedRecipientIds() ) . ' )';
		}

		if( true == valStr( $objScheduledEmailFilter->getStatusTypes() ) ) {
			$arrstrWhere[] = ' ce.employee_status_type_id IN (' . $objScheduledEmailFilter->getStatusTypes() . ')';
		}

		if( true == $objScheduledEmailFilter->getShowEventRecipients() && true == valStr( $objScheduledEmailFilter->getScheduledEmailEventTypeId() ) ) {
			switch( $objScheduledEmailFilter->getScheduledEmailEventTypeId() ) {
				case CScheduledEmailEventType::BIRTHDAY:
					if( 1 == $objScheduledEmailFilter->getIsAfterEvent() ) {
						$arrstrWhere[] = 'date_part( \'day\', ce.birth_date + INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = date_part(\'day\', CURRENT_DATE )
										AND date_part( \'month\', ce.birth_date + INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = date_part( \'month\', CURRENT_DATE )';
					} else {
						$arrstrWhere[] = 'date_part( \'day\', ce.birth_date - INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = date_part(\'day\', CURRENT_DATE )
										AND date_part( \'month\', ce.birth_date - INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = date_part( \'month\', CURRENT_DATE )';
					}
					break;

				case CScheduledEmailEventType::EMPLOYEE_CREATED_DATE:
					if( 1 == $objScheduledEmailFilter->getIsAfterEvent() ) {
						$arrstrWhere[] = 'ce.created_on IS NOT NULL AND ( DATE_TRUNC( \'day\', ce.created_on ) + INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
					} else {
						$arrstrWhere[] = 'ce.created_on IS NOT NULL AND ( DATE_TRUNC( \'day\', ce.created_on ) - INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
					}
					break;

				default:
					// default case
					break;
			}
		}

		if( true == valArr( $arrstrWhere ) ) {
			$strSqlCondition .= ' AND ';
			$strSqlCondition .= implode( ' AND ', $arrstrWhere );
		}

		return $strSqlCondition;
	}

	public static function fetchCompanyEmployeesByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $objScheduledEmailFilter, $intCid, $objDatabase, $boolIsRestrictBlockedRecipients = false ) {
		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) ) return NULL;

		$arrstrWhere = [];
		$strJoin = '';

		if( true == valStr( $objScheduledEmailFilter->getCompanyUserTypes() ) ) {
			$strUserTypeWhere = [];

			if( false !== \Psi\CStringService::singleton()->strpos( $objScheduledEmailFilter->getCompanyUserTypes(), 'is_non_employee' ) ) {
				$strUserTypeWhere[] = '(ce.is_employee = 0 AND cu.is_administrator = 0)';
			}
			if( false !== \Psi\CStringService::singleton()->strpos( $objScheduledEmailFilter->getCompanyUserTypes(), 'is_employee' ) ) {
				$strUserTypeWhere[] = '(ce.is_employee = 1 AND cu.is_administrator = 0)';
			}
			if( false !== \Psi\CStringService::singleton()->strpos( $objScheduledEmailFilter->getCompanyUserTypes(), 'is_administrator' ) ) {
				$strUserTypeWhere[] = 'cu.is_administrator = 1';
			}

			$arrstrWhere[] = '(' . implode( ' OR ', $strUserTypeWhere ) . ')';
		}

		if( true == valStr( $objScheduledEmailFilter->getCompanyGroups() ) ) {
			$arrstrWhere[] = 'cg.id IN (' . $objScheduledEmailFilter->getCompanyGroups() . ')';
		}

		if( true == valStr( $objScheduledEmailFilter->getCompanyDepartments() ) ) {
			$arrstrWhere[]  = 'cd.id IN (' . $objScheduledEmailFilter->getCompanyDepartments() . ')';
		}

		$strProperties = $objScheduledEmailFilter->getProperties();
		if( true == valArr( $arrintPropertyIds ) ) {
			$strProperties = implode( ',', $arrintPropertyIds );
		}

		$arrintTempPropertyIds = explode( ',', $strProperties );
		$intPropertyId = current( $arrintTempPropertyIds );

		if( true == $boolIsRestrictBlockedRecipients ) {
			$strJoin = ' LEFT JOIN LATERAL(
											SELECT id FROM 
											(
												SELECT
													id,
													regexp_split_to_array ( blocked_recipients, \',\' ) AS blocked_recipients
												FROM
													scheduled_email_filters
												WHERE 
													id = ' . ( int ) $objScheduledEmailFilter->getId() . '
											 ) AS blocked_recipient_lists
											 WHERE
													ce.id::TEXT = ANY( blocked_recipient_lists.blocked_recipients )
										) AS sef ON ( true )';

			$arrstrWhere[] = ' sef.id IS NULL AND ce.email_address IS NOT NULL';
		}

		$strSql = 'SELECT
						DISTINCT ON( subquery.id, subquery.name_last )
						subquery.id,
						subquery.name_first,
						subquery.name_last,
						subquery.employee_status_type_id,
						subquery.email_address,
						subquery.phone_number1,
						subquery.phone_number2,
						subquery.phone_number1_type_id,
						subquery.phone_number2_type_id,
						subquery.date_started,
						subquery.created_on,
						subquery.is_administrator,
						COALESCE( vcup.property_id, ' . ( int ) $intPropertyId . ' ) as property_id,
						COALESCE( pp.property_name, ( SELECT property_name FROM properties WHERE id = ' . ( int ) $intPropertyId . ' ) ) property_name,
						subquery.company_department_name,
						subquery.company_group_name,
						subquery.is_administrator,
						subquery.is_employee 
					FROM
					(
					    SELECT
							ce.id,
							ce.name_first,
							ce.name_last,
							ce.employee_status_type_id,
							ce.email_address,
							ce.phone_number1,
							ce.phone_number2,
							ce.phone_number1_type_id,
							ce.phone_number2_type_id,
							ce.date_started,
							ce.created_on,
							( array_to_string ( array_agg ( DISTINCT ( cd.name ) ), \', \' ) ) as company_department_name,
							( array_to_string ( array_agg ( DISTINCT ( cg.name  ) ), \', \' ) ) as company_group_name,
							cu.is_administrator,
							ce.is_employee,
							cu.id AS company_user_id,
							ce.cid
						FROM
							company_employees AS ce
							LEFT JOIN company_users AS cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
							LEFT OUTER JOIN scheduled_email_address_filters AS seaf ON ( seaf.company_employee_id = ce.id AND seaf.cid = ce.cid )
							LEFT JOIN company_employee_departments ced ON (ce.cid = ced.cid AND ce.id = ced.company_employee_id)
							LEFT JOIN company_departments cd ON ( ced.cid = cd.cid AND ced.company_department_id = cd.id )
							LEFT JOIN company_user_groups cug ON ( cug.cid = cu.cid AND cug.company_user_id = cu.id )
							LEFT JOIN company_groups cg ON ( cg.cid = cug.cid AND cg.id = cug.company_group_id )
							' . $strJoin . '
						WHERE
							ce.cid = ' . ( int ) $intCid . '
							AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
							AND seaf.company_employee_id IS NULL
							AND cu.is_disabled = 0
							AND ce.email_address IS NOT NULL ' . self::loadScheduledEmailFilterSql( $objScheduledEmailFilter, $arrintPropertyIds );

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere );
		}

		$strSql .= ' GROUP BY ce.id,
						ce.name_first,
						ce.name_last,
						ce.employee_status_type_id,
						ce.email_address,
						ce.created_on,
						ce.phone_number1_type_id,
						ce.phone_number1,
						ce.phone_number2_type_id,
						ce.phone_number2,
						ce.date_started,
						cu.is_administrator,
						ce.is_employee,
						cu.id,
						ce.cid';

		$strSql .= '
					) as subquery
					LEFT JOIN view_company_user_properties vcup ON ( vcup.company_user_id = subquery.company_user_id AND vcup.cid = subquery.cid AND vcup.property_id IN ( ' . $strProperties . ' ) )
					LEFT JOIN properties pp ON ( pp.id = vcup.property_id AND pp.cid = subquery.cid )';

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveLeasingAgentsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $intCompanyDepartmentId = NULL, $boolIsLeasingAdmin = false ) {
		if( false == valArr( $arrintPropertyIds ) )
			return NULL;

		$strSql = '
			SELECT *
			FROM (
	            SELECT DISTINCT
					ce.*
				FROM
					property_leasing_agents AS pla
					JOIN company_employees AS ce ON pla.company_employee_id = ce.id AND pla.cid = ce.cid
					LEFT JOIN company_users AS cu ON cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
				WHERE
					pla.cid = ' . ( int ) $intCid . '
					AND pla.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' . '
					AND COALESCE( pla.is_disabled, 0 ) = 0
					AND COALESCE( cu.is_disabled, 0 ) = 0
					AND EXISTS (
						SELECT
                           1
                        FROM
                           company_departments cd
                           JOIN company_employee_departments ced ON ced.cid = cd.cid AND ced.company_department_id = cd.id
                        WHERE
                           ced.cid = ' . ( int ) $intCid . '
                           AND ced.company_employee_id = ce.id
                           AND lower( cd.name ) = \'' . \Psi\CStringService::singleton()->strtolower( CCompanyDepartment::LEASING ) . '\' 
                ) ';

		if( false == is_null( $intCompanyDepartmentId ) ) {
			$strSql .= self::buildSqlToFetchCompanyEmployeesByDepertmentIdByPropertyIdsByCid( $intCompanyDepartmentId, $arrintPropertyIds, $intCid, $boolIsActive = true, $boolIsLeasingAdmin );
		}

		$strSql .= ' ) AS res ORDER BY res.name_first ASC, res.name_last';

		return self::fetchCompanyEmployees( $strSql, $objClientDatabase );
	}

	public static function fetchLeasingAgentsByPropertyIdsByIdByCid( $arrintPropertyIds, $intId, $intCid, $objClientDatabase, $intCompanyDepartmentId = NULL ) {
		if( false == valArr( $arrintPropertyIds ) )
			return NULL;

		$strSql = 'SELECT * FROM (
		            SELECT
						DISTINCT( ce.* )
					FROM
					   property_leasing_agents AS pla
					   JOIN company_employees AS ce ON ( pla.company_employee_id = ce.id AND pla.cid = ce.cid )
					WHERE
		               ce.cid = ' . ( int ) $intCid . '
					   AND (
		                   ce.id = ' . ( int ) $intId . '
    					   OR (
								COALESCE( pla.is_disabled, 0 ) = 0
								AND pla.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ) ';

		if( false == is_null( $intCompanyDepartmentId ) ) {
			$strSql .= self::buildSqlToFetchCompanyEmployeesByDepertmentIdByPropertyIdsByCid( $intCompanyDepartmentId, $arrintPropertyIds, $intCid, $boolIsActive = true );
		}

		$strSql .= ' ) ) AS res ORDER BY res.name_first ASC, res.name_last';

		return self::fetchCompanyEmployees( $strSql, $objClientDatabase );
	}

	public static function fetchPaginatedAssociatedCompanyEmployeesByCid( $intPageNo, $intPageSize, $intCid, $objDatabase, $intIsDisabled, $arrmixSearchCriteria = [], $objCompanyUser = NULL, $intCompanyUserTypeId = CCompanyUserType::ENTRATA ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strConditions = '';
		if( 0 == $intIsDisabled ) {
			$strConditions .= ' AND cu.is_disabled = 0';
		} elseif( 1 == $intIsDisabled ) {
			$strConditions .= ' AND cu.is_disabled = 1';
		}

		if( true == valArr( $arrmixSearchCriteria ) ) {

			if( true == valStr( $arrmixSearchCriteria['search_text'] ) ) {

				$strCompanyUserSearchCriteria = addslashes( $arrmixSearchCriteria['search_text'] );

				$strConditions .= ' AND (
										lower( ce.name_first || \' \' || ce.name_last ) LIKE lower( \'%' . $strCompanyUserSearchCriteria . '%\' )
										OR lower( ce.name_last || \' \' || ce.name_first ) LIKE lower( \'%' . $strCompanyUserSearchCriteria . '%\' )
										OR lower( cu.username ) LIKE lower( \'%' . $strCompanyUserSearchCriteria . '%\' )
										OR lower( ce.email_address ) LIKE lower( \'%' . preg_replace( '/\s+/', '', $strCompanyUserSearchCriteria ) . '%\' )
									)';
			}
			if( true == valStr( $arrmixSearchCriteria['is_active_directory_user'] ) ) {
				$strConditions .= ' AND cu.is_active_directory_user = ' . $arrmixSearchCriteria['is_active_directory_user'];
			}
		}

		if( true != is_null( $objCompanyUser ) && true == valObj( $objCompanyUser, 'CCompanyUser' ) && false == $objCompanyUser->getIsAdministrator() && false == $objCompanyUser->getIsPSIUser() ) {
			$strConditions .= ' AND cu.id != ' . ( int ) $objCompanyUser->getId() . '
								AND cu.is_administrator = 0 ';
		}

		$strSql = 'SELECT
						ce.*,
						cu.id AS company_user_id,
						cu.is_administrator,
						cu.is_disabled,
						cu.username as username
					FROM
						company_employees ce
						INNER JOIN company_users cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . ( int ) $intCompanyUserTypeId . ' )
					WHERE
						cu.cid = ' . ( int ) $intCid . $strConditions . '
						AND cu.default_company_user_id IS NULL
						AND cu.id != 1
						AND cu.username NOT LIKE  \'PS Admin%\'
						AND cu.deleted_on IS NULL
					ORDER BY
						LOWER(ce.name_first), LOWER(ce.name_last),LOWER(cu.username)
						OFFSET ' . ( int ) $intOffset . '
						LIMIT ' . ( int ) $intLimit;

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchAssociatedCompanyEmployeesCountByCid( $intCid, $objDatabase, $boolIsDisabled = 0, $strCompanyUserSearchCriteria = NULL, $objCompanyUser = NULL ) {
		$strConditions = '';
		if( false == $boolIsDisabled ) {
			$strConditions .= ' AND cu.is_disabled = 0';
		} else {
			$strConditions .= ' AND cu.is_disabled = 1';
		}

		if( false == is_null( $strCompanyUserSearchCriteria ) ) {
			$strCompanyUserSearchCriteria = addslashes( $strCompanyUserSearchCriteria );
			$strConditions .= ' AND (
										lower( ce.name_last ) LIKE lower( \'%' . addslashes( $strCompanyUserSearchCriteria ) . '%\' )
										OR lower( ce.name_first ) LIKE lower( \'%' . addslashes( $strCompanyUserSearchCriteria ) . '%\' )
										OR lower( cu.username ) LIKE lower( \'%' . addslashes( $strCompanyUserSearchCriteria ) . '%\' )
									)';
		}

		if( true != is_null( $objCompanyUser ) && true == valObj( $objCompanyUser, 'CCompanyUser' ) && false == $objCompanyUser->getIsAdministrator() && false == $objCompanyUser->getIsPSIUser() ) {
			$strConditions .= ' AND cu.id != ' . ( int ) $objCompanyUser->getId() . '
								AND cu.is_administrator = 0';
		}

		$strSql = 'SELECT
						count(ce.id) as count
					FROM
						company_employees ce
						INNER JOIN company_users cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
					WHERE
						cu.cid = ' . ( int ) $intCid . $strConditions . '
						AND cu.default_company_user_id IS NULL
						AND cu.id != 1
						AND cu.username NOT LIKE  \'PS Admin%\'
						AND cu.deleted_on IS NULL';

		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrstrData ) ) ? $arrstrData[0]['count'] : 0;
	}

	public static function fetchPaginatedAssociatedCompanyEmployeesByGroupIdByCid( $intPageNo, $intPageSize, $intGroupId, $intCid, $objDatabase, $boolIncludeAdminUsers ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = 'SELECT
						ce.id,
						ce.name_first,
						ce.name_last,
						ce.email_address,
						cu.is_administrator,
						cu.is_disabled,
						cu.username as username,
						cu.id as company_user_id,
						cug.id as company_user_group_id
					FROM
						company_employees ce
						JOIN company_users cu ON ( ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' ) ';
		if( false == $boolIncludeAdminUsers ) {
			$strSql .= ' AND cu.is_administrator = 0  ';
		}

		$strSql .= ' ) JOIN company_user_groups cug ON ( cug.company_user_id = cu.id AND cug.cid = cu.cid )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND cug.company_group_id = ' . ( int ) $intGroupId . '
						AND cu.is_disabled = 0
					ORDER BY
						LOWER(ce.name_first), LOWER(ce.name_last)
						OFFSET ' . ( int ) $intOffset . '
						LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAssociatedCompanyEmployeesCountByGroupIdByCid( $intGroupId, $intCid, $objDatabase, $boolIncludeAdminUsers = false ) {
		$strSql = 'SELECT
						count(ce.id) as count
					FROM
						company_employees ce
						JOIN company_users cu ON ( ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )';

		if( false == $boolIncludeAdminUsers ) {
			$strSql .= ' AND cu.is_administrator = 0  ';
		}

		$strSql .= ' ) JOIN company_user_groups cug ON ( cug.company_user_id = cu.id AND cug.cid = cu.cid )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND cu.is_disabled = 0
						AND cug.company_group_id = ' . ( int ) $intGroupId;

		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		return( true == valArr( $arrstrData ) ) ? $arrstrData[0]['count'] : 0;
	}

	public static function fetchUnAssociatedCompanyEmployeesByGroupIdByCid( $intGroupId, $intCid, $objDatabase, $boolIncludeAdminUsers ) {
		$strSql = '	SELECT
						  DISTINCT ce.id,
						  ce.name_first,
						  ce.name_last,
						  cu.id as company_user_id
					  FROM
						  company_employees ce
						  JOIN company_users cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.is_disabled = 0 AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
					  WHERE
						  cu.cid = ' . ( int ) $intCid . '
						  AND cu.default_company_user_id IS NULL
						  AND ce.id NOT IN (
											  SELECT
											  		DISTINCT ce1.id
										  		FROM
													company_employees ce1
													JOIN company_users cu1 ON ( cu1.company_employee_id = ce1.id AND cu1.cid = ce1.cid AND cu1.company_user_type_id = cu.company_user_type_id ) AND cu1.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
													JOIN company_user_groups cug1 ON ( cug1.company_user_id = cu1.id AND cug1.cid = cu1.cid )
												WHERE
											  		cu1.cid = ' . ( int ) $intCid . '
											  		AND cug1.company_group_id = ' . ( int ) $intGroupId . '
						 					) ';

		if( false == $boolIncludeAdminUsers ) {
			$strSql .= ' AND cu.is_administrator = 0 ';
		}

		$strSql .= '  ORDER BY ce.name_first,  ce.name_last';

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchSimpleCompanyEmployeesByIdsByCid( $arrintCompanyEmployeeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyEmployeeIds ) )
			return NULL;

		$strSql = 'SELECT
						DISTINCT ON( ce.email_address ) ce.*
					FROM
						company_employees ce
					WHERE
						ce.id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' )
						AND ce.cid = ' . ( int ) $intCid . '
						AND ce.email_address IS NOT NULL';

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesByCompanyDepartmentNameByPropertyIdsByStatusTypeIdsByCid( $arrstrCompanyDepartmentName, $arrintPropertyIds, $arrintEmployeeStatusTypeIds, $intCid, $boolHideAdmin, $objDatabase, $boolReturnArray = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCheckIncludeAdmin = ( false == $boolHideAdmin ) ? ' OR cu.is_administrator = 1 ':'';
		// Did Temp fix need to work on this SQL
			 $strSql = ' 
			 			DROP TABLE IF EXISTS pg_temp.temp_load_properties;
		
						CREATE TEMP TABLE temp_load_properties AS (
							SELECT
								lp.property_id
							FROM
							 load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[ ' . implode( ',', $arrintPropertyIds ) . ' ], ARRAY[]::INT[], true ) lp
							);
						ANALYZE temp_load_properties;
					
						DROP TABLE IF EXISTS pg_temp.temp_company_employees;
			 			CREATE TEMP TABLE temp_company_employees AS (
			 				SELECT
			 					cd.cid,
			 					ced.company_employee_id
			 				FROM
			 					company_departments cd
			 				JOIN company_employee_departments ced ON cd.cid = ced.cid AND cd.id = ced.company_department_id
			 				WHERE
			 					ced.cid = ' . ( int ) $intCid . '
					            AND (  LOWER( cd.name ) IN ( \'' . implode( '\',\'', $arrstrCompanyDepartmentName ) . '\' ) ' . ' )
			 			);
			 			
			 			ANALYZE temp_company_employees;
			 			
			 			SELECT
				    		DISTINCT (cu.id) AS company_user_id,
							ce.id ,
			 				ce.name_first,
			 				ce.name_last,
			 				ce.name_first||\' \'||ce.name_last as namefull,
			 				lp.property_id
						FROM
							company_employees ce
							JOIN company_users cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
							LEFT JOIN temp_company_employees tce ON ce.cid = tce.cid AND ce.id = tce.company_employee_id
							LEFT JOIN company_user_property_groups cupg ON cupg.cid = cu.cid AND cupg.company_user_id = cu.id 
							LEFT JOIN property_groups pg ON pg.cid = cupg.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL 
							LEFT JOIN property_group_associations pga ON cupg.cid = pga.cid AND pg.id = pga.property_group_id
							LEFT JOIN temp_load_properties lp ON lp.property_id = pga.property_id							
						WHERE
							ce.cid =  ' . ( int ) $intCid . '
							AND ( ( cu.default_company_user_id IS NULL ) ';

			$strSql .= ( false == $boolHideAdmin ) ? $strCheckIncludeAdmin : '';

			$strSql .= ' )
							AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' 
							AND cu.is_disabled = 0
							AND cu.deleted_on IS NULL
							AND ( tce.company_employee_id IS NOT NULL ';

			$strSql .= ( false == $boolHideAdmin ) ? $strCheckIncludeAdmin : '';
			$strSql .= ' )';

			if( true == valArr( $arrintEmployeeStatusTypeIds ) ) {
				$strSql .= ' AND ce.employee_status_type_id IN (\'' . implode( ',', $arrintEmployeeStatusTypeIds ) . '\')';
			}

			$strSql .= 'ORDER BY
					 	ce.name_first,
						ce.name_last';

		if( false != $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchUnassociatedCompanyEmployeesWithPropertyNameWithIntegrationDatabaseNameByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ce.id,
						ce.name_last,
						ce.name_first,
						array_to_string( array_agg( DISTINCT( p.property_name ) ), \', \' ) as property_name,
						id.name as integration_database_name
					FROM
						 company_employees ce
						 LEFT JOIN property_leasing_agents plg ON ( plg.company_employee_id = ce.id AND plg.cid = ce.cid )
						 LEFT JOIN property_integration_databases pid ON ( pid.property_id = plg.property_id AND pid.cid = plg.cid )
						 LEFT JOIN integration_databases id ON ( pid.integration_database_id = id.id AND pid.cid = id.cid )
						 LEFT JOIN properties p ON ( p.id = plg.property_id AND p.cid = plg.cid AND p.is_disabled = 0 )
					WHERE
						 ce.cid = ' . ( int ) $intCid . '
					AND
						ce.id NOT IN ( SELECT cu.company_employee_id FROM company_users cu WHERE cu.cid = ' . ( int ) $intCid . ' AND cu.default_company_user_id IS NULL AND ( cu.company_employee_id IS NOT NULL ) )
					GROUP BY ce.id, ce.name_last, ce.name_first, integration_database_name
					ORDER BY ce.name_last, ce.name_first ASC';

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveAndInActivePropertyLeasingAgentsAssociatedToActiveApplicationsByPropertyIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase, $intCompanyDepartmentId = NULL, $boolAddCheckForDisabledColumn = false ) {

		$arrintPropertyGroupIds = array_filter( ( array ) $arrintPropertyGroupIds, 'is_numeric' );

		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSelectDisabledColumnName = '';
		$arrintInActiveApplicationStatusIds = [
			CApplicationStatus::ON_HOLD,
			CApplicationStatus::CANCELLED
		];

		if( true == $boolAddCheckForDisabledColumn ) {
			$strSelectDisabledColumnName   = ', cu.is_disabled ';
		}

		$strLoadPropertiesSql = '
			CREATE TEMP TABLE temp_properties AS(
			    SELECT
					lp.property_id
				FROM
			        load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ] ) lp
			        JOIN properties p ON p.cid = lp.cid AND p.id = lp.property_id
					AND NOT( p.occupancy_type_ids IS DISTINCT FROM NULL AND ' . ( int ) COccupancyType::COMMERCIAL . ' = ANY ( p.occupancy_type_ids ) AND array_length( p.occupancy_type_ids, 1 ) = 1 )
			    WHERE
			        lp.is_disabled = 0
			        AND lp.is_test = 0 
		    );
			ANALYZE temp_properties;';

		$strSql = $strLoadPropertiesSql . '
			SELECT
				*
			FROM (
				SELECT
	                true AS is_pls_exists,
					ce.*
					' . $strSelectDisabledColumnName . '
				FROM
					company_employees AS ce
					JOIN property_leasing_agents pla ON pla.cid = ce.cid AND pla.company_employee_id = ce.id
					LEFT JOIN company_users AS cu ON cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
				WHERE
					ce.cid = ' . ( int ) $intCid . '
					AND pla.property_id IN( SELECT property_id FROM temp_properties )
					AND (
							( pla.is_disabled = 0 AND COALESCE( cu.is_disabled, 0 ) = 0 )
							OR
		                    EXISTS (
		                        SELECT
                                    1
                                FROM
                                    cached_applications ca
                                WHERE
                                    pla.cid = ca.cid
									AND pla.property_id = ca.property_id
									AND pla.company_employee_id = ca.leasing_agent_id
									AND ca.application_status_id NOT IN ( ' . implode( ',', $arrintInActiveApplicationStatusIds ) . ' )
                                    AND ca.lease_interval_type_id != ' . CLeaseIntervalType::RENEWAL . '
                            )
					)

				UNION

				SELECT
						true AS is_pls_exists,
						ce.*
						' . $strSelectDisabledColumnName . '
					FROM
						company_employees ce
						JOIN company_employee_departments ced ON ( ced.cid = ce.cid AND ced.company_employee_id = ce.id )
						JOIN company_departments cd ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
						JOIN company_users cu ON ( ce.id = cu.company_employee_id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
                        LEFT JOIN property_leasing_agents pla on ( pla.company_employee_id = ce.id AND pla.cid = ce.cid and pla.property_id IN( SELECT property_id FROM temp_properties ) )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND cd.default_company_department_id = ' . CCompanyDepartment::DEFAULT_COMPANY_DEPARTMENT_LEASING_ID . '
					    AND cu.is_administrator = 1
						AND pla.id IS NULL ';

		if( false == is_null( $intCompanyDepartmentId ) ) {
			$strSql .= self::buildSqlToFetchCompanyEmployeesByDepertmentIdByPropertyIdsByCid( $intCompanyDepartmentId, $arrintPropertyGroupIds, $intCid, $boolIsActive = false, $boolIsLeasingAdmin = false, $boolIsFromLeadApplicantTab = true, $boolAddCheckForDisabledColumn );
		}

		$strSql .= ' ) AS res
			ORDER BY res.name_first ASC, res.name_last';

		return self::fetchCachedCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchCustomCompanyEmployeesByCid( $intCid, $objDatabase, $intClusterId, $boolBothRelease ) {

		$strWhereCondition = ' AND cupre.value SIMILAR TO \'(' . ( ( true == $boolBothRelease ) ? ( CCompanyUserPreference::STANDARD_ONLY_WEEKLY_EMAIL_REMAINDER . '|' . CCompanyUserPreference::RAPID_ONLY_WEEKLY_EMAIL_REMAINDER ) : ( ( $intClusterId == CCluster::RAPID ) ? CCompanyUserPreference::RAPID_ONLY_WEEKLY_EMAIL_REMAINDER : CCompanyUserPreference::STANDARD_ONLY_WEEKLY_EMAIL_REMAINDER ) ) . '|' . CCompanyUserPreference::BOTH_WEEKLY_EMAIL_REMINDER . ')\' ';

		$strSql = ' SELECT
 						final.*,
 						cu1.id as company_user_id,
 						cupre1.value
 					FROM
 					(
						SELECT
							ce.name_first,
							ce.email_address,
							ce.id
						FROM
							company_employees AS ce
						WHERE
							ce.email_address IS NOT NULL
							AND ce.id IN (
										SELECT
											cu.company_employee_id
										FROM
											company_users AS cu LEFT JOIN
											company_user_preferences AS cupre ON ( cu.id = cupre.company_user_id AND cu.cid = cupre.cid  ) LEFT JOIN
											view_company_user_properties AS cup ON ( cu.cid = cup.cid )
										WHERE
											( cu.id = cup.company_user_id
											OR cu.is_administrator = 1 )
											AND cupre.key = \'WEEKLY_EMAIL_REMINDER_OF_PRODUCT_UPDATES\'' . $strWhereCondition .
											'AND cu.cid = ' . ( int ) $intCid . '
											AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
											AND cu.default_company_user_id IS NULL
											AND cu.is_disabled = 0
										) AND ce.cid = ' . ( int ) $intCid . '
					) AS final
					JOIN company_users AS cu1 ON final.id = cu1.company_employee_id
					JOIN company_user_preferences AS cupre1 ON ( cu1.id = cupre1.company_user_id AND cu1.cid = cupre1.cid )
					WHERE
						cupre1.key = \'WEEKLY_EMAIL_REMINDER_OF_PRODUCT_UPDATES\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveCompanyEmployeesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $intCompanyDepartmentId = NULL, $arrintIncludeDefaultCompanyDepartmentIds = [], $boolIsAdmin = false ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return [];
		}

		$strIsAdminSql = ( true == $boolIsAdmin ) ? ' OR cu.is_administrator = 1 ' : '';

		$arrintInActiveApplicationStageStatusIds = [
			CApplicationStage::APPLICATION => [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ]
		];

		$strSql = ( true == is_null( $intCompanyDepartmentId ) ) ? '' : 'SELECT * FROM ( (';

		$strSql .= ' SELECT
						ce.name_first,
						ce.name_last,
						func_format_customer_name( ce.name_first, ce.name_last ) as name_full,
						ce.id,
						cu.id AS company_user_id
					FROM
						company_employees ce
						LEFT JOIN company_users cu ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						LEFT JOIN cached_applications ca ON ( ce.id = ca.leasing_agent_id AND ce.cid = ca.cid )';

		$strSql .= ' WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND
							( CASE
								WHEN cu.is_disabled = 1 THEN
									( ca.application_stage_id, ca.application_status_id ) NOT IN ( ' . sqlIntMultiImplode( $arrintInActiveApplicationStageStatusIds ) . ' ) AND ca.lease_interval_type_id  != ' . CLeaseIntervalType::RENEWAL . '
								ELSE
									( cu.id IS NULL OR cu.is_disabled = 0 )
							END )
						AND EXISTS (
			                         SELECT
			                             1
			                         FROM
			                             company_departments cd
			                             JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
			                         WHERE
			                             ced.cid = ' . ( int ) $intCid . '
			                             AND ced.company_employee_id = ce.id
			                             AND cd.default_company_department_id = ' . ( int ) CCompanyDepartment::DEFAULT_COMPANY_DEPARTMENT_LEASING_ID . '
			            )
						AND
							ce.id IN ( SELECT company_employee_id  FROM property_leasing_agents WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND property_leasing_agents.cid = ' . ( int ) $intCid . ' AND is_disabled = 0 )';

		$strSql .= ( true == is_null( $intCompanyDepartmentId ) ) ? 'GROUP BY ce.name_first,ce.name_last,ce.id,company_user_id ORDER BY lower( ce.name_last)' : ' ) ';

		if( true == valArr( $arrintIncludeDefaultCompanyDepartmentIds ) ) {
			$strSql .= ' UNION
						(
							SELECT
								ce.name_first,
								ce.name_last,
								func_format_customer_name ( ce.name_first, ce.name_last ) AS name_full,
								ce.id,
								cu.id AS company_user_id
							FROM
								company_employees ce
								JOIN company_users cu ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id AND cu.is_administrator = 1 AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' ) 
							WHERE
								ce.cid = ' . ( int ) $intCid . '
								AND cu.default_company_user_id IS NULL
								AND cu.username NOT LIKE \'%PS Admin%\'
								AND cu.is_disabled = 0
								AND ce.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
								AND EXISTS (
									SELECT
										1
									FROM
										company_departments cd
										JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND ( cd.id = ced.company_department_id ' . $strIsAdminSql . ' ) )
									WHERE
										ced.cid = ' . ( int ) $intCid . '
										AND ced.company_employee_id = ce.id
										AND cd.default_company_department_id IN ( ' . implode( ',', $arrintIncludeDefaultCompanyDepartmentIds ) . ' )
								)
							)
						UNION
						(	
							SELECT
								ce.name_first,
								ce.name_last,
								func_format_customer_name ( ce.name_first, ce.name_last ) AS name_full,
								ce.id,
								cu.id AS company_user_id
							FROM
								company_employees ce
								JOIN company_users cu ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
								JOIN company_user_property_groups cupg ON ( cupg.cid = cu.cid AND cupg.company_user_id = cu.id )
								JOIN property_group_associations pga ON ( cupg.cid = pga.cid AND cupg.property_group_id = pga.property_group_id )
								JOIN property_preferences pp ON ( pga.cid = pp.cid AND pga.property_id = pp.property_id AND pp.key =\'WORK_ORDER_ALLOW_MULTIPLE_USER_DEPARTMENT\' )
								JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[ ' . implode( ',', $arrintPropertyIds ) . ' ] ) lp ON ( lp.is_disabled = 0 AND lp.property_id = pga.property_id )
								JOIN company_employee_departments ced ON ( ce.cid = ced.cid AND  ced.company_employee_id = ce.id)
								JOIN company_departments cd ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id ) 
							WHERE
								ce.cid =  ' . ( int ) $intCid . '
								AND cu.default_company_user_id IS NULL
								AND cu.username NOT LIKE \'%PS Admin%\'
								AND cu.is_disabled = 0
								AND ce.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
								AND cd.default_company_department_id:: TEXT = ANY ( string_to_array(pp.value, \',\') )
								AND cd.default_company_department_id IN ( ' . implode( ',', $arrintIncludeDefaultCompanyDepartmentIds ) . ' )
						)';
		}

		if( false == is_null( $intCompanyDepartmentId ) ) {
			$strSql .= ' UNION ';

			$strSql .= '( SELECT
							  ce.name_first,
							  ce.name_last,
							  func_format_customer_name( ce.name_first, ce.name_last ) as name_full,
							  ce.id,
							  cu.id AS company_user_id
						  FROM
							  company_employees ce
							  JOIN company_users cu ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
							  JOIN property_preferences pp ON ( pp.cid = ce.cid )
							  JOIN view_company_user_properties cup ON ( cup.company_user_id = cu.id AND cup.property_id = pp.property_id AND ce.cid = cup.cid )
						  WHERE
							  pp.key = \'ALLOW_MANAGERS_TO_BE_ASSIGNED_TO_LEADS\'
							  AND value = \'1\'
							  AND ce.cid = ' . ( int ) $intCid . '
							  AND cu.is_disabled = 0
							  AND EXISTS (
					                       SELECT
					                           1
					                       FROM
					                           company_departments cd
					                           JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
					                       WHERE
					                           ced.cid = ' . ( int ) $intCid . '
					                           AND ced.company_employee_id = ce.id
					                           AND ced.company_department_id = ' . ( int ) $intCompanyDepartmentId . '
					          )
							  AND pp.property_id IN (' . implode( ',', $arrintPropertyIds ) . ' ) ) ) as res
						  GROUP BY
							  res.name_first,
							  res.name_last,
							  res.name_full,
							  res.id,
							  res.company_user_id
						  ORDER BY lower( res.name_last)';
		}

		$arrstrCompanyEmployees = ( array ) fetchData( $strSql, $objDatabase );
		$arrstrReorderedCompanyEmployees = [];

		if( true == valArr( $arrstrCompanyEmployees ) ) {
			foreach( $arrstrCompanyEmployees as $arrstrCompanyEmployee ) {
				$arrstrReorderedCompanyEmployees[$arrstrCompanyEmployee['id']] = $arrstrCompanyEmployee;
			}
		}
		return $arrstrReorderedCompanyEmployees;
	}

	public static function fetchActiveCompanyEmployeesByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase, $intCompanyDepartmentId = NULL, $arrintIncludeDefaultCompanyDepartmentIds = [] ) {

		$arrintPropertyGroupIds = array_filter( ( array ) $arrintPropertyGroupIds, 'is_numeric' );

		if( false == valArr( $arrintPropertyGroupIds ) ) {
			return [];
		}

		$arrintInActiveApplicationStageStatusIds = [
			CApplicationStage::APPLICATION => [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ]
		];

		$strSql = ( true == is_null( $intCompanyDepartmentId ) ) ? '' : 'SELECT * FROM ( (';

		$strSql .= ' SELECT
						ce.name_first,
						ce.name_last,
						func_format_customer_name( ce.name_first, ce.name_last ) as name_full,
						ce.id,
						cu.id AS company_user_id
					FROM
						company_employees ce
						LEFT JOIN company_users cu ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						LEFT JOIN cached_applications ca ON ( ce.id = ca.leasing_agent_id AND ce.cid = ca.cid )';

		$strSql .= ' WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND
							( CASE
								WHEN cu.is_disabled = 1 THEN
									  ( ca.application_stage_id, ca.application_status_id ) NOT IN ( ' . sqlIntMultiImplode( $arrintInActiveApplicationStageStatusIds ) . ' ) AND ca.lease_interval_type_id  != ' . CLeaseIntervalType::RENEWAL . '
								ELSE
									( cu.id IS NULL OR cu.is_disabled = 0 )
							END )
						AND EXISTS (
			                         SELECT
			                             1
			                         FROM
			                             company_departments cd
			                             JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
			                         WHERE
			                             ced.cid = ' . ( int ) $intCid . '
			                             AND ced.company_employee_id = ce.id
			                             AND cd.default_company_department_id = ' . ( int ) CCompanyDepartment::DEFAULT_COMPANY_DEPARTMENT_LEASING_ID . '
			            )
						AND
							ce.id IN ( 
								SELECT 
									company_employee_id
								FROM 
									property_leasing_agents pla 
									JOIN load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], array [ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ] ) lp ON lp.property_id = pla.property_id
								WHERE 
									pla.cid = ' . ( int ) $intCid . ' 
									AND pla.is_disabled = 0 
							)';

		$strSql .= ( true == is_null( $intCompanyDepartmentId ) ) ? 'GROUP BY ce.name_first,ce.name_last,ce.id,company_user_id ORDER BY lower( ce.name_last)' : ' ) ';

		if( true == valArr( $arrintIncludeDefaultCompanyDepartmentIds ) ) {
			$strSql .= ' UNION
						(
							SELECT
								ce.name_first,
								ce.name_last,
								func_format_customer_name ( ce.name_first, ce.name_last ) AS name_full,
								ce.id,
								cu.id AS company_user_id
							FROM
								company_employees ce
								JOIN company_employee_departments ced ON ( ced.company_employee_id = ce.id AND ced.cid = ce.cid )
								JOIN company_departments cd ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id  )
								JOIN company_users cu ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid AND cu.company_user_type_id = 2 )
								JOIN view_company_user_properties cup ON ( cup.company_user_id = cu.id AND ce.cid = cup.cid )
								JOIN load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], array [ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ] ) lp ON lp.property_id = cup.property_id
							WHERE
								ce.cid = ' . ( int ) $intCid . '
								AND cu.is_disabled = 0
								AND cd.default_company_department_id IN ( ' . implode( ',', $arrintIncludeDefaultCompanyDepartmentIds ) . ' )
						)';
		}

		if( false == is_null( $intCompanyDepartmentId ) ) {
			$strSql .= ' UNION ';

			$strSql .= '( SELECT
							  ce.name_first,
							  ce.name_last,
							  func_format_customer_name( ce.name_first, ce.name_last ) as name_full,
							  ce.id,
							  cu.id AS company_user_id
						  FROM
							  company_employees ce
							  JOIN company_employee_departments ced ON ced.cid = ce.cid AND ced.company_employee_id = ce.id AND ced.company_department_id = ' . ( int ) $intCompanyDepartmentId . '
							  JOIN company_users cu ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
							  JOIN property_preferences pp ON ( pp.cid = ce.cid AND pp.key = \'ALLOW_MANAGERS_TO_BE_ASSIGNED_TO_LEADS\' AND value = \'1\' )
							  JOIN view_company_user_properties cup ON ( cup.company_user_id = cu.id AND cup.property_id = pp.property_id AND ce.cid = cup.cid )
							  JOIN load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], array [ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ] ) lp ON lp.property_id = pp.property_id
						  WHERE
							  ce.cid = ' . ( int ) $intCid . '
							  AND cu.is_disabled = 0
						  ) ) as res
						  GROUP BY
							  res.name_first,
							  res.name_last,
							  res.name_full,
							  res.id,
							  res.company_user_id
						  ORDER BY lower( res.name_last)';
		}

		$arrstrCompanyEmployees = ( array ) fetchData( $strSql, $objDatabase );
		$arrstrReorderedCompanyEmployees = [];

		if( true == valArr( $arrstrCompanyEmployees ) ) {
			foreach( $arrstrCompanyEmployees as $arrstrCompanyEmployee ) {
				$arrstrReorderedCompanyEmployees[$arrstrCompanyEmployee['id']] = $arrstrCompanyEmployee;
			}
		}
		return $arrstrReorderedCompanyEmployees;
	}

	public static function buildSqlToFetchCompanyEmployeesByDepertmentIdByPropertyIdsByCid( $intCompanyDepartmentId, $arrintPropertyGroupIds, $intCid, $boolIsActive = false, $boolIsLeasingAdmin = false, $boolIsFromLeadApplicantTab = false, $boolAddCheckForDisabledColumn = false ) {
		$strSql 		= '';
		$strSelectSql   = '';
		$strSelectDisabledColumnName = '';
		$arrintPropertyGroupIds = array_filter( ( array ) $arrintPropertyGroupIds, 'is_numeric' );

		if( true == $boolIsFromLeadApplicantTab ) {
			$strSelectSql   = ' TRUE AS is_pls_exists, ';
		}

		if( true == $boolAddCheckForDisabledColumn ) {
			$strSelectDisabledColumnName   = ', cu.is_disabled ';
		}

		if( false == is_null( $intCompanyDepartmentId ) && false == is_null( $intCid ) && true == valArr( $arrintPropertyGroupIds ) ) {

			$strSql .= '
				UNION

	            SELECT
	                ' . $strSelectSql . '
    				ce.*
    				' . $strSelectDisabledColumnName . '
    				
				FROM
					company_employees ce
					JOIN company_users AS cu ON cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
				WHERE
					cu.cid = ' . ( int ) $intCid . '
					AND EXISTS (
						SELECT
							1
						FROM
							property_preferences pp
							JOIN property_group_associations pga ON pga.cid = pp.cid AND pga.property_id = pp.property_id
							JOIN property_groups pg ON pg.cid = pga.cid AND pg.id = pga.property_group_id
							JOIN company_user_property_groups cupg ON cupg.cid = pg.cid AND cupg.property_group_id = pg.id 
						WHERE
							pp.cid = ' . ( int ) $intCid . '
							AND cupg.company_user_id = cu.id
							AND pp.key = \'ALLOW_MANAGERS_TO_BE_ASSIGNED_TO_LEADS\'
							AND pp.value = \'1\'
							AND pp.property_id IN( SELECT property_id FROM load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ] ) lp WHERE lp.is_disabled = 0 AND lp.is_test = 0 ) 
					)
					AND EXISTS (
						SELECT
		                     1
		                 FROM
		                     company_employee_departments ced
		                 WHERE
		                     ced.cid = ' . ( int ) $intCid . '
		                     AND ced.company_employee_id = ce.id
		                     AND ced.company_department_id = ' . ( int ) $intCompanyDepartmentId . '
				    )';

			$strSql .= ( true == $boolIsActive ) ? ' AND COALESCE( cu.is_disabled, 0 ) = 0 ' : '';

			if( true == $boolIsLeasingAdmin ) {

				$strSql .= '
					UNION

					SELECT
						 ce.*
						 ' . $strSelectDisabledColumnName . '
					FROM
						company_employees ce
						JOIN company_users cu ON ce.id = cu.company_employee_id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND EXISTS (
							SELECT
	                           1
	                        FROM
	                           company_departments cd
	                           JOIN company_employee_departments ced ON ced.cid = cd.cid AND ced.company_department_id = cd.id
	                        WHERE
	                           ced.cid = ' . ( int ) $intCid . '
	                           AND ced.company_employee_id = ce.id
	                           AND cd.default_company_department_id = ' . ( int ) CCompanyDepartment::DEFAULT_COMPANY_DEPARTMENT_LEASING_ID . ' 
			            )
						' . ( ( true == $boolIsActive ) ? ' AND COALESCE( cu.is_disabled, 0 ) = 0 ' : '' ) . '
						AND cu.is_administrator = 1';
			}
		}

		return $strSql;
	}

	public static function fetchPaginatedGroupAssociatedCompanyEmployees( $intPageNo, $intPageSize, $arrintCompanyGroupIds,  $intCid, $objDatabase ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;
		$strWhereSql = '';

		if( true == valArr( $arrintCompanyGroupIds ) ) {
			$strWhereSql = ' AND company_group_id IN ( ' . implode( ',', $arrintCompanyGroupIds ) . ' )';
		}

		$strSql = 'SELECT
					    cu.id AS company_user_id,
					    ce.id,
					    ce.name_first,
					    ce.name_last,
					    ce.is_employee,
					    cu.username
					FROM
					    company_users cu
					    JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
					WHERE
					   cu.cid = ' . ( int ) $intCid . '
					    AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
					    AND cu.is_administrator = 0
					    AND cu.is_disabled = 0
					    AND EXISTS (
					                 SELECT
					                     1
					                 FROM
					                     company_user_permissions cup
					                 WHERE
					                     cup.cid = cu.cid
					                     AND cup.company_user_id = cu.id
					                     AND ( cup.is_allowed = 1
					                     OR cup.is_inherited = 0 )
					    )
					    AND EXISTS (
					                 SELECT
					                     1
					                 FROM
					                     company_user_groups cug
					                 WHERE
					                     cug.cid = cu.cid
					                     AND cug.company_user_id = cu.id  ' . $strWhereSql . '
					    )
					ORDER BY
					    ce.name_first ASC,
					    ce.name_last ASC
						OFFSET ' . ( int ) $intOffset . '
						LIMIT ' . ( int ) $intLimit;

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchGroupAssociatedCompanyEmployeesCount( $intCid, $arrintCompanyGroupIds, $objDatabase ) {
		$strWhereSql = '';

		if( true == valArr( $arrintCompanyGroupIds ) ) {
			$strWhereSql = ' AND company_group_id IN ( ' . implode( ',', $arrintCompanyGroupIds ) . ' )';
		}

		$strSql = 'SELECT
					    COUNT( DISTINCT cu.id )
					FROM
					    company_users cu
					    JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
					WHERE
					   cu.cid = ' . ( int ) $intCid . '
					    AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
					    AND cu.is_administrator = 0
					    AND cu.is_disabled = 0
					    AND EXISTS (
					                 SELECT
					                     1
					                 FROM
					                     company_user_permissions cup
					                 WHERE
					                     cup.cid = cu.cid
					                     AND cup.company_user_id = cu.id
					                     AND ( cup.is_allowed = 1
					                     OR cup.is_inherited = 0 )
					    )
					    AND EXISTS (
					                 SELECT
					                     1
					                 FROM
					                     company_user_groups cug
					                 WHERE
					                     cug.cid = cu.cid
					                     AND cug.company_user_id = cu.id ' . $strWhereSql . '
					    )';
		$arrintCompanyEmployee = ( array ) fetchData( $strSql, $objDatabase );

		return $arrintCompanyEmployee[0]['count'];
	}

	public static function fetchUnassociatedCompanyEmployeeIdByFirstNameByLastnameByCid( $strFirstName, $strLastName, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ce.id
					FROM
						company_employees ce
						LEFT JOIN company_users cu ON ( cu.cid = ' . ( int ) $intCid . ' AND ce.id = cu.company_employee_id )
					WHERE
						cu.company_employee_id IS NULL
						AND ce.cid = ' . ( int ) $intCid . '
						AND lower( ce.name_first ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $strFirstName ) ) ) . '\'
						AND lower( ce.name_last ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $strLastName ) ) ) . '\'
						LIMIT 1';

		$arrintCompanyEmployee = ( array ) fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintCompanyEmployee ) && isset( $arrintCompanyEmployee[0]['id'] ) ) ? $arrintCompanyEmployee[0]['id'] : 0;
	}

	public static function fetchCompanyEmployeesDetailsByIdsByCid( $arrintCompanyEmployeeIds, $arrstrFields, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyEmployeeIds ) )
			return NULL;

		$strSql = 'SELECT
					' . implode( ', ', $arrstrFields ) . '
					FROM
						company_employees
					WHERE
						id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	/** Query for new dashbaord**/

	public static function fetchLeasingAgentsByPropertyIdsByCid( $arrintPropertyIds, $intCompanyDepartmentId, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) )
			return NULL;

		$strSql = 'SELECT * FROM (
						SELECT
							DISTINCT( ce.id ),
							ce.name_first,
							ce.name_last,
							pla.property_id
						FROM
							property_leasing_agents AS pla
							JOIN company_employees AS ce ON ( pla.company_employee_id = ce.id AND pla.cid = ce.cid )
							LEFT JOIN company_users AS cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						WHERE
							COALESCE( pla.is_disabled, 0 ) = 0
							AND COALESCE ( cu.is_disabled, 0 ) = 0
							AND pla.cid = ' . ( int ) $intCid . '
							AND pla.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							AND EXISTS (
			                         SELECT
			                             1
			                         FROM
			                             company_departments cd
			                             JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
			                         WHERE
			                             ced.cid = ' . ( int ) $intCid . '
			                             AND ced.company_employee_id = ce.id
			                             AND cd.default_company_department_id = ' . ( int ) CCompanyDepartment::DEFAULT_COMPANY_DEPARTMENT_LEASING_ID . '
			                )
						UNION
							SELECT
								DISTINCT( ce.id ),
								ce.name_first,
								ce.name_last,
                                NULL::INTEGER AS property_id
							FROM
								company_employees ce
								JOIN company_users cu ON ( ce.id = cu.company_employee_id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
								LEFT JOIN property_leasing_agents pla ON ( pla.company_employee_id = ce.id AND pla.cid = ce.cid AND ( pla.is_disabled IS NULL OR pla.is_disabled = 0 ) AND pla.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
							WHERE
								ce.cid = ' . ( int ) $intCid . '
								AND EXISTS (
												SELECT
						                           1
						                        FROM
						                           company_departments cd
						                           JOIN company_employee_departments ced ON ( ced.cid = cd.cid AND ced.company_department_id = cd.id )
						                        WHERE
						                           ced.cid = ' . ( int ) $intCid . '
						                           AND ced.company_employee_id = ce.id
						                           AND cd.default_company_department_id = ' . ( int ) CCompanyDepartment::DEFAULT_COMPANY_DEPARTMENT_LEASING_ID . ' 
						        )
								AND COALESCE( cu.is_disabled, 0 ) = 0
								AND cu.is_administrator = 1
								AND pla.id IS NULL 
						 UNION
							SELECT
								DISTINCT( ce.id ),
								ce.name_first,
								ce.name_last,
								pp.property_id
							FROM
								company_employees ce
								JOIN company_users AS cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid ) AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
								JOIN property_preferences pp ON ( pp.cid = ce.cid )
								JOIN view_company_user_properties cup ON ( cup.company_user_id = cu.id AND cup.property_id = pp.property_id AND ce.cid = cup.cid )
							WHERE
								pp.key = \'ALLOW_MANAGERS_TO_BE_ASSIGNED_TO_LEADS\'
								AND value = \'1\'
								AND ce.cid = ' . ( int ) $intCid . '
								AND EXISTS (
											SELECT
								                     1
								                 FROM
								                     company_departments cd
								                     JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
								                 WHERE
								                     ced.cid = ' . ( int ) $intCid . '
								                     AND ced.company_employee_id = ce.id
								                     AND ced.company_department_id = ' . ( int ) $intCompanyDepartmentId . '
						        )
								AND pp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								AND COALESCE ( cu.is_disabled, 0 ) = 0
						) AS res
					ORDER BY res.name_first ASC, res.name_last';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyEmployeeDetailsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cu.id,
						CASE
							WHEN ce.name_first is NULL
								THEN cu.username
								ELSE CONCAT ( ce.name_first, \' \', ce.name_last )
							END AS username,
						ce.name_prefix,
						ce.name_first,
						ce.name_last,
						ce.title,
						ce.phone_number1,
						ce.phone_number1_type_id,
						ce.phone_number2,
						ce.phone_number2_type_id,
						ce.email_address,
						ce.secondary_email_address,
						ce.phone_number1_extension,
						ce.phone_number2_extension
					FROM
						company_employees ce
						RIGHT JOIN company_users cu ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.id = ' . ( int ) $intCompanyUserId . ' LIMIT 1 ';

		return self::fetchCompanyEmployee( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesUnassociatedPropertyLeasingAgentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
					    ce.id,
					    ce.name_last,
					    ce.name_first
					FROM
					    company_employees ce
					    JOIN company_users cu ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id AND cu.is_administrator = 0 AND cu.is_disabled = 0 AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
					WHERE
					    ce.cid = ' . ( int ) $intCid . '
					    AND EXISTS (
									SELECT
						                     1
						                 FROM
						                     company_departments cd
						                     JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
						                 WHERE
						                     ced.cid = ' . ( int ) $intCid . '
						                     AND ced.company_employee_id = ce.id
						                     AND cd.default_company_department_id = ' . CCompanyDepartment::DEFAULT_COMPANY_DEPARTMENT_LEASING_ID . '
						)
					    AND NOT EXISTS (
					                     SELECT
					                         1
					                     FROM
					                         property_leasing_agents pla
					                     WHERE
					                         pla.cid = ce.cid
					                         AND pla.company_employee_id = ce.id
					                         AND pla.property_id = ' . ( int ) $intPropertyId . '
					                         AND pla.is_disabled = 0
					    )
					ORDER BY
					    ce.name_first,
					    ce.name_last';

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchCustomBlockedCompanyEmployeeEmailAddresses( $objClientDatabase ) {

		$strSql = 'SELECT
						DISTINCT( lower( ce.email_address ) ) AS email_address
					FROM
						company_employees ce
						JOIN company_preferences cp ON( ce.cid = cp.cid )
						JOIN clients c ON( c.id = cp.cid )
					WHERE cp.key = \'BLOCK_NEWSLETTERS\'
						AND ce.email_address IS NOT NULL
						AND c.company_status_type_id NOT IN( ' . implode( ',', CCompanyStatusType::$c_arrintTestCompanyStatusTypeIds ) . ' )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyEmployeesDepartmentNameByIdsByPropertyIdByCid( $arrintCompanyEmployeeIds, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyEmployeeIds ) || false == valArr( array_filter( $arrintCompanyEmployeeIds ) ) ) return NULL;

		$objAdminUserPropertyPreference = CPropertyPreferences :: fetchPropertyPreferencesByKeyByPropertyIdByCid( 'INCLUDE_ADMIN_USER_IN_ASSIGN_TO_WORK_ORDER', $intPropertyId, $intCid, $objDatabase );
		$strAdminUser = ( true == valObj( $objAdminUserPropertyPreference, 'CPropertyPreference' ) && true == $objAdminUserPropertyPreference->getValue() ) ? '1' : '0';
		$strSql = 'SELECT
						cd.company_department_ids,
					    CASE
           					WHEN cu.is_administrator = 1 AND 1 = ' . $strAdminUser . ' THEN \'System Administrator\'
           					ELSE cd.company_departments
           				END as company_departments,
						ce.id,
						cu.id as company_user_id
					FROM
						company_employees ce
           				JOIN company_users cu ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
           				LEFT JOIN LATERAL 
						    (
						      SELECT
						          string_agg ( util_get_translated( \'name\', cd.name, cd.details ), \',\' ) AS company_departments,
						          ced.company_employee_id,
						          cd.cid,
						          string_agg ( ced.company_department_id::TEXT, \',\' ) AS company_department_ids
						      FROM
						          company_employee_departments ced
						          JOIN company_departments cd ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
						      WHERE
						          ced.cid = ce.cid
						          AND ced.company_employee_id = ce.id
						      GROUP BY
						          ced.company_employee_id,
						          cd.cid
						    ) cd ON ( cd.cid = ce.cid AND cd.company_employee_id = ce.id )
						LEFT JOIN view_company_user_properties vcup ON ( vcup.cid = cu.cid AND vcup.company_user_id = cu.id AND vcup.property_id = ' . ( int ) $intPropertyId . ')
					WHERE
						ce.cid = ' . ( int ) $intCid . '
					   	AND ce.id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesByIdsByCids( $arrintCompanyEmployeeIds, $arrintCid, $objDatabase ) {

		if( false == valArr( $arrintCompanyEmployeeIds ) || false == valArr( $arrintCid ) ) return NULL;

		$strSql = 'SELECT * FROM company_employees WHERE id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' ) AND cid IN ( ' . implode( ',', $arrintCid ) . ')';

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentCompanyEmployeesByCompanyDepartmentIdsByPropertyIdsByCid( $arrmixCompanyDepartmentIds, $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeSystemAdmin, $boolGetProperty = true, $boolIsFromFilter = false, $intLastSyncOn = NULL, $boolIncludeInactiveUsers = false, $boolShowDisabledData = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strIsAdminSql      = '';
		$strIsFilterSql     = '';
		$strWhereSql        = '';
		$strAdminWhereSql   = '';
		$strOrderBy         = '';
		$strJoinSql = '';

		if( true == $boolIncludeSystemAdmin ) {
			$strIsAdminSql = ' OR cu.is_administrator = 1 ';
		}

		if( !$boolIsFromFilter && !$boolIncludeInactiveUsers ) {
			$strIsFilterSql = ' AND cu.is_disabled = 0
								AND cu.deleted_on IS NULL';
		} else {
			$strIsFilterSql = 'AND cu.deleted_on IS NULL';
			$strOrderBy .= 'is_disabled,';
		}

		if( true == valId( $intLastSyncOn ) ) {
			$strJoinSql = ' LEFT JOIN company_user_preferences cup ON (cup.cid = cu.cid AND cup.company_user_id = cu.id AND cup.key = \'HOURLY_RATE\') ';
			$strAdminWhereSql   .= ' AND ( cu.updated_on > \'' . date( 'Y-m-d H:i:s', $intLastSyncOn ) . '\' OR cup.updated_on > \'' . date( 'Y-m-d H:i:s', $intLastSyncOn ) . '\' ) ';
			$strWhereSql        = ' AND ( ce.updated_on > \'' . date( 'Y-m-d H:i:s', $intLastSyncOn ) . '\' OR cupg.created_on > \'' . date( 'Y-m-d H:i:s', $intLastSyncOn ) . '\' OR pga.updated_on > \'' . date( 'Y-m-d H:i:s', $intLastSyncOn ) . '\' OR ced.created_on > \'' . date( 'Y-m-d H:i:s', $intLastSyncOn ) . '\' OR cup.updated_on > \'' . date( 'Y-m-d H:i:s', $intLastSyncOn ) . '\' ) ';
		}

			$strSql = 'SELECT
							*
						FROM
							(
								SELECT
									DISTINCT ( cu.id ) AS company_user_id,
									ce.id,
									cu.is_disabled,
									ce.name_first,
									ce.name_last,
									ce.name_first || \' \' || ce.name_last AS namefull
									' . ( ( true == $boolGetProperty ) ? ', 0 AS property_id ' : '' ) . '
								FROM
									company_employees ce
									JOIN company_users cu ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id AND cu.is_administrator = 1 AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' ) ' . $strJoinSql . '
								WHERE
									ce.cid =  ' . ( int ) $intCid . '
									AND cu.default_company_user_id IS NULL
									AND cu.username NOT LIKE \'%PS Admin%\'
									' . $strIsFilterSql . '
									AND ce.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . $strAdminWhereSql . '
									AND EXISTS (
												SELECT
									                     1
									                 FROM
									                     company_departments cd
									                     JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id ' . $strIsAdminSql . ' )
									                 WHERE
									                     ced.cid = ' . ( int ) $intCid . '
									                     AND ced.company_employee_id = ce.id
									                     AND cd.default_company_department_id IN ( ' . $arrmixCompanyDepartmentIds . ' )
									) 
							UNION
								SELECT
						    		DISTINCT (cu.id) AS company_user_id,
									ce.id,
									cu.is_disabled,
					 				ce.name_first,
					 				ce.name_last,
					 				ce.name_first||\' \'||ce.name_last as namefull
									' . ( ( true == $boolGetProperty ) ? ', CASE WHEN cu.is_administrator = 1 THEN 0 ELSE pga.property_id END AS property_id ' : '' ) . '
								FROM
									company_employees ce
									JOIN company_users cu ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
							        JOIN company_user_property_groups cupg ON ( cupg.cid = cu.cid AND cupg.company_user_id = cu.id )
									JOIN property_group_associations pga ON ( cupg.cid = pga.cid AND cupg.property_group_id = pga.property_group_id )
									JOIN property_preferences pp ON ( pga.cid = pp.cid AND pga.property_id = pp.property_id AND pp.key =\'WORK_ORDER_ALLOW_MULTIPLE_USER_DEPARTMENT\' )
						 			JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[ ' . implode( ',', $arrintPropertyIds ) . ' ] ) lp ON ( lp.is_disabled = ' . ( ( false == $boolShowDisabledData ) ? 0 : 1 ) . ' AND lp.property_id = pga.property_id )
						 			JOIN company_employee_departments ced ON ( ce.cid = ced.cid AND  ced.company_employee_id = ce.id)
						 			JOIN company_departments cd ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id ) ' . $strJoinSql . '
								WHERE
									ce.cid =  ' . ( int ) $intCid . '
									AND cu.default_company_user_id IS NULL
									AND cu.username NOT LIKE \'%PS Admin%\'
									' . $strIsFilterSql . $strWhereSql . '
									AND ce.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
									AND cd.default_company_department_id:: TEXT = ANY ( string_to_array(pp.value, \',\') )
									AND cd.default_company_department_id IN ( ' . $arrmixCompanyDepartmentIds . ' )
								) AS sub
							ORDER BY
							 ' . $strOrderBy . ' 
								namefull ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllCurrentCompanyEmployeesEmailAddressAndIdByCid( $intCid, $objDatabase, $arrstrEmailAddresses = NULL ) {

		$strWhereClause = ( true == valArr( $arrstrEmailAddresses ) ) ? ' AND email_address IN ( \'' . implode( '\',\'', $arrstrEmailAddresses ) . '\' )' : '';

		$strSql = '	SELECT
						DISTINCT( email_address ),
						id
					FROM
						company_employees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( date_terminated IS NULL OR date_terminated > NOW() )' . $strWhereClause;

		$arrmixResponse = ( array ) fetchData( $strSql, $objDatabase );
		$arrstrEmails 	= [];

		foreach( $arrmixResponse as $arrstrResponse ) {
			$arrstrEmails[$arrstrResponse['id']] = $arrstrResponse['email_address'];
		}

		return $arrstrEmails;
	}

	public static function fetchCompanyEmployeeByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ce.id,
						ce.name_first || \' \' || ce.name_last AS employee_name
					FROM
						company_employees ce
					WHERE
						ce.employee_status_type_id = 1
						AND ce.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesByCompanyDepartmentIdsByPropertyIdsByStatusTypeIdsByCid( $arrstrCompanyDepartmentIds, $arrintPropertyIds, $arrintEmployeeStatusTypeIds, $intCid, $boolHideAdmin, $objDatabase, $boolReturnArray = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCheckIncludeAdmin = ( false == $boolHideAdmin ) ? ' OR cu.is_administrator = 1 ':'';

		$strSql = ' SELECT
				    		DISTINCT (cu.id) AS company_user_id,
							ce.id ,
			 				ce.name_first,
			 				ce.name_last,
			 				ce.name_first||\' \'||ce.name_last as namefull,
			 				lp.property_id
						FROM
							company_employees ce
							JOIN company_users cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
							LEFT JOIN view_company_user_properties cup ON ( cup.company_user_id = cu.id AND cup.cid = cu.cid )
				 			JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[ ' . sqlIntImplode( $arrintPropertyIds ) . ' ] ) lp ON lp.is_disabled = 0 AND ( lp.property_id = cup.property_id OR cu.is_administrator = 1 )
						WHERE
							ce.cid =  ' . ( int ) $intCid . '
							AND ( ( cu.default_company_user_id IS NULL ) ';
		$strSql .= ( false == $boolHideAdmin ) ? $strCheckIncludeAdmin : '';
		$strSql .= ' )
								AND cu.username NOT LIKE \'%PS Admin%\'
								AND cu.is_disabled = 0
								AND cu.deleted_on IS NULL
								AND EXISTS (
											SELECT
							                     1
							                 FROM
							                     company_departments cd
							                     JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
							                 WHERE
							                     ced.cid = ' . ( int ) $intCid . '
							                     AND ced.company_employee_id = ce.id
							                     AND (  cd.default_company_department_id IN ( ' . $arrstrCompanyDepartmentIds . ' ) 
							    ) ';

		$strSql .= ( false == $boolHideAdmin ) ? $strCheckIncludeAdmin : '';
		$strSql .= ' )';

		if( true == valArr( $arrintEmployeeStatusTypeIds ) ) {
			$strSql .= ' AND ce.employee_status_type_id IN (\'' . sqlIntImplode( $arrintEmployeeStatusTypeIds ) . '\')';
		}

		$strSql .= 'ORDER BY
					 	ce.name_first,
						ce.name_last';

		if( false != $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesByCidByIdByEmployeeCode( $intCid, $intCompanyEmployeeId, $strEmployeeCode, $objDatabase ) {
		$strSql = 'SELECT 
						ce.id,
						cu.is_disabled,
						cu.is_active_directory_user
					FROM
						company_employees ce 
						JOIN company_users cu ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
					WHERE 
						ce.cid = ' . ( int ) $intCid . '
						AND ce.employee_code = ( \'' . $strEmployeeCode . '\' ) 
						' . ( 0 < ( int ) $intCompanyEmployeeId ? ' AND ce.id <> ' . ( int ) $intCompanyEmployeeId : '' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeIdsByEmployeeCodeByCid( $arrstrEmployeeCode, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						lower( ce.employee_code ) as employee_code,
						ce.id as company_employee_id
					FROM
						company_employees ce
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ce.employee_code IN ( \'' . implode( '\',\'', $arrstrEmployeeCode ) . '\' )';

		$arrstrResults = ( array ) fetchData( $strSql, $objDatabase );
		$arrintCompanyEmployeeIds = [];
		foreach( $arrstrResults as $arrstrResult ) {
			$arrintCompanyEmployeeIds[$arrstrResult['employee_code']] = $arrstrResult['company_employee_id'];
		}

		return $arrintCompanyEmployeeIds;
	}

	public static function fetchCurrentCompanyEmployeesByIdsByCid( $arrintCompanyEmployeeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCompanyEmployeeIds ) ) return NULL;

		$strSql = 'SELECT
						ce.*
					FROM
						company_employees ce
						JOIN company_users cu ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid AND cu.is_disabled = 0 )
					WHERE
						ce.id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' )
						AND ce.cid = ' . ( int ) $intCid . '
						AND ce.is_employee = 1
						AND ce.email_address IS NOT NULL
						AND ( ce.date_terminated IS NULL OR ce.date_terminated > NOW() )';

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentCompanyEmployeesByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ce.*
					FROM
						company_employees ce
						JOIN company_users cu ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid AND cu.is_disabled = 0 )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.is_employee = 1
						AND ce.email_address IS NOT NULL 
						AND ( ce.date_terminated IS NULL OR ce.date_terminated > NOW() )';

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchSimpleCompanyEmployeesQuickSearchByCidByPropertyId( $intCid, $intPropertyId, $objDatabase, $strSearchInput = NULL ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strWhereClause = ( true == valStr( trim( $strSearchInput ) ) ) ? ' AND ( ce.name_first ILIKE \'%' . addslashes( trim( $strSearchInput ) ) . '%\' OR ce.name_last ILIKE \'%t' . addslashes( trim( $strSearchInput ) ) . '%\' )' : '';

		$strSql = 'SELECT
						DISTINCT ce.id,
						ce.name_first,
						ce.name_last,
						ce.phone_number1,
						ce.phone_number2
					FROM
						company_employees ce
						JOIN company_users cu ON ( cu.company_employee_id = ce.id AND ( cu.cid = ce.cid AND cu.default_company_user_id IS NULL ) )
						JOIN view_company_user_properties cup ON ( cup.company_user_id = cu.id AND ( cup.cid = cu.cid AND cu.default_company_user_id IS NULL ) )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND cup.property_id = ' . ( int ) $intPropertyId
						. $strWhereClause . '
						AND cu.is_disabled = 0
					ORDER BY
						ce.name_last, ce.name_first, ce.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesDataByIdsByCIds( $arrintCompanyEmployeeIds, $arrintCId, $objDatabase ) {

		if( false == ( $arrintCompanyEmployeeIds = getIntValuesFromArr( $arrintCompanyEmployeeIds ) ) || false == ( $arrintCId = getIntValuesFromArr( $arrintCId ) ) ) return NULL;

		$strSql = 'SELECT 
						id, 
						name_first,
						name_last
					FROM 
						company_employees 
					WHERE
						id IN ( ' . sqlIntImplode( $arrintCompanyEmployeeIds ) . ' ) AND cid IN ( ' . sqlIntImplode( $arrintCId ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveLeasingAgentByPropertyIdByFirstNameByLastNameByCid( $intPropertyId, $strFirstName, $strLastName, $intCid, $objClientDatabase ) {
		if( false == valId( $intPropertyId ) )
			return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( ce.id )
						ce.*
					FROM
						company_employees AS ce
						JOIN property_leasing_agents AS pla ON ( pla.company_employee_id = ce.id AND pla.cid = ce.cid AND pla.is_disabled = 0 )
						LEFT JOIN company_users AS cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' AND cu.is_disabled = 0 )
					WHERE
						pla.cid = ' . ( int ) $intCid . '
						AND pla.property_id = ' . ( int ) $intPropertyId . '
						AND EXISTS (
						    SELECT
						     1
						    FROM
						     company_departments cd
						     JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
						    WHERE
						     ced.cid = ' . ( int ) $intCid . '
						     AND ced.company_employee_id = ce.id
						     AND cd.default_company_department_id = ' . ( int ) CCompanyDepartment::DEFAULT_COMPANY_DEPARTMENT_LEASING_ID . '
						)
						AND LOWER( ce.name_first ) = ' . pg_escape_literal( $objClientDatabase->getHandle(), \Psi\CStringService::singleton()->strtolower( $strFirstName ) ) . '
						AND LOWER( ce.name_last ) = ' . pg_escape_literal( $objClientDatabase->getHandle(), \Psi\CStringService::singleton()->strtolower( $strLastName ) );

		return self::fetchCompanyEmployee( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyEmployeeRelieverByRuleStopIdsByCid( $arrintRuleStopIds, $intCId, $objDatabase ) {

		if( false == valArr( $arrintRuleStopIds ) ) return;

		$strSql = 'SELECT
						ce.*
					FROM
						company_employees ce
						JOIN company_employee_relievers cer ON ce.id = cer.reliever_company_employee_id AND ce.cid = cer.cid
						JOIN company_users cu ON cer.company_employee_id = cu.company_employee_id AND ( cer.cid = cu.cid AND cu.default_company_user_id IS NULL )
						JOIN rule_stops rs ON cu.id = rs.company_user_id AND ( cu.cid = rs.cid AND cu.default_company_user_id IS NULL )
					WHERE
						rs.id IN ( ' . sqlIntImplode( $arrintRuleStopIds ) . ' )
						AND \'' . date( 'Y-m-d' ) . '\' between DATE ( cer.begin_datetime ) and DATE ( cer.end_datetime )
						AND	cer.deleted_by IS NULL
	 					AND cer.deleted_on IS NULL
						AND cer.cid = ' . ( int ) $intCId;

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchInspectionUsersByCompanyEmployeesByInspectionModuleIdsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'WITH CTE_inspected_by AS (
					SELECT
						cu.cid,
						cu.id AS company_user_id,
						ce.id AS company_employee_id,
						ce.name_first,
						ce.name_last,
						cu.default_company_user_id,
						cu.is_administrator
					FROM
						company_employees ce
						JOIN company_users cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND cu.username != \'%PS Admin%\'
						AND cu.is_disabled = 0
						AND cu.deleted_on IS NULL
						AND ce.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( cu.is_administrator = 1
						OR EXISTS (
							SELECT
								1
							FROM
								view_company_user_properties cup
								JOIN load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], ARRAY [ ' . implode( ',', $arrintPropertyIds ) . ' ] ) lp ON ( lp.is_disabled = 0 AND lp.is_test = 0 AND lp.property_id = cup.property_id )
							WHERE
								cup.company_user_id = cu.id
								AND cup.cid = cu.cid
							)
						)
					)';

		/** * Query to fetch users having permission to module = 'START NEW INSPECTION NEW'. */
		$strSql .= 'SELECT
						*
					FROM
						(
						SELECT
							cte_insby.company_user_id,
							cte_insby.company_employee_id,
							cte_insby.name_first,
							cte_insby.name_last
						FROM
							CTE_inspected_by cte_insby
							LEFT JOIN modules m ON ( m.is_published = 1 AND m.id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . ' AND m.id = ' . CModule::CREATE_NEW_INSPECTIONS . ' )
							LEFT JOIN company_user_permissions cup1 ON ( cup1.cid = cte_insby.cid AND cup1.company_user_id = cte_insby.company_user_id AND cup1.module_id = m.id AND cup1.is_allowed = 1 )
							LEFT JOIN company_user_groups cug ON ( cug.cid = cte_insby.cid AND cug.company_user_id = cte_insby.company_user_id )
							LEFT JOIN company_group_permissions cgp ON ( cgp.cid = cug.cid AND cgp.company_group_id = cug.company_group_id AND cgp.module_id = m.id AND cgp.is_allowed = 1 )
							LEFT JOIN company_group_permissions cgp1 ON ( cgp1.cid = cug.cid AND cgp1.company_group_id = cug.company_group_id AND cgp1.module_id = m.parent_module_id AND cgp1.is_allowed = 1 )
						WHERE
							cte_insby.cid = ' . ( int ) $intCid . '
							AND ( cte_insby.default_company_user_id IS NULL )
							AND ( cup1.id IS NOT NULL OR cgp.module_id IS NOT NULL )
				UNION
					/* Query to fetch Managerial departments users who might not have permissions to module = \'START NEW INSPECTION NEW\' still they are allowed to inspect any inspection from Maintenance product side. */
						SELECT
							cte_insby.company_user_id,
							cte_insby.company_employee_id,
							cte_insby.name_first,
							cte_insby.name_last
						FROM
							CTE_inspected_by cte_insby
						WHERE
							EXISTS (
								SELECT
									1
								FROM
									inspections i
								WHERE
									i.deleted_by IS NULL
									AND i.cid = cte_insby.cid
									AND i.inspected_by = cte_insby.company_employee_id
							) OR cte_insby.is_administrator = 1
						) AS inspected_by
					ORDER BY
						name_first,
						name_last';

					return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesByDefaultCompanyDepartmentIdByCid( $intDefaultCompanyDepartmentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    ce.*
					FROM
					    company_employees ce
					    JOIN company_employee_departments ced ON ( ce.cid = ced.cid AND ce.id = ced.company_employee_id )
					    JOIN company_departments cd ON ( ced.cid = cd.cid AND ced.company_department_id = cd.id )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
					    AND cd.default_company_department_id = ' . ( int ) $intDefaultCompanyDepartmentId;
		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesAdministratorByCompanyDepartmentIdByPropertyIdByCid( $intCompanyDepartmentId, $intPropertyId, $intCid, $objDatabase ) {

        $strSql = 'SELECT
                      *
                    FROM (  (
                            SELECT
                                DISTINCT( ce.id),
                                ce.name_first,
                                ce.name_last
                            FROM
                                company_users cu
                                JOIN company_employees ce ON (ce.id = cu.company_employee_id AND ce.cid = cu.cid)
                            WHERE
                                cu.cid = ' . ( int ) $intCid . '
                                AND COALESCE(cu.is_disabled, 0) = 0
                                AND cu.is_administrator = 1
                                AND EXISTS (
                                    SELECT
                                        1
                                    FROM
                                        company_departments cd
                                        JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
                                    WHERE
                                        ced.cid = ' . ( int ) $intCid . '
                                        AND ced.company_employee_id = ce.id
                                        AND cd.default_company_department_id =  ' . ( int ) $intCompanyDepartmentId . ' 
                                )
                            )
                            UNION 
                            (
                            SELECT
                                DISTINCT( ce.id ),
                                ce.name_first,
                                ce.name_last
                            FROM
                                company_users cu
                                JOIN company_employees ce ON (ce.id = cu.company_employee_id AND ce.cid = cu.cid)
                                JOIN view_company_user_properties cup ON (cup.cid = cu.cid AND cu.id = cup.company_user_id)
                                JOIN properties p ON (p.cid = cup.cid AND p.id = cup.property_id)
                            WHERE
                                cu.cid = ' . ( int ) $intCid . '
                                AND COALESCE(cu.is_disabled, 0) = 0
                                AND p.id = ' . ( int ) $intPropertyId . '
                                AND EXISTS (
                                    SELECT
                                        1
                                    FROM
                                        company_departments cd
                                        JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
                                    WHERE
                                        ced.cid = ' . ( int ) $intCid . '
                                        AND ced.company_employee_id = ce.id
                                        AND cd.default_company_department_id =  ' . ( int ) $intCompanyDepartmentId . ' 
                                ) 
                            )
                        ) as sub_query
                    ORDER BY
                        name_first,
                        name_last';

		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeByIdByDepartmentIdsByPropertyIdByCid( $intCompanyEmployeeId, $arrintDepartmentIds, $intPropertyId, $intCid, $objDatabase, $boolIsShowAdmin = false ) {

		$strCaseCondition = ( true == $boolIsShowAdmin ) ? 1 : 0;
		$strSql = '	SELECT
						CASE
		 				    WHEN cu.is_administrator = 1 AND 1  = ' . $strCaseCondition . ' THEN \'System Administrator\'
			 				ElSE util_get_translated( \'name\', cd.name, cd.details )
			 			END AS company_department,
   	                    cu.is_administrator,
						ce.id,
						cu.id as company_user_id
					FROM
						company_employees ce
                        JOIN company_users cu ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '  )
                        LEFT JOIN company_employee_departments ced ON ( ced.cid = ce.cid AND ced.company_employee_id = ce.id )
                        LEFT JOIN company_departments cd ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
                        LEFT JOIN view_company_user_properties vcup ON ( vcup.cid = cu.cid AND vcup.company_user_id = cu.id AND vcup.property_id = ' . ( int ) $intPropertyId . ' ) 
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.id = ' . ( int ) $intCompanyEmployeeId . '
						AND cu.is_disabled = 0
						AND cu.deleted_on IS NULL
						AND ce.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
                        AND ( ( cd.default_company_department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . '   )  AND vcup.property_id = ' . ( int ) $intPropertyId . ' )';

		if( true == $boolIsShowAdmin ) {
			$strSql .= ' OR cu.is_administrator = 1';
		}

		$strSql .= ' ) ';
		$strSql .= 'GROUP BY
 	                    ce.id,
						cu.id,
                        cu.is_administrator,
                        company_department';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesByEmployeeCodeByCid( $arrstrEmployeeCode, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ce.*
					FROM
						company_employees ce
						JOIN company_users cu ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.employee_code IN ( \'' . implode( '\',\'', $arrstrEmployeeCode ) . '\' )
						AND ( cu.is_disabled = 1 OR cu.is_active_directory_user = 0 )';
		return self::fetchCompanyEmployees( $strSql, $objDatabase );
	}

	public static function fetchCustomCompanyEmployeesByPropertyIdByCid( $intCid, $intPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						ce.id,
						ce.name_first,
						ce.name_last,
						cu.is_disabled,
						cu.username
					FROM
						company_employees ce
						LEFT JOIN company_users cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						LEFT JOIN property_leasing_agents pla ON( pla.company_employee_id = ce.id AND pla.property_id = ' . ( int ) $intPropertyId . ' AND pla.cid = ce.cid )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND pla.id IS NULL
					ORDER BY
						ce.name_last, ce.name_first, cu.username, cu.is_disabled;';

		return fetchData( $strSql, $objDatabase );

	}

	public static function updateCompanyEmployeesDomainName( $arrintCompanyEmployeeIds, $strOldDomain, $strNewDomain, $intCid, $objDatabase ) {
		$strSql = 'UPDATE 
					company_employees
				   SET 
					email_address = replace( lower( email_address ), \'@' . ( string ) $strOldDomain . '\', \'@' . ( string ) $strNewDomain . '\' )
				   WHERE
				    lower( email_address ) ILIKE \'%@' . ( string ) $strOldDomain . '\'  
                    AND id IN (' . implode( ',', $arrintCompanyEmployeeIds ) . ')   
		            AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesByMaintenanceRequestTypeByPropertyIdsByCid( $arrintMaintenanceRequestTypeIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintMaintenanceRequestTypeIds ) ) {
			return [];
		}

		$strSql = 'SELECT
						ce.id,
						ce.name_first,
						ce.name_last,
						ce.name_first||\' \'||ce.name_last as name_full
					FROM
						company_employees ce
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.id IN (
							SELECT 
								mr.company_employee_id
							FROM 
								maintenance_requests mr
							WHERE 
								mr.maintenance_request_type_id IN ( ' . sqlIntImplode( $arrintMaintenanceRequestTypeIds ) . ' ) 
								AND mr.deleted_on IS NULL 
								AND mr.cid = ' . ( int ) $intCid . ' 
								AND mr.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						)';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveCompanyEmployeesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) ) return NULL;

		$arrstrWhere = [];

		$strSql = 'SELECT
						DISTINCT ON ( ce.id, ce.name_last )
						ce.id AS recipient_id,
						ce.name_first,
						ce.name_last,
						cup.property_id,
						cu.is_administrator,
						ce.is_employee
					FROM
						company_employees AS ce
						LEFT JOIN company_users AS cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						LEFT JOIN view_company_user_properties AS cup ON ( cup.company_user_id = cu.id AND cup.cid = cu.cid )
						LEFT JOIN properties AS p ON ( p.id = cup.property_id AND p.cid = cup.cid )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND cu.is_disabled = 0
						AND ce.email_address IS NOT NULL
						AND( cup.property_id = ' . ( int ) $intPropertyId . ' OR cu.is_administrator = 1 )';

		$strSql .= ' GROUP BY ce.id,
						ce.name_first,
						ce.name_last,
						cup.property_id,
						cu.is_administrator,
						ce.is_employee limit 100';

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeesDetailsByPropertyIdByCid( $intCompanyEmployeeId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intCompanyEmployeeId ) || false == valId( $intPropertyId ) ) return NULL;

		$arrstrWhere = [];

		$strSql = 'SELECT DISTINCT ON ( ce.id, ce.name_last )
						ce.id,
						ce.name_first,
						ce.name_last,
						ce.employee_status_type_id,
						ce.email_address,
						ce.phone_number1,
						ce.phone_number2,
						ce.phone_number1_type_id,
						ce.phone_number2_type_id,
						ce.date_started,
						ce.created_on,
						cu.is_administrator,
						cup.property_id,
						p.property_name,
						( array_to_string ( array_agg ( DISTINCT ( cd.name ) ), \', \' ) ) as company_department_name,
						( array_to_string ( array_agg ( DISTINCT ( cg.name  ) ), \', \' ) ) as company_group_name,
						cu.is_administrator,
						ce.is_employee
					FROM
						company_employees AS ce
						LEFT JOIN company_users AS cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						LEFT JOIN view_company_user_properties AS cup ON ( cup.company_user_id = cu.id AND cup.cid = cu.cid )
						LEFT JOIN properties AS p ON ( p.id = cup.property_id AND p.cid = cup.cid )
						LEFT JOIN company_employee_departments ced ON (ce.cid = ced.cid AND ce.id = ced.company_employee_id)
						LEFT JOIN company_departments cd ON ( ced.cid = cd.cid AND ced.company_department_id = cd.id )
						LEFT JOIN company_user_groups cug ON ( cug.cid = cu.cid AND cug.company_user_id = cu.id )
						LEFT JOIN company_groups cg ON ( cg.cid = cug.cid AND cg.id = cug.company_group_id )
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.id = ' . ( int ) $intCompanyEmployeeId . '
						AND cu.is_disabled = 0
						AND ce.email_address IS NOT NULL
						AND( cup.property_id = ' . ( int ) $intPropertyId . ' OR cu.is_administrator = 1 )';

		$strSql .= ' GROUP BY ce.id,
						ce.name_first,
						ce.name_last,
						ce.employee_status_type_id,
						ce.email_address,
						ce.date_started,
						ce.created_on,
						ce.phone_number1_type_id,
						ce.phone_number1,
						ce.phone_number2_type_id,
						ce.phone_number2,
						cup.property_id,
						p.property_name,
						ce.date_started,
						cu.is_administrator,
						ce.is_employee';

		return self::fetchCompanyEmployees( $strSql, $objDatabase, false );
	}

	public static function fetchEnabledCompanyEmployeesByPropertyIdsByCids( $arrintCompanyEmployeeIds, $arrintPropertyIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) || false == valArr( $arrintCompanyEmployeeIds ) ) {
			return [];
		}

		$strSql = 'SELECT
						cu.cid,
						cup.property_id,
						ce.name_first,
						ce.name_last,
						ce.email_address
					FROM
						company_employees ce
						LEFT JOIN company_users cu ON cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						LEFT JOIN view_company_user_properties cup ON cup.company_user_id = cu.id AND cup.cid = cu.cid
					WHERE
						ce.id IN ( ' . sqlIntImplode( $arrintCompanyEmployeeIds ) . ' )
						AND cu.cid IN ( ' . sqlIntImplode( $arrintCids ) . ' )
						AND cu.default_company_user_id IS NULL
						AND cup.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						AND cu.username != \'PS Admin\'
						AND cu.is_disabled=0
						AND cu.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllCompanyEmployeesByEmailAddressByCid( $intCid, $intPropertyId, $arrstrEmailAddresses, $objDatabase ) {

		if( false == valArr( $arrstrEmailAddresses ) ) {
			return [];
		}

		$strSql = '	SELECT
						cu.cid,
						cup.property_id,
						ce.name_first,
						ce.name_last,
						ce.email_address
					FROM
						company_employees ce
						LEFT JOIN company_users cu ON cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						LEFT JOIN view_company_user_properties cup ON cup.company_user_id = cu.id AND cup.cid = cu.cid
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND cup.property_id = ' . ( int ) $intPropertyId . '
						AND ce.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( ce.date_terminated IS NULL OR ce.date_terminated > NOW() )
						AND email_address IN ( \'' . implode( '\',\'', $arrstrEmailAddresses ) . '\' )
						AND cu.username != \'PS Admin\'
						AND cu.is_disabled=0
						AND cu.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCurrentCompanyEmployeesByIdsByCids( $arrintCompanyEmployeeIds, $arrintCids, $objDatabase ) {

		if( false == valIntArr( $arrintCompanyEmployeeIds ) || false == valIntArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ce.*
					FROM
						company_employees ce
						JOIN company_users cu ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid AND cu.is_disabled = 0 )
					WHERE
						ce.id IN ( ' . sqlIntImplode( $arrintCompanyEmployeeIds ) . ' )
						AND ce.cid IN ( ' . sqlIntImplode( $arrintCids ) . ' )
						AND ce.is_employee = 1
						AND ce.email_address IS NOT NULL
						AND ( ce.date_terminated IS NULL OR ce.date_terminated > NOW() )';

		return self::fetchCompanyEmployees( $strSql, $objDatabase, false );
	}

	public static function fetchCompanyEmployeeByIdByDepartmentIdByPropertyIdByCid( $intCompanyEmployeeId, $intCompanyDepartmentId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intCompanyEmployeeId ) || false == valId( $intCid ) || false == valId( $intCompanyDepartmentId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
                        ce.*
                   FROM
                        company_employees ce
                        JOIN company_employee_departments ced ON ( ced.cid = ce.cid AND ced.company_employee_id = ce.id )
                        JOIN company_departments cd ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
                        JOIN company_users cu ON ( ce.id = cu.company_employee_id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
                        LEFT JOIN property_leasing_agents pla ON ( pla.company_employee_id = ce.id AND pla.cid = ce.cid AND pla.property_id = ' . ( int ) $intPropertyId . ' )
                   WHERE
                        ce.cid = ' . ( int ) $intCid . '
                        AND ce.id = ' . ( int ) $intCompanyEmployeeId . '
                        AND ( ( pla.id IS NOT NULL and pla.is_disabled = 0 ) OR cu.is_administrator = 1 )
                        AND cd.default_company_department_id =' . ( int ) $intCompanyDepartmentId;

		return self::fetchCompanyEmployee( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeByDefaultPropertyLeaseAgentByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT 
						ce.*
                   FROM
                        company_employees ce 
                        JOIN property_leasing_agents pla ON ( pla.company_employee_id = ce.id AND pla.cid = ce.cid AND pla.property_id = ' . ( int ) $intPropertyId . ' )
                        LEFT JOIN company_users cu ON( ce.id = cu.company_employee_id AND ce.cid = cu.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
                   WHERE
                        pla.property_id = ' . ( int ) $intPropertyId . '
                        AND pla.cid = ' . ( int ) $intCid . '
                        AND pla.is_disabled = 0
                        AND pla.is_default = 1
                        AND ( cu.id IS NULL OR cu.is_disabled <> 1 )
                   ORDER BY pla.id DESC
                   LIMIT 1';

		return self::fetchCompanyEmployee( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeEmailsByPropertyIdByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						DISTINCT ON ( subquery.id, subquery.name )
						subquery.id,
						subquery.name,
						subquery.email_address
					FROM (
						SELECT
							ce.id,
							CONCAT( ce.name_first, \' \', ce.name_last ) AS name,
							ce.email_address,
							cu.is_administrator,
							ce.is_employee,
							cu.id AS company_user_id,
							ce.cid
						FROM
							company_employees AS ce
							LEFT JOIN company_users AS cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						WHERE
							ce.cid = ' . ( int ) $intCid . '
							AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
							AND cu.default_company_user_id IS NULL
							AND cu.username <> \'PS Admin\'
							AND cu.is_disabled = 0
							AND ce.email_address IS NOT NULL  AND ( cu.is_administrator = 1
							OR EXISTS (
										SELECT
											1
										FROM
											view_company_user_properties cup
										WHERE
											cup.company_user_id = cu.id
											AND cup.cid = cu.cid
											AND cup.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
										) 
							) AND ce.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' 
						GROUP BY ce.id,
							ce.name_first,
							ce.name_last,
							cu.is_administrator,
							ce.is_employee,
							cu.id,
							ce.cid
					) AS subquery
						LEFT JOIN view_company_user_properties vcup ON ( vcup.company_user_id = subquery.company_user_id AND vcup.cid = subquery.cid )
						LEFT JOIN properties pp ON ( pp.id = vcup.property_id AND pp.cid = subquery.cid )
					ORDER BY subquery.name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeEmailByIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT 
						ce.email_address
					FROM
						company_employees ce
						JOIN company_users cu ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE 
						ce.id = ' . ( int ) $intCompanyEmployeeId . '
						AND ce.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.is_disabled = 0';

		$arrmixResult = fetchData( $strSql, $objDatabase );
		return $arrmixResult[0]['email_address'] ?? NULL;
	}

}
?>