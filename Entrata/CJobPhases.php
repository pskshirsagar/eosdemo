<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobPhases
 * Do not add any new functions to this class.
 */

class CJobPhases extends CBaseJobPhases {

	public static function fetchMaxOrderNum( $intJobId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intJobId ) ) return NULL;

		$strSql = 'SELECT
						max( order_num ) + 1 as next_order_num
					FROM
						job_phases
					WHERE
						cid = ' . ( int ) $intCid . '
						AND job_id = ' . ( int ) $intJobId;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchJobPhasesByJobIdByCidByOrderNum( $intJobId, $intCid, $intOrderNum, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intJobId ) ) return NULL;

		$strSql = 'SELECT *
				FROM
					job_phases
				WHERE
					cid = ' . ( int ) $intCid . '
					AND job_id = ' . ( int ) $intJobId . '
					AND order_num > ' . ( int ) $intOrderNum . '
				ORDER BY order_num ASC';

		return parent::fetchJobPhases( $strSql, $objDatabase );
	}

	public static function fetchJobPhasesWithOrderNumByJobIdByCid( $intJobId, $intCid, $objDatabase, $boolFetchData = false, $intUnitTypeId = NULL ) {

		$strSelect = '';
		$strJoinCondition = '';

		if( false == valId( $intCid ) || false == valId( $intJobId ) ) return NULL;

		if( true == valId( $intUnitTypeId ) ) {
			$strJoinCondition = 'LEFT JOIN job_unit_type_phases jutp ON( jp.cid = jutp.cid AND jp.id=jutp.job_phase_id AND jutp.unit_type_id = ' . ( int ) $intUnitTypeId . ' )';
			$strSelect        = ',jutp.unit_count';
		}

		$strSql = 'SELECT
						jp.*
						' . $strSelect . '
					FROM
						job_phases jp
						' . $strJoinCondition . '
					WHERE
						jp.cid = ' . ( int ) $intCid . '
						AND jp.job_id = ' . ( int ) $intJobId . '
					ORDER BY
						jp.order_num ASC';

		if( true == $boolFetchData ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchJobPhases( $strSql, $objDatabase );
	}

	public static function fetchJobPhasesByIdsByCid( $arrintIds, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valIntArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						job_phases
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN( ' . sqlIntImplode( $arrintIds ) . ' )';

		return parent::fetchJobPhases( $strSql, $objDatabase );
	}

	public static function fetchJobPhasesWithActualBudgetAmountByJobIdByCid( $intJobId, $intCid, $objDatabase, $arrmixParameters = NULL ) {

		$strConditions  = '';

		if( false == valId( $intCid ) || false == valId( $intJobId ) ) return NULL;

		if( true == valArr( $arrmixParameters ) ) {
			$strConditions .= ( true == $arrmixParameters['is_from_contract'] ) ? ' AND ad.ap_contract_id IS NOT NULL AND ad.unit_type_id IS NULL AND ad.maintenance_location_id IS NULL' : '';

			$strConditions .= ( 'job_budget' == $arrmixParameters['budget_type'] ) ? ' AND ad.unit_type_id IS NULL AND ad.maintenance_location_id IS NULL' : '';
			$strConditions .= ( 'unit_budget' == $arrmixParameters['budget_type'] && true == valId( $arrmixParameters['budget_type_id'] ) ) ? ' AND ad.maintenance_location_id IS NULL AND ad.unit_type_id =' . $arrmixParameters['budget_type_id'] : '';
			$strConditions .= ( 'maintenance_location_budget' == $arrmixParameters['budget_type'] && true == valId( $arrmixParameters['budget_type_id'] ) ) ? ' AND ad.unit_type_id IS NULL AND ad.maintenance_location_id=' . $arrmixParameters['budget_type_id'] : '';
		}

		$strSql = ' SELECT
						DISTINCT jp.id,
						jp.name,
						jp.order_num,
						SUM(
								CASE
								WHEN ah.ap_header_sub_type_id IN ( ' . CApHeaderSubType::JOB_CHANGE_ORDER . ', ' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . ' ) AND ah.approved_on IS NOT NULL THEN ad.transaction_amount
								WHEN ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_PHASE_BUDGET . ' THEN ad.transaction_amount
								WHEN ah.id IS NULL THEN 0
								ELSE 0
								END
							) OVER ( PARTITION BY jp.id, jp.cid ) AS actual_budget_amount
					FROM
						job_phases jp
						LEFT JOIN ap_headers ah ON ( jp.cid = ah.cid AND jp.id = ah.job_phase_id )
						LEFT JOIN ap_details ad ON ( ah.cid = ad.cid AND jp.id = ad.job_phase_id AND ad.ap_header_id = ah.id ' . $strConditions . ' )
					WHERE
						jp.cid = ' . ( int ) $intCid . '
						AND jp.job_id = ' . ( int ) $intJobId . '
					ORDER BY
						jp.order_num';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobPhasesWithActualBudgetAmountByJobIdByApHeaderSubTypeIdsByCid( $intJobId, $arrintApHeaderSubTypeIds, $intCid, $objDatabase, $arrmixParameters = NULL ) {

		if( true == valArr( $arrmixParameters ) ) {
			$intApCodeId                    = ( true == isset( $arrmixParameters['ap_code_id'] ) ) ? $arrmixParameters['ap_code_id'] : NULL;
			$intJobPhaseId                  = ( true == isset( $arrmixParameters['job_phase_id'] ) ) ? $arrmixParameters['job_phase_id'] : NULL;
			$strPostMonth                   = ( true == isset( $arrmixParameters['post_month'] ) ) ? $arrmixParameters['post_month'] : NULL;
			$boolIsListing                  = ( true == isset( $arrmixParameters['is_listing'] ) ) ? $arrmixParameters['is_listing'] : false;
			$intApContractId                = ( true == isset( $arrmixParameters['ap_contract_id'] ) ) ? $arrmixParameters['ap_contract_id'] : NULL;
			$strOrderBy                     = ( true == isset( $arrmixParameters['sort_by'] ) ) ? $arrmixParameters['sort_by'] : NULL;
			$boolConsiderCancelledContracts = ( true == isset( $arrmixParameters['is_cancelled_contract'] ) ) ? $arrmixParameters['is_cancelled_contract'] : false;
			$boolGlHeaderSubTypeId          = ( true == isset( $arrmixParameters['is_gl_transaction'] ) ) ? $arrmixParameters['is_gl_transaction'] : false;
			$boolIsUnitBudget               = ( true == isset( $arrmixParameters['is_unit_type_budget'] ) ) ? $arrmixParameters['is_unit_type_budget'] : '';
			$boolIsPropertyLocationBudget   = ( true == isset( $arrmixParameters['is_property_location_budget'] ) ) ? $arrmixParameters['is_property_location_budget'] : '';
			$boolIsAllTypeJobBudget         = ( true == isset( $arrmixParameters['is_all_type_budget'] ) ) ? $arrmixParameters['is_all_type_budget'] : false;
			$boolIsJobByUnitLocation        = ( true == isset( $arrmixParameters['is_job_by_unit_location'] ) ) ? $arrmixParameters['is_job_by_unit_location'] : false;
		}

		if( false == valId( $intCid ) || false == valId( $intJobId ) || false == valArr( $arrintApHeaderSubTypeIds ) ) return NULL;

		$strSubSql							= '';
		$strCancelledContracts				= '';
		$strApCodeConditionForJE			= '';
		$strPostMonthConditionForJE			= '';
		$strApCodeConditionForInvoice		= '';
		$strPostMonthConditionForInvoice	= '';
		$strGlHeaderSubTypeId				= '';
		$strUnitOrPLWhereCondition          = '';

		$strWhereCondition          = ( true == valId( $intJobPhaseId ) ) ? ' AND jp.id =' . ( int ) $intJobPhaseId : '';
		$strWhereCondition          .= ( true == valId( $intApContractId ) ) ? ' AND apc.id =' . ( int ) $intApContractId : '';
		$strUnitOrPLWhereCondition  = ( true == $boolIsUnitBudget ) ? ' AND ad.unit_type_id IS NOT NULL ' : '';
		$strUnitOrPLWhereCondition  .= ( true == $boolIsPropertyLocationBudget ) ? ' AND ad.maintenance_location_id IS NOT NULL ' : '';
		$strUnitOrPLWhereCondition  .= ( false == $boolIsUnitBudget && false == $boolIsPropertyLocationBudget && false == $boolIsAllTypeJobBudget && true == $boolIsJobByUnitLocation ) ? ' AND ad.unit_type_id IS NULL AND ad.maintenance_location_id IS NULL AND ad.details IS NULL ' : '';

		$strUnitOrPLWhereConditionJE  = ( true == $boolIsUnitBudget ) ? ' AND gd.unit_type_id IS NOT NULL ' : '';
		$strUnitOrPLWhereConditionJE  .= ( true == $boolIsPropertyLocationBudget ) ? ' AND gd.maintenance_location_id IS NOT NULL ' : '';
		$strUnitOrPLWhereConditionJE  .= ( false == $boolIsUnitBudget && false == $boolIsPropertyLocationBudget && false == $boolIsAllTypeJobBudget && true == $boolIsJobByUnitLocation ) ? ' AND gd.unit_type_id IS NULL AND gd.maintenance_location_id IS NULL ' : '';

		if( false == $boolConsiderCancelledContracts ) {
			$strCancelledContracts = 'AND apc.ap_contract_status_id <> ' . CApContractStatus::CANCELLED;
		}

		$strGlHeaderSubTypeId = ( false == $boolGlHeaderSubTypeId ) ? ' AND gh.gl_header_status_type_id = ' . CGlHeaderStatusType::POSTED : ' AND gh.gl_header_status_type_id <> ' . CGlHeaderStatusType::DELETED;

		if( true == $boolIsListing ) {

			$strApCodeConditionForInvoice       = ( true == valId( $intApCodeId ) ) ? ' AND ad.ap_code_id =' . ( int ) $intApCodeId : '';
			$strApCodeConditionForJE            = ( true == valId( $intApCodeId ) ) ? ' AND gd.ap_code_id =' . ( int ) $intApCodeId : '';
			$strPostMonthConditionForJE         = ( true == valStr( $strPostMonth ) ) ? ' AND gd.post_month =\'' . $strPostMonth . '\'' : '';
			$strPostMonthConditionForInvoice    = ( true == valStr( $strPostMonth ) ) ? ' AND ad.post_month =\'' . $strPostMonth . '\'' : '';
		}

		$strOrderBy = ( false == valStr( $strOrderBy ) ) ? ' sub.post_date DESC, sub.header_id DESC ' : ' sub.' . $strOrderBy;

		if( true == in_array( CApHeaderSubType::STANDARD_JOB_INVOICE, $arrintApHeaderSubTypeIds ) ) {

			$strSubSql = '
						UNION
						( SELECT
						    jp.id,
						    jp.name,
						    gh.post_date,
						    true as is_posted,
						    gh.post_month,
						    gh.header_number::TEXT as header_number,
						    gh.id as header_id,
						    NULL as ap_header_sub_type_id,
						    NULL as approved_on,
						    gd.post_month as gl_post_month,
						    gh.id as gl_header_id,
						    apc.id as ap_contract_id,
						    apc.name as ap_contract_name,
						    gd.id as detail_id,
						    gd.ap_code_id,
						    gd.memo,
						    sum( gd.amount ) AS actual_budget_amount,
						    NULL AS ap_payee_name,
						    jp.is_budget_by_period,
						    gd.unit_type_id,
							gd.maintenance_location_id
						FROM
							job_phases jp
						    JOIN gl_details gd ON ( jp.cid = gd.cid AND jp.id = gd.job_phase_id AND gd.reclass_gl_detail_id IS NULL AND gd.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . ' )
						    JOIN ap_codes ac ON ( ac.cid = gd.cid AND ac.id = gd.ap_code_id AND ac.deleted_on IS NULL )
						    JOIN gl_headers gh ON ( jp.cid = gh.cid AND gh.id = gd.gl_header_id AND gh.gl_header_type_id = ' . CGlHeaderType::STANDARD . ' AND gh.reclass_gl_header_id IS NULL )
						    LEFT JOIN ap_contracts apc ON ( apc.cid = gd.cid AND apc.job_id = jp.job_id AND apc.id = gd.ap_contract_id ' . $strCancelledContracts . ' )
						WHERE
						    gh.cid = ' . ( int ) $intCid . '
						    AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . $strGlHeaderSubTypeId . '
						    AND jp.job_id = ' . ( int ) $intJobId . $strWhereCondition . $strPostMonthConditionForJE . $strApCodeConditionForJE . $strUnitOrPLWhereConditionJE . '
							AND gh.is_template IS FALSE
						GROUP BY
							jp.id,
						    jp.cid,
						    gh.id,
						    gh.cid,
						    gd.id,
						    gd.cid ,
						    apc.id,
						    apc.cid )';
		}

		$strSql = 'SELECT sub.* from ( ( SELECT
						jp.id,
						jp.name,
						ah.post_date,
						ah.is_posted,
						ah.post_month,
						ah.header_number,
						ah.id as header_id,
						ah.ap_header_sub_type_id,
						ah.approved_on,
						NULL as gl_post_month,
						NULL as gl_header_id,
						apc.id as ap_contract_id,
						apc.name as ap_contract_name,
						ad.id as detail_id,
						ad.ap_code_id,
						ad.description,
						sum( ad.transaction_amount ) AS actual_budget_amount,
						CASE
							WHEN ah.lease_customer_id IS NULL THEN ( COALESCE ( ap.company_name, \'\' ) || \' \' || COALESCE ( apl.location_name, \'\' ) )
							ELSE func_format_refund_customer_names ( ah.header_memo )
						END AS ap_payee_name,
						jp.is_budget_by_period,
						ad.unit_type_id,
						ad.maintenance_location_id
					FROM
						job_phases jp
						 JOIN ap_headers ah ON ( jp.cid = ah.cid AND ah.ap_header_sub_type_id IN (' . sqlIntImplode( $arrintApHeaderSubTypeIds ) . ') )
						 JOIN ap_details ad ON ( ah.cid = ad.cid AND jp.id = ad.job_phase_id AND ad.ap_header_id = ah.id )
						 JOIN ap_codes ac ON ( ac.cid = ad.cid AND ac.id = ad.ap_code_id AND ac.deleted_on IS NULL )
						 JOIN ap_payees ap ON ( ah.cid = ap.cid AND ap.id = ah.ap_payee_id )
						 JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
						LEFT JOIN ap_contracts apc ON ( apc.cid = ad.cid AND apc.job_id = jp.job_id AND apc.id = ad.ap_contract_id ' . $strCancelledContracts . ' )
					WHERE
						jp.cid = ' . ( int ) $intCid . '
						AND jp.job_id = ' . ( int ) $intJobId . '
						AND ah.reversal_ap_header_id IS NULL
						AND ad.is_deleted = false 
						AND ad.deleted_on IS NULL ' . $strWhereCondition . $strApCodeConditionForInvoice . $strPostMonthConditionForInvoice . $strUnitOrPLWhereCondition . '
						AND ( ( ah.ap_header_sub_type_id IN ( ' . CApHeaderSubType::JOB_CHANGE_ORDER . ', ' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . ' ) AND ah.approved_on IS NOT NULL ) OR ah.ap_header_sub_type_id NOT IN ( ' . CApHeaderSubType::JOB_CHANGE_ORDER . ', ' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . ' ) )
					GROUP BY
						jp.id,
						jp.cid,
						ah.id,
						ah.cid,
						ad.ap_code_id,
						ad.description,
						ap.id,
						ad.id,
						ap.cid,
						apl.id,
						apl.cid,
						apc.id,
						apc.cid, 
						ad.unit_type_id, 
						ad.maintenance_location_id )' . $strSubSql . ' ) AS sub ORDER BY ' . $strOrderBy;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobPhasesByJobIdByCid( $intJobId, $intCid, $objDatabase, $strOrderBy = 'order_num' ) {

		if( false == valId( $intJobId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						job_phases jp
					WHERE
						jp.cid = ' . ( int ) $intCid . ' 
						AND jp.job_id = ' . ( int ) $intJobId . '
					ORDER BY ' . $strOrderBy;

		return self::fetchJobPhases( $strSql, $objDatabase );
	}

	public static function fetchContractPhasesWithActualBudgetAmountByJobIdByApContractIdByCid( $intJobId, $intApContractId, $intCid, $objDatabase, $arrmixParameters = NULL ) {

		$strConditions  = '';

		if( false == valId( $intJobId ) || false == valId( $intCid ) || false == valId( $intApContractId ) ) return NULL;

		if( true == valArr( $arrmixParameters ) ) {
			$strConditions .= ( true == $arrmixParameters['is_from_contract'] ) ? ' AND ad.ap_contract_id IS NOT NULL AND ad.unit_type_id IS NULL AND ad.maintenance_location_id IS NULL' : '';

			$strConditions .= ( 'job_budget' == $arrmixParameters['budget_type'] ) ? ' AND ad.unit_type_id IS NULL AND ad.maintenance_location_id IS NULL' : '';
			$strConditions .= ( 'unit_budget' == $arrmixParameters['budget_type'] && true == valId( $arrmixParameters['budget_type_id'] ) ) ? ' AND ad.maintenance_location_id IS NULL AND ad.unit_type_id =' . $arrmixParameters['budget_type_id'] : '';
			$strConditions .= ( 'maintenance_location_budget' == $arrmixParameters['budget_type'] && true == valId( $arrmixParameters['budget_type_id'] ) ) ? ' AND ad.unit_type_id IS NULL AND ad.maintenance_location_id=' . $arrmixParameters['budget_type_id'] : '';
		}

		$strSql = ' SELECT
						DISTINCT jp.id,
						jp.name,
						jp.order_num,
						sum( CASE
								WHEN ( ah.ap_header_sub_type_id = ' . CApHeaderSubType::CONTRACT_BUDGET . ' OR ( ( ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_CHANGE_ORDER . ' OR ah.ap_header_sub_type_id = ' . CApHeaderSubType::CONTRACT_CHANGE_ORDER . ' ) AND ah.approved_on IS NOT NULL ) )
									THEN ad.transaction_amount
								ELSE 0
								END
							) OVER ( PARTITION BY ad.ap_contract_id, jp.id, jp.cid ) AS actual_budget_amount
					FROM
						job_phases jp
						LEFT JOIN ap_details ad ON ( jp.cid = ad.cid AND jp.id = ad.job_phase_id AND ad.is_deleted = false AND ad.deleted_on IS NULL AND ad.ap_contract_id = ' . ( int ) $intApContractId . ' )
						LEFT JOIN ap_headers ah ON ( jp.cid = ah.cid AND ad.ap_header_id = ah.id )
					WHERE
						jp.cid = ' . ( int ) $intCid . '
						AND jp.job_id = ' . ( int ) $intJobId . $strConditions . '
 					ORDER BY
						jp.order_num';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobPhaseIdsByJobIdsByCid( $arrintJobs, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valIntArr( $arrintJobs ) ) return NULL;

		$strSql = 'SELECT
						jp.id
					FROM
						job_phases jp
					WHERE
						jp.cid = ' . ( int ) $intCid . '
						AND jp.job_id IN ( ' . sqlIntImplode( $arrintJobs ) . ' )';

		$arrintJobPhasesData = ( array ) fetchData( $strSql, $objDatabase );
		return array_keys( rekeyArray( 'id', $arrintJobPhasesData ) );
	}

}