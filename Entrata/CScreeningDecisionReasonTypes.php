<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningDecisionReasonTypes
 * Do not add any new functions to this class.
 */

class CScreeningDecisionReasonTypes extends CBaseScreeningDecisionReasonTypes {

	public static function fetchScreeningDecisionReasonTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningDecisionReasonType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningDecisionReasonType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningDecisionReasonType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedScreeningDecisionReasonTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM screening_decision_reason_types WHERE is_published = true ORDER BY order_num';

		return self::fetchScreeningDecisionReasonTypes( $strSql, $objDatabase );
	}

	public static function fetchScreeningDecisionReasonTypesByScreeningDecisionReasonTypeIds( $arrintScreeningDecisionReasonTypeIds, $objDatabase ) {

		if( false == valArr( $arrintScreeningDecisionReasonTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM screening_decision_reason_types WHERE id IN( ' . implode( ',', $arrintScreeningDecisionReasonTypeIds ) . ' ) ';

		return self::fetchScreeningDecisionReasonTypes( $strSql, $objDatabase );
	}
}
?>