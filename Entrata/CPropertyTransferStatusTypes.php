<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyTransferStatusTypes
 * Do not add any new functions to this class.
 */

class CPropertyTransferStatusTypes extends CBasePropertyTransferStatusTypes {

    public static function fetchPropertyTransferStatusTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CPropertyTransferStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchPropertyTransferStatusType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CPropertyTransferStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }
}
?>