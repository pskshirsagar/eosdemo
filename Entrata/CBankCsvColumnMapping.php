<?php

class CBankCsvColumnMapping extends CBaseBankCsvColumnMapping {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMappingName( $objClientDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getMappingName() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_mapping_name', __( 'Mapping name is required.' ) ) );

			return $boolIsValid;
		}

		$strWhere = 'WHERE
			cid = ' . ( int ) $this->getCid() . '
			AND bank_account_id = ' . ( int ) $this->getBankAccountId() . '
			AND mapping_name like \'%' . trim( $this->getMappingName() ) . '%\'';
		if( 0 < \Psi\Eos\Entrata\CBankCsvColumnMappings::createService()->fetchBankCsvColumnMappingCount( $strWhere, $objClientDatabase ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_mapping_name', __( 'Mapping name already exists, please create a new mapping name.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDateColumn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionTypeColumn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescriptionColumn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmountColumn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionNumberColumn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequiredColumns() {
		$boolIsValid = true;

		if( true == is_null( $this->getDateColumn() ) || true == is_null( $this->getAmountColumn() ) || true == is_null( $this->getTransactionNumberColumn() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_required_column', __( 'Date, Amount and Check number are required to save mapping.' ) ) );
		}

		return $boolIsValid;
	}

	public function valBankTransactions( $intGlReconciliationId, $arrobjGlReconciliationBankTransactions ) {
		$boolIsValid = true;

		if( true == valArr( $arrobjGlReconciliationBankTransactions ) ) {
			if( true == valId( $intGlReconciliationId ) ) {
				foreach( $arrobjGlReconciliationBankTransactions as $objGlReconciliationBankTransaction ) {
					$strFormattedDate = date( 'm/d/Y', strtotime( $objGlReconciliationBankTransaction->getTransactionDate() ) );
					if( $strFormattedDate != $objGlReconciliationBankTransaction->getTransactionDate() ) {
						$boolIsValid &= false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_transaction_date', 'Date Format must be [MM/DD/YYYY]. Please update the format on your CSV and re-upload.' ) );
						break;
					}

					if( false == is_numeric( $objGlReconciliationBankTransaction->getAmount() ) || 0 == $objGlReconciliationBankTransaction->getAmount() ) {
						$boolIsValid &= false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_transaction_amount', 'The column mapping for [Amount] does not appear to contain [Amount] information. Please review your mapping.' ) );
						break;
					}
				}
			} else {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_transactions', __( 'Unable to delete the mapping as it might be associated with an active or paused bank reconciliation.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase, $arrmixParameters = [] ) {
		$boolIsValid = true;

		$intGlReconciliationId					= ( true == isset( $arrmixParameters['gl_reconciliation_id'] ) ) ? $arrmixParameters['gl_reconciliation_id'] : NULL;
		$arrobjGlReconciliationBankTransactions	= ( true == isset( $arrmixParameters['gl_reconciliation_bank_transactions'] ) ) ? $arrmixParameters['gl_reconciliation_bank_transactions'] : [];

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valMappingName( $objClientDatabase );
				$boolIsValid &= $this->valRequiredColumns();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valRequiredColumns();
				$boolIsValid &= $this->valBankTransactions( $intGlReconciliationId, $arrobjGlReconciliationBankTransactions );
				break;

			case VALIDATE_DELETE:
			case 'validate_bank_transactions':
				$boolIsValid &= $this->valBankTransactions( $intGlReconciliationId, $arrobjGlReconciliationBankTransactions );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>