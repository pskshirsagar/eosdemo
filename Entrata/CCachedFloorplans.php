<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedFloorplans
 * Do not add any new functions to this class.
 */

class CCachedFloorplans extends CBaseCachedFloorplans {

	public static function fetchConventionalFloorplansByPropertyIdByCid( $arrintPropertyFloorplanIds, $intCid, $objDatabase ) {
		return self::fetchCachedFloorplansByPropertyFloorplanIdsByCid( $arrintPropertyFloorplanIds, $intCid, date( 'Y-m-d' ), $objDatabase, [COccupancyType::CONVENTIONAL] );
	}

	public static function fetchCachedFloorplansByPropertyFloorplanIdsByCid( $arrintPropertyFloorplanIds, $intCid, $objDatabase, $strMoveInDate = NULL, $arrintOccupancyTypeIds ) {

		if( false == valId( $intCid ) || false == valArr( $arrintPropertyFloorplanIds ) ) {
			return NULL;
		}

		$strOccupancyTypeCondition = '';
		if( true == valArr( $arrintOccupancyTypeIds ) ) {
			$strOccupancyTypeCondition = sprintf(' AND occupancy_type_id IN ( %s )', implode( ', ', $arrintOccupancyTypeIds ) );
		}

		if( false == valStr( $strMoveInDate ) ) {
			$strMoveInDate = date( 'Y-m-d' );
		}

		return self::fetchCachedFloorplans( sprintf( 'SELECT * FROM cached_floorplans WHERE cid = %d AND effective_date = \'%s\' AND property_floorplan_id IN ( %s )%s', ( int ) $intCid, $strMoveInDate, implode( ', ', $arrintPropertyFloorplanIds ), $strOccupancyTypeCondition ), $objDatabase );
	}

	public static function fetchCachedFloorplansByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $strMoveInDate = NULL, $arrintOccupancyTypeIds ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strOccupancyTypeCondition = '';
		if( true == valArr( $arrintOccupancyTypeIds ) ) {
			$strOccupancyTypeCondition = sprintf(' AND occupancy_type_id IN ( %s )', implode( ', ', $arrintOccupancyTypeIds ) );
		}

		if( false == valStr( $strMoveInDate ) ) {
			$strMoveInDate = date( 'Y-m-d' );
		}

		return self::fetchCachedFloorplans( sprintf( 'SELECT * FROM cached_floorplans WHERE property_id = %d AND cid = %d AND effective_date = \'%s\'%s', ( int ) $intPropertyId, ( int ) $intCid, $strMoveInDate, $strOccupancyTypeCondition ), $objDatabase );
	}

}
?>