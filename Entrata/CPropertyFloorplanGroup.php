<?php

class CPropertyFloorplanGroup extends CBasePropertyFloorplanGroup {

	protected $m_strFloorplanGroupName;

    /**
     * Get Functions
     */

	public function getFloorplanGroupName() {
		return $this->m_strFloorplanGroupName;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( true == isset( $arrmixValues['floorplan_group_name'] ) )			$this->setFloorplanGroupName( $arrmixValues['floorplan_group_name'] );

		return parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

	public function setFloorplanGroupName( $strFloorplanGroupName ) {
		$this->m_strFloorplanGroupName = $strFloorplanGroupName;
	}

	/**
	 * Validate Functions
	 */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyFloorplanId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFloorplanGroupId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>