<?php

class CCompanyUserGroup extends CBaseCompanyUserGroup {

	/**
	 * Create Functions Get Functions
	 */

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getCompanyUserGroupsFields() {
		return [
			'user_groups'           => __( 'User Groups' ),
			'company_user_groups'   => __( 'Company User Groups' )
		];
	}

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

     /**
      * Validation example
      */

        // if( false == isset( $this->m_intId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

     /**
      * Validation example
      */

        // if( false == isset( $this->m_intCid ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;

     /**
      * Validation example
      */

        // if( false == isset( $this->m_intCompanyUserId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_user_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCompanyGroupId() {
        $boolIsValid = true;

     /**
      * Validation example
      */

        // if( false == isset( $this->m_intCompanyGroupId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_group_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

     /**
      * Validation example
      */

        // if( false == isset( $this->m_intCreatedBy ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

     /**
      * Validation example
      */

        // if( false == isset( $this->m_strCreatedOn ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>