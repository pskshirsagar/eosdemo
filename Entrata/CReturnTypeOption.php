<?php

class CReturnTypeOption extends CBaseReturnTypeOption {
	public $m_strReturnTypeName;
	public $m_strReturnTypeDescription;
	public $m_fltReturnTypeOccurrencePercent;
	protected $m_strPaymentType;

	/**
     * Get functions
     */

	public function getReturnTypeName() {
		return  $this->m_strReturnTypeName;
    }

    public function getReturnTypeDescription() {
		return  $this->m_strReturnTypeDescription;
	}

	public function getReturnTypeOccurrencePercent() {
		return  $this->m_fltReturnTypeOccurrencePercent;
	}

	public function getPaymentType() {
		return $this->m_strPaymentType;
    }

	/**
     * Set functions
     */

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrValues['return_type_name'] ) )
        	$this->setReturnTypeName( $arrValues['return_type_name'] );
        if( true == isset( $arrValues['return_type_description'] ) )
        	$this->setReturnTypeDescription( $arrValues['return_type_description'] );
        if( true == isset( $arrValues['return_type_occurrence_percent'] ) )
        	$this->setReturnTypeOccurrencePercent( $arrValues['return_type_occurrence_percent'] );
        if( true == isset( $arrValues['payment_type'] ) )
        	$this->setPaymentType( $arrValues['payment_type'] );

        return;
    }

    public function setReturnTypeName( $strReturnTypeName ) {
    	$this->m_strReturnTypeName = $strReturnTypeName;
    }

    public function setReturnTypeDescription( $strReturnTypeDescription ) {
    	$this->m_strReturnTypeDescription = $strReturnTypeDescription;
    }

    public function setReturnTypeOccurrencePercent( $fltReturnTypeOccurrencePercent ) {
    	$this->m_fltReturnTypeOccurrencePercent = $fltReturnTypeOccurrencePercent;
    }

	public function setPaymentType( $strPaymentType ) {
		$this->m_strPaymentType = $strPaymentType;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>