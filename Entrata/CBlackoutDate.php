<?php

class CBlackoutDate extends CBaseBlackoutDate {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScheduledBlackoutDateId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBlackoutDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsMoveIn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsMoveOut() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsAmenity() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>