<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportNewInstances
 * Do not add any new functions to this class.
 */

class CReportNewInstances extends CBaseReportNewInstances {

	public static function fetchReportInstancesByCid( $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				ri.id,
				ri.name,
				r.id AS report_id,
				util_get_translated( \'title\', r.title, r.details ) AS title,
				r.is_schedulable,
				rv.id AS report_version_id,
				rv.major,
				rv.minor,
				rv.definition
			FROM
				report_new_instances ri
				JOIN reports r ON ( r.cid = ri.cid AND r.id = ri.report_id )
				JOIN report_versions rv ON ( rv.cid = r.cid AND rv.report_id = r.id AND rv.id = ri.report_version_id )
				JOIN default_report_versions drv ON ( rv.default_report_version_id = drv.id )
				JOIN company_users cu ON rv.cid = cu.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND
					CASE
						WHEN ( rv.definition->>\'allow_only_admin\' )::BOOLEAN THEN cu.is_administrator = 1
						ELSE TRUE
					END
					AND
					CASE
						WHEN ( rv.definition->>\'allow_only_psi_admin\' )::BOOLEAN THEN cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
						ELSE cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					END
				JOIN report_new_group_instances rgn ON ( rgn.cid = ri.cid AND rgn.report_new_instance_id = ri.id )
				JOIN report_new_groups rg ON ( rg.cid = ri.cid AND rg.id = rgn.report_new_group_id )
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND r.is_schedulable = TRUE
				AND r.is_published = TRUE
				AND COALESCE( ( rv.definition->>\'is_published\' )::BOOLEAN, TRUE ) = TRUE
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
				AND r.report_type_id IN( ' . implode( ',', [ CReportType::SYSTEM, CReportType::CUSTOM ] ) . ' )
				AND rg.report_group_type_id = ' . CReportGroupType::STANDARD . '
			ORDER BY
				r.title,
				rv.major DESC,
				rv.minor DESC
		';

		return self::fetchReportNewInstances( $strSql, $objDatabase, true );
	}

	public static function fetchReportInstancesByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return [];

		$strSql = '
			SELECT
				ri.*
			FROM
				report_new_instances ri
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND ri.id IN( ' . implode( ', ', $arrintIds ) . ' )
		';

		return self::fetchReportNewInstances( $strSql, $objDatabase );
	}

	public static function fetchReportInstancesForGroup( $intReportGroupId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				ri.*,
				r.report_type_id
			FROM
				report_new_groups rg
				JOIN report_new_group_instances rgi ON rg.cid = rgi.cid AND rg.id = rgi.report_new_group_id
				JOIN report_new_instances ri ON rgi.cid = ri.cid AND rgi.report_new_instance_id = ri.id
				JOIN report_versions rv ON ri.cid = rv.cid AND ri.report_version_id = rv.id
				JOIN reports r ON ri.cid = r.cid AND ri.report_id = r.id
			WHERE
				rg.cid = ' . ( int ) $intCid . '
				AND rg.id = ' . ( int ) $intReportGroupId . '
				AND rg.deleted_on IS NULL
				AND ri.deleted_on IS NULL
				AND rgi.deleted_on IS NULL
				AND COALESCE( ( rv.definition->>\'is_published\' )::BOOLEAN, TRUE ) = TRUE
				AND COALESCE( r.is_published, TRUE ) = TRUE
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
			ORDER BY
				rgi.id';

		return self::fetchReportNewInstances( $strSql, $objDatabase );
	}

	public static function fetchSchedulableReportInstances( $intCompanyUserId, $intCid, $objDatabase, CReportsValidation $objReportValidator = NULL ) {
		$strSql = '
			SELECT
				ri.*,
				jsonb_build_object(
					\'id\', r.id,
					\'cid\', r.cid,
					\'report_type_id\', r.report_type_id,
					\'name\', r.name,
					\'title\', util_get_translated( \'title\', r.title, r.details ),
					\'created_on\', r.created_on,
					\'description\', util_get_translated( \'description\', r.description, r.details ),
					\'report_version_id\', rv.id,
					\'definition\', rv.definition,
					\'major\', rv.major,
					\'minor\', rv.minor,
					\'expiration\', rv.expiration,
					\'is_expired\', COALESCE( rv.expiration, NOW() ) < NOW(),
					\'title_addendum\', rv.title_addendum,
					\'report_instance_id\', ri.id
				) AS report
			FROM
				report_new_instances ri
				JOIN reports r ON r.cid = ri.cid AND r.id = ri.report_id
				JOIN report_versions rv ON rv.cid = ri.cid AND rv.id = ri.report_version_id
				JOIN company_users cu ON cu.cid = rv.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND
					CASE
						WHEN (rv.definition->>\'allow_only_admin\')::BOOLEAN THEN cu.is_administrator = 1
						ELSE TRUE
					END
					AND
					CASE
						WHEN (rv.definition->>\'allow_only_psi_admin\')::BOOLEAN THEN cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
						ELSE cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					END
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND r.is_published = TRUE
				AND r.is_schedulable = TRUE
				AND ri.deleted_on IS NULL
				AND ri.deleted_by IS NULL
				AND COALESCE( (rv.definition->>\'is_published\')::BOOLEAN, TRUE) = TRUE
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
			ORDER BY
				ri.name
		';

		$arrobjReportInstances = self::fetchReportNewInstances( $strSql, $objDatabase ) ?: [];

		if( false == is_null( $objReportValidator ) ) {
			foreach( $arrobjReportInstances as $intReportInstanceId => $objReportInstance ) {
				if( false == $objReportValidator->validateReport( $objReportInstance->getReport() ) ) {
					unset( $arrobjReportInstances[$intReportInstanceId] );
				}
			}
		}

		return $arrobjReportInstances;
	}

	public static function fetchReportNewInstanceDetailsByReportIdByCid( $intReportId, $intCid, $objDatabase, $boolIsViewInstances = false ) {

		$strJoinSql = 'JOIN';
		$strWhereClauseCondition = 'AND rg.report_group_type_id = ' . CReportGroupType::STANDARD . '';

		if( $boolIsViewInstances ) {
			$strJoinSql = 'LEFT JOIN ';
			$strWhereClauseCondition = '';
		}

		$strSql = '
		SELECT
			DISTINCT ON ( ri.name, ri.id )
			ri.id AS report_instance_id,
			util_get_translated( \'name\', ri.name, ri.details ) AS name,
			ri.report_id,
			ri.company_user_id,
			ce.name_first,
			ce.name_last,
			rv.major,
			rv.minor,
			rv.is_default,
			rv.id AS report_version_id,
			CASE 
				WHEN COALESCE( rv.expiration, NOW() ) >= NOW() THEN FALSE 
				ELSE TRUE 
			END AS is_expired,
			CASE WHEN MAX( COALESCE( rng.report_group_type_id, 0 ) ) OVER ( PARTITION BY ri.id, rv.id ) = ' . CReportGroupType::STANDARD . ' THEN true ELSE false END AS company_reports,
			CASE WHEN MAX( COALESCE( rng.report_group_type_id, 0 ) ) OVER ( PARTITION BY ri.id, rv.id ) = ' . CReportGroupType::STANDARD . ' THEN MAX( rng.name ) OVER ( PARTITION BY ri.id, rv.id ) ELSE \'--\' END AS group,
			rg.report_group_type_id,
			ri.module_id,
			ri.filters,
			rgi.id AS report_group_instance_id,
			util_get_translated( \'description\', ri.description, ri.details ) AS description,
			COALESCE( sch_count.schedule_count, 0 ) AS schedule_count,
			packet_count.packet_count
		FROM
			report_new_instances ri
			JOIN report_versions rv ON ( ri.cid = rv.cid AND ri.report_version_id = rv.id )
			' . $strJoinSql . ' report_new_group_instances AS rgi ON ri.cid = rgi.cid AND ri.id = rgi.report_new_instance_id
			' . $strJoinSql . ' report_new_groups AS rg ON rg.cid = rgi.cid AND rg.id = rgi.report_new_group_id
			LEFT JOIN LATERAL 
			(
				SELECT
					COUNT ( rs.id ) AS schedule_count,
					rs.report_filter_id
				FROM
					report_schedules rs
				WHERE
					rs.cid = ' . ( int ) $intCid . '
					AND rs.report_new_instance_id = ri.id
					AND rs.deleted_by IS NULL
				GROUP BY
					rs.report_filter_id
			) sch_count ON TRUE
			LEFT JOIN LATERAL 
			(
				SELECT
					COUNT ( rgi_sub.report_new_instance_id ) AS packet_count
				FROM 
					report_new_groups rg_sub
					JOIN report_new_group_instances rgi_sub ON ( rg_sub.cid = rgi_sub.cid AND rg_sub.id = rgi_sub.report_new_group_id )
				WHERE 
					rg_sub.cid = ri.cid AND
					rgi_sub.report_new_instance_id = ri.id AND
					rg_sub.report_group_type_id = ' . CReportGroupType::PACKET . '
					AND rgi_sub.deleted_by IS NULL
			) AS packet_count ON TRUE
			JOIN company_users cu ON ( cu.cid = ri.cid AND cu.id = ri.company_user_id )
			LEFT JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
			LEFT JOIN  report_new_groups AS rng ON rng.cid = ri.cid AND rng.id = rgi.report_new_group_id AND rng.report_group_type_id = ' . CReportGroupType::STANDARD . '
		WHERE
			ri.cid = ' . ( int ) $intCid . '
			AND ri.report_id = ' . ( int ) $intReportId . '
			' . $strWhereClauseCondition . '
			AND ri.deleted_by IS NULL
			AND rgi.deleted_by IS NULL
			AND CASE 
					WHEN (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedClient\')::text IS NOT NULL
						THEN  rv.cid = ANY ((\'{ \' || trim( both \'[]\' from (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedClient\')::text ) || \' }\' ) ::INT[]) 
					WHEN (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedCid\')::text IS NOT NULL OR (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedCid\')::text IS NOT NULL
						THEN  rv.cid = ANY ((\'{ \' || trim( both \'[]\' from (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedCid\')::text ) || \' }\' ) ::INT[])
					ELSE TRUE
				END
		ORDER BY 
			ri.name,
			ri.id,
			rgi.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportNewInstancesByInstanceNamesByReportGroupTypeIdByCid( $arrstrReportNames, $intReportNewGroupTypeId, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrReportNames ) ) {
			return [];
		}

		$strSql = 'SELECT
						rni.*,
						util_get_translated( \'name\', rni.name, rni.details ) AS name,
						util_get_translated( \'description\', rni.description, rni.details ) AS description
					FROM
						report_new_instances rni
						JOIN report_new_group_instances rngi ON ( rni.cid = rngi.cid AND rni.id = rngi.report_new_instance_id )
						JOIN report_new_groups rng ON ( rng.cid = rngi.cid AND rngi.report_new_group_id = rng.id )
					WHERE
						rni.cid = ' . ( int ) $intCid . '
						AND rni.name IN ( \'' . implode( '\',\'', $arrstrReportNames ) . '\' )
						AND rni.deleted_by IS NULL
						AND rngi.deleted_by IS NULL
						AND rng.report_group_type_id = ' . ( int ) $intReportNewGroupTypeId;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveReportNewInstanceByIdByCid( $intId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				ri.*,
				r.report_type_id
			FROM
				report_new_instances AS ri
				JOIN reports AS r ON ri.cid = r.cid AND ri.report_id = r.id
			WHERE
				ri.id = ' . ( int ) $intId . '
				AND ri.cid = ' . ( int ) $intCid . '
				AND ri.deleted_by IS NULL
		';

		return self::fetchReportNewInstance( $strSql, $objDatabase );
	}

	public static function fetchReportInstancesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) : array {

		$strSql = '
			SELECT
				DISTINCT ON ( ri.name, ri.id )
				ri.id,
				ri.id AS report_instance_id,
				ri.name,
				r.id AS report_id,
				ri.filters,
				util_get_translated( \'title\', r.title, r.details ) AS title,
				r.is_schedulable,
				ce.name_first,
				ce.name_last,
				rv.is_default,
				rv.id AS report_version_id,
				rv.major,
				rv.minor,
				util_get_translated( \'description\', ri.description, ri.details ) AS description,
				COALESCE( sch_count.schedule_count, 0 ) AS schedule_count,
				COUNT( packet_count.report_new_instance_id ) OVER ( PARTITION BY ri.id ) AS packet_count
			FROM
				report_new_instances AS ri
				JOIN reports AS r ON r.cid = ri.cid AND r.id = ri.report_id
				JOIN report_versions AS rv ON rv.cid = r.cid AND rv.report_id = r.id AND rv.id = ri.report_version_id
				JOIN default_report_versions AS drv ON rv.default_report_version_id = drv.id
				JOIN company_users AS cu ON rv.cid = cu.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND
					CASE
						WHEN ( rv.definition->>\'allow_only_admin\' )::BOOLEAN THEN cu.is_administrator = 1
						ELSE TRUE
					END
					AND
					CASE
						WHEN ( rv.definition->>\'allow_only_psi_admin\' )::BOOLEAN THEN cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
						ELSE cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					END
				JOIN report_new_group_instances AS rgn ON rgn.cid = ri.cid AND rgn.report_new_instance_id = ri.id
				JOIN report_new_groups AS rg ON rg.cid = ri.cid AND rg.id = rgn.report_new_group_id
				LEFT JOIN LATERAL 
				(
					SELECT
						COUNT ( rs.id ) AS schedule_count,
						rs.report_filter_id
					FROM
						report_schedules AS rs
					WHERE
						rs.cid = ' . ( int ) $intCid . '
						AND rs.report_new_instance_id = ri.id
						AND rs.deleted_by IS NULL
					GROUP BY
						rs.report_filter_id
				) AS sch_count ON TRUE
				LEFT JOIN LATERAL 
				(
					SELECT
						rgi_sub.report_new_instance_id
					FROM 
						report_new_groups AS rg_sub
						JOIN report_new_group_instances AS rgi_sub ON rg_sub.cid = rgi_sub.cid AND rg_sub.id = rgi_sub.report_new_group_id
					WHERE 
						rg_sub.cid = ri.cid AND
						rgi_sub.report_new_instance_id = ri.id AND
						rg_sub.report_group_type_id = ' . CReportGroupType::PACKET . '
						AND rgi_sub.deleted_by IS NULL
				) AS packet_count ON TRUE
				LEFT JOIN company_employees AS ce ON cu.company_employee_id = ce.id AND cu.cid = ce.cid
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND r.is_published = TRUE
				AND COALESCE( ( rv.definition->>\'is_published\' )::BOOLEAN, TRUE ) = TRUE
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
				AND r.report_type_id IN( ' . implode( ',', [ CReportType::SYSTEM, CReportType::CUSTOM ] ) . ' )
				AND rg.report_group_type_id = ' . CReportGroupType::STANDARD . '
				AND ri.deleted_by IS NULL
				AND rgn.deleted_by IS NULL
				AND rv.is_default IS false
				AND CASE 
						WHEN (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedClient\')::text IS NOT NULL
							THEN  rv.cid = ANY ((\'{ \' || trim( both \'[]\' from (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedClient\')::text ) || \' }\' ) ::INT[]) 
						WHEN (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedCid\')::text IS NOT NULL OR (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedCid\')::text IS NOT NULL
							THEN  rv.cid = ANY ((\'{ \' || trim( both \'[]\' from (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedCid\')::text ) || \' }\' ) ::INT[])
						ELSE TRUE
					END
			ORDER BY
				ri.name,
				ri.id
		';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportNewInstancesModuleIdCountByReporIdByCid( $intReportId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						COUNT ( rni.id ) OVER ( PARTITION BY rni.module_id ) AS module_count
					FROM
						report_new_instances AS rni
					WHERE
						rni.cid = ' . ( int ) $intCid . '
						AND rni.report_id = ' . ( int ) $intReportId . '
					ORDER BY
						module_count DESC';

		$arrmixReportNewInstancesModuleIdCount = fetchData( $strSql, $objDatabase );

		return $arrmixReportNewInstancesModuleIdCount[0]['module_count'] ?? 0;
	}

	// TODO: currently adding this for Dashboard reports.
	public static function fetchDuplicateReportInstanceNames( CReportNewInstance $objReportInstance, $objDatabase ) {
		$strInstanceIdSql = NULL !== $objReportInstance->getId() ? ' AND rni.id != ' . $objReportInstance->getId() : '';

		$strSql = '
			SELECT
				*
			FROM
				report_new_instances rni
				JOIN reports r ON r.cid = rni.cid AND r.id = rni.report_id
			WHERE
				rni.cid = ' . ( int ) $objReportInstance->getCid() . '
				AND rni.deleted_by IS NULL
				AND r.report_type_id = ' . CReportType::SISENSE . '
				AND LOWER( rni.name ) = LOWER( ' . $objReportInstance->sqlName() . ' )
				' . $strInstanceIdSql;

		return self::fetchReportNewInstances( $strSql, $objDatabase );
	}

}