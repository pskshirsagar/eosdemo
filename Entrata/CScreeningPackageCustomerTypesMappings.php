<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningPackageCustomerTypesMappings
 * Do not add any new functions to this class.
 */

class CScreeningPackageCustomerTypesMappings extends CBaseScreeningPackageCustomerTypesMappings {

	public static function fetchActiveScreeningPackageCustomerTypesMappingsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						spctm.*
					FROM
						screening_package_customer_types_mappings spctm
						JOIN lease_types lt ON ( lt.id = spctm.lease_type_id )
					WHERE
						spctm.cid = ' . ( int ) $intCid . '
						AND spctm.property_id = ' . ( int ) $intPropertyId . '
						AND lt.is_published = true
						AND spctm.is_active = 1';

		return self::fetchScreeningPackageCustomerTypesMappings( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningPackageCustomerTypesMappingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						default_screening_package.default_screening_package_id, array_to_string( array_agg( DISTINCT default_screening_package.property_id ), \',\' ) AS property_ids
					FROM
						( SELECT
								spctm.*
							FROM
								screening_package_customer_types_mappings spctm
								JOIN lease_types lt ON ( lt.id = spctm.lease_type_id )
							WHERE
								spctm.cid = ' . ( int ) $intCid . '
								AND spctm.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								AND lt.is_published = true
								AND spctm.is_active = 1
				  		) AS default_screening_package
					GROUP BY
						default_screening_package.default_screening_package_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningPackageCustomerTypesMappingsByLeaseTypeIdByPropertyIdByCid( $intLeaseTypeId, $intPropertyId, $intCid, $objDatabase, $boolIsProspectPortalRequest = false ) {

		$strCondition = ( true == $boolIsProspectPortalRequest ) ? ' AND screening_applicant_requirement_type_id <> ' . CScreeningApplicantRequirementType::NOT_REQUIRED : '';

		$strSql = 'SELECT
					    *
					FROM
					    screening_package_customer_types_mappings
					WHERE
					    cid = ' . ( int ) $intCid . '
					    AND property_id = ' . ( int ) $intPropertyId . '
					    AND lease_type_id = ' . ( int ) $intLeaseTypeId . '
						AND is_active = 1 ' .
						$strCondition;

		return self::fetchScreeningPackageCustomerTypesMappings( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningPackageCustomerTypesMappingsByScreeningPackageIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {
		$strSql = 'SELECT 
						* 
					FROM
						screening_package_customer_types_mappings
					WHERE
						cid = ' . ( int ) $intCid . '
						AND default_screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND is_active = 1';

		return self::fetchScreeningPackageCustomerTypesMappings( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningPackageCustomerTypesMappingsByPropertyIdsByScreeningPackageIdByCid( $arrintPropertyIds, $intScreeningPackageId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_package_customer_types_mappings
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND default_screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND is_active = 1';

		return self::fetchScreeningPackageCustomerTypesMappings( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningPackageCustomerTypesMappingsByDefaultScreeningPackageIdsByCid( $arrintDefaultScreeningPackageIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintDefaultScreeningPackageIds ) ) return;

		$strSql = 'SELECT 
						*  
					FROM 
						screening_package_customer_types_mappings 
					WHERE 
						cid = ' . ( int ) $intCid . ' 
						AND default_screening_package_id IN (' . sqlIntImplode( $arrintDefaultScreeningPackageIds ) . ') 
						AND is_active = 1';

		return CScreeningPackageCustomerTypesMappings::fetchScreeningPackageCustomerTypesMappings( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningPackageCustomerTypesMappingsByScreeningPackageIdByPropertyIdsByCid( $intScreeningPackageId, $arrintPropertyIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_package_customer_types_mappings
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND default_screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND is_active = 1';

		$arrmixResult = executeSql( $strSql, $objDatabase );
		$arrmixResult = ( false == getArrayElementByKey( 'failed', $arrmixResult ) ) ? getArrayElementByKey( 'data', $arrmixResult ) : [];

		return $arrmixResult;
	}

	public static function fetchAllActiveScreeningPackageCustomerTypesMappingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( !valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						screening_package_customer_types_mappings
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						AND is_active = 1';

		return self::fetchScreeningPackageCustomerTypesMappings( $strSql, $objDatabase );
	}

	public static function fetchAllActiveScreeningPackageCustomerTypesMappingsByPrimaryApplicantIdByPropertyIdByCid( $arrintApplicantId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicantId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return false;

		$strSql = 'SELECT 
						ap.id 
					FROM 
						applicant_applications aa
						JOIN applicants ap ON ( aa.cid = ap.cid and aa.applicant_id = ap.id )
						JOIN screening_package_customer_types_mappings spm ON ( aa.cid = spm.cid and aa.customer_relationship_id = spm.customer_relationship_id )
					WHERE 
					ap.id IN ( ' . sqlIntImplode( $arrintApplicantId ) . ' )
					AND ap.cid =' . ( int ) $intCid . '
					AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
					AND spm.property_id =' . ( int ) $intPropertyId . '
					AND is_active = 1';

		return self::fetchScreeningPackageCustomerTypesMappings( $strSql, $objDatabase );
	}

}
?>