<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAncillaryPromoRegions
 * Do not add any new functions to this class.
 */

class CAncillaryPromoRegions extends CBaseAncillaryPromoRegions {

	public static function deleteAncillaryPromoRegionsByAncillaryPlanPromoDetailsIds( $arrintAncillaryPlanPromoDetailsIds, $objDatabase ) {
		if( false == valArr( $arrintAncillaryPlanPromoDetailsIds ) ) {
			return NULL;
		}

		$strSql = 'DELETE
					FROM
						ancillary_promo_regions
					WHERE
						ancillary_plan_promo_details_id IN ( ' . implode( ',', $arrintAncillaryPlanPromoDetailsIds ) . ' ) ';

		if( false == $objDatabase->execute( $strSql ) ) {
			return false;
		}

		return true;
	}

}
?>