<?php

class CPropertyEmailRule extends CBasePropertyEmailRule {

	protected $m_boolSendsToResident;
	protected $m_boolSendsToProspect;
	protected $m_boolSendsToEmployee;
	protected $m_boolSendsToPsEmployee;
	protected $m_boolAllowDisabling;
	protected $m_boolAllowCustomHeaderText;
	protected $m_boolAllowCustomFooterText;

    public function setSendsToResident( $boolSendsToResident ) {
    	$this->m_boolSendsToResident = CStrings::strToBool( $boolSendsToResident );
    }

    public function getSendsToResident() {
    	return $this->m_boolSendsToResident;
    }

    public function setSendsToProspect( $boolSendsToProspect ) {
    	$this->m_boolSendsToProspect = CStrings::strToBool( $boolSendsToProspect );
    }

    public function getSendsToProspect() {
    	return $this->m_boolSendsToProspect;
    }

    public function setSendsToEmployee( $boolSendsToEmployee ) {
    	$this->m_boolSendsToEmployee = CStrings::strToBool( $boolSendsToEmployee );
    }

    public function getSendsToEmployee() {
    	return $this->m_boolSendsToEmployee;
    }

    public function setSendsToPsEmployee( $boolSendsToPsEmployee ) {
    	$this->m_boolSendsToPsEmployee = CStrings::strToBool( $boolSendsToPsEmployee );
    }

    public function getSendsToPsEmployee() {
    	return $this->m_boolSendsToPsEmployee;
    }

    public function setAllowDisabling( $boolAllowDisabling ) {
    	$this->m_boolAllowDisabling = CStrings::strToBool( $boolAllowDisabling );
    }

    public function getAllowDisabling() {
    	return $this->m_boolAllowDisabling;
    }

    public function setAllowCustomHeaderText( $boolAllowCustomHeaderText ) {
    	$this->m_boolAllowCustomHeaderText = CStrings::strToBool( $boolAllowCustomHeaderText );
    }

    public function getAllowCustomHeaderText() {
    	return $this->m_boolAllowCustomHeaderText;
    }

    public function setAllowCustomFooterText( $boolAllowCustomFooterText ) {
    	$this->m_boolAllowCustomFooterText = CStrings::strToBool( $boolAllowCustomFooterText );
    }

    public function getAllowCustomFooterText() {
    	return $this->m_boolAllowCustomFooterText;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( isset( $arrmixValues['sends_to_resident'] ) )			$this->setSendsToResident( $arrmixValues['sends_to_resident'] );
        if( isset( $arrmixValues['sends_to_prospect'] ) )			$this->setSendsToProspect( $arrmixValues['sends_to_prospect'] );
        if( isset( $arrmixValues['sends_to_employee'] ) )			$this->setSendsToEmployee( $arrmixValues['sends_to_employee'] );
        if( isset( $arrmixValues['sends_to_ps_employee'] ) )		$this->setSendsToPsEmployee( $arrmixValues['sends_to_employee'] );
        if( isset( $arrmixValues['allow_disabling'] ) )				$this->setAllowDisabling( $arrmixValues['allow_disabling'] );
        if( isset( $arrmixValues['allow_custom_header_text'] ) )	$this->setAllowCustomHeaderText( $arrmixValues['allow_custom_header_text'] );
        if( isset( $arrmixValues['allow_custom_footer_text'] ) )	$this->setAllowCustomFooterText( $arrmixValues['allow_custom_footer_text'] );

        return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSystemEmailTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomHeaderText() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomFooterText() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valResidentsDisabled() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valProspectsDisabled() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmployeesDisabled() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public function getCustomPropertyHeaderText() {
		$strPropertyCustomHeaderText = '';
		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $this->getSystemEmailTypeId(), $this->m_objDatabase );

		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) && true == $objSystemEmailType->getAllowCustomHeaderText() ) {
			$strPropertyCustomHeaderText = '<div align="left" style="width:580px;"><table cellpadding="0" cellspacing="0" border="0" width="570px" ><tr><td style="font:normal normal normal 12px arial;line-height: 20px;color:#808080;">' . $this->getCustomHeaderText() . '</td></tr></table></div><br/>';
		}

		return $strPropertyCustomHeaderText;
	}

	public function getCustomPropertyFooterText() {
		$strPropertyCustomFooterText = '';
		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $this->getSystemEmailTypeId(), $this->m_objDatabase );

		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) && true == $objSystemEmailType->getAllowCustomFooterText() ) {
			$strPropertyCustomFooterText = '<br/><div align="left" style="width:580px;"><table cellpadding="0" cellspacing="0" border="0" width="570px" ><tr><td class="custom-footer" style="font:normal normal normal 12px arial;line-height: 20px;color:#808080;">' . $this->getCustomFooterText() . '</td></tr></table></div>';
		}

		return $strPropertyCustomFooterText;

	}

}
?>