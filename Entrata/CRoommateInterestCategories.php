<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRoommateInterestCategories
 * Do not add any new functions to this class.
 */

class CRoommateInterestCategories extends CBaseRoommateInterestCategories {

	public static function fetchPublishedRoommateInterestCategories( $objDatabase ) {

		$strSql = 'SELECT * FROM roommate_interest_categories WHERE is_published = 1';
		return parent::fetchRoommateInterestCategories( $strSql, $objDatabase );
	}

	public static function fetchRoommateInterestCategoriesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    DISTINCT ON ( ric.id ) ric.*
					FROM
					    roommate_interest_categories ric
					    JOIN roommate_interests ri ON ( ri.roommate_interest_category_id = ric.id )
					WHERE
						ri.cid = ' . ( int ) $intCid . '
						AND ric.is_published = 1';

		return self::fetchRoommateInterestCategories( $strSql, $objDatabase );
	}
}
?>