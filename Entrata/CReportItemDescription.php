<?php

class CReportItemDescription extends CBaseReportItemDescription {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportItemTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valItemKey() {
		$boolIsValid = true;

		if( false == isset( $this->m_strItemKey ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'key is required.' ) );
		}
		return $boolIsValid;
	}

	public function valItemName() {
		$boolIsValid = true;

		if( false == isset( $this->m_strItemName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valItemDescription() {
		$boolIsValid = true;

		if( false == isset( $this->m_strItemDescription ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'description is required.' ) );
		}
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOnStandard() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valItemName();
				$boolIsValid &= $this->valItemKey();
				$boolIsValid &= $this->valItemDescription();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>