<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportBatches
 * Do not add any new functions to this class.
 */

class CImportBatches extends CBaseImportBatches {

	public static function fetchUnProcessedImportBatchByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM import_batches WHERE cid =' . ( int ) $intCid . ' AND processed_on IS NULL;';
		return self::fetchImportBatch( $strSql, $objDatabase );
	}

	public static function fetchCustomUnProcessedAprrovedImportBatches( $objDatabase ) {
		$strSql = 'SELECT
						imb.*
					FROM
						import_batches imb
						INNER JOIN import_sources ims ON ( imb.import_source_id = ims.id AND imb.cid = ims.cid )
						INNER JOIN integration_databases ind ON ( ims.integration_database_id = ind.id AND ims.cid = ind.cid )
					WHERE
						imb.approved_by IS NOT NULL
						AND imb.approved_on IS NOT NULL
						AND imb.processed_on IS NULL
						AND ind.integration_client_type_id IN ( ' . implode( ',', CIntegrationClientType::$c_arrintMigrateIntegrationClientTypeIds ) . ' );';

		return self::fetchImportBatches( $strSql, $objDatabase );
	}

	public static function fetchAprrovedImportBatchByImportSourceIdByCid( $intImportSourceId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM import_batches WHERE cid =' . ( int ) $intCid . ' AND import_source_id = ' . ( int ) $intImportSourceId . ' AND approved_by IS NOT NULL AND approved_on IS NOT NULL AND PROCESSED_ON IS NULL;';
		return self::fetchImportBatch( $strSql, $objDatabase );
	}

	public static function fetchCustomUnProcessedIntegrationAprrovedImportBatches( $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ib.*
					FROM
						import_batches ib
						JOIN import_sources ims ON (ims.id = ib.import_source_id AND ims.cid = ib.cid)
						JOIN import_mappings im ON (ims.id = im.import_source_id AND ims.cid = im.cid)
						JOIN integration_databases idb ON (ims.integration_database_id = idb.id AND ims.cid = idb.cid)
					WHERE
						im.import_data_type_id = ' . CImportDataType::PROPERTIES . '
						AND idb.integration_client_type_id NOT IN ( ' . implode( ',', CIntegrationClientType::$c_arrintMigrateIntegrationClientTypeIds ) . ' )
						AND approved_by IS NOT NULL
						AND approved_on IS NOT NULL
						AND processed_on IS NULL';

		return self::fetchImportBatches( $strSql, $objDatabase );
	}

	public static function fetchUnProcessedAprrovedImportBatchesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ib.*
					FROM
						import_batches ib
						JOIN import_sources ims ON (ims.id = ib.import_source_id AND ims.cid = ib.cid)
					WHERE
						ib.cid = ' . ( int ) $intCid . '
						AND ib.approved_by IS NOT NULL
						AND ib.approved_on IS NOT NULL
						AND ib.processed_on IS NULL';

		return self::fetchImportBatches( $strSql, $objDatabase );
	}

	public static function fetchImportBatchesByImportSourceIdsByCid( $arrintImportSourceIds, $intCid, $objDatabase ) {
	    if( false == valArr( $arrintImportSourceIds ) ) return NULL;

		$strSql = 'SELECT * FROM import_batches WHERE cid =' . ( int ) $intCid . ' AND import_source_id IN (' . implode( ',', $arrintImportSourceIds ) . ');';
		return self::fetchImportBatches( $strSql, $objDatabase );
	}

	public static function fetchCustomUnProcessedUpdateDefaultChargeCodesApprovedImportBatches( $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ib.*
					FROM
						import_batches ib
						JOIN import_sources ims ON (ims.id = ib.import_source_id AND ims.cid = ib.cid)
						JOIN import_mappings im ON (ims.id = im.import_source_id AND ims.cid = im.cid)
						JOIN integration_databases idb ON (ims.integration_database_id = idb.id AND ims.cid = idb.cid)
					WHERE
						im.import_data_type_id = ' . CImportDataType::CHARGE_CODES . '
						AND idb.integration_client_type_id NOT IN ( ' . implode( ',', CIntegrationClientType::$c_arrintMigrateIntegrationClientTypeIds ) . ' )
						AND approved_by IS NOT NULL
						AND approved_on IS NOT NULL
						AND processed_on IS NULL';

		return self::fetchImportBatches( $strSql, $objDatabase );
	}
}
?>