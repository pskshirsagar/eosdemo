<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPackageStatusTypes
 * Do not add any new functions to this class.
 */

class CPackageStatusTypes extends CBasePackageStatusTypes {

	public static function fetchPackageStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPackageStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchPackageStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPackageStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchPublishedPackageStatusTypes( $objDatabase ) {

		$strSql = 'SELECT * FROM package_status_types WHERE is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}

}
?>