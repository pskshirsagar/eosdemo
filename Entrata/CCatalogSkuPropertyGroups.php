<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCatalogSkuPropertyGroups
 * Do not add any new functions to this class.
 */

class CCatalogSkuPropertyGroups extends CBaseCatalogSkuPropertyGroups {

	public static function fetchCatalogSkuPropertyGroupsByApCatalogItemIdByCid( $intApCodeId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApCodeId ) ) return NULL;

		$strSql	= ' SELECT
						cspg.*,
						cs.ap_payee_id
					FROM
						catalog_sku_property_groups cspg
						JOIN ap_catalog_items cs ON ( cspg.cid = cs.cid AND cspg.ap_catalog_item_id = cs.id )
					WHERE
						cspg.cid =' . ( int ) $intCid . '
						AND cs.ap_code_id = ' . ( int ) $intApCodeId . '
					ORDER BY
						cs.ap_payee_id ASC';

		return self::fetchCatalogSkuPropertyGroups( $strSql, $objClientDatabase );
	}

	public static function fetchCatalogSkuPropertyGroupsByApCodesApPayeeIdsByCid( $arrintApCatalogItemIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApCatalogItemIds ) ) return NULL;

		$strSql = 'SELECT
						cspg.*,
						cs.ap_payee_id,
						cs.ap_code_id,
						ac.item_gl_account_id,
						pg.system_code
					FROM
						catalog_sku_property_groups cspg
						JOIN ap_catalog_items cs ON ( cspg.cid = cs.cid AND cspg.ap_catalog_item_id = cs.id )
						JOIN ap_codes ac ON ( ac.cid = cs.cid AND ac.id = cs.ap_code_id )
						JOIN property_groups pg ON ( cspg.cid = pg.cid AND cspg.property_group_id = pg.id )
					WHERE
						cspg.cid =' . ( int ) $intCid . '
						AND cspg.ap_catalog_item_id IN( ' . implode( ',', array_filter( $arrintApCatalogItemIds ) ) . ' )
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL';

		return self::fetchCatalogSkuPropertyGroups( $strSql, $objClientDatabase );
	}

	public static function fetchAssociatedPropertyGroupsAndApCodesByApPayeeAndApCodeIds( $arrmixApPayeeAndApCodeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrmixApPayeeAndApCodeIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT pga.property_id,
						cs.ap_payee_id,
						cs.ap_code_id
					FROM
						catalog_sku_property_groups cspg
						JOIN ap_catalog_items cs ON ( cspg.cid = cs.cid AND cs.id = cspg.ap_catalog_item_id )
						JOIN property_group_associations pga ON (  cspg.property_group_id = pga.property_group_id AND pga.cid = cspg.cid )
					WHERE
						cspg.cid = ' . ( int ) $intCid . '
						AND ( cs.ap_code_id, cs.ap_payee_id ) IN ( ' . implode( ',', $arrmixApPayeeAndApCodeIds ) . ' )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCatalogSkuPropertyGroupsByApCatalogItemIdsByCid( $arrintApCatalogItemIds, $intCid, $objClientDatabase ) {
		if( false == ( $arrintApCatalogItemIds = getIntValuesFromArr( $arrintApCatalogItemIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						catalog_sku_property_groups
					WHERE
						cid =' . ( int ) $intCid . '
						AND ap_catalog_item_id IN( ' . sqlIntImplode( $arrintApCatalogItemIds ) . ' )';

		return self::fetchCatalogSkuPropertyGroups( $strSql, $objClientDatabase );
	}

}
?>