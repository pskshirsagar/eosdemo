<?php

class COrganization extends CBaseOrganization {

	protected $m_strPhoneNumber;
	protected $m_strEmailAddress;
	protected $m_strTaxNumberEncrypted;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strProvince;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_strGroupName;

	protected $m_intPropertyId;

	protected $m_fltGroupBalance;

	protected $m_arrstrOrganizationStatusTypes;

	const ORGANIZATION_ACTIVE = 1;
	const ORGANIZATION_INACTIVE = 2;

	/*
	 * SET Functions
	 */

	public function setStreetLine1( $strStreetLine1 ) {
		$this->m_strStreetLine1 = $strStreetLine1;
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->set( 'm_strStreetLine2', CStrings::strTrimDef( $strStreetLine2, 100, NULL, true ) );
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->set( 'm_strStreetLine3', CStrings::strTrimDef( $strStreetLine3, 100, NULL, true ) );
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function setProvince( $strProvince ) {
		$this->set( 'm_strProvince', CStrings::strTrimDef( $strProvince, 50, NULL, true ) );
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function setGroupName( $strGroupName ) {
		$this->m_strGroupName = CStrings::strTrimDef( $strGroupName, 100, NULL, true );
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true );
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = CStrings::strTrimDef( $strEmailAddress, 240, NULL, true );
	}

	public function setGroupBalance( $fltGroupBalance ) {
		$this->m_fltGroupBalance = $fltGroupBalance;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	/*
	 * GET Functions
	 */

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function getStreetLine3() {
		return $this->m_strStreetLine3;
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function getGroupName() {
		return $this->m_strGroupName;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getBalance() {
		return $this->m_fltGroupBalance;
	}

	public function getFormattedBalance() {

		$strBalance = ( 0 > $this->m_fltGroupBalance ) ? '($' . number_format( abs( $this->m_fltGroupBalance ), 2 ) . ')' : '$' . number_format( $this->m_fltGroupBalance, 2 );

		return $strBalance;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['group_name'] ) ) $this->setGroupName( $arrmixValues['group_name'] );
		if( true == isset( $arrmixValues['phone_number'] ) ) $this->setPhoneNumber( $arrmixValues['phone_number'] );
		if( true == isset( $arrmixValues['email_address'] ) ) $this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['street_line1'] ) ) $this->setStreetLine1( $arrmixValues['street_line1'] );
		if( true == isset( $arrmixValues['street_line2'] ) ) $this->setStreetLine2( $arrmixValues['street_line2'] );
		if( true == isset( $arrmixValues['street_line3'] ) ) $this->setStreetLine3( $arrmixValues['street_line3'] );
		if( true == isset( $arrmixValues['city'] ) ) $this->setCity( $arrmixValues['city'] );
		if( true == isset( $arrmixValues['state_code'] ) ) $this->setStateCode( $arrmixValues['state_code'] );
		if( true == isset( $arrmixValues['province'] ) ) $this->setProvince( $arrmixValues['province'] );
		if( true == isset( $arrmixValues['postal_code'] ) ) $this->setPostalCode( $arrmixValues['postal_code'] );
		if( true == isset( $arrmixValues['country_code '] ) ) $this->setCountryCode( $arrmixValues['country_code '] );
		if( true == isset( $arrmixValues['property_id '] ) ) $this->setPropertyId( $arrmixValues['property_id'] );

	}

	/*
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsArchived() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/*
	 * Other Functions
	 */

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getOrganizationStatusTypes() {

		if( true == valArr( $this->m_arrstrOrganizationStatusTypes ) )
			return $this->m_arrstrOrganizationStatusTypes;

		$this->m_arrstrOrganizationStatusTypes = [
			self::ORGANIZATION_ACTIVE		=> __( 'Active' ),
			self::ORGANIZATION_INACTIVE		=> __( 'Inactive' ),
		];

		return $this->m_arrstrOrganizationStatusTypes;
	}

	public function createCustomerContact() {

		$objCustomerContact = new CCustomerContact();
		$objCustomerContact->setCid( $this->m_intCid );
		$objCustomerContact->setCustomerId( $this->m_intCustomerId );

		return $objCustomerContact;
	}

	public function createOrganizationContract() {

		$objOrganizationContract = new COrganizationContract();
		$objOrganizationContract->setCid( $this->m_intCid );
		$objOrganizationContract->setOrganizationId( $this->m_intId );
		$objOrganizationContract->setCustomerId( $this->m_intCustomerId );

		return $objOrganizationContract;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchGroupBalance( $objGroupLedgersFilter, $objDatabase ) {

		if( false == valObj( $objGroupLedgersFilter, CGroupLedgersFilter::class ) ) {
			trigger_error( 'Group ledger filter failed to load.', E_USER_ERROR );
		}

		$strSql = 'SELECT
						sum( at.transaction_amount_due )
					FROM
						organization_contracts oc
						JOIN cached_leases cl ON ( oc.cid = cl.cid AND oc.id = cl.organization_contract_id )
						JOIN view_ar_transactions at ON ( cl.cid = at.cid AND cl.id = at.lease_id )
						JOIN ledger_filters lf ON ( at.cid = lf.cid AND lf.id = at.ledger_filter_id )
					WHERE
						oc.cid = ' . ( int ) $objGroupLedgersFilter->getCid() . '
						AND oc.organization_id = ' . ( int ) $objGroupLedgersFilter->getOrganizationId() . '
						AND lf.default_ledger_filter_id = ' . CDefaultLedgerFilter::GROUP;

		$arrstrBalance           = fetchData( $strSql, $objDatabase );
		$this->m_fltGroupBalance = sprintf( '%01.2f', $arrstrBalance[0]['sum'] );

		return $this->m_fltGroupBalance;
	}

}
?>