<?php
use Psi\Eos\Entrata\CPropertyMaintenanceTemplates;

class CPropertyMaintenanceLocation extends CBasePropertyMaintenanceLocation {

	protected $m_strName;
	protected $m_intMaintenanceLocationTypeId;
	protected $m_intIsInspectionLocation;
	protected $m_intTotalProblemCount;
	protected $m_strPropertyName;
	protected $m_strIntegrationClientTypeName;
	protected $m_strMaintenanceLocationTypeName;
	protected $m_intSettingsTemplateId;

	public function applyRequestForm( $arrmixRequestForm, $arrmixFormFields = NULL ) {

		parent::applyRequestForm( $arrmixRequestForm, $arrmixFormFields );

    	if( true == valArr( $arrmixFormFields ) ) {
            $arrmixRequestForm = mergeIntersectArray( $arrmixFormFields, $arrmixRequestForm );
        }

        $this->setValues( $arrmixRequestForm, false );

        return;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['name'] ) ) $this->setName( $arrmixValues['name'] );
        if( true == isset( $arrmixValues['maintenance_location_type_id'] ) ) $this->setMaintenanceLocationTypeId( $arrmixValues['maintenance_location_type_id'] );
        if( true == isset( $arrmixValues['total_problem_count'] ) ) $this->setTotalProblemCount( $arrmixValues['total_problem_count'] );
        if( true == isset( $arrmixValues['is_inspection_location'] ) ) $this->setIsInspectionLocation( $arrmixValues['is_inspection_location'] );
        if( true == isset( $arrmixValues['show_on_resident_portal_company_level'] ) ) $this->setShowOnResidentPortalCompanyLevel( $arrmixValues['show_on_resident_portal_company_level'] );
        if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
        if( true == isset( $arrmixValues['maintenance_location_type_name'] ) ) $this->setMaintenanceLocationTypeName( $arrmixValues['maintenance_location_type_name'] );
        if( true == isset( $arrmixValues['settings_template_id'] ) ) $this->setSettingsTemplateId( $arrmixValues['settings_template_id'] );
	    if( true == isset( $arrmixValues['integration_client_type_name'] ) ) $this->setIntegrationClientTypeName( $arrmixValues['integration_client_type_name'] );

        return;
    }

    public function setName( $strName ) {
        $this->m_strName = CStrings::strTrimDef( $strName, 240, NULL, true );
    }

    public function getName() {
        return $this->m_strName;
    }

    public function setMaintenanceLocationTypeId( $intMaintenanceLocationTypeId ) {
    	$this->m_intMaintenanceLocationTypeId = $intMaintenanceLocationTypeId;
    }

    public function getMaintenanceLocationTypeId() {
    	return $this->m_intMaintenanceLocationTypeId;
    }

    public function setTotalProblemCount( $intTotalProblemCount ) {
    	$this->m_intTotalProblemCount = CStrings::strTrimDef( $intTotalProblemCount, 240, NULL, true );
    }

    public function getTotalProblemCount() {
    	return $this->m_intTotalProblemCount;
    }

    public function setIsInspectionLocation( $boolIsInspectionLocation ) {
    	$this->m_intIsInspectionLocation = $boolIsInspectionLocation;
    }

    public function getIsInspectionLocation() {
    	return $this->m_intIsInspectionLocation;
    }

    public function setIntegrationClientTypeName( $strIntegrationClientTypeName ) {
		$this->m_strIntegrationClientTypeName = $strIntegrationClientTypeName;
	}

	public function getIntegrationClientTypeName() {
		return $this->m_strIntegrationClientTypeName;
	}

    public function setShowOnResidentPortalCompanyLevel( $intShowOnResidentPortalCompanyLevel ) {
    	$this->m_intShowOnResidentPortalCompanyLevel = $intShowOnResidentPortalCompanyLevel;
    }

    public function getShowOnResidentPortalCompanyLevel() {
    	return $this->m_intShowOnResidentPortalCompanyLevel;
    }

    public function setPropertyName( $strName ) {
    	$this->m_strPropertyName = CStrings::strTrimDef( $strName, 240, NULL, true );
    }

    public function getPropertyName() {
    	return $this->m_strPropertyName;
    }

    public function setMaintenanceLocationTypeName( $strMaintenanceLocationTypeName ) {
    	$this->m_strMaintenanceLocationTypeName = CStrings::strTrimDef( $strMaintenanceLocationTypeName, 240, NULL, true );
    }

    public function getMaintenanceLocationTypeName() {
    	return $this->m_strMaintenanceLocationTypeName;
    }

    public function setSettingsTemplateId( $intSettingsTemplateId ) {
    	$this->m_intSettingsTemplateId = $intSettingsTemplateId;
    }

    public function getSettingsTemplateId() {
    	return $this->m_intSettingsTemplateId;
    }

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaintenanceLocationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyMaintenanceLocationMaintenanceRequests( $objDatabase ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getMaintenanceLocationId() ) ) {

    		$arrobjMaintenanceRequests = \Psi\Eos\Entrata\CMaintenanceRequests::createService()->fetchMaintenanceRequestsByPropertyIdByMaintenanceLocationIdByCid( $this->getPropertyId(), $this->getMaintenanceLocationId(), $this->getCid(), $objDatabase );

    		if( true == valArr( $arrobjMaintenanceRequests ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', __( 'Maintenance request(s) exists for the maintenance location. ' ) ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function valPropertyMaintenanceLocationMaintenanceTemplates( $objDatabase ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getMaintenanceLocationId() ) ) {

    		$arrobjPropertyMaintenanceTemplates = CPropertyMaintenanceTemplates::createService()->fetchPropertyMaintenanceTemplatesByCidByPropertyIdByMaintenanceLocationId( $this->getCid(), $this->getPropertyId(), $this->getMaintenanceLocationId(), $objDatabase );

    		if( true == valArr( $arrobjPropertyMaintenanceTemplates ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_status_type_id', __( 'Maintenance template(s) exists for the maintenance location. ' ) ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function valPropertyMaintenanceLocationMaintenanceProblems( $objDatabase ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getMaintenanceLocationId() ) ) {

    		$arrobjPropertyMaintenanceLocationProblems = \Psi\Eos\Entrata\CPropertyMaintenanceLocationProblems::createService()->fetchPropertyMaintenanceLocationProblemsByCidByPropertyIdByLocationId( $this->getCid(), $this->getPropertyId(), $this->getMaintenanceLocationId(), $objDatabase );

    		if( true == valArr( $arrobjPropertyMaintenanceLocationProblems ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', __( 'Maintenance problem(s) exists for the maintenance location. ' ) ) );
    		}
    	}

    	return $boolIsValid;
    }

	public function valPropertyMaintenanceUnitTypeLocations( $objDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getMaintenanceLocationId() ) ) {

			$intCountPropertyMaintenanceUnitTypeLocations = \Psi\Eos\Entrata\CPropertyUnitMaintenanceLocations::createService()->fetchSimpleUnitTypeLocationsCountByCidByPropertyIdByMaintenanceLocationId( $this->getCid(), $this->getPropertyId(), $this->getMaintenanceLocationId(), $objDatabase );
			if( true == valId( $intCountPropertyMaintenanceUnitTypeLocations ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', __( 'Maintenance location associated with unit type. ' ) ) );
			}
		}

		return $boolIsValid;
	}

    public function valPropertyLocation( $objDatabase, $strErrorMsg ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getMaintenanceLocationId() ) ) {

    		$arrobjPropertyMaintenanceTemplates = CPropertyMaintenanceTemplates::createService()->fetchPropertyMaintenanceTemplatesByCidByPropertyIdByMaintenanceLocationId( $this->getCid(), $this->getPropertyId(), $this->getMaintenanceLocationId(), $objDatabase );

    		if( true == valArr( $arrobjPropertyMaintenanceTemplates ) ) {
    			$boolIsValid = false;

    			if( false == valStr( $strErrorMsg ) ) {
    				$strErrorMsg = __( 'Maintenance template(s) exists for the maintenance location: ' ) . ' ' . $this->getName();
    			} else {
    				$strErrorMsg .= ',' . $this->getName();
    			}

    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_status_type_id', $strErrorMsg ) );
    		}
    	}

    	return $strErrorMsg;
    }

    public function validate( $strAction, $objDatabase = NULL, $strErrorMsg = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valPropertyMaintenanceLocationMaintenanceTemplates( $objDatabase );
            	break;

            case VALIDATE_DELETE:
				$boolIsValid &= $this->valPropertyMaintenanceUnitTypeLocations( $objDatabase );
            	break;

            case 'validate_delete_new':
            	$boolIsValid &= $this->valPropertyMaintenanceLocationMaintenanceRequests( $objDatabase );
            	$boolIsValid &= $this->valPropertyMaintenanceLocationMaintenanceTemplates( $objDatabase );
            	break;

           	case 'validate_property_location':
           		$strErrorMsg = $this->valPropertyLocation( $objDatabase, $strErrorMsg );
           		return $strErrorMsg;
           		break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>