<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertySellingPoints
 * Do not add any new functions to this class.
 */

class CPropertySellingPoints extends CBasePropertySellingPoints {

	public static function fetchPropertySellingPointsByCidByPropertyId( $intCid, $intPropertyId, $objDatabase, $strSellingPointsSortBy = 'ASC' ) {

		$strSql = 'SELECT
						psp.*,
						util_get_system_translated( \'name\', pspc.name, pspc.details ) AS category_name,
						ra.company_media_file_id,
						cmf.fullsize_uri AS full_size_uri,
						a.name AS amenity_name
					FROM
						property_selling_points AS psp
						JOIN property_selling_point_categories AS pspc ON ( psp.property_selling_point_category_id = pspc.id )
						LEFT JOIN rate_associations AS ra ON ( ra.id = psp.rate_association_id AND ra.cid = psp.cid )
						LEFT JOIN company_media_files AS cmf ON ( cmf.id = ra.company_media_file_id AND cmf.cid = ra.cid )
						LEFT JOIN amenities AS a ON ( ra.ar_origin_reference_id = a.id AND ra.cid = a.cid)
					WHERE
						psp.cid = ' . ( int ) $intCid . '
						AND psp.property_id = ' . ( int ) $intPropertyId;

		if( NULL != $strSellingPointsSortBy ) {
			$strSql .= ' ORDER BY pspc.name ' . addslashes( $strSellingPointsSortBy ) . ', psp.id ASC';
		} else {
			$strSql .= ' ORDER BY psp.order_num';
		}

		return self::fetchPropertySellingPoints( $strSql, $objDatabase );
	}

	public static function fetchPropertySellingPointByCidByPropertyIdById( $intCid, $intPropertyId, $intPropertySellingPointId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						property_selling_points
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND id = ' . ( int ) $intPropertySellingPointId;

		return self::fetchPropertySellingPoint( $strSql, $objDatabase );
	}

	public static function fetchPropertySellingPointByRateAssociationIdByCidByPropertyId( $intRateAssociationId, $intCid, $intPropertyId, $objDatabase ) {
		if( false == valId( $intRateAssociationId ) || false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_selling_points
					WHERE
						rate_association_id = ' . ( int ) $intRateAssociationId . '
						AND cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchPropertySellingPoint( $strSql, $objDatabase );
	}

	public static function fetchPropertySellingPointsByRateAssociationIdsByCidByPropertyId( $arrintRateAssociationIds, $intCid, $intPropertyId, $objDatabase ) {
		if( false == valArr( $arrintRateAssociationIds ) || false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_selling_points
					WHERE
						rate_association_id IN ( ' . implode( ',', $arrintRateAssociationIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchPropertySellingPoints( $strSql, $objDatabase );
	}

}
?>