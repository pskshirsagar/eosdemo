<?php

class CReportType extends CBaseReportType {

	const SYSTEM			= 1;
	const SAP 				= 2;
	const CONTROLLER		= 3;
	const CUSTOM			= 4;
	const SISENSE			= 5;

	public static $c_arrintAllReportTypes		= [ self::SYSTEM, self::SAP, self::CONTROLLER, self::CUSTOM, self::SISENSE ];
	public static $c_arrintLibraryReportTypes	= [ self::SYSTEM, self::SAP, self::CUSTOM, self::SISENSE ];
	public static $c_arrintSystemReportTypes	= [ self::SYSTEM, self::CUSTOM ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
			default:
				// default
				break;
		}

		return $boolIsValid;
	}

}
