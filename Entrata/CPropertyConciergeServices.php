<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyConciergeServices
 * Do not add any new functions to this class.
 */

class CPropertyConciergeServices extends CBasePropertyConciergeServices {

	public static function fetchPropertyConciergeServiceByCompanyConciergeServiceIdByPropertyIdByCid( $intCompanyConciergeServiceId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						pcs.*
					FROM
						property_concierge_services pcs,
						company_concierge_services ccs
					WHERE
						 ccs.id = pcs.company_concierge_service_id
						 AND pcs.cid = ccs.cid
						 AND company_concierge_service_id = ' . ( int ) $intCompanyConciergeServiceId . '
						 AND pcs.property_id = ' . ( int ) $intPropertyId . '
						 AND pcs.cid = ' . ( int ) $intCid . ' LIMIT 1';

		return self::fetchPropertyConciergeService( $strSql, $objDatabase );
	}

	public function fetchPropertyConciergeServiceCountByCompanyConciergeServiceIdByPropertyIdByCid( $intCompanyConciergeServiceId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intCompanyConciergeServiceId ) || false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						COUNT( pcs.id ) as count
					FROM
						property_concierge_services pcs,
						company_concierge_services ccs
					WHERE
						 ccs.id = pcs.company_concierge_service_id
						 AND pcs.cid = ccs.cid
						 AND company_concierge_service_id = ' . ( int ) $intCompanyConciergeServiceId . '
						 AND pcs.property_id = ' . ( int ) $intPropertyId . '
						 AND pcs.cid = ' . ( int ) $intCid;

		$arrintCount = fetchData( $strSql, $objDatabase );

		return ( 0 == $arrintCount[0]['count'] ) ? NULL : $arrintCount;
	}

}

?>