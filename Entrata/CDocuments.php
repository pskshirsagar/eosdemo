<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocuments
 * Do not add any new functions to this class.
 */

class CDocuments extends CBaseDocuments {

	public static function fetchCustomChecklistDocumentsByCid( $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) return NULL;

		$strSql = '	SELECT
					    id,
						name
					FROM
					    documents
					WHERE
					    cid = ' . ( int ) $intCid . '
					    AND document_type_id = ' . ( int ) CDocumentType::MERGE_DOCUMENT . '
					    AND deleted_on IS NULL
					    AND deleted_by IS NULL
					    AND name IS NOT NULL
					ORDER BY
					    order_num';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchDocumentsByPropertyIdsByDocumentAssociationTypeIdByCid( $arrintPropertyIds, $intDocumentAssociationTypeId, $intCid, $objDatabase, $boolFetchChildDocuments = true ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.*,
						pga.property_id
					 FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
						AND d.archived_on IS NULL
						AND d.deleted_by IS NULL';

		if( CDocumentAssociationType::EMPLOYEE_PORTAL == $intDocumentAssociationTypeId ) {
			$strSql .= ' AND d.document_type_id <> ' . CDocumentType::FILES;
		}

		$strSql .= ' ORDER BY
						d.order_num,
						d.id;';

		$arrobjParentDocuments = self::fetchDocuments( $strSql, $objDatabase );

		if( false == $boolFetchChildDocuments ) {
			return $arrobjParentDocuments;
		}

		$objStdObjectContainer = new stdClass();
		$objStdObjectContainer->m_arrobjDocuments = $arrobjParentDocuments;

		if( true == valArr( $arrobjParentDocuments ) ) {
			self::fetchRecursiveChildrenDocumentsByParentDocumentIdsByCid( array_keys( $arrobjParentDocuments ), $objStdObjectContainer, $intCid, $objDatabase );
		} else {
			return NULL;
		}

		// Rekey Documents
		$arrobjDocuments = $objStdObjectContainer->m_arrobjDocuments;
		$arrobjRekeyedDocuments = [];

		foreach( $arrobjDocuments as $objDocument ) {
			$arrobjRekeyedDocuments[$objDocument->getId()] = $objDocument;
		}

		return $arrobjRekeyedDocuments;
	}

	public static function fetchEmployeePortalDocumentsByPropertyIdsByDocumentAssociationTypeIdByCid( $arrintPropertyIds, $intDocumentAssociationTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT * 
					FROM ( 
					SELECT
						DISTINCT ON ( d.name, d.order_num, d.id ) d.*,
						pga.property_id,
						count( pga.property_id ) OVER ( PARTITION BY d.id ) AS properties_count
					 FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
						AND d.archived_on IS NULL
						AND d.deleted_by IS NULL
						AND d.document_type_id = ' . CDocumentType::FILES . '
						) AS d
					ORDER BY
					    ' . $objDatabase->getCollateSort( 'd.name', true ) . ',
						d.order_num,
						d.id';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchDocumentsByDocumentTypeByDocumentInclusionTypeIdByCid( $arrintDocumentTypeIds, $intDocumentInclusionTypeId, $intCid, $objDatabase, $boolFetchChildDocuments = true ) {

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.* FROM
						documents d
					WHERE
						d.document_type_id IN(' . implode( ',', $arrintDocumentTypeIds ) . ')
						AND ( d.document_sub_type_id IS NULL OR d.document_sub_type_id NOT IN (' . CDocumentSubType::RESIDENT_PORTAL_MOVE_IN_SURVEY . ',' . CDocumentSubType::RESIDENT_PORTAL_MOVE_OUT_SURVEY . ',' . CDocumentSubType::RESIDENT_PORTAL_MAINTENANCE_SURVEY . ' ) )
						AND d.archived_on IS NULL
						AND d.cid = ' . ( int ) $intCid . '
						AND d.document_inclusion_type_id = ' . ( int ) $intDocumentInclusionTypeId . '
						AND	d.is_published = 1
						AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL';

		if( false == $boolFetchChildDocuments ) {
			$strSql .= ' AND d.document_id IS NULL';
		}

		$strSql .= '	AND d.deleted_by IS NULL
					ORDER BY
						d.order_num,
						d.id;';

		$arrobjParentDocuments = self::fetchDocuments( $strSql, $objDatabase );

		if( false == $boolFetchChildDocuments ) {
			return $arrobjParentDocuments;
		}

		$objStdObjectContainer = new stdClass();
		$objStdObjectContainer->m_arrobjDocuments = $arrobjParentDocuments;

		if( true == valArr( $arrobjParentDocuments ) ) {
			self::fetchRecursiveChildrenDocumentsByParentDocumentIdsByCid( array_keys( $arrobjParentDocuments ), $objStdObjectContainer, $intCid, $objDatabase );
		} else {
			return NULL;
		}

		// Rekey Documents
		$arrobjDocuments = $objStdObjectContainer->m_arrobjDocuments;
		$arrobjRekeyedDocuments = [];

		foreach( $arrobjDocuments as $objDocument ) {
			$arrobjRekeyedDocuments[$objDocument->getId()] = $objDocument;
		}

		return $arrobjRekeyedDocuments;
	}

	public static function fetchCustomDocumentsByDocumentInclusionTypeIdByCid( $intDocumentInclusionTypeId, $intCid, $objDatabase, $boolFetchChildDocuments = true ) {
		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.* FROM
						documents d
					WHERE
						d.archived_on IS NULL
						AND d.cid = ' . ( int ) $intCid . '
						AND d.document_inclusion_type_id = ' . ( int ) $intDocumentInclusionTypeId;

		if( false == $boolFetchChildDocuments ) {
			$strSql .= ' AND document_id IS NULL';
		}

		$strSql .= '	AND d.deleted_by IS NULL
					ORDER BY
						d.order_num,
						d.id;';

		$arrobjParentDocuments = self::fetchDocuments( $strSql, $objDatabase );

		if( false == $boolFetchChildDocuments ) {
			return $arrobjParentDocuments;
		}

		$objStdObjectContainer = new stdClass();
		$objStdObjectContainer->m_arrobjDocuments = $arrobjParentDocuments;

		if( true == valArr( $arrobjParentDocuments ) ) {
			self::fetchRecursiveChildrenDocumentsByParentDocumentIdsByCid( array_keys( $arrobjParentDocuments ), $objStdObjectContainer, $intCid, $objDatabase );
		} else {
			return NULL;
		}

		// Rekey Documents
		$arrobjDocuments = $objStdObjectContainer->m_arrobjDocuments;
		$arrobjRekeyedDocuments = [];

		foreach( $arrobjDocuments as $objDocument ) {
			$arrobjRekeyedDocuments[$objDocument->getId()] = $objDocument;
		}

		return $arrobjRekeyedDocuments;
	}

	public static function fetchDocumentsByDocumentInclusionTypeIdByDocumentIdByCid( $intDocumentInclusionTypeId, $intDocumentId, $intCid, $objDatabase, $boolFetchChildDocuments = true ) {
		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.*
					FROM
						documents d
					WHERE
						d.archived_on IS NULL
						AND d.cid = ' . ( int ) $intCid . '
						AND d.document_inclusion_type_id = ' . ( int ) $intDocumentInclusionTypeId . '
						AND d.id = ' . ( int ) $intDocumentId . '
						AND d.deleted_by IS NULL
					ORDER BY
						d.order_num,
						d.id;';

		$arrobjParentDocuments = self::fetchDocuments( $strSql, $objDatabase );

		if( false == $boolFetchChildDocuments ) {
			return $arrobjParentDocuments;
		}

		$objStdObjectContainer = new stdClass();
		$objStdObjectContainer->m_arrobjDocuments = $arrobjParentDocuments;

		if( true == valArr( $arrobjParentDocuments ) ) {
			self::fetchRecursiveChildrenDocumentsByParentDocumentIdsByCid( array_keys( $arrobjParentDocuments ), $objStdObjectContainer, $intCid, $objDatabase );
		} else {
			return NULL;
		}

		// Rekey Documents
		$arrobjDocuments = $objStdObjectContainer->m_arrobjDocuments;
		$arrobjRekeyedDocuments = [];

		foreach( $arrobjDocuments as $objDocument ) {
			$arrobjRekeyedDocuments[$objDocument->getId()] = $objDocument;
		}

		return $arrobjRekeyedDocuments;
	}

	public static function fetchDocumentsByDocumentTypeByOwnerIdByCid( $arrintDocumentTypeIds, $intOwnerId, $intCid, $objDatabase, $boolFetchChildDocuments = true ) {

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.*
					FROM
						documents d,
						owner_documents od
					WHERE
						d.id = od.document_id
						AND d.cid = od.cid
						AND d.document_type_id IN(' . implode( ',', $arrintDocumentTypeIds ) . ')
						AND ( d.document_sub_type_id IS NULL OR d.document_sub_type_id NOT IN (' . CDocumentSubType::RESIDENT_PORTAL_MOVE_IN_SURVEY . ',' . CDocumentSubType::RESIDENT_PORTAL_MOVE_OUT_SURVEY . ',' . CDocumentSubType::RESIDENT_PORTAL_MAINTENANCE_SURVEY . ' ) )
						AND d.archived_on IS NULL
						AND d.cid = ' . ( int ) $intCid . '
						AND od.ap_payee_id = ' . ( int ) $intOwnerId . '
						AND	d.is_published = 1';

		if( false == $boolFetchChildDocuments ) {
			$strSql .= ' AND d.document_id IS NULL';
		}

		$strSql .= ' AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL
					ORDER BY
						d.order_num,
						d.id;';

		$arrobjParentDocuments = self::fetchDocuments( $strSql, $objDatabase );

		if( false == $boolFetchChildDocuments ) {
			return $arrobjParentDocuments;
		}

		$objStdObjectContainer = new stdClass();
		$objStdObjectContainer->m_arrobjDocuments = $arrobjParentDocuments;

		if( true == valArr( $arrobjParentDocuments ) ) {
			self::fetchPublishRecursiveChildrenDocumentsByParentDocumentIdsByCid( $intCid, array_keys( $arrobjParentDocuments ), $objStdObjectContainer, $objDatabase );
		} else {
			return NULL;
		}

		// Rekey Documents
		$arrobjDocuments = $objStdObjectContainer->m_arrobjDocuments;
		$arrobjRekeyedDocuments = [];

		foreach( $arrobjDocuments as $objDocument ) {
			$arrobjRekeyedDocuments[$objDocument->getId()] = $objDocument;
		}

		return $arrobjRekeyedDocuments;

	}

	public static function fetchDocumentsByOwnerIdsByCid( $arrintOwnerIds, $intCid, $objDatabase, $boolFetchChildDocuments = true ) {

		if( false == valArr( $arrintOwnerIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.*
					FROM
						documents d,
						owner_documents od
					WHERE
						d.id = od.document_id
						AND d.cid = od.cid
						AND d.archived_on IS NULL
						AND d.cid = ' . ( int ) $intCid . '
						AND od.ap_payee_id IN ( ' . implode( ',', $arrintOwnerIds ) . ' )
						AND d.deleted_by IS NULL
					ORDER BY
						d.order_num,
						d.id;';

		$arrobjParentDocuments = self::fetchDocuments( $strSql, $objDatabase );

		if( false == $boolFetchChildDocuments ) {
			return $arrobjParentDocuments;
		}

		$objStdObjectContainer = new stdClass();
		$objStdObjectContainer->m_arrobjDocuments = $arrobjParentDocuments;

		if( true == valArr( $arrobjParentDocuments ) ) {
			self::fetchRecursiveChildrenDocumentsByParentDocumentIdsByCid( array_keys( $arrobjParentDocuments ), $objStdObjectContainer, $intCid, $objDatabase );
		} else {
			return NULL;
		}

		// Rekey Documents
		$arrobjDocuments = $objStdObjectContainer->m_arrobjDocuments;
		$arrobjRekeyedDocuments = [];

		foreach( $arrobjDocuments as $objDocument ) {
			$arrobjRekeyedDocuments[$objDocument->getId()] = $objDocument;
		}

		return $arrobjRekeyedDocuments;
	}

	public static function fetchRecursiveChildrenDocumentsByParentDocumentIdsByCid( $arrintParentDocumentIds, $objStdObjectContainer, $intCid, $objDatabase ) {

		if( false == valArr( $arrintParentDocumentIds ) ) return $objStdObjectContainer;

		$strSql = 'SELECT
						d.*
					FROM
						documents d
					WHERE
						document_id IN ( ' . implode( ',', $arrintParentDocumentIds ) . ' )
						AND d.archived_on IS NULL
						AND d.cid = ' . ( int ) $intCid . '
						AND d.deleted_by IS NULL
					ORDER BY
						d.order_num';

		$arrobjChildrenDocuments = self::fetchDocuments( $strSql, $objDatabase );

		if( true == valArr( $arrobjChildrenDocuments ) ) {
			$objStdObjectContainer->m_arrobjDocuments = array_merge( $objStdObjectContainer->m_arrobjDocuments, $arrobjChildrenDocuments );
			self::fetchRecursiveChildrenDocumentsByParentDocumentIdsByCid( array_keys( $arrobjChildrenDocuments ), $objStdObjectContainer, $intCid, $objDatabase );
		} else {
			return $objStdObjectContainer->m_arrobjDocuments;
		}
	}

	public static function fetchChildDocumentsByParentDocumentIdByCid( $intParentDocumentId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						d.*
					FROM
						documents d
					WHERE
						d.document_id = ' . ( int ) $intParentDocumentId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL';

		$arrobjDocuments = self::fetchDocuments( $strSql, $objDatabase );

		$objStdObjectContainer = new stdClass();
		$objStdObjectContainer->m_arrobjDocuments = $arrobjDocuments;

		if( true == valArr( $arrobjDocuments ) ) {
			self::fetchRecursiveChildrenDocumentsByParentDocumentIdsByCid( array_keys( $arrobjDocuments ), $objStdObjectContainer, $intCid, $objDatabase );
		} else {
			return NULL;
		}

		$arrobjDocuments = $objStdObjectContainer->m_arrobjDocuments;
		$arrobjRekeyedDocuments = [];

		foreach( $arrobjDocuments as $objDocument ) {
			$arrobjRekeyedDocuments[$objDocument->getId()] = $objDocument;
		}

		return $arrobjRekeyedDocuments;
	}

	public static function fetchPublishedDocumentsByDocumentTypeIdsByDocumentAssociationTypeIdByPropertyIdByCid( $arrintDocumentTypeIds, $intDocumentAssociationTypeId, $intPropertyId, $boolIsLoggedIn, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.*
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						d.document_type_id IN(' . implode( ',', $arrintDocumentTypeIds ) . ')
						AND ( d.document_sub_type_id IS NULL OR d.document_sub_type_id NOT IN (' . CDocumentSubType::RESIDENT_PORTAL_MOVE_IN_SURVEY . ',' . CDocumentSubType::RESIDENT_PORTAL_MOVE_OUT_SURVEY . ',' . CDocumentSubType::RESIDENT_PORTAL_MAINTENANCE_SURVEY . ' ) )
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND	d.is_published = 1
						AND d.archived_on IS NULL
						AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL';

		if( CDocumentAssociationType::RESIDENT_PORTAL == $intDocumentAssociationTypeId && false === $boolIsLoggedIn ) {
			$strSql	.= ' AND d.require_login <> 1 ';
		}

		$strSql .= ' ORDER BY d.order_num, d.id;';

		$arrobjParentDocuments = self::fetchDocuments( $strSql, $objDatabase );

		$objStdObjectContainer = new stdClass();
		$objStdObjectContainer->m_arrobjDocuments = $arrobjParentDocuments;

		if( true == valArr( $arrobjParentDocuments ) ) {
			self::fetchPublishRecursiveChildrenDocumentsByParentDocumentIdsByCid( $intCid, array_keys( $arrobjParentDocuments ), $objStdObjectContainer, $objDatabase );
		} else {
			return NULL;
		}

		// Rekey Documents
		$arrobjDocuments = $objStdObjectContainer->m_arrobjDocuments;
		$arrobjRekeyedDocuments = [];

		foreach( $arrobjDocuments as $objDocument ) {
			$arrobjRekeyedDocuments[$objDocument->getId()] = $objDocument;
		}

		return $arrobjRekeyedDocuments;
	}

	public static function fetchPublishedRequiredLoginDocumentsByDocumentTypeIdsByDocumentAssociationTypeIdByPropertyIdByCid( $arrintDocumentTypeIds, $intDocumentAssociationTypeId, $intPropertyId, $boolIsLoggedIn, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.*
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						d.document_type_id IN(' . implode( ',', $arrintDocumentTypeIds ) . ')
						AND ( d.document_sub_type_id IS NULL OR d.document_sub_type_id NOT IN (' . CDocumentSubType::RESIDENT_PORTAL_MOVE_IN_SURVEY . ',' . CDocumentSubType::RESIDENT_PORTAL_MOVE_OUT_SURVEY . ',' . CDocumentSubType::RESIDENT_PORTAL_MAINTENANCE_SURVEY . ' ) )
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND	d.is_published = 1
						AND	d.require_login = 1
						AND d.archived_on IS NULL
						AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL';

		if( CDocumentAssociationType::RESIDENT_PORTAL == $intDocumentAssociationTypeId && false === $boolIsLoggedIn ) {
			$strSql	.= ' AND d.require_login <> 1 ';
		}

		$strSql .= ' ORDER BY d.order_num, d.id;';

		$arrobjParentDocuments = self::fetchDocuments( $strSql, $objDatabase );

		$objStdObjectContainer = new stdClass();
		$objStdObjectContainer->m_arrobjDocuments = $arrobjParentDocuments;

		if( true == valArr( $arrobjParentDocuments ) ) {
			self::fetchPublishRecursiveChildrenDocumentsByParentDocumentIdsByCid( $intCid, array_keys( $arrobjParentDocuments ), $objStdObjectContainer, $objDatabase );
		} else {
			return NULL;
		}

		// Rekey Documents
		$arrobjDocuments = $objStdObjectContainer->m_arrobjDocuments;
		$arrobjRekeyedDocuments = [];

		foreach( $arrobjDocuments as $objDocument ) {
			$arrobjRekeyedDocuments[$objDocument->getId()] = $objDocument;
		}

		return $arrobjRekeyedDocuments;
	}

	public static function fetchPublishRecursiveChildrenDocumentsByParentDocumentIdsByCid( $intCid, $arrintParentDocumentIds, $objStdObjectContainer, $objDatabase, $boolIsCheckRedirectURL = false ) {

		if( false == valArr( $arrintParentDocumentIds ) ) return $objStdObjectContainer;

		$strSql = 'SELECT
						d.*
					FROM
						documents d
					WHERE
						document_id IN ( ' . implode( ',', $arrintParentDocumentIds ) . ' )
						AND d.cid = ' . ( int ) $intCid . '
						AND	d.is_published = 1
						AND d.archived_on IS NULL
						AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL';

		if( true == $boolIsCheckRedirectURL ) {
			$strSql .= ' AND d.redirect_url IS NOT NULL';
		}

		$strSql .= ' ORDER BY
						d.order_num';

		$arrobjChildrenDocuments = CDocuments::fetchDocuments( $strSql, $objDatabase );

		if( true == valArr( $arrobjChildrenDocuments ) ) {
			$objStdObjectContainer->m_arrobjDocuments = array_merge( $objStdObjectContainer->m_arrobjDocuments, $arrobjChildrenDocuments );
			self::fetchPublishRecursiveChildrenDocumentsByParentDocumentIdsByCid( $intCid, array_keys( $arrobjChildrenDocuments ), $objStdObjectContainer, $objDatabase, $boolIsCheckRedirectURL );
		} else {
			return $objStdObjectContainer->m_arrobjDocuments;
		}
	}

	public static function fetchPublishRecursiveChildrenDocumentsByDocumentTypeIdsByDocumentSubTypeIdsByParentDocumentIdsByCid( $intCid, $arrintDocumentTypeIds, $arrintDocumentSubTypeIds, $arrintParentDocumentIds, $objStdObjectContainer, $objDatabase ) {

		if( false == valArr( $arrintParentDocumentIds ) ) return $objStdObjectContainer;

		$strSql = 'SELECT
						d.*
					FROM
						documents d
					WHERE
						document_id IN ( ' . implode( ',', $arrintParentDocumentIds ) . ' )
						AND d.document_type_id IN (' . implode( ',', $arrintDocumentTypeIds ) . ')
						AND d.document_sub_type_id IN (' . implode( ',', $arrintDocumentSubTypeIds ) . ')
						AND d.cid = ' . ( int ) $intCid . '
						AND	d.is_published = 1
						AND d.archived_on IS NULL
						AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL
					ORDER BY
						d.order_num';

		$arrobjChildrenDocuments = self::fetchDocuments( $strSql, $objDatabase );

		if( true == valArr( $arrobjChildrenDocuments ) ) {
			$objStdObjectContainer->m_arrobjDocuments = array_merge( $objStdObjectContainer->m_arrobjDocuments, $arrobjChildrenDocuments );
			self::fetchPublishRecursiveChildrenDocumentsByDocumentTypeIdsByDocumentSubTypeIdsByParentDocumentIdsByCid( $intCid, $arrintDocumentTypeIds, $arrintDocumentSubTypeIds, array_keys( $arrobjChildrenDocuments ), $objStdObjectContainer, $objDatabase );
		} else {
			return $objStdObjectContainer->m_arrobjDocuments;
		}
	}

	public static function fetchPublishedDocumentsByDocumentTypeIdByDocumentAssociationTypeIdByPropertyIdsByCid( $arrintDocumentTypeIds, $intDocumentAssociationTypeId, $arrintPropertyIds, $boolIsLoggedIn, $intCid, $objDatabase, $boolIsCheckRedirectURL = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.*,
						pga.property_id
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.document_type_id IN (' . implode( ',', $arrintDocumentTypeIds ) . ')
						AND ( d.document_sub_type_id IS NULL OR d.document_sub_type_id NOT IN (' . CDocumentSubType::RESIDENT_PORTAL_MOVE_IN_SURVEY . ',' . CDocumentSubType::RESIDENT_PORTAL_MOVE_OUT_SURVEY . ',' . CDocumentSubType::RESIDENT_PORTAL_MAINTENANCE_SURVEY . ' ) )
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
						AND pga.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND d.is_published = 1
						AND d.archived_on IS NULL
						AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL';

		if( CDocumentAssociationType::RESIDENT_PORTAL == $intDocumentAssociationTypeId && false === $boolIsLoggedIn ) {
			$strSql	.= ' AND d.require_login <> 1 ';
		}

		if( true == $boolIsCheckRedirectURL ) {
			$strSql .= ' AND d.redirect_url IS NOT NULL ';
		}

		$strSql .= ' ORDER BY d.order_num, d.id;';

		$arrobjParentDocuments = self::fetchDocuments( $strSql, $objDatabase );

		$objStdObjectContainer = new stdClass();
		$objStdObjectContainer->m_arrobjDocuments = $arrobjParentDocuments;

		if( true == valArr( $arrobjParentDocuments ) ) {
			self::fetchPublishRecursiveChildrenDocumentsByParentDocumentIdsByCid( $intCid, array_keys( $arrobjParentDocuments ), $objStdObjectContainer, $objDatabase, $boolIsCheckRedirectURL );
		} else {
			return NULL;
		}

		// Rekey Documents
		$arrobjDocuments = $objStdObjectContainer->m_arrobjDocuments;
		$arrobjRekeyedDocuments = [];

		foreach( $arrobjDocuments as $objDocument ) {
			$arrobjRekeyedDocuments[$objDocument->getId()] = $objDocument;
		}

		return $arrobjRekeyedDocuments;
	}

	public static function fetchPublishedParentDocumentsByDocumentTypeIdByDocumentAssociationTypeIdByPropertyIdsByCid( $arrintDocumentTypeIds, $intDocumentAssociationTypeId, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						d.id as parent_document_id,
						d.title,
						d.name,
						d.activate_on,
						d.deactivate_on,
						pga.property_id
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						d.document_type_id IN (' . implode( ',', $arrintDocumentTypeIds ) . ')
						AND ( d.document_sub_type_id IS NULL OR d.document_sub_type_id NOT IN (' . CDocumentSubType::RESIDENT_PORTAL_MOVE_IN_SURVEY . ',' . CDocumentSubType::RESIDENT_PORTAL_MOVE_OUT_SURVEY . ',' . CDocumentSubType::RESIDENT_PORTAL_MAINTENANCE_SURVEY . ' ) )
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND pga.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND d.is_published = 1
						AND d.archived_on IS NULL
						AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL
						ORDER BY
							d.order_num,
							d.id';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchPublishedDocumentsByDocumentTypeIdsByDocumentSubTypeIdsByDocumentAssociationTypeIdByPropertyIdsByCid( $arrintDocumentTypeIds, $arrintDocumentSubTypeIds, $intDocumentAssociationTypeId, $arrintPropertyIds, $boolIsLoggedIn, $intCid, $objDatabase, $boolIsFromResidentWorks = false, $boolIsCreateWorkOrderSurvey = false ) {

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.*
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.document_type_id IN (' . implode( ',', $arrintDocumentTypeIds ) . ')
						AND d.document_sub_type_id IN (' . implode( ',', $arrintDocumentSubTypeIds ) . ')
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
						AND pga.property_id IN (' . implode( ',', $arrintPropertyIds ) . ' )
						AND d.is_published = 1
						AND d.archived_on IS NULL
						AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL';

		if( true == $boolIsCreateWorkOrderSurvey ) {
			$strSql	.= ' AND ( d.details ->> \'create_work_order_survey\' )::boolean = true ';
		} else {
			$strSql	.= ' AND ( d.details ->> \'create_work_order_survey\' IS NULL OR ( d.details ->> \'create_work_order_survey\' )::boolean = false ) ';
		}

		if( CDocumentAssociationType::RESIDENT_PORTAL == $intDocumentAssociationTypeId && false === $boolIsLoggedIn ) {
			$strSql	.= ' AND d.require_login <> 1 ';
		}
		$strSql .= ' ORDER BY d.order_num, d.id;';

		$arrobjParentDocuments = self::fetchDocuments( $strSql, $objDatabase );

		$objStdObjectContainer = new stdClass();
		$objStdObjectContainer->m_arrobjDocuments = $arrobjParentDocuments;

		if( true == valArr( $arrobjParentDocuments ) ) {
			if( true == isset( $boolIsFromResidentWorks ) && true == $boolIsFromResidentWorks ) {
				self::fetchPublishRecursiveChildrenDocumentsByDocumentTypeIdsByDocumentSubTypeIdsByParentDocumentIdsByCid( $intCid, $arrintDocumentTypeIds, $arrintDocumentSubTypeIds, array_keys( $arrobjParentDocuments ), $objStdObjectContainer, $objDatabase );
			} else {
				self::fetchPublishRecursiveChildrenDocumentsByParentDocumentIdsByCid( $intCid, array_keys( $arrobjParentDocuments ), $objStdObjectContainer, $objDatabase );
			}

		} else {
			return NULL;
		}

		// Rekey Documents
		$arrobjDocuments = $objStdObjectContainer->m_arrobjDocuments;
		$arrobjRekeyedDocuments = [];

		foreach( $arrobjDocuments as $objDocument ) {
			$arrobjRekeyedDocuments[$objDocument->getId()] = $objDocument;
		}

		return $arrobjRekeyedDocuments;
	}

	public static function fetchActiveDocumentsByDocumentTypeIdByDocumentSubTypeIdByDocumentAssociationTypeIdBySystemCodeByPropertyIdsByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intDocumentAssociationTypeId, $strFileTypeSystemCode, $strPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.*, pga.property_group_id
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid )
	                    JOIN file_types ft ON ( d.file_type_id = ft.id AND ft.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
						AND ft.system_code = \'' . $strFileTypeSystemCode . '\'
						AND pga.property_id IN (' . $strPropertyIds . ' )
						AND d.is_published = 1
						AND d.archived_on IS NULL
						AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActivePropertyDocumentsByDocumentTypeIdByDocumentSubTypeIdByDocumentAssociationTypeIdBySystemCodeByPropertyIdsByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intDocumentAssociationTypeId, $strFileTypeSystemCode, $strPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id, pga.property_id ) d.*, pga.property_group_id, pga.property_id
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid )
	                    JOIN file_types ft ON ( d.file_type_id = ft.id AND ft.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
						AND ft.system_code = \'' . $strFileTypeSystemCode . '\'
						AND pga.property_id IN (' . $strPropertyIds . ' )
						AND d.is_published = 1
						AND d.archived_on IS NULL
						AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublishedDocumentsByDocumentTypeIdByDocumentAssociationTypeIdByUserTypeIdByCid( $arrintDocumentTypeIds, $intDocumentAssociationTypeId, $strUserTypeId, $boolIsLoggedIn, $strSearchFilter, $strSearchContent, $intCid, $objDatabase ) {

		$strSearchSql = 'AND d.title ILIKE \'%' . addslashes( $strSearchContent ) . '%\'';

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.*,
						pga.property_id
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )';

		if( 'content' == $strSearchFilter && false == empty( $strSearchContent ) ) {
			$strSql .= 'left join document_parameters dp on ( d.id = dp.document_id AND d.cid = dp.cid )';

			$strSearchSql = 'AND ( ( regexp_replace( regexp_replace( dp.value, E\'(?x)<[^>]*?(\s alt \s* = \s* ([\\\'" ] ) ( [^>]*? ) \2 ) [^>]*? >\', E\'\3\',\'g\' ), E\'( ?x )( < [^>]*? > )\',\' \' ,\'g\' ) ILIKE \'%' . addslashes( $strSearchContent ) . '%\' AND dp.key = \'title\' ) OR
							( regexp_replace(regexp_replace( dp.value, E\'(?x)<[^>]*?(\s alt \s* = \s* ([\\\'"] ) ( [^>]*? ) \2 ) [^>]*? >\', E\'\3\',\'g\' ), E\'( ?x )(< [^>]*? > )\',\' \',\'g\' ) ILIKE \'%' . addslashes( $strSearchContent ) . '%\' AND dp.key = \'text\' ) OR
							( regexp_replace(regexp_replace( dp.value, E\'(?x)<[^>]*?(\s alt \s* = \s* ([\\\'"] ) ( [^>]*? ) \2 ) [^>]*? >\', E\'\3\',\'g\' ), E\'( ?x )( < [^>]*? > )\',\' \',\'g\' ) ILIKE \'%' . addslashes( $strSearchContent ) . '%\' AND dp.key = \'title_text\' ) OR
							( regexp_replace(regexp_replace( dp.value, E\'(?x)<[^>]*?(\s alt \s* = \s* ([\\\'"] ) ( [^>]*? ) \2 ) [^>]*? >\', E\'\3\',\'g\' ), E\'( ?x )( < [^>]*? > )\',\' \',\'g\' ) ILIKE \'%' . addslashes( $strSearchContent ) . '%\' AND dp.key = \'file\' ) )';

		} elseif( 'all' == $strSearchFilter && false == empty( $strSearchContent ) ) {
			$strSql .= 'left join document_parameters dp on ( d.id = dp.document_id AND d.cid = dp.cid )';

			$strSearchSql = 'AND ( ( d.title ILIKE \'%' . addslashes( $strSearchContent ) . '%\' ) OR ( ( dp.value ILIKE \'%' . addslashes( $strSearchContent ) . '%\' AND dp.key = \'title\' ) OR
							( regexp_replace(regexp_replace(dp.value, E\'(?x)<[^>]*?(\s alt \s* =\s* ( [\\\'"] ) ( [^>]*? ) ) [^>]*? >\', E\'\3\',\'g\' ), E\'( ?x )(< [^>]*? >)\',\' \',\'g\') ILIKE \'%' . addslashes( $strSearchContent ) . '%\' AND dp.key = \'text\') OR
							( regexp_replace(regexp_replace(dp.value, E\'(?x)<[^>]*?(\s alt \s* =\s* ( [\\\'"] ) ( [^>]*? ) \2 ) [^>]*? >\', E\'\3\',\'g\' ), E\'( ?x )( < [^>]*? >)\',\' \',\'g\') ILIKE \'%' . addslashes( $strSearchContent ) . '%\' AND dp.key = \'title_text\' ) OR
							( regexp_replace(regexp_replace(dp.value, E\'(?x)<[^>]*?(\s alt \s* =\s* ( [\\\'"] ) ( [^>]*? ) \2 ) [^>]*? >\', E\'\3\',\'g\' ), E\'( ?x )( < [^>]*? >)\',\' \',\'g\') ILIKE \'%' . addslashes( $strSearchContent ) . '%\' AND dp.key = \'file\' ) ) )';
		}

		$strSql .= 'WHERE
					d.document_type_id IN ( ' . implode( ',', $arrintDocumentTypeIds ) . ' )
					AND ( d.document_sub_type_id IS NULL OR d.document_sub_type_id NOT IN (' . CDocumentSubType::RESIDENT_PORTAL_MOVE_IN_SURVEY . ',' . CDocumentSubType::RESIDENT_PORTAL_MOVE_OUT_SURVEY . ',' . CDocumentSubType::RESIDENT_PORTAL_MAINTENANCE_SURVEY . ' ) )
					AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
					AND d.cid = ' . ( int ) $intCid . '';

		if( false == empty( $strUserTypeId ) ) {
			$strSql .= ' AND pga.property_id IN ( SELECT cup.property_id FROM view_company_user_properties cup WHERE cup.company_user_id = ' . ( int ) $strUserTypeId . ' AND cup.cid = ' . ( int ) $intCid . ' )';
		}

		$strSql	.= $strSearchSql;
		$strSql	.= 'AND d.is_published = 1
					AND d.archived_on IS NULL
					AND ( d.activate_on IS NULL OR d.activate_on <= now() )
					AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
					AND d.deleted_by IS NULL';

		if( CDocumentAssociationType::RESIDENT_PORTAL == $intDocumentAssociationTypeId && false === $boolIsLoggedIn ) {
			$strSql	.= ' AND d.require_login <> 1 ';
		}
		$strSql .= ' ORDER BY d.order_num, d.id;';

		$arrobjParentDocuments = self::fetchDocuments( $strSql, $objDatabase );

		$objStdObjectContainer = new stdClass();
		$objStdObjectContainer->m_arrobjDocuments = $arrobjParentDocuments;

		// Rekey Documents
		$arrobjDocuments = $objStdObjectContainer->m_arrobjDocuments;
		$arrobjRekeyedDocuments = [];

		if( false == is_null( $arrobjDocuments ) ) {
			foreach( $arrobjDocuments as $objDocument ) {
				$arrobjRekeyedDocuments[$objDocument->getId()] = $objDocument;
			}
		}
		return $arrobjRekeyedDocuments;
	}

	public static function fetchPublishedDocumentIdByDocumentTypeIdsByDocumentSubTypeIdsByDocumentAssociationTypeIdByPropertyIdsByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intDocumentAssociationTypeId, $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( d.id ) d.id
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
						AND pga.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND d.is_published = 1
						AND d.archived_on IS NULL
						AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL
					ORDER BY
						d.id
						LIMIT 1';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintResponse[0]['id'] ) ) {
			return $arrintResponse[0]['id'];
		} else {
			return 0;
		}
	}

	public static function fetchPublishedDocumentByDocumentTypeIdsByDocumentSubTypeIdsByDocumentAssociationTypeIdByPropertyIdsByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intDocumentAssociationTypeId, $arrintPropertyIds, $intCid, $objDatabase, $boolIsExcludeArchived = false, $boolIsCreateWorkOrderSurvey = false ) {

		$strSql = 'SELECT
						d.*
					 FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND pga.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND d.is_published = 1
						AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL';

					if( true == $boolIsExcludeArchived ) {
						$strSql .= ' AND d.archived_on IS NULL';
					}
					if( true == $boolIsCreateWorkOrderSurvey ) {
						$strSql	.= ' AND ( d.details ->> \'create_work_order_survey\' )::boolean = true ';
					} else {
						$strSql	.= ' AND ( d.details ->> \'create_work_order_survey\' IS NULL OR ( d.details ->> \'create_work_order_survey\' )::boolean = false ) ';
					}

					$strSql .= ' ORDER BY d.order_num
								LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchPublishedDocumentsCountByDocumentTypeIdsByDocumentSubTypeIdsByDocumentAssociationTypeIdByPropertyIdsByCid( $arrintDocumentTypeIds, $arrintDocumentSubTypeIds, $intDocumentAssociationTypeId, $arrintPropertyIds, $boolIsLoggedIn, $intCid, $objDatabase, $boolIsCreateWorkOrderSurvey = false ) {

		$strSql = 'SELECT
						COUNT( d.id )
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						d.document_type_id IN (' . implode( ',', $arrintDocumentTypeIds ) . ')
						AND d.document_sub_type_id IN (' . implode( ',', $arrintDocumentSubTypeIds ) . ')
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND pga.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND d.is_published = 1
						AND d.archived_on IS NULL
						AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL';

		if( true == $boolIsCreateWorkOrderSurvey ) {
			$strSql	.= ' AND ( d.details ->> \'create_work_order_survey\' )::boolean = true ';
		} else {
			$strSql	.= ' AND ( d.details ->> \'create_work_order_survey\' IS NULL OR ( d.details ->> \'create_work_order_survey\' )::boolean = false ) ';
		}

		if( CDocumentAssociationType::RESIDENT_PORTAL == $intDocumentAssociationTypeId && false === $boolIsLoggedIn ) {
			$strSql	.= ' AND d.require_login <> 1 ';
		}

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return $arrintResponse[0]['count'];
	}

	public static function fetchApplicationDocumentByIdByCid( $intDocumentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						documents
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intDocumentId . '
					ORDER BY
						order_num';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchSwitchDocumentByDirectionByCid( $objDocument, $strDirection, $intDocumentAssociationTypeId, $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strDirection = trim( $strDirection );
		if( 'up' != $strDirection && 'down' != $strDirection ) return false;
		if( false == valObj( $objDocument, 'CDocument' ) ) return false;

		$strDirectionIndicator = ( 'up' == $strDirection ) ? ' < ' : ' > ';
		$strOrderBy = ( 'up' == $strDirection ) ? ' DESC ' : ' ASC ';

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id )
						d.*
					FROM
						documents d
						LEFT OUTER JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						LEFT OUTER JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT OUTER JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						( pga.property_id IN(' . implode( ',', $arrintPropertyIds ) . ') OR d.document_id IS NOT NULL )
						AND d.cid = ' . ( int ) $objDocument->getCid() . '
						AND d.id != ' . ( int ) $objDocument->getId() . '
						AND d.deleted_by IS NULL
						AND ( pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . ' OR d.document_id IS NOT NULL )
						AND d.order_num ' . $strDirectionIndicator . ' ' . $objDocument->getOrderNum() . ' ';

		$strSql .= ( true == is_numeric( $objDocument->getDocumentId() ) ) ? ' AND d.document_id = ' . $objDocument->getDocumentId() . ' ' : ' AND d.document_id IS NULL ';
		$strSql .= ' ORDER BY d.order_num ' . $strOrderBy . ', d.id LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchSwitchOwnerDocumentByDirectionByCid( $objDocument, $strDirection, $arrintOwnerIds, $objDatabase ) {
		if( false == valArr( $arrintOwnerIds ) ) return NULL;

		$strDirection = trim( $strDirection );
		if( 'up' != $strDirection && 'down' != $strDirection ) return false;
		if( false == valObj( $objDocument, 'CDocument' ) ) return false;

		$strDirectionIndicator = ( 'up' == $strDirection ) ? ' < ' : ' > ';
		$strOrderBy = ( 'up' == $strDirection ) ? ' DESC ' : ' ASC ';

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id )
						d.*
					FROM
						documents d
						LEFT OUTER JOIN owner_documents od ON ( d.id = od.document_id AND d.cid = od.cid )
					WHERE
						( od.ap_payee_id IN(' . implode( ',', $arrintOwnerIds ) . ') OR d.document_id IS NOT NULL )
						AND d.cid = ' . ( int ) $objDocument->getCid() . '
						AND d.id != ' . ( int ) $objDocument->getId() . '
						-- AND ( d.document_id IS NOT NULL )
						AND d.order_num ' . $strDirectionIndicator . ' ' . $objDocument->getOrderNum() . ' ';

		$strSql .= ( true == is_numeric( $objDocument->getDocumentId() ) ) ? ' AND d.document_id = ' . ( int ) $objDocument->getDocumentId() . ' ' : ' AND d.document_id IS NULL ';
		$strSql .= ' ORDER BY d.order_num ' . $strOrderBy . ', d.id LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchSwitchDocumentByDirectionByDocumentInclusionTypeIdByCid( $objDocument, $strDirection, $intDocumentInclusionTypeId, $objDatabase ) {

		$strDirection = trim( $strDirection );
		if( 'up' != $strDirection && 'down' != $strDirection ) return false;
		if( false == valObj( $objDocument, 'CDocument' ) ) return false;

		$strDirectionIndicator = ( 'up' == $strDirection ) ? ' < ' : ' > ';
		$strOrderBy = ( 'up' == $strDirection ) ? ' DESC ' : ' ASC ';

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id )
						d.*
					FROM
						documents d
					WHERE
						-- d.document_id IS NOT NULL
						-- AND
						d.cid = ' . ( int ) $objDocument->getCid() . '
						AND document_inclusion_type_id = ' . ( int ) $intDocumentInclusionTypeId . '
						AND d.id != ' . ( int ) $objDocument->getId() . '
						AND d.order_num ' . $strDirectionIndicator . ' ' . $objDocument->getOrderNum() . ' ';

		$strSql .= ( true == is_numeric( $objDocument->getDocumentId() ) ) ? ' AND d.document_id = ' . ( int ) $objDocument->getDocumentId() . ' ' : ' AND d.document_id IS NULL ';
		$strSql .= ' ORDER BY d.order_num ' . $strOrderBy . ', d.id LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchDisplacedDocumentByDirectionByCid( $strDirection, $objDocument, $objWebsite, $objDatabase ) {

		$strDirection = trim( $strDirection );
		if( 'up' != $strDirection && 'down' != $strDirection ) return false;
		if( false == valObj( $objDocument, 'CDocument' ) ) return false;

		$strDirectionIndicator = ( 'up' == $strDirection ) ? ' < ' : ' > ';
		$strOrderBy = ( 'up' == $strDirection ) ? ' DESC ' : ' ASC ';

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id )
						d.*
					FROM
						documents d
						LEFT OUTER JOIN website_documents wd ON ( d.id = wd.document_id AND d.cid = wd.cid )
					WHERE
						wd.website_id = ' . ( int ) $objWebsite->getId() . '
						AND d.cid = ' . ( int ) $objDocument->getCid() . '
						AND d.id != ' . ( int ) $objDocument->getId() . '
						and d.deleted_by IS NULL
						AND d.order_num ' . $strDirectionIndicator . ' ' . $objDocument->getOrderNum() . ' ';

		$strSql .= ( true == is_numeric( $objDocument->getDocumentId() ) ) ? ' AND d.document_id = ' . ( int ) $objDocument->getDocumentId() . ' ' : ' AND d.document_id IS NULL ';
		$strSql .= ' ORDER BY d.order_num ' . $strOrderBy . ', d.id LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchDocumentsByIdsByCid( $arrintDocumentIds, $intCid, $objDatabase, $boolIsIncludeDeletedDocuments = false ) {
		if( false == valArr( $arrintDocumentIds ) ) return NULL;

		$strSql = 'SELECT * FROM documents WHERE cid = ' . ( int ) $intCid . ' AND id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )';

		if( false == $boolIsIncludeDeletedDocuments ) {
			$strSql .= ' AND deleted_by IS NULL';
		}

		$strSql .= ' ORDER BY order_num';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchDocumentByPropertyIdByDocumentIdByCid( $intPropertyId, $intDocumentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( d.id ) d.*, d1.*
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
						LEFT JOIN documents d1 ON ( d.id = d1.document_id )
					 WHERE
						d.id = ' . ( int ) $intDocumentId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND ( d.activate_on IS NULL OR d.activate_on <= NOW() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on > NOW() )
						';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchDocumentByIdByWebsiteIdByCid( $intDocumentId, $intWebsiteId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (d.id)
						d.*,
						wd.show_in_mobile_portal
					FROM
						documents d,
						website_documents wd
					WHERE
						d.id = wd.document_id
						AND d.cid = wd.cid
						AND wd.website_id = ' . ( int ) $intWebsiteId . '
						AND d.id = ' . ( int ) $intDocumentId . '
						AND d.cid = ' . ( int ) $intCid;

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchDocumentByTitleByWebsiteIdByCid( $strTitle, $intWebsiteId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (d.id)
						d.*
					FROM
						documents d,
						website_documents wd
					WHERE
						d.id = wd.document_id
						AND d.cid = wd.cid
						AND wd.website_id = ' . ( int ) $intWebsiteId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.title = \'' . addslashes( $strTitle ) . '\'';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchDocumentBySeoUriByCid( $strSeoUri, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (d.id)
						d.*
					FROM
						documents d
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.seo_uri = \'' . addslashes( $strSeoUri ) . '\'
						AND d.deleted_by IS NULL
						AND d.archived_on IS NULL 
						AND d.is_published = 1
					LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchParentDocumentBySeoUriByCid( $strSeoUri, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (d.id)
						d.*
					FROM
						documents d LEFT JOIN documents d1 ON d.id = d1.document_id
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.seo_uri = \'' . addslashes( $strSeoUri ) . '\'
						AND d.deleted_by IS NULL
						AND d.archived_on IS NULL 
						AND d.is_published = 1
					LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchDocumentByNameByDocumentTypeIdByDocumentSubTypeIdByCid( $strName, $intDocumentTypeId, $intDocumentSubTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (d.id)
						d.*
					FROM
						documents d
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND lower( d.name ) = \'' . ( string ) addslashes( str_replace( '-', ' ', $strName ) ) . '\'
						AND d.deleted_by IS NULL
						AND d.is_published = 1
						AND d.archived_on IS NULL
						AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
					LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchDocumentByNameByCid( $strName, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (d.id)
						d.*
					FROM
						documents d
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND lower( d.name ) = \'' . ( string ) addslashes( str_replace( '-', ' ', $strName ) ) . '\'
						AND d.deleted_by IS NULL
						AND d.is_published = 1
						AND d.archived_on IS NULL
					LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchDocumentByTitleByCid( $strTitle, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (d.id)
						d.*
					FROM
						documents d
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND lower( d.title ) = lower(\'' . ( string ) addslashes( $strTitle ) . '\')
						AND d.deleted_by IS NULL
						AND d.is_published = 1
						AND d.archived_on IS NULL
					LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchTenantTechDocumentByNameByCid( $strName, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (d.id)
						d.*
					FROM
						documents d
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.document_type_id = ' . CDocumentType::MERGE_DOCUMENT . '
						AND d.document_sub_type_id = ' . CDocumentSubType::DOCUMENT_TEMPLATE . '
						AND d.transmission_vendor_id = ' . CTransmissionVendor::TENANT_TECH . '
						AND lower( d.name ) = lower(\'' . ( string ) addslashes( $strName ) . '\')
						AND d.deleted_by IS NULL
						AND d.is_published = 1
						AND d.archived_on IS NULL
					LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchMilitaryDocumentByFileNameByCid( $strFileName, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (d.id)
						d.*
					FROM
						documents d
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND lower( d.file_name ) = lower(\'' . ( string ) addslashes( $strFileName ) . '\')
						AND d.deleted_by IS NULL
						AND d.is_published = 1
						AND d.is_system = 1
					LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchCustomDocumentByIdByCid( $intDocumentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (d.id)
						d.*
					FROM
						documents d
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.id = ' . ( int ) $intDocumentId . '
					LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchPublishedDocumentsByWebsiteIdByCid( $intWebsiteId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intWebsiteId ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON (d.order_num, d.id)
						d.*,
						wd.show_in_primary_nav,
						wd.show_in_secondary_nav,
						wd.show_in_tertiary_nav
					FROM
						documents d,
						website_documents wd
					WHERE
						d.id = wd.document_id
						AND d.cid = wd.cid
						AND wd.website_id = ' . ( int ) $intWebsiteId . '
						AND wd.cid = ' . ( int ) $intCid . '
						AND ( d.activate_on IS NULL OR d.activate_on <= NOW() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on > NOW() )
						AND d.is_published = 1
						AND d.archived_on IS NULL
						AND d.deleted_by IS NULL
						ORDER BY
							d.order_num,
							d.id';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchPublishedMobileWebsiteDocumentsByWebsiteIdByCid( $intWebsiteId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intWebsiteId ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON (d.order_num, d.id)
						d.*
					FROM
						documents d,
						website_documents wd
					WHERE
						d.id = wd.document_id
						AND d.cid = wd.cid
						AND wd.website_id = ' . ( int ) $intWebsiteId . '
						AND wd.cid = ' . ( int ) $intCid . '
						AND wd.show_in_mobile_portal = 1
						AND ( d.activate_on IS NULL OR d.activate_on <= NOW() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on > NOW() )
						AND d.is_published = 1
						AND d.archived_on IS NULL
						AND d.deleted_by IS NULL
						ORDER BY
							d.order_num,
							d.id';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function determineHasPublishedOnlineApplicationByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count(id)
					FROM (
						SELECT
							DISTINCT ON (d.id)
							d.id
						FROM
							documents d
							JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
							JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
							JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
						WHERE
							pgd.property_id = ' . ( int ) $intPropertyId . '
							AND d.cid = ' . ( int ) $intCid . '
							AND pgd.document_association_type_id = ' . ( int ) CDocumentAssociationType::ONLINE_APPLICATION . '
							AND ( d.activate_on IS NULL OR d.activate_on <= NOW() )
							AND ( d.deactivate_on IS NULL OR d.deactivate_on > NOW() )
								AND d.is_published = 1
						 ) AS sub_query1';

		$arrintDocumentCount = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintDocumentCount[0]['count'] ) && 0 < $arrintDocumentCount[0]['count'] ) {
			return true;
		}

		return false;
	}

	public static function fetchPublishedActiveDocumentsByPropertyIdByDocumentTypeIdByDocumentAssociationTypeIdByDocumentSubTypeIdsByCid( $intPropertyId, $intDocumentTypeId, $arrintDocumentSubTypeIds, $intCid, $objDatabase, $arrintLeaseIntervalIds = [] ) {

		$strSql = 'SELECT
						d.*
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						pga.property_id = ' . ( int ) $intPropertyId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.archived_on IS NULL
						AND d.is_published = 1
						AND d.deleted_by IS NULL
						AND ( d.activate_on <= NOW() OR d.activate_on IS NULL )
						AND ( d.deactivate_on >= NOW() OR d.deactivate_on IS NULL )';

		if( false == in_array( CDocumentSubType::BLUE_MOON_LEASE_AGREEMENT, $arrintDocumentSubTypeIds ) ) {
			$strSql .= ' AND d.file_name IS NOT NULL
						 AND d.file_path IS NOT NULL ';
		}

		if( true == valArr( $arrintLeaseIntervalIds ) ) {
			$strSql .= ' AND ( \'{ ' . implode( ',', $arrintLeaseIntervalIds ) . ' }\' && d.lease_interval_type_ids )';
		}

		$strSql .= ' ORDER BY
							' . $objDatabase->getCollateSort( 'd.name', true ) . ',
							pgd.order_num,
							pgd.id';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchPublishedActiveDocumentsByPropertyIdByDocumentTypeIdByDocumentSubTypeIdByCid( $intPropertyId, $intDocumentTypeId, $intDocumentSubTypeId, $intCid, $objDatabase, $strTargetLocale = NULL ) {

		$strSqlWhereLocale = '';
		if( true == valStr( $strTargetLocale ) ) {
			$strSqlWhereLocale = ' AND d.locale_code = \'' . $strTargetLocale . '\' ';
		}

		$strSql = 'SELECT
						d.*
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						pga.property_id = ' . ( int ) $intPropertyId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
						AND d.archived_on IS NULL
						AND d.is_published = 1
						AND d.deleted_by IS NULL
						AND ( d.activate_on <= NOW() OR d.activate_on IS NULL )
						AND ( d.deactivate_on >= NOW() OR d.deactivate_on IS NULL )' . $strSqlWhereLocale;

		$strSql .= ' ORDER BY
							' . $objDatabase->getCollateSort( 'd.name', true ) . ',
							pgd.order_num,
							pgd.id';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchDocumentsByDocumentTypeIdByDocumentSubTypeIdByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT DISTINCT ON ( d.order_num, d.id ) d.*
					FROM
						documents d
					WHERE
						d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.deleted_by IS NULL
					ORDER BY
						d.order_num,
						d.id;';

		return self::fetchDocuments( $strSql, $objDatabase );

	}

	public static function fetchCustomDocumentsByDocumentTypeIdByDocumentSubTypeIdByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.id,d.created_on::date as created_on, title
					FROM
						documents d
					WHERE
						d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.deleted_by IS NULL
						AND d.is_published = 1
					ORDER BY
						d.order_num,
						d.id
					LIMIT 1;';
		return self::fetchDocument( $strSql, $objDatabase );

	}

	public static function fetchNSFCustomDocumentByDocumentTypeIdByCid( $intDocumentTypeId, $intCid, $objDatabase ) {
		if( false == valId( $intDocumentTypeId ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.id,d.created_on::date as created_on, title
					FROM
						documents d
						LEFT JOIN file_types ft ON( ft.cid = d.cid AND ft.id = d.file_type_id  )
					WHERE
						d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND ( d.document_sub_type_id = ' . CDocumentSubType::NSF_NOTICE . ' OR ( d.document_sub_type_id = ' . CDocumentSubType::DOCUMENT_TEMPLATE . ' AND ft.system_code = \'' . CFileType::SYSTEM_CODE_NSF . '\' ) )
						AND d.cid = ' . ( int ) $intCid . '
						AND d.deleted_by IS NULL
						AND d.is_published = 1
					ORDER BY
						d.order_num,
						d.id
					LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchDocumentsByPropertyIdsByIdByCid( $arrintPropertyIds, $intDocumentId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valId( $intDocumentId ) ) return NULL;

		$strSql = 'SELECT
						d.*, pga.property_id
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						pga.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND d.id = ' . ( int ) $intDocumentId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.is_published = 1
						AND d.deleted_on IS NULL
						AND d.deleted_by IS NULL
						AND d.archived_on IS NULL
						AND ( d.activate_on <= NOW() OR d.activate_on IS NULL )
						AND ( d.deactivate_on >= NOW() OR d.deactivate_on IS NULL )';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchCustomDocumentsCreatedOnByDocumentTypeIdByDocumentSubTypeIdByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.id,d.created_on::date as created_on
					FROM
						documents d
					WHERE
						d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.deleted_by IS NULL
						AND d.is_published = 1
					ORDER BY
						d.order_num,
						d.id
					LIMIT 1;';
		$arrmixDocuments = fetchData( $strSql, $objDatabase );
		if( true == isset ( $arrmixDocuments[0]['created_on'] ) ) {
			return $arrmixDocuments[0]['created_on'];
		} else {
			return NULL;
		}
	}

	public static function fetchCustomPublishedDocumentsByDocumentTemplateId( $intDocumentTemplateId, $objClientDatabase ) {

		$strSql = 'SELECT
						d.*
					FROM
						documents d
					WHERE
						d.document_template_id = ' . ( int ) $intDocumentTemplateId . '
						AND d.archived_on IS NULL
						AND d.deleted_by IS NULL
					ORDER BY
						d.order_num,
						d.id';

		return self::fetchDocuments( $strSql, $objClientDatabase );
	}

	public static function fetchParentDocumentsByDocumentIdByCid( $intChildrenDocumentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.*
					FROM
						documents d
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.id = ' . ( int ) $intChildrenDocumentId . '
						AND d.deleted_by IS NULL
					ORDER BY
						d.order_num';

		$arrobjParentDocuments = self::fetchDocuments( $strSql, $objDatabase );

		$objStdObjectContainer = new stdClass();
		$objStdObjectContainer->m_arrobjDocuments = $arrobjParentDocuments;

		if( true == valArr( $arrobjParentDocuments ) ) {
			$objStdObjectContainer->m_arrobjDocuments = array_merge( $objStdObjectContainer->m_arrobjDocuments, $arrobjParentDocuments );
			self::fetchRecursiveParentDocumentsByDocumentIdByCid( $objStdObjectContainer, $intCid, $objDatabase );
		} else {
			return NULL;
		}

		// Rekey Documents
		$arrobjDocuments = $objStdObjectContainer->m_arrobjDocuments;
		$arrobjRekeyedDocuments = [];

		foreach( $arrobjDocuments as $objDocument ) {
			// if( $objDocument->getId() != $intChildrenDocumentId ) {
			$arrobjRekeyedDocuments[$objDocument->getId()] = $objDocument;
			// }
		}

		return $arrobjRekeyedDocuments;
	}

	public static function fetchRecursiveParentDocumentsByDocumentIdByCid( $objStdObjectContainer, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						d.*
					FROM
						documents d
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.id = ' . ( int ) $objStdObjectContainer->m_arrobjDocuments[0]->getDocumentId() . '
						AND d.deleted_by IS NULL
					ORDER BY
						d.order_num';

		$arrobjParentDocuments = self::fetchDocument( $strSql, $objDatabase );

		if( true == valObj( $arrobjParentDocuments, 'CDocument' ) ) {
			array_unshift( $objStdObjectContainer->m_arrobjDocuments, $arrobjParentDocuments );
			self::fetchRecursiveParentDocumentsByDocumentIdByCid( $objStdObjectContainer, $intCid, $objDatabase );
		} else {
			return $objStdObjectContainer->m_arrobjDocuments;
		}
		return false;
	}

	public static function fetchDocumentByWebsiteIdByDocumentNameByCid( $intWebsiteId, $strDocumentName, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						d.*
					FROM
						documents d,
						website_documents wd
					WHERE
						d.id = wd.document_id
						AND d.cid = wd.cid
						AND d.deleted_by IS NULL
						AND	CASE WHEN 1 <= ( SELECT
											     count(d.id)
											 FROM
												 documents d,
												 website_documents wd
											 WHERE
												 d.id = wd.document_id
												 AND d.cid = wd.cid
												 AND d.deleted_by IS NULL
												 AND d.document_id IS NULL
												 AND wd.website_id = ' . ( int ) $intWebsiteId . '
												 AND wd.cid = ' . ( int ) $intCid . '
												 AND regexp_replace( d.name,\'[^0-9a-zA-Z]+\', \'\', \'g\') ILIKE \'' . \Psi\CStringService::singleton()->preg_replace( '/[^0-9a-zA-Z]+/', '', addslashes( $strDocumentName ) ) . '\' )
								 THEN
	                            	 d.document_id IS NULL
	                             ELSE
	                            	 d.document_id IS NOT NULL
	                        END
						AND wd.website_id = ' . ( int ) $intWebsiteId . '
						AND d.is_published = 1
						AND wd.cid = ' . ( int ) $intCid . '
						AND regexp_replace( d.name,\'[^0-9a-zA-Z]+\', \'\', \'g\') ILIKE \'' . \Psi\CStringService::singleton()->preg_replace( '/[^0-9a-zA-Z]+/', '', addslashes( $strDocumentName ) ) . '\'
					LIMIT 1';
		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchDocumentByWebsiteIdBySeoUriByCid( $intWebsiteId, $strSeoUri, $intCid, $objDatabase ) {

		$strSql = sprintf( 'SELECT
							   d.*
						   FROM
							   documents d,
							   website_documents wd
						   WHERE
							   d.id = wd.document_id
							   AND d.cid = wd.cid
							   AND d.deleted_by IS NULL
							   AND d.is_published = 1
							   AND wd.website_id = %d
							   AND wd.cid = %d
							   AND d.seo_uri = \'%s\'
						   LIMIT 1', ( int ) $intWebsiteId, ( int ) $intCid, addslashes( iconv( 'UTF-8', 'UTF-8//IGNORE', $strSeoUri ) ) );

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchConflictingWebsiteDocumentCount( $objConflictingDocument, $objDatabase ) {

		$strFrom = ' documents d
					JOIN website_documents wd ON ( d.id = wd.document_id AND d.cid = wd.cid )';

		$strWhere = ' WHERE
						d.deleted_by IS NULL
						AND wd.website_id = ' . ( int ) $objConflictingDocument->getWebsiteId() . '
						AND wd.cid = ' . ( int ) $objConflictingDocument->getCid() . '
						AND d.seo_uri = \'' . addslashes( $objConflictingDocument->getSeoUri() ) . '\'
						AND d.id <>' . ( int ) $objConflictingDocument->getId() . '
					LIMIT 1';

		return self::fetchRowCount( $strWhere, $strFrom, $objDatabase );
	}

	public static function fetchConflictingDocumentCount( $objConflictingDocument, $boolIsValidateSeoUri, $objDatabase, $boolCheckDeletedBy = false, $intIsArchived = NULL, $intDocumentInclusionType = 0 ) {

		$intId = ( false == is_numeric( $objConflictingDocument->getId() ) ) ? 0 : $objConflictingDocument->getId();

		$strWhereSql = 'WHERE
							cid = ' . ( int ) $objConflictingDocument->getCid() . '
							AND id <> ' . ( int ) $intId;

		if( CDocumentInclusionType::COMPANY == $intDocumentInclusionType ) {
			$strWhereSql .= ' AND document_inclusion_type_id = ' . ( int ) $intDocumentInclusionType;
		}

		if( true == $boolIsValidateSeoUri ) {
			$strWhereSql .= ' AND seo_uri = \'' . ( string ) addslashes( $objConflictingDocument->getSeoUri() ) . '\'';
		} else {
			$strWhereSql .= ' AND lower(name) = \'' . ( string ) addslashes( \Psi\CStringService::singleton()->strtolower( $objConflictingDocument->getName() ) ) . '\'';
		}

		if( true == $boolCheckDeletedBy ) {
			$strWhereSql .= ' AND deleted_by IS NULL ';
		}

		if( false == is_null( $intIsArchived ) ) {
			$strWhereSql .= ' AND archived_on IS NOT NULL';
		}

		$strWhereSql .= ' LIMIT 1 ';

		return self::fetchDocumentCount( $strWhereSql, $objDatabase );
	}

	public static function fetchConflictingPropertyGroupDocumentCount( $objConflictingDocument, $boolIsValidateSeoUri, $objDatabase, $boolCheckDeletedBy = false, $intDocumentInclusionType = 0 ) {

		$intId = ( false == is_numeric( $objConflictingDocument->getId() ) ) ? 0 : $objConflictingDocument->getId();

		$strFrom = ' documents d ';

		if( CDocumentInclusionType::OWNERS == $intDocumentInclusionType ) {
			$strFrom .= ' INNER JOIN owner_documents od ON d.id = od.document_id AND  od.cid = d.cid ';
		} else {
			$strFrom .= ' INNER JOIN  property_group_documents pgd ON d.id = pgd.document_id AND  pgd.cid = d.cid  ';
		}

		$strWhereSql = ' WHERE
										d.cid = ' . ( int ) $objConflictingDocument->getCid() . '
										AND d.id <>' . ( int ) $intId;

		if( true == $boolIsValidateSeoUri ) {
			$strWhereSql .= ' AND d.seo_uri = \'' . ( string ) addslashes( $objConflictingDocument->getSeoUri() ) . '\'';
		} else {
			$strWhereSql .= ' AND lower(d.name) = \'' . ( string ) addslashes( \Psi\CStringService::singleton()->strtolower( $objConflictingDocument->getName() ) ) . '\'';
		}

		if( true == $boolCheckDeletedBy ) {
			$strWhereSql .= ' AND d.deleted_by IS NULL ';
		}

		$strWhereSql .= ' LIMIT 1 ';

		return self::fetchDocumentCount( $strWhereSql, $objDatabase, $strFrom );
	}

	public static function fetchLeaseDocumentsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase, $boolIsOptional = NULL, $boolIsPublished = true, $boolHasExternalKey = NULL, $boolIncludeArchived = false ) {

		$strSql = 'SELECT
						d.*,
						da.external_key,
						da.id as document_addenda_id,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional,
						da.is_default_selected,
						da.is_household,
						da.order_num as document_addenda_order_num,
						da.last_sync_on as last_sync_on
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid )
					WHERE
						da.master_document_id = ' . ( int ) $intDocumentId . '
						AND d.cid = ' . ( int ) $intCid;
		if( true === $boolIsOptional ) {
			$strSql .= ' AND da.is_optional = 1 ';
		} elseif( false === $boolIsOptional ) {
			$strSql .= ' AND da.is_optional <> 1 ';
		}

		if( true === $boolIsPublished ) {
			$strSql .= ' AND d.is_published = 1 ';
		} elseif( false === $boolIsPublished ) {
			$strSql .= ' AND d.is_published <> 1 ';
		}

		if( true === $boolHasExternalKey ) {
			$strSql .= ' AND da.external_key IS NOT NULL ';
		} elseif( false === $boolHasExternalKey ) {
			$strSql .= ' AND da.external_key IS NULL ';
		}

		if( false == $boolIncludeArchived ) {
			$strSql .= ' AND d.archived_on IS NULL ';
		}

		$strSql .= ' AND d.deleted_by IS NULL AND da.deleted_by IS NULL ORDER BY da.order_num, da.created_on ';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchLeaseDocumentsByDocumentTypeIdByDocumentSubTypeIdByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intCid, $objDatabase, $strSortBy = 'name', $strSortDirection = 'asc' ) {

		if( 'name' == $strSortBy || 'title' == $strSortBy || 'description' == $strSortBy || 'keywords' == $strSortBy ) {
			$strOrderBy = $objDatabase->getCollateSort( 'd.' . $strSortBy . '', true );
		} else {
			$strOrderBy = 'd.' . $strSortBy;
		}

		$strSql = 'SELECT
						d.*,
						da.id as document_addenda_id,
						da.external_key,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional,
						da.is_auto_generate,
						da.require_sign,
						da.order_num as document_addenda_order_num,
						da.last_sync_on as last_sync_on,
						ft.name AS file_type_name
					FROM
						documents d
					    JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid';

		if( CDocumentSubType::DOCUMENT_TEMPLATE == $intDocumentSubTypeId ) {
			$strSql .= ' AND da.master_document_id IS NULL ';
		}

		$strSql .= ' )
					LEFT JOIN file_types ft ON ( ft.cid = d.cid AND ft.id = d.file_type_id )
					WHERE
						d.cid = ' . ( int ) $intCid . '
					    AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
					    AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
					    AND d.document_id IS NULL
					    AND d.archived_on IS NULL
					    AND d.deleted_by IS NULL
						AND d.is_system = 0
						AND is_bluemoon = false
					ORDER BY ' . $strOrderBy . ' ' . $strSortDirection;

		return self::fetchDocuments( $strSql, $objDatabase );
	}

    public static function fetchDocumentsByDocumentsFilter( $intCid, $objDatabase, $objDocumentsFilter, $boolSqlOnly = false, $strSortBy = 'name', $strSortDirection = 'ASC', $boolIsReturnCount = false, $objPagination = NULL ) {

        if( 'name' == $strSortBy || 'title' == $strSortBy || 'description' == $strSortBy || 'keywords' == $strSortBy ) {
	        $strOrderBy = $objDatabase->getCollateSort( 'd.' . $strSortBy . '' );
        } else {
	        $strOrderBy = 'd.' . $strSortBy;
        }

		if( true == $boolSqlOnly ) {
            $strWhereCondition = ( true == valStr( $objDocumentsFilter->getDocumentName() ) ) ? " AND ( LOWER (jsonb_details.value) LIKE LOWER ('%" . addslashes( $objDocumentsFilter->getDocumentName() ) . "%') )" : '';
        } else {
            $strWhereCondition = ( true == valStr( $objDocumentsFilter->getDocumentName() ) ) ? " AND ( LOWER (d.name) LIKE LOWER ('%" . addslashes( $objDocumentsFilter->getDocumentName() ) . "%') OR LOWER (d.details->>'marketing_name') LIKE LOWER ('%" . addslashes( $objDocumentsFilter->getDocumentName() ) . "%') )" : '';
        }

        $strWhereCondition .= ( true == valArr( $objDocumentsFilter->getFileTypeIds() ) ) ? ' AND ft.id IN (' . implode( ',', $objDocumentsFilter->getFileTypeIds() ) . ') ' : '';

        if( true == valStr( $objDocumentsFilter->getUsage() ) ) {
            switch( $objDocumentsFilter->getUsage() ) {
                case 'DOCUMENT_USAGE_ADHOC_AND_PACKET':
                    $strWhereCondition .= ' AND ( d.is_packet = TRUE AND d.is_adhoc = TRUE )';
                    break;

                case 'DOCUMENT_USAGE_ADHOC_ONLY':
                    $strWhereCondition .= ' AND d.is_packet = FALSE AND d.is_adhoc = TRUE ';
                    break;

                case 'DOCUMENT_USAGE_PACKETS_ONLY':
                    $strWhereCondition .= ' AND d.is_packet = TRUE AND d.is_adhoc = FALSE ';
                    break;

                default:
            }
        }

	    $strSql = 'SELECT ';

        if( true == $boolIsReturnCount ) {
	        $strSql .= 'count(DISTINCT d.id) as template_count';
        } else {

	        $strSql .= 'd.id,';

	        if( true == $boolSqlOnly ) {
		        $strSql .= 'jsonb_details.value as name,';
	        } else {
		        $strSql .= ' d.name,';
	        }

	        $strSql .= 'd.details->>\'marketing_name\' as marketing_name,
	                    d.is_packet,
	                    d.is_adhoc,
	                    d.is_published,
						da.id as document_addenda_id,
						da.external_key,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional,
						da.is_auto_generate,
						da.require_sign,
						da.order_num as document_addenda_order_num,
						da.last_sync_on as last_sync_on,
						ft.name AS file_type_name,
						d.details';
        }

	    $strSql .= ' FROM
						documents d
					    JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid';

        if( CDocumentSubType::DOCUMENT_TEMPLATE == $objDocumentsFilter->getDocumentSubTypeId() ) {
            $strSql .= ' AND da.master_document_id IS NULL ';
        }

        $strSql .= 'AND da.deleted_by IS NULL AND da.deleted_on IS NULL )
					LEFT JOIN file_types ft ON ( ft.cid = d.cid AND ft.id = d.file_type_id ) ';

        if( true == valArr( $objDocumentsFilter->getFileTypeIds() ) ) {
            $strWhereCondition .= ' AND ft.id IN ( ' . implode( ',', $objDocumentsFilter->getFileTypeIds() ) . ' ) ';
        }

        if( true == valArr( $objDocumentsFilter->getPropertyIds() ) ) {
            $strSql .= ' JOIN property_group_documents pgd ON ( pgd.document_id = d.id AND pgd.cid = d.cid)
    					 JOIN property_groups pg ON ( pgd.property_group_id = pg.id AND pgd.cid = pg.cid AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL ) ';
            $strWhereCondition .= ' AND pg.id IN ( ' . implode( ',', $objDocumentsFilter->getPropertyIds() ) . ' ) ';
        }

        if( true == $objDocumentsFilter->getIsExcludeEntrataDefault() ) {
            $strWhereCondition .= ' AND d.is_entrata_default = false ';
        }

	    if( CDocumentSubType::DOCUMENT_TEMPLATE == $objDocumentsFilter->getDocumentSubTypeId() && false == $objDocumentsFilter->getIsAllTemplates() ) {

		    if( true == $objDocumentsFilter->getIsTenantTechTemplates() ) {
			    $strWhereCondition .= ' AND d.is_system = 1 AND d.transmission_vendor_id = 48 ';
		    } else {
			    if( true == $objDocumentsFilter->getIsBluemoonTemplates() ) {
				    $strWhereCondition .= ' AND d.is_system = 1 AND d.is_bluemoon = true ';
			    } else {
				    $strWhereCondition .= '  AND d.is_system = 0 AND d.is_bluemoon = false ';
			    }
		    }
	    }

        if( true == $boolSqlOnly ) {
            $strSql .= ',jsonb_each_text(jsonb_build_object(\'name\',d.name,\'marketing_name\', d.details->>\'marketing_name\') ) as jsonb_details ';
            $strWhereCondition .= ' AND jsonb_details.value IS NOT NULL ';
        }

        $strSql .= 'WHERE
						d.cid = ' . ( int ) $intCid . '
					    AND d.document_type_id = ' . ( int ) $objDocumentsFilter->getDocumentTypeId() . '
					    AND d.document_sub_type_id = ' . ( int ) $objDocumentsFilter->getDocumentSubTypeId() . ' ' . $strWhereCondition . '
					    AND d.document_id IS NULL
					    AND d.archived_on IS NULL
					    AND d.deleted_by IS NULL';

	    if( false === $boolSqlOnly && true == valArr( $objDocumentsFilter->getPropertyIds() ) && false == $boolIsReturnCount ) {
		    $strSql .= ' GROUP BY
			                d.id,
			                d.name,
			                d.details->>\'marketing_name\', 
			                d.is_packet,
	                        d.is_adhoc,
	                        d.is_published,
							da.id,
							da.external_key,
							da.is_for_primary_applicant,
							da.is_for_co_applicant,
							da.is_for_co_signer,
							da.is_optional,
							da.is_auto_generate,
							da.require_sign,
							da.order_num,
							da.last_sync_on,
							ft.name,
							d.details';
	    }

	    if( false == $boolIsReturnCount ) {
		    $strSql .= ' ORDER BY ' . $strOrderBy . ' ' . $strSortDirection;
	    }

	    if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
		    $strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
	    }

	    if( true === $boolSqlOnly ) {
	       return $strSql;
	    } elseif( true == $boolIsReturnCount ) {
		    return self::fetchColumn( $strSql, 'template_count', $objDatabase );
	    } else {

	       return self::fetchDocuments( $strSql, $objDatabase );
	    }
    }

	public static function fetchLeaseDocumentsByDocumentTypeIdByDocumentSubTypeIdsByDocumentAddendaTypeIdsByCid( $intDocumentTypeId, $arrintDocumentSubTypeIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.*,
						da.last_sync_on
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
					    AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
					    AND d.document_sub_type_id IN ( ' . implode( ',', $arrintDocumentSubTypeIds ) . ' )
					    AND d.document_id IS NULL
					    AND d.archived_on IS NULL
					    AND d.deleted_by IS NULL
					ORDER BY
						' . $objDatabase->getCollateSort( ' d.name ', true );

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchParentDocumentByIdByCid( $intId, $intCid, $objDatabase, $boolIncludeArchived = false, $boolIncludeDeleted = false ) {
		$strSql = 'SELECT * FROM documents WHERE cid = ' . ( int ) $intCid . '  AND document_id IS NULL AND id = ' . ( int ) $intId;

		if( false == $boolIncludeArchived ) {
			$strSql .= ' AND archived_on IS NULL ';
		}

		if( false == $boolIncludeDeleted ) {
			$strSql .= ' AND deleted_by IS NULL ';
		}

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchCustomDocumentsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase, $boolIncludeMasterDocument = false ) {

		$strSql = 'SELECT
						*
					FROM
						documents
					WHERE ';

		$strSql .= ( true == $boolIncludeMasterDocument ) ? ' id = ' . ( int ) $intDocumentId . ' OR document_id = ' . ( int ) $intDocumentId : ' document_id = ' . ( int ) $intDocumentId;

		$strSql .= ' AND cid = ' . ( int ) $intCid . ' AND archived_on IS NULL AND deleted_by IS NULL ORDER BY ' . $objDatabase->getCollateSort( 'name', true );

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchDocumentByIdByDocumentIdByCid( $intId, $intDocumentId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM documents WHERE document_id = ' . ( int ) $intDocumentId . ' AND id = ' . ( int ) $intId . ' AND cid = ' . ( int ) $intCid;

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchDocumentsByCompanyApplicationIdByCid( $intCompanyApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						d.*,
						da.company_application_id,
						da.id as document_addenda_id
					 FROM
					 	documents d
						JOIN document_addendas da ON ( d.id = da.document_id AND d.cid = da.cid )
					WHERE
					 	da.company_application_id =  ' . ( int ) $intCompanyApplicationId . '
					 	AND d.document_type_id = ' . CDocumentType::MERGE_DOCUMENT . '
					 	AND d.document_sub_type_id IN ( ' . CDocumentSubType::APPLICATION_POLICY_DOCUMENT . ', ' . CDocumentSubType::RESIDENT_VERIFY_SCREENING_POLICY_DOCUMENT . ' )
					 	AND da.cid = ' . ( int ) $intCid . '
					 	AND da.deleted_by IS NULL
					 	AND da.archived_on IS NULL
					 	AND d.deleted_by IS NULL
					 	AND d.archived_on IS NULL
					ORDER BY
						da.order_num,
						da.created_on';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchPublishedDocumentsByCompanyApplicationIdByCid( $intCompanyApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.*,da.company_application_id, da.id as document_addenda_id, da.master_document_id
					 FROM
					 	documents d JOIN document_addendas da ON ( d.id = da.document_id AND d.cid = da.cid )
					 	AND ( 
					 			da.company_application_id =  ' . ( int ) $intCompanyApplicationId . ' 
					 			OR 
					 			( 
					 				da.master_document_id IN ( SELECT id FROM documents WHERE cid = ' . ( int ) $intCid . ' AND company_application_ids @> ARRAY[ ' . ( int ) $intCompanyApplicationId . ' ] AND deleted_on IS NULL )
					 			) 
					 		)
					 	AND da.cid = ' . ( int ) $intCid . '
					 	AND d.is_published = 1
					 	AND ( da.is_for_primary_applicant = 1 OR da.is_for_co_applicant = 1 OR da.is_for_co_signer = 1 )
					 	AND da.deleted_by IS NULL
					 	AND da.archived_on IS NULL
					 	AND d.deleted_by IS NULL
					 	AND d.archived_on IS NULL
					ORDER BY
						da.order_num,
						da.created_on';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchDocumentByIdByCompanyApplicationIdByCid( $intId, $intCompanyApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.*,
						da.company_application_id,
						da.id as document_addenda_id,
						da.attach_to_email,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.require_confirmation
					 FROM
					 	documents d JOIN document_addendas da ON ( d.id = da.document_id AND d.cid = da.cid AND d.id = ' . ( int ) $intId . ')
					 	AND da.company_application_id =  ' . ( int ) $intCompanyApplicationId . '
					 	AND da.cid = ' . ( int ) $intCid;

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchLeaseDocumentsByIdsByCid( $arrintDocumentIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintDocumentIds ) ) return NULL;

		$strSql = 'SELECT
						d.*,
						da.id as document_addedna_id,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional,
						da.is_household,
						da.order_num as document_addenda_order_num
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
						AND d.deleted_on IS NULL
						AND d.deleted_by IS NULL
						AND d.archived_on IS NULL
				    ORDER BY
						' . $objDatabase->getCollateSort( 'd.name', true );
		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchLeaseDocumentsByIdsByDocumentIdsByCid( $arrintIds, $arrintDocumentIds, $intCid, $objDatabase, $boolIncludeArchived = false ) {

		if( false == valArr( $arrintDocumentIds ) || false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
						d.*,
						da.external_key as external_key,
						da.id as document_addedna_id,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional as is_optional,
						da.is_household,
						da.is_default_selected as is_default_selected,
						da.order_num as document_addenda_order_num,
						da.last_sync_on as last_sync_on
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND da.master_document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
						AND d.id IN(' . implode( ',', $arrintIds ) . ') '
		          . ( ( false == $boolIncludeArchived ) ? ' AND d.archived_on IS NULL ' : '' ) . '
						AND d.deleted_by IS NULL
					ORDER BY
						da.order_num,
						da.created_on';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchDocumentPacketsByArTriggerTypeIdByPropertyIdByCid( $intArTriggerId, $intPropertyId, $intCid, $objDatabase, $arrintLeaseIntervalId, $boolExternalPacketsOnly, $strPreferredLocaleCode, $arrintExcludeOccupancyTypeIds = NULL ) {

		$strSql = 'SELECT
						d.*
					FROM
						documents d
					    JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
					    JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					    JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						pga.property_id = ' . ( int ) $intPropertyId . '
					    AND pgd.document_association_type_id = ' . CDocumentAssociationType::PACKET . '
						AND d.ar_trigger_id = ' . ( int ) $intArTriggerId . '
					    AND d.is_published = 1
					    AND ( d.activate_on <= NOW() OR d.activate_on IS NULL )
						AND ( d.deactivate_on >= NOW() OR d.deactivate_on IS NULL )
					    AND d.deleted_on IS NULL
					    AND d.deleted_by IS NULL
						AND d.archived_on IS NULL
						AND d.cid = ' . ( int ) $intCid;

		if( true == isset( $arrintLeaseIntervalId ) ) {
			$strSql .= ' AND ( \'{ ' . implode( ',', $arrintLeaseIntervalId ) . ' }\' && d.lease_interval_type_ids )';
		}

		if( true == $boolExternalPacketsOnly ) {
			$strSql .= ' AND d.transmission_vendor_id IS NOT NULL';
		}

		if( true == valStr( $strPreferredLocaleCode ) ) {
			$strSql .= ' AND ( d.transmission_vendor_id = ' . CTransmissionVendor::BLUE_MOON_LEASE . ' OR d.locale_code = \'' . $strPreferredLocaleCode . '\' )';
        }

		if( true == valArr( $arrintExcludeOccupancyTypeIds ) ) {
			$strSql .= ' AND ( d.occupancy_type_id IS NULL OR d.occupancy_type_id NOT IN (' . implode( ',',  $arrintExcludeOccupancyTypeIds ) . ') )';
		}

		$strSql .= ' ORDER BY ' . $objDatabase->getCollateSort( 'd.name', true ) . ' ASC';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchLeaseDocumentsByLeaseUsagePreferencesByPropertyIdByCid( $arrintLeaseIntervalIds, $intPropertyId, $intCid, $objDatabase, $boolOtherEsignDocument = false ) {

		if( true == $boolOtherEsignDocument ) {
			$arrintDocumentSubTypeIds 			= [ CDocumentAssociationType::PACKET, CDocumentSubType::DOCUMENT_TEMPLATE ];
			$arrintDocumentAssociationTypeIds 	= [ CDocumentAssociationType::PACKET, CDocumentAssociationType::DOCUMENT_TEMPLATE ];
		} else {
			$arrintDocumentSubTypeIds 			= [ CDocumentAssociationType::PACKET ];
			$arrintDocumentAssociationTypeIds 	= [ CDocumentAssociationType::PACKET ];
		}

		$strSql = 'SELECT
						d.*';

		if( true == $boolOtherEsignDocument )	$strSql .= ',	da.require_sign';

		$strSql .= ' FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )';

		if( true == $boolOtherEsignDocument )	$strSql .= ' JOIN document_addendas da ON ( d.id = da.document_id AND d.cid = da.cid )';

		$strSql .= ' WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId;

		if( true == valArr( $arrintLeaseIntervalIds ) ) {
			$strSql .= ' AND CASE WHEN ' . CDocumentSubType::DOCUMENT_TEMPLATE . ' != d.document_sub_type_id THEN
							( \'{ ' . implode( ',', $arrintLeaseIntervalIds ) . ' }\' && d.lease_interval_type_ids )
						ELSE
							true
						END ';
		}

		$strSql .= ' AND pgd.document_association_type_id IN ( ' . implode( ', ', $arrintDocumentAssociationTypeIds ) . ' )';

		if( true == $boolOtherEsignDocument )
			$strSql .= 'AND ( ( d.document_sub_type_id IN ( ' . implode( ', ', $arrintDocumentSubTypeIds ) . ' ) AND d.file_name IS NOT NULL AND d.file_path IS NOT NULL )
						OR d.is_bluemoon = true )';

		$strSql .= ' AND d.archived_on IS NULL
					AND d.deleted_on IS NULL
					AND d.is_published = 1
					AND ( d.activate_on <= NOW() OR d.activate_on IS NULL )
					AND ( d.deactivate_on >= NOW() OR d.deactivate_on IS NULL )
				ORDER BY
					' . $objDatabase->getCollateSort( 'd.name', true ) . ',
					pgd.id';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchLeaseTemplatesByLeaseUsagePreferencesByPropertyIdExcludingFileTypeSystemCodesIdsByCid( $intPropertyId, $arrstrFileTypeSystemCodes, $intCid, $objDatabase, $boolOtherEsignDocument = false ) {

		if( true == $boolOtherEsignDocument ) {
			$arrintDocumentSubTypeIds 			= [ CDocumentAssociationType::PACKET, CDocumentSubType::DOCUMENT_TEMPLATE ];
			$arrintDocumentAssociationTypeIds 	= [ CDocumentAssociationType::PACKET, CDocumentAssociationType::DOCUMENT_TEMPLATE ];
		} else {
			$arrintDocumentSubTypeIds 			= [ CDocumentAssociationType::PACKET ];
			$arrintDocumentAssociationTypeIds 	= [ CDocumentAssociationType::PACKET ];
		}

		$strSql = 'SELECT
						d.*,
						da.require_sign';

		$strSql .= ' FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_group_associations pga ON ( pga.property_group_id = pgd.property_group_id AND pga.cid = pgd.cid )
		 				JOIN document_addendas da ON ( d.id = da.document_id AND d.cid = da.cid )
						JOIN file_types ft ON ( d.cid = ft.cid AND d.file_type_id = ft.id )
					 WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId;

		$strSql .= ' AND pgd.document_association_type_id IN ( ' . implode( ', ', $arrintDocumentAssociationTypeIds ) . ' )';

		$strSql .= 'AND d.document_sub_type_id IN ( ' . implode( ', ', $arrintDocumentSubTypeIds ) . ' )';

		$strSql .= 'AND ( ft.system_code NOT IN ( \'' . implode( '\',\'', $arrstrFileTypeSystemCodes ) . '\' ) OR ft.system_code IS NULL )';

		$strSql .= ' AND d.archived_on IS NULL
					AND d.deleted_on IS NULL
					AND d.is_published = 1
					AND ( d.activate_on <= NOW() OR d.activate_on IS NULL )
					AND ( d.deactivate_on >= NOW() OR d.deactivate_on IS NULL )
					AND d.transmission_vendor_id IS NULL
				ORDER BY
					' . $objDatabase->getCollateSort( 'd.name', true ) . ',
					pgd.id';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchLeaseDocumentCountByLeaseUsagePreferencesByPropertyIdByCid( $arrintLeaseIntervalIds, $intPropertyId, $intCid, $objDatabase, $boolOtherEsignDocument = false ) {

		if( true == $boolOtherEsignDocument ) {
			$arrintDocumentSubTypeIds 			= [ CDocumentSubType::LEASE_AGREEMENT, CDocumentSubType::DOCUMENT_TEMPLATE ];
			$arrintDocumentAssociationTypeIds 	= [ CDocumentAssociationType::PACKET, CDocumentAssociationType::DOCUMENT_TEMPLATE ];
		} else {
			$arrintDocumentSubTypeIds 			= NULL;
			$arrintDocumentAssociationTypeIds 	= [ CDocumentAssociationType::PACKET ];
		}

		$strSql = 'SELECT
						count(d.id) as count
					FROM
						documents d
						JOIN property_group_documents pgd ON ( pgd.cid = d.cid AND pgd.document_id = d.id )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId;

		if( true == valArr( $arrintLeaseIntervalIds ) )
			$strSql .= ' AND ( \'{ ' . implode( ',', $arrintLeaseIntervalIds ) . ' }\' && d.lease_interval_type_ids ) ';

		$strSql .= 'AND pgd.document_association_type_id IN ( ' . implode( ', ', $arrintDocumentAssociationTypeIds ) . ' )';

		if( true == $boolOtherEsignDocument )
			$strSql .= 'AND ( ( d.document_sub_type_id IN ( ' . implode( ', ', $arrintDocumentSubTypeIds ) . ' ) AND d.file_name IS NOT NULL AND d.file_path IS NOT NULL )
						OR d.is_bluemoon = true )';

		$strSql .= 'AND d.archived_on IS NULL
					AND d.deleted_on IS NULL
					AND d.is_published = 1
					AND ( d.activate_on <= NOW() OR d.activate_on IS NULL )
					AND ( d.deactivate_on >= NOW() OR d.deactivate_on IS NULL )';

		$arrintLeaseDocumentCount = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintLeaseDocumentCount[0]['count'] ) ) {
			return $arrintLeaseDocumentCount[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchActiveLeaseDocumentPacketByIdByPropertyIdByCid( $intId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.*
					FROM
						documents d
						JOIN property_group_documents pgd ON ( pgd.document_id = d.id AND pgd.cid = d.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						pga.property_id = ' . ( int ) $intPropertyId . '
						AND d.id = ' . ( int ) $intId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND ( d.activate_on <= NOW() OR d.activate_on IS NULL )
						AND ( d.deactivate_on >= NOW() OR d.deactivate_on IS NULL )
						AND d.document_id IS NULL
						AND d.deleted_by IS NULL
						AND d.archived_on IS NULL
						AND d.is_published = 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchDocumentByIdByPropertyIdByCid( $intDocumentId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.*
					FROM
						documents d
						JOIN property_group_documents pgd ON ( pgd.cid = d.cid AND pgd.document_id = d.id )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						pga.property_id = ' . ( int ) $intPropertyId . '
						AND d.id = ' . ( int ) $intDocumentId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.deleted_by IS NULL
						AND d.archived_on IS NULL
						AND d.is_published = 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchCustomLeaseDocumentsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
						d.*,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional as is_optional
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid )
					WHERE
						d.id IN (' . implode( ',', $arrintIds ) . ' )
						AND d.cid = ' . ( int ) $intCid . '
						AND d.archived_on IS NULL
						AND d.deleted_by IS NULL
						ORDER BY
							da.order_num';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchUnpublishedActiveDocumentsByPropertyIdByDocumentTypeIdByDocumentAssociationTypeIdByCid( $intPropertyId, $intDocumentTypeId, $intDocumentAssociationTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT d.*
					FROM
						documents d
						JOIN property_group_documents pgd ON ( pgd.cid = d.cid AND pgd.document_id = d.id )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.archived_on IS NULL
						AND d.is_published = 0
						AND d.deleted_by IS NULL
						AND ( d.activate_on <= NOW() OR d.activate_on IS NULL )
						AND ( d.deactivate_on >= NOW() OR d.deactivate_on IS NULL )
					ORDER BY
						pgd.order_num,
						pgd.id ';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchExternalLeaseDocumentByNameByTransmissionVendorIdByCid( $strName, $intTransmissionVendorId, $intCid, $objDatabase ) {
		if( false == valStr( $strName ) || false == valId( $intTransmissionVendorId ) ) return NULL;

		// Remove this condition when $intTransmissionVendorId is used for BM as well
		if( CTransmissionVendor::BLUE_MOON_LEASE == $intTransmissionVendorId ) {
			$strCond = 'AND is_bluemoon = true';
		}

		$strSql = 'SELECT
						*
					FROM
						documents
					WHERE
						document_type_id = ' . ( int ) CDocumentType::PACKET . '
						' . $strCond . '
						AND transmission_vendor_id = ' . ( int ) $intTransmissionVendorId . '
						AND cid = ' . ( int ) $intCid . '
						AND name ILIKE E\'' . trim( addslashes( $strName ) ) . '\'
						AND document_id IS NULL
						AND deleted_by IS NULL
						AND archived_on IS NULL
					ORDER BY
						order_num,
						id
					LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchBlueMoonLeaseDocumentByNameByCid( $strName, $intCid, $objDatabase ) {
		if( false == valStr( $strName ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						documents
					WHERE
						document_type_id = ' . ( int ) CDocumentType::PACKET . '
						AND is_bluemoon = true
						AND cid = ' . ( int ) $intCid . '
						AND name ILIKE E\'' . trim( addslashes( $strName ) ) . '\'
						AND document_id IS NULL
						AND deleted_by IS NULL
						AND archived_on IS NULL
					ORDER BY
						order_num,
						id
					LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchTenantTechLeaseDocumentByNameByCid( $strName, $intCid, $objDatabase ) {
		if( false == valStr( $strName ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						documents
					WHERE
						document_type_id = ' . ( int ) CDocumentType::PACKET . '
						AND transmission_vendor_id = ' . CTransmissionVendor::TENANT_TECH . '
						AND cid = ' . ( int ) $intCid . '
						AND name ILIKE E\'' . trim( addslashes( $strName ) ) . '\'
						AND document_id IS NULL
						AND deleted_by IS NULL
						AND archived_on IS NULL
					ORDER BY
						order_num,
						id
					LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchLeaseDocumentPacketByIdsByPropertyIdByCid( $arrintIds, $intPropertyId, $intCid, $objDatabase, $arrintLeaseIntervalIds = array() ) {

		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
						d.*
					FROM
						documents d
						JOIN property_group_documents pgd ON ( pgd.document_id = d.id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.document_type_id = ' . ( int ) CDocumentType::PACKET . '
						AND d.ar_trigger_id = ' . CArTrigger::LEASE_STARTED . '
						AND d.id IN(' . implode( ',', $arrintIds ) . ')
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND ( d.activate_on <= NOW() OR d.activate_on IS NULL )
						AND ( d.deactivate_on >= NOW() OR d.deactivate_on IS NULL )
						AND d.document_id IS NULL
						AND d.deleted_by IS NULL
						AND d.archived_on IS NULL
						AND d.is_published = 1
						AND d.cid = ' . ( int ) $intCid;

		if( true == valArr( $arrintLeaseIntervalIds ) ) {
			$strSql .= ' AND d.lease_interval_type_id @> ' . $arrintLeaseIntervalIds;
		}

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchDocumentSubTypeIdsByPropertyIdsByCidForLateNotices( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
					     DISTINCT *
					 FROM
					     (
					       SELECT
					           lnt.cid,
					           pga.property_id,
					           lnt.document_id,
					           d.document_sub_type_id,
					           rank ( ) OVER ( partition BY lnt.cid, pga.property_id, d.document_sub_type_id
					       ORDER BY
					           lnt.order_num ASC ) AS rank
					       FROM
					           documents d
					           JOIN late_notice_types lnt ON ( lnt.document_id = d.id AND lnt.cid = d.cid )
					           JOIN property_group_associations pga ON ( pga.property_group_id = lnt.property_group_id AND pga.cid = lnt.cid )
					       WHERE
					           d.cid = ' . ( int ) $intCid . '
					           AND pga.property_id IN  ( ' . implode( ',', $arrintPropertyIds ) . ')
					           AND d.deleted_by IS NULL
					           AND lnt.deleted_by IS NULL
					           AND d.is_published = 1
							   AND d.document_sub_type_id IN ( ' . CDocumentSubType::FIRST_NOTICE . ',' . CDocumentSubType::REPEAT_NOTICE . ',' . CDocumentSubType::FINAL_NOTICE . ' ,' . CDocumentSubType::DEMAND_NOTICE . ' ,' . CDocumentSubType::NON_RENEWAL_NOTICE . ', ' . CDocumentSubType::NSF_NOTICE . ', ' . CDocumentSubType::REPAYMENT_AGREEMENT_LATE_NOTICE . ' )
					     ) AS sub
					 WHERE
					     sub.rank = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDocumentSubTypeIdsByPropertyIdsByCidForNSF( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
					     DISTINCT *
					 FROM
					     (
					       SELECT
					           d.cid,
					           pga.property_id,
					           d.id,
					           d.document_sub_type_id,
					           rank ( ) OVER ( partition BY d.cid, pga.property_id, d.document_sub_type_id ORDER BY lnt.order_num ASC ) AS rank
					       FROM
					           documents d
					           JOIN late_notice_types lnt ON ( lnt.document_id = d.id AND lnt.cid = d.cid )
					           JOIN property_group_associations pga ON ( pga.property_group_id = lnt.property_group_id AND pga.cid = lnt.cid )
					       WHERE
					           d.cid = ' . ( int ) $intCid . '
					           AND pga.property_id IN  ( ' . implode( ',', $arrintPropertyIds ) . ')
					           AND d.deleted_by IS NULL
					           AND lnt.deleted_by IS NULL
					           AND d.is_published = 1
							   AND d.document_sub_type_id = ' . CDocumentSubType::NSF_NOTICE . '
							UNION
							SELECT
								d.cid, 
								pga.property_id ,
								d.id,
								d.document_sub_type_id,
								rank ( ) OVER ( partition BY d.cid, pga.property_id, d.document_sub_type_id ORDER BY d.order_num ASC ) AS rank
							From                
								documents d
								JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
								JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
								JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
								JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid )
								JOIN file_types ft ON ( d.file_type_id = ft.id AND ft.cid = d.cid )
							WHERE 
								d.cid = ' . ( int ) $intCid . '
								AND pga.property_id IN  ( ' . implode( ',', $arrintPropertyIds ) . ')
								AND d.document_type_id = ' . CDocumentType::MERGE_DOCUMENT . '
								AND d.document_sub_type_id = ' . CDocumentSubType::DOCUMENT_TEMPLATE . '
								AND pgd.document_association_type_id = ' . CDocumentAssociationType::DOCUMENT_TEMPLATE . '
								AND ft.system_code = \'' . CFileType::SYSTEM_CODE_NSF . '\'
								AND d.is_published = 1
								AND d.archived_on IS NULL
								AND ( d.activate_on IS NULL OR d.activate_on <= now() )
								AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
								AND d.deleted_by IS NULL
					     ) AS sub
					 WHERE
					     sub.rank = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNSFDocumentSubTypeIdsByPropertyIdsByCidForLateNotices( $arrintPropertyIds, $intCid, $objDatabase ) {

			if( false == valArr( $arrintPropertyIds ) ) return NULL;

			$strSql = 'SELECT
							DISTINCT *
						 FROM
							(
								SELECT
									lnt.cid,
									pga.property_id,
									lnt.document_id,
									d.document_sub_type_id,
									rank ( ) OVER ( partition BY lnt.cid, pga.property_id, d.document_sub_type_id
								ORDER BY
									lnt.order_num ASC ) AS rank
								FROM
									documents d
									JOIN late_notice_types lnt ON ( lnt.document_id = d.id AND lnt.cid = d.cid )
									JOIN property_group_associations pga ON ( pga.property_group_id = lnt.property_group_id AND pga.cid = lnt.cid )
								WHERE
								d.cid = ' . ( int ) $intCid . '
								AND pga.property_id IN  ( ' . implode( ',', $arrintPropertyIds ) . ')
								AND d.deleted_by IS NULL
								AND lnt.deleted_by IS NULL
								AND d.is_published = 1
								AND d.document_sub_type_id IN ( ' . CDocumentSubType::NSF_NOTICE . ', ' . CDocumentSubType::DOCUMENT_TEMPLATE . ')
							) AS sub
						 WHERE
							sub.rank = 1';

			return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLateNoticeDocumentsByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = 'SELECT
						d.*,
						lnt.property_group_id
					FROM
						documents d
						JOIN late_notice_types lnt ON ( d.id = lnt.document_id AND lnt.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND lnt.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ')
						AND d.deleted_by IS NULL
						AND d.is_published = 1
						AND d.archived_on IS NULL
						AND lnt.deleted_by IS NULL
					ORDER BY
						d.id;';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchDocumentsByDocumentSubTypeIdByPropertyGroupIdsByCidForLateNotices( $intDocumentSubTypeId, $arrintPropertyGroupIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = 'SELECT
						d.*,
						lnt.property_group_id
					FROM
						documents d
						JOIN late_notice_types lnt ON ( d.id = lnt.document_id AND lnt.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND lnt.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ')
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
						AND lnt.deleted_by IS NULL
						AND d.deleted_by IS NULL
						AND lnt.deleted_by IS NULL
						AND d.archived_on IS NULL
						AND d.deleted_by IS NULL
					ORDER BY
						d.id;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublishedLateNoticeDocumentsByDocumentTypeIdByDocumentSubTypeIdsByPropertyGroupIdsByCid( $intDocumentTypeId, $arrintDocumentSubTypeIds, $arrintPropertyGroupIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyGroupIds ) || false == valArr( $arrintDocumentSubTypeIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON( lnt.property_group_id, d.id )
				 		lnt.*,
						d.document_sub_type_id,
						dp.value as document_content
					FROM
						late_notice_types lnt
						JOIN documents d ON ( d.id = lnt.document_id AND d.cid = lnt.cid )
						LEFT JOIN document_parameters dp ON ( d.id = dp.document_id AND d.cid = dp.cid AND dp.key = \'text\' )
					WHERE
						d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.document_sub_type_id IN (' . implode( ', ', $arrintDocumentSubTypeIds ) . ' )
						AND d.cid = ' . ( int ) $intCid . '
						AND lnt.property_group_id IN (' . implode( ',', $arrintPropertyGroupIds ) . ' )
						AND d.is_published = 1
						AND lnt.is_published = 1
						AND d.archived_on IS NULL
						AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL
						AND lnt.deleted_by IS NULL
					ORDER BY
						d.id,
						lnt.property_group_id;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDocumentsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return false;

		$strSql = 'SELECT
						pga.property_id,
						d.*
					FROM
						documents d
						JOIN property_group_documents pgd ON ( pgd.cid = d.cid AND pgd.document_id = d.id )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
						JOIN late_notice_types lnt ON ( lnt.property_id = pga.property_id AND lnt.document_id = pgd.document_id AND lnt.cid = pgd.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND d.document_sub_type_id IN ( ' . CDocumentSubType::FIRST_NOTICE . ',' . CDocumentSubType::REPEAT_NOTICE . ',' . CDocumentSubType::FINAL_NOTICE . ' ,' . CDocumentSubType::DEMAND_NOTICE . ' )
						AND d.deleted_by IS NULL
						AND lnt.deleted_by IS NULL
						AND d.is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublishedDocumentsByCompanyApplicationIdByDocumentAddendaTypeIdByDocumentSubTypeIdByCid( $intCompanyApplicationId, $intDocumentSubTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.*
					 FROM
					 	documents d JOIN document_addendas da ON ( d.id = da.document_id AND d.cid = da.cid )
					 	AND ( 
					 			da.company_application_id =  ' . ( int ) $intCompanyApplicationId . ' 
					 			OR 
					 			( 
					 				da.master_document_id IN ( SELECT id FROM documents WHERE cid = ' . ( int ) $intCid . ' AND company_application_ids @> ARRAY[ ' . ( int ) $intCompanyApplicationId . ' ] AND deleted_on IS NULL )
					 			) 
					 	    ) 
					 	AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
					 	AND da.cid = ' . ( int ) $intCid . '
					 	AND d.is_published = 1
					 	AND ( da.is_for_primary_applicant = 1 OR da.is_for_co_applicant = 1 OR da.is_for_co_signer = 1 )
					 	AND da.deleted_by IS NULL
					 	AND d.archived_on IS NULL
					ORDER BY
						da.order_num,
						da.created_on';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchReportTemplateDocumentsBySubTypeIdByDocumentTypeIdByCid( $intDocumentSubTypeId, $intDocumentTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
							*
						FROM
							documents d
						WHERE
							d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
							AND d.cid = ' . ( int ) $intCid . '
							AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						    AND d.is_published = 1
						    AND d.archived_on IS NULL
						    AND d.deleted_by IS NULL
						ORDER BY
							d.order_num,
							d.id';
		return CDocuments::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchReportTemplateDocumentsBySubTypeIdByDocumentTypeIdByUserIdByCid( $intDocumentSubTypeId, $intDocumentTypeId, $intUserId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						d.id,
						d.name,
						d.title,
						d.created_by,
						d.updated_by,
						d.order_num
					FROM(
						SELECT
								DISTINCT( d.id ),
								d.name,
								d.title,
								d.created_by,
								d.updated_by,
								d.order_num
							FROM
								documents d
							    JOIN company_users cu ON ( d.created_by = cu.id AND d.cid = cu.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
							WHERE
								d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
								AND d.cid = ' . ( int ) $intCid . '
								AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
							    AND cu.is_administrator = 1
							    AND d.deleted_by IS NULL
							    AND d.is_published = 1
							    AND d.archived_on IS NULL
							UNION
							SELECT
							    d.id,
								d.name,
								d.title,
								d.created_by,
								d.updated_by,
							    d.order_num
							FROM
								documents d
							     JOIN company_users cu ON ( d.created_by = cu.id AND ( d.cid = cu.cid AND cu.default_company_user_id IS NULL ) AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
							WHERE
								d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
								AND d.cid = ' . ( int ) $intCid . '
								AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
							    AND cu.is_administrator = 0
								AND d.created_by = ' . ( int ) $intUserId . '
							    AND d.deleted_by IS NULL
							    AND d.is_published = 1
							    AND d.archived_on IS NULL
						) d
					ORDER BY
						d.order_num,
						d.id';

		return CDocuments::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchReportTemplateDocumentByNameByFilePathByActivateOnByBoolIsPrivateByCid( $strName, $strFilePath, $strActivateOn, $boolIsPrivate, $intCid, $objDatabase ) {

		if( true == $boolIsPrivate ) {
			$strFilePath = 'report_templates/' . $strFilePath;
			$boolIsSystem = 0;
		} else {
			$boolIsSystem = 1;
			$intCid = CClient::ID;
		}

		$strSql = 'SELECT
						d.*
					FROM
						documents d
					WHERE
						d.name = \'' . $strName . '\'
						AND d.file_path = \'' . $strFilePath . '\'
					    AND d.is_system = ' . $boolIsSystem . '
					    AND d.deleted_on IS NULL
					    AND d.is_published = 1
						AND d.document_sub_type_id = ' . ( int ) CDocumentSubType::REPORTS . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.document_type_id = ' . ( int ) CDocumentType::FILES;

		$strSql .= ( '' != $strActivateOn ) ? ' AND d.id IN ( SELECT
																	d.id
															  FROM
																	documents d
															  WHERE
																	d.activate_on = \'' . $strActivateOn . '\' AND d.cid = ' . ( int ) $intCid . ' )' : '
												AND d.deactivate_on IS NULL';
		$strSql .= ' LIMIT 1';

		return CDocuments::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchCommonLeaseDocumentsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolOtherEsignDocument = false, $intChecklistItemId = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strChecklistJoin = '';
		if( true == valId( $intChecklistItemId ) ) {
			$strChecklistJoin = ' JOIN checklist_items ci ON ( ci.cid = d.cid AND ci.document_id = d.id AND ci.id = ' . ( int ) $intChecklistItemId . ' ) ';
		}

		$intPropertyCount = \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds );

		if( true == $boolOtherEsignDocument ) {
			$arrintDocumentSubTypeIds = [ CDocumentSubType::LEASE_AGREEMENT, CDocumentSubType::DOCUMENT_TEMPLATE ];
			$arrintDocumentAssociationTypeIds = [ CDocumentAssociationType::PACKET, CDocumentAssociationType::DOCUMENT_TEMPLATE ];
		} else {
			$arrintDocumentSubTypeIds = [ CDocumentSubType::LEASE_AGREEMENT, CDocumentSubType::BLUE_MOON_LEASE_AGREEMENT ];
			$arrintDocumentAssociationTypeIds = [ CDocumentAssociationType::PACKET ];
		}

		$strSql = 'WITH common_docs As ( SELECT
											d.cid, d.id AS document_id
										 FROM
											documents d
											JOIN property_group_documents pgd ON ( pgd.cid = d.cid AND pgd.document_id = d.id )
											JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
											JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid AND pgd.cid = ' . ( int ) $intCid . ' )
											' . $strChecklistJoin . '
										WHERE
											pga.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
											AND pgd.document_association_type_id IN ( ' . implode( ', ', $arrintDocumentAssociationTypeIds ) . ' )
											AND ( ( d.document_sub_type_id IN ( ' . implode( ', ', $arrintDocumentSubTypeIds ) . ' ) AND d.file_name IS NOT NULL AND d.file_path IS NOT NULL )
										    		OR d.transmission_vendor_id <> ' . CTransmissionVendor::TENANT_TECH . '
												)
										    AND d.archived_on IS NULL
										    AND d.deleted_on IS NULL
										    AND d.is_published = 1
										    AND ( d.activate_on <= NOW() OR d.activate_on IS NULL)
										   	AND ( d.deactivate_on >= NOW() OR d.deactivate_on IS NULL )
										GROUP BY
											d.id,
											d.cid,
											d.document_id HAVING count( DISTINCT pga.property_id ) = ' . ( int ) $intPropertyCount . ' )

					SELECT
						d.*,
						da.require_sign
					FROM
						documents d
						JOIN common_docs cd ON ( d.id = cd.document_id and d.cid = cd.cid )
						LEFT JOIN document_addendas da ON ( d.id = da.document_id AND d.cid = da.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
					ORDER BY
						' . $objDatabase->getCollateSort( 'd.name', true );
		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchCommonLeaseDocumentsByPropertyIdsExcludingFileTypeSyatemCodesByCid( $arrintPropertyIds, $arrstrFileTypeSystemCodes, $intCid, $objDatabase, $boolOtherEsignDocument = false, $intChecklistItemId = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strChecklistJoin = '';
		if( true == valId( $intChecklistItemId ) ) {
			$strChecklistJoin = ' JOIN checklist_items ci ON ( ci.cid = d.cid AND ci.document_id = d.id AND ci.id = ' . ( int ) $intChecklistItemId . ' ) ';
		}

		$intPropertyCount = \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds );

		if( true == $boolOtherEsignDocument ) {
			$arrintDocumentSubTypeIds = [ CDocumentSubType::LEASE_AGREEMENT, CDocumentSubType::DOCUMENT_TEMPLATE ];
			$arrintDocumentAssociationTypeIds = [ CDocumentAssociationType::PACKET, CDocumentAssociationType::DOCUMENT_TEMPLATE ];
		} else {
			$arrintDocumentSubTypeIds = [ CDocumentSubType::LEASE_AGREEMENT, CDocumentSubType::BLUE_MOON_LEASE_AGREEMENT ];
			$arrintDocumentAssociationTypeIds = [ CDocumentAssociationType::PACKET ];
		}

		$strSql = 'WITH common_docs As ( SELECT
											d.cid, d.id AS document_id
										 FROM
											documents d
											JOIN property_group_documents pgd ON ( pgd.cid = d.cid AND pgd.document_id = d.id )
											JOIN property_group_associations pga ON ( pga.property_group_id = pgd.property_group_id AND pga.cid = pgd.cid AND pgd.cid = ' . ( int ) $intCid . ' )
											JOIN file_types ft ON ( ft.cid = d.cid AND d.file_type_id = ft.id AND ft.cid = ' . ( int ) $intCid . ' )
											' . $strChecklistJoin . '
										WHERE
											pga.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
											AND pgd.document_association_type_id IN ( ' . implode( ', ', $arrintDocumentAssociationTypeIds ) . ' )
											AND ( ( d.document_sub_type_id IN ( ' . implode( ', ', $arrintDocumentSubTypeIds ) . ' ) AND d.file_name IS NOT NULL AND d.file_path IS NOT NULL )
										    		OR d.is_bluemoon = true
												)
											AND ft.system_code NOT IN ( \'' . implode( '\',\'', $arrstrFileTypeSystemCodes ) . '\' )
										    AND d.archived_on IS NULL
										    AND d.deleted_on IS NULL
										    AND d.is_published = 1
										    AND ( d.activate_on <= NOW() OR d.activate_on IS NULL)
										   	AND ( d.deactivate_on >= NOW() OR d.deactivate_on IS NULL )
										GROUP BY
											d.id,
											d.cid,
											d.document_id HAVING count( DISTINCT pga.property_id ) = ' . ( int ) $intPropertyCount . ' )

					SELECT
						d.*,
						da.require_sign
					FROM
						documents d
						JOIN common_docs cd ON ( d.id = cd.document_id and d.cid = cd.cid )
						LEFT JOIN document_addendas da ON ( d.id = da.document_id AND d.cid = da.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
					ORDER BY
						' . $objDatabase->getCollateSort( 'd.name', true );

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchCommonLeaseDocumentsByPropertyIdsByCidNew( $arrintPropertyIds, $intCid, $objDatabase, $boolOtherEsignDocument = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$intPropertyCount = \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds );

		if( true == $boolOtherEsignDocument ) {
			$arrintDocumentSubTypeIds = [ CDocumentSubType::LEASE_AGREEMENT, CDocumentSubType::DOCUMENT_TEMPLATE ];
			$arrintDocumentAssociationTypeIds = [ CDocumentAssociationType::PACKET, CDocumentAssociationType::DOCUMENT_TEMPLATE ];
		} else {
			$arrintDocumentSubTypeIds = [ CDocumentSubType::LEASE_AGREEMENT, CDocumentSubType::BLUE_MOON_LEASE_AGREEMENT ];
			$arrintDocumentAssociationTypeIds = [ CDocumentAssociationType::PACKET ];
		}

		$strSql = 'WITH common_docs As ( SELECT
											d.cid, d.id AS document_id
										 FROM
											documents d
											JOIN property_group_documents pgd ON ( pgd.cid = d.cid AND pgd.document_id = d.id )
											JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
											JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid AND pgd.cid = ' . ( int ) $intCid . ' )
										WHERE
											pga.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
											AND pgd.document_association_type_id IN ( ' . implode( ', ', $arrintDocumentAssociationTypeIds ) . ' )
											AND ( ( d.document_sub_type_id IN ( ' . implode( ', ', $arrintDocumentSubTypeIds ) . ' ) AND d.file_name IS NOT NULL AND d.file_path IS NOT NULL )
										    		OR d.is_bluemoon = true
												)
										    AND d.archived_on IS NULL
										    AND d.deleted_on IS NULL
										    AND d.is_published = 1
										    AND ( d.activate_on <= NOW() OR d.activate_on IS NULL)
										   	AND ( d.deactivate_on >= NOW() OR d.deactivate_on IS NULL )
										GROUP BY
											d.id,
											d.cid,
											d.document_id HAVING count( DISTINCT pga.property_id ) = ' . ( int ) $intPropertyCount . ' )

					SELECT
						DISTINCT d.*,
						da.require_sign
					FROM
						documents d
						JOIN common_docs cd ON ( d.id = cd.document_id and d.cid = cd.cid )
						LEFT JOIN document_addendas da ON ( d.id = da.document_id AND d.cid = da.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
					ORDER BY
						' . $objDatabase->getCollateSort( 'd.name', true );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDocumentSubTypeIdByCidByFileId( $intCid, $intFileId, $objDatabase ) {

		if( false == is_numeric( $intCid ) ) return NULL;
		if( false == is_numeric( $intFileId ) ) return NULL;

		$strSql = 'SELECT
						d.document_sub_type_id
					FROM
						files f
						JOIN documents d ON ( f.cid = d.cid AND f.document_id = d.id )
					WHERE
						f.cid = ' . ( int ) $intCid . '
						AND f.id = ' . ( int ) $intFileId;

		return self::fetchColumn( $strSql, 'document_sub_type_id', $objDatabase );
	}

	public static function fetchDocumentTypeIdByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intCid ) ) return NULL;
		if( false == is_numeric( $intDocumentId ) ) return NULL;

		$strSql = 'SELECT
						is_bluemoon
					FROM
						documents
					WHERE
						id = ' . ( int ) $intDocumentId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'is_bluemoon', $objDatabase );
	}

	public static function fetchDocumentNameByFieldNameByCidById( $intId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intCid ) ) return NULL;
		if( false == is_numeric( $intId ) ) return NULL;

		$strSql = 'SELECT
						d.name
					FROM
						documents d
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.id = ' . ( int ) $intId;

		return self::fetchColumn( $strSql, 'name', $objDatabase );
	}

	public static function fetchDocumentCount( $strWhere, $objDatabase, $strFrom = 'documents' ) {
		return parent::fetchRowCount( $strWhere, $strFrom, $objDatabase );
	}

	public static function fetchPropertiesCountByDocumentIdsByCid( $arrintDocumentIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintDocumentIds ) ) return NULL;

		$strSql = 'SELECT
						d.id,
						count( DISTINCT( pga.property_id ) ) AS properties_count
					FROM
						documents d
						JOIN property_group_documents pgd ON ( pgd.document_id = d.id AND pgd.cid = d.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pg.id = pga.property_group_id AND pgd.cid = pga.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
					    AND d.document_id IS NULL
					    AND d.archived_on IS NULL
					    AND d.deleted_by IS NULL
					GROUP BY
						d.id';

		$arrintResponses = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintResponses ) ) return [];

		foreach( $arrintResponses as $arrintResponse ) {
			$arrmixDocumentPropertiesCount[$arrintResponse['id']] = $arrintResponse['properties_count'];
		}

		return $arrmixDocumentPropertiesCount;
	}

	public static function fetchPublishedActiveDocumentsByPropertyIdsByDocumentTypeIdByDocumentAssociationTypeIdByCid( $arrintPropertyIds, $intDocumentTypeId, $intCid, $objDatabase, $arrintLeaseIntervalIds = [] ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						d.*
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND d.cid = ' . ( int ) $intCid . '

						AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.archived_on IS NULL
						AND d.is_published = 1
						AND d.deleted_by IS NULL
						AND ( d.activate_on <= NOW() OR d.activate_on IS NULL )
						AND ( d.deactivate_on >= NOW() OR d.deactivate_on IS NULL )';

		if( true == valArr( $arrintLeaseIntervalIds ) ) {
			$strSql .= ' AND ( \'{ ' . implode( ',', $arrintLeaseIntervalIds ) . ' }\' && d.lease_interval_type_ids )';
		}

		$strSql .= ' ORDER BY
							' . $objDatabase->getCollateSort( 'd.name', true ) . ',
							pgd.order_num,
							pgd.id';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchDuplicateDocumentNameCountByExcludingDocumentIdsByFileTypeIdByCid( $arrintExcludingDocumentIds, $objConflictingDocument, $objDatabase, $intDocumentTypeId = NULL, $intDocumentSubTypeId = NULL, $intFileTypeId = NULL, $boolCheckDeletedBy = false, $boolCheckIsArchived = false ) {

		$strWhereSql = 'WHERE cid = ' . ( int ) $objConflictingDocument->getCid();

		if( true == valArr( $arrintExcludingDocumentIds ) ) {
			$strWhereSql .= ' AND id NOT IN ( ' . implode( ',', $arrintExcludingDocumentIds ) . ') ';
		}

		if( true == valId( $intFileTypeId ) ) {
			$strWhereSql .= ' AND file_type_id = ( ' . $intFileTypeId . ') ';
		}

		$strWhereSql .= ' AND lower(name) = \'' . ( string ) addslashes( \Psi\CStringService::singleton()->strtolower( $objConflictingDocument->getName() ) ) . '\'';

		if( true == $boolCheckDeletedBy ) {
			$strWhereSql .= ' AND deleted_by IS NULL ';
		}

		if( true == $boolCheckIsArchived ) {
			$strWhereSql .= ' AND archived_on IS NULL';
		}

		if( CDocumentType::PACKET == $intDocumentTypeId ) {

			$strWhereSql .= ' AND document_type_id = ' . ( int ) $intDocumentTypeId;
		} elseif( CDocumentType::MERGE_DOCUMENT == $intDocumentTypeId && CDocumentSubType::DOCUMENT_TEMPLATE == $intDocumentSubTypeId ) {

			$strWhereSql .= ' AND document_type_id = ' . ( int ) $intDocumentTypeId . ' AND document_sub_type_id = ' . ( int ) $intDocumentSubTypeId;
		} else {

			$strWhereSql .= ' AND document_sub_type_id = ' . CDocumentSubType::DOCUMENT_TEMPLATE;
		}

		$strWhereSql .= ' LIMIT 1 ';

		return self::fetchDocumentCount( $strWhereSql, $objDatabase );
	}

	public static function fetchLeaseDocumentsByIdsByMasterDocumentIdByCid( $arrintDocumentIds, $intMasterDocumentId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintDocumentIds ) ) return NULL;

		$strSql = 'SELECT
						d.*,
						da.id as document_addenda_id,
						da.master_document_id,
						da.external_key,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional,
						da.is_household,
						da.exclude_other_signers_info,
						da.require_sign,
						da.order_num as document_addenda_order_num,
						da.last_sync_on as last_sync_on,
						da.require_countersign as require_countersign
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid AND da.master_document_id = ' . ( int ) $intMasterDocumentId . ' )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
						AND da.deleted_on IS NULL
					ORDER BY da.order_num';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchLeaseDocumentByIdByCid( $intId, $intCid, $objDatabase, $boolIsDocumentTemplate = false, $intMasterDocumentId = NULL, $boolFetchSystemCode = false ) {

		$strSelectSql = '';
		$strJoinSql   = '';
		$strWhere     = '';

		if( true == $boolFetchSystemCode ) {
			$strSelectSql = ',ft.system_code';
			$strJoinSql   = 'LEFT JOIN file_types ft ON ( ft.cid = d.cid AND ft.id = d.file_type_id )';
		}

		$strSql = 'SELECT
						d.*,
						da.id as document_addenda_id,
						da.master_document_id,
						da.external_key,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional,
						da.is_household,
						da.is_auto_generate,
						da.require_sign,
						da.exclude_other_signers_info,
						da.order_num as document_addenda_order_num,
						da.last_sync_on as last_sync_on,
						da.require_countersign,
						da.countersign_company_group_ids,
						da.show_in_portal
						' . $strSelectSql . '
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid )  
						' . $strJoinSql . ' 
						WHERE ';

		if( true == $boolIsDocumentTemplate ) {
			if( true == valId( $intMasterDocumentId ) ) {
				$strWhere .= ' AND da.master_document_id = ' . ( int ) $intMasterDocumentId;
			} else {
				$strWhere .= ' AND CASE WHEN d.transmission_vendor_id = ' . CTransmissionVendor::BLUE_MOON_LEASE . ' THEN TRUE ELSE da.master_document_id IS NULL END ';
			}
		}

		$strSql .= 'd.cid = ' . ( int ) $intCid . '
					' . $strWhere . ' 
					AND d.id = ' . ( int ) $intId . '
					AND da.deleted_by IS NULL';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchLeaseDocumentTemplatesByIdsByCid( $arrintDocumentIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintDocumentIds ) ) return NULL;

		$strSql = 'SELECT
						d.*,
						da.id as document_addenda_id,
						da.master_document_id,
						da.external_key,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional,
						da.is_household,
						da.is_auto_generate,
						da.require_sign,
						da.exclude_other_signers_info,
						da.order_num as document_addenda_order_num,
						da.last_sync_on as last_sync_on,
						da.require_countersign,
						da.countersign_company_group_ids,
						da.show_in_portal
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid )';

		$strSql .= ' WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.id IN ( ' . implode( ',', $arrintDocumentIds ) . ' ) 
						AND da.deleted_by IS NULL
						AND da.master_document_id IS NULL';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

    public static function fetchLeaseDocumentTemplateByIdByCid( $intDocumentId, $intCid, $objDatabase ) {

        if( false == valId( $intDocumentId ) ) return NULL;

        $strSql = 'SELECT
						d.*,
						da.id as document_addenda_id,
						da.master_document_id,
						da.external_key,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional,
						da.is_household,
						da.is_auto_generate,
						da.require_sign,
						da.exclude_other_signers_info,
						da.order_num as document_addenda_order_num,
						da.last_sync_on as last_sync_on,
						da.require_countersign,
						da.countersign_company_group_ids,
						da.show_in_portal,
						ft.name as file_type_name
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid AND da.master_document_id IS NULL )
						LEFT JOIN file_types ft ON ( d.cid = ft.cid AND d.file_type_id = ft.id)
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.id = ' . ( int ) $intDocumentId . '
						AND d.deleted_on IS NULL
						AND da.deleted_by IS NULL';

        return self::fetchDocument( $strSql, $objDatabase );
    }

	public static function fetchLeaseDocumentPacketByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.*
					FROM
						documents d
		 			WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.id = ' . ( int ) $intId . '
						AND d.document_type_id = ' . CDocumentType::PACKET;
		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchLeaseDocumentsByLeaseDocumentPacketIdByCid( $intMasterDocumentId, $intCid, $objDatabase, $boolIsPublished = true, $boolIsCheckedHidden = false, $boolIsActiveTemplate = false ) {

		$strSql = 'SELECT
						d.*,
						da.external_key,
						da.id AS document_addenda_id,
						da.document_id,
						da.master_document_id,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional,
						da.is_published as document_addenda_is_published,
						da.is_default_selected,
						da.is_household,
						da.exclude_other_signers_info,
						da.order_num as document_addenda_order_num,
						da.last_sync_on as last_sync_on,
						da.countersign_company_group_ids,
						da.is_hidden,
						da.require_sign,
						ft.system_code
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid AND da.master_document_id = ' . ( int ) $intMasterDocumentId . ' )
						LEFT JOIN file_types ft ON ( ft.id = d.file_type_id AND ft.cid = d.cid  )
					WHERE
						d.cid = ' . ( int ) $intCid;

		if( true == $boolIsPublished ) {
			$strSql .= ' AND da.is_published = 1 ';
		}

		if( true == $boolIsCheckedHidden ) {
			$strSql .= ' AND da.is_hidden = false ';
		}

		if( true == $boolIsActiveTemplate ) {
			$strSql .= ' AND ( ( d.activate_on IS NULL and d.deactivate_on IS NULL) or ( d.activate_on IS NULL and d.deactivate_on >= now()::date ) or ( d.activate_on <= now()::date and d.deactivate_on IS NULL ) or ( d.activate_on <= now()::date and d.deactivate_on >= now()::date ) ) ';
		}

		$strSql .= ' AND d.archived_on IS NULL
				AND d.deleted_by IS NULL
				AND da.deleted_by IS NULL
				ORDER BY da.order_num, da.created_on';
		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchLeaseDocumentsByMasterDocumentIdsByCid( $arrintMasterDocumentIds, $intCid, $objDatabase, $boolOnlyChildDocuments = false, $boolIsCheckedHidden = false, $boolExternalTemplatesOnly = false ) {

		if( false == valArr( $arrintMasterDocumentIds ) ) return NULL;

		$strSql = 'SELECT
						d.*,
						da.external_key,
						da.id as document_addenda_id,
						da.master_document_id,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional,
						da.is_published as document_addenda_is_published,
						da.is_default_selected,
						da.is_household,
						da.order_num as document_addenda_order_num,
						da.last_sync_on as last_sync_on
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid AND da.master_document_id IN ( ' . implode( ',', $arrintMasterDocumentIds ) . ' ) )
					WHERE
						d.cid = ' . ( int ) $intCid;

		if( true == $boolOnlyChildDocuments ) {
			$strSql .= ' AND da.document_id NOT IN ( ' . implode( ',', $arrintMasterDocumentIds ) . ' ) ';
		}

        if( true == $boolExternalTemplatesOnly ) {
            $strSql .= ' AND d.transmission_vendor_id IS NOT NULL';
        }

		if( true == $boolIsCheckedHidden ) {
			$strSql .= ' AND da.is_hidden = true ';
		}

		$strSql .= ' AND d.archived_on IS NULL
				AND d.deleted_by IS NULL
				AND da.archived_on IS NULL
				AND da.deleted_by IS NULL
				AND da.is_published = 1
				ORDER BY da.order_num, da.created_on';

		return self::fetchDocuments( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchDocumentByExcludingDocumentIdsByDocumentTypeIdByDocumentSubTypeIdByCid( $arrintExcludingDocumentIds, $intDocumentTypeId, $intDocumentSubTypeId, $intCid, $objDatabase, $strWhere = NULL ) {

		$strSql = 'SELECT
						d.*
					FROM
						documents d
					WHERE
						d.cid = ' . ( int ) $intCid . '
					    AND d.document_type_id = ' . ( int ) $intDocumentTypeId;

		if( true == valArr( $arrintExcludingDocumentIds ) ) {
			$strSql .= ' AND d.id NOT IN ( ' . implode( ',', $arrintExcludingDocumentIds ) . ') ';
		}

		if( false == is_null( $intDocumentSubTypeId ) ) {
			$strSql .= '  AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . ' AND d.is_packet = true';
		}

		if( false == is_null( $strWhere ) ) {
			$strSql .= ' ' . $strWhere;
		}

		if( CDocumenttype::MERGE_DOCUMENT == $intDocumentTypeId && CDocumentSubType::DOCUMENT_TEMPLATE == $intDocumentSubTypeId ) {
			$strSql .= ' AND d.is_bluemoon = false';
		}

		$strSql .= ' AND d.deleted_on IS NULL
					     AND d.archived_on IS NULL
						 AND d.is_published = 1
						 ORDER BY ' . $objDatabase->getCollateSort( 'd.name', true );

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchLeaseDocumentHistoryByOriginalDocumentIdByCid( $intOriginalDocumentId, $intCid, $objDatabase ) {

		if( true == is_null( $intOriginalDocumentId ) ) return NULL;

		$strSql = ' SELECT d.*,
                        da.id as document_addenda_id,
						CONCAT ( ce.name_first, \' \', ce.name_last) as created_by_username
                    FROM
						documents d
                    JOIN
						document_addendas da ON ( d.cid = da.cid AND d.id = da.document_id )
						LEFT JOIN company_users cu ON ( cu.cid = d.cid AND cu.id = d.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
                    WHERE
						d.cid = ' . ( int ) $intCid . '
                    	AND ( d.id = ' . ( int ) $intOriginalDocumentId . ' OR d.original_document_id = ' . ( int ) $intOriginalDocumentId . ' )
                    	AND d.deleted_on IS NULL
                    ORDER BY d.id DESC';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchDocumentPacketByIdByCid( $intId,$intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						documents d
					WHERE
						d.id = ' . ( int ) $intId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.deleted_by IS NULL
						AND d.archived_on IS NULL';

		return self::fetchDocument( $strSql, $objDatabase );

	}

	public static function fetchCustomeDocumentsByDocumentTypeIdByCid( $intDocumentTypeId, $intCid, $objDatabase, $strSortBy = 'name', $strSortDirection = 'asc' ) {

		if( 'name' == $strSortBy || 'title' == $strSortBy || 'description' == $strSortBy || 'keywords' == $strSortBy ) {

			$strOrderBy = $objDatabase->getCollateSort( ' d.' . $strSortBy . ' ', true );

		} else {
			$strOrderBy = 'd.' . $strSortBy;
		}

		$strSql = 'SELECT
						*
					FROM
						documents d
					WHERE
						d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.deleted_by IS NULL
						AND d.archived_on IS NULL
						AND d.is_entrata_default = false
					ORDER BY ' . $strOrderBy . ' ' . $strSortDirection;

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchDocumentPacketsByDocumentsFilter( $intCid, $objDatabase, $objDocumentsFilter, $boolSqlOnly = false, $strSortBy = 'name', $strSortDirection = 'asc' ) {

		if( 'name' == $strSortBy || 'title' == $strSortBy || 'description' == $strSortBy || 'keywords' == $strSortBy ) {
			$strOrderBy = $objDatabase->getCollateSort( 'd.' . $strSortBy . ' ', true );
		} else {
			$strOrderBy = 'd.' . $strSortBy;
		}
		$strWhereCondition 	= '';
		$strWhereCondition .= ( true == valStr( $objDocumentsFilter->getDocumentName() ) ) ? " AND LOWER (d.name) LIKE LOWER ('%" . addslashes( $objDocumentsFilter->getDocumentName() ) . "%') " : '';
		$strWhereCondition .= ( true == valArr( $objDocumentsFilter->getArTriggerIds() ) ) ? ( true == valArr( $objDocumentsFilter->getLeaseIntervalTypeIds() ) ) ? ' AND ( d.ar_trigger_id IN ( ' . implode( ',', $objDocumentsFilter->getArTriggerIds() ) . ' ) ' : ' AND d.ar_trigger_id IN ( ' . implode( ',', $objDocumentsFilter->getArTriggerIds() ) . ' ) ' : '';

		if( true == valArr( $objDocumentsFilter->getLeaseIntervalTypeIds() ) ) {

			$strWhereCondition .= ( true == valArr( $objDocumentsFilter->getArTriggerIds() ) ) ? ' OR ( ' : ' AND ( ';

			$strWhereCondition .= 'd.ar_trigger_id = ' . CArTrigger::LEASE_STARTED . " AND ( '{ " . implode( ',', $objDocumentsFilter->getLeaseIntervalTypeIds() ) . " }' && d.lease_interval_type_ids ) )";

			$strWhereCondition .= ( true == valArr( $objDocumentsFilter->getArTriggerIds() ) ) ? ' ) ' : ' ';
		}

		$strSql	= 'SELECT
							d.*
						FROM
							documents d ';

		if( true == valArr( $objDocumentsFilter->getPropertyIds() ) ) {
			$strSql .= ' LEFT JOIN property_group_documents pgd ON ( pgd.document_id = d.id AND pgd.cid = d.cid)
	    					 LEFT JOIN property_groups pg ON ( pgd.property_group_id = pg.id AND pgd.cid = pg.cid AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL ) ';

			$strWhereCondition .= ' AND pg.id IN ( ' . implode( ',', $objDocumentsFilter->getPropertyIds() ) . ' ) ';
		}

		if( true == $objDocumentsFilter->getIsExcludeEntrataDefault() ) {
			$strWhereCondition .= ' AND d.is_entrata_default = false ';
		}

		$strSql	.= 'WHERE
						d.document_type_id = ' . ( int ) $objDocumentsFilter->getDocumentTypeId() . '
						AND d.cid = ' . ( int ) $intCid . $strWhereCondition . '
						AND d.deleted_by IS NULL
						AND d.archived_on IS NULL
					ORDER BY ' . $strOrderBy . ' ' . $strSortDirection;

		return ( true === $boolSqlOnly ) ? $strSql : self::fetchDocuments( $strSql, $objDatabase );

	}

	public static function fetchLeaseDocumentsByLeaseDocumentPacketIdByExcludingDocumentIdByCid( $intMasterDocumentId, $intExcludingDocumentId, $intCid, $objDatabase, $boolIsPublished = true ) {

		$strSql = 'SELECT
						d.*,
						da.external_key,
						da.id AS document_addenda_id,
						da.master_document_id,
						da.document_id,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional,
						da.is_published as document_addenda_is_published,
						da.is_default_selected,
						da.is_household,
						da.exclude_other_signers_info,
						da.order_num as document_addenda_order_num,
						da.last_sync_on as last_sync_on,
						da.countersign_company_group_ids,
						da.is_hidden
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid AND da.master_document_id = ' . ( int ) $intMasterDocumentId . ' )
					WHERE
						d.cid = ' . ( int ) $intCid;

		if( true == $boolIsPublished ) {
			$strSql .= ' AND da.is_published = 1 ';
		}

		$strSql .= ' AND d.id != ' . ( int ) $intExcludingDocumentId . '
				AND d.archived_on IS NULL
				AND d.deleted_by IS NULL
				AND da.deleted_by IS NULL
				ORDER BY da.order_num, da.created_on';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchLastInsertedLeaseDocumentTemplateIdDocumentTypeIdDocumentSubTypeIdByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.id
					FROM
						documents d
						JOIN document_addendas da ON (d.cid =  da.cid AND d.id = da.document_id)
					WHERE
						da.master_document_id IS NULL
						AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
						AND d.cid = ' . ( int ) $intCid . '
					ORDER BY
						id desc LIMIT 1';

		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchLeaseDocumentsByDocumentAddendaIdsByCid( $arrintDocumentAddendaIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintDocumentAddendaIds ) ) return NULL;

		$strSql = 'SELECT
						d.*,
						da.external_key,
						da.id AS document_addenda_id,
						da.master_document_id,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional,
						da.is_published as document_addenda_is_published,
						da.is_default_selected,
						da.is_household,
						da.exclude_other_signers_info,
						da.order_num as document_addenda_order_num,
						da.last_sync_on as last_sync_on,
						da.countersign_company_group_ids,
						da.is_hidden,
						da.require_sign
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid )
					WHERE
						da.id IN ( ' . implode( ',', $arrintDocumentAddendaIds ) . ' )
						AND da.cid = ' . ( int ) $intCid;

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchAllDocumentsByDocumentTypeIdByDocumentSubTypeIdByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT id
					FROM
						documents
					WHERE
						deleted_by IS NULL 
						AND deleted_on IS NULL
						AND 
						( archived_on IS NULL OR ( original_document_id IS NULL AND document_sub_type_id IN (' . CDocumentSubType::DOCUMENT_TEMPLATE . ',' . CDocumentSubType::ELECTRONIC_SIGNATURE_ACT . ' ) ) )
						AND cid = ' . ( int ) $intCid;
		$arrintData = fetchData( $strSql, $objDatabase );
		$arrstrDocumentIds = [];
		if( true == $arrintData ) {
			foreach( $arrintData as $arrintDocumentId ) {
				$arrstrDocumentIds[] = $arrintDocumentId['id'];
			}
		}
		return $arrstrDocumentIds;
	}

	public static function fetchBluemoonLeasePacketsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (d.id) d.*
					FROM
						documents d
						JOIN property_group_documents pgd ON ( pgd.document_id = d.id AND pgd.cid = d.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						d.document_type_id = ' . CDocumentType::PACKET . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.is_bluemoon = true
						AND d.deleted_by IS NULL
						AND d.deleted_on IS NULL
						';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchSystemDocumentByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.*,
						da.id as document_addenda_id,
	                    ft.system_code,
						util_get_translated( \'name\', ft.name, ft.details ) as file_type_name,
						da.is_auto_generate
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid AND da.master_document_id IS NULL)
	                    JOIN file_types ft ON ( d.file_type_id = ft.id AND ft.cid = d.cid )
					WHERE
						d.id = ' . ( int ) $intId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.archived_on IS NULL
						AND d.deleted_by IS NULL';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchSystemDocumentsByFileTypeSystemCodesByCid( $arrstrSystemCodes, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.*,
						da.require_sign,
						util_get_translated( \'name\', ft.name, ft.details ) as file_type_name,
						count( p.* ) as properties_count
					FROM
						documents d
					    JOIN document_addendas da ON ( d.id = da.document_id AND da.cid = d.cid )
						LEFT JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND pgd.cid = d.cid AND pgd.document_association_type_id = ' . CDocumentAssociationType::SYSTEM_TEMPLATE . ' )
						LEFT JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN properties p ON ( pgd.property_group_id = p.id AND pgd.cid = p.cid AND p.is_disabled = 0 AND p.is_managerial = 0 )
					    JOIN file_types ft ON ( d.file_type_id = ft.id  AND ft.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
					    AND d.is_system = 1
						AND d.deleted_by IS NULL
						AND d.deleted_on IS NULL
						AND d.archived_on IS NULL
					    AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )
					GROUP BY
					    d.id,
					    d.cid,
					    ft.name,
					    da.require_sign,
					    ft.details
					ORDER BY
					    d.file_type_id ASC,
					    d.is_entrata_default DESC,
					    ' . $objDatabase->getCollateSort( 'd.name ', true ) . ' ASC';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchCrpDocumentsByFileNameByByCid( $strFileName, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.*,
						da.require_sign,
						ft.name as file_type_name,
						count( p.* ) as properties_count
					FROM
						documents d
					    JOIN document_addendas da ON ( d.id = da.document_id AND da.cid = d.cid )
						LEFT JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND pgd.cid = d.cid AND pgd.document_association_type_id = ' . CDocumentAssociationType::SYSTEM_TEMPLATE . ' )
						LEFT JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN properties p ON ( pgd.property_group_id = p.id AND pgd.cid = p.cid AND p.is_disabled = 0 AND p.is_managerial = 0 )
					    JOIN file_types ft ON ( d.file_type_id = ft.id  AND ft.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
					    AND d.is_system = 1
						AND d.deleted_by IS NULL
						AND d.deleted_on IS NULL
						AND d.archived_on IS NULL
					    AND ft.system_code = \'' . CFileType::SYSTEM_CODE_CERTIFICATE_OF_RENT_PAID . '\'
					    AND d.file_name = \'' . $strFileName . '\'
					GROUP BY
					    d.id,
					    d.cid,
					    ft.name,
					    da.require_sign
					ORDER BY
					    d.file_type_id ASC,
					    d.is_entrata_default DESC,
					    ' . $objDatabase->getCollateSort( 'd.name ', true ) . ' ASC';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchCRPDocumentByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						d.*,
						da.require_sign
					FROM
						documents d
					    JOIN document_addendas da ON ( d.id = da.document_id AND da.cid = d.cid )
						LEFT JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND pgd.cid = d.cid AND pgd.document_association_type_id = ' . CDocumentAssociationType::SYSTEM_TEMPLATE . ' )
						LEFT JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN properties p ON ( pgd.property_group_id = p.id AND pgd.cid = p.cid AND p.is_disabled = 0 AND p.is_managerial = 0 )
					    JOIN file_types ft ON ( d.file_type_id = ft.id  AND ft.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND pgd.property_group_id IN (' . implode( ',', $arrintPropertyIds ) . ')
					    AND d.is_system = 1
						AND d.deleted_by IS NULL
						AND d.deleted_on IS NULL
						AND d.archived_on IS NULL
					    AND ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_CERTIFICATE_OF_RENT_PAID . '\' )    
					GROUP BY
					    d.id,
					    d.cid,
					    da.require_sign';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchMilitaryDocumentByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						d.*,
						da.require_sign
					FROM
						documents d
					    JOIN document_addendas da ON ( d.id = da.document_id AND da.cid = d.cid )
						LEFT JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND pgd.cid = d.cid AND pgd.document_association_type_id = ' . CDocumentAssociationType::SYSTEM_TEMPLATE . ' )
						LEFT JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN properties p ON ( pgd.property_group_id = p.id AND pgd.cid = p.cid AND p.is_disabled = 0 AND p.is_managerial = 0 )
					    JOIN file_types ft ON ( d.file_type_id = ft.id  AND ft.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND pgd.property_group_id IN (' . implode( ',', $arrintPropertyIds ) . ')
					    AND d.is_system = 1
						AND d.deleted_by IS NULL
						AND d.deleted_on IS NULL
						AND d.archived_on IS NULL
					    AND ft.system_code IN ( \'' . CFileType::SYSTEM_CODE_MILITARY_DD_1746 . '\' )    
					GROUP BY
					    d.id,
					    d.cid,
					    da.require_sign';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchSystemDocumentsByFileTypeSystemCodesByPropertyIdByCid( $arrstrSystemCodes, $intCid, $intPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						d.*,
						util_get_translated( \'name\', ft.name, ft.details ) as file_type_name
					FROM
						documents d
					    JOIN document_addendas da ON ( d.id = da.document_id AND da.cid = d.cid )
					    JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND pgd.cid = d.cid )
					    JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					    JOIN file_types ft ON ( d.file_type_id = ft.id  AND ft.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
					    AND d.is_system = 1
					    AND ft.system_code IN ( \'' . implode( '\',\'', $arrstrSystemCodes ) . '\' )
					    AND pgd.document_association_type_id = ' . CDocumentAssociationType::SYSTEM_TEMPLATE . '
					    AND pgd.property_group_id = ' . ( int ) $intPropertyId . '
					ORDER BY
					    d.file_type_id ASC,
					    d.is_entrata_default DESC';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchEntrataDefaultDocumentByFileTypeIdByCid( $intFileTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.*
					 FROM
						documents d
					WHERE
						d.file_type_id = ' . ( int ) $intFileTypeId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.is_entrata_default = true';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchSystemDocumentsByFileTypeIdByCid( $intFileTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
	                   d.*
	               FROM
	                   documents d
	               WHERE
	                   d.cid = ' . ( int ) $intCid . '
	                   AND d.file_type_id = ' . ( int ) $intFileTypeId . '
	                   AND d.archived_on IS NULL
	                   AND d.deleted_by IS NULL';

		return self::fetchDocuments( $strSql, $objDatabase );

	}

	public static function fetchPublishedAffordableDocumentsByFileTypeSystemCodeByCid( $strFileTypeSystemCode, $intCid, $objDatabase ) {
		if( false == valStr( $strFileTypeSystemCode ) ) return;

		$strSql = 'WITH document_template AS (

						SELECT
							d.id AS document_id,
							d.cid
						FROM
							documents d
							JOIN document_addendas da ON ( da.cid = d.cid AND da.document_id = d.id AND da.master_document_id IS NULL )
							JOIN file_types ft ON ( ft.cid = d.cid AND d.file_type_id = ft.id AND ft.system_code = \'' . $strFileTypeSystemCode . '\' )
						WHERE
							d.cid = ' . ( int ) $intCid . '
							AND d.document_type_id = ' . CDocumentType::MERGE_DOCUMENT . '
							AND d.document_sub_type_id = ' . CDocumentSubType::DOCUMENT_TEMPLATE . '
							AND d.is_packet
							AND	d.is_published = 1
							AND d.is_system = 1
							AND d.archived_on IS NULL
							AND ( d.activate_on IS NULL OR d.activate_on <= now() )
							AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
							AND d.deleted_by IS NULL
						LIMIT 1

					), document_packet AS (

						SELECT
							d.id as document_id,
							da.document_id AS document_template_id,
							d.cid
						FROM
							documents d
							JOIN file_types ft ON ( ft.cid = d.cid AND d.file_type_id = ft.id AND ft.system_code = \'' . $strFileTypeSystemCode . '\' )
							JOIN document_addendas da ON ( da.cid = d.cid AND da.master_document_id = d.id )
							JOIN document_template dt ON ( dt.cid = da.cid AND dt.document_id = da.document_id )
						WHERE
							d.cid = ' . ( int ) $intCid . '
							AND d.document_type_id = ' . CDocumentType::PACKET . '
							AND NOT d.is_packet
							AND	d.is_published = 1
							AND d.is_system = 1
							AND d.archived_on IS NULL
							AND ( d.activate_on IS NULL OR d.activate_on <= now() )
							AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
							AND d.deleted_by IS NULL
						LIMIT 1
					)

					SELECT
						DISTINCT ON ( d.id )
						d.*
					FROM
						documents d
						JOIN document_packet dp ON ( dp.cid = d.cid AND ( dp.document_id = d.id OR dp.document_template_id = d.id ) )
					WHERE
						d.cid = ' . ( int ) $intCid;

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchCustomDocumentsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolConditionCheck = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strJoinSql = '';
		$strCondForStudent = '';
		$strCond = ( true == $boolConditionCheck ) ? ' d.lease_interval_type_ids @>  ARRAY[' . CLeaseIntervalType::RENEWAL . '] AND ' : '';
		if( true == $boolConditionCheck ) {
			$strCondForStudent = ' ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids ) AND ';
			$strJoinSql = ' JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.key = \'ENABLE_SEMESTER_SELECTION\' AND pp.value = \'1\' ) ';
		}

		$strSql = 'SELECT
						DISTINCT pgd.property_group_id as property_id,
						(CASE WHEN pgd.property_group_id IS NOT NULL  THEN 1 ELSE 0 END ) AS value
					FROM
						documents d
						JOIN property_group_documents pgd ON ( pgd.document_id = d.id AND pgd.cid = d.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN properties p ON ( p.id = pgd.property_group_id AND p.cid = pgd.cid ) 
						' . $strJoinSql . '
					WHERE
						' . $strCond . '
						' . $strCondForStudent . '
						d.document_type_id = ' . ( int ) CDocumentType::PACKET . '
						AND d.deleted_by IS NULL
						AND d.archived_on IS NULL
						AND pgd.property_group_id  IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND d.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomDocumentAddendasByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT pgd.property_group_id as property_id,
						(CASE WHEN da.master_document_id IS NOT NULL THEN 1 ELSE NULL END ) AS value
					FROM
						documents d
						LEFT JOIN document_addendas da ON ( d.cid = da.cid AND d.id= da.master_document_id)
						JOIN property_group_documents pgd ON ( pgd.document_id = d.id AND pgd.cid = d.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						d.document_type_id = ' . ( int ) CDocumentType::PACKET . '
						AND d.is_bluemoon = true
						AND d.deleted_by IS NULL
						AND d.archived_on IS NULL
						AND pgd.property_group_id  IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND d.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDocumentsByPropertySettingKeyByDocumentSubTypeIdsInPropertyIdsByCid( $strKey, $arrintDocumentSubTypeIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintDocumentSubTypeIds ) ) return NULL;

		$strSql = 'SELECT
					    foo.document_sub_type_id,
					    foo.name AS NAME,
					    p.id AS property_id,
					    foo.property_count,
					    bar.*,
					    pp.value
					FROM
						properties p
                        LEFT JOIN property_preferences pp ON ( pp.property_id = p.id AND pp.cid = p.cid AND pp.key = \'' . $strKey . '\' )
                        LEFT JOIN
					    (
					      SELECT
					          d.document_sub_type_id,
					          dbt.name,
					          lnt.deleted_by,
					          lnt.property_group_id,
					          lnt.cid,
					          count ( lnt.id ) AS property_count
					      FROM
					          late_notice_types lnt
					          LEFT JOIN documents d ON ( d.cid = lnt.cid AND lnt.document_id = d.id AND d.deleted_by IS NULL )
					          LEFT JOIN document_sub_types dbt ON ( dbt.id = d.document_sub_type_id )
					      WHERE
					          lnt.cid = ' . ( int ) $intCid . '
					          AND d.document_type_id = ' . ( int ) CDocumentType::MERGE_DOCUMENT . '
					          AND d.document_sub_type_id IN (' . implode( ',', $arrintDocumentSubTypeIds ) . ')
					      GROUP BY
					          d.document_sub_type_id,
					          lnt.property_group_id,
					          lnt.deleted_by,
					          lnt.cid,
					          dbt.name
					    ) AS foo ON ( foo.cid = p.cid AND foo.property_group_id = p.id AND foo.deleted_by IS NULL )
					    LEFT JOIN 
					    (
					      SELECT
					          d.document_sub_type_id,
					          count ( d.id ) AS client_count
					      FROM
					          documents d
					      WHERE
					          d.document_type_id = ' . ( int ) CDocumentType::MERGE_DOCUMENT . '
					          AND d.document_sub_type_id IN (' . implode( ',', $arrintDocumentSubTypeIds ) . ')
					          AND d.cid = ' . ( int ) $intCid . '
					          AND d.deleted_by IS NULL
					      GROUP BY
					          d.document_sub_type_id
					    ) AS bar ON ( foo.document_sub_type_id = bar.document_sub_type_id )
					WHERE
						p.id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
					    p.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchQuoteSystemDocumentByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.*,
						da.id as document_addenda_id,
						da.master_document_id,
						da.external_key,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional,
						da.is_household,
						da.is_auto_generate,
						da.require_sign,
						da.exclude_other_signers_info,
						da.order_num as document_addenda_order_num,
						da.last_sync_on as last_sync_on,
						da.require_countersign,
						da.countersign_company_group_ids,
						da.show_in_portal
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid )
						JOIN property_group_documents pgd ON ( pgd.document_id = d.id AND d.cid = pgd.cid)
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN properties p ON ( p.cid = pgd.cid AND p.id = pgd.property_group_id )
						JOIN file_types ft ON ( ft.cid = d.cid AND ft.system_code = \'' . CFileType::SYSTEM_CODE_QUOTE . '\'  AND ft.id = d.file_type_id )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND p.id = ' . ( int ) $intPropertyId . '
						AND d.is_entrata_default = false';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchDocumentsByFileTypeIdByPropertyIdByCid( $arrintFileTypeIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintFileTypeIds ) ) return [];

		$strSql = 'SELECT
						d.*,
						da.require_sign,
						da.id as document_addenda_id
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
		 				JOIN document_addendas da ON ( d.id = da.document_id AND d.cid = da.cid )
					WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND d.file_type_id IN ( ' . implode( ',', $arrintFileTypeIds ) . ' )
						AND d.archived_on IS NULL
						AND d.deleted_on IS NULL
						AND d.is_published = 1
						AND ( d.activate_on <= CURRENT_DATE OR d.activate_on IS NULL )
						AND ( d.deactivate_on >= CURRENT_DATE OR d.deactivate_on IS NULL )
					ORDER BY
						' . $objDatabase->getCollateSort( 'd.name ', true ) . ',
						pgd.id';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchCommonDocumentsByFileTypeIdsByPropertyIdsByCid( $arrintFileTypeIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintFileTypeIds ) || false == valArr( $arrintPropertyIds ) ) return [];

		$intPropertyCount = \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds );

		$strSql = 'SELECT
						d.*
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
		 			WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND d.file_type_id IN ( ' . implode( ',', $arrintFileTypeIds ) . ' )
						AND d.archived_on IS NULL
						AND d.deleted_on IS NULL
						AND d.is_published = 1
						AND pgd.is_published = 1
						AND COALESCE( d.activate_on, NOW() ) <= NOW()
						AND COALESCE( d.deactivate_on, NOW() ) >= NOW()
					GROUP BY 
						d.id,
						d.cid
					HAVING count( DISTINCT pga.property_id ) = ' . ( int ) $intPropertyCount . '
					ORDER BY
						' . $objDatabase->getCollateSort( 'd.name ', true );

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchCustomDocumentsByFileTypeIdByPropertyIdByCid( $intFileTypeId, $intPropertyId, $intCid, $objDatabase, $intDocumentId = NULL ) {

		if( false == valId( $intFileTypeId ) || false == valId( $intPropertyId ) ) return NULL;

		$strCondition = ( true == valId( $intDocumentId ) ) ? ' AND d.id = ' . ( int ) $intDocumentId : '';

		$strSql = 'SELECT 
						count( d.* )
					FROM
						documents d
						JOIN property_group_documents pgd ON ( pgd.document_id = d.id AND pgd.cid = d.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND d.file_type_id = ' . ( int ) $intFileTypeId . '
						AND d.archived_on IS NULL
						AND d.deleted_on IS NULL
						AND d.is_published = 1
						AND pgd.is_published = 1
						AND ( d.activate_on <= NOW() OR d.activate_on IS NULL )
						AND ( d.deactivate_on >= NOW() OR d.deactivate_on IS NULL )
						' . $strCondition;

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchRenewalOfferSystemDocumentByPropertyIdByFileTypeIdByCid( $intPropertyId, $strFileType, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.*,
						da.id as document_addenda_id,
						da.master_document_id,
						da.external_key,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.is_optional,
						da.is_household,
						da.is_auto_generate,
						da.require_sign,
						da.exclude_other_signers_info,
						da.order_num as document_addenda_order_num,
						da.last_sync_on as last_sync_on,
						da.require_countersign,
						da.countersign_company_group_ids,
						da.show_in_portal
					FROM
						documents d
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid )
						JOIN property_group_documents pgd ON ( pgd.document_id = d.id AND d.cid = pgd.cid)
						JOIN properties p ON ( p.cid = pgd.cid AND p.id = pgd.property_group_id )
						JOIN file_types ft ON ( ft.cid = d.cid AND ft.system_code = \'' . $strFileType . '\' AND ft.id = d.file_type_id )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND p.id = ' . ( int ) $intPropertyId . '
						AND d.is_entrata_default = false';
		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchLatestPackageDocumentByCompanyApplicationIdByCid( $intCompanyApplicationId, $intCid, $objDatabase ) {

		$strSql = ' SELECT * FROM documents 
                    WHERE cid = ' . ( int ) $intCid . '
                        AND company_application_ids @> ARRAY[ ' . ( int ) $intCompanyApplicationId . ' ]
                        AND document_type_id = ' . CDocumentType::PACKET . '
                        AND document_sub_type_id = ' . CDocumentSubType::APPLICATION . '
                        AND deleted_on IS NULL
                        AND archived_on IS NULL
                    ORDER BY id DESC LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchNoticeDocumentsBySystemCodeByDocumentTypeIdByDocumentSubTypeIdByCid( $strFileTypeSystemCode, $intDocumentTypeId, $intDocumentSubTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT 
						d.id,
						d.file_type_id,
						util_get_translated( \'name\', d.name, d.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS name
					FROM 
						documents d
						JOIN document_addendas da ON ( d.cid = da.cid AND d.id = da.document_id AND da.master_document_id IS NULL ) 
						JOIN file_types ft ON ( ft.cid = d.cid AND ft.id = d.file_type_id )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND ft.system_code = \'' . $strFileTypeSystemCode . '\'
						AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
						AND d.deleted_on IS NULL
						AND d.archived_on IS NULL
					ORDER BY
						' . $objDatabase->getCollateSort( 'd.name', true );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDocumentsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		$strSql = 'select file_type_id from documents where cid = ' . ( int ) $intCid . ' and id = ' . ( int ) $intDocumentId;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDocumentsByDocumentTypeIdsByPropertyIdByCid( $arrintDocumentTypeIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintDocumentTypeIds ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						d.*
					FROM
						property_group_documents pgd 
						JOIN documents d ON (d.cid = pgd.cid AND d.id = pgd.document_id )  
						JOIN properties p ON ( p.cid = pgd.cid AND p.id = pgd.property_group_id )
					WHERE 
						d.cid = ' . ( int ) $intCid . '
						AND d.document_type_id IN ( ' . implode( ',', $arrintDocumentTypeIds ) . ' )
						AND pgd.property_group_id = ' . ( int ) $intPropertyId . '
						AND d.deleted_by IS NULL';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchBlueMoonDocumentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) ) return false;

		$strSql = 'SELECT
						pga.property_id,
						d.*
					FROM
						documents d
						JOIN property_group_documents pgd ON ( pgd.cid = d.cid AND pgd.document_id = d.id )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.is_bluemoon = true
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND d.deleted_by IS NULL
						AND d.is_published = 1';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchDocumentByFileTypeIdByOccupancyTypeIdByCid( $intFileTypeId, $intOccupancyTypeId, $intCid, $objDatabase ) {

		if( false == valId( $intFileTypeId ) || false == valId( $intOccupancyTypeId ) ) return false;

		$strSql = ' SELECT
						COUNT ( d.id ) AS count
					FROM
						documents d
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.file_type_id = ' . ( int ) $intFileTypeId . '
						AND d.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND is_system = 1
						AND d.is_entrata_default = TRUE
						AND d.is_packet = FALSE
						AND d.deleted_by IS NULL';

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchActiveDocumentsByDocumentTypeIdByDocumentSubTypeIdByDocumentAssociationTypeIdBySystemCodeByPropertyGropuIdsByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intDocumentAssociationTypeId, $strFileTypeSystemCode, $strPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.id as document_id, pga.property_group_id
					FROM
						documents d
						JOIN property_group_documents pgd ON ( d.id = pgd.document_id AND d.cid = pgd.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.property_group_id = pg.id AND pga.cid = pgd.cid )
						JOIN document_addendas da ON ( da.document_id = d.id AND da.cid = d.cid )
						JOIN file_types ft ON ( d.file_type_id = ft.id AND ft.cid = d.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
						AND ft.system_code = \'' . $strFileTypeSystemCode . '\'
						AND pga.property_group_id IN (' . implode( ',', $strPropertyIds ) . ' ) 
						AND d.is_published = 1
						AND d.archived_on IS NULL
						AND ( d.activate_on IS NULL OR d.activate_on <= now() )
						AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
						AND d.deleted_by IS NULL';

		$arrstrActions = fetchData( $strSql, $objDatabase );
		return rekeyArray( 'document_id', $arrstrActions );
	}

	public static function fetchPublishedActiveDocumentsByDocumentTypeIdByDocumentSubTypeIdByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( d.order_num, d.id ) d.*
					FROM
						documents d
					WHERE
						d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.is_published = 1
						AND d.deleted_by IS NULL
						AND d.archived_on IS NULL
					ORDER BY
						d.order_num,
						d.id;';

		return self::fetchDocuments( $strSql, $objDatabase );
	}

	public static function fetchDocumentIdByArTriggerIdByDocumentTypeIdByOccupancyTypeIdByCid( $intArTriggerId, $intDocumentTypeId, $intOccupancyTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						id
					FROM
						documents 
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ar_trigger_id  = ' . ( int ) $intArTriggerId . '
						AND document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND file_type_id IS NULL
						AND deleted_by IS NULL
						AND is_published = 1
						AND archived_on IS NULL';

		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchPublshedDocumentsCountByDocumentTypeIdDocumentSubTypeIdByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intCid, $objDatabase ) {

		$strFromSql = ' documents d ';

		$strWhereSql = ' WHERE
						d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.is_published = 1
						AND d.deleted_by IS NULL';

		return self::fetchDocumentCount( $strWhereSql, $objDatabase, $strFromSql );
	}

	public static function fetchExternalTemplateSetupExistanceByTransmissionVendorIdByCid( $intTransmissionVendorId, $intCid, $objDatabase ) {

		$strIsBluemoon = '';
		if( 5 == $intTransmissionVendorId ) {
			$strIsBluemoon = 'AND d.is_bluemoon = true';
		}
        $strSql = '
            SELECT
                d.id
            FROM
                documents d 
                JOIN document_addendas da ON ( d.cid = da.cid AND d.id = da.document_id AND da.master_document_id IS NULl AND da.external_key IS NOT NULL )
              WHERE
                d.cid = ' . ( int ) $intCid . '
                AND d.transmission_vendor_id = ' . ( int ) $intTransmissionVendorId . '
                ' . $strIsBluemoon . '
                AND d.deleted_on IS NULL
                AND d.document_type_id = ' . CDocumentType::MERGE_DOCUMENT . '
                AND d.document_sub_type_id = ' . CDocumentSubType::DOCUMENT_TEMPLATE . '
               LIMIT 1';

        $arrstrResult = fetchData( $strSql, $objDatabase );

        if( true == valArr( $arrstrResult ) && true == valArr( $arrstrResult[0] ) && true == valId( $arrstrResult[0]['id'] ) ) return true;

    }

    public static function fetchDefaultTemplateListByTransmissionVendorIdBySearchFilterByCid( $intCid, $objDatabase, $intTransmissionVendorId, $arrstrSearchFilter ) {

        $strWhereCondition 	= '';
        $strWhereConditionForTransmissionVendor = '';
        $strSortBy = 'name';
        $strSortDirection = 'asc';

        if( CTransmissionVendor::BLUE_MOON == $intTransmissionVendorId ) {
	        $strWhereConditionForTransmissionVendor = 'AND d.transmission_vendor_id = 5 AND d.is_bluemoon = true';
        } elseif( ( CTransmissionVendor::TENANT_TECH == $intTransmissionVendorId ) ) {
	        $strWhereConditionForTransmissionVendor = 'AND d.transmission_vendor_id = 48 ';
        }

        if( true == array_key_exists( 'document_name', $arrstrSearchFilter ) ) {
            $strWhereCondition = " AND ( LOWER (d.name) LIKE LOWER ('%" . addslashes( $arrstrSearchFilter['document_name'] ) . "%') OR LOWER (d.details->>'marketing_name') LIKE LOWER ('%" . addslashes( $arrstrSearchFilter['document_name'] ) . "%') )";
        }

        if( true == valStr( $arrstrSearchFilter['document_usage'] ) ) {
            switch( $arrstrSearchFilter['document_usage'] ) {
                case 'DOCUMENT_USAGE_ADHOC_AND_PACKET':
                    $strWhereCondition .= ' AND ( d.is_packet = TRUE AND d.is_adhoc = TRUE )';
                    break;

                case 'DOCUMENT_USAGE_ADHOC_ONLY':
                    $strWhereCondition .= ' AND d.is_packet = FALSE AND d.is_adhoc = TRUE ';
                    break;

                case 'DOCUMENT_USAGE_PACKETS_ONLY':
                    $strWhereCondition .= ' AND d.is_packet = TRUE AND d.is_adhoc = FALSE ';
                    break;

                default:
            }
        }

       if( true == valStr( $arrstrSearchFilter['sort_by'] ) ) {
           $strSortBy = $arrstrSearchFilter['sort_by'];
       }

        if( true == valStr( $arrstrSearchFilter['sort_direction'] ) ) {
            $strSortDirection = $arrstrSearchFilter['sort_direction'];
        }

	    if( 'name' == $strSortBy || 'title' == $strSortBy || 'description' == $strSortBy || 'keywords' == $strSortBy ) {
		    $strOrderBy = ' natural_sort( ' . $objDatabase->getCollateSort( 'd.' . $strSortBy . '', true ) . ')';
	    } else {
		    $strOrderBy = 'd.' . $strSortBy;
	    }

        $strSql = 'SELECT
                        d.cid,
                        d.id,
                        util_get_translated( \'name\',  d.name, d.details ) AS name,
                        util_get_translated( \'name\',  d.name, d.details ) AS default_name,
                        da.require_sign,
                        d.details,
                    CASE 
                        WHEN is_packet = TRUE AND is_adhoc = TRUE THEN 1
                        WHEN is_packet = TRUE THEN 2
                    ELSE	3
                    END AS usage,
                    count(DISTINCT pa1.state_code) as state_count,
                    array_agg(pa.state_code) as state_codes    
                    FROM
                        documents d
                    JOIN document_addendas da ON ( da.cid = d.cid AND da.document_id = d.id AND da.master_document_id IS NULL )
                    LEFT JOIN property_group_documents pgd ON ( d.cid = pgd.cid AND da.document_id = pgd.document_id )
                    LEFT JOIN property_group_associations pga ON ( pgd.cid = pga.cid AND pgd.property_group_id = pga.property_group_id )
                    LEFT JOIN property_addresses pa ON ( pga.cid = pa.cid AND pga.property_id = pa.property_id AND pa.state_code IS NOT NULL AND pa.is_alternate = false )
                    LEFT JOIN property_addresses pa1 ON ( pga.cid = pa1.cid AND pga.property_id = pa1.property_id AND pa1.state_code IS NOT NULL AND pa1.is_alternate = false )
                    LEFT JOIN states s ON (s.code = pa1.state_code)
                    WHERE
                        d.cid = ' . ( int ) $intCid . '
                        ' . $strWhereConditionForTransmissionVendor . '
                        AND da.external_key IS NOT NULL                        
                        AND d.document_type_id = ' . CDocumentType::MERGE_DOCUMENT . '
                        AND d.document_sub_type_id = ' . CDocumentSubType::DOCUMENT_TEMPLATE . '
                        AND d.deleted_on IS NULL ' . $strWhereCondition . '
                    GROUP BY
                        d.cid,
                        d.id,
                        d.name,
                        da.external_key,
                        da.require_sign,
                        usage
            ORDER BY ' . $strOrderBy . ' ' . $strSortDirection;

	    if( true == valArr( $arrstrSearchFilter['state_codes'] ) ) {
		    $strSql = 'SELECT
		        *
		        FROM
		        ( ' . $strSql . ' ) AS sub
		       WHERE
		       ( ARRAY[sub.state_codes] )::VARCHAR[] && ( ARRAY[ ' . implode( ',', $arrstrSearchFilter['state_codes'] ) . ' ] )::VARCHAR[]';
	    }

        return fetchData( $strSql, $objDatabase );
    }

    public static function fetchExternalTemplateStatesByIdByCid( $intId, $intCid, $objDatabase ) {

	    $strWhereConditionForTransmissionVendor = '';

       $strSql = 'SELECT
                        DISTINCT s.name as state
                    FROM
                        documents d
                        JOIN document_addendas da ON ( da.cid = d.cid AND da.document_id = d.id AND da.master_document_id IS NULL )
                        LEFT JOIN property_group_documents pgd ON ( d.cid = pgd.cid AND d.id = pgd.document_id )
                        LEFT JOIN property_group_associations pga ON ( pgd.cid = pga.cid AND pgd.property_group_id = pga.property_group_id )
                        LEFT JOIN property_addresses pa ON ( pga.cid = pa.cid AND pga.property_id = pa.property_id AND pa.state_code IS NOT NULL AND pa.is_alternate = false )
                        LEFT JOIN states s ON (s.code = pa.state_code)
                    WHERE
                        d.cid = ' . ( int ) $intCid . '
                        AND da.external_key IS NOT NULL                        
                        AND d.document_type_id = ' . CDocumentType::MERGE_DOCUMENT . '
                        AND d.document_sub_type_id = ' . CDocumentSubType::DOCUMENT_TEMPLATE . '
                        AND d.deleted_on IS NULL 
                        AND d.id = ' . ( int ) $intId . '
                        AND s.name IS NOT NULL
            ORDER BY  s.name asc';

        return fetchData( $strSql, $objDatabase );
    }

    public static function fetchBluemoonTemplatesByCid( $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) return false;

		$strSql = 'SELECT
						d.*,
						da.external_key,
						da.show_in_portal,
						da.is_household,
						da.require_sign,
						da.require_countersign
				    FROM
				        documents d 
				        JOIN document_addendas da ON (d.cid = da.cid AND d.id = da.document_id AND da.master_document_id IS NULL )
				    WHERE
                        d.cid = ' . ( int ) $intCid . '
                        AND d.transmission_vendor_id = 5
                        AND da.external_key IS NOT NULL
                        AND d.is_bluemoon = true
                        AND d.document_type_id = ' . CDocumentType::MERGE_DOCUMENT . '
                        AND d.document_sub_type_id = ' . CDocumentSubType::DOCUMENT_TEMPLATE . '
                        AND d.deleted_on IS NULL
                        AND da.deleted_on IS NULL
                        AND d.archived_on IS NULL
                        AND da.archived_on IS NULL';

	    return self::fetchDocuments( $strSql, $objDatabase );

    }

	public static function fetchSimpleViolationManagerDocumentsByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT 
						DISTINCT( d.id ),
						d.file_type_id,
						d.name 
					FROM 
						documents d
						JOIN file_types ft ON ( ft.cid = d.cid AND ft.id = d.file_type_id )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND ft.system_code = \'' . CFileType::SYSTEM_CODE_VIOLATION_MANAGER . '\'
						AND d.deleted_on IS NULL
						AND d.deleted_by IS NULL
						AND d.archived_on IS NULL
					ORDER BY
						d.name';

		$arrmixResults = fetchData( $strSql, $objDatabase );
		return rekeyArray( 'id', $arrmixResults );
	}

	public static function fetchActiveDocumentTemplateBySystemCodeByPropertyIdByCid( $strFileTypeSystemCode, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valStr( $strFileTypeSystemCode ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = '	SELECT
							d.*
						FROM
							documents d
							JOIN file_types ft ON ( ft.cid = d.cid AND d.file_type_id = ft.id AND ft.system_code = \'' . $strFileTypeSystemCode . '\' )
							JOIN property_group_documents pgd ON ( pgd.cid = d.cid AND pgd.document_id = d.id )
						WHERE
							d.cid = ' . ( int ) $intCid . '
							AND pgd.property_group_id = ' . ( int ) $intPropertyId . '
							AND d.document_type_id = ' . CDocumentType::MERGE_DOCUMENT . '
							AND d.document_sub_type_id = ' . CDocumentSubType::DOCUMENT_TEMPLATE . '
							AND	d.is_published = 1
							AND d.archived_on IS NULL
							AND ( d.activate_on IS NULL OR d.activate_on <= now() )
							AND ( d.deactivate_on IS NULL OR d.deactivate_on >= now() )
							AND d.deleted_by IS NULL
						LIMIT 1';

		return self::fetchDocument( $strSql, $objDatabase );
	}

	public static function fetchDocumentWithFileTypeByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						d.*,
						ft.system_code
					FROM
						documents d						
	                    LEFT JOIN file_types ft ON ( d.file_type_id = ft.id AND ft.cid = d.cid )
					WHERE
						d.id = ' . ( int ) $intId . '
						AND d.cid = ' . ( int ) $intCid . '
						AND d.archived_on IS NULL
						AND d.deleted_by IS NULL';

		return self::fetchDocument( $strSql, $objDatabase );
	}

}
?>