<?php

class CScheduledBlackoutDate extends CBaseScheduledBlackoutDate {

	const HOLIDAY = 3;

	protected $m_boolUseLastDayOfMonth;
	protected $m_boolUseNextScheduledDay;

	protected $m_strDayOfWeek;
	protected $m_strBeginBlackoutDate;
	protected $m_strEndBlackoutDate;
	protected $m_intBlackoutDatePropertyCount;

	protected $m_intOccurenceId = 1;
	protected $m_intFrequencyId = 1;
	protected $m_intRepeatEvery = 1;
	protected $m_intBlackoutDateCount;

	protected $m_arrintDaysOfMonth		= [];
	protected $m_arrstrSchedule			= [];
	protected $m_arrstrBlackoutDates	= [];
	protected $m_arrintHolidays			= [];
	protected $m_arrstrHolidayDates		= [];
	protected $m_arrstrWeekDays			= [
		1 => 'sunday',
		2 => 'monday',
		3 => 'tuesday',
		4 => 'wednesday',
		5 => 'thursday',
		6 => 'friday',
		7 => 'saturday'
	];

	protected $m_arrintWeekDays = [
		'sunday' => 1,
		'monday' => 2,
		'tuesday' => 3,
		'wednesday' => 4,
		'thursday' => 5,
		'friday' => 6,
		'saturday' => 7
	];

	public static  $c_arrstrOccurences = [
		1 	=> 'once',
		2	=> 'recurring',
		3	=> 'holidays'
	];

    public function __construct() {
        parent::__construct();

        $this->m_strBeginBlackoutDate 	= date( 'm/d/Y' );
        $this->m_strEndBlackoutDate 	= date( 'm/d/Y', strtotime( '+2 years 1 month', strtotime( $this->m_strBeginBlackoutDate ) ) );

    }

    public function setOccurenceId( $intOccurenceId ) {
		$this->m_intOccurenceId = $intOccurenceId;

		if( CFrequency::ONCE == $this->m_intOccurenceId ) {
			$this->m_intFrequencyId = CFrequency::ONCE;
		}
		if( CScheduledBlackoutDate::HOLIDAY == $this->m_intOccurenceId ) {
			$this->m_intFrequencyId = CFrequency::YEARLY;
		}
    }

    public function setRepeatEvery( $intRepeatEvery ) {
    	$this->m_intRepeatEvery = $intRepeatEvery;
    }

    public function setDayOfWeek( $strDayOfWeek ) {
    	$this->m_strDayOfWeek = $strDayOfWeek;
    }

    public function setDaysOfMonth( $arrintDaysOfMonth ) {

    	$this->m_arrintDaysOfMonth = [];

    	foreach( $arrintDaysOfMonth as $intDaysOfMonth => $boolValue ) {
    		if( false != $boolValue ) {
			    $this->m_arrintDaysOfMonth[$intDaysOfMonth] = $intDaysOfMonth;
		    }
    	}
    }

    public function setUseLastDayOfMonth( $boolUseLastDayOfMonth ) {
    	$this->m_boolUseLastDayOfMonth = $boolUseLastDayOfMonth;
    }

    public function setUseNextScheduledDay( $boolUseNextScheduledDay ) {
		$this->m_boolUseNextScheduledDay = $boolUseNextScheduledDay;
    }

    public function setBeginBlackoutDate( $strBeginBlackoutDate ) {
    	$this->m_strBeginBlackoutDate = $strBeginBlackoutDate;
    }

    public function setEndBlackoutDate( $strEndBlackoutDate ) {
    	$this->m_strEndBlackoutDate = $strEndBlackoutDate;
    }

	public function setHolidays( $arrintHolidays ) {
    	$this->m_arrintHolidays = [];

    	foreach( $arrintHolidays as $intDaysOfMonth ) {
    		$this->m_arrintHolidays[$intDaysOfMonth] = $intDaysOfMonth;
    	}
    }

    public function setHolidayDates( $arrstrHolidayDates ) {
    	$this->m_arrstrHolidayDates = $arrstrHolidayDates;
    }

	public function setBlackoutDateCount( $intBlackoutDateCount ) {
		$this->m_intBlackoutDateCount = $intBlackoutDateCount;
	}

    public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );
	    $this->m_intOccurenceId = ( CFrequency::ONCE == $this->m_intFrequencyId ) ? 1 : ( ( true == $this->m_intIsAmenity ) ? 3 : 2 );
	    if( true == isset( $arrmixValues['occurence_id'] ) ) 			$this->setOccurenceId( $arrmixValues['occurence_id'] );
    	if( true == isset( $arrmixValues['repeat_every'] ) ) 			$this->setRepeatEvery( $arrmixValues['repeat_every'] );
    	if( true == isset( $arrmixValues['day_of_week'] ) ) 			$this->setDayOfWeek( $arrmixValues['day_of_week'] );
    	if( true == isset( $arrmixValues['day_of_month'] ) ) 			$this->setDaysOfMonth( $arrmixValues['day_of_month'] );
    	if( true == isset( $arrmixValues['last_day_of_month'] ) ) 		$this->setUseLastDayOfMonth( $arrmixValues['last_day_of_month'] );
    	if( true == isset( $arrmixValues['use_next_scheduled_day'] ) ) 	$this->setUseNextScheduledDay( $arrmixValues['use_next_scheduled_day'] );
    	if( true == isset( $arrmixValues['holidays'] ) ) 				$this->setHolidays( $arrmixValues['holidays'] );
    	if( true == isset( $arrmixValues['blackout_date_count'] ) ) 	$this->setBlackoutDateCount( $arrmixValues['blackout_date_count'] );


    	return;
    }

	public function setHolidayDatesForCurrentYear() {
		$arrstrHolidayDates = [];
		$arrobjPaidHolidays = \Psi\Libraries\UtilHolidays\CHolidays::create()->getHolidayNames();
		$arrintBlackoutHolidays	= explode( ',', \Psi\CStringService::singleton()->substr( $this->getSchedule(), 10 ) );

		foreach( $arrintBlackoutHolidays as $strHoliday ) {
			$arrstrHolidayDates[] = date( 'm/d/Y', strtotime( $arrobjPaidHolidays[$strHoliday] ) );
		}
		$this->setHolidayDates( $arrstrHolidayDates );
	}

    public function getOccurenceId() {
    	return $this->m_intOccurenceId;
    }

    public function getRepeatEvery() {
    	return $this->m_intRepeatEvery;
    }

    public function getDayOfWeek() {
		return $this->m_strDayOfWeek;
    }

    public function getDaysOfMonth() {
    	return $this->m_arrintDaysOfMonth;
    }

    public function getUseLastDayOfMonth() {
		return $this->m_boolUseLastDayOfMonth;
    }

    public function getUseNextScheduledDay() {
    	return $this->m_boolUseNextScheduledDay;
    }

    public function getBeginBlackoutDate() {
    	return $this->m_strBeginBlackoutDate;
    }

    public function getEndBlackoutDate() {
    	return $this->m_strEndBlackoutDate;
    }

	public function getHolidays() {
    	return $this->m_arrintHolidays;
	}

	public function getBlackoutDateCount() {
		return $this->m_intBlackoutDateCount;
	}

    public function valId() {
        return true;
    }

    public function valCid() {
        return true;
    }

    public function valPropertyId() {
        return true;
    }

    public function valFrequencyId() {
        return true;
    }

    public function valStartDate() {
        return true;
    }

    public function valEndDate() {
        return true;
    }

    public function valSchedule() {
        return true;
    }

    public function valIsMoveIn() {
        return true;
    }

    public function valIsMoveOut() {
        return true;
    }

    public function valIsAmenity() {
    	return true;
    }

    public function validate( $strAction ) {

        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		switch( $this->m_intFrequencyId ) {

        			case CFrequency::WEEKLY:
        				if( true == is_null( $this->getDayOfWeek() ) || 0 == strlen( $this->getDayOfWeek() ) ) {
        					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( '"Day of week" is required.' ) ) );
        					$boolIsValid = false;
        				}

        				if( true == is_null( $this->getRepeatEvery() ) ) {
        					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( '"Every" is required.' ) ) );
        					$boolIsValid = false;
        				} elseif( false == is_numeric( $this->getRepeatEvery() ) || 1 > $this->getRepeatEvery() ) {
        					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( '"Every" accepts only numeric value.' ) ) );
        					$boolIsValid = false;
        				}
        				break;

        			case CFrequency::WEEKDAY_OF_MONTH:
        				if( true == is_null( $this->getDayOfWeek() ) ) {
        					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( '"Day of week" is required.' ) ) );
        					$boolIsValid = false;
        				}
        				break;

        			case CFrequency::MONTHLY:
        				if( true == is_null( $this->getRepeatEvery() ) ) {
        					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( '"Every" is required.' ) ) );
        					$boolIsValid = false;
        				} elseif( false == is_numeric( $this->getRepeatEvery() ) ) {
        					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( '"Every" accepts only numeric value.' ) ) );
        					$boolIsValid = false;
        				}

        				if( false == valArr( $this->m_arrintDaysOfMonth ) && false == ( bool ) $this->getUseLastDayOfMonth() ) {
        					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( '"Days of month" are required.' ) ) );
        					$boolIsValid = false;
        				}
        				break;

        			case SELF::HOLIDAY:
        				if( false == valArr( $this->m_arrintHolidays ) ) {
        					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( '"Holidays" are required.' ) ) );
        					$boolIsValid = false;
        				}
        				break;

         			default:
         				$boolIsValid = true;
        				break;
        		}

        		if( false == $this->getUseNextScheduledDay() && true == is_null( $this->getStartDate() ) ) {
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Start date is required.' ) ) );
        			$boolIsValid = false;

        		} elseif( false == $this->getUseNextScheduledDay() && false == CValidation::validateDate( $this->getStartDate(), false ) ) {
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Invalid "Start On" date provided.' ) ) );
        			$boolIsValid = false;

        		} elseif( false == is_null( $this->getStartDate() ) && strtotime( $this->getStartDate() ) < strtotime( date( 'm/d/Y' ) ) ) {
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Start date cannot be in past.' ) ) );
        			$boolIsValid = false;
        		}

        		if( false == is_null( $this->getStartDate() ) && false == is_null( $this->getEndDate() ) && strtotime( $this->getEndDate() ) <= strtotime( $this->getStartDate() ) ) {
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Start date should be less than end date.' ) ) );
        			$boolIsValid = false;
        		}

        		if( false == $this->getIsLeaseStartDate() && false == $this->getIsLeaseEndDate() && false == ( bool ) $this->getIsMoveIn() && false == ( bool ) $this->getIsMoveOut() && false == ( bool ) $this->getIsAmenity() ) {
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Apply To is required.' ) ) );
        			$boolIsValid = false;
        		}
        		break;

        	default:
        	$boolIsValid = true;
        }
        return $boolIsValid;
    }

    public function getFrequencyToStr() {
    	return CFrequency::frequencyIdToStr( $this->m_intFrequencyId );
    }

    public function createBlackoutDate() {

    	$objBlackoutDate = new CBlackoutDate();
    	$objBlackoutDate->setCid( $this->getCid() );
    	$objBlackoutDate->setPropertyId( $this->getPropertyId() );
    	$objBlackoutDate->setScheduledBlackoutDateId( $this->getId() );
	    $objBlackoutDate->setIsLeaseStartDate( $this->getIsLeaseStartDate() );
	    $objBlackoutDate->setIsLeaseEndDate( $this->getIsLeaseEndDate() );
    	$objBlackoutDate->setIsMoveIn( $this->getIsMoveIn() );
    	$objBlackoutDate->setIsMoveOut( $this->getIsMoveOut() );
    	$objBlackoutDate->setIsAmenity( $this->getIsAmenity() );

    	return $objBlackoutDate;
    }

    public function toString() {

    	$arrstrSchedule['frequency'] 	= $this->m_intRepeatEvery;
    	$arrstrSchedule['day_of_week'] 	= $this->m_strDayOfWeek;
    	$arrstrSchedule['day_of_month'] = implode( ',', $this->m_arrintDaysOfMonth );

    	if( true == $this->m_boolUseLastDayOfMonth ) {
    		if( '*' == $arrstrSchedule['day_of_month'] ) {
    			$arrstrSchedule['day_of_month'] = __( 'Last Day of month' );
    		} else {
    			$arrstrSchedule['day_of_month'] .= __( ',Last Day of month' );
    		}
    	}

     	switch( $this->m_intFrequencyId ) {

    		case CFrequency::ONCE:
    			if( false == is_null( $this->m_strEndDate ) ) {
    				$strSchedule = __( 'from {%t,0, DATE_ALPHA_SHORT} - {%t,1, DATE_ALPHA_SHORT}', [ $this->m_strStartDate, $this->m_strEndDate ] );
    			} else {
    				$strSchedule = __( 'on {%t,0, DATE_ALPHA_SHORT}', [ $this->m_strStartDate ] );
    			}
    			break;

    		case CFrequency::WEEKDAY_OF_MONTH:
    			if( 1 == $arrstrSchedule['frequency'] ) $arrstrSchedule['frequency'] = '1st';
    			if( 2 == $arrstrSchedule['frequency'] ) $arrstrSchedule['frequency'] = '2nd';
    			if( 3 == $arrstrSchedule['frequency'] ) $arrstrSchedule['frequency'] = '3rd';
    			if( 4 == $arrstrSchedule['frequency'] ) $arrstrSchedule['frequency'] = '4th';
    			$strSchedule = __( 'Every {%s,0}  {%s,1} starting on {%t,2, DATE_ALPHA_SHORT}', [ $arrstrSchedule['frequency'], \Psi\CStringService::singleton()->ucfirst( $arrstrSchedule['day_of_week'] ), $this->m_strStartDate ] );
    			break;

    		case CFrequency::WEEKLY:
    			if( 1 < $arrstrSchedule['frequency'] ) {
    				$arrstrSchedule['frequency'] = __( '{%d,0} weeks', [ $arrstrSchedule['frequency'] ] );
    			} else {
    				$arrstrSchedule['frequency'] = __( 'week' );
    			}
				$strSchedule = __( 'Every {%s,0} on {%s,1} starting on {%t,2, DATE_ALPHA_SHORT}', [ $arrstrSchedule['frequency'], \Psi\CStringService::singleton()->ucfirst( $arrstrSchedule['day_of_week'] ), $this->m_strStartDate ] );
    			break;

    		case CFrequency::MONTHLY:
    			if( 1 < $arrstrSchedule['frequency'] ) {
    				$arrstrSchedule['frequency'] = __( '{%d,0} months', [ $arrstrSchedule['frequency'] ] );
    			} else {
    				$arrstrSchedule['frequency'] = __( 'month' );
    			}
    			$strSchedule = __( 'Every {%s,0} on {%s,1} starting on {%t,2, DATE_ALPHA_SHORT}', [ $arrstrSchedule['frequency'], \Psi\CStringService::singleton()->ucfirst( $arrstrSchedule['day_of_month'] ), $this->m_strStartDate ] );
    			break;

    		case CFrequency::YEARLY:
     			if( true == $this->m_intIsAmenity ) {
    				$strPostText = '';
    				$intCountBlackoutHolidays	= \Psi\Libraries\UtilFunctions\count( ( explode( ',', \Psi\CStringService::singleton()->substr( $this->getSchedule(), 10 ) ) ) );
    				if( 1 < $intCountBlackoutHolidays ) {
					    $strPostText = __( 's' );
				    }
    				$strSchedule = __( '{%d,0} Holiday{%s,1} Selected', [ $intCountBlackoutHolidays, $strPostText ] );
    			} else {
     				if( 1 < $arrstrSchedule['frequency'] ) {
     					$arrstrSchedule['frequency'] = __( '{%d,0} years', [ $arrstrSchedule['frequency'] ] );
     				} else {
     					$arrstrSchedule['frequency'] = __( 'year' );
     				}
     				$strSchedule = __( 'Every {%s,0} on {%t,1, DATE_ALPHA_MONTH} {%t,1,DATE_NUMERIC_DAY } starting on {%t,1, DATE_ALPHA_SHORT}', [ $arrstrSchedule['frequency'], $this->m_strStartDate ] );
    			}
    			break;

    		default:
    			$strSchedule = '';
    	}

    	return $strSchedule;
    }

    public function formatSchedule() {
    	switch( $this->m_intFrequencyId ) {

    		case CFrequency::WEEKLY:
    			$this->m_strSchedule = '* * ' . $this->m_arrintWeekDays[$this->m_strDayOfWeek] . ' * ' . $this->m_intRepeatEvery;
    			break;

    		case CFrequency::WEEKDAY_OF_MONTH:
    			$this->m_strDayOfWeek = ( false == is_null( $this->m_strDayOfWeek ) && 0 < strlen( $this->m_strDayOfWeek ) ) ? $this->m_strDayOfWeek : \Psi\CStringService::singleton()->strtolower( date( 'l' ) );
    			$this->m_strSchedule = '* * ' . $this->m_arrintWeekDays[$this->m_strDayOfWeek] . ' * ' . $this->m_intRepeatEvery;
    			break;

    		case CFrequency::MONTHLY:
				$strDates = ( false == valArr( $this->m_arrintDaysOfMonth ) ) ? '*' : implode( ',', $this->m_arrintDaysOfMonth );
    			$strUseLastDayOfMonth = ( true == $this->m_boolUseLastDayOfMonth ) ? '-1' : '*';
    			$this->m_strSchedule = $strDates . ' * ' . $strUseLastDayOfMonth . ' * ' . $this->m_intRepeatEvery;
    			break;

    		case CFrequency::YEARLY:
    			$this->m_strSchedule = '* * * * ' . $this->m_intRepeatEvery;
    			if( CScheduledBlackoutDate::HOLIDAY == $this->m_intOccurenceId ) {
    				$strHolidays = ( false == valArr( $this->m_arrintHolidays ) ) ? '*' : implode( ',', $this->m_arrintHolidays );
    				$this->m_strSchedule = '* * * * * ' . $strHolidays;
    			}
    			break;

    		default:
    			$this->m_strSchedule = '* * * * *';
    			break;
    	}
    }

    public function parseSchedule() {
    	$arrintSchedule = explode( ' ', $this->m_strSchedule );

    	$this->m_intRepeatEvery = $arrintSchedule[4];

    	switch( $this->m_intFrequencyId ) {

    		case CFrequency::WEEKLY:
    			$this->m_strDayOfWeek = $this->m_arrstrWeekDays[$arrintSchedule[2]];
    			break;

    		case CFrequency::WEEKDAY_OF_MONTH:
    			$this->m_strDayOfWeek = $this->m_arrstrWeekDays[$arrintSchedule[2]];
    			break;

    		case CFrequency::MONTHLY:
    			$arrintDaysOfMonth = explode( ',', $arrintSchedule[0] );

    			foreach( $arrintDaysOfMonth as $intDaysOfMonth ) {
    				$this->m_arrintDaysOfMonth[$intDaysOfMonth] = $intDaysOfMonth;
    			}

    			if( -1 == $arrintSchedule[2] ) {
    				$this->m_boolUseLastDayOfMonth = true;
    			}
    			break;

    		case CFrequency::YEARLY:
    			if( CScheduledBlackoutDate::HOLIDAY == $this->getOccurenceId() ) {
    				$arrintHolidays = explode( ',', $arrintSchedule[5] );

    				foreach( $arrintHolidays as $intHoliday ) {
    					$this->m_arrintHolidays[$intHoliday] = $intHoliday;
    				}
    			}
    			break;

    		default:
    			// default
    			break;
    	}
    }

    public function getNextScheduledDay() {

		$strFrequency = NULL;

    	switch( $this->m_intFrequencyId ) {

    		case CFrequency::WEEKLY:
    			return ( ( 0 < strlen( $this->getDayOfWeek() ) ) ? date( 'm/d/Y', strtotime( 'next ' . $this->getDayOfWeek() ) ) : date( 'm/d/Y' ) );
    			break;

    		case CFrequency::MONTHLY:
    			$strBlackoutDate = date( 'm/01/Y' );

    			foreach( $this->m_arrintDaysOfMonth as $intDay ) {
    				$strMonthlyBlackoutDate = date( 'm/' . $intDay . '/Y', strtotime( $strBlackoutDate ) );

    				if( true == CValidation::validateDate( $strMonthlyBlackoutDate, false ) && strtotime( date( 'm/d/Y' ) ) < strtotime( $strMonthlyBlackoutDate ) ) {
    					return $strMonthlyBlackoutDate;
    				}

    			}

    			if( true == $this->m_boolUseLastDayOfMonth ) {
    				return date( 'm/d/Y', strtotime( '-1 day', strtotime( '+1 month', strtotime( $strBlackoutDate ) ) ) );
    			}

    			$strBlackoutDate = date( 'm/d/Y', strtotime( '+1 month', strtotime( date( 'm/01/Y' ) ) ) );

    			foreach( $this->m_arrintDaysOfMonth as $intDay ) {
    				$strMonthlyBlackoutDate = date( 'm/' . $intDay . '/Y', strtotime( $strBlackoutDate ) );

    				if( true == CValidation::validateDate( $strMonthlyBlackoutDate, false ) && strtotime( date( 'm/d/Y' ) ) < strtotime( $strMonthlyBlackoutDate ) ) {
    					return $strMonthlyBlackoutDate;
    				}

    			}
    			break;

    		case CFrequency::WEEKDAY_OF_MONTH:

				switch( $this->getRepeatEvery() ) {
    				case 1:
    					$strFrequency = 'first';
    					break;

   					case 2:
    					$strFrequency = 'second';
    					break;

    				case 3:
    					$strFrequency = 'third';
    					break;

   					case 4:
   						$strFrequency = 'fourth';
   						break;

   					default:
						// default
   						break;
				}

				if( 0 == strlen( trim( $this->getDayOfWeek() ) ) ) {
					$this->setDayOfWeek( \Psi\CStringService::singleton()->strtolower( date( 'l' ) ) );
				}

    			$strBlackoutDate = date( 'm/01/Y' );
    			$strBlackoutWeekDayOfMonth = date( 'm/d/Y', strtotime( $strFrequency . ' ' . $this->getDayOfWeek(), strtotime( $strBlackoutDate ) ) );

    			if( strtotime( date( 'm/d/Y' ) ) < strtotime( $strBlackoutWeekDayOfMonth ) ) {
    				return $strBlackoutWeekDayOfMonth;
    			}

    			$strBlackoutDate = date( 'm/d/Y', strtotime( '+1 month', strtotime( $strBlackoutDate ) ) );
    			return date( 'm/d/Y', strtotime( $strFrequency . ' ' . $this->getDayOfWeek(), strtotime( $strBlackoutDate ) ) );
    			break;

    		default:
    			// default
    			break;
    	}
    }

    public function buildBlackoutDates( $intCompanyUserId, $objDatabase ) {

		$arrobjBlackoutDates = [];

    	$this->generateBlackoutDates();

    	$objBlackoutDate = $this->createBlackoutDate();

    	if( false == valArr( $this->m_arrstrBlackoutDates ) ) {
		    return true;
	    }

    	foreach( $this->m_arrstrBlackoutDates as $strBlackoutDate ) {
    		$objClonedBlackoutDate = clone $objBlackoutDate;
    		$objClonedBlackoutDate->setBlackoutDate( $strBlackoutDate );
    		$arrobjBlackoutDates[] = $objClonedBlackoutDate;
    	}

    	return \Psi\Eos\Entrata\CBlackoutDates::createService()->bulkInsert( $arrobjBlackoutDates, $intCompanyUserId, $objDatabase );
    }

    public function generateBlackoutDates() {

		 $strFrequency = NULL;

    	$this->parseSchedule();

    	switch( $this->getFrequencyId() ) {

    		case CFrequency::ONCE:
    			if( false == is_null( $this->getEndDate() ) ) {
    				$strBlackoutDate = $this->getStartDate();
    				$intMinDate = strtotime( $strBlackoutDate );
    				$intMaxDate = strtotime( $this->getEndDate() );
    				for( $strBlackoutDate; $intMinDate <= $intMaxDate; ) {
    					$this->m_arrstrBlackoutDates[] = $strBlackoutDate;
    					$strBlackoutDate = date( 'm/d/Y', strtotime( '+1 day', strtotime( $strBlackoutDate ) ) );
    					$intMinDate = strtotime( $strBlackoutDate );
    				}
    			} else {
    				$this->m_arrstrBlackoutDates = [ $this->getStartDate() ];
    			}
    			break;

    		case CFrequency::WEEKLY:
    			if( \Psi\CStringService::singleton()->strtolower( $this->getDayOfWeek() ) == \Psi\CStringService::singleton()->strtolower( date( 'l', strtotime( $this->getStartDate() ) ) ) ) {
    				$strBlackoutDate = $this->getStartDate();
    			} else {
    				$strBlackoutDate = ( false == $this->getUseNextScheduledDay() ) ? date( 'm/d/Y', strtotime( 'next ' . $this->getDayOfWeek(), strtotime( $this->getStartDate() ) ) ) : $this->getStartDate();
    			}
    			$intMinDate = strtotime( $strBlackoutDate );
    			$intEndDate = strtotime( $this->m_strEndBlackoutDate );
    			$intBeginDate = strtotime( $this->m_strBeginBlackoutDate );
    			for( $intInterval = $this->getRepeatEvery() * 7; $intMinDate < $intEndDate; ) {

    				if( $intMinDate > $intBeginDate ) {
    					$this->m_arrstrBlackoutDates[] = $strBlackoutDate;
    				}

    				$strBlackoutDate = date( 'm/d/Y', strtotime( '+' . $intInterval . ' days', strtotime( $strBlackoutDate ) ) );
    				$intMinDate = strtotime( $strBlackoutDate );
    			}
    			break;

    		case CFrequency::WEEKDAY_OF_MONTH:
			    $strBlackoutDate = $this->getStartDate();
    			$intMinDate = strtotime( $strBlackoutDate );
    			$intMaxDate = strtotime( $this->m_strEndBlackoutDate );
    			for( $strBlackoutDate; $intMinDate < $intMaxDate; ) {

    				switch( $this->getRepeatEvery() ) {
    					case 1:
    						$strFrequency = 'first';
    						break;

    					case 2:
    						$strFrequency = 'second';
    						break;

    					case 3:
    						$strFrequency = 'third';
    						break;

    					case 4:
    						$strFrequency = 'fourth';
    						break;

    					default:
    						// default
    						break;
    				}

    				$strBlackoutDate = date( 'm/01/Y', strtotime( $strBlackoutDate ) );
    				$strBlackoutWeekDayOfMonth = date( 'm/d/Y', strtotime( $strFrequency . ' ' . $this->getDayOfWeek(), strtotime( $strBlackoutDate ) ) );

    				if( strtotime( $strBlackoutDate ) > strtotime( $this->m_strBeginBlackoutDate ) && strtotime( $this->getStartDate() ) <= strtotime( $strBlackoutWeekDayOfMonth ) ) {
    					$this->m_arrstrBlackoutDates[] = $strBlackoutWeekDayOfMonth;
    				}

    				$strBlackoutDate = date( 'm/d/Y', strtotime( '+1 month', strtotime( $strBlackoutDate ) ) );
    				$intMinDate = strtotime( $strBlackoutDate );
    			}
    			break;

    		case CFrequency::MONTHLY:
    			$strBlackoutDate = $this->getStartDate();
    			$intMinDate = strtotime( $strBlackoutDate );
    			$intMaxDate = strtotime( $this->m_strEndBlackoutDate );

    			for( $intInterval = $this->getRepeatEvery(); $intMinDate < $intMaxDate; ) {

    				foreach( $this->getDaysOfMonth() as $intDay ) {
    					$strMonthlyBlackoutDate = date( 'm/' . $intDay . '/Y', strtotime( $strBlackoutDate ) );
    					if( true == CValidation::validateDate( $strMonthlyBlackoutDate, false ) && strtotime( $this->getStartDate() ) <= strtotime( $strMonthlyBlackoutDate ) ) {
    						if( strtotime( $strBlackoutDate ) >= strtotime( $this->m_strBeginBlackoutDate ) ) {
    							$this->m_arrstrBlackoutDates[] = $strMonthlyBlackoutDate;
    						}
    					}
    				}

    				$strBlackoutDate = date( 'm/01/Y', strtotime( $strBlackoutDate ) );
    				if( true == $this->getUseLastDayOfMonth() ) {
    					$strLastDayOfMonth = date( 'm/d/Y', strtotime( '-1 day', strtotime( '+1 month', strtotime( $strBlackoutDate ) ) ) );
    					if( false == in_array( $strLastDayOfMonth, $this->m_arrstrBlackoutDates ) ) {
    						if( strtotime( $strLastDayOfMonth ) > strtotime( $this->m_strBeginBlackoutDate ) ) {
    							$this->m_arrstrBlackoutDates[] = $strLastDayOfMonth;
    						}
    					}
    				}
    				$strBlackoutDate = date( 'm/d/Y', strtotime( '+' . $intInterval . ' months', strtotime( $strBlackoutDate ) ) );
    				$intMinDate = strtotime( $strBlackoutDate );
    			}
    			break;

    		case CFrequency::YEARLY:
    			if( CScheduledBlackoutDate::HOLIDAY == $this->getOccurenceId() ) {

				    if( true == valArr( $this->getHolidayDates() ) ) {
					    for( $intBlackoutYear = 0; $intBlackoutYear <= 3; $intBlackoutYear++ ) {
						    foreach( $this->getHolidayDates() as $strBlackoutDate ) {
							    $strBlackoutDate               = date( 'm/d/Y', strtotime( '+' . $intBlackoutYear . ' year', strtotime( $strBlackoutDate ) ) );

							    if( strtotime( $strBlackoutDate ) > strtotime( $this->getStartDate() ) && strtotime( $strBlackoutDate ) < strtotime( $this->getEndBlackoutDate() ) ) {
								    $this->m_arrstrBlackoutDates[] = $strBlackoutDate;
							    }
						    }
					    }
				    }
    			} else {
    				$strBlackoutDate = $this->getStartDate();
    				$intMinDate = strtotime( $strBlackoutDate );
    				$intBeginDate = strtotime( $this->m_strBeginBlackoutDate );
    				$intEndDate = strtotime( $this->m_strEndBlackoutDate );
	    			for( $strBlackoutDate; $intMinDate < $intEndDate; ) {

	    				if( $intMinDate > $intBeginDate ) {
	    					$this->m_arrstrBlackoutDates[] = $strBlackoutDate;
	    				}

	    				$strBlackoutDate = date( 'm/d/Y', strtotime( '+1 year', strtotime( $strBlackoutDate ) ) );
	    				$intMinDate = strtotime( $strBlackoutDate );
	    			}
    			}
    			break;

    		default:
    			// default
    			break;
    	}
    }

    public function getHolidayDates() {
	    return $this->m_arrstrHolidayDates;
    }

    public function rebuildBlackoutDatesRequired( $objScheduledBlackoutDate ) {

	    if( $this->getIsLeaseStartDate() <> $objScheduledBlackoutDate->getIsLeaseStartDate() ) {
		    return true;
	    }

	    if( $this->getIsLeaseEndDate() <> $objScheduledBlackoutDate->getIsLeaseEndDate() ) {
		    return true;
	    }

    	if( $this->getIsMoveIn() <> $objScheduledBlackoutDate->getIsMoveIn() ) {
		    return true;
	    }

    	if( $this->getIsMoveOut() <> $objScheduledBlackoutDate->getIsMoveOut() ) {
		    return true;
	    }

    	if( $this->getIsAmenity() <> $objScheduledBlackoutDate->getIsAmenity() ) {
		    return true;
	    }

    	if( $this->getFrequencyId() <> $objScheduledBlackoutDate->getFrequencyId() ) {
		    return true;
	    }

    	if( $this->getSchedule() <> $objScheduledBlackoutDate->getSchedule() ) {
		    return true;
	    }

    	if( $this->getStartDate() <> $objScheduledBlackoutDate->getStartDate() ) {
		    return true;
	    }

    	if( $this->getEndDate() <> $objScheduledBlackoutDate->getEndDate() ) {
		    return true;
	    }

    	return false;
    }

}
?>