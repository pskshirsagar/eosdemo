<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportArchives
 * Do not add any new functions to this class.
 */

class CReportArchives extends CBaseReportArchives {

	public static function fetchReportArchiveDataByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						file_name,
						file_path,
						serialized_data
				   FROM
						report_archives
				   WHERE
				 		cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intId;

		return fetchData( $strSql, $objDatabase );
	}
}
?>