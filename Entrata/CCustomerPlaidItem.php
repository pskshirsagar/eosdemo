<?php

use Psi\Libraries\Cryptography\CCrypto;

class CCustomerPlaidItem extends CBaseCustomerPlaidItem {

	public static function getDecryptedAccessToken( $strAccessTokenEncrypted ) {
		if( false == \valStr( $strAccessTokenEncrypted ) ) {
			return NULL;
		}

		return CCrypto::createService()->decrypt( $strAccessTokenEncrypted, \CConfig::get( 'SODIUM_KEY_PLAID_ACCESS_TOKEN' ) );
	}

	public static function getEncryptedAccessToken( $strAccessToken ) {
		if( false == \valStr( $strAccessToken ) ) {
			return NULL;
		}

		return CCrypto::createService()->encrypt( $strAccessToken, \CConfig::get( 'SODIUM_KEY_PLAID_ACCESS_TOKEN' ) );
	}

	public function getAccessToken() {
		return self::getDecryptedAccessToken( $this->getAccessTokenEncrypted() );
	}

	public function setAccessToken( $strAccessToken ) {
		$strAccessToken = CStrings::strTrimDef( $strAccessToken, 1000, NULL, true );

		if( true == \valStr( $strAccessToken ) ) {
			$this->setAccessTokenEncrypted( self::getEncryptedAccessToken( $strAccessToken ) );
		}
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['item_id'] ) ) $this->setItemId( $arrmixValues['item_id'] );
		if( true == isset( $arrmixValues['access_token'] ) ) $this->setAccessToken( $arrmixValues['access_token'] );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valItemIdEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebhook() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConsentExpirationTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valError() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastWebhookSentAt() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastWebhookCodeSent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPublicToken() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccessTokenEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valItemIdBindex() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getDecryptedItemId( $strItemIdEncrypted ) {
		if( false == \valStr( $strItemIdEncrypted ) ) {
			return NULL;
		}
		return CCrypto::createService()->decrypt( $strItemIdEncrypted, \CConfig::get( 'SODIUM_KEY_ID' ) );
	}

	public static function getEncryptedItemId( $strItemId ) {
		if( false == \valStr( $strItemId ) ) {
			return NULL;
		}

		return CCrypto::createService()->encrypt( $strItemId, \CConfig::get( 'SODIUM_KEY_ID' ) );
	}

	public function getItemId() : string {
		return self::getDecryptedItemId( $this->getItemIdEncrypted() );
	}

	public function setItemId( $strItemId ) {
		if( false == valStr( $strItemId ) ) {
			return;
		}
		$strItemId = CStrings::strTrimDef( $strItemId, 1000, NULL, true );

		if( true == \valStr( $strItemId ) ) {
			$this->setItemIdEncrypted( self::getEncryptedItemId( $strItemId ) );
			$this->setItemIdBindex( \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( $strItemId, CONFIG_SODIUM_KEY_BLIND_INDEX ) );
		}
	}

}
?>