<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAuthentications
 * Do not add any new functions to this class.
 */

class CCustomerAuthentications extends CBaseCustomerAuthentications {

	public static function fetchCustomerAuthenticationByCustomerIdByCustomerAuthenticationTypeIdByPropertyIdByCid( $intCustomerId, $intCCustomerAuthenticationTypeId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * from customer_authentications WHERE customer_id = ' . ( int ) $intCustomerId . ' AND property_id = ' . ( int ) $intPropertyId . ' AND customer_authentication_type_id = ' . ( int ) $intCCustomerAuthenticationTypeId . ' AND cid = ' . $intCid . ' LIMIT 1';
		return self::fetchCustomerAuthentication( $strSql, $objDatabase );
	}
}
?>