<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlHeaderTypes
 * Do not add any new functions to this class.
 */

class CGlHeaderTypes extends CBaseGlHeaderTypes {

	public static function fetchGlHeaderTypes( $strSql, $objClientDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CGlHeaderType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchGlHeaderType( $strSql, $objClientDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGlHeaderType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllGlHeaderTypes( $objClientDatabase, $boolIsPublished = false ) {

		$strSql = 'SELECT * FROM gl_header_types';

		if( false != $boolIsPublished ) {
			$strSql = 'SELECT * FROM gl_header_types WHERE is_published = 1 ORDER BY id';
		}

		return self::fetchGlHeaderTypes( $strSql, $objClientDatabase );
	}
}
?>