<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyTransferEntities
 * Do not add any new functions to this class.
 */

class CPropertyTransferEntities extends CBasePropertyTransferEntities {

	public static function fetchPropertyTransferEntitiesByPropertyTransferIdByCid( $intPropertyTransferId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM property_transfer_entities WHERE cid = ' . ( int ) $intCid . ' AND property_transfer_id = ' . ( int ) $intPropertyTransferId . ';';
		return self::fetchPropertyTransferEntities( $strSql, $objDatabase );
	}
}
?>