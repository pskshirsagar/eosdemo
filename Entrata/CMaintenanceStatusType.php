<?php

class CMaintenanceStatusType extends CBaseMaintenanceStatusType {

	const OPEN			 	= 1;
	const SUSPENDED			= 2;
	const COMPLETED			= 3;
	const CANCELLED			= 4;

	const CODE_MAINTENANCE_STATUS_TYPE_NEW 			 	= 'NE';
	const CODE_MAINTENANCE_STATUS_TYPE_OPEN 			= 'OP';
	const CODE_MAINTENANCE_STATUS_TYPE_SUSPENDED 	 	= 'SU';
	const CODE_MAINTENANCE_STATUS_TYPE_AWAITING_PARTS 	= 'AP';
	const CODE_MAINTENANCE_STATUS_TYPE_COMPLETED 	 	= 'CP';
	const CODE_MAINTENANCE_STATUS_TYPE_CANCELLED 	 	= 'CE';

	public static $c_arrintClosedStatusTypes = [
		self::COMPLETED,
		self::CANCELLED
	];

    public static function getMaintenanceStatusTypeNameById( $intId ) {
    	$strTemplateTypeName = NULL;

    	switch( $intId ) {

    		case CMaintenanceStatusType::OPEN:
    			$strTemplateTypeName = __( 'OPEN' );
    			break;

    		case CMaintenanceStatusType::SUSPENDED:
    			$strTemplateTypeName = __( 'SUSPENDED' );
    			break;

    		case CMaintenanceStatusType::COMPLETED:
    			$strTemplateTypeName = __( 'COMPLETED' );
    			break;

		    case CMaintenanceStatusType::CANCELLED:
			    $strTemplateTypeName = __( 'CANCELLED' );
			    break;

    		default:
    			// default case
    			break;
    	}

    	return $strTemplateTypeName;
    }

}
?>