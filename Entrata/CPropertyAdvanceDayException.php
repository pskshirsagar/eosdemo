<?php

class CPropertyAdvanceDayException extends CBasePropertyAdvanceDayException {

	/**
	 * @var CPropertyGlSetting
	 */
	private $m_objPropertyGlSetting;
	/**
	 * @var string
	 */
	private $m_strMonthName;

	/**
	 * @return CPropertyGlSetting
	 */
	public function getPropertyGlSetting() {
		if( true == is_null( $this->m_objPropertyGlSetting ) ) {
			$this->setPropertyGlSetting( CPropertyGlSettings::fetchPropertyGlSettingByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $this->getDatabase() ) );
		}
		return $this->m_objPropertyGlSetting;
	}

	/**
	 * @return string
	 */
	public function getMonthName() {
		return $this->m_strMonthName;
	}

	/**
	 * @param $objPropertyGlSetting
	 */
	public function setPropertyGlSetting( $objPropertyGlSetting ) {
		$this->m_objPropertyGlSetting = $objPropertyGlSetting;
	}

	/**
	 * @param $strMonthName
	 */
	private function setMonthName( $strMonthName ) {
		$this->m_strMonthName = $strMonthName;
	}

	/**
	 * @param      $arrmixValues
	 * @param bool $boolStripSlashes
	 * @param bool $boolDirectSet
	 */
	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( false == is_null( $this->getMonth() ) ) {
			$objDate		= DateTime::createFromFormat( '!m', $this->getMonth() );
			$this->setMonthName( $objDate->format( 'F' ) );
		}
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonth() {

		$boolIsValid = true;

		if( false == is_numeric( $this->getMonth() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'month', __( 'Month is required.' ) ) );

		} elseif( 2 == intval( $this->getMonth() ) ) {

			// Static value 2 represents February month which does not (always) have 29th, 30th or 31st

			if( true == is_numeric( $this->getArAdvanceDay() ) && 28 < $this->getArAdvanceDay() ) {
				$boolIsValid = false;
				$this->setArAdvanceDay( 28 );
			}

			if( true == is_numeric( $this->getApAdvanceDay() ) && 28 < $this->getApAdvanceDay() ) {
				$boolIsValid = false;
				$this->setApAdvanceDay( 28 );
			}

			if( true == is_numeric( $this->getGlAdvanceDay() ) && 28 < $this->getGlAdvanceDay() ) {
				$boolIsValid = false;
				$this->setGlAdvanceDay( 28 );
			}

			if( false == $boolIsValid ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'month', __( 'Invalid date is corrected.' ) ) );
			}

		} elseif( 4 == intval( $this->getMonth() ) || 6 == intval( $this->getMonth() ) || 9 == intval( $this->getMonth() ) || 11 == intval( $this->getMonth() ) ) {

			// Static value 4, 6, 9, 11 represents April, June, September and November month which does not have 31st

			if( true == is_numeric( $this->getArAdvanceDay() ) && 30 < $this->getArAdvanceDay() ) {
				$boolIsValid = false;
				$this->setArAdvanceDay( 30 );
			}

			if( true == is_numeric( $this->getApAdvanceDay() ) && 30 < $this->getApAdvanceDay() ) {
				$boolIsValid = false;
				$this->setApAdvanceDay( 30 );
			}

			if( true == is_numeric( $this->getGlAdvanceDay() ) && 30 < $this->getGlAdvanceDay() ) {
				$boolIsValid = false;
				$this->setGlAdvanceDay( 30 );
			}

			if( false == $boolIsValid ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'month', __( 'Invalid date is corrected.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valArAdvanceDay() {

		$objPropertyGlSetting = $this->getPropertyGlSetting();

		if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, 'property_gl_setting', __( 'Developer must set PropertyGlSettings object to the PropertyAdvanceDayException object before calling validate functions.' ) ) );
			return false;
		}

		if( true == is_numeric( $this->getArAdvanceDay() ) ) {

			if( true == is_numeric( $this->getGlAdvanceDay() ) && $this->getGlAdvanceDay() < $this->getArAdvanceDay() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_advance_day', __( 'AR closing day should not be greater than GL closing day of exception month.' ) ) );
				return false;
			}

			if( true == is_null( $this->getGlAdvanceDay() ) && true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' )
				&& $objPropertyGlSetting->getGLAdvanceDay() < $this->getArAdvanceDay() ) {

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_advance_day', __( 'AR closing day should not be greater than GL advance day.' ) ) );
				return false;
			}
		}

		return true;
	}

	public function valApAdvanceDay() {

		$objPropertyGlSetting = $this->getPropertyGlSetting();

		if( true == is_numeric( $this->getApAdvanceDay() ) ) {

			if( true == is_numeric( $this->getGlAdvanceDay() ) && $this->getGlAdvanceDay() < $this->getApAdvanceDay() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_advance_day', __( 'AP closing day should not be greater than GL closing day of exception month.' ) ) );
				return false;
			}

			if( true == is_null( $this->getGlAdvanceDay() ) && true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' )
				&& $objPropertyGlSetting->getGLAdvanceDay() < $this->getApAdvanceDay() ) {

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_advance_day', __( 'AP closing day should not be greater than GL advance day.' ) ) );
				return false;
			}
		}

		return true;
	}

	public function valGlAdvanceDay() {

		$boolIsValid = true;

		$objPropertyGlSetting = $this->getPropertyGlSetting();

		if( true == is_numeric( $this->getGlAdvanceDay() )
			&& ( ( false == is_numeric( $this->getArAdvanceDay() ) && $this->getGlAdvanceDay() < $objPropertyGlSetting->getArAdvanceDay() )
					|| ( false == is_numeric( $this->getApAdvanceDay() ) && $this->getGlAdvanceDay() < $objPropertyGlSetting->getApAdvanceDay() ) ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_advance_day', __( 'GL closing day should not be less than AP and AR advance day.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {

		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'advance_day_exception':
				$boolIsValid &= $this->valMonth();
				$boolIsValid &= $this->valArAdvanceDay();
				$boolIsValid &= $this->valApAdvanceDay();
				$boolIsValid &= $this->valGlAdvanceDay();
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * @param $intCurrentUserId
	 * @param $objDatabase
	 * @return bool|string|void
	 */
	public function insertOrUpdateOrDelete( $intCurrentUserId, $objDatabase ) {
		if( false === is_numeric( $this->getArAdvanceDay() ) && false === is_numeric( $this->getApAdvanceDay() ) && false === is_numeric( $this->getGlAdvanceDay() ) ) {
			if( true == is_null( $this->getId() ) ) {
				// not inserted yet, no values added, so just skip doing anything
				return true;
			}
			return $this->delete( $intCurrentUserId, $objDatabase );
		} else {
			return $this->insertOrUpdate( $intCurrentUserId, $objDatabase );
		}

	}

}