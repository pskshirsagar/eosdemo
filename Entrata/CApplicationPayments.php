<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationPayments
 * Do not add any new functions to this class.
 */

class CApplicationPayments extends CBaseApplicationPayments {

	public static function fetchApplicationPaymentsByApplicationIdByApplicationStageIdByApplicationStatusIdByCid( $intApplicationId, $intApplicationStageId, $intApplicationStatusId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						application_payments
					WHERE
						application_id = ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid . '
						AND application_stage_id = ' . ( int ) $intApplicationStageId . '
						AND application_status_id = ' . ( int ) $intApplicationStatusId;

		return self::fetchApplicationPayments( $strSql, $objDatabase );
	}

	public static function fetchApplicationPaymentByApplicationIdByArPaymentIdByCid( $intApplicationId, $intArPaymentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						application_payments
					WHERE
						application_id = ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid . '
						AND ar_payment_id = ' . ( int ) $intArPaymentId;

		return self::fetchApplicationPayment( $strSql, $objDatabase );
	}

	public static function fetchApplicationPaymentsCountByApplicationIdsByCidByExcludingPaymentStatusTypeIds( $arrintApplicationIds, $intCid, $arrintPaymentStatusTypeIds, $objDatabase ) {

		if( false == valArr( $arrintApplicationIds ) || false == valArr( $arrintPaymentStatusTypeIds ) ) return false;

		$strSql = 'SELECT
					    count(ap.id)
					FROM
						application_payments as ap
						JOIN ar_payments arp ON ( ( ap.ar_payment_id = arp.id AND arp.cid = ap.cid ) AND arp.batched_on IS NULL )
					WHERE
						ap.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND ap.cid = ' . ( int ) $intCid . '
						AND arp.payment_status_type_id NOT IN ( ' . implode( ',', $arrintPaymentStatusTypeIds ) . ' ) ';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];
		return 0;
	}

	public static function fetchApplicationPaymentsCountByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    count(ap.id)
					FROM
						application_payments as ap
						JOIN ar_payments arp ON ( ( ap.ar_payment_id = arp.id AND arp.cid = ap.cid ) AND arp.batched_on IS NULL )
					WHERE
						ap.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND ap.cid = ' . ( int ) $intCid;

		$arrstrData = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];
		return 0;
	}

}
?>