<?php

class CImportSource extends CBaseImportSource {

    public function valCid() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCid ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'You must provide an client id.' ) );
        }

        return $boolIsValid;
    }

    public function valIntegrationDatabaseId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intIntegrationDatabaseId ) || 1 > ( int ) $this->m_intIntegrationDatabaseId ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integration_database_id', 'Select the connection type.' ) );
        }

        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

        if( false == valStr( $this->getName() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Connection name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valDuplicateImportSource( $objDatabase ) {

    	$boolIsValid = true;

    	if( false == valStr( $this->getName() ) ) {
    		$boolIsValid = false;
    		// $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Duplicate import source.' ) );
    	}

    	if( true == is_numeric( $this->getCid() ) && true == is_numeric( $this->getIntegrationDatabaseId() ) ) {
		    $arrobjImportSources  = CImportSources::fetchCustomImportSourceByIntegrationDatabaseIdByCid( $this->getIntegrationDatabaseId(), $this->getCid(), $objDatabase );

    		if( true == valArr( $arrobjImportSources ) ) {
	    		$boolIsValid = false;
	    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Duplicate import batch please process current added batch.' ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valName();
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valIntegrationDatabaseId();
            	$boolIsValid &= $this->valDuplicateImportSource( $objDatabase );
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

           	default:
           		$boolIsValid = true;
           		break;
        }

        return $boolIsValid;
    }

}
?>