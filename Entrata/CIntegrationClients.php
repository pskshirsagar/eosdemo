<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationClients
 * Do not add any new functions to this class.
 */

class CIntegrationClients extends CBaseIntegrationClients {

    public static function fetchCustomIntegrationClientsByCid( $intCid, $objDatabase ) {
        return self::fetchIntegrationClients( sprintf( 'SELECT * FROM %s WHERE cid = %d ORDER BY integration_sync_type_id, order_num', 'integration_clients', ( int ) $intCid ), $objDatabase );
    }

    public static function fetchCustomIntegrationClientsByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objDatabase ) {
        return self::fetchIntegrationClients( sprintf( 'SELECT * FROM %s WHERE integration_database_id = %d AND cid = %d ORDER BY integration_sync_type_id, order_num', 'integration_clients', ( int ) $intIntegrationDatabaseId, ( int ) $intCid ), $objDatabase );
    }

    public static function fetchCustomIntegrationClientByIntegrationDatabaseNameByServiceIdByCid( $strIntegrationDatabaseName, $intIntegrationServiceId, $intCid, $objDatabase ) {
    	$strSql = 'SELECT
					    *
					FROM
					    integration_clients
					WHERE
						cid = ' . ( int ) $intCid . '
						AND integration_service_id =' . ( int ) $intIntegrationServiceId . '
					    AND integration_database_id IN (
					                                     SELECT
					                                         id.id
					                                     FROM
					                                         integration_databases id
					                                     WHERE
					                                         lower ( id.database_name ) = \'' . \Psi\CStringService::singleton()->strtolower( $strIntegrationDatabaseName ) . '\'
					                        AND id.cid = ' . ( int ) $intCid . '
					                        AND id.deleted_on IS NULL
					                    LIMIT 1 )';

    	return self::fetchIntegrationClient( $strSql, $objDatabase );
    }

    public static function fetchIntegrationClientByIntegrationServiceIdByIntegrationDatabaseIdByCid( $intIntegrationServiceId, $intIntegrationDatabaseId, $intCid, $objDatabase ) {
    	if( false == is_numeric( $intIntegrationServiceId ) || false == is_numeric( $intIntegrationDatabaseId ) ) return NULL;

    	$strSql = 'SELECT * FROM integration_clients WHERE integration_database_id = ' . ( int ) $intIntegrationDatabaseId . ' AND integration_service_id = ' . ( int ) $intIntegrationServiceId . ' AND cid = ' . ( int ) $intCid;

        return self::fetchIntegrationClient( $strSql, $objDatabase );
    }

    public static function fetchIntegrationClientsByIdsByCid( $arrintClientIntegrationIds, $intCid, $objDatabase ) {
        return self::fetchIntegrationClients( sprintf( 'SELECT * FROM %s WHERE cid = %d AND id IN ( %s ) ', 'integration_clients', ( int ) $intCid, implode( ',', $arrintClientIntegrationIds ) ), $objDatabase );
    }

    public static function fetchIntegrationClientByIntegrationDatabaseIdByServiceIdByCid( $intIntegrationDatabaseId, $intIntegrationServiceId, $intCid, $objDatabase ) {
    	$strSql = ' SELECT * FROM integration_clients
    				WHERE cid = ' . ( int ) $intCid . '
    					AND integration_database_id =' . ( int ) $intIntegrationDatabaseId . '
    					AND integration_service_id =' . ( int ) $intIntegrationServiceId;

        return self::fetchIntegrationClient( $strSql, $objDatabase );
    }

    public static function fetchIntegrationClientCountByIntegrationDatabaseIdByServiceIdByCid( $intIntegrationDatabaseId, $intIntegrationServiceId, $intCid, $objDatabase ) {
    	$strSql = ' WHERE cid = ' . ( int ) $intCid . '
    	AND integration_database_id =' . ( int ) $intIntegrationDatabaseId . '
    	AND integration_service_id =' . ( int ) $intIntegrationServiceId;

    	return self::fetchIntegrationClientCount( $strSql, $objDatabase );
    }

	public static function fetchIntegrationClientsByPropertyIdByIntegrationServiceIdsByIntegrationPeriodIdsByCid( $intPropertyId, $arrintIntegrationServiceIds, $arrintIntegrationPeriodIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIntegrationServiceIds ) || false == valArr( $arrintIntegrationPeriodIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( ic.id ) ic.integration_service_id
					FROM
						integration_clients ic,
						integration_databases id,
						property_integration_databases pid
					WHERE
						ic.integration_database_id = id.id
						AND ic.cid = id.cid
						AND id.id = pid.integration_database_id
						AND id.cid = pid.cid
						AND pid.property_id = ' . ( int ) $intPropertyId . '
						AND ic.cid = ' . ( int ) $intCid . '
						AND ic.integration_service_id IN(' . implode( ',', $arrintIntegrationServiceIds ) . ')
						AND ic.integration_period_id IN(' . implode( ',', $arrintIntegrationPeriodIds ) . ')';

		$arrintData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintData ) ) ? $arrintData[0]['integration_service_id'] : NULL;
	}

	public static function fetchIntegrationClientsByPropertyIdsByIntegrationServiceIdsByIntegrationSyncTypeIdsByCid( $arrintPropertyIds, $arrintIntegrationServiceIds, $arrintIntegrationSyncTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintIntegrationServiceIds ) || false == valArr( $arrintIntegrationSyncTypeIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( ic.id ) ic.*
					FROM
						integration_clients ic,
						integration_databases id,
						property_integration_databases pid
					WHERE
						ic.integration_database_id = id.id
						AND ic.cid = id.cid
						AND id.id = pid.integration_database_id
						AND id.cid = pid.cid
						AND ic.cid = ' . ( int ) $intCid . '
						AND pid.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ic.integration_service_id IN(' . implode( ',', $arrintIntegrationServiceIds ) . ')
						AND ic.integration_sync_type_id IN(' . implode( ',', $arrintIntegrationSyncTypeIds ) . ')';

    	return self::fetchIntegrationClients( $strSql, $objDatabase );
	}

    public static function fetchIntegrationClientByPropertyIdByIntegrationServiceIdByCid( $intPropertyId, $intIntegrationServiceId, $intCid, $objDatabase ) {

    	$strSql = 'SELECT
						DISTINCT ON ( ic.id ) ic.*,
						id.integration_client_status_type_id
						as integration_database_integration_client_status_type_id
					FROM
						integration_clients ic,
						integration_databases id,
						property_integration_databases pid
					WHERE
						ic.integration_database_id = id.id
    					AND ic.cid = id.cid
						AND id.id = pid.integration_database_id
    					AND id.cid = pid.cid
						AND pid.property_id = ' . ( int ) $intPropertyId . '
						AND ic.cid = ' . ( int ) $intCid . '
						AND ic.integration_service_id = ' . ( int ) $intIntegrationServiceId . '
					LIMIT 1';

    	return self::fetchIntegrationClient( $strSql, $objDatabase );
    }

	public static function fetchIntegrationClientsByPropertyIdByIntegrationServiceIdsByCid( $intPropertyId, $arrintIntegrationServiceIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( ic.id ) ic.*,
						id.integration_client_status_type_id
						as integration_database_integration_client_status_type_id
					FROM
						integration_clients ic,
						integration_databases id,
						property_integration_databases pid
					WHERE
						ic.integration_database_id = id.id
    					AND ic.cid = id.cid
						AND id.id = pid.integration_database_id
    					AND id.cid = pid.cid
						AND pid.property_id = ' . ( int ) $intPropertyId . '
						AND ic.integration_service_id IN (  ' . implode( ',', $arrintIntegrationServiceIds ) . ' )
						AND ic.cid = ' . ( int ) $intCid . ' ';

		return self::fetchIntegrationClients( $strSql, $objDatabase );
	}

	public static function fetchCustomActiveNightlyIntegrationClientsByIntegrationServiceIdsByIntegrationClientTypeIdsByIntegrationSyncTypeId( $arrintIntegrationServiceIds, $arrintIntegrationClientTypeIds, $arrintIntegrationSyncTypeIds, $objDatabase ) {
		if( false == valArr( $arrintIntegrationServiceIds ) || false == valArr( $arrintIntegrationClientTypeIds ) || false == valArr( $arrintIntegrationSyncTypeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						integration_clients
					WHERE
						integration_sync_type_id IN ( ' . implode( ',', $arrintIntegrationSyncTypeIds ) . ' )
						AND integration_client_status_type_id <> ' . CIntegrationClientStatusType::DISABLED . '
						AND integration_client_type_id IN( ' . implode( ',', $arrintIntegrationClientTypeIds ) . ')
						AND integration_service_id IN( ' . implode( ',', $arrintIntegrationServiceIds ) . ')
						AND integration_period_id IN( ' . CIntegrationPeriod::NIGHTLY . ', ' . CIntegrationPeriod::EVERY_TWO_DAYS . ', ' . CIntegrationPeriod::WEEKLY . ' )
						AND ( last_sync_on IS NULL
						        OR ( integration_period_id = ' . CIntegrationPeriod::NIGHTLY . ' )
								OR ( integration_period_id = ' . CIntegrationPeriod::EVERY_TWO_DAYS . ' AND ( last_sync_on < ( NOW() - INTERVAL \'1 days\' ) ) )
								OR ( integration_period_id = ' . CIntegrationPeriod::WEEKLY . ' AND ( last_sync_on < ( NOW() - INTERVAL \'6 days\' ) ) )
							)
					ORDER BY cid ASC, order_num';

		$arrobjIntegrationClients			= ( array ) parent::fetchIntegrationClients( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
		$arrobjIntegrationClientsReKeyed	= [];

		foreach( $arrobjIntegrationClients as $objIntegrationClient ) {
			$arrobjIntegrationClientsReKeyed[$objIntegrationClient->getCid()][$objIntegrationClient->getId()] = $objIntegrationClient;
		}

		return $arrobjIntegrationClientsReKeyed;
	}

	public static function fetchCustomActiveNightlyIntegrationClientsByMaintenanceIntegrationServiceIdsByIntegrationClientTypeIdsByIntegrationSyncTypeId( $arrintIntegrationServiceIds, $arrintIntegrationClientTypeIds, $arrintIntegrationSyncTypeIds, $objDatabase ) {
		if( false == valArr( $arrintIntegrationServiceIds ) || false == valArr( $arrintIntegrationClientTypeIds ) || false == valArr( $arrintIntegrationSyncTypeIds ) ) return NULL;

		$strSql = 'SELECT
						ic.*
					FROM
						integration_clients ic
						JOIN property_integration_databases pid ON ( pid.cid = ic.cid AND pid.integration_database_id = ic.integration_database_id )
					WHERE
						ic.integration_sync_type_id IN ( ' . implode( ',', $arrintIntegrationSyncTypeIds ) . ' )
						AND ic.integration_client_status_type_id <> ' . CIntegrationClientStatusType::DISABLED . '
						AND ic.integration_client_type_id IN( ' . implode( ',', $arrintIntegrationClientTypeIds ) . ')
						AND ic.integration_service_id IN( ' . implode( ',', $arrintIntegrationServiceIds ) . ')
						AND ic.integration_period_id IN( ' . CIntegrationPeriod::EVERY_TWO_DAYS . ', ' . CIntegrationPeriod::WEEKLY . ' )
						AND pid.created_on > ( NOW() - INTERVAL \'1 days\' )
					ORDER BY ic.cid ASC, ic.order_num';

		$arrobjIntegrationClients			= ( array ) parent::fetchIntegrationClients( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
		$arrobjIntegrationClientsReKeyed	= [];

		foreach( $arrobjIntegrationClients as $objIntegrationClient ) {
			$arrobjIntegrationClientsReKeyed[$objIntegrationClient->getCid()][$objIntegrationClient->getId()] = $objIntegrationClient;
		}

		return $arrobjIntegrationClientsReKeyed;
	}

	public static function fetchCustomAllActiveHourlyIntegrationClientsByIntegrationServiceIdsByIntegrationSyncTypeIds( $arrintIntegrationServiceIds, $arrintIntegrationSyncTypeIds, $objDatabase ) {
		if( false == valArr( $arrintIntegrationServiceIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						integration_clients
					WHERE
						integration_sync_type_id IN ( ' . implode( ',', $arrintIntegrationSyncTypeIds ) . ' )
						AND integration_client_status_type_id <> ' . CIntegrationClientStatusType::DISABLED . '
						AND integration_service_id IN( ' . implode( ',', $arrintIntegrationServiceIds ) . ')
						AND integration_period_id = ' . CIntegrationPeriod::HOURLY . '
						AND hourly_run_values IS NOT NULL
					ORDER BY
						cid ASC, order_num';

		return self::fetchIntegrationClients( $strSql, $objDatabase );
	}

	public static function fetchCustomActiveIntegrationClientsByIntegrationServiceId( $intIntegrationServiceId, $objDatabase ) {

		$strSql = 'SELECT * FROM integration_clients
							WHERE integration_sync_type_id <> 2
							AND   integration_client_status_type_id <> 3
							AND   integration_service_id = ' . ( int ) $intIntegrationServiceId . '
					ORDER BY cid, order_num';

		return self::fetchIntegrationClients( $strSql, $objDatabase );
	}

	public static function fetchActiveIntegrationClientsByIntegrationServiceIdsByCid( $arrstrIntegrationServicesIds, $intCid, $objDatabase, $boolIsFetchIdsOnly = false ) {
		if( false == valArr( $arrstrIntegrationServicesIds ) ) return NULL;

		if( true == $boolIsFetchIdsOnly ) {
			$strSql = 'SELECT id FROM integration_clients
							WHERE integration_client_status_type_id <> 3
							AND cid = ' . ( int ) $intCid . '
							AND integration_service_id IN ( ' . implode( ',', $arrstrIntegrationServicesIds ) . ')
					ORDER BY order_num';

			return fetchData( $strSql, $objDatabase );
		} else {
			$strSql = 'SELECT * FROM integration_clients
							WHERE integration_client_status_type_id <> 3
							AND cid = ' . ( int ) $intCid . '
							AND integration_service_id IN ( ' . implode( ',', $arrstrIntegrationServicesIds ) . ')
					ORDER BY order_num';

			return self::fetchIntegrationClients( $strSql, $objDatabase );
		}
	}

    public static function fetchCustomIntegrationClientsByIdsByCid( $arrintIntegrationClientIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIntegrationClientIds ) ) return NULL;
		$strSql = 'SELECT * FROM integration_clients WHERE id IN ( ' . implode( ',', $arrintIntegrationClientIds ) . ' ) AND cid = ' . ( int ) $intCid;
		return self::fetchIntegrationClients( $strSql, $objDatabase );
	}

	public static function fetchCustomIntegrationClientsByCompositeKey( $arrintIntegrationClientIdsKeyedByCid, $objDatabase ) {
		if( false == valArr( $arrintIntegrationClientIdsKeyedByCid ) ) return NULL;

		$arrmixIntegrationClientIdByCids = [];
		foreach( $arrintIntegrationClientIdsKeyedByCid as $intCid => $arrintIntegrationClientIds ) {
			foreach( $arrintIntegrationClientIds as $intIntegrationClientId ) {
				$arrmixIntegrationClientIdintCids[$intCid . ',' . $intIntegrationClientId] = '(' . $intCid . ',' . $intIntegrationClientId . ')';
			}
		}

		$strSql = 'SELECT * FROM integration_clients WHERE ( cid , id ) IN ( ' . implode( ',', $arrmixIntegrationClientIdintCids ) . ' )';

		$arrobjIntegrationClients = self::fetchIntegrationClients( $strSql, $objDatabase, false );

		$arrobjIntegrationClientsKeyedByClient = [];
		foreach( $arrobjIntegrationClients as $arrobjIntegrationClient ) {
			$arrobjIntegrationClientsKeyedByClient[$arrobjIntegrationClient->getCid()][$arrobjIntegrationClient->getId()] = $arrobjIntegrationClient;
		}

		return $arrobjIntegrationClientsKeyedByClient;
	}

	public static function fetchCustomIntegrationDatabaseNameKeyedByCid( $arrintIntegrationClientIdsKeyedByCid, $objDatabase ) {
		if( false == valArr( $arrintIntegrationClientIdsKeyedByCid ) ) return NULL;
		$arrstrIntegrationClientTypeIds = [ CIntegrationClientType::REAL_PAGE ];

		$arrmixIntegrationClientIdByCids = [];
		foreach( $arrintIntegrationClientIdsKeyedByCid as $intCid => $arrintIntegrationClientIds ) {
			foreach( $arrintIntegrationClientIds as $intIntegrationClientId ) {
				$arrmixIntegrationClientIdintCids[$intCid . ',' . $intIntegrationClientId] = '(' . $intCid . ',' . $intIntegrationClientId . ')';
			}
		}

		$strSql = 'SELECT
							ic.id,
							id.cid,
							CASE
						      WHEN ic.integration_client_type_id IN ( ' . implode( ',', $arrstrIntegrationClientTypeIds ) . ' ) THEN id.name
						      ELSE id.database_name
						    END AS name
					   FROM
							integration_clients ic
					 		join integration_databases id on ( ic.integration_database_id = id.id AND ic.cid = id.cid )
					   WHERE
							( ic.cid , ic.id ) IN ( ' . implode( ',', $arrmixIntegrationClientIdintCids ) . ' )';

		$arrobjCustomIntegrationDatabases = self::fetchIntegrationClients( $strSql, $objDatabase, false );

		$arrobjIntegrationClientsKeyedByClient = [];
		foreach( $arrobjCustomIntegrationDatabases as $arrobjCustomIntegrationDatabase ) {
			$arrobjIntegrationDatabaseNameKeyedByClient[$arrobjCustomIntegrationDatabase->getCid()][$arrobjCustomIntegrationDatabase->getId()] = $arrobjCustomIntegrationDatabase->getIntegrationClientDatabaseName();
		}

		return $arrobjIntegrationDatabaseNameKeyedByClient;
	}

	public static function fetchIntegrationClientsByIntegrationDatabaseIdByIntegrationServiceIdsByCid( $intIntegrationDatabaseId, $arrintIntegrationServiceIds, $intCid, $objDatabase, $boolIsActiveIntegrationClient = false ) {
		if( false == valArr( $arrintIntegrationServiceIds ) ) return NULL;

		$strSubCondition = '';
		if( true == $boolIsActiveIntegrationClient ) {
			$strSubCondition = ' AND integration_client_status_type_id <> ' . CIntegrationClientStatusType::DISABLED . '';
		}

		$strSql = 'SELECT * FROM integration_clients
					WHERE cid = ' . ( int ) $intCid . '
					  AND integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
					  AND integration_service_id IN ( ' . implode( ',', $arrintIntegrationServiceIds ) . ')' . $strSubCondition;

		return self::fetchIntegrationClients( $strSql, $objDatabase );
	}

	public static function fetchCustomIntegrationClientsBySearchFilter( $objClientDatabase, $objIntegratedClientFilter = NULL ) {

		if( true == valObj( $objIntegratedClientFilter, 'CIntegratedClientsFilter' ) ) {
			$arrstrAndSearchParameters = self::fetchSearchCriteria( $objIntegratedClientFilter );
		}

		$strSql = 'SELECT
						ic.*,
						icst.name as integration_client_status_type_name,
						id.name as integration_database_name,
						c.company_name as client_name,
						c.company_status_type_id as client_status_type_id,
						ict.name as integration_client_type_name,
						iv.name as integration_version_name,
						cst.name as company_status_type_name,
						ist.name as integration_sync_type_name,
						ip.name as integration_period_name,
						iss.name as integration_service_name,
						c.database_id
					FROM
						integration_clients ic
						JOIN integration_client_status_types icst ON( ic.integration_client_status_type_id = icst.id )
						JOIN integration_databases id ON( ic.integration_database_id = id.id AND ic.cid = id.cid )
						JOIN clients c ON( ic.cid = c.id )
						JOIN company_status_types cst ON( c.company_status_type_id = cst.id )
						JOIN integration_client_types ict ON( id.integration_client_type_id = ict.id )
						JOIN integration_services iss ON( iss.id = ic.integration_service_id )
						LEFT JOIN integration_sync_types ist ON( ist.id = ic.integration_sync_type_id )
						LEFT JOIN integration_periods ip ON( ip.id = ic.integration_period_id )
						LEFT JOIN integration_versions iv ON( id.integration_version_id = iv.id )';

		$strSql .= ( true == valArr( $arrstrAndSearchParameters ) ) ? ' WHERE ' . implode( ' AND ', $arrstrAndSearchParameters ) : '';

		$strSql .= ' ORDER BY
						c.company_name ASC, id.id, ist.name, iss.name';

		return self::fetchIntegrationClients( $strSql, $objClientDatabase );
	}

	public static function fetchSearchCriteria( $objIntegratedClientFilter = NULL ) {

		$arrstrAndSearchParameters = NULL;

		if( false == is_null( $objIntegratedClientFilter->getIntegrationClientTypeIds() ) )			$arrstrAndSearchParameters[] = 'id.integration_client_type_id IN ( ' . $objIntegratedClientFilter->getIntegrationClientTypeIds() . ' ) ';
		if( false == is_null( $objIntegratedClientFilter->getCompanyStatusTypeIds() ) )				$arrstrAndSearchParameters[] = 'c.company_status_type_id IN ( ' . $objIntegratedClientFilter->getCompanyStatusTypeIds() . ' ) ';
		if( false == is_null( $objIntegratedClientFilter->getIntegrationServiceIds() ) )			$arrstrAndSearchParameters[] = 'ic.integration_service_id IN ( ' . $objIntegratedClientFilter->getIntegrationServiceIds() . ' ) ';
		if( false == is_null( $objIntegratedClientFilter->getIntegrationClientStatusTypeIds() ) )	$arrstrAndSearchParameters[] = 'ic.integration_client_status_type_id IN ( ' . $objIntegratedClientFilter->getIntegrationClientStatusTypeIds() . ' ) ';
		if( false == is_null( $objIntegratedClientFilter->getIntegrationSyncTypeIds() ) )			$arrstrAndSearchParameters[] = 'ic.integration_sync_type_id IN ( ' . $objIntegratedClientFilter->getIntegrationSyncTypeIds() . ' ) ';
		if( false == is_null( $objIntegratedClientFilter->getIntegrationPeriodIds() ) )				$arrstrAndSearchParameters[] = 'ic.integration_period_id IN ( ' . $objIntegratedClientFilter->getIntegrationPeriodIds() . ' ) ';
		if( true == $objIntegratedClientFilter->getIsHourlyRunValuesNotNull() ) 					$arrstrAndSearchParameters[] = 'ic.hourly_run_values IS NOT NULL';
		if( true == $objIntegratedClientFilter->getIsFailedScheduledService() ) 					$arrstrAndSearchParameters[] = ' (
																																( integration_period_id = ' . CIntegrationPeriod::NIGHTLY . ' AND ( ic.last_sync_on IS NULL OR last_sync_on < ( NOW() - INTERVAL \'24 hours\' ) ) )
																																OR ( integration_period_id = ' . CIntegrationPeriod::EVERY_TWO_DAYS . ' AND ( ic.last_sync_on IS NULL OR last_sync_on < ( NOW() - INTERVAL \'48 hours\' ) ) )
																																OR ( integration_period_id = ' . CIntegrationPeriod::WEEKLY . ' AND ( ic.last_sync_on IS NULL OR last_sync_on < ( NOW() - INTERVAL \'7 days\' ) ) )
																																)';
		if( false == is_null( $objIntegratedClientFilter->getIsSelfHosted() ) )						$arrstrAndSearchParameters[] = 'id.is_self_hosted = ' . $objIntegratedClientFilter->getIsSelfHosted();

		if( true == valArr( $arrstrAndSearchParameters ) ) {
			return $arrstrAndSearchParameters;
		} else {
			return NULL;
		}
	}

	public static function fetchActiveIntegrationClientByServiceIdByIntegrationDatabaseIdByCid( $intIntegrationServiceId, $intIntegrationDatabaseId, $intCid, $objDatabase ) {
    	$strSql = ' SELECT
    					*
    				FROM
    					integration_clients
    				WHERE
    					cid = ' . ( int ) $intCid . '
    					AND integration_service_id =' . ( int ) $intIntegrationServiceId . '
    					AND integration_database_id =' . ( int ) $intIntegrationDatabaseId . '
    					AND integration_client_status_type_id <> 3';

        return self::fetchIntegrationClient( $strSql, $objDatabase );
    }

    public static function fetchActiveIntegrationClientCountByIntegrationServiceIdsByCid( $arrstrIntegrationServicesIds, $intCid, $objDatabase ) {
    	if( false == valArr( $arrstrIntegrationServicesIds ) ) return NULL;

    	$strWhere = 'WHERE integration_client_status_type_id <> 3
					AND cid = ' . ( int ) $intCid . '
					AND integration_service_id IN ( ' . implode( ',', $arrstrIntegrationServicesIds ) . ')';

    	return self::fetchIntegrationClientCount( $strWhere, $objDatabase );
    }

    public static function fetchIntegrationClientsByIntegrationClientStatusTypeIdsByIntegrationDatabaseIdByCid( $arrintIntegrationClientStatusTypeIds, $intIntegrationDatabaseId, $intCid, $objDatabase ) {
    	if( false == valArr( $arrintIntegrationClientStatusTypeIds ) ) return NULL;

    	$strWhere = '
    				 SELECT
    					*
    				FROM
    					integration_clients
    				WHERE integration_client_status_type_id IN ( ' . implode( ',', $arrintIntegrationClientStatusTypeIds ) . ' )
					AND cid = ' . ( int ) $intCid . '
					AND integration_database_id =' . ( int ) $intIntegrationDatabaseId;

    	return self::fetchIntegrationClients( $strWhere, $objDatabase );
    }

	public static function fetchIntegrationClientLastSyncOnByIntegrationDatabaseIdByIntegrationServiceIdByCid( $intIntegrationDatabaseId, $intIntegrationServiceId, $intCid, $objDatabase ) {

		$strSql = 'SELECT last_sync_on
					FROM integration_clients
					WHERE integration_database_id = ' . ( int ) $intIntegrationDatabaseId . ' AND
						integration_service_id = ' . ( int ) $intIntegrationServiceId . ' AND
						cid = ' . ( int ) $intCid;

		$strIntegrationClientLastSyncOn = fetchData( $strSql, $objDatabase );

		return ( true == valStr( $strIntegrationClientLastSyncOn ) ) ? $strIntegrationClientLastSyncOn : NULL;
	}

}
?>