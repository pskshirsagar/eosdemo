<?php

class CGlReconciliationItem extends CBaseGlReconciliationItem {

	protected $m_boolIsSelected;

	protected $m_strGlReconciliationBeginningDate;
	protected $m_strGlReconciliationStatementDate;

	/**
	 * Get Functions
	 */

    public function getGlReconciliationBeginningDate() {
    	return $this->m_strGlReconciliationBeginningDate;
    }

    public function getGLReconciliationStatementDate() {
    	return $this->m_strGlReconciliationStatementDate;
    }

	public function getIsSelected() {
		return $this->m_boolIsSelected;
	}

	/**
	 * Set Functions
	 */

    public function setGlReconciliationBeginningDate( $strGlReconciliationBeginningDate ) {
    	return $this->m_strGlReconciliationBeginningDate = $strGlReconciliationBeginningDate;
    }

    public function setGlReconciliationStatementDate( $strGlReconciliationStatementDate ) {
    	return $this->m_strGlReconciliationStatementDate = $strGlReconciliationStatementDate;
    }

	public function setIsSelected( $boolIsSelected ) {
		return $this->m_boolIsSelected = $boolIsSelected;
	}

	public function setDate( $strDate, $boolIsBankRecOnPostMonth = false ) {

		if( false == $boolIsBankRecOnPostMonth ) {
			parent::setDate( $strDate );

		} else {
			if( true == valStr( $strDate ) ) {

				$arrstrPostMonth = explode( '/', $strDate );

				if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
					$strDate = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];
				}
			}

			$this->m_strDate = $strDate;
		}
	}

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['statement_date'] ) ) $this->setGlReconciliationStatementDate( $arrmixValues['statement_date'] );
    	if( true == isset( $arrmixValues['beginning_date'] ) ) $this->setGlReconciliationBeginningDate( $arrmixValues['beginning_date'] );

    	return;
    }

	/**
	 * Validate Functions
	 */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valGlReconciliationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAmount() {
        $boolIsValid = true;

        if( true == is_null( $this->getAmount() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', 'Reconciliation amount is required.' ) );
        }

        return $boolIsValid;
    }

    public function valNotes() {
        $boolIsValid = true;

        if( true == is_null( $this->getNotes() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', 'Description is required.' ) );
        }

        return $boolIsValid;
    }

    public function valDate( $boolIsBankRecOnPostMonth ) {

        $boolIsValid = true;
		if( false == $boolIsBankRecOnPostMonth && true == is_null( $this->getDate() ) ) {
	       	$boolIsValid = false;
	       	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', 'Reconciling item date is required.' ) );
	    } elseif( false == $boolIsBankRecOnPostMonth && false == preg_match( '/(((^[0-9]{1})|0[0-9]|1[0-9]|2[0-3]))/', $this->getDate() ) ) {
	       	$boolIsValid = false;
	       	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', 'Reconciling item date of format mm/dd/yyyy is required.' ) );
	    } elseif( false == $boolIsBankRecOnPostMonth && false == is_null( $this->getDate() ) ) {
			$strDate = __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getDate() ] );
			$intMonth	= ( int ) \Psi\CStringService::singleton()->substr( $strDate, 0, 2 );
	    	$intDay		= ( int ) \Psi\CStringService::singleton()->substr( $strDate, 3, 2 );
	    	$intYear	= ( int ) \Psi\CStringService::singleton()->substr( $strDate, 6, 4 );

		    if( false == checkdate( $intMonth, $intDay, $intYear ) ) {
		    	$boolIsValid = false;
		    	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', 'Reconcile item date is invalid.' ) );
		    }

		    if( ( strtotime( $this->getGLReconciliationStatementDate() ) < strtotime( $this->getDate() ) ) || ( true == valStr( $this->getGlReconciliationBeginningDate() && strtotime( $this->getGlReconciliationBeginningDate() ) > strtotime( $this->getDate() ) ) ) ) {
		    	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', 'Reconciliation item date is invalid.' ) );
		    	$boolIsValid &= false;
		    }

		} elseif( true == $boolIsBankRecOnPostMonth && true == is_null( $this->getDate() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', 'Reconciling item through post month is required.' ) );
		} elseif( true == $boolIsBankRecOnPostMonth && false == is_null( $this->getDate() ) && false == preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->getDate() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', 'Reconciling item through post month of format mm/yyyy is required.' ) );

		} elseif( ( true == $boolIsBankRecOnPostMonth && false == is_null( $this->getDate() ) && ( strtotime( $this->getGLReconciliationStatementDate() ) < strtotime( $this->getDate() ) ) )
			|| ( true == valStr( $this->getGlReconciliationBeginningDate() && strtotime( $this->getGlReconciliationBeginningDate() ) > strtotime( $this->getDate() ) ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', 'Reconciliation item through post month is invalid.' ) );
			$boolIsValid &= false;
		}

        return $boolIsValid;
    }

	public function valImportedReconciliationItemsFile( $arrmixFile, $objGlReconciliation, $boolIsBankRecOnPostMonth ) {

		$boolIsValid = true;

		if( false == valArr( $arrmixFile ) || UPLOAD_ERR_NO_FILE == $arrmixFile['error'] ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'no_file', 'No file was uploaded. Select a file to upload.' ) );
			return $boolIsValid;
		}

		$strExtension				= pathinfo( $arrmixFile['name'], PATHINFO_EXTENSION );
		$strUploadMaxFilesize		= ini_get( 'upload_max_filesize' );
		$strUploadTempDir			= PATH_NON_BACKUP_MOUNTS_GLOBAL_TMP;
		$intMaxUpload				= ( int ) str_replace( 'M', '', $strUploadMaxFilesize ) * 1024 * 1024;
		$arrstrRequiredHeaders		= array( 'date', 'amount', 'description', 'reference_number' );
		$arrmixAllowedExtensions	= array( 'csv'	=> array( 'mimes' => array( 'application/vnd.ms-excel' ), 'exts' => array( 'csv' ) ) );

		if( false != valStr( $strExtension ) && false == array_key_exists( $strExtension, $arrmixAllowedExtensions ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_type', 'Cannot upload other than CSV file.' ) );
			return $boolIsValid;
		}

		if( true == valArr( $arrmixFile ) && ( $intMaxUpload < $arrmixFile['size'] || UPLOAD_ERR_OK != $arrmixFile['error'] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_size', 'Files larger than ' . $strUploadMaxFilesize . 'B cannot be uploaded.' ) );
			return $boolIsValid;
		}

		$strTempUri = getUniqueString();

		if( true == valArr( $arrmixFile ) && ( false == move_uploaded_file( $arrmixFile['tmp_name'], $strUploadTempDir . $strTempUri ) || UPLOAD_ERR_NO_TMP_DIR == $arrmixFile['error'] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_upload', $arrmixFile['name'] . ' could not be moved to the temporary directory.' ) );
			return $boolIsValid;
		}

		// Feed data from CSV file.
		$arrmixRecItemsData	= ( array ) $this->parseCsvFileImport( $arrstrRequiredHeaders, $strUploadTempDir . $strTempUri );

		// Check if file is empty
		if( false == valArr( $arrmixRecItemsData ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'empty_file', 'File is missing data.' ) );
			return $boolIsValid;
		}

		foreach( $arrmixRecItemsData as $intRefKey => $arrmixRecItemData ) {

			$fltRecItemAmount			= NULL;
			$strRecItemDescription		= NULL;
			$strRecItemReferenceNumber	= NULL;

			if( true == isset( $arrmixRecItemData['amount'] ) ) {
				$fltRecItemAmount = trim( $arrmixRecItemData['amount'] );
			}

			if( true == isset( $arrmixRecItemData['description'] ) ) {
				$strRecItemDescription = trim( $arrmixRecItemData['description'] );
			}

			if( true == isset( $arrmixRecItemData['reference_number'] ) ) {
				$strRecItemReferenceNumber = trim( $arrmixRecItemData['reference_number'] );
			}

			// Date validation
			if( true == $boolIsBankRecOnPostMonth ) {

				$strRecPostMonth = NULL;

				if( true == isset( $arrmixRecItemData['post_month'] ) ) {
					$strRecPostMonth = trim( $arrmixRecItemData['post_month'] );
				}

				if( false == valStr( $strRecPostMonth ) ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Post month is required for Row #' . ( $intRefKey + 2 ) . ' Column #Post Month.' ) );

				} elseif( 7 < strlen( $strRecPostMonth ) ) {

					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Post month is invalid for Row #' . ( $intRefKey + 2 ) . ' Column #Post month.' ) );
					$boolIsValid &= false;
				} elseif( false == preg_match( '#^(((0?[1-9])|(1[012]))/([12]\d)?\d\d)$#', $strRecPostMonth ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Post month of format mm/yyyy is required for Row #' . ( $intRefKey + 2 ) . ' Column #Post month.' ) );
				} else {

					$intYear								= ( int ) \Psi\CStringService::singleton()->substr( $strRecPostMonth, 3, 4 );
					$intMonth								= ( int ) \Psi\CStringService::singleton()->substr( $strRecPostMonth, 0, 2 );
					$arrmixRecItemsData[$intRefKey]['date']	= $intMonth . '/01/' . $intYear;

					if( false == checkdate( $intMonth, 1, $intYear ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Post month is invalid for Row #' . ( $intRefKey + 2 ) . ' Column #Post month.' ) );
					} elseif( ( strtotime( $this->getGLReconciliationStatementDate() ) < strtotime( $arrmixRecItemsData[$intRefKey]['date'] ) )
								|| ( true == valStr( $this->getGlReconciliationBeginningDate() && strtotime( $this->getGlReconciliationBeginningDate() ) > strtotime( $arrmixRecItemsData[$intRefKey]['date'] ) ) ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Post month is invalid for Row #' . ( $intRefKey + 2 ) . ' Column #Post month.' ) );
						$boolIsValid &= false;
					}
				}
			} else {

				$strRecItemDate = NULL;

				if( true == isset( $arrmixRecItemData['date'] ) ) {
					$strRecItemDate = trim( $arrmixRecItemData['date'] );
				}

				if( false == valStr( $strRecItemDate ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', 'Date is required for Row #' . ( $intRefKey + 2 ) . ' Column #Date.' ) );
				} elseif( false == preg_match( '/(((^[0-9]{1})|0[0-9]|1[0-9]|2[0-3]))/', $strRecItemDate ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', 'Date of format mm/dd/yyyy is required for Row #' . ( $intRefKey + 2 ) . ' Column #Date.' ) );
				} elseif( true == valStr( $strRecItemDate ) ) {

					$intDay		= ( int ) \Psi\CStringService::singleton()->substr( $strRecItemDate, 3, 2 );
					$intYear	= ( int ) \Psi\CStringService::singleton()->substr( $strRecItemDate, 6, 4 );
					$intMonth	= ( int ) \Psi\CStringService::singleton()->substr( $strRecItemDate, 0, 2 );

					if( false == checkdate( $intMonth, $intDay, $intYear ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', 'Date is invalid for Row #' . ( $intRefKey + 2 ) . ' Column #Date.' ) );
					} elseif( ( strtotime( $objGlReconciliation->getStatementDate() ) < strtotime( $strRecItemDate ) )
							|| ( true == valStr( $objGlReconciliation->getBeginningDate() && strtotime( $objGlReconciliation->getBeginningDate() ) > strtotime( $strRecItemDate ) ) ) ) {

						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', 'Date is invalid for Row #' . ( $intRefKey + 2 ) . ' Column #Date.' ) );
						$boolIsValid &= false;
					}
				}
			}

			// Amount validation
			if( false == valStr( $fltRecItemAmount ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', 'Amount is required Row #' . ( $intRefKey + 2 ) . ' Column #Amount.' ) );
			} elseif( 0 == ( float ) $fltRecItemAmount ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', 'Amount is invalid Row #' . ( $intRefKey + 2 ) . ' Column #Amount.' ) );
			} elseif( 12 < strlen( $fltRecItemAmount ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', 'Amount can not be greater than 12 digits Row #' . ( $intRefKey + 2 ) . ' Column #Amount.' ) );
			}

			// Description validation
			if( false == valStr( $strRecItemDescription ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required Row #' . ( $intRefKey + 2 ) . ' Column #Description.' ) );
			}

			if( true == valStr( $strRecItemReferenceNumber ) && 40 < strlen( $strRecItemReferenceNumber ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reference_number', 'Reference number length should be less 40 characters Row #' . ( $intRefKey + 2 ) . ' Column #Reference number.' ) );
			}
		}

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		return $arrmixRecItemsData;
	}

	public function parseCsvFileImport( $arrstrRequiredHeaders, $strFileUri ) {

		$arrmixRawData			= array();
		$boolHeaderRowSelected	= false;

		if( false == file_exists( $strFileUri ) ) {
			$this->displayMessage( 'Application Error: Failed to load file.', NULL, NULL, NULL, NULL, 1 );
			exit;
		}

		if( false !== ( $resHandle = fopen( $strFileUri, 'r' ) ) ) {

			while( false !== ( $arrmixData = fgetcsv( $resHandle, 0, ',' ) ) ) {

				if( \Psi\Libraries\UtilFunctions\count( $arrmixData ) != \Psi\Libraries\UtilFunctions\count( $arrstrRequiredHeaders ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'headers', 'Column headers not present or do not match required headers.' ) );
					return;
				}

				if( false == $boolHeaderRowSelected ) {
					$boolHeaderRowSelected	= ftell( $resHandle );
					$arrstrCsvHeader		= array_map( 'str_replace', array_fill( 0, \Psi\Libraries\UtilFunctions\count( $arrmixData ), ' ' ), array_fill( 0, \Psi\Libraries\UtilFunctions\count( $arrmixData ), '_' ), array_map( 'strtolower', array_map( 'trim', $arrmixData ) ) );
					continue;
				}

				if( \Psi\Libraries\UtilFunctions\count( $arrstrCsvHeader ) == \Psi\Libraries\UtilFunctions\count( $arrmixData ) ) {
					$arrmixRawData[] = array_combine( $arrstrCsvHeader, $arrmixData );
				}
			}

			fclose( $resHandle );
		}
		return $arrmixRawData;
	}

    public function validate( $strAction, $boolIsBankRecOnPostMonth = false ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valDate( $boolIsBankRecOnPostMonth );
            	$boolIsValid &= $this->valAmount();
            	$boolIsValid &= $this->valNotes();
				break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

			case 'validate_cc_bank_rec':
				$boolIsValid &= $this->valAmount();
				break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>