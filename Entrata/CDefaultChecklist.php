<?php

class CDefaultChecklist extends CBaseDefaultChecklist {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChecklistTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChecklistTriggerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsEnabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>