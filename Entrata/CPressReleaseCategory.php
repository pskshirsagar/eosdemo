<?php

class CPressReleaseCategory extends CBasePressReleaseCategory {

    /**
     * Fetch Functions
     */

    public function fetchPressReleases( $objDatabase ) {

		return \Psi\Eos\Entrata\CPressReleases::createService()->fetchPressReleasesByPressReleaseCategoryIdByCid( $this->getId(), $this->getCid(), $objDatabase );
    }

    /**
     * Validate Functions
     */

     public function valName( $objDatabase = NULL ) {

    	$boolIsValid = true;

        if( false == isset( $this->m_strName ) || 1 > strlen( $this->m_strName ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Title is required.' ) ) );

        } else {
        	$intConflictingPressReleaseCategoryCount = \Psi\Eos\Entrata\CPressReleaseCategories::createService()->fetchConflictingPressReleaseCategoryCount( $this->getCid(), $this->getName(), $this->getId(), $objDatabase );

			if( 0 < $intConflictingPressReleaseCategoryCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Title is already in use.' ) ) );
				$boolIsValid = false;
			}
        }

        return $boolIsValid;
     }

    public function valDescription( $objDatabase = NULL ) {

    	$boolIsValid = true;

        if( false == isset( $this->m_strDescription ) || 1 > strlen( $this->m_strDescription ) ) {
            $boolIsValid &= false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'You must enter a description.' ) ) );

        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
           		$boolIsValid &= $this->valName( $objDatabase );
            	// $boolIsValid &= $this->valDescription();
                break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName( $objDatabase );
            	// $boolIsValid &= $this->valDescription();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>