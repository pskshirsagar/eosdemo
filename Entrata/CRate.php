<?php
use Psi\Eos\Entrata\CUnitSpaces;

class CRate extends CBaseRate {

	protected $m_intArCodeTypeId;
	protected $m_intArTriggerTypeId;
	protected $m_intLeaseTermOccupancyTypeId;
	protected $m_intLeaseExpiringCount;
	protected $m_intArOriginReferenceGroupId;
	protected $m_intSpecialQuantityRemaining;
	protected $m_intSpecialMaxDaysToLeaseStart;
	protected $m_intExternalReferenceNumber;

	protected $m_strArCodeName;
	protected $m_strSpecialName;
	protected $m_strLeaseTermName;
	protected $m_strLeaseStartWindow;
	protected $m_strArFormulaReferenceName;
	protected $m_strArTriggerName;
	protected $m_strRateRange;
	protected $m_strTaxStartDate;

	protected $m_fltTaxPercent;
	protected $m_fltTaxAmount;
	/**
	 * @var array Used by CRatesLibrary to collapse related rates into a single rate with children
	 */
	protected $m_arrobjChildRates;

	protected $m_arrstrRateRanges;

	// Overrides

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrmixValues['ar_code_type_id'] );
		if( true == isset( $arrmixValues['ar_trigger_type_id'] ) ) $this->setArTriggerTypeId( $arrmixValues['ar_trigger_type_id'] );
		if( true == isset( $arrmixValues['ar_code_name'] ) ) $this->setArCodeName( $arrmixValues['ar_code_name'] );
		if( true == isset( $arrmixValues['ar_origin_reference_group_id'] ) ) $this->setArOriginReferenceGroupId( $arrmixValues['ar_origin_reference_group_id'] );
		if( true == isset( $arrmixValues['lease_term_name'] ) ) $this->setLeaseTermName( $arrmixValues['lease_term_name'] );
		if( true == isset( $arrmixValues['special_name'] ) ) $this->setSpecialName( $arrmixValues['special_name'] );
		if( true == isset( $arrmixValues['special_quantity_remaining'] ) ) $this->setSpecialQuantityRemaining( $arrmixValues['special_quantity_remaining'] );
		if( true == isset( $arrmixValues['special_max_days_to_lease_start'] ) ) $this->setSpecialMaxDaysToLeaseStart( $arrmixValues['special_max_days_to_lease_start'] );
		if( true == isset( $arrmixValues['lease_expiring_count'] ) ) $this->setLeaseExpiringCount( $arrmixValues['lease_expiring_count'] );
		if( true == isset( $arrmixValues['ar_formula_reference_name'] ) ) $this->setArFormulaReferenceName( $arrmixValues['ar_formula_reference_name'] );
		if( true == isset( $arrmixValues['lease_term_occupancy_type_id'] ) ) $this->setLeaseTermOccupancyTypeId( $arrmixValues['lease_term_occupancy_type_id'] );
		if( true == isset( $arrmixValues['tax_percent'] ) ) $this->setTaxPercent( $arrmixValues['tax_percent'] );
		if( true == isset( $arrmixValues['tax_amount'] ) ) $this->setTaxAmount( $arrmixValues['tax_amount'] );
		if( true == isset( $arrmixValues['lease_start_window'] ) ) $this->setLeaseStartWindow( $arrmixValues['lease_start_window'] );
	}

	// Getters

	public function getArCodeTypeId() {
		return $this->m_intArCodeTypeId;
	}

	public function getArTriggerTypeId() {
		return $this->m_intArTriggerTypeId;
	}

	public function getArCodeName() {
		return $this->m_strArCodeName;
	}

	public function getArOriginReferenceGroupId() {
		return $this->m_intArOriginReferenceGroupId;
	}

	public function getChildRates() {
		return $this->m_arrobjChildRates;
	}

	public function getLeaseTermName() {
		return $this->m_strLeaseTermName;
	}

	public function getLeaseStartWindow() {
		return $this->m_strLeaseStartWindow;
	}

	public function getArTriggerName() {
		return $this->m_strArTriggerName;
	}

	public function getIsDeleted() {
		if( true == $this->hasChildRates() ) {
			return array_reduce( $this->getChildRates(), function( $boolCarry, CRate $objRate ) {
				return $boolCarry && $objRate->getIsDeleted();
			}, true );
		} else {
			return 0 == bccomp( 0, $this->getRateAmount(), 2 ) && 0 == bccomp( 0, $this->getRateIncreaseIncrement(), 2 ) && false == $this->getShowInEntrata() && false == $this->getShowOnWebsite();
		}
	}

	public function getDisplayRateAmount() {
		if( true == $this->hasChildRates() ) {
			if( false == isset( $this->m_arrstrRateRanges ) ) {
				$this->computeRateRanges();
			}
			return $this->m_arrstrRateRanges['rate_amount'];
		} else {
			if( CArOrigin::SPECIAL == $this->getArOriginId() && 0 != bccomp( $this->getRateAmount(), 0 ) ) {
				return -1 * $this->getRateAmount();
			} else {
				return $this->getRateAmount();
			}
		}
	}

	public function getDisplayMinRateAmount() {
		if( true == $this->hasChildRates() ) {
			if( false == isset( $this->m_arrstrRateRanges ) ) {
				$this->computeRateRanges();
			}
			return $this->m_arrstrRateRanges['min_rate_amount'];
		} else {
			if( CArOrigin::SPECIAL == $this->getArOriginId() && 0 != bccomp( $this->getRateAmount(), 0 ) ) {
				return -1 * $this->getRateAmount();
			} else {
				return $this->getRateAmount();
			}
		}
	}

	public function getDisplayMaxRateAmount() {
		if( true == $this->hasChildRates() ) {
			if( false == isset( $this->m_arrstrRateRanges ) ) {
				$this->computeRateRanges();
			}
			return $this->m_arrstrRateRanges['max_rate_amount'];
		} else {
			if( CArOrigin::SPECIAL == $this->getArOriginId() && 0 != bccomp( $this->getRateAmount(), 0 ) ) {
				return -1 * $this->getRateAmount();
			} else {
				return $this->getRateAmount();
			}
		}
	}

	public function getDisplayRateIncreaseIncrement() {
		if( true == $this->hasChildRates() ) {
			if( false == isset( $this->m_arrstrRateRanges ) ) {
				$this->computeRateRanges();
			}
			return $this->m_arrstrRateRanges['rate_increase_increment'];
		} else {
			if( CArOrigin::SPECIAL == $this->getArOriginId() ) {
				return -1 * $this->getRateIncreaseIncrement();
			} else {
				return $this->getRateIncreaseIncrement();
			}
		}
	}

	public function getDisplayMaxAmount() {
		if( true == $this->hasChildRates() ) {
			if( false == isset( $this->m_arrstrRateRanges ) ) {
				$this->computeRateRanges();
			}
			return $this->m_arrstrRateRanges['max_amount'];
		} else {
			return $this->getMaxAmount();
		}
	}

	public function getDisplayMinAmount() {
		if( true == $this->hasChildRates() ) {
			if( false == isset( $this->m_arrstrRateRanges ) ) {
				$this->computeRateRanges();
			}
			return $this->m_arrstrRateRanges['min_amount'];
		} else {
			return $this->getMinAmount();
		}
	}

	public function getDisplayMonthToMonthMultiplier() {
		if( true == $this->hasChildRates() ) {
			if( false == isset( $this->m_arrstrRateRanges ) ) {
				$this->computeRateRanges();
			}
			return $this->m_arrstrRateRanges['month_to_month_multiplier'];
		} else {
			return $this->getMonthToMonthMultiplier();
		}
	}

	public function getDisplayMonthToMonthAmount() {
		if( true == $this->hasChildRates() ) {
			if( false == isset( $this->m_arrstrRateRanges ) ) {
				$this->computeRateRanges();
			}
			return $this->m_arrstrRateRanges['month_to_month_amount'];
		} else {
			return $this->getRateAmount() * $this->getMonthToMonthMultiplier();
		}
	}

	public function getDisplayArFormulaReferenceId() {
		if( true == $this->hasChildRates() ) {
			if( false == isset( $this->m_arrstrRateRanges ) ) {
				$this->computeRateRanges();
			}
			return $this->m_arrstrRateRanges['ar_formula_reference_id'];
		} else {
			return $this->getArFormulaReferenceId();
		}
	}

	public function getDisplayArTriggerId() {
		if( true == $this->hasChildRates() ) {
			if( false == isset( $this->m_arrstrRateRanges ) ) {
				$this->computeRateRanges();
			}
			return $this->m_arrstrRateRanges['ar_trigger_id'];
		} else {
			return $this->getArTriggerId();
		}
	}

	public function getDisplayArFormulaId() {
		if( true == $this->hasChildRates() ) {
			if( false == isset( $this->m_arrstrRateRanges ) ) {
				$this->computeRateRanges();
			}
			return $this->m_arrstrRateRanges['ar_formula_id'];
		} else {
			return $this->getArFormulaId();
		}
	}

	public function getDisplayShowInEntrata() {
		if( true == $this->hasChildRates() ) {
			if( false == isset( $this->m_arrstrRateRanges ) ) {
				$this->computeRateRanges();
			}
			return $this->m_arrstrRateRanges['show_in_entrata'];
		} else {
			return $this->getShowInEntrata();
		}
	}

	public function getDisplayShowOnWebsite() {
		if( true == $this->hasChildRates() ) {
			if( false == isset( $this->m_arrstrRateRanges ) ) {
				$this->computeRateRanges();
			}
			return $this->m_arrstrRateRanges['show_on_website'];
		} else {
			return $this->getShowOnWebsite();
		}
	}

	public function getSpecialName() {
		return $this->m_strSpecialName;
	}

	public function getSpecialQuantityRemaining() {
		return $this->m_intSpecialQuantityRemaining;
	}

	public function getSpecialMaxDaysToLeaseStart() {
		return $this->m_intSpecialMaxDaysToLeaseStart;
	}

	public function getLeaseExpiringCount() {
		return $this->m_intLeaseExpiringCount;
	}

	public function getArFormulaReferenceName() {
		return $this->m_strArFormulaReferenceName;
	}

	public function getHudBmirRentPercent( $fltRateAmount ) {
		return ( 0 < $fltRateAmount ) ? ( $fltRateAmount * 110 / 100 ) : 0;
	}

	public function getExternalReferenceNumber() {
		return $this->m_intExternalReferenceNumber;
	}

	public function getRateRange() {
		return $this->m_strRateRange;
	}

	public function getLeaseTermOccupancyTypeId() {
		return $this->m_intLeaseTermOccupancyTypeId;
	}

	public function getTaxPercent() {
		return ( float ) $this->m_fltTaxPercent;
	}

	public function getTaxAmount() {
		return ( float ) $this->m_fltTaxAmount;
	}

	public function getTaxStartDate() {
		return $this->m_strTaxStartDate;
	}

	// Setters

	public function setArCodeTypeId( $intArCodeTypeId ) {
		$this->m_intArCodeTypeId = ( int ) $intArCodeTypeId;
	}

	public function setArTriggerTypeId( $intArTriggerTypeId ) {
		$this->m_intArTriggerTypeId = ( int ) $intArTriggerTypeId;
	}

	public function setArCodeName( $strArCodeName ) {
		$this->m_strArCodeName = $strArCodeName;
	}

	public function setArOriginReferenceGroupId( $intArOriginReferenceGroupId ) {
		$this->m_intArOriginReferenceGroupId = ( int ) $intArOriginReferenceGroupId;
	}

	public function setLeaseTermName( $strLeaseTermName ) {
		$this->m_strLeaseTermName = $strLeaseTermName;
	}

	public function setLeaseStartWindow( $strLeaseStartWindow ) {
		$this->m_strLeaseStartWindow = $strLeaseStartWindow;
	}

	public function setSpecialName( $strSpecialName ) {
		$this->m_strSpecialName = $strSpecialName;
	}

	public function setArTriggerName( $strArTriggerName ) {
		$this->m_strArTriggerName = $strArTriggerName;
	}

	public function setSpecialQuantityRemaining( $intSpecialQuantityRemaining ) {
		$this->m_intSpecialQuantityRemaining = $intSpecialQuantityRemaining;
	}

	public function setSpecialMaxDaysToLeaseStart( $intSpecialMaxDaysToLeaseStart ) {
		$this->m_intSpecialMaxDaysToLeaseStart = $intSpecialMaxDaysToLeaseStart;
	}

	public function setLeaseExpiringCount( $intLeaseExpiringCount ) {
		$this->m_intLeaseExpiringCount = $intLeaseExpiringCount;
	}

	public function setArFormulaReferenceName( $strArFormulaReferenceName ) {
		$this->m_strArFormulaReferenceName = $strArFormulaReferenceName;
	}

	public function setExternalReferenceNumber( $intExternalReferenceNumber ) {
		$this->m_intExternalReferenceNumber = $intExternalReferenceNumber;
	}

	public function setRateRange( $strRateRange ) {
		$this->m_strRateRange = $strRateRange;
	}

	public function setLeaseTermOccupancyTypeId( $intLeaseTermOccupancyTypeId ) {
		$this->m_intLeaseTermOccupancyTypeId = ( int ) $intLeaseTermOccupancyTypeId;
	}

	public function setTaxPercent( $fltTaxPercent ) {
		$this->set( 'm_fltTaxPercent', CStrings::strToFloatDef( $fltTaxPercent, NULL, false, 6 ) );
	}

	public function setTaxAmount( $fltTaxAmount ) {
		$this->set( 'm_fltTaxAmount', CStrings::strToFloatDef( $fltTaxAmount, NULL, false, 6 ) );
	}

	public function setTaxStartDate( $strTaxStartDate ) {
		$this->m_strTaxStartDate = $strTaxStartDate;
	}

	// Validate Functions

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCascadeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCascadeReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArOriginId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArOriginReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArPhaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArTriggerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartWindowId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArFormulaId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArFormulaReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpaceConfigurationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerRelationshipId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermMonths() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWindowStartDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWindowEndDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWindowStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWindowEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRateAmount( $intDefaultArCodeId = NULL ) {
		$boolIsValid = true;

		if( CDefaultArCode::SUBSIDY_RENT == $intDefaultArCodeId && $this->getRateAmount() <= 0 ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate_amount', __( 'Rent amount must be greater than zero.' ) ) );
		}

		if( CDefaultArCode::UTILITY_ALLOWANCE == $intDefaultArCodeId && $this->getRateAmount() < 0 ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate_amount', __( 'Utility allowance must be greater than or equal to 0.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMonthToMonthMultiplier() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRateMultiplierAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRatePercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNormalizedAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNormalizedPercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRateIntervalStart() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRateIntervalOccurances() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRateIntervalOffset() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDatetime() {
		$boolIsValid = true;

		// Past and future effective dates are valid for affordable rates
		if( COccupancyType::AFFORDABLE == $this->getOccupancyTypeId() ) {

			if( false == valStr( $this->getEffectiveDatetime() ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( NULL, 'effective_datetime', __( 'Please choose an valid effective date.' ) ) );
			}

			return $boolIsValid;
		}

		$objToday = new DateTime();
		$objEffectiveDate = new DateTime( $this->m_strEffectiveDatetime );
		if( $objToday->format( 'Y-m-d' ) > $objEffectiveDate->format( 'Y-m-d' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'effective_datetime', __( 'Please choose an effective date on or after today.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDeactivationDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRenewal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOptional() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldRateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $intDefaultArCodeId = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid = $this->valEffectiveDatetime();
				break;

			case VALIDATE_DELETE:
				break;

			case 'affordable_rates':
 				$boolIsValid &= $this->valEffectiveDatetime();
				$boolIsValid &= $this->valRateAmount( $intDefaultArCodeId );
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	// Other functions

	public function hasChildRates() {
		return valArr( $this->m_arrobjChildRates );
	}

	public function addChildRate( CRate $objRate, $mixKey = NULL ) {
		if( false == valArr( $this->m_arrobjChildRates ) ) {
			$this->m_arrobjChildRates = [];
		}

		if( false == is_null( $mixKey ) ) {
			if( true == isset( $this->m_arrobjChildRates[$mixKey] ) ) {
				throw new Exception( __( 'Attempting to add duplicate child rate, key={%s,0}', [ $mixKey ] ) );
			}
			$this->m_arrobjChildRates[$mixKey] = $objRate;
		} else {
			$this->m_arrobjChildRates[] = $objRate;
		}
	}

	public function clearChildRates() {
		$this->m_arrobjChildRates = NULL;
	}

	public function computeRateRanges() {
		$this->m_arrstrRateRanges = [];
		$arrfltMins = [];
		$arrfltMaxs = [];
		$intArFormulaId = NULL;
		$intArFormulaReferenceId = NULL;
		$intArTriggerId = NULL;
		$boolShowInEntrata = false;
		$boolShowOnWebsite = false;

		// Compute the min and max for the values that can differ with child rates
		/** @var CRate $objChildRate */
		foreach( $this->getChildRates() as $objChildRate ) {
			if( false == isset( $arrfltMins['amt'] ) || $arrfltMins['amt'] > $objChildRate->getRateAmount() ) $arrfltMins['amt'] = $objChildRate->getRateAmount();
			if( false == isset( $arrfltMaxs['amt'] ) || $arrfltMaxs['amt'] < $objChildRate->getRateAmount() ) $arrfltMaxs['amt'] = $objChildRate->getRateAmount();
			if( false == isset( $arrfltMins['min'] ) || $arrfltMins['min'] > $objChildRate->getMinAmount() ) $arrfltMins['min'] = $objChildRate->getMinAmount();
			if( false == isset( $arrfltMaxs['min'] ) || $arrfltMaxs['min'] < $objChildRate->getMinAmount() ) $arrfltMaxs['min'] = $objChildRate->getMinAmount();
			if( false == isset( $arrfltMins['max'] ) || $arrfltMins['max'] > $objChildRate->getMaxAmount() ) $arrfltMins['max'] = $objChildRate->getMaxAmount();
			if( false == isset( $arrfltMaxs['max'] ) || $arrfltMaxs['max'] < $objChildRate->getMaxAmount() ) $arrfltMaxs['max'] = $objChildRate->getMaxAmount();
			if( false == isset( $arrfltMins['inc'] ) || $arrfltMins['inc'] > $objChildRate->getRateIncreaseIncrement() ) $arrfltMins['inc'] = $objChildRate->getRateIncreaseIncrement();
			if( false == isset( $arrfltMaxs['inc'] ) || $arrfltMaxs['inc'] < $objChildRate->getRateIncreaseIncrement() ) $arrfltMaxs['inc'] = $objChildRate->getRateIncreaseIncrement();
			if( false == isset( $arrfltMins['mtmmul'] ) || $arrfltMins['mtmmul'] > $objChildRate->getMonthToMonthMultiplier() ) $arrfltMins['mtmmul'] = $objChildRate->getMonthToMonthMultiplier();
			if( false == isset( $arrfltMaxs['mtmmul'] ) || $arrfltMaxs['mtmmul'] < $objChildRate->getMonthToMonthMultiplier() ) $arrfltMaxs['mtmmul'] = $objChildRate->getMonthToMonthMultiplier();
			if( false == isset( $arrfltMins['mtmamt'] ) || $arrfltMins['mtmamt'] > $objChildRate->getRateAmount() * $objChildRate->getMonthToMonthMultiplier() ) $arrfltMins['mtmamt'] = $objChildRate->getRateAmount() * $objChildRate->getMonthToMonthMultiplier();
			if( false == isset( $arrfltMaxs['mtmamt'] ) || $arrfltMaxs['mtmamt'] < $objChildRate->getRateAmount() * $objChildRate->getMonthToMonthMultiplier() ) $arrfltMaxs['mtmamt'] = $objChildRate->getRateAmount() * $objChildRate->getMonthToMonthMultiplier();
			if( true == is_null( $intArFormulaReferenceId ) ) $intArFormulaReferenceId = $objChildRate->getArFormulaReferenceId();
			elseif( $intArFormulaReferenceId != $objChildRate->getArFormulaReferenceId() ) $intArFormulaReferenceId = -1;
			if( true == is_null( $intArTriggerId ) ) $intArTriggerId = $objChildRate->getArTriggerId();
			elseif( $intArTriggerId != $objChildRate->getArTriggerId() ) $intArTriggerId = -1;
			if( true == is_null( $intArFormulaId ) ) $intArFormulaId = $objChildRate->getArFormulaId();
			elseif( $intArFormulaId != $objChildRate->getArFormulaId() ) $intArFormulaId = -1;
			$boolShowInEntrata = $boolShowInEntrata || $objChildRate->getShowInEntrata();
			$boolShowOnWebsite = $boolShowOnWebsite || $objChildRate->getShowOnWebsite();
		}

		// Adjust multiplier-type values for display purposes
		if( true == in_array( $this->getArFormulaId(), CArFormula::$c_arrintPercentageFormulas ) ) {
			$arrfltMins['inc'] *= 100;
			$arrfltMaxs['inc'] *= 100;
		}
		$arrfltMins['mtmmul'] = ( $arrfltMins['mtmmul'] - 1 ) * 100;
		$arrfltMaxs['mtmmul'] = ( $arrfltMaxs['mtmmul'] - 1 ) * 100;

		// Flip the sign on special amounts and increments, and flip the max/mins
		if( CArOrigin::SPECIAL == $this->getArOriginId() ) {
			$arrfltTmpRateAmount = - $arrfltMins['amt'];
			$arrfltMins['amt'] = - $arrfltMaxs['amt'];
			$arrfltMaxs['amt'] = $arrfltTmpRateAmount;

			$arrfltTmpRateInc = - $arrfltMins['inc'];
			$arrfltMins['inc'] = - $arrfltMaxs['inc'];
			$arrfltMaxs['inc'] = $arrfltTmpRateInc;
		}

		// Replace null min/max amounts with the string 'null'
		if( true == is_null( $arrfltMins['min'] ) ) $arrfltMins['min'] = 'null';
		if( true == is_null( $arrfltMaxs['min'] ) ) $arrfltMaxs['min'] = 'null';
		if( true == is_null( $arrfltMins['max'] ) ) $arrfltMins['max'] = 'null';
		if( true == is_null( $arrfltMaxs['max'] ) ) $arrfltMaxs['max'] = 'null';

		// Save either the range, or a single value if the max and min are equal
		$this->m_arrstrRateRanges['rate_amount']				= ( 0 == bccomp( $arrfltMaxs['amt'], $arrfltMins['amt'], 2 ) ? $arrfltMins['amt'] : $arrfltMins['amt'] . '-' . $arrfltMaxs['amt'] );
		$this->m_arrstrRateRanges['min_rate_amount']			= $arrfltMins['amt'];
		$this->m_arrstrRateRanges['max_rate_amount']			= $arrfltMaxs['amt'];
		$this->m_arrstrRateRanges['max_amount']					= ( 0 == bccomp( $arrfltMaxs['max'], $arrfltMins['max'], 2 ) ? $arrfltMins['max'] : $arrfltMins['max'] . '-' . $arrfltMaxs['max'] );
		$this->m_arrstrRateRanges['min_amount']					= ( 0 == bccomp( $arrfltMaxs['min'], $arrfltMins['min'], 2 ) ? $arrfltMins['min'] : $arrfltMins['min'] . '-' . $arrfltMaxs['min'] );
		$this->m_arrstrRateRanges['rate_increase_increment']	= ( 0 == bccomp( $arrfltMaxs['inc'], $arrfltMins['inc'], 2 ) ? $arrfltMins['inc'] : $arrfltMins['inc'] . '-' . $arrfltMaxs['inc'] );
		$this->m_arrstrRateRanges['month_to_month_multiplier']	= ( 0 == bccomp( $arrfltMaxs['mtmmul'], $arrfltMins['mtmmul'], 4 ) ? $arrfltMins['mtmmul'] : $arrfltMins['mtmmul'] . '-' . $arrfltMaxs['mtmmul'] );
		$this->m_arrstrRateRanges['month_to_month_amount']		= ( 0 == bccomp( $arrfltMaxs['mtmamt'], $arrfltMins['mtmamt'], 4 ) ? $arrfltMins['mtmamt'] : $arrfltMins['mtmamt'] . '-' . $arrfltMaxs['mtmamt'] );
		$this->m_arrstrRateRanges['ar_formula_id']	            = $intArFormulaId;
		$this->m_arrstrRateRanges['ar_formula_reference_id']	= $intArFormulaReferenceId;
		$this->m_arrstrRateRanges['ar_trigger_id']	            = $intArTriggerId;
		$this->m_arrstrRateRanges['show_in_entrata']			= $boolShowInEntrata;
		$this->m_arrstrRateRanges['show_on_website']			= $boolShowOnWebsite;
	}

	public function getFieldValues( $arrstrFieldNames ) {
		$arrmixFieldValues = [];

		foreach( $arrstrFieldNames as $strFieldName ) {
			$strGetter = 'get' . \Psi\Libraries\UtilInflector\CInflector::createService()->camelize( $strFieldName );
			if( true == method_exists( $this, $strGetter ) ) {
				$arrmixFieldValues[$strFieldName] = $this->$strGetter();
			}
		}

		return $arrmixFieldValues;
	}

	public function getDisplayAmount() {

		$strSign = $this->getDisplayRateAmount() < 0 ? '-' : '';

		switch( $this->getArFormulaId() ) {
			case CArFormula::FIXED_AMOUNT:
				return sprintf( '%s$%.2f', $strSign, abs( $this->getDisplayRateAmount() ) );

			case CArFormula::PER_ITEM:
				$strSign = $this->getDisplayRateIncreaseIncrement() < 0 ? '-' : '';
				return sprintf( '%s$%.2f per %s', $strSign, abs( $this->getDisplayRateIncreaseIncrement() ), $this->getArFormulaReferenceName() );

			case CArFormula::PER_ITEM_WITH_VOLUME_PRICING:
				return sprintf( '%s', $this->getArFormulaReferenceName() );

			case CArFormula::PER_ITEM_WITH_TIERED_PRICING:
				return sprintf( '%s', $this->getArFormulaReferenceName() );

			case CArFormula::PERCENT_OF_CHARGE_CODE:
				return sprintf( '%.2f%% of %s', $this->getDisplayRateIncreaseIncrement() * 100, $this->getArFormulaReferenceName() );

			case CArFormula::PERCENT_OF_CHARGE_CODE_GROUP:
				return sprintf( '%.2f%% of %s group', $this->getDisplayRateIncreaseIncrement() * 100, $this->getArFormulaReferenceName() );

			case CArFormula::PERCENT_OF_CHARGE_CODE_TYPE:
				return sprintf( '%.2f%% of %s type', $this->getDisplayRateIncreaseIncrement() * 100, $this->getArFormulaReferenceName() );

			case CArFormula::MARKET_TIMES_PERCENT:
				return sprintf( 'Market rent + %.2f%%', $this->getDisplayRateIncreaseIncrement() * 100 - 100 );

			case CArFormula::CURRENT_TIMES_PERCENT:
				return sprintf( 'Current rent + %.2f%%', $this->getDisplayRateIncreaseIncrement() * 100 - 100 );

			case CArFormula::MARKET_PLUS_FIXED_AMOUNT:
				return sprintf( 'Market rent + %s$%.2f', $strSign, abs( $this->getDisplayRateAmount() ) );

			case CArFormula::CURRENT_PLUS_FIXED_AMOUNT:
				return sprintf( 'Current rent + %s$%.2f', $strSign, abs( $this->getDisplayRateAmount() ) );

			default:
				return NULL;
		}
	}

	// Rate Caching functions

	public function fetchAssociatedUnitSpaces( $objDatabase ) {

		switch( $this->getArCascadeId() ) {
			case CArCascade::PROPERTY:
				return CUnitSpaces::createService()->fetchUnitSpacesByPropertyIdsByCid( [ $this->getPropertyId() ], $this->getCid(), $objDatabase );
				break;

			case CArCascade::FLOOR_PLAN:
				return CUnitSpaces::createService()->fetchPublishedUnitSpacesByPropertyIdByPropertyFloorPlanIdByCid( $this->getPropertyId(), $this->getPropertyFloorplanId(), $this->getCid(), $objDatabase, $boolIncludeDeletedUnits = false, $boolShowOnWebsite = false );
				break;

			case CArCascade::UNIT_TYPE:
				return CUnitSpaces::createService()->fetchUnitSpacesByUnitTypeIdByCid( $this->getUnitTypeId(), $this->getCid(), $objDatabase );
				break;

			case CArCascade::SPACE:
				return CUnitSpaces::createService()->fetchUnitSpacesByIdsByCid( [ $this->getUnitSpaceId() ], $this->getCid(), $objDatabase );
				break;

			default:
				// default case
				break;
		}

		return NULL;
	}

	public function recordRateLogs( $intCompanyUserId, $objDatabase ) {
		if( false == in_array( $this->getArTriggerId(), [ CArTrigger::MONTHLY, CArTrigger::MOVE_IN ] ) ) return true;

		$intCid 					= $objReference->getCid();
		$intPropertyId				= $objReference->getPropertyId();
		$intPropertyFloorplanId		= $objReference->getPropertyFloorplanId();
		$intUnitTypeId				= $objReference->getUnitTypeId();
		$arrintUnitSpaceIds			= [ $objReference->getUnitSpaceId() ];

		return \Psi\Eos\Entrata\CUnitSpaceRateLogs::createService()->recordRateLogs( $this->getCid(), [ $this->getPropertyId() ], [ $this->getPropertyFloorplanId() ], [ $this->getUnitTypeId() ], [ $this->getUnitSpaceId() ], $intCompanyUserId, $objDatabase );
	}

	/**
	 *  Insert & Update & Delete Functions
	 */

	// This is a temporary function created to block all delete operations that are triggered from different places within the system. Under new implementations, rate record will never be deleted.

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		trigger_error( __( 'Deletion of rate record is not permitted' ), E_USER_ERROR );
		exit;
	}

	public function createScheduledCharge() {

		$objScheduledCharge = new CScheduledCharge();
		$objScheduledCharge->setArCodeTypeId( $this->getArCodeTypeId() );
		$objScheduledCharge->setArCodeId( $this->getArCodeId() );
		$objScheduledCharge->setArTriggerId( $this->getArTriggerId() );
		$objScheduledCharge->setArFormulaId( $this->getArFormulaId() );
		$objScheduledCharge->setChargeAmount( $this->getRateAmount() );
		$objScheduledCharge->setRateId( $this->getId() );
		$objScheduledCharge->setCid( $this->getCid() );
		$objScheduledCharge->setPropertyId( $this->getPropertyId() );

		return $objScheduledCharge;

	}

}
?>