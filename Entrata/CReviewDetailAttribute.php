<?php

class CReviewDetailAttribute extends CBaseReviewDetailAttribute {

	const POSITIVE = '1';
	const NEGATIVE = '-1';
	const NEUTRAL  = '0';

	protected $m_strAttributeName;
	protected $m_intCompanyReviewAttributeId;

	/**
	 * Set Functions
	 *
	 **/

	public function setAttributeName( $strAttributeName ) {
		$this->m_strAttributeName = $strAttributeName;
	}

	public function setCompanyReviewAttributeId( $intCompanyReviewAttributeId ) {
		$this->m_intCompanyReviewAttributeId = CStrings::strToIntDef( $intCompanyReviewAttributeId, NULL, false );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_review_attribute_id'] ) ) {
			$this->setCompanyReviewAttributeId( $arrmixValues['company_review_attribute_id'] );
		}

		if( true == isset( $arrmixValues['attribute_name'] ) ) {
			$this->setAttributeName( $arrmixValues['attribute_name'] );
		}
	}

	/**
	 * Get Functions
	 *
	 **/

	public function getAttributeName() {
		return $this->m_strAttributeName;
	}

	public function getCompanyReviewAttributeId() {
		return $this->m_intCompanyReviewAttributeId;
	}

	/**
	 * Val Functions
	 *
	 **/

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyReviewAttributeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAttributeAssociatedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAttributeAssociatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>