<?php

class CAnnouncement extends CBaseAnnouncement {

	protected $m_arrintRequestedPropertyAssociations;
	protected $m_intAnnouncementPsProductId;

	/**
	 * Get Functions
	 */

	public function getRequestedPropertyAssociations() {
		return $this->m_arrintRequestedPropertyAssociations;
	}

	public function getAnnouncementPsProductId() {
		return $this->m_intAnnouncementPsProductId;
	}

	/**
	 * Set Functions
	 */

	public function setRequestedPropertyAssociations( $arrintRequestProperty = NULL ) {
		$this->m_arrintRequestedPropertyAssociations = [];

		if( true == valArr( $arrintRequestProperty ) ) {
			$this->m_arrintRequestedPropertyAssociations = array_values( $arrintRequestProperty );
		}

		return $this->m_arrintRequestedPropertyAssociations;
	}

	public function setAnnouncementPsProductId( $intAnnouncementPsProductId ) {
		$this->m_intAnnouncementPsProductId = $intAnnouncementPsProductId;
	}

	/**
	 * Validation Functions
	 */

	public function valRequestedAssociations() {
		// It is required
		$boolIsValid = true;
		if( false == valArr( $this->m_arrintRequestedPropertyAssociations ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'At least one property is required.' ) ) );
			$boolIsValid = false;
		}
		// It has to have at least one requested property
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Announcement title is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAnnouncementTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getAnnouncementTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'announcement_type_id', __( 'Announcement type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAnnouncement() {
		$boolIsValid = true;

		if( true == is_null( $this->getAnnouncement() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'announcement', __( 'Announcement description is required.' ) ) );
		}

		return $boolIsValid;
	}

 	public function valDateStart() {
			$boolIsValid = true;

			if( false == is_null( $this->getDateStart() ) ) {
				if( false == CValidation::validateDate( $this->getDateStart() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_start', __( 'Valid start date is required.' ) ) );
				}
			}

			return $boolIsValid;
 	}

		public function valDateEnd() {

			$boolIsValid = true;

			if( false == is_null( $this->getDateEnd() ) ) {
				if( strtotime( $this->getDateStart() ) > strtotime( $this->getDateEnd() ) ) {
					$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_end', __( 'End date should be greater than start date.' ) ) );
				} elseif( true == is_null( $this->getDateStart() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_start', __( 'Start date is required.' ) ) );
				} elseif( false == is_null( $this->getDateStart() ) ) {
					if( $this->getDateStart() == $this->getDateEnd() ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_start', __( 'Start date and End date should not be same.' ) ) );
					}
				}
			}

			return $boolIsValid;

		}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valAnnouncementTypeId();
				$boolIsValid &= $this->valAnnouncement();
				$boolIsValid &= $this->valDateStart();
				$boolIsValid &= $this->valDateEnd();
				if( CAnnouncementType::COMPANY == $this->getAnnouncementTypeId() ) {
					$boolIsValid &= $this->valRequestedAssociations();
				}
		  		break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valAnnouncementTypeId();
				$boolIsValid &= $this->valAnnouncement();
				$boolIsValid &= $this->valDateStart();
				$boolIsValid &= $this->valDateEnd();
				if( CAnnouncementType::COMPANY == $this->getAnnouncementTypeId() ) {
					$boolIsValid &= $this->valRequestedAssociations();
				}
				break;

			case VALIDATE_DELETE:
				// validate delete
		 		break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPropertyAnnouncements( $objDatabase ) {

		return CPropertyAnnouncements::fetchPropertyAnnouncementsByAnnouncementIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ps_product_id'] ) ) $this->setAnnouncementPsProductId( $arrmixValues['ps_product_id'] );

	}

}
?>