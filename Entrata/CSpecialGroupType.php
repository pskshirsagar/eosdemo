<?php

class CSpecialGroupType extends CBaseSpecialGroupType {

	const CONVENTIONAL_LEASING					= 1;
	const DATE_BASED_TIERS						= 2;
	const DATE_BASED_TIERS_WITH_CAPS			= 3;
	const AUTO_ADVANCING_DATE_BASED_TIERS		= 4;
	const CAP_BASED_TIERS						= 5;


	public static $c_arrintDateBasedTiersTypeIds	= [ CSpecialGroupType::DATE_BASED_TIERS, CSpecialGroupType::DATE_BASED_TIERS_WITH_CAPS, CSpecialGroupType::AUTO_ADVANCING_DATE_BASED_TIERS ];
	public static $c_arrintCapBasedTiersTypeIds		= [ CSpecialGroupType::CONVENTIONAL_LEASING, CSpecialGroupType::DATE_BASED_TIERS_WITH_CAPS, CSpecialGroupType::AUTO_ADVANCING_DATE_BASED_TIERS, CSpecialGroupType::CAP_BASED_TIERS ];

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>