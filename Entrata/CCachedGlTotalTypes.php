<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedGlTotalTypes
 * Do not add any new functions to this class.
 */

class CCachedGlTotalTypes extends CBaseCachedGlTotalTypes {

	public static function fetchCachedGlTotalTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCachedGlTotalType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCachedGlTotalType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCachedGlTotalType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>