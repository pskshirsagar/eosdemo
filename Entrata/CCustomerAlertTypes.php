<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAlertTypes
 * Do not add any new functions to this class.
 */

class CCustomerAlertTypes extends CBaseCustomerAlertTypes {

	public static function fetchCustomerAlertTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CCustomerAlertType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCustomerAlertType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CCustomerAlertType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>