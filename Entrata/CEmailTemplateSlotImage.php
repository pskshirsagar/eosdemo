<?php

class CEmailTemplateSlotImage extends CBaseEmailTemplateSlotImage {

    public function valId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valScheduledEmailId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getScheduledEmailId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_email_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valEmailTemplateId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getEmailTemplateId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_template_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valEmailTemplateSlotId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getEmailTemplateSlotId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_template_slot_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCompanyMediaFileId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getCompanyMediaFileId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_media_file_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valKey() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getKey() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', '' ) );
        // }

        return $boolIsValid;
    }

    public function valTitle() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getTitle() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', '' ) );
        // }

        return $boolIsValid;
    }

    public function valLink() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getLink() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'link', '' ) );
        // }

        return $boolIsValid;
    }

    public function valLinkText() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getLinkText() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'link_text', '' ) );
        // }

        return $boolIsValid;
    }

    public function valAlt() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getAlt() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'alt', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getIsPublished() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>