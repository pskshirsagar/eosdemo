<?php

class CSocialPostMediaFile extends CBaseSocialPostMediaFile {

	protected $m_strFullsizeUri;

	/**
	 * Get Functions
	 */

	public function getFullsizeUri() {
		return $this->m_strFullsizeUri;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['fullsize_uri'] ) ) 	$this->setFullsizeUri( $arrmixValues['fullsize_uri'] );

		return;
	}

	public function setFullsizeUri( $strFullsizeUri ) {
		$this->set( 'm_strFullsizeUri', CStrings::strTrimDef( $strFullsizeUri, 4096, NULL, true ) );
	}

	/**
	 * Validation Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>