<?php

class CCachedRateRange extends CBaseCachedRateRange {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyFloorplanId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArTriggerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartWindowId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpaceConfigurationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermMonths() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveRange() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseTax() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityTax() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialTax() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddOnAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddOnTax() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddOnRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseDepositAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseDepositTax() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityDepositAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityDepositTax() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialDepositAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialDepositTax() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddOnDepositAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddOnDepositTax() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddOnDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataBaseAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataBaseTaxAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAmenityAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAmenityTaxAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAmenityRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataSpecialAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataSpecialTaxAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataSpecialRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAddOnAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAddOnTaxAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAddOnRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMarketRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataBaseDepositAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataBaseDepositTaxAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataBaseDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAmenityDepositAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAmenityDepositTaxAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAmenityDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataSpecialDepositAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataSpecialDepositTaxAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataSpecialDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAddOnDepositAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAddOnDepositTaxAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAddOnDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataTotalDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseAmountMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseTaxMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseRentMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityAmountMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityTaxMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityRentMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialAmountMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialTaxMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialRentMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddOnAmountMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddOnTaxMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddOnRentMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketRentMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketAmountMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataMarketAmountMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCachedDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShowOnWebsite() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIncludeTaxInAdvertisedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIncludeOtherInAdvertisedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseOther() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityOther() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialOther() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddOnOther() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataBaseOther() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAmenityOther() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataSpecialOther() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataAddOnOther() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseOtherMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityOtherMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialOtherMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddOnOtherMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>