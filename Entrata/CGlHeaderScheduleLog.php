<?php

class CGlHeaderScheduleLog extends CBaseGlHeaderScheduleLog {

	const ACTION_CREATED	= 'Created';
	const ACTION_EDITED		= 'Edited';
	const ACTION_APPROVED	= 'Approved';
	const ACTION_DISABLED	= 'Disabled';
    const ACTION_ENABLED	= 'Enabled';

	protected $m_strCompanyEmployeeFullName;

	protected $m_arrobjProperties;

	/**
	 * Get Functions
	 */

	public function getCompanyEmployeeFullName() {
		return $this->m_strCompanyEmployeeFullName;
	}

	public function getProperties() {
		return $this->m_arrobjProperties;
	}

	/**
	 * Set Functions
	 */

	public function setCompanyEmployeeFullName( $strCompanyEmployeeFullName ) {
		$this->m_strCompanyEmployeeFullName = CStrings::strTrimDef( $strCompanyEmployeeFullName, 100, NULL, true );
	}

	public function setProperties( $arrobjProperties ) {
		$this->m_arrobjProperties = $arrobjProperties;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['company_employee_full_name'] ) ) $this->setCompanyEmployeeFullName( $arrValues['company_employee_full_name'] );

		return;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlHeaderScheduleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFrequencyInterval() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDayOfWeek() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDaysOfMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNumberOfOccurrences() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastPostedDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNextPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsReverseNextMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPaused() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisabledBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisabledOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				return $boolIsValid;
		}

		return $boolIsValid;
	}

}
?>