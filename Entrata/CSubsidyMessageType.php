<?php

class CSubsidyMessageType extends CBaseSubsidyMessageType {

	const TRACS_MAT_SUBMISSION = 1;
	const TRACS_MAT_RESULT = 2;
	const IMAX_MAT_SUBMISSION = 3;
	const IMAX_MAT_RESULT = 4;
	const IMAX_BROADCAST = 5;

	public static $c_arrintTransmissionMessageTypes	= [ CSubsidyMessageType::TRACS_MAT_SUBMISSION, CSubsidyMessageType::IMAX_MAT_SUBMISSION ];
	public static $c_arrintResponseMessageTypes		= [ CSubsidyMessageType::TRACS_MAT_RESULT, CSubsidyMessageType::IMAX_MAT_RESULT ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>