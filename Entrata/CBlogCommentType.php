<?php

class CBlogCommentType extends CBaseBlogCommentType {

	const POST_PUBLISHED_COMMENT_TYPE_ID        = 5;
	const COMMENT_TYPE_ID_FOR_NOTE              = 6;
	const POST_UNPUBLISHED_COMMENT_TYPE_ID      = 7;
	const POST_DRAFTED_COMMENT_TYPE_ID          = 8;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>