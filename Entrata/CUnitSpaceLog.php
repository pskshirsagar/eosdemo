<?php

class CUnitSpaceLog extends CBaseUnitSpaceLog {
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_intOldUnitSpaceStatusTypeId;
	protected $m_strUserName;
	protected $m_strFloorPlan;
	protected $m_strUnitType;
	protected $m_strUnitSpaceStatusType;

	const IS_IGNORED_FALSE = '0';

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyUnitId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitSpaceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitSpaceLogTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReferenceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReferenceLogId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportingPeriodId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEffectivePeriodId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOriginalPeriodId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitExclusionReasonTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOldUnitSpaceStatusTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNewUnitSpaceStatusTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportingPostMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplyThroughPostMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplyThroughEffectiveDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valExpectedRent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReserveUntil() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMakeReadyDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAvailableOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLogDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEffectiveDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsOpeningLog() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPostMonthIgnored() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsEffectiveDateIgnored() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function getNameFirst() {
       	return  $this->m_strNameFirst;
    }

    public function setNameFirst( $strNameFirst ) {
    	$this->m_strNameFirst = $strNameFirst;
    }

    public function getNameLast() {
    	return  $this->m_strNameLast;
    }

	public function setNameLast( $strNameLast ) {
    	$this->m_strNameLast = $strNameLast;
    }

	public function getUserName() {
		return  $this->m_strUserName;
	}

	public function getBudgetedRents() {
		return $this->m_arrobjBudgetedRents;
	}

	public function getFloorPlan() {
		return $this->m_strFloorPlan;
	}

	public function getUnitType() {
		return $this->m_strUnitType;
	}

	public function getUnitSpaceStatusType() {
		return $this->m_strUnitSpaceStatusType;
	}

	public function setFloorPlan( $strFloorPlan ) {
		$this->m_strFloorPlan = $strFloorPlan;
	}

	public function setUnitType( $strUnitType ) {
		$this->m_strUnitType = $strUnitType;
	}

	public function setUnitSpaceStatusType( $strUnitSpaceStatusType ) {
		$this->m_strUnitSpaceStatusType = $strUnitSpaceStatusType;
	}

	public function setBudgetedRents( $arrobjBudgetedRents ) {
		$this->m_arrobjBudgetedRents = $arrobjBudgetedRents;
	}

	public function setUserName( $strUserName ) {
		$this->m_strUserName = $strUserName;
	}

	public function getOldUnitSpaceStatusTypeId() {
    	return $this->m_intOldUnitSpaceStatusTypeId;
    }

    public function setOldUnitSpaceStatusTypeId( $intOldUnitSpaceStatusTypeId ) {
    	$this->m_intOldUnitSpaceStatusTypeId = $intOldUnitSpaceStatusTypeId;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );
    	if( $boolDirectSet && isset( $arrmixValues['name_first'] ) ) {
    		$this->m_strNameFirst = trim( $arrmixValues['name_first'] );
    	} elseif( isset( $arrmixValues['name_first'] ) ) {
    		$this->setNameFirst( $arrmixValues['name_first'] );
    	}

    	if( $boolDirectSet && isset( $arrmixValues['name_last'] ) ) {
    		$this->m_strNameLast = trim( $arrmixValues['name_last'] );
    	} elseif( isset( $arrmixValues['name_last'] ) ) {
    		$this->setNameLast( $arrmixValues['name_last'] );
    	}

    	if( isset( $arrmixValues['old_unit_space_status_type_id'] ) ) {
		    $this->setOldUnitSpaceStatusTypeId( $arrmixValues['old_unit_space_status_type_id'] );
	    }

	    if( isset( $arrmixValues['floorplan_name'] ) ) {
		    $this->setFloorPlan( $arrmixValues['floorplan_name'] );
	    }

	    if( isset( $arrmixValues['unit_type'] ) ) {
		    $this->setUnitType( $arrmixValues['unit_type'] );
	    }

	    if( isset( $arrmixValues['unit_space_status_type'] ) ) {
		    $this->setUnitSpaceStatusType( $arrmixValues['unit_space_status_type'] );
	    }

	    if( isset( $arrmixValues['username'] ) ) {
		    $this->setUserName( $arrmixValues['username'] );
	    }

    }

}
?>