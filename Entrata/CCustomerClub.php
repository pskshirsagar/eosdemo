<?php

class CCustomerClub extends CBaseCustomerClub {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerId() {
	    $boolIsValid = true;

	    if( false == valId( $this->getCustomerId() ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', __( 'Resident Name is required.' ) ) );
	    }

	    return $boolIsValid;
    }

    public function valClubId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valJoinDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
	            $boolIsValid &= $this->valCustomerId();
		        break;

	        case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>