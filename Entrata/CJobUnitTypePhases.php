<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobUnitTypePhases
 * Do not add any new functions to this class.
 */

class CJobUnitTypePhases extends CBaseJobUnitTypePhases {

	public static function fetchJobUnitTypePhasesByJobIdByUnitTypeIdsByCid( $intJobId, $arrintUnitTypeId, $intCid, $objDatabase ) {

		if( false == valId( $intJobId ) || false == valIntArr( $arrintUnitTypeId ) ) return NULL;

		$strSql = 'SELECT
						jutp.*,
						jp.id AS job_phase_id,
						jp.name AS job_phase_name
					FROM
						job_unit_type_phases jutp
						JOIN job_phases jp ON( jp.cid = jutp.cid AND jp.id=jutp.job_phase_id AND jutp.unit_type_id IN( ' . implode( ',', $arrintUnitTypeId ) . ' ) )
					WHERE
						jp.job_id = ' . ( int ) $intJobId . '
						AND jp.cid = ' . ( int ) $intCid . '
					ORDER BY
						jp.order_num ASC';

			return self::fetchJobUnitTypePhases( $strSql, $objDatabase );
	}

}
?>