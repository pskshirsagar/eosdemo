<?php

class CMediaFileType extends CBaseMediaFileType {

	const FLASH		= 1;
	const TYPE_360	= 2;
	const JPG		= 3;
	const PNG		= 4;
	const GIF		= 5;
	const DOC		= 6;
	const PDF		= 7;
	const XLS		= 8;
	const WAV		= 9;
	const AVI		= 10;
	const MOV		= 11;
	const FLV		= 12;
	const MPEG		= 13;
	const MP4		= 14;
	const TYPE_3GP	= 15;
	const PPT		= 16;
	const WMV		= 17;
	const MP3		= 18;
	const WMA		= 19;
	const ICO		= 20;
	const XLSX		= 21;
	const DOCX		= 22;
	const CSS		= 23;
	const VCF		= 24;

}
?>