<?php

class CUnitSpaceRateLog extends CBaseUnitSpaceRateLog {
	protected $m_boolIsPeriodClosed;
	const IS_IGNORED_FALSE = '0';

    public function getIsPeriodClosed() {
    	return $this->m_boolIsPeriodClosed;
    }

    public function setIsPeriodClosed( $boolIsPeriodClosed ) {
    	$this->m_boolIsPeriodClosed = $boolIsPeriodClosed;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitSpaceId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUnitSpaceId() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_space_id', __( 'Unit is required.' ) ) );
        	$boolIsValid &= false;
        }

        return $boolIsValid;
    }

    public function valEffectiveDate() {
        $boolIsValid = true;

        if( true == is_null( $this->getEffectiveDate() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'advertised_date', __( 'Advertised date is required.' ) ) );
        	$boolIsValid &= false;
        }

        return $boolIsValid;
    }

    public function valAmenityRent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSpecialRent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	// For some of the migrated companies $0 rent is allowd

    public function valMarketRent() {
        $boolIsValid = true;

        // if( 0 >= $this->getMarketRent() ) {
        //	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Market_rent', 'Rent amount is required.' ) );
        //	$boolIsValid &= false;
        // }

        return $boolIsValid;
    }

    public function valBaseDeposit() {
        $boolIsValid = true;

        if( 0 > $this->getBaseDeposit() ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'base_deposit', __( 'Deposit amount is required.' ) ) );
        	$boolIsValid &= false;
        }

        return $boolIsValid;
    }

    public function isRateModified( $objOldUnitSpaceRateLog = NULL ) {
    	if( true == valObj( $objOldUnitSpaceRateLog, 'CUnitSpaceRateLog' ) && strtotime( $objOldUnitSpaceRateLog->getEffectiveDate() ) == strtotime( $this->getEffectiveDate() ) &&
    		( float ) $objOldUnitSpaceRateLog->getMarketRent() != ( float ) $this->getMarketRent() ) {

    		return true;
    	}

    	return false;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valUnitSpaceId();
            	$boolIsValid &= $this->valEffectiveDate();
            	$boolIsValid &= $this->valMarketRent();
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valEffectiveDate();
            	$boolIsValid &= $this->valMarketRent();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// no default action
            	break;
        }

        return $boolIsValid;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
    	if( true == isset( $arrmixValues['is_period_closed'] ) ) $this->setIsPeriodClosed( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_period_closed'] ) : $arrmixValues['is_period_closed'] );
    }

}
?>