<?php

class CApPayeeProperty extends CBaseApPayeeProperty {

	protected $m_strPropertyName;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApPayeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApPayeeLocationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>