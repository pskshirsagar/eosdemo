<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeStatusTypes
 * Do not add any new functions to this class.
 */

class CApPayeeStatusTypes extends CBaseApPayeeStatusTypes {

	public static function fetchApPayeeStatusTypes( $strSql, $objClientDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CApPayeeStatusType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchApPayeeStatusType( $strSql, $objClientDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApPayeeStatusType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllApPayeeStatusTypes( $objClientDatabase ) {
		return self::fetchApPayeeStatusTypes( 'SELECT * FROM ap_payee_status_types', $objClientDatabase );
	}

	public static function fetchApPayeeStatusTypeByIds( $arrintIds, $objClientDatabase ) {
		if ( false == valArr( $arrintIds ) ) return NULL;
		return self::fetchApPayeeStatusTypes( 'SELECT * FROM ap_payee_status_types WHERE id IN ( ' . implode( ',', $arrintIds ) . ' )', $objClientDatabase );
	}

	public static function fetchPublishedApPayeeStatusTypes( $objClientDatabase ) {
		return self::fetchApPayeeStatusTypes( 'SELECT * FROM ap_payee_status_types WHERE is_published = 1 ORDER BY id', $objClientDatabase );
	}

}
?>