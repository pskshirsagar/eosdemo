<?php

class COfferItem extends CBaseOfferItem {

	protected $m_intSpecialTypeId;
	protected $m_intGiftValue;
	protected $m_boolIsAutofulfilled;
	protected $m_boolHideDescription;
	protected $m_intPropertyId;
	protected $m_intArCascadeId;
	protected $m_intLeaseIntervalTypeId;
	protected $m_intMinDaysToLeaseStart;
	protected $m_intApplicationStageStatusId;

	protected $m_arrintUnitTypeIds;

	public function getLeaseIntervalTypeId() {
		return $this->m_intLeaseIntervalTypeId;
	}

	public function setLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
		$this->m_intLeaseIntervalTypeId = $intLeaseIntervalTypeId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function getSpecialTypeId() {
		return $this->m_intSpecialTypeId;
	}

	public function setSpecialTypeId( $intSpecialTypeId ) {
		$this->m_intSpecialTypeId = $intSpecialTypeId;
	}

	public function getGiftValue() {
		return $this->m_intGiftValue;
	}

	public function setGiftValue( $intGiftValue ) {
		$this->m_intGiftValue = $intGiftValue;
	}

	public function getHideDescription() {
		return $this->m_boolHideDescription;
	}

	public function getIsAutofulfilled() {
		return $this->m_boolIsAutofulfilled;
	}

	public function getArCascadeId() {
		return $this->m_intArCascadeId;
	}

	public function getUnitTypeIds() {
		return $this->m_arrintUnitTypeIds;
	}

	public function getTierSpecialId() {
		return $this->m_intTierSpecialId;
	}

	public function getSpecialId() {
		return $this->m_intSpecialId;
	}

	public function setIsAutofulfilled( $boolIsAutofulfilled ) {
		$this->m_boolIsAutofulfilled = $boolIsAutofulfilled;
	}

	public function setHideDescription( $boolHideDescription ) {
		$this->m_boolHideDescription = $boolHideDescription;
	}

	public function setArCascadeId( $intArCascadeId ) {
		$this->m_intArCascadeId = $intArCascadeId;
	}

	public function setUnitTypeIds( $arrintUnitTypeIds ) {
		$this->m_arrintUnitTypeIds = $arrintUnitTypeIds;
	}

	public function setTierSpecialId( $intTierSpecialId ) {
		$this->m_intTierSpecialId = $intTierSpecialId;
	}

	public function setSpecialId( $intSpecialId ) {
		$this->m_intSpecialId = $intSpecialId;
	}

	public function setApplicationStageStatusId( $intApplicationStageStatusId ) {
		$this->m_intApplicationStageStatusId = CStrings::strToIntDef( $intApplicationStageStatusId, NULL, false );
	}

	public function setMinDaysToLeaseStart( $intMinDaysToLeaseStart ) {
		$this->m_intMinDaysToLeaseStart = CStrings::strToIntDef( $intMinDaysToLeaseStart, NULL, false );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['tier_special_id'] ) ) $this->setTierSpecialId( $arrmixValues['tier_special_id'] );
		if( true == isset( $arrmixValues['special_id'] ) ) $this->setSpecialId( $arrmixValues['special_id'] );
		if( true == isset( $arrmixValues['special_type_id'] ) ) $this->setSpecialTypeId( $arrmixValues['special_type_id'] );
		if( true == isset( $arrmixValues['gift_value'] ) ) $this->setGiftValue( $arrmixValues['gift_value'] );
		if( true == isset( $arrmixValues['is_auto_fulfilled'] ) ) $this->setIsAutofulfilled( $arrmixValues['is_auto_fulfilled'] );
		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['lease_interval_type_id'] ) ) $this->setLeaseIntervalTypeId( $arrmixValues['lease_interval_type_id'] );
		if( true == isset( $arrmixValues['hide_description'] ) ) $this->setHideDescription( CStrings::strToBool( $arrmixValues['hide_description'] ) );
		if( true == isset( $arrmixValues['ar_cascade_id'] ) ) $this->setArCascadeId( $arrmixValues['ar_cascade_id'] );
		if( true == isset( $arrmixValues['unit_type_ids'] ) ) $this->setUnitTypeIds( $arrmixValues['unit_type_ids'] );
		if( true == isset( $arrmixValues['application_stage_status_id'] ) ) $this->setApplicationStageStatusId( $arrmixValues['application_stage_status_id'] );
		if( true == isset( $arrmixValues['min_days_to_lease_start'] ) ) $this->setMinDaysToLeaseStart( $arrmixValues['min_days_to_lease_start'] );
	}

	public function __construct( $arrmixOfferTemplate = NULL ) {
		parent::__construct();
		$this->setValues( $arrmixOfferTemplate );
		$this->setArOriginId( CArOrigin::SPECIAL );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOfferId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArLookupTableIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxItemCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArOriginId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArOriginReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportOfferItemId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function getApplicationStageStatusId() {
		return $this->m_intApplicationStageStatusId;
	}

	public function getMinDaysToLeaseStart() {
		return $this->m_intMinDaysToLeaseStart;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsHardDelete = false, $boolReturnSqlOnly = false ) {
		if( true == $boolIsHardDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {

			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );

			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

}
?>