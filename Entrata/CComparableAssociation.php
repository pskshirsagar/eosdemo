<?php

class CComparableAssociation extends CBaseComparableAssociation {

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( isset( $arrmixValues['amenity_name'] ) ) $this->setAmenityName( $arrmixValues['amenity_name'] );
		if( isset( $arrmixValues['default_amenity_name'] ) ) $this->setDefaultAmenityName( $arrmixValues['default_amenity_name'] );
		if( isset( $arrmixValues['utility_name'] ) ) $this->setUtilityName( $arrmixValues['utility_name'] );
		return;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setAmenityName( $strAmenityName ) {
		$this->m_strAmenityName = CStrings::strTrimDef( $strAmenityName, NULL, NULL, true );
	}

	public function setDefaultAmenityName( $strDefaultAmenityName ) {
		$this->m_strDefaultAmenityName = CStrings::strTrimDef( $strDefaultAmenityName, NULL, NULL, true );
	}

	public function setUtilityName( $strUtilityName ) {
		$this->m_strUtilityName = CStrings::strTrimDef( $strUtilityName, NULL, NULL, true );
	}

	/**
	 * Get Functions
	 *
	 */

	public function getAmenityName() {
		return $this->m_strAmenityName;
	}

	public function getDefaultAmenityName() {
		return $this->m_strDefaultAmenityName;
	}

	public function getUtilityName() {
		return $this->m_strUtilityName;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;
		if( false == valId( $this->getId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Comparable Assoication id is required.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valComparableOptionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valComparableRoomTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultAmenityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valComparableReferenceId( $boolIsUtility = false ) {
		$boolIsValid = true;
		$strErrorMsg = __( 'Property Amenity is required.' );

		if( true == $boolIsUtility ) {
			$strErrorMsg = __( 'Utility is required.' );
		}

		if( true == is_null( $this->getComparableReferenceId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'comparable_reference_id', $strErrorMsg ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valUtilityTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		if( true == is_null( $this->getNotes() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', __( 'Custom LC Description is required.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valIsFeatured() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valComparableReferenceId();
				$boolIsValid &= $this->valNotes();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			case 'insert_utility':
			case 'update_utility':
				$boolIsValid &= $this->valComparableReferenceId( $boolIsUtility = true );
				$boolIsValid &= $this->valNotes();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>