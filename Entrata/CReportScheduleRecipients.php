<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportScheduleRecipients
 * Do not add any new functions to this class.
 */

class CReportScheduleRecipients extends CBaseReportScheduleRecipients {

	public static function fetchRecipientsByScheduledTaskIdByCid( $intScheduledTaskId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						rsrt . name AS recipient_type,
						rsr.reference_id as recipient_id,
						CASE WHEN rsrt.id = ' . CReportScheduleRecipientType::COMPANY_USER . ' THEN concat(ce.name_first, \' \', ce.name_last)
							 WHEN rsrt.id = ' . CReportScheduleRecipientType::COMPANY_GROUP . ' THEN util_get_translated( \'name\', cg.name, cg.details )
							 WHEN rsrt.id = ' . CReportScheduleRecipientType::AP_PAYEE_CONTACT . ' THEN apc.email_address
							 WHEN rsrt.id = ' . CReportScheduleRecipientType::EMAIL_ADDRESS . ' THEN rsr.details->>\'email\'
						END AS name,
						cu.is_disabled,
						rsr.details ->> \'output_locale\' as locale
					FROM
						report_schedule_recipients rsr
						JOIN report_schedules rs ON ( rs.cid = rsr.cid AND rs.id = rsr.report_schedule_id )
						JOIN report_schedule_recipient_types rsrt ON ( rsrt.id = rsr.report_schedule_recipient_type_id )
						LEFT JOIN company_users cu ON ( cu.cid = rsr.cid AND cu.id = rsr.reference_id AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
						LEFT JOIN company_groups cg ON ( cg.cid = rsr.cid AND cg.id = rsr.reference_id )
						LEFT JOIN ap_payee_contacts apc ON ( apc.cid = rsr.cid AND apc.id = rsr.reference_id )
					WHERE
						rsr.cid = ' . ( int ) $intCid . '
						AND rs.scheduled_task_id = ' . ( int ) $intScheduledTaskId . '
						AND rs.report_schedule_type_id <> ' . CReportScheduleType::MANUAL;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRecipientsLocalesByReportScheduleIdByCid( $intReportScheduledId, $intCid, $objDatabase ) {
		$strSql = '
					SELECT
						DISTINCT jsonb_array_elements_text( ( details ->> \'output_locale\' )::jsonb ) AS output_locale
					FROM
						report_schedule_recipients
					WHERE
						cid = ' . ( int ) $intCid . '
						AND report_schedule_id = ' . ( int ) $intReportScheduledId;

		return fetchData( $strSql, $objDatabase );
	}

}
?>