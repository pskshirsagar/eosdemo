<?php

class CRevenueOverrideRentsLog extends CBaseRevenueOverrideRentsLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvailableOnDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveInDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOverrideRentAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOverrideRentReason() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalOptimalRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExpiryDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartWindowId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>