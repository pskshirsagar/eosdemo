<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyApplicationPreferences
 * Do not add any new functions to this class.
 */

class CPropertyApplicationPreferences extends CBasePropertyApplicationPreferences {

	public static function fetchPropertyApplicationPreferencesByCompanyApplicationIdByPropertyIdByApplicationPreferenceTypeIdByCid( $intCompanyApplicationId, $intPropertyId, $intApplicationPreferenceTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM property_application_preferences WHERE company_application_id = ' . ( int ) $intCompanyApplicationId . ' AND application_preference_type_id = ' . ( int ) $intApplicationPreferenceTypeId . ' AND property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchPropertyApplicationPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyApplicationPreferenceByKeyByCompanyApplicationIdByPropertyIdByApplicationPreferenceTypeIdByCid( $strKey, $intCompanyApplicationId, $intPropertyId, $intApplicationPreferenceTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM property_application_preferences WHERE key = \'' . $strKey . '\' AND company_application_id = ' . ( int ) $intCompanyApplicationId . ' AND property_id = ' . ( int ) $intPropertyId . ' AND application_preference_type_id = ' . ( int ) $intApplicationPreferenceTypeId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchPropertyApplicationPreference( $strSql, $objDatabase );
	}

	public static function fetchPropertyApplicationPreferencesByPropertyIdByCompanyApplicationIdByKeysByApplicationPreferenceTypeIdByCid( $intPropertyId, $intCompanyApplicationId, $arrstrKeys, $intApplicationPreferenceTypeId, $intCid, $objDatabase ) {
		if ( false == valArr( $arrstrKeys ) ) return NULL;
		$strSql = 'SELECT * FROM property_application_preferences WHERE key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )  AND property_id = ' . ( int ) $intPropertyId . ' AND company_application_id = ' . ( int ) $intCompanyApplicationId . ' AND application_preference_type_id = ' . ( int ) $intApplicationPreferenceTypeId . ' AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyApplicationPreferences( $strSql, $objDatabase );
	}

	public static function fetchHideAndRequirePreferenceValuesByPropertyIdByCompanyApplicationIdsByCid( $intPropertyId, $arrintCompanyApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyApplicationIds ) ) return NULL;

		$strSql = '	SELECT
						ca.id,
						max(CASE
							WHEN pap.id IS NOT NULL AND pap.key = \'HIDE_BASIC_INFO_SECONDARY_NUMBER\' THEN pap.value
							ELSE \'0\'
						END) as hide_property_application_preferences_secondary_number,
						max(CASE
							WHEN pap.id IS NOT NULL AND pap.key = \'REQUIRE_BASIC_INFO_SECONDARY_NUMBER\' THEN pap.value
							ELSE \'0\'
						END) as require_property_application_preferences_secondary_number,
						max(CASE
							WHEN pap.id IS NOT NULL AND pap.key = \'HIDE_LEASE_PREFERENCES_LEAD_SOURCE_ID\' THEN pap.value
							ELSE \'0\'
						END) as hide_lease_preferences_lead_source_id,
						max(CASE
							WHEN pap.id IS NOT NULL AND pap.key = \'REQUIRE_LEASE_PREFERENCES_LEAD_SOURCE_ID\' THEN pap.value
							ELSE \'0\'
						END) as require_lease_preferences_lead_source_id,
						max(CASE
							WHEN pap.id IS NOT NULL AND pap.key = \'SHOW_LEASE_PREFERENCES_PREFERRED_LANGUAGE\' THEN pap.value
							ELSE \'0\'
						END) as hide_lease_preferences_preferred_language,
						max(CASE
							WHEN pap.id IS NOT NULL AND pap.key = \'REQUIRE_LEASE_PREFERENCES_PREFERRED_LANGUAGE\' THEN pap.value
							ELSE \'0\'
						END) as require_lease_preferences_preferred_language

					FROM company_applications ca
						LEFT JOIN property_application_preferences pap ON ( pap.company_application_id = ca.id AND pap.cid = ca.cid AND pap.property_id = ' . ( int ) $intPropertyId . ' AND pap.key IN ( \'HIDE_LEASE_PREFERENCES_LEAD_SOURCE_ID\', \'REQUIRE_LEASE_PREFERENCES_LEAD_SOURCE_ID\', \'HIDE_BASIC_INFO_SECONDARY_NUMBER\', \'REQUIRE_BASIC_INFO_SECONDARY_NUMBER\', \'SHOW_LEASE_PREFERENCES_PREFERRED_LANGUAGE\', \'REQUIRE_LEASE_PREFERENCES_PREFERRED_LANGUAGE\' ) )
					WHERE ca.id IN ( ' . implode( ',', $arrintCompanyApplicationIds ) . ' ) AND ca.cid = ' . ( int ) $intCid . ' GROUP BY ca.id';

		$arrmixResponses = fetchData( $strSql, $objDatabase );

		return $arrmixResponses;
	}

	public static function fetchPropertyApplicationPreferencesByKeysByValueByCid( $arrstrKeys, $strValue, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT * FROM property_application_preferences WHERE cid = ' . ( int ) $intCid . ' AND key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' ) AND value = \'' . $strValue . '\'';

		return self::fetchPropertyApplicationPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyApplicationPreferencesByPropertyApplicationIdByKeysByPropertyIdByCid( $intCompanyApplicationId, $arrstrKeys, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						pap.*
					FROM
						property_application_preferences pap
						JOIN property_applications pa ON ( pap.cid = pa.cid AND pap.property_id = pa.property_id AND pap.company_application_id = pa.company_application_id )
					WHERE
						pap.cid = ' . ( int ) $intCid . ' AND pap.key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' ) AND pap.property_id = ' . ( int ) $intPropertyId . ' AND pa.id = ' . ( int ) $intCompanyApplicationId;

		return self::fetchPropertyApplicationPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyApplicationPreferencesByKeyByPropertyIdsByCid( $strKey, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) )  return NULL;

		$strSql = 'SELECT
					pa.id,
					   p.id AS property_id,
					   ca.title AS NAME,
					   ppr.value,
						CASE
                            WHEN ppr.value = \'1\' THEN NULL ELSE 1
                        END as key_value
					FROM
						properties p
					   LEFT JOIN property_applications pa ON ( p.id = pa.property_id AND pa.cid = p.cid )
					   LEFT JOIN property_application_preferences ppr ON ( pa.cid = ppr.cid AND pa.property_id = ppr.property_id AND pa.company_application_id = ppr.company_application_id AND ppr.key = \'' . $strKey . '\' )
					   LEFT JOIN company_applications ca ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
					WHERE
						pa.property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND pa.cid::integer = ' . ( int ) $intCid . '
						AND ca.deleted_on IS NULL
					ORDER BY
                        property_id';

		return fetchData( $strSql, $objDatabase );
	}

    public static function fetchPropertyApplicationPreferencesByCompanyApplicationIdByKeysByPropertyIdByCid( $intCompanyApplicationId, $arrstrKeys, $arrintPropertyIds, $intCid, $objDatabase ) {
        if( false == valArr( $arrstrKeys ) || false == valArr( $arrintPropertyIds ) ) return NULL;

        $strSql = 'SELECT 
                        * 
                    FROM 
                        property_application_preferences 
                    WHERE 
                        cid = ' . ( int ) $intCid . ' 
                        AND company_application_id = ' . ( int ) $intCompanyApplicationId . ' 
                        AND key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' ) 
                        AND property_id IN ( \'' . implode( '\',\'', $arrintPropertyIds ) . '\' ) ';

        return self::fetchPropertyApplicationPreferences( $strSql, $objDatabase );
    }

	public static function fetchExistingPropertyApplicationPreferencesByCompanyApplicationIdByKeysByPropertyIdByCid( $intCompanyApplicationId, $arrstrKeys, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT 
						pap.* 
					FROM 
						property_application_preferences as pap
						LEFT JOIN property_applications as pa ON ( pap.cid = pa.cid AND pap.property_id = pa.property_id AND pap.company_application_id = pa.company_application_id ) 
					WHERE 
						pap.cid = ' . ( int ) $intCid . ' 
						AND pap.company_application_id = ' . ( int ) $intCompanyApplicationId . ' 
						AND pap.key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' ) 
						AND pap.property_id IN ( \'' . implode( '\',\'', $arrintPropertyIds ) . '\' ) 
						AND pa.id IS NULL';

		return self::fetchPropertyApplicationPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyApplicationPreferencesByCompanyApplicationIdByPropertyIdsByCid( $intCompanyApplicationId, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT 
						* 
					FROM 
						property_application_preferences 
					WHERE 
						cid = ' . ( int ) $intCid . '
						AND company_application_id = ' . ( int ) $intCompanyApplicationId . ' 
						AND property_id IN ( \'' . implode( '\',\'', $arrintPropertyIds ) . '\' ) ';

		return self::fetchPropertyApplicationPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyApplicationPreferencesByKeyByCid( $strKey, $intCid, $objDatabase ) {
		if( false == valArr( $strKey ) ) return NULL;

		$strSql = 'SELECT property_id,company_application_id FROM property_application_preferences WHERE cid = ' . ( int ) $intCid . ' AND key IN (\'' . implode( "','", $strKey ) . '\')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyApplicationPreferencesByPropertyApplicationIdsIdByKeysByPropertyIdByCid( $arrintCompanyApplicationIds, $arrstrKeys, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) || false == valArr( $arrintCompanyApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						pap.*
					FROM
						property_application_preferences pap
						JOIN property_applications pa ON ( pap.cid = pa.cid AND pap.property_id = pa.property_id AND pap.company_application_id = pa.company_application_id )
					WHERE
						pap.cid = ' . ( int ) $intCid . ' 
						AND pap.key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' ) 
						AND pap.property_id = ' . ( int ) $intPropertyId . ' 
						AND pa.company_application_id IN ( \'' . implode( '\',\'', $arrintCompanyApplicationIds ) . '\' ) ';

		return self::fetchPropertyApplicationPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyApplicationPreferencesByCompanyApplicationIdsByPropertyIdByCid( $arrintCompanyApplicationIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCompnayApplicationIds ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						property_application_preferences
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND company_application_id IN ( ' . implode( ',', $arrintCompanyApplicationIds ) . ' )';

		return self::fetchPropertyApplicationPreferences( $strSql, $objDatabase );

	}

	public static function fetchPropertyApplicationPreferenceByCidByPropertyIdByCompanyApplicationIdByKey( $intCid, $intPropertyId, $intCompanyApplicationId, $strKey, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intCompanyApplicationId ) || false == valStr( $strKey ) ) return NULL;

		$strSql = ' SELECT
						* 
					FROM 
						property_application_preferences
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND company_application_id = ' . ( int ) $intCompanyApplicationId . '
						AND key = \'' . $strKey . '\'';

		return self::fetchPropertyApplicationPreferences( $strSql, $objDatabase );

	}

}
?>