<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportSources
 * Do not add any new functions to this class.
 */

class CImportSources extends CBaseImportSources {

	public static function fetchCustomImportSourceByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM import_sources WHERE integration_database_id = ' . ( int ) $intIntegrationDatabaseId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchImportSources( $strSql, $objDatabase );
	}

	public static function fetchUnProcessCustomImportSourceByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ims.*
					FROM
						import_sources ims
						JOIN import_batches ib ON (ims.id = ib.import_source_id AND ims.cid = ib.cid)
					WHERE
						ims.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
						AND ims.cid = ' . ( int ) $intCid . '
						AND processed_on IS NULL ORDER BY ims.id LIMIT 1';

		return self::fetchImportSource( $strSql, $objDatabase );
	}

	public static function fetchImportSourcesByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
					    *
					FROM
					    import_sources
					WHERE
					    CID = ' . ( int ) $intCid;

		return self::fetchImportSources( $strSql, $objDatabase );
	}

	public static function fetchImportSourcesByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM import_sources WHERE cid = ' . ( int ) $intCid . ' AND id IN( ' . implode( ',', $arrintIds ) . ')';
		return parent::fetchImportSources( $strSql, $objDatabase );
	}

}
?>