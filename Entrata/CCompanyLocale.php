<?php

class CCompanyLocale extends CBaseCompanyLocale {

	public function valDuplicateLocaleCode( $objClientDatabase ) {
		$boolIsValid = false;

		if( !valStr( $this->m_strLocaleCode ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'locale_code', 'Please enter valid company locale code.' ) );

			return $boolIsValid;
		}

		if( valObj( $objClientDatabase, 'CDatabase' ) ) {
			$intExistLocaleCodeCount = CCompanyLocales::fetchCompanyLocaleCodeCountByCidByLocaleCode( $this->m_intCid, $this->m_strLocaleCode, $objClientDatabase );

			if( 0 < $intExistLocaleCodeCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'locale_code', 'Duplicate client language' ) );

				return $boolIsValid;
			}
		}

		return true;
	}

	public function validate( $strAction, $objClientDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'validate_duplicate_locale_code':
				$boolIsValid = $this->valDuplicateLocaleCode( $objClientDatabase );
				break;

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>