<?php

class CScheduledChargeLog extends CBaseScheduledChargeLog {

	const IS_IGNORED_FALSE = 'false';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledChargeLogTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledChargeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledChargeLogId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldReportingEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewReportingEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldPostedThroughDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewPostedThroughDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valModifiedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequiresModification() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDelete() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function getLogDescription( $intScheduledChargeLogTypeId ) {

		$strLogDescription = NULL;

		switch( $intScheduledChargeLogTypeId ) {
			case CScheduledChargeLogType::MANUAL_CHANGE:
				$strLogDescription = __( 'Manual change' );
				break;

			case CScheduledChargeLogType::CHARGE_CODE_CHANGED_THROUGH_BULK_UPDATE:
				$strLogDescription = __( 'Charge code changed through bulk update' );
				break;

			case CScheduledChargeLogType::CHARGE_CODE_CODE_CLEAN_UP_REQUEST:
				$strLogDescription = __( 'Charge code changed through Charge code clean-up request' );
				break;

			case CScheduledChargeLogType::APPLICATION_RENEWAL_UPDATE:
				$strLogDescription = __( 'Lease renewed' );
				break;

			case CScheduledChargeLogType::LEASE_START_DATE_CHANGED:
				$strLogDescription = __( 'Lease start date modified' );
				break;

			case CScheduledChargeLogType::LEASE_END_DATE_CHANGED:
				$strLogDescription = __( 'Lease end date modified' );
				break;

			case CScheduledChargeLogType::MOVE_IN_DATE_CHANGED:
				$strLogDescription = __( 'Move in date modified' );
				break;

			case CScheduledChargeLogType::MOVE_OUT_DATE_APPLIED:
				$strLogDescription = __( 'Move out date applied' );
				break;

			case CScheduledChargeLogType::MOVE_OUT_DATE_REVERTED:
				$strLogDescription = __( 'Notice reverted' );
				break;

			case CScheduledChargeLogType::RENEWAL_APPROVED:
				$strLogDescription = __( 'Lease renewal approved' );
				break;

			case CScheduledChargeLogType::RENEWAL_TRUNCATION:
				$strLogDescription = __( 'Lease renewal truncation' );
				break;

			case CScheduledChargeLogType::RENEWAL_REVERTED:
				$strLogDescription = __( 'Lease approval reverted' );
				break;

			case CScheduledChargeLogType::PRORATION_DISABLED:
				$strLogDescription = __( 'Proration disabled' );
				break;

			case CScheduledChargeLogType::PRORATION_RE_ENABLED:
				$strLogDescription = __( 'Proration enabled' );
				break;

			case CScheduledChargeLogType::PM_SOFTWARE_INTEGRATION:
				$strLogDescription = __( 'Integration update' );
				break;

			case CScheduledChargeLogType::ENTRATA_CORE_DATA_MIGRATION:
				$strLogDescription = __( 'Data migrated' );
				break;

			case CScheduledChargeLogType::MONTH_TO_MONTH_LEASE_TRUNCATION_INSERT:
				$strLogDescription = __( 'Month to month period adjustment insert' );
				break;

			case CScheduledChargeLogType::MONTH_TO_MONTH_LEASE_TRUNCATION_UPDATE:
				$strLogDescription = __( 'Month to month period adjustment update' );
				break;

			case CScheduledChargeLogType::MONTH_TO_MONTH_LEASE_TRUNCATION_FEE_INSERT:
				$strLogDescription = __( 'Month to month fee created' );
				break;

			case CScheduledChargeLogType::SCHEDULED_CHARGE_POSTING_REWIND:
				$strLogDescription = __( 'Backing off scheduled charges' );
				break;

			case CScheduledChargeLogType::SCHEDULED_CHARGE_POSTING:
				$strLogDescription = __( 'Scheduled charges posted' );
				break;

			case CScheduledChargeLogType::INTERVAL_CANCELLATION:
				$strLogDescription = __( 'Lease interval cancelled' );
				break;

			case CScheduledChargeLogType::TRUNCATION_REVERSAL:
			case CScheduledChargeLogType::TRUNCATION_FEE_REVERSAL:
			case CScheduledChargeLogType::TRUNCATION_REVERSAL_PARENT_UPDATE:
				$strLogDescription = __( 'Month to month interval reversed' );
				break;

			case CScheduledChargeLogType::MOVE_IN_DATE_CLEAN_UP:
				$strLogDescription = __( 'Move in dates modified' );
				break;

			case CScheduledChargeLogType::RECALCULATE_CHARGE_AMOUNT:
				$strLogDescription = __( 'Recalculated charge amount' );
				break;

			case CScheduledChargeLogType::APPROVAL_ROUTING:
				$strLogDescription = __( 'Approval routing processed' );
				break;

			case CScheduledChargeLogType::TOTAL_VALUE_CALCULATED:
				$strLogDescription = __( 'Total value calculated' );
				break;

			case CScheduledChargeLogType::EXTENSION_APPROVED:
				$strLogDescription = __( 'Extension approved' );
				break;

			case CScheduledChargeLogType::EXTENSION_REVERTED:
				$strLogDescription = __( 'Extension reverted' );
				break;

			case CScheduledChargeLogType::BULK_SCHEDULED_CHARGE_ADJUSTMENT:
				$strLogDescription = __( 'Bulk scheduled charge adjustment' );
				break;

			default:
				$strLogDescription = '';
		}

		return $strLogDescription;
	}

}
?>