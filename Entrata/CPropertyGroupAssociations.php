<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyGroupAssociations
 * Do not add any new functions to this class.
 */

class CPropertyGroupAssociations extends CBasePropertyGroupAssociations {

	public static function fetchPropertyGroupAssociationByCidByPropertyGroupIdByPropertyId( $intCid, $intPropertyGroupId, $intPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						pga.*
					FROM
						property_group_associations pga
					    JOIN properties p ON ( pga.cid = p.cid AND p.id = pga.property_id AND p.is_disabled = 0 )
						JOIN property_groups pg ON ( pg.cid = pga.cid AND pg.id = p.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						pga.cid = ' . ( int ) $intCid . '
						AND pga.property_group_id = ' . ( int ) $intPropertyGroupId . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
					LIMIT
						1';

		return self::fetchPropertyGroupAssociation( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupAssociationsByCidByPropertyGroupId( $intCid, $intPropertyGroupId, $objDatabase ) {

		$strSql = 'SELECT
						pga.*
					FROM
						property_group_associations pga
						JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE 
						pga.cid = ' . ( int ) $intCid . '
						AND pga.property_group_id = ' . ( int ) $intPropertyGroupId;

		return self::fetchPropertyGroupAssociations( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupAssociationsByCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {

		$strSql = ' SELECT
						pga.*
					FROM
						property_group_associations pga
						JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						pga.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId;

		return self::fetchPropertyGroupAssociations( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupAssociationCountByCidByPropertyGroupId( $intCid, $intPropertyGroupId, $objDatabase ) {

		$strSql = ' SELECT
						count(1) as count
					FROM
						property_group_associations pga
						JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						pga.cid = ' . ( int ) $intCid . '
						AND pga.property_group_id = ' . ( int ) $intPropertyGroupId;

		return parent::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchPropertyGroupAssociationByCidByPropertyGroupIdByPropertyIds( $intCid, $arrintPropertyGroupIds, $intPropertyId, $objDatabase ) {

		$strSql = ' SELECT
						pga.*
					FROM
						property_group_associations pga
						JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						pga.cid = ' . ( int ) $intCid . '
						AND pga.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' )
						AND pga.property_id = ' . ( int ) $intPropertyId;

		return self::fetchPropertyGroupAssociations( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyGroupAssociationsByCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {

		$strSql = ' SELECT
						pga.*
					FROM
						property_group_associations pga
						JOIN property_groups pg ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )
					WHERE
						 pga.cid = ' . ( int ) $intCid . '
						 AND pga.property_id = ' . ( int ) $intPropertyId . '
						 AND pg.is_system = 0
						 AND pg.deleted_by IS NULL
						 AND pg.deleted_on IS NULL';

		return self::fetchPropertyGroupAssociations( $strSql, $objDatabase );
	}

	public static function fetchSystemPropertyGroupAssociationsBySystemCodeByCid( $strSystemCode, $intCid, $objDatabase ) {

		switch( $strSystemCode ) {

			case 'STATE':
				$strSql = 'SELECT
							    p.cid,
							    p.id property_id,
							    pg.id property_group_id,
							    NOW() updated_on,
							    NOW() created_on
							  FROM
							  	properties p
							    JOIN property_addresses pa ON ( p.id = pa.property_id
							                                   AND p.cid = pa.cid
							                                   AND pa.address_type_id = ' . CAddressType::PRIMARY . '
							                                   AND pa.is_alternate = false
							                                   AND pa.state_code IS NOT NULL )
							    JOIN property_groups pg ON ( pa.cid = pg.cid
							                                 AND pa.state_code = pg.system_code )
							    JOIN property_group_types pgt ON ( pg.cid = pgt.cid
							                                        AND pg.property_group_type_id = pgt.id
							                                        AND pgt.system_code=\'STATE\'
							                                        AND pgt.deleted_by IS NULL
							                                        AND pgt.deleted_on IS NULL )
							  WHERE
							  	p.cid = ' . ( int ) $intCid . '
							  	AND pg.deleted_by IS NULL
							  	AND pg.deleted_on IS NULL;';
				break;

			case 'PROPTYPE':
				$strSql = 'SELECT
							    p.cid,
							    p.id property_id,
							    pg.id property_group_id,
							    NOW() updated_on,
							    NOW() created_on
							  FROM
							  	properties p
							    JOIN property_types pt ON ( p.property_type_id = pt.id )
							    JOIN property_groups pg ON ( p.cid =  pg.cid
							                                 AND p.property_type_id::VARCHAR = pg.system_code )
							    JOIN property_group_types pgt ON ( pg.property_group_type_id = pgt.id
							                                        AND pgt.system_code=\'PROPTYPE\'
							                                        AND pgt.deleted_by IS NULL
							                                        AND pgt.deleted_on IS NULL )
							  WHERE
							  	p.cid = ' . ( int ) $intCid . '
							  	AND pg.deleted_by IS NULL
							  	AND pg.deleted_on IS NULL;';
				break;

			default:
				return NULL;

		}

		return self::fetchPropertyGroupAssociations( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupAssociationsByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = ' SELECT
						pga.*
					FROM
						property_group_associations pga
						JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						pga.cid = ' . ( int ) $intCid . '
						AND pga.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' )';

		return self::fetchPropertyGroupAssociations( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupAssociationsPropertyIdsByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase ) {
		if( false == valIntArr( $arrintPropertyGroupIds ) ) return NULL;
		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = '	SELECT
						pga.property_id
					FROM
						property_group_associations pga
						JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						pga.cid =' . ( int ) $intCid . '
						AND pga.property_group_id IN (' . implode( ',', $arrintPropertyGroupIds ) . ')';

		$arrmixValues = fetchData( $strSql, $objDatabase );

		$arrmixRekeyedValues = [];

		if( true == valArr( $arrmixValues ) ) {
			foreach( $arrmixValues as $arrmixValue ) {
				$arrmixRekeyedValues[] = $arrmixValue['property_id'];
			}
		}

		return $arrmixRekeyedValues;
	}

	public static function fetchActiveManagerialPropertyGroupAssociationsPropertyIdsByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase, $boolShowDisabledData = false ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = ' SELECT
						DISTINCT pga.property_id
					FROM
						property_group_associations pga
						JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN properties AS p ON ( p.id = pga.property_id AND p.cid = pga.cid )
					WHERE
						' . ( ( false == $boolShowDisabledData ) ? 'p.is_disabled <> 1 AND' : '' ) . '
						p.is_managerial <> 1
						AND pga.cid =' . ( int ) $intCid . '
						AND pga.property_group_id IN (' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ) ';

		$arrmixValues = fetchData( $strSql, $objDatabase );

		$arrmixRekeyedValues = [];

		if( true == valArr( $arrmixValues ) ) {
			foreach( $arrmixValues as $arrmixValue ) {
				$arrmixRekeyedValues[] = $arrmixValue['property_id'];
			}
		}

		return $arrmixRekeyedValues;
	}

	public static function fetchPropertyGroupAssociationsByCidsByPropertyIds( $arrintCids, $arrintPropertyIds, $objDatabase ) {
		if( false == valIntArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						pga.*,
						pg.property_group_type_id
					FROM
						property_group_associations pga
						JOIN property_groups pg ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )
						JOIN properties p ON ( pga.cid = p.cid AND p.id = pga.property_id AND p.is_disabled = 0 )
					WHERE
						 pga.cid IN ( ' . implode( ',', $arrintCids ) . ' ) 
						 AND pga.property_id IN ( ' . implode( ' ,', $arrintPropertyIds ) . ' )
						 AND pg.deleted_by IS NULL
						 AND pg.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						pga.*
					FROM
						property_group_associations pga
						JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						pga.property_id = ' . ( int ) $intPropertyId . '
						AND pga.cid = ' . ( int ) $intCid;

		return self::fetchPropertyGroupAssociations( $strSql, $objDatabase );
	}

	/*
	 * This function will fetch count of properties which is not associated with first ap payee location of $arrintApPayeeLocationIds.
	 *  Array $arrintApPayeeLocationIds should contain two ap payee locations
	 */

	public static function fetchPropertyGroupAssociationCountOfUnassociatedPropertiesByApPayeeLocationIdsByCid( $arrintApPayeeLocationIds, $intCid, $objDatabase ) {

		list( $intFirstApPayeeLocationId, $intSecondApPayeeLocationId ) = $arrintApPayeeLocationIds;

		$strSql = 'SELECT
						pga.property_id
					FROM
						ap_payee_property_groups appg
						JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = appg.property_group_id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_location_id = ' . ( int ) $intFirstApPayeeLocationId . '
					EXCEPT
					SELECT
						pga.property_id
					FROM
						ap_payee_property_groups appg
						JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = appg.property_group_id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_location_id = ' . ( int ) $intSecondApPayeeLocationId;

		return self::fetchPropertyGroupAssociations( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupAssociationsPropertyIdsByPropertyGroupIdsByTransmissionVendorIdByKeyByCompanyUserIdByCid( $arrintPropertyGroupIds, $intTransmissionVendorId, $strKey, $intCompanyUserId, $intCid, $objDatabase, $boolIsAdministrator = true ) {
		if( false == valIntArr( $arrintPropertyGroupIds ) ) return NULL;
		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = '	SELECT
						pga.property_id
					FROM
						property_group_associations pga
						JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_preferences pp ON  (pga.cid = pp.cid AND  pga.property_id = pp.property_id AND pp.key = ' . pg_escape_literal( $strKey ) . ' AND pp.value = \'' . $intTransmissionVendorId . '\' )
						JOIN property_gl_settings pgs ON ( pp.property_id = pgs.property_id AND pp.cid = pgs.cid )
						JOIN properties p ON ( pgs.property_id = p.id AND pgs.cid = p.cid AND p.is_disabled = 0 )';
		if( false == $boolIsAdministrator ) {
			$strSql .= ' JOIN view_company_user_properties vcup ON ( vcup.company_user_id = ' . ( int ) $intCompanyUserId . ' AND vcup.property_id = pp.property_id AND vcup.cid = pp.cid ) ';
		}
		$strSql .= ' WHERE
						pga.cid =' . ( int ) $intCid . '
						AND pgs.activate_standard_posting = true
						AND pga.property_group_id IN (' . implode( ',', $arrintPropertyGroupIds ) . ')';

		$arrmixValues = fetchData( $strSql, $objDatabase );

		$arrmixRekeyedValues = [];

		if( true == valArr( $arrmixValues ) ) {
			foreach( $arrmixValues as $arrmixValue ) {
				$arrmixRekeyedValues[] = $arrmixValue['property_id'];
			}
		}

		return $arrmixRekeyedValues;
	}

	public static function fetchPropertyGroupAssociationByApPayeeLocationIdsByCid( $arrintApPayeeLocationIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApPayeeLocationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pga.property_id,
						appg.ap_payee_location_id
					FROM
						ap_payee_property_groups appg
						JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = appg.property_group_id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

}
?>