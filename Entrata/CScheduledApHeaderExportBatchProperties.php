<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledApHeaderExportBatchProperties
 * Do not add any new functions to this class.
 */

class CScheduledApHeaderExportBatchProperties extends CBaseScheduledApHeaderExportBatchProperties {

	public static function fetchCustomScheduledApHeaderExportBatchPropertiesByScheduledApHeaderExportBatchIdByCid( $intScheduledApHeaderExportBatchId, $intCid, $objDatabase ) {

		$strSql = '  SELECT sahebp.*,
                        p.property_name
                    FROM 
                        scheduled_ap_header_export_batch_properties sahebp
                        JOIN properties p ON ( sahebp.property_id = p.id AND p.cid =sahebp.cid )
                    WHERE 
                        sahebp.cid = ' . ( int ) $intCid . '
                        AND sahebp.scheduled_ap_header_export_batch_id = ' . ( int ) $intScheduledApHeaderExportBatchId;

		return self::fetchScheduledApHeaderExportBatchProperties( $strSql, $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchPropertiesByScheduledApHeaderExportBatchIdsByCids( $arrintScheduledApHeaderExportBatchIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintCids ) || false == valArr( $arrintScheduledApHeaderExportBatchIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT * 
					FROM 
						scheduled_ap_header_export_batch_properties
					WHERE
						scheduled_ap_header_export_batch_id IN ( ' . implode( ',', $arrintScheduledApHeaderExportBatchIds ) . ' )
						AND cid IN ( ' . implode( ',', $arrintCids ) . ' )';

		return self::fetchScheduledApHeaderExportBatchProperties( $strSql, $objDatabase );
	}
}
?>