<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAnnouncementPsProducts
 * Do not add any new functions to this class.
 */

class CAnnouncementPsProducts extends CBaseAnnouncementPsProducts {

	public static function fetchAnnouncementPsProductByAnnouncementIdByPropertyIdByCid( $intAnnouncementId, $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchAnnouncementPsProduct( sprintf( 'SELECT app.* FROM %s AS app , %s AS pa WHERE pa.announcement_id::integer = app.announcement_id::integer AND pa.cid = app.cid AND pa.property_id = %d AND app.announcement_id = %d AND app.cid = %d LIMIT 1', 'announcement_ps_products', 'property_announcements', ( int ) $intPropertyId, ( int ) $intAnnouncementId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAnnouncementPsProductByAnnouncementIdByCid( $intAnnouncementId, $intCid, $objDatabase ) {
		return self::fetchAnnouncementPsProduct( sprintf( 'SELECT app.* FROM %s AS app WHERE app.cid = %d AND app.announcement_id = %d ', 'announcement_ps_products', ( int ) $intCid, ( int ) $intAnnouncementId ), $objDatabase );

	}

}
?>