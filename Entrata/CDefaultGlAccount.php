<?php

class CDefaultGlAccount extends CBaseDefaultGlAccount {

	protected $m_intGlAccountTypeId;
	protected $m_strDefaultGlGroupName;

	const ACCOUNTS_RECEIVABLE = 99;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultGlChartId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valGlAccountTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSystemCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsMappable() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>