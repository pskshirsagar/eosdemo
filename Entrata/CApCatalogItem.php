<?php

class CApCatalogItem extends CBaseApCatalogItem {

	protected $m_intPropertyId;
	protected $m_intApPayeeLocationId;

	protected $m_strApPayeeName;
	protected $m_strApPayeeLocationName;

	protected $m_arrobjCatalogSkuPropertyGroups;

	protected $m_arrmixApPayeeNames;

	/**
	 * Create Functions
	 */

	public function createCatalogSkuPropertyGroup() {

		$objCatalogSkuPropertyGroup	= new CCatalogSkuPropertyGroup();
		$objCatalogSkuPropertyGroup->setCid( $this->getCid() );

		return $objCatalogSkuPropertyGroup;
	}

	/**
	 * Get Functions
	 */

	public function getApPayeeName() {
		return $this->m_strApPayeeName;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getApPayeeLocationName() {
		return $this->m_strApPayeeLocationName;
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function getCatalogSkuPropertyGroups() {
		return $this->m_arrobjCatalogSkuPropertyGroups;
	}

	/**
	 * Set Functions
	 */

	public function setApPayeeName( $strApPayeeName ) {
		$this->m_strApPayeeName = $strApPayeeName;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setApPayeeLocationName( $strApPayeeLocationName ) {
		$this->m_strApPayeeLocationName = $strApPayeeLocationName;
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->m_intApPayeeLocationId = $intApPayeeLocationId;
	}

	public function setCatalogSkuPropertyGroups( $arrobjCatalogSkuPropertyGroups ) {
		$this->m_arrobjCatalogSkuPropertyGroups = ( true == valArr( $arrobjCatalogSkuPropertyGroups ) ) ? $arrobjCatalogSkuPropertyGroups : [];
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ap_payee_name'] ) ) {
			$this->setApPayeeName( $arrmixValues['ap_payee_name'] );
		}
		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
		if( true == isset( $arrmixValues['ap_payee_location_name'] ) ) {
			$this->setApPayeeLocationName( $arrmixValues['ap_payee_location_name'] );
		}
		if( true == isset( $arrmixValues['ap_payee_location_id'] ) ) {
			$this->setApPayeeLocationId( $arrmixValues['ap_payee_location_id'] );
		}
		return;
	}

	/**
	 * Val Functions
	 */

	public function valApPayeeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intApPayeeId ) || ( 1 > $this->m_intApPayeeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', __( 'Please select vendor.' ) ) );
		}

		return $boolIsValid;
	}

	public function valUnitOfMeasureId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intUnitOfMeasureId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', __( 'Please select purchase unit of measure.' ) ) );
		}

		return $boolIsValid;
	}

	public function valVendorSku( $arrmixCurrentVendorSkus, $arrmixExistingVendorSkus, $strCatalogItemName ) {

		$strNewVendorSku = \Psi\CStringService::singleton()->strtolower( $this->getVendorSku() );

		$strKey = $this->getApPayeeId() . '_' . $strNewVendorSku;

		if( true == valStr( $strNewVendorSku ) && ( true == array_key_exists( $strKey, $arrmixExistingVendorSkus ) && $strNewVendorSku == $arrmixExistingVendorSkus[$strKey]['vendor_sku'] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_sku', __( 'The SKU {%s,0} and {%s, 1} combination has already been added to {%s, 2} and cannot be duplicated.', [ $this->getVendorSku(), $this->getApPayeeName(), $arrmixExistingVendorSkus[$strKey]['catalog_item_name'] ] ) ) );
			return false;
		}

		if( true == valArr( $arrmixCurrentVendorSkus ) && true == array_key_exists( $strKey, $arrmixCurrentVendorSkus ) ) {

			$strNewlyAddedVendorSku = \Psi\CStringService::singleton()->strtolower( $arrmixCurrentVendorSkus[$strKey] );

			if( false == valStr( $this->getVendorSku() ) && false == valstr( $arrmixCurrentVendorSkus[$strKey] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_sku', __( 'Vendor SKU should be mandatory for vendor \'{%s, 0}.\'', [ $this->getApPayeeName() ] ) ) );
				return false;
			}

			if( true == valStr( $strNewVendorSku ) && ( true == valStr( $strNewlyAddedVendorSku ) && $strNewVendorSku == $strNewlyAddedVendorSku ) ) {

				if( true == valStr( $strCatalogItemName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_sku', __( 'The SKU {%s,0} and {%s, 1} combination has already been added to {%s, 2} and cannot be duplicated.', [ $this->getVendorSku(), $this->getApPayeeName(), $strCatalogItemName ] ) ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_sku', __( 'The SKU {%s,0} and {%s, 1} combination has already been added and cannot be duplicated.', [ $this->getVendorSku(), $this->getApPayeeName() ] ) ) );
				}

				return false;
			}
		}

		return true;
	}

	public function validate( $strAction, $arrmixCurrentVendorSkus = [], $arrmixExistingVendorSkus = [], $strCatalogItemName = '' ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valApPayeeId();
				$boolIsValid &= $this->valUnitOfMeasureId();
				$boolIsValid &= $this->valVendorSku( $arrmixCurrentVendorSkus, $arrmixExistingVendorSkus, $strCatalogItemName );
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>