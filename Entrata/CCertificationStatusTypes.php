<?php

class CCertificationStatusTypes extends CBaseCertificationStatusTypes {

	public static function fetchCertificationStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCertificationStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCertificationStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCertificationStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>