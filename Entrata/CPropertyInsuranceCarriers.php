<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyInsuranceCarriers
 * Do not add any new functions to this class.
 */

class CPropertyInsuranceCarriers extends CBasePropertyInsuranceCarriers {

	public static function fetchAllPropertyInsuranceCarriersByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_insurance_carriers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) 
					ORDER BY
						name';

		return self::fetchPropertyInsuranceCarriers( $strSql, $objDatabase );
	}

	public static function fetchPropertyInsuranceCarriersByNameByCid( $strName, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						id,
						name
					FROM
						property_insurance_carriers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND  name =  \'' . $strName . '\'';
		return self::fetchPropertyInsuranceCarriers( $strSql, $objDatabase );
	}

	public static function deletePropertyInsuranceCarriersByIdByCid( $arrInsuranceCarrierIds, $intCid, $objDatabase ) {

		$strSql = 'DELETE
					FROM
						property_insurance_carriers
					WHERE
						cid = ' . ( int ) $intCid . '
					    AND id in(' . sqlIntImplode( $arrInsuranceCarrierIds ) . ')';

		return executeSql( $strSql, $objDatabase );

	}

	public static function updatePropertyInsuranceCarriersByNameByIdByCid( $strName, $arrInsuranceCarrierIds, $intCid, $objDatabase ) {
		$strSql = 'UPDATE
						property_insurance_carriers
				    SET	name = \'' . $strName . '\'
				    WHERE
						cid = ' . ( int ) $intCid . '
					    AND id in(' . sqlIntImplode( $arrInsuranceCarrierIds ) . ')';

		return executeSql( $strSql, $objDatabase );
	}

}

?>