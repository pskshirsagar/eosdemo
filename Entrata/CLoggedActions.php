<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLoggedActions
 * Do not add any new functions to this class.
 */

class CLoggedActions extends CBaseLoggedActions {

    public static function fetchLoggedActions( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CLoggedAction', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchLoggedAction( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CLoggedAction', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>