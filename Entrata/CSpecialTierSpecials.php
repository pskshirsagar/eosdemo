<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSpecialTierSpecials
 * Do not add any new functions to this class.
 */

class CSpecialTierSpecials extends CBaseSpecialTierSpecials {

	public static function fetchSpecialTierSpecials( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return self::fetchObjects( $strSql, 'CSpecialTierSpecial', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false, $boolIsReturnKeyedArray );
	}

	public static function fetchSpecialTierSpecial( $strSql, $objDatabase ) {
		return self::fetchObjects( $strSql, 'CSpecialTierSpecial', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSpecialTierSpecialBySpecialTierIdsByCid( $arrintSpecialTierIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintSpecialTierIds ) ) 	return NULL;

		$strSql = '
					SELECT
						sts.*,
						s.special_type_id,
						s.name 
					FROM
						special_tier_specials sts
						JOIN specials s ON( s.cid = sts.cid AND sts.special_id = s.id AND s.special_type_id IN( ' . CSpecialType::STANDARD . ', ' . CSpecialType::GIFT_INCENTIVE . ' ) )
					WHERE
						sts.cid = ' . ( int ) $intCid . '
						AND sts.deleted_by IS NULL 
						AND sts.deleted_on IS NULL 
						AND sts.tier_special_id IN ( ' . implode( ',', $arrintSpecialTierIds ) . ' )';

		return self::fetchSpecialTierSpecials( $strSql, $objDatabase, $boolIsReturnKeyedArray = true );
	}

	public static function fetchCustomSpecialTierSpecialsByTierSpecialIdByCid( $intSpecialTierId, $intCid, $objDatabase ) {
		$strSql = '
					SELECT
						sts.*,
						s.name
					FROM
						special_tier_specials sts
						JOIN specials s ON ( s.cid = sts.cid AND s.id = sts.special_id )
					WHERE
						sts.cid = ' . ( int ) $intCid . '
						AND sts.tier_special_id = ' . ( int ) $intSpecialTierId . '
						AND s.deleted_by IS NULL
						AND s.deleted_on IS NULL';

		return self::fetchSpecialTierSpecials( $strSql, $objDatabase, $boolIsReturnKeyedArray = true );
	}

	public static function fetchCustomSpecialTierSpecialLogsBySpecialGroupIdByCid( $intSpecialGroupId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					*
					FROM
						(
						 SELECT
						      DATE ( sl.created_on ) as created_date,
						      sl.created_by as user_id,
						      initcap( s1.name ) ||\'( \' || st.name ||\')\' as name,
						      COALESCE( initcap( ce.name_first || \' \' || ce.name_last ), cu.username ) as user_name,
						      sl.*,
						     
						     rank ( ) OVER ( PARTITION BY DATE ( sl.created_on ),sl.created_by
						 ORDER BY
						     sl.created_on DESC, sl.id DESC ) AS rank
						 FROM
						     special_tier_specials_logs sl
						     JOIN specials s ON ( sl.cid = s.cid AND sl.tier_special_id = s.id )
						     JOIN specials s1 ON ( s1.cid = sl.cid AND s1.id = sl.special_id )
						     JOIN special_types st ON ( st.id = s1.special_type_id )
						     LEFT JOIN company_users cu ON ( cu.cid = sl.cid AND cu.id = sl.created_by )
						     LEFT JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
						 WHERE
						     sl.cid = ' . ( int ) $intCid . '
						     AND s.special_group_id = ' . ( int ) $intSpecialGroupId . '
						) AS sub
					ORDER BY
					sub.created_on DESC, sub.created_by';

		$arrmixSpecialTierSpecialLogs = fetchData( $strSql, $objDatabase );
		return $arrmixSpecialTierSpecialLogs;
	}

}
?>