<?php

class CDefaultGlAccountMask extends CBaseDefaultGlAccountMask {

	protected $m_intGlAccountTypeId;

	protected $m_strGlAccountTypeName;
	protected $m_strAccountNumberPattern;
	protected $m_strAccountNumberDelimiter;

	/**
	 * Get Functions
	 */

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function getGlAccountTypeName() {
		return $this->m_strGlAccountTypeName;
	}

	public function getAccountNumberPattern() {
		return $this->m_strAccountNumberPattern;
	}

	public function getAccountNumberDelimiter() {
		return $this->m_strAccountNumberDelimiter;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrValues['gl_account_type_id'] ) && $boolDirectSet ) $this->m_intGlAccountTypeId = trim( stripcslashes( $arrValues['gl_account_type_id'] ) );
		elseif ( isset( $arrValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gl_account_type_id'] ) : $arrValues['gl_account_type_id'] );
		if( isset( $arrValues['gl_account_type_name'] ) && $boolDirectSet ) $this->m_intGlAccountTypeName = trim( stripcslashes( $arrValues['gl_account_type_name'] ) );
		elseif ( isset( $arrValues['gl_account_type_name'] ) ) $this->setGlAccountTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gl_account_type_name'] ) : $arrValues['gl_account_type_name'] );
		if( isset( $arrValues['account_number_pattern'] ) && $boolDirectSet ) $this->m_strAccountNumberPattern = trim( stripcslashes( $arrValues['account_number_pattern'] ) );
		elseif ( isset( $arrValues['account_number_pattern'] ) ) $this->setAccountNumberPattern( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_number_pattern'] ) : $arrValues['account_number_pattern'] );
		if( isset( $arrValues['account_number_delimiter'] ) && $boolDirectSet ) $this->m_strAccountNumberDelimiter = trim( stripcslashes( $arrValues['account_number_delimiter'] ) );
		elseif ( isset( $arrValues['account_number_delimiter'] ) ) $this->setAccountNumberDelimiter( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_number_delimiter'] ) : $arrValues['account_number_delimiter'] );

		return;
	}

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->m_intGlAccountTypeId = CStrings::strToIntDef( $intGlAccountTypeId, NULL, false );
	}

	public function setGlAccountTypeName( $strGlAccountTypeName ) {
		$this->m_strGlAccountTypeName = CStrings::strTrimDef( $strGlAccountTypeName, 50, NULL, true );
	}

	public function setAccountNumberPattern( $strAccountNumberPattern ) {
		$this->m_strAccountNumberPattern = CStrings::strTrimDef( $strAccountNumberPattern, 50, NULL, true );
	}

	public function setAccountNumberDelimiter( $strAccountNumberDelimiter ) {
		$this->m_strAccountNumberDelimiter = CStrings::strTrimDef( $strAccountNumberDelimiter, 50, NULL, true );
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultGlChartId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultGlTreeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultGlGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHideIfZero() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>