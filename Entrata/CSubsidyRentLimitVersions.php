<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyRentLimitVersions
 * Do not add any new functions to this class.
 */

class CSubsidyRentLimitVersions extends CBaseSubsidyRentLimitVersions {

	public static function fetchPublishedLatestSubsidyRentLimitVersionByPropertyIdByCid( $objDatabase, $intPropertyId = NULL, $intCid = CClient::ID_DEFAULT ) {

		$strSql = 'SELECT
						srlv.id,
						srlv.year_published,
						srlv.effective_date
					FROM
						subsidy_rent_limit_versions srlv
					WHERE
						srlv.effective_date < NOW()
						AND srlv.is_published = TRUE
						AND srlv.property_id = ' . ( int ) $intPropertyId . '
						AND srlv.cid = ' . ( int ) $intCid . '
					ORDER BY
						srlv.year_published DESC';
		$arrstrRentLimitVersion = ( array ) fetchData( $strSql, $objDatabase );

		return array_values( $arrstrRentLimitVersion );

	}

	public static function fetchSubsidyRentLimitVersionIdByYearPublishedByPropertyIdByCid( string $strYearPublished, int $intPropertyId, int $intCid, CDatabase $objDatabase ) {

		$strSql = 'SELECT
						srlv.id AS subsidy_rent_limit_version_id
					FROM
						subsidy_rent_limit_versions srlv
					WHERE
						srlv.cid = ' . ( int ) $intCid . '
						AND srlv.property_id = ' . ( int ) $intPropertyId . '
						AND srlv.year_published = ' . $strYearPublished . '
						AND srlv.is_published = TRUE';

		return self::fetchColumn( $strSql, 'subsidy_rent_limit_version_id', $objDatabase );
	}

}
?>