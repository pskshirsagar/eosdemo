<?php

class CGuestFrequencyType extends CBaseGuestFrequencyType {

	const DAILY 		= 1;
	const WEEKLY 		= 2;
	const MONTHLY		= 3;
	const WEEK_DAY 		= 4;
	const YEARLY		= 5;
	const NEVER 		= 6;


	public static $c_arrintRecurringGuestFrequencyTypes = [ self::DAILY, self::WEEKLY, self::MONTHLY, self::WEEK_DAY, self::YEARLY ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>