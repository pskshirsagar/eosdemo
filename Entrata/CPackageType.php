<?php

class CPackageType extends CBasePackageType {

	const LETTER		= 1;
	const US_MAIL 		= 2;
	const MOVIE  		= 3;
	const PACKAGE  		= 4;
	const FEDEX  		= 5;
	const UPS			= 6;
	const DHL			= 7;
	const DRY_CLEANING	= 8;
	const PACKAGE_OTHERS = 9;
	const PETS			= 10;
	const FLOWERS 		= 11;
	const RX_MEDICAL	= 12;
	const GROCERIES 	= 13;
	const LASERSHIP		= 14;
	const AMAZON		= 15;

    public function valId() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getName() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getDescription() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	public static function getPackageTypeNames() {

    	$arrstrPackageTypeNames[CPackageType::LETTER] 			= 'Letter';
    	$arrstrPackageTypeNames[CPackageType::US_MAIL] 		= 'USPS';
    	$arrstrPackageTypeNames[CPackageType::MOVIE] 			= 'Movie';
    	$arrstrPackageTypeNames[CPackageType::PACKAGE] 		= 'Package';
    	$arrstrPackageTypeNames[CPackageType::FEDEX] 			= 'FedEx';
    	$arrstrPackageTypeNames[CPackageType::UPS] 			= 'UPS';
    	$arrstrPackageTypeNames[CPackageType::DHL] 			= 'DHL';
    	$arrstrPackageTypeNames[CPackageType::DRY_CLEANING] 	= 'Dry Cleaning';
    	$arrstrPackageTypeNames[CPackageType::PACKAGE_OTHERS] 				= 'Other';
    	$arrstrPackageTypeNames[CPackageType::PETS] 			= 'Pets';
    	$arrstrPackageTypeNames[CPackageType::FLOWERS] 		= 'Flowers';
    	$arrstrPackageTypeNames[CPackageType::RX_MEDICAL] 		= 'Rx (medical)';
    	$arrstrPackageTypeNames[CPackageType::GROCERIES] 		= 'Groceries';
    	$arrstrPackageTypeNames[CPackageType::LASERSHIP]		= 'Lasership';

    	return $arrstrPackageTypeNames;
    }
}
?>