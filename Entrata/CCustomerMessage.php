<?php

class CCustomerMessage extends CBaseCustomerMessage {

	protected $m_strSenderName;
	protected $m_strFormattedCustomerMessageDate;
	protected $m_strFormattedFromEmailAddress;
	protected $m_strSystemEmailContent;

	protected $m_intSystemEmailId;

    public function __construct() {
        parent::__construct();
        $this->m_arrobjEmailAttachments = NULL;

        return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function addEmailAttachment( $objEmailAttachment ) {
    	$this->m_arrobjEmailAttachments[$objEmailAttachment->getId()] = $objEmailAttachment;
    }

    /**
     * Get Functions
     */

    public function getSenderName() {
    	return $this->m_strSenderName;
    }

    public function getFormattedCustomerMessageDate() {
    	return $this->m_strFormattedCustomerMessageDate;
    }

    public function getFormattedFromEmailAddress() {
    	return $this->m_strFormattedFromEmailAddress;
    }

    public function getEmailAttachments() {
    	return $this->m_arrobjEmailAttachments;
    }

    public function getMessage( $objEmailDatabase = NULL ) {
    	// If a system email ID is set, return the message content based on the system email.
    	// Otherwise return the message field of the CustomerMessage
    	if( false == is_null( $this->getSystemEmailId() ) && true == is_null( $this->m_strMessage ) ) {
    		if( true == is_null( $this->m_strSystemEmailContent ) ) {
    			$this->m_strSystemEmailContent = $this->getMessageBySystemEmail( $objEmailDatabase );
    		}
    		return $this->m_strSystemEmailContent;
    	}

    	return $this->m_strMessage;
    }

    public function getMessageBySystemEmail( $objEmailDatabase = NULL ) {
    	if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			$objEmailDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::PS_DEVELOPER );
		}
		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) return false;

		$objSystemEmail = CSystemEmails::fetchSystemEmailById( $this->getSystemEmailId(), $objEmailDatabase );

		if( false == valObj( $objSystemEmail, 'CSystemEmail' ) ) return false;

    	$objSystemEmailRecipient = CSystemEmailRecipients::fetchSystemEmailRecipientBySystemEmailIdByRecipientTypeIdByReferenceNumber( $objSystemEmail->getId(), CSystemEmailRecipientType::RESIDENT, $this->getCustomerId(), $objEmailDatabase );

    	if( true == valObj( $objSystemEmailRecipient, 'CSystemEmailRecipient' ) ) {
    		$arrstrMergeFields 	= unserialize( $objSystemEmailRecipient->getMergeFields() );
    		$strContent 		= $objSystemEmail->getHtmlContent();

    		if( true == valArr( $arrstrMergeFields ) ) {
    			foreach( $arrstrMergeFields as $strKey => $strValue ) {
    				$strContent	= \Psi\CStringService::singleton()->str_ireplace( $strKey, $strValue, $strContent );
    			}
    		}

    		$objSystemEmail->setFormattedToEmailAddress( $objSystemEmailRecipient->getEmailAddress() );

    		$objSystemEmail->setHtmlContent( $strContent );
    	}

    	return $objSystemEmail->getHtmlContent();
    }

    /**
     * Set Functions
     */

    public function setSenderName( $strSenderName ) {
    	$this->m_strSenderName = $strSenderName;
    }

    public function setFormattedCustomerMessageDate( $strFormattedCustomerMessageDate ) {
    	$this->m_strFormattedCustomerMessageDate = $strFormattedCustomerMessageDate;
    }

    public function setFormattedFromEmailAddress( $strFormattedFromEmailAddress ) {
    	$this->m_strFormattedFromEmailAddress = $strFormattedFromEmailAddress;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrValues['sender_name'] ) ) $this->setSenderName( $arrValues['sender_name'] );
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSenderId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSenderTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSystemEmailId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSubject() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strSubject ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subject', 'Subject is required.', 502 ) );
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function valMessage() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strMessage ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', 'Message is required.', 502 ) );
        	$boolIsValid = false;
        }
        return $boolIsValid;
    }

    public function valIsUrgent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsRead() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

			case 'validate_insert_resident_portal20':
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valMessage();
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>