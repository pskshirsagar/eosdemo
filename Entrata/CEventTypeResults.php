<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEventTypeResults
 * Do not add any new functions to this class.
 */

class CEventTypeResults extends CBaseEventTypeResults {


	public static function fetchEventTypeResultByEventTypeIdByCid( $intEventTypeId, $intCid, $objDatabase ) {
		return self::fetchEventTypeResult( sprintf( 'SELECT * FROM event_type_results WHERE event_type_id = %d AND cid = %d', ( int ) $intEventTypeId, ( int ) $intCid ), $objDatabase );
	}
}
?>