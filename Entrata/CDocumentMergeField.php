<?php

class CDocumentMergeField extends CBaseDocumentMergeField {

	protected $m_intIsDisabled;
	protected $m_intIsLocked;
	protected $m_intMaxlength;
	protected $m_intMergeFieldTypeId;
	protected $m_intMergeFieldSubGroupId;

	protected $m_strFieldValue;
	protected $m_strViewExpression;
	protected $m_strSystemCode;
	protected $m_strTitle;

	protected $m_arrobjDocuments;
	protected $m_arrobjDocumentMergeFields;
	protected $m_arrobjChildDocumentMergeFields;

	protected $m_jsonDetails;

	protected $m_boolIsEntrataDefault;

    /**
     * Get Functions
     */

    public function getDocuments() {
    	return $this->m_arrobjDocuments;
    }

    public function getDocumentMergeFields() {
    	return $this->m_arrobjDocumentMergeFields;
    }

	public function getChildDocumentMergeFields() {
		return $this->m_arrobjChildDocumentMergeFields;
	}

    public function getDocumentNames() {
    	$strNames = NULL;
    	if( true == valArr( $this->m_arrobjDocuments ) ) {
    		foreach( $this->m_arrobjDocuments as $objDocument ) {
    			$arrstrNames[] = $objDocument->getName();
    		}

    		$strNames = implode( ', ', $arrstrNames );
    	}

    	return $strNames;
    }

	public function getDocumentMergeFieldNames() {
    	$strNames    = NULL;
    	$arrstrNames = [];

    	if( true == valArr( $this->m_arrobjDocumentMergeFields ) ) {
    		foreach( $this->m_arrobjDocumentMergeFields as $objDocumentMergeField ) {
	    		if( false == in_array( $objDocumentMergeField->getField(), $arrstrNames ) )
	    			$arrstrNames[]    = $objDocumentMergeField->getField();
    		}

    		$strNames = implode( ', ', $arrstrNames );
    	}

    	return $strNames;
    }

    public function getIsDisabled() {
    	return $this->m_intIsDisabled;
    }

    public function getFieldValue() {
    	return $this->m_strFieldValue;
    }

    public function getIsLocked() {
    	return $this->m_intIsLocked;
    }

    public function getMaxlength() {
    	return $this->m_intMaxlength;
    }

	public function getMergeFieldSubGroupId() {
		return $this->m_intMergeFieldSubGroupId;
	}

    public function getViewExpression() {
        return $this->m_strViewExpression;
    }

    public function getTitle() {
        return $this->m_strTitle;
    }

	public function setTitle( $strTitle ) {
		$this->m_strTitle = $strTitle;
	}

	public function setMergeFieldSubGroupId( $intMergeFieldSubGroupId ) {
		$this->m_intMergeFieldSubGroupId = $intMergeFieldSubGroupId;
	}

    public function getSystemCode() {
        return $this->m_strSystemCode;
    }

    public function getIsEntrataDefault() {
        return $this->m_boolIsEntrataDefault;
    }

	public function getMergeFieldTypeId() {
		return $this->m_intMergeFieldTypeId;
	}

	public function setMergeFieldTypeId( $intMergeFieldTypeId ) {
		$this->m_intMergeFieldTypeId = $intMergeFieldTypeId;
	}

    /**
     * Set Functions
     */

	public function setChildDocumentMergeFields( $arrobjBlockChildMergeFields ) {
		$this->m_arrobjChildDocumentMergeFields = $arrobjBlockChildMergeFields;
	}

	public function setIsDisabled( $intIsDisabled ) {
    	$this->m_intIsDisabled = $intIsDisabled;
    }

    public function setFieldValue( $strFieldValue ) {
    	$this->m_strFieldValue = $strFieldValue;
    }

    public function setIsLocked( $intIsLocked ) {
    	$this->m_intIsLocked = $intIsLocked;
    }

    public function setMaxlength( $intMaxlength ) {
    	$this->m_intMaxlength = $intMaxlength;
    }

    public function setViewExpression( $strViewExpression ) {
        $this->m_strViewExpression = $strViewExpression;
    }

    public function setSystemCode( $intSystemCode ) {
        $this->m_strSystemCode = $intSystemCode;
    }

    public function setIsEntrataDefault( $boolIsEntrataDefault ) {
        $this->m_boolIsEntrataDefault = $boolIsEntrataDefault;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrmixValues['is_locked'] ) && $boolDirectSet ) $this->m_intIsLocked = trim( $arrmixValues['is_locked'] );
		elseif( isset( $arrmixValues['is_locked'] ) ) {
			$this->setIsLocked( $arrmixValues['is_locked'] );
		}

	    if( isset( $arrmixValues['merge_field_sub_group_id'] ) && $boolDirectSet ) $this->m_intMergeFieldSubGroupId = trim( $arrmixValues['merge_field_sub_group_id'] );
	    elseif( isset( $arrmixValues['merge_field_sub_group_id'] ) ) {
		    $this->setMergeFieldSubGroupId( $arrmixValues['merge_field_sub_group_id'] );
	    }

	    if( isset( $arrmixValues['title'] ) && $boolDirectSet ) $this->m_strTitle = trim( $arrmixValues['title'] );
	    elseif( isset( $arrmixValues['title'] ) ) {
		    $this->setTitle( $arrmixValues['title'] );
	    }

		if( isset( $arrmixValues['view_expression'] ) && $boolDirectSet ) $this->m_strViewExpression = trim( $arrmixValues['view_expression'] );
		elseif( isset( $arrmixValues['view_expression'] ) ) {
			$this->setViewExpression( $arrmixValues['view_expression'] );
		}

		if( true == isset( $arrmixValues['system_code'] ) ) $this->setSystemCode( $arrmixValues['system_code'] );

		if( true == isset( $arrmixValues['is_entrata_default'] ) ) $this->setIsEntrataDefault( ( 't' == $arrmixValues['is_entrata_default'] ) ? true : false );

		if( true == isset( $arrmixValues['details'] ) ) $this->setDetails( $arrmixValues['details'] );

	    if( true == isset( $arrmixValues['merge_field_type_id'] ) ) $this->setMergeFieldTypeId( $arrmixValues['merge_field_type_id'] );

		return;
    }

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * other Functions
     */

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( 'NOW()' );

		if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

    public function addDocument( $objDocument ) {
	    if( true == valObj( $objDocument, 'CDocument' ) ) {
            $this->m_arrobjDocuments[$objDocument->getId()] = $objDocument;
        }
    }

    public function addDocumentMergeField( $objDocumentMergeField ) {
    	$this->m_arrobjDocumentMergeFields[$objDocumentMergeField->getId()] = $objDocumentMergeField;
    }

	public function addChildDocumentMergeField( $objDocumentMergeField ) {
		if( true == valId( $objDocumentMergeField->getOrderNum() ) ) {
			$strKey = $objDocumentMergeField->getOrderNum();
		} else {
			$strKey = $objDocumentMergeField->getField();
		}
		$this->m_arrobjChildDocumentMergeFields[$strKey] = $objDocumentMergeField;
	}

    public function mapDefaultMergeField( $objDefaultMergeField, $boolSetExternalMergeFieldGroupId = false ) {

		if( false == valObj( $objDefaultMergeField, 'CDefaultMergeField' ) ) return false;

    	$this->setDefaultMergeFieldId( $objDefaultMergeField->getId() );
    	$this->setMergeFieldGroupId( $objDefaultMergeField->getMergeFieldGroupId() );
    	if( true == $boolSetExternalMergeFieldGroupId ) {
    		$this->setExternalMergeFieldGroupId( $objDefaultMergeField->getExternalMergeFieldGroupId() );
    	}
		$this->setDefaultValue( $objDefaultMergeField->getDefaultValue() );
		$this->setAllowUserUpdate( $objDefaultMergeField->getAllowUserUpdate() );
		$this->setAllowAdminUpdate( $objDefaultMergeField->getAllowAdminUpdate() );
		$this->setForceOriginUpdate( $objDefaultMergeField->getAllowOriginUpdate() );
		$this->setShowInMergeDisplay( $objDefaultMergeField->getShowInMergeDisplay() );
		$this->setIsBlockField( $objDefaultMergeField->getIsBlockField() );
		$this->setIsRequired( $objDefaultMergeField->getIsRequired() );
    }

}
?>