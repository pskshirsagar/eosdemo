<?php

class CJobPhaseMaintenanceProblem extends CBaseJobPhaseMaintenanceProblem {

	protected $m_intRenovationDaysToComplete;

	public function getRenovationDaysToComplete() {
		return $this->m_intRenovationDaysToComplete;
	}

	public function setRenovationDaysToComplete( $intRenovationDaysToComplete ) {
		$this->m_intRenovationDaysToComplete = $intRenovationDaysToComplete;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobId() {
		$boolIsValid = true;

		if( true == is_null( $this->getJobId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'job_id', 'Job id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valJobPhaseId() {
		$boolIsValid = true;

		if( true == is_null( $this->getJobPhaseId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'job_phase_id', 'Job phase id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMaintenanceLocationId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMaintenanceLocationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', 'Maintenance location is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMaintenanceProblemId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMaintenanceProblemId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', 'Maintenance location problem is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valJobId();
				$boolIsValid &= $this->valJobPhaseId();
				$boolIsValid &= $this->valMaintenanceLocationId();
				$boolIsValid &= $this->valMaintenanceProblemId();
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( $boolDirectSet && isset( $arrmixValues['renovation_days_to_complete'] ) ) {
			$this->set( 'm_intRenovationDaysToComplete', trim( $arrmixValues['renovation_days_to_complete'] ) );
		} elseif( isset( $arrmixValues['renovation_days_to_complete'] ) ) {
			$this->setRenovationDaysToComplete( trim( $arrmixValues['renovation_days_to_complete'] ) );
		}

	}

}
?>