<?php

class CAmenityFilter extends CBaseAmenityFilter {

	protected $m_intCid;
	protected $m_strDescription;

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );
		if( true == isset( $arrmixValues['cid'] ) )			$this->setCid( $arrmixValues['cid'] );
		if( true == isset( $arrmixValues['description'] ) )	$this->setDescription( $arrmixValues['description'] );
	}

	/**
	 * Get Functions
	 *
	 */

	public function getCid() {
		return $this->m_intCid;
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setCid( $intCid ) {
		$this->m_intCid = $intCid;
	}

	public function setDescription( $strDescription ) {
		$this->m_strDescription = $strDescription;
	}

	/**
	 * Validate functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultAmenityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $intClient, $objClientDatabase ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strName ) || 0 == strlen( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
		}

		if( false == is_null( $intClient ) ) {
			$intConflinctingAmenityFiltersCount = 0;

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND LOWER(name) = \'' . trim( \Psi\CStringService::singleton()->strtolower( addslashes( $this->getName() ) ) ) . '\'
							AND id <>' . ( int ) ( ( false == is_numeric( $this->getId() ) ) ? 0 : $this->getId() );

			$intConflinctingAmenityFiltersCount = ( int ) \Psi\Eos\Entrata\CAmenityFilters::createService()->fetchAmenityFilterCount( $strWhere, $objClientDatabase );

			if( 0 < $intConflinctingAmenityFiltersCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name already exists.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $intCid = NULL, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $intCid, $objClientDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $intCid, $objClientDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function getFormattedName() {
		if( true == is_null( $this->getName() ) || 0 == strlen( $this->getName() ) ) {
			return NULL;
		}

		$strAmenityName = $this->getName();

		\Psi\CStringService::singleton()->preg_match_all( '/[A-Z0-9]+/', $strAmenityName, $arrstrOut );

		$arrstrOut = array_pop( $arrstrOut );

		if( true == valArr( $arrstrOut ) ) {

			if( 1 == \Psi\CStringService::singleton()->strlen( $arrstrOut[0] ) ) {
				unset( $arrstrOut[0] );
			} elseif( 2 < \Psi\CStringService::singleton()->strlen( $arrstrOut[0] ) ) {
				$strOut  		= $arrstrOut[0];
				$arrstrOut[0] 	= $strOut[( \Psi\CStringService::singleton()->strlen( $strOut ) - 1 )];
			}

			foreach( $arrstrOut as $strOut ) {
				$arrstrPattern[] 		= '/' . $strOut . '/';
				$arrstrReplacements[] 	= ' ' . $strOut;
			}

			if( true == isset( $arrstrPattern ) && true == isset( $arrstrReplacements ) ) {
				$strAmenityName = \Psi\CStringService::singleton()->preg_replace( $arrstrPattern, $arrstrReplacements, $strAmenityName );
			}

			$strAmenityName = \Psi\CStringService::singleton()->preg_replace( '/\s+/i', ' ', $strAmenityName );
		}

		return $strAmenityName;
	}

}
?>