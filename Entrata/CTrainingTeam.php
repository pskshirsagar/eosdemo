<?php

class CTrainingTeam extends CBaseTrainingTeam {

	const UNIVERSAL_TEAM_NAME 	= 'Universal Training Team';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $strName, $objDatabase = NULL ) {
		$boolIsValid = true;

		$this->m_strName = $strName;
		if( is_null( $this->m_objDatabase ) ) {
			$this->m_objDatabase = $objDatabase;
		}

		if( empty( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Team name is required.' ) ) );
		} elseif( $this->m_strName != strip_tags( $this->getName() ) || 2 > strlen( trim( $this->m_strName ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Valid team name is required.' ) ) );
		}

		if( $boolIsValid && isset( $objDatabase ) ) {
			$strSql 	= ' WHERE name ILIKE E' . $this->sqlName() . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' );
			$intCount 	= \Psi\Eos\Entrata\CTrainingTeams::createService()->fetchTrainingTeamCount( $strSql, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Team name { %s, team_name } already exist.', [ 'team_name' => $this->m_strName ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valExpirationDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $strTeamName, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $strTeamName, $objDatabase );
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>