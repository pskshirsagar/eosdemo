<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTransmissions
 * Do not add any new functions to this class.
 */

class CTransmissions extends CBaseTransmissions {

	public static function fetchLatestTransmissionsByApplicantApplicationIdsByCid( $intApplicationId, $arrintApplicantApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicantApplicationIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						transmissions
					WHERE
						applicant_application_id IN( ' . implode( ',', $arrintApplicantApplicationIds ) . ' )
						AND application_id = ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid . '
					ORDER BY id DESC LIMIT 1';

		return self::fetchTransmissions( $strSql, $objDatabase );
	}

	public static function fetchTransmissionsByApplicantApplicationIdsByCid( $arrintApplicantApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicantApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						transmissions
					WHERE
						applicant_application_id IN( ' . implode( ',', $arrintApplicantApplicationIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
					ORDER BY id';

		return self::fetchTransmissions( $strSql, $objDatabase );
	}

	public static function fetchLatestTransmissionByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM
						transmissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND application_id = ' . ( int ) $intApplicationId . '
						AND transmission_type_id = ' . ( int ) CTransmissionType::SCREENING . '
					ORDER BY id DESC LIMIT 1';

		return self::fetchTransmission( $strSql, $objDatabase );
	}

	public static function fetchCustomTransmissionsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM transmissions WHERE application_id = ' . ( int ) $intApplicationId . ' AND cid = ' . ( int ) $intCid . ' AND applicant_application_id IS NOT NULL ORDER BY id DESC';
		return self::fetchTransmissions( $strSql, $objDatabase );
	}

	public static function fetchTransmissionByApplicationIdByTransmissionTypeIdByLimitOneByCid( $intApplicationId, $intTransmissionTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM transmissions WHERE application_id = ' . ( int ) $intApplicationId . ' AND cid = ' . ( int ) $intCid . ' AND transmission_type_id = ' . ( int ) $intTransmissionTypeId . ' ORDER BY id DESC LIMIT 1';
		return self::fetchTransmission( $strSql, $objDatabase );
	}

	public static function fetchTransmissionsByApplicationIdByTransmissionTypeIdByTransmissionVendorIdByCid( $intApplicationId, $intTransmissionTypeId, $intTransmissionVendorId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM transmissions WHERE application_id = ' . ( int ) $intApplicationId . ' AND transmission_type_id = ' . ( int ) $intTransmissionTypeId . ' AND cid = ' . ( int ) $intCid . ' AND transmission_vendor_id = ' . ( int ) $intTransmissionVendorId . ' ORDER BY id DESC ';
		return self::fetchTransmissions( $strSql, $objDatabase );
	}

	public static function fetchTransmissionsByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM transmissions WHERE application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' ) AND cid = ' . ( int ) $intCid;
		return self::fetchTransmissions( $strSql, $objDatabase );
	}

	public static function fetchLastTransmissionsByTransmissionTypeIdByApplicationIdsByCid( $intTransmissionTypeId, $arrintApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM (
							SELECT
								*,
								MAX( id ) OVER( PARTITION BY application_id ) as max_application_id
							FROM
								transmissions
							WHERE
								cid = ' . ( int ) $intCid . '
								AND transmission_type_id = ' . ( int ) $intTransmissionTypeId . '
								AND application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						) as sub
					WHERE
						id = max_application_id
						AND sub.cid = ' . ( int ) $intCid;

		return self::fetchTransmissions( $strSql, $objDatabase );
	}

	public static function fetchTransmissionsByCompanyTransmissionVendorIdByPropertyIdByCid( $intCompanyTransmissionVendorId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = ' SELECT 
                        *
                        FROM transmissions
                        WHERE company_transmission_vendor_id = ' . ( int ) $intCompanyTransmissionVendorId . '
                        AND property_id = ' . ( int ) $intPropertyId . '
                        AND cid = ' . ( int ) $intCid;

		return self::fetchTransmissions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTransmissionResults( $objTransmissionResultsFilter, $intArrPropertyIds =[], $intPageNo, $intPageSize, $objDatabase ) {
		if( true == valObj( $objTransmissionResultsFilter, 'CTransmissionsFilter' ) ) {
			$intTransmissionVendorId = $objTransmissionResultsFilter->getTransmissionVendorId();
			$intTransmissionTypeId   = $objTransmissionResultsFilter->getTransmissionTypeId();
			$intClientId             = $objTransmissionResultsFilter->getCid();
			$intId                   = $objTransmissionResultsFilter->getId();
			$intIsFailed             = $objTransmissionResultsFilter->getIsFailed();
			$strFromDate             = $objTransmissionResultsFilter->getFromDate();
			$strToDate               = $objTransmissionResultsFilter->getToDate();

		}

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'SELECT * FROM transmissions ';

		if( true == valId( $intClientId ) ) {
			$strSql .= ( ( \Psi\CStringService::singleton()->stripos( $strSql, 'where' ) == true ) ? ' AND ' : ' WHERE ' ) . ' cid = ' . ( int ) $intClientId;
		}

		if( true == valId( $intTransmissionTypeId ) ) {
			$strSql .= ( ( \Psi\CStringService::singleton()->stripos( $strSql, 'where' ) == true ) ? ' AND ' : ' WHERE ' ) . ' transmission_type_id = ' . ( int ) $intTransmissionTypeId;
		}

		if( true == valId( $intId ) ) {
			$strSql .= ( ( \Psi\CStringService::singleton()->stripos( $strSql, 'where' ) == true ) ? ' AND ' : ' WHERE ' ) . 'id = ' . ( int ) $intId;
		}

		if( true == valStr( $strFromDate ) ) {
			$strSql .= ( ( \Psi\CStringService::singleton()->stripos( $strSql, 'where' ) == true ) ? ' AND ' : ' WHERE' ) . ' created_on >= \'' . date( 'Y-m-d', strtotime( $strFromDate ) ) . '\' AND created_on <= \'' . date( 'Y-m-d', strtotime( $strToDate ) ) . ' 23:59:59\'';
		}

		if( 1 == valId( $intIsFailed ) ) {
			$strSql .= ( ( \Psi\CStringService::singleton()->stripos( $strSql, 'where' ) == true ) ? ' AND ' : ' WHERE ' ) . ' is_failed = ' . ( int ) $intIsFailed;
		}

		if( true == valId( $intTransmissionVendorId ) ) {
			$strSql .= ' AND transmission_vendor_id = ' . ( int ) $intTransmissionVendorId;
		}

		if( true == valArr( $intArrPropertyIds ) ) {
			$strSql .= ' AND property_id IN ( ' . implode( ',', $intArrPropertyIds ) . ')';
		}

		$strSql .= ' ORDER BY id desc  OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit . ' ;';

		return self::fetchTransmissions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTransmissionResultsCount( $objTransmissionResultsFilter, $intArrPropertyIds = [], $objDatabase ) {
		if( true == valObj( $objTransmissionResultsFilter, 'CTransmissionsFilter' ) ) {
			$intTransmissionVendorId = $objTransmissionResultsFilter->getTransmissionVendorId();
			$intTransmissionTypeId   = $objTransmissionResultsFilter->getTransmissionTypeId();
			$intIsFailed             = $objTransmissionResultsFilter->getIsFailed();
			$intClientId             = $objTransmissionResultsFilter->getCid();
			$strFromDate             = $objTransmissionResultsFilter->getFromDate();
			$strToDate               = $objTransmissionResultsFilter->getToDate();
			$intId                   = $objTransmissionResultsFilter->getId();
		}

		$strWhere = '';
		if( true == valId( $intClientId ) ) {
			$strWhere .= ( ( \Psi\CStringService::singleton()->stripos( $strWhere, 'where' ) == true ) ? ' AND ' : ' WHERE ' ) . 'cid = ' . ( int ) $intClientId;
		}

		if( true == valId( $intTransmissionTypeId ) ) {
			$strWhere .= ( ( \Psi\CStringService::singleton()->stripos( $strWhere, 'where' ) == true ) ? ' AND ' : ' WHERE ' ) . ' transmission_type_id = ' . ( int ) $intTransmissionTypeId;
		}

		if( true == valStr( $strFromDate ) ) {
			$strWhere .= ( ( \Psi\CStringService::singleton()->stripos( $strWhere, 'where' ) == true ) ? ' AND ' : ' WHERE ' ) . ' created_on >= \'' . date( 'Y-m-d', strtotime( $strFromDate ) ) . '\' AND created_on <= \'' . date( 'Y-m-d', strtotime( $strToDate ) ) . ' 23:59:59\'';
		}

		if( true == valId( $intId ) ) {
			$strWhere .= ( ( \Psi\CStringService::singleton()->stripos( $strWhere, 'where' ) == true ) ? ' AND ' : ' WHERE ' ) . 'id = ' . ( int ) $intId;
		}

		if( 1 == valId( $intIsFailed ) ) {
			$strWhere .= ( ( \Psi\CStringService::singleton()->stripos( $strWhere, 'where' ) == true ) ? ' AND ' : ' WHERE ' ) . ' is_failed = ' . ( int ) $intIsFailed;
		}

		if( true == valId( $intTransmissionVendorId ) ) {
			$strWhere .= ' AND transmission_vendor_id = ' . ( int ) $intTransmissionVendorId;
		}

		if( true == valArr( $intArrPropertyIds ) ) {
			$strWhere .= ' AND property_id IN ( ' . implode( ',', $intArrPropertyIds ) . ')';
		}

		$strWhere .= ' ;';

		return self::fetchTransmissionCount( $strWhere, $objDatabase );

	}

	public static function fetchLatestFailedTransmissionByCompanyTransmissionVendorIdByPropertyIdByTransmissionDateTimeByCid( $intCompanyTransmissionVendorId, $intPropertyId, $strTransmissionDateTime, $intCid, $objDatabase ) {
		$strSql = ' SELECT 
                        *
                        FROM transmissions
                        WHERE company_transmission_vendor_id = ' . ( int ) $intCompanyTransmissionVendorId . '
                        AND property_id = ' . ( int ) $intPropertyId . '
                        AND transmission_datetime::DATE = \'' . $strTransmissionDateTime . '\'
                        AND cid = ' . ( int ) $intCid . '
                    ORDER BY id DESC
					LIMIT 1';

		return self::fetchTransmission( $strSql, $objDatabase );
	}

}
?>