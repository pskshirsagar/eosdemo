<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChoreReferenceTypes
 * Do not add any new functions to this class.
 */

class CChoreReferenceTypes extends CBaseChoreReferenceTypes {

	public static function fetchChoreReferenceTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CChoreReferenceType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchChoreReferenceType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CChoreReferenceType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }
}
?>