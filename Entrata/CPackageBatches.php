<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPackageBatches
 * Do not add any new functions to this class.
 */

class CPackageBatches extends CBasePackageBatches {

	public static function loadPackageBatchSearchSqlByCid( $intCid, $objPackageFilter ) {
		$strSearchSql = NULL;

		if( true == valObj( $objPackageFilter, 'CPackageFilter' ) ) {
			if( true == valArr( $objPackageFilter->getFilterPropertyIds() ) ) {
				if( true == $objPackageFilter->getIsActive() ) {
				$strSearchSql .= ' AND ( SELECT count(id) from packages where package_batch_id = pb.id AND cid = pb.cid
									AND cid = ' . ( int ) $intCid . '
									AND package_status_type_id IN (' . CPackageStatusType::PICKEDUP . ', ' . CPackageStatusType::RETURNED_TO_SENDER . ' , ' . CPackageStatusType::DELIVERED . ' )
									AND property_id IN ( ' . implode( ',', $objPackageFilter->getFilterPropertyIds() ) . ' ) ) <> (SELECT count(id) from packages where package_batch_id = pb.id AND cid = pb.cid AND property_id IN( ' . implode( ',', $objPackageFilter->getFilterPropertyIds() ) . ' ) AND cid = ' . ( int ) $intCid . ' )
								  ';
				}
				$strSearchSql .= '  AND p.property_id IN ( ' . implode( ',', $objPackageFilter->getFilterPropertyIds() ) . ' )
									AND p.cid = ' . ( int ) $intCid;
			}
		}

		return $strSearchSql;
	}

	public static function fetchPaginatedPackageBatchesByCidByPackageFilter( $intPageNo, $intPageSize, $intCid, $intCustomerId, $objPackageFilter, $objDatabase ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		$strSql = 'SELECT
						DISTINCT ( pb.id ),
						pb.batch_datetime::TIMESTAMP as batch_datetime,
						pb.name,
						count(p.id) OVER (PARTITION BY pb.id) as total_packages,
						SUM( CASE WHEN p.package_status_type_id = ' . CPackageStatusType::PICKEDUP . ' THEN
								1
							 ELSE
								0
							 END
						) OVER (PARTITION BY pb.id) as total_picked_up,
						SUM( CASE WHEN p.package_status_type_id = ' . CPackageStatusType::DELIVERED . ' THEN
								1
							 ELSE
								0
							 END
						) OVER (PARTITION BY pb.id) as total_delivered,
						SUM( CASE WHEN p.package_status_type_id = ' . CPackageStatusType::RETURNED_TO_SENDER . ' THEN
								1
							 ELSE
								0
							 END
						) OVER (PARTITION BY pb.id) as total_resend,
						SUM( CASE WHEN p.emailed_on IS NULL AND p.package_status_type_id = ' . CPackageStatusType::UNDELIVERED . ' THEN
								1
							 ELSE
								0
							 END
						) OVER (PARTITION BY pb.id) as total_print,
						prt.property_names,
						pkpr.property_count
		 			FROM
						package_batches pb
						LEFT JOIN packages p ON ( p.package_batch_id = pb.id AND p.cid = pb.cid )
						INNER JOIN
							(  SELECT
									pb.id, pb.cid,
									array_to_string( array_agg( DISTINCT ( prt.property_name ) ), \',\') AS property_names
								FROM
									package_batches pb
									LEFT JOIN packages p ON ( p.package_batch_id = pb.id AND p.cid = pb.cid )
									INNER JOIN properties prt ON ( prt.id = p.property_id AND prt.cid = p.cid )
								WHERE
							 		pb.cid = ' . ( int ) $intCid . ' ' . CPackageBatches::loadPackageBatchSearchSqlByCid( $intCid, $objPackageFilter ) . '
								GROUP BY
									pb.id,
							 		pb.cid
							) prt ON ( pb.id = prt.id AND pb.cid = prt.cid )
						INNER JOIN
							(
							  SELECT
									COUNT( prcnt.property_id ) as property_count,
									prcnt.package_batch_id,
									prcnt.cid
							  FROM (
										SELECT
											DISTINCT pk.property_id as property_id,
											pk.cid,
											pk.package_batch_id
										FROM
											packages pk
										WHERE
											pk.cid = ' . ( int ) $intCid . '
									) AS prcnt
								WHERE
								  prcnt.cid = ' . ( int ) $intCid . '
							   GROUP BY
								  prcnt.package_batch_id,
								  prcnt.cid
							 ) pkpr ON ( pb.id = pkpr.package_batch_id AND pkpr.cid = pb.cid )';

		$strSql .= ' WHERE pb.cid = ' . ( int ) $intCid;

		if( false == is_null( $intCustomerId ) && 0 != $intCustomerId ) {
			$strSql .= ' AND p.customer_id =' . ( int ) $intCustomerId;
		}

		if( false == is_null( $objPackageFilter->getLeaseId() ) && 0 != $objPackageFilter->getLeaseId() ) {
			$strSql .= ' AND p.lease_id =' . ( int ) $objPackageFilter->getLeaseId();
		}

		$strSql .= CPackageBatches::loadPackageBatchSearchSqlByCid( $intCid, $objPackageFilter );

		// $strSql.= ' GROUP BY pb.id, pb.batch_datetime, pb.item_count';
		$strSql .= ' ORDER BY pb.batch_datetime::TIMESTAMP DESC';
		$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return self::fetchPackageBatches( $strSql, $objDatabase );
	}

	public static function fetchPackageBatchesCountsByCidByPackageFilter( $intCid, $intCustomerId, $objPackageFilter, $objDatabase ) {

		$strSql = 'SELECT
						count(pb.id) as count
					FROM
						package_batches pb
						LEFT JOIN packages p ON ( p.package_batch_id = pb.id AND p.cid = pb.cid )';

					if( false == is_null( $intCustomerId ) && 0 != $intCustomerId ) {

						$strSql .= ' LEFT JOIN packages p2 ON ( p2.package_batch_id = pb.id AND p2.cid = pb.cid ) ';
					}

		$strSql .= ' WHERE pb.cid = ' . ( int ) $intCid;

		if( false == is_null( $intCustomerId ) && 0 != $intCustomerId ) {
			$strSql .= ' AND p.customer_id =' . ( int ) $intCustomerId;
		}

		if( false == is_null( $objPackageFilter->getLeaseId() ) && 0 != $objPackageFilter->getLeaseId() ) {
			$strSql .= ' AND p.lease_id =' . ( int ) $objPackageFilter->getLeaseId();
		}

		$strSql .= CPackageBatches::loadPackageBatchSearchSqlByCid( $intCid, $objPackageFilter );

		$strSql .= ' GROUP BY pb.id, pb.batch_datetime';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if ( true == isset ( $arrintResponse ) ) return \Psi\Libraries\UtilFunctions\count( $arrintResponse );

		return 0;
	}

	public static function fetchPackageBatchPropertiesByCidByPackageBatchId( $intCid, $intPackageBatchId, $objDatabase ) {

		$strSql = 'SELECT
						pb.name,
						prt.property_name as property_names
					FROM
						package_batches pb
						INNER JOIN packages p ON ( p.package_batch_id = pb.id AND p.cid = pb.cid )
				 		INNER JOIN properties prt ON ( prt.id = p.property_id AND prt.cid = p.cid )
					WHERE
						pb.cid = ' . ( int ) $intCid . '
						AND pb.id = ' . ( int ) $intPackageBatchId . '
					GROUP BY
						pb.name, prt.property_name';

		return self::fetchPackageBatches( $strSql, $objDatabase );
	}

	public static function fetchPackageBatchesCountByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count( distinct pkbt.package_batch_id) as count
					FROM
						(
							SELECT
								p.package_batch_id
							FROM
								packages p
								JOIN package_batches pb ON ( pb.id = p.package_batch_id AND pb.cid = p.cid )
							WHERE
								p.cid = ' . ( int ) $intCid . '
								AND p.property_id = ' . ( int ) $intPropertyId . '
								AND p.deleted_by IS NULL
								AND p.deleted_on IS NULL
								AND p.delivered_on IS NULL
								AND p.delivered_by IS NULL
								AND p.package_status_type_id NOT IN ( ' . CPackageStatusType::PICKEDUP . ', ' . CPackageStatusType::RETURNED_TO_SENDER . ' , ' . CPackageStatusType::DELIVERED . ' ) 
						) pkbt';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActivePackageBatchesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						pb.*
					FROM
						package_batches AS pb
						JOIN packages p on ( p.cid = pb.cid AND p.package_batch_id = pb.id )
					WHERE
						pb.cid = ' . ( int ) $intCid . '
						AND p.property_id = ' . ( int ) $intPropertyId . '
						AND p.deleted_on IS NULL';

		return self::fetchPackageBatches( $strSql, $objDatabase );
	}

}
?>