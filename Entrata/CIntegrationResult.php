<?php

class CIntegrationResult extends CBaseIntegrationResult {

	use TEosStoredObject;

	protected $m_strDataSent;
	protected $m_strDataReceived;
	protected $m_strUserName;
	protected $m_strIntegrationDatabaseName;
	protected $m_strIntegrationResultData;
	protected $m_strReferenceNumbers;
	protected $m_strPropertyNames;
	protected $m_strPropertyIds;

	protected $m_boolStoreResultInOneFile;
	protected $m_boolIsSerializedDataReceived;
	protected $m_boolIsSerializedDataSent;
	protected $m_strTitle;

	protected $m_arrobjIntegrationResultReferences;

	protected $m_objStorageGateway;
	protected $m_strStorageGatewayName;

	public function __construct() {
		parent::__construct();

		$this->m_strDataSent 					= NULL;
		$this->m_strDataReceived 				= NULL;
		$this->m_strIntegrationResultData 		= NULL;
		$this->m_boolStoreResultInOneFile		= false;
		$this->m_boolIsSerializedDataReceived	= false;
		$this->m_boolIsSerializedDataSent		= false;

		return;
	}

	public function setDefaults() {
		$this->m_strResultDatetime = date( 'm/d/Y H:i:s ' );
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrstrValues['data_sent'] ) ) $this->setDataSent( ( $boolStripSlashes ) ? stripslashes( $arrstrValues['data_sent'] ) : $arrstrValues['data_sent'] );
		if( isset( $arrstrValues['data_received'] ) ) $this->setDataReceived( ( $boolStripSlashes ) ? stripslashes( $arrstrValues['data_received'] ) : $arrstrValues['data_received'] );
		if( isset( $arrstrValues['username'] ) ) $this->setUserName( $arrstrValues['username'] );
		if( isset( $arrstrValues['database_name'] ) ) $this->setIntegrationDatabaseName( $arrstrValues['database_name'] );
		if( isset( $arrstrValues['property_names'] ) ) $this->setPropertyNames( $arrstrValues['property_names'] );
		if( isset( $arrstrValues['reference_numbers'] ) ) $this->setReferenceNumbers( $arrstrValues['reference_numbers'] );
		if( isset( $arrstrValues['property_ids'] ) ) $this->setPropertyIds( $arrstrValues['property_ids'] );

		return;
	}

	/**
	 * Set Functions
	 */

	public function setPropertyIds( $strPropertyIds ) {
		$this->m_strPropertyIds = $strPropertyIds;
	}

	public function setPropertyNames( $strPropertyNames ) {
		$this->m_strPropertyNames = $strPropertyNames;
	}

	public function setIntegrationResultReferences( $arrobjIntegrationResultReferences ) {
		$this->m_arrobjIntegrationResultReferences = $arrobjIntegrationResultReferences;
	}

	public function setReferenceNumbers( $strReferenceNumbers ) {
		$this->m_strReferenceNumbers = $strReferenceNumbers;
	}

	public function setStoreResultInOneFile( $boolStoreResultInOneFile ) {
		$this->m_boolStoreResultInOneFile = $boolStoreResultInOneFile;
	}

	public function setIntegrationResultData( $strIntegrationResultData ) {
		$this->m_strIntegrationResultData = $strIntegrationResultData;
	}

	public function setDataSent( $strDataSent ) {
		$this->m_strDataSent = CStrings::strTrimDef( $strDataSent, -1, NULL, true );
	}

	public function setDataReceived( $strDataReceived ) {
		$this->m_strDataReceived = CStrings::strTrimDef( $strDataReceived, -1, NULL, true );
	}

	public function setUserName( $strUserName ) {
		$this->m_strUserName = CStrings::strTrimDef( $strUserName, -1, NULL, true );
	}

	public function setIntegrationDatabaseName( $strIntegrationDatabaseName ) {
		return $this->m_strIntegrationDatabaseName = $strIntegrationDatabaseName;
	}

	public function setEncodedExtraInfo( $arrstrExtraInfo ) {
		$strExtraInfo = NULL;
		if( false != valArr( $arrstrExtraInfo ) ) {
			$arrstrExtraInfo = $this->clearEmptyElementsFromArray( $arrstrExtraInfo );
			if( false != valArr( $arrstrExtraInfo ) ) {
				$strExtraInfo = json_encode( $arrstrExtraInfo );
			}
		}
		parent::setExtraInfo( $strExtraInfo );
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = CStrings::strTrimDef( $strTitle, -1, NULL, true );
	}

	public function setStorageGatewayName( $strStorageGatewayName ) {
		$this->m_strStorageGatewayName = $strStorageGatewayName;
	}

	/**
	 * Get Functions
	 */

	public function getPropertyIds() {
		return $this->m_strPropertyIds;
	}

	public function getPropertyNames() {
		return $this->m_strPropertyNames;
	}

	public function getIntegrationResultReferences() {
		return $this->m_arrobjIntegrationResultReferences;
	}

	public function getReferenceNumbers() {
		return $this->m_strReferenceNumbers;
	}

	public function getStoreResultInOneFile() {
		return $this->m_boolStoreResultInOneFile;
	}

	public function getIntegrationResultData( $objDatabase = NULL ) {
		if( valStr( $this->m_strIntegrationResultData ) ) {
			return $this->m_strIntegrationResultData;
		}

		if( false === $this->loadData( $objDatabase ) ) return false;

		// Is integration result stored in one file?
		if( valStr( $this->m_strIntegrationResultData ) ) {

			$objIntegrationClient		= ( is_null( $this->getServiceId() ) ) ? \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientByIdByCid( $this->getIntegrationClientId(), $this->getCid(), $objDatabase ) : NULL;
			$intIntegrationClientTypeId	= ( valObj( $objIntegrationClient, 'CIntegrationClient' ) ) ? $objIntegrationClient->getIntegrationClientTypeId() : NULL;

			switch( $intIntegrationClientTypeId ) {
				case CIntegrationClientType::JENARK:
				case CIntegrationClientType::YARDI_RPORTAL:
					list( $this->m_strDataSent, $this->m_strDataReceived ) = explode( 'PSI_SEPARATOR', $this->m_strIntegrationResultData );
					$this->m_boolIsSerializedDataSent	= true;
					$this->m_strDataReceived			= CBaseTransport::formatXml( $this->m_strDataReceived );
					break;

				case CIntegrationClientType::AMSI:
				case CIntegrationClientType::REAL_PAGE:
					list( $this->m_strDataSent, $this->m_strDataReceived ) = explode( 'PSI_SEPARATOR', $this->m_strIntegrationResultData );
					$this->m_strDataSent 		= CBaseTransport::formatXml( $this->m_strDataSent );
					$this->m_strDataReceived 	= CBaseTransport::formatXml( $this->m_strDataReceived );
					break;

				default:
					list( $this->m_strDataSent, $this->m_strDataReceived ) = explode( 'PSI_SEPARATOR', $this->m_strIntegrationResultData );
					if( is_null( $this->getServiceId() ) ) {
						$this->m_boolIsSerializedDataSent = true;
						if( $this->unserializeObject( $this->m_strDataReceived ) ) {
							$this->m_boolIsSerializedDataReceived = true;
						}
					}
					break;
			}

		} else {

			if( !$this->unserializeObject( $this->m_strDataSent ) ) {
				$this->m_strDataSent = CFormatText::formatXml( $this->m_strDataSent );
			} else {
				$this->m_boolIsSerializedDataSent = true;
			}

			if( !$this->unserializeObject( $this->m_strDataReceived ) ) {
				$this->m_strDataReceived = CBaseTransport::formatXml( $this->m_strDataReceived );
			} else {
				$this->m_boolIsSerializedDataReceived = true;
			}
		}

		return $this->m_strIntegrationResultData;
	}

	public function getDataSent( $boolReturnInHumanReadableFormat = true ) {
		if( valStr( $this->m_strDataSent ) ) {
			if( $this->m_boolIsSerializedDataSent && $boolReturnInHumanReadableFormat ) {
				return print_r( $this->unserializeObject( $this->m_strDataSent ), true );
			}
			return $this->m_strDataSent;
		}

		return NULL;
	}

	public function getDataReceived( $boolReturnInHumanReadableFormat = true ) {
		if( valStr( $this->m_strDataReceived ) ) {
			if( $this->m_boolIsSerializedDataReceived && $boolReturnInHumanReadableFormat ) {
				return print_r( $this->unserializeObject( $this->m_strDataReceived ), true );
			}
			return $this->m_strDataReceived;
		}

		return NULL;
	}

	public function getUserName() {
		return $this->m_strUserName;
	}

	public function getIntegrationDatabaseName() {
		return $this->m_strIntegrationDatabaseName;
	}

	public function getDecodedExtraInfo() {
		$arrstrExtraInfo = NULL;
		if( !is_null( $this->getExtraInfo() ) ) {
			$arrstrExtraInfo = json_decode( $this->getExtraInfo(), true );
		}
		return $arrstrExtraInfo;
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function getStorageGatewayName() {
		return $this->m_strStorageGatewayName;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchWebMethod( $objDatabase ) {
		return	CWebMethods::fetchWebMethodById( $this->getWebMethodId(), $objDatabase );
	}

	public function fetchService( $objDatabase ) {
		return \Psi\Eos\Entrata\CServices::createService()->fetchServiceById( $this->getServiceId(), $objDatabase );
	}

	public function fetchClient( $objDatabase ) {
		return	CClients::fetchClientById( $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClient( $objDatabase ) {
		return	\Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientByIdByCid( $this->getIntegrationClientId(), $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationService( $objDatabase ) {
		return	CIntegrationServices::fetchIntegrationServiceById( $this->getIntegrationServiceId(), $objDatabase );
	}

	public function fetchProperty( $objDatabase ) {
		if( is_null( $this->getPropertyId() ) ) return NULL;
		return	\Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function buildStoragePath() {
		return getNonBackupMountsPath( $this->getCid(), PATH_MOUNTS_INTEGRATION_RESULTS ) . date( 'Y/m/d', strtotime( $this->getResultDatetime() ) ) . '/';
	}

	public function getValidIntegrationResultFileSize( $objDatabase ) {

		$arrobjStoredObjects = ( array ) \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectsByReferenceTableByReferenceIdByCid( self::TABLE_NAME, $this->getId(), $this->getCid(), $objDatabase );

		foreach( $arrobjStoredObjects as $objStoredObject ) {

			if( stristr( $objStoredObject->getKey(), '_result.txt' ) || stristr( $objStoredObject->getKey(), '_received.txt' ) ) {
				$intFileSizeInBytes = $objStoredObject->getContentLength();
				$intFileSizeInMB 	= ( 0 < $intFileSizeInBytes ) ? number_format( $intFileSizeInBytes / 1048576, 2 ) : $intFileSizeInBytes;

				$boolIsDownloadIntegrationResult = ( 40 <= $intFileSizeInMB ) ? true : false;
				return $boolIsDownloadIntegrationResult;
			}
		}

		//TODO:: remove the below code after four months from the file commit date - SC

		$intFileSizeInMB 				 = 0;
		$strFilePath	 				 = NULL;
		$boolIsDownloadIntegrationResult = false;

		$strPath = $this->buildStoragePath();

		if( CFileIo::fileExists( $strPath . $this->getId() . '_received.txt' ) ) {
			$strFilePath = $strPath . $this->getId() . '_received.txt';
		} elseif( CFileIo::fileExists( $strPath . $this->getId() . '_result.txt' ) ) {
			$strFilePath = $strPath . $this->getId() . '_result.txt';
		}

		if( valStr( $strFilePath ) ) {
			require_once( PATH_LIBRARIES_PSI . 'CFileIo.class.php' );
			$intFileSizeInBytes = CFileIo::getFileSize( $strFilePath );
			$intFileSizeInMB 	= ( 0 < $intFileSizeInBytes ) ? number_format( $intFileSizeInBytes / 1048576, 2 ) : $intFileSizeInBytes;
		}

		// 40 MB is the size of actual file on server//
		$boolIsDownloadIntegrationResult = ( 40 <= $intFileSizeInMB ) ? true : false;
		return $boolIsDownloadIntegrationResult;
	}

	public function unserializeObject( $strSerializedObject ) {

		$strSerializedObject = preg_replace_callback( '!s:(\d+):"(.*?)";!',
			function( $arrstrMatches ) {
				return 's:' . \Psi\CStringService::singleton()->strlen( $arrstrMatches[2] ) . ':"' . $arrstrMatches[2] . '";';
			},
			$strSerializedObject );

		if( unserialize( $strSerializedObject ) ) {
			return unserialize( $strSerializedObject );
		} else {
			return false;
		}
	}

	/**
	 * Validate Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Database Functions
	 */

	public function loadData( $objDatabase ) {
		$arrmixStoredObjects = [];
		$arrstrReferenceKeys = [ '_result.txt', '_sent.txt', '_received.txt', NULL ];

		foreach( $arrstrReferenceKeys as $strReferenceKey ) {
			$arrmixStoredObjects[] = $this->fetchStoredObject( $objDatabase, $strReferenceKey )->createGatewayRequest( [ 'mountSystem' => 'NON_BACKUP_MOUNTS', 'noEncrypt' => true ] );
		}

		if( !valArr( $arrmixStoredObjects ) ) {
			return false;
		}

		$boolIsValid = false;

		foreach( $arrmixStoredObjects as $arrmixStoredObject ) {

			$arrmixGatewayResponse = $this->getObjectStorageGateway()->getObject( $arrmixStoredObject );

			if( $arrmixGatewayResponse->hasErrors() ) {
				continue;
			}

			if( \Psi\CStringService::singleton()->stripos( $arrmixStoredObject['key'], '_result.txt' ) !== false ) {
				$this->m_strIntegrationResultData = $arrmixGatewayResponse['data'];
				$this->setStoreResultInOneFile( true );
				$boolIsValid = true;
			}

			if( \Psi\CStringService::singleton()->stripos( $arrmixStoredObject['key'], '_sent.txt' ) !== false ) {
				$this->m_strDataSent = $arrmixGatewayResponse['data'];
				$boolIsValid         = true;
			}

			if( \Psi\CStringService::singleton()->stripos( $arrmixStoredObject['key'], '_received.txt' ) !== false ) {
				$this->m_strDataReceived = $arrmixGatewayResponse['data'];
				$boolIsValid             = true;
			}
		}

		return $boolIsValid;
	}

	public function storeResultFiles( $intCurrentUserId, $objDatabase ) {

		$intCurrentTime = microtime( true );
		$boolIsValid    = true;

		if( $this->getStoreResultInOneFile() ) {
			$arrmixRequest                   = $this->createPutGatewayRequest( [ 'data' => $this->getIntegrationResultData(), 'noEncrypt' => true, 'mountSystem' => 'NON_BACKUP_MOUNTS', 'storageGateway' => $this->getStorageGatewayName() ], '_result.txt' );
			$objObjectStorageGatewayResponse = $this->getObjectStorageGateway()->putObject( $arrmixRequest );
			$this->setTitle( 'Result File:' . $this->getId() );

			if( !is_null( $objObjectStorageGatewayResponse ) && $objObjectStorageGatewayResponse->isSuccessful() ) {
				$this->setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse )->insertOrUpdateStoredObject( $intCurrentUserId, $objDatabase, NULL, '_result.txt' );
			} else {
				$boolIsValid = false;
			}

		} else {

			$arrmixRequest                   = $this->createPutGatewayRequest( [ 'data' => $this->getDataSent(), 'noEncrypt' => true, 'mountSystem' => 'NON_BACKUP_MOUNTS', 'storageGateway' => $this->getStorageGatewayName() ], '_sent.txt' );
			$objObjectStorageGatewayResponse = $this->getObjectStorageGateway()->putObject( $arrmixRequest );
			$this->setTitle( 'Sent File:' . $this->getId() );

			if( !is_null( $objObjectStorageGatewayResponse ) && $objObjectStorageGatewayResponse->isSuccessful() ) {
				$this->setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse )->insertOrUpdateStoredObject( $intCurrentUserId, $objDatabase, NULL, '_sent.txt' );
			} else {
				$boolIsValid = false;
			}

			if( $boolIsValid ) {
				$arrmixRequest                   = $this->createPutGatewayRequest( [ 'data' => $this->getDataReceived(), 'noEncrypt' => true, 'mountSystem' => 'NON_BACKUP_MOUNTS', 'storageGateway' => $this->getStorageGatewayName() ], '_received.txt' );
				$objObjectStorageGatewayResponse = $this->getObjectStorageGateway()->putObject( $arrmixRequest );
				$this->setTitle( 'Received File:' . $this->getId() );

				if( !is_null( $objObjectStorageGatewayResponse ) && $objObjectStorageGatewayResponse->isSuccessful() ) {
					$this->setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse )->insertOrUpdateStoredObject( $intCurrentUserId, $objDatabase, NULL, '_received.txt' );
				} else {
					$boolIsValid = false;
				}
			}
		}

		if( !$boolIsValid ) {
			$this->setUseAmazonFileStorage( false );
		}

		$fltProcessingTime = microtime( true ) - $intCurrentTime;
		$this->setFileStorageTime( $fltProcessingTime );
		$this->update( $intCurrentUserId, $objDatabase );

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setUseAmazonFileStorage( true );
		$this->setStorageGatewayName( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 );

		if( !parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			return false;
		}

		if( valArr( $this->m_arrobjIntegrationResultReferences ) ) {
			foreach( $this->m_arrobjIntegrationResultReferences as $intKey => $objIntegrationResultReference ) {
				if( !valStr( $objIntegrationResultReference->getReferenceNumber() ) ) {
					unset( $this->m_arrobjIntegrationResultReferences[$intKey] );
					continue;
				}

				$objIntegrationResultReference->setIntegrationResultId( $this->getId() );

				if( !is_null( $this->getPropertyId() ) && is_null( $objIntegrationResultReference->getPropertyId() ) ) {
					$objIntegrationResultReference->setPropertyId( $this->getPropertyId() );
				}

				$this->m_arrobjIntegrationResultReferences[$intKey] = $objIntegrationResultReference;
			}

			if( valArr( $this->m_arrobjIntegrationResultReferences ) && false === \Psi\Eos\Entrata\CIntegrationResultReferences::createService()->bulkInsert( $this->m_arrobjIntegrationResultReferences, $intCurrentUserId, $objDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );
			}
		}

		if( $this->getUseAmazonFileStorage() && $this->storeResultFiles( $intCurrentUserId, $objDatabase ) ) {
			return true;
		}

		$this->setStorageGatewayName( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM );

		// STORE FILE(S) ON NON-BACKUP-MOUNTS
		$this->storeResultFiles( $intCurrentUserId, $objDatabase );

		return true;
	}

	function getObjectStorageGateway() {
		if( !is_null( $this->m_objStorageGateway ) ) {
			return $this->m_objStorageGateway;
		}

		$this->m_objStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_MULTI_SOURCED );
		$this->m_objStorageGateway->initialize();

		return $this->m_objStorageGateway;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function clearEmptyElementsFromArray( $arrmixSourceArray ) {
		if( !valArr( $arrmixSourceArray ) ) {
			return $arrmixSourceArray;
		}
		foreach( $arrmixSourceArray as $strKey => $strValue ) {
			if( false != valArr( $strValue ) ) {
				$arrmixSourceArray[$strKey] = $this->clearEmptyElementsFromArray( $arrmixSourceArray[$strKey] );
			}
			if( false != empty( $arrmixSourceArray[$strKey] ) ) {
				unset( $arrmixSourceArray[$strKey] );
			}
		}
		return $arrmixSourceArray;
	}

	public function deleteIntegrationResultFiles() {
		$strStoragePath				= $this->buildStoragePath() . $this->getId();
		$strInputFilePathResult		= $strStoragePath . '_result.txt';
		$strInputFilePathSent		= $strStoragePath . '_sent.txt';
		$strInputFilePathReceived	= $strStoragePath . '_received.txt';
		$boolIsSuccess				= true;

		if( CFileIo::fileExists( $strInputFilePathResult ) ) {
			if( !CFileIo::deleteFile( $strInputFilePathResult ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'Failed to delete _result.txt file for integration result id : ' . $this->getId() ) );
				$boolIsSuccess = false;
			}
		} else {

			if( CFileIo::fileExists( $strInputFilePathSent ) && !CFileIo::deleteFile( $strInputFilePathSent ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'Failed to delete _sent.txt file for integration result id : ' . $this->getId() ) );
				$boolIsSuccess = false;
			}

			if( CFileIo::fileExists( $strInputFilePathReceived ) && !CFileIo::deleteFile( $strInputFilePathReceived ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'Failed to delete _received.txt file for integration result id : ' . $this->getId() ) );
				$boolIsSuccess = false;
			}
		}

		return $boolIsSuccess;
	}

	public function calcStorageContainer( $strVendor = NULL ) {
		return ( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 == $this->getStorageGatewayName() ) ? CONFIG_OSG_BUCKET_SYSTEM_INTEGRATIONS : 'integration_results';
	}

	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {
		// KEY NEEDS TO BE UNIQUE UNDER THE AWS BUCKET
		$strKey = date( 'Y/m/d/', strtotime( $this->getResultDatetime() ) ) . $this->getId() . $strReferenceTag;

		if( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 == $this->getStorageGatewayName() ) {
			$strKey = $this->getCid() . '/' . $strKey;
		}

		return $strKey;
	}

}
?>