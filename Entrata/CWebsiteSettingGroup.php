<?php

use Psi\Eos\Entrata\CWebsiteSettingGroups;

class CWebsiteSettingGroup extends CBaseWebsiteSettingGroup {

	const TEMPLATE_SPECIFIC_SETTINGS 			= 37;
	const HOMEPAGE_CALL_TO_ACTION 			    = 38;
	const LANDING_TRANSITION_TIME 			    = 39;
	const SLIDER_OVERLAY 			            = 40;
	const CORPORATE_INFORMATION 			    = 41;
	const CORPORATE_SOCIAL_MEDIA 			    = 42;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valModuleId() {
		$boolIsValid = true;
		if( true == is_null( $this->getModuleId() ) || 0 == $this->getModuleId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'module_id', 'Module is required.' ) );
		}
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Website Setting Group name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valNameExistsInSameModule( $strAction, $objDatabase ) {
		$boolIsValid = true;
		$arrobjWebsiteSettingGroups = \Psi\Eos\Entrata\CWebsiteSettingGroups::createService()->fetchWebsiteSettingGroupsByModuleIdByName( $this->getModuleId(), $this->getName(), $objDatabase );

		if( true == valArr( $arrobjWebsiteSettingGroups ) ) {

			if( VALIDATE_INSERT == $strAction ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Website Setting Group already exists.' ) );
				$boolIsValid = false;
			}

			if( VALIDATE_UPDATE == $strAction ) {
				foreach( $arrobjWebsiteSettingGroups as $objWebsiteSettingGroups ) {
					if( $this->getId() != $objWebsiteSettingGroups->getId() ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Website Setting Group already exists.' ) );
						$boolIsValid = false;
						break;
					}
				}
			}
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Website Setting Group description is required.' ) );
		}
		return $boolIsValid;
	}

	public function valToolTipId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valModuleId();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valNameExistsInSameModule( $strAction, $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function updateSequence( $objClientDatabase ) {
		$intNextSeqId = $this->getId() + 1;
		$arrmixData = fetchData( 'SELECT setval( \'public.website_setting_groups_id_seq\', ' . ( int ) $intNextSeqId . ' , FALSE );', $objClientDatabase );

		if( 0 >= $arrmixData[0]['setval'] ) {
			return false;
		}

		return true;
	}

	public function getInsertSql( $intCurrentUserId ) {
		return $this->insert( $intCurrentUserId, NULL, true );
	}

	public function getUpdateSql( $intCurrentUserId ) {
		return $this->update( $intCurrentUserId, NULL, true );
	}

}
?>