<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CDeployments
 * Do not add any new functions to this class.
 */

class CDeployments extends CBaseDeployments {

	public static function fetchPaginatedDeployments( $intPageNo, $intPageSize, $intDeploymentTypeId, $objDatabase, $strField=NULL, $strFieldType=NULL, $strParameters = NULL ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		if( 'id' == $strField ) {
			$strField = 'd.id';
		}

		if( 'deployment_type_id' == $strField ) {
			$strField = 'd.deployment_type_id';
		}

		if( false == empty( $strParameters ) ) {
			$strParameters = ' And parameters = \'' . $strParameters . '\'';
		}

		$strOrderBy = ( false == is_null( $strField ) && false == is_null( $strFieldType ) ) ? ' ORDER BY ' . $strField . ' ' . $strFieldType  : ' ORDER BY d.id DESC';

		$strSql 	= 'SELECT
							DISTINCT( d.* )
						FROM
							deployments d LEFT JOIN database_script_releases db ON ( d.id = db.deployment_id )
						WHERE
							deployment_type_id =  ' . ( int ) $intDeploymentTypeId . $strParameters
							. $strOrderBy . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchDeploymentsByIds( $arrintDeploymentsIds, $objDatabase ) {

		if( false == valArr( $arrintDeploymentsIds ) ) return NULL;

		return self::fetchDeployments( 'SELECT * FROM deployments WHERE id IN ( ' . implode( ',', $arrintDeploymentsIds ) . ' )', $objDatabase );
	}

	public static function fetchNotProcessDeploymentByDeploymentTypeIds( $arrintDeploymentTypeIds,  $objDatabase ) {

		if( false == valArr( $arrintDeploymentTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM deployments WHERE start_datetime IS NULL AND deployment_type_id IN ( ' . implode( ', ', $arrintDeploymentTypeIds ) . ' ) ORDER BY id';
		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchApprovedProcessDeploymentByDeploymentTypeIds( $arrintDeploymentTypeIds,  $objDatabase ) {

		if( false == valArr( $arrintDeploymentTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM deployments WHERE start_datetime IS NULL AND is_critical_push = 1 AND approved_by is NOT NULL AND deployment_type_id IN ( ' . implode( ', ', $arrintDeploymentTypeIds ) . ' ) ORDER BY id';
		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchNotProcessDeploymentByDeploymentTypeIdsByClusterId( $arrintDeploymentTypeIds, $intClusterId, $objDatabase, $boolIsDR ) {

		if( false == valArr( $arrintDeploymentTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM deployments WHERE start_datetime IS NULL AND deployment_type_id IN ( ' . implode( ', ', $arrintDeploymentTypeIds ) . ' ) AND cluster_id = ' . ( int ) $intClusterId . ' AND is_dr = ' . $boolIsDR . ' ORDER BY id LIMIT 1';
		return self::fetchDeployment( $strSql, $objDatabase );
	}

	public static function fetchLatestDeploymentByDeploymentTypeId( $intDeploymentTypeId, $objDatabase ) {
		return self::fetchDeployment( 'SELECT * FROM deployments WHERE deployment_type_id = ' . ( int ) $intDeploymentTypeId . ' AND parent_deployment_id IS NULL ORDER BY id DESC LIMIT 1', $objDatabase );
	}

	public static function fetchLatestDeploymentByDeploymentTypeIdByClusterId( $intDeploymentTypeId, $intClusterId, $objDatabase ) {
		return self::fetchDeployment( 'SELECT * FROM deployments WHERE deployment_type_id = ' . ( int ) $intDeploymentTypeId . ' AND cluster_id = ' . ( int ) $intClusterId . ' AND parent_deployment_id IS NULL ORDER BY id DESC LIMIT 1', $objDatabase );
	}

	public static function fetchPaginatedDeploymentsByDeploymentTypeIds( $intPageNo, $intPageSize, $arrintDeploymentTypeIds, $strField=NULL, $strFieldType=NULL, $intCreatedBy=NULL, $strFromDate=NULL, $strToDate=NULL, $objDatabase ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		if( 'id' == $strField ) {
			$strField = 'd.id';
		}

		if( 'deployment_type_id' == $strField ) {
			$strField = 'd.deployment_type_id';
		}

		$strOrderBy = ( false == is_null( $strField ) && false == is_null( $strFieldType ) ) ? ' ORDER BY ' . $strField . ' ' . $strFieldType  : ' ORDER BY  d.id DESC';
		$strCreatedBy = ( false == is_null( $intCreatedBy ) ) ? ' AND created_by =' . ( int ) $intCreatedBy  : '';

		$strDateCondition = '';

		if( true != is_null( $strFromDate ) ) {
			$strDateCondition	= 'created_on >= \'' . $strFromDate . ' 00:00:00\' AND created_on <= \'' . $strToDate . ' 23:59:59\' AND ';
		}

		$strSql 	= 'SELECT
							 d.*
						FROM
							 deployments d
						WHERE ' . $strDateCondition . 'deployment_type_id IN ( ' . implode( ',', $arrintDeploymentTypeIds ) . ' )' . $strCreatedBy . $strOrderBy . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchPaginatedDeploymentsCountByDeploymentTypeIds( $arrintDeploymentTypeIds, $intCreatedBy=NULL, $strFromDate=NULL, $strToDate=NULL, $objDatabase, $strParameters=NULL ) {
		$strCreatedBy = ( false == is_null( $intCreatedBy ) ) ? ' AND created_by =' . ( int ) $intCreatedBy  : '';
		$strDateCondition = ' ';
		if( false == is_null( $strFromDate ) ) {
			$strDateCondition = 'created_on >= \'' . $strFromDate . ' 00:00:00\' AND created_on <= \'' . $strToDate . ' 23:59:59\' AND ';
		}

		if( false == empty( $strParameters ) ) {
			$strParameters = ' And parameters = \'' . $strParameters . '\'';
		}
		$arrintResponse	= fetchData( 'SELECT count(d.id) FROM deployments d WHERE ' . $strDateCondition . ' deployment_type_id IN ( ' . implode( ',', $arrintDeploymentTypeIds ) . ' )' . $strCreatedBy . $strParameters, $objDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchDeploymentsByUsersByClustersWithinDateRange( $arrintUserIds, $arrintClusterIds, $strFromDate, $strToDate,  $objDatabase ) {

		$strSqlCondition = ( true == valArr( $arrintUserIds ) ) ? ' AND created_by IN ( ' . implode( ',', $arrintUserIds ) . ' )' : '';
		$strSqlCondition .= ( true == valArr( $arrintClusterIds ) ) ? ' AND cluster_id IN ( ' . implode( ',', $arrintClusterIds ) . ' )' : '';

		$strSql = 'SELECT
						count(d.id) as deployment_count,
						created_by,
						CAST ( d.created_on AS DATE ) AS deployment_date
					FROM
						deployments d
					WHERE
						created_on >\'' . $strFromDate . ' 00:00:00\'
						AND created_on <= \' ' . $strToDate . ' 23:59:59\'
						AND deployment_type_id IN ( ' . CDeploymentType::PARTIAL_PUSH . ',' . CDeploymentType::PARTIAL_PUSH_GIT . ' ) ' . $strSqlCondition . '
					GROUP BY
						CAST ( d.created_on AS DATE ),
						created_by
					ORDER BY
						deployment_date';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLatestNotFailedDeploymentByDeploymentTypeIdByClusterId( $intDeploymentTypeId, $intClusterId, $objDatabase, $boolIsGit = false ) {

		$strSql = 'SELECT
					    *
					FROM
					    deployments
					WHERE
					    deployment_type_id = ' . ( int ) $intDeploymentTypeId . '
					    AND cluster_id = ' . ( int ) $intClusterId . '
					    AND ps_product_id IS NULL
					    AND parent_deployment_id IS NULL
					    AND failed_on IS NULL';

		if( true == $boolIsGit ) {
			$strSql .= ' AND exec_command ILIKE \'%git%\'';
		} else {
			$strSql .= ' AND exec_command NOT ILIKE \'%git%\'';
		}

		$strSql .= ' ORDER BY
					        id DESC
						 LIMIT 1';
		return self::fetchDeployment( $strSql, $objDatabase );
	}

	public static function fetchLatestNotFailedDeploymentByDeploymentTypeIdsByClusterIdByPsProductId( $arrintDeploymentTypeId, $intClusterId, $intPsProductId, $objDatabase, $boolIsGit = false ) {

		$strSql = 'SELECT
					    *
					FROM
					    deployments
					WHERE
					    deployment_type_id IN ( ' . implode( ',', $arrintDeploymentTypeId ) . ' )' . '
					    AND cluster_id = ' . ( int ) $intClusterId . '
					    AND ps_product_id = ' . ( int ) $intPsProductId . '
					    AND parent_deployment_id IS NULL
					    AND failed_on IS NULL';

		if( true == $boolIsGit ) {
			$strSql .= ' AND exec_command ILIKE \'%git%\'';
		} else {
			$strSql .= ' AND exec_command NOT ILIKE \'%git%\'';
		}

		$strSql .= ' ORDER BY
					        id DESC
						 LIMIT 1';
		return self::fetchDeployment( $strSql, $objDatabase );
	}

	public static function fetchPaginatedSqlDeployments( $arrintDeploymentTypeIds, $intPageNo, $intPageSize, $objDatabase, $arrstrSerchCrietria, $strOrderByField, $strOrderByType ) {
		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		$strWhere				= NULL;

		if( ( false == isset( $arrstrSerchCrietria['is_show_all'] ) && ( false == isset( $arrstrSerchCrietria['bool_is_extended_search'] ) ) ) || ( false == $arrstrSerchCrietria['is_show_all'] && ( true == isset( $arrstrSerchCrietria['bool_is_extended_search'] ) && false == $arrstrSerchCrietria['bool_is_extended_search'] ) ) ) {
			$strWhere = $strWhere . ' AND end_datetime is NULL AND ( is_critical_push is null OR ( is_critical_push = 1 AND approved_by is NULL ) )';
		}

		if( true == isset( $arrstrSerchCrietria['user_id'] ) ) {
			$strWhere = $strWhere . ' AND created_by = ' . $arrstrSerchCrietria['user_id'];
		}

		if( false != $arrstrSerchCrietria ) {
			if( true == isset( $arrstrSerchCrietria['cluster_id'] ) && 0 < strlen( $arrstrSerchCrietria['cluster_id'] ) ) {
				$strWhere = $strWhere . ' AND cluster_id = ' . ( int ) $arrstrSerchCrietria['cluster_id'];
			}

			if( true == isset( $arrstrSerchCrietria['deployment_type_id'] ) && 0 < strlen( $arrstrSerchCrietria['deployment_type_id'] ) ) {
				$strWhere = $strWhere . ' AND deployment_type_id = ' . ( int ) $arrstrSerchCrietria['deployment_type_id'];
			}

			if( true == isset( $arrstrSerchCrietria['from_date'] ) && 0 < strlen( $arrstrSerchCrietria['from_date'] ) ) {
				$strWhere = $strWhere . ' AND to_char( created_on, \'YYYY-MM-DD\' ) >= to_char(\'' . date( 'Y-m-d', strtotime( $arrstrSerchCrietria['from_date'] ) ) . '\'::DATE, \'YYYY-MM-DD\' )
							AND to_char( created_on, \'YYYY-MM-DD\' ) <= to_char(\'' . date( 'Y-m-d', strtotime( $arrstrSerchCrietria['to_date'] ) ) . '\'::DATE, \'YYYY-MM-DD\' )';
			}

			if( true == isset( $arrstrSerchCrietria['database_ids'] ) && true == valArr( $arrstrSerchCrietria['database_ids'] ) ) {
				$strWhere = $strWhere . ' AND database_ids = \'' . implode( ',',  $arrstrSerchCrietria['database_ids'] ) . '\'';
			}
		} else {
			$strWhere = $strWhere . ' AND deployment_type_id IN ( ' . implode( ',', array( CDeploymentType::MANUAL_SQL_EXECUTION, CDeploymentType::MANUAL_SQL_EXECUTION_SCHEMA_CHANGES, CDeploymentType::PROCEDURE_EXECUTION, CDeploymentType::VIEWS_EXECUTION  ) ) . ') ';
		}

		$strSql 	= 'SELECT *
						FROM
							deployments
						WHERE
							svn_file_id is NOT NULL
							AND deployment_type_id IN ( ' . implode( ', ', $arrintDeploymentTypeIds ) . ' )' .
							$strWhere . '
						ORDER BY ' . $strOrderByField . ' ' . $strOrderByType . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchDeploymentsByClusterIdBySvnFileIdsByDeploymentTypeIds( $intClusterId, $arrstrSvnFileIds, $arrintDeploymentTypeIds, $objDatabase ) {

		$strSql = 'SELECT
						d.id,
						d.database_ids,
						d.created_by,
						d.svn_file_id
					FROM
						deployments d
					WHERE
						d.database_ids is NOT NULL
						AND d.svn_file_id is NOT NULL
						AND d.cluster_id = ' . ( int ) $intClusterId . '
						AND deployment_type_id IN (' . implode( ',', $arrintDeploymentTypeIds ) . ' )
						AND d.svn_file_id IN (' . implode( ',', $arrstrSvnFileIds ) . ' )';

		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchPaginatedDeploymentsByDeploymentTypeIdsByClusterId( $intPageNo, $intPageSize, $arrintDeploymentTypeIds, $intClusterId, $strField=NULL, $strFieldType=NULL, $intCreatedBy=NULL, $strFromDate=NULL, $strToDate=NULL, $objDatabase ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		if( 'id' == $strField ) {
			$strField = 'd.id';
		}

		if( 'deployment_type_id' == $strField ) {
			$strField = 'd.deployment_type_id';
		}

		$strOrderBy = ( false == is_null( $strField ) && false == is_null( $strFieldType ) ) ? ' ORDER BY ' . $strField . ' ' . $strFieldType  : ' ORDER BY  d.id DESC';
		$strCreatedBy = ( false == is_null( $intCreatedBy ) ) ? ' AND created_by =' . ( int ) $intCreatedBy  : '';

		$strDateCondition = '';

		if( true != is_null( $strFromDate ) ) {
			$strDateCondition	= 'created_on >= \'' . $strFromDate . ' 00:00:00\' AND created_on <= \'' . $strToDate . ' 23:59:59\' AND ';
		}

		$strSql 	= 'SELECT
							 d.*
						FROM
							 deployments d
						WHERE cluster_id = ' . ( int ) $intClusterId . ' AND ' . $strDateCondition . 'deployment_type_id IN ( ' . implode( ',', $arrintDeploymentTypeIds ) . ' )' . $strCreatedBy . $strOrderBy . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchPaginatedDeploymentsCountByDeploymentTypeIdsByClusterId( $arrintDeploymentTypeIds, $intClusterId, $intCreatedBy=NULL, $strFromDate=NULL, $strToDate=NULL, $objDatabase ) {
		$strCreatedBy = ( false == is_null( $intCreatedBy ) ) ? ' AND created_by =' . ( int ) $intCreatedBy  : '';
		$strDateCondition = ' ';
		if( false == is_null( $strFromDate ) ) {
			$strDateCondition = 'created_on >= \'' . $strFromDate . ' 00:00:00\' AND created_on <= \'' . $strToDate . ' 23:59:59\' AND ';
		}

		$arrintResponse	= fetchData( 'SELECT count(d.id) FROM deployments d WHERE cluster_id = ' . ( int ) $intClusterId . ' AND ' . $strDateCondition . ' deployment_type_id IN ( ' . implode( ',', $arrintDeploymentTypeIds ) . ' )' . $strCreatedBy, $objDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchDeploymentsByDeploymentTypeIdsBetweenDates( $intDeploymentTypeId, $strStartDate, $strEndDate, $objDatabase ) {

		return self::fetchDeployments( 'SELECT
											id,
											start_datetime,
											end_datetime,
											Description
										 FROM
											public.deployments
										 WHERE
											end_datetime::date <=\'' . $strEndDate . '\'::date
											AND start_datetime::date >= \'' . $strStartDate . '\'::date
											AND deployment_type_id = ' . ( int ) $intDeploymentTypeId . '
										 ORDER BY
											start_datetime ASC ', $objDatabase );
	}

	public static function fetchPaginatedSqlDeploymentsCount( $arrintDeploymentTypeIds, $objDatabase, $arrstrSerchCrietria ) {

		$strClusterId 			= NULL;
		$strDeploymentTypeId	= NULL;
		$strDateRange 			= NULL;
		$strDatabaseId 			= NULL;
		$strWhere				= NULL;

		if( ( false == isset( $arrstrSerchCrietria['is_show_all'] ) && ( false == isset( $arrstrSerchCrietria['bool_is_extended_search'] ) ) ) || ( false == $arrstrSerchCrietria['is_show_all'] && ( true == isset( $arrstrSerchCrietria['bool_is_extended_search'] ) && false == $arrstrSerchCrietria['bool_is_extended_search'] ) ) ) {
			$strWhere = ' AND end_datetime is NULL AND deployment_type_id IN ( ' . implode( ', ', $arrintDeploymentTypeIds ) . '  ) AND ( is_critical_push is null OR ( is_critical_push = 1 AND approved_by is NULL ) )';
		}

		if( true == isset( $arrstrSerchCrietria['user_id'] ) ) {
			$strWhere = $strWhere . ' AND created_by = ' . $arrstrSerchCrietria['user_id'];
		}

		if( false != $arrstrSerchCrietria ) {
			if( true == isset( $arrstrSerchCrietria['cluster_id'] ) && 0 < strlen( $arrstrSerchCrietria['cluster_id'] ) ) {
				$strClusterId = ' AND cluster_id = ' . ( int ) $arrstrSerchCrietria['cluster_id'];
			}

			if( true == isset( $arrstrSerchCrietria['deployment_type_id'] ) && 0 < strlen( $arrstrSerchCrietria['deployment_type_id'] ) ) {
				$strDeploymentTypeId = ' AND deployment_type_id = ' . ( int ) $arrstrSerchCrietria['deployment_type_id'];
			}

			if( true == isset( $arrstrSerchCrietria['from_date'] ) && 0 < strlen( $arrstrSerchCrietria['from_date'] ) ) {
				$strDateRange = ' AND to_char( created_on, \'YYYY-MM-DD\' ) >= to_char(\'' . date( 'Y-m-d', strtotime( $arrstrSerchCrietria['from_date'] ) ) . '\'::DATE, \'YYYY-MM-DD\' )
							AND to_char( created_on, \'YYYY-MM-DD\' ) <= to_char(\'' . date( 'Y-m-d', strtotime( $arrstrSerchCrietria['to_date'] ) ) . '\'::DATE, \'YYYY-MM-DD\' )';
			}

			if( true == isset( $arrstrSerchCrietria['database_ids'] ) && true == valArr( $arrstrSerchCrietria['database_ids'] ) ) {
				$strDatabaseId = ' AND database_ids = \'' . implode( ',',  $arrstrSerchCrietria['database_ids'] ) . '\'';
			}
		} else {
			$strDeploymentTypeId = ' AND deployment_type_id IN ( ' . implode( ',', array( CDeploymentType::MANUAL_SQL_EXECUTION, CDeploymentType::MANUAL_SQL_EXECUTION_SCHEMA_CHANGES, CDeploymentType::PROCEDURE_EXECUTION, CDeploymentType::VIEWS_EXECUTION  ) ) . ') ';
		}

		$strSql 	= 'SELECT count(*) as count
						FROM
							deployments
						WHERE
							database_ids is NOT NULL
							AND svn_file_id is NOT NULL ' .
							$strClusterId . $strDeploymentTypeId . $strDatabaseId . $strDateRange .
							$strWhere;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchPaginatedDeploymentsCountByDeploymentTypeId( $intDeploymentTypeId, $objDatabase ) {

		$arrintResponse	= fetchData( 'SELECT count( DISTINCT( d.id ) ) FROM deployments d LEFT JOIN database_script_releases db ON ( d.id = db.deployment_id ) WHERE d.deployment_type_id =  ' . ( int ) $intDeploymentTypeId, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function addClearDataCacheDeployment( $intUserId, $intClusterId, $arrstrCacheKeys, $objDatabase ) {

		if( false == valArr( $arrstrCacheKeys ) ) {
			return false;
		}

		$strExecCommand 	= CDeploymentType::CLEAR_DATA_CACHE_COMMAND;
		$objDeployment 		= new CDeployment();

		foreach( $arrstrCacheKeys as $strCacheKey ) {
			$strExecCommand .= ' ' . sha1( $strCacheKey );
		}

		$objDeployment->setApprovedBy( NULL );
		$objDeployment->setDeploymentTypeId( CDeploymentType::CLEAR_CACHE );
		$objDeployment->setClusterId( $intClusterId );
		$objDeployment->setExecCommand( $strExecCommand );
		$objDeployment->setDescription( 'Clear specific data cache.' );

		switch( NULL ) {
			default:

				$boolIsValid	= true;
				$boolIsValid	&= $objDeployment->validate( 'VALIDATE_INSERT_DEPLOYMENT_ID' );

				if( false == $boolIsValid ) {
					break;
				}

				if( false == $objDeployment->insert( $intUserId, $objDatabase ) ) {
					break;
				}

				return true;
		}

		return false;
	}

	public static function fetchCustomDeploymentById( $intId, $objDatabase ) {

		if( false == is_numeric( $intId ) ) return NULL;

		$strSql = '	SELECT
						d.id,
						d.task_ids,
						d.created_by,
						d.cluster_id,
						d.deployment_type_id,
						dd.push_revision_id,
						dd.cluster_repository_id,
						cr.name AS repository_name
					FROM
						deployments d
						JOIN deployment_details dd ON ( dd.deployment_id = d.id )
						JOIN cluster_repositories cr ON ( cr.id = dd.cluster_repository_id )
					WHERE
						d.id = ' . ( int ) $intId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDeploymentsByDeploymentTypeIdBetweenDatesAndTime( $intDeploymentTypeId, $strStartDate, $strEndDate, $intDeploymentId, $objDatabase ) {

		if( true == empty( $strStartDate ) && true == empty( $strEndDate ) ) return NULL;

		$strWhere = '';
		if( false == empty( $intDeploymentId ) ) {
			$strWhere = ' id <> ' . ( int ) $intDeploymentId . ' AND ';
		}

				$strSql = 'SELECT
								id,
								start_datetime,
								end_datetime,
								Description
							FROM
								public.deployments
							WHERE
							' . $strWhere . '(
							( start_datetime::timestamp
							BETWEEN \'' . $strStartDate . '\'::timestamp
							AND \'' . $strEndDate . '\'::timestamp )
							OR ( end_datetime::timestamp
							BETWEEN \'' . $strStartDate . '\'::timestamp
							AND \'' . $strEndDate . '\'::timestamp ) )
							UNION
 							SELECT
								id,
								start_datetime,
								end_datetime,
								Description
							FROM public.deployments
							WHERE' . $strWhere . ' (
								( start_datetime::timestamp <= \'' . $strStartDate . '\'::timestamp
							AND
								end_datetime::timestamp >= \'' . $strEndDate . '\'::timestamp ) )
							AND
								deployment_type_id = ' . ( int ) $intDeploymentTypeId;

		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchDeploymentsByDeploymentTypeIdByDateAndTime( $intDeploymentTypeId, $strStartDate, $strEndDate, $objDatabase, $boolOrderByStartDateDesc = false ) {

		$strOrderByCondition = '';

		if( true == $boolOrderByStartDateDesc ) {
			$strOrderByCondition = ' ORDER BY start_datetime ASC';
		}

		$strSql = 'SELECT
						*
				 	FROM
						deployments
					WHERE
						( start_datetime::timestamp
						BETWEEN \'' . $strStartDate . '\'::timestamp
						AND \'' . $strEndDate . '\'::timestamp)
						AND deployment_type_id = ' . ( int ) $intDeploymentTypeId . $strOrderByCondition;

		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchNotEndedDeploymentsByDeploymentTypeIdByDeploymentIds( $intDeploymentTypeId, $arrintDeploymentIds = NULL, $objDatabase ) {

		if( true == empty( $intDeploymentTypeId ) ) return NULL;
		$strWhere = ( false == empty( $arrintDeploymentIds ) ) ? ' And id IN ( ' . implode( ',', $arrintDeploymentIds ) . ' )' : '';
		$strSql = 'SELECT * FROM deployments WHERE end_datetime IS NULL AND deployment_type_id =' . ( int ) $intDeploymentTypeId . $strWhere;
		return self::fetchDeployments( $strSql, $objDatabase );

	}

	public static function fetchDeploymentsByDeploymentTypeIdByStartDatetime( $intDeploymentTypeId, $strStartDate, $objDatabase ) {

		$strSql = 'SELECT * FROM deployments WHERE start_datetime IS ' . $strStartDate . ' AND deployment_type_id = ' . ( int ) $intDeploymentTypeId;

		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchDeploymentsForAllBetweenDatesByClusterIdAndDeploymentTypeId( $intClusterId, $strStartDate, $strEndDate, $intDeploymentTypeId, $objDatabase, $arrintDatabaseIds = array() ) {

		$strCommandCheck = '';

		if( NULL != $arrintDatabaseIds && true == valArr( $arrintDatabaseIds ) )
			$strCommandCheck = ' AND exec_command SIMILAR TO \'database_maintenance%(' . implode( '|', $arrintDatabaseIds ) . ')%\'';

		$strSql = '	SELECT
						*
					FROM
						deployments
					WHERE
						deployment_type_id = ' . ( int ) $intDeploymentTypeId . '
						AND to_char( start_datetime, \'YYYY-MM-DD\' ) >= to_char( \'' . $strStartDate . '\'::DATE, \'YYYY-MM-DD\' )
						AND to_char( end_datetime, \'YYYY-MM-DD\' ) <= to_char( \'' . $strEndDate . '\'::DATE, \'YYYY-MM-DD\' )
						AND cluster_id = ' . ( int ) $intClusterId . $strCommandCheck . '
					ORDER BY
						id';

		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchNotApprovedProcessDeploymentByDeploymentTypeIds( $arrintDeploymentTypeIds, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						deployments
					WHERE
						is_critical_push = 1 AND approved_by is NULL AND deployment_type_id IN ( ' . implode( ', ', $arrintDeploymentTypeIds ) . ' ) ';

		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function addLessVarCacheDeployment( $intUserId, $intClusterId, $arrstrTemplates, $objDatabase ) {

		if( false == valArr( $arrstrTemplates ) ) {
			return false;
		}

		$objDeployment 		= new CDeployment();

		foreach( $arrstrTemplates as $arrstrTemplate ) {

			if( 'less_cache' == $arrstrTemplate ) {
				$strExecCommand = 'clear_less_cache [[CLUSTER_ID]] ' . $arrstrTemplate . ' [[DEPLOYMENTID]]';
			} else {
				$strExecCommand = 'clear_less_cache [[CLUSTER_ID]] template_' . $arrstrTemplate . ' [[DEPLOYMENTID]]';
			}
		}
		$objDeployment->setApprovedBy( NULL );
		$objDeployment->setDeploymentTypeId( CDeploymentType::CLEAR_LESS_CACHE );
		$objDeployment->setClusterId( $intClusterId );
		$objDeployment->setExecCommand( $strExecCommand );
		$objDeployment->setDescription( 'Clear specific Less Var cache.' );

		switch( NULL ) {
			default:

				if( false == $objDeployment->validate( 'VALIDATE_INSERT_DEPLOYMENT_ID' ) ) {
					break;
				}

				if( false == $objDeployment->insert( $intUserId, $objDatabase ) ) {
					break;
				}

				return true;
		}

		return false;
	}

	public static function addCdnCacheDeployment( $intUserId, $strMediaPath, $objDatabase ) {

		$objDeployment 		= new CDeployment();

		$objDeployment->setDeploymentTypeId( CDeploymentType::CLEAR_CDN_CACHE );
		$objDeployment->setExecCommand( $strMediaPath );
		$objDeployment->setDescription( 'Clear Cdn cache.' );
		switch( NULL ) {
			default:

				if( false == $objDeployment->validate( 'VALIDATE_INSERT_DEPLOYMENT_ID' ) ) {
					break;
				}

				if( false == $objDeployment->insert( $intUserId, $objDatabase ) ) {
					break;
				}

				return true;
		}

		return false;
	}

	public static function fetchDeploymentCountByParameters( $arrstrParameters, $objDatabase ) {

		if( false == valArr( $arrstrParameters ) ) return false;

		 $strSql = ' SELECT
						count(id)
				    FROM
						deployments
					WHERE
						parameters = \'' . implode( ',', $arrstrParameters ) . '\'';

		$arrintResponse = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchLatestUnCompletedDeploymentByScriptId( $strScriptId, $objDatabase ) {
		$strSql = 'SELECT
					    id,
					    end_datetime
					FROM
					    deployments
					WHERE
					    parameters= \'' . $strScriptId . '\'
					    AND end_datetime IS NULL
					ORDER BY
					    id DESC
					LIMIT
					    1;';

		return self::fetchDeployment( $strSql, $objDatabase );
	}

	public static function fetchLatestDeploymentByDeploymentTypeIdByParameters( $intDeploymentTypeId, $strParameters, $objDatabase ) {
		$strSql = '
			SELECT *
			FROM deployments
			WHERE
				deployment_type_id = ' . ( int ) $intDeploymentTypeId . '
				AND parameters = ' . pg_escape_literal( $strParameters ) . '
			ORDER BY id DESC
			LIMIT 1
		';

		return self::fetchDeployment( $strSql, $objDatabase );
	}

	public static function fetchNotProcessManualSqlDeploymentByDeploymentTypeIds( $arrintDeploymentTypeIds, $objDatabase ) {

		if( false == valArr( $arrintDeploymentTypeIds ) ) return false;

		$strSql = 'SELECT * FROM deployments WHERE start_datetime IS NULL AND deployment_type_id IN (' . implode( ',', $arrintDeploymentTypeIds ) . ') AND is_auto_execute = 1 ORDER BY id';
		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchNotProcessManualSqlDeploymentByDeploymentTypeIdsOrderBySvnFileOrderNum( $arrintDeploymentTypeIds, $objDatabase ) {

		if( false == valArr( $arrintDeploymentTypeIds ) ) return false;

		$strSql = 'SELECT d.* 
					FROM deployments d
					JOIN svn_files sf ON ( d.svn_file_id = sf.id )
					WHERE 
						start_datetime IS NULL 
						AND d.deployment_type_id IN (' . implode( ',', $arrintDeploymentTypeIds ) . ') 
						AND d.is_auto_execute = 1 
					ORDER BY d.id,sf.task_id,sf.order_num';
		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchDeploymentsBySvnFileIdsByDeploymentTypeIds( $arrintSvnFileIds, $arrintDeploymentTypeIds, $strWhere, $objDatabase ) {

		$strSql = 'SELECT
						d.id,
						d.database_ids,
						d.created_by,
						d.svn_file_id
					FROM
						deployments d
					WHERE
						d.database_ids is NOT NULL
						AND d.svn_file_id is NOT NULL
						AND d.deployment_type_id IN (' . implode( ',', $arrintDeploymentTypeIds ) . ')' . $strWhere . '
						AND d.svn_file_id IN (' . implode( ',', $arrintSvnFileIds ) . ')';

		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchNotFailedOnScriptsDeployments( $intDeploymentTypeId, $objDatabase, $strParameters = NULL ) {

		if( false == empty( $strParameters ) ) {
			$strParameters = ' And parameters = \'' . $strParameters . '\'';
		}

		$strSql 	= 'SELECT
							DISTINCT( d.* )
						FROM
							deployments d LEFT JOIN database_script_releases db ON ( d.id = db.deployment_id )
						WHERE
							deployment_type_id =  ' . ( int ) $intDeploymentTypeId . $strParameters . '
							AND
								d.failed_on IS NULL
							AND
								d.end_datetime IS NULL';

		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchDeploymentByDeploymentTypeByClusterIdByReleaseDate( $intDeploymentTypeId, $intClusterId, $strReleaseDateTime, $objDatabase ) {

		$strSql 	= 'SELECT
							*
						FROM
							deployments d
						WHERE
							deployment_type_id =  ' . ( int ) $intDeploymentTypeId . '
							AND
								d.cluster_id =  ' . ( int ) $intClusterId . '
							AND
								d.scheduled_datetime =  \'' . $strReleaseDateTime . '\'
						ORDER BY
					        id DESC
						LIMIT
					        1';
		return self::fetchDeployment( $strSql, $objDatabase );
	}

	public static function fetchDeploymentsByUsersByClustersByDate( $arrintUserIds, $arrintClusterIds, $strFromDate, $strToDate,  $objDatabase ) {

		$strSqlCondition = ( true == valArr( $arrintUserIds ) ) ? ' AND d.created_by IN ( ' . implode( ',', $arrintUserIds ) . ' )' : '';
		$strSqlCondition .= ( true == valArr( $arrintClusterIds ) ) ? ' AND cluster_id IN ( ' . implode( ',', $arrintClusterIds ) . ' )' : '';

		$strSql = 'SELECT 
						d.*,
						dt.name as deployment_type,
						sr.name as tag
					FROM 
						deployments d
				    JOIN
				        deployment_types dt ON dt.id = d.deployment_type_id
				    JOIN
				        svn_repositories sr ON sr.deployment_id = d.id
					WHERE
						d.created_on >\'' . $strFromDate . ' 00:00:00\'
						AND d.created_on <= \'' . $strToDate . ' 23:59:59\' ' . $strSqlCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDeploymentsBetweenPreviousAndNextStandardReleaseByDeploymentTypeIds( $strPreviousStandardRelease, $strNextStandardRelease, $arrintDeploymentTypeIds, $objDatabase ) {

		if( false == valArr( $arrintDeploymentTypeIds ) ) {
			return false;
		}

		$strSql = 'SELECT 
						d.*
					FROM
						deployments d 
						JOIN database_script_releases dsr ON( d.id = dsr.deployment_id )
						JOIN database_script_batches dsb ON( dsr.database_script_batch_id = dsb.id )
					WHERE
						svn_file_id is NOT NULL
						AND deployment_type_id IN (' . implode( ',', $arrintDeploymentTypeIds ) . ')
		                AND cluster_id = ' . CCluster::RAPID . ' 
						AND is_run_on_all_clusters = 0
						AND dsb.database_type_id = ' . CDatabaseType::CLIENT . '
						AND deployment_datetime between ' . '\'' . $strPreviousStandardRelease . '\'' . ' AND ' . '\'' . $strNextStandardRelease . '\'' . '
						AND d.end_datetime IS NOT NULL
						AND d.completed_on IS NOT NULL
		            ORDER BY d.id DESC';

		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchDeploymentsByDeploymentTypeIds( $arrintDeploymentTypeIds, $strField=NULL, $strFieldType=NULL, $strFromDate=NULL, $strToDate=NULL, $objDatabase ) {

		if( 'id' == $strField ) {
			$strField = 'd.id';
		}

		if( 'deployment_type_id' == $strField ) {
			$strField = 'd.deployment_type_id';
		}

		$strOrderBy = ( false == is_null( $strField ) && false == is_null( $strFieldType ) ) ? ' ORDER BY ' . $strField . ' ' . $strFieldType  : ' ORDER BY d.id DESC';

		$strDateCondition = '';

		if( true != is_null( $strFromDate ) ) {
			$strDateCondition	= 'created_on >= \'' . $strFromDate . ' 00:00:00\' AND created_on <= \'' . $strToDate . ' 23:59:59\' AND ';
		}

		if( true != is_null( $strFromDate ) ) {
			$strDateCondition	= 'created_on >= \'' . $strFromDate . ' 00:00:00\' AND created_on <= \'' . $strToDate . ' 23:59:59\' AND ';
		}

		$strSql 	= 'SELECT
							 d.*
						FROM
							 deployments d
						WHERE ' . $strDateCondition . 'deployment_type_id IN ( ' . implode( ',', $arrintDeploymentTypeIds ) . ' )' . $strOrderBy;

		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchDeploymentsByExecCommandByClusterId( $strExecCommand, $intClusterId, $objDatabase ) {
		$strSql = 'SELECT 
						* 
					FROM
						Deployments
					WHERE
						exec_command SIMILAR TO \'database_maintenance%' . $strExecCommand . ' %\'
						AND 
						cluster_id = ' . ( int ) $intClusterId . '
					ORDER BY
						id DESC
					LIMIT
						1';
		return self::fetchDeployment( $strSql, $objDatabase );
	}

	public static function fetchUnProcessedGlobalDeploymentsByClusterIdByDeploymentTypeIds( $intClusterId, $arrintDeploymentTypeIds, $objDatabase, $boolIsSyncingSql = false ) {

		if( false == valArr( $arrintDeploymentTypeIds ) ) {
			return false;
		}

		$strSyncSqlCondition = ( true == $boolIsSyncingSql ) ? ' AND details::text LIKE \'%is_sync%\' ' : ' AND details IS NULL ';

		$strSql = 'SELECT * FROM deployments WHERE start_datetime IS NULL AND cluster_id = ' . ( int ) $intClusterId . $strSyncSqlCondition . ' and ( ( description IS NOT NULL and deployment_type_id IN ( ' . implode( ',', $arrintDeploymentTypeIds ) . ' ) AND parent_deployment_id IS NOT NULL ) OR ( deployment_type_id IN ( ' . CDeploymentType::MOUNT_TEST_CLIENTS_MAPPING . ', ' . CDeploymentType::CLEAR_LESS_CACHE . ' ) ) ) ORDER BY id LIMIT 1';

		return self::fetchDeployment( $strSql, $objDatabase );
	}

	public static function fetchDeploymentsByDeploymentTypeIdByScheduleDateTime( $arrintDeploymentTypeIds, $strScheduledDateTime, $intClusterId, $objDatabase ) {
		$strCondition = ( CCluster::RAPID == $intClusterId ) ? ' AND ( d.cluster_id = ' . ( int ) $intClusterId . ' OR d.cluster_id = ' . ( int ) CCluster::HA . ' )' : ' AND d.cluster_id = ' . ( int ) $intClusterId;
		$strSql 	= 'SELECT
							*
						FROM
							deployments d
						WHERE
							deployment_type_id IN ( ' . implode( ',', $arrintDeploymentTypeIds ) . ' )
							AND d.scheduled_datetime::date =  \'' . $strScheduledDateTime . '\'
							' . $strCondition . '
							AND ( d.failed_on IS NULL OR( d.failed_on IS NOT NULL AND d.end_datetime IS NOT NULL AND d.completed_on IS NOT NULL  ) )
						ORDER BY
							id DESC';

		return self::fetchDeployments( $strSql, $objDatabase );
	}

	public static function fetchDeploymentStartAndEndTimeByParentDeploymentId( $intParentDeploymentId, $objDeployDatabase ) {

		$strSql = '( SELECT
					    id, start_datetime, end_datetime
					  FROM deployments
					  WHERE parent_deployment_id IS NOT NULL AND
					        parent_deployment_id = ' . ( int ) $intParentDeploymentId . '
					  ORDER BY ID ASC
					  LIMIT 1 )
					UNION ALL
					( SELECT
					   id, start_datetime, end_datetime
					 FROM deployments
					 WHERE parent_deployment_id IS NOT NULL AND
					       parent_deployment_id = ' . ( int ) $intParentDeploymentId . '
					 ORDER BY ID DESC
					 LIMIT 1 )';

		return fetchData( $strSql, $objDeployDatabase );
	}

	public static function fetchUnprocessedDeploymentsByDeploymentTypeIdsByTime( $strDeploymentTypeIds, $intTimeDelayInMins, $objDatabase, $strDetails = NULL ) {

		$strDetailsCondition = ( false == is_null( $strDetails ) ) ? ' AND details::text like \'' . $strDetails . '\'' : '';

		$strSql = 'SELECT 
						* 
					FROM 
						deployments 
					WHERE 
					created_by = ' . CUser::ID_SYSTEM . ' 
					AND start_datetime IS NULL 
					AND end_datetime IS NULL 
					AND completed_on IS NULL  
					AND deployment_type_id IN ( ' . $strDeploymentTypeIds . ' )
					AND EXTRACT( MINUTES FROM AGE( CURRENT_TIMESTAMP , created_on ) ) >= ' . ( int ) $intTimeDelayInMins . $strDetailsCondition . '
					ORDER BY id ASC;';

		return self::fetchDeployments( $strSql, $objDatabase );

	}

	public static function fetchLatestNotFailedDeploymentByDeploymentTypeIdsByClusterId( $arrintDeploymentTypeId, $intClusterId, $objDatabase, $boolIsGit = false ) {

		$strSql = 'SELECT
					    *
					FROM
					    deployments
					WHERE
					     deployment_type_id IN ( ' . implode( ',', $arrintDeploymentTypeId ) . ' )' . '
					    AND cluster_id = ' . ( int ) $intClusterId . '
					    AND ps_product_id IS NULL
					    AND parent_deployment_id IS NULL
					    AND failed_on IS NULL';

		if( true == $boolIsGit ) {
			$strSql .= ' AND exec_command ILIKE \'%git%\'';
		} else {
			$strSql .= ' AND exec_command NOT ILIKE \'%git%\'';
		}

		$strSql .= ' ORDER BY
					        id DESC
						 LIMIT 1';
		return self::fetchDeployment( $strSql, $objDatabase );
	}

}
?>