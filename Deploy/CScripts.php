<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CScripts
 * Do not add any new functions to this class.
 */

class CScripts extends CBaseScripts {

	public static function fetchPaginatedScriptsCount( $objDatabase, $boolIsDisabledData, $arrintRegionIds = NULL, $arrintProductIds = NULL, $arrintProductOptionIds = NULL, $arrintExecuterTypeIds = NULL, $arrintScriptTypeIds = NULL, $arrintHardwareIds = NULL, $boolIsPublishedScript = false, $boolIsRunOnTestDbScript = false, $boolIsDoNotRunInRentWeekScript = false, $boolIsScheduledScript = false, $boolIsCriticalScript = false ) {
		$strWhere = '';
    	$strJoin = ( false == empty( $arrintHardwareIds ) )? ' JOIN script_hardwares as sh ON ( sh.script_id = s.id) WHERE sh.hardware_id IN( ' . implode( ', ', $arrintHardwareIds ) . ' )' : ' where 1 = 1 ';
    	$strWhere .= ( false == empty( $arrintExecuterTypeIds ) ) ? ' AND s.script_executor_type_id IN( ' . implode( ', ', $arrintExecuterTypeIds ) . ' )' : ' ';
		$strWhere .= ( false == empty( $arrintRegionIds ) ) ? ' AND ( ( s.details -> \'region_ids\' )::jsonb @> \'[' . implode( ', ', $arrintRegionIds ) . ']\'' : ' ';
		$strWhere .= ( false == empty( $arrintRegionIds ) ) ? ' OR  ( s.details -> \'region_ids\' )::jsonb <@ \'[' . implode( ', ', $arrintRegionIds ) . ']\')' : ' ';

		$strWhere .= ( false == empty( $arrintProductIds ) ) ? ' AND ( ( s.details -> \'ps_product_ids\' -> \'product_ids\' )::jsonb @> \'[' . implode( ', ', $arrintProductIds ) . ']\'' : ' ';
		$strWhere .= ( false == empty( $arrintProductIds ) ) ? ' OR ( s.details -> \'ps_product_ids\' -> \'product_ids\' )::jsonb <@ \'[' . implode( ', ', $arrintProductIds ) . ']\' )' : ' ';

		$strWhere .= ( false == empty( $arrintProductOptionIds ) ) ? ' AND ( ( s.details -> \'ps_product_ids\' -> \'product_option_ids\' )::jsonb @> \'[' . implode( ', ', $arrintProductOptionIds ) . ']\'' : ' ';
		$strWhere .= ( false == empty( $arrintProductOptionIds ) ) ? ' OR ( s.details -> \'ps_product_ids\' -> \'product_option_ids\' )::jsonb <@ \'[' . implode( ', ', $arrintProductOptionIds ) . ']\' )' : ' ';

		$strWhere .= ( false == empty( $arrintScriptTypeIds ) ) ? ' AND s.script_type_id IN( ' . implode( ', ', $arrintScriptTypeIds ) . ' ) ' : ' ';
		$strWhere .= ( false == empty( $boolIsPublishedScript ) ) ? 'AND s.is_published = 1 ' : ' ';
		$strWhere .= ( false == empty( $boolIsRunOnTestDbScript ) ) ? 'AND s.run_on_test_db = true ' : ' ';
		$strWhere .= ( false == empty( $boolIsDoNotRunInRentWeekScript ) ) ? 'AND s.do_not_run_in_rent_week = 1 ' : ' ';
		$strWhere .= ( false == empty( $boolIsScheduledScript ) ) ? 'AND s.script_path LIKE \'%Scheduled%\' ' : ' ';
		$strWhere .= ( false == empty( $boolIsCriticalScript ) ) ? 'AND s.is_critical = 1 ' : ' ';

    	if( 1 != $boolIsDisabledData ) {
    		$strWhere .= 'AND s.is_published = 1';
    	}

    	$strSql = 'SELECT COUNT(DISTINCT s.id) FROM scripts as s ' . $strJoin . $strWhere . 'AND s.deleted_by is NULL';

    	$arrintResponse = fetchData( $strSql, $objDatabase );

    	if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
        return 0;
	}

	public static function fetchScriptsByIds( $arrintScriptIds, $objDatabase ) {
        return self::fetchScripts( sprintf( 'SELECT * FROM scripts WHERE deleted_by is NULL AND id IN( %s )', implode( ',', $arrintScriptIds ) ), $objDatabase );
    }

    public static function fetchScriptByName( $strName, $objDatabase ) {
    	return self::fetchScript( sprintf( 'SELECT * FROM scripts WHERE deleted_by is NULL AND name = \'%s\'', addslashes( trim( $strName ) ) ), $objDatabase );
    }

    public static function fetchAllScripts( $objDatabase ) {

    	$strSql = 'SELECT * FROM scripts WHERE deleted_by is NULL';

    	return self::fetchScripts( $strSql, $objDatabase );
	}

    public static function fetchPublishedMonitoredScripts( $objDatabase, $boolIsScheduled = false, $boolIsCritical = false, $arrintExecutorIds = NULL ) {

    	$strSql = 'SELECT * FROM scripts WHERE is_published = 1 AND do_not_monitor <> 1 AND deleted_by is NULL AND run_on_specific_databases IS NULL';

    	if( true == $boolIsScheduled ) {
    		$strSql .= ' AND script_path LIKE \'%Scheduled%\'';
    	}

    	if( true == $boolIsCritical ) {
    		$strSql .= ' AND is_critical = 1';
    	}

    	if( false == empty( $arrintExecutorIds ) ) {
    		$strSql .= ' AND script_executor_type_id IN( ' . implode( ', ', $arrintExecutorIds ) . ' ) ';
    	}

    	return self::fetchScripts( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedScripts( $intPageNo, $intPageSize, $objDatabase, $arrintRegionIds = NULL, $arrintProductIds = NULL, $arrintProductOptionIds = NULL, $arrintExecuterTypeIds = NULL, $arrintScriptTypeIds = NULL, $arrintHardwareIds = NULL, $boolIsPublishedScript = false, $boolIsRunOnTestDbScript = false, $boolIsDoNotRunInRentWeekScript = false, $boolIsScheduledScript = false, $boolIsCriticalScript = false,  $strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;
		$strWhere = '';
		if( false == empty( $strOrderByField ) ) {

			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == empty( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY s.updated_on DESC';
		}

		$strJoin = ( false == empty( $arrintHardwareIds ) )? ' JOIN script_hardwares as sh ON ( sh.script_id = s.id) WHERE sh.hardware_id IN( ' . implode( ', ', $arrintHardwareIds ) . ' )' : ' where 1 = 1 ';

		$strWhere .= ( false == empty( $arrintExecuterTypeIds ) ) ? ' AND s.script_executor_type_id IN( ' . implode( ', ', $arrintExecuterTypeIds ) . ' )' : ' ';

		$strWhere .= ( false == empty( $arrintRegionIds ) ) ? ' AND ( ( s.details -> \'region_ids\' )::jsonb @> \'[' . implode( ', ', $arrintRegionIds ) . ']\'' : ' ';
		$strWhere .= ( false == empty( $arrintRegionIds ) ) ? ' OR  ( s.details -> \'region_ids\' )::jsonb <@ \'[' . implode( ', ', $arrintRegionIds ) . ']\')' : ' ';

		$strWhere .= ( false == empty( $arrintProductIds ) ) ? ' AND ( ( s.details -> \'ps_product_ids\' -> \'product_ids\' )::jsonb @> \'[' . implode( ', ', $arrintProductIds ) . ']\'' : ' ';
		$strWhere .= ( false == empty( $arrintProductIds ) ) ? ' OR (s.details -> \'ps_product_ids\' -> \'product_ids\' )::jsonb <@ \'[' . implode( ', ', $arrintProductIds ) . ']\' )' : ' ';

		$strWhere .= ( false == empty( $arrintProductOptionIds ) ) ? ' AND ( ( s.details -> \'ps_product_ids\' -> \'product_option_ids\' )::jsonb @> \'[' . implode( ', ', $arrintProductOptionIds ) . ']\'' : ' ';
		$strWhere .= ( false == empty( $arrintProductOptionIds ) ) ? ' OR ( s.details -> \'ps_product_ids\' -> \'product_option_ids\' )::jsonb >@ \'[' . implode( ', ', $arrintProductOptionIds ) . ']\' )' : ' ';

		$strWhere .= ( false == empty( $arrintScriptTypeIds ) ) ? ' AND s.script_type_id IN( ' . implode( ', ', $arrintScriptTypeIds ) . ' ) ' : ' ';

		$strWhere .= ( false == empty( $boolIsPublishedScript ) ) ? ' AND s.is_published = 1 ' : ' ';

		$strWhere .= ( false == empty( $boolIsRunOnTestDbScript ) ) ? ' AND s.run_on_test_db = true ' : ' ';

		$strWhere .= ( false == empty( $boolIsDoNotRunInRentWeekScript ) ) ? ' AND s.do_not_run_in_rent_week = 1 ' : ' ';

		$strWhere .= ( false == empty( $boolIsScheduledScript ) ) ? ' AND s.script_path LIKE \'%Scheduled%\' ' : ' ';

		$strWhere .= ( false == empty( $boolIsCriticalScript ) ) ? ' AND s.is_critical = 1 ' : ' ';

		$strSql = 'SELECT	s.*, s.name as script_type_name
					FROM scripts as s ' . $strJoin . $strWhere .
					'AND s.deleted_by is NULL' . $strOrderBy . '
					OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchScripts( $strSql, $objDatabase );
	}

	public static function fetchSearchedScripts( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSqlCondition = '';

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrFilteredExplodedSearch ) && true == is_numeric( $arrstrFilteredExplodedSearch[0] ) ) {
			$strSqlCondition = ' AND s.id = ' . ( int ) $arrstrFilteredExplodedSearch[0];
		}

		$strSql = 'SELECT	s.*
					FROM scripts as s
						JOIN script_types as st ON ( s.script_type_id = st.id )
					WHERE
						s.deleted_by is NULL AND 1 = 1';

		if( '' != $strSqlCondition ) {
			$strSql .= $strSqlCondition;
		} else {
			$strSql .= 'AND s.name ILIKE \'%' . implode( '%\' AND st.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR st.name ILIKE \'%' . implode( '%\' AND s.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' order by id LIMIT 10';
		}

		return self::fetchScripts( $strSql, $objDatabase );

	}

	public static function fetchCriticalScripts( $objDatabase ) {

		$strSql = 'SELECT * FROM scripts WHERE is_published = 1 AND do_not_monitor <> 1 AND is_critical = 1 AND deleted_by IS NULL';

		return self::fetchScripts( $strSql, $objDatabase );
	}

	public static function fetchCustomScriptsByScriptIds( $arrintScriptIds, $objDatabase ) {

		if( false == valArr( $arrintScriptIds ) ) return NULL;

		$strSql = 'SELECT
						s.id,
						s.name,
						s.created_by as owner_user_id,
						s.execution_minutes,
						s.schedule,
						s.do_not_monitor,
				 		CASE WHEN s.execution_minutes < 60 THEN 2 * s.execution_minutes ELSE s.execution_minutes + 60 END as max_execution_minutes
					FROM
						scripts s
					WHERE
						s.id IN ( ' . implode( ',', $arrintScriptIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllScriptsByExecutorTypeIdsByHardwareIds( $arrintExecuterTypeIds, $arrintHardwareIds = NULL, $objDatabase ) {

		$strJoin 	= ( false == empty( $arrintHardwareIds ) )? ' JOIN script_hardwares as sh ON ( sh.script_id = s.id) WHERE sh.hardware_id IN( ' . implode( ', ', $arrintHardwareIds ) . ' )' : ' where 1 = 1 ';
		$strWhere 	.= ( false == empty( $arrintExecuterTypeIds ) ) ? ' AND s.script_executor_type_id IN( ' . implode( ', ', $arrintExecuterTypeIds ) . ' )' : ' ';

		$strSql = 'SELECT
						s.*
					FROM
						scripts as s' . $strJoin . $strWhere . '
						AND s.deleted_by is NULL';

		return self::fetchScripts( $strSql, $objDatabase );
	}

	public static function fetchScriptsByScriptExecutorTypeId( $intExecutorTypeId, $objDatabase, $intScriptTypeId = NULL ) {

		$strWhere = '';
		if( false == empty( $intScriptTypeId ) ) {
			$strWhere .= ' AND script_type_id = ' . ( int ) $intScriptTypeId;
		}

		$strSql = 'SELECT
						*
					FROM
						scripts
					WHERE
						script_executor_type_id = ' . ( int ) $intExecutorTypeId . $strWhere .
					' AND deleted_by is NULL';

		return self::fetchScripts( $strSql, $objDatabase );

	}

	public static function fetchAllScriptOwners( $objDatabase ) {

		$strSql = ' SELECT
						s.name as script_name,
						so.employee_id
					FROM
						scripts s
						JOIN script_owners so ON (s.id = so.script_id)';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllScriptByScriptOwners( $objDatabase ) {

		$strSql = 'SELECT s.*,so.employee_id  FROM scripts s JOIN script_owners so ON (s.id = so.script_id) and s.deleted_by is NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublishedScriptsByScriptExecutorTypeId( $intExecutorTypeId, $objDatabase ) {

		$strSql = 'SELECT
						s.id,s.script_type_id,s.name,run_on_specific_databases,s.script_path,s.schedule,s.parameters,s.log_path,sh.hardware_id
					FROM
						scripts s JOIN script_hardwares sh on ( s.id=sh.script_id )
					WHERE
						script_executor_type_id = ' . ( int ) $intExecutorTypeId .
		                'AND deleted_by is NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScriptInfoByScriptTypeId( $intScriptTypeId, $objDatabase ) {
		$strSql = 'SELECT
						s.id,s.script_type_id,s.name
					FROM
						scripts s
					WHERE
						script_type_id = ' . ( int ) $intScriptTypeId .
		          ' AND deleted_by is NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEntrataScriptByIsRunOnSpecificDatabases( $objDatabase ) {
		$strSql = 'SELECT * 
					FROM
						scripts
					WHERE
						is_published = 1
						AND do_not_monitor <> 1
						AND deleted_by is NULL
						AND run_on_specific_databases IS NOT NULL
						AND script_type_id = ' . CScriptType::ENTRATA_PHP . '
						AND script_executor_type_id =' . CScriptExecutorType::BEACONBOX;

		return self::fetchScripts( $strSql, $objDatabase );
	}

}
?>
