<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CDatabaseScriptBatches
 * Do not add any new functions to this class.
 */

class CDatabaseScriptBatches extends CBaseDatabaseScriptBatches {

	public static function fetchDatabaseScriptBatchesByIds( $arrintDatabaseScriptBatchIds, $objDatabase ) {

		$strSql = 'SELECT * FROM database_script_batches WHERE id IN ( ' . implode( ',', $arrintDatabaseScriptBatchIds ) . ' ) ';

		return self::fetchDatabaseScriptBatches( $strSql, $objDatabase );
	}

	public static function fetchDatabaseScriptBatchesByDatabaseTypeIdByDeploymentId( $intDatabaseTypeId, $intDeploymentId, $objDatabase ) {

		$strSql = 'SELECT
    					DISTINCT dsb.*
					FROM
    					database_script_batches dsb
    					JOIN database_script_releases dsr ON ( dsr.database_script_batch_id = dsb.id )
					WHERE
    					dsb.database_type_id = ' . ( int ) $intDatabaseTypeId . '
    					AND dsr.deployment_id = ' . ( int ) $intDeploymentId;

		return self::fetchDatabaseScriptBatch( $strSql, $objDatabase );
	}

	public static function fetchDatabaseScriptBatchesBySvnFileIdsByDatabaseTypeId( $arrintSvnFileIds, $intDatabaseTypeId, $objDatabase ) {

		$strCondition = NULL;

		foreach( $arrintSvnFileIds as $intSvnFileId ) {
			if( true == is_null( $strCondition ) ) {
				$strCondition .= ' svn_file_ids LIKE \'%' . $intSvnFileId . '%\'';
			} else {
				$strCondition .= ' OR svn_file_ids LIKE \'%' . $intSvnFileId . '%\'';
			}
		}

		$strSql = 'SELECT
       					*
    				FROM
        				database_script_batches dsb
					WHERE
						dsb.database_type_id = ' . ( int ) $intDatabaseTypeId;

		if( false == is_null( $strCondition ) ) {
			$strSql .= ' AND ( ' . $strCondition . ')';
		}

		return self::fetchDatabaseScriptBatches( $strSql, $objDatabase );
	}

	public static function fetchDatabaseScriptBatchesIdsFromStartDateTOEndDateByClusterIdByDeplymentTypeIdsByDatabaseTypeIds( $intStartDate, $intEndDate, $intClusterId, $arrintDeploymentTypeIds, $arrintDatabaseTypeIds, $objDatabase ) {

		$strSql = 'SELECT
		                DISTINCT ( dsb.id ) AS batch_id,
		                dsb.order_num,
				    	dt.name As deployment_type,
				    	dt.id As deployment_type_id,
    					dsr.release_datetime,
						d.start_datetime,
						d.id,
						d.approved_by
		            FROM
		                database_script_batches dsb
		                JOIN database_script_releases dsr ON ( dsb.id = dsr.database_script_batch_id )
		                JOIN deployments AS d ON ( d.id = dsr.deployment_id )
    					JOIN deployment_types AS dt ON ( dt.id = d.deployment_type_id )
		            WHERE
						d.is_run_on_all_clusters = 0
		                ANd dsb.database_type_id IN( ' . implode( ',', $arrintDatabaseTypeIds ) . ' )
		                AND dsr.release_datetime >= \'' . $intStartDate . '\'
		                AND dsr.release_datetime <= \'' . $intEndDate . '\'
		                AND d.cluster_id = ' . ( int ) $intClusterId . '
		                AND d.deployment_type_id IN ( ' . implode( ',', $arrintDeploymentTypeIds ) . ' )
		                AND d.start_datetime IS NOT NULL
						AND d.completed_on IS NOT NULL
		            ORDER BY
						 d.id,
		                 d.start_datetime,
		                 dsr.release_datetime;';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDatabaseScriptBatchesDetailsFromDeploymentIdByClusterIdByDeplymentTypeIdsByDatabaseTypeIds( $intdeploymentId, $intClusterId, $arrintDeploymentTypeIds, $arrintDatabaseTypeIds, $objDatabase ) {

		$strSql = 'SELECT
		                DISTINCT ( dsb.id ) AS batch_id,
		                dsb.order_num,
		                dsb.svn_file_ids,
		                dsb.database_type_id,
				    	dt.name As deployment_type,
				    	dt.id As deployment_type_id,
    					dsr.release_datetime,
						d.start_datetime,
						d.id
		            FROM
		                database_script_batches dsb
		                JOIN database_script_releases dsr ON ( dsb.id = dsr.database_script_batch_id )
		                JOIN deployments AS d ON ( d.id = dsr.deployment_id )
    					JOIN deployment_types AS dt ON ( dt.id = d.deployment_type_id )
		            WHERE
		                dsb.database_type_id IN( ' . implode( ',', $arrintDatabaseTypeIds ) . ' )
		                AND d.id > ' . ( int ) $intdeploymentId . ' 
		                AND d.cluster_id = ' . ( int ) $intClusterId . '
		                AND d.deployment_type_id IN ( ' . implode( ',', $arrintDeploymentTypeIds ) . ' )
		                AND d.start_datetime IS NOT NULL
						AND d.completed_on IS NOT NULL
		            ORDER BY
						 d.id,
		                 d.start_datetime,
		                 dsr.release_datetime;';
		return fetchData( $strSql, $objDatabase );
	}

    public static function fetchDatabaseScriptBatchesBySqlFileIdsByDatabaseTypeId( $arrintSqlFileIds, $intDatabaseTypeId, $objDatabase ) {

        $strCondition = NULL;

        foreach( $arrintSqlFileIds as $intSqlFileId ) {
            if( true == is_null( $strCondition ) ) {
                $strCondition .= ' details->>\'sql_file_ids\' LIKE \'' . $intSqlFileId . '\'';
            } else {
                $strCondition .= ' OR details->>\'sql_file_ids\' LIKE \'' . $intSqlFileId . '\'';
            }
        }

        $strSql = 'SELECT
       					*
    				FROM
        				database_script_batches dsb
					WHERE
						dsb.database_type_id = ' . ( int ) $intDatabaseTypeId;

        if( false == is_null( $strCondition ) ) {
            $strSql .= ' AND ( ' . $strCondition . ')';
        }

        return self::fetchDatabaseScriptBatches( $strSql, $objDatabase );
    }

}
?>