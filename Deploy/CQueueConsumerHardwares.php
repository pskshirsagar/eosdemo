<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CQueueConsumerHardwares
 * Do not add any new functions to this class.
 */

class CQueueConsumerHardwares extends CBaseQueueConsumerHardwares {

	public static function fetchQueueConsumerHardwaresByQueueConsumerIds( $arrintQueueConsumerIds, $objDatabase ) {

		if( false == valArr( $arrintQueueConsumerIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						queue_consumer_hardwares
					WHERE
						queue_consumer_id IN ( ' . implode( ',', $arrintQueueConsumerIds ) . ' ) ';

		return self::fetchQueueConsumerHardwares( $strSql, $objDatabase );
	}

	public static function fetchQueueConsumerHardwaresByQueueConsumerId( $intQueueConsumerId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						queue_consumer_hardwares
					WHERE
						queue_consumer_id = ' . ( int ) $intQueueConsumerId . '
					ORDER BY id DESC';

		return self::fetchQueueConsumerHardwares( $strSql, $objDatabase );
	}

	public static function fetchQueueConsumerHardwaresByIds( $arrintDeletedQueueConsumerHardwareIds, $objDatabase ) {

		if( false == valArr( $arrintDeletedQueueConsumerHardwareIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						queue_consumer_hardwares
					WHERE
						id IN ( ' . implode( ',', $arrintDeletedQueueConsumerHardwareIds ) . ' ) ';

		return self::fetchQueueConsumerHardwares( $strSql, $objDatabase );
	}

	public static function fetchQueueConsumerHardwareByQueueConsumerIdByHardwareId( $intQueueConsumerId, $intHardwareId, $objDatabase ) {

		$strSql = '
			SELECT
				qch.*
			FROM
				queue_consumer_hardwares qch
			WHERE
				qch.queue_consumer_id = ' . ( int ) $intQueueConsumerId . '
				AND qch.hardware_id = ' . ( int ) $intHardwareId;

		return self::fetchQueueConsumerHardware( $strSql, $objDatabase );
	}

	public static function fetchQueueCosnumerCountOnHardwares( $objDatabase ) {

		$strSql = 'SELECT
				        qch.hardware_id,
				        count(qch.hardware_id) as count
					FROM
					    queue_consumers qc
					    JOIN queue_consumer_hardwares qch ON ( qc.id = qch.queue_consumer_id )
					    JOIN hardwares h ON ( h.id = qch.hardware_id ) AND h.cluster_id IN ( ' . CCluster::RAPID . ', ' . CCluster::STANDARD . ' ) AND h.is_published = 1 AND qch.is_published = TRUE
						GROUP BY qch.hardware_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchQueueConsumerHardwaresByQueueConsumerIdsByClusterId( $arrintQueueConsumerIds, $intClusterId, $objDatabase ) {
		$strSql = 'SELECT
						qch.*
					FROM
						queue_consumer_hardwares qch
						JOIN hardwares h ON ( qch.hardware_id = h.id )
					WHERE
						qch. queue_consumer_id IN ( ' . implode( ',', $arrintQueueConsumerIds ) . ' ) ' . '
						AND h.cluster_id  = ' . ( int ) $intClusterId . '
					ORDER BY id DESC';

		return self::fetchQueueConsumerHardwares( $strSql, $objDatabase );

	}

}
?>