<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CEntratamationVersions
 * Do not add any new functions to this class.
 */

class CEntratamationVersions extends CBaseEntratamationVersions {

	public static function fetchAllEntrataMationVersions( $objDatabase ) {
		$strSql = 'SELECT 
						* 
					FROM
						entratamation_versions
					WHERE
						deleted_on IS NULL
					ORDER BY 
						id 
					ASC';

		return parent::fetchEntratamationVersions( $strSql, $objDatabase );
	}

	public static function fetchEntrataMationVersionByStage( $strStage, $boolIsRelease = false, $objDatabase ) {
		$strCondition = '';
		if( true == $boolIsRelease ) {
			$strCondition = 'AND ready_for_release = true';
		}

		$strSql = 'SELECT
					    *
					FROM
					    entratamation_versions
					WHERE
					    stage like \'%' . $strStage . '%\''
					    . $strCondition .
					' AND deleted_on IS NULL
					ORDER BY
					    id DESC
					LIMIT
					    1';

		return parent::fetchEntratamationVersion( $strSql, $objDatabase );
	}

	public static function fetchEntrataMationVersionsByStage( $strStage, $boolIsRelease = false, $objDatabase, $intLimit = 3 ) {
		$strCondition = '';
		if( true == $boolIsRelease ) {
			$strCondition = 'AND ready_for_release = true';
		}

		$strSql = 'SELECT
					    *
					FROM
					    entratamation_versions
					WHERE
					    stage like \'%' . $strStage . '%\''
		          . $strCondition .
		          ' AND deleted_on IS NULL 
		          ORDER BY
					    id DESC LIMIT ' . ( int ) $intLimit;

		return parent::fetchEntratamationVersions( $strSql, $objDatabase );
	}

	public static function fetchLastestEntrataMationVersion( $objDatabase ) {
		$strSql = 'SELECT 
                       version_number
                   FROM 
                        entratamation_versions 
                   ORDER BY
				        version_number 
				   DESC
				   LIMIT 1';

		return parent::fetchEntratamationVersion( $strSql, $objDatabase );
	}

	public static function fetchLastestEntrataMationProductionVersion( $objDatabase ) {
		$strSql = 'SELECT 
						id,
						name,
						version_number
                   FROM 
                        entratamation_versions 
                    WHERE
                        stage = \'Production\'
                        AND ready_for_release = true
                   ORDER BY
				        version_number 
				   DESC
				   LIMIT 1';

		return parent::fetchEntratamationVersion( $strSql, $objDatabase );
	}

	public static function fetchLatestEntrataMationVersions( $objDatabase ) {

		$strSql = 'SELECT
		                   *
		                   FROM
                            entratamation_versions
                        WHERE
                            id IN (
								SELECT
                                        version.id
                                    FROM
                                    (
	                                    SELECT
                                              max ( id ) AS id,
                                              stage
                                          FROM
                                              entratamation_versions
                                          GROUP BY
                                              stage
                                        ) AS version
                            )
                            AND deleted_on IS NULL
                        ORDER BY
                            updated_on DESC';

		return parent::fetchEntratamationVersions( $strSql, $objDatabase );
	}

	public static function fetchEntrataMationVersionByVersionNumberByStage( $intVersionNumber, $strStage, $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    entratamation_versions
					WHERE
					    version_number = ' . ( int ) $intVersionNumber .
						' AND
						stage like \'%' . $strStage . '%\'';

		return parent::fetchEntratamationVersion( $strSql, $objDatabase );
	}

	public function fetchOldEntratamationVersions( $objDatabase ) {

		$strSql = 'SELECT
					*
				FROM
					entratamation_versions
				WHERE
					name NOT IN (
						SELECT 
							name
						FROM
							entratamation_versions
						WHERE
							stage = \'Alpha\'
						ORDER BY version_number DESC
						LIMIT 2
					) AND
					name NOT IN (
						SELECT
							name
						FROM
							entratamation_versions
						WHERE
							stage = \'Production\'
						ORDER BY version_number DESC
						LIMIT 2
					) AND
					deleted_on IS NULL';

		return parent::fetchEntratamationVersions( $strSql, $objDatabase );
	}

}
?>