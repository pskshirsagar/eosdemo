<?php

class CSystemModule extends CBaseSystemModule {

	const RELEASE              = 2;
	const MAINTENANCES         = 3;
	const EXECUTE_SQL          = 8;
	const VIEW_MAINTENANCE     = 9;
	const DATABASE_DEPLOYMENTS = 133;
	const DATABASE_DUMPS_LIVE  = 149;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>