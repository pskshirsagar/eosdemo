<?php

class CHardware extends CBaseHardware {

	const LN_VCENTER    = 346;
	const LN_FSCRIPT01R = 611;
	const LN_FSCRIPT01S = 619;
	const LN_RSCRIPT04  = 609;
	const LN_RSCRIPT07  = 612;
	const LN_RSCRIPT08  = 613;
	const LN_RSCRIPT09  = 835;
	const LN_RSCRIPT10  = 836;
	const LN_RSCRIPT11  = 1102;
	const LN_RSCRIPT13  = 1101;

	protected $m_strHardwareTypeName;
	protected $m_strClusterName;
	protected $m_strExpiryStatus;
	protected $m_strMachineName;

	public static $c_arrstrFreeSwitchHardwareTypes = array(
		self::LN_FSCRIPT01R,
		Self::LN_FSCRIPT01S
	);

	public static $c_arrstrRapidQueueConsumerHardware = array(
		self::LN_FSCRIPT01R,
		Self::LN_RSCRIPT04,
		self::LN_RSCRIPT07,
		Self::LN_RSCRIPT08,
		self::LN_RSCRIPT09,
		Self::LN_RSCRIPT10
	);

    /**
     * Get Functions
     *
     */

    public function getHardwareTypeName() {
    	return $this->m_strHardwareTypeName;
    }

	public function getClusterName() {
    	return $this->m_strClusterName;
    }

    public function getExpiryStatus() {
    	return $this->m_strExpiryStatus;
    }

    public function getMachineName() {
    	return $this->m_strMachineName;
    }

	public static function getFreeSwitchServerIps() {

		return [ '10.1.1.20', '10.1.1.9', '10.1.1.24', '10.1.1.208', '10.1.1.151', '10.1.1.152' ];
	}

    /**
     * Set Functions
     *
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['hardware_type_name'] ) ) $this->setHardwareTypeName( $arrmixValues['hardware_type_name'] );
    	if( true == isset( $arrmixValues['cluster_name'] ) ) $this->setClusterName( $arrmixValues['cluster_name'] );
    	if( true == isset( $arrmixValues['machine_name'] ) ) $this->setMachineName( $arrmixValues['machine_name'] );
    	if( true == isset( $arrmixValues['expiry_status'] ) ) $this->setExpiryStatus( $arrmixValues['expiry_status'] );

    	return;
    }

    public function setHardwareTypeName( $strHardwareTypeName ) {
    	$this->m_strHardwareTypeName = $strHardwareTypeName;
    }

  	public function setClusterName( $strClusterName ) {
    	$this->m_strClusterName = $strClusterName;
    }

    public function setExpiryStatus( $strExpiryStatus ) {
    	$this->m_strExpiryStatus = $strExpiryStatus;
    }

    public function setMachineName( $strMachineName ) {
    	$this->m_strMachineName = $strMachineName;
    }

    /**
     * Validate Functions
     *
     */

    public function valHardwareTypeId() {
        $boolIsValid = true;
        if( true == is_null( $this->getHardwareTypeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hardware_type_id', 'Hardware type is required.' ) );
        }

        return $boolIsValid;
    }

    public function valHardwareStatusTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getHardwareStatusTypeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hardware_status_type_id', 'Hardware status type is required.' ) );
        }

        return $boolIsValid;
    }

    public function valEnvironmentTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getEnvironmentTypeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'environment_type_id', 'Environment type is required.' ) );
        }

        return $boolIsValid;
    }

    public function valMachineTypeId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getMachineTypeId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'machine_type_id', 'Machine Type is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valClusterId() {
        $boolIsValid = true;

        if( true == is_null( $this->getClusterId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cluster_id', 'Cluster is required.' ) );
        }

        return $boolIsValid;
    }

    public function valParentHardwareId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getParentHardwareId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'parent_hardware_id', 'VM is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valInstanceId( $objDatabase ) {
    	$boolIsValid = true;

    	if( true == is_null( $this->getInstanceId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'instance_id', 'Instance is required.' ) );
    	} elseif( true == strpos( $this->getInstanceId(), ' ' ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'instance_id', 'Space is not allowed in instance id.' ) );
    	} else {
			$strSqlCondition	= ( 0 < $this->getId() ? ' AND id <> ' . $this->getId() : '' );
			$strSql				= 'WHERE lower( instance_id ) = \'' . trim( strtolower( addslashes( $this->getInstanceId() ) ) ) . '\' AND deleted_by IS NULL' . $strSqlCondition;
			$intCount			= CHardwares::fetchHardwareCount( $strSql, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'Instance id is already in use for other hardware.' ) );
			}
    	}
    	return $boolIsValid;
    }

    public function valIpAddress( $objDatabase ) {
        $boolIsValid = true;

        if( true == is_null( $this->getIpAddress() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'IP address is required.' ) );

        } elseif( false == CValidation::validateIpAddress( $this->getIpAddress() ) ) {
			$boolIsValid = false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'Invalid IP address entered.' ) );
		} else {

			// hardware
			$strSqlCondition	= ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			$strSql				= 'WHERE lower( ip_address ) = \'' . trim( strtolower( addslashes( $this->getIpAddress() ) ) ) . '\' AND deleted_by IS NULL' . $strSqlCondition;
			$intCount			= CHardwares::fetchHardwareCount( $strSql, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'IP address is already in use for other hardware.' ) );
			}

        }

        return $boolIsValid;
    }

    public function valAssetNumber( $objDatabase ) {
        $boolIsValid = true;

        if( 0 == $this->getAssetNumber() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'asset_number', 'Asset number should be integer.' ) );

        } elseif( 8 < strlen( ( string ) $this->getAssetNumber() ) ) {
			$boolIsValid = false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'asset_number', 'Asset number should not be more than 8 digits.' ) );

        } else {
	        $strSqlCondition	= ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
	        $strSql				= 'WHERE asset_number = \'' . $this->getAssetNumber() . '\' AND deleted_by IS NULL' . $strSqlCondition;
	        $intCount			= CHardwares::fetchHardwareCount( $strSql, $objDatabase );

	        if( 0 < $intCount ) {
	        	$boolIsValid = false;
	        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'asset_number', 'Asset number is already in use.' ) );
	        }
        }

        return $boolIsValid;
    }

    public function valName( $objDatabase ) {
        $boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Host Name is required.' ) );
        } else {
        	$strSqlCondition	= ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
        	$strSql				= 'WHERE name = \'' . $this->getName() . '\' AND deleted_by IS NULL' . $strSqlCondition;
        	$intCount			= CHardwares::fetchHardwareCount( $strSql, $objDatabase );

        	if( 0 < $intCount ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Host Name is already in use.' ) );
        	}
        }

        return $boolIsValid;
    }

    public function valHardwareInstance( $objDeployDatabase, $objConnectDatabase ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getId() ) ) {
		    $strWhereSql = 'WHERE hardware_id = ' . $this->getId() . ' AND deleted_by IS NULL AND deleted_on IS NULL;';
    		$intDatabaseCount = CDatabases::fetchDatabaseCount( $strWhereSql, $objConnectDatabase );
    	}

    	if( 0 < $intDatabaseCount ) {
    		$boolIsValid = false;
    		$objHardware = CHardwares::fetchHardwareById( $this->getId(), $objDeployDatabase );
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hardwares', 'Unable to delete hardware , ' . $objHardware->getName() . ' hardware is associated in database table ' ) );
    	}
    	return $boolIsValid;
    }

	public function valClientDataInstances( $objDeployDatabase, $objConnectDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getId() ) ) {
			$intlientDataInstanceCount = CClientDataInstances::fetchClientDataInstanceCount( 'WHERE hardware_id=' . $this->getId(), $objDeployDatabase );
		}
		if( 0 < $intlientDataInstanceCount ) {
			$boolIsValid = false;
			$objHardware = CHardwares::fetchHardwareById( $this->getId(), $objDeployDatabase );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hardwares', 'Unable to delete hardware , ' . $objHardware->getName() . ' hardware is associated client data instances table. ' ) );
		}
		return $boolIsValid;
	}

    public function valIsMiscellaneous( $objDatabase ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getId() ) ) {
			$objHardware = CHardwares::fetchHardwareById( $this->getId(), $objDatabase );
				if( 0 == $objHardware->getIsMiscellaneous() && 1 == $this->getIsMiscellaneous() ) {
 		  			$arrobjHardwareProducts = CHardwareProducts::fetchHardwareProductsByHardwareId( $this->getId(), $objDatabase );
    				if( true == valArr( $arrobjHardwareProducts ) ) {
    					$boolIsValid = false;
    					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hardwares', 'Unable to update hardware, hardware is associated to hardware product(s). ' ) );
    				}
	    		}
    	}
    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL, $boolIsInsert = false, $objConnectDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valHardwareTypeId();
            	$boolIsValid &= $this->valHardwareStatusTypeId();
            	$boolIsValid &= $this->valEnvironmentTypeId();
            	$boolIsValid &= $this->valClusterId();
            	$boolIsValid &= $this->valMachineTypeId();
            	$boolIsValid &= $this->valIpAddress( $objDatabase );
            	$boolIsValid &= $this->valIsMiscellaneous( $objDatabase );

            	if( true == is_numeric( $this->getAssetNumber() ) ) $boolIsValid &= $this->valAssetNumber( $objDatabase );

            	$boolIsValid &= $this->valName( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valHardwareInstance( $objDatabase, $objConnectDatabase );
	            $boolIsValid &= $this->valClientDataInstances( $objDatabase, $objConnectDatabase );
            	break;

            case 'Hardware':
            	$boolIsValid &= $this->valHardwareTypeId();
            	$boolIsValid &= $this->valHardwareStatusTypeId();
            	$boolIsValid &= $this->valEnvironmentTypeId();
            	$boolIsValid &= $this->valClusterId();
            	$boolIsValid &= $this->valMachineTypeId();
            	$boolIsValid &= $this->valIpAddress( $objDatabase );
            	$boolIsValid &= $this->valIsMiscellaneous( $objDatabase );

            	if( true == is_numeric( $this->getAssetNumber() ) ) $boolIsValid &= $this->valAssetNumber( $objDatabase );
            	$boolIsValid &= $this->valName( $objDatabase );
            	break;

            case 'VM':
            	$boolIsValid &= $this->valHardwareTypeId();
            	$boolIsValid &= $this->valHardwareStatusTypeId();
            	$boolIsValid &= $this->valEnvironmentTypeId();
            	$boolIsValid &= $this->valClusterId();
            	$boolIsValid &= $this->valMachineTypeId();
            	$boolIsValid &= $this->valIpAddress( $objDatabase );
            	$boolIsValid &= $this->valParentHardwareId();
            	$boolIsValid &= $this->valName( $objDatabase );
            	$boolIsValid &= $this->valIsMiscellaneous( $objDatabase );

            	if( true == is_numeric( $this->getAssetNumber() ) ) $boolIsValid &= $this->valAssetNumber( $objDatabase );
            	break;

            case 'AWS':
            	$boolIsValid &= $this->valHardwareTypeId();
            	$boolIsValid &= $this->valHardwareStatusTypeId();
            	$boolIsValid &= $this->valEnvironmentTypeId();
            	$boolIsValid &= $this->valClusterId();
            	$boolIsValid &= $this->valMachineTypeId();
            	$boolIsValid &= $this->valIpAddress( $objDatabase );
            	$boolIsValid &= $this->valName( $objDatabase );
            	$boolIsValid &= $this->valInstanceId( $objDatabase );
            	$boolIsValid &= $this->valIsMiscellaneous( $objDatabase );
            	break;

          	case 'val_api_hardware':
            	$boolIsValid &= $this->valIpAddress( $objDatabase );
            	$boolIsValid &= $this->valName( $objDatabase );
            	break;

            default:
            	// empty
            	$boolIsValid = false;
            	break;
        }

        return $boolIsValid;
    }

}

?>