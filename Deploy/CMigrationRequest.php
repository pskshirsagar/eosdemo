<?php

use Psi\Eos\Admin\CPsProducts;

class CMigrationRequest extends CBaseMigrationRequest {

	const STATUS_TYPE_PENDING				= 1;
	const STATUS_TYPE_APPROVED				= 2;
	const STATUS_TYPE_CANCELLED				= 3;
	const STATUS_TYPE_WAITING				= 4;
	const STATUS_TYPE_IN_PROCESS			= 5;
	const STATUS_TYPE_FAILED				= 6;
	const STATUS_TYPE_RESUME				= 'Resume';
	const STATUS_TYPE_DELETE_DATA			= 8;
	const STATUS_TYPE_CLEAN_UP_IN_PROCESS	= 9;
	const STATUS_TYPE_COMPLETED				= 10;

	public function valCid() {
		$boolIsValid = true;
		if( false == isset( $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
		}
		return $boolIsValid;
	}

	public function valSourceDatabaseId() {
		$boolIsValid = true;
		if( false == isset( $this->m_intSourceDatabaseId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'source_database_id', 'Source database is required.' ) );
		}
		return $boolIsValid;
	}

	public function valDestinationDatabaseId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intDestinationDatabaseId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'destination_database_id', 'Destination database is required.' ) );
		}

		if( true == in_array( $this->m_intDestinationDatabaseId, array( CDatabase::ENTRATA53, CDatabase::ENTRATA39, CDatabase::ENTRATA25, CDatabase::ENTRATA52, CDatabase::ENTRATA60, CDatabase::ENTRATA61, CDatabase::ENTRATA62, CDatabase::ENTRATA64, CDatabase::ENTRATA65  ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Entrata 25, 39, 53, 52, 60, 61, 62, 64, 65 are for specific purpose, please select different database to continue.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valSourceDatabaseId();
				$boolIsValid &= $this->valDestinationDatabaseId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createTask( $objConnectDatabase, $objClientDatabase, $objAdminDatabase, $objClient ) {

		$objTargetClientDatase = CDatabases::fetchDatabaseById( $this->getDestinationDatabaseId(), $objConnectDatabase );

		if( false == valObj( $objConnectDatabase,  'CDatabase' ) || false == valObj( $objTargetClientDatase,  'CDatabase' ) ) {
			trigger_error( 'Application Error: Failed to database.', E_USER_ERROR );
		}

		$objGroup = CGroups::fetchGroupByName( 'DBA', $objAdminDatabase );

		$objAssignUser = array_pop( CUsers::fetchUsersByGroupIds( array( $objGroup->getId() ), $objAdminDatabase ) );

		if( false == valObj( $objGroup,  'CGroup' ) || false == valObj( $objAssignUser,  'CUser' ) ) {
		trigger_error( 'Application Error: Failed to load users and groups.', E_USER_ERROR );
		}

		$objTask = new CTask();

		$objTask->setDefaults();

		$strSubject = 'Request to migrate client ' . $objClient->getCompanyName();

		$strDescription = 'Please migrate the client ' . $objClient->getCompanyName() . ' from database ' . $objClientDatabase->getDatabaseName() . ' to ' . $objTargetClientDatase->getDatabaseName() . '.';

		$objTask->setTaskTypeId( CTaskType::FEATURE );
		$objTask->setDescription( nl2br( $strDescription ) );
		$objTask->setTitle( $strSubject );
		$objTask->setPsProductId( CPsProduct::SYSTEM );
		$objTask->setUserId( $objAssignUser->getId() );
		$objTask->setTaskPriorityId( CTaskPriority::NORMAL );

		$objTask->setTaskDatetime( 'now()' );

		$objTask->setDueDate( date( 'Y-m-d H:i:s', strtotime( '+2 days' ) ) );

		$objTask->setId( $objTask->fetchNextId( $objAdminDatabase ) );

		$this->setTaskId( $objTask->getId() );
		$this->setCreatedOn( 'now()' );

    	return $objTask;
    }

	public function buildHtmlEmailContent( $objTask, $objEmployee, $objClient, $objClientDatabase, $objTargetClientDatase, $objAdminDatabase ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		if( false == is_null( $objTask->getTaskPriorityId() ) ) {
			$objTaskPriority = CTaskPriorities::fetchTaskPriorityById( $objTask->getTaskPriorityId(), $objAdminDatabase );
		}

		if( false == is_null( $objTask->getTaskStatusId() ) ) {
			$objTaskStatus = \Psi\Eos\Admin\CTaskStatuses::createService()->fetchTaskStatusById( $objTask->getTaskStatusId(), $objAdminDatabase );
		}

		if( false == is_null( $objTask->getTaskTypeId() ) ) {
			$objTaskType = CTaskTypes::fetchTaskTypeById( $objTask->getTaskTypeId(), $objAdminDatabase );
		}

		if( false == is_null( $objTask->getPsProductId() ) ) {
			$objPsProduct = CPsProducts::createService()->fetchPsProductById( $objTask->getPsProductId(), $objAdminDatabase );
		}

		$objGroup = CGroups::fetchGroupByName( 'DBA', $objAdminDatabase );

		$objAssignUser = array_pop( CUsers::fetchUsersByGroupIds( array( $objGroup->getId() ), $objAdminDatabase ) );

		if( false == valObj( $objGroup,  'CGroup' ) || false == valObj( $objAssignUser,  'CUser' ) ) {
			trigger_error( 'Application Error: Failed to load users and groups.', E_USER_ERROR );
		}

		if( false == is_null( $objAssignUser->getEmployeeId() ) ) {
			$objAssignToEmployee = CEmployees::fetchEmployeeById( $objAssignUser->getEmployeeId(), $objAdminDatabase );
		}

		$objSmarty = new CPsSmarty( PATH_INTERFACES_ADMIN, false );

		$objSmarty->assign_by_ref( 'task', 				$objTask );
		$objSmarty->assign_by_ref( 'task_type',		    $objTaskType );
		$objSmarty->assign_by_ref( 'task_priority', 	$objTaskPriority );
		$objSmarty->assign_by_ref( 'ps_product', 		$objPsProduct );
		$objSmarty->assign_by_ref( 'task_status',		$objTaskStatus );
		$objSmarty->assign_by_ref( 'assigned_by_employee', $objEmployee );
		$objSmarty->assign_by_ref( 'assign_to_employee', $objAssignToEmployee );
		$objSmarty->assign_by_ref( 'client', $objClient );
		$objSmarty->assign_by_ref( 'client_database', 	 $objClientDatabase );
		$objSmarty->assign_by_ref( 'target_client_database', $objTargetClientDatase );
		$objSmarty->assign( 'ps_lead_id', $objClient->getPsLeadId() );

		$objSmarty->assign( 'base_uri',					     CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'task_reuqest_uri', 		     CONFIG_SECURE_HOST_PREFIX . 'clientadmin.' . CONFIG_COMPANY_BASE_DOMAIN . '/' );
		$objSmarty->assign( 'CONFIG_COMPANY_BASE_DOMAIN',	 CONFIG_COMPANY_BASE_DOMAIN );
		$objSmarty->assign( 'image_url',	                 CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$strHtmlTaskContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'ps_lead/view_company_migration_email.tpl', PATH_INTERFACES_ADMIN . 'common/layouts/ps_email_admin.tpl' );
		$boolIsSend = $this->sendEmail( $strHtmlTaskContent, $objTask, $objEmployee, $objAssignToEmployee );
		return $boolIsSend;
	}

	public function sendEmail( $strHtmlTaskContent, $objTask, $objEmployee, $objAssignToEmployee ) {

		$strFromEmailAddress = CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS;

		if( false == is_null( $objEmployee->getEmailAddress() ) ) {
			$strFromEmailAddress = $objEmployee->getEmailAddress();
		}

		$objEmailDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::PS_DEVELOPER );

		$strTaskSubject = 'Task ID ' . $objTask->getId() . ' : ' . $objTask->getTitle();

		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setFromEmailAddress( $strFromEmailAddress );

		$objSystemEmail->setSubject( $strTaskSubject );
		$objSystemEmail->setToEmailAddress( $objAssignToEmployee->getEmailAddress() );
		$objSystemEmail->setHtmlContent( $strHtmlTaskContent );
		$objSystemEmail->setCid( $this->getCid() );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::COMPANY_EMPLOYEE );

		if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
			return false;
		}

		return true;
	}

}
?>