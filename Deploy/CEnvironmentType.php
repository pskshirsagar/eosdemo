<?php

class CEnvironmentType extends CBaseEnvironmentType {

	const PRODUCTION 			= 1;
	const STAGE 				= 2;
	const DR_REPLICATION 		= 3;
	const LOCAL					= 4;
	const TRUNK					= 5;
	const RAPID_STAGE			= 6;
	const STANDARD_STAGE		= 7;
	const RAPID_PRODUCTION		= 8;
	const STANDARD_PRODUCTION	= 9;

	public static $c_arrintEnvironmentTypeIds = array(
		self::PRODUCTION,
		self::STAGE,
		self::DR_REPLICATION,
		self::LOCAL
	);

	// static array of published environment types
	public static $c_arrstrEnvironmentTypes = array(
		self::PRODUCTION 		=> 'Production',
		self::STAGE 			=> 'Stage',
		self::DR_REPLICATION 	=> 'DR Replication',
		self::LOCAL				=> 'Local'
	);

	public static $c_arrintLocalEnvironmentTypes = array(
		self::LOCAL,
		self::STAGE,
		self::TRUNK
	);

	// static array of published environment types for defect
	public static $c_arrmixEnvironmentTypes = [
		self::LOCAL					=> [ 'id' => self::LOCAL, 'name' => 'Local/QA' ],
		self::TRUNK					=> [ 'id' => self::TRUNK, 'name' => 'Trunk' ],
		self::RAPID_PRODUCTION		=> [ 'id' => self::RAPID_PRODUCTION, 'name' => 'Rapid Production' ],
		self::RAPID_STAGE			=> [ 'id' => self::RAPID_STAGE, 'name' => 'Rapid Stage' ],
		self::STANDARD_PRODUCTION	=> [ 'id' => self::STANDARD_PRODUCTION, 'name' => 'Standard Production' ],
		self::STANDARD_STAGE		=> [ 'id' => self::STANDARD_STAGE, 'name' => 'Standard Stage' ]
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>