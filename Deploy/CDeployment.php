<?php

class CDeployment extends CBaseDeployment {

	protected $m_intDatabaseScriptReleaseId;
	protected $m_intEmployeeId;

	protected $m_strUsername;
	protected $m_strRevisionIds;
	protected $m_strLogFile;
	protected $m_strClusterName;
	protected $m_strQAStatus;
	protected $m_strTaskCount;

	/**
	 * Get Functions
	 */

	public function getDatabaseScriptReleaseId() {
		return $this->m_intDatabaseScriptReleaseId;
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function getQAStatus() {
		return $this->m_strQAStatus;
	}

	public function getTaskCount() {
		return $this->m_strTaskCount;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function getRevisionIds() {
		return $this->m_strRevisionIds;
	}

	public function getLogFile() {
		return $this->m_strLogFile;
	}

	public function getClusterName() {
		return $this->m_strClusterName;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['database_script_release_id'] ) )	$this->setDatabaseScriptReleaseId( $arrmixValues['database_script_release_id'] );
		if( true == isset( $arrmixValues['username'] ) )					$this->setUsername( $arrmixValues['username'] );
		if( true == isset( $arrmixValues['employee_id'] ) )					$this->setEmployeeId( $arrmixValues['employee_id'] );
		if( true == isset( $arrmixValues['log_file'] ) )					$this->setLogFile( $arrmixValues['log_file'] );
		if( true == isset( $arrmixValues['cluster_name'] ) )				$this->setClusterName( $arrmixValues['cluster_name'] );
		if( true == isset( $arrmixValues['qa_status'] ) )					$this->setQAStatus( $arrmixValues['qa_status'] );
		if( true == isset( $arrmixValues['task_count'] ) )					$this->setQAStatus( $arrmixValues['task_count'] );
		return;
	}

	public function setCustomDescription( $strDescriptionData ) {
		$this->m_strDescription = unserialize( $strDescriptionData );
	}

	public function setDatabaseScriptReleaseId( $intDatabaseScriptReleaseId ) {
		$this->m_intDatabaseScriptReleaseId = $intDatabaseScriptReleaseId;
	}

	public function setUsername( $strUsername ) {
		$this->m_strUsername = $strUsername;
	}

	public function setQAStatus( $strQAStatus ) {
		$this->m_strQAStatus = $strQAStatus;
	}

	public function setTaskCount( $strTaskCount ) {
		$this->m_strTaskCount = $strTaskCount;
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function setRevisionIds( $strRevisionIds ) {
		$this->m_strRevisionIds = $strRevisionIds;
	}

	public function setLogFile( $strLogFile ) {
		$this->m_strLogFile = $strLogFile;
	}

	public function setClustername( $strClusterName ) {
		$this->m_strClusterName = $strClusterName;
	}

 	/**
 	 * Validate Functions
 	 */

   public function valDeploymentTypeId() {

    	$boolIsValid = true;

    	if( true == is_null( $this->getDeploymentTypeId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deployment_type_id', 'Deployment type is required.' ) );
    	}
    	return $boolIsValid;

   }

   public function valScheduledDatetime() {

        $boolIsValid = true;

         if( true == is_null( $this->getScheduledDatetime() ) || 'now()' != $this->getScheduledDatetime() ) {
	        if( false == CValidation::validateDate( $this->getScheduledDatetime() ) ) {
	        	$boolIsValid = false;
	        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Scheduled Datetime is not a valid date.' ) );
	        }

	        if( true == isset( $this->m_strScheduledDatetime ) && false == is_null( $this->m_strScheduledDatetime ) ) {
	        	if( ( strtotime( $this->m_strScheduledDatetime ) ) < ( strtotime( date( 'm/d/Y' ) ) ) ) {
	        		$boolIsValid = false;
	                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Scheduled Datetime should be greater than current date.' ) );
	        	}
	        }
         }
        return $boolIsValid;
   }

    public function valDeploymentStartDatetime() {

    	$boolIsValid = true;

    	if( false == is_null( $this->getStartDatetime() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deployment_start_datetime', 'Deployment start date time is not null.' ) );
    	}

    	return $boolIsValid;

    }

	public function valSvnFileId( $objDatabase ) {

    	$intConfictingSvnFileCount = CDeployments::fetchRowCount( 'WHERE svn_file_id=' . $this->getSvnFileId() . ' AND deployment_type_id !=' . CDeploymentType::MANUAL_SQL_DRY_RUN, 'deployments', $objDatabase );

    	$boolIsValid = true;
    	if( 0 != $intConfictingSvnFileCount ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'svn_file_id', 'SQL deployment is already added for this file.' ) );
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valDeploymentTypeId();
            	$boolIsValid &= $this->valScheduledDatetime();
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valDeploymentStartDatetime();
            	break;

            case 'VALIDATE_INSERT_SQL_DEPLOYMENT':
            	$boolIsValid &= $this->valSvnFileId( $objDatabase );
            	break;

			case 'VALIDATE_INSERT_DEPLOYMENT_ID':
            	$boolIsValid &= $this->valDeploymentTypeId();
            	break;

			default:
            	// default case
            	$boolIsValid = false;
            	break;
        }

        return $boolIsValid;
    }

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == defined( 'CONFIG_DR_MODE' ) && true == CONFIG_DR_MODE ) {
			$this->setIsDr( true );
		}

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>