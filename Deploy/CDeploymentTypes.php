<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CDeploymentTypes
 * Do not add any new functions to this class.
 */

class CDeploymentTypes extends CBaseDeploymentTypes {

	public static function fetchDeploymentTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDeploymentType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDeploymentType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDeploymentType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDeploymentTypesByIds( $arrintDeploymentTypeIds, $objDatabase ) {
		$strSql = 'SELECT * FROM deployment_types WHERE id IN ( ' . implode( ',', $arrintDeploymentTypeIds ) . ' )';

		return self::fetchDeploymentTypes( $strSql, $objDatabase );
	}

	public static function getDeploymentTypesByPriority( $intPriorityNumber ) {

		switch( $intPriorityNumber ) {
		    case 1:
					$arrintDeploymentTypes		= array(
						CDeploymentType::CLEAR_SESSION,
						CDeploymentType::CLEAR_INTERFACE_CACHE,
		                CDeploymentType::CLEAR_CACHE,
		                CDeploymentType::MIGRATE_COMPANY,
		                CDeploymentType::RELOAD_HARDWARE_STATUS,
		                CDeploymentType::CHECK21_IMAGE_RESTORATION,
		                CDeploymentType::ROP_DATABASE_SCRIPT_FILES,
		                CDeploymentType::BLOCK_BRANCH_COMMITS,
		                CDeploymentType::MOUNT_TEST_CLIENTS_MAPPING,
		                CDeploymentType::UPDATE_DEPLOYMENT_SERVER,
		                CDeploymentType::SYSTEM_CRON,
		                CDeploymentType::SHARED_CRONS,
		                CDeploymentType::DATABASE_MAINTENANCE,
		                CDeploymentType::SYSTEM_MAINTENANCE,
		                CDeploymentType::PRODUCT_MAINTENANCE,
		                CDeploymentType::PUSH_CONFIG,
		                CDeploymentType::CLEAR_LESS_CACHE,
		                CDeploymentType::DATABASE_CRON_MAINTENANCE,
					    // CDeploymentType::RELOAD_SSL_F5_CONFIGURATION,
					    CDeploymentType::AUTO_POST_RELEASE_VERIFICATION_SMOKE,
						CDeploymentType::CREATE_BRANCH,
						CDeploymentType::CRONTAB_BACKUP,
						CDeploymentType::UPDATE_TASK_STATUS,
						CDeploymentType::UPDATE_CRON_ENTRIES,
						CDeploymentType::GIT_PUSH_CONFIG
					);
		        break;

		    case 2:
		        $arrintDeploymentTypes		= array(
			        CDeploymentType::PARTIAL_PUSH,
			        CDeploymentType::FULL_PUSH,
			        CDeploymentType::BUILD_PUSH_FILES,
			        CDeploymentType::UPDATE_CODE_ON_SERVER,
			        CDeploymentType::GIT_FULL_PUSH,
			        CDeploymentType::PARTIAL_PUSH_GIT,
			        CDeploymentType::ENTRATAMATION_VERSION,
			        CDeploymentType::SYSTEM_QUEUE_CONSUMER
		        );
		        break;

		    case 3:
		        $arrintDeploymentTypes		= array( CDeploymentType::DATABASE_DEPLOYMENT_EXECUTION, CDeploymentType::RELOAD_PSI_AUTOLOAD_FILES, CDeploymentType::UPDATE_BEACONBOX_SERVER );
		        break;

			default:
				$arrintDeploymentTypes		= array();
		}

		return $arrintDeploymentTypes;
	}

}
?>