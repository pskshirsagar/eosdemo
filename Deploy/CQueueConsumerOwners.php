<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CQueueConsumerOwners
 * Do not add any new functions to this class.
 */

class CQueueConsumerOwners extends CBaseQueueConsumerOwners {

	public static function fetchQueueConsumerOwnersByQueueConsumerIds( $arrintQueueConsumerIds, $objDatabase ) {

		if( false == valArr( $arrintQueueConsumerIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						queue_consumer_owners
					WHERE
						queue_consumer_id IN ( ' . implode( ',', $arrintQueueConsumerIds ) . ' )';

		return self::fetchQueueConsumerOwners( $strSql, $objDatabase );
	}

}
?>