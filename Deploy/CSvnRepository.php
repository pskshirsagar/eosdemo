<?php

class CSvnRepository extends CBaseSvnRepository {

	const PS_CORE_TRUNK				= 1;
	const PS_BRANCHES				= 2;
	const TAG						= 3;
	const PS_CORE					= 5;
	const PS_CORE_COMMON			= 6;
	const PS_CORE_CONFIG			= 7;
	const DOT_NET					= 9;
	const PSI_MIGRATION				= 10;
	const DM_REVENUE_MANAGEMENT		= 11;
	const PS_INTEGRATIONS			= 12;
	const PS_VOIP					= 13;
	const PS_DESKTOP				= 14;
	const PS_CORE_STAGE				= 26;
	const PS_CORE_PRODUCTION		= 27;
	const PS_CORE_COMMON_TRUNK		= 28;
	const PS_CORE_COMMON_STAGE		= 29;
	const PS_CORE_COMMON_PRODUCTION	= 30;
	const PS_CORE_MOBILE_SOURCE		= 886;
	const QA_AUTOMATION_SCRIPTS		= 3600;
	const PS_CORE_DBSCRIPTS			= 4516;
	const PS_LIBRARIES				= 11217;
	const PS_PAYMENTS				= 7000;

	const RV_SECURE_DBSCRIPTS		= 8134;
	const PS_CORE_HOME_AUTOMATION	= 35012;

	const ENTRATA_HOME_AUTOMATION	= 38925;
	const ENTRATA_CORE_TEST_AUTOMATION	= 37967;

	const PS_CORE_RAPIDSTAGE			= 4204;
	const PS_CORE_RAPIDPRODUCTION		= 4207;
	const PS_CORE_STANDARDSTAGE			= 4208;
	const PS_CORE_STANDARDPRODUCTION	= 4209;

	const PS_CORE_COMMON_RAPIDSTAGE			                = 4213;
	const PS_CORE_COMMON_RAPIDPRODUCTION	                = 4214;
	const PS_CORE_COMMON_STANDARDSTAGE		                = 4216;
	const PS_CORE_COMMON_STANDARDPRODUCTION	                = 4215;

	const PS_LIBRARIES_TRUNK				                = 15723;
	const PS_LIBRARIES_RAPIDSTAGE			                = 16236;
	const PS_LIBRARIES_RAPIDPRODUCTION		                = 16237;
	const PS_LIBRARIES_STANDARDSTAGE		                = 16238;
	const PS_LIBRARIES_STANDARDPRODUCTION	                = 16239;

	const PS_PAYMENTS_TRUNK					                = 15724;
	const PS_PAYMENTS_RAPIDSTAGE			                = 19004;
	const PS_PAYMENTS_RAPIDPRODUCTION		                = 19005;

	const ENTRATA_HOME_AUTOMATION_TRUNK                     = 38901;
	const ENTRATA_HOME_AUTOMATION_STAGE                     = 38902;
	const ENTRATA_HOME_AUTOMATION_PRODUCTION                = 38903;

	const  ENTRATA_CORE_TEST_AUTOMATION_TRUNK               = 39543;
	const  ENTRATA_CORE_TEST_AUTOMATION_RAPIDSTAGE          = 39544;
	const  ENTRATA_CORE_TEST_AUTOMATION_RAPIDPRODUCTION     = 39545;
	const  ENTRATA_CORE_TEST_AUTOMATION_STANDARDSTAGE       = 39603;
	const  ENTRATA_CORE_TEST_AUTOMATION_STANDARDPRODUCTION  = 39605;

	const TRUNK_BRANCH_NAME					= 'trunk';
	const STAGE_BRANCH_NAME					= 'stage';
	const PRODUCTION_BRANCH_NAME			= 'production';

	const RAPIDSTAGE_BRANCH_NAME 			= 'rapidstage';
	const RAPIDPRODUCTION_BRANCH_NAME 		= 'rapidproduction';
	const STANDARDSTAGE_BRANCH_NAME 		= 'standardstage';
	const STANDARDPRODUCTION_BRANCH_NAME 	= 'standardproduction';

	const PSCORE_TRUNK_SVN_PATH			= '/svn/PsCore/trunk/';
	const PSCORECOMMON_TRUNK_SVN_PATH 	= '/svn/PsCoreCommon/trunk';
	const PSCOREDBSCRIPTS_SVN_PATH 		= '/svn/PsCoreDbScripts/';

	const RVSECURE_SVN_PATH 		    = '/svn/PsResidentVerifySecure/trunk/DbScripts';

	public static $c_arrintSourceRepositoriesForCustomBranch = [
		self::PS_CORE 								=> 'PsCore',
		self::PS_CORE_COMMON 					=> 'PsCoreCommon',
	];
	public static $c_arrintCodeAuditSurveySvnRepositories = array(
		self::PS_CORE 								=> 'PsCore',
		self::PS_CORE_TRUNK 						=> 'PsCore Trunk',
		self::PS_CORE_STAGE 						=> 'PsCore Stage',
		self::PS_CORE_PRODUCTION 					=> 'PsCore Production',

		self::PS_CORE_RAPIDSTAGE					=> 'PsCore RapidStage',
		self::PS_CORE_RAPIDPRODUCTION				=> 'PsCore RapidProduction',
		self::PS_CORE_STANDARDSTAGE					=> 'PsCore StandardStage',
		self::PS_CORE_STANDARDPRODUCTION 			=> 'PsCore StandardProduction',

		self::PS_CORE_COMMON_TRUNK 					=> 'PsCoreCommon Trunk',
		self::PS_CORE_COMMON_STAGE 					=> 'PsCoreCommon Stage',
		self::PS_CORE_COMMON_PRODUCTION 			=> 'PsCoreCommon Production',
		self::PS_CORE_COMMON_RAPIDSTAGE				=> 'PsCoreCommon RapidStage',
		self::PS_CORE_COMMON_RAPIDPRODUCTION		=> 'PsCoreCommon RapidProduction',
		self::PS_CORE_COMMON_STANDARDSTAGE			=> 'PsCoreCommon StandardStage',
		self::PS_CORE_COMMON_STANDARDPRODUCTION		=> 'PsCoreCommon StandardProduction',

		self::PS_CORE_DBSCRIPTS 					=> 'PsCoreDbScripts',
	);

	public static $c_arrstrPrefixForSvnRepositories = array(
		self::PS_CORE 								=> 'T',
		self::PS_CORE_TRUNK 						=> 'T',
		self::PS_CORE_STAGE 						=> 'S',
		self::PS_CORE_PRODUCTION 					=> 'P',

		self::PS_CORE_RAPIDSTAGE					=> 'RS',
		self::PS_CORE_RAPIDPRODUCTION				=> 'RP',
		self::PS_CORE_STANDARDSTAGE					=> 'SS',
		self::PS_CORE_STANDARDPRODUCTION 			=> 'SP',

		self::PS_CORE_COMMON_TRUNK 					=> 'PCT',
		self::PS_CORE_COMMON_STAGE 					=> 'SC',
		self::PS_CORE_COMMON_PRODUCTION 			=> 'PC',
		self::PS_CORE_COMMON_RAPIDSTAGE				=> 'PCRS',
		self::PS_CORE_COMMON_RAPIDPRODUCTION		=> 'PCRP',
		self::PS_CORE_COMMON_STANDARDSTAGE			=> 'PCSS',
		self::PS_CORE_COMMON_STANDARDPRODUCTION		=> 'PCSP',

		self::PS_LIBRARIES_TRUNK                    => 'PSL',
		self::PS_LIBRARIES_RAPIDSTAGE               => 'PSLRS ',
		self::PS_LIBRARIES_RAPIDPRODUCTION          => 'PSLRP',
		self::PS_LIBRARIES_STANDARDSTAGE            => 'PSLSS',
		self::PS_LIBRARIES_STANDARDPRODUCTION       => 'PSLSP',

		self::PS_CORE_DBSCRIPTS                     => 'PCDS',

		self::PS_PAYMENTS_TRUNK                     => 'PST',
		self::PS_PAYMENTS_RAPIDPRODUCTION           => 'PSPRP',

	);

	public function valId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intId ) || ( 1 > $this->m_intId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Repository branch id does not appear valid.' ) );
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase, $boolCheckForDuplicateRecord = false ) {

		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		} elseif( false !== strpos( $this->getName(), ' ' ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Repository name can not contain spaces.' ) );
		} elseif( strlen( $this->m_strName ) > 50 ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name should be more than 50 characters.' ) );
		} else {
			$intSvnRepositoryCount = 0;
			$arrCustomBranches = [];
			if( $boolCheckForDuplicateRecord == true ) {

				$strSubSql = ( false == is_null( $this->getId() ) ) ? ' AND id <> ' . $this->getId() : '';
				if( self::PS_CORE == $this->getRepositoryId() ) {
					$intRepositoryName = self::$c_arrintSourceRepositoriesForCustomBranch[self::PS_CORE];
				} else if( self::PS_CORE_COMMON == $this->getRepositoryId() ) {
					$intRepositoryName = self::$c_arrintSourceRepositoriesForCustomBranch[self::PS_CORE_COMMON];
				}
				$strCommand = ' svn ls https://code.entrata.com/svn/' . $intRepositoryName . '/branches/';
				$strCustomBranches = shell_exec( $strCommand );
				$arrCustomBranches = explode( '/', $strCustomBranches );
				array_pop( $arrCustomBranches );
				$arrCustomBranches = array_map( 'trim', $arrCustomBranches );
				$intSvnRepositoryCount = CSvnRepositories::fetchRowCount( 'WHERE name = \'' . $this->getName() . '\'' . $strSubSql . ' AND deleted_by Is NULL AND deleted_on Is Null', 'svn_repositories', $objDatabase );

			} elseif( false == is_null( $this->getRepositoryId() ) ) {
				$intSvnRepositoryCount = CSvnRepositories::fetchRowCount( 'WHERE name = \'' . $this->getName() . '\' AND repository_id = ' . $this->getRepositoryId() . ' AND deleted_by Is NULL AND deleted_on Is Null', 'svn_repositories', $objDatabase );
			}

			if( 0 != $intSvnRepositoryCount || true == in_array( $this->getName(), $arrCustomBranches ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Repository with name ' . $this->getName() . ' already exist.' ) );
			}

		}
		return $boolIsValid;
	}

	public function valUserId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSystemUserId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'Owner name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;

		if( false == isset( $this->m_strTaskId ) || ( 1 > $this->m_strTaskId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_id', 'Task id required.' ) );
		}

		return $boolIsValid;
	}

	public function valRepositoryId() {
		$boolIsValid = true;

		if( true == is_null( $this->getRepositoryId() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'repository_id', 'Repository name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valPath() {
		$boolIsValid 	= true;

		if( true == is_null( $this->getPath() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'path', 'Path is required.' ) );
		}
		if( strlen( $this->m_strPath ) > 80 ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'path', 'Path should not be more than 80 characters.' ) );
		}
		$arrstrPath = explode( '/', parse_url( $this->getPath(), PHP_URL_PATH ) );
		if( 4 > \Psi\Libraries\UtilFunctions\count( $arrstrPath ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'path', 'Path should have a slash after repo name, e.g. /svn/PsCore/.' ) );
		}

		if( 4 < \Psi\Libraries\UtilFunctions\count( $arrstrPath ) && '' === end( $arrstrPath ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'path', 'Path should not have a trailing slash after subfolder, e.g. /svn/PsCore/branches/rapidproduction.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailNotifications() {
		$boolIsValid = true;
		if( true == is_null( $this->getEmailNotifications() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Email address(s) are required.' ) );
		} else {
			if( false == CValidation::validateEmailAddresses( $this->getEmailNotifications() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Enter valid email address(s).' ) );
			}
		}
		return $boolIsValid;
	}

	public function valSvnRepositoryTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getSvnRepositoryTypeId() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'svn_repository_type_id', 'Repository type is required.' ) );
		}
		return $boolIsValid;
	}

	public function valExpectedEndDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getExpectedEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expected_end_date', 'Expected end date is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valRepositoryId();
				$boolIsValid &= $this->valSvnRepositoryTypeId();
				$boolIsValid &= $this->valName( $objDatabase, $boolCheckForDuplicateRecord = true );
				$boolIsValid &= $this->valUserId();
				$boolIsValid &= $this->valPath();

				if( true == $this->getIsAutomerge() ) {
					$boolIsValid &= $this->valExpectedEndDate();
				}

				$boolIsValid &= $this->valEmailNotifications();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valUserId();

				if( true == $this->getIsAutomerge() ) {
					$boolIsValid &= $this->valExpectedEndDate();
				}

				$boolIsValid &= $this->valEmailNotifications();
				break;

			case 'validate_repository':
				$boolIsValid &= $this->valName( $objDatabase, $boolCheckForDuplicateRecord = true );
				$boolIsValid &= $this->valPath();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>