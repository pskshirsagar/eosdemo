<?php

class CQueueConsumer extends CBaseQueueConsumer {

	protected $m_boolIsPublished;

	protected $m_intMinConsumersCount;

	protected $m_intMaxConsumersCount;

	const PROVIDER_RABBITMQ           = 'RABBIT_MQ';
	const ARCHITECTURE_PUBSUB         = 'PUB_SUB';
	const ARCHITECTURE_QUEUE_CONSUMER = 'QUEUE_CONSUMER';

	const QUEUE_PROVIDERS = [
		'RabbitMq' => self::PROVIDER_RABBITMQ
	];

	const QUEUE_ARCHITECTURE = [
		'QueueConsumer/RabbitMq' => self::ARCHITECTURE_QUEUE_CONSUMER,
		'PubSub'                 => self::ARCHITECTURE_PUBSUB
	];

	const QUEUE_APPLICATIONS = [
		'entrata' => 'Entrata',
		'client_admin' => 'Client Admin',
	];

	/*
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['min_consumers_count'] ) ) $this->setMinConsumersCount( $arrmixValues['min_consumers_count'] );
		if( isset( $arrmixValues['max_consumers_count'] ) ) $this->setMaxConsumersCount( $arrmixValues['max_consumers_count'] );
		if( isset( $arrmixValues['is_published'] ) ) $this->setIsPublished( stripcslashes( $arrmixValues['is_published'] ) );
		if( isset( $arrmixValues['dependent_database_type_ids'] ) ) $this->setDependentDatabaseTypeIds( $arrmixValues['dependent_database_type_ids'] );
		if( isset( $arrmixValues['is_newrelic_instrumentation_enabled'] ) ) $this->setIsNewrelicInstrumentationEnabled( $arrmixValues['is_newrelic_instrumentation_enabled'] );
		if( isset( $arrmixValues['is_remote_logging_enabled'] ) ) $this->setIsRemoteLoggingEnabled( $arrmixValues['is_remote_logging_enabled'] );
		if( isset( $arrmixValues['publishing_interval_minutes'] ) ) $this->setPublishingIntervalMinutes( $arrmixValues['publishing_interval_minutes'] );
		if( isset( $arrmixValues['is_limit_database_connections'] ) ) $this->setIsLimitDatabaseConnections( $arrmixValues['is_limit_database_connections'] );
		if( isset( $arrmixValues['max_database_connections'] ) ) $this->setMaxDatabaseConnections( ( int ) $arrmixValues['max_database_connections'] );
		if( isset( $arrmixValues['max_message_threshold'] ) ) $this->setMaxMessageThreshold( $arrmixValues['max_message_threshold'] );
		if( isset( $arrmixValues['message_count'] ) ) $this->setMessageCount( $arrmixValues['message_count'] );
		if( isset( $arrmixValues['is_alert_on_resolved'] ) ) $this->setIsAlertOnResolved( $arrmixValues['is_alert_on_resolved'] );
		if( isset( $arrmixValues['time_interval'] ) ) $this->setTimeInterval( $arrmixValues['time_interval'] );
		if( isset( $arrmixValues['message_rate_threshold'] ) ) $this->setMessageRateThreshold( $arrmixValues['message_rate_threshold'] );
		if( isset( $arrmixValues['deliver_rate_threshold'] ) ) $this->setDeliverRateThreshold( $arrmixValues['deliver_rate_threshold'] );
		if( isset( $arrmixValues['message_class_name'] ) ) $this->setMessageClassName( $arrmixValues['message_class_name'] );
	}

	/*
	 * Fetch Functions
	 */

	public function fetchQueueConsumerHardwares( $objDatabase ) {
		return CQueueConsumerHardwares::fetchQueueConsumerHardwaresByQueueConsumerId( $this->m_intId, $objDatabase );
	}

	public function fetchQueueConsumerOwners( $objDatabase ) {
		return CQueueConsumerOwners::fetchQueueConsumerOwnersByQueueConsumerId( $this->m_intId, $objDatabase );
	}

	public function createQueueConsumerHardware() {

		$objQueueConsumerHardware = new CQueueConsumerHardware();
		$objQueueConsumerHardware->setQueueConsumerId( $this->getId() );

		return $objQueueConsumerHardware;
	}

	public function valBasicQueueName( $strAction, $objDatabase ) {
		$boolIsValid = true;

		$objExistingQueueConsumer = CQueueConsumers::fetchQueueConsumerByBasicQueueName( $this->getBasicQueueName(), $objDatabase );

		if( VALIDATE_INSERT == $strAction && true == valObj( $objExistingQueueConsumer, 'CQueueConsumer' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'basic_queue_name',  'Queue consumer name already exist.' ) );
		}

		if( VALIDATE_UPDATE == $strAction && true == valObj( $objExistingQueueConsumer, 'CQueueConsumer' ) && $objExistingQueueConsumer->getId() != $this->getId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'basic_queue_name',  'Queue consumer name already exist.' ) );
		}

		if( 1 !== preg_match( '/^([a-z0-9_])+(\.([a-z0-9_])+)*$/', $this->getBasicQueueName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'basic_queue_name',  'Invalid queue consumer name format, Allowed characters are a-z 0-9 . _' ) );
		}
		// reserved names used for main systemd service files, should not be used as a basic queue name.
		if( true == in_array( strtolower( $this->getBasicQueueName() ), [ 'master.consumer', 'qc.update', 'qc.logs' ] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'basic_queue_name',  'Do not use reserved name for basic queue name :: ' . $this->getBasicQueueName() ) );
		}

		return $boolIsValid;
	}

	public function valConsumerClassName() {
		$boolIsValid = true;

		if( false == valStr( $this->getConsumerClassName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'consumer_class_name',  'Consumer class name should not be empty.' ) );
		}

		// We cannot check if the class exists from ClientAdmin, as it would be namespaced and present under Entrata application.
		if( self::ARCHITECTURE_PUBSUB == $this->getQueueArchitecture() ) {
			return $boolIsValid;
		}

		if( false == $this->getIsPaymentsSpecific() && false === strpos( $this->getConsumerClassName(), '\\EntrataHA\\' ) && false == class_exists( $this->getConsumerClassName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'consumer_class_name',  'Consumer class does not exist or it is not been autoloaded.' ) );
		}

		return $boolIsValid;
	}

	public function valAlertThreshold() {
		$boolIsValid = true;

		if( false == preg_match( '/^([0-9])*$/', $this->getMessageCount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_threshold', 'Invalid message threshold.It should be between 0-9' ) );
		}

		if( false == preg_match( '/^([0-9])*$/', $this->getMaxMessageThreshold() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_message_threshold', 'Invalid Message threshold.It should be between 0-9' ) );
		}

		if( false == preg_match( '/^([0-9])*$/', $this->getMessageRateThreshold() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_rate_threshold', 'Invalid message count for Proportion check.It should be between 0-9' ) );
		}

		if( false == preg_match( '/^([0-9])*$/', $this->getDeliverRateThreshold() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deliver_rate_threshold', 'Invalid delivery rate threshold.It should be between 0-9' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valBasicQueueName( $strAction, $objDatabase );
				$boolIsValid &= $this->valConsumerClassName();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valBasicQueueName( $strAction, $objDatabase );
				$boolIsValid &= $this->valConsumerClassName();
				$boolIsValid &= $this->valAlertThreshold();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setMinConsumersCount( $intMinConsumersCount ) {
		$this->m_intMinConsumersCount = CStrings::strToIntDef( $intMinConsumersCount, NULL, false );
	}

	public function getMinConsumersCount() {
		return $this->m_intMinConsumersCount;
	}

	public function setMaxConsumersCount( $intMaxConsumersCount ) {
		$this->m_intMaxConsumersCount = CStrings::strToIntDef( $intMaxConsumersCount, NULL, false );
	}

	public function getMaxConsumersCount() {
		return $this->m_intMaxConsumersCount;
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->m_boolIsPublished = CStrings::strToBool( $boolIsPublished );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function toArray() {
		$arrmixData = parent::toArray();
		$arrmixData['min_consumers_count']	= $this->getMinConsumersCount();
		$arrmixData['max_consumers_count']	= $this->getMaxConsumersCount();
		$arrmixData['is_published']			= $this->getIsPublished();
		return $arrmixData;
	}

}
?>