<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CCodeLibraries
 * Do not add any new functions to this class.
 */

class CCodeLibraries extends CBaseCodeLibraries {

	public static function fetchAllCodeLibraries( $objDatabase ) {
		$strSql = 'SELECT * FROM code_libraries ORDER BY library_name';
		return self::fetchCodeLibraries( $strSql, $objDatabase );
	}

	public static function fetchCodeLibrariesById( $intId, $objDatabase ) {
		return self::fetchCodeLibrary( 'SELECT * FROM code_libraries WHERE id = ' . ( int ) $intId, $objDatabase );

	}

	public static function fetchCodeLibrariesByIds( $arrintCodeLibraryIds, $objDatabase ) {
		return self::fetchCodeLibraries( sprintf( 'SELECT * FROM %s WHERE deleted_by Is NULL AND deleted_on Is NULL AND id IN( %s )', 'code_libraries', implode( ',', $arrintCodeLibraryIds ) ), $objDatabase );
	}

	public static function fetchAllPaginatedCodeLibraries( $intPageNo, $intPageSize, $objDeployDatabase,  $strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		if( false == is_null( $strOrderByField ) ) {

			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY cl.library_name, cl.updated_on DESC';
		}

		$strSql = 'SELECT
						cl.*,
						lt.licence_type_name,
						lt.is_opensource
					FROM
						code_libraries AS cl
						LEFT JOIN licence_types AS lt ON ( cl.licence_type_id = lt.id )
						LEFT JOIN product_code_libraries AS pcl ON ( cl.id = pcl.code_libraries_id )
						WHERE
						cl.deleted_by IS NULL
						AND cl.deleted_on IS NULL '
						. $strOrderBy . ' OFFSET ' . ( int ) $intOffset . '
						LIMIT ' . $intLimit;

		return self::fetchCodeLibraries( $strSql, $objDeployDatabase );
	}

	public static function fetchAllPaginatedCodeLibrariesCount( $objDatabase ) {
		$strWhere = ' WHERE deleted_by IS NULL AND deleted_on IS NULL';
		return self::fetchCodeLibraryCount( $strWhere, $objDatabase );
	}

	public static function fetchSearchedCodeLibraries( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
						cl.*,
						lt.licence_type_name,
						lt.is_opensource
					FROM
						code_libraries AS cl
						JOIN licence_types AS lt ON ( cl.licence_type_id = lt.id )
						LEFT JOIN product_code_libraries AS pcl ON ( cl.id = pcl.code_libraries_id )
					WHERE
						cl.deleted_by IS NULL
						AND cl.deleted_on IS NULL
						AND (
							cl.library_name ILIKE \'%' . implode( '%\' AND cl.library_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
							OR cl.library_name ILIKE \'%' . implode( '%\' AND cl.library_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
							)
					ORDER BY cl.id
					LIMIT 15';

		return self::fetchCodeLibraries( $strSql, $objDatabase );
	}

}
?>