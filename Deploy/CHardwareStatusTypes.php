<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CHardwareStatusTypes
 * Do not add any new functions to this class.
 */

class CHardwareStatusTypes extends CBaseHardwareStatusTypes {

	public static function fetchHardwareStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CHardwareStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

    public static function fetchHardwareStatusType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CHardwareStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchPublishedHardwareStatusTypes( $objDatabase ) {

    	$strSql = 'SELECT * FROM hardware_status_types WHERE is_published = 1';

    	return self::fetchHardwareStatusTypes( $strSql, $objDatabase );
    }

}
?>