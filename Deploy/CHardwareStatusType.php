<?php

class CHardwareStatusType extends CBaseHardwareStatusType {

	const UNUSED			= 1;
	const MAINTENANCE		= 2;
	const RETIRED			= 3;
	const INSTALLATION		= 4;
	const LIVE				= 5;

	// static array of published hardware status types
	public static $c_arrstrHardwareStatusTypes = array(
			self::UNUSED 			=> 'New',
			self::MAINTENANCE		=> 'Maintenance',
			self::RETIRED			=> 'Retired',
			self::INSTALLATION 		=> 'Installation',
			self::LIVE 				=> 'Live'
	);

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
           		break;

            default:
            	// empty
            	break;
        }

        return $boolIsValid;
    }

}
?>