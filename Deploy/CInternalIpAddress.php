<?php

class CInternalIpAddress extends CBaseInternalIpAddress {

    public function valName( $objDatabase ) {
        $boolIsValid = true;

        if( false == isset( $this->m_strName ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
        } else {

        	$strSql = ' WHERE LOWER(name) LIKE LOWER(\'' . $this->m_strName . '\')' . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' );
        	$intCount = CInternalIpAddresses::fetchInternalIpAddressCount( $strSql, $objDatabase );

        	if( 0 < $intCount ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Name', 'Name already exists.' ) );
        	}
        }

        return $boolIsValid;
    }

    public function valIpAddress( $objDatabase ) {
        $boolIsValid = true;

        if( true == is_null( $this->getIpAddress() ) ) {

        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'IP address is required.' ) );
        	return false;
        }

        if( false == CValidation::validateIpAddress( $this->getIpAddress() ) ) {

        	// Validating Range for set of two and three octate.
        	$strRegExRange = '/^([1-9]?\d|1\d\d|2[0-4]\d|25[0-5])((\.([1-9]?\d|1\d\d|2[0-4]\d|25[0-5]))|(\.([1-9]?\d|1\d\d|2[0-4]\d|25[0-5])\.([1-9]?\d|1\d\d|2[0-4]\d|25[0-5])))$/';

        	if( false == preg_match( $strRegExRange, $this->getIpAddress() ) ) {
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'Invalid IP address or range.' ) );
        		return false;
        	} else {
	        	$this->setIsRange( true );
        	}
        }

        $strSqlCondition	= ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
        $strSql				= 'WHERE lower( ip_address ) = \'' . trim( strtolower( addslashes( $this->getIpAddress() ) ) ) . '\'' . $strSqlCondition;
        $intCount			= CInternalIpAddresses::fetchInternalIpAddressCount( $strSql, $objDatabase );

        if( 0 < $intCount ) {
       		$boolIsValid = false;
       		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'IP address is already in use.' ) );
       	}

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valName( $objDatabase );
        		$boolIsValid &= $this->valIpAddress( $objDatabase );
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function insertOrUpdate( $intCurrentUserId, $objDataset, $boolReturnSqlOnly = false ) {
    	if( isset( $this->m_intId ) ) {
    		return $this->update( $intCurrentUserId, $objDataset, $boolReturnSqlOnly );
    	} else {
    		return $this->insert( $intCurrentUserId, $objDataset, $boolReturnSqlOnly );
    	}
    }

    /**
     * Other Functions
     *
     */

    public static function syncInternalIpAddresses( $objDatabase ) {

    	$strInternalIpAddresses = '';

    	$strSql = 'SELECT DISTINCT ip_address, is_range FROM internal_ip_addresses WHERE is_published = 1';
    	$arrstrInternalIpAddresses	= fetchData( $strSql, $objDatabase );

    	if( true == valArr( $arrstrInternalIpAddresses ) ) {
    		$strInternalIpAddresses = implode( ',', array_keys( rekeyArray( 'ip_address', $arrstrInternalIpAddresses ) ) );
    	}

		CCache::StoreObject( 'internalIpAddressess', $strInternalIpAddresses, NULL, [], DATA_CACHE_MEMORY );
    	return;
    }

	public static function syncClusterWiseInternalIpAddresses( $intClusterId, $objDatabase ) {
		$strSql = 'SELECT DISTINCT ip_address, is_range FROM internal_ip_addresses WHERE is_published = 1';
		$arrstrInternalIpAddresses	= fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrInternalIpAddresses ) ) {
			$strInternalIpAddresses = implode( ',', array_keys( rekeyArray( 'ip_address', $arrstrInternalIpAddresses ) ) );
		}

		$objRedisCache = new CRedisCache( true );

		$objRedisCache->save( $strInternalIpAddresses, 'internalIpAddressess' );
		return;
	}

}
?>