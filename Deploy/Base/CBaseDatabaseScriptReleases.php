<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CDatabaseScriptReleases
 * Do not add any new functions to this class.
 */

class CBaseDatabaseScriptReleases extends CEosPluralBase {

	/**
	 * @return CDatabaseScriptRelease[]
	 */
	public static function fetchDatabaseScriptReleases( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDatabaseScriptRelease', $objDatabase );
	}

	/**
	 * @return CDatabaseScriptRelease
	 */
	public static function fetchDatabaseScriptRelease( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDatabaseScriptRelease', $objDatabase );
	}

	public static function fetchDatabaseScriptReleaseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'database_script_releases', $objDatabase );
	}

	public static function fetchDatabaseScriptReleaseById( $intId, $objDatabase ) {
		return self::fetchDatabaseScriptRelease( sprintf( 'SELECT * FROM database_script_releases WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDatabaseScriptReleasesByDatabaseScriptBatchId( $intDatabaseScriptBatchId, $objDatabase ) {
		return self::fetchDatabaseScriptReleases( sprintf( 'SELECT * FROM database_script_releases WHERE database_script_batch_id = %d', ( int ) $intDatabaseScriptBatchId ), $objDatabase );
	}

	public static function fetchDatabaseScriptReleasesByDeploymentId( $intDeploymentId, $objDatabase ) {
		return self::fetchDatabaseScriptReleases( sprintf( 'SELECT * FROM database_script_releases WHERE deployment_id = %d', ( int ) $intDeploymentId ), $objDatabase );
	}

	public static function fetchDatabaseScriptReleasesByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchDatabaseScriptReleases( sprintf( 'SELECT * FROM database_script_releases WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

	public static function fetchDatabaseScriptReleasesByTaskReleaseId( $intTaskReleaseId, $objDatabase ) {
		return self::fetchDatabaseScriptReleases( sprintf( 'SELECT * FROM database_script_releases WHERE task_release_id = %d', ( int ) $intTaskReleaseId ), $objDatabase );
	}

}
?>