<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClientDataInstances
 * Do not add any new functions to this class.
 */

class CBaseClientDataInstances extends CEosPluralBase {

	/**
	 * @return CClientDataInstance[]
	 */
	public static function fetchClientDataInstances( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CClientDataInstance::class, $objDatabase );
	}

	/**
	 * @return CClientDataInstance
	 */
	public static function fetchClientDataInstance( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CClientDataInstance::class, $objDatabase );
	}

	public static function fetchClientDataInstanceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'client_data_instances', $objDatabase );
	}

	public static function fetchClientDataInstanceById( $intId, $objDatabase ) {
		return self::fetchClientDataInstance( sprintf( 'SELECT * FROM client_data_instances WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchClientDataInstancesByCid( $intCid, $objDatabase ) {
		return self::fetchClientDataInstances( sprintf( 'SELECT * FROM client_data_instances WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchClientDataInstancesByTrainingClientId( $intTrainingClientId, $objDatabase ) {
		return self::fetchClientDataInstances( sprintf( 'SELECT * FROM client_data_instances WHERE training_client_id = %d', $intTrainingClientId ), $objDatabase );
	}

	public static function fetchClientDataInstancesByParentClientId( $intParentClientId, $objDatabase ) {
		return self::fetchClientDataInstances( sprintf( 'SELECT * FROM client_data_instances WHERE parent_client_id = %d', $intParentClientId ), $objDatabase );
	}

	public static function fetchClientDataInstancesBySourceDatabaseId( $intSourceDatabaseId, $objDatabase ) {
		return self::fetchClientDataInstances( sprintf( 'SELECT * FROM client_data_instances WHERE source_database_id = %d', $intSourceDatabaseId ), $objDatabase );
	}

	public static function fetchClientDataInstancesByDestinationDatabaseId( $intDestinationDatabaseId, $objDatabase ) {
		return self::fetchClientDataInstances( sprintf( 'SELECT * FROM client_data_instances WHERE destination_database_id = %d', $intDestinationDatabaseId ), $objDatabase );
	}

	public static function fetchClientDataInstancesByHardwareId( $intHardwareId, $objDatabase ) {
		return self::fetchClientDataInstances( sprintf( 'SELECT * FROM client_data_instances WHERE hardware_id = %d', $intHardwareId ), $objDatabase );
	}

	public static function fetchClientDataInstancesByResetFrequencyTypeId( $intResetFrequencyTypeId, $objDatabase ) {
		return self::fetchClientDataInstances( sprintf( 'SELECT * FROM client_data_instances WHERE reset_frequency_type_id = %d', $intResetFrequencyTypeId ), $objDatabase );
	}

}
?>