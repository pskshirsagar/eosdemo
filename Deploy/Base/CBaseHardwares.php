<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CHardwares
 * Do not add any new functions to this class.
 */

class CBaseHardwares extends CEosPluralBase {

	/**
	 * @return CHardware[]
	 */
	public static function fetchHardwares( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CHardware::class, $objDatabase );
	}

	/**
	 * @return CHardware
	 */
	public static function fetchHardware( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CHardware::class, $objDatabase );
	}

	public static function fetchHardwareCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'hardwares', $objDatabase );
	}

	public static function fetchHardwareById( $intId, $objDatabase ) {
		return self::fetchHardware( sprintf( 'SELECT * FROM hardwares WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchHardwaresByHardwareTypeId( $intHardwareTypeId, $objDatabase ) {
		return self::fetchHardwares( sprintf( 'SELECT * FROM hardwares WHERE hardware_type_id = %d', ( int ) $intHardwareTypeId ), $objDatabase );
	}

	public static function fetchHardwaresByHardwareStatusTypeId( $intHardwareStatusTypeId, $objDatabase ) {
		return self::fetchHardwares( sprintf( 'SELECT * FROM hardwares WHERE hardware_status_type_id = %d', ( int ) $intHardwareStatusTypeId ), $objDatabase );
	}

	public static function fetchHardwaresByEnvironmentTypeId( $intEnvironmentTypeId, $objDatabase ) {
		return self::fetchHardwares( sprintf( 'SELECT * FROM hardwares WHERE environment_type_id = %d', ( int ) $intEnvironmentTypeId ), $objDatabase );
	}

	public static function fetchHardwaresByClusterId( $intClusterId, $objDatabase ) {
		return self::fetchHardwares( sprintf( 'SELECT * FROM hardwares WHERE cluster_id = %d', ( int ) $intClusterId ), $objDatabase );
	}

	public static function fetchHardwaresByMachineTypeId( $intMachineTypeId, $objDatabase ) {
		return self::fetchHardwares( sprintf( 'SELECT * FROM hardwares WHERE machine_type_id = %d', ( int ) $intMachineTypeId ), $objDatabase );
	}

	public static function fetchHardwaresByParentHardwareId( $intParentHardwareId, $objDatabase ) {
		return self::fetchHardwares( sprintf( 'SELECT * FROM hardwares WHERE parent_hardware_id = %d', ( int ) $intParentHardwareId ), $objDatabase );
	}

	public static function fetchHardwaresByInstanceId( $strInstanceId, $objDatabase ) {
		return self::fetchHardwares( sprintf( 'SELECT * FROM hardwares WHERE instance_id = \'%s\'', $strInstanceId ), $objDatabase );
	}

}
?>