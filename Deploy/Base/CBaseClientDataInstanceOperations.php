<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClientDataInstanceOperations
 * Do not add any new functions to this class.
 */

class CBaseClientDataInstanceOperations extends CEosPluralBase {

	/**
	 * @return CClientDataInstanceOperation[]
	 */
	public static function fetchClientDataInstanceOperations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CClientDataInstanceOperation::class, $objDatabase );
	}

	/**
	 * @return CClientDataInstanceOperation
	 */
	public static function fetchClientDataInstanceOperation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CClientDataInstanceOperation::class, $objDatabase );
	}

	public static function fetchClientDataInstanceOperationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'client_data_instance_operations', $objDatabase );
	}

	public static function fetchClientDataInstanceOperationById( $intId, $objDatabase ) {
		return self::fetchClientDataInstanceOperation( sprintf( 'SELECT * FROM client_data_instance_operations WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchClientDataInstanceOperationsByClientDataInstanceId( $intClientDataInstanceId, $objDatabase ) {
		return self::fetchClientDataInstanceOperations( sprintf( 'SELECT * FROM client_data_instance_operations WHERE client_data_instance_id = %d', $intClientDataInstanceId ), $objDatabase );
	}

	public static function fetchClientDataInstanceOperationsByClientDataInstanceOperationStatusTypeId( $intClientDataInstanceOperationStatusTypeId, $objDatabase ) {
		return self::fetchClientDataInstanceOperations( sprintf( 'SELECT * FROM client_data_instance_operations WHERE client_data_instance_operation_status_type_id = %d', $intClientDataInstanceOperationStatusTypeId ), $objDatabase );
	}

	public static function fetchClientDataInstanceOperationsByClientDataInstanceOperationTypeId( $intClientDataInstanceOperationTypeId, $objDatabase ) {
		return self::fetchClientDataInstanceOperations( sprintf( 'SELECT * FROM client_data_instance_operations WHERE client_data_instance_operation_type_id = %d', $intClientDataInstanceOperationTypeId ), $objDatabase );
	}

}
?>