<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CSystemUserPermissions
 * Do not add any new functions to this class.
 */

class CBaseSystemUserPermissions extends CEosPluralBase {

	/**
	 * @return CSystemUserPermission[]
	 */
	public static function fetchSystemUserPermissions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSystemUserPermission', $objDatabase );
	}

	/**
	 * @return CSystemUserPermission
	 */
	public static function fetchSystemUserPermission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSystemUserPermission', $objDatabase );
	}

	public static function fetchSystemUserPermissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_user_permissions', $objDatabase );
	}

	public static function fetchSystemUserPermissionById( $intId, $objDatabase ) {
		return self::fetchSystemUserPermission( sprintf( 'SELECT * FROM system_user_permissions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSystemUserPermissionsBySystemUserId( $intSystemUserId, $objDatabase ) {
		return self::fetchSystemUserPermissions( sprintf( 'SELECT * FROM system_user_permissions WHERE system_user_id = %d', ( int ) $intSystemUserId ), $objDatabase );
	}

	public static function fetchSystemUserPermissionsBySystemModuleId( $intSystemModuleId, $objDatabase ) {
		return self::fetchSystemUserPermissions( sprintf( 'SELECT * FROM system_user_permissions WHERE system_module_id = %d', ( int ) $intSystemModuleId ), $objDatabase );
	}

}
?>