<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CScriptExecutionCommands
 * Do not add any new functions to this class.
 */

class CBaseScriptExecutionCommands extends CEosPluralBase {

	/**
	 * @return CScriptExecutionCommand[]
	 */
	public static function fetchScriptExecutionCommands( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScriptExecutionCommand::class, $objDatabase );
	}

	/**
	 * @return CScriptExecutionCommand
	 */
	public static function fetchScriptExecutionCommand( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScriptExecutionCommand::class, $objDatabase );
	}

	public static function fetchScriptExecutionCommandCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'script_execution_commands', $objDatabase );
	}

	public static function fetchScriptExecutionCommandById( $intId, $objDatabase ) {
		return self::fetchScriptExecutionCommand( sprintf( 'SELECT * FROM script_execution_commands WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScriptExecutionCommandsByScriptId( $intScriptId, $objDatabase ) {
		return self::fetchScriptExecutionCommands( sprintf( 'SELECT * FROM script_execution_commands WHERE script_id = %d', ( int ) $intScriptId ), $objDatabase );
	}

	public static function fetchScriptExecutionCommandsByScriptDatabaseId( $intScriptDatabaseId, $objDatabase ) {
		return self::fetchScriptExecutionCommands( sprintf( 'SELECT * FROM script_execution_commands WHERE script_database_id = %d', ( int ) $intScriptDatabaseId ), $objDatabase );
	}

}
?>