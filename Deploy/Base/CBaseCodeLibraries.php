<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CCodeLibraries
 * Do not add any new functions to this class.
 */

class CBaseCodeLibraries extends CEosPluralBase {

	/**
	 * @return CCodeLibrary[]
	 */
	public static function fetchCodeLibraries( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCodeLibrary::class, $objDatabase );
	}

	/**
	 * @return CCodeLibrary
	 */
	public static function fetchCodeLibrary( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCodeLibrary::class, $objDatabase );
	}

	public static function fetchCodeLibraryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'code_libraries', $objDatabase );
	}

	public static function fetchCodeLibraryById( $intId, $objDatabase ) {
		return self::fetchCodeLibrary( sprintf( 'SELECT * FROM code_libraries WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCodeLibrariesByLicenceTypeId( $intLicenceTypeId, $objDatabase ) {
		return self::fetchCodeLibraries( sprintf( 'SELECT * FROM code_libraries WHERE licence_type_id = %d', ( int ) $intLicenceTypeId ), $objDatabase );
	}

}
?>