<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CRepositoryTypes
 * Do not add any new functions to this class.
 */

class CBaseRepositoryTypes extends CEosPluralBase {

	/**
	 * @return CRepositoryType[]
	 */
	public static function fetchRepositoryTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CRepositoryType', $objDatabase );
	}

	/**
	 * @return CRepositoryType
	 */
	public static function fetchRepositoryType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRepositoryType', $objDatabase );
	}

	public static function fetchRepositoryTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'repository_types', $objDatabase );
	}

	public static function fetchRepositoryTypeById( $intId, $objDatabase ) {
		return self::fetchRepositoryType( sprintf( 'SELECT * FROM repository_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>