<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CCleanupDetails
 * Do not add any new functions to this class.
 */

class CBaseCleanupDetails extends CEosPluralBase {

	/**
	 * @return CCleanupDetail[]
	 */
	public static function fetchCleanupDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCleanupDetail', $objDatabase );
	}

	/**
	 * @return CCleanupDetail
	 */
	public static function fetchCleanupDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCleanupDetail', $objDatabase );
	}

	public static function fetchCleanupDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cleanup_details', $objDatabase );
	}

	public static function fetchCleanupDetailById( $intId, $objDatabase ) {
		return self::fetchCleanupDetail( sprintf( 'SELECT * FROM cleanup_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>