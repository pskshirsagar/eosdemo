<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CDeploymentProducts
 * Do not add any new functions to this class.
 */

class CBaseDeploymentProducts extends CEosPluralBase {

	/**
	 * @return CDeploymentProduct[]
	 */
	public static function fetchDeploymentProducts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDeploymentProduct::class, $objDatabase );
	}

	/**
	 * @return CDeploymentProduct
	 */
	public static function fetchDeploymentProduct( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDeploymentProduct::class, $objDatabase );
	}

	public static function fetchDeploymentProductCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'deployment_products', $objDatabase );
	}

	public static function fetchDeploymentProductById( $intId, $objDatabase ) {
		return self::fetchDeploymentProduct( sprintf( 'SELECT * FROM deployment_products WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDeploymentProductsByClusterId( $intClusterId, $objDatabase ) {
		return self::fetchDeploymentProducts( sprintf( 'SELECT * FROM deployment_products WHERE cluster_id = %d', ( int ) $intClusterId ), $objDatabase );
	}

	public static function fetchDeploymentProductsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchDeploymentProducts( sprintf( 'SELECT * FROM deployment_products WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

}
?>