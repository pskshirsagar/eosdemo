<?php

class CBaseHardwareLog extends CEosSingularBase {

    protected $m_intId;
    protected $m_intHardwareLogTypeId;
    protected $m_intCount;
    protected $m_strIpAddress;
    protected $m_strLogDatetime;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['hardware_log_type_id'] ) && $boolDirectSet ) $this->m_intHardwareLogTypeId = trim( $arrValues['hardware_log_type_id'] ); else if( isset( $arrValues['hardware_log_type_id'] ) ) $this->setHardwareLogTypeId( $arrValues['hardware_log_type_id'] );
        if( isset( $arrValues['count'] ) && $boolDirectSet ) $this->m_intCount = trim( $arrValues['count'] ); else if( isset( $arrValues['count'] ) ) $this->setCount( $arrValues['count'] );
        if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->m_strIpAddress = trim( stripcslashes( $arrValues['ip_address'] ) ); else if( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ip_address'] ) : $arrValues['ip_address'] );
        if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->m_strLogDatetime = trim( $arrValues['log_datetime'] ); else if( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setHardwareLogTypeId( $intHardwareLogTypeId ) {
        $this->m_intHardwareLogTypeId = CStrings::strToIntDef( $intHardwareLogTypeId, NULL, false );
    }

    public function getHardwareLogTypeId() {
        return $this->m_intHardwareLogTypeId;
    }

    public function sqlHardwareLogTypeId() {
        return ( true == isset( $this->m_intHardwareLogTypeId ) ) ? (string) $this->m_intHardwareLogTypeId : 'NULL';
    }

    public function setCount( $intCount ) {
        $this->m_intCount = CStrings::strToIntDef( $intCount, NULL, false );
    }

    public function getCount() {
        return $this->m_intCount;
    }

    public function sqlCount() {
        return ( true == isset( $this->m_intCount ) ) ? (string) $this->m_intCount : 'NULL';
    }

    public function setIpAddress( $strIpAddress ) {
        $this->m_strIpAddress = CStrings::strTrimDef( $strIpAddress, 50, NULL, true );
    }

    public function getIpAddress() {
        return $this->m_strIpAddress;
    }

    public function sqlIpAddress() {
        return ( true == isset( $this->m_strIpAddress ) ) ? '\'' . addslashes( $this->m_strIpAddress ) . '\'' : 'NULL';
    }

    public function setLogDatetime( $strLogDatetime ) {
        $this->m_strLogDatetime = CStrings::strTrimDef( $strLogDatetime, -1, NULL, true );
    }

    public function getLogDatetime() {
        return $this->m_strLogDatetime;
    }

    public function sqlLogDatetime() {
        return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.hardware_logs_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO
					  public.hardware_logs
					VALUES ( ' .
	                    $strId . ', ' .
 		                $this->sqlHardwareLogTypeId() . ', ' .
 		                $this->sqlCount() . ', ' .
 		                $this->sqlIpAddress() . ', ' .
 		                $this->sqlLogDatetime() . ', ' .
                    	(int) $intCurrentUserId . ', ' .
 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.hardware_logs
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hardware_log_type_id = ' . $this->sqlHardwareLogTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlHardwareLogTypeId() ) != $this->getOriginalValueByFieldName ( 'hardware_log_type_id' ) ) { $arrstrOriginalValueChanges['hardware_log_type_id'] = $this->sqlHardwareLogTypeId(); $strSql .= ' hardware_log_type_id = ' . $this->sqlHardwareLogTypeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' count = ' . $this->sqlCount() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCount() ) != $this->getOriginalValueByFieldName ( 'count' ) ) { $arrstrOriginalValueChanges['count'] = $this->sqlCount(); $strSql .= ' count = ' . $this->sqlCount() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIpAddress() ) != $this->getOriginalValueByFieldName ( 'ip_address' ) ) { $arrstrOriginalValueChanges['ip_address'] = $this->sqlIpAddress(); $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLogDatetime() ) != $this->getOriginalValueByFieldName ( 'log_datetime' ) ) { $arrstrOriginalValueChanges['log_datetime'] = $this->sqlLogDatetime(); $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }

        $strSql = substr( $strSql, 0, -1 );

        $strSql .= ' WHERE
						id = ' . (int) $this->sqlId() . ';';

        if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
        } else {
			if( true == $boolUpdate ) {
			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
				    if( true == $this->getAllowDifferentialUpdate() ) {
				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
				    }
			    } else {
			        return false;
				}
			}
			return true;
		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.hardware_logs WHERE id = ' . (int) $this->sqlId() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.hardware_logs_id_seq', $objDatabase );
    }

}
?>