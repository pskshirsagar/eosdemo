<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CQueueConsumerOwners
 * Do not add any new functions to this class.
 */

class CBaseQueueConsumerOwners extends CEosPluralBase {

	/**
	 * @return CQueueConsumerOwner[]
	 */
	public static function fetchQueueConsumerOwners( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CQueueConsumerOwner', $objDatabase );
	}

	/**
	 * @return CQueueConsumerOwner
	 */
	public static function fetchQueueConsumerOwner( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CQueueConsumerOwner', $objDatabase );
	}

	public static function fetchQueueConsumerOwnerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'queue_consumer_owners', $objDatabase );
	}

	public static function fetchQueueConsumerOwnerById( $intId, $objDatabase ) {
		return self::fetchQueueConsumerOwner( sprintf( 'SELECT * FROM queue_consumer_owners WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchQueueConsumerOwnersByQueueConsumerId( $intQueueConsumerId, $objDatabase ) {
		return self::fetchQueueConsumerOwners( sprintf( 'SELECT * FROM queue_consumer_owners WHERE queue_consumer_id = %d', ( int ) $intQueueConsumerId ), $objDatabase );
	}

	public static function fetchQueueConsumerOwnersByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchQueueConsumerOwners( sprintf( 'SELECT * FROM queue_consumer_owners WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>