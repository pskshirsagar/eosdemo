<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CHardwareProducts
 * Do not add any new functions to this class.
 */

class CBaseHardwareProducts extends CEosPluralBase {

	/**
	 * @return CHardwareProduct[]
	 */
	public static function fetchHardwareProducts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CHardwareProduct', $objDatabase );
	}

	/**
	 * @return CHardwareProduct
	 */
	public static function fetchHardwareProduct( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CHardwareProduct', $objDatabase );
	}

	public static function fetchHardwareProductCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'hardware_products', $objDatabase );
	}

	public static function fetchHardwareProductById( $intId, $objDatabase ) {
		return self::fetchHardwareProduct( sprintf( 'SELECT * FROM hardware_products WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchHardwareProductsByHardwareId( $intHardwareId, $objDatabase ) {
		return self::fetchHardwareProducts( sprintf( 'SELECT * FROM hardware_products WHERE hardware_id = %d', ( int ) $intHardwareId ), $objDatabase );
	}

	public static function fetchHardwareProductsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchHardwareProducts( sprintf( 'SELECT * FROM hardware_products WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

}
?>