<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CHardwareTypes
 * Do not add any new functions to this class.
 */

class CBaseHardwareTypes extends CEosPluralBase {

	/**
	 * @return CHardwareType[]
	 */
	public static function fetchHardwareTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CHardwareType', $objDatabase );
	}

	/**
	 * @return CHardwareType
	 */
	public static function fetchHardwareType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CHardwareType', $objDatabase );
	}

	public static function fetchHardwareTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'hardware_types', $objDatabase );
	}

	public static function fetchHardwareTypeById( $intId, $objDatabase ) {
		return self::fetchHardwareType( sprintf( 'SELECT * FROM hardware_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>