<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CScripts
 * Do not add any new functions to this class.
 */

class CBaseScripts extends CEosPluralBase {

	/**
	 * @return CScript[]
	 */
	public static function fetchScripts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScript::class, $objDatabase );
	}

	/**
	 * @return CScript
	 */
	public static function fetchScript( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScript::class, $objDatabase );
	}

	public static function fetchScriptCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scripts', $objDatabase );
	}

	public static function fetchScriptById( $intId, $objDatabase ) {
		return self::fetchScript( sprintf( 'SELECT * FROM scripts WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScriptsByScriptTypeId( $intScriptTypeId, $objDatabase ) {
		return self::fetchScripts( sprintf( 'SELECT * FROM scripts WHERE script_type_id = %d', ( int ) $intScriptTypeId ), $objDatabase );
	}

	public static function fetchScriptsByScriptExecutorTypeId( $intScriptExecutorTypeId, $objDatabase ) {
		return self::fetchScripts( sprintf( 'SELECT * FROM scripts WHERE script_executor_type_id = %d', ( int ) $intScriptExecutorTypeId ), $objDatabase );
	}

}
?>