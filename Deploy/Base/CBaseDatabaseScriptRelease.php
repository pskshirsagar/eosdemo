<?php

class CBaseDatabaseScriptRelease extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.database_script_releases';

	protected $m_intId;
	protected $m_intDatabaseScriptBatchId;
	protected $m_intDeploymentId;
	protected $m_intDatabaseId;
	protected $m_intTaskReleaseId;
	protected $m_strReleaseDatetime;
	protected $m_strStartDatetime;
	protected $m_strEndDatetime;
	protected $m_strFailedOn;
	protected $m_intIsDryRun;
	protected $m_strScriptOutput;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['database_script_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseScriptBatchId', trim( $arrValues['database_script_batch_id'] ) ); elseif( isset( $arrValues['database_script_batch_id'] ) ) $this->setDatabaseScriptBatchId( $arrValues['database_script_batch_id'] );
		if( isset( $arrValues['deployment_id'] ) && $boolDirectSet ) $this->set( 'm_intDeploymentId', trim( $arrValues['deployment_id'] ) ); elseif( isset( $arrValues['deployment_id'] ) ) $this->setDeploymentId( $arrValues['deployment_id'] );
		if( isset( $arrValues['database_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseId', trim( $arrValues['database_id'] ) ); elseif( isset( $arrValues['database_id'] ) ) $this->setDatabaseId( $arrValues['database_id'] );
		if( isset( $arrValues['task_release_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskReleaseId', trim( $arrValues['task_release_id'] ) ); elseif( isset( $arrValues['task_release_id'] ) ) $this->setTaskReleaseId( $arrValues['task_release_id'] );
		if( isset( $arrValues['release_datetime'] ) && $boolDirectSet ) $this->set( 'm_strReleaseDatetime', trim( $arrValues['release_datetime'] ) ); elseif( isset( $arrValues['release_datetime'] ) ) $this->setReleaseDatetime( $arrValues['release_datetime'] );
		if( isset( $arrValues['start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartDatetime', trim( $arrValues['start_datetime'] ) ); elseif( isset( $arrValues['start_datetime'] ) ) $this->setStartDatetime( $arrValues['start_datetime'] );
		if( isset( $arrValues['end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndDatetime', trim( $arrValues['end_datetime'] ) ); elseif( isset( $arrValues['end_datetime'] ) ) $this->setEndDatetime( $arrValues['end_datetime'] );
		if( isset( $arrValues['failed_on'] ) && $boolDirectSet ) $this->set( 'm_strFailedOn', trim( $arrValues['failed_on'] ) ); elseif( isset( $arrValues['failed_on'] ) ) $this->setFailedOn( $arrValues['failed_on'] );
		if( isset( $arrValues['is_dry_run'] ) && $boolDirectSet ) $this->set( 'm_intIsDryRun', trim( $arrValues['is_dry_run'] ) ); elseif( isset( $arrValues['is_dry_run'] ) ) $this->setIsDryRun( $arrValues['is_dry_run'] );
		if( isset( $arrValues['script_output'] ) && $boolDirectSet ) $this->set( 'm_strScriptOutput', trim( stripcslashes( $arrValues['script_output'] ) ) ); elseif( isset( $arrValues['script_output'] ) ) $this->setScriptOutput( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['script_output'] ) : $arrValues['script_output'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDatabaseScriptBatchId( $intDatabaseScriptBatchId ) {
		$this->set( 'm_intDatabaseScriptBatchId', CStrings::strToIntDef( $intDatabaseScriptBatchId, NULL, false ) );
	}

	public function getDatabaseScriptBatchId() {
		return $this->m_intDatabaseScriptBatchId;
	}

	public function sqlDatabaseScriptBatchId() {
		return ( true == isset( $this->m_intDatabaseScriptBatchId ) ) ? ( string ) $this->m_intDatabaseScriptBatchId : 'NULL';
	}

	public function setDeploymentId( $intDeploymentId ) {
		$this->set( 'm_intDeploymentId', CStrings::strToIntDef( $intDeploymentId, NULL, false ) );
	}

	public function getDeploymentId() {
		return $this->m_intDeploymentId;
	}

	public function sqlDeploymentId() {
		return ( true == isset( $this->m_intDeploymentId ) ) ? ( string ) $this->m_intDeploymentId : 'NULL';
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->set( 'm_intDatabaseId', CStrings::strToIntDef( $intDatabaseId, NULL, false ) );
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function sqlDatabaseId() {
		return ( true == isset( $this->m_intDatabaseId ) ) ? ( string ) $this->m_intDatabaseId : 'NULL';
	}

	public function setTaskReleaseId( $intTaskReleaseId ) {
		$this->set( 'm_intTaskReleaseId', CStrings::strToIntDef( $intTaskReleaseId, NULL, false ) );
	}

	public function getTaskReleaseId() {
		return $this->m_intTaskReleaseId;
	}

	public function sqlTaskReleaseId() {
		return ( true == isset( $this->m_intTaskReleaseId ) ) ? ( string ) $this->m_intTaskReleaseId : 'NULL';
	}

	public function setReleaseDatetime( $strReleaseDatetime ) {
		$this->set( 'm_strReleaseDatetime', CStrings::strTrimDef( $strReleaseDatetime, -1, NULL, true ) );
	}

	public function getReleaseDatetime() {
		return $this->m_strReleaseDatetime;
	}

	public function sqlReleaseDatetime() {
		return ( true == isset( $this->m_strReleaseDatetime ) ) ? '\'' . $this->m_strReleaseDatetime . '\'' : 'NOW()';
	}

	public function setStartDatetime( $strStartDatetime ) {
		$this->set( 'm_strStartDatetime', CStrings::strTrimDef( $strStartDatetime, -1, NULL, true ) );
	}

	public function getStartDatetime() {
		return $this->m_strStartDatetime;
	}

	public function sqlStartDatetime() {
		return ( true == isset( $this->m_strStartDatetime ) ) ? '\'' . $this->m_strStartDatetime . '\'' : 'NULL';
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->set( 'm_strEndDatetime', CStrings::strTrimDef( $strEndDatetime, -1, NULL, true ) );
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function sqlEndDatetime() {
		return ( true == isset( $this->m_strEndDatetime ) ) ? '\'' . $this->m_strEndDatetime . '\'' : 'NULL';
	}

	public function setFailedOn( $strFailedOn ) {
		$this->set( 'm_strFailedOn', CStrings::strTrimDef( $strFailedOn, -1, NULL, true ) );
	}

	public function getFailedOn() {
		return $this->m_strFailedOn;
	}

	public function sqlFailedOn() {
		return ( true == isset( $this->m_strFailedOn ) ) ? '\'' . $this->m_strFailedOn . '\'' : 'NULL';
	}

	public function setIsDryRun( $intIsDryRun ) {
		$this->set( 'm_intIsDryRun', CStrings::strToIntDef( $intIsDryRun, NULL, false ) );
	}

	public function getIsDryRun() {
		return $this->m_intIsDryRun;
	}

	public function sqlIsDryRun() {
		return ( true == isset( $this->m_intIsDryRun ) ) ? ( string ) $this->m_intIsDryRun : 'NULL';
	}

	public function setScriptOutput( $strScriptOutput ) {
		$this->set( 'm_strScriptOutput', CStrings::strTrimDef( $strScriptOutput, -1, NULL, true ) );
	}

	public function getScriptOutput() {
		return $this->m_strScriptOutput;
	}

	public function sqlScriptOutput() {
		return ( true == isset( $this->m_strScriptOutput ) ) ? '\'' . addslashes( $this->m_strScriptOutput ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, database_script_batch_id, deployment_id, database_id, task_release_id, release_datetime, start_datetime, end_datetime, failed_on, is_dry_run, script_output, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlDatabaseScriptBatchId() . ', ' .
		          $this->sqlDeploymentId() . ', ' .
		          $this->sqlDatabaseId() . ', ' .
		          $this->sqlTaskReleaseId() . ', ' .
		          $this->sqlReleaseDatetime() . ', ' .
		          $this->sqlStartDatetime() . ', ' .
		          $this->sqlEndDatetime() . ', ' .
		          $this->sqlFailedOn() . ', ' .
		          $this->sqlIsDryRun() . ', ' .
		          $this->sqlScriptOutput() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_script_batch_id = ' . $this->sqlDatabaseScriptBatchId(). ',' ; } elseif( true == array_key_exists( 'DatabaseScriptBatchId', $this->getChangedColumns() ) ) { $strSql .= ' database_script_batch_id = ' . $this->sqlDatabaseScriptBatchId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deployment_id = ' . $this->sqlDeploymentId(). ',' ; } elseif( true == array_key_exists( 'DeploymentId', $this->getChangedColumns() ) ) { $strSql .= ' deployment_id = ' . $this->sqlDeploymentId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId(). ',' ; } elseif( true == array_key_exists( 'DatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_release_id = ' . $this->sqlTaskReleaseId(). ',' ; } elseif( true == array_key_exists( 'TaskReleaseId', $this->getChangedColumns() ) ) { $strSql .= ' task_release_id = ' . $this->sqlTaskReleaseId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' release_datetime = ' . $this->sqlReleaseDatetime(). ',' ; } elseif( true == array_key_exists( 'ReleaseDatetime', $this->getChangedColumns() ) ) { $strSql .= ' release_datetime = ' . $this->sqlReleaseDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime(). ',' ; } elseif( true == array_key_exists( 'StartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime(). ',' ; } elseif( true == array_key_exists( 'EndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn(). ',' ; } elseif( true == array_key_exists( 'FailedOn', $this->getChangedColumns() ) ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_dry_run = ' . $this->sqlIsDryRun(). ',' ; } elseif( true == array_key_exists( 'IsDryRun', $this->getChangedColumns() ) ) { $strSql .= ' is_dry_run = ' . $this->sqlIsDryRun() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' script_output = ' . $this->sqlScriptOutput(). ',' ; } elseif( true == array_key_exists( 'ScriptOutput', $this->getChangedColumns() ) ) { $strSql .= ' script_output = ' . $this->sqlScriptOutput() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'database_script_batch_id' => $this->getDatabaseScriptBatchId(),
			'deployment_id' => $this->getDeploymentId(),
			'database_id' => $this->getDatabaseId(),
			'task_release_id' => $this->getTaskReleaseId(),
			'release_datetime' => $this->getReleaseDatetime(),
			'start_datetime' => $this->getStartDatetime(),
			'end_datetime' => $this->getEndDatetime(),
			'failed_on' => $this->getFailedOn(),
			'is_dry_run' => $this->getIsDryRun(),
			'script_output' => $this->getScriptOutput(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>