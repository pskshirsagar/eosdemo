<?php

class CBaseMigrationRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.migration_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMigrationStatusTypeId;
	protected $m_intSourceDatabaseId;
	protected $m_intDestinationDatabaseId;
	protected $m_intTaskId;
	protected $m_strStartTime;
	protected $m_strEndTime;
	protected $m_strFailedOn;
	protected $m_strNextCommandParameters;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intMigrationStatusTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['migration_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMigrationStatusTypeId', trim( $arrValues['migration_status_type_id'] ) ); elseif( isset( $arrValues['migration_status_type_id'] ) ) $this->setMigrationStatusTypeId( $arrValues['migration_status_type_id'] );
		if( isset( $arrValues['source_database_id'] ) && $boolDirectSet ) $this->set( 'm_intSourceDatabaseId', trim( $arrValues['source_database_id'] ) ); elseif( isset( $arrValues['source_database_id'] ) ) $this->setSourceDatabaseId( $arrValues['source_database_id'] );
		if( isset( $arrValues['destination_database_id'] ) && $boolDirectSet ) $this->set( 'm_intDestinationDatabaseId', trim( $arrValues['destination_database_id'] ) ); elseif( isset( $arrValues['destination_database_id'] ) ) $this->setDestinationDatabaseId( $arrValues['destination_database_id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['start_time'] ) && $boolDirectSet ) $this->set( 'm_strStartTime', trim( $arrValues['start_time'] ) ); elseif( isset( $arrValues['start_time'] ) ) $this->setStartTime( $arrValues['start_time'] );
		if( isset( $arrValues['end_time'] ) && $boolDirectSet ) $this->set( 'm_strEndTime', trim( $arrValues['end_time'] ) ); elseif( isset( $arrValues['end_time'] ) ) $this->setEndTime( $arrValues['end_time'] );
		if( isset( $arrValues['failed_on'] ) && $boolDirectSet ) $this->set( 'm_strFailedOn', trim( $arrValues['failed_on'] ) ); elseif( isset( $arrValues['failed_on'] ) ) $this->setFailedOn( $arrValues['failed_on'] );
		if( isset( $arrValues['next_command_parameters'] ) && $boolDirectSet ) $this->set( 'm_strNextCommandParameters', trim( stripcslashes( $arrValues['next_command_parameters'] ) ) ); elseif( isset( $arrValues['next_command_parameters'] ) ) $this->setNextCommandParameters( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['next_command_parameters'] ) : $arrValues['next_command_parameters'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMigrationStatusTypeId( $intMigrationStatusTypeId ) {
		$this->set( 'm_intMigrationStatusTypeId', CStrings::strToIntDef( $intMigrationStatusTypeId, NULL, false ) );
	}

	public function getMigrationStatusTypeId() {
		return $this->m_intMigrationStatusTypeId;
	}

	public function sqlMigrationStatusTypeId() {
		return ( true == isset( $this->m_intMigrationStatusTypeId ) ) ? ( string ) $this->m_intMigrationStatusTypeId : '1';
	}

	public function setSourceDatabaseId( $intSourceDatabaseId ) {
		$this->set( 'm_intSourceDatabaseId', CStrings::strToIntDef( $intSourceDatabaseId, NULL, false ) );
	}

	public function getSourceDatabaseId() {
		return $this->m_intSourceDatabaseId;
	}

	public function sqlSourceDatabaseId() {
		return ( true == isset( $this->m_intSourceDatabaseId ) ) ? ( string ) $this->m_intSourceDatabaseId : 'NULL';
	}

	public function setDestinationDatabaseId( $intDestinationDatabaseId ) {
		$this->set( 'm_intDestinationDatabaseId', CStrings::strToIntDef( $intDestinationDatabaseId, NULL, false ) );
	}

	public function getDestinationDatabaseId() {
		return $this->m_intDestinationDatabaseId;
	}

	public function sqlDestinationDatabaseId() {
		return ( true == isset( $this->m_intDestinationDatabaseId ) ) ? ( string ) $this->m_intDestinationDatabaseId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setStartTime( $strStartTime ) {
		$this->set( 'm_strStartTime', CStrings::strTrimDef( $strStartTime, -1, NULL, true ) );
	}

	public function getStartTime() {
		return $this->m_strStartTime;
	}

	public function sqlStartTime() {
		return ( true == isset( $this->m_strStartTime ) ) ? '\'' . $this->m_strStartTime . '\'' : 'NULL';
	}

	public function setEndTime( $strEndTime ) {
		$this->set( 'm_strEndTime', CStrings::strTrimDef( $strEndTime, -1, NULL, true ) );
	}

	public function getEndTime() {
		return $this->m_strEndTime;
	}

	public function sqlEndTime() {
		return ( true == isset( $this->m_strEndTime ) ) ? '\'' . $this->m_strEndTime . '\'' : 'NULL';
	}

	public function setFailedOn( $strFailedOn ) {
		$this->set( 'm_strFailedOn', CStrings::strTrimDef( $strFailedOn, -1, NULL, true ) );
	}

	public function getFailedOn() {
		return $this->m_strFailedOn;
	}

	public function sqlFailedOn() {
		return ( true == isset( $this->m_strFailedOn ) ) ? '\'' . $this->m_strFailedOn . '\'' : 'NULL';
	}

	public function setNextCommandParameters( $strNextCommandParameters ) {
		$this->set( 'm_strNextCommandParameters', CStrings::strTrimDef( $strNextCommandParameters, 100, NULL, true ) );
	}

	public function getNextCommandParameters() {
		return $this->m_strNextCommandParameters;
	}

	public function sqlNextCommandParameters() {
		return ( true == isset( $this->m_strNextCommandParameters ) ) ? '\'' . addslashes( $this->m_strNextCommandParameters ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, migration_status_type_id, source_database_id, destination_database_id, task_id, start_time, end_time, failed_on, next_command_parameters, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlMigrationStatusTypeId() . ', ' .
 						$this->sqlSourceDatabaseId() . ', ' .
 						$this->sqlDestinationDatabaseId() . ', ' .
 						$this->sqlTaskId() . ', ' .
 						$this->sqlStartTime() . ', ' .
 						$this->sqlEndTime() . ', ' .
 						$this->sqlFailedOn() . ', ' .
 						$this->sqlNextCommandParameters() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' migration_status_type_id = ' . $this->sqlMigrationStatusTypeId() . ','; } elseif( true == array_key_exists( 'MigrationStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' migration_status_type_id = ' . $this->sqlMigrationStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_database_id = ' . $this->sqlSourceDatabaseId() . ','; } elseif( true == array_key_exists( 'SourceDatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' source_database_id = ' . $this->sqlSourceDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_database_id = ' . $this->sqlDestinationDatabaseId() . ','; } elseif( true == array_key_exists( 'DestinationDatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' destination_database_id = ' . $this->sqlDestinationDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_time = ' . $this->sqlStartTime() . ','; } elseif( true == array_key_exists( 'StartTime', $this->getChangedColumns() ) ) { $strSql .= ' start_time = ' . $this->sqlStartTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_time = ' . $this->sqlEndTime() . ','; } elseif( true == array_key_exists( 'EndTime', $this->getChangedColumns() ) ) { $strSql .= ' end_time = ' . $this->sqlEndTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; } elseif( true == array_key_exists( 'FailedOn', $this->getChangedColumns() ) ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' next_command_parameters = ' . $this->sqlNextCommandParameters() . ','; } elseif( true == array_key_exists( 'NextCommandParameters', $this->getChangedColumns() ) ) { $strSql .= ' next_command_parameters = ' . $this->sqlNextCommandParameters() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'migration_status_type_id' => $this->getMigrationStatusTypeId(),
			'source_database_id' => $this->getSourceDatabaseId(),
			'destination_database_id' => $this->getDestinationDatabaseId(),
			'task_id' => $this->getTaskId(),
			'start_time' => $this->getStartTime(),
			'end_time' => $this->getEndTime(),
			'failed_on' => $this->getFailedOn(),
			'next_command_parameters' => $this->getNextCommandParameters(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>