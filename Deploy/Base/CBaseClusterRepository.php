<?php

class CBaseClusterRepository extends CEosSingularBase {

	const TABLE_NAME = 'public.cluster_repositories';

	protected $m_intId;
	protected $m_intClusterId;
	protected $m_strName;
	protected $m_intBaseSvnRepositoryId;
	protected $m_intStageSvnRepositoryId;
	protected $m_intProductionSvnRepositoryId;
	protected $m_strSvnTagPath;
	protected $m_strExportPath;
	protected $m_boolIsCodeBase;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intClusterId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['base_svn_repository_id'] ) && $boolDirectSet ) $this->set( 'm_intBaseSvnRepositoryId', trim( $arrValues['base_svn_repository_id'] ) ); elseif( isset( $arrValues['base_svn_repository_id'] ) ) $this->setBaseSvnRepositoryId( $arrValues['base_svn_repository_id'] );
		if( isset( $arrValues['stage_svn_repository_id'] ) && $boolDirectSet ) $this->set( 'm_intStageSvnRepositoryId', trim( $arrValues['stage_svn_repository_id'] ) ); elseif( isset( $arrValues['stage_svn_repository_id'] ) ) $this->setStageSvnRepositoryId( $arrValues['stage_svn_repository_id'] );
		if( isset( $arrValues['production_svn_repository_id'] ) && $boolDirectSet ) $this->set( 'm_intProductionSvnRepositoryId', trim( $arrValues['production_svn_repository_id'] ) ); elseif( isset( $arrValues['production_svn_repository_id'] ) ) $this->setProductionSvnRepositoryId( $arrValues['production_svn_repository_id'] );
		if( isset( $arrValues['svn_tag_path'] ) && $boolDirectSet ) $this->set( 'm_strSvnTagPath', trim( stripcslashes( $arrValues['svn_tag_path'] ) ) ); elseif( isset( $arrValues['svn_tag_path'] ) ) $this->setSvnTagPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['svn_tag_path'] ) : $arrValues['svn_tag_path'] );
		if( isset( $arrValues['export_path'] ) && $boolDirectSet ) $this->set( 'm_strExportPath', trim( stripcslashes( $arrValues['export_path'] ) ) ); elseif( isset( $arrValues['export_path'] ) ) $this->setExportPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['export_path'] ) : $arrValues['export_path'] );
		if( isset( $arrValues['is_code_base'] ) && $boolDirectSet ) $this->set( 'm_boolIsCodeBase', trim( stripcslashes( $arrValues['is_code_base'] ) ) ); elseif( isset( $arrValues['is_code_base'] ) ) $this->setIsCodeBase( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_code_base'] ) : $arrValues['is_code_base'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : '1';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 200, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setBaseSvnRepositoryId( $intBaseSvnRepositoryId ) {
		$this->set( 'm_intBaseSvnRepositoryId', CStrings::strToIntDef( $intBaseSvnRepositoryId, NULL, false ) );
	}

	public function getBaseSvnRepositoryId() {
		return $this->m_intBaseSvnRepositoryId;
	}

	public function sqlBaseSvnRepositoryId() {
		return ( true == isset( $this->m_intBaseSvnRepositoryId ) ) ? ( string ) $this->m_intBaseSvnRepositoryId : 'NULL';
	}

	public function setStageSvnRepositoryId( $intStageSvnRepositoryId ) {
		$this->set( 'm_intStageSvnRepositoryId', CStrings::strToIntDef( $intStageSvnRepositoryId, NULL, false ) );
	}

	public function getStageSvnRepositoryId() {
		return $this->m_intStageSvnRepositoryId;
	}

	public function sqlStageSvnRepositoryId() {
		return ( true == isset( $this->m_intStageSvnRepositoryId ) ) ? ( string ) $this->m_intStageSvnRepositoryId : 'NULL';
	}

	public function setProductionSvnRepositoryId( $intProductionSvnRepositoryId ) {
		$this->set( 'm_intProductionSvnRepositoryId', CStrings::strToIntDef( $intProductionSvnRepositoryId, NULL, false ) );
	}

	public function getProductionSvnRepositoryId() {
		return $this->m_intProductionSvnRepositoryId;
	}

	public function sqlProductionSvnRepositoryId() {
		return ( true == isset( $this->m_intProductionSvnRepositoryId ) ) ? ( string ) $this->m_intProductionSvnRepositoryId : 'NULL';
	}

	public function setSvnTagPath( $strSvnTagPath ) {
		$this->set( 'm_strSvnTagPath', CStrings::strTrimDef( $strSvnTagPath, 500, NULL, true ) );
	}

	public function getSvnTagPath() {
		return $this->m_strSvnTagPath;
	}

	public function sqlSvnTagPath() {
		return ( true == isset( $this->m_strSvnTagPath ) ) ? '\'' . addslashes( $this->m_strSvnTagPath ) . '\'' : 'NULL';
	}

	public function setExportPath( $strExportPath ) {
		$this->set( 'm_strExportPath', CStrings::strTrimDef( $strExportPath, 500, NULL, true ) );
	}

	public function getExportPath() {
		return $this->m_strExportPath;
	}

	public function sqlExportPath() {
		return ( true == isset( $this->m_strExportPath ) ) ? '\'' . addslashes( $this->m_strExportPath ) . '\'' : 'NULL';
	}

	public function setIsCodeBase( $boolIsCodeBase ) {
		$this->set( 'm_boolIsCodeBase', CStrings::strToBool( $boolIsCodeBase ) );
	}

	public function getIsCodeBase() {
		return $this->m_boolIsCodeBase;
	}

	public function sqlIsCodeBase() {
		return ( true == isset( $this->m_boolIsCodeBase ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCodeBase ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cluster_id, name, base_svn_repository_id, stage_svn_repository_id, production_svn_repository_id, svn_tag_path, export_path, is_code_base, order_num, updated_by, updated_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlClusterId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlBaseSvnRepositoryId() . ', ' .
 						$this->sqlStageSvnRepositoryId() . ', ' .
 						$this->sqlProductionSvnRepositoryId() . ', ' .
 						$this->sqlSvnTagPath() . ', ' .
 						$this->sqlExportPath() . ', ' .
 						$this->sqlIsCodeBase() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_svn_repository_id = ' . $this->sqlBaseSvnRepositoryId() . ','; } elseif( true == array_key_exists( 'BaseSvnRepositoryId', $this->getChangedColumns() ) ) { $strSql .= ' base_svn_repository_id = ' . $this->sqlBaseSvnRepositoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' stage_svn_repository_id = ' . $this->sqlStageSvnRepositoryId() . ','; } elseif( true == array_key_exists( 'StageSvnRepositoryId', $this->getChangedColumns() ) ) { $strSql .= ' stage_svn_repository_id = ' . $this->sqlStageSvnRepositoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' production_svn_repository_id = ' . $this->sqlProductionSvnRepositoryId() . ','; } elseif( true == array_key_exists( 'ProductionSvnRepositoryId', $this->getChangedColumns() ) ) { $strSql .= ' production_svn_repository_id = ' . $this->sqlProductionSvnRepositoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_tag_path = ' . $this->sqlSvnTagPath() . ','; } elseif( true == array_key_exists( 'SvnTagPath', $this->getChangedColumns() ) ) { $strSql .= ' svn_tag_path = ' . $this->sqlSvnTagPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_path = ' . $this->sqlExportPath() . ','; } elseif( true == array_key_exists( 'ExportPath', $this->getChangedColumns() ) ) { $strSql .= ' export_path = ' . $this->sqlExportPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_code_base = ' . $this->sqlIsCodeBase() . ','; } elseif( true == array_key_exists( 'IsCodeBase', $this->getChangedColumns() ) ) { $strSql .= ' is_code_base = ' . $this->sqlIsCodeBase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cluster_id' => $this->getClusterId(),
			'name' => $this->getName(),
			'base_svn_repository_id' => $this->getBaseSvnRepositoryId(),
			'stage_svn_repository_id' => $this->getStageSvnRepositoryId(),
			'production_svn_repository_id' => $this->getProductionSvnRepositoryId(),
			'svn_tag_path' => $this->getSvnTagPath(),
			'export_path' => $this->getExportPath(),
			'is_code_base' => $this->getIsCodeBase(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>