<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CDeployments
 * Do not add any new functions to this class.
 */

class CBaseDeployments extends CEosPluralBase {

	/**
	 * @return CDeployment[]
	 */
	public static function fetchDeployments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDeployment', $objDatabase );
	}

	/**
	 * @return CDeployment
	 */
	public static function fetchDeployment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDeployment', $objDatabase );
	}

	public static function fetchDeploymentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'deployments', $objDatabase );
	}

	public static function fetchDeploymentById( $intId, $objDatabase ) {
		return self::fetchDeployment( sprintf( 'SELECT * FROM deployments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDeploymentsByClusterId( $intClusterId, $objDatabase ) {
		return self::fetchDeployments( sprintf( 'SELECT * FROM deployments WHERE cluster_id = %d', ( int ) $intClusterId ), $objDatabase );
	}

	public static function fetchDeploymentsByDeploymentTypeId( $intDeploymentTypeId, $objDatabase ) {
		return self::fetchDeployments( sprintf( 'SELECT * FROM deployments WHERE deployment_type_id = %d', ( int ) $intDeploymentTypeId ), $objDatabase );
	}

	public static function fetchDeploymentsByParentDeploymentId( $intParentDeploymentId, $objDatabase ) {
		return self::fetchDeployments( sprintf( 'SELECT * FROM deployments WHERE parent_deployment_id = %d', ( int ) $intParentDeploymentId ), $objDatabase );
	}

	public static function fetchDeploymentsByGoogleCalendarEventId( $strGoogleCalendarEventId, $objDatabase ) {
		return self::fetchDeployments( sprintf( 'SELECT * FROM deployments WHERE google_calendar_event_id = \'%s\'', $strGoogleCalendarEventId ), $objDatabase );
	}

	public static function fetchDeploymentsBySvnFileId( $intSvnFileId, $objDatabase ) {
		return self::fetchDeployments( sprintf( 'SELECT * FROM deployments WHERE svn_file_id = %d', ( int ) $intSvnFileId ), $objDatabase );
	}

}
?>