<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CPsProductDetails
 * Do not add any new functions to this class.
 */

class CBasePsProductDetails extends CEosPluralBase {

	/**
	 * @return CPsProductDetail[]
	 */
	public static function fetchPsProductDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsProductDetail', $objDatabase );
	}

	/**
	 * @return CPsProductDetail
	 */
	public static function fetchPsProductDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsProductDetail', $objDatabase );
	}

	public static function fetchPsProductDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_product_details', $objDatabase );
	}

	public static function fetchPsProductDetailById( $intId, $objDatabase ) {
		return self::fetchPsProductDetail( sprintf( 'SELECT * FROM ps_product_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsProductDetailsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchPsProductDetails( sprintf( 'SELECT * FROM ps_product_details WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchPsProductDetailsByClusterId( $intClusterId, $objDatabase ) {
		return self::fetchPsProductDetails( sprintf( 'SELECT * FROM ps_product_details WHERE cluster_id = %d', ( int ) $intClusterId ), $objDatabase );
	}

}
?>