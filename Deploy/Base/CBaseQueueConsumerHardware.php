<?php

class CBaseQueueConsumerHardware extends CEosSingularBase {

	const TABLE_NAME = 'public.queue_consumer_hardwares';

	protected $m_intId;
	protected $m_intQueueConsumerId;
	protected $m_intHardwareId;
	protected $m_intMinConsumersCount;
	protected $m_intMaxConsumersCount;
	protected $m_boolIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intMinConsumersCount = '1';
		$this->m_intMaxConsumersCount = '1';
		$this->m_boolIsPublished = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['queue_consumer_id'] ) && $boolDirectSet ) $this->set( 'm_intQueueConsumerId', trim( $arrValues['queue_consumer_id'] ) ); elseif( isset( $arrValues['queue_consumer_id'] ) ) $this->setQueueConsumerId( $arrValues['queue_consumer_id'] );
		if( isset( $arrValues['hardware_id'] ) && $boolDirectSet ) $this->set( 'm_intHardwareId', trim( $arrValues['hardware_id'] ) ); elseif( isset( $arrValues['hardware_id'] ) ) $this->setHardwareId( $arrValues['hardware_id'] );
		if( isset( $arrValues['min_consumers_count'] ) && $boolDirectSet ) $this->set( 'm_intMinConsumersCount', trim( $arrValues['min_consumers_count'] ) ); elseif( isset( $arrValues['min_consumers_count'] ) ) $this->setMinConsumersCount( $arrValues['min_consumers_count'] );
		if( isset( $arrValues['max_consumers_count'] ) && $boolDirectSet ) $this->set( 'm_intMaxConsumersCount', trim( $arrValues['max_consumers_count'] ) ); elseif( isset( $arrValues['max_consumers_count'] ) ) $this->setMaxConsumersCount( $arrValues['max_consumers_count'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setQueueConsumerId( $intQueueConsumerId ) {
		$this->set( 'm_intQueueConsumerId', CStrings::strToIntDef( $intQueueConsumerId, NULL, false ) );
	}

	public function getQueueConsumerId() {
		return $this->m_intQueueConsumerId;
	}

	public function sqlQueueConsumerId() {
		return ( true == isset( $this->m_intQueueConsumerId ) ) ? ( string ) $this->m_intQueueConsumerId : 'NULL';
	}

	public function setHardwareId( $intHardwareId ) {
		$this->set( 'm_intHardwareId', CStrings::strToIntDef( $intHardwareId, NULL, false ) );
	}

	public function getHardwareId() {
		return $this->m_intHardwareId;
	}

	public function sqlHardwareId() {
		return ( true == isset( $this->m_intHardwareId ) ) ? ( string ) $this->m_intHardwareId : 'NULL';
	}

	public function setMinConsumersCount( $intMinConsumersCount ) {
		$this->set( 'm_intMinConsumersCount', CStrings::strToIntDef( $intMinConsumersCount, NULL, false ) );
	}

	public function getMinConsumersCount() {
		return $this->m_intMinConsumersCount;
	}

	public function sqlMinConsumersCount() {
		return ( true == isset( $this->m_intMinConsumersCount ) ) ? ( string ) $this->m_intMinConsumersCount : '1';
	}

	public function setMaxConsumersCount( $intMaxConsumersCount ) {
		$this->set( 'm_intMaxConsumersCount', CStrings::strToIntDef( $intMaxConsumersCount, NULL, false ) );
	}

	public function getMaxConsumersCount() {
		return $this->m_intMaxConsumersCount;
	}

	public function sqlMaxConsumersCount() {
		return ( true == isset( $this->m_intMaxConsumersCount ) ) ? ( string ) $this->m_intMaxConsumersCount : '1';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, queue_consumer_id, hardware_id, min_consumers_count, max_consumers_count, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlQueueConsumerId() . ', ' .
 						$this->sqlHardwareId() . ', ' .
 						$this->sqlMinConsumersCount() . ', ' .
 						$this->sqlMaxConsumersCount() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' queue_consumer_id = ' . $this->sqlQueueConsumerId() . ','; } elseif( true == array_key_exists( 'QueueConsumerId', $this->getChangedColumns() ) ) { $strSql .= ' queue_consumer_id = ' . $this->sqlQueueConsumerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hardware_id = ' . $this->sqlHardwareId() . ','; } elseif( true == array_key_exists( 'HardwareId', $this->getChangedColumns() ) ) { $strSql .= ' hardware_id = ' . $this->sqlHardwareId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_consumers_count = ' . $this->sqlMinConsumersCount() . ','; } elseif( true == array_key_exists( 'MinConsumersCount', $this->getChangedColumns() ) ) { $strSql .= ' min_consumers_count = ' . $this->sqlMinConsumersCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_consumers_count = ' . $this->sqlMaxConsumersCount() . ','; } elseif( true == array_key_exists( 'MaxConsumersCount', $this->getChangedColumns() ) ) { $strSql .= ' max_consumers_count = ' . $this->sqlMaxConsumersCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'queue_consumer_id' => $this->getQueueConsumerId(),
			'hardware_id' => $this->getHardwareId(),
			'min_consumers_count' => $this->getMinConsumersCount(),
			'max_consumers_count' => $this->getMaxConsumersCount(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>