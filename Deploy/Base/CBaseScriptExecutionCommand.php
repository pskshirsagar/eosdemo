<?php

class CBaseScriptExecutionCommand extends CEosSingularBase {

	const TABLE_NAME = 'public.script_execution_commands';

	protected $m_intId;
	protected $m_intScriptId;
	protected $m_intScriptDatabaseId;
	protected $m_strScriptSchedule;
	protected $m_strScriptCommand;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['script_id'] ) && $boolDirectSet ) $this->set( 'm_intScriptId', trim( $arrValues['script_id'] ) ); elseif( isset( $arrValues['script_id'] ) ) $this->setScriptId( $arrValues['script_id'] );
		if( isset( $arrValues['script_database_id'] ) && $boolDirectSet ) $this->set( 'm_intScriptDatabaseId', trim( $arrValues['script_database_id'] ) ); elseif( isset( $arrValues['script_database_id'] ) ) $this->setScriptDatabaseId( $arrValues['script_database_id'] );
		if( isset( $arrValues['script_schedule'] ) && $boolDirectSet ) $this->set( 'm_strScriptSchedule', trim( stripcslashes( $arrValues['script_schedule'] ) ) ); elseif( isset( $arrValues['script_schedule'] ) ) $this->setScriptSchedule( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['script_schedule'] ) : $arrValues['script_schedule'] );
		if( isset( $arrValues['script_command'] ) && $boolDirectSet ) $this->set( 'm_strScriptCommand', trim( stripcslashes( $arrValues['script_command'] ) ) ); elseif( isset( $arrValues['script_command'] ) ) $this->setScriptCommand( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['script_command'] ) : $arrValues['script_command'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScriptId( $intScriptId ) {
		$this->set( 'm_intScriptId', CStrings::strToIntDef( $intScriptId, NULL, false ) );
	}

	public function getScriptId() {
		return $this->m_intScriptId;
	}

	public function sqlScriptId() {
		return ( true == isset( $this->m_intScriptId ) ) ? ( string ) $this->m_intScriptId : 'NULL';
	}

	public function setScriptDatabaseId( $intScriptDatabaseId ) {
		$this->set( 'm_intScriptDatabaseId', CStrings::strToIntDef( $intScriptDatabaseId, NULL, false ) );
	}

	public function getScriptDatabaseId() {
		return $this->m_intScriptDatabaseId;
	}

	public function sqlScriptDatabaseId() {
		return ( true == isset( $this->m_intScriptDatabaseId ) ) ? ( string ) $this->m_intScriptDatabaseId : 'NULL';
	}

	public function setScriptSchedule( $strScriptSchedule ) {
		$this->set( 'm_strScriptSchedule', CStrings::strTrimDef( $strScriptSchedule, 255, NULL, true ) );
	}

	public function getScriptSchedule() {
		return $this->m_strScriptSchedule;
	}

	public function sqlScriptSchedule() {
		return ( true == isset( $this->m_strScriptSchedule ) ) ? '\'' . addslashes( $this->m_strScriptSchedule ) . '\'' : 'NULL';
	}

	public function setScriptCommand( $strScriptCommand ) {
		$this->set( 'm_strScriptCommand', CStrings::strTrimDef( $strScriptCommand, -1, NULL, true ) );
	}

	public function getScriptCommand() {
		return $this->m_strScriptCommand;
	}

	public function sqlScriptCommand() {
		return ( true == isset( $this->m_strScriptCommand ) ) ? '\'' . addslashes( $this->m_strScriptCommand ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, script_id, script_database_id, script_schedule, script_command, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlScriptId() . ', ' .
 						$this->sqlScriptDatabaseId() . ', ' .
 						$this->sqlScriptSchedule() . ', ' .
 						$this->sqlScriptCommand() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' script_id = ' . $this->sqlScriptId() . ','; } elseif( true == array_key_exists( 'ScriptId', $this->getChangedColumns() ) ) { $strSql .= ' script_id = ' . $this->sqlScriptId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' script_database_id = ' . $this->sqlScriptDatabaseId() . ','; } elseif( true == array_key_exists( 'ScriptDatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' script_database_id = ' . $this->sqlScriptDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' script_schedule = ' . $this->sqlScriptSchedule() . ','; } elseif( true == array_key_exists( 'ScriptSchedule', $this->getChangedColumns() ) ) { $strSql .= ' script_schedule = ' . $this->sqlScriptSchedule() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' script_command = ' . $this->sqlScriptCommand() . ','; } elseif( true == array_key_exists( 'ScriptCommand', $this->getChangedColumns() ) ) { $strSql .= ' script_command = ' . $this->sqlScriptCommand() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'script_id' => $this->getScriptId(),
			'script_database_id' => $this->getScriptDatabaseId(),
			'script_schedule' => $this->getScriptSchedule(),
			'script_command' => $this->getScriptCommand(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>