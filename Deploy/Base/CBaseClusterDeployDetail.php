<?php

class CBaseClusterDeployDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.cluster_deploy_details';

	protected $m_intId;
	protected $m_intClusterId;
	protected $m_intDeploymentRoleId;
	protected $m_intApprovalRoleId;
	protected $m_strCdnUrls;
	protected $m_strSvnConifgPath;
	protected $m_strConifgExportPath;
	protected $m_strSvnConifgMountExportPath;
	protected $m_intIsDeploymentApprovalRequired;
	protected $m_boolUseCodeSlave;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intClusterId = '1';
		$this->m_intIsDeploymentApprovalRequired = '0';
		$this->m_boolUseCodeSlave = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['deployment_role_id'] ) && $boolDirectSet ) $this->set( 'm_intDeploymentRoleId', trim( $arrValues['deployment_role_id'] ) ); elseif( isset( $arrValues['deployment_role_id'] ) ) $this->setDeploymentRoleId( $arrValues['deployment_role_id'] );
		if( isset( $arrValues['approval_role_id'] ) && $boolDirectSet ) $this->set( 'm_intApprovalRoleId', trim( $arrValues['approval_role_id'] ) ); elseif( isset( $arrValues['approval_role_id'] ) ) $this->setApprovalRoleId( $arrValues['approval_role_id'] );
		if( isset( $arrValues['cdn_urls'] ) && $boolDirectSet ) $this->set( 'm_strCdnUrls', trim( stripcslashes( $arrValues['cdn_urls'] ) ) ); elseif( isset( $arrValues['cdn_urls'] ) ) $this->setCdnUrls( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cdn_urls'] ) : $arrValues['cdn_urls'] );
		if( isset( $arrValues['svn_conifg_path'] ) && $boolDirectSet ) $this->set( 'm_strSvnConifgPath', trim( stripcslashes( $arrValues['svn_conifg_path'] ) ) ); elseif( isset( $arrValues['svn_conifg_path'] ) ) $this->setSvnConifgPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['svn_conifg_path'] ) : $arrValues['svn_conifg_path'] );
		if( isset( $arrValues['conifg_export_path'] ) && $boolDirectSet ) $this->set( 'm_strConifgExportPath', trim( stripcslashes( $arrValues['conifg_export_path'] ) ) ); elseif( isset( $arrValues['conifg_export_path'] ) ) $this->setConifgExportPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['conifg_export_path'] ) : $arrValues['conifg_export_path'] );
		if( isset( $arrValues['svn_conifg_mount_export_path'] ) && $boolDirectSet ) $this->set( 'm_strSvnConifgMountExportPath', trim( stripcslashes( $arrValues['svn_conifg_mount_export_path'] ) ) ); elseif( isset( $arrValues['svn_conifg_mount_export_path'] ) ) $this->setSvnConifgMountExportPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['svn_conifg_mount_export_path'] ) : $arrValues['svn_conifg_mount_export_path'] );
		if( isset( $arrValues['is_deployment_approval_required'] ) && $boolDirectSet ) $this->set( 'm_intIsDeploymentApprovalRequired', trim( $arrValues['is_deployment_approval_required'] ) ); elseif( isset( $arrValues['is_deployment_approval_required'] ) ) $this->setIsDeploymentApprovalRequired( $arrValues['is_deployment_approval_required'] );
		if( isset( $arrValues['use_code_slave'] ) && $boolDirectSet ) $this->set( 'm_boolUseCodeSlave', trim( stripcslashes( $arrValues['use_code_slave'] ) ) ); elseif( isset( $arrValues['use_code_slave'] ) ) $this->setUseCodeSlave( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_code_slave'] ) : $arrValues['use_code_slave'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : '1';
	}

	public function setDeploymentRoleId( $intDeploymentRoleId ) {
		$this->set( 'm_intDeploymentRoleId', CStrings::strToIntDef( $intDeploymentRoleId, NULL, false ) );
	}

	public function getDeploymentRoleId() {
		return $this->m_intDeploymentRoleId;
	}

	public function sqlDeploymentRoleId() {
		return ( true == isset( $this->m_intDeploymentRoleId ) ) ? ( string ) $this->m_intDeploymentRoleId : 'NULL';
	}

	public function setApprovalRoleId( $intApprovalRoleId ) {
		$this->set( 'm_intApprovalRoleId', CStrings::strToIntDef( $intApprovalRoleId, NULL, false ) );
	}

	public function getApprovalRoleId() {
		return $this->m_intApprovalRoleId;
	}

	public function sqlApprovalRoleId() {
		return ( true == isset( $this->m_intApprovalRoleId ) ) ? ( string ) $this->m_intApprovalRoleId : 'NULL';
	}

	public function setCdnUrls( $strCdnUrls ) {
		$this->set( 'm_strCdnUrls', CStrings::strTrimDef( $strCdnUrls, 500, NULL, true ) );
	}

	public function getCdnUrls() {
		return $this->m_strCdnUrls;
	}

	public function sqlCdnUrls() {
		return ( true == isset( $this->m_strCdnUrls ) ) ? '\'' . addslashes( $this->m_strCdnUrls ) . '\'' : 'NULL';
	}

	public function setSvnConifgPath( $strSvnConifgPath ) {
		$this->set( 'm_strSvnConifgPath', CStrings::strTrimDef( $strSvnConifgPath, 500, NULL, true ) );
	}

	public function getSvnConifgPath() {
		return $this->m_strSvnConifgPath;
	}

	public function sqlSvnConifgPath() {
		return ( true == isset( $this->m_strSvnConifgPath ) ) ? '\'' . addslashes( $this->m_strSvnConifgPath ) . '\'' : 'NULL';
	}

	public function setConifgExportPath( $strConifgExportPath ) {
		$this->set( 'm_strConifgExportPath', CStrings::strTrimDef( $strConifgExportPath, 500, NULL, true ) );
	}

	public function getConifgExportPath() {
		return $this->m_strConifgExportPath;
	}

	public function sqlConifgExportPath() {
		return ( true == isset( $this->m_strConifgExportPath ) ) ? '\'' . addslashes( $this->m_strConifgExportPath ) . '\'' : 'NULL';
	}

	public function setSvnConifgMountExportPath( $strSvnConifgMountExportPath ) {
		$this->set( 'm_strSvnConifgMountExportPath', CStrings::strTrimDef( $strSvnConifgMountExportPath, 500, NULL, true ) );
	}

	public function getSvnConifgMountExportPath() {
		return $this->m_strSvnConifgMountExportPath;
	}

	public function sqlSvnConifgMountExportPath() {
		return ( true == isset( $this->m_strSvnConifgMountExportPath ) ) ? '\'' . addslashes( $this->m_strSvnConifgMountExportPath ) . '\'' : 'NULL';
	}

	public function setIsDeploymentApprovalRequired( $intIsDeploymentApprovalRequired ) {
		$this->set( 'm_intIsDeploymentApprovalRequired', CStrings::strToIntDef( $intIsDeploymentApprovalRequired, NULL, false ) );
	}

	public function getIsDeploymentApprovalRequired() {
		return $this->m_intIsDeploymentApprovalRequired;
	}

	public function sqlIsDeploymentApprovalRequired() {
		return ( true == isset( $this->m_intIsDeploymentApprovalRequired ) ) ? ( string ) $this->m_intIsDeploymentApprovalRequired : '0';
	}

	public function setUseCodeSlave( $boolUseCodeSlave ) {
		$this->set( 'm_boolUseCodeSlave', CStrings::strToBool( $boolUseCodeSlave ) );
	}

	public function getUseCodeSlave() {
		return $this->m_boolUseCodeSlave;
	}

	public function sqlUseCodeSlave() {
		return ( true == isset( $this->m_boolUseCodeSlave ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseCodeSlave ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cluster_id, deployment_role_id, approval_role_id, cdn_urls, svn_conifg_path, conifg_export_path, svn_conifg_mount_export_path, is_deployment_approval_required, use_code_slave, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlClusterId() . ', ' .
 						$this->sqlDeploymentRoleId() . ', ' .
 						$this->sqlApprovalRoleId() . ', ' .
 						$this->sqlCdnUrls() . ', ' .
 						$this->sqlSvnConifgPath() . ', ' .
 						$this->sqlConifgExportPath() . ', ' .
 						$this->sqlSvnConifgMountExportPath() . ', ' .
 						$this->sqlIsDeploymentApprovalRequired() . ', ' .
 						$this->sqlUseCodeSlave() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deployment_role_id = ' . $this->sqlDeploymentRoleId() . ','; } elseif( true == array_key_exists( 'DeploymentRoleId', $this->getChangedColumns() ) ) { $strSql .= ' deployment_role_id = ' . $this->sqlDeploymentRoleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approval_role_id = ' . $this->sqlApprovalRoleId() . ','; } elseif( true == array_key_exists( 'ApprovalRoleId', $this->getChangedColumns() ) ) { $strSql .= ' approval_role_id = ' . $this->sqlApprovalRoleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cdn_urls = ' . $this->sqlCdnUrls() . ','; } elseif( true == array_key_exists( 'CdnUrls', $this->getChangedColumns() ) ) { $strSql .= ' cdn_urls = ' . $this->sqlCdnUrls() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_conifg_path = ' . $this->sqlSvnConifgPath() . ','; } elseif( true == array_key_exists( 'SvnConifgPath', $this->getChangedColumns() ) ) { $strSql .= ' svn_conifg_path = ' . $this->sqlSvnConifgPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' conifg_export_path = ' . $this->sqlConifgExportPath() . ','; } elseif( true == array_key_exists( 'ConifgExportPath', $this->getChangedColumns() ) ) { $strSql .= ' conifg_export_path = ' . $this->sqlConifgExportPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_conifg_mount_export_path = ' . $this->sqlSvnConifgMountExportPath() . ','; } elseif( true == array_key_exists( 'SvnConifgMountExportPath', $this->getChangedColumns() ) ) { $strSql .= ' svn_conifg_mount_export_path = ' . $this->sqlSvnConifgMountExportPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deployment_approval_required = ' . $this->sqlIsDeploymentApprovalRequired() . ','; } elseif( true == array_key_exists( 'IsDeploymentApprovalRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_deployment_approval_required = ' . $this->sqlIsDeploymentApprovalRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_code_slave = ' . $this->sqlUseCodeSlave() . ','; } elseif( true == array_key_exists( 'UseCodeSlave', $this->getChangedColumns() ) ) { $strSql .= ' use_code_slave = ' . $this->sqlUseCodeSlave() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cluster_id' => $this->getClusterId(),
			'deployment_role_id' => $this->getDeploymentRoleId(),
			'approval_role_id' => $this->getApprovalRoleId(),
			'cdn_urls' => $this->getCdnUrls(),
			'svn_conifg_path' => $this->getSvnConifgPath(),
			'conifg_export_path' => $this->getConifgExportPath(),
			'svn_conifg_mount_export_path' => $this->getSvnConifgMountExportPath(),
			'is_deployment_approval_required' => $this->getIsDeploymentApprovalRequired(),
			'use_code_slave' => $this->getUseCodeSlave(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>