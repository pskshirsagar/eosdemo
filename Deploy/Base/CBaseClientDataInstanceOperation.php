<?php

class CBaseClientDataInstanceOperation extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.client_data_instance_operations';

	protected $m_intId;
	protected $m_intClientDataInstanceId;
	protected $m_intClientDataInstanceOperationStatusTypeId;
	protected $m_intClientDataInstanceOperationTypeId;
	protected $m_strNote;
	protected $m_strStatus;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_strProcessedOn;
	protected $m_strFailedOn;
	protected $m_strCompletedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsRequestedByCsm;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intSourceClusterId;
	protected $m_intDestinationClusterId;

	public function __construct() {
		parent::__construct();

		$this->m_intClientDataInstanceOperationStatusTypeId = '1';
		$this->m_intClientDataInstanceOperationTypeId = '1';
		$this->m_boolIsRequestedByCsm = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['client_data_instance_id'] ) && $boolDirectSet ) $this->set( 'm_intClientDataInstanceId', trim( $arrValues['client_data_instance_id'] ) ); elseif( isset( $arrValues['client_data_instance_id'] ) ) $this->setClientDataInstanceId( $arrValues['client_data_instance_id'] );
		if( isset( $arrValues['client_data_instance_operation_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intClientDataInstanceOperationStatusTypeId', trim( $arrValues['client_data_instance_operation_status_type_id'] ) ); elseif( isset( $arrValues['client_data_instance_operation_status_type_id'] ) ) $this->setClientDataInstanceOperationStatusTypeId( $arrValues['client_data_instance_operation_status_type_id'] );
		if( isset( $arrValues['client_data_instance_operation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intClientDataInstanceOperationTypeId', trim( $arrValues['client_data_instance_operation_type_id'] ) ); elseif( isset( $arrValues['client_data_instance_operation_type_id'] ) ) $this->setClientDataInstanceOperationTypeId( $arrValues['client_data_instance_operation_type_id'] );
		if( isset( $arrValues['note'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['status'] ) && $boolDirectSet ) $this->set( 'm_strStatus', trim( stripcslashes( $arrValues['status'] ) ) ); elseif( isset( $arrValues['status'] ) ) $this->setStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status'] ) : $arrValues['status'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['failed_on'] ) && $boolDirectSet ) $this->set( 'm_strFailedOn', trim( $arrValues['failed_on'] ) ); elseif( isset( $arrValues['failed_on'] ) ) $this->setFailedOn( $arrValues['failed_on'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_requested_by_csm'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequestedByCsm', trim( stripcslashes( $arrValues['is_requested_by_csm'] ) ) ); elseif( isset( $arrValues['is_requested_by_csm'] ) ) $this->setIsRequestedByCsm( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_requested_by_csm'] ) : $arrValues['is_requested_by_csm'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['source_cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intSourceClusterId', trim( $arrValues['source_cluster_id'] ) ); elseif( isset( $arrValues['source_cluster_id'] ) ) $this->setSourceClusterId( $arrValues['source_cluster_id'] );
		if( isset( $arrValues['destination_cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intDestinationClusterId', trim( $arrValues['destination_cluster_id'] ) ); elseif( isset( $arrValues['destination_cluster_id'] ) ) $this->setDestinationClusterId( $arrValues['destination_cluster_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setClientDataInstanceId( $intClientDataInstanceId ) {
		$this->set( 'm_intClientDataInstanceId', CStrings::strToIntDef( $intClientDataInstanceId, NULL, false ) );
	}

	public function getClientDataInstanceId() {
		return $this->m_intClientDataInstanceId;
	}

	public function sqlClientDataInstanceId() {
		return ( true == isset( $this->m_intClientDataInstanceId ) ) ? ( string ) $this->m_intClientDataInstanceId : 'NULL';
	}

	public function setClientDataInstanceOperationStatusTypeId( $intClientDataInstanceOperationStatusTypeId ) {
		$this->set( 'm_intClientDataInstanceOperationStatusTypeId', CStrings::strToIntDef( $intClientDataInstanceOperationStatusTypeId, NULL, false ) );
	}

	public function getClientDataInstanceOperationStatusTypeId() {
		return $this->m_intClientDataInstanceOperationStatusTypeId;
	}

	public function sqlClientDataInstanceOperationStatusTypeId() {
		return ( true == isset( $this->m_intClientDataInstanceOperationStatusTypeId ) ) ? ( string ) $this->m_intClientDataInstanceOperationStatusTypeId : '1';
	}

	public function setClientDataInstanceOperationTypeId( $intClientDataInstanceOperationTypeId ) {
		$this->set( 'm_intClientDataInstanceOperationTypeId', CStrings::strToIntDef( $intClientDataInstanceOperationTypeId, NULL, false ) );
	}

	public function getClientDataInstanceOperationTypeId() {
		return $this->m_intClientDataInstanceOperationTypeId;
	}

	public function sqlClientDataInstanceOperationTypeId() {
		return ( true == isset( $this->m_intClientDataInstanceOperationTypeId ) ) ? ( string ) $this->m_intClientDataInstanceOperationTypeId : '1';
	}

	public function setNote( $strNote, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strNote', CStrings::strTrimDef( $strNote, 400, NULL, true ), $strLocaleCode );
	}

	public function getNote( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strNote', $strLocaleCode );
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setStatus( $strStatus ) {
		$this->set( 'm_strStatus', CStrings::strTrimDef( $strStatus, 100, NULL, true ) );
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function sqlStatus() {
		return ( true == isset( $this->m_strStatus ) ) ? '\'' . addslashes( $this->m_strStatus ) . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setFailedOn( $strFailedOn ) {
		$this->set( 'm_strFailedOn', CStrings::strTrimDef( $strFailedOn, -1, NULL, true ) );
	}

	public function getFailedOn() {
		return $this->m_strFailedOn;
	}

	public function sqlFailedOn() {
		return ( true == isset( $this->m_strFailedOn ) ) ? '\'' . $this->m_strFailedOn . '\'' : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsRequestedByCsm( $boolIsRequestedByCsm ) {
		$this->set( 'm_boolIsRequestedByCsm', CStrings::strToBool( $boolIsRequestedByCsm ) );
	}

	public function getIsRequestedByCsm() {
		return $this->m_boolIsRequestedByCsm;
	}

	public function sqlIsRequestedByCsm() {
		return ( true == isset( $this->m_boolIsRequestedByCsm ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequestedByCsm ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSourceClusterId( $intSourceClusterId ) {
		$this->set( 'm_intSourceClusterId', CStrings::strToIntDef( $intSourceClusterId, NULL, false ) );
	}

	public function getSourceClusterId() {
		return $this->m_intSourceClusterId;
	}

	public function sqlSourceClusterId() {
		return ( true == isset( $this->m_intSourceClusterId ) ) ? ( string ) $this->m_intSourceClusterId : 'NULL';
	}

	public function setDestinationClusterId( $intDestinationClusterId ) {
		$this->set( 'm_intDestinationClusterId', CStrings::strToIntDef( $intDestinationClusterId, NULL, false ) );
	}

	public function getDestinationClusterId() {
		return $this->m_intDestinationClusterId;
	}

	public function sqlDestinationClusterId() {
		return ( true == isset( $this->m_intDestinationClusterId ) ) ? ( string ) $this->m_intDestinationClusterId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, client_data_instance_id, client_data_instance_operation_status_type_id, client_data_instance_operation_type_id, note, status, approved_by, approved_on, processed_on, failed_on, completed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, is_requested_by_csm, details, source_cluster_id, destination_cluster_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlClientDataInstanceId() . ', ' .
						$this->sqlClientDataInstanceOperationStatusTypeId() . ', ' .
						$this->sqlClientDataInstanceOperationTypeId() . ', ' .
						$this->sqlNote() . ', ' .
						$this->sqlStatus() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlProcessedOn() . ', ' .
						$this->sqlFailedOn() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsRequestedByCsm() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlSourceClusterId() . ', ' .
						$this->sqlDestinationClusterId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_data_instance_id = ' . $this->sqlClientDataInstanceId(). ',' ; } elseif( true == array_key_exists( 'ClientDataInstanceId', $this->getChangedColumns() ) ) { $strSql .= ' client_data_instance_id = ' . $this->sqlClientDataInstanceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_data_instance_operation_status_type_id = ' . $this->sqlClientDataInstanceOperationStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ClientDataInstanceOperationStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' client_data_instance_operation_status_type_id = ' . $this->sqlClientDataInstanceOperationStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_data_instance_operation_type_id = ' . $this->sqlClientDataInstanceOperationTypeId(). ',' ; } elseif( true == array_key_exists( 'ClientDataInstanceOperationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' client_data_instance_operation_type_id = ' . $this->sqlClientDataInstanceOperationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote(). ',' ; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status = ' . $this->sqlStatus(). ',' ; } elseif( true == array_key_exists( 'Status', $this->getChangedColumns() ) ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn(). ',' ; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn(). ',' ; } elseif( true == array_key_exists( 'FailedOn', $this->getChangedColumns() ) ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_requested_by_csm = ' . $this->sqlIsRequestedByCsm(). ',' ; } elseif( true == array_key_exists( 'IsRequestedByCsm', $this->getChangedColumns() ) ) { $strSql .= ' is_requested_by_csm = ' . $this->sqlIsRequestedByCsm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_cluster_id = ' . $this->sqlSourceClusterId(). ',' ; } elseif( true == array_key_exists( 'SourceClusterId', $this->getChangedColumns() ) ) { $strSql .= ' source_cluster_id = ' . $this->sqlSourceClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_cluster_id = ' . $this->sqlDestinationClusterId(). ',' ; } elseif( true == array_key_exists( 'DestinationClusterId', $this->getChangedColumns() ) ) { $strSql .= ' destination_cluster_id = ' . $this->sqlDestinationClusterId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'client_data_instance_id' => $this->getClientDataInstanceId(),
			'client_data_instance_operation_status_type_id' => $this->getClientDataInstanceOperationStatusTypeId(),
			'client_data_instance_operation_type_id' => $this->getClientDataInstanceOperationTypeId(),
			'note' => $this->getNote(),
			'status' => $this->getStatus(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'processed_on' => $this->getProcessedOn(),
			'failed_on' => $this->getFailedOn(),
			'completed_on' => $this->getCompletedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_requested_by_csm' => $this->getIsRequestedByCsm(),
			'details' => $this->getDetails(),
			'source_cluster_id' => $this->getSourceClusterId(),
			'destination_cluster_id' => $this->getDestinationClusterId()
		);
	}

}
?>