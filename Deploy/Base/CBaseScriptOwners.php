<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CScriptOwners
 * Do not add any new functions to this class.
 */

class CBaseScriptOwners extends CEosPluralBase {

	/**
	 * @return CScriptOwner[]
	 */
	public static function fetchScriptOwners( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScriptOwner', $objDatabase );
	}

	/**
	 * @return CScriptOwner
	 */
	public static function fetchScriptOwner( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScriptOwner', $objDatabase );
	}

	public static function fetchScriptOwnerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'script_owners', $objDatabase );
	}

	public static function fetchScriptOwnerById( $intId, $objDatabase ) {
		return self::fetchScriptOwner( sprintf( 'SELECT * FROM script_owners WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScriptOwnersByScriptId( $intScriptId, $objDatabase ) {
		return self::fetchScriptOwners( sprintf( 'SELECT * FROM script_owners WHERE script_id = %d', ( int ) $intScriptId ), $objDatabase );
	}

	public static function fetchScriptOwnersByEmployeeId( $strEmployeeId, $objDatabase ) {
		return self::fetchScriptOwners( sprintf( 'SELECT * FROM script_owners WHERE employee_id = \'%s\'', $strEmployeeId ), $objDatabase );
	}

}
?>