<?php

class CBaseHardwareLogs extends CEosPluralBase {

    const TABLE_HARDWARE_LOGS = 'public.hardware_logs';

    public static function fetchHardwareLogs( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CHardwareLog', $objDatabase );
    }

    public static function fetchHardwareLog( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CHardwareLog', $objDatabase );
    }

    public static function fetchHardwareLogCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'hardware_logs', $objDatabase );
    }

    public static function fetchHardwareLogById( $intId, $objDatabase ) {
        return self::fetchHardwareLog( sprintf( 'SELECT * FROM hardware_logs WHERE id = %d', (int) $intId ), $objDatabase );
    }

    public static function fetchHardwareLogsByHardwareLogTypeId( $intHardwareLogTypeId, $objDatabase ) {
        return self::fetchHardwareLogs( sprintf( 'SELECT * FROM hardware_logs WHERE hardware_log_type_id = %d', (int) $intHardwareLogTypeId ), $objDatabase );
    }

}
?>