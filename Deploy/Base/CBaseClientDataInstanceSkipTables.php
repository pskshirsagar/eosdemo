<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClientDataInstanceSkipTables
 * Do not add any new functions to this class.
 */

class CBaseClientDataInstanceSkipTables extends CEosPluralBase {

	/**
	 * @return CClientDataInstanceSkipTable[]
	 */
	public static function fetchClientDataInstanceSkipTables( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CClientDataInstanceSkipTable::class, $objDatabase );
	}

	/**
	 * @return CClientDataInstanceSkipTable
	 */
	public static function fetchClientDataInstanceSkipTable( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CClientDataInstanceSkipTable::class, $objDatabase );
	}

	public static function fetchClientDataInstanceSkipTableCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'client_data_instance_skip_tables', $objDatabase );
	}

	public static function fetchClientDataInstanceSkipTableById( $intId, $objDatabase ) {
		return self::fetchClientDataInstanceSkipTable( sprintf( 'SELECT * FROM client_data_instance_skip_tables WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchClientDataInstanceSkipTablesByDatabaseTypeId( $intDatabaseTypeId, $objDatabase ) {
		return self::fetchClientDataInstanceSkipTables( sprintf( 'SELECT * FROM client_data_instance_skip_tables WHERE database_type_id = %d', $intDatabaseTypeId ), $objDatabase );
	}

}
?>