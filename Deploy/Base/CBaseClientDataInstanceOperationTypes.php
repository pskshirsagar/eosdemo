<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClientDataInstanceOperationTypes
 * Do not add any new functions to this class.
 */

class CBaseClientDataInstanceOperationTypes extends CEosPluralBase {

	/**
	 * @return CClientDataInstanceOperationType[]
	 */
	public static function fetchClientDataInstanceOperationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CClientDataInstanceOperationType', $objDatabase );
	}

	/**
	 * @return CClientDataInstanceOperationType
	 */
	public static function fetchClientDataInstanceOperationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CClientDataInstanceOperationType', $objDatabase );
	}

	public static function fetchClientDataInstanceOperationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'client_data_instance_operation_types', $objDatabase );
	}

	public static function fetchClientDataInstanceOperationTypeById( $intId, $objDatabase ) {
		return self::fetchClientDataInstanceOperationType( sprintf( 'SELECT * FROM client_data_instance_operation_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>