<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CResetFrequencyTypes
 * Do not add any new functions to this class.
 */

class CBaseResetFrequencyTypes extends CEosPluralBase {

	/**
	 * @return CResetFrequencyType[]
	 */
	public static function fetchResetFrequencyTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CResetFrequencyType::class, $objDatabase );
	}

	/**
	 * @return CResetFrequencyType
	 */
	public static function fetchResetFrequencyType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CResetFrequencyType::class, $objDatabase );
	}

	public static function fetchResetFrequencyTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reset_frequency_types', $objDatabase );
	}

	public static function fetchResetFrequencyTypeById( $intId, $objDatabase ) {
		return self::fetchResetFrequencyType( sprintf( 'SELECT * FROM reset_frequency_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>