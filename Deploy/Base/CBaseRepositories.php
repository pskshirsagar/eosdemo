<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CRepositories
 * Do not add any new functions to this class.
 */

class CBaseRepositories extends CEosPluralBase {

	/**
	 * @return CRepository[]
	 */
	public static function fetchRepositories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CRepository', $objDatabase );
	}

	/**
	 * @return CRepository
	 */
	public static function fetchRepository( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRepository', $objDatabase );
	}

	public static function fetchRepositoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'repositories', $objDatabase );
	}

	public static function fetchRepositoryById( $intId, $objDatabase ) {
		return self::fetchRepository( sprintf( 'SELECT * FROM repositories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchRepositoriesByRepositoryTypeId( $intRepositoryTypeId, $objDatabase ) {
		return self::fetchRepositories( sprintf( 'SELECT * FROM repositories WHERE repository_type_id = %d', ( int ) $intRepositoryTypeId ), $objDatabase );
	}

	public static function fetchRepositoriesByDeploymentId( $intDeploymentId, $objDatabase ) {
		return self::fetchRepositories( sprintf( 'SELECT * FROM repositories WHERE deployment_id = %d', ( int ) $intDeploymentId ), $objDatabase );
	}

	public static function fetchRepositoriesBySystemUserId( $intSystemUserId, $objDatabase ) {
		return self::fetchRepositories( sprintf( 'SELECT * FROM repositories WHERE system_user_id = %d', ( int ) $intSystemUserId ), $objDatabase );
	}

	public static function fetchRepositoriesByTaskId( $strTaskId, $objDatabase ) {
		return self::fetchRepositories( sprintf( 'SELECT * FROM repositories WHERE task_id = \'%s\'', $strTaskId ), $objDatabase );
	}

	public static function fetchRepositoriesByRepositoryId( $intRepositoryId, $objDatabase ) {
		return self::fetchRepositories( sprintf( 'SELECT * FROM repositories WHERE repository_id = %d', ( int ) $intRepositoryId ), $objDatabase );
	}

	public static function fetchRepositoriesByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchRepositories( sprintf( 'SELECT * FROM repositories WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

}
?>