<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClusterRepositories
 * Do not add any new functions to this class.
 */

class CBaseClusterRepositories extends CEosPluralBase {

	/**
	 * @return CClusterRepository[]
	 */
	public static function fetchClusterRepositories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CClusterRepository', $objDatabase );
	}

	/**
	 * @return CClusterRepository
	 */
	public static function fetchClusterRepository( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CClusterRepository', $objDatabase );
	}

	public static function fetchClusterRepositoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cluster_repositories', $objDatabase );
	}

	public static function fetchClusterRepositoryById( $intId, $objDatabase ) {
		return self::fetchClusterRepository( sprintf( 'SELECT * FROM cluster_repositories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchClusterRepositoriesByClusterId( $intClusterId, $objDatabase ) {
		return self::fetchClusterRepositories( sprintf( 'SELECT * FROM cluster_repositories WHERE cluster_id = %d', ( int ) $intClusterId ), $objDatabase );
	}

	public static function fetchClusterRepositoriesByBaseSvnRepositoryId( $intBaseSvnRepositoryId, $objDatabase ) {
		return self::fetchClusterRepositories( sprintf( 'SELECT * FROM cluster_repositories WHERE base_svn_repository_id = %d', ( int ) $intBaseSvnRepositoryId ), $objDatabase );
	}

	public static function fetchClusterRepositoriesByStageSvnRepositoryId( $intStageSvnRepositoryId, $objDatabase ) {
		return self::fetchClusterRepositories( sprintf( 'SELECT * FROM cluster_repositories WHERE stage_svn_repository_id = %d', ( int ) $intStageSvnRepositoryId ), $objDatabase );
	}

	public static function fetchClusterRepositoriesByProductionSvnRepositoryId( $intProductionSvnRepositoryId, $objDatabase ) {
		return self::fetchClusterRepositories( sprintf( 'SELECT * FROM cluster_repositories WHERE production_svn_repository_id = %d', ( int ) $intProductionSvnRepositoryId ), $objDatabase );
	}

}
?>