<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CEntratamationVersions
 * Do not add any new functions to this class.
 */

class CBaseEntratamationVersions extends CEosPluralBase {

	/**
	 * @return CEntratamationVersion[]
	 */
	public static function fetchEntratamationVersions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEntratamationVersion::class, $objDatabase );
	}

	/**
	 * @return CEntratamationVersion
	 */
	public static function fetchEntratamationVersion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEntratamationVersion::class, $objDatabase );
	}

	public static function fetchEntratamationVersionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'entratamation_versions', $objDatabase );
	}

	public static function fetchEntratamationVersionById( $intId, $objDatabase ) {
		return self::fetchEntratamationVersion( sprintf( 'SELECT * FROM entratamation_versions WHERE id = %d', $intId ), $objDatabase );
	}

}
?>