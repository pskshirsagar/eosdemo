<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CQueueConsumers
 * Do not add any new functions to this class.
 */

class CBaseQueueConsumers extends CEosPluralBase {

	/**
	 * @return CQueueConsumer[]
	 */
	public static function fetchQueueConsumers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CQueueConsumer::class, $objDatabase );
	}

	/**
	 * @return CQueueConsumer
	 */
	public static function fetchQueueConsumer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CQueueConsumer::class, $objDatabase );
	}

	public static function fetchQueueConsumerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'queue_consumers', $objDatabase );
	}

	public static function fetchQueueConsumerById( $intId, $objDatabase ) {
		return self::fetchQueueConsumer( sprintf( 'SELECT * FROM queue_consumers WHERE id = %d', $intId ), $objDatabase );
	}

}
?>