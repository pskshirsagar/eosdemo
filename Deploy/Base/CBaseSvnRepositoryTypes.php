<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CSvnRepositoryTypes
 * Do not add any new functions to this class.
 */

class CBaseSvnRepositoryTypes extends CEosPluralBase {

	/**
	 * @return CSvnRepositoryType[]
	 */
	public static function fetchSvnRepositoryTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSvnRepositoryType', $objDatabase );
	}

	/**
	 * @return CSvnRepositoryType
	 */
	public static function fetchSvnRepositoryType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSvnRepositoryType', $objDatabase );
	}

	public static function fetchSvnRepositoryTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'svn_repository_types', $objDatabase );
	}

	public static function fetchSvnRepositoryTypeById( $intId, $objDatabase ) {
		return self::fetchSvnRepositoryType( sprintf( 'SELECT * FROM svn_repository_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>