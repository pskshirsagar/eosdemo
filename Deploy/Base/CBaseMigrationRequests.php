<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CMigrationRequests
 * Do not add any new functions to this class.
 */

class CBaseMigrationRequests extends CEosPluralBase {

	/**
	 * @return CMigrationRequest[]
	 */
	public static function fetchMigrationRequests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMigrationRequest', $objDatabase );
	}

	/**
	 * @return CMigrationRequest
	 */
	public static function fetchMigrationRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMigrationRequest', $objDatabase );
	}

	public static function fetchMigrationRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'migration_requests', $objDatabase );
	}

	public static function fetchMigrationRequestById( $intId, $objDatabase ) {
		return self::fetchMigrationRequest( sprintf( 'SELECT * FROM migration_requests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMigrationRequestsByCid( $intCid, $objDatabase ) {
		return self::fetchMigrationRequests( sprintf( 'SELECT * FROM migration_requests WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMigrationRequestsByMigrationStatusTypeId( $intMigrationStatusTypeId, $objDatabase ) {
		return self::fetchMigrationRequests( sprintf( 'SELECT * FROM migration_requests WHERE migration_status_type_id = %d', ( int ) $intMigrationStatusTypeId ), $objDatabase );
	}

	public static function fetchMigrationRequestsBySourceDatabaseId( $intSourceDatabaseId, $objDatabase ) {
		return self::fetchMigrationRequests( sprintf( 'SELECT * FROM migration_requests WHERE source_database_id = %d', ( int ) $intSourceDatabaseId ), $objDatabase );
	}

	public static function fetchMigrationRequestsByDestinationDatabaseId( $intDestinationDatabaseId, $objDatabase ) {
		return self::fetchMigrationRequests( sprintf( 'SELECT * FROM migration_requests WHERE destination_database_id = %d', ( int ) $intDestinationDatabaseId ), $objDatabase );
	}

	public static function fetchMigrationRequestsByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchMigrationRequests( sprintf( 'SELECT * FROM migration_requests WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

}
?>