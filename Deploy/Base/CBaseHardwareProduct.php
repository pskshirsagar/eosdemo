<?php

class CBaseHardwareProduct extends CEosSingularBase {

	const TABLE_NAME = 'public.hardware_products';

	protected $m_intId;
	protected $m_intHardwareId;
	protected $m_intPsProductId;
	protected $m_strPortalIp;
	protected $m_strLoadBalancerCookieValue;
	protected $m_intIsDisableFromLoadbalancer;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsDisableFromLoadbalancer = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['hardware_id'] ) && $boolDirectSet ) $this->set( 'm_intHardwareId', trim( $arrValues['hardware_id'] ) ); elseif( isset( $arrValues['hardware_id'] ) ) $this->setHardwareId( $arrValues['hardware_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['portal_ip'] ) && $boolDirectSet ) $this->set( 'm_strPortalIp', trim( stripcslashes( $arrValues['portal_ip'] ) ) ); elseif( isset( $arrValues['portal_ip'] ) ) $this->setPortalIp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['portal_ip'] ) : $arrValues['portal_ip'] );
		if( isset( $arrValues['load_balancer_cookie_value'] ) && $boolDirectSet ) $this->set( 'm_strLoadBalancerCookieValue', trim( stripcslashes( $arrValues['load_balancer_cookie_value'] ) ) ); elseif( isset( $arrValues['load_balancer_cookie_value'] ) ) $this->setLoadBalancerCookieValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['load_balancer_cookie_value'] ) : $arrValues['load_balancer_cookie_value'] );
		if( isset( $arrValues['is_disable_from_loadbalancer'] ) && $boolDirectSet ) $this->set( 'm_intIsDisableFromLoadbalancer', trim( $arrValues['is_disable_from_loadbalancer'] ) ); elseif( isset( $arrValues['is_disable_from_loadbalancer'] ) ) $this->setIsDisableFromLoadbalancer( $arrValues['is_disable_from_loadbalancer'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setHardwareId( $intHardwareId ) {
		$this->set( 'm_intHardwareId', CStrings::strToIntDef( $intHardwareId, NULL, false ) );
	}

	public function getHardwareId() {
		return $this->m_intHardwareId;
	}

	public function sqlHardwareId() {
		return ( true == isset( $this->m_intHardwareId ) ) ? ( string ) $this->m_intHardwareId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPortalIp( $strPortalIp ) {
		$this->set( 'm_strPortalIp', CStrings::strTrimDef( $strPortalIp, 20, NULL, true ) );
	}

	public function getPortalIp() {
		return $this->m_strPortalIp;
	}

	public function sqlPortalIp() {
		return ( true == isset( $this->m_strPortalIp ) ) ? '\'' . addslashes( $this->m_strPortalIp ) . '\'' : 'NULL';
	}

	public function setLoadBalancerCookieValue( $strLoadBalancerCookieValue ) {
		$this->set( 'm_strLoadBalancerCookieValue', CStrings::strTrimDef( $strLoadBalancerCookieValue, 240, NULL, true ) );
	}

	public function getLoadBalancerCookieValue() {
		return $this->m_strLoadBalancerCookieValue;
	}

	public function sqlLoadBalancerCookieValue() {
		return ( true == isset( $this->m_strLoadBalancerCookieValue ) ) ? '\'' . addslashes( $this->m_strLoadBalancerCookieValue ) . '\'' : 'NULL';
	}

	public function setIsDisableFromLoadbalancer( $intIsDisableFromLoadbalancer ) {
		$this->set( 'm_intIsDisableFromLoadbalancer', CStrings::strToIntDef( $intIsDisableFromLoadbalancer, NULL, false ) );
	}

	public function getIsDisableFromLoadbalancer() {
		return $this->m_intIsDisableFromLoadbalancer;
	}

	public function sqlIsDisableFromLoadbalancer() {
		return ( true == isset( $this->m_intIsDisableFromLoadbalancer ) ) ? ( string ) $this->m_intIsDisableFromLoadbalancer : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, hardware_id, ps_product_id, portal_ip, load_balancer_cookie_value, is_disable_from_loadbalancer, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlHardwareId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlPortalIp() . ', ' .
 						$this->sqlLoadBalancerCookieValue() . ', ' .
 						$this->sqlIsDisableFromLoadbalancer() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hardware_id = ' . $this->sqlHardwareId() . ','; } elseif( true == array_key_exists( 'HardwareId', $this->getChangedColumns() ) ) { $strSql .= ' hardware_id = ' . $this->sqlHardwareId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' portal_ip = ' . $this->sqlPortalIp() . ','; } elseif( true == array_key_exists( 'PortalIp', $this->getChangedColumns() ) ) { $strSql .= ' portal_ip = ' . $this->sqlPortalIp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' load_balancer_cookie_value = ' . $this->sqlLoadBalancerCookieValue() . ','; } elseif( true == array_key_exists( 'LoadBalancerCookieValue', $this->getChangedColumns() ) ) { $strSql .= ' load_balancer_cookie_value = ' . $this->sqlLoadBalancerCookieValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disable_from_loadbalancer = ' . $this->sqlIsDisableFromLoadbalancer() . ','; } elseif( true == array_key_exists( 'IsDisableFromLoadbalancer', $this->getChangedColumns() ) ) { $strSql .= ' is_disable_from_loadbalancer = ' . $this->sqlIsDisableFromLoadbalancer() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'hardware_id' => $this->getHardwareId(),
			'ps_product_id' => $this->getPsProductId(),
			'portal_ip' => $this->getPortalIp(),
			'load_balancer_cookie_value' => $this->getLoadBalancerCookieValue(),
			'is_disable_from_loadbalancer' => $this->getIsDisableFromLoadbalancer(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>