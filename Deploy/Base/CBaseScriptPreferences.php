<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CScriptPreferences
 * Do not add any new functions to this class.
 */

class CBaseScriptPreferences extends CEosPluralBase {

	/**
	 * @return CScriptPreference[]
	 */
	public static function fetchScriptPreferences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScriptPreference', $objDatabase );
	}

	/**
	 * @return CScriptPreference
	 */
	public static function fetchScriptPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScriptPreference', $objDatabase );
	}

	public static function fetchScriptPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'script_preferences', $objDatabase );
	}

	public static function fetchScriptPreferenceById( $intId, $objDatabase ) {
		return self::fetchScriptPreference( sprintf( 'SELECT * FROM script_preferences WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScriptPreferencesByScriptId( $intScriptId, $objDatabase ) {
		return self::fetchScriptPreferences( sprintf( 'SELECT * FROM script_preferences WHERE script_id = %d', ( int ) $intScriptId ), $objDatabase );
	}

}
?>