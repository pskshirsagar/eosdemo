<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CDeploymentDetails
 * Do not add any new functions to this class.
 */

class CBaseDeploymentDetails extends CEosPluralBase {

	/**
	 * @return CDeploymentDetail[]
	 */
	public static function fetchDeploymentDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDeploymentDetail::class, $objDatabase );
	}

	/**
	 * @return CDeploymentDetail
	 */
	public static function fetchDeploymentDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDeploymentDetail::class, $objDatabase );
	}

	public static function fetchDeploymentDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'deployment_details', $objDatabase );
	}

	public static function fetchDeploymentDetailById( $intId, $objDatabase ) {
		return self::fetchDeploymentDetail( sprintf( 'SELECT * FROM deployment_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDeploymentDetailsByDeploymentId( $intDeploymentId, $objDatabase ) {
		return self::fetchDeploymentDetails( sprintf( 'SELECT * FROM deployment_details WHERE deployment_id = %d', ( int ) $intDeploymentId ), $objDatabase );
	}

	public static function fetchDeploymentDetailsByClusterRepositoryId( $intClusterRepositoryId, $objDatabase ) {
		return self::fetchDeploymentDetails( sprintf( 'SELECT * FROM deployment_details WHERE cluster_repository_id = %d', ( int ) $intClusterRepositoryId ), $objDatabase );
	}

	public static function fetchDeploymentDetailsByPushRevisionId( $strPushRevisionId, $objDatabase ) {
		return self::fetchDeploymentDetails( sprintf( 'SELECT * FROM deployment_details WHERE push_revision_id = \'%s\'', $strPushRevisionId ), $objDatabase );
	}

}
?>