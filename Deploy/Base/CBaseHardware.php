<?php

class CBaseHardware extends CEosSingularBase {

	const TABLE_NAME = 'public.hardwares';

	protected $m_intId;
	protected $m_intHardwareTypeId;
	protected $m_intHardwareStatusTypeId;
	protected $m_intEnvironmentTypeId;
	protected $m_intClusterId;
	protected $m_intMachineTypeId;
	protected $m_intParentHardwareId;
	protected $m_strInstanceId;
	protected $m_strIpAddress;
	protected $m_intAssetNumber;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strPrimaryUses;
	protected $m_strOperatingSystem;
	protected $m_strDellServiceTag;
	protected $m_strBackupNotes;
	protected $m_strNotes;
	protected $m_strMakeModel;
	protected $m_strUpdatesInstalledOn;
	protected $m_strExpiryDate;
	protected $m_strLastBackupTest;
	protected $m_intIsMiscellaneous;
	protected $m_intIsPublished;
	protected $m_boolIsQueueConsumerDeployed;
	protected $m_intUseHostname;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intEnableCodePush;
	protected $m_strConfiguredBranch;

	public function __construct() {
		parent::__construct();

		$this->m_intIsMiscellaneous = '0';
		$this->m_intIsPublished = '1';
		$this->m_boolIsQueueConsumerDeployed = false;
		$this->m_intUseHostname = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['hardware_type_id'] ) && $boolDirectSet ) $this->set( 'm_intHardwareTypeId', trim( $arrValues['hardware_type_id'] ) ); elseif( isset( $arrValues['hardware_type_id'] ) ) $this->setHardwareTypeId( $arrValues['hardware_type_id'] );
		if( isset( $arrValues['hardware_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intHardwareStatusTypeId', trim( $arrValues['hardware_status_type_id'] ) ); elseif( isset( $arrValues['hardware_status_type_id'] ) ) $this->setHardwareStatusTypeId( $arrValues['hardware_status_type_id'] );
		if( isset( $arrValues['environment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEnvironmentTypeId', trim( $arrValues['environment_type_id'] ) ); elseif( isset( $arrValues['environment_type_id'] ) ) $this->setEnvironmentTypeId( $arrValues['environment_type_id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['machine_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMachineTypeId', trim( $arrValues['machine_type_id'] ) ); elseif( isset( $arrValues['machine_type_id'] ) ) $this->setMachineTypeId( $arrValues['machine_type_id'] );
		if( isset( $arrValues['parent_hardware_id'] ) && $boolDirectSet ) $this->set( 'm_intParentHardwareId', trim( $arrValues['parent_hardware_id'] ) ); elseif( isset( $arrValues['parent_hardware_id'] ) ) $this->setParentHardwareId( $arrValues['parent_hardware_id'] );
		if( isset( $arrValues['instance_id'] ) && $boolDirectSet ) $this->set( 'm_strInstanceId', trim( stripcslashes( $arrValues['instance_id'] ) ) ); elseif( isset( $arrValues['instance_id'] ) ) $this->setInstanceId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['instance_id'] ) : $arrValues['instance_id'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( stripcslashes( $arrValues['ip_address'] ) ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ip_address'] ) : $arrValues['ip_address'] );
		if( isset( $arrValues['asset_number'] ) && $boolDirectSet ) $this->set( 'm_intAssetNumber', trim( $arrValues['asset_number'] ) ); elseif( isset( $arrValues['asset_number'] ) ) $this->setAssetNumber( $arrValues['asset_number'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['primary_uses'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryUses', trim( stripcslashes( $arrValues['primary_uses'] ) ) ); elseif( isset( $arrValues['primary_uses'] ) ) $this->setPrimaryUses( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['primary_uses'] ) : $arrValues['primary_uses'] );
		if( isset( $arrValues['operating_system'] ) && $boolDirectSet ) $this->set( 'm_strOperatingSystem', trim( stripcslashes( $arrValues['operating_system'] ) ) ); elseif( isset( $arrValues['operating_system'] ) ) $this->setOperatingSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['operating_system'] ) : $arrValues['operating_system'] );
		if( isset( $arrValues['dell_service_tag'] ) && $boolDirectSet ) $this->set( 'm_strDellServiceTag', trim( stripcslashes( $arrValues['dell_service_tag'] ) ) ); elseif( isset( $arrValues['dell_service_tag'] ) ) $this->setDellServiceTag( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dell_service_tag'] ) : $arrValues['dell_service_tag'] );
		if( isset( $arrValues['backup_notes'] ) && $boolDirectSet ) $this->set( 'm_strBackupNotes', trim( stripcslashes( $arrValues['backup_notes'] ) ) ); elseif( isset( $arrValues['backup_notes'] ) ) $this->setBackupNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['backup_notes'] ) : $arrValues['backup_notes'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['make_model'] ) && $boolDirectSet ) $this->set( 'm_strMakeModel', trim( stripcslashes( $arrValues['make_model'] ) ) ); elseif( isset( $arrValues['make_model'] ) ) $this->setMakeModel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['make_model'] ) : $arrValues['make_model'] );
		if( isset( $arrValues['updates_installed_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatesInstalledOn', trim( $arrValues['updates_installed_on'] ) ); elseif( isset( $arrValues['updates_installed_on'] ) ) $this->setUpdatesInstalledOn( $arrValues['updates_installed_on'] );
		if( isset( $arrValues['expiry_date'] ) && $boolDirectSet ) $this->set( 'm_strExpiryDate', trim( $arrValues['expiry_date'] ) ); elseif( isset( $arrValues['expiry_date'] ) ) $this->setExpiryDate( $arrValues['expiry_date'] );
		if( isset( $arrValues['last_backup_test'] ) && $boolDirectSet ) $this->set( 'm_strLastBackupTest', trim( $arrValues['last_backup_test'] ) ); elseif( isset( $arrValues['last_backup_test'] ) ) $this->setLastBackupTest( $arrValues['last_backup_test'] );
		if( isset( $arrValues['is_miscellaneous'] ) && $boolDirectSet ) $this->set( 'm_intIsMiscellaneous', trim( $arrValues['is_miscellaneous'] ) ); elseif( isset( $arrValues['is_miscellaneous'] ) ) $this->setIsMiscellaneous( $arrValues['is_miscellaneous'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_queue_consumer_deployed'] ) && $boolDirectSet ) $this->set( 'm_boolIsQueueConsumerDeployed', trim( stripcslashes( $arrValues['is_queue_consumer_deployed'] ) ) ); elseif( isset( $arrValues['is_queue_consumer_deployed'] ) ) $this->setIsQueueConsumerDeployed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_queue_consumer_deployed'] ) : $arrValues['is_queue_consumer_deployed'] );
		if( isset( $arrValues['use_hostname'] ) && $boolDirectSet ) $this->set( 'm_intUseHostname', trim( $arrValues['use_hostname'] ) ); elseif( isset( $arrValues['use_hostname'] ) ) $this->setUseHostname( $arrValues['use_hostname'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['enable_code_push'] ) && $boolDirectSet ) $this->set( 'm_intEnableCodePush', trim( $arrValues['enable_code_push'] ) ); elseif( isset( $arrValues['enable_code_push'] ) ) $this->setEnableCodePush( $arrValues['enable_code_push'] );
		if( isset( $arrValues['configured_branch'] ) && $boolDirectSet ) $this->set( 'm_strConfiguredBranch', trim( stripcslashes( $arrValues['configured_branch'] ) ) ); elseif( isset( $arrValues['configured_branch'] ) ) $this->setConfiguredBranch( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['configured_branch'] ) : $arrValues['configured_branch'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setHardwareTypeId( $intHardwareTypeId ) {
		$this->set( 'm_intHardwareTypeId', CStrings::strToIntDef( $intHardwareTypeId, NULL, false ) );
	}

	public function getHardwareTypeId() {
		return $this->m_intHardwareTypeId;
	}

	public function sqlHardwareTypeId() {
		return ( true == isset( $this->m_intHardwareTypeId ) ) ? ( string ) $this->m_intHardwareTypeId : 'NULL';
	}

	public function setHardwareStatusTypeId( $intHardwareStatusTypeId ) {
		$this->set( 'm_intHardwareStatusTypeId', CStrings::strToIntDef( $intHardwareStatusTypeId, NULL, false ) );
	}

	public function getHardwareStatusTypeId() {
		return $this->m_intHardwareStatusTypeId;
	}

	public function sqlHardwareStatusTypeId() {
		return ( true == isset( $this->m_intHardwareStatusTypeId ) ) ? ( string ) $this->m_intHardwareStatusTypeId : 'NULL';
	}

	public function setEnvironmentTypeId( $intEnvironmentTypeId ) {
		$this->set( 'm_intEnvironmentTypeId', CStrings::strToIntDef( $intEnvironmentTypeId, NULL, false ) );
	}

	public function getEnvironmentTypeId() {
		return $this->m_intEnvironmentTypeId;
	}

	public function sqlEnvironmentTypeId() {
		return ( true == isset( $this->m_intEnvironmentTypeId ) ) ? ( string ) $this->m_intEnvironmentTypeId : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : 'NULL';
	}

	public function setMachineTypeId( $intMachineTypeId ) {
		$this->set( 'm_intMachineTypeId', CStrings::strToIntDef( $intMachineTypeId, NULL, false ) );
	}

	public function getMachineTypeId() {
		return $this->m_intMachineTypeId;
	}

	public function sqlMachineTypeId() {
		return ( true == isset( $this->m_intMachineTypeId ) ) ? ( string ) $this->m_intMachineTypeId : 'NULL';
	}

	public function setParentHardwareId( $intParentHardwareId ) {
		$this->set( 'm_intParentHardwareId', CStrings::strToIntDef( $intParentHardwareId, NULL, false ) );
	}

	public function getParentHardwareId() {
		return $this->m_intParentHardwareId;
	}

	public function sqlParentHardwareId() {
		return ( true == isset( $this->m_intParentHardwareId ) ) ? ( string ) $this->m_intParentHardwareId : 'NULL';
	}

	public function setInstanceId( $strInstanceId ) {
		$this->set( 'm_strInstanceId', CStrings::strTrimDef( $strInstanceId, 30, NULL, true ) );
	}

	public function getInstanceId() {
		return $this->m_strInstanceId;
	}

	public function sqlInstanceId() {
		return ( true == isset( $this->m_strInstanceId ) ) ? '\'' . addslashes( $this->m_strInstanceId ) . '\'' : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 50, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? '\'' . addslashes( $this->m_strIpAddress ) . '\'' : 'NULL';
	}

	public function setAssetNumber( $intAssetNumber ) {
		$this->set( 'm_intAssetNumber', CStrings::strToIntDef( $intAssetNumber, NULL, false ) );
	}

	public function getAssetNumber() {
		return $this->m_intAssetNumber;
	}

	public function sqlAssetNumber() {
		return ( true == isset( $this->m_intAssetNumber ) ) ? ( string ) $this->m_intAssetNumber : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setPrimaryUses( $strPrimaryUses ) {
		$this->set( 'm_strPrimaryUses', CStrings::strTrimDef( $strPrimaryUses, 240, NULL, true ) );
	}

	public function getPrimaryUses() {
		return $this->m_strPrimaryUses;
	}

	public function sqlPrimaryUses() {
		return ( true == isset( $this->m_strPrimaryUses ) ) ? '\'' . addslashes( $this->m_strPrimaryUses ) . '\'' : 'NULL';
	}

	public function setOperatingSystem( $strOperatingSystem ) {
		$this->set( 'm_strOperatingSystem', CStrings::strTrimDef( $strOperatingSystem, 240, NULL, true ) );
	}

	public function getOperatingSystem() {
		return $this->m_strOperatingSystem;
	}

	public function sqlOperatingSystem() {
		return ( true == isset( $this->m_strOperatingSystem ) ) ? '\'' . addslashes( $this->m_strOperatingSystem ) . '\'' : 'NULL';
	}

	public function setDellServiceTag( $strDellServiceTag ) {
		$this->set( 'm_strDellServiceTag', CStrings::strTrimDef( $strDellServiceTag, 240, NULL, true ) );
	}

	public function getDellServiceTag() {
		return $this->m_strDellServiceTag;
	}

	public function sqlDellServiceTag() {
		return ( true == isset( $this->m_strDellServiceTag ) ) ? '\'' . addslashes( $this->m_strDellServiceTag ) . '\'' : 'NULL';
	}

	public function setBackupNotes( $strBackupNotes ) {
		$this->set( 'm_strBackupNotes', CStrings::strTrimDef( $strBackupNotes, 240, NULL, true ) );
	}

	public function getBackupNotes() {
		return $this->m_strBackupNotes;
	}

	public function sqlBackupNotes() {
		return ( true == isset( $this->m_strBackupNotes ) ) ? '\'' . addslashes( $this->m_strBackupNotes ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 240, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setMakeModel( $strMakeModel ) {
		$this->set( 'm_strMakeModel', CStrings::strTrimDef( $strMakeModel, 50, NULL, true ) );
	}

	public function getMakeModel() {
		return $this->m_strMakeModel;
	}

	public function sqlMakeModel() {
		return ( true == isset( $this->m_strMakeModel ) ) ? '\'' . addslashes( $this->m_strMakeModel ) . '\'' : 'NULL';
	}

	public function setUpdatesInstalledOn( $strUpdatesInstalledOn ) {
		$this->set( 'm_strUpdatesInstalledOn', CStrings::strTrimDef( $strUpdatesInstalledOn, -1, NULL, true ) );
	}

	public function getUpdatesInstalledOn() {
		return $this->m_strUpdatesInstalledOn;
	}

	public function sqlUpdatesInstalledOn() {
		return ( true == isset( $this->m_strUpdatesInstalledOn ) ) ? '\'' . $this->m_strUpdatesInstalledOn . '\'' : 'NULL';
	}

	public function setExpiryDate( $strExpiryDate ) {
		$this->set( 'm_strExpiryDate', CStrings::strTrimDef( $strExpiryDate, -1, NULL, true ) );
	}

	public function getExpiryDate() {
		return $this->m_strExpiryDate;
	}

	public function sqlExpiryDate() {
		return ( true == isset( $this->m_strExpiryDate ) ) ? '\'' . $this->m_strExpiryDate . '\'' : 'NULL';
	}

	public function setLastBackupTest( $strLastBackupTest ) {
		$this->set( 'm_strLastBackupTest', CStrings::strTrimDef( $strLastBackupTest, -1, NULL, true ) );
	}

	public function getLastBackupTest() {
		return $this->m_strLastBackupTest;
	}

	public function sqlLastBackupTest() {
		return ( true == isset( $this->m_strLastBackupTest ) ) ? '\'' . $this->m_strLastBackupTest . '\'' : 'NULL';
	}

	public function setIsMiscellaneous( $intIsMiscellaneous ) {
		$this->set( 'm_intIsMiscellaneous', CStrings::strToIntDef( $intIsMiscellaneous, NULL, false ) );
	}

	public function getIsMiscellaneous() {
		return $this->m_intIsMiscellaneous;
	}

	public function sqlIsMiscellaneous() {
		return ( true == isset( $this->m_intIsMiscellaneous ) ) ? ( string ) $this->m_intIsMiscellaneous : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsQueueConsumerDeployed( $boolIsQueueConsumerDeployed ) {
		$this->set( 'm_boolIsQueueConsumerDeployed', CStrings::strToBool( $boolIsQueueConsumerDeployed ) );
	}

	public function getIsQueueConsumerDeployed() {
		return $this->m_boolIsQueueConsumerDeployed;
	}

	public function sqlIsQueueConsumerDeployed() {
		return ( true == isset( $this->m_boolIsQueueConsumerDeployed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsQueueConsumerDeployed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseHostname( $intUseHostname ) {
		$this->set( 'm_intUseHostname', CStrings::strToIntDef( $intUseHostname, NULL, false ) );
	}

	public function getUseHostname() {
		return $this->m_intUseHostname;
	}

	public function sqlUseHostname() {
		return ( true == isset( $this->m_intUseHostname ) ) ? ( string ) $this->m_intUseHostname : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setEnableCodePush( $intEnableCodePush ) {
		$this->set( 'm_intEnableCodePush', CStrings::strToIntDef( $intEnableCodePush, NULL, false ) );
	}

	public function getEnableCodePush() {
		return $this->m_intEnableCodePush;
	}

	public function sqlEnableCodePush() {
		return ( true == isset( $this->m_intEnableCodePush ) ) ? ( string ) $this->m_intEnableCodePush : 'NULL';
	}

	public function setConfiguredBranch( $strConfiguredBranch ) {
		$this->set( 'm_strConfiguredBranch', CStrings::strTrimDef( $strConfiguredBranch, 50, NULL, true ) );
	}

	public function getConfiguredBranch() {
		return $this->m_strConfiguredBranch;
	}

	public function sqlConfiguredBranch() {
		return ( true == isset( $this->m_strConfiguredBranch ) ) ? '\'' . addslashes( $this->m_strConfiguredBranch ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, hardware_type_id, hardware_status_type_id, environment_type_id, cluster_id, machine_type_id, parent_hardware_id, instance_id, ip_address, asset_number, name, description, primary_uses, operating_system, dell_service_tag, backup_notes, notes, make_model, updates_installed_on, expiry_date, last_backup_test, is_miscellaneous, is_published, is_queue_consumer_deployed, use_hostname, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, enable_code_push, configured_branch )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlHardwareTypeId() . ', ' .
 						$this->sqlHardwareStatusTypeId() . ', ' .
 						$this->sqlEnvironmentTypeId() . ', ' .
 						$this->sqlClusterId() . ', ' .
 						$this->sqlMachineTypeId() . ', ' .
 						$this->sqlParentHardwareId() . ', ' .
 						$this->sqlInstanceId() . ', ' .
 						$this->sqlIpAddress() . ', ' .
 						$this->sqlAssetNumber() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlPrimaryUses() . ', ' .
 						$this->sqlOperatingSystem() . ', ' .
 						$this->sqlDellServiceTag() . ', ' .
 						$this->sqlBackupNotes() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlMakeModel() . ', ' .
 						$this->sqlUpdatesInstalledOn() . ', ' .
 						$this->sqlExpiryDate() . ', ' .
 						$this->sqlLastBackupTest() . ', ' .
 						$this->sqlIsMiscellaneous() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlIsQueueConsumerDeployed() . ', ' .
 						$this->sqlUseHostname() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlEnableCodePush() . ', ' .
 						$this->sqlConfiguredBranch() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hardware_type_id = ' . $this->sqlHardwareTypeId() . ','; } elseif( true == array_key_exists( 'HardwareTypeId', $this->getChangedColumns() ) ) { $strSql .= ' hardware_type_id = ' . $this->sqlHardwareTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hardware_status_type_id = ' . $this->sqlHardwareStatusTypeId() . ','; } elseif( true == array_key_exists( 'HardwareStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' hardware_status_type_id = ' . $this->sqlHardwareStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' environment_type_id = ' . $this->sqlEnvironmentTypeId() . ','; } elseif( true == array_key_exists( 'EnvironmentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' environment_type_id = ' . $this->sqlEnvironmentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' machine_type_id = ' . $this->sqlMachineTypeId() . ','; } elseif( true == array_key_exists( 'MachineTypeId', $this->getChangedColumns() ) ) { $strSql .= ' machine_type_id = ' . $this->sqlMachineTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_hardware_id = ' . $this->sqlParentHardwareId() . ','; } elseif( true == array_key_exists( 'ParentHardwareId', $this->getChangedColumns() ) ) { $strSql .= ' parent_hardware_id = ' . $this->sqlParentHardwareId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' instance_id = ' . $this->sqlInstanceId() . ','; } elseif( true == array_key_exists( 'InstanceId', $this->getChangedColumns() ) ) { $strSql .= ' instance_id = ' . $this->sqlInstanceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_number = ' . $this->sqlAssetNumber() . ','; } elseif( true == array_key_exists( 'AssetNumber', $this->getChangedColumns() ) ) { $strSql .= ' asset_number = ' . $this->sqlAssetNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_uses = ' . $this->sqlPrimaryUses() . ','; } elseif( true == array_key_exists( 'PrimaryUses', $this->getChangedColumns() ) ) { $strSql .= ' primary_uses = ' . $this->sqlPrimaryUses() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' operating_system = ' . $this->sqlOperatingSystem() . ','; } elseif( true == array_key_exists( 'OperatingSystem', $this->getChangedColumns() ) ) { $strSql .= ' operating_system = ' . $this->sqlOperatingSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dell_service_tag = ' . $this->sqlDellServiceTag() . ','; } elseif( true == array_key_exists( 'DellServiceTag', $this->getChangedColumns() ) ) { $strSql .= ' dell_service_tag = ' . $this->sqlDellServiceTag() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' backup_notes = ' . $this->sqlBackupNotes() . ','; } elseif( true == array_key_exists( 'BackupNotes', $this->getChangedColumns() ) ) { $strSql .= ' backup_notes = ' . $this->sqlBackupNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' make_model = ' . $this->sqlMakeModel() . ','; } elseif( true == array_key_exists( 'MakeModel', $this->getChangedColumns() ) ) { $strSql .= ' make_model = ' . $this->sqlMakeModel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' updates_installed_on = ' . $this->sqlUpdatesInstalledOn() . ','; } elseif( true == array_key_exists( 'UpdatesInstalledOn', $this->getChangedColumns() ) ) { $strSql .= ' updates_installed_on = ' . $this->sqlUpdatesInstalledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expiry_date = ' . $this->sqlExpiryDate() . ','; } elseif( true == array_key_exists( 'ExpiryDate', $this->getChangedColumns() ) ) { $strSql .= ' expiry_date = ' . $this->sqlExpiryDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_backup_test = ' . $this->sqlLastBackupTest() . ','; } elseif( true == array_key_exists( 'LastBackupTest', $this->getChangedColumns() ) ) { $strSql .= ' last_backup_test = ' . $this->sqlLastBackupTest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_miscellaneous = ' . $this->sqlIsMiscellaneous() . ','; } elseif( true == array_key_exists( 'IsMiscellaneous', $this->getChangedColumns() ) ) { $strSql .= ' is_miscellaneous = ' . $this->sqlIsMiscellaneous() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_queue_consumer_deployed = ' . $this->sqlIsQueueConsumerDeployed() . ','; } elseif( true == array_key_exists( 'IsQueueConsumerDeployed', $this->getChangedColumns() ) ) { $strSql .= ' is_queue_consumer_deployed = ' . $this->sqlIsQueueConsumerDeployed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_hostname = ' . $this->sqlUseHostname() . ','; } elseif( true == array_key_exists( 'UseHostname', $this->getChangedColumns() ) ) { $strSql .= ' use_hostname = ' . $this->sqlUseHostname() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enable_code_push = ' . $this->sqlEnableCodePush() . ','; } elseif( true == array_key_exists( 'EnableCodePush', $this->getChangedColumns() ) ) { $strSql .= ' enable_code_push = ' . $this->sqlEnableCodePush() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' configured_branch = ' . $this->sqlConfiguredBranch() . ','; } elseif( true == array_key_exists( 'ConfiguredBranch', $this->getChangedColumns() ) ) { $strSql .= ' configured_branch = ' . $this->sqlConfiguredBranch() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'hardware_type_id' => $this->getHardwareTypeId(),
			'hardware_status_type_id' => $this->getHardwareStatusTypeId(),
			'environment_type_id' => $this->getEnvironmentTypeId(),
			'cluster_id' => $this->getClusterId(),
			'machine_type_id' => $this->getMachineTypeId(),
			'parent_hardware_id' => $this->getParentHardwareId(),
			'instance_id' => $this->getInstanceId(),
			'ip_address' => $this->getIpAddress(),
			'asset_number' => $this->getAssetNumber(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'primary_uses' => $this->getPrimaryUses(),
			'operating_system' => $this->getOperatingSystem(),
			'dell_service_tag' => $this->getDellServiceTag(),
			'backup_notes' => $this->getBackupNotes(),
			'notes' => $this->getNotes(),
			'make_model' => $this->getMakeModel(),
			'updates_installed_on' => $this->getUpdatesInstalledOn(),
			'expiry_date' => $this->getExpiryDate(),
			'last_backup_test' => $this->getLastBackupTest(),
			'is_miscellaneous' => $this->getIsMiscellaneous(),
			'is_published' => $this->getIsPublished(),
			'is_queue_consumer_deployed' => $this->getIsQueueConsumerDeployed(),
			'use_hostname' => $this->getUseHostname(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'enable_code_push' => $this->getEnableCodePush(),
			'configured_branch' => $this->getConfiguredBranch()
		);
	}

}
?>