<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CHardwareStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseHardwareStatusTypes extends CEosPluralBase {

	/**
	 * @return CHardwareStatusType[]
	 */
	public static function fetchHardwareStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CHardwareStatusType', $objDatabase );
	}

	/**
	 * @return CHardwareStatusType
	 */
	public static function fetchHardwareStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CHardwareStatusType', $objDatabase );
	}

	public static function fetchHardwareStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'hardware_status_types', $objDatabase );
	}

	public static function fetchHardwareStatusTypeById( $intId, $objDatabase ) {
		return self::fetchHardwareStatusType( sprintf( 'SELECT * FROM hardware_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>