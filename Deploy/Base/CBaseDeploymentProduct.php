<?php

class CBaseDeploymentProduct extends CEosSingularBase {

	const TABLE_NAME = 'public.deployment_products';

	protected $m_intId;
	protected $m_intClusterId;
	protected $m_intPsProductId;
	protected $m_strName;
	protected $m_intIsGitHosted;
	protected $m_intIsDeployed;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strCloneUrl;

	public function __construct() {
		parent::__construct();

		$this->m_intIsGitHosted = '0';
		$this->m_intIsDeployed = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['is_git_hosted'] ) && $boolDirectSet ) $this->set( 'm_intIsGitHosted', trim( $arrValues['is_git_hosted'] ) ); elseif( isset( $arrValues['is_git_hosted'] ) ) $this->setIsGitHosted( $arrValues['is_git_hosted'] );
		if( isset( $arrValues['is_deployed'] ) && $boolDirectSet ) $this->set( 'm_intIsDeployed', trim( $arrValues['is_deployed'] ) ); elseif( isset( $arrValues['is_deployed'] ) ) $this->setIsDeployed( $arrValues['is_deployed'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['clone_url'] ) && $boolDirectSet ) $this->set( 'm_strCloneUrl', trim( stripcslashes( $arrValues['clone_url'] ) ) ); elseif( isset( $arrValues['clone_url'] ) ) $this->setCloneUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['clone_url'] ) : $arrValues['clone_url'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 40, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setIsGitHosted( $intIsGitHosted ) {
		$this->set( 'm_intIsGitHosted', CStrings::strToIntDef( $intIsGitHosted, NULL, false ) );
	}

	public function getIsGitHosted() {
		return $this->m_intIsGitHosted;
	}

	public function sqlIsGitHosted() {
		return ( true == isset( $this->m_intIsGitHosted ) ) ? ( string ) $this->m_intIsGitHosted : '0';
	}

	public function setIsDeployed( $intIsDeployed ) {
		$this->set( 'm_intIsDeployed', CStrings::strToIntDef( $intIsDeployed, NULL, false ) );
	}

	public function getIsDeployed() {
		return $this->m_intIsDeployed;
	}

	public function sqlIsDeployed() {
		return ( true == isset( $this->m_intIsDeployed ) ) ? ( string ) $this->m_intIsDeployed : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCloneUrl( $strCloneUrl ) {
		$this->set( 'm_strCloneUrl', CStrings::strTrimDef( $strCloneUrl, -1, NULL, true ) );
	}

	public function getCloneUrl() {
		return $this->m_strCloneUrl;
	}

	public function sqlCloneUrl() {
		return ( true == isset( $this->m_strCloneUrl ) ) ? '\'' . addslashes( $this->m_strCloneUrl ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cluster_id, ps_product_id, name, is_git_hosted, is_deployed, updated_by, updated_on, created_by, created_on, clone_url )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlClusterId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlIsGitHosted() . ', ' .
 						$this->sqlIsDeployed() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlCloneUrl() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_git_hosted = ' . $this->sqlIsGitHosted() . ','; } elseif( true == array_key_exists( 'IsGitHosted', $this->getChangedColumns() ) ) { $strSql .= ' is_git_hosted = ' . $this->sqlIsGitHosted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deployed = ' . $this->sqlIsDeployed() . ','; } elseif( true == array_key_exists( 'IsDeployed', $this->getChangedColumns() ) ) { $strSql .= ' is_deployed = ' . $this->sqlIsDeployed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clone_url = ' . $this->sqlCloneUrl() . ','; } elseif( true == array_key_exists( 'CloneUrl', $this->getChangedColumns() ) ) { $strSql .= ' clone_url = ' . $this->sqlCloneUrl() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cluster_id' => $this->getClusterId(),
			'ps_product_id' => $this->getPsProductId(),
			'name' => $this->getName(),
			'is_git_hosted' => $this->getIsGitHosted(),
			'is_deployed' => $this->getIsDeployed(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'clone_url' => $this->getCloneUrl()
		);
	}

}
?>