<?php

class CBaseDeployment extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.deployments';

	protected $m_intId;
	protected $m_intClusterId;
	protected $m_intDeploymentTypeId;
	protected $m_intParentDeploymentId;
	protected $m_strGoogleCalendarEventId;
	protected $m_strExecCommand;
	protected $m_strTaskIds;
	protected $m_strDatabaseIds;
	protected $m_strParameters;
	protected $m_strDescription;
	protected $m_intVerifiedConflictedFiles;
	protected $m_intSvnFileId;
	protected $m_strScheduledDatetime;
	protected $m_strStartDatetime;
	protected $m_strEndDatetime;
	protected $m_strCompletedOn;
	protected $m_strFailedOn;
	protected $m_strDeploymentDatetime;
	protected $m_intIsCriticalPush;
	protected $m_intIsRunOnAllClusters;
	protected $m_intIsAutoExecute;
	protected $m_boolIsDr;
	protected $m_strSvnCommittedOn;
	protected $m_intApprovedBy;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intPsProductId;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intClusterId = '1';
		$this->m_intVerifiedConflictedFiles = '0';
		$this->m_intIsRunOnAllClusters = '0';
		$this->m_intIsAutoExecute = '0';
		$this->m_boolIsDr = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['deployment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDeploymentTypeId', trim( $arrValues['deployment_type_id'] ) ); elseif( isset( $arrValues['deployment_type_id'] ) ) $this->setDeploymentTypeId( $arrValues['deployment_type_id'] );
		if( isset( $arrValues['parent_deployment_id'] ) && $boolDirectSet ) $this->set( 'm_intParentDeploymentId', trim( $arrValues['parent_deployment_id'] ) ); elseif( isset( $arrValues['parent_deployment_id'] ) ) $this->setParentDeploymentId( $arrValues['parent_deployment_id'] );
		if( isset( $arrValues['google_calendar_event_id'] ) && $boolDirectSet ) $this->set( 'm_strGoogleCalendarEventId', trim( stripcslashes( $arrValues['google_calendar_event_id'] ) ) ); elseif( isset( $arrValues['google_calendar_event_id'] ) ) $this->setGoogleCalendarEventId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['google_calendar_event_id'] ) : $arrValues['google_calendar_event_id'] );
		if( isset( $arrValues['exec_command'] ) && $boolDirectSet ) $this->set( 'm_strExecCommand', trim( stripcslashes( $arrValues['exec_command'] ) ) ); elseif( isset( $arrValues['exec_command'] ) ) $this->setExecCommand( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['exec_command'] ) : $arrValues['exec_command'] );
		if( isset( $arrValues['task_ids'] ) && $boolDirectSet ) $this->set( 'm_strTaskIds', trim( stripcslashes( $arrValues['task_ids'] ) ) ); elseif( isset( $arrValues['task_ids'] ) ) $this->setTaskIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['task_ids'] ) : $arrValues['task_ids'] );
		if( isset( $arrValues['database_ids'] ) && $boolDirectSet ) $this->set( 'm_strDatabaseIds', trim( stripcslashes( $arrValues['database_ids'] ) ) ); elseif( isset( $arrValues['database_ids'] ) ) $this->setDatabaseIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['database_ids'] ) : $arrValues['database_ids'] );
		if( isset( $arrValues['parameters'] ) && $boolDirectSet ) $this->set( 'm_strParameters', trim( stripcslashes( $arrValues['parameters'] ) ) ); elseif( isset( $arrValues['parameters'] ) ) $this->setParameters( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['parameters'] ) : $arrValues['parameters'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['verified_conflicted_files'] ) && $boolDirectSet ) $this->set( 'm_intVerifiedConflictedFiles', trim( $arrValues['verified_conflicted_files'] ) ); elseif( isset( $arrValues['verified_conflicted_files'] ) ) $this->setVerifiedConflictedFiles( $arrValues['verified_conflicted_files'] );
		if( isset( $arrValues['svn_file_id'] ) && $boolDirectSet ) $this->set( 'm_intSvnFileId', trim( $arrValues['svn_file_id'] ) ); elseif( isset( $arrValues['svn_file_id'] ) ) $this->setSvnFileId( $arrValues['svn_file_id'] );
		if( isset( $arrValues['scheduled_datetime'] ) && $boolDirectSet ) $this->set( 'm_strScheduledDatetime', trim( $arrValues['scheduled_datetime'] ) ); elseif( isset( $arrValues['scheduled_datetime'] ) ) $this->setScheduledDatetime( $arrValues['scheduled_datetime'] );
		if( isset( $arrValues['start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartDatetime', trim( $arrValues['start_datetime'] ) ); elseif( isset( $arrValues['start_datetime'] ) ) $this->setStartDatetime( $arrValues['start_datetime'] );
		if( isset( $arrValues['end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndDatetime', trim( $arrValues['end_datetime'] ) ); elseif( isset( $arrValues['end_datetime'] ) ) $this->setEndDatetime( $arrValues['end_datetime'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['failed_on'] ) && $boolDirectSet ) $this->set( 'm_strFailedOn', trim( $arrValues['failed_on'] ) ); elseif( isset( $arrValues['failed_on'] ) ) $this->setFailedOn( $arrValues['failed_on'] );
		if( isset( $arrValues['deployment_datetime'] ) && $boolDirectSet ) $this->set( 'm_strDeploymentDatetime', trim( $arrValues['deployment_datetime'] ) ); elseif( isset( $arrValues['deployment_datetime'] ) ) $this->setDeploymentDatetime( $arrValues['deployment_datetime'] );
		if( isset( $arrValues['is_critical_push'] ) && $boolDirectSet ) $this->set( 'm_intIsCriticalPush', trim( $arrValues['is_critical_push'] ) ); elseif( isset( $arrValues['is_critical_push'] ) ) $this->setIsCriticalPush( $arrValues['is_critical_push'] );
		if( isset( $arrValues['is_run_on_all_clusters'] ) && $boolDirectSet ) $this->set( 'm_intIsRunOnAllClusters', trim( $arrValues['is_run_on_all_clusters'] ) ); elseif( isset( $arrValues['is_run_on_all_clusters'] ) ) $this->setIsRunOnAllClusters( $arrValues['is_run_on_all_clusters'] );
		if( isset( $arrValues['is_auto_execute'] ) && $boolDirectSet ) $this->set( 'm_intIsAutoExecute', trim( $arrValues['is_auto_execute'] ) ); elseif( isset( $arrValues['is_auto_execute'] ) ) $this->setIsAutoExecute( $arrValues['is_auto_execute'] );
		if( isset( $arrValues['is_dr'] ) && $boolDirectSet ) $this->set( 'm_boolIsDr', trim( stripcslashes( $arrValues['is_dr'] ) ) ); elseif( isset( $arrValues['is_dr'] ) ) $this->setIsDr( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_dr'] ) : $arrValues['is_dr'] );
		if( isset( $arrValues['svn_committed_on'] ) && $boolDirectSet ) $this->set( 'm_strSvnCommittedOn', trim( $arrValues['svn_committed_on'] ) ); elseif( isset( $arrValues['svn_committed_on'] ) ) $this->setSvnCommittedOn( $arrValues['svn_committed_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : '1';
	}

	public function setDeploymentTypeId( $intDeploymentTypeId ) {
		$this->set( 'm_intDeploymentTypeId', CStrings::strToIntDef( $intDeploymentTypeId, NULL, false ) );
	}

	public function getDeploymentTypeId() {
		return $this->m_intDeploymentTypeId;
	}

	public function sqlDeploymentTypeId() {
		return ( true == isset( $this->m_intDeploymentTypeId ) ) ? ( string ) $this->m_intDeploymentTypeId : 'NULL';
	}

	public function setParentDeploymentId( $intParentDeploymentId ) {
		$this->set( 'm_intParentDeploymentId', CStrings::strToIntDef( $intParentDeploymentId, NULL, false ) );
	}

	public function getParentDeploymentId() {
		return $this->m_intParentDeploymentId;
	}

	public function sqlParentDeploymentId() {
		return ( true == isset( $this->m_intParentDeploymentId ) ) ? ( string ) $this->m_intParentDeploymentId : 'NULL';
	}

	public function setGoogleCalendarEventId( $strGoogleCalendarEventId ) {
		$this->set( 'm_strGoogleCalendarEventId', CStrings::strTrimDef( $strGoogleCalendarEventId, 100, NULL, true ) );
	}

	public function getGoogleCalendarEventId() {
		return $this->m_strGoogleCalendarEventId;
	}

	public function sqlGoogleCalendarEventId() {
		return ( true == isset( $this->m_strGoogleCalendarEventId ) ) ? '\'' . addslashes( $this->m_strGoogleCalendarEventId ) . '\'' : 'NULL';
	}

	public function setExecCommand( $strExecCommand ) {
		$this->set( 'm_strExecCommand', CStrings::strTrimDef( $strExecCommand, 1024, NULL, true ) );
	}

	public function getExecCommand() {
		return $this->m_strExecCommand;
	}

	public function sqlExecCommand() {
		return ( true == isset( $this->m_strExecCommand ) ) ? '\'' . addslashes( $this->m_strExecCommand ) . '\'' : 'NULL';
	}

	public function setTaskIds( $strTaskIds ) {
		$this->set( 'm_strTaskIds', CStrings::strTrimDef( $strTaskIds, 1000, NULL, true ) );
	}

	public function getTaskIds() {
		return $this->m_strTaskIds;
	}

	public function sqlTaskIds() {
		return ( true == isset( $this->m_strTaskIds ) ) ? '\'' . addslashes( $this->m_strTaskIds ) . '\'' : 'NULL';
	}

	public function setDatabaseIds( $strDatabaseIds ) {
		$this->set( 'm_strDatabaseIds', CStrings::strTrimDef( $strDatabaseIds, 1024, NULL, true ) );
	}

	public function getDatabaseIds() {
		return $this->m_strDatabaseIds;
	}

	public function sqlDatabaseIds() {
		return ( true == isset( $this->m_strDatabaseIds ) ) ? '\'' . addslashes( $this->m_strDatabaseIds ) . '\'' : 'NULL';
	}

	public function setParameters( $strParameters ) {
		$this->set( 'm_strParameters', CStrings::strTrimDef( $strParameters, -1, NULL, true ) );
	}

	public function getParameters() {
		return $this->m_strParameters;
	}

	public function sqlParameters() {
		return ( true == isset( $this->m_strParameters ) ) ? '\'' . addslashes( $this->m_strParameters ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setVerifiedConflictedFiles( $intVerifiedConflictedFiles ) {
		$this->set( 'm_intVerifiedConflictedFiles', CStrings::strToIntDef( $intVerifiedConflictedFiles, NULL, false ) );
	}

	public function getVerifiedConflictedFiles() {
		return $this->m_intVerifiedConflictedFiles;
	}

	public function sqlVerifiedConflictedFiles() {
		return ( true == isset( $this->m_intVerifiedConflictedFiles ) ) ? ( string ) $this->m_intVerifiedConflictedFiles : '0';
	}

	public function setSvnFileId( $intSvnFileId ) {
		$this->set( 'm_intSvnFileId', CStrings::strToIntDef( $intSvnFileId, NULL, false ) );
	}

	public function getSvnFileId() {
		return $this->m_intSvnFileId;
	}

	public function sqlSvnFileId() {
		return ( true == isset( $this->m_intSvnFileId ) ) ? ( string ) $this->m_intSvnFileId : 'NULL';
	}

	public function setScheduledDatetime( $strScheduledDatetime ) {
		$this->set( 'm_strScheduledDatetime', CStrings::strTrimDef( $strScheduledDatetime, -1, NULL, true ) );
	}

	public function getScheduledDatetime() {
		return $this->m_strScheduledDatetime;
	}

	public function sqlScheduledDatetime() {
		return ( true == isset( $this->m_strScheduledDatetime ) ) ? '\'' . $this->m_strScheduledDatetime . '\'' : 'NOW()';
	}

	public function setStartDatetime( $strStartDatetime ) {
		$this->set( 'm_strStartDatetime', CStrings::strTrimDef( $strStartDatetime, -1, NULL, true ) );
	}

	public function getStartDatetime() {
		return $this->m_strStartDatetime;
	}

	public function sqlStartDatetime() {
		return ( true == isset( $this->m_strStartDatetime ) ) ? '\'' . $this->m_strStartDatetime . '\'' : 'NULL';
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->set( 'm_strEndDatetime', CStrings::strTrimDef( $strEndDatetime, -1, NULL, true ) );
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function sqlEndDatetime() {
		return ( true == isset( $this->m_strEndDatetime ) ) ? '\'' . $this->m_strEndDatetime . '\'' : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setFailedOn( $strFailedOn ) {
		$this->set( 'm_strFailedOn', CStrings::strTrimDef( $strFailedOn, -1, NULL, true ) );
	}

	public function getFailedOn() {
		return $this->m_strFailedOn;
	}

	public function sqlFailedOn() {
		return ( true == isset( $this->m_strFailedOn ) ) ? '\'' . $this->m_strFailedOn . '\'' : 'NULL';
	}

	public function setDeploymentDatetime( $strDeploymentDatetime ) {
		$this->set( 'm_strDeploymentDatetime', CStrings::strTrimDef( $strDeploymentDatetime, -1, NULL, true ) );
	}

	public function getDeploymentDatetime() {
		return $this->m_strDeploymentDatetime;
	}

	public function sqlDeploymentDatetime() {
		return ( true == isset( $this->m_strDeploymentDatetime ) ) ? '\'' . $this->m_strDeploymentDatetime . '\'' : 'NOW()';
	}

	public function setIsCriticalPush( $intIsCriticalPush ) {
		$this->set( 'm_intIsCriticalPush', CStrings::strToIntDef( $intIsCriticalPush, NULL, false ) );
	}

	public function getIsCriticalPush() {
		return $this->m_intIsCriticalPush;
	}

	public function sqlIsCriticalPush() {
		return ( true == isset( $this->m_intIsCriticalPush ) ) ? ( string ) $this->m_intIsCriticalPush : 'NULL';
	}

	public function setIsRunOnAllClusters( $intIsRunOnAllClusters ) {
		$this->set( 'm_intIsRunOnAllClusters', CStrings::strToIntDef( $intIsRunOnAllClusters, NULL, false ) );
	}

	public function getIsRunOnAllClusters() {
		return $this->m_intIsRunOnAllClusters;
	}

	public function sqlIsRunOnAllClusters() {
		return ( true == isset( $this->m_intIsRunOnAllClusters ) ) ? ( string ) $this->m_intIsRunOnAllClusters : '0';
	}

	public function setIsAutoExecute( $intIsAutoExecute ) {
		$this->set( 'm_intIsAutoExecute', CStrings::strToIntDef( $intIsAutoExecute, NULL, false ) );
	}

	public function getIsAutoExecute() {
		return $this->m_intIsAutoExecute;
	}

	public function sqlIsAutoExecute() {
		return ( true == isset( $this->m_intIsAutoExecute ) ) ? ( string ) $this->m_intIsAutoExecute : '0';
	}

	public function setIsDr( $boolIsDr ) {
		$this->set( 'm_boolIsDr', CStrings::strToBool( $boolIsDr ) );
	}

	public function getIsDr() {
		return $this->m_boolIsDr;
	}

	public function sqlIsDr() {
		return ( true == isset( $this->m_boolIsDr ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDr ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSvnCommittedOn( $strSvnCommittedOn ) {
		$this->set( 'm_strSvnCommittedOn', CStrings::strTrimDef( $strSvnCommittedOn, -1, NULL, true ) );
	}

	public function getSvnCommittedOn() {
		return $this->m_strSvnCommittedOn;
	}

	public function sqlSvnCommittedOn() {
		return ( true == isset( $this->m_strSvnCommittedOn ) ) ? '\'' . $this->m_strSvnCommittedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cluster_id, deployment_type_id, parent_deployment_id, google_calendar_event_id, exec_command, task_ids, database_ids, parameters, description, verified_conflicted_files, svn_file_id, scheduled_datetime, start_datetime, end_datetime, completed_on, failed_on, deployment_datetime, is_critical_push, is_run_on_all_clusters, is_auto_execute, is_dr, svn_committed_on, approved_by, updated_by, updated_on, created_by, created_on, ps_product_id, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlClusterId() . ', ' .
						$this->sqlDeploymentTypeId() . ', ' .
						$this->sqlParentDeploymentId() . ', ' .
						$this->sqlGoogleCalendarEventId() . ', ' .
						$this->sqlExecCommand() . ', ' .
						$this->sqlTaskIds() . ', ' .
						$this->sqlDatabaseIds() . ', ' .
						$this->sqlParameters() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlVerifiedConflictedFiles() . ', ' .
						$this->sqlSvnFileId() . ', ' .
						$this->sqlScheduledDatetime() . ', ' .
						$this->sqlStartDatetime() . ', ' .
						$this->sqlEndDatetime() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						$this->sqlFailedOn() . ', ' .
						$this->sqlDeploymentDatetime() . ', ' .
						$this->sqlIsCriticalPush() . ', ' .
						$this->sqlIsRunOnAllClusters() . ', ' .
						$this->sqlIsAutoExecute() . ', ' .
						$this->sqlIsDr() . ', ' .
						$this->sqlSvnCommittedOn() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId(). ',' ; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deployment_type_id = ' . $this->sqlDeploymentTypeId(). ',' ; } elseif( true == array_key_exists( 'DeploymentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' deployment_type_id = ' . $this->sqlDeploymentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_deployment_id = ' . $this->sqlParentDeploymentId(). ',' ; } elseif( true == array_key_exists( 'ParentDeploymentId', $this->getChangedColumns() ) ) { $strSql .= ' parent_deployment_id = ' . $this->sqlParentDeploymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' google_calendar_event_id = ' . $this->sqlGoogleCalendarEventId(). ',' ; } elseif( true == array_key_exists( 'GoogleCalendarEventId', $this->getChangedColumns() ) ) { $strSql .= ' google_calendar_event_id = ' . $this->sqlGoogleCalendarEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exec_command = ' . $this->sqlExecCommand(). ',' ; } elseif( true == array_key_exists( 'ExecCommand', $this->getChangedColumns() ) ) { $strSql .= ' exec_command = ' . $this->sqlExecCommand() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_ids = ' . $this->sqlTaskIds(). ',' ; } elseif( true == array_key_exists( 'TaskIds', $this->getChangedColumns() ) ) { $strSql .= ' task_ids = ' . $this->sqlTaskIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_ids = ' . $this->sqlDatabaseIds(). ',' ; } elseif( true == array_key_exists( 'DatabaseIds', $this->getChangedColumns() ) ) { $strSql .= ' database_ids = ' . $this->sqlDatabaseIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parameters = ' . $this->sqlParameters(). ',' ; } elseif( true == array_key_exists( 'Parameters', $this->getChangedColumns() ) ) { $strSql .= ' parameters = ' . $this->sqlParameters() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verified_conflicted_files = ' . $this->sqlVerifiedConflictedFiles(). ',' ; } elseif( true == array_key_exists( 'VerifiedConflictedFiles', $this->getChangedColumns() ) ) { $strSql .= ' verified_conflicted_files = ' . $this->sqlVerifiedConflictedFiles() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_file_id = ' . $this->sqlSvnFileId(). ',' ; } elseif( true == array_key_exists( 'SvnFileId', $this->getChangedColumns() ) ) { $strSql .= ' svn_file_id = ' . $this->sqlSvnFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_datetime = ' . $this->sqlScheduledDatetime(). ',' ; } elseif( true == array_key_exists( 'ScheduledDatetime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_datetime = ' . $this->sqlScheduledDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime(). ',' ; } elseif( true == array_key_exists( 'StartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime(). ',' ; } elseif( true == array_key_exists( 'EndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn(). ',' ; } elseif( true == array_key_exists( 'FailedOn', $this->getChangedColumns() ) ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deployment_datetime = ' . $this->sqlDeploymentDatetime(). ',' ; } elseif( true == array_key_exists( 'DeploymentDatetime', $this->getChangedColumns() ) ) { $strSql .= ' deployment_datetime = ' . $this->sqlDeploymentDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_critical_push = ' . $this->sqlIsCriticalPush(). ',' ; } elseif( true == array_key_exists( 'IsCriticalPush', $this->getChangedColumns() ) ) { $strSql .= ' is_critical_push = ' . $this->sqlIsCriticalPush() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_run_on_all_clusters = ' . $this->sqlIsRunOnAllClusters(). ',' ; } elseif( true == array_key_exists( 'IsRunOnAllClusters', $this->getChangedColumns() ) ) { $strSql .= ' is_run_on_all_clusters = ' . $this->sqlIsRunOnAllClusters() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_execute = ' . $this->sqlIsAutoExecute(). ',' ; } elseif( true == array_key_exists( 'IsAutoExecute', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_execute = ' . $this->sqlIsAutoExecute() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_dr = ' . $this->sqlIsDr(). ',' ; } elseif( true == array_key_exists( 'IsDr', $this->getChangedColumns() ) ) { $strSql .= ' is_dr = ' . $this->sqlIsDr() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_committed_on = ' . $this->sqlSvnCommittedOn(). ',' ; } elseif( true == array_key_exists( 'SvnCommittedOn', $this->getChangedColumns() ) ) { $strSql .= ' svn_committed_on = ' . $this->sqlSvnCommittedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cluster_id' => $this->getClusterId(),
			'deployment_type_id' => $this->getDeploymentTypeId(),
			'parent_deployment_id' => $this->getParentDeploymentId(),
			'google_calendar_event_id' => $this->getGoogleCalendarEventId(),
			'exec_command' => $this->getExecCommand(),
			'task_ids' => $this->getTaskIds(),
			'database_ids' => $this->getDatabaseIds(),
			'parameters' => $this->getParameters(),
			'description' => $this->getDescription(),
			'verified_conflicted_files' => $this->getVerifiedConflictedFiles(),
			'svn_file_id' => $this->getSvnFileId(),
			'scheduled_datetime' => $this->getScheduledDatetime(),
			'start_datetime' => $this->getStartDatetime(),
			'end_datetime' => $this->getEndDatetime(),
			'completed_on' => $this->getCompletedOn(),
			'failed_on' => $this->getFailedOn(),
			'deployment_datetime' => $this->getDeploymentDatetime(),
			'is_critical_push' => $this->getIsCriticalPush(),
			'is_run_on_all_clusters' => $this->getIsRunOnAllClusters(),
			'is_auto_execute' => $this->getIsAutoExecute(),
			'is_dr' => $this->getIsDr(),
			'svn_committed_on' => $this->getSvnCommittedOn(),
			'approved_by' => $this->getApprovedBy(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'ps_product_id' => $this->getPsProductId(),
			'details' => $this->getDetails()
		);
	}

}
?>