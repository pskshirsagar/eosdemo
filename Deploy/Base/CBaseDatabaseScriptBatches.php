<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CDatabaseScriptBatches
 * Do not add any new functions to this class.
 */

class CBaseDatabaseScriptBatches extends CEosPluralBase {

	/**
	 * @return CDatabaseScriptBatch[]
	 */
	public static function fetchDatabaseScriptBatches( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDatabaseScriptBatch', $objDatabase );
	}

	/**
	 * @return CDatabaseScriptBatch
	 */
	public static function fetchDatabaseScriptBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDatabaseScriptBatch', $objDatabase );
	}

	public static function fetchDatabaseScriptBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'database_script_batches', $objDatabase );
	}

	public static function fetchDatabaseScriptBatchById( $intId, $objDatabase ) {
		return self::fetchDatabaseScriptBatch( sprintf( 'SELECT * FROM database_script_batches WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDatabaseScriptBatchesByDatabaseTypeId( $intDatabaseTypeId, $objDatabase ) {
		return self::fetchDatabaseScriptBatches( sprintf( 'SELECT * FROM database_script_batches WHERE database_type_id = %d', ( int ) $intDatabaseTypeId ), $objDatabase );
	}

}
?>