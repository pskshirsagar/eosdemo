<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CSvnRepositories
 * Do not add any new functions to this class.
 */

class CBaseSvnRepositories extends CEosPluralBase {

	/**
	 * @return CSvnRepository[]
	 */
	public static function fetchSvnRepositories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSvnRepository::class, $objDatabase );
	}

	/**
	 * @return CSvnRepository
	 */
	public static function fetchSvnRepository( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSvnRepository::class, $objDatabase );
	}

	public static function fetchSvnRepositoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'svn_repositories', $objDatabase );
	}

	public static function fetchSvnRepositoryById( $intId, $objDatabase ) {
		return self::fetchSvnRepository( sprintf( 'SELECT * FROM svn_repositories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSvnRepositoriesBySvnRepositoryTypeId( $intSvnRepositoryTypeId, $objDatabase ) {
		return self::fetchSvnRepositories( sprintf( 'SELECT * FROM svn_repositories WHERE svn_repository_type_id = %d', ( int ) $intSvnRepositoryTypeId ), $objDatabase );
	}

	public static function fetchSvnRepositoriesByDeploymentId( $intDeploymentId, $objDatabase ) {
		return self::fetchSvnRepositories( sprintf( 'SELECT * FROM svn_repositories WHERE deployment_id = %d', ( int ) $intDeploymentId ), $objDatabase );
	}

	public static function fetchSvnRepositoriesBySystemUserId( $intSystemUserId, $objDatabase ) {
		return self::fetchSvnRepositories( sprintf( 'SELECT * FROM svn_repositories WHERE system_user_id = %d', ( int ) $intSystemUserId ), $objDatabase );
	}

	public static function fetchSvnRepositoriesByTaskId( $strTaskId, $objDatabase ) {
		return self::fetchSvnRepositories( sprintf( 'SELECT * FROM svn_repositories WHERE task_id = \'%s\'', $strTaskId ), $objDatabase );
	}

	public static function fetchSvnRepositoriesByRepositoryId( $intRepositoryId, $objDatabase ) {
		return self::fetchSvnRepositories( sprintf( 'SELECT * FROM svn_repositories WHERE repository_id = %d', ( int ) $intRepositoryId ), $objDatabase );
	}

}
?>