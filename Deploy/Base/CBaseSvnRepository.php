<?php

class CBaseSvnRepository extends CEosSingularBase {

	const TABLE_NAME = 'public.svn_repositories';

	protected $m_intId;
	protected $m_intSvnRepositoryTypeId;
	protected $m_intDeploymentId;
	protected $m_intSystemUserId;
	protected $m_strTaskId;
	protected $m_intRepositoryId;
	protected $m_strName;
	protected $m_strPath;
	protected $m_strEmailNotifications;
	protected $m_intIsSystem;
	protected $m_intIsAutomerge;
	protected $m_intIsParsed;
	protected $m_intIsDeleted;
	protected $m_boolIsJenkinsChecksEnable;
	protected $m_boolIsUrlRequired;
	protected $m_boolSyncToSlave;
	protected $m_strExpectedEndDate;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intSvnRepositoryTypeId = '1';
		$this->m_intIsSystem = '0';
		$this->m_intIsParsed = '0';
		$this->m_intIsDeleted = '0';
		$this->m_boolIsJenkinsChecksEnable = false;
		$this->m_boolIsUrlRequired = false;
		$this->m_boolSyncToSlave = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['svn_repository_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSvnRepositoryTypeId', trim( $arrValues['svn_repository_type_id'] ) ); elseif( isset( $arrValues['svn_repository_type_id'] ) ) $this->setSvnRepositoryTypeId( $arrValues['svn_repository_type_id'] );
		if( isset( $arrValues['deployment_id'] ) && $boolDirectSet ) $this->set( 'm_intDeploymentId', trim( $arrValues['deployment_id'] ) ); elseif( isset( $arrValues['deployment_id'] ) ) $this->setDeploymentId( $arrValues['deployment_id'] );
		if( isset( $arrValues['system_user_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemUserId', trim( $arrValues['system_user_id'] ) ); elseif( isset( $arrValues['system_user_id'] ) ) $this->setSystemUserId( $arrValues['system_user_id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_strTaskId', trim( stripcslashes( $arrValues['task_id'] ) ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['task_id'] ) : $arrValues['task_id'] );
		if( isset( $arrValues['repository_id'] ) && $boolDirectSet ) $this->set( 'm_intRepositoryId', trim( $arrValues['repository_id'] ) ); elseif( isset( $arrValues['repository_id'] ) ) $this->setRepositoryId( $arrValues['repository_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['path'] ) && $boolDirectSet ) $this->set( 'm_strPath', trim( stripcslashes( $arrValues['path'] ) ) ); elseif( isset( $arrValues['path'] ) ) $this->setPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['path'] ) : $arrValues['path'] );
		if( isset( $arrValues['email_notifications'] ) && $boolDirectSet ) $this->set( 'm_strEmailNotifications', trim( stripcslashes( $arrValues['email_notifications'] ) ) ); elseif( isset( $arrValues['email_notifications'] ) ) $this->setEmailNotifications( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_notifications'] ) : $arrValues['email_notifications'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['is_automerge'] ) && $boolDirectSet ) $this->set( 'm_intIsAutomerge', trim( $arrValues['is_automerge'] ) ); elseif( isset( $arrValues['is_automerge'] ) ) $this->setIsAutomerge( $arrValues['is_automerge'] );
		if( isset( $arrValues['is_parsed'] ) && $boolDirectSet ) $this->set( 'm_intIsParsed', trim( $arrValues['is_parsed'] ) ); elseif( isset( $arrValues['is_parsed'] ) ) $this->setIsParsed( $arrValues['is_parsed'] );
		if( isset( $arrValues['is_deleted'] ) && $boolDirectSet ) $this->set( 'm_intIsDeleted', trim( $arrValues['is_deleted'] ) ); elseif( isset( $arrValues['is_deleted'] ) ) $this->setIsDeleted( $arrValues['is_deleted'] );
		if( isset( $arrValues['is_jenkins_checks_enable'] ) && $boolDirectSet ) $this->set( 'm_boolIsJenkinsChecksEnable', trim( stripcslashes( $arrValues['is_jenkins_checks_enable'] ) ) ); elseif( isset( $arrValues['is_jenkins_checks_enable'] ) ) $this->setIsJenkinsChecksEnable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_jenkins_checks_enable'] ) : $arrValues['is_jenkins_checks_enable'] );
		if( isset( $arrValues['is_url_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsUrlRequired', trim( stripcslashes( $arrValues['is_url_required'] ) ) ); elseif( isset( $arrValues['is_url_required'] ) ) $this->setIsUrlRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_url_required'] ) : $arrValues['is_url_required'] );
		if( isset( $arrValues['sync_to_slave'] ) && $boolDirectSet ) $this->set( 'm_boolSyncToSlave', trim( stripcslashes( $arrValues['sync_to_slave'] ) ) ); elseif( isset( $arrValues['sync_to_slave'] ) ) $this->setSyncToSlave( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sync_to_slave'] ) : $arrValues['sync_to_slave'] );
		if( isset( $arrValues['expected_end_date'] ) && $boolDirectSet ) $this->set( 'm_strExpectedEndDate', trim( $arrValues['expected_end_date'] ) ); elseif( isset( $arrValues['expected_end_date'] ) ) $this->setExpectedEndDate( $arrValues['expected_end_date'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSvnRepositoryTypeId( $intSvnRepositoryTypeId ) {
		$this->set( 'm_intSvnRepositoryTypeId', CStrings::strToIntDef( $intSvnRepositoryTypeId, NULL, false ) );
	}

	public function getSvnRepositoryTypeId() {
		return $this->m_intSvnRepositoryTypeId;
	}

	public function sqlSvnRepositoryTypeId() {
		return ( true == isset( $this->m_intSvnRepositoryTypeId ) ) ? ( string ) $this->m_intSvnRepositoryTypeId : '1';
	}

	public function setDeploymentId( $intDeploymentId ) {
		$this->set( 'm_intDeploymentId', CStrings::strToIntDef( $intDeploymentId, NULL, false ) );
	}

	public function getDeploymentId() {
		return $this->m_intDeploymentId;
	}

	public function sqlDeploymentId() {
		return ( true == isset( $this->m_intDeploymentId ) ) ? ( string ) $this->m_intDeploymentId : 'NULL';
	}

	public function setSystemUserId( $intSystemUserId ) {
		$this->set( 'm_intSystemUserId', CStrings::strToIntDef( $intSystemUserId, NULL, false ) );
	}

	public function getSystemUserId() {
		return $this->m_intSystemUserId;
	}

	public function sqlSystemUserId() {
		return ( true == isset( $this->m_intSystemUserId ) ) ? ( string ) $this->m_intSystemUserId : 'NULL';
	}

	public function setTaskId( $strTaskId ) {
		$this->set( 'm_strTaskId', CStrings::strTrimDef( $strTaskId, -1, NULL, true ) );
	}

	public function getTaskId() {
		return $this->m_strTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_strTaskId ) ) ? '\'' . addslashes( $this->m_strTaskId ) . '\'' : 'NULL';
	}

	public function setRepositoryId( $intRepositoryId ) {
		$this->set( 'm_intRepositoryId', CStrings::strToIntDef( $intRepositoryId, NULL, false ) );
	}

	public function getRepositoryId() {
		return $this->m_intRepositoryId;
	}

	public function sqlRepositoryId() {
		return ( true == isset( $this->m_intRepositoryId ) ) ? ( string ) $this->m_intRepositoryId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setPath( $strPath ) {
		$this->set( 'm_strPath', CStrings::strTrimDef( $strPath, -1, NULL, true ) );
	}

	public function getPath() {
		return $this->m_strPath;
	}

	public function sqlPath() {
		return ( true == isset( $this->m_strPath ) ) ? '\'' . addslashes( $this->m_strPath ) . '\'' : 'NULL';
	}

	public function setEmailNotifications( $strEmailNotifications ) {
		$this->set( 'm_strEmailNotifications', CStrings::strTrimDef( $strEmailNotifications, 1000, NULL, true ) );
	}

	public function getEmailNotifications() {
		return $this->m_strEmailNotifications;
	}

	public function sqlEmailNotifications() {
		return ( true == isset( $this->m_strEmailNotifications ) ) ? '\'' . addslashes( $this->m_strEmailNotifications ) . '\'' : 'NULL';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setIsAutomerge( $intIsAutomerge ) {
		$this->set( 'm_intIsAutomerge', CStrings::strToIntDef( $intIsAutomerge, NULL, false ) );
	}

	public function getIsAutomerge() {
		return $this->m_intIsAutomerge;
	}

	public function sqlIsAutomerge() {
		return ( true == isset( $this->m_intIsAutomerge ) ) ? ( string ) $this->m_intIsAutomerge : 'NULL';
	}

	public function setIsParsed( $intIsParsed ) {
		$this->set( 'm_intIsParsed', CStrings::strToIntDef( $intIsParsed, NULL, false ) );
	}

	public function getIsParsed() {
		return $this->m_intIsParsed;
	}

	public function sqlIsParsed() {
		return ( true == isset( $this->m_intIsParsed ) ) ? ( string ) $this->m_intIsParsed : '0';
	}

	public function setIsDeleted( $intIsDeleted ) {
		$this->set( 'm_intIsDeleted', CStrings::strToIntDef( $intIsDeleted, NULL, false ) );
	}

	public function getIsDeleted() {
		return $this->m_intIsDeleted;
	}

	public function sqlIsDeleted() {
		return ( true == isset( $this->m_intIsDeleted ) ) ? ( string ) $this->m_intIsDeleted : '0';
	}

	public function setIsJenkinsChecksEnable( $boolIsJenkinsChecksEnable ) {
		$this->set( 'm_boolIsJenkinsChecksEnable', CStrings::strToBool( $boolIsJenkinsChecksEnable ) );
	}

	public function getIsJenkinsChecksEnable() {
		return $this->m_boolIsJenkinsChecksEnable;
	}

	public function sqlIsJenkinsChecksEnable() {
		return ( true == isset( $this->m_boolIsJenkinsChecksEnable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsJenkinsChecksEnable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsUrlRequired( $boolIsUrlRequired ) {
		$this->set( 'm_boolIsUrlRequired', CStrings::strToBool( $boolIsUrlRequired ) );
	}

	public function getIsUrlRequired() {
		return $this->m_boolIsUrlRequired;
	}

	public function sqlIsUrlRequired() {
		return ( true == isset( $this->m_boolIsUrlRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsUrlRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSyncToSlave( $boolSyncToSlave ) {
		$this->set( 'm_boolSyncToSlave', CStrings::strToBool( $boolSyncToSlave ) );
	}

	public function getSyncToSlave() {
		return $this->m_boolSyncToSlave;
	}

	public function sqlSyncToSlave() {
		return ( true == isset( $this->m_boolSyncToSlave ) ) ? '\'' . ( true == ( bool ) $this->m_boolSyncToSlave ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setExpectedEndDate( $strExpectedEndDate ) {
		$this->set( 'm_strExpectedEndDate', CStrings::strTrimDef( $strExpectedEndDate, -1, NULL, true ) );
	}

	public function getExpectedEndDate() {
		return $this->m_strExpectedEndDate;
	}

	public function sqlExpectedEndDate() {
		return ( true == isset( $this->m_strExpectedEndDate ) ) ? '\'' . $this->m_strExpectedEndDate . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, svn_repository_type_id, deployment_id, system_user_id, task_id, repository_id, name, path, email_notifications, is_system, is_automerge, is_parsed, is_deleted, is_jenkins_checks_enable, is_url_required, sync_to_slave, expected_end_date, created_by, created_on, approved_by, approved_on, deleted_by, deleted_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSvnRepositoryTypeId() . ', ' .
 						$this->sqlDeploymentId() . ', ' .
 						$this->sqlSystemUserId() . ', ' .
 						$this->sqlTaskId() . ', ' .
 						$this->sqlRepositoryId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlPath() . ', ' .
 						$this->sqlEmailNotifications() . ', ' .
 						$this->sqlIsSystem() . ', ' .
 						$this->sqlIsAutomerge() . ', ' .
 						$this->sqlIsParsed() . ', ' .
 						$this->sqlIsDeleted() . ', ' .
 						$this->sqlIsJenkinsChecksEnable() . ', ' .
 						$this->sqlIsUrlRequired() . ', ' .
 						$this->sqlSyncToSlave() . ', ' .
 						$this->sqlExpectedEndDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_repository_type_id = ' . $this->sqlSvnRepositoryTypeId() . ','; } elseif( true == array_key_exists( 'SvnRepositoryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' svn_repository_type_id = ' . $this->sqlSvnRepositoryTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deployment_id = ' . $this->sqlDeploymentId() . ','; } elseif( true == array_key_exists( 'DeploymentId', $this->getChangedColumns() ) ) { $strSql .= ' deployment_id = ' . $this->sqlDeploymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_user_id = ' . $this->sqlSystemUserId() . ','; } elseif( true == array_key_exists( 'SystemUserId', $this->getChangedColumns() ) ) { $strSql .= ' system_user_id = ' . $this->sqlSystemUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repository_id = ' . $this->sqlRepositoryId() . ','; } elseif( true == array_key_exists( 'RepositoryId', $this->getChangedColumns() ) ) { $strSql .= ' repository_id = ' . $this->sqlRepositoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' path = ' . $this->sqlPath() . ','; } elseif( true == array_key_exists( 'Path', $this->getChangedColumns() ) ) { $strSql .= ' path = ' . $this->sqlPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_notifications = ' . $this->sqlEmailNotifications() . ','; } elseif( true == array_key_exists( 'EmailNotifications', $this->getChangedColumns() ) ) { $strSql .= ' email_notifications = ' . $this->sqlEmailNotifications() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_automerge = ' . $this->sqlIsAutomerge() . ','; } elseif( true == array_key_exists( 'IsAutomerge', $this->getChangedColumns() ) ) { $strSql .= ' is_automerge = ' . $this->sqlIsAutomerge() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_parsed = ' . $this->sqlIsParsed() . ','; } elseif( true == array_key_exists( 'IsParsed', $this->getChangedColumns() ) ) { $strSql .= ' is_parsed = ' . $this->sqlIsParsed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; } elseif( true == array_key_exists( 'IsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_jenkins_checks_enable = ' . $this->sqlIsJenkinsChecksEnable() . ','; } elseif( true == array_key_exists( 'IsJenkinsChecksEnable', $this->getChangedColumns() ) ) { $strSql .= ' is_jenkins_checks_enable = ' . $this->sqlIsJenkinsChecksEnable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_url_required = ' . $this->sqlIsUrlRequired() . ','; } elseif( true == array_key_exists( 'IsUrlRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_url_required = ' . $this->sqlIsUrlRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sync_to_slave = ' . $this->sqlSyncToSlave() . ','; } elseif( true == array_key_exists( 'SyncToSlave', $this->getChangedColumns() ) ) { $strSql .= ' sync_to_slave = ' . $this->sqlSyncToSlave() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_end_date = ' . $this->sqlExpectedEndDate() . ','; } elseif( true == array_key_exists( 'ExpectedEndDate', $this->getChangedColumns() ) ) { $strSql .= ' expected_end_date = ' . $this->sqlExpectedEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'svn_repository_type_id' => $this->getSvnRepositoryTypeId(),
			'deployment_id' => $this->getDeploymentId(),
			'system_user_id' => $this->getSystemUserId(),
			'task_id' => $this->getTaskId(),
			'repository_id' => $this->getRepositoryId(),
			'name' => $this->getName(),
			'path' => $this->getPath(),
			'email_notifications' => $this->getEmailNotifications(),
			'is_system' => $this->getIsSystem(),
			'is_automerge' => $this->getIsAutomerge(),
			'is_parsed' => $this->getIsParsed(),
			'is_deleted' => $this->getIsDeleted(),
			'is_jenkins_checks_enable' => $this->getIsJenkinsChecksEnable(),
			'is_url_required' => $this->getIsUrlRequired(),
			'sync_to_slave' => $this->getSyncToSlave(),
			'expected_end_date' => $this->getExpectedEndDate(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn()
		);
	}

}
?>