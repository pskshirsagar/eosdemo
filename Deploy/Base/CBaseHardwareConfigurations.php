<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CHardwareConfigurations
 * Do not add any new functions to this class.
 */

class CBaseHardwareConfigurations extends CEosPluralBase {

	/**
	 * @return CHardwareConfiguration[]
	 */
	public static function fetchHardwareConfigurations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CHardwareConfiguration', $objDatabase );
	}

	/**
	 * @return CHardwareConfiguration
	 */
	public static function fetchHardwareConfiguration( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CHardwareConfiguration', $objDatabase );
	}

	public static function fetchHardwareConfigurationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'hardware_configurations', $objDatabase );
	}

	public static function fetchHardwareConfigurationById( $intId, $objDatabase ) {
		return self::fetchHardwareConfiguration( sprintf( 'SELECT * FROM hardware_configurations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchHardwareConfigurationsByHardwareId( $intHardwareId, $objDatabase ) {
		return self::fetchHardwareConfigurations( sprintf( 'SELECT * FROM hardware_configurations WHERE hardware_id = %d', ( int ) $intHardwareId ), $objDatabase );
	}

}
?>