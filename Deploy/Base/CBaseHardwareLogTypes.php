<?php

class CBaseHardwareLogTypes extends CEosPluralBase {

    const TABLE_HARDWARE_LOG_TYPES = 'public.hardware_log_types';

    public static function fetchHardwareLogTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CHardwareLogType', $objDatabase );
    }

    public static function fetchHardwareLogType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CHardwareLogType', $objDatabase );
    }

    public static function fetchHardwareLogTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'hardware_log_types', $objDatabase );
    }

    public static function fetchHardwareLogTypeById( $intId, $objDatabase ) {
        return self::fetchHardwareLogType( sprintf( 'SELECT * FROM hardware_log_types WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>