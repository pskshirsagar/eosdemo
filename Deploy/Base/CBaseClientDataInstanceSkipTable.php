<?php

class CBaseClientDataInstanceSkipTable extends CEosSingularBase {

	const TABLE_NAME = 'public.client_data_instance_skip_tables';

	protected $m_intId;
	protected $m_strTableName;
	protected $m_intDatabaseTypeId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['table_name'] ) && $boolDirectSet ) $this->set( 'm_strTableName', trim( stripcslashes( $arrValues['table_name'] ) ) ); elseif( isset( $arrValues['table_name'] ) ) $this->setTableName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['table_name'] ) : $arrValues['table_name'] );
		if( isset( $arrValues['database_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseTypeId', trim( $arrValues['database_type_id'] ) ); elseif( isset( $arrValues['database_type_id'] ) ) $this->setDatabaseTypeId( $arrValues['database_type_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTableName( $strTableName ) {
		$this->set( 'm_strTableName', CStrings::strTrimDef( $strTableName, 60, NULL, true ) );
	}

	public function getTableName() {
		return $this->m_strTableName;
	}

	public function sqlTableName() {
		return ( true == isset( $this->m_strTableName ) ) ? '\'' . addslashes( $this->m_strTableName ) . '\'' : 'NULL';
	}

	public function setDatabaseTypeId( $intDatabaseTypeId ) {
		$this->set( 'm_intDatabaseTypeId', CStrings::strToIntDef( $intDatabaseTypeId, NULL, false ) );
	}

	public function getDatabaseTypeId() {
		return $this->m_intDatabaseTypeId;
	}

	public function sqlDatabaseTypeId() {
		return ( true == isset( $this->m_intDatabaseTypeId ) ) ? ( string ) $this->m_intDatabaseTypeId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, table_name, database_type_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlTableName() . ', ' .
						$this->sqlDatabaseTypeId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' table_name = ' . $this->sqlTableName(). ',' ; } elseif( true == array_key_exists( 'TableName', $this->getChangedColumns() ) ) { $strSql .= ' table_name = ' . $this->sqlTableName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_type_id = ' . $this->sqlDatabaseTypeId() ; } elseif( true == array_key_exists( 'DatabaseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' database_type_id = ' . $this->sqlDatabaseTypeId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'table_name' => $this->getTableName(),
			'database_type_id' => $this->getDatabaseTypeId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>