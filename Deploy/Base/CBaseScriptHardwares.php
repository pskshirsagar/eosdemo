<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CScriptHardwares
 * Do not add any new functions to this class.
 */

class CBaseScriptHardwares extends CEosPluralBase {

	/**
	 * @return CScriptHardware[]
	 */
	public static function fetchScriptHardwares( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScriptHardware', $objDatabase );
	}

	/**
	 * @return CScriptHardware
	 */
	public static function fetchScriptHardware( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScriptHardware', $objDatabase );
	}

	public static function fetchScriptHardwareCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'script_hardwares', $objDatabase );
	}

	public static function fetchScriptHardwareById( $intId, $objDatabase ) {
		return self::fetchScriptHardware( sprintf( 'SELECT * FROM script_hardwares WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScriptHardwaresByScriptId( $intScriptId, $objDatabase ) {
		return self::fetchScriptHardwares( sprintf( 'SELECT * FROM script_hardwares WHERE script_id = %d', ( int ) $intScriptId ), $objDatabase );
	}

	public static function fetchScriptHardwaresByHardwareId( $intHardwareId, $objDatabase ) {
		return self::fetchScriptHardwares( sprintf( 'SELECT * FROM script_hardwares WHERE hardware_id = %d', ( int ) $intHardwareId ), $objDatabase );
	}

}
?>