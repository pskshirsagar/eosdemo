<?php

class CBasePsProductDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_product_details';

	protected $m_intId;
	protected $m_intPsProductId;
	protected $m_strPsiPublicIp;
	protected $m_strTitle;
	protected $m_intClusterId;
	protected $m_strLoadBalancerNodeIp;
	protected $m_strNagiosGraphUrl;
	protected $m_strProductUrl;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intClusterId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['psi_public_ip'] ) && $boolDirectSet ) $this->set( 'm_strPsiPublicIp', trim( stripcslashes( $arrValues['psi_public_ip'] ) ) ); elseif( isset( $arrValues['psi_public_ip'] ) ) $this->setPsiPublicIp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['psi_public_ip'] ) : $arrValues['psi_public_ip'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['load_balancer_node_ip'] ) && $boolDirectSet ) $this->set( 'm_strLoadBalancerNodeIp', trim( stripcslashes( $arrValues['load_balancer_node_ip'] ) ) ); elseif( isset( $arrValues['load_balancer_node_ip'] ) ) $this->setLoadBalancerNodeIp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['load_balancer_node_ip'] ) : $arrValues['load_balancer_node_ip'] );
		if( isset( $arrValues['nagios_graph_url'] ) && $boolDirectSet ) $this->set( 'm_strNagiosGraphUrl', trim( stripcslashes( $arrValues['nagios_graph_url'] ) ) ); elseif( isset( $arrValues['nagios_graph_url'] ) ) $this->setNagiosGraphUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['nagios_graph_url'] ) : $arrValues['nagios_graph_url'] );
		if( isset( $arrValues['product_url'] ) && $boolDirectSet ) $this->set( 'm_strProductUrl', trim( stripcslashes( $arrValues['product_url'] ) ) ); elseif( isset( $arrValues['product_url'] ) ) $this->setProductUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['product_url'] ) : $arrValues['product_url'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsiPublicIp( $strPsiPublicIp ) {
		$this->set( 'm_strPsiPublicIp', CStrings::strTrimDef( $strPsiPublicIp, 20, NULL, true ) );
	}

	public function getPsiPublicIp() {
		return $this->m_strPsiPublicIp;
	}

	public function sqlPsiPublicIp() {
		return ( true == isset( $this->m_strPsiPublicIp ) ) ? '\'' . addslashes( $this->m_strPsiPublicIp ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : '1';
	}

	public function setLoadBalancerNodeIp( $strLoadBalancerNodeIp ) {
		$this->set( 'm_strLoadBalancerNodeIp', CStrings::strTrimDef( $strLoadBalancerNodeIp, 20, NULL, true ) );
	}

	public function getLoadBalancerNodeIp() {
		return $this->m_strLoadBalancerNodeIp;
	}

	public function sqlLoadBalancerNodeIp() {
		return ( true == isset( $this->m_strLoadBalancerNodeIp ) ) ? '\'' . addslashes( $this->m_strLoadBalancerNodeIp ) . '\'' : 'NULL';
	}

	public function setNagiosGraphUrl( $strNagiosGraphUrl ) {
		$this->set( 'm_strNagiosGraphUrl', CStrings::strTrimDef( $strNagiosGraphUrl, 240, NULL, true ) );
	}

	public function getNagiosGraphUrl() {
		return $this->m_strNagiosGraphUrl;
	}

	public function sqlNagiosGraphUrl() {
		return ( true == isset( $this->m_strNagiosGraphUrl ) ) ? '\'' . addslashes( $this->m_strNagiosGraphUrl ) . '\'' : 'NULL';
	}

	public function setProductUrl( $strProductUrl ) {
		$this->set( 'm_strProductUrl', CStrings::strTrimDef( $strProductUrl, 240, NULL, true ) );
	}

	public function getProductUrl() {
		return $this->m_strProductUrl;
	}

	public function sqlProductUrl() {
		return ( true == isset( $this->m_strProductUrl ) ) ? '\'' . addslashes( $this->m_strProductUrl ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_product_id, psi_public_ip, title, cluster_id, load_balancer_node_ip, nagios_graph_url, product_url, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlPsiPublicIp() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlClusterId() . ', ' .
 						$this->sqlLoadBalancerNodeIp() . ', ' .
 						$this->sqlNagiosGraphUrl() . ', ' .
 						$this->sqlProductUrl() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' psi_public_ip = ' . $this->sqlPsiPublicIp() . ','; } elseif( true == array_key_exists( 'PsiPublicIp', $this->getChangedColumns() ) ) { $strSql .= ' psi_public_ip = ' . $this->sqlPsiPublicIp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' load_balancer_node_ip = ' . $this->sqlLoadBalancerNodeIp() . ','; } elseif( true == array_key_exists( 'LoadBalancerNodeIp', $this->getChangedColumns() ) ) { $strSql .= ' load_balancer_node_ip = ' . $this->sqlLoadBalancerNodeIp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nagios_graph_url = ' . $this->sqlNagiosGraphUrl() . ','; } elseif( true == array_key_exists( 'NagiosGraphUrl', $this->getChangedColumns() ) ) { $strSql .= ' nagios_graph_url = ' . $this->sqlNagiosGraphUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' product_url = ' . $this->sqlProductUrl() . ','; } elseif( true == array_key_exists( 'ProductUrl', $this->getChangedColumns() ) ) { $strSql .= ' product_url = ' . $this->sqlProductUrl() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_product_id' => $this->getPsProductId(),
			'psi_public_ip' => $this->getPsiPublicIp(),
			'title' => $this->getTitle(),
			'cluster_id' => $this->getClusterId(),
			'load_balancer_node_ip' => $this->getLoadBalancerNodeIp(),
			'nagios_graph_url' => $this->getNagiosGraphUrl(),
			'product_url' => $this->getProductUrl(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>