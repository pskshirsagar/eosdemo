<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CSystemUsers
 * Do not add any new functions to this class.
 */

class CBaseSystemUsers extends CEosPluralBase {

	/**
	 * @return CSystemUser[]
	 */
	public static function fetchSystemUsers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSystemUser', $objDatabase );
	}

	/**
	 * @return CSystemUser
	 */
	public static function fetchSystemUser( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSystemUser', $objDatabase );
	}

	public static function fetchSystemUserCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_users', $objDatabase );
	}

	public static function fetchSystemUserById( $intId, $objDatabase ) {
		return self::fetchSystemUser( sprintf( 'SELECT * FROM system_users WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>