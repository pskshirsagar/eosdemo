<?php

class CBaseGitClusterRepository extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.git_cluster_repositories';

	protected $m_intId;
	protected $m_intClusterId;
	protected $m_strName;
	protected $m_intPsProductId;
	protected $m_intBaseRepositoryId;
	protected $m_intStageRepositoryId;
	protected $m_intProductionRepositoryId;
	protected $m_strTagPath;
	protected $m_strExportPath;
	protected $m_boolIsCodeBase;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intClusterId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['base_repository_id'] ) && $boolDirectSet ) $this->set( 'm_intBaseRepositoryId', trim( $arrValues['base_repository_id'] ) ); elseif( isset( $arrValues['base_repository_id'] ) ) $this->setBaseRepositoryId( $arrValues['base_repository_id'] );
		if( isset( $arrValues['stage_repository_id'] ) && $boolDirectSet ) $this->set( 'm_intStageRepositoryId', trim( $arrValues['stage_repository_id'] ) ); elseif( isset( $arrValues['stage_repository_id'] ) ) $this->setStageRepositoryId( $arrValues['stage_repository_id'] );
		if( isset( $arrValues['production_repository_id'] ) && $boolDirectSet ) $this->set( 'm_intProductionRepositoryId', trim( $arrValues['production_repository_id'] ) ); elseif( isset( $arrValues['production_repository_id'] ) ) $this->setProductionRepositoryId( $arrValues['production_repository_id'] );
		if( isset( $arrValues['tag_path'] ) && $boolDirectSet ) $this->set( 'm_strTagPath', trim( $arrValues['tag_path'] ) ); elseif( isset( $arrValues['tag_path'] ) ) $this->setTagPath( $arrValues['tag_path'] );
		if( isset( $arrValues['export_path'] ) && $boolDirectSet ) $this->set( 'm_strExportPath', trim( $arrValues['export_path'] ) ); elseif( isset( $arrValues['export_path'] ) ) $this->setExportPath( $arrValues['export_path'] );
		if( isset( $arrValues['is_code_base'] ) && $boolDirectSet ) $this->set( 'm_boolIsCodeBase', trim( stripcslashes( $arrValues['is_code_base'] ) ) ); elseif( isset( $arrValues['is_code_base'] ) ) $this->setIsCodeBase( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_code_base'] ) : $arrValues['is_code_base'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : '1';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 200, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setBaseRepositoryId( $intBaseRepositoryId ) {
		$this->set( 'm_intBaseRepositoryId', CStrings::strToIntDef( $intBaseRepositoryId, NULL, false ) );
	}

	public function getBaseRepositoryId() {
		return $this->m_intBaseRepositoryId;
	}

	public function sqlBaseRepositoryId() {
		return ( true == isset( $this->m_intBaseRepositoryId ) ) ? ( string ) $this->m_intBaseRepositoryId : 'NULL';
	}

	public function setStageRepositoryId( $intStageRepositoryId ) {
		$this->set( 'm_intStageRepositoryId', CStrings::strToIntDef( $intStageRepositoryId, NULL, false ) );
	}

	public function getStageRepositoryId() {
		return $this->m_intStageRepositoryId;
	}

	public function sqlStageRepositoryId() {
		return ( true == isset( $this->m_intStageRepositoryId ) ) ? ( string ) $this->m_intStageRepositoryId : 'NULL';
	}

	public function setProductionRepositoryId( $intProductionRepositoryId ) {
		$this->set( 'm_intProductionRepositoryId', CStrings::strToIntDef( $intProductionRepositoryId, NULL, false ) );
	}

	public function getProductionRepositoryId() {
		return $this->m_intProductionRepositoryId;
	}

	public function sqlProductionRepositoryId() {
		return ( true == isset( $this->m_intProductionRepositoryId ) ) ? ( string ) $this->m_intProductionRepositoryId : 'NULL';
	}

	public function setTagPath( $strTagPath ) {
		$this->set( 'm_strTagPath', CStrings::strTrimDef( $strTagPath, 500, NULL, true ) );
	}

	public function getTagPath() {
		return $this->m_strTagPath;
	}

	public function sqlTagPath() {
		return ( true == isset( $this->m_strTagPath ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTagPath ) : '\'' . addslashes( $this->m_strTagPath ) . '\'' ) : 'NULL';
	}

	public function setExportPath( $strExportPath ) {
		$this->set( 'm_strExportPath', CStrings::strTrimDef( $strExportPath, 500, NULL, true ) );
	}

	public function getExportPath() {
		return $this->m_strExportPath;
	}

	public function sqlExportPath() {
		return ( true == isset( $this->m_strExportPath ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExportPath ) : '\'' . addslashes( $this->m_strExportPath ) . '\'' ) : 'NULL';
	}

	public function setIsCodeBase( $boolIsCodeBase ) {
		$this->set( 'm_boolIsCodeBase', CStrings::strToBool( $boolIsCodeBase ) );
	}

	public function getIsCodeBase() {
		return $this->m_boolIsCodeBase;
	}

	public function sqlIsCodeBase() {
		return ( true == isset( $this->m_boolIsCodeBase ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCodeBase ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cluster_id, name, ps_product_id, base_repository_id, stage_repository_id, production_repository_id, tag_path, export_path, is_code_base, order_num, updated_by, updated_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlClusterId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlBaseRepositoryId() . ', ' .
						$this->sqlStageRepositoryId() . ', ' .
						$this->sqlProductionRepositoryId() . ', ' .
						$this->sqlTagPath() . ', ' .
						$this->sqlExportPath() . ', ' .
						$this->sqlIsCodeBase() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId(). ',' ; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_repository_id = ' . $this->sqlBaseRepositoryId(). ',' ; } elseif( true == array_key_exists( 'BaseRepositoryId', $this->getChangedColumns() ) ) { $strSql .= ' base_repository_id = ' . $this->sqlBaseRepositoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' stage_repository_id = ' . $this->sqlStageRepositoryId(). ',' ; } elseif( true == array_key_exists( 'StageRepositoryId', $this->getChangedColumns() ) ) { $strSql .= ' stage_repository_id = ' . $this->sqlStageRepositoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' production_repository_id = ' . $this->sqlProductionRepositoryId(). ',' ; } elseif( true == array_key_exists( 'ProductionRepositoryId', $this->getChangedColumns() ) ) { $strSql .= ' production_repository_id = ' . $this->sqlProductionRepositoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tag_path = ' . $this->sqlTagPath(). ',' ; } elseif( true == array_key_exists( 'TagPath', $this->getChangedColumns() ) ) { $strSql .= ' tag_path = ' . $this->sqlTagPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_path = ' . $this->sqlExportPath(). ',' ; } elseif( true == array_key_exists( 'ExportPath', $this->getChangedColumns() ) ) { $strSql .= ' export_path = ' . $this->sqlExportPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_code_base = ' . $this->sqlIsCodeBase(). ',' ; } elseif( true == array_key_exists( 'IsCodeBase', $this->getChangedColumns() ) ) { $strSql .= ' is_code_base = ' . $this->sqlIsCodeBase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cluster_id' => $this->getClusterId(),
			'name' => $this->getName(),
			'ps_product_id' => $this->getPsProductId(),
			'base_repository_id' => $this->getBaseRepositoryId(),
			'stage_repository_id' => $this->getStageRepositoryId(),
			'production_repository_id' => $this->getProductionRepositoryId(),
			'tag_path' => $this->getTagPath(),
			'export_path' => $this->getExportPath(),
			'is_code_base' => $this->getIsCodeBase(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>