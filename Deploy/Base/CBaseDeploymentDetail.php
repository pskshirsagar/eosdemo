<?php

class CBaseDeploymentDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.deployment_details';

	protected $m_intId;
	protected $m_intDeploymentId;
	protected $m_intClusterRepositoryId;
	protected $m_strPushRevisionId;
	protected $m_strPushFiles;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['deployment_id'] ) && $boolDirectSet ) $this->set( 'm_intDeploymentId', trim( $arrValues['deployment_id'] ) ); elseif( isset( $arrValues['deployment_id'] ) ) $this->setDeploymentId( $arrValues['deployment_id'] );
		if( isset( $arrValues['cluster_repository_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterRepositoryId', trim( $arrValues['cluster_repository_id'] ) ); elseif( isset( $arrValues['cluster_repository_id'] ) ) $this->setClusterRepositoryId( $arrValues['cluster_repository_id'] );
		if( isset( $arrValues['push_revision_id'] ) && $boolDirectSet ) $this->set( 'm_strPushRevisionId', trim( stripcslashes( $arrValues['push_revision_id'] ) ) ); elseif( isset( $arrValues['push_revision_id'] ) ) $this->setPushRevisionId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['push_revision_id'] ) : $arrValues['push_revision_id'] );
		if( isset( $arrValues['push_files'] ) && $boolDirectSet ) $this->set( 'm_strPushFiles', trim( stripcslashes( $arrValues['push_files'] ) ) ); elseif( isset( $arrValues['push_files'] ) ) $this->setPushFiles( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['push_files'] ) : $arrValues['push_files'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDeploymentId( $intDeploymentId ) {
		$this->set( 'm_intDeploymentId', CStrings::strToIntDef( $intDeploymentId, NULL, false ) );
	}

	public function getDeploymentId() {
		return $this->m_intDeploymentId;
	}

	public function sqlDeploymentId() {
		return ( true == isset( $this->m_intDeploymentId ) ) ? ( string ) $this->m_intDeploymentId : 'NULL';
	}

	public function setClusterRepositoryId( $intClusterRepositoryId ) {
		$this->set( 'm_intClusterRepositoryId', CStrings::strToIntDef( $intClusterRepositoryId, NULL, false ) );
	}

	public function getClusterRepositoryId() {
		return $this->m_intClusterRepositoryId;
	}

	public function sqlClusterRepositoryId() {
		return ( true == isset( $this->m_intClusterRepositoryId ) ) ? ( string ) $this->m_intClusterRepositoryId : 'NULL';
	}

	public function setPushRevisionId( $strPushRevisionId ) {
		$this->set( 'm_strPushRevisionId', CStrings::strTrimDef( $strPushRevisionId, 50, NULL, true ) );
	}

	public function getPushRevisionId() {
		return $this->m_strPushRevisionId;
	}

	public function sqlPushRevisionId() {
		return ( true == isset( $this->m_strPushRevisionId ) ) ? '\'' . addslashes( $this->m_strPushRevisionId ) . '\'' : 'NULL';
	}

	public function setPushFiles( $strPushFiles ) {
		$this->set( 'm_strPushFiles', CStrings::strTrimDef( $strPushFiles, -1, NULL, true ) );
	}

	public function getPushFiles() {
		return $this->m_strPushFiles;
	}

	public function sqlPushFiles() {
		return ( true == isset( $this->m_strPushFiles ) ) ? '\'' . addslashes( $this->m_strPushFiles ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, deployment_id, cluster_repository_id, push_revision_id, push_files, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDeploymentId() . ', ' .
 						$this->sqlClusterRepositoryId() . ', ' .
 						$this->sqlPushRevisionId() . ', ' .
 						$this->sqlPushFiles() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deployment_id = ' . $this->sqlDeploymentId() . ','; } elseif( true == array_key_exists( 'DeploymentId', $this->getChangedColumns() ) ) { $strSql .= ' deployment_id = ' . $this->sqlDeploymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_repository_id = ' . $this->sqlClusterRepositoryId() . ','; } elseif( true == array_key_exists( 'ClusterRepositoryId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_repository_id = ' . $this->sqlClusterRepositoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' push_revision_id = ' . $this->sqlPushRevisionId() . ','; } elseif( true == array_key_exists( 'PushRevisionId', $this->getChangedColumns() ) ) { $strSql .= ' push_revision_id = ' . $this->sqlPushRevisionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' push_files = ' . $this->sqlPushFiles() . ','; } elseif( true == array_key_exists( 'PushFiles', $this->getChangedColumns() ) ) { $strSql .= ' push_files = ' . $this->sqlPushFiles() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'deployment_id' => $this->getDeploymentId(),
			'cluster_repository_id' => $this->getClusterRepositoryId(),
			'push_revision_id' => $this->getPushRevisionId(),
			'push_files' => $this->getPushFiles(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>