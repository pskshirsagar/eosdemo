<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CEnvironmentTypes
 * Do not add any new functions to this class.
 */

class CBaseEnvironmentTypes extends CEosPluralBase {

	/**
	 * @return CEnvironmentType[]
	 */
	public static function fetchEnvironmentTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEnvironmentType', $objDatabase );
	}

	/**
	 * @return CEnvironmentType
	 */
	public static function fetchEnvironmentType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEnvironmentType', $objDatabase );
	}

	public static function fetchEnvironmentTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'environment_types', $objDatabase );
	}

	public static function fetchEnvironmentTypeById( $intId, $objDatabase ) {
		return self::fetchEnvironmentType( sprintf( 'SELECT * FROM environment_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>