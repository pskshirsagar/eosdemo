<?php

class CBaseLicenceType extends CEosSingularBase {

	const TABLE_NAME = 'public.licence_types';

	protected $m_intId;
	protected $m_strLicenceTypeName;
	protected $m_strDescription;
	protected $m_intIsPublished;
	protected $m_intIsOpensource;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_intIsOpensource = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['licence_type_name'] ) && $boolDirectSet ) $this->set( 'm_strLicenceTypeName', trim( stripcslashes( $arrValues['licence_type_name'] ) ) ); elseif( isset( $arrValues['licence_type_name'] ) ) $this->setLicenceTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['licence_type_name'] ) : $arrValues['licence_type_name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_opensource'] ) && $boolDirectSet ) $this->set( 'm_intIsOpensource', trim( $arrValues['is_opensource'] ) ); elseif( isset( $arrValues['is_opensource'] ) ) $this->setIsOpensource( $arrValues['is_opensource'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setLicenceTypeName( $strLicenceTypeName ) {
		$this->set( 'm_strLicenceTypeName', CStrings::strTrimDef( $strLicenceTypeName, 1024, NULL, true ) );
	}

	public function getLicenceTypeName() {
		return $this->m_strLicenceTypeName;
	}

	public function sqlLicenceTypeName() {
		return ( true == isset( $this->m_strLicenceTypeName ) ) ? '\'' . addslashes( $this->m_strLicenceTypeName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsOpensource( $intIsOpensource ) {
		$this->set( 'm_intIsOpensource', CStrings::strToIntDef( $intIsOpensource, NULL, false ) );
	}

	public function getIsOpensource() {
		return $this->m_intIsOpensource;
	}

	public function sqlIsOpensource() {
		return ( true == isset( $this->m_intIsOpensource ) ) ? ( string ) $this->m_intIsOpensource : '1';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'licence_type_name' => $this->getLicenceTypeName(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished(),
			'is_opensource' => $this->getIsOpensource()
		);
	}

}
?>