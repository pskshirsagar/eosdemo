<?php

class CBaseQueueConsumer extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.queue_consumers';

	protected $m_intId;
	protected $m_strBasicQueueName;
	protected $m_strDescription;
	protected $m_strConsumerClassName;
	protected $m_intMaxExecutionSeconds;
	protected $m_strSchedule;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIsClusterSpecific;
	protected $m_boolIsPaymentsSpecific;
	protected $m_boolIsLogging;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intHardwareRamSize;
	protected $m_intHardwareCores;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsClusterSpecific = false;
		$this->m_boolIsPaymentsSpecific = false;
		$this->m_boolIsLogging = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['basic_queue_name'] ) && $boolDirectSet ) $this->set( 'm_strBasicQueueName', trim( stripcslashes( $arrValues['basic_queue_name'] ) ) ); elseif( isset( $arrValues['basic_queue_name'] ) ) $this->setBasicQueueName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['basic_queue_name'] ) : $arrValues['basic_queue_name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['consumer_class_name'] ) && $boolDirectSet ) $this->set( 'm_strConsumerClassName', trim( stripcslashes( $arrValues['consumer_class_name'] ) ) ); elseif( isset( $arrValues['consumer_class_name'] ) ) $this->setConsumerClassName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['consumer_class_name'] ) : $arrValues['consumer_class_name'] );
		if( isset( $arrValues['max_execution_seconds'] ) && $boolDirectSet ) $this->set( 'm_intMaxExecutionSeconds', trim( $arrValues['max_execution_seconds'] ) ); elseif( isset( $arrValues['max_execution_seconds'] ) ) $this->setMaxExecutionSeconds( $arrValues['max_execution_seconds'] );
		if( isset( $arrValues['schedule'] ) && $boolDirectSet ) $this->set( 'm_strSchedule', trim( stripcslashes( $arrValues['schedule'] ) ) ); elseif( isset( $arrValues['schedule'] ) ) $this->setSchedule( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['schedule'] ) : $arrValues['schedule'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_cluster_specific'] ) && $boolDirectSet ) $this->set( 'm_boolIsClusterSpecific', trim( stripcslashes( $arrValues['is_cluster_specific'] ) ) ); elseif( isset( $arrValues['is_cluster_specific'] ) ) $this->setIsClusterSpecific( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_cluster_specific'] ) : $arrValues['is_cluster_specific'] );
		if( isset( $arrValues['is_payments_specific'] ) && $boolDirectSet ) $this->set( 'm_boolIsPaymentsSpecific', trim( stripcslashes( $arrValues['is_payments_specific'] ) ) ); elseif( isset( $arrValues['is_payments_specific'] ) ) $this->setIsPaymentsSpecific( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_payments_specific'] ) : $arrValues['is_payments_specific'] );
		if( isset( $arrValues['is_logging'] ) && $boolDirectSet ) $this->set( 'm_boolIsLogging', trim( stripcslashes( $arrValues['is_logging'] ) ) ); elseif( isset( $arrValues['is_logging'] ) ) $this->setIsLogging( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_logging'] ) : $arrValues['is_logging'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['hardware_ram_size'] ) && $boolDirectSet ) $this->set( 'm_intHardwareRamSize', trim( $arrValues['hardware_ram_size'] ) ); elseif( isset( $arrValues['hardware_ram_size'] ) ) $this->setHardwareRamSize( $arrValues['hardware_ram_size'] );
		if( isset( $arrValues['hardware_cores'] ) && $boolDirectSet ) $this->set( 'm_intHardwareCores', trim( $arrValues['hardware_cores'] ) ); elseif( isset( $arrValues['hardware_cores'] ) ) $this->setHardwareCores( $arrValues['hardware_cores'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setBasicQueueName( $strBasicQueueName ) {
		$this->set( 'm_strBasicQueueName', CStrings::strTrimDef( $strBasicQueueName, 150, NULL, true ) );
	}

	public function getBasicQueueName() {
		return $this->m_strBasicQueueName;
	}

	public function sqlBasicQueueName() {
		return ( true == isset( $this->m_strBasicQueueName ) ) ? '\'' . addslashes( $this->m_strBasicQueueName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setConsumerClassName( $strConsumerClassName ) {
		$this->set( 'm_strConsumerClassName', CStrings::strTrimDef( $strConsumerClassName, 250, NULL, true ) );
	}

	public function getConsumerClassName() {
		return $this->m_strConsumerClassName;
	}

	public function sqlConsumerClassName() {
		return ( true == isset( $this->m_strConsumerClassName ) ) ? '\'' . addslashes( $this->m_strConsumerClassName ) . '\'' : 'NULL';
	}

	public function setMaxExecutionSeconds( $intMaxExecutionSeconds ) {
		$this->set( 'm_intMaxExecutionSeconds', CStrings::strToIntDef( $intMaxExecutionSeconds, NULL, false ) );
	}

	public function getMaxExecutionSeconds() {
		return $this->m_intMaxExecutionSeconds;
	}

	public function sqlMaxExecutionSeconds() {
		return ( true == isset( $this->m_intMaxExecutionSeconds ) ) ? ( string ) $this->m_intMaxExecutionSeconds : 'NULL';
	}

	public function setSchedule( $strSchedule ) {
		$this->set( 'm_strSchedule', CStrings::strTrimDef( $strSchedule, 250, NULL, true ) );
	}

	public function getSchedule() {
		return $this->m_strSchedule;
	}

	public function sqlSchedule() {
		return ( true == isset( $this->m_strSchedule ) ) ? '\'' . addslashes( $this->m_strSchedule ) . '\'' : 'NULL';
	}

	public function setIsClusterSpecific( $boolIsClusterSpecific ) {
		$this->set( 'm_boolIsClusterSpecific', CStrings::strToBool( $boolIsClusterSpecific ) );
	}

	public function getIsClusterSpecific() {
		return $this->m_boolIsClusterSpecific;
	}

	public function sqlIsClusterSpecific() {
		return ( true == isset( $this->m_boolIsClusterSpecific ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsClusterSpecific ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPaymentsSpecific( $boolIsPaymentsSpecific ) {
		$this->set( 'm_boolIsPaymentsSpecific', CStrings::strToBool( $boolIsPaymentsSpecific ) );
	}

	public function getIsPaymentsSpecific() {
		return $this->m_boolIsPaymentsSpecific;
	}

	public function sqlIsPaymentsSpecific() {
		return ( true == isset( $this->m_boolIsPaymentsSpecific ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPaymentsSpecific ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLogging( $boolIsLogging ) {
		$this->set( 'm_boolIsLogging', CStrings::strToBool( $boolIsLogging ) );
	}

	public function getIsLogging() {
		return $this->m_boolIsLogging;
	}

	public function sqlIsLogging() {
		return ( true == isset( $this->m_boolIsLogging ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLogging ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setHardwareRamSize( $intHardwareRamSize ) {
		$this->set( 'm_intHardwareRamSize', CStrings::strToIntDef( $intHardwareRamSize, NULL, false ) );
	}

	public function getHardwareRamSize() {
		return $this->m_intHardwareRamSize;
	}

	public function sqlHardwareRamSize() {
		return ( true == isset( $this->m_intHardwareRamSize ) ) ? ( string ) $this->m_intHardwareRamSize : 'NULL';
	}

	public function setHardwareCores( $intHardwareCores ) {
		$this->set( 'm_intHardwareCores', CStrings::strToIntDef( $intHardwareCores, NULL, false ) );
	}

	public function getHardwareCores() {
		return $this->m_intHardwareCores;
	}

	public function sqlHardwareCores() {
		return ( true == isset( $this->m_intHardwareCores ) ) ? ( string ) $this->m_intHardwareCores : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, basic_queue_name, description, consumer_class_name, max_execution_seconds, schedule, details, is_cluster_specific, is_payments_specific, is_logging, updated_by, updated_on, created_by, created_on, hardware_ram_size, hardware_cores )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlBasicQueueName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlConsumerClassName() . ', ' .
						$this->sqlMaxExecutionSeconds() . ', ' .
						$this->sqlSchedule() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsClusterSpecific() . ', ' .
						$this->sqlIsPaymentsSpecific() . ', ' .
						$this->sqlIsLogging() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlHardwareRamSize() . ', ' .
						$this->sqlHardwareCores() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' basic_queue_name = ' . $this->sqlBasicQueueName(). ',' ; } elseif( true == array_key_exists( 'BasicQueueName', $this->getChangedColumns() ) ) { $strSql .= ' basic_queue_name = ' . $this->sqlBasicQueueName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumer_class_name = ' . $this->sqlConsumerClassName(). ',' ; } elseif( true == array_key_exists( 'ConsumerClassName', $this->getChangedColumns() ) ) { $strSql .= ' consumer_class_name = ' . $this->sqlConsumerClassName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_execution_seconds = ' . $this->sqlMaxExecutionSeconds(). ',' ; } elseif( true == array_key_exists( 'MaxExecutionSeconds', $this->getChangedColumns() ) ) { $strSql .= ' max_execution_seconds = ' . $this->sqlMaxExecutionSeconds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' schedule = ' . $this->sqlSchedule(). ',' ; } elseif( true == array_key_exists( 'Schedule', $this->getChangedColumns() ) ) { $strSql .= ' schedule = ' . $this->sqlSchedule() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_cluster_specific = ' . $this->sqlIsClusterSpecific(). ',' ; } elseif( true == array_key_exists( 'IsClusterSpecific', $this->getChangedColumns() ) ) { $strSql .= ' is_cluster_specific = ' . $this->sqlIsClusterSpecific() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_payments_specific = ' . $this->sqlIsPaymentsSpecific(). ',' ; } elseif( true == array_key_exists( 'IsPaymentsSpecific', $this->getChangedColumns() ) ) { $strSql .= ' is_payments_specific = ' . $this->sqlIsPaymentsSpecific() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_logging = ' . $this->sqlIsLogging(). ',' ; } elseif( true == array_key_exists( 'IsLogging', $this->getChangedColumns() ) ) { $strSql .= ' is_logging = ' . $this->sqlIsLogging() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hardware_ram_size = ' . $this->sqlHardwareRamSize(). ',' ; } elseif( true == array_key_exists( 'HardwareRamSize', $this->getChangedColumns() ) ) { $strSql .= ' hardware_ram_size = ' . $this->sqlHardwareRamSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hardware_cores = ' . $this->sqlHardwareCores(). ',' ; } elseif( true == array_key_exists( 'HardwareCores', $this->getChangedColumns() ) ) { $strSql .= ' hardware_cores = ' . $this->sqlHardwareCores() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'basic_queue_name' => $this->getBasicQueueName(),
			'description' => $this->getDescription(),
			'consumer_class_name' => $this->getConsumerClassName(),
			'max_execution_seconds' => $this->getMaxExecutionSeconds(),
			'schedule' => $this->getSchedule(),
			'details' => $this->getDetails(),
			'is_cluster_specific' => $this->getIsClusterSpecific(),
			'is_payments_specific' => $this->getIsPaymentsSpecific(),
			'is_logging' => $this->getIsLogging(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'hardware_ram_size' => $this->getHardwareRamSize(),
			'hardware_cores' => $this->getHardwareCores()
		);
	}

}
?>