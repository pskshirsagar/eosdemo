<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CQueueConsumerHardwares
 * Do not add any new functions to this class.
 */

class CBaseQueueConsumerHardwares extends CEosPluralBase {

	/**
	 * @return CQueueConsumerHardware[]
	 */
	public static function fetchQueueConsumerHardwares( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CQueueConsumerHardware', $objDatabase );
	}

	/**
	 * @return CQueueConsumerHardware
	 */
	public static function fetchQueueConsumerHardware( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CQueueConsumerHardware', $objDatabase );
	}

	public static function fetchQueueConsumerHardwareCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'queue_consumer_hardwares', $objDatabase );
	}

	public static function fetchQueueConsumerHardwareById( $intId, $objDatabase ) {
		return self::fetchQueueConsumerHardware( sprintf( 'SELECT * FROM queue_consumer_hardwares WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchQueueConsumerHardwaresByQueueConsumerId( $intQueueConsumerId, $objDatabase ) {
		return self::fetchQueueConsumerHardwares( sprintf( 'SELECT * FROM queue_consumer_hardwares WHERE queue_consumer_id = %d', ( int ) $intQueueConsumerId ), $objDatabase );
	}

	public static function fetchQueueConsumerHardwaresByHardwareId( $intHardwareId, $objDatabase ) {
		return self::fetchQueueConsumerHardwares( sprintf( 'SELECT * FROM queue_consumer_hardwares WHERE hardware_id = %d', ( int ) $intHardwareId ), $objDatabase );
	}

}
?>