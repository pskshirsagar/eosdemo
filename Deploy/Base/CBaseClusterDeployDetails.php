<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClusterDeployDetails
 * Do not add any new functions to this class.
 */

class CBaseClusterDeployDetails extends CEosPluralBase {

	/**
	 * @return CClusterDeployDetail[]
	 */
	public static function fetchClusterDeployDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CClusterDeployDetail', $objDatabase );
	}

	/**
	 * @return CClusterDeployDetail
	 */
	public static function fetchClusterDeployDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CClusterDeployDetail', $objDatabase );
	}

	public static function fetchClusterDeployDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cluster_deploy_details', $objDatabase );
	}

	public static function fetchClusterDeployDetailById( $intId, $objDatabase ) {
		return self::fetchClusterDeployDetail( sprintf( 'SELECT * FROM cluster_deploy_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchClusterDeployDetailsByClusterId( $intClusterId, $objDatabase ) {
		return self::fetchClusterDeployDetails( sprintf( 'SELECT * FROM cluster_deploy_details WHERE cluster_id = %d', ( int ) $intClusterId ), $objDatabase );
	}

	public static function fetchClusterDeployDetailsByDeploymentRoleId( $intDeploymentRoleId, $objDatabase ) {
		return self::fetchClusterDeployDetails( sprintf( 'SELECT * FROM cluster_deploy_details WHERE deployment_role_id = %d', ( int ) $intDeploymentRoleId ), $objDatabase );
	}

	public static function fetchClusterDeployDetailsByApprovalRoleId( $intApprovalRoleId, $objDatabase ) {
		return self::fetchClusterDeployDetails( sprintf( 'SELECT * FROM cluster_deploy_details WHERE approval_role_id = %d', ( int ) $intApprovalRoleId ), $objDatabase );
	}

}
?>