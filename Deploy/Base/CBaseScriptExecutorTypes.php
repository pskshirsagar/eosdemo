<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CScriptExecutorTypes
 * Do not add any new functions to this class.
 */

class CBaseScriptExecutorTypes extends CEosPluralBase {

	/**
	 * @return CScriptExecutorType[]
	 */
	public static function fetchScriptExecutorTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScriptExecutorType', $objDatabase );
	}

	/**
	 * @return CScriptExecutorType
	 */
	public static function fetchScriptExecutorType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScriptExecutorType', $objDatabase );
	}

	public static function fetchScriptExecutorTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'script_executor_types', $objDatabase );
	}

	public static function fetchScriptExecutorTypeById( $intId, $objDatabase ) {
		return self::fetchScriptExecutorType( sprintf( 'SELECT * FROM script_executor_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>