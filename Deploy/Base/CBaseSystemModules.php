<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CSystemModules
 * Do not add any new functions to this class.
 */

class CBaseSystemModules extends CEosPluralBase {

	/**
	 * @return CSystemModule[]
	 */
	public static function fetchSystemModules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSystemModule', $objDatabase );
	}

	/**
	 * @return CSystemModule
	 */
	public static function fetchSystemModule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSystemModule', $objDatabase );
	}

	public static function fetchSystemModuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_modules', $objDatabase );
	}

	public static function fetchSystemModuleById( $intId, $objDatabase ) {
		return self::fetchSystemModule( sprintf( 'SELECT * FROM system_modules WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSystemModulesByParentModuleId( $intParentModuleId, $objDatabase ) {
		return self::fetchSystemModules( sprintf( 'SELECT * FROM system_modules WHERE parent_module_id = %d', ( int ) $intParentModuleId ), $objDatabase );
	}

}
?>