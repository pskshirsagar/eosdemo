<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CInternalIpAddresses
 * Do not add any new functions to this class.
 */

class CBaseInternalIpAddresses extends CEosPluralBase {

	/**
	 * @return CInternalIpAddress[]
	 */
	public static function fetchInternalIpAddresses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInternalIpAddress', $objDatabase );
	}

	/**
	 * @return CInternalIpAddress
	 */
	public static function fetchInternalIpAddress( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInternalIpAddress', $objDatabase );
	}

	public static function fetchInternalIpAddressCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'internal_ip_addresses', $objDatabase );
	}

	public static function fetchInternalIpAddressById( $intId, $objDatabase ) {
		return self::fetchInternalIpAddress( sprintf( 'SELECT * FROM internal_ip_addresses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>