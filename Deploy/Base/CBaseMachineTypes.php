<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CMachineTypes
 * Do not add any new functions to this class.
 */

class CBaseMachineTypes extends CEosPluralBase {

	/**
	 * @return CMachineType[]
	 */
	public static function fetchMachineTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMachineType', $objDatabase );
	}

	/**
	 * @return CMachineType
	 */
	public static function fetchMachineType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMachineType', $objDatabase );
	}

	public static function fetchMachineTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'machine_types', $objDatabase );
	}

	public static function fetchMachineTypeById( $intId, $objDatabase ) {
		return self::fetchMachineType( sprintf( 'SELECT * FROM machine_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>