<?php

class CBaseSqlFile extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.sql_files';

	protected $m_intId;
	protected $m_strSqlFileName;
	protected $m_strDescription;
	protected $m_intDatabaseTypeId;
	protected $m_intTaskId;
	protected $m_intSqlReviewedBy;
	protected $m_boolIsPublished;
	protected $m_intOrderNumber;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intDatabaseTypeId = '0';
		$this->m_intTaskId = '0';
		$this->m_intSqlReviewedBy = '0';
		$this->m_boolIsPublished = true;
		$this->m_intOrderNumber = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['sql_file_name'] ) && $boolDirectSet ) $this->set( 'm_strSqlFileName', trim( $arrValues['sql_file_name'] ) ); elseif( isset( $arrValues['sql_file_name'] ) ) $this->setSqlFileName( $arrValues['sql_file_name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['database_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseTypeId', trim( $arrValues['database_type_id'] ) ); elseif( isset( $arrValues['database_type_id'] ) ) $this->setDatabaseTypeId( $arrValues['database_type_id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['sql_reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intSqlReviewedBy', trim( $arrValues['sql_reviewed_by'] ) ); elseif( isset( $arrValues['sql_reviewed_by'] ) ) $this->setSqlReviewedBy( $arrValues['sql_reviewed_by'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_number'] ) && $boolDirectSet ) $this->set( 'm_intOrderNumber', trim( $arrValues['order_number'] ) ); elseif( isset( $arrValues['order_number'] ) ) $this->setOrderNumber( $arrValues['order_number'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSqlFileName( $strSqlFileName ) {
		$this->set( 'm_strSqlFileName', CStrings::strTrimDef( $strSqlFileName, 1024, NULL, true ) );
	}

	public function getSqlFileName() {
		return $this->m_strSqlFileName;
	}

	public function sqlSqlFileName() {
		return ( true == isset( $this->m_strSqlFileName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSqlFileName ) : '\'' . addslashes( $this->m_strSqlFileName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setDatabaseTypeId( $intDatabaseTypeId ) {
		$this->set( 'm_intDatabaseTypeId', CStrings::strToIntDef( $intDatabaseTypeId, NULL, false ) );
	}

	public function getDatabaseTypeId() {
		return $this->m_intDatabaseTypeId;
	}

	public function sqlDatabaseTypeId() {
		return ( true == isset( $this->m_intDatabaseTypeId ) ) ? ( string ) $this->m_intDatabaseTypeId : '0';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : '0';
	}

	public function setSqlReviewedBy( $intSqlReviewedBy ) {
		$this->set( 'm_intSqlReviewedBy', CStrings::strToIntDef( $intSqlReviewedBy, NULL, false ) );
	}

	public function getSqlReviewedBy() {
		return $this->m_intSqlReviewedBy;
	}

	public function sqlSqlReviewedBy() {
		return ( true == isset( $this->m_intSqlReviewedBy ) ) ? ( string ) $this->m_intSqlReviewedBy : '0';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNumber( $intOrderNumber ) {
		$this->set( 'm_intOrderNumber', CStrings::strToIntDef( $intOrderNumber, NULL, false ) );
	}

	public function getOrderNumber() {
		return $this->m_intOrderNumber;
	}

	public function sqlOrderNumber() {
		return ( true == isset( $this->m_intOrderNumber ) ) ? ( string ) $this->m_intOrderNumber : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, sql_file_name, description, database_type_id, task_id, sql_reviewed_by, is_published, order_number, updated_by, updated_on, created_by, created_on, deleted_by, deleted_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlSqlFileName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDatabaseTypeId() . ', ' .
						$this->sqlTaskId() . ', ' .
						$this->sqlSqlReviewedBy() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlOrderNumber() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sql_file_name = ' . $this->sqlSqlFileName(). ',' ; } elseif( true == array_key_exists( 'SqlFileName', $this->getChangedColumns() ) ) { $strSql .= ' sql_file_name = ' . $this->sqlSqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_type_id = ' . $this->sqlDatabaseTypeId(). ',' ; } elseif( true == array_key_exists( 'DatabaseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' database_type_id = ' . $this->sqlDatabaseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId(). ',' ; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sql_reviewed_by = ' . $this->sqlSqlReviewedBy(). ',' ; } elseif( true == array_key_exists( 'SqlReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' sql_reviewed_by = ' . $this->sqlSqlReviewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_number = ' . $this->sqlOrderNumber(). ',' ; } elseif( true == array_key_exists( 'OrderNumber', $this->getChangedColumns() ) ) { $strSql .= ' order_number = ' . $this->sqlOrderNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'sql_file_name' => $this->getSqlFileName(),
			'description' => $this->getDescription(),
			'database_type_id' => $this->getDatabaseTypeId(),
			'task_id' => $this->getTaskId(),
			'sql_reviewed_by' => $this->getSqlReviewedBy(),
			'is_published' => $this->getIsPublished(),
			'order_number' => $this->getOrderNumber(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>