<?php

class CBaseClientDataInstance extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.client_data_instances';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intTrainingClientId;
	protected $m_intParentClientId;
	protected $m_intSourceDatabaseId;
	protected $m_intDestinationDatabaseId;
	protected $m_intIsTemplate;
	protected $m_intIsCsvUploaded;
	protected $m_strPropertyIds;
	protected $m_intHardwareId;
	protected $m_intResetFrequencyTypeId;
	protected $m_strDescription;
	protected $m_strStakeholders;
	protected $m_strResetOn;
	protected $m_strExpiresOn;
	protected $m_intRequestedBy;
	protected $m_strRequestedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strReservedPropertyIds;
	protected $m_jsonReservedPropertyIds;

	public function __construct() {
		parent::__construct();

		$this->m_intIsTemplate = '1';
		$this->m_intIsCsvUploaded = '0';
		$this->m_intResetFrequencyTypeId = '4';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['training_client_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingClientId', trim( $arrValues['training_client_id'] ) ); elseif( isset( $arrValues['training_client_id'] ) ) $this->setTrainingClientId( $arrValues['training_client_id'] );
		if( isset( $arrValues['parent_client_id'] ) && $boolDirectSet ) $this->set( 'm_intParentClientId', trim( $arrValues['parent_client_id'] ) ); elseif( isset( $arrValues['parent_client_id'] ) ) $this->setParentClientId( $arrValues['parent_client_id'] );
		if( isset( $arrValues['source_database_id'] ) && $boolDirectSet ) $this->set( 'm_intSourceDatabaseId', trim( $arrValues['source_database_id'] ) ); elseif( isset( $arrValues['source_database_id'] ) ) $this->setSourceDatabaseId( $arrValues['source_database_id'] );
		if( isset( $arrValues['destination_database_id'] ) && $boolDirectSet ) $this->set( 'm_intDestinationDatabaseId', trim( $arrValues['destination_database_id'] ) ); elseif( isset( $arrValues['destination_database_id'] ) ) $this->setDestinationDatabaseId( $arrValues['destination_database_id'] );
		if( isset( $arrValues['is_template'] ) && $boolDirectSet ) $this->set( 'm_intIsTemplate', trim( $arrValues['is_template'] ) ); elseif( isset( $arrValues['is_template'] ) ) $this->setIsTemplate( $arrValues['is_template'] );
		if( isset( $arrValues['is_csv_uploaded'] ) && $boolDirectSet ) $this->set( 'm_intIsCsvUploaded', trim( $arrValues['is_csv_uploaded'] ) ); elseif( isset( $arrValues['is_csv_uploaded'] ) ) $this->setIsCsvUploaded( $arrValues['is_csv_uploaded'] );
		if( isset( $arrValues['property_ids'] ) && $boolDirectSet ) $this->set( 'm_strPropertyIds', trim( $arrValues['property_ids'] ) ); elseif( isset( $arrValues['property_ids'] ) ) $this->setPropertyIds( $arrValues['property_ids'] );
		if( isset( $arrValues['hardware_id'] ) && $boolDirectSet ) $this->set( 'm_intHardwareId', trim( $arrValues['hardware_id'] ) ); elseif( isset( $arrValues['hardware_id'] ) ) $this->setHardwareId( $arrValues['hardware_id'] );
		if( isset( $arrValues['reset_frequency_type_id'] ) && $boolDirectSet ) $this->set( 'm_intResetFrequencyTypeId', trim( $arrValues['reset_frequency_type_id'] ) ); elseif( isset( $arrValues['reset_frequency_type_id'] ) ) $this->setResetFrequencyTypeId( $arrValues['reset_frequency_type_id'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['stakeholders'] ) && $boolDirectSet ) $this->set( 'm_strStakeholders', trim( $arrValues['stakeholders'] ) ); elseif( isset( $arrValues['stakeholders'] ) ) $this->setStakeholders( $arrValues['stakeholders'] );
		if( isset( $arrValues['reset_on'] ) && $boolDirectSet ) $this->set( 'm_strResetOn', trim( $arrValues['reset_on'] ) ); elseif( isset( $arrValues['reset_on'] ) ) $this->setResetOn( $arrValues['reset_on'] );
		if( isset( $arrValues['expires_on'] ) && $boolDirectSet ) $this->set( 'm_strExpiresOn', trim( $arrValues['expires_on'] ) ); elseif( isset( $arrValues['expires_on'] ) ) $this->setExpiresOn( $arrValues['expires_on'] );
		if( isset( $arrValues['requested_by'] ) && $boolDirectSet ) $this->set( 'm_intRequestedBy', trim( $arrValues['requested_by'] ) ); elseif( isset( $arrValues['requested_by'] ) ) $this->setRequestedBy( $arrValues['requested_by'] );
		if( isset( $arrValues['requested_on'] ) && $boolDirectSet ) $this->set( 'm_strRequestedOn', trim( $arrValues['requested_on'] ) ); elseif( isset( $arrValues['requested_on'] ) ) $this->setRequestedOn( $arrValues['requested_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['reserved_property_ids'] ) ) $this->set( 'm_strReservedPropertyIds', trim( $arrValues['reserved_property_ids'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setTrainingClientId( $intTrainingClientId ) {
		$this->set( 'm_intTrainingClientId', CStrings::strToIntDef( $intTrainingClientId, NULL, false ) );
	}

	public function getTrainingClientId() {
		return $this->m_intTrainingClientId;
	}

	public function sqlTrainingClientId() {
		return ( true == isset( $this->m_intTrainingClientId ) ) ? ( string ) $this->m_intTrainingClientId : 'NULL';
	}

	public function setParentClientId( $intParentClientId ) {
		$this->set( 'm_intParentClientId', CStrings::strToIntDef( $intParentClientId, NULL, false ) );
	}

	public function getParentClientId() {
		return $this->m_intParentClientId;
	}

	public function sqlParentClientId() {
		return ( true == isset( $this->m_intParentClientId ) ) ? ( string ) $this->m_intParentClientId : 'NULL';
	}

	public function setSourceDatabaseId( $intSourceDatabaseId ) {
		$this->set( 'm_intSourceDatabaseId', CStrings::strToIntDef( $intSourceDatabaseId, NULL, false ) );
	}

	public function getSourceDatabaseId() {
		return $this->m_intSourceDatabaseId;
	}

	public function sqlSourceDatabaseId() {
		return ( true == isset( $this->m_intSourceDatabaseId ) ) ? ( string ) $this->m_intSourceDatabaseId : 'NULL';
	}

	public function setDestinationDatabaseId( $intDestinationDatabaseId ) {
		$this->set( 'm_intDestinationDatabaseId', CStrings::strToIntDef( $intDestinationDatabaseId, NULL, false ) );
	}

	public function getDestinationDatabaseId() {
		return $this->m_intDestinationDatabaseId;
	}

	public function sqlDestinationDatabaseId() {
		return ( true == isset( $this->m_intDestinationDatabaseId ) ) ? ( string ) $this->m_intDestinationDatabaseId : 'NULL';
	}

	public function setIsTemplate( $intIsTemplate ) {
		$this->set( 'm_intIsTemplate', CStrings::strToIntDef( $intIsTemplate, NULL, false ) );
	}

	public function getIsTemplate() {
		return $this->m_intIsTemplate;
	}

	public function sqlIsTemplate() {
		return ( true == isset( $this->m_intIsTemplate ) ) ? ( string ) $this->m_intIsTemplate : '1';
	}

	public function setIsCsvUploaded( $intIsCsvUploaded ) {
		$this->set( 'm_intIsCsvUploaded', CStrings::strToIntDef( $intIsCsvUploaded, NULL, false ) );
	}

	public function getIsCsvUploaded() {
		return $this->m_intIsCsvUploaded;
	}

	public function sqlIsCsvUploaded() {
		return ( true == isset( $this->m_intIsCsvUploaded ) ) ? ( string ) $this->m_intIsCsvUploaded : '0';
	}

	public function setPropertyIds( $strPropertyIds ) {
		$this->set( 'm_strPropertyIds', CStrings::strTrimDef( $strPropertyIds, -1, NULL, true ) );
	}

	public function getPropertyIds() {
		return $this->m_strPropertyIds;
	}

	public function sqlPropertyIds() {
		return ( true == isset( $this->m_strPropertyIds ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPropertyIds ) : '\'' . addslashes( $this->m_strPropertyIds ) . '\'' ) : 'NULL';
	}

	public function setHardwareId( $intHardwareId ) {
		$this->set( 'm_intHardwareId', CStrings::strToIntDef( $intHardwareId, NULL, false ) );
	}

	public function getHardwareId() {
		return $this->m_intHardwareId;
	}

	public function sqlHardwareId() {
		return ( true == isset( $this->m_intHardwareId ) ) ? ( string ) $this->m_intHardwareId : 'NULL';
	}

	public function setResetFrequencyTypeId( $intResetFrequencyTypeId ) {
		$this->set( 'm_intResetFrequencyTypeId', CStrings::strToIntDef( $intResetFrequencyTypeId, NULL, false ) );
	}

	public function getResetFrequencyTypeId() {
		return $this->m_intResetFrequencyTypeId;
	}

	public function sqlResetFrequencyTypeId() {
		return ( true == isset( $this->m_intResetFrequencyTypeId ) ) ? ( string ) $this->m_intResetFrequencyTypeId : '4';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 1000, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setStakeholders( $strStakeholders ) {
		$this->set( 'm_strStakeholders', CStrings::strTrimDef( $strStakeholders, -1, NULL, true ) );
	}

	public function getStakeholders() {
		return $this->m_strStakeholders;
	}

	public function sqlStakeholders() {
		return ( true == isset( $this->m_strStakeholders ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStakeholders ) : '\'' . addslashes( $this->m_strStakeholders ) . '\'' ) : 'NULL';
	}

	public function setResetOn( $strResetOn ) {
		$this->set( 'm_strResetOn', CStrings::strTrimDef( $strResetOn, -1, NULL, true ) );
	}

	public function getResetOn() {
		return $this->m_strResetOn;
	}

	public function sqlResetOn() {
		return ( true == isset( $this->m_strResetOn ) ) ? '\'' . $this->m_strResetOn . '\'' : 'NULL';
	}

	public function setExpiresOn( $strExpiresOn ) {
		$this->set( 'm_strExpiresOn', CStrings::strTrimDef( $strExpiresOn, -1, NULL, true ) );
	}

	public function getExpiresOn() {
		return $this->m_strExpiresOn;
	}

	public function sqlExpiresOn() {
		return ( true == isset( $this->m_strExpiresOn ) ) ? '\'' . $this->m_strExpiresOn . '\'' : 'NULL';
	}

	public function setRequestedBy( $intRequestedBy ) {
		$this->set( 'm_intRequestedBy', CStrings::strToIntDef( $intRequestedBy, NULL, false ) );
	}

	public function getRequestedBy() {
		return $this->m_intRequestedBy;
	}

	public function sqlRequestedBy() {
		return ( true == isset( $this->m_intRequestedBy ) ) ? ( string ) $this->m_intRequestedBy : 'NULL';
	}

	public function setRequestedOn( $strRequestedOn ) {
		$this->set( 'm_strRequestedOn', CStrings::strTrimDef( $strRequestedOn, -1, NULL, true ) );
	}

	public function getRequestedOn() {
		return $this->m_strRequestedOn;
	}

	public function sqlRequestedOn() {
		return ( true == isset( $this->m_strRequestedOn ) ) ? '\'' . $this->m_strRequestedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setReservedPropertyIds( $jsonReservedPropertyIds ) {
		if( true == valObj( $jsonReservedPropertyIds, 'stdClass' ) ) {
			$this->set( 'm_jsonReservedPropertyIds', $jsonReservedPropertyIds );
		} elseif( true == valJsonString( $jsonReservedPropertyIds ) ) {
			$this->set( 'm_jsonReservedPropertyIds', CStrings::strToJson( $jsonReservedPropertyIds ) );
		} else {
			$this->set( 'm_jsonReservedPropertyIds', NULL ); 
		}
		unset( $this->m_strReservedPropertyIds );
	}

	public function getReservedPropertyIds() {
		if( true == isset( $this->m_strReservedPropertyIds ) ) {
			$this->m_jsonReservedPropertyIds = CStrings::strToJson( $this->m_strReservedPropertyIds );
			unset( $this->m_strReservedPropertyIds );
		}
		return $this->m_jsonReservedPropertyIds;
	}

	public function sqlReservedPropertyIds() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getReservedPropertyIds() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getReservedPropertyIds() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getReservedPropertyIds() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, training_client_id, parent_client_id, source_database_id, destination_database_id, is_template, is_csv_uploaded, property_ids, hardware_id, reset_frequency_type_id, description, stakeholders, reset_on, expires_on, requested_by, requested_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, reserved_property_ids )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlTrainingClientId() . ', ' .
						$this->sqlParentClientId() . ', ' .
						$this->sqlSourceDatabaseId() . ', ' .
						$this->sqlDestinationDatabaseId() . ', ' .
						$this->sqlIsTemplate() . ', ' .
						$this->sqlIsCsvUploaded() . ', ' .
						$this->sqlPropertyIds() . ', ' .
						$this->sqlHardwareId() . ', ' .
						$this->sqlResetFrequencyTypeId() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlStakeholders() . ', ' .
						$this->sqlResetOn() . ', ' .
						$this->sqlExpiresOn() . ', ' .
						$this->sqlRequestedBy() . ', ' .
						$this->sqlRequestedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlReservedPropertyIds() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_client_id = ' . $this->sqlTrainingClientId(). ',' ; } elseif( true == array_key_exists( 'TrainingClientId', $this->getChangedColumns() ) ) { $strSql .= ' training_client_id = ' . $this->sqlTrainingClientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_client_id = ' . $this->sqlParentClientId(). ',' ; } elseif( true == array_key_exists( 'ParentClientId', $this->getChangedColumns() ) ) { $strSql .= ' parent_client_id = ' . $this->sqlParentClientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_database_id = ' . $this->sqlSourceDatabaseId(). ',' ; } elseif( true == array_key_exists( 'SourceDatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' source_database_id = ' . $this->sqlSourceDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_database_id = ' . $this->sqlDestinationDatabaseId(). ',' ; } elseif( true == array_key_exists( 'DestinationDatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' destination_database_id = ' . $this->sqlDestinationDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_template = ' . $this->sqlIsTemplate(). ',' ; } elseif( true == array_key_exists( 'IsTemplate', $this->getChangedColumns() ) ) { $strSql .= ' is_template = ' . $this->sqlIsTemplate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_csv_uploaded = ' . $this->sqlIsCsvUploaded(). ',' ; } elseif( true == array_key_exists( 'IsCsvUploaded', $this->getChangedColumns() ) ) { $strSql .= ' is_csv_uploaded = ' . $this->sqlIsCsvUploaded() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_ids = ' . $this->sqlPropertyIds(). ',' ; } elseif( true == array_key_exists( 'PropertyIds', $this->getChangedColumns() ) ) { $strSql .= ' property_ids = ' . $this->sqlPropertyIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hardware_id = ' . $this->sqlHardwareId(). ',' ; } elseif( true == array_key_exists( 'HardwareId', $this->getChangedColumns() ) ) { $strSql .= ' hardware_id = ' . $this->sqlHardwareId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reset_frequency_type_id = ' . $this->sqlResetFrequencyTypeId(). ',' ; } elseif( true == array_key_exists( 'ResetFrequencyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' reset_frequency_type_id = ' . $this->sqlResetFrequencyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' stakeholders = ' . $this->sqlStakeholders(). ',' ; } elseif( true == array_key_exists( 'Stakeholders', $this->getChangedColumns() ) ) { $strSql .= ' stakeholders = ' . $this->sqlStakeholders() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reset_on = ' . $this->sqlResetOn(). ',' ; } elseif( true == array_key_exists( 'ResetOn', $this->getChangedColumns() ) ) { $strSql .= ' reset_on = ' . $this->sqlResetOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn(). ',' ; } elseif( true == array_key_exists( 'ExpiresOn', $this->getChangedColumns() ) ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_by = ' . $this->sqlRequestedBy(). ',' ; } elseif( true == array_key_exists( 'RequestedBy', $this->getChangedColumns() ) ) { $strSql .= ' requested_by = ' . $this->sqlRequestedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_on = ' . $this->sqlRequestedOn(). ',' ; } elseif( true == array_key_exists( 'RequestedOn', $this->getChangedColumns() ) ) { $strSql .= ' requested_on = ' . $this->sqlRequestedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reserved_property_ids = ' . $this->sqlReservedPropertyIds(). ',' ; } elseif( true == array_key_exists( 'ReservedPropertyIds', $this->getChangedColumns() ) ) { $strSql .= ' reserved_property_ids = ' . $this->sqlReservedPropertyIds() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'training_client_id' => $this->getTrainingClientId(),
			'parent_client_id' => $this->getParentClientId(),
			'source_database_id' => $this->getSourceDatabaseId(),
			'destination_database_id' => $this->getDestinationDatabaseId(),
			'is_template' => $this->getIsTemplate(),
			'is_csv_uploaded' => $this->getIsCsvUploaded(),
			'property_ids' => $this->getPropertyIds(),
			'hardware_id' => $this->getHardwareId(),
			'reset_frequency_type_id' => $this->getResetFrequencyTypeId(),
			'description' => $this->getDescription(),
			'stakeholders' => $this->getStakeholders(),
			'reset_on' => $this->getResetOn(),
			'expires_on' => $this->getExpiresOn(),
			'requested_by' => $this->getRequestedBy(),
			'requested_on' => $this->getRequestedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'reserved_property_ids' => $this->getReservedPropertyIds()
		);
	}

}
?>