<?php

class CBaseHardwareConfiguration extends CEosSingularBase {

	const TABLE_NAME = 'public.hardware_configurations';

	protected $m_intId;
	protected $m_intHardwareId;
	protected $m_intTotalCores;
	protected $m_intRamSize;
	protected $m_intDiskSpace;
	protected $m_strZendServerVersion;
	protected $m_strZendUrl;
	protected $m_strPhpVersion;
	protected $m_boolIsBackup;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['hardware_id'] ) && $boolDirectSet ) $this->set( 'm_intHardwareId', trim( $arrValues['hardware_id'] ) ); elseif( isset( $arrValues['hardware_id'] ) ) $this->setHardwareId( $arrValues['hardware_id'] );
		if( isset( $arrValues['total_cores'] ) && $boolDirectSet ) $this->set( 'm_intTotalCores', trim( $arrValues['total_cores'] ) ); elseif( isset( $arrValues['total_cores'] ) ) $this->setTotalCores( $arrValues['total_cores'] );
		if( isset( $arrValues['ram_size'] ) && $boolDirectSet ) $this->set( 'm_intRamSize', trim( $arrValues['ram_size'] ) ); elseif( isset( $arrValues['ram_size'] ) ) $this->setRamSize( $arrValues['ram_size'] );
		if( isset( $arrValues['disk_space'] ) && $boolDirectSet ) $this->set( 'm_intDiskSpace', trim( $arrValues['disk_space'] ) ); elseif( isset( $arrValues['disk_space'] ) ) $this->setDiskSpace( $arrValues['disk_space'] );
		if( isset( $arrValues['zend_server_version'] ) && $boolDirectSet ) $this->set( 'm_strZendServerVersion', trim( stripcslashes( $arrValues['zend_server_version'] ) ) ); elseif( isset( $arrValues['zend_server_version'] ) ) $this->setZendServerVersion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['zend_server_version'] ) : $arrValues['zend_server_version'] );
		if( isset( $arrValues['zend_url'] ) && $boolDirectSet ) $this->set( 'm_strZendUrl', trim( stripcslashes( $arrValues['zend_url'] ) ) ); elseif( isset( $arrValues['zend_url'] ) ) $this->setZendUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['zend_url'] ) : $arrValues['zend_url'] );
		if( isset( $arrValues['php_version'] ) && $boolDirectSet ) $this->set( 'm_strPhpVersion', trim( stripcslashes( $arrValues['php_version'] ) ) ); elseif( isset( $arrValues['php_version'] ) ) $this->setPhpVersion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['php_version'] ) : $arrValues['php_version'] );
		if( isset( $arrValues['is_backup'] ) && $boolDirectSet ) $this->set( 'm_boolIsBackup', trim( stripcslashes( $arrValues['is_backup'] ) ) ); elseif( isset( $arrValues['is_backup'] ) ) $this->setIsBackup( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_backup'] ) : $arrValues['is_backup'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setHardwareId( $intHardwareId ) {
		$this->set( 'm_intHardwareId', CStrings::strToIntDef( $intHardwareId, NULL, false ) );
	}

	public function getHardwareId() {
		return $this->m_intHardwareId;
	}

	public function sqlHardwareId() {
		return ( true == isset( $this->m_intHardwareId ) ) ? ( string ) $this->m_intHardwareId : 'NULL';
	}

	public function setTotalCores( $intTotalCores ) {
		$this->set( 'm_intTotalCores', CStrings::strToIntDef( $intTotalCores, NULL, false ) );
	}

	public function getTotalCores() {
		return $this->m_intTotalCores;
	}

	public function sqlTotalCores() {
		return ( true == isset( $this->m_intTotalCores ) ) ? ( string ) $this->m_intTotalCores : 'NULL';
	}

	public function setRamSize( $intRamSize ) {
		$this->set( 'm_intRamSize', CStrings::strToIntDef( $intRamSize, NULL, false ) );
	}

	public function getRamSize() {
		return $this->m_intRamSize;
	}

	public function sqlRamSize() {
		return ( true == isset( $this->m_intRamSize ) ) ? ( string ) $this->m_intRamSize : 'NULL';
	}

	public function setDiskSpace( $intDiskSpace ) {
		$this->set( 'm_intDiskSpace', CStrings::strToIntDef( $intDiskSpace, NULL, false ) );
	}

	public function getDiskSpace() {
		return $this->m_intDiskSpace;
	}

	public function sqlDiskSpace() {
		return ( true == isset( $this->m_intDiskSpace ) ) ? ( string ) $this->m_intDiskSpace : 'NULL';
	}

	public function setZendServerVersion( $strZendServerVersion ) {
		$this->set( 'm_strZendServerVersion', CStrings::strTrimDef( $strZendServerVersion, 240, NULL, true ) );
	}

	public function getZendServerVersion() {
		return $this->m_strZendServerVersion;
	}

	public function sqlZendServerVersion() {
		return ( true == isset( $this->m_strZendServerVersion ) ) ? '\'' . addslashes( $this->m_strZendServerVersion ) . '\'' : 'NULL';
	}

	public function setZendUrl( $strZendUrl ) {
		$this->set( 'm_strZendUrl', CStrings::strTrimDef( $strZendUrl, 240, NULL, true ) );
	}

	public function getZendUrl() {
		return $this->m_strZendUrl;
	}

	public function sqlZendUrl() {
		return ( true == isset( $this->m_strZendUrl ) ) ? '\'' . addslashes( $this->m_strZendUrl ) . '\'' : 'NULL';
	}

	public function setPhpVersion( $strPhpVersion ) {
		$this->set( 'm_strPhpVersion', CStrings::strTrimDef( $strPhpVersion, 240, NULL, true ) );
	}

	public function getPhpVersion() {
		return $this->m_strPhpVersion;
	}

	public function sqlPhpVersion() {
		return ( true == isset( $this->m_strPhpVersion ) ) ? '\'' . addslashes( $this->m_strPhpVersion ) . '\'' : 'NULL';
	}

	public function setIsBackup( $boolIsBackup ) {
		$this->set( 'm_boolIsBackup', CStrings::strToBool( $boolIsBackup ) );
	}

	public function getIsBackup() {
		return $this->m_boolIsBackup;
	}

	public function sqlIsBackup() {
		return ( true == isset( $this->m_boolIsBackup ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBackup ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, hardware_id, total_cores, ram_size, disk_space, zend_server_version, zend_url, php_version, is_backup, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlHardwareId() . ', ' .
 						$this->sqlTotalCores() . ', ' .
 						$this->sqlRamSize() . ', ' .
 						$this->sqlDiskSpace() . ', ' .
 						$this->sqlZendServerVersion() . ', ' .
 						$this->sqlZendUrl() . ', ' .
 						$this->sqlPhpVersion() . ', ' .
 						$this->sqlIsBackup() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hardware_id = ' . $this->sqlHardwareId() . ','; } elseif( true == array_key_exists( 'HardwareId', $this->getChangedColumns() ) ) { $strSql .= ' hardware_id = ' . $this->sqlHardwareId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_cores = ' . $this->sqlTotalCores() . ','; } elseif( true == array_key_exists( 'TotalCores', $this->getChangedColumns() ) ) { $strSql .= ' total_cores = ' . $this->sqlTotalCores() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ram_size = ' . $this->sqlRamSize() . ','; } elseif( true == array_key_exists( 'RamSize', $this->getChangedColumns() ) ) { $strSql .= ' ram_size = ' . $this->sqlRamSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disk_space = ' . $this->sqlDiskSpace() . ','; } elseif( true == array_key_exists( 'DiskSpace', $this->getChangedColumns() ) ) { $strSql .= ' disk_space = ' . $this->sqlDiskSpace() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' zend_server_version = ' . $this->sqlZendServerVersion() . ','; } elseif( true == array_key_exists( 'ZendServerVersion', $this->getChangedColumns() ) ) { $strSql .= ' zend_server_version = ' . $this->sqlZendServerVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' zend_url = ' . $this->sqlZendUrl() . ','; } elseif( true == array_key_exists( 'ZendUrl', $this->getChangedColumns() ) ) { $strSql .= ' zend_url = ' . $this->sqlZendUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' php_version = ' . $this->sqlPhpVersion() . ','; } elseif( true == array_key_exists( 'PhpVersion', $this->getChangedColumns() ) ) { $strSql .= ' php_version = ' . $this->sqlPhpVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_backup = ' . $this->sqlIsBackup() . ','; } elseif( true == array_key_exists( 'IsBackup', $this->getChangedColumns() ) ) { $strSql .= ' is_backup = ' . $this->sqlIsBackup() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'hardware_id' => $this->getHardwareId(),
			'total_cores' => $this->getTotalCores(),
			'ram_size' => $this->getRamSize(),
			'disk_space' => $this->getDiskSpace(),
			'zend_server_version' => $this->getZendServerVersion(),
			'zend_url' => $this->getZendUrl(),
			'php_version' => $this->getPhpVersion(),
			'is_backup' => $this->getIsBackup(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>