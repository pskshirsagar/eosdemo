<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CScriptTypes
 * Do not add any new functions to this class.
 */

class CBaseScriptTypes extends CEosPluralBase {

	/**
	 * @return CScriptType[]
	 */
	public static function fetchScriptTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScriptType', $objDatabase );
	}

	/**
	 * @return CScriptType
	 */
	public static function fetchScriptType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScriptType', $objDatabase );
	}

	public static function fetchScriptTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'script_types', $objDatabase );
	}

	public static function fetchScriptTypeById( $intId, $objDatabase ) {
		return self::fetchScriptType( sprintf( 'SELECT * FROM script_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>