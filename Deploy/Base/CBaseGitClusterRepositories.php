<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CGitClusterRepositories
 * Do not add any new functions to this class.
 */

class CBaseGitClusterRepositories extends CEosPluralBase {

	/**
	 * @return CGitClusterRepository[]
	 */
	public static function fetchGitClusterRepositories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CGitClusterRepository', $objDatabase );
	}

	/**
	 * @return CGitClusterRepository
	 */
	public static function fetchGitClusterRepository( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGitClusterRepository', $objDatabase );
	}

	public static function fetchGitClusterRepositoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'git_cluster_repositories', $objDatabase );
	}

	public static function fetchGitClusterRepositoryById( $intId, $objDatabase ) {
		return self::fetchGitClusterRepository( sprintf( 'SELECT * FROM git_cluster_repositories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchGitClusterRepositoriesByClusterId( $intClusterId, $objDatabase ) {
		return self::fetchGitClusterRepositories( sprintf( 'SELECT * FROM git_cluster_repositories WHERE cluster_id = %d', ( int ) $intClusterId ), $objDatabase );
	}

	public static function fetchGitClusterRepositoriesByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchGitClusterRepositories( sprintf( 'SELECT * FROM git_cluster_repositories WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchGitClusterRepositoriesByBaseRepositoryId( $intBaseRepositoryId, $objDatabase ) {
		return self::fetchGitClusterRepositories( sprintf( 'SELECT * FROM git_cluster_repositories WHERE base_repository_id = %d', ( int ) $intBaseRepositoryId ), $objDatabase );
	}

	public static function fetchGitClusterRepositoriesByStageRepositoryId( $intStageRepositoryId, $objDatabase ) {
		return self::fetchGitClusterRepositories( sprintf( 'SELECT * FROM git_cluster_repositories WHERE stage_repository_id = %d', ( int ) $intStageRepositoryId ), $objDatabase );
	}

	public static function fetchGitClusterRepositoriesByProductionRepositoryId( $intProductionRepositoryId, $objDatabase ) {
		return self::fetchGitClusterRepositories( sprintf( 'SELECT * FROM git_cluster_repositories WHERE production_repository_id = %d', ( int ) $intProductionRepositoryId ), $objDatabase );
	}

}
?>