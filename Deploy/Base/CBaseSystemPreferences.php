<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CSystemPreferences
 * Do not add any new functions to this class.
 */

class CBaseSystemPreferences extends CEosPluralBase {

	/**
	 * @return CSystemPreference[]
	 */
	public static function fetchSystemPreferences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSystemPreference', $objDatabase );
	}

	/**
	 * @return CSystemPreference
	 */
	public static function fetchSystemPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSystemPreference', $objDatabase );
	}

	public static function fetchSystemPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_preferences', $objDatabase );
	}

	public static function fetchSystemPreferenceById( $intId, $objDatabase ) {
		return self::fetchSystemPreference( sprintf( 'SELECT * FROM system_preferences WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>