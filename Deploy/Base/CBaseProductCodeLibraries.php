<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CProductCodeLibraries
 * Do not add any new functions to this class.
 */

class CBaseProductCodeLibraries extends CEosPluralBase {

	/**
	 * @return CProductCodeLibrary[]
	 */
	public static function fetchProductCodeLibraries( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CProductCodeLibrary::class, $objDatabase );
	}

	/**
	 * @return CProductCodeLibrary
	 */
	public static function fetchProductCodeLibrary( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CProductCodeLibrary::class, $objDatabase );
	}

	public static function fetchProductCodeLibraryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'product_code_libraries', $objDatabase );
	}

	public static function fetchProductCodeLibraryById( $intId, $objDatabase ) {
		return self::fetchProductCodeLibrary( sprintf( 'SELECT * FROM product_code_libraries WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchProductCodeLibrariesByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchProductCodeLibraries( sprintf( 'SELECT * FROM product_code_libraries WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchProductCodeLibrariesByCodeLibrariesId( $intCodeLibrariesId, $objDatabase ) {
		return self::fetchProductCodeLibraries( sprintf( 'SELECT * FROM product_code_libraries WHERE code_libraries_id = %d', ( int ) $intCodeLibrariesId ), $objDatabase );
	}

}
?>