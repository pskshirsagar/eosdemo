<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CLicenceTypes
 * Do not add any new functions to this class.
 */

class CBaseLicenceTypes extends CEosPluralBase {

	/**
	 * @return CLicenceType[]
	 */
	public static function fetchLicenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CLicenceType::class, $objDatabase );
	}

	/**
	 * @return CLicenceType
	 */
	public static function fetchLicenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CLicenceType::class, $objDatabase );
	}

	public static function fetchLicenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'licence_types', $objDatabase );
	}

	public static function fetchLicenceTypeById( $intId, $objDatabase ) {
		return self::fetchLicenceType( sprintf( 'SELECT * FROM licence_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>