<?php

class CBaseCodeLibrary extends CEosSingularBase {

	const TABLE_NAME = 'public.code_libraries';

	protected $m_intId;
	protected $m_strLibraryName;
	protected $m_strCurrentVersion;
	protected $m_strAvailableVersion;
	protected $m_boolIsModified;
	protected $m_strLicence;
	protected $m_intLicenceTypeId;
	protected $m_strAccessedOn;
	protected $m_strExpiresOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_strDeletedOn;
	protected $m_intDeletedBy;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsModified = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['library_name'] ) && $boolDirectSet ) $this->set( 'm_strLibraryName', trim( stripcslashes( $arrValues['library_name'] ) ) ); elseif( isset( $arrValues['library_name'] ) ) $this->setLibraryName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['library_name'] ) : $arrValues['library_name'] );
		if( isset( $arrValues['current_version'] ) && $boolDirectSet ) $this->set( 'm_strCurrentVersion', trim( stripcslashes( $arrValues['current_version'] ) ) ); elseif( isset( $arrValues['current_version'] ) ) $this->setCurrentVersion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['current_version'] ) : $arrValues['current_version'] );
		if( isset( $arrValues['available_version'] ) && $boolDirectSet ) $this->set( 'm_strAvailableVersion', trim( stripcslashes( $arrValues['available_version'] ) ) ); elseif( isset( $arrValues['available_version'] ) ) $this->setAvailableVersion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['available_version'] ) : $arrValues['available_version'] );
		if( isset( $arrValues['is_modified'] ) && $boolDirectSet ) $this->set( 'm_boolIsModified', trim( stripcslashes( $arrValues['is_modified'] ) ) ); elseif( isset( $arrValues['is_modified'] ) ) $this->setIsModified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_modified'] ) : $arrValues['is_modified'] );
		if( isset( $arrValues['licence'] ) && $boolDirectSet ) $this->set( 'm_strLicence', trim( stripcslashes( $arrValues['licence'] ) ) ); elseif( isset( $arrValues['licence'] ) ) $this->setLicence( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['licence'] ) : $arrValues['licence'] );
		if( isset( $arrValues['licence_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLicenceTypeId', trim( $arrValues['licence_type_id'] ) ); elseif( isset( $arrValues['licence_type_id'] ) ) $this->setLicenceTypeId( $arrValues['licence_type_id'] );
		if( isset( $arrValues['accessed_on'] ) && $boolDirectSet ) $this->set( 'm_strAccessedOn', trim( $arrValues['accessed_on'] ) ); elseif( isset( $arrValues['accessed_on'] ) ) $this->setAccessedOn( $arrValues['accessed_on'] );
		if( isset( $arrValues['expires_on'] ) && $boolDirectSet ) $this->set( 'm_strExpiresOn', trim( $arrValues['expires_on'] ) ); elseif( isset( $arrValues['expires_on'] ) ) $this->setExpiresOn( $arrValues['expires_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setLibraryName( $strLibraryName ) {
		$this->set( 'm_strLibraryName', CStrings::strTrimDef( $strLibraryName, 1024, NULL, true ) );
	}

	public function getLibraryName() {
		return $this->m_strLibraryName;
	}

	public function sqlLibraryName() {
		return ( true == isset( $this->m_strLibraryName ) ) ? '\'' . addslashes( $this->m_strLibraryName ) . '\'' : 'NULL';
	}

	public function setCurrentVersion( $strCurrentVersion ) {
		$this->set( 'm_strCurrentVersion', CStrings::strTrimDef( $strCurrentVersion, 255, NULL, true ) );
	}

	public function getCurrentVersion() {
		return $this->m_strCurrentVersion;
	}

	public function sqlCurrentVersion() {
		return ( true == isset( $this->m_strCurrentVersion ) ) ? '\'' . addslashes( $this->m_strCurrentVersion ) . '\'' : 'NULL';
	}

	public function setAvailableVersion( $strAvailableVersion ) {
		$this->set( 'm_strAvailableVersion', CStrings::strTrimDef( $strAvailableVersion, 255, NULL, true ) );
	}

	public function getAvailableVersion() {
		return $this->m_strAvailableVersion;
	}

	public function sqlAvailableVersion() {
		return ( true == isset( $this->m_strAvailableVersion ) ) ? '\'' . addslashes( $this->m_strAvailableVersion ) . '\'' : 'NULL';
	}

	public function setIsModified( $boolIsModified ) {
		$this->set( 'm_boolIsModified', CStrings::strToBool( $boolIsModified ) );
	}

	public function getIsModified() {
		return $this->m_boolIsModified;
	}

	public function sqlIsModified() {
		return ( true == isset( $this->m_boolIsModified ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsModified ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLicence( $strLicence ) {
		$this->set( 'm_strLicence', CStrings::strTrimDef( $strLicence, -1, NULL, true ) );
	}

	public function getLicence() {
		return $this->m_strLicence;
	}

	public function sqlLicence() {
		return ( true == isset( $this->m_strLicence ) ) ? '\'' . addslashes( $this->m_strLicence ) . '\'' : 'NULL';
	}

	public function setLicenceTypeId( $intLicenceTypeId ) {
		$this->set( 'm_intLicenceTypeId', CStrings::strToIntDef( $intLicenceTypeId, NULL, false ) );
	}

	public function getLicenceTypeId() {
		return $this->m_intLicenceTypeId;
	}

	public function sqlLicenceTypeId() {
		return ( true == isset( $this->m_intLicenceTypeId ) ) ? ( string ) $this->m_intLicenceTypeId : 'NULL';
	}

	public function setAccessedOn( $strAccessedOn ) {
		$this->set( 'm_strAccessedOn', CStrings::strTrimDef( $strAccessedOn, -1, NULL, true ) );
	}

	public function getAccessedOn() {
		return $this->m_strAccessedOn;
	}

	public function sqlAccessedOn() {
		return ( true == isset( $this->m_strAccessedOn ) ) ? '\'' . $this->m_strAccessedOn . '\'' : 'NULL';
	}

	public function setExpiresOn( $strExpiresOn ) {
		$this->set( 'm_strExpiresOn', CStrings::strTrimDef( $strExpiresOn, -1, NULL, true ) );
	}

	public function getExpiresOn() {
		return $this->m_strExpiresOn;
	}

	public function sqlExpiresOn() {
		return ( true == isset( $this->m_strExpiresOn ) ) ? '\'' . $this->m_strExpiresOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, library_name, current_version, available_version, is_modified, licence, licence_type_id, accessed_on, expires_on, created_by, created_on, updated_by, updated_on, deleted_on, deleted_by )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlLibraryName() . ', ' .
 						$this->sqlCurrentVersion() . ', ' .
 						$this->sqlAvailableVersion() . ', ' .
 						$this->sqlIsModified() . ', ' .
 						$this->sqlLicence() . ', ' .
 						$this->sqlLicenceTypeId() . ', ' .
 						$this->sqlAccessedOn() . ', ' .
 						$this->sqlExpiresOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
 						$this->sqlDeletedBy() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' library_name = ' . $this->sqlLibraryName() . ','; } elseif( true == array_key_exists( 'LibraryName', $this->getChangedColumns() ) ) { $strSql .= ' library_name = ' . $this->sqlLibraryName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_version = ' . $this->sqlCurrentVersion() . ','; } elseif( true == array_key_exists( 'CurrentVersion', $this->getChangedColumns() ) ) { $strSql .= ' current_version = ' . $this->sqlCurrentVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' available_version = ' . $this->sqlAvailableVersion() . ','; } elseif( true == array_key_exists( 'AvailableVersion', $this->getChangedColumns() ) ) { $strSql .= ' available_version = ' . $this->sqlAvailableVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_modified = ' . $this->sqlIsModified() . ','; } elseif( true == array_key_exists( 'IsModified', $this->getChangedColumns() ) ) { $strSql .= ' is_modified = ' . $this->sqlIsModified() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' licence = ' . $this->sqlLicence() . ','; } elseif( true == array_key_exists( 'Licence', $this->getChangedColumns() ) ) { $strSql .= ' licence = ' . $this->sqlLicence() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' licence_type_id = ' . $this->sqlLicenceTypeId() . ','; } elseif( true == array_key_exists( 'LicenceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' licence_type_id = ' . $this->sqlLicenceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accessed_on = ' . $this->sqlAccessedOn() . ','; } elseif( true == array_key_exists( 'AccessedOn', $this->getChangedColumns() ) ) { $strSql .= ' accessed_on = ' . $this->sqlAccessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn() . ','; } elseif( true == array_key_exists( 'ExpiresOn', $this->getChangedColumns() ) ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'library_name' => $this->getLibraryName(),
			'current_version' => $this->getCurrentVersion(),
			'available_version' => $this->getAvailableVersion(),
			'is_modified' => $this->getIsModified(),
			'licence' => $this->getLicence(),
			'licence_type_id' => $this->getLicenceTypeId(),
			'accessed_on' => $this->getAccessedOn(),
			'expires_on' => $this->getExpiresOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'deleted_on' => $this->getDeletedOn(),
			'deleted_by' => $this->getDeletedBy()
		);
	}

}
?>