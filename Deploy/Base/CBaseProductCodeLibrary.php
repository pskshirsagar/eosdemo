<?php

class CBaseProductCodeLibrary extends CEosSingularBase {

	const TABLE_NAME = 'public.product_code_libraries';

	protected $m_intId;
	protected $m_intPsProductId;
	protected $m_intCodeLibrariesId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['code_libraries_id'] ) && $boolDirectSet ) $this->set( 'm_intCodeLibrariesId', trim( $arrValues['code_libraries_id'] ) ); elseif( isset( $arrValues['code_libraries_id'] ) ) $this->setCodeLibrariesId( $arrValues['code_libraries_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setCodeLibrariesId( $intCodeLibrariesId ) {
		$this->set( 'm_intCodeLibrariesId', CStrings::strToIntDef( $intCodeLibrariesId, NULL, false ) );
	}

	public function getCodeLibrariesId() {
		return $this->m_intCodeLibrariesId;
	}

	public function sqlCodeLibrariesId() {
		return ( true == isset( $this->m_intCodeLibrariesId ) ) ? ( string ) $this->m_intCodeLibrariesId : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_product_id' => $this->getPsProductId(),
			'code_libraries_id' => $this->getCodeLibrariesId()
		);
	}

}
?>