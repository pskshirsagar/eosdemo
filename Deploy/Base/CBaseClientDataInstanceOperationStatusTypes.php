<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClientDataInstanceOperationStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseClientDataInstanceOperationStatusTypes extends CEosPluralBase {

	/**
	 * @return CClientDataInstanceOperationStatusType[]
	 */
	public static function fetchClientDataInstanceOperationStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CClientDataInstanceOperationStatusType', $objDatabase );
	}

	/**
	 * @return CClientDataInstanceOperationStatusType
	 */
	public static function fetchClientDataInstanceOperationStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CClientDataInstanceOperationStatusType', $objDatabase );
	}

	public static function fetchClientDataInstanceOperationStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'client_data_instance_operation_status_types', $objDatabase );
	}

	public static function fetchClientDataInstanceOperationStatusTypeById( $intId, $objDatabase ) {
		return self::fetchClientDataInstanceOperationStatusType( sprintf( 'SELECT * FROM client_data_instance_operation_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>