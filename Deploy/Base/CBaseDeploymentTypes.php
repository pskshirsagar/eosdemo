<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CDeploymentTypes
 * Do not add any new functions to this class.
 */

class CBaseDeploymentTypes extends CEosPluralBase {

	/**
	 * @return CDeploymentType[]
	 */
	public static function fetchDeploymentTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDeploymentType', $objDatabase );
	}

	/**
	 * @return CDeploymentType
	 */
	public static function fetchDeploymentType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDeploymentType', $objDatabase );
	}

	public static function fetchDeploymentTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'deployment_types', $objDatabase );
	}

	public static function fetchDeploymentTypeById( $intId, $objDatabase ) {
		return self::fetchDeploymentType( sprintf( 'SELECT * FROM deployment_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>