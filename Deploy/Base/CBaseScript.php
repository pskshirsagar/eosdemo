<?php

class CBaseScript extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.scripts';

	protected $m_intId;
	protected $m_intScriptTypeId;
	protected $m_intScriptExecutorTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strScriptPath;
	protected $m_strLogPath;
	protected $m_strSchedule;
	protected $m_strRunOnSpecificDatabases;
	protected $m_strParameters;
	protected $m_intExecutionMinutes;
	protected $m_intDoNotRunInRentWeek;
	protected $m_intDoNotMonitor;
	protected $m_intIsCritical;
	protected $m_intIsPublished;
	protected $m_boolRunOnTestDb;
	protected $m_boolIsCentralizedLogging;
	protected $m_boolIsNewrelicInstrumentation;
	protected $m_intOrderNum;
	protected $m_strCompletedOn;
	protected $m_strDeletedOn;
	protected $m_intDeletedBy;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolRunOnTemplateDb;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intScriptExecutorTypeId = '3';
		$this->m_intExecutionMinutes = '1';
		$this->m_intDoNotRunInRentWeek = '0';
		$this->m_intDoNotMonitor = '0';
		$this->m_intIsCritical = '0';
		$this->m_intIsPublished = '1';
		$this->m_boolRunOnTestDb = false;
		$this->m_boolIsCentralizedLogging = false;
		$this->m_boolIsNewrelicInstrumentation = false;
		$this->m_intOrderNum = '0';
		$this->m_boolRunOnTemplateDb = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['script_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScriptTypeId', trim( $arrValues['script_type_id'] ) ); elseif( isset( $arrValues['script_type_id'] ) ) $this->setScriptTypeId( $arrValues['script_type_id'] );
		if( isset( $arrValues['script_executor_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScriptExecutorTypeId', trim( $arrValues['script_executor_type_id'] ) ); elseif( isset( $arrValues['script_executor_type_id'] ) ) $this->setScriptExecutorTypeId( $arrValues['script_executor_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['script_path'] ) && $boolDirectSet ) $this->set( 'm_strScriptPath', trim( stripcslashes( $arrValues['script_path'] ) ) ); elseif( isset( $arrValues['script_path'] ) ) $this->setScriptPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['script_path'] ) : $arrValues['script_path'] );
		if( isset( $arrValues['log_path'] ) && $boolDirectSet ) $this->set( 'm_strLogPath', trim( stripcslashes( $arrValues['log_path'] ) ) ); elseif( isset( $arrValues['log_path'] ) ) $this->setLogPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['log_path'] ) : $arrValues['log_path'] );
		if( isset( $arrValues['schedule'] ) && $boolDirectSet ) $this->set( 'm_strSchedule', trim( stripcslashes( $arrValues['schedule'] ) ) ); elseif( isset( $arrValues['schedule'] ) ) $this->setSchedule( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['schedule'] ) : $arrValues['schedule'] );
		if( isset( $arrValues['run_on_specific_databases'] ) && $boolDirectSet ) $this->set( 'm_strRunOnSpecificDatabases', trim( stripcslashes( $arrValues['run_on_specific_databases'] ) ) ); elseif( isset( $arrValues['run_on_specific_databases'] ) ) $this->setRunOnSpecificDatabases( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['run_on_specific_databases'] ) : $arrValues['run_on_specific_databases'] );
		if( isset( $arrValues['parameters'] ) && $boolDirectSet ) $this->set( 'm_strParameters', trim( stripcslashes( $arrValues['parameters'] ) ) ); elseif( isset( $arrValues['parameters'] ) ) $this->setParameters( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['parameters'] ) : $arrValues['parameters'] );
		if( isset( $arrValues['execution_minutes'] ) && $boolDirectSet ) $this->set( 'm_intExecutionMinutes', trim( $arrValues['execution_minutes'] ) ); elseif( isset( $arrValues['execution_minutes'] ) ) $this->setExecutionMinutes( $arrValues['execution_minutes'] );
		if( isset( $arrValues['do_not_run_in_rent_week'] ) && $boolDirectSet ) $this->set( 'm_intDoNotRunInRentWeek', trim( $arrValues['do_not_run_in_rent_week'] ) ); elseif( isset( $arrValues['do_not_run_in_rent_week'] ) ) $this->setDoNotRunInRentWeek( $arrValues['do_not_run_in_rent_week'] );
		if( isset( $arrValues['do_not_monitor'] ) && $boolDirectSet ) $this->set( 'm_intDoNotMonitor', trim( $arrValues['do_not_monitor'] ) ); elseif( isset( $arrValues['do_not_monitor'] ) ) $this->setDoNotMonitor( $arrValues['do_not_monitor'] );
		if( isset( $arrValues['is_critical'] ) && $boolDirectSet ) $this->set( 'm_intIsCritical', trim( $arrValues['is_critical'] ) ); elseif( isset( $arrValues['is_critical'] ) ) $this->setIsCritical( $arrValues['is_critical'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['run_on_test_db'] ) && $boolDirectSet ) $this->set( 'm_boolRunOnTestDb', trim( stripcslashes( $arrValues['run_on_test_db'] ) ) ); elseif( isset( $arrValues['run_on_test_db'] ) ) $this->setRunOnTestDb( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['run_on_test_db'] ) : $arrValues['run_on_test_db'] );
		if( isset( $arrValues['is_centralized_logging'] ) && $boolDirectSet ) $this->set( 'm_boolIsCentralizedLogging', trim( stripcslashes( $arrValues['is_centralized_logging'] ) ) ); elseif( isset( $arrValues['is_centralized_logging'] ) ) $this->setIsCentralizedLogging( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_centralized_logging'] ) : $arrValues['is_centralized_logging'] );
		if( isset( $arrValues['is_newrelic_instrumentation'] ) && $boolDirectSet ) $this->set( 'm_boolIsNewrelicInstrumentation', trim( stripcslashes( $arrValues['is_newrelic_instrumentation'] ) ) ); elseif( isset( $arrValues['is_newrelic_instrumentation'] ) ) $this->setIsNewrelicInstrumentation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_newrelic_instrumentation'] ) : $arrValues['is_newrelic_instrumentation'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['run_on_template_db'] ) && $boolDirectSet ) $this->set( 'm_boolRunOnTemplateDb', trim( stripcslashes( $arrValues['run_on_template_db'] ) ) ); elseif( isset( $arrValues['run_on_template_db'] ) ) $this->setRunOnTemplateDb( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['run_on_template_db'] ) : $arrValues['run_on_template_db'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScriptTypeId( $intScriptTypeId ) {
		$this->set( 'm_intScriptTypeId', CStrings::strToIntDef( $intScriptTypeId, NULL, false ) );
	}

	public function getScriptTypeId() {
		return $this->m_intScriptTypeId;
	}

	public function sqlScriptTypeId() {
		return ( true == isset( $this->m_intScriptTypeId ) ) ? ( string ) $this->m_intScriptTypeId : 'NULL';
	}

	public function setScriptExecutorTypeId( $intScriptExecutorTypeId ) {
		$this->set( 'm_intScriptExecutorTypeId', CStrings::strToIntDef( $intScriptExecutorTypeId, NULL, false ) );
	}

	public function getScriptExecutorTypeId() {
		return $this->m_intScriptExecutorTypeId;
	}

	public function sqlScriptExecutorTypeId() {
		return ( true == isset( $this->m_intScriptExecutorTypeId ) ) ? ( string ) $this->m_intScriptExecutorTypeId : '3';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 255, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setScriptPath( $strScriptPath ) {
		$this->set( 'm_strScriptPath', CStrings::strTrimDef( $strScriptPath, -1, NULL, true ) );
	}

	public function getScriptPath() {
		return $this->m_strScriptPath;
	}

	public function sqlScriptPath() {
		return ( true == isset( $this->m_strScriptPath ) ) ? '\'' . addslashes( $this->m_strScriptPath ) . '\'' : 'NULL';
	}

	public function setLogPath( $strLogPath ) {
		$this->set( 'm_strLogPath', CStrings::strTrimDef( $strLogPath, -1, NULL, true ) );
	}

	public function getLogPath() {
		return $this->m_strLogPath;
	}

	public function sqlLogPath() {
		return ( true == isset( $this->m_strLogPath ) ) ? '\'' . addslashes( $this->m_strLogPath ) . '\'' : 'NULL';
	}

	public function setSchedule( $strSchedule ) {
		$this->set( 'm_strSchedule', CStrings::strTrimDef( $strSchedule, 255, NULL, true ) );
	}

	public function getSchedule() {
		return $this->m_strSchedule;
	}

	public function sqlSchedule() {
		return ( true == isset( $this->m_strSchedule ) ) ? '\'' . addslashes( $this->m_strSchedule ) . '\'' : 'NULL';
	}

	public function setRunOnSpecificDatabases( $strRunOnSpecificDatabases ) {
		$this->set( 'm_strRunOnSpecificDatabases', CStrings::strTrimDef( $strRunOnSpecificDatabases, 1000, NULL, true ) );
	}

	public function getRunOnSpecificDatabases() {
		return $this->m_strRunOnSpecificDatabases;
	}

	public function sqlRunOnSpecificDatabases() {
		return ( true == isset( $this->m_strRunOnSpecificDatabases ) ) ? '\'' . addslashes( $this->m_strRunOnSpecificDatabases ) . '\'' : 'NULL';
	}

	public function setParameters( $strParameters ) {
		$this->set( 'm_strParameters', CStrings::strTrimDef( $strParameters, -1, NULL, true ) );
	}

	public function getParameters() {
		return $this->m_strParameters;
	}

	public function sqlParameters() {
		return ( true == isset( $this->m_strParameters ) ) ? '\'' . addslashes( $this->m_strParameters ) . '\'' : 'NULL';
	}

	public function setExecutionMinutes( $intExecutionMinutes ) {
		$this->set( 'm_intExecutionMinutes', CStrings::strToIntDef( $intExecutionMinutes, NULL, false ) );
	}

	public function getExecutionMinutes() {
		return $this->m_intExecutionMinutes;
	}

	public function sqlExecutionMinutes() {
		return ( true == isset( $this->m_intExecutionMinutes ) ) ? ( string ) $this->m_intExecutionMinutes : '1';
	}

	public function setDoNotRunInRentWeek( $intDoNotRunInRentWeek ) {
		$this->set( 'm_intDoNotRunInRentWeek', CStrings::strToIntDef( $intDoNotRunInRentWeek, NULL, false ) );
	}

	public function getDoNotRunInRentWeek() {
		return $this->m_intDoNotRunInRentWeek;
	}

	public function sqlDoNotRunInRentWeek() {
		return ( true == isset( $this->m_intDoNotRunInRentWeek ) ) ? ( string ) $this->m_intDoNotRunInRentWeek : '0';
	}

	public function setDoNotMonitor( $intDoNotMonitor ) {
		$this->set( 'm_intDoNotMonitor', CStrings::strToIntDef( $intDoNotMonitor, NULL, false ) );
	}

	public function getDoNotMonitor() {
		return $this->m_intDoNotMonitor;
	}

	public function sqlDoNotMonitor() {
		return ( true == isset( $this->m_intDoNotMonitor ) ) ? ( string ) $this->m_intDoNotMonitor : '0';
	}

	public function setIsCritical( $intIsCritical ) {
		$this->set( 'm_intIsCritical', CStrings::strToIntDef( $intIsCritical, NULL, false ) );
	}

	public function getIsCritical() {
		return $this->m_intIsCritical;
	}

	public function sqlIsCritical() {
		return ( true == isset( $this->m_intIsCritical ) ) ? ( string ) $this->m_intIsCritical : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setRunOnTestDb( $boolRunOnTestDb ) {
		$this->set( 'm_boolRunOnTestDb', CStrings::strToBool( $boolRunOnTestDb ) );
	}

	public function getRunOnTestDb() {
		return $this->m_boolRunOnTestDb;
	}

	public function sqlRunOnTestDb() {
		return ( true == isset( $this->m_boolRunOnTestDb ) ) ? '\'' . ( true == ( bool ) $this->m_boolRunOnTestDb ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCentralizedLogging( $boolIsCentralizedLogging ) {
		$this->set( 'm_boolIsCentralizedLogging', CStrings::strToBool( $boolIsCentralizedLogging ) );
	}

	public function getIsCentralizedLogging() {
		return $this->m_boolIsCentralizedLogging;
	}

	public function sqlIsCentralizedLogging() {
		return ( true == isset( $this->m_boolIsCentralizedLogging ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCentralizedLogging ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsNewrelicInstrumentation( $boolIsNewrelicInstrumentation ) {
		$this->set( 'm_boolIsNewrelicInstrumentation', CStrings::strToBool( $boolIsNewrelicInstrumentation ) );
	}

	public function getIsNewrelicInstrumentation() {
		return $this->m_boolIsNewrelicInstrumentation;
	}

	public function sqlIsNewrelicInstrumentation() {
		return ( true == isset( $this->m_boolIsNewrelicInstrumentation ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNewrelicInstrumentation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setRunOnTemplateDb( $boolRunOnTemplateDb ) {
		$this->set( 'm_boolRunOnTemplateDb', CStrings::strToBool( $boolRunOnTemplateDb ) );
	}

	public function getRunOnTemplateDb() {
		return $this->m_boolRunOnTemplateDb;
	}

	public function sqlRunOnTemplateDb() {
		return ( true == isset( $this->m_boolRunOnTemplateDb ) ) ? '\'' . ( true == ( bool ) $this->m_boolRunOnTemplateDb ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, script_type_id, script_executor_type_id, name, description, script_path, log_path, schedule, run_on_specific_databases, parameters, execution_minutes, do_not_run_in_rent_week, do_not_monitor, is_critical, is_published, run_on_test_db, is_centralized_logging, is_newrelic_instrumentation, order_num, completed_on, deleted_on, deleted_by, updated_by, updated_on, created_by, created_on, run_on_template_db, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlScriptTypeId() . ', ' .
						$this->sqlScriptExecutorTypeId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlScriptPath() . ', ' .
						$this->sqlLogPath() . ', ' .
						$this->sqlSchedule() . ', ' .
						$this->sqlRunOnSpecificDatabases() . ', ' .
						$this->sqlParameters() . ', ' .
						$this->sqlExecutionMinutes() . ', ' .
						$this->sqlDoNotRunInRentWeek() . ', ' .
						$this->sqlDoNotMonitor() . ', ' .
						$this->sqlIsCritical() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlRunOnTestDb() . ', ' .
						$this->sqlIsCentralizedLogging() . ', ' .
						$this->sqlIsNewrelicInstrumentation() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlRunOnTemplateDb() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' script_type_id = ' . $this->sqlScriptTypeId(). ',' ; } elseif( true == array_key_exists( 'ScriptTypeId', $this->getChangedColumns() ) ) { $strSql .= ' script_type_id = ' . $this->sqlScriptTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' script_executor_type_id = ' . $this->sqlScriptExecutorTypeId(). ',' ; } elseif( true == array_key_exists( 'ScriptExecutorTypeId', $this->getChangedColumns() ) ) { $strSql .= ' script_executor_type_id = ' . $this->sqlScriptExecutorTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' script_path = ' . $this->sqlScriptPath(). ',' ; } elseif( true == array_key_exists( 'ScriptPath', $this->getChangedColumns() ) ) { $strSql .= ' script_path = ' . $this->sqlScriptPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_path = ' . $this->sqlLogPath(). ',' ; } elseif( true == array_key_exists( 'LogPath', $this->getChangedColumns() ) ) { $strSql .= ' log_path = ' . $this->sqlLogPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' schedule = ' . $this->sqlSchedule(). ',' ; } elseif( true == array_key_exists( 'Schedule', $this->getChangedColumns() ) ) { $strSql .= ' schedule = ' . $this->sqlSchedule() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' run_on_specific_databases = ' . $this->sqlRunOnSpecificDatabases(). ',' ; } elseif( true == array_key_exists( 'RunOnSpecificDatabases', $this->getChangedColumns() ) ) { $strSql .= ' run_on_specific_databases = ' . $this->sqlRunOnSpecificDatabases() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parameters = ' . $this->sqlParameters(). ',' ; } elseif( true == array_key_exists( 'Parameters', $this->getChangedColumns() ) ) { $strSql .= ' parameters = ' . $this->sqlParameters() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' execution_minutes = ' . $this->sqlExecutionMinutes(). ',' ; } elseif( true == array_key_exists( 'ExecutionMinutes', $this->getChangedColumns() ) ) { $strSql .= ' execution_minutes = ' . $this->sqlExecutionMinutes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' do_not_run_in_rent_week = ' . $this->sqlDoNotRunInRentWeek(). ',' ; } elseif( true == array_key_exists( 'DoNotRunInRentWeek', $this->getChangedColumns() ) ) { $strSql .= ' do_not_run_in_rent_week = ' . $this->sqlDoNotRunInRentWeek() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' do_not_monitor = ' . $this->sqlDoNotMonitor(). ',' ; } elseif( true == array_key_exists( 'DoNotMonitor', $this->getChangedColumns() ) ) { $strSql .= ' do_not_monitor = ' . $this->sqlDoNotMonitor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_critical = ' . $this->sqlIsCritical(). ',' ; } elseif( true == array_key_exists( 'IsCritical', $this->getChangedColumns() ) ) { $strSql .= ' is_critical = ' . $this->sqlIsCritical() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' run_on_test_db = ' . $this->sqlRunOnTestDb(). ',' ; } elseif( true == array_key_exists( 'RunOnTestDb', $this->getChangedColumns() ) ) { $strSql .= ' run_on_test_db = ' . $this->sqlRunOnTestDb() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_centralized_logging = ' . $this->sqlIsCentralizedLogging(). ',' ; } elseif( true == array_key_exists( 'IsCentralizedLogging', $this->getChangedColumns() ) ) { $strSql .= ' is_centralized_logging = ' . $this->sqlIsCentralizedLogging() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_newrelic_instrumentation = ' . $this->sqlIsNewrelicInstrumentation(). ',' ; } elseif( true == array_key_exists( 'IsNewrelicInstrumentation', $this->getChangedColumns() ) ) { $strSql .= ' is_newrelic_instrumentation = ' . $this->sqlIsNewrelicInstrumentation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' run_on_template_db = ' . $this->sqlRunOnTemplateDb(). ',' ; } elseif( true == array_key_exists( 'RunOnTemplateDb', $this->getChangedColumns() ) ) { $strSql .= ' run_on_template_db = ' . $this->sqlRunOnTemplateDb() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'script_type_id' => $this->getScriptTypeId(),
			'script_executor_type_id' => $this->getScriptExecutorTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'script_path' => $this->getScriptPath(),
			'log_path' => $this->getLogPath(),
			'schedule' => $this->getSchedule(),
			'run_on_specific_databases' => $this->getRunOnSpecificDatabases(),
			'parameters' => $this->getParameters(),
			'execution_minutes' => $this->getExecutionMinutes(),
			'do_not_run_in_rent_week' => $this->getDoNotRunInRentWeek(),
			'do_not_monitor' => $this->getDoNotMonitor(),
			'is_critical' => $this->getIsCritical(),
			'is_published' => $this->getIsPublished(),
			'run_on_test_db' => $this->getRunOnTestDb(),
			'is_centralized_logging' => $this->getIsCentralizedLogging(),
			'is_newrelic_instrumentation' => $this->getIsNewrelicInstrumentation(),
			'order_num' => $this->getOrderNum(),
			'completed_on' => $this->getCompletedOn(),
			'deleted_on' => $this->getDeletedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'run_on_template_db' => $this->getRunOnTemplateDb(),
			'details' => $this->getDetails()
		);
	}

}
?>