<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CSystemUserAuthenticationLogs
 * Do not add any new functions to this class.
 */

class CBaseSystemUserAuthenticationLogs extends CEosPluralBase {

	/**
	 * @return CSystemUserAuthenticationLog[]
	 */
	public static function fetchSystemUserAuthenticationLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSystemUserAuthenticationLog', $objDatabase );
	}

	/**
	 * @return CSystemUserAuthenticationLog
	 */
	public static function fetchSystemUserAuthenticationLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSystemUserAuthenticationLog', $objDatabase );
	}

	public static function fetchSystemUserAuthenticationLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_user_authentication_logs', $objDatabase );
	}

	public static function fetchSystemUserAuthenticationLogById( $intId, $objDatabase ) {
		return self::fetchSystemUserAuthenticationLog( sprintf( 'SELECT * FROM system_user_authentication_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSystemUserAuthenticationLogsBySystemUserId( $intSystemUserId, $objDatabase ) {
		return self::fetchSystemUserAuthenticationLogs( sprintf( 'SELECT * FROM system_user_authentication_logs WHERE system_user_id = %d', ( int ) $intSystemUserId ), $objDatabase );
	}

}
?>