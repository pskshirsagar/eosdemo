<?php

class CClientDataInstanceOperation extends CBaseClientDataInstanceOperation {

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function setUserName( $strUserName ) {
    	$this->m_strUserName = CStrings::strTrimDef( $strUserName, 200, NULL, true );
    }

    public function getUserName() {
    	return $this->m_strUserName;
    }

    public function sqlUserName() {
    	return ( true == isset( $this->m_strUserName ) ) ? '\'' . addslashes( $this->m_strUserName ) . '\'' : 'NULL';
    }

}
?>