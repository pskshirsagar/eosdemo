<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClientDataInstances
 * Do not add any new functions to this class.
 */

class CClientDataInstances extends CBaseClientDataInstances {

	public static function fetchPaginatedClientDataInstancesByClientId( $intClientId, $objConnectDatabase, $boolIsTemplate, $intPageNo=NULL, $intPageSize=NULL ) {

		$strPageCondition = '';
		$strWhereCondition = '';

		if( true == $boolIsTemplate ) {
			$strWhereCondition = ' AND cta.is_template = 1';
		} elseif( false == $boolIsTemplate ) {
			$strWhereCondition = ' AND cta.is_template = 0';
		}

		if( true != is_null( $intPageNo ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$strPageCondition = ' OFFSET ' . ( int ) $intOffset;
		} else {
			$strPageCondition = '';
		}
		if( true != is_null( $intPageSize ) ) {
			$strPageCondition .= ' LIMIT ' . ( int ) $intPageSize;
		}

		$strSql = 'SELECT
            			*
        			FROM
            			client_data_instances cta
        			WHERE
            			cta.training_client_id IS NOT NULL
			            AND cta.cid = ' . ( int ) $intClientId . '
			            AND cta.deleted_by IS NULL
			            AND cta.deleted_on IS NULL ' . $strWhereCondition . ' ORDER BY cta.id ' . $strPageCondition;

		return fetchData( $strSql, $objConnectDatabase );
	}

	public static function fetchClientDataInstanceByTrainingClientIdByClientId( $intTrainingClientId, $intClientId, $objConnectDatabase ) {

		$strSql = ' SELECT * FROM client_data_instances WHERE training_client_id = ' . ( int ) $intTrainingClientId . ' AND cid = ' . ( int ) $intClientId . ' AND deleted_on IS NULL';
		return self::fetchClientDataInstance( $strSql, $objConnectDatabase );
	}

	public static function fetchClientDataInstanceByParentClientIdByClientId( $intTrainingClientId, $intClientId, $objConnectDatabase ) {

		$strSql = ' SELECT * FROM client_data_instances WHERE parent_client_id = ' . ( int ) $intTrainingClientId . ' AND cid = ' . ( int ) $intClientId . ' AND deleted_on IS NULL';
		return self::fetchClientDataInstances( $strSql, $objConnectDatabase );
	}

	public static function fetchClientDataInstanceForTestCountByParentClientIdByClientId( $intTrainingClientId, $intClientId, $objConnectDatabase ) {

		$strSql = ' SELECT * FROM client_data_instances WHERE parent_client_id = ' . ( int ) $intTrainingClientId . ' AND cid = ' . ( int ) $intClientId . ' AND training_client_id IS NOT NULL AND deleted_on IS NULL';
		return self::fetchClientDataInstances( $strSql, $objConnectDatabase );
	}

	public static function fetchClientDataInstanceByClientId( $intClientId, $objConnectDatabase ) {

		$strSql = 'SELECT
                            cta.*
                        FROM
                            client_data_instances cta
                        WHERE
                            cta.cid = ' . ( int ) $intClientId . '
                            AND cta.parent_client_id = ' . ( int ) $intClientId . '
                            AND cta.deleted_by IS NULL
                            AND cta.deleted_on IS NULL
                            AND cta.training_client_id IS NOT NULL
                            AND cta.is_template = 0';

		return fetchData( $strSql, $objConnectDatabase );
	}

	public static function fetchActiveClientDataInstanceByClientIdByTrainingClientId( $intClientId, $intTrainingClientId, $objDatabase ) {

		$strSql = 'SELECT
						tc.*
    				FROM
    					client_data_instances tc
					WHERE
    					tc.cid = ' . ( int ) $intClientId . '
    					AND tc.training_client_id = ' . ( int ) $intTrainingClientId . '
						AND tc.deleted_on IS NULL
					ORDER BY tc.id ASC';

			return self::fetchClientDataInstance( $strSql, $objDatabase );
	}

	public static function fetchTestClientByTrainingClientId( $intTrainingClientId, $objDatabase ) {

		$strSql = 'SELECT * FROM client_data_instances WHERE training_client_id = ' . ( int ) $intTrainingClientId;

		return self::fetchClientDataInstances( $strSql, $objDatabase );
	}

	public static function fetchParentClientIdByTrainingClientId( $intTrainingClientId, $objConnectDatabase ) {
		$strSql = 'SELECT parent_client_id FROM client_data_instances WHERE training_client_id = ' . ( int ) $intTrainingClientId . ' AND deleted_by IS NULL AND deleted_on IS NULL ';

		$arrintParentId = fetchData( $strSql, $objConnectDatabase );

		if( false == valArr( $arrintParentId ) ) return;

		return $arrintParentId[0]['parent_client_id'];

	}

	public static function fetchClientDataInstanceByTrainingClientId( $intTrainingClientId, $objDatabase ) {
		$strSql = 'SELECT * FROM client_data_instances WHERE training_client_id = ' . ( int ) $intTrainingClientId . ' limit 1';
		return self::fetchClientDataInstance( $strSql, $objDatabase );
	}

	public static function fetchClientDataInstancesbyCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT count(id) FROM client_data_instances WHERE training_client_id IS NOT NULL AND deleted_on IS NULL AND cid = ' . ( int ) $intCid;
		return fetchData( $strSql, $objDatabase )[0]['count'];
	}

	public static function fetchSearchedClientDataInstance( $arrstrFilteredExplodedSearch, $objDatabase ) {

	  	 $strSql = 'SELECT
					  	 cdi.id,
					  	 cdi.cid,
					  	 cdi.source_database_id,
					  	 cdi.destination_database_id,
					  	 cdio.client_data_instance_operation_status_type_id,
					  	 cdiot.name as request_type,
					  	 cdiost.name as status
					FROM
					  	 client_data_instances AS cdi
					  	 JOIN client_data_instance_operations AS cdio ON ( cdi.id = cdio.client_data_instance_id )
					  	 JOIN client_data_instance_operation_status_types AS cdiost ON ( cdiost.id = cdio.client_data_instance_operation_status_type_id )
					  	 JOIN client_data_instance_operation_types as cdiot on( cdio.client_data_instance_operation_type_id = cdiot.id )
					WHERE
					  	 cdi.deleted_by IS NULL
					  	 AND cdi.deleted_on IS NULL
					  	 AND cdi.cid IN ( ' . ( int ) implode( ',', $arrstrFilteredExplodedSearch ) . ')
					ORDER BY
					  	  cdi.id DESC
					LIMIT 15';

		return self::fetchClientDataInstances( $strSql, $objDatabase );
	}

	public static function fetchClientDataInstancesById( $intClientDataInstanceId, $objDatabase ) {

	    $strSql = 'SELECT
						cdi.id,
					  	cdi.cid,
					  	cdi.source_database_id,
					  	cdi.destination_database_id,
						cdi.description,
						cdi.requested_on,
						cdi.requested_by,
						cdio.client_data_instance_operation_status_type_id,
					  	cdiot.name as request_type,
					  	cdiost.name as status
				  FROM client_data_instances AS cdi
				  JOIN client_data_instance_operations AS cdio ON ( cdi.id = cdio.client_data_instance_id )
				  JOIN client_data_instance_operation_status_types AS cdiost ON ( cdiost.id = cdio.client_data_instance_operation_status_type_id )
				  JOIN client_data_instance_operation_types AS cdiot on( cdio.client_data_instance_operation_type_id = cdiot.id )
				  WHERE cdi.id = ' . ( int ) $intClientDataInstanceId;

		return self::fetchClientDataInstance( $strSql, $objDatabase );
	}

	public static function updateDeletedHardwareIds( $arrintHardwareIds, $objDatabase ) {

		$strSql = 'UPDATE client_data_instances SET hardware_id = NULL WHERE hardware_id IN ( ' . implode( ',', $arrintHardwareIds ) . ')';

		fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllActiveClientDataInstances( $objDeployDatabase, $intPageNo = NULL, $intPageSize = NULL, $strOrderByField = NULL, $strOrderByType = NULL ) {

		if( false == is_null( $intPageNo ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit  = ( int ) $intPageSize;
			$strPageCondition = ' OFFSET ' . ( int ) $intOffset . '	LIMIT ' . ( int ) $intLimit;
		} else {
			$strPageCondition = '';
		}

		if( false == is_null( $strOrderByField ) ) {
			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {

			$strOrderBy = ' ORDER BY cid ASC';
		}

		$strSql = 'SELECT
						c.*
					FROM 
						client_data_instances c
					WHERE
						c.training_client_id is NOT NULL AND
				    	c.deleted_on is NULL AND
				    	c.deleted_by is NULL
					' . $strOrderBy . $strPageCondition;
		return self::fetchClientDataInstances( $strSql, $objDeployDatabase );

	}

	public static function fetchAllActiveClientDataInstancesByIsRequestedByCsm( $intPageNo, $intPageSize, $objDeployDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		if( false == is_null( $strOrderByField ) ) {
			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY cid ASC';
		}

		$strSql = 'SELECT
						c.*
					FROM 
						client_data_instances c
						JOIN client_data_instance_operations AS cdio ON ( c.id = cdio.client_data_instance_id )
					WHERE
						c.training_client_id is NOT NULL AND
				    	cdio.is_requested_by_csm = true AND
						cdio.client_data_instance_operation_status_type_id = ' . CClientDataInstanceOperationStatusType::COMPLETED
					 . $strOrderBy . ' OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . $intLimit;
		return self::fetchClientDataInstances( $strSql, $objDeployDatabase );

	}

	public static function fetchAllActiveClientDataInstancesCount( $objDeployDatabase ) {

		$strWhere = 'WHERE
						training_client_id is NOT NULL AND
				    	deleted_on is NULL AND
				    	deleted_by is NULL';

		return self::fetchClientDataInstanceCount( $strWhere, $objDeployDatabase );
	}

}
?>