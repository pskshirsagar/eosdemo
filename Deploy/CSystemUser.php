<?php

use Psi\Libraries\UtilHash\CHash;

class CSystemUser extends CBaseSystemUser {

	const USER_ID_ADMIN					= 2;
	const TOTAL_LOGIN_ATTEMPTS 		 	= 5;
	const LOGIN_ATTEMPT_PERIOD			= 300;	// 5 minutes
	const PASSWORD_ROTATION_DAYS  	 	= 180;
	const PASSWORD_ROTATION_NUMBER 	 	= 3;
	const LOGIN_ATTEMPT_WAITING_TIME 	= 1200;	// 20 minutes

	const TOKEN_EXPIRATION              = 259200; // 3 days -> 3 * 72 * 60 * 60
	const TOKEN_PARTS_SIZE              = 5;
	const TOKEN_DEVICE_ID_INDEX         = 3;
	const TOKEN_TIMESTAMP_INDEX         = 2;
	const TOKEN_ENTRATA_ID_INDEX        = 1;

	const TOKEN_ERROR_EXPIRED_TOKEN                     = 'Resident token has expired.';
	const TOKEN_ERROR_INVALID_TOKEN                     = 'Invalid resident token.';
	const TOKEN_ERROR_INVALID_DEVICE_ID                 = 'Unable to authenticate resident.';
	const TOKEN_ERROR_CANT_FIND_RESIDENT                = 'Invalid username or password.';
	const TOKEN_ERROR_CANT_FIND_RESIDENT_USING_TOKEN    = 'Unable to find resident with current resident token.';

	protected $m_boolIsLoggedIn;
	protected $m_intSystemUserAuthenticationLogId;

	protected $m_strEncryptedPassword;
	protected $m_strUnencryptedPassword;

	/**
	 * Create Functions
	 *
	 */

	public function createUserAuthenticationLog() {

		$objSystemUserAuthenticationLog = new CSystemUserAuthenticationLog();
		$objSystemUserAuthenticationLog->setSystemUserId( $this->getId() );
		$objSystemUserAuthenticationLog->setLoginDatetime( 'NOW()' );
		$objSystemUserAuthenticationLog->setIpAddress( $_SERVER['REMOTE_ADDR'] );

		return $objSystemUserAuthenticationLog;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setIsLoggedIn( $boolIsLoggedIn ) {
		$this->m_boolIsLoggedIn = ( bool ) $boolIsLoggedIn;
	}

	public function setUnencryptedPassword( $strUnencryptedPassword ) {

		$this->m_strUnencryptedPassword = trim( $strUnencryptedPassword );
		$this->m_strPasswordEncrypted 	= CHash::createService()->hashUserOld( trim( $strUnencryptedPassword ) );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['unencrypted_password'] ) ) $this->setUnencryptedPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unencrypted_password'] ) : $arrmixValues['unencrypted_password'] );
		return;
	}

	public function setSystemUserAuthenticationLogId( $intSystemUserAuthenticationLogId ) {
		$this->m_intSystemUserAuthenticationLogId = ( int ) $intSystemUserAuthenticationLogId;
	}

	public function setLoginAttemptData( $boolIsLoggedIn = false ) {

		if( true == $boolIsLoggedIn ) {
			$this->setLoginAttemptCount( 0 );
		} elseif( time() > ( strtotime( date( 'c', strtotime( $this->getLastLoginAttemptOn() ) ) ) + self::LOGIN_ATTEMPT_WAITING_TIME ) && 0 < $this->getLoginAttemptCount() ) {
			$this->setLoginAttemptCount( 1 );
		} else {
			$this->setLoginAttemptCount( $this->getLoginAttemptCount() + 1 );
		}

		$this->setLastLoginAttemptOn( date( 'c', time() ) );

	}

	public static function getEncryptedPassword( $strClearTextPassword ) {
		return CHash::createService()->hashCustomer( $strClearTextPassword );
	}

	public static function encryptResidentToken( $intId, $intTimestamp, $strDeviceId ) {
		return CEncryption::encryptText( rand( 1000, getrandmax() ) . '_' . $intId . '_' . ( $intTimestamp + self::TOKEN_EXPIRATION ) . '_' . $strDeviceId . '_' . rand( 1000, getrandmax() ), CONFIG_KEY_LOGIN_PASSWORD );
	}

	public static function decryptResidentToken( $strEncryptedResidentToken ) {
		$strResidentToken = CEncryption::decryptText( $strEncryptedResidentToken, CONFIG_KEY_LOGIN_PASSWORD );
		return explode( '_', $strResidentToken );
	}

	/**
	 * Get Functions
	 *
	 */

	public function getIsLoggedIn() {
		return $this->m_boolIsLoggedIn;
	}

	public function getSystemUserAuthenticationLogId() {
		return $this->m_intSystemUserAuthenticationLogId;
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valUsername() {
		$boolIsValid = true;

		if( false == valStr( $this->getUsername() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Username is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPasswordEncrypted() {
		$boolIsValid = true;

		if( false == valStr( $this->getPasswordEncrypted() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( false == valStr( $this->getEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email', 'Email is required.' ) );
		}

		if( !filter_var( trim( $this->getEmailAddress() ), FILTER_VALIDATE_EMAIL ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email', 'Enter valid Email address.' ) );
		}

		return $boolIsValid;
	}

	public function valLogin() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strUsername ) || false == valStr( $this->m_strUnencryptedPassword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unencrypted_password', 'Username and/or password is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPassword() {

		$boolIsValid = true;

		if( false == isset( $this->m_strUnencryptedPassword ) || 0 == strlen( $this->m_strUnencryptedPassword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password is required.' ) );
		}

		if( true == valStr( $this->m_strUnencryptedPassword ) && 8 > strlen( $this->m_strUnencryptedPassword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password cannot be less than 8 characters.' ) );

		} elseif( true == valStr( $this->m_strUnencryptedPassword ) && false == preg_match( '/(?=^.{8,}$)(?=.*[\d|!@#$%^&*]+)(?=.*[A-Z]).*$/', $this->m_strUnencryptedPassword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password must contain at least 1 capital letter AND 1 number or special character.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valUsername();
				$boolIsValid &= $this->valPasswordEncrypted();
				$boolIsValid &= $this->valEmailAddress();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_login':
				$boolIsValid = $this->valLogin();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function logout() {

		$this->setIsLoggedIn( false );
		return true;
	}

	public function mapUserData( $objUser ) {

		$this->setId( $objUser->getId() );
		$this->setUsername( $objUser->getUsername() );
		$this->setPasswordEncrypted( $objUser->getPasswordEncrypted() );
		$this->setPreferredName( $objUser->getPreferredName() );
		$this->setPhoneNumber( $objUser->getPhoneNumber() );
		$this->setEmailAddress( $objUser->getEmailAddress() );
		$this->setIsAdministrator( $objUser->getIsAdministrator() );
		$this->setIsDisabled( $objUser->getIsDisabled() );
		$this->setIsActiveDirectoryUser( $objUser->getIsActiveDirectory() );
		$this->setUpdatedBy( $objUser->getUpdatedBy() );
		$this->setUpdatedOn( $objUser->getUpdatedOn() );
		$this->setCreatedBy( $objUser->getCreatedBy() );
		$this->setCreatedOn( $objUser->getCreatedOn() );
	}

	public function systemLogin( $objDeployDatabase ) {

		$objSystemUser = CSystemUsers::fetchActiveSystemUserByUsername( $this->getUsername(), $objDeployDatabase );

		if( false == valObj( $objSystemUser, 'CSystemUser' ) ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'Invalid username and/or password', 304 ) );
			return false;
		}

		$objSystemUser	= $this->authenticateUser( $objSystemUser );

		if( true == valObj( $objSystemUser, 'CSystemUser' ) ) {

			$objSystemUser->setIsLoggedIn( true );
			$objSystemUser->logSystemUserAuthentication( $objDeployDatabase );
			$objSystemUser->setLoginAttemptData( true );
			$objSystemUser->setLastLogin( 'NOW()' );
			$objSystemUser->setLastAccess( 'NOW()' );

			if( false == $objSystemUser->update( $objSystemUser->getId(), $objDeployDatabase ) ) {
				trigger_error( 'User record failed to update for System User ID: ' . $objSystemUser->getId(), E_USER_WARNING );
			}

			return $objSystemUser;
		}

		return false;
	}

	public function logSystemUserAuthentication( $objDeployDatabase ) {

		if( false == is_null( $this->getSystemUserAuthenticationLogId() ) && true == is_numeric( $this->getSystemUserAuthenticationLogId() ) ) {

			$objSystemUserAuthenticationLog = CSystemUserAuthenticationLogs::fetchSystemUserAuthenticationLogByIdBySystemUserId( $this->getSystemUserAuthenticationLogId(), $this->getId(), $objDeployDatabase );

			if( true == valObj( $objSystemUserAuthenticationLog, 'CSystemUserAuthenticationLog' ) ) {
				$objSystemUserAuthenticationLog->setLogoutDateTime( 'NOW()' );
			}
		} else {
			$objSystemUserAuthenticationLog = $this->createUserAuthenticationLog();
		}

		if( false == is_null( $objSystemUserAuthenticationLog ) ) {

			if( false == $objSystemUserAuthenticationLog->validate( VALIDATE_INSERT ) || false == $objSystemUserAuthenticationLog->insertOrUpdate( $this->getId(), $objDeployDatabase ) ) {
				trigger_error( 'Failed to insert or update authentication log for the User : ' . $this->getId(), E_USER_WARNING );
			}
			$this->setSystemUserAuthenticationLogId( $objSystemUserAuthenticationLog->getId() );
		}

		return true;
	}

	public function authenticateUser( $objSystemUser ) {

		if( 'stage' == CONFIG_ENVIRONMENT || true == extension_loaded( 'sodium' ) ) {
			$objCrypto = \Psi\Libraries\Cryptography\CCrypto::create();
			if( 'production' == CONFIG_ENVIRONMENT && true == valObj( $objSystemUser, 'CSystemUser' ) && true == $objSystemUser->getIsActiveDirectoryUser() ) {
				require_once( PATH_APPLICATION_CLIENT_ADMIN . 'Library/Ldap/CClientAdminLdap.class.php' );

				$objClientAdminLdap = new CClientAdminLdap();
				$boolIsValid        = $objClientAdminLdap->authenticate( $this->getUsername(), $this->m_strUnencryptedPassword );

				if( true == $boolIsValid ) {

					// Login Success, Encrypt and update password
					if( false == $objCrypto->verifyHash( $this->m_strUnencryptedPassword, $objSystemUser->getPasswordEncrypted() ) ) {
						$objSystemUser->setPasswordEncrypted( $objCrypto->hash( trim( $this->m_strUnencryptedPassword ) ) );
					}

				} elseif( -1 == $objClientAdminLdap->getErrorNumber() && false == $objCrypto->verifyHash( $this->m_strUnencryptedPassword, $objSystemUser->getPasswordEncrypted() ) ) {
					// Unable to connect Active Directory server, so check password from database
					$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'Invalid username and/or password', 304 ) );

					return false;
				} elseif( -1 != $objClientAdminLdap->getErrorNumber() ) {
					// Invalid credentials
					$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'Invalid username and/or password', 304 ) );

					return false;
				}
			} elseif( true == valObj( $objSystemUser, 'CSystemUser' ) && false == $objCrypto->verifyHash( $this->m_strUnencryptedPassword, $objSystemUser->getPasswordEncrypted() ) ) {
				$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'Invalid username and/or password', 304 ) );
				return false;
			}

			if( \Psi\Libraries\Cryptography\CCryptoProviderFactory::CRYPTO_PROVIDER_MCRYPT == $objCrypto->getProviderNameForHash( $objSystemUser->getPasswordEncrypted() ) ) {
				$objSystemUser->setPasswordEncrypted( $objCrypto->hash( trim( $this->m_strUnencryptedPassword ) ) );
			}

			return $objSystemUser;
		} else {
			if( 'production' == CONFIG_ENVIRONMENT && true == valObj( $objSystemUser, 'CSystemUser' ) && true == $objSystemUser->getIsActiveDirectoryUser() ) {
				require_once( PATH_APPLICATION_CLIENT_ADMIN . 'Library/Ldap/CClientAdminLdap.class.php' );

				$objClientAdminLdap	= new CClientAdminLdap();
				$boolIsValid		= $objClientAdminLdap->authenticate( $this->getUsername(), $this->m_strUnencryptedPassword );

				if( true == $boolIsValid ) {

					// Login Success, Encrypt and update password
					$objSystemUser->setPasswordEncrypted( CHash::createService()->hashUserOld( trim( $this->m_strUnencryptedPassword ) ) );

				} elseif( -1 == $objClientAdminLdap->getErrorNumber() && $this->getPasswordEncrypted() != $objSystemUser->getPasswordEncrypted() ) {
					// Unable to connect Active Directory server, so check password from database
					$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'Invalid username and/or password', 304 ) );
					return false;
				} elseif( -1 != $objClientAdminLdap->getErrorNumber() ) {
					// Invalid credentials
					$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'Invalid username and/or password', 304 ) );
					return false;
				}
			} elseif( true == valObj( $objSystemUser, 'CSystemUser' ) ) {
				if( $this->getPasswordEncrypted() != $objSystemUser->getPasswordEncrypted() ) {
					$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'Invalid username and/or password', 304 ) );
					return false;
				}
			}

			return $objSystemUser;
		}
	}

}
?>