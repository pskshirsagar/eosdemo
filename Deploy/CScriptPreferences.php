<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CScriptPreferences
 * Do not add any new functions to this class.
 */

class CScriptPreferences extends CBaseScriptPreferences {

	public static function fetchKeyedScriptPreferencesByScriptId( $intScriptId, $objDatabase ) {

		$strSql = 'SELECT * FROM script_preferences WHERE script_id = ' . ( int ) $intScriptId . 'and is_custom = 0';

		$arrobjScriptPreferences = self::fetchScriptPreferences( $strSql, $objDatabase );

		if( true == valArr( $arrobjScriptPreferences ) ) {
			return rekeyObjects( 'Key', $arrobjScriptPreferences );
		} else {
			return NULL;
		}
	}

	public static function fetchKeyedCustomScriptPreferencesByScriptId( $intScriptId, $objDatabase ) {

		$strSql = 'SELECT * FROM script_preferences WHERE script_id = ' . ( int ) $intScriptId . 'and is_custom = 1';

		$arrobjScriptPreferences = self::fetchScriptPreferences( $strSql, $objDatabase );

		if( true == valArr( $arrobjScriptPreferences ) ) {
			return rekeyObjects( 'Key', $arrobjScriptPreferences );
		} else {
			return NULL;
		}
	}

    public static function fetchCustomScriptPreferencesByScriptId( $arrintScriptPreferenceId, $objDatabase ) {

    	$strSql = 'SELECT * FROM script_preferences WHERE script_id = ' . $arrintScriptPreferenceId . 'and is_custom = 1 ORDER BY created_on desc';
    	return self::fetchScriptPreferences( $strSql, $objDatabase );
    }

    public static function fetchConflictingScriptPreferencesKey( $strScriptPreferenceKey, $intScriptId, $intId = NULL, $objDeployDatabase=NULL ) {

		$strWhere = ' WHERE key = \'' . addslashes( $strScriptPreferenceKey ) . '\' AND script_id = ' . ( int ) $intScriptId;

		if( true == isset( $intId ) ) {
			$strWhere .= ' AND id !=' . ( int ) $intId;
		}

		return self::fetchScriptPreferenceCount( $strWhere, $objDeployDatabase );
	}

	public static function fetchKeyedAllScriptPreferencesByScriptId( $intScriptId, $objDatabase ) {

		$strSql = 'SELECT * FROM script_preferences WHERE script_id = ' . ( int ) $intScriptId;

		$arrobjScriptPreferences = self::fetchScriptPreferences( $strSql, $objDatabase );

		if( true == valArr( $arrobjScriptPreferences ) ) {
			return rekeyObjects( 'Key', $arrobjScriptPreferences );
		} else {
			return NULL;
		}
	}

	public static function fetchScriptPreferenceByKeyByScriptId( $strKey, $intScriptId, $objDatabase ) {

		if( false == valStr( $strKey ) || false == valId( $intScriptId ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM script_preferences WHERE script_id = ' . ( int ) $intScriptId . ' AND key = \'' . $strKey . '\'';

		return self::fetchScriptPreference( $strSql, $objDatabase );

	}

	public static function fetchScriptPreferencesByKeyByScriptId( $strKey, $intScriptId, $objDatabase ) {
		if( false == valStr( $strKey ) || false == valId( $intScriptId ) ) {
			return NULL;
		}
		$strSql = 'SELECT * FROM script_preferences WHERE script_id = ' . ( int ) $intScriptId . ' AND key = \'' . $strKey . '\'';

		return self::fetchScriptPreferences( $strSql, $objDatabase );
	}

}
?>