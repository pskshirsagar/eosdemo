<?php

class CScriptExecutionCommand extends CBaseScriptExecutionCommand {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScriptId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScriptDatabaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScriptSchedule() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScriptCommand() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>