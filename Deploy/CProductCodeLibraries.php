<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CProductCodeLibraries
 * Do not add any new functions to this class.
 */

class CProductCodeLibraries extends CBaseProductCodeLibraries {

	public static function fetchProductCodeLibraries( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CProductCodeLibrary::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchProductCodeLibrary( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CProductCodeLibrary::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>