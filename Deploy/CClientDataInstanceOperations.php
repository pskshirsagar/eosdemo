<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClientDataInstanceOperations
 * Do not add any new functions to this class.
 */
class CClientDataInstanceOperations extends CBaseClientDataInstanceOperations {

	public static function fetchPaginatedClientDataInstanceOperationByCid( $arrintCids, $intStatusType, $objDatabase, $intPageNo=NULL, $intPageSize=NULL ) {

		$strPageCondition = '';
		$strCids = implode( ',', $arrintCids );

		if( true != is_null( $intPageNo ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$strPageCondition = ' OFFSET ' . ( int ) $intOffset;
		} else {
			$strPageCondition = '';
		}
		if( true != is_null( $intPageSize ) ) {
			$strPageCondition .= ' LIMIT ' . ( int ) $intPageSize;
		}
		$arrintStatusType = ( CClientDataInstanceOperationStatusType::PENDING == $intStatusType ) ? [1,2,3] : array( $intStatusType );

		$strSql = 'SELECT
    					co.*,
						c.cid,
    					c.parent_client_id,
						c.training_client_id,
					    c.is_template,
					    c.description,
					    c.expires_on,
    					c.requested_by,
    					c.property_ids
					FROM
					    client_data_instance_operations co
					    LEFT JOIN client_data_instances c ON ( c.id = co.client_data_instance_id )
					WHERE
					    c.cid IN ( ' . $strCids . ' )
					    AND co.client_data_instance_operation_status_type_id IN( ' . implode( ',', $arrintStatusType ) . ' )
					ORDER BY
					    co.created_on DESC ' . $strPageCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientDataInstanceOperationRequestById( $intId, $objDatabase ) {

		$strSql = 'SELECT * FROM client_data_instance_operations WHERE id = ' . ( int ) $intId . ' AND deleted_on IS NULL';

		return self::fetchClientDataInstanceOperation( $strSql, $objDatabase );
	}

	public static function fetchCustomClientDataInstanceOperation( $intStatusType, $strOrderByField, $strOrderByType, $objDatabase, $intPageNo=NULL, $intPageSize=NULL, $boolIsMigrationRequest = false, $boolIsMigrationCount = false, $arrmixFilter ) {
		if( false == $strOrderByField || false == $strOrderByType ) {
			$strOrderByField = 'co.updated_on DESC';
		} else {
			$strOrderByField = ( CClientDataInstanceOperationStatusType::COMPLETED == $intStatusType ) ? ( ( 'requested_on' == $strOrderByField ) ? 'co.processed_on' : $strOrderByField ) : $strOrderByField;
			$strOrderByField = ( 'reset_frequency' == $strOrderByField ) ? 'r.name' : $strOrderByField;
			$strOrderByField = $strOrderByField . ' ' . $strOrderByType;
		}

		if( true != is_null( $intPageNo ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$strPageCondition = ' OFFSET ' . ( int ) $intOffset;
		} else {
			$strPageCondition = '';
		}

		if( true != is_null( $intPageSize ) ) {
			$strPageCondition .= ' LIMIT ' . ( int ) $intPageSize;
		}
		$arrintStatusTypes = ( CClientDataInstanceOperationStatusType::PENDING == $intStatusType ) ? [ 1, 2, 3 ] : [ $intStatusType ];

		$strSql = 'SELECT
	                    co.*,
	                    c.parent_client_id,
	                    c.is_template,
	                    c.description,
	                    c.expires_on,
	                    c.requested_by,
	                    c.requested_on,
						c.cid,
						c.training_client_id,
						c.destination_database_id,
						c.reset_on,
						c.reset_frequency_type_id,
						c.created_on AS instance_created_on,
						c.updated_on AS instance_updated_on,
						c.property_ids,
						co.created_by,
						co.created_on,
						h.name as hardware_name,
						CASE WHEN ' . CResetFrequencyType::BIWEEKLY . ' = r.id THEN 
							\'Every Other Week\'
							ELSE r.name
						END AS reset_frequency
	                FROM
	                    client_data_instance_operations co
	                    LEFT JOIN client_data_instances c ON ( c.id = co.client_data_instance_id )
						LEFT JOIN hardwares h ON ( h.id = c.hardware_id )
						LEFT JOIN reset_frequency_types r ON ( r.id = c.reset_frequency_type_id )';
		$strFilter = '';

		if( true == valArr( $arrmixFilter ) ) {
			if( false == empty( $arrmixFilter['client_id'] ) ) {
				if( true == is_numeric( $arrmixFilter['client_id'] ) ) {
					$strFilter .= ' AND c.cid = ' . $arrmixFilter['client_id'];
				}
			}
			if( true == isset( $arrmixFilter['using_company'] ) ) {
				$strFilter .= ' AND c.parent_client_id IN ( ' . $arrmixFilter['using_company'] . ' )';
			}
			if( true == isset( $arrmixFilter['operating_on'] ) ) {
				if( true == $boolIsMigrationRequest ) {
					$strFilter .= ' AND c.parent_client_id IN ( ' . $arrmixFilter['operating_on'] . ' )';
				} else {
					$strFilter .= ' AND c.training_client_id IN ( ' . $arrmixFilter['operating_on'] . ' )';
				}
			}
			if( false == empty( $arrmixFilter['request_type'] ) ) {
				$strFilter .= ' AND co.client_data_instance_operation_type_id = ' . $arrmixFilter['request_type'];
			}
			if( false == empty( $arrmixFilter['created_on'] ) ) {
				$strDeliveryDate = date( 'Y-m-d', strtotime( $arrmixFilter['created_on'] ) );
				if( CClientDataInstanceOperationStatusType::COMPLETED == $intStatusType ) {
					$strFilter .= ' AND co.processed_on::date = \'' . $strDeliveryDate . '\' ';
				} else {
					$strFilter .= ' AND co.created_on::date = \'' . $strDeliveryDate . '\' ';
				}
			}
			if( true == isset( $arrmixFilter['requested_by'] ) ) {
				$strFilter .= ' AND co.created_by IN( ' . $arrmixFilter['requested_by'] . ' )';
			}
			if( false == empty( $arrmixFilter['not_in_request_type'] ) ) {
				$strFilter .= ' AND co.client_data_instance_operation_type_id NOT IN ( ' . $arrmixFilter['not_in_request_type'] . ' )';
			}
		}

		if( true == $boolIsMigrationRequest ) {
			$strSql .= ' WHERE co.client_data_instance_operation_type_id = ' . CClientDataInstanceOperationType::MIGRATE . $strFilter;
			$strSql .= ( true == $boolIsMigrationCount ) ? ' AND co.client_data_instance_operation_status_type_id IN( ' . implode( ',', $arrintStatusTypes ) . ' )' : ' ';
			$strSql .= ' ORDER BY ' . $strOrderByField . ', co.created_on' . ' ' . $strPageCondition;

		} else {
			$strSql .= ' WHERE co.client_data_instance_operation_status_type_id IN( ' . implode( ',', $arrintStatusTypes ) . ' ) AND co.client_data_instance_operation_type_id != ' . CClientDataInstanceOperationType::MIGRATE . ' AND co.details ->> \'is_future_request\' is NULL ' . $strFilter . ' ORDER BY ' . $strOrderByField . ' ' . $strPageCondition;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnprocessedClientAccountRequest( $objDatabase, $strServerIp = NULL ) {

		$strSql = 'SELECT
                        cta.id AS client_data_instance_id,
                        cta.cid,
                        cta.parent_client_id,
						cta.training_client_id,
                        cta.reset_frequency_type_id,
						cta.destination_database_id,
                        cta.is_template,
                        cta.description,
                        cta.expires_on,
                        cta.reset_on,
                        cta.requested_by,
                        cta.requested_on,
						cta.created_by as request_created_by,
		                cta.stakeholders,
						h.ip_address,
                        ctar.*
                  FROM
                        client_data_instance_operations ctar
                        LEFT JOIN client_data_instances cta ON ( cta.id = ctar.client_data_instance_id )
				        LEFT JOIN hardwares h on( h.id = cta.hardware_id )
                  WHERE
                        ctar.completed_on IS NULL
                        AND ctar.processed_on IS NULL
                        AND ctar.deleted_on IS NULL
						AND ctar.approved_by IS NOT NULL
						AND ctar.approved_on IS NOT NULL';

		$strSql .= ( true == $strServerIp ) ? ' AND h.ip_address = \'' . $strServerIp . '\' ORDER BY cta.id limit 1' : ' AND h.ip_address IS NULL ORDER BY cta.id limit 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientDataInstanceOperationByRequestTypeByClientDataInstanceId( $intRequestTypeId, $intClientDataInstanceId, $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    client_data_instance_operations c
					WHERE
					    c.client_data_instance_id = ' . ( int ) $intClientDataInstanceId . '
					    AND c.client_data_instance_operation_type_id = ' . ( int ) $intRequestTypeId . '
					    AND c.processed_on IS NULL
					    AND c.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientDataInstanceOperationsByCidByRequestTypeId( $intRequestTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    co.*,
					    c.cid,
					    c.parent_client_id,
					    c.training_client_id,
					    c.is_template,
					    c.description,
					    c.expires_on,
					    c.requested_by
					FROM
					    client_data_instance_operations co
					    LEFT JOIN client_data_instances c ON ( c.id = co.client_data_instance_id )
					WHERE
					    c.cid = ' . ( int ) $intCid . '
					    AND co.client_data_instance_operation_status_type_id IN ( ' . CClientDataInstanceOperationStatusType::PENDING . ',' . CClientDataInstanceOperationStatusType::APPROVED . ',' . CClientDataInstanceOperationStatusType::IN_PROCESS . ' )
					    AND co.client_data_instance_operation_type_id = ' . ( int ) $intRequestTypeId . '
					ORDER BY
					    co.approved_by ASC limit 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomClientDataInstanceOperationById( $intId, $objDatabase ) {

		$strSql = 'SELECT * FROM client_data_instance_operations WHERE id = ' . ( int ) $intId . ' AND deleted_on IS NULL';
		return self::fetchClientDataInstanceOperation( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedMigrationRequests( $intPageNo, $intPageSize, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		$strSql = 'SELECT
	                    co.*,
	                    c.parent_client_id,
	                    c.is_template,
	                    c.description,
	                    c.expires_on,
	                    c. requested_by,
	                    c.requested_on,
						c.cid,
						c.training_client_id,
						c.destination_database_id,
	                    c.source_database_id,
						h.name as hardware_name
	                FROM
	                    client_data_instance_operations co
	                    LEFT JOIN client_data_instances c ON ( c.id = co.client_data_instance_id )
						LEFT JOIN hardwares h ON ( h.id = c.hardware_id )
	                WHERE
	                    co.client_data_instance_operation_type_id = ' . CClientDataInstanceOperationType::MIGRATE . '
	                ORDER BY c.id desc';

		$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMigrationRequestsCount( $objDatabase ) {

		$strSql = 'SELECT
						count( co.id )
		 			FROM
						client_data_instance_operations co
		 			WHERE
						co.client_data_instance_operation_type_id = ' . CClientDataInstanceOperationType::MIGRATE;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return $arrintResponse[0]['count'];
	}

	public static function fetchPendingMigrationRequestscount( $objDatabase ) {

		$strSql = 'SELECT
						 count(id)
					FROM
						 client_data_instance_operations
					WHERE
						client_data_instance_operation_status_type_id IN( ' . CClientDataInstanceOperationStatusType::PENDING . ', ' . CClientDataInstanceOperationStatusType::APPROVED . ', ' . CClientDataInstanceOperationStatusType::IN_PROCESS . ' )
						AND client_data_instance_operation_type_id =' . CClientDataInstanceOperationType::MIGRATE . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return $arrintResponse[0]['count'];
	}

	public static function fetchCustomClientDataInstanceOperationByClientDataInstanceId( $intClientDataInstanceId, $objDatabase ) {

		$strSql = 'SELECT
					    c.*,
					    cta.id AS client_data_instance_id,
                        cta.cid,
                        cta.parent_client_id,
						cta.training_client_id,
                        cta.reset_frequency_type_id,
						cta.destination_database_id,
                        cta.is_template,
                        cta.description,
                        cta.expires_on,
                        cta.reset_on,
                        cta.requested_by,
                        cta.requested_on,
						cta.created_by as request_created_by,
		                cta.stakeholders
					FROM
					    client_data_instance_operations c
					    LEFT JOIN client_data_instances cta ON ( cta.id = c.client_data_instance_id )
					WHERE
					    c.client_data_instance_id = ' . ( int ) $intClientDataInstanceId . ' 
					    ORDER BY c.id DESC LIMIT 1';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientDataInstanceOperationByClientDataInstanceIds( $arrintClientDataInstanceIds, $objDatabase ) {

		$strSql = 'SELECT
					    c.*
					    
					FROM
					    client_data_instance_operations c
					WHERE
						c.client_data_instance_operation_type_id = ' . CClientDataInstanceOperationType::DELETE . '
                        AND c.client_data_instance_operation_status_type_id NOT IN( ' . CClientDataInstanceOperationStatusType::CANCELLED . ' )
                        AND c.is_requested_by_csm = true
                        AND c.client_data_instance_id IN( ' . implode( ',', $arrintClientDataInstanceIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientDataInstanceOperationByIsRequestedByCsm( $objDatabase ) {

		$strSql = 'SELECT 
						* 
					FROM 
						client_data_instance_operations
					WHERE 
						is_requested_by_csm IS NOT NULL 
						AND client_data_instance_operation_status_type_id = ' . CClientDataInstanceOperationStatusType::COMPLETED;

		return self::fetchClientDataInstanceOperations( $strSql, $objDatabase );
	}

}
?>
