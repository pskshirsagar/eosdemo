<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CDatabaseScriptReleases
 * Do not add any new functions to this class.
 */

class CDatabaseScriptReleases extends CBaseDatabaseScriptReleases {

	public static function fetchDatabaseScriptReleasesByDeploymentId( $intDeploymentId, $objDatabase ) {
		$strSql = 'SELECT * FROM database_script_releases WHERE deployment_id = ' . ( int ) $intDeploymentId . ' ORDER BY failed_on DESC';

		return self::fetchDatabaseScriptReleases( $strSql, $objDatabase );
	}

	public static function fetchDatabaseScriptReleasesByDeploymentIdByDatabaseId( $intDeploymentId, $intDatabaseId, $objDatabase ) {
		$strSql = 'SELECT * FROM database_script_releases WHERE deployment_id = ' . ( int ) $intDeploymentId . ' AND database_id = ' . ( int ) $intDatabaseId . ' ORDER BY id desc';

		return self::fetchDatabaseScriptReleases( $strSql, $objDatabase );
	}

	public static function fetchDatabaseScriptReleaseByDeploymentIdByDatabaseId( $intDeploymentId, $intDatabaseId, $objDatabase ) {

		$strSql 	= 'SELECT
    						dsr.*
    					 FROM
    					 	database_script_releases dsr
    					 WHERE
    					 	dsr.deployment_id = ' . ( int ) $intDeploymentId . '
    					 	AND dsr.database_id = ' . ( int ) $intDatabaseId;

		return self::fetchDatabaseScriptRelease( $strSql, $objDatabase );
	}

	public static function fetchTaskReleaseIdAndTaskReleaseIdByDeploymentIds( $arrintDeploymentIds, $objDatabase ) {
		$strSql = 'SELECT
    					DISTINCT task_release_id,
    					deployment_id,
    					details
					FROM
    					database_script_releases
					WHERE
    					deployment_id IN ( ' . implode( ',', $arrintDeploymentIds ) . ' ) GROUP BY task_release_id, deployment_id, details';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDatabaseScriptReleasesByDeploymentTypeIdsByTaskReleaseId( $arrintDeploymentTypeIds, $intTaskReleaseId, $objDatabase ) {
		$strSql = 'SELECT
						dsr.*
					FROM
						database_script_releases dsr JOIN deployments d ON ( d.id = dsr.deployment_id )
					WHERE
						d.deployment_type_id IN (  ' . implode( ',', $arrintDeploymentTypeIds ) . ' )
						AND dsr.task_release_id = ' . ( int ) $intTaskReleaseId .
						' ORDER BY dsr.id ASC';

		return self::fetchDatabaseScriptReleases( $strSql, $objDatabase );
	}

	public static function fetchDatabaseScriptReleasesDetailsByDatabaseScriptReleaseIds( $arrintDatabaseScriptReleaseIds, $objDatabase ) {

		if( false == valArr( $arrintDatabaseScriptReleaseIds ) ) return NULL;

		$strCondition = 'WHERE dsr.id IN( ' . implode( ',', $arrintDatabaseScriptReleaseIds ) . ' )';

		$strSql = 'SELECT
					    dsr.id AS release_id,
					    dsr.database_id,
					    dsb.id AS batch_id,
					    dsb.script_sql,
					    dsb.svn_file_ids,
					    dsb.order_num,
					    dsr.details
					FROM
					    database_script_releases dsr
					    JOIN database_script_batches dsb ON ( dsr.database_script_batch_id = dsb.id )
					' . $strCondition . '
					ORDER BY
					    dsr.id , dsb.order_num ASC;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDatabaseScriptReleaseByDeploymentIdByDatabaseIds( $intDeploymentId, $arrintDatabaseIds, $objDatabase ) {

		if( false == valArr( $arrintDatabaseIds ) ) return NULL;

	   	$strSql 	= 'SELECT
    						*
    					 FROM
    					 	database_script_releases
    					 WHERE
	   						deployment_id = ' . ( int ) $intDeploymentId . ' AND
    						database_id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' ) 
    					 ORDER BY database_id';

		return self::fetchDatabaseScriptReleases( $strSql, $objDatabase );
	}

	public static function fetchDatabaseScriptReleasesByDatabaseBatchIds( $strDatabaseBatchIds, $objDatabase ) {

		if( false == valStr( $strDatabaseBatchIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						dsr.*
					FROM
						database_script_releases dsr
					WHERE
						database_script_batch_id IN ( ' . $strDatabaseBatchIds . ')';

		return self::fetchDatabaseScriptReleases( $strSql, $objDatabase );
	}

	public static function fetchUnCompletedDatabaseScriptReleasesByDeploymentId( $intDeploymentId, $objDatabase ) {

		if( true == is_null( $intDeploymentId ) ) return NULL;

		$strSql = 'SELECT
						id,
						database_script_batch_id,
    					deployment_id
					FROM
						database_script_releases
					WHERE
						deployment_id = ' . ( int ) $intDeploymentId . '
						AND end_datetime IS NULL';

		return self::fetchDatabaseScriptReleases( $strSql, $objDatabase );
	}

}
?>