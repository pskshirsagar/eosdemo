<?php

class CSystemUserAuthenticationLog extends CBaseSystemUserAuthenticationLog {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	// other function

	public function calculateTimeSpent( $intLogOutTime, $intLoginTime ) {

		$intdiff		= abs( $intLogOutTime - $intLoginTime );
		$inthours 		= floor( $intdiff / ( 60 * 60 ) );
		$intmins 		= floor( ( $intdiff - ( $inthours * 60 * 60 ) ) / ( 60 ) );
		$intsecs 		= floor( ( $intdiff - ( ( $inthours * 60 * 60 ) + ( $intmins * 60 ) ) ) );
		$intTotalTime 	= sprintf( '%02d', $inthours ) . ':' . sprintf( '%02d', $intmins ) . ':' . sprintf( '%02d', $intsecs );

		return $intTotalTime;
	}

}
?>