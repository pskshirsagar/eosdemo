<?php

class CCodeLibrary extends CBaseCodeLibrary {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLibraryName() {
		$boolIsValid = true;
		if( true == is_null( $this->getLibraryName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'library_name', 'Library Name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valCurrentVersion() {
		$boolIsValid = true;
		if( true == is_null( $this->getCurrentVersion() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'library_name', 'Current Version is required.' ) );
		}
		return $boolIsValid;
	}

	public function valAvailableVersion() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsModified() {
		$boolIsValid = true;
		if( true == is_null( $this->getIsModified() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_modified', 'Is Modified is required.' ) );
		}
		return $boolIsValid;
	}

	public function valLicence() {
		$boolIsValid = true;
		if( true == is_null( $this->getLicence() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'licence', 'Licence is required.' ) );
		}
		return $boolIsValid;
	}

	public function valLicenceTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getLicenceTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'licence_type_id', 'Licence type is required.' ) );
		}
		return $boolIsValid;
	}

	public function valAccessedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExpiresOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUpdateOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
					$boolIsValid &= $this->valLibraryName();
					$boolIsValid &= $this->valLicence();
					$boolIsValid &= $this->valLicenceTypeId();
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );

		if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

}
?>