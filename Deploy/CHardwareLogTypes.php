<?php

class CHardwareLogTypes extends CBaseHardwareLogTypes {

	public static function fetchHardwareLogTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CHardwareLogType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

    public static function fetchHardwareLogType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CHardwareLogType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }
}
?>