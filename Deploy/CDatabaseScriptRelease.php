<?php

class CDatabaseScriptRelease extends CBaseDatabaseScriptRelease {

	protected $m_strSqlFilPath;

	public function setSqlFilePath( $strSqlFilPath ) {
		$this->m_strSqlFilPath = $strSqlFilPath;
	}

	public function getSqlFilePath() {
		return $this->m_strSqlFilPath;
	}

    public function valTaskReleaseId() {
        $boolIsValid = true;

        if( true == is_null( $this->getTaskReleaseId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_release_id', 'Task release ID is required.' ) );
        }

        return $boolIsValid;
    }

    public function valReleaseDatetime() {
        $boolIsValid = true;

        if( true == is_null( $this->getReleaseDatetime() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'release_datetime', 'Release datetime is required.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valTaskReleaseId();
        		$boolIsValid &= $this->valReleaseDatetime();
        		break;

        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function buildSqlLogFilePath() {
    	return PATH_NON_BACKUP_MOUNTS_GLOBAL_RELEASE_LOGS . $this->getDeploymentId() . '/';
    }

    public function buildSqlOutputLogFile() {
    	return str_replace( '.sql', '_output.log', $this->getSqlFilePath() );
    }

    public function writeSqlFile( $objDatabase, $boolIsDryRun, $boolIsReturnContent = false, $strSql, $intDatabaseReleaseId = 0 ) {

    	$strSqlFileName = 'sql_' . $objDatabase->getDatabaseName() . '_' . $objDatabase->getId() . '.sql';

    	if( 0 == $intDatabaseReleaseId ) {
		    $strSqlFilePath = $this->buildSqlLogFilePath();
	    } else {
		    $strSqlFilePath = $this->buildSqlLogFilePath() . $intDatabaseReleaseId . '/';
	    }

    	$this->m_strSqlFilPath 		 = $strSqlFilePath . $strSqlFileName;

    	if( true == file_exists( $this->m_strSqlFilPath ) ) {
    		unlink( $this->m_strSqlFilPath );
    	}

    	if( false == file_exists( $strSqlFilePath . $strSqlFileName ) ) {
    		CFileIo::recursiveMakeDir( $strSqlFilePath );
    		chmod( $strSqlFilePath, 0775 );
    	}

    	$objResourceHandle = CFileIo::fileOpen( $strSqlFilePath . $strSqlFileName, 'w' );

    	if( true == $boolIsDryRun ) {
    		fwrite( $objResourceHandle, "BEGIN;\r\n" . $strSql . "\r\n\r\nROLLBACK;" );
    	} else {
    		fwrite( $objResourceHandle, "BEGIN;\r\n" . $strSql . "\r\n\r\nCOMMIT;" );
    	}

    	fclose( $objResourceHandle );

    	if( true == $boolIsReturnContent ) {
		    return file_get_contents( $strSqlFilePath . $strSqlFileName );
    		// unlink( $strSqlFilePath . $strSqlFileName );
    	}
    }

}
?>