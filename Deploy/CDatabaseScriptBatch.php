<?php

class CDatabaseScriptBatch extends CBaseDatabaseScriptBatch {

    public function valDatabaseTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getDatabaseTypeId() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'database_type_id', 'Database type is required.' ) );
        	$boolIsValid = false;
        }
        return $boolIsValid;
    }

    public function valSvnFileIds() {
        $boolIsValid = true;

        if( true == is_null( $this->getSvnFileIds() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'svn_file_ids', 'Svn files are required.' ) );
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valDatabaseTypeId();
        		$boolIsValid &= $this->valSvnFileIds();
        		break;

        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = false, $boolDirectSet = false );
	}

}
?>