<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CMachineTypes
 * Do not add any new functions to this class.
 */

class CMachineTypes extends CBaseMachineTypes {

	public static function fetchMachineTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMachineType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMachineType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMachineType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllMachineTypes( $objDatabase ) {

		$strSql = 'SELECT *	FROM machine_types';

		return self::fetchMachineTypes( $strSql, $objDatabase );
	}
}
?>
