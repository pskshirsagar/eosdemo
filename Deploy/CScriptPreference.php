<?php

class CScriptPreference extends CBaseScriptPreference {

	/**
	 * Get Functions
	 */

	public function getValue() {
		if( false == valStr( $this->m_strValueEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strValueEncrypted, CONFIG_SODIUM_KEY_LOGIN_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_USERNAME ] );
	}

	/**
	 * Set Functions
	 */

	public function setValue( $strValue ) {
		if( true == valStr( $strValue ) ) {
			$this->setValueEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strValue ), CONFIG_SODIUM_KEY_LOGIN_USERNAME ) );
		}
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['value'] ) ) $this->setValue( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['value'] ) : $arrstrValues['value'] );

		return;
	}

	/**
	 * Validate Functions
	 */

	public function valScriptId() {
		$boolIsValid = true;

		if( true == is_null( $this->getScriptId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'script_id', 'Script id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valKey( $objDeployDatabase ) {

		$boolIsValid = true;
		if( true == is_null( $this->getKey() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Key is required.' ) );
			return false;
		}

		$intCountScriptPreferencesKey = CScriptPreferences::fetchConflictingScriptPreferencesKey( $this->getKey(), $this->getScriptId(), $this->getId(), $objDeployDatabase );

		if( 0 < $intCountScriptPreferencesKey ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Key name ' . $this->getKey() . ' is already in use.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valValueEncrypted() {
		return true;
	}

	/**
	 * Validate Functions
	 */

	public function validate( $strAction, $objDeployDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valKey( $objDeployDatabase );
				$boolIsValid &= $this->valValueEncrypted();
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				$boolIsValid &= $this->valScriptId();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>