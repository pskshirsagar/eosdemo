<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CScriptTypes
 * Do not add any new functions to this class.
 */

class CScriptTypes extends CBaseScriptTypes {

	public static function fetchScriptTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScriptType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScriptType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScriptType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllScriptTypes( $objDatabase ) {
		return self::fetchScriptTypes( 'SELECT * FROM script_types WHERE is_published = 1 ORDER BY order_num', $objDatabase );
	}
}
?>