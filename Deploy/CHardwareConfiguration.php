<?php

class CHardwareConfiguration extends CBaseHardwareConfiguration {

	protected $m_intClusterId;
	protected $m_intHardwareTypeId;
	protected $m_strHardwareName;
	protected $m_strClusterName;
	protected $m_strHardwareIpAddress;
	protected $m_strOperatingSystem;
	protected $m_strHardwareDescription;

	/**
	 * Get Functions
	 *
	 */

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function getHardwareName() {
		return $this->m_strHardwareName;
	}

	public function getClusterName() {
		return $this->m_strClusterName;
	}

	public function getHardwareIpAddress() {
		return $this->m_strHardwareIpAddress;
	}

	public function getOperatingSystem() {
		return $this->m_strOperatingSystem;
	}

	public function getHardwareDescription() {
		return $this->m_strHardwareDescription;
	}

	public function getHardwareTypeId() {
		return $this->m_intHardwareTypeId;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['cluster_id'] ) )				$this->setClusterId( $arrmixValues['cluster_id'] );
		if( true == isset( $arrmixValues['hardware_name'] ) )			$this->setHardwareName( $arrmixValues['hardware_name'] );
		if( true == isset( $arrmixValues['cluster_name'] ) )			$this->setClusterName( $arrmixValues['cluster_name'] );
		if( true == isset( $arrmixValues['hardware_ip_address'] ) )		$this->setHardwareIpAddress( $arrmixValues['hardware_ip_address'] );
		if( true == isset( $arrmixValues['operating_system'] ) )		$this->setOperatingSystem( $arrmixValues['operating_system'] );
		if( true == isset( $arrmixValues['hardware_description'] ) )	$this->setHardwareDescription( $arrmixValues['hardware_description'] );
		if( true == isset( $arrmixValues['hardware_type_id'] ) )	    $this->setHardwareTypeId( $arrmixValues['hardware_type_id'] );

		return;
	}

	public function setClusterId( $intClusterId ) {
		$this->m_intClusterId = $intClusterId;
	}

	public function setHardwareName( $strHardwareName ) {
		$this->m_strHardwareName = $strHardwareName;
	}

	public function setClusterName( $strClusterName ) {
		$this->m_strClusterName = $strClusterName;
	}

	public function setHardwareIpAddress( $strHardwareIpAddress ) {
		$this->m_strHardwareIpAddress = $strHardwareIpAddress;
	}

	public function setOperatingSystem( $strOperatingSystem ) {
		$this->m_strOperatingSystem = $strOperatingSystem;
	}

	public function setHardwareDescription( $strHardwareDescription ) {
		return $this->m_strHardwareDescription = $strHardwareDescription;
	}

	public function setHardwareTypeId( $intHardwareTypeId ) {
		return $this->m_intHardwareTypeId = $intHardwareTypeId;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valHardwareId( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getHardwareId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hardware_id', 'Hardware Name is required.' ) );
		} else {

			$strSqlCondition	= ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			$strSql				= 'WHERE hardware_id = ' . $this->getHardwareId() . $strSqlCondition;
			$intCount			= CHardwareConfigurations::fetchHardwareConfigurationCount( $strSql, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hardware_id', 'This Hardware is already in use.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valTotalCores() {
		$boolIsValid = true;

		if( true == is_null( $this->getTotalCores() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_cores', 'Total Cores required.' ) );

		} elseif( false == is_numeric( $this->getTotalCores() ) || 0 >= $this->getTotalCores() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_cores', 'Total Cores should be numeric.' ) );
		}
		return $boolIsValid;
	}

	public function valRamSize() {
		$boolIsValid = true;
		$strRamSize = ( string ) $this->getRamSize();

		if( true == is_null( $this->getRamSize() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ram_size', 'RAM Size is required.' ) );

		} elseif( false == ctype_digit( $strRamSize ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ram_size', 'RAM Size should be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function valDiskSpace() {
		$boolIsValid = true;
		$strDiskSpace = ( string ) $this->getDiskSpace();

		if( true == empty( $this->getDiskSpace() ) ) {
			return $boolIsValid;
		}
		if( false == ctype_digit( $strDiskSpace ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'disk_space', 'Disk Space should be numeric.' ) );
		}
		return $boolIsValid;
	}

	public function valPhpVersion( $objDatabase ) {
		$boolIsValid = true;
		if( true == is_null( $this->getHardwareId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hardware_id', 'PHP Version is required.' ) );
		} else {
			$objHardware = Chardwares::fetchHardwareById( $this->getHardwareId(), $objDatabase );

			if( 0 == $objHardware->getIsMiscellaneous() && CHardwareType::DATABASE_SERVER != $objHardware->getHardwareTypeId() ) {
				if( true == is_null( $this->getPhpVersion() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'php_version', 'PHP Version is required.' ) );
				} elseif( false == preg_match( '/^\d+(\.\d+)*$/', $this->getPhpVersion() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'php_version', 'Invalid PHP Version format.' ) );
				}
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolIsInsert = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valHardwareId( $objDatabase );
				$boolIsValid &= $this->valTotalCores();
				$boolIsValid &= $this->valRamSize();
				$boolIsValid &= $this->valDiskSpace();
				$boolIsValid &= $this->valPhpVersion( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>
