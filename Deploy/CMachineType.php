<?php

class CMachineType extends CBaseMachineType {

	const HARDWARE 	= 1;
	const VM 		= 2;
	const AWS 		= 3;

	public static $c_arrintMachineTypeIds = array(
		CMachineType::HARDWARE,
		CMachineType::VM,
		CMachineType::AWS,
	);

	public static $c_arrintMachineTypeIdsForAssetTypes = array(
		self::HARDWARE	=> 'Hardware',
		self::VM		=> 'VM',
		self::AWS		=> 'AWS',

	);

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>