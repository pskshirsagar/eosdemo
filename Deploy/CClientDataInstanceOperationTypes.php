<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClientDataInstanceOperationTypes
 * Do not add any new functions to this class.
 */

class CClientDataInstanceOperationTypes extends CBaseClientDataInstanceOperationTypes {

    public static function fetchClientDataInstanceOperationType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CClientDataInstanceOperationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchPublishedClientDataInstanceOperationTypes( $objDatabase ) {
    	$strSql = 'SELECT *
    	            FROM client_data_instance_operation_types
    	            WHERE 
    	            is_published = 1
    	            AND id <> ' . CClientDataInstanceOperationType::MIGRATE;

    	return self::fetchClientDataInstanceOperationTypes( $strSql, $objDatabase );
    }

}
?>