<?php

class CQueueConsumerHardware extends CBaseQueueConsumerHardware {

	const MIN_COUNSUMERS_COUNT      = 1;
	const MAX_COUNSUMERS_COUNT      = 20;

	public function valMinConsumersCount() {
		$boolIsValid = true;
		if( self::MIN_COUNSUMERS_COUNT > ( int ) $this->getMinConsumersCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_consumers_count',  'Min Consumers Count should be greater than 0.' ) );
		}
		if( self::MAX_COUNSUMERS_COUNT < ( int ) $this->getMinConsumersCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_consumers_count',  'Min Consumers Count should be maximum 20.' ) );
		}
		if( $this->getMaxConsumersCount() < $this->getMinConsumersCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_consumers_count',  'Min Consumers Count should be less than or equal to max consumer count.' ) );
		}
		return $boolIsValid;
	}

	public function valMaxConsumersCount() {
		$boolIsValid = true;
		if( self::MIN_COUNSUMERS_COUNT > ( int ) $this->getMaxConsumersCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_consumers_count',  'Max Consumers Count should be greater than 0.' ) );
		}
		if( self::MAX_COUNSUMERS_COUNT < ( int ) $this->getMaxConsumersCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_consumers_count',  'Max Consumers Count should be maximum 20.' ) );
		}
		if( $this->getMinConsumersCount() > $this->getMaxConsumersCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_consumers_count',  'Max Consumers Count should be greater than or equal to min consumer count.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valMinConsumersCount();
				$boolIsValid &= $this->valMaxConsumersCount();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>