<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CScriptExecutorTypes
 * Do not add any new functions to this class.
 */

class CScriptExecutorTypes extends CBaseScriptExecutorTypes {

    public static function fetchScriptExecutorTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CScriptExecutorType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchScriptExecutorType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScriptExecutorType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchAllScriptExecutorType( $objDatabase ) {
    	return self::fetchScriptExecutorTypes( 'SELECT * FROM script_executor_types WHERE is_published = 1 ORDER BY order_num', $objDatabase );
    }

}
?>