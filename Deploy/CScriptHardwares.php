<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CScriptHardwares
 * Do not add any new functions to this class.
 */

class CScriptHardwares extends CBaseScriptHardwares {

	public static function fetchAllScriptHardwares( $objDatabase ) {
		return parent::fetchScriptHardwares( 'SELECT * from script_hardwares', $objDatabase );
	}

	public static function fetchScriptHardwaresByScriptIds( $arrintScriptIds, $objDatabase ) {

		if( false == valArr( $arrintScriptIds ) ) {
			return NULL;
		}

		return parent::fetchScriptHardwares( 'SELECT * FROM script_hardwares WHERE script_id IN ( ' . implode( ',', $arrintScriptIds ) . ')', $objDatabase );
	}

	public static function fetchScriptHardwareByScriptIdByClusterId( $intScriptId, $intClusterId, $objDatabase ) {

		$strSql = 'SELECT
						sh.script_id, sh.hardware_id,
						h.name
					FROM
						script_hardwares sh
						JOIN hardwares h ON ( sh.hardware_id = h.id )
					WHERE
						sh.script_id = ' . ( int ) $intScriptId . '
						AND h.cluster_id = ' . ( int ) $intClusterId;

		return parent::fetchScriptHardware( $strSql, $objDatabase );
	}

	public static function fetchScriptHardwaresByHardwareIds( $arrintHardwareIds, $objDatabase ) {
		return self::fetchScriptHardwares( 'SELECT * FROM script_hardwares WHERE hardware_id IN ( ' . implode( ',', $arrintHardwareIds ) . ' )', $objDatabase );
	}

	public static function fetchScriptHardwaresByScriptIdsByClusterId( $arrintScriptIds, $intClusterId, $objDatabase ) {

		$strSql = 'SELECT
						sh.id, sh.script_id, sh.hardware_id,
						h.name
					FROM
						script_hardwares sh
						JOIN hardwares h ON ( sh.hardware_id = h.id ) 
					WHERE
						sh.script_id IN ( ' . implode( ',', $arrintScriptIds ) . ') 
						AND h.cluster_id = ' . ( int ) $intClusterId;

		return parent::fetchScriptHardwares( $strSql, $objDatabase );
	}

}
?>