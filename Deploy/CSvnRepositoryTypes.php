<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CSvnRepositoryTypes
 * Do not add any new functions to this class.
 */

class CSvnRepositoryTypes extends CBaseSvnRepositoryTypes {

	public static function fetchSvnRepositoryTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CSvnRepositoryType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSvnRepositoryTypeIds( $objDatabase ) {
    	return self::fetchSvnRepositoryTypes( 'SELECT * FROM svn_repository_types ', $objDatabase );
    }
}
?>