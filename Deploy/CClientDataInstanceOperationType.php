<?php

class CClientDataInstanceOperationType extends CBaseClientDataInstanceOperationType {

	const CREATE	= 1;
	const DELETE	= 2;
	const RESET		= 3;
	const MIGRATE	= 4;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public function testCommit() {
		echo 'Testing jenkins code push';
	}

}
?>