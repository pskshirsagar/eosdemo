<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CEnvironmentTypes
 * Do not add any new functions to this class.
 */

class CEnvironmentTypes extends CBaseEnvironmentTypes {

	public static function fetchEnvironmentTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEnvironmentType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEnvironmentType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEnvironmentType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedEnvironmentTypes( $objDatabase ) {
		return self::fetchEnvironmentTypes( 'SELECT * FROM environment_types WHERE is_published = 1', $objDatabase );
	}
}
?>