<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClusterDeployDetails
 * Do not add any new functions to this class.
 */

class CClusterDeployDetails extends CBaseClusterDeployDetails {

	public static function fetchAllClusterDeployDetails( $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {

		if( false == is_null( $strOrderByField ) ) {
			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );
			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY updated_on DESC';
		}

		$strSql = 'SELECT
		    			cdd.*,
						c.name As cluster_name
				   	FROM
		    			cluster_deploy_details AS cdd
						LEFT JOIN clusters As c ON( cdd.cluster_id = c.id )
					WHERE
						c.is_published = 1'
						. $strOrderBy;

		return self::fetchClusterDeployDetails( $strSql, $objDatabase );
	}

	public static function fetchClusterDeployDetailsByIds( $arrintClusterDeployDetailIds, $objDatabase ) {

		if( false == valArr( $arrintClusterDeployDetailIds ) ) return NULL;

		$strSql = 'SELECT * FROM cluster_deploy_details WHERE id IN ( ' . implode( ',', $arrintClusterDeployDetailIds ) . ' )';

		return self::fetchClusterDeployDetails( $strSql, $objDatabase );
	}

	public static function fetchClusterDeployDetailByClusterId( $intClusterId, $objDatabase ) {

		$strSql = 'SELECT
					    cdd.*,
					    c.name As cluster_name
					FROM
					    cluster_deploy_details as cdd JOIN clusters c ON ( cdd.cluster_id = c.id )
					WHERE
					    cluster_id = ' . ( int ) $intClusterId;

		return self::fetchClusterDeployDetail( $strSql, $objDatabase );
	}

	public static function fetchClusterDeployDetailsByClusterIds( $arrintClusterIds, $objDatabase ) {

		if( false == valArr( $arrintClusterIds ) ) return NULL;

		$strSql = 'SELECT * FROM cluster_deploy_details WHERE cluster_id IN ( ' . implode( ',', $arrintClusterIds ) . ' )';

		return self::fetchClusterDeployDetails( $strSql, $objDatabase );
	}
}
?>
