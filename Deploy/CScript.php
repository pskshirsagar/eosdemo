<?php

class CScript extends CBaseScript {

	protected $m_arrobjScriptPreferences;
	protected $m_arrobjDatabaseTransactions;
	protected $m_arrobjScriptGroups;
	protected $m_arrstrScriptHardwareIds;
	protected $m_arrintFailedDatabaseIds;

	protected $m_intMonth;
	protected $m_intDay;
	protected $m_intYear;
	protected $m_intLastMinute;
	protected $m_intLastHour;

	const INTEGRATION_QUEUE_PROCESSOR	= 23;
	const DIAGNOSTICS					= 1602;
	const POST_EFT_CHARGES              = 477;
	const QUEUE_CONSUMER				= 1008;
	const DATABASE_DIAGNOSTIC           = 1174;
	const BULK_CHECK21_RESTORATION      = 1234;

	/**
	 * ILS Feed
	 */

	const MOVE_FEED 					= 38;
	const APARTMENT_FINDER_FEED 		= 41;
	const MY_NEW_PLACE_FEED 			= 42;
	const APARTMENT_GUIDE_FEED 			= 43;
	const APARTMENT_MARKETER_FEED 		= 67;
	const FOUR_WALLS_FEED 				= 129;
	const RENTALS_MITS_FEED 			= 138;
	const APARTMENT_SEARCH_FEED 		= 162;
	const RENT_JUNGLE_FEED 				= 171;
	const APARTMENT_SHOW_CASE_FEED 		= 189;
	const HOT_PADS_FEED 				= 191;
	const APARTMENTS_FEED_SINGLE_XML 	= 205;
	const RENT_WIKI_PROSPECT_DATA_FEED 	= 210;
	const PROCESS_INVOICE_BATCH         = 246;
	const RENT_CAFE_FEED 				= 271;
	const FOR_RENT_MITS_FEED 			= 313;
	const GOOGLE_MY_BUSINESS_FEED		= 2077;
	const FACEBOOK_MARKETPLACE_FEED		= 1759;

	/**
	 * ILS Parser
	 */

	const PRE_PARSE_ILS_GUEST_CARD      = 1335;
	const PARSE_ILS_GUEST_CARD          = 1336;

	const MOUNT_COPY                    = 1493;

	/**
	 * Reputation Advisor
	 */

	const FACEBOOK_DATA_IMPORT			= 1298;

	/**
	 * Screening Script
	 */

	const SCREENING_CHECK_STATUS                        = 6;
	const RESIDENT_VERIFY_BILLING                       = 508;
	const SEND_SCREENING_CONSUMER_REPORT                = 1633;
	const UPDATE_SCREENING_CONSUMER_REPORT_STATUS       = 1634;
	const COPY_SCENING_PACKAGES_INTO_TEST_COMAPNY		= 1898;

	public static $c_arrmixScriptDetails = array(
		self::RESIDENT_VERIFY_BILLING	                => 'Resident Verify Billing',
		self::SCREENING_CHECK_STATUS		            => 'Screening Check Status',
		self::SEND_SCREENING_CONSUMER_REPORT		    => 'Send Screening Consumer Report',
		self::UPDATE_SCREENING_CONSUMER_REPORT_STATUS	=> 'Update Screening Consumer Report Status'
	);

	public function __construct() {
		parent::__construct();

		$this->m_arrobjScriptPreferences = array();
		$this->m_arrobjDatabaseTransactions = array();
		$this->m_arrobjScriptGroups	= array();

		return;
	}

	/**
	 * Create Functions
	 */

	public function createDatabaseTransaction() {

		$objDatabaseTransaction = new CDatabaseTransaction();
		$objDatabaseTransaction->setScriptId( $this->getId() );
		$objDatabaseTransaction->setDatabaseTransactionTypeId( CDatabaseTransactionType::CONFIG_SCHEDULED_SCRIPT );
		$objDatabaseTransaction->setBeginDatetime( date( 'm/d/Y H:i:s' ) );

		return $objDatabaseTransaction;
	}

	/**
	 * Add Functions
	 */

	public function addScriptPreference( $objScriptPreference ) {
		$this->m_arrobjScriptPreferences[$objScriptPreference->getKey()] = $objScriptPreference;
	}

	public function addDatabaseTransaction( $objDatabaseTransaction ) {
		$this->m_arrobjDatabaseTransactions[$objDatabaseTransaction->getId()] = $objDatabaseTransaction;
	}

	public function addScriptGroup( $objScriptGroup ) {
		$this->m_arrobjScriptGroups[$objScriptGroup->getId()] = $objScriptGroup;
	}

	/**
	 * Get Functions
	 */

	public function getScriptPreferences() {
		return $this->m_arrobjScriptPreferences;
	}

	public function getDatabaseTransactions() {
		return $this->m_arrobjDatabaseTransactions;
	}

	public function getScriptGroups() {
		return $this->m_arrobjScriptGroups;
	}

	public function getMonth() {
		return $this->m_intMonth;
	}

	public function getDay() {
		return $this->m_intDay;
	}

	public function getYear() {
		return $this->m_intYear;
	}

	public function getLastMinute() {
		return $this->m_intLastMinute;
	}

	public function getLastHour() {
		return $this->m_intLastHour;
	}

	public function getScriptHardwareIds() {
		return $this->m_arrstrScriptHardwareIds;
	}

	/**
	 * Set Functions
	 */

	public function setScriptHardwareIds( $strScriptHardwareIds ) {
		$this->m_arrstrScriptHardwareIds = $strScriptHardwareIds;
	}

	public function setDefaults() {
		$this->m_intExecutionMinutes = '60';
		$this->m_intDoNotMonitor = '0';

		return;
	}

	public function setScriptPreferences( $arrobjScriptPreferences ) {
		$this->m_arrobjScriptPreferences = $arrobjScriptPreferences;
	}

	public function setDatabaseTransactions( $arrobjDatabaseTransactions ) {
		$this->m_arrobjDatabaseTransactions = $arrobjDatabaseTransactions;
	}

	public function setScriptGroups( $arrobjScriptGroups ) {
		$this->m_arrobjScriptGroups = $arrobjScriptGroups;
	}

	public function setMonth( $intMonth ) {
		$this->m_intMonth = $intMonth;
	}

	public function setDay( $intDay ) {
		$this->m_intDay = $intDay;
	}

	public function setYear( $intYear ) {
		$this->m_intYear = $intYear;
	}

	public function setLastMinute( $intLastMinute ) {
		$this->m_intLastMinute = $intLastMinute;
	}

	public function setLastHour( $intLastHour ) {
		$this->m_intLastHour = $intLastHour;
	}

	public function setScriptPath( $strScriptPath ) {

		parent::setScriptPath( $strScriptPath );

		$arrstrPathInfo = pathinfo( $strScriptPath );

		if( true == valArr( $arrstrPathInfo ) && true == isset( $arrstrPathInfo['extension'] ) && 0 != strlen( $arrstrPathInfo['extension'] ) ) {
			$this->m_strScriptPath = $arrstrPathInfo['dirname'] . '/';
		}
	}

	/**
	 * Validation
	 */

	public function valScriptTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getScriptTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'script_type_id', 'Script type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
			return $boolIsValid;
		}

		$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
		$strWhere = 'WHERE lower( name )= \'' . addslashes( strtolower( $this->getName() ) ) . '\' AND deleted_by is NULL ' . $strSqlCondition;
		$intCount = CScripts::fetchScriptCount( $strWhere, $objDatabase );
		if( 0 < $intCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Script name is already in use for this or other script type.' ) );
		}

		return $boolIsValid;

	}

	public function valScriptPath() {
		$boolIsValid = true;

		if( true == is_null( $this->getScriptPath() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'script_path', 'Script path is required.' ) );
		} else {
			if( CScriptType::HA_PHP != $this->getScriptTypeId() ) {
				if( 'production' == CONFIG_ENVIRONMENT ) {

					if( true != file_exists( $this->getScriptPath() . '/' . $this->getName() ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'script_path', 'Script path is invalid.' ) );
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valSchedule() {

		$boolIsValid = true;

		if( true == is_null( $this->getSchedule() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'schedule', 'Schedule is required.' ) );
		}

		return $boolIsValid;
	}

	public function validateDatabaseTransactions( $arrobjWorksDatabases, $arrobjClusters, $arrstrScriptDetails, $boolIsNagiosCheck = false ) {

		$boolIsValid = true;

		$this->m_arrintFailedDatabaseIds = [];

		if( false == is_numeric( $this->m_intLastMinute ) || false == is_numeric( $this->m_intLastHour ) ) {
			return $boolIsValid;
		}

		if( false == valArr( $arrobjWorksDatabases ) && CScriptType::ENTRATA_PHP == $this->getScriptTypeId() ) {
			return true;
		}

		$strCurrentTime = ( true == $boolIsNagiosCheck ) ? time() - 300 : time();

		$intScriptStartTime = mktime( $this->m_intLastHour, $this->m_intLastMinute, 0, $this->m_intMonth, $this->m_intDay,  $this->m_intYear );
		// date( "n" ), date( "d" ), date( "Y" )

		if( CScriptType::ENTRATA_PHP == $this->getScriptTypeId() ) {
			$arrobjRekeyedDatabaseTransactions = $this->buildDatabaseTransactionsKeyedByDatabaseId();
			$boolIsRunOnTestDB  = $this->getRunOnTestDb();

			foreach( $arrobjWorksDatabases as $objWorksDatabase ) {
				$boolIsTestDB       = $objWorksDatabase->getIsTestDatabase();
				if( true == $boolIsNagiosCheck && true == $boolIsTestDB ) {
					continue;
				}
				$arrobjCurrentDatabaseTransactions = ( true == isset ( $arrobjRekeyedDatabaseTransactions[$objWorksDatabase->getId()] ) && true == valArr( $arrobjRekeyedDatabaseTransactions[$objWorksDatabase->getId()] ) ) ? $arrobjRekeyedDatabaseTransactions[$objWorksDatabase->getId()] : [];

				if( ( false == empty( $boolIsTestDB ) && false == empty( $boolIsRunOnTestDB ) ) || ( true == empty( $boolIsTestDB ) ) ) {

					// Check to see if 1.) There is a db script that should have finished but didn't and 2.) If a script didn't run and should have.
					if( ( false == valArr( $arrobjCurrentDatabaseTransactions ) ) && $strCurrentTime > $intScriptStartTime && true == valArr( $arrstrScriptDetails ) && true == in_array( $objWorksDatabase->getClusterId(), $arrstrScriptDetails ) ) {
						$this->addErrorMsg( new CErrorMsg( NULL, 'begin_datetime', 'Script did not run on ' . $objWorksDatabase->getDatabaseName() . ' at ' . $this->getLastHour() . ':' . $this->getLastMinute() . ' and should have.', 1 ) );
						$this->m_arrintFailedDatabaseIds[] = $objWorksDatabase->getId();
						$boolIsValid &= false;

					} else {
						foreach( $arrobjCurrentDatabaseTransactions as $objDatabaseTransaction ) {

							if( true == is_null( $objDatabaseTransaction->getDismissedOn() ) && true == is_null( $objDatabaseTransaction->getEndDatetime() ) && ( strtotime( $objDatabaseTransaction->getBeginDatetime() ) + ( $this->getExecutionMinutes() * 60 ) ) < $strCurrentTime ) {

								$this->addErrorMsg( new CErrorMsg( NULL, 'end_datetime', 'Script execution started at ' . date( 'm/d/Y H:i', ( strtotime( $objDatabaseTransaction->getBeginDatetime() ) ) ) . ' for (database transaction) #' . $objDatabaseTransaction->getId() . ' on ' . $objWorksDatabase->getDatabaseName() . ' did not finish on time (' . $this->m_intMonth . '/' . $this->m_intDay . '/' . $this->m_intYear . ' ' . date( 'H:i', ( strtotime( $objDatabaseTransaction->getBeginDatetime() ) + ( $this->getExecutionMinutes() * 60 ) ) ) . ').' ) );
								$boolIsValid &= false;
							}
						}
					}
				}
			}

		} elseif( true == valArr( $arrstrScriptDetails ) ) {
			// It checks for script which runs on all clusters.
			// Check to see if 1.) There is a db script that should have finished but didn't and 2.) If a script didn't run and should have.

			if( ( false == valArr( $this->getDatabaseTransactions() ) ) && $strCurrentTime > $intScriptStartTime ) {
				foreach( $arrstrScriptDetails as $intClusterId ) {
					$this->addErrorMsg( new CErrorMsg( NULL, 'begin_datetime', 'Script did not run on the cluster ' . $arrobjClusters[$intClusterId]->getName() . $this->getLastHour() . ':' . $this->getLastMinute() . ' and should have.', 1 ) );
					$boolIsValid &= false;
				}
			} else {
				foreach( $this->getDatabaseTransactions() as $objDatabaseTransaction ) {
					if( true == is_null( $objDatabaseTransaction->getDismissedOn() ) && true == is_null( $objDatabaseTransaction->getEndDatetime() ) && true == array_key_exists( $objDatabaseTransaction->getClusterId(), $arrobjClusters ) && ( strtotime( $objDatabaseTransaction->getBeginDatetime() ) + ( $this->getExecutionMinutes() * 60 ) ) < $strCurrentTime ) {
						$this->addErrorMsg( new CErrorMsg( NULL, 'end_datetime', 'Script execution started at ' . date( 'm/d/Y H:i', ( strtotime( $objDatabaseTransaction->getBeginDatetime() ) ) ) . ' for (database transaction) #' . $objDatabaseTransaction->getId() . ' did not finish on time (' . date( 'm/d/Y H:i', ( strtotime( $objDatabaseTransaction->getBeginDatetime() ) + ( $this->getExecutionMinutes() * 60 ) ) ) . ') at cluster ' . $arrobjClusters[$objDatabaseTransaction->getClusterId()]->getName() . '.' ) );
						$boolIsValid &= false;
					}
				}
			}

			// Check whether script run on the specific track.
			// eg:- run on rapid cluster but not on standard or wise-versa
			if( true == valArr( $this->getDatabaseTransactions() ) ) {

				$arrintClusterIds = array();

				foreach( $this->getDatabaseTransactions() as $objDatabaseTransaction ) {
					$arrintClusterIds[$objDatabaseTransaction->getClusterId()] = $objDatabaseTransaction->getClusterId();
				}

				foreach( $arrstrScriptDetails as $intClusterId ) {
					$boolIsRunOnCluster = true;
					if( false == in_array( $intClusterId, $arrintClusterIds ) ) {
						$boolIsRunOnCluster = false;

						if( true == $boolIsNagiosCheck && $strCurrentTime < $intScriptStartTime ) {
							continue;
						}
					}
					if( false == $boolIsRunOnCluster ) {
						$this->addErrorMsg( new CErrorMsg( NULL, 'begin_datetime', 'Script did not run on ' . $arrobjClusters[$intClusterId]->getName() . ' cluster at' . $this->getLastHour() . ':' . $this->getLastMinute() . ' and should have.', 1 ) );
						$boolIsValid &= false;
					}
				}
			}

		}

		return $boolIsValid;
	}

	public function valForBeaconBox( $arrstrScriptHardwares ) {

		$boolIsValid = true;
		if( true == is_null( $arrstrScriptHardwares ) ) {
			return $boolIsValid;
		}
		if( CScriptExecutorType::BEACONBOX == $this->getScriptExecutorTypeId() ) {

			if( true == valArr( $arrstrScriptHardwares ) ) {
				if( false == in_array( ( CCluster::RAPID || CCluster::HA ), array_keys( $arrstrScriptHardwares ) ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'script_type', 'Please select Cluster Type Hardware of Rapid', 1 ) );
					$boolIsValid &= false;
				}
			}

			/*
			 if( false == preg_match( '/Scheduled/i', $this->getScriptPath() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'script_type', 'Please select Executor Type as Cron for non-Scheduled script ', 1 ) );
				$boolIsValid &= false;
			 }
			 */

		}
		return $boolIsValid;

	}

	public function valForRunOnTemplateDb() {
		$boolIsValid = true;
		if( true == $this->getRunOnTemplateDb() ) {
			if( false == $this->getRunOnTestDb() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'run_on_template_db', 'Please enable run on test database to run script on template database', 1 ) );
				$boolIsValid &= false;
			}
		}
		return $boolIsValid;
	}

	public function valDetails() {

		$boolIsValid = true;
		if( true == is_null( $this->getRegionIds() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'details', 'Please select atleast one region.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $arrstrScriptHardwares = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valScriptTypeId();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valScriptPath();
				$boolIsValid &= $this->valSchedule();
				$boolIsValid &= $this->valForBeaconBox( $arrstrScriptHardwares );
				$boolIsValid &= $this->valForRunOnTemplateDb();
				$boolIsValid &= $this->valDetails();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valForBeaconBox( $arrstrScriptHardwares );
				$boolIsValid &= $this->valForRunOnTemplateDb();
				$boolIsValid &= $this->valDetails();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valScriptTypeId();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchClients( $objDatabase ) {
		return CClients::fetchClientsByScriptId( $this->getId(), $objDatabase );
	}

	public function fetchScriptPreferences( $objDatabase ) {
		return CScriptPreferences::fetchScriptPreferencesByScriptId( $this->getId(), $objDatabase );
	}

	public function fetchKeyedAllScriptPreferences( $objDatabase ) {
		return CScriptPreferences::fetchKeyedAllScriptPreferencesByScriptId( $this->getId(), $objDatabase );
	}

	public function fetchDatabaseTransactionsByDate( $strDate, $objDatabase ) {

		$this->m_arrobjDatabaseTransactions = CDatabaseTransactions::fetchDatabaseTransactionsByDayByScriptIds( $strDate, array( $this->getId() ), $objDatabase );
		return $this->m_arrobjDatabaseTransactions;
	}

	public function fetchNextId( $objDatabase, $strTableName = NULL ) {
		$objDataset = $objDatabase->createDataset();

		$strSql = "SELECT nextval('scripts_id_seq'::text) AS id";

		if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to get next property id. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		$arrmixValues = $objDataset->fetchArray();
		$intId = $arrmixValues['id'];

		$objDataset->cleanup();

		return $intId;
	}

	public function getFailedIds() {
		return $this->m_arrintFailedDatabaseIds;
	}

	/**
	 * Create Functions
	 */

	public function createScriptPreference() {

		$objScriptPreference = new CScriptPreference();
		$objScriptPreference->setScriptId( $this->getId() );

		return $objScriptPreference;
	}

	public function createScriptHardware() {

		$objScriptHardware = new CScriptHardware();
		$objScriptHardware->setScriptId( $this->getId() );

		return $objScriptHardware;
	}

	public function getCronContents( $arrintDatabaseIds = NULL, $objConnectDatabase = NULL ) {

		$strScriptPath	= $this->getScriptPath() . '/' . $this->getName();
		$strScriptPath	= str_replace( '//', '/', $strScriptPath );
		$strLogFilePath = '/srv/www/vhosts/Logs/crons/';

		if( true == valArr( $arrintDatabaseIds ) ) {

			$strCronContents = '';
			$strCronContents .= " \r\n\r\n######################################################################################################################################################";
			$strCronContents .= " \r\n";
			$strCronContents .= '#';
			$strCronContents .= " \r\n";
			$strCronContents .= '# ' . $this->getName();
			$strCronContents .= " \r\n";
			$strCronContents .= '#';
			$strCronContents .= " \r\n";
			( false == is_null( $this->getDescription() ) ) ? $strCronContents .= '# ' . wordwrap( $this->getDescription(), 100, "\r\n# ", true ) . " \r\n" . '# ' . " \r\n" : $strCronContents .= '';

			foreach( $arrintDatabaseIds as $intDatabaseId ) {

				$strCronContents .= $this->getSchedule() . ' /usr/bin/php ' . $strScriptPath . ' ' . ( int ) $intDatabaseId;

				if( false == is_null( $this->getLogpath() ) ) {
					$objDatabase = CDatabases::fetchDatabaseById( $intDatabaseId, $objConnectDatabase );
					$strCronContents .= ' > ' . $strLogFilePath . $objDatabase->getDatabaseName() . '/' . str_replace( '.php', '', $this->getName() ) . '.cron.log' . " \r\n";
				} else {
					$strCronContents .= " \r\n";
				}
			}

			$strCronContents .= " \r\n\r\n";
			$strCronContents .= '# ';
			$strCronContents .= substr( $this->getUpdatedOn(), 0, 19 );
			$strCronContents .= " \r\n";

		} else {

			$strCronContents = '';
			$strCronContents .= " \r\n\r\n######################################################################################################################################################";
			$strCronContents .= " \r\n";
			$strCronContents .= '#';
			$strCronContents .= " \r\n";
			$strCronContents .= '# ' . $this->getName();
			$strCronContents .= " \r\n";
			$strCronContents .= '#';
			$strCronContents .= " \r\n";
			( false == is_null( $this->getDescription() ) ) ? $strCronContents .= '# ' . wordwrap( $this->getDescription(), 100, "\r\n# ", true ) . " \r\n" . '# ' . " \r\n" : $strCronContents .= '';
			$strCronContents .= $this->getSchedule() . ' /usr/bin/php ' . $strScriptPath;

			if( CScriptType::ADMIN_PHP == $this->getScriptTypeId() ) {
				$strLogFilePath .= 'admin/';
			}

			if( false == is_null( $this->getLogpath() ) ) {
				$strCronContents .= ' > ' . $this->getLogpath() . " \r\n";
			} elseif( false == is_null( $this->getLogpath() ) ) {
				$strCronContents .= ' > ' . $strLogFilePath . str_replace( '.php', '', $this->getName() ) . '.cron.log' . " \r\n";
			}

			$strCronContents .= " \r\n\r\n";
			$strCronContents .= '# ';
			$strCronContents .= substr( $this->getUpdatedOn(), 0, 19 );
			$strCronContents .= " \r\n";
		}

		return $strCronContents;
	}

	public function buildDatabaseTransactionsKeyedByDatabaseId() {
		$arrobjDatabaseTransactions = array();

		if( true == valArr( $this->m_arrobjDatabaseTransactions ) ) {
			foreach( $this->m_arrobjDatabaseTransactions as $objDatabaseTransaction ) {
				$arrobjDatabaseTransactions[$objDatabaseTransaction->getDatabaseId()][$objDatabaseTransaction->getId()] = $objDatabaseTransaction;
			}
		}

		return $arrobjDatabaseTransactions;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == $boolReturnSqlOnly ) {
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = true );
		}

		$objOldData = CScripts::fetchScriptById( $this->getId(), $objDatabase );
		if( false == parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) ) {
			return false;
		}

		$strScriptOldData = 'Id: ' . $objOldData->getId() . ',';
		$strScriptOldData .= 'Name: ' . $objOldData->getName() . ',';
		$strScriptOldData .= 'Script Hardware Id: ' . $this->getScriptHardwareIds() . ',';
		$strScriptOldData .= 'Script Type ID: ' . $objOldData->getScriptTypeId() . ',';
		$strScriptOldData .= 'Script Executor Type ID:' . $objOldData->getScriptExecutorTypeId() . ',';
		$strScriptOldData .= 'Script Description:' . $objOldData->getDescription() . ',';
		$strScriptOldData .= 'Script Schedule: ' . $objOldData->getSchedule() . ',';
		$strScriptOldData .= 'DoNotRunInRentWeek: ' . $objOldData->getDoNotRunInRentWeek() . ',';
		$strScriptOldData .= 'DoNotMonitor: ' . $objOldData->getDoNotMonitor() . ',';
		$strScriptOldData .= 'IsCritical:' . $objOldData->getIsCritical() . ',';
		$strScriptOldData .= 'IsPublished:' . $objOldData->getIsPublished() . ',';
		$strScriptOldData .= 'RunOnTestDb:' . $objOldData->getRunOnTestDb() . ',';
		$strScriptOldData .= 'IsCentralizedLogging:' . $objOldData->getIsCentralizedLogging() . ',';
		$strScriptOldData .= 'Data:' . $objOldData->getData() . ',';
		$strScriptOldData .= 'UpdatedBy:' . $objOldData->getUpdatedBy() . ',';
		$strScriptOldData .= 'UpdatedOn:' . $objOldData->getUpdatedOn() . ',';
		$strScriptOldData .= 'CreatedBy:' . $objOldData->getCreatedBy() . ',';
		$strScriptOldData .= 'CreatedOn:' . $objOldData->getCreatedOn();

		$objNewData        = CScripts::fetchScriptById( $this->getId(), $objDatabase );
		$objScriptHardware = CScriptHardwares::fetchScriptHardwaresByScriptId( $this->getId(), $objDatabase );

		if( true == valArr( $objScriptHardware ) ) {
			$objScriptHardware = implode( ',', array_keys( rekeyObjects( 'HardwareId', $objScriptHardware ) ) );
		} else {
			$objScriptHardware = '';
		}

		$strScriptNewData  = 'Id: ' . $objNewData->getId() . ',';
		$strScriptNewData .= 'Name: ' . $objNewData->getName() . ',';
		$strScriptNewData .= 'Script Hardware Id: ' . $objScriptHardware . ',';
		$strScriptNewData .= 'Script Type ID: ' . $objNewData->getScriptTypeId() . ',';
		$strScriptNewData .= 'Script Executor Type ID:' . $objNewData->getScriptExecutorTypeId() . ',';
		$strScriptNewData .= 'Script Description:' . $objNewData->getDescription() . ',';
		$strScriptNewData .= 'Script Schedule: ' . $objNewData->getSchedule() . ',';
		$strScriptNewData .= 'DoNotRunInRentWeek: ' . $objNewData->getDoNotRunInRentWeek() . ',';
		$strScriptNewData .= 'DoNotMonitor: ' . $objNewData->getDoNotMonitor() . ',';
		$strScriptNewData .= 'IsCritical:' . $objNewData->getIsCritical() . ',';
		$strScriptNewData .= 'IsPublished:' . $objNewData->getIsPublished() . ',';
		$strScriptNewData .= 'RunOnTestDb:' . $objNewData->getRunOnTestDb() . ',';
		$strScriptNewData .= 'IsCentralizedLogging:' . $objNewData->getIsCentralizedLogging() . ',';
		$strScriptNewData .= 'Data:' . $objNewData->getData() . ',';
		$strScriptNewData .= 'UpdatedBy:' . $objNewData->getUpdatedBy() . ',';
		$strScriptNewData .= 'UpdatedOn:' . $objNewData->getUpdatedOn() . ',';
		$strScriptNewData .= 'CreatedBy:' . $objNewData->getCreatedBy() . ',';
		$strScriptNewData .= 'CreatedOn:' . $objNewData->getCreatedOn();

		$objTableLog = new CTableLog();
		if( false == $objTableLog->insert( $intCurrentUserId, $objDatabase, $strTableName = 'scripts', $intReferenceNumber = $this->getId(), $strAction = 'UPDATE', $intCid = NULL, $strOldData = $strScriptOldData, $strNewData = $strScriptNewData, $strDescription = NULL, $boolReturnSqlOnly = false ) ) {
			return false;
		}

		return true;
	}

}
?>
