<?php

class CClusterDeployDetail extends CBaseClusterDeployDetail {

	protected $m_strClusterName;

    /**
     * Get Functions
     *
     */

    public function getClusterName() {
    	return $this->m_strClusterName;
    }

    /**
     * Set Functions
     *
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
    	if( true == isset( $arrmixValues['cluster_name'] ) ) $this->setClusterName( $arrmixValues['cluster_name'] );
    	return;
    }

    public function setClusterName( $strClusterName ) {
    	$this->m_strClusterName = $strClusterName;
    }

    /**
     * Validate Functions
     *
     */

    public function valClusterId() {

    	$boolIsValid = true;

        if( true == is_null( $this->getClusterId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cluster_id', 'Cluster Name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valDeploymentRoleId() {
        $boolIsValid = true;
        if( true == is_null( $this->getDeploymentRoleId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deployment_role_id', 'Deployment Role is required.' ) );
        }

        return $boolIsValid;
    }

    public function valApprovalRoleId() {
        $boolIsValid = true;
        if( true == is_null( $this->getApprovalRoleId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'approval _role _id', 'Deployment Approval Role is required.' ) );
        }
        return $boolIsValid;
    }

    public function valSvnConifgPath() {
        $boolIsValid = true;
        if( 0 == strlen( $this->getSvnConifgPath() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'svn_conifg_path', 'SVN Config Path is required.' ) );
        } elseif( false == preg_match( '~^[0-9a-zA-Z0-9^/][0-9a-zA-Z_^/]+$~', $this->getSvnConifgPath() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'svn_conifg_path', 'Please enter SVN Config Path.' ) );
        }

        return $boolIsValid;
    }

    public function valConifgExportPath() {
        $boolIsValid = true;
        if( 0 == strlen( $this->getConifgExportPath() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'conifg_export_path', 'Config Export Path is required.' ) );
        } elseif( false == preg_match( '~^[0-9a-zA-Z0-9^/][0-9a-zA-Z_^/]+$~', $this->getConifgExportPath() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'conifg_export_path', 'Please enter Config Export Path.' ) );
        }

        return $boolIsValid;
    }

    public function valSvnConifgMountExportPath() {
        $boolIsValid = true;
        if( 0 == strlen( $this->getSvnConifgMountExportPath() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'svn_conifg_mount_export_path', 'SVN Config Mount Export Path is required.' ) );
        } elseif( false == preg_match( '~^[0-9a-zA-Z0-9^/][0-9a-zA-Z_^/]+$~', $this->getSvnConifgMountExportPath() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'svn_conifg_mount_export_path', 'Please enter SVN Config Mount Export Path.' ) );
        }

        return $boolIsValid;
    }

    public function valUrlWithSvnUrlPath( $strUrl ) {

    	$objDeployDatbase = CDatabases::createDeployDatabase();

		$objSvnRepositories = CSvnRepositories::fetchSvnRepositoriesBySvnRepositoryTypeIdsByRepositoryIds( array( CSvnRepositoryType::TRUNK, CSvnRepositoryType::BRANCH ), array( CSvnRepository::PS_CORE, CSvnRepository::PS_CORE_COMMON ), $objDeployDatbase, $boolCheckIsSystem = false );

		$arrstrSvnRepositoriesPath = array_keys( rekeyObjects( 'path', $objSvnRepositories ) );

		return ( true == in_array( $strUrl, $arrstrSvnRepositoriesPath ) ) ? true : false;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valClusterId();
        		$boolIsValid &= $this->valDeploymentRoleId();
        		$boolIsValid &= $this->valApprovalRoleId();
        		$boolIsValid &= $this->valSvnConifgPath();
        		$boolIsValid &= $this->valConifgExportPath();
        		$boolIsValid &= $this->valSvnConifgMountExportPath();
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>