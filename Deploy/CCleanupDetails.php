<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CCleanupDetails
 * Do not add any new functions to this class.
 */

class CCleanupDetails extends CBaseCleanupDetails {

	public static function fetchCleanupDetailsByRepository( $strRrepository, $objDatabase ) {

		if( true == empty( $strRrepository ) ) return NULL;
		return self::fetchCleanupDetail( 'SELECT * FROM cleanup_details WHERE repository = \'' . $strRrepository . '\' ORDER BY created_on DESC LIMIT 1', $objDatabase );
	}

}
?>