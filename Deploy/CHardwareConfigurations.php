<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CHardwareConfigurations
 * Do not add any new functions to this class.
 */

class CHardwareConfigurations extends CBaseHardwareConfigurations {

	public static function fetchAllPaginatedHardwareConfigurations( $intPageNo, $intPageSize, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$strLimit = ( int ) $intPageSize;

		if( false == is_null( $strOrderByField ) ) {
			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {

			$strOrderBy = ' ORDER BY hc.updated_on DESC';
		}

		$strSql = ' SELECT
						hc.*,
    					h.name AS hardware_name
					FROM
						hardware_configurations AS hc
						JOIN hardwares AS h ON ( h.id = hc.hardware_id AND h.deleted_by IS NULL AND h.deleted_on IS NULL ) '
				. $strOrderBy . ' OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . $strLimit;

		return parent::fetchHardwareConfigurations( $strSql, $objDatabase );
	}

	public static function fetchHardwareConfigurationsByIds( $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;
		return self::fetchHardwareConfigurations( 'SELECT * FROM hardware_configurations WHERE id IN ( ' . implode( ',', $arrintIds ) . ' )', $objDatabase );
	}

	public static function fetchSearchedHardwareConfigurations( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
						hc.*,
						h.name AS hardware_name
					FROM
					    hardware_configurations AS hc
						LEFT JOIN hardwares AS h ON( hc.hardware_id = h.id )
					WHERE
					    h.deleted_by IS NULL
					    AND h.deleted_on IS NULL
					    AND h.name ILIKE \'%' . implode( '%\' AND h.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
					ORDER BY h.name
					LIMIT 15';

		return self::fetchHardwareConfigurations( $strSql, $objDatabase );
	}

	public static function fetchHardwareConfigurationsByHardwareIds( $arrintHardwareIds, $objDatabase ) {

		if( false == valArr( $arrintHardwareIds ) ) return NULL;

		$strSql = ' SELECT
						hc.*,
						h.name AS hardware_name,
						h.ip_address AS hardware_ip_address,
						h.cluster_id,
						h.operating_system
					FROM
						hardware_configurations AS hc
						JOIN hardwares AS h ON ( h.id = hc.hardware_id AND h.deleted_by IS NULL AND h.deleted_on IS NULL )
					Where
						hc.hardware_id IN ( ' . implode( ',', $arrintHardwareIds ) . ' )';

		return self::fetchHardwareConfigurations( $strSql, $objDatabase );
	}

	public static function fetchHardwareConfigurationsByHardwareTypeIds( $arrintHardwareTypeIds, $objDatabase ) {

		if( false == valArr( $arrintHardwareTypeIds ) ) return NULL;

		$strSql = 'SELECT
					    h.name AS hardware_name,
						h.ip_address AS hardware_ip_address,
						h.cluster_id,
						h.operating_system,
						h.hardware_type_id,
						c.name AS cluster_name,
					    hc.*
					FROM
					    hardware_configurations AS hc
					    LEFT JOIN hardwares AS h ON ( h.id = hc.hardware_id AND h.deleted_by IS NULL AND h.deleted_on IS NULL )
						LEFT JOIN clusters AS c ON ( c.id = h.cluster_id )
					WHERE
					    h.hardware_type_id IN ( ' . implode( ',', $arrintHardwareTypeIds ) . ' )
					    AND c.is_published=1
                        AND h.is_published=1';

		return self::fetchHardwareConfigurations( $strSql, $objDatabase );
	}

	public static function fetchHardwareConfigurationByHardwareId( $intHardwareId, $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    hardware_configurations
					WHERE
					    hardware_id = ' . ( int ) $intHardwareId;

		return self::fetchHardwareConfiguration( $strSql, $objDatabase );
	}

	public static function fetchMiscellaneousHardwaresWithConfigurations( $objDatabase ) {

		$strSql = " SELECT
						hc.*,
						h.name AS hardware_name,
						h.description AS hardware_description,
						h.ip_address AS hardware_ip_address,
						h.cluster_id,
						h.operating_system
					FROM
						hardware_configurations AS hc
						JOIN hardwares AS h ON ( h.id = hc.hardware_id AND h.deleted_by IS NULL AND h.deleted_on IS NULL )
					Where
						h.is_miscellaneous = '1'";

		return self::fetchHardwareConfigurations( $strSql, $objDatabase );
	}

	public static function fetchOtherHardwaresWithConfigurations( $objDatabase ) {

		$strSql = 'SELECT
					    h.name AS hardware_name,
						h.ip_address AS hardware_ip_address,
						h.operating_system,
					    hc.*
					FROM
					    hardware_configurations AS hc
					    JOIN hardwares AS h ON ( h.id = hc.hardware_id AND h.deleted_by IS NULL AND h.deleted_on IS NULL )
					WHERE
					    h.hardware_type_id=' . CHardwareType::OTHER . '
                        AND h.is_published = 1 AND h.is_miscellaneous = 0';

		return self::fetchHardwareConfigurations( $strSql, $objDatabase );

	}

}
?>