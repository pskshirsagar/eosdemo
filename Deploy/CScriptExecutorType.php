<?php

class CScriptExecutorType extends CBaseScriptExecutorType {

	const CRON 				= 1;
	const BEACONBOX			= 2;
	const NONE        		= 3;

	public static $c_arrintExecuterTypeIds = array(
		CScriptExecutorType::CRON,
		CScriptExecutorType::BEACONBOX,
	);

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>