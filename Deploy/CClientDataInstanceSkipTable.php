<?php

class CClientDataInstanceSkipTable extends CBaseClientDataInstanceSkipTable {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTableName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDatabaseTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>