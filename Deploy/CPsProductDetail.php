<?php

class CPsProductDetail extends CBasePsProductDetail {

	protected $m_strClusterName;

    /**
     * Get Functions
     *
     */

    public function getClusterName() {
    	return $this->m_strClusterName;
    }

    /**
     * Set Functions
     *
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['cluster_name'] ) )	$this->setClusterName( $arrmixValues['cluster_name'] );

    	return;
    }

    public function setClusterName( $strClusterName ) {
    	$this->m_strClusterName = $strClusterName;
    }

    /**
     * Validate Functions
     *
     */

    public function valPsProductId( $objDatabase ) {
        $boolIsValid = true;

        if( true == is_null( $this->getPsProductId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product is required.' ) );
        } else {

        	$strSqlCondition	= ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
        	$strSql				= 'WHERE ps_product_id = ' . $this->getPsProductId() . $strSqlCondition;
        	$strSql				.= ( false == is_null( $this->getClusterId() ) ) ? ' AND cluster_id = ' . $this->getClusterId() : '';
        	$intCount			= CPsProductDetails::fetchPsProductDetailCount( $strSql, $objDatabase );

        	if( 0 < $intCount ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'This Product is already in use with this cluster.' ) );
        	}
        }

        return $boolIsValid;
    }

    public function valPsiPublicIp( $objDatabase ) {
        $boolIsValid = true;

        if( false == is_null( $this->getPsiPublicIp() ) ) {

        	if( false == CValidation::validateIpAddress( $this->getPsiPublicIp() ) ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'psi_public_ip', 'Invalid PSI Public IP entered.' ) );
        	} else {

        		$strSqlCondition	= ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
        		$strSql				= ' WHERE psi_public_ip = \'' . $this->getPsiPublicIp() . '\'' . $strSqlCondition;
        		$intCount			= CPsProductDetails::fetchPsProductDetailCount( $strSql, $objDatabase );

        		if( 0 < $intCount ) {
        			$boolIsValid = false;
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'psi_public_ip', 'PSI Public IP is already in use.' ) );
        		}
        	}
        }

        return $boolIsValid;
    }

    public function valClusterId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getClusterId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cluster_id', 'Cluster is required.' ) );
    	}
    	return $boolIsValid;
    }

    public function valLoadBalancerNodeIp( $objDatabase ) {
        $boolIsValid = true;

        if( false == is_null( $this->getLoadBalancerNodeIp() ) ) {

        	if( false == CValidation::validateIpAddress( $this->getLoadBalancerNodeIp() ) ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'load_balancer_node_ip', 'Invalid Load Balancer Node IP entered.' ) );
        	} else {

        		$strSqlCondition	= ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
		        $strSql				= ' WHERE load_balancer_node_ip = \'' . $this->getLoadBalancerNodeIp() . '\'' . $strSqlCondition;
		        $intCount			= CPsProductDetails::fetchPsProductDetailCount( $strSql, $objDatabase );

		        if( 0 < $intCount ) {
		        	$boolIsValid = false;
		        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'load_balancer_node_ip', 'Load Balancer Node IP is already in use.' ) );
		        }
        	}
        }

        return $boolIsValid;
    }

    public function valNagiosGraphUrl( $objDatabase ) {
        $boolIsValid = true;

        if( false == is_null( $this->getNagiosGraphUrl() ) ) {
	        if( false == filter_var( $this->getNagiosGraphUrl(), FILTER_VALIDATE_URL ) ) {

	        	$boolIsValid = false;
	        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'nagios_graph_url', 'Invalid Nagios Graph URL.' ) );

	        } else {

	        	$strSqlCondition	= ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
		        $strSql				= ' WHERE nagios_graph_url ILIKE \'%' . $this->getNagiosGraphUrl() . '%\'' . $strSqlCondition;
		        $intCount			= CPsProductDetails::fetchPsProductDetailCount( $strSql, $objDatabase );

		        if( 0 < $intCount ) {
		        	$boolIsValid = false;
		        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'nagios_graph_url', 'Nagios Graph URL is already in use.' ) );
		        }
	        }
        }

        return $boolIsValid;
    }

    public function valProductUrl( $objDatabase ) {
        $boolIsValid = true;

        if( false == is_null( $this->getProductUrl() ) ) {

        	if( false == filter_var( $this->getProductUrl(), FILTER_VALIDATE_URL ) ) {

        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'product_url', 'Invalid Product URL.' ) );

        	} else {

        		$strSqlCondition	= ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
		        $strSql				= ' WHERE product_url ILIKE \'%' . $this->getProductUrl() . '%\'' . $strSqlCondition;
		        $intCount			= CPsProductDetails::fetchPsProductDetailCount( $strSql, $objDatabase );

		        if( 0 < $intCount ) {
		        	$boolIsValid = false;
		        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'product_url', 'Product URL is already in use.' ) );
		        }
        	}
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valPsProductId( $objDatabase );
        		$boolIsValid &= $this->valPsiPublicIp( $objDatabase );
        		$boolIsValid &= $this->valClusterId();
        		$boolIsValid &= $this->valLoadBalancerNodeIp( $objDatabase );
        		$boolIsValid &= $this->valNagiosGraphUrl( $objDatabase );
        		$boolIsValid &= $this->valProductUrl( $objDatabase );
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>