<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CPsProductDetails
 * Do not add any new functions to this class.
 */

class CPsProductDetails extends CBasePsProductDetails {

	public static function fetchAllPaginatedPsProductDetails( $intPageNo, $intPageSize, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$strLimit = ( int ) $intPageSize;

		if( false == is_null( $strOrderByField ) ) {
			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {

			$strOrderBy = ' ORDER BY updated_on DESC';
		}

		$strSql = ' SELECT
                            ppd.*
                        FROM
                            ps_product_details AS ppd
                            LEFT JOIN clusters AS c ON ( c.id = ppd.cluster_id )
                        WHERE
                            c.is_published = 1 '
				. $strOrderBy . ' OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . $strLimit;

		return parent::fetchPsProductDetails( $strSql, $objDatabase );
	}

	public static function fetchPsProductDetailsByIds( $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;
		return self::fetchPsProductDetails( 'SELECT * FROM ps_product_details WHERE id IN ( ' . implode( ',', $arrintIds ) . ' )', $objDatabase );
	}

	public static function fetchSearchedPsProductDetails( $arrstrFilteredExplodedSearch, $objDatabase ) {

	 	$strSql = 'SELECT
						*
					FROM
					    ps_product_details AS ppd
					WHERE
					    ppd.psi_public_ip ILIKE \'%' . implode( '%\' AND  ppd.load_balancer_node_ip ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
					    OR ppd.load_balancer_node_ip ILIKE \'%' . implode( '%\' AND  ppd.psi_public_ip ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
					    OR ppd.title ILIKE \'%' . implode( '%\' AND  ppd.load_balancer_node_ip ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
					    OR ppd.title ILIKE \'%' . implode( '%\' AND  ppd.psi_public_ip ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
					ORDER BY ppd.title
					LIMIT 15';

		return self::fetchPsProductDetails( $strSql, $objDatabase );
	}

	public static function fetchAllPsProductDetails( $objDatabase ) {

		$strSql = 'SELECT
					    ppd.*,
					    c.name AS cluster_name
					FROM
					    ps_product_details AS ppd
					    LEFT JOIN clusters AS c ON ( c.id = ppd.cluster_id )
					WHERE
						c.is_published = 1';

		return self::fetchPsProductDetails( $strSql, $objDatabase );
	}

	public static function fetchPsProductDetailsWithHardwares( $objDatabase ) {

		$strSql = 'SELECT
					    ppd.ps_product_id AS product_id,
					    c.name AS cluster_name,
					    h.name AS hardware_name
					FROM
					    ps_product_details AS ppd
					    JOIN hardware_products AS hp ON ( hp.ps_product_id = ppd.ps_product_id )
					    JOIN hardwares AS h ON ( h.id = hp.hardware_id AND h.cluster_id = ppd.cluster_id )
					    JOIN clusters AS c ON ( c.id = h.cluster_id )
					ORDER BY
					    ppd.ps_product_id,
					    ppd.cluster_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedPsProductDetailCount( $objDatabase ) {

		$strSql			= '	SELECT
								count( DISTINCT( ppd.id ) )
							FROM
								ps_product_details ppd
								LEFT JOIN clusters AS c ON ( c.id = ppd.cluster_id )
							WHERE c.is_published = 1';

		$arrintResponse	= fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintResponse ) && true == isset( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;
	}

}
?>