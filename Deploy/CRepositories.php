<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CRepositories
 * Do not add any new functions to this class.
 */

class CRepositories extends CBaseRepositories {

	public static function fetchGitRepositoryByNameByProductId( $strName, $intRepositoryId, $intProductId, $objDatabase, $intRepositoryTypeId = CRepositoryType::REPOSITORY ) {
		$strSql = 'SELECT * FROM repositories WHERE repository_id = ' . ( int ) $intRepositoryId . ' AND ';
		$strSql .= ( 0 != $intProductId ) ? ' repository_type_id = ' . ( int ) $intRepositoryTypeId . ' AND ps_product_id = ' . ( int ) $intProductId : ' name = \'' . $strName . '\'';
		$strSql .= ' LIMIT 1';

		return parent::fetchRepository( $strSql, $objDatabase );
	}

	public static function fetchRepositoryCountByCreatedOn( $strDate, $intGiRepositoryTypeId, $intRepositoryId, $intProductId, $objDatabase ) {

		$strSql = 'SELECT count(id) FROM repositories WHERE repository_type_id = ' . ( int ) $intGiRepositoryTypeId . ' AND created_on > \'' . $strDate . '\' AND ';

		$strSql .= ( 0 != $intProductId ) ? ' ps_product_id = ' . ( int ) $intProductId : ' repository_id = ' . ( int ) $intRepositoryId;

		$arrintCount = fetchData( $strSql, $objDatabase );

		return $arrintCount[0]['count'];
	}

	public static function fetchGitRepositoriesByGitRepositoryTypeIdsByRepositoryIds( $intSvnRepositoryTypeIds, $arrintRepositoryIds, $objDatabase ) {
		$strSql = 'SELECT
					    *
					FROM
					    repositories
					WHERE
					    repository_id IN ( ' . implode( ',', $arrintRepositoryIds ) . ' )
					    AND repository_type_id IN ( ' . implode( ',', $intSvnRepositoryTypeIds ) . ' )
			    		AND deleted_by IS NULL';
		return self::fetchRepositories( $strSql, $objDatabase );
	}

	public static function fetchGitRepositoriesByGitRepositoryTypeIdsByPsProductId( $intGitRepositoryTypeIds, $intRepositoryId, $objDatabase ) {
		$strSql = 'SELECT
					    *
					FROM
					    repositories
					WHERE
					    ps_product_id IN ( ' . ( int ) $intRepositoryId . ' )
					    AND repository_type_id IN ( ' . implode( ',', $intGitRepositoryTypeIds ) . ' )
			    		AND deleted_by IS NULL';
		return self::fetchRepositories( $strSql, $objDatabase );
	}

	public static function fetchGitRepositoriesByGitRepositoryTypeIdsByPsProductIds( $intRepositoryTypeId, $objDatabase ) {
		$strSql = 'SELECT
                         r.*,
                         dp.clone_url,
                         dp.cluster_id
                     FROM
                         repositories r
                         JOIN deployment_products dp ON ( dp.name = r.name ) 
                     WHERE
                          r.repository_type_id  = ' . ( int ) $intRepositoryTypeId . '
                          AND dp.is_git_hosted = 1 
						  AND dp.is_deployed = 1
                          AND r.deleted_by IS NULL';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchGitCustomBranchesRepository( $intRepositoryId, $objDatabase ) {

		$strSql = 'SELECT * FROM repositories WHERE repository_id = ' . ( int ) $intRepositoryId . ' AND deleted_by IS NULL AND deleted_on IS NULL AND email_notifications IS NOT NULL and  is_automerge = 1 ';

		return parent::fetchRepositories( $strSql, $objDatabase );
	}

	public static function fetchGitRepositoriesByRepositoryIds( $arrintRepositoryIds, $objDatabase ) {
		if( false == valArr( array_filter( $arrintRepositoryIds ) ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						repositories
					WHERE
						id IN ( ' . implode( ',', $arrintRepositoryIds ) . ' )
					ORDER BY
						name';

		return self::fetchRepositories( $strSql, $objDatabase );
	}

	public static function fetchCustomRepositoryByDeploymentId( $intDeploymentId, $objDatabase ) {
		return self::fetchRepository( sprintf( 'SELECT * FROM repositories WHERE deployment_id = %d', ( int ) $intDeploymentId ), $objDatabase );
	}

	public static function fetchAllRepositories( $objDatabase ) {
		return self::fetchRepositories( sprintf( 'SELECT * FROM repositories where deleted_by IS NULL AND deleted_on IS NULL' ), $objDatabase );
	}

}
?>