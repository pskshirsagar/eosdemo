<?php

class CClientDataInstanceOperationStatusType extends CBaseClientDataInstanceOperationStatusType {

	const PENDING		= 1;
	const APPROVED		= 2;
	const IN_PROCESS	= 3;
	const COMPLETED		= 4;
	const CANCELLED		= 5;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>
