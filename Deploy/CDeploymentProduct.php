<?php

class CDeploymentProduct extends CBaseDeploymentProduct {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClusterId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsGitHosted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDeployed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>