<?php

class CRepository extends CBaseRepository {

	const TRUNK							= 1;
	const BRANCH						= 2;
	const TAG							= 3;
	const REPOSITORY 					= 4;
	const PS_CORE						= 5;
	const PS_CORE_COMMON 				= 6;
	const PSPAYMENT						= 7000;
	const PSSECURE						= 7002;
	const RESIDENTINSURE 				= 7003;
	const PS_LIBRARIES					= 11217;
	const HA							= 38925;
	const ENTRATA_FI					= 43943;
	const ENTRATA_CORE_TEST_AUTOMATION	= 37967;
	const RVSECURE		                = 8134;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRepositoryTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeploymentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRepositoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailNotifications() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAutomerge() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsParsed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDeleted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsJenkinsChecksEnable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsUrlRequired() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExpectedEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>