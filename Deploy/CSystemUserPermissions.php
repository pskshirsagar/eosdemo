<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CSystemUserPermissions
 * Do not add any new functions to this class.
 */

class CSystemUserPermissions extends CBaseSystemUserPermissions {

	public static function fetchPermissionedModuleIds( $objSystemUser, $objDatabase ) {

		if( 1 == $objSystemUser->getIsAdministrator() ) {
			$strSql = 'SELECT id AS system_module_id FROM system_modules WHERE is_published = 1';
		} else {
			// user permissioned modules
			$strSql = '	SELECT
							system_module_id
						FROM
							system_user_permissions
						WHERE
							is_allowed = 1
							AND system_user_id = ' . ( int ) $objSystemUser->getId() . '';
			// public modules
			$strSql .= 'UNION
							SELECT
								id
							FROM
								system_modules
							WHERE
								is_public = 1';
		}

		$arrintResultData = fetchData( $strSql, $objDatabase );

		$arrintModuleIds = array();

		if( true == valArr( $arrintResultData ) ) {
			foreach( $arrintResultData as $arrmixModuleIds ) {
				$arrintModuleIds[$arrmixModuleIds['system_module_id']] = $arrmixModuleIds['system_module_id'];
			}
		}

		return $arrintModuleIds;
	}

}
?>