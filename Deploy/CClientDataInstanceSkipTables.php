<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClientDataInstanceSkipTables
 * Do not add any new functions to this class.
 */

class CClientDataInstanceSkipTables extends CBaseClientDataInstanceSkipTables {

	public static function fetchAllClientDataInstanceSkipTables( $objDatabase ) {
		$strSql = 'SELECT 
						table_name,
						database_type_id
					FROM 
						client_data_instance_skip_tables';

		return rekeyObjects( 'TableName', self::fetchClientDataInstanceSkipTables( $strSql, $objDatabase ) );
	}

}
?>