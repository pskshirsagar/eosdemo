<?php

class CDeploymentType extends CBaseDeploymentType {

	const WEEKLY_RELEASE				        = 1;
	const IT_MAINTENANCE				        = 2;
	const SQL_EXECUTION					        = 3;
	const PARTIAL_PUSH					        = 4;
	const FULL_PUSH						        = 5;
	const PUSH_CONFIG					        = 7;
	const SYSTEM_MAINTENANCE 			        = 8;
	const CLEAR_SESSION					        = 9;
	const CLEAR_INTERFACE_CACHE 		        = 10;
	const RELOAD_PSI_AUTOLOAD_FILES 	        = 11;
	const SYSTEM_CRON					        = 12;
	const CLEAR_CACHE					        = 13;
	const MANUAL_SQL_EXECUTION 			        = 14;
	const DATABASE_MAINTENANCE 			        = 15;
	const MIGRATE_COMPANY				        = 16;
	const RELOAD_F5_CONFIGURATION 		        = 17;
	const PROCEDURE_EXECUTION 			        = 18;
	const WEEKLY_PROCEDURE_EXECUTION	        = 19;
	const RELOAD_HARDWARE_STATUS		        = 20;
	const RENAME_SVN_FILES				        = 21;
	const MANUAL_SCRIPT_EXECUTION		        = 22;
	const CHECK21_IMAGE_RESTORATION 	        = 23;
	const MOUNT_TEST_CLIENTS_MAPPING	        = 24;
	const CLEAR_LESS_CACHE			            = 26;
	const ROP_DATABASE_SCRIPT_FILES		        = 27;
	const BLOCK_BRANCH_COMMITS 			        = 28;
	const BUILD_PUSH_FILES				        = 29;
	const SHARED_CRONS					        = 30;
	const UPDATE_CODE_ON_SERVER			        = 31;
	const UPDATE_DEPLOYMENT_SERVER 		        = 32;
	const PRODUCT_MAINTENANCE			        = 33;
	const VIEWS_EXECUTION 				        = 34;
	const WEEKLY_VIEWS_EXECUTION		        = 35;
	const CLOUD_INSTANCE				        = 36;
	const DATABASE_CRON_MAINTENANCE		        = 37;
	const MANUAL_SQL_EXECUTION_SCHEMA_CHANGES   = 38;
	const RELOAD_SSL_F5_CONFIGURATION           = 39;
	const CHECK21_BULK_IMAGE_RESTORATION 	    = 40;
	const AUTO_POST_RELEASE_VERIFICATION_SMOKE 	= 41;
	const MANUAL_SQL_DRY_RUN			        = 42;
	const SYSTEM_QUEUE_CONSUMER			        = 43;
	const RVSECURE_DATA_FIXES			        = 44;
	const RVSECURE_DB_FIXES 			        = 45;
	const CLEAR_CDN_CACHE			            = 46;
	const CREATE_BRANCH			                = 47;
	const GIT_FULL_PUSH							= 48;
	const PARTIAL_PUSH_GIT						= 49;
	const ENTRATAMATION_VERSION                 = 50;
	const DATABASE_DEPLOYMENT_EXECUTION         = 51;
	const UPDATE_BEACONBOX_SERVER               = 52;
	const CRONTAB_BACKUP                        = 53;
	const UPDATE_TASK_STATUS                    = 54;
	const GIT_PUSH_CONFIG						= 55;
	const RELOAD_RI_F5_CONFIGURATION            = 56;
	const UPDATE_CRON_ENTRIES                   = 57;

	const PARTIAL_PUSH_COMMAND					        = 'partial_push [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const PARTIAL_PUSH_GIT_COMMAND				        = 'partial_push_git [[CLUSTER_ID]] [[DEPLOYMENTID]] [[PRODUCTID]]';
	const PARTIAL_PUSH_GIT_TAG_COMMAND				    = 'partial_push_git_tag [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const FULL_PUSH_COMMAND						        = 'push_all_files [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const PUSH_CONFIG_COMMAND					        = 'push_configs [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const GIT_FULL_PUSH_COMMAND						    = 'git_push_all_files [[CLUSTER_ID]] [[DEPLOYMENTID]] [[PRODUCTID]]';
	const SCHEDULED_MAINTENANCE_COMMAND  		        = 'scheduled_maintenance [[CLUSTER_ID]] [[PARAM]] [[DEPLOYMENTID]]';
	const UNSCHEDULED_MAINTENANCE_COMMAND  		        = 'unscheduled_maintenance [[CLUSTER_ID]] [[PARAM]] [[DEPLOYMENTID]]';
	const CLEAR_SESSION_COMMAND					        = 'clear_session_files [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const CLEAR_INTERFACE_CACHE_COMMAND			        = 'clear_interface_cache [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const RELOAD_PSI_AUTOLOAD_FILES_COMMAND		        = 'reload_psi_autoload_files [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const SYSTEM_CRON_COMMAND					        = 'all_crons [[CLUSTER_ID]] [[PARAM]] [[DEPLOYMENTID]]';
	const SYSTEM_QUEUE_CONSUMER_COMMAND					= 'all_queue_consumer [[CLUSTER_ID]] [[PARAM]] [[DATABASEIDS]] [[FREESWITCH]] [[DEPLOYMENTID]]';
	const CLEAR_DATA_CACHE_COMMAND				        = 'clear_data_cache [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const CLEAR_PAGE_CACHE_COMMAND				        = 'clear_page_cache [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const CLEAR_ALL_CACHE_COMMAND				        = 'clear_all_cache [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const DATABASE_MAINTENANCE_COMMAND			        = 'database_maintenance [[PARAM]] [[DATABASEIDS]] [[DEPLOYMENTID]]';
	const RELOAD_F5_CONFIGURATION_COMMAND		        = 'reload_f5_config [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const RELOAD_HARDWARE_STATUS_COMMAND		        = 'update_server_maintenance_info [[DEPLOYMENTID]]';
	const CHECK21_IMAGE_RESTORATION_COMMAND		        = 'check21_image_restoration [[DEPLOYMENTID]]';
	const MOUNT_TEST_CLIENTS_MAPPING_COMMAND	        = 'mount_test_clients_mapping [[DEPLOYMENTID]]';
	const CLEAR_LESS_CACHE_COMMAND 				        = 'clear_less_cache [[CLUSTER_ID]] [[PARAM]] [[DEPLOYMENTID]]';
	const ROP_DATABASE_SCRIPT_FILES_COMMAND 	        = 'rop_database_script_files [[ROP_DEPLOYMENT_ID]] [[DEPLOYMENTID]]';
	const BLOCK_BRANCH_COMMITS_COMMAND			        = 'block_branch_commits [[PARAM]] [[DEPLOYMENTID]]';
	const BUILD_PUSH_FILES_COMMAND				        = 'build_push_files [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const SHARED_CRONS_COMMAND					        = 'shared_crons [[PARAM]] [[DEPLOYMENTID]]';
	const UPDATE_CODE_ON_SERVER_COMMAND 		        = 'update_code_on_server [[CLUSTER_ID]] [[IP_ADDRESS]] [[DEPLOYMENTID]]';
	const UPDATE_DEPLOYMENT_SERVER_COMMAND  	        = 'update_deployment_server [[DEPLOYMENTID]]';
	const PRODUCT_MAINTENANCE_COMMAND			        = 'product_maintenance [[CLUSTER_ID]] [[PARAM]] [[PRODUCTIDS]] [[DEPLOYMENTID]]';
	const DATABASE_CRON_MAINTENANCE_COMMAND		        = 'database_cron_maintenance [[PARAM]] [[DATABASEIDS]] [[DEPLOYMENTID]]';
	const RELOAD_SSL_F5_CONFIGURATION_COMMAND	        = 'reload_ssl_f5_config [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const AUTO_POST_RELEASE_VERIFICATION_SMOKE_COMMAND	= 'auto_post_release_verification_smoke [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const CREATE_BRANCH_COMMAND	                        = 'create_branch [[CLUSTER_ID]] [[REPOSITORY_ID]] [[DEPLOYMENTID]]';
	const CREATE_ENTRATAMATION_VERSION_COMMAND          = 'entratamation_version [[CLUSTER_ID]] [[PARAM]] [[DEPLOYMENTID]]';
	const DATABASE_DEPLOYMENT_EXECUTION_COMMAND         = 'database_deployment_execution [[DATABASEDEPLOYMENTID]] [[DATABASESCRIPTRELEASEID]] [[MANUALSQLRUNSTATUS]] [[DEPLOYMENTID]]';
	const UPDATE_BEACONBOX_SERVER_COMMAND               = 'update_beaconbox_server [[CLUSTER_ID]] [[PARAM]] [[IP_ADDRESS]] [[DEPLOYMENTID]]';
	const CRONTAB_BACKUP_COMMNAD                        = 'crontab_backup [[DEPLOYMENTID]]';
	const UPDATE_TASK_STATUS_COMMAND                    = 'update_task_status [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const GIT_PUSH_CONFIG_COMMAND						= 'git_push_configs [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const RELOAD_RI_F5_CONFIGURATION_COMMAND    		= 'reload_ri_f5_config [[CLUSTER_ID]] [[DEPLOYMENTID]]';
	const UPDATE_CRON_ENTRIES_COMMAND                   = 'update_cron_entries [[CLUSTER_ID]] [[PARAM]] [[OPTION]] [[SEARCHDBID]] [[NEWDBID]] [[DEPLOYMENTID]]';

	public static $c_arrstrDeploymentTypes = [
		CDeploymentType::SYSTEM_MAINTENANCE   => 'System Maintenance',
		CDeploymentType::DATABASE_MAINTENANCE => 'Database Maintenance'
	];

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = false;
            	break;
        }

        return $boolIsValid;
    }

}
?>