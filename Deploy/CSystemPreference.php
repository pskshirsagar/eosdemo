<?php

class CSystemPreference extends CBaseSystemPreference {

	const CANCELLED_LEASE_ARCHIVE_LAST_DATE			= 'cancelled_lease_archive_last_date';
	const PAST_LEASE_ARCHIVE_LAST_DATE				= 'past_lease_archive_last_date';
	const PS_LEAD_DOCUMENTS_KEY						= 'ps_lead_documents_last_synced_date';
	const ESIGN_FILE_UPLOAD_ARCHIVE_LAST_DATE		= 'esign_file_upload_archive_last_date';
	const CURRENT_APPROVED_LEASE_ARCHIVE_LAST_DATE 	= 'current_approved_lease_archive_last_date';

	public static $c_arrstrSystemPreferences = array(
		CSystemPreference::CANCELLED_LEASE_ARCHIVE_LAST_DATE,
		CSystemPreference::PAST_LEASE_ARCHIVE_LAST_DATE
	);

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>