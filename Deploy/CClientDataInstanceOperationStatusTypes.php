<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClientDataInstanceOperationStatusTypes
 * Do not add any new functions to this class.
 */

class CClientDataInstanceOperationStatusTypes extends CBaseClientDataInstanceOperationStatusTypes {

    public static function fetchClientDataInstanceOperationStatusType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CClientDataInstanceOperationStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>