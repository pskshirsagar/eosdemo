<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CLicenceTypes
 * Do not add any new functions to this class.
 */

class CLicenceTypes extends CBaseLicenceTypes {

	public static function fetchLicenceTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CLicenceType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchLicenceType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CLicenceType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllLicenceTypes( $objDatabase ) {
		return self::fetchLicenceTypes( 'SELECT * FROM licence_types WHERE is_published = 1 ORDER BY id', $objDatabase );
	}

}
?>