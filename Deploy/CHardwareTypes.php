<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CHardwareTypes
 * Do not add any new functions to this class.
 */

class CHardwareTypes extends CBaseHardwareTypes {

	public static function fetchHardwareTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CHardwareType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

    public static function fetchHardwareType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CHardwareType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchAllHardwareTypes( $objDatabase ) {
    	return parent::fetchObjects( 'SELECT * FROM hardware_types', 'CHardwareType', $objDatabase );
    }

    public static function fetchHardwareTypeByName( $strTypeName, $objDatabase ) {

    	 $strSql = 'SELECT * FROM hardware_types where name ILIKE \'' . trim( $strTypeName ) . '\'';

    	 return self::fetchHardwareType( $strSql, $objDatabase );
    }
}
?>