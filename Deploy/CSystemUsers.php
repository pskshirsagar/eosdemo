<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CSystemUsers
 * Do not add any new functions to this class.
 */

class CSystemUsers extends CBaseSystemUsers {

	public static function fetchSystemUserByUsername( $strUsername, $objDatabase ) {

		if( true == empty( $strUsername ) ) return;

		$strSql = 'SELECT
						*
					FROM
						system_users
					WHERE
						username ILIKE \'' . $strUsername . '\'
						AND deleted_by IS NULL
						AND deleted_on IS NULL ';

		return self::fetchSystemUsers( $strSql, $objDatabase );
	}

	public static function fetchPaginatedSystemUsers( $intPageNo, $intPageSize, $objDatabase,  $strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		if( false == is_null( $strOrderByField ) ) {
			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY updated_on DESC';
		}

		$strSql = 'SELECT
							*
						FROM
							system_users
						WHERE
							is_disabled = 0
							AND deleted_by IS NULL
							AND deleted_on IS NULL ' . $strOrderBy . ' OFFSET ' . ( int ) $intOffset . '
							LIMIT ' . ( int ) $intLimit;

		return self::fetchSystemUsers( $strSql, $objDatabase );
	}

	public static function fetchPaginatedSystemUsersCount( $objDatabase ) {

		$strWhere = 'WHERE
						is_disabled = 0
						AND deleted_by IS NULL
						AND deleted_on IS NULL';
		return self::fetchSystemUserCount( $strWhere, $objDatabase );
	}

	public static function fetchActiveSystemUserByUsername( $strUsername, $objDatabase ) {

		$strSql = 'SELECT
						 *
					FROM
						system_users
					WHERE
						(
							(
								is_active_directory_user = 0
								AND username = \'' . addslashes( ( string ) $strUsername ) . '\'
							)
							OR
							(
								is_active_directory_user = 1
								AND LOWER( username ) = LOWER( \'' . addslashes( ( string ) $strUsername ) . '\' )
							)
						)
						AND is_disabled = 0';

		return self::fetchSystemUser( $strSql, $objDatabase );
	}

}
?>