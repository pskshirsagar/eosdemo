<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CInternalIpAddresses
 * Do not add any new functions to this class.
 */

class CInternalIpAddresses extends CBaseInternalIpAddresses {

	public static function fetchInternalIpAddressesByIds( $arrintInternalIpAddressIds, $objDatabase ) {
		if( false == valArr( $arrintInternalIpAddressIds ) ) return NULL;
		return self::fetchInternalIpAddresses( 'SELECT * FROM internal_ip_addresses WHERE id IN ( ' . implode( ',', $arrintInternalIpAddressIds ) . ' )', $objDatabase );
	}

	public static function fetchAllPaginatedInternalIpAddresses( $intPageNo, $intPageSize, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		if( false == is_null( $strOrderByField ) ) {
			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {

			$strOrderBy = ' ORDER BY updated_on DESC';
		}

		$strSql = ' SELECT
						*
					FROM
						internal_ip_addresses '
		 . $strOrderBy . ' OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . $intLimit;

		return parent::fetchInternalIpAddresses( $strSql, $objDatabase );
	}

	public static function fetchSearchedIpAddresses( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    internal_ip_addresses As i
					WHERE
					    i.id IS NOT NULL
					    AND (
							 i.name ILIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
							 OR i.description ILIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
							 OR i.ip_address ILIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
						     )
					ORDER BY i.name
					LIMIT 15';
		return self::fetchInternalIpAddresses( $strSql, $objDatabase );
	}

}
?>