<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CScriptOwners
 * Do not add any new functions to this class.
 */

class CScriptOwners extends CBaseScriptOwners {

	public static function fetchScriptOwnerByScriptId( $intScriptId, $objDatabase ) {
		return self::fetchScriptOwner( sprintf( 'SELECT * FROM script_owners WHERE script_id = %d', ( int ) $intScriptId ), $objDatabase );
	}

	public static function fetchAllScriptOwners( $objDatabase ) {
		return self::fetchScriptOwners( 'SELECT * FROM script_owners', $objDatabase );
	}

	public static function fetchAllScriptOwnersByEmployeeId( $intEmployeeId,$objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						script_owners
					WHERE
						\'' . $intEmployeeId . '\'  = ANY( string_to_array( employee_id, \',\' ) )';

				return self::fetchScriptOwners( $strSql, $objDatabase );
	}

	public static function fetchEmployeeIdByScriptId( $arrintScriptIds, $objDeployDatabase ) {
		$strSql = 'SELECT
					employee_id
				FROM
					script_owners
				WHERE
					script_id IN( ' . implode( ',',  $arrintScriptIds ) . ')';
		return fetchData( $strSql, $objDeployDatabase );
	}

}
?>