<?php

class CSvnRepositoryType extends CBaseSvnRepositoryType {

	const TRUNK			= 1;
	const BRANCH		= 2;
	const TAG			= 3;
	const REPOSITORY 	= 4;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

        	default:
           		// default case
            	$boolIsValid = false;
           		break;
        }

        return $boolIsValid;
    }
}
?>