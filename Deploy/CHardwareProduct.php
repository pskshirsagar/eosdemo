<?php

class CHardwareProduct extends CBaseHardwareProduct {

	protected $m_strHardwareName;

    /**
     * Get Functions
     *
     */

    public function getHardwareName() {
    	return $this->m_strHardwareName;
    }

    /**
     * Set Functions
     *
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['hardware_name'] ) )	$this->setHardwareName( $arrmixValues['hardware_name'] );

    	return;
    }

    public function setHardwareName( $strHardwareName ) {
    	$this->m_strHardwareName = $strHardwareName;
    }

    /**
     * Validate Functions
     *
     */

    public function valHardwareId() {
        $boolIsValid = true;

        if( true == is_null( $this->getHardwareId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hardware_id', 'Hardware Name is required.' ) );
        }
        return $boolIsValid;
    }

    public function valPsProductId( $objDatabase ) {
        $boolIsValid = true;

        if( true == is_null( $this->getPsProductId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product Name is required.' ) );
        } elseif( false == is_null( $this->getHardwareId() ) ) {

        	$strSqlCondition	= ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
        	$strSql				= 'WHERE ps_product_id = ' . $this->getPsProductId() . $strSqlCondition;
        	$strSql				.= ( false == is_null( $this->getHardwareId() ) ) ?  ' AND hardware_id = ' . $this->getHardwareId() : '';
        	$intCount			= CHardwareProducts::fetchHardwareProductCount( $strSql, $objDatabase );

        	if( 0 < $intCount ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product is already associated with this Hardware.' ) );
        	}
        }

        return $boolIsValid;
    }

    public function valPortalIp( $objDatabase ) {
    	$boolIsValid = true;

    	if( true == is_null( $this->getPortalIp() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'portal_ip', 'Portal IP is required.' ) );

    	} elseif( false == CValidation::validateIpAddress( $this->getPortalIp() ) ) {

    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'portal_ip', 'Invalid Portal IP entered.' ) );
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL, $boolIsInsert = false ) {

        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valHardwareId();
        		$boolIsValid &= $this->valPsProductId( $objDatabase );
        		$boolIsValid &= $this->valPortalIp( $objDatabase );
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>