<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CDeploymentProducts
 * Do not add any new functions to this class.
 */

class CDeploymentProducts extends CBaseDeploymentProducts {

	public static function fetchAllDeploymentProducts( $objDatabase ) {
		$strSql = 'SELECT * FROM deployment_products WHERE is_git_hosted = 1 AND is_deployed = 1';

		return self::fetchDeploymentProducts( $strSql, $objDatabase );
	}

}
?>