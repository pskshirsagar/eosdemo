<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClusterRepositories
 * Do not add any new functions to this class.
 */

class CClusterRepositories extends CBaseClusterRepositories {

    public static function fetchClusterRepositories( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CClusterRepository', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchClusterRepository( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CClusterRepository', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchAllClusterRepositories( $objDatabase ) {

    	$strSql = 'SELECT
		    			*
				   	FROM
		    			cluster_repositories';

    	return self::fetchObjects( $strSql, 'CClusterRepository', $objDatabase );
    }

    public static function fetchClusterRepositoriesByIds( $arrintClusterRepositoryIds, $objDatabase ) {

    	$strSql = 'SELECT * FROM cluster_repositories WHERE id IN( ' . implode( ',', $arrintClusterRepositoryIds ) . ' )';
    	return self::fetchObjects( $strSql, 'CClusterRepository', $objDatabase );
    }

    public static function fetchClusterRepositoriesByClusterIds( $arrintClusterIds, $objDatabase ) {

    	$strSql = 'SELECT * FROM cluster_repositories WHERE cluster_id IN( ' . implode( ',', $arrintClusterIds ) . ' )';
    	return self::fetchObjects( $strSql, 'CClusterRepository', $objDatabase );
    }

    public static function fetchClusterRepositoriesByIsCodeBase( $boolIsCodeBase, $objDatabase ) {

    	$strSql = 'SELECT * FROM cluster_repositories WHERE is_code_base = \'' . $boolIsCodeBase . '\'';

    	return self::fetchClusterRepositories( $strSql, $objDatabase );
    }

    public static function fetchCodeBaseClusterRepositoryByClusterId( $intClusterId, $objDatabase ) {

		$strSql = 'SELECT * FROM cluster_repositories WHERE cluster_id = ' . ( int ) $intClusterId . ' AND is_code_base = true';
    	return self::fetchObject( $strSql, 'CClusterRepository', $objDatabase );
    }

    public static function fetchClusterRepositoryIdsByClusterIds( $arrintClusterIds, $objDatabase ) {
    	$strSql = 'SELECT id FROM cluster_repositories WHERE cluster_id IN( ' . implode( ',', $arrintClusterIds ) . ' )';
    	return self::fetchClusterRepositories( $strSql, $objDatabase );
    }
}
?>