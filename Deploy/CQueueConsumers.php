<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CQueueConsumers
 * Do not add any new functions to this class.
 */

class CQueueConsumers extends CBaseQueueConsumers {

	public static function fetchPublishedQueueConsumersByHardWareId( $intHardwareId, $objDatabase ) {

		$strSql = '
			SELECT
				DISTINCT( qc.id ),
				qc.*,
				qch.min_consumers_count AS min_consumers_count,
				qch.max_consumers_count AS max_consumers_count,
				qch.is_published AS is_published
			FROM
				queue_consumers qc
				JOIN queue_consumer_hardwares qch ON( qc.id = qch.queue_consumer_id AND qch.hardware_id = ' . ( int ) $intHardwareId . ' AND qch.is_published = true )
				JOIN queue_consumer_owners qco ON( qco.queue_consumer_id = qc.id );';

		return self::fetchQueueConsumers( $strSql, $objDatabase );
	}

	public static function fetchQueueConsumerByIdByHardwareId( $intQueueConsumerId, $intHardwareId, $objDatabase ) {

		$strSql = 'SELECT
						qc.*,
						qch.min_consumers_count AS min_consumers_count,
						qch.max_consumers_count AS max_consumers_count,
						qch.is_published AS is_published
					FROM
						queue_consumers qc
						JOIN queue_consumer_hardwares qch ON( qc.id = qch.queue_consumer_id AND qch.hardware_id = ' . ( int ) $intHardwareId . ' )
					WHERE
						qc.id = ' . ( int ) $intQueueConsumerId . '
					LIMIT 1;';

		return self::fetchQueueConsumer( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedQueueConsumers( $intPageNo, $intPageSize, $objDatabase, $boolIsPaymentSpecific, $arrintHardwareIds, $boolIsClusterSpecific, $arrintDependentDatabaseTypes, $boolIsLoggingQueueConsumer = NULL, $boolIsPublishedQueueConsumer = NULL, $boolIsRemoteLoggingQueueConsumer = NULL, $strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strWhere  = '';
		if( false == empty( $strOrderByField ) ) {
			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );
			if( false == empty( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY qc.basic_queue_name';
		}
		$boolIsRemoteLoggingQueueConsumer = ( true == isset( $boolIsRemoteLoggingQueueConsumer ) ) ? ( true == ( bool ) $boolIsRemoteLoggingQueueConsumer ) ? '"1"' : '"0"' : NULL;

		$strWhere .= ( false == empty( $boolIsPublishedQueueConsumer ) ) ? ' AND qch.is_published = true ' : ' ';
		$strWhere .= ( false == empty( $boolIsLoggingQueueConsumer ) ) ? ' AND qc.is_logging= true ' : ' ';
		$strWhere .= ( false == empty( $boolIsRemoteLoggingQueueConsumer ) && $boolIsRemoteLoggingQueueConsumer != '"0"' ) ? ' AND ( ( qc.details -> \'is_remote_logging_enabled\' )::jsonb <@ \'[' . $boolIsRemoteLoggingQueueConsumer . ']\' )' : ' ';
		$strWhere .= ( false == empty( $boolIsPaymentSpecific ) ) ? ' AND qc.is_payments_specific = true ' : '';
		$strWhere .= ( false == empty( $boolIsClusterSpecific ) ) ? ' AND qc.is_cluster_specific = true ' : '';
		$strWhere .= ( false == empty( $arrintDependentDatabaseTypes ) ) ? ' AND ( ( qc.details -> \'dependent_database_type_ids\' )::jsonb @> \'["' . implode( '", "', $arrintDependentDatabaseTypes ) . '"]\'' : ' ';
		$strWhere .= ( false == empty( $arrintDependentDatabaseTypes ) ) ? ' OR  ( qc.details -> \'dependent_database_type_ids\' )::jsonb <@ \'["' . implode( '", "', $arrintDependentDatabaseTypes ) . '"]\')' : ' ';
		$strWhere .= ( false == empty( $arrintHardwareIds ) ) ? ' AND qch.hardware_id IN( ' . implode( ', ', $arrintHardwareIds ) . ' )' : ' ';

		$strSql = 'SELECT
						qc.*,
						SUM( qch.min_consumers_count ) AS min_consumers_count,
						SUM( qch.max_consumers_count ) AS max_consumers_count,
						MIN( qch.hardware_id ) AS hardware_id,
						BOOL_OR( qch.is_published ) AS is_published,
						MIN( qco.employee_id ) AS employee_id
					FROM
						queue_consumers qc
						LEFT JOIN queue_consumer_hardwares qch ON qch.queue_consumer_id = qc.id
						LEFT JOIN queue_consumer_owners qco ON qco.queue_consumer_id = qc.id
					WHERE 1=1' . $strWhere . '
					GROUP BY
						qc.id' . $strOrderBy . '
					OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchQueueConsumers( $strSql, $objDatabase );
	}

	public static function fetchPaginatedQueueConsumersCount( $objDatabase, $boolIsDisabledData, $boolIsPaymentSpecific, $arrintHardwareIds, $boolIsClusterSpecific, $arrintDependentDatabaseTypes, $boolIsLoggingQueueConsumer = NULL, $boolIsPublishedQueueConsumer = NULL, $boolIsRemoteLoggingQueueConsumer = NULL ) {

		$strWhere						 = '';

		$boolIsRemoteLoggingQueueConsumer	 = ( true == isset( $boolIsRemoteLoggingQueueConsumer ) ) ? ( true == ( bool ) $boolIsRemoteLoggingQueueConsumer ) ? '"1"' : '"0"' : NULL;

		if( 1 != $boolIsDisabledData ) {
			$strWhere .= 'AND qch.is_published = true';
		}

		$strWhere .= ( false == empty( $boolIsRemoteLoggingQueueConsumer ) && $boolIsRemoteLoggingQueueConsumer != '"0"' ) ? ' AND ( ( qc.details -> \'is_remote_logging_enabled\' )::jsonb <@ \'[' . $boolIsRemoteLoggingQueueConsumer . ']\' )' : ' ';
		$strWhere .= ( false == empty( $boolIsPublishedQueueConsumer ) ) ? ' AND qch.is_published = true ' : ' ';
		$strWhere .= ( false == empty( $boolIsLoggingQueueConsumer ) ) ? ' AND qc.is_logging= true ' : ' ';
		$strWhere .= ( false == empty( $arrintHardwareIds ) ) ? ' AND qch.hardware_id IN( ' . implode( ', ', $arrintHardwareIds ) . ' )' : ' ';
		$strWhere .= ( false == empty( $boolIsPaymentSpecific ) ) ? ' AND qc.is_payments_specific = true ' : '';
		$strWhere .= ( false == empty( $boolIsClusterSpecific ) ) ? ' AND qc.is_cluster_specific = true ' : '';
		$strWhere .= ( false == empty( $arrintDependentDatabaseTypes ) ) ? ' AND ( ( qc.details -> \'dependent_database_type_ids\' )::jsonb @> \'["' . implode( '", "', $arrintDependentDatabaseTypes ) . '"]\'' : ' ';
		$strWhere .= ( false == empty( $arrintDependentDatabaseTypes ) ) ? ' OR  ( qc.details -> \'dependent_database_type_ids\' )::jsonb <@ \'["' . implode( '", "', $arrintDependentDatabaseTypes ) . '"]\')' : ' ';

		$strSql = 'SELECT
						COUNT( DISTINCT( qc.id ) )
					FROM
						queue_consumers qc
						LEFT JOIN queue_consumer_hardwares qch ON( qc.id = qch.queue_consumer_id )
						LEFT JOIN queue_consumer_owners qco ON( qco.queue_consumer_id = qc.id )
						WHERE 1=1 ' . $strWhere;

		$arrintResponse = fetchData( $strSql, $objDatabase );

    	if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
        return 0;

	}

	public static function fetchPaginatedQueueConsumersCountByHardwareIds( $arrintHardwareIds, $objDatabase ) {

		$strSql = 'SELECT
						COUNT( DISTINCT( qc.id ) )
					FROM
						queue_consumers qc
						LEFT JOIN queue_consumer_hardwares qch ON( qc.id = qch.queue_consumer_id )
						LEFT JOIN queue_consumer_owners qco ON( qco.queue_consumer_id = qc.id )
					WHERE 
						qch.hardware_id IN ( ' . implode( ',', $arrintHardwareIds ) . ' )';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;

	}

	public static function fetchQueueConsumersByIds( $arrintQueueConsumerIds, $objDatabase ) {

		if( false == valArr( $arrintQueueConsumerIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						queue_consumers
					WHERE
						id IN ( ' . implode( ',', $arrintQueueConsumerIds ) . ' ) ';

		return self::fetchQueueConsumers( $strSql, $objDatabase );

	}

	public static function fetchSearchedQueueConsumers( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSqlCondition = '';

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrFilteredExplodedSearch ) && true == is_numeric( $arrstrFilteredExplodedSearch[0] ) ) {
			$strSqlCondition = ' AND qc.id = ' . ( int ) $arrstrFilteredExplodedSearch[0];
		}

		$strSql = 'SELECT	qc.*
					FROM queue_consumers as qc
					WHERE
						1 = 1';

		if( '' != $strSqlCondition ) {
			$strSql .= $strSqlCondition;
		} else {
			$strSql .= ' AND ( ( qc.basic_queue_name ILIKE \'%' . implode( '%\' AND qc.basic_queue_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )
					OR ( qc.consumer_class_name ILIKE \'%' . implode( '%\' AND qc.consumer_class_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' ) ) order by id LIMIT 10';
		}

		return self::fetchQueueConsumers( $strSql, $objDatabase );

	}

	public static function fetchQueueConsumerByBasicQueueName( $strBasicQueueName, $objDatabase ) {

		$strSql = 'select
						*
					FROM
						queue_consumers
					WHERE
						basic_queue_name = \'' . $strBasicQueueName . '\'';

		return self::fetchQueueConsumer( $strSql, $objDatabase );
	}

	public static function fetchQueueConsumersByBasicQueueNames( $arrstrBasicQueueNames, $objDatabase ) {

		if( false == valArr( $arrstrBasicQueueNames ) ) {
			return NULL;
		}
		$strBasicQueueNames = implode( '\',\'', $arrstrBasicQueueNames );
		$strBasicQueueNames = '\'' . $strBasicQueueNames . '\'';
		$strSql = 'select
						id,
						basic_queue_name
					FROM
						queue_consumers
					WHERE
						basic_queue_name in (' . $strBasicQueueNames . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRunningQueueConsumersCountByClusterId( $intClusterId, $objDatabase ) {
		$strSql = 'SELECT
	                    qch.hardware_id,
	                    h.name,
	                    h.ip_address,
	                    count(qch.hardware_id)
					FROM
					    queue_consumers qc
					    JOIN queue_consumer_hardwares qch ON ( qc.id = qch.queue_consumer_id )
					    JOIN hardwares h ON ( h.id = qch.hardware_id ) AND h.cluster_id IN ( ' . ( int ) $intClusterId . ' ) AND h.is_published = 1 AND qch.is_published = TRUE
						GROUP BY qch.hardware_id,h.name,h.ip_address';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllQueueConsumersByDatabaseIdsByClusterId( $strFinalMaintenanceDatabaseIds, $intClusterId, $objDatabase ) {

		$strSql = 'SELECT
						qc.id,
						qc.basic_queue_name,
						qch.hardware_id,
						qc.details,
						qc.details->>\'dependent_database_type_ids\'
					FROM
						queue_consumers qc
						JOIN queue_consumer_hardwares qch ON ( qc.id = qch.queue_consumer_id )
						JOIN hardwares h ON ( h.id = qch.hardware_id  )
					WHERE
						qc.details IS NOT NULL
						AND
						h.cluster_id = ' . ( int ) $intClusterId . '
						AND
						(qc.details->>\'dependent_database_type_ids\')::JSONB ?| ARRAY[' . $strFinalMaintenanceDatabaseIds . ']';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchQueueConsumerDetailsByQueueName( $strQueueName, $objDatabase ) {

		$strSql = 'SELECT details from queue_consumers WHERE basic_queue_name = \'' . ( string ) $strQueueName . '\'';
		return self::fetchColumn( $strSql, 'details', $objDatabase );
	}

	public static function fetchAllUnpublishedQueueConsumers( $objDatabase ) {
		$strSql = 'SELECT
						qc.id,
						qc.basic_queue_name,
						qch.hardware_id,
						qch.is_published
	               FROM
	                    queue_consumers qc
	                    JOIN queue_consumer_hardwares qch ON ( qc.id = qch.queue_consumer_id )
	               WHERE
	                    qch.is_published = FALSE';
		return fetchData( $strSql, $objDatabase );
	}

}
?>
