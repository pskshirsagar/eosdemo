<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CHardwares
 * Do not add any new functions to this class.
 */

class CHardwares extends CBaseHardwares {

	public static function fetchAllHardwares( $objDatabase, $strField = NULL ) {
		if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;

		$strOrderBy = ( false == is_null( $strField ) ) ? addslashes( $strField ) : 'id DESC';

		$strSql = 'SELECT * FROM hardwares WHERE deleted_by IS NULL AND deleted_on IS NULL ORDER BY ' . $strOrderBy;

		return self::fetchHardwares( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedHardwaresBySerachCrieteria( $intPageNo, $intPageSize, $arrstrSerchCriteria, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		if( false == is_null( $strOrderByField ) ) {

			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY h.updated_on DESC';
		}

		if( false != $arrstrSerchCriteria ) {

			if( 0 < strlen( $arrstrSerchCriteria['ip_address'] ) ) {
				$strIPAddress = ' AND h.ip_address = \'' . $arrstrSerchCriteria['ip_address'] . '\'';
			}

			if( 0 < strlen( $arrstrSerchCriteria['cluster_id'] ) ) {
				$strClusterId = ' AND h.cluster_id = ' . $arrstrSerchCriteria['cluster_id'];
			}

			if( 0 < strlen( $arrstrSerchCriteria['machine_type_id'] ) ) {
				$strClusterId = ' AND h.machine_type_id = ' . $arrstrSerchCriteria['machine_type_id'];
			}

			if( 0 < strlen( $arrstrSerchCriteria['hardware_type_id'] ) ) {
				$strHardwareTypeId = ' AND h.hardware_type_id = ' . $arrstrSerchCriteria['hardware_type_id'];
			}

			if( 0 < strlen( $arrstrSerchCriteria['hardware_status_type_id'] ) ) {
				$strHardwareStatusTypeId = ' AND h.hardware_status_type_id = ' . $arrstrSerchCriteria['hardware_status_type_id'];
			}

			if( 0 < strlen( $arrstrSerchCriteria['hardware_product_id'] ) ) {
				$strHardwareProductId = ' AND hp.ps_product_id = ' . $arrstrSerchCriteria['hardware_product_id'];
			}

			if( 0 < strlen( $arrstrSerchCriteria['environment_type_id'] ) ) {
				$strEnvironmentTypeId = ' AND h.environment_type_id = ' . $arrstrSerchCriteria['environment_type_id'];
			}

		}
		$strSql = 'SELECT
		    			h.*,
						cl.name As cluster_name,
						ht.name AS hardware_type_name,
						to_char ( h.expiry_date - now(), \'FMDDD\' ) AS expiry_status,
						mt.name As machine_name
				   FROM
		    			hardwares AS h
						LEFT JOIN clusters As cl ON( h.cluster_id = cl.id )
						LEFT JOIN hardware_types AS ht ON( h.hardware_type_id = ht.id )
						LEFT JOIN machine_types As mt ON( h.machine_type_id = mt.id )';
						if( 0 < strlen( $arrstrSerchCriteria['hardware_product_id'] ) ) {
							$strSql .= ' LEFT JOIN hardware_products AS hp ON( h.id = hp.hardware_id )';
						}

					$strSql .= 'WHERE
						h.deleted_by IS NULL
		    			AND h.deleted_on IS NULL ' . $strIPAddress . $strClusterId . $strHardwareTypeId . $strHardwareStatusTypeId . $strEnvironmentTypeId;
		    			if( 0 < strlen( $arrstrSerchCriteria['hardware_product_id'] ) ) {
		    				$strSql .= $strHardwareProductId;
		    			}

					$strSql .= $strOrderBy . ' OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchHardwares( $strSql, $objDatabase );
	}

	public static function fetchHardwaresByIds( $arrintHardwareIds, $objDatabase ) {
		if( false == valArr( $arrintHardwareIds ) ) return NULL;
		return self::fetchHardwares( 'SELECT * FROM hardwares WHERE id IN ( ' . implode( ',', $arrintHardwareIds ) . ' )', $objDatabase );
	}

	public static function fetchPublishedHardwaresByHardwareTypeIdsByClusterIds( $arrintHardwareTypeIds, $intHardwareStatusTypeId, $arrintClusterIds, $objDeployDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						hardwares
					WHERE
						is_published = 1
					AND
						cluster_id IN (' . implode( ', ', $arrintClusterIds ) . ')
					AND
						hardware_status_type_id = ' . ( int ) $intHardwareStatusTypeId . '
					AND
						environment_type_id = ' . ( int ) CEnvironmentType::PRODUCTION . '
					AND
						hardware_type_id IN (' . implode( ', ', $arrintHardwareTypeIds ) . ')
					ORDER BY
						name, cluster_id';

		return self::fetchHardwares( $strSql, $objDeployDatabase );
	}

	public static function fetchUniqueHardwaresDetailsByIpAddress( $strIpAddress, $objConnectDatabase ) {

		$strSql = 'SELECT
					    DISTINCT h.ip_address,
					    h.name,
					    c.Name AS cluster_name
					FROM
					    hardwares AS h
					    JOIN clusters AS c ON ( c.id = h.cluster_id )
					WHERE
					    h.ip_address IN ( ' . $strIpAddress . ' )
					ORDER BY
					    c.name ';

		return fetchData( $strSql, $objConnectDatabase );
	}

	public static function fetchHardwaresByDescription( $strDescription, $objConnectDatabase ) {
		$strSql = 'SELECT * FROM hardwares WHERE description ILIKE \'%' . $strDescription . '%\'';
		return self::fetchHardwares( $strSql, $objConnectDatabase );
	}

	public static function fetchHardwaresByHardwareTypeIds( $arrintHardwareTypeIds, $intHardwareStatusTypeId = NULL, $objConnectDatabase ) {

		if( 0 < $intHardwareStatusTypeId ) {
			$strSql = 'SELECT * FROM hardwares WHERE hardware_status_type_id = ' . ( int ) $intHardwareStatusTypeId . ' AND environment_type_id = ' . ( 'prodcution' == CONFIG_ENVIRONMENT ? 1 : 2 ) . ' AND hardware_type_id IN (  ' . implode( ',', $arrintHardwareTypeIds ) . ');';
		} else {
			$strSql = 'SELECT * FROM hardwares WHERE hardware_type_id IN ( ' . implode( ',', $arrintHardwareTypeIds ) . ' ) Order By name';
		}

		return self::fetchHardwares( $strSql, $objConnectDatabase );
	}

	public static function fetchHardwaresByIpAddresses( $arrstrIpAdresses, $objConnectDatabase, $intHardwareTypeId = NULL, $boolIsPublished = false ) {
		$strWhereCondition = '';

		if( true == $boolIsPublished ) {
			$strWhereCondition .= ' And is_published = 1';
		}

		if( false == empty( $intHardwareTypeId ) ) {
			$strWhereCondition .= ' And hardware_type_id = ' . ( int ) $intHardwareTypeId;
		}

		$strSql = 'SELECT * FROM hardwares WHERE ip_address IN ( \'' . implode( '\',\'', $arrstrIpAdresses ) . '\')' . $strWhereCondition;

		return self::fetchHardwares( $strSql, $objConnectDatabase );
	}

	public static function fetchSearchedHardwares( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
					    h.*,
						ht.name as hardware_type_name,
						cl.name as cluster_name
					FROM
					    hardwares As h
						LEFT JOIN hardware_types AS ht ON ( h.hardware_type_id = ht.id )
						LEFT JOIN clusters As cl ON( h.cluster_id = cl.id )
					WHERE
					    h.deleted_by IS NULL
					    AND h.deleted_on IS NULL
					    AND (
							 h.name ILIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
							 OR ht.name ILIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
							 OR h.name ILIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
						     OR ht.name ILIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
						     OR h.ip_address LIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
						     OR h.ip_address LIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
						     OR cl.name LIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
						     OR h.name LIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
							)
					ORDER BY h.name
					LIMIT 15';

		return self::fetchHardwares( $strSql, $objDatabase );
	}

	public static function fetchPublishedHardwaresByHardwareStatusTypeIdByClusterIdsByHardwareTypeIds( $intHardwareStatusTypeId, $arrintClusterIds, $arrintHardwareTypeIds, $objConnectDatabase ) {
		$strSql = 'SELECT * FROM hardwares WHERE is_published = 1 AND cluster_id IN(  ' . implode( ',', $arrintClusterIds ) . ' ) AND  hardware_status_type_id = ' . ( int ) $intHardwareStatusTypeId . ' AND environment_type_id = ' . CEnvironmentType::PRODUCTION . ' AND hardware_type_id IN(  ' . implode( ',', $arrintHardwareTypeIds ) . ');';
		return self::fetchHardwares( $strSql, $objConnectDatabase );
	}

	public static function fetchPublishedHardwaresByHardwareStatusTypeIdByEnvironmentTypesByClusterIdsByHardwareTypeIds( $intHardwareStatusTypeId, $arrintEnvironmentTypes, $arrintClusterIds, $arrintHardwareTypeIds, $objConnectDatabase, $boolIsFreeSwitch = NULL ) {
		$strSqlFreeSwitchCondition = '';
		if( true == $boolIsFreeSwitch ) {
			$strSqlFreeSwitchCondition .= ' AND name LIKE \'%ln-fscript%\' ';
		}
	    $strSql = 'SELECT * FROM hardwares WHERE is_published = 1 AND cluster_id IN(  ' . implode( ',', $arrintClusterIds ) . ' ) AND  hardware_status_type_id = ' . ( int ) $intHardwareStatusTypeId . ' AND environment_type_id IN(  ' . implode( ',', $arrintEnvironmentTypes ) . ' ) AND hardware_type_id IN(  ' . implode( ',', $arrintHardwareTypeIds ) . ') AND name NOT LIKE \'%-tst%\' ' . $strSqlFreeSwitchCondition . ' ORDER BY hardware_type_id, cluster_id;';
	    return self::fetchHardwares( $strSql, $objConnectDatabase );
	}

	public static function fetchVmWareDetails( $objDatabase ) {

		$strSql = 'SELECT
					    h.id,
						h.name,
						h.ip_address,
						h.is_published,
						h.hardware_type_id,
						h.parent_hardware_id,
						ht.name AS hardware_type,
						cl.name AS cluster_name
					FROM
					    hardwares h
					    LEFT JOIN hardware_types ht ON ( h.hardware_type_id = ht.id )
						LEFT JOIN clusters cl ON ( h.cluster_id = cl.id )
					WHERE
					    ( h.hardware_type_id = ' . CHardwareType::ESXI_SERVER . '
					    OR h.parent_hardware_id IS NOT NULL )
					    AND h.deleted_on IS NULL
					    AND h.is_published = 1
					ORDER BY
					    h.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchHardwaresWithEpiryStatusByStatusType( $strStatusType, $objDatabase ) {

		if( true == empty( $strStatusType ) ) return;

		switch( $strStatusType ) {

			case 'both':
				$strConditionSql = ' AND CAST( coalesce( to_char ( expiry_date - now(), \'FMDDD\' ), \'0\') AS integer) < 61 ';
				break;

			case 'expiry_within_60':
				$strConditionSql = ' AND CAST( coalesce( to_char ( expiry_date - now(), \'FMDDD\' ), \'0\') AS integer) Between 0 AND 60 ';
				break;

			case 'expired':
				$strConditionSql = ' AND CAST( coalesce( to_char ( expiry_date - now(), \'FMDDD\' ), \'0\') AS integer) < 0 ';
				break;

			default:
				$strConditionSql = '';
		}

		$strSql = 'SELECT
					    *,
					    CAST( coalesce( to_char ( expiry_date - now(), \'FMDDD\' ), \'0\') AS integer) AS expiry_status
					FROM
					    hardwares
					WHERE
					    deleted_by IS NULL
					    AND deleted_on IS NULL
					    AND expiry_date IS NOT NULL ' .
					$strConditionSql
					. 'Order by
					      expiry_status ASC';

		return self::fetchHardwares( $strSql, $objDatabase );
	}

	public static function fetchHardwareExpiryStatusById( $intHardwareId, $objDatabase ) {

		$strSql = 'SELECT
					    id,
					    to_char ( expiry_date - now() , \'FMDDD\' ) AS expiry_status
					FROM
					    hardwares
					WHERE
					    id = ' . ( int ) $intHardwareId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchHardwareByName( $strHardwareName,  $objDatabase ) {

		$strSql = 'SELECT * FROM hardwares WHERE name ILIKE \'' . $strHardwareName . '\'';

		return self::fetchHardware( $strSql, $objDatabase );
	}

	public static function fetchPaginatedHardwaresCountBySerachCrieteria( $arrstrSerchCriteria, $objDatabase ) {

		if( false != $arrstrSerchCriteria ) {

			if( 0 < strlen( $arrstrSerchCriteria['ip_address'] ) ) {
				$strIPAddress = ' AND h.ip_address = \'' . $arrstrSerchCriteria['ip_address'] . '\'';
			}

			if( 0 < strlen( $arrstrSerchCriteria['cluster_id'] ) ) {
				$strClusterId = ' AND h.cluster_id = ' . $arrstrSerchCriteria['cluster_id'];
			}

			if( 0 < strlen( $arrstrSerchCriteria['hardware_type_id'] ) ) {
				$strHardwareTypeId = ' AND h.hardware_type_id = ' . $arrstrSerchCriteria['hardware_type_id'];
			}

			if( 0 < strlen( $arrstrSerchCriteria['hardware_status_type_id'] ) ) {
				$strHardwareStatusTypeId = ' AND h.hardware_status_type_id = ' . $arrstrSerchCriteria['hardware_status_type_id'];
			}

			if( 0 < strlen( $arrstrSerchCriteria['hardware_product_id'] ) ) {
				$strHardwareProductId = ' AND hp.ps_product_id = ' . $arrstrSerchCriteria['hardware_product_id'];
			}

			if( 0 < strlen( $arrstrSerchCriteria['environment_type_id'] ) ) {
				$strEnvironmentTypeId = ' AND h.environment_type_id = ' . $arrstrSerchCriteria['environment_type_id'];
			}

		}
		$strSql = 'SELECT
		    			count( h.*)
				   FROM
		    			hardwares AS h
						LEFT JOIN clusters As cl ON( h.cluster_id = cl.id )
						LEFT JOIN hardware_types AS ht ON( h.hardware_type_id = ht.id )';
						if( 0 < strlen( $arrstrSerchCriteria['hardware_product_id'] ) ) {
							$strSql .= ' LEFT JOIN hardware_products AS hp ON( h.id = hp.hardware_id )';
						}

					$strSql .= ' WHERE
						h.deleted_by IS NULL
		    			AND h.deleted_on IS NULL ' . $strIPAddress . $strClusterId . $strHardwareTypeId . $strHardwareStatusTypeId . $strEnvironmentTypeId;

					if( 0 < strlen( $arrstrSerchCriteria['hardware_product_id'] ) ) {
						$strSql .= $strHardwareProductId;
					}

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchHardwareData( $objDatabase ) {

		if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;

		$strSql = 'SELECT
						name,
						id
					FROM
						hardwares
					WHERE
						deleted_by IS NULL
						AND deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSearchHardwares( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSearch = array_shift( $arrstrFilteredExplodedSearch );

		$strSql = 'SELECT
						h.id,
						h.name
					FROM
						hardwares as h
					WHERE
						h.deleted_by IS NULL
						AND h.deleted_on IS NULL
						AND (
							 h.name ILIKE \'%' . $strSearch . '%\'
							)
					ORDER BY ' . $objDatabase->getCollateSort( 'h.name' ) . '
					LIMIT 10';

		return self::fetchHardwares( $strSql, $objDatabase );
	}

	public static function fetchPublishedHardwaresByIdsByClusterIds( $arrintHardwareIds, $intClusterId, $objDatabase ) {

		$strSql = 'SELECT * FROM hardwares WHERE cluster_id = ' . ( int ) $intClusterId . ' AND id IN ( ' . implode( ',', $arrintHardwareIds ) . ' ) AND is_published = 1';

		return self::fetchHardwares( $strSql, $objDatabase );
	}

	public static function fetchPublishedHardwaresByIdsByClusterIdsWithMiscellaneousHardwares( $arrintHardwareIds, $intClusterId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						hardwares
					WHERE
						cluster_id = ' . ( int ) $intClusterId . '
						AND id IN ( ' . implode( ',', $arrintHardwareIds ) . ' )
						AND is_published = 1
						AND name NOT LIKE \'%-tst%\'
						AND environment_type_id != ' . CEnvironmentType::DR_REPLICATION;

		return self::fetchHardwares( $strSql, $objDatabase );
	}

	public static function fetchHardwaresByHardwareTypeIdByEnvironmentTypeId( $intHardwareTypeId, $intEnvironmentTypeId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						hardwares
					WHERE
						hardware_type_id = ' . ( int ) $intHardwareTypeId .
						' AND environment_type_id = ' . ( int ) $intEnvironmentTypeId;

		return self::fetchHardwares( $strSql, $objDatabase );
	}

	public static function fetchHardwaresDetailByIpAddress( $strIpAddress, $objDatabase ) {

	 $strSql = 'SELECT * FROM hardwares	WHERE ip_address = \'' . trim( $strIpAddress ) . '\'';

	return self::fetchHardware( $strSql, $objDatabase );
 	}

 	public static function fetchPublishedHardwareByIpAddressByClusterIdByHardwareStatusTypeId( $strIpAddress, $intClusterId, $intHardwareStatusTypeId, $objDatabase ) {
 		$strSql = 'SELECT * FROM hardwares WHERE is_published = 1 AND cluster_id =' . ( int ) $intClusterId . ' AND  hardware_status_type_id = ' . ( int ) $intHardwareStatusTypeId . ' AND environment_type_id in ( ' . CEnvironmentType::PRODUCTION . ', ' . CEnvironmentType::DR_REPLICATION . ' ) AND ip_address = \'' . trim( $strIpAddress ) . '\'';
 		return self::fetchHardware( $strSql, $objDatabase );
 	}

 	public static function fetchHardwareByIpAddressByClusterId( $strIpAddress, $intClusterId, $objDatabase ) {

 		$strSql = 'SELECT
 						*
 					FROM
 						hardwares
 					WHERE
 					    cluster_id = ' . ( int ) $intClusterId . '
 						AND environment_type_id = ' . CEnvironmentType::PRODUCTION . '
 						AND ip_address = \'' . trim( $strIpAddress ) . '\'';

 		return self::fetchHardware( $strSql, $objDatabase );
 	}

 	Public static function fetchHardwaresWithoutMiscellaneousHardwares( $objDatabase, $strField = NULL ) {

 		if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;

 		$strOrderBy = ( false == is_null( $strField ) ) ? addslashes( $strField ) : 'id DESC';

 		$strSql = 'SELECT
 						*
 					FROM
 						hardwares
 					WHERE
 						deleted_by IS NULL
 						AND deleted_on IS NULL
 						AND is_miscellaneous = 0
 					ORDER BY ' . $strOrderBy;

 		return self::fetchHardwares( $strSql, $objDatabase );
 	}

 	Public static function fetchPaginatedHardwaresByMachineTypeIds( $intPageNo, $intPageSize, $arrintMachineTypeIds, $objDatabase, $arrstrInstanceIds = NULL ) {

 		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
 		$intLimit = ( int ) $intPageSize;

 		if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
 		$strWhere = '';
 		$strWhere .= ( false == is_null( $arrintMachineTypeIds ) ) ? ' AND machine_type_id IN ( ' . implode( ',', $arrintMachineTypeIds ) . ' ) ' : ' ';
 		$strWhere .= ( false == is_null( $arrstrInstanceIds ) ) ? ' AND instance_id IN ( \'' . implode( '\' , \'', $arrstrInstanceIds ) . '\' ) ' : ' ';

 		$strSql = 'SELECT
 						*
 					FROM
 						hardwares
 					WHERE
 						deleted_by IS NULL
 						AND deleted_on IS NULL
 						AND instance_id IS NOT NULL'
 						. $strWhere .
 					'OFFSET ' . ( int ) $intOffset .
 					' LIMIT ' . $intLimit;

 		return self::fetchHardwares( $strSql, $objDatabase );
 	}

 	public static function fetchPaginatedHardwaresCount( $arrintMachineTypeIds, $objDatabase, $arrstrInstanceIds = NULL ) {

 		$strWhere = '';
 		$strWhere .= ( false == is_null( $arrintMachineTypeIds ) ) ? ' AND machine_type_id IN ( ' . implode( ',', $arrintMachineTypeIds ) . ' ) ' : ' ';
 		$strWhere .= ( false == is_null( $arrstrInstanceIds ) ) ? ' AND instance_id IN ( \'' . implode( '\' , \'', $arrstrInstanceIds ) . '\' ) ' : ' ';

 		$strSql = 'SELECT
 						count(*)
 					FROM
 						hardwares
 					WHERE
 						deleted_by IS NULL
 						AND deleted_on IS NULL
 						AND instance_id IS NOT NULL'
 				. $strWhere;

 		$arrintResponse = fetchData( $strSql, $objDatabase );

 		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

 		return 0;

 	}

 	public static function fetchHardwaresByMachineTypeIds( $arrintMachineTypeIds, $objDatabase, $arrstrInstanceIds = NULL ) {

 		$strWhere .= ( false == is_null( $arrintMachineTypeIds ) ) ? ' AND machine_type_id IN ( ' . implode( ',', $arrintMachineTypeIds ) . ' ) ' : ' ';
 		$strWhere .= ( false == is_null( $arrstrInstanceIds ) ) ? ' AND instance_id IN ( \'' . implode( '\' , \'', $arrstrInstanceIds ) . '\' ) ' : ' ';

 		$strSql = 'SELECT
 						*
 					FROM
 						hardwares
 					WHERE
 						deleted_by IS NULL
 						AND deleted_on IS NULL' . $strWhere;

 		return self::fetchHardwares( $strSql, $objDatabase );

 	}

 	public static function fetchHardwaresByInstanceIds( $arrstrInstanceIds, $objDatabase ) {

 		$strWhere .= ( false == is_null( $arrstrInstanceIds ) ) ? ' AND instance_id IN ( \'' . implode( '\' , \'', $arrstrInstanceIds ) . '\' ) ' : ' ';

 		$strSql = 'SELECT
 						*
 					FROM
 						hardwares
 					WHERE
 						deleted_by IS NULL
 						AND deleted_on IS NULL' . $strWhere;

 		return self::fetchHardwares( $strSql, $objDatabase );

 	}

 	public static function fetchHardwareByNameByInstanceId( $strInstanceId, $objDatabase ) {

 		return self::fetchHardware( sprintf( 'SELECT * FROM hardwares WHERE instance_id = \'%s\'', $strInstanceId ), $objDatabase );
 	}

	public static function fetchInstanceIdByIds( $arrintHardwaresIds, $objDatabase ) {
		if( false == valArr( $arrintHardwaresIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id,
						instance_id
					FROM
						hardwares
					WHERE
						deleted_by IS NULL
						AND deleted_on IS NULL
						AND instance_id IS NOT NULL
						AND id IN ( ' . implode( ',', $arrintHardwaresIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	// This function is temp, as on Rapid track servers are incompatible. Linux version.

	public static function fetchPublishedQueueConsumerHardwaresByHardwareTypeIdsByClusterIds( $arrintHardwareTypeIds, $intHardwareStatusTypeId, $arrintClusterIds, $objDeployDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						hardwares
					WHERE
						is_published = 1
					AND
						id NOT IN ( ' . implode( ', ', CHardware::$c_arrstrRapidQueueConsumerHardware ) . ' )
					AND
						cluster_id IN ( ' . implode( ', ', $arrintClusterIds ) . ' )
					AND
						hardware_status_type_id = ' . ( int ) $intHardwareStatusTypeId . '
					AND
						environment_type_id = ' . ( int ) CEnvironmentType::PRODUCTION . '
					AND
						hardware_type_id IN ( ' . implode( ', ', $arrintHardwareTypeIds ) . ' )
					ORDER BY
						name, cluster_id';

		return self::fetchHardwares( $strSql, $objDeployDatabase );
	}

	public static function fetchHardwaresByScriptIdByClusterId( $strScriptId, $intClusterId, $objDeployDatabase ) {

		if( false == valStr( $strScriptId ) || false == valId( $intClusterId ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						*
					FROM
						hardwares
					WHERE
						is_published = 1
						AND cluster_id = ' . ( int ) $intClusterId . '
						AND hardware_status_type_id = ' . CHardwareStatusType::LIVE . '
						AND environment_type_id = ' . CEnvironmentType::PRODUCTION . '
						AND hardware_type_id = ' . CHardwareType::SCRIPT_SERVER . '
						AND name NOT LIKE \'%-tst%\'
						AND ip_address NOT IN (
											SELECT
												d.description
											FROM
												deployments d
											WHERE
												d.parameters = \'' . $strScriptId . '\'
												AND d.failed_on IS NULL
												AND d.end_datetime IS NULL
												AND d.created_on::date = now()::date
						)';

		return self::fetchHardwares( $strSql, $objDeployDatabase );
	}

	public static function fetchPublishedHardwareByHardwareStatusTypeIdByEnvironmentTypesByProductIdByClusterIdByHardwareTypeIds( $intHardwareStatusTypeId, $arrintEnvironmentTypes, $intProductId, $intClusterId, $arrintHardwareTypeIds, $objDeployDatabase ) {

		$strSql = 'SELECT 
						ip_address
					FROM 
						hardwares
					WHERE id IN (
								SELECT hardware_id FROM hardware_products WHERE ps_product_id = ' . ( int ) $intProductId . '
						) 
					AND is_published = 1
					AND cluster_id = ' . ( int ) $intClusterId . '
					AND hardware_status_type_id = ' . ( int ) $intHardwareStatusTypeId . '
					AND environment_type_id IN ( ' . implode( ',', $arrintEnvironmentTypes ) . ' )
					AND hardware_type_id IN ( ' . implode( ',', $arrintHardwareTypeIds ) . ' )
					AND name NOT LIKE \'%-tst%\'
					AND deleted_by IS NULL 
					AND deleted_on IS NULL';

		return fetchData( $strSql, $objDeployDatabase );
	}

	public static function fetchPublishedStageHardwaresByEnvironmentTypeIds( $arrintEnvironmentTypeId, $objDatabase ) {

		$strSql = 'SELECT
 						*
 					FROM
 					    hardwares
 					WHERE 
 					    is_published = 1 
 					AND 
 					    enable_code_push = 1 
 					AND 
 					    environment_type_id IN (' . implode( ',', $arrintEnvironmentTypeId ) . ')
 					AND 
 					    deleted_by IS NULL 
					AND 
						deleted_on IS NULL';

		return self::fetchHardwares( $strSql, $objDatabase );

	}

}
?>
