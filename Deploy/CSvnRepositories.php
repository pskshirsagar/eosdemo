<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CSvnRepositories
 * Do not add any new functions to this class.
 */

class CSvnRepositories extends CBaseSvnRepositories {

	public static function fetchRespositoryBranchesByIds( $arrintRespositoryBranchIds, $objDatabase ) {

		return self::fetchSvnRepositories( sprintf( 'SELECT * FROM svn_repositories WHERE deleted_by is NULL AND id IN(%s) ', implode( ',', $arrintRespositoryBranchIds ) ), $objDatabase );
	}

	public static function fetchPaginatedRepositoryBranches( $intPageNo, $intPageSize, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'SELECT * FROM svn_repositories WHERE is_deleted = 0 AND deleted_by IS NULL AND svn_repository_type_id =' . CSvnRepositoryType::BRANCH . ' AND is_system = 0 ORDER BY is_automerge, path, id DESC OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit . ' ';
		return self::fetchSvnRepositories( $strSql, $objDatabase );
	}

	public static function fetchPaginatedRepositoryBranchesCount( $objDatabase ) {

		$strSql = 'SELECT count(id) FROM svn_repositories WHERE is_deleted = 0 AND deleted_by IS NULL AND is_system = 0 AND svn_repository_type_id = ' . CSvnRepositoryType::BRANCH;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchSvnRepositoryByName( $strName, $intRepositoryId, $objDatabase ) {
		$strSql = 'SELECT * FROM svn_repositories WHERE name = \'' . $strName . '\' AND repository_id = ' . ( int ) $intRepositoryId;
		return self::fetchSvnRepository( $strSql, $objDatabase );
	}

	public static function fetchParsedSvnRespositories( $objDatabase ) {
		$strSql = 'SELECT * FROM svn_repositories WHERE is_parsed = 1 AND deleted_by IS NULL ORDER BY id';
		return self::fetchSvnRepositories( $strSql, $objDatabase );
	}

	public static function fetchAllSvnRespositories( $objDatabase ) {
		return self::fetchSvnRepositories( 'SELECT * FROM svn_repositories ORDER BY name', $objDatabase );
	}

	public static function fetchSearchSvnRepositories( $arrstrFilteredExplodedSearch, $objDatabase ) {
		$strSql = 'SELECT id, name, path from svn_repositories where name ILIKE \'%' . $arrstrFilteredExplodedSearch . '%\'';
		return self::fetchSvnRepositories( $strSql, $objDatabase );

	}

	public static function fetchRepositoryCountByCreatedOn( $strDate, $intSvnRepositoryType, $intRepositoryId, $objDatabase ) {
		$strSql = 'SELECT count(id) FROM svn_repositories WHERE svn_repository_type_id = ' . ( int ) $intSvnRepositoryType . ' AND created_on > \'' . $strDate . '\' AND repository_id = ' . ( int ) $intRepositoryId;
		$arrintCount = fetchData( $strSql, $objDatabase );

		return $arrintCount[0]['count'];
	}

	public static function fetchSvnRepositoriesByRepositoryTypeIdByBeforeWeeks( $intRepositoryId, $intNumberOfWeeks, $objDatabase ) {
		return self::fetchSvnRepositories( sprintf( 'SELECT * FROM svn_repositories WHERE svn_repository_type_id = %d AND created_on <= ( NOW() - (INTERVAL \'' . $intNumberOfWeeks . ' WEEK\') ) AND deleted_by is NULL ', ( int ) $intRepositoryId ), $objDatabase );
	}

	public static function fetchSvnRepositoriesBySvnRepositoryTypeId( $intSvnRepositoryTypeId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						svn_repositories
					WHERE
						svn_repository_type_id = ' . ( int ) $intSvnRepositoryTypeId . '
					ORDER BY
						repository_id ASC NULLS FIRST,
						name';

		return self::fetchSvnRepositories( $strSql, $objDatabase );
	}

	public static function fetchSvnRepositoriesByNamesByRepositoryId( $strSvnRepoitoryNames, $intRepositoryId, $objDatabase ) {
		$strSql = 'SELECT * FROM svn_repositories WHERE name IN ( ' . $strSvnRepoitoryNames . ' ) AND repository_id = ' . ( int ) $intRepositoryId . 'ORDER BY id ASC';
		return self::fetchSvnRepositories( $strSql, $objDatabase );
	}

	public static function fetchSvnRepositoriesByRepositoryIds( $arrintRepositoryIds, $objDatabase ) {
		if( false == valArr( array_filter( $arrintRepositoryIds ) ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						svn_repositories
					WHERE
						id IN ( ' . implode( ',', $arrintRepositoryIds ) . ' )
					ORDER BY
						name';

		return self::fetchSvnRepositories( $strSql, $objDatabase );

	}

	public static function fetchSvnRepositoriesBySvnRepositoryTypeIdsByRepositoryIds( $intSvnRepositoryTypeIds, $arrintRepositoryIds, $objDatabase, $boolCheckIsSystem = true ) {

		$strSql = 'SELECT
					    *
					FROM
					    svn_repositories
					WHERE
					    repository_id IN ( ' . implode( ',', $arrintRepositoryIds ) . ' )
					    AND svn_repository_type_id IN ( ' . implode( ',', $intSvnRepositoryTypeIds ) . ' )
			    		AND deleted_by IS NULL';

		if( true == $boolCheckIsSystem ) {
			$strSql .= ' AND is_system = 1';
		}

		$strSql .= 'Order By id';

		return self::fetchSvnRepositories( $strSql, $objDatabase );
	}

	public static function fetchSvnRepositoriesForWebserviceByRepositoryId( $intRepositoryId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						svn_repositories
					WHERE
						svn_repository_type_id <> ' . CSvnRepositoryType::TAG . '
						AND repository_id = ' . ( int ) $intRepositoryId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND email_notifications IS NOT NULL';

		return self::fetchSvnRepositories( $strSql, $objDatabase );

	}

	public static function fetchPaginatedRepositories( $objPagination, $intSvnRepositoryTypeId, $objDatabase ) {

		$intOffset 		= ( 0 < ( $objPagination->getPageNo() ) ) ? $objPagination->getPageSize() * ( ( $objPagination->getPageNo() ) - 1 ) : 0;
		$intLimit  		= ( int ) $objPagination->getPageSize();
		$strOrderByType = ( 0 == $objPagination->getOrderByType() ) ? ' ASC ' : ' DESC ';

		$strSql = 'SELECT
						sr.id,
						sr.name,
						sr.repository_id,
						sr.path,
						sr_parent.name as parent_repository_name
					FROM
						svn_repositories sr
						LEFT JOIN svn_repositories sr_parent ON( sr.repository_id = sr_parent.id )
					WHERE
						sr.svn_repository_type_id = ' . ( int ) $intSvnRepositoryTypeId . '
					ORDER BY ' .
						$objPagination->getOrderByField() . $strOrderByType . '
					OFFSET ' .
						( int ) $intOffset . '
					LIMIT ' .
						$intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedRepositoriesCount( $intSvnRepositoryTypeId, $objDatabase ) {

		$strSql = 'SELECT
						count(id)
					FROM
						svn_repositories
				    WHERE
						svn_repository_type_id = ' . ( int ) $intSvnRepositoryTypeId;

		$arrintResult = fetchData( $strSql, $objDatabase );

		return ( true == isset( $arrintResult[0] ) ) ? $arrintResult[0]['count'] : 0;

	}

	public static function fetchSearchRepositories( $arrstrExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
						sr.id,
						sr.name,
						sr.repository_id,
						sr.path,
						sr_parent.name as parent_repository_name
					FROM
						svn_repositories sr
						LEFT JOIN svn_repositories sr_parent ON( sr.repository_id = sr_parent.id )
					WHERE
						sr.svn_repository_type_id = ' . CSvnRepositoryType::REPOSITORY . '
						AND
						(
							( sr.name ILIKE \'%' . implode( '%\' AND sr.name ILIKE \'%', $arrstrExplodedSearch ) . '%\' )
							OR ( sr.path ILIKE \'%' . implode( '%\' AND sr.path ILIKE \'%', $arrstrExplodedSearch ) . '%\' )
						)
					ORDER BY
						sr.name
					LIMIT
						20';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRepositoryDetailsByRepositoryTypeId( $intSvnRepositoryTypeId, $objDatabase ) {

		$strSql = 'SELECT
						id,
						repository_id,
						name
					FROM
						svn_repositories
					WHERE
						svn_repository_type_id = ' . ( int ) $intSvnRepositoryTypeId . '
					ORDER BY
						name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSvnRepositoriesByRepositoryIdsByNameBySvnRepositoryTypeId( $arrintRepositoryIds, $strName, $intSvnRepositoryTypeId, $objDatabase ) {

		$strSql = 'SELECT
						id,
						repository_id,
						name,
						path
					FROM
						svn_repositories
					WHERE
						repository_id IN ( ' . implode( ',', $arrintRepositoryIds ) . ' )
					AND
						name  ILIKE \'%' . $strName . '%\'
					AND
						svn_repository_type_id = ' . ( int ) $intSvnRepositoryTypeId . '
					ORDER BY repository_id  ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchParsedSvnRespositoriesByIds( $arrintSvnRespositoryIds, $objDatabase ) {

		if( false == valArr( $arrintSvnRespositoryIds ) ) return NULL;

		$strSql = ' SELECT
						id,
						name,
						path
					FROM
						svn_repositories
					WHERE
						is_parsed = 1
						AND is_system = 1
						AND svn_repository_type_id IN ( ' . CSvnRepository::PS_CORE_TRUNK . ',' . CSvnRepository::PS_BRANCHES . ' )
						AND id IN (' . implode( ',', $arrintSvnRespositoryIds ) . ')
						AND deleted_by IS NULL
					ORDER BY
						id';

		return self::fetchSvnRepositories( $strSql, $objDatabase );
	}

	public static function fetchActiveRepositoryBranches( $objDatabase ) {

		$strSql = 'SELECT * FROM svn_repositories WHERE deleted_by IS NULL ORDER BY id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllSvnRespositoriesByEmailAddress( $strEmailAddress, $objDatabase ) {
		if( false == valStr( $strEmailAddress ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						svn_repositories
					WHERE
						\'' . $strEmailAddress . '\'  = ANY ( string_to_array( email_notifications, \',\' ) ) ';

		return self::fetchSvnRepositories( $strSql, $objDatabase );
	}

	public static function fetchSvnRepositoriesByIds( $arrintSvnRespositoryIds, $objDatabase ) {
		$strSql = 'SELECT
		 				id,
						name
					FROM
						svn_repositories
					WHERE
						id IN (' . implode( ',', $arrintSvnRespositoryIds ) . ')
						AND deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSvnRepositoryNamesByDeploymentIds( $arrintDeploymentIds, $objDatabase ) {
		$strSql = 'SELECT
						d.id,sr.name
					FROM
						svn_repositories sr
					JOIN 
						deployments d ON sr.deployment_id = d.id 
					WHERE
						d.id IN (' . implode( ',', $arrintDeploymentIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSvnRepositoryNameBySvnRepositoryTypeIdsBySyncToSlave( $arrintSvnRepositoryTypeId , $objDatabase ) {

		if( false == valArr( $arrintSvnRepositoryTypeId ) ) return NULL;

		$strSql = 'SELECT 
						name
					FROM 
						svn_repositories 
					WHERE 
						svn_repository_type_id IN (' . implode( ',', $arrintSvnRepositoryTypeId ) . ' )
					AND 
						sync_to_slave = true
					AND 
						is_deleted = 0 
					AND 
						deleted_by IS NULL 
					AND 
						deleted_on IS NULL ';

		return fetchData( $strSql, $objDatabase );
	}

}
?>