<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CDeploymentDetails
 * Do not add any new functions to this class.
 */

class CDeploymentDetails extends CBaseDeploymentDetails {

    public static function fetchDeploymentDetailsByClusterRepositoryIds( $arrintClusterRepositoryIds, $objDatabase ) {

   		$strSql = 'SELECT * FROM deployment_details WHERE cluster_repository_id IN( ' . implode( ',', $arrintClusterRepositoryIds ) . ' )';
   		return self::fetchObjects( $strSql, 'CDeploymentDetail', $objDatabase );
    }

    public static function fetchDeploymentRevisionsByDeploymentIds( $arrintDeploymentIds, $objDatabase ) {

    	if( false == valArr( $arrintDeploymentIds ) ) return;
    	$strSql = 'SELECT
    					dd.deployment_id, cr.name as repository_name, dd.push_revision_id as revision_id
					FROM
    					deployment_details dd
    					JOIN cluster_repositories cr ON ( cr.id = dd.cluster_repository_id )
    				WHERE
    					dd.deployment_id IN ( ' . implode( ',', $arrintDeploymentIds ) . ' ) ';

    	return fetchData( $strSql, $objDatabase );
    }

    public static function fetchDeploymentDetailsByDeploymentIdByClusterRepositoryIsCodeBase( $intDeploymentId, $boolIsCodeBase, $objDatabase ) {

    	$strSql = 'SELECT
    					*
					FROM
    					deployment_details dd
    					JOIN cluster_repositories cr ON ( cr.id = dd.cluster_repository_id )
					WHERE
						dd.deployment_id = ' . ( int ) $intDeploymentId . '
    					AND cr.is_code_base = \'' . $boolIsCodeBase . '\'';

    	return parent::fetchObject( $strSql, 'CDeploymentDetail', $objDatabase );
    }

    public static function fetchDeploymentDetailsByDeploymentTypeIdByClusterIds( $intDeploymentId, $arrintClusterId, $objDatabase ) {

		$strSql = ' SELECT
                        * 
                    FROM 
                        deployment_details
                    WHERE
                        deployment_id IN (
                            SELECT 
                                id
                            FROM
                                deployments d
                            WHERE
                                d.deployment_type_id = ' . ( int ) $intDeploymentId . '
                                AND d.cluster_id IN ( ' . implode( ',', $arrintClusterId ) . ' )
                                AND ( d.created_on > CURRENT_TIMESTAMP - INTERVAL \'5 MINUTE\' )
                             ORDER BY d.id DESC LIMIT 1
                                ) ';

	    return parent::fetchObjects( $strSql, 'CDeploymentDetail', $objDatabase );
    }

	public static function fetchDeploymentRevisionsForGitByDeploymentIds( $arrintDeploymentIds, $objDatabase ) {

		if( false == valArr( $arrintDeploymentIds ) ) return;
		$strSql = 'SELECT
    					dd.deployment_id, cr.name as repository_name, dd.push_revision_id as revision_id
					FROM
    					deployment_details dd
    					JOIN git_cluster_repositories cr ON ( cr.id = dd.cluster_repository_id )
    				WHERE
    					dd.deployment_id IN ( ' . implode( ',', $arrintDeploymentIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

}
?>