<?php

class CResetFrequencyType extends CBaseResetFrequencyType {

	const WEEKLY 					= 1;
	const MONTHLY 					= 2;
    const BIWEEKLY                  = 3;
    const QUARTERLY                 = 4;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {

            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            default:
            	// default case
            	break;
        }

        return $boolIsValid;
    }

}
?>