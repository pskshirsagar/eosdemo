<?php

class CScriptType extends CBaseScriptType {

	const ADMIN_PHP 				= 1;
	const ENTRATA_PHP				= 2;
	const COMPANY_OPTIONAL_PHP		= 3;
	const EMAILS_PHP				= 4;
	const VOIP_PHP					= 5;
	const UTILITIES_PHP				= 6;
	const CHAT_PHP					= 7;
	const DATABASE_LOGS_PHP			= 8;
	const SMS_PHP					= 9;
	const CONNECT_PHP				= 10;
	const INSURANCE_PHP				= 11;
	const SHARED_PHP				= 12;
	const PAYMENT_PHP				= 13;
	const MIGRATION_SCRIPT_SQL		= 14;
	const SCREENING_PHP				= 16;
	const COMPETITOR_PHP			= 17;
	const MESSAGING_PHP			    = 18;
	const DEPLOY_PHP			    = 19;
	const HA_PHP			        = 20;
	const COMMON_PHP		        = 21;

	public static $c_arrintSkipScriptTypes 			= array( self::COMPANY_OPTIONAL_PHP, self::SHARED_PHP, self::MIGRATION_SCRIPT_SQL );
	public static $c_arrstrScriptMapWithDbId			= array( self::ADMIN_PHP => 'admin', self::ENTRATA_PHP => 'entrata', self::COMPANY_OPTIONAL_PHP => 'entrata', self::EMAILS_PHP => 'emails', self::VOIP_PHP => 'voip', self::UTILITIES_PHP => 'utilities', self::CHAT_PHP => 'chats', self::DATABASE_LOGS_PHP => 'logs', self::SMS_PHP => 'sms', self::CONNECT_PHP => 'connect', self::INSURANCE_PHP => 'insurance', self::SHARED_PHP => 'entrata', self::PAYMENT_PHP => 'payments', self::SCREENING_PHP => 'screening', self::COMPETITOR_PHP => 'competitor', self::MESSAGING_PHP => 'messaging', self::DEPLOY_PHP => 'deploy', self::HA_PHP => 'EntrataHA', self::COMMON_PHP => 'common' );

	public static $c_arrintScriptTypeMapWithDatabasebId = array( self::ADMIN_PHP         => CDatabase::ADMIN,
		self::EMAILS_PHP        => CDATABASE::EMAIL,
		self::VOIP_PHP          => CDATABASE::VOIP,
		self::UTILITIES_PHP     => CDATABASE::UTILITIES,
		self::CHAT_PHP          => CDATABASE::CHAT,
		self::DATABASE_LOGS_PHP => CDATABASE::LOGS,
		self::SMS_PHP           => CDATABASE::SMS,
		self::CONNECT_PHP       => CDATABASE::CONNECT,
		self::INSURANCE_PHP     => CDATABASE::INSURANCE,
		self::PAYMENT_PHP       => CDATABASE::PAYMENTS,
		self::SCREENING_PHP     => CDATABASE::SCREENING,
		self::COMPETITOR_PHP    => CDATABASE::COMPETITOR,
		self::MESSAGING_PHP     => CDATABASE::MESSAGING,
		self::DEPLOY_PHP        => CDATABASE::DEPLOY
	);

	public static $c_arrintScriptTypeMapWithDatabaseTypeId = [
		self::ADMIN_PHP => CDatabaseType::ADMIN,
		self::ENTRATA_PHP => CDatabaseType::CLIENT,
		self::COMPANY_OPTIONAL_PHP => CDatabaseType::CLIENT,
		self::EMAILS_PHP => CDatabaseType::EMAIL,
		self::VOIP_PHP => CDatabaseType::VOIP,
		self::UTILITIES_PHP => CDatabaseType::UTILITIES,
		self::CHAT_PHP => CDatabaseType::CHAT,
		self::DATABASE_LOGS_PHP => CDatabaseType::LOGS,
		self::SMS_PHP => CDatabaseType::SMS,
		self::CONNECT_PHP => CDatabaseType::CONNECT,
		self::INSURANCE_PHP => CDatabaseType::INSURANCE,
		self::SHARED_PHP => CDatabaseType::CLIENT,
		self::PAYMENT_PHP => CDatabaseType::PAYMENT,
		self::SCREENING_PHP => CDatabaseType::SCREENING,
		self::COMPETITOR_PHP => CDatabaseType::COMPETITOR,
		self::MESSAGING_PHP => CDatabaseType::MESSAGING,
		self::DEPLOY_PHP => CDatabaseType::DEPLOY,
		self::HA_PHP => CDatabaseType::HA
	];

}
?>