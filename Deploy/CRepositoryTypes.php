<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CRepositoryTypes
 * Do not add any new functions to this class.
 */

class CRepositoryTypes extends CBaseRepositoryTypes {

	public static function fetchRepositoryTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CRepositoryType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchRepositoryType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CRepositoryType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>