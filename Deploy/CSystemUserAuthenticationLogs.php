<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CSystemUserAuthenticationLogs
 * Do not add any new functions to this class.
 */

class CSystemUserAuthenticationLogs extends CBaseSystemUserAuthenticationLogs {

	public static function fetchPaginatedSystemUserAuthenticationLogsBySystemUserId( $intPageNo, $intPageSize, $intSystemUserId, $objDatabase ) {

		$intOffset   = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit    = ( int ) $intPageSize;

		$strSql = 'SELECT
					    *
					FROM
					    system_user_authentication_logs
					WHERE
					    system_user_id = ' . ( int ) $intSystemUserId . '
					ORDER BY
					    id DESC OFFSET ' . ( int ) $intOffset . '
					LIMIT
					    ' . $intLimit;

		return self::fetchSystemUserAuthenticationLogs( $strSql, $objDatabase );
	}

	public static function fetchSystemUserAuthenticationLogsCountBySystemUserId( $intSystemUserId, $objDatabase ) {

		$strWhere = 'WHERE system_user_id = ' . ( int ) $intSystemUserId;

		return self::fetchSystemUserAuthenticationLogCount( $strWhere, $objDatabase );
	}

	public static function fetchSystemUserAuthenticationLogByIdBySystemUserId( $intId, $intSystemUserId, $objDatabase ) {
		return self::fetchSystemUserAuthenticationLog( sprintf( 'SELECT * FROM system_user_authentication_logs WHERE id = %d AND system_user_id = %d', ( int ) $intId, ( int ) $intSystemUserId ), $objDatabase );
	}

}
?>