<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CHardwareProducts
 * Do not add any new functions to this class.
 */

class CHardwareProducts extends CBaseHardwareProducts {

	public static function fetchHardwareProductsByHardwareIds( $arrintHardwareIds, $objDatabase ) {
		if( false == valArr( $arrintHardwareIds ) ) return NULL;
		return self::fetchHardwareProducts( 'SELECT * FROM hardware_products WHERE hardware_id IN ( ' . implode( ',', $arrintHardwareIds ) . ' )', $objDatabase );
	}

	public static function fetchAllHardwareProducts( $objDatabase ) {
		return self::fetchHardwareProducts( 'SELECT * FROM hardware_products', $objDatabase );
	}

	public static function fetchAllPaginatedHardwareProducts( $intPageNo, $intPageSize, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$strLimit = ( int ) $intPageSize;

		if( false == is_null( $strOrderByField ) ) {
			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' DESC ';
			}
		} else {

			$strOrderBy = ' ORDER BY hp.updated_on DESC';
		}

		$strSql = ' SELECT
						hp.*,
    					h.name AS hardware_name
					FROM
						hardware_products AS hp
						JOIN hardwares AS h ON ( h.id = hp.hardware_id AND h.deleted_by IS NULL AND h.deleted_on IS NULL )
					WHERE h.is_published = 1 '
				. $strOrderBy . ' OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . $strLimit;

		return parent::fetchHardwareProducts( $strSql, $objDatabase );
	}

	public static function fetchHardwareProductPublishedHardareCount( $objDatabase ) {

		$strSql = 'SELECT
					    count ( * )
					FROM
					    hardware_products AS hp
					    JOIN hardwares AS h ON ( h.id = hp.hardware_id AND h.deleted_by IS NULL AND h.deleted_on IS NULL )
					WHERE
					    h.is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchHardwareProductsByIds( $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;
		return self::fetchHardwareProducts( 'SELECT * FROM hardware_products WHERE id IN ( ' . implode( ',', $arrintIds ) . ' )', $objDatabase );
	}

	public static function fetchSearchedHardwareProducts( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
						hp.*,
						h.name AS hardware_name
					FROM
					    hardware_products AS hp
						LEFT JOIN hardwares AS h ON( hp.hardware_id = h.id )
					WHERE
					    h.deleted_by IS NULL
					    AND h.deleted_on IS NULL
						AND h.is_published = 1
					    AND
							(
							 h.name ILIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
							 OR h.name ILIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
							 OR hp.load_balancer_cookie_value ILIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
							 OR hp.load_balancer_cookie_value ILIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
						     OR hp.portal_ip LIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
						     OR hp.portal_ip LIKE \'%' . implode( ' ', $arrstrFilteredExplodedSearch ) . '%\'
							)
					ORDER BY h.name
					LIMIT 15';

		return self::fetchHardwareProducts( $strSql, $objDatabase );
	}

	public static function fetchHardwareProductsByPsProductIds( $arrintPsProductIds, $objDatabase ) {
		if( false == valArr( $arrintPsProductIds ) ) return NULL;
		$strSql = 'SELECT
						hp.*
					FROM
					    hardware_products AS hp
					LEFT JOIN hardwares AS h ON( hp.hardware_id = h.id )
					WHERE
						hp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						AND h.is_published = 1
						AND h.hardware_status_type_id = ' . CHardwareStatusType::LIVE . '
					ORDER BY
    					hp.ps_product_id';

		return self::fetchHardwareProducts( $strSql, $objDatabase );
	}

}
?>