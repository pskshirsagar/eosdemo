<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CSystemPreferences
 * Do not add any new functions to this class.
 */

class CSystemPreferences extends CBaseSystemPreferences {

	public static function fetchPaginatedSystemPreferences( $intPageNo, $intPageSize, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = '	SELECT
						*
					FROM
					    system_preferences
					ORDER BY id';

		$strSql	.= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return self::fetchSystemPreferences( $strSql, $objDatabase );
	}

	public static function fetchSystemPreferenceByKey( $strKey, $objDatabase ) {

		if( false == valStr( $strKey ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
					    system_preferences
					WHERE key = ' . '\'' . $strKey . '\'';

		return self::fetchSystemPreference( $strSql, $objDatabase );
	}

	public static function putSystemPreference( $strKey, $strValue, $objDatabase, $intCurrentUserId = CUser::ID_SYSTEM, $boolIsEncryptValue = false ) {

		if( false == valStr( $strKey ) || true == is_null( $strValue ) ) return false;

		$objSystemPreference 	= self::fetchSystemPreferenceByKey( $strKey, $objDatabase );
		$boolIsSuccess 			= true;

		if( false == valObj( $objSystemPreference, 'CSystemPreference' ) ) {
			$objSystemPreference = new CSystemPreference();
			$objSystemPreference->setKey( $strKey );
		}

		$objSystemPreference->setValue( ( true == $boolIsEncryptValue ) ? CEncryption::encryptText( $strValue, CONFIG_KEY_LOGIN_PASSWORD, true ) : $strValue );

		if( false == is_numeric( $objSystemPreference->getId() ) ) {
			$boolIsSuccess &= $objSystemPreference->insert( $intCurrentUserId, $objDatabase );
		} else {
			$boolIsSuccess &= $objSystemPreference->update( $intCurrentUserId, $objDatabase );
		}

		return $boolIsSuccess;
	}

	public static function getSystemPreference( $strKey, $objDatabase, $boolIsDecryptValue = false ) {

		if( false == valStr( $strKey ) ) return false;

		$objSystemPreference = self::fetchSystemPreferenceByKey( $strKey, $objDatabase );

		if( true == valObj( $objSystemPreference, 'CSystemPreference' ) ) {
			return ( true == $boolIsDecryptValue ) ? CEncryption::decryptText( $objSystemPreference->getValue(), CONFIG_KEY_LOGIN_PASSWORD ) : $objSystemPreference->getValue();
		}

		return false;
	}

}
?>