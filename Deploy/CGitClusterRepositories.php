<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CGitClusterRepositories
 * Do not add any new functions to this class.
 */

class CGitClusterRepositories extends CBaseGitClusterRepositories {

	public static function fetchGitClusterRepositoriesByClusterIdByProductId( $intClusterId, $intProductId, $objDatabase ) {
		return parent::fetchGitClusterRepositories( 'SELECT * FROM git_cluster_repositories WHERE cluster_id = ' . ( int ) $intClusterId . ' AND ps_product_id = ' . ( int ) $intProductId, $objDatabase );
	}

	public static function fetchCustomGitClusterRepositoriesByClusterId( $intClusterId, $objDatabase ) {
		return parent::fetchGitClusterRepositories( 'SELECT * FROM git_cluster_repositories WHERE cluster_id = ' . ( int ) $intClusterId . ' AND ps_product_id IS NULL', $objDatabase );

	}

	public static function fetchDeployedGitClusterRepositories( $intClusterId, $objDatabase ) {
		$strSql = 'SELECT
							gcr.*
	                   FROM 
	                        deployment_products dp
	                        JOIN git_cluster_repositories gcr 
	                        ON (dp.ps_product_id = gcr.ps_product_id) 
	                   WHERE 
	                        dp.cluster_id = ' . ( int ) $intClusterId . '
							AND dp.is_git_hosted = 1 
							AND dp.is_deployed = 1';

		return parent::fetchGitClusterRepositories( $strSql, $objDatabase );
	}

	public static function fetchCodeBaseGitClusterRepositoryByClusterId( $intClusterId, $objDatabase ) {

		$strSql = 'SELECT * FROM git_cluster_repositories WHERE cluster_id = ' . ( int ) $intClusterId . ' AND is_code_base = true';
		return self::fetchObject( $strSql, 'CGitClusterRepository', $objDatabase );
	}

}
?>