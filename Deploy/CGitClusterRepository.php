<?php

class CGitClusterRepository extends CBaseGitClusterRepository {

	const PSCORE_RAPID				= 1;
	const PSCORE_COMMON_RAPID		= 2;
	const PSLIBRARY_RAPID			= 3;

	const PSCORE_STANDARD			= 4;
	const PSCORE_COMMON_STANDARD	= 5;
	const PSLIBRARY_STANDARD		= 6;

	const SYSTEM_RAPID				= 7;
	const ENTRATAWEBSITE			= 9;
	const PSPAYMENT					= 12;
	const HA						= 13;
	const ENTRATAFI					= 14;
	const PSSECURE					= 15;
	const RESIDENTINSURE			= 18;

	public static $c_arrintGitClusterRepositories = [
		self::PSCORE_RAPID 				=> 'core',
		self::PSCORE_COMMON_RAPID 		=> 'core-common',
		self::PSLIBRARY_RAPID 			=> 'libraries',

		self::PSCORE_STANDARD			=> 'core',
		self::PSCORE_COMMON_STANDARD	=> 'core-common',
		self::PSLIBRARY_STANDARD		=> 'libraries',

		self::SYSTEM_RAPID 				=> 'deployment',
		self::ENTRATAWEBSITE 			=> 'entrata',
		self::PSPAYMENT 				=> 'payments',
		self::HA 						=> 'em-services',
		self::ENTRATAFI					=> 'efi-services',
		self::PSSECURE					=> '',
		self::RESIDENTINSURE			=> 'ri-services'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClusterId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseRepositoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStageRepositoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProductionRepositoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTagPath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportPath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCodeBase() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>