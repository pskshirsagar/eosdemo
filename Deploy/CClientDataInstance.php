<?php

class CClientDataInstance extends CBaseClientDataInstance {

	protected $m_strCompanyName;
	protected $m_strCompanyTrack;
	protected $m_strRequestType;
	protected $m_strStatus;

    public function valExpiresOn() {

    	$boolIsValid = true;
    	$strCurrentDate = date( 'm/d/Y' );
    	$strExpireDate = $this->m_strExpiresOn;

    	if( true == is_null( $strExpireDate ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expires_on', 'Expires On is required.' ) );
    		$boolIsValid = false;
    	}

    	if( strtotime( $strExpireDate ) <= strtotime( $strCurrentDate ) ) {
    		if( NULL != $strExpireDate ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expires_on', 'Expiry date should be a future date.' ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function valResetOn() {

        $boolIsValid = true;
        $strCurrentDate = date( 'm/d/Y' );
        $strResetOnDate = $this->m_strResetOn;

        if( false == is_null( $strResetOnDate ) && strtotime( $strResetOnDate ) <= strtotime( $strCurrentDate ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reset_on', 'Reset on date should be a future date.' ) );
        }

        return $boolIsValid;

    }

    public function valStakeholders() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getStakeholders() ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'stakeholders', 'Stakeholders email is required.' ) );
		    return $boolIsValid;
    	}

	    $strEmailAddress = $this->getStakeholders();
	    if( false == isset( $strEmailAddress ) || ( false == CValidation::validateEmailAddresses( $strEmailAddress ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'stakeholders', 'Enter valid Email address.' ) );
		}

    	return $boolIsValid;
    }

    public function valPropertyIds() {
	    $boolIsValid = true;

	    if( true == is_null( $this->getPropertyIds() ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Property_ids', 'Select at least one property from the Properties field' ) );
		    return $boolIsValid;
	    }

	    return $boolIsValid;
    }

    public function validate( $strAction, $boolIsPropertySpecific = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valExpiresOn();
		        $boolIsValid &= $this->valStakeholders();
		        if( true == $boolIsPropertySpecific ) {
			        $boolIsValid &= $this->valPropertyIds();
		        }
		        break;

        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valExpiresOn();
        		$boolIsValid &= $this->valStakeholders();
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function setCompanyName( $strNewCompanyName ) {
    	$this->m_strCompanyName = CStrings::strTrimDef( $strNewCompanyName, 200, NULL, true );
    }

    public function getCompanyName() {
    	return $this->m_strCompanyName;
    }

    public function sqlCompanyName() {
    	return ( true == isset( $this->m_strCompanyName ) ) ? '\'' . addslashes( $this->m_strCompanyName ) . '\'' : 'NULL';
    }

    public function setCompanyTrack( $strNewCompanyTrack ) {
    	$this->m_strCompanyTrack = CStrings::strTrimDef( $strNewCompanyTrack, 200, NULL, true );
    }

    public function getCompanyTrack() {
    	return $this->m_strCompanyTrack;
    }

    public function sqlCompanyTrack() {
    	return ( true == isset( $this->m_strCompanyTrack ) ) ? '\'' . addslashes( $this->m_strCompanyTrack ) . '\'' : 'NULL';
    }

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['request_type'] ) )		$this->setRequestType( $arrmixValues['request_type'] );
		if( true == isset( $arrmixValues['status'] ) )		        $this->setStatus( $arrmixValues['status'] );

	}

	public function setRequestType( $strRequestType ) {
		$this->m_strRequestType = $strRequestType;
	}

	public function getRequestType() {
		return $this->m_strRequestType;
	}

	public function setStatus( $strStatus ) {
		$this->m_strStatus = $strStatus;
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

}
?>