<?php

class CHardwareType extends CBaseHardwareType {

	const WEB_SERVER 			= 1;
	const DATABASE_SERVER 		= 2;
	const SCRIPT_SERVER 		= 3;
	const DNS_SERVER 			= 4;
	const MAIL_SERVER 			= 5;
	const VOIP_SERVER 			= 6;
	const VM_SERVER 			= 7;
	const LOG_SERVER 			= 8;
	const NAGIOS_SERVER 		= 9;
	const BACKUP_SERVER 		= 10;
	const ESXI_SERVER 			= 11;
	const PROXY_SERVER 			= 12;
	const DOMAIN_SERVER 		= 13;
	const FILE_SERVER 			= 14;
	const SECURITY_SERVER 		= 15;
	const LOAD_BALANCER 		= 16;
	const SWAP 					= 17;
	const OTHER 				= 18;
	const WIN_MSSQL_SERVER		= 19;
	const DEPLOYMENT_SERVER  	= 20;

	// static array of published hardware types
	public static $c_arrstrHardwareTypes = array(
			self::WEB_SERVER 				=> 'Web Server',
			self::DATABASE_SERVER 			=> 'Database Server',
			self::SCRIPT_SERVER 			=> 'Script Server',
			self::DNS_SERVER 				=> 'DNS Server',
			self::MAIL_SERVER 				=> 'Mail Server',
			self::VOIP_SERVER 				=> 'VoIP Server',
			self::VM_SERVER 				=> 'VM Server',
			self::LOG_SERVER 				=> 'Log Server',
			self::NAGIOS_SERVER 			=> 'Nagios Server',
			self::BACKUP_SERVER 			=> 'Backup Server',
			self::ESXI_SERVER 				=> 'ESXi Server',
			self::PROXY_SERVER 				=> 'Proxy Server',
			self::DOMAIN_SERVER 			=> 'Domain Server',
			self::FILE_SERVER 				=> 'File Server',
			self::SECURITY_SERVER 			=> 'Security Server',
			self::LOAD_BALANCER 			=> 'Load Balancer',
			self::SWAP 						=> 'Switch',
			self::OTHER 					=> 'Other',
			self::WIN_MSSQL_SERVER 			=> 'WIN - MSSQL Server',
			self::DEPLOYMENT_SERVER 		=> 'Deployment Server'
	);

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// empty
            	break;
        }

        return $boolIsValid;
    }
}
?>