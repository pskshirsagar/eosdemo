<?php

class CScriptOwner extends CBaseScriptOwner {

	public function valEmployeeId() {
		$boolIsValid = true;
        if( true == is_null( $this->getEmployeeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'owner_id', 'Script Owner is required.' ) );
        }

        return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEmployeeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>