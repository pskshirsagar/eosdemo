<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CResetFrequencyTypes
 * Do not add any new functions to this class.
 */

class CResetFrequencyTypes extends CBaseResetFrequencyTypes {

    public static function fetchResetFrequencyTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CResetFrequencyType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchResetFrequencyType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CResetFrequencyType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchAllResetFrequencyTypes( $objDatabase ) {
    	return parent::fetchResetFrequencyTypes( 'SELECT * FROM reset_frequency_types', $objDatabase );
    }
}
?>