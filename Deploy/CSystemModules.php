<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CSystemModules
 * Do not add any new functions to this class.
 */

class CSystemModules extends CBaseSystemModules {

    public static function fetchSystemModules( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CSystemModule', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchSystemModule( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CSystemModule', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchPublishedSystemModules( $objDatabase ) {
    	return self::fetchSystemModules( 'SELECT * FROM system_modules where is_published = 1 ORDER BY order_num', $objDatabase );
    }

}
?>