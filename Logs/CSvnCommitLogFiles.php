<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSvnCommitLogFiles
 * Do not add any new functions to this class.
 */

class CSvnCommitLogFiles extends CBaseSvnCommitLogFiles {

	public static function fetchActiveSvnCommitLogFiles( $objDatabase ) {

		$strFileExtension = '.php';

		$strSql = 'SELECT
						subsql.file_path,
						subsql.user_id
					FROM (
							SELECT
								sclf.file_path,
								scl.user_id,
								rank() OVER( PARTITION BY sclf.file_path ORDER BY scl.commit_datetime, scl.created_on ASC )
							FROM
								svn_commit_logs scl
								JOIN svn_commit_log_files sclf ON ( scl.id = sclf.svn_commit_log_id )
							WHERE
								( sclf.file_path LIKE \'%' . $strFileExtension . '%\' OR sclf.file_path NOT LIKE \'%.%\' )
								AND scl.created_on >= NOW( ) - INTERVAL \'18 month\'
						) as subsql
					WHERE subsql.rank = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLastWeekSvnCommitLogFiles( $objDatabase ) {

		$strSql = ' SELECT
						svn_log.dir_file_path,
						svn_log.file_path,
						svn_log.user_id,
						svn_log.svn_repository_id,
						svn_log.id as svn_commit_log_id
					FROM (
							SELECT
								subsql.file_path,
								subsql.user_id,
								subsql.svn_repository_id,
								subsql.id,
								CASE
									WHEN subsql.svn_repository_id IN ( ' . CSvnRepository::PS_CORE . ', ' . CSvnRepository::PS_CORE_TRUNK . ', ' . CSvnRepository::PS_CORE_RAPIDSTAGE . ', ' . CSvnRepository::PS_CORE_RAPIDPRODUCTION . ', ' . CSvnRepository::PS_CORE_STANDARDSTAGE . ', ' . CSvnRepository::PS_CORE_STANDARDPRODUCTION . ' ) THEN concat(\'Entrata/\', subsql.file_path)
									WHEN subsql.svn_repository_id IN ( ' . CSvnRepository::PS_CORE_COMMON . ', ' . CSvnRepository::PS_CORE_COMMON_TRUNK . ', ' . CSvnRepository::PS_CORE_COMMON_RAPIDSTAGE . ', ' . CSvnRepository::PS_CORE_COMMON_RAPIDPRODUCTION . ', ' . CSvnRepository::PS_CORE_COMMON_STANDARDPRODUCTION . ', ' . CSvnRepository::PS_CORE_COMMON_STANDARDSTAGE . ' ) THEN concat(\'Common/\',subsql.file_path)
									END as dir_file_path
							FROM (
									SELECT
										sclf.file_path,
										scl.user_id,
										scl.svn_repository_id,
										scl.id,
										rank() OVER(PARTITION BY sclf.file_path ORDER BY scl.commit_datetime, scl.created_on ASC)
									FROM
										svn_commit_logs scl
										JOIN svn_commit_log_files sclf ON (scl.id = sclf.svn_commit_log_id)
									WHERE
										( sclf.file_path LIKE \'%.php%\' OR sclf.file_path LIKE \'%.tpl%\' )
										 AND scl.created_on >= NOW() - INTERVAL \'1 week\'
								) as subsql
							WHERE
								subsql.rank = 1
						) svn_log
						LEFT JOIN folders_files_owner_associations ffoa ON (svn_log.dir_file_path = ffoa.file_path)
					WHERE
						( ffoa.id IS NULL OR ( ffoa.deleted_by IS NOT NULL AND ffoa.deleted_on IS NOT NULL) )
						AND svn_log.dir_file_path NOT LIKE \'%/TestSuite/%\'
						AND svn_log.dir_file_path NOT LIKE \'%branches%\'
						AND svn_log.dir_file_path NOT LIKE \'%/Frameworks/%\'
					GROUP BY
						svn_log.dir_file_path,
						svn_log.file_path,
						svn_log.user_id,
						svn_log.svn_repository_id,
						svn_log.id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchLatestSvnCommitLogFilesByUserId( $intUserId, $objDatabase ) {
		if( false == is_numeric( $intUserId ) ) return false;

		$strSql = 'SELECT
						MAX( scl.created_on ) as start_date
					FROM
						svn_commit_log_files sclf
						JOIN svn_commit_logs scl ON ( sclf.svn_commit_log_id = scl.id )
					WHERE
						sclf.created_by = ' . ( int ) $intUserId;

		$arrstrData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrstrData ) ) ? $arrstrData[0]['start_date'] : NULL;
	}

	public static function fetchSvnCommitLogFilesBySvnCommitLogIds( $arrintSvnCommitLogIds, $objDatabase ) {
		if( false == valArr( $arrintSvnCommitLogIds ) ) return NULL;

		$strSql = 'SELECT
				       sclf.svn_commit_log_id, sclf.file_path
				   FROM
				       svn_commit_log_files sclf
				   WHERE
				       svn_commit_log_id IN ( ' . implode( ',', $arrintSvnCommitLogIds ) . ' )';

		return parent::fetchSvnCommitLogFiles( $strSql, $objDatabase );
	}

}
?>