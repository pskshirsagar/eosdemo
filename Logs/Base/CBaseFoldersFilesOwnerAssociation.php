<?php

class CBaseFoldersFilesOwnerAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.folders_files_owner_associations';

	protected $m_intId;
	protected $m_intFoldersFilesOwnerAssociationsId;
	protected $m_strName;
	protected $m_strFilePath;
	protected $m_strModuleName;
	protected $m_intPsProductId;
	protected $m_intPsProductOptionId;
	protected $m_intSdmEmployeeId;
	protected $m_intArchitectEmployeeId;
	protected $m_intDevEmployeeId;
	protected $m_intQaEmployeeId;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsMultilingualSupport;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsMultilingualSupport = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['folders_files_owner_associations_id'] ) && $boolDirectSet ) $this->set( 'm_intFoldersFilesOwnerAssociationsId', trim( $arrValues['folders_files_owner_associations_id'] ) ); elseif( isset( $arrValues['folders_files_owner_associations_id'] ) ) $this->setFoldersFilesOwnerAssociationsId( $arrValues['folders_files_owner_associations_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['module_name'] ) && $boolDirectSet ) $this->set( 'm_strModuleName', trim( stripcslashes( $arrValues['module_name'] ) ) ); elseif( isset( $arrValues['module_name'] ) ) $this->setModuleName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['module_name'] ) : $arrValues['module_name'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductOptionId', trim( $arrValues['ps_product_option_id'] ) ); elseif( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['sdm_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSdmEmployeeId', trim( $arrValues['sdm_employee_id'] ) ); elseif( isset( $arrValues['sdm_employee_id'] ) ) $this->setSdmEmployeeId( $arrValues['sdm_employee_id'] );
		if( isset( $arrValues['architect_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intArchitectEmployeeId', trim( $arrValues['architect_employee_id'] ) ); elseif( isset( $arrValues['architect_employee_id'] ) ) $this->setArchitectEmployeeId( $arrValues['architect_employee_id'] );
		if( isset( $arrValues['dev_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intDevEmployeeId', trim( $arrValues['dev_employee_id'] ) ); elseif( isset( $arrValues['dev_employee_id'] ) ) $this->setDevEmployeeId( $arrValues['dev_employee_id'] );
		if( isset( $arrValues['qa_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intQaEmployeeId', trim( $arrValues['qa_employee_id'] ) ); elseif( isset( $arrValues['qa_employee_id'] ) ) $this->setQaEmployeeId( $arrValues['qa_employee_id'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_multilingual_support'] ) && $boolDirectSet ) $this->set( 'm_boolIsMultilingualSupport', trim( stripcslashes( $arrValues['is_multilingual_support'] ) ) ); elseif( isset( $arrValues['is_multilingual_support'] ) ) $this->setIsMultilingualSupport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_multilingual_support'] ) : $arrValues['is_multilingual_support'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setFoldersFilesOwnerAssociationsId( $intFoldersFilesOwnerAssociationsId ) {
		$this->set( 'm_intFoldersFilesOwnerAssociationsId', CStrings::strToIntDef( $intFoldersFilesOwnerAssociationsId, NULL, false ) );
	}

	public function getFoldersFilesOwnerAssociationsId() {
		return $this->m_intFoldersFilesOwnerAssociationsId;
	}

	public function sqlFoldersFilesOwnerAssociationsId() {
		return ( true == isset( $this->m_intFoldersFilesOwnerAssociationsId ) ) ? ( string ) $this->m_intFoldersFilesOwnerAssociationsId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setModuleName( $strModuleName ) {
		$this->set( 'm_strModuleName', CStrings::strTrimDef( $strModuleName, 4096, NULL, true ) );
	}

	public function getModuleName() {
		return $this->m_strModuleName;
	}

	public function sqlModuleName() {
		return ( true == isset( $this->m_strModuleName ) ) ? '\'' . addslashes( $this->m_strModuleName ) . '\'' : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->set( 'm_intPsProductOptionId', CStrings::strToIntDef( $intPsProductOptionId, NULL, false ) );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setSdmEmployeeId( $intSdmEmployeeId ) {
		$this->set( 'm_intSdmEmployeeId', CStrings::strToIntDef( $intSdmEmployeeId, NULL, false ) );
	}

	public function getSdmEmployeeId() {
		return $this->m_intSdmEmployeeId;
	}

	public function sqlSdmEmployeeId() {
		return ( true == isset( $this->m_intSdmEmployeeId ) ) ? ( string ) $this->m_intSdmEmployeeId : 'NULL';
	}

	public function setArchitectEmployeeId( $intArchitectEmployeeId ) {
		$this->set( 'm_intArchitectEmployeeId', CStrings::strToIntDef( $intArchitectEmployeeId, NULL, false ) );
	}

	public function getArchitectEmployeeId() {
		return $this->m_intArchitectEmployeeId;
	}

	public function sqlArchitectEmployeeId() {
		return ( true == isset( $this->m_intArchitectEmployeeId ) ) ? ( string ) $this->m_intArchitectEmployeeId : 'NULL';
	}

	public function setDevEmployeeId( $intDevEmployeeId ) {
		$this->set( 'm_intDevEmployeeId', CStrings::strToIntDef( $intDevEmployeeId, NULL, false ) );
	}

	public function getDevEmployeeId() {
		return $this->m_intDevEmployeeId;
	}

	public function sqlDevEmployeeId() {
		return ( true == isset( $this->m_intDevEmployeeId ) ) ? ( string ) $this->m_intDevEmployeeId : 'NULL';
	}

	public function setQaEmployeeId( $intQaEmployeeId ) {
		$this->set( 'm_intQaEmployeeId', CStrings::strToIntDef( $intQaEmployeeId, NULL, false ) );
	}

	public function getQaEmployeeId() {
		return $this->m_intQaEmployeeId;
	}

	public function sqlQaEmployeeId() {
		return ( true == isset( $this->m_intQaEmployeeId ) ) ? ( string ) $this->m_intQaEmployeeId : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsMultilingualSupport( $boolIsMultilingualSupport ) {
		$this->set( 'm_boolIsMultilingualSupport', CStrings::strToBool( $boolIsMultilingualSupport ) );
	}

	public function getIsMultilingualSupport() {
		return $this->m_boolIsMultilingualSupport;
	}

	public function sqlIsMultilingualSupport() {
		return ( true == isset( $this->m_boolIsMultilingualSupport ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMultilingualSupport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, folders_files_owner_associations_id, name, file_path, module_name, ps_product_id, ps_product_option_id, sdm_employee_id, architect_employee_id, dev_employee_id, qa_employee_id, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, is_multilingual_support )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlFoldersFilesOwnerAssociationsId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlModuleName() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlPsProductOptionId() . ', ' .
 						$this->sqlSdmEmployeeId() . ', ' .
 						$this->sqlArchitectEmployeeId() . ', ' .
 						$this->sqlDevEmployeeId() . ', ' .
 						$this->sqlQaEmployeeId() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlIsMultilingualSupport() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' folders_files_owner_associations_id = ' . $this->sqlFoldersFilesOwnerAssociationsId() . ','; } elseif( true == array_key_exists( 'FoldersFilesOwnerAssociationsId', $this->getChangedColumns() ) ) { $strSql .= ' folders_files_owner_associations_id = ' . $this->sqlFoldersFilesOwnerAssociationsId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' module_name = ' . $this->sqlModuleName() . ','; } elseif( true == array_key_exists( 'ModuleName', $this->getChangedColumns() ) ) { $strSql .= ' module_name = ' . $this->sqlModuleName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; } elseif( true == array_key_exists( 'PsProductOptionId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sdm_employee_id = ' . $this->sqlSdmEmployeeId() . ','; } elseif( true == array_key_exists( 'SdmEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sdm_employee_id = ' . $this->sqlSdmEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' architect_employee_id = ' . $this->sqlArchitectEmployeeId() . ','; } elseif( true == array_key_exists( 'ArchitectEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' architect_employee_id = ' . $this->sqlArchitectEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dev_employee_id = ' . $this->sqlDevEmployeeId() . ','; } elseif( true == array_key_exists( 'DevEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' dev_employee_id = ' . $this->sqlDevEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId() . ','; } elseif( true == array_key_exists( 'QaEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_multilingual_support = ' . $this->sqlIsMultilingualSupport() . ','; } elseif( true == array_key_exists( 'IsMultilingualSupport', $this->getChangedColumns() ) ) { $strSql .= ' is_multilingual_support = ' . $this->sqlIsMultilingualSupport() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'folders_files_owner_associations_id' => $this->getFoldersFilesOwnerAssociationsId(),
			'name' => $this->getName(),
			'file_path' => $this->getFilePath(),
			'module_name' => $this->getModuleName(),
			'ps_product_id' => $this->getPsProductId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'sdm_employee_id' => $this->getSdmEmployeeId(),
			'architect_employee_id' => $this->getArchitectEmployeeId(),
			'dev_employee_id' => $this->getDevEmployeeId(),
			'qa_employee_id' => $this->getQaEmployeeId(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_multilingual_support' => $this->getIsMultilingualSupport()
		);
	}

}
?>