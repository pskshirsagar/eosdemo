<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSvnCommitLogs
 * Do not add any new functions to this class.
 */

class CBaseSvnCommitLogs extends CEosPluralBase {

	/**
	 * @return CSvnCommitLog[]
	 */
	public static function fetchSvnCommitLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSvnCommitLog', $objDatabase );
	}

	/**
	 * @return CSvnCommitLog
	 */
	public static function fetchSvnCommitLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSvnCommitLog', $objDatabase );
	}

	public static function fetchSvnCommitLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'svn_commit_logs', $objDatabase );
	}

	public static function fetchSvnCommitLogById( $intId, $objDatabase ) {
		return self::fetchSvnCommitLog( sprintf( 'SELECT * FROM svn_commit_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSvnCommitLogsByUserId( $intUserId, $objDatabase ) {
		return self::fetchSvnCommitLogs( sprintf( 'SELECT * FROM svn_commit_logs WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

	public static function fetchSvnCommitLogsBySvnRepositoryId( $intSvnRepositoryId, $objDatabase ) {
		return self::fetchSvnCommitLogs( sprintf( 'SELECT * FROM svn_commit_logs WHERE svn_repository_id = %d', ( int ) $intSvnRepositoryId ), $objDatabase );
	}

	public static function fetchSvnCommitLogsByRevisionId( $intRevisionId, $objDatabase ) {
		return self::fetchSvnCommitLogs( sprintf( 'SELECT * FROM svn_commit_logs WHERE revision_id = %d', ( int ) $intRevisionId ), $objDatabase );
	}

	public static function fetchSvnCommitLogsByCodeAuditorId( $intCodeAuditorId, $objDatabase ) {
		return self::fetchSvnCommitLogs( sprintf( 'SELECT * FROM svn_commit_logs WHERE code_auditor_id = %d', ( int ) $intCodeAuditorId ), $objDatabase );
	}

}
?>