<?php

class CBaseDiagnosticAuditLog extends CEosSingularBase {

	const TABLE_NAME = 'public.diagnostic_audit_logs';

	protected $m_intId;
	protected $m_intDiagnosticId;
	protected $m_intTaskId;
	protected $m_strDiagnosticLogDetails;
	protected $m_jsonDiagnosticLogDetails;
	protected $m_intTotalErrorCount;
	protected $m_strAuditLogDatetime;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intTotalErrorCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['diagnostic_id'] ) && $boolDirectSet ) $this->set( 'm_intDiagnosticId', trim( $arrValues['diagnostic_id'] ) ); elseif( isset( $arrValues['diagnostic_id'] ) ) $this->setDiagnosticId( $arrValues['diagnostic_id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['diagnostic_log_details'] ) ) $this->set( 'm_strDiagnosticLogDetails', trim( $arrValues['diagnostic_log_details'] ) );
		if( isset( $arrValues['total_error_count'] ) && $boolDirectSet ) $this->set( 'm_intTotalErrorCount', trim( $arrValues['total_error_count'] ) ); elseif( isset( $arrValues['total_error_count'] ) ) $this->setTotalErrorCount( $arrValues['total_error_count'] );
		if( isset( $arrValues['audit_log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAuditLogDatetime', trim( $arrValues['audit_log_datetime'] ) ); elseif( isset( $arrValues['audit_log_datetime'] ) ) $this->setAuditLogDatetime( $arrValues['audit_log_datetime'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDiagnosticId( $intDiagnosticId ) {
		$this->set( 'm_intDiagnosticId', CStrings::strToIntDef( $intDiagnosticId, NULL, false ) );
	}

	public function getDiagnosticId() {
		return $this->m_intDiagnosticId;
	}

	public function sqlDiagnosticId() {
		return ( true == isset( $this->m_intDiagnosticId ) ) ? ( string ) $this->m_intDiagnosticId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setDiagnosticLogDetails( $jsonDiagnosticLogDetails ) {
		if( true == valObj( $jsonDiagnosticLogDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonDiagnosticLogDetails', $jsonDiagnosticLogDetails );
		} elseif( true == valJsonString( $jsonDiagnosticLogDetails ) ) {
			$this->set( 'm_jsonDiagnosticLogDetails', CStrings::strToJson( $jsonDiagnosticLogDetails ) );
		} else {
			$this->set( 'm_jsonDiagnosticLogDetails', NULL ); 
		}
		unset( $this->m_strDiagnosticLogDetails );
	}

	public function getDiagnosticLogDetails() {
		if( true == isset( $this->m_strDiagnosticLogDetails ) ) {
			$this->m_jsonDiagnosticLogDetails = CStrings::strToJson( $this->m_strDiagnosticLogDetails );
			unset( $this->m_strDiagnosticLogDetails );
		}
		return $this->m_jsonDiagnosticLogDetails;
	}

	public function sqlDiagnosticLogDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getDiagnosticLogDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getDiagnosticLogDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setTotalErrorCount( $intTotalErrorCount ) {
		$this->set( 'm_intTotalErrorCount', CStrings::strToIntDef( $intTotalErrorCount, NULL, false ) );
	}

	public function getTotalErrorCount() {
		return $this->m_intTotalErrorCount;
	}

	public function sqlTotalErrorCount() {
		return ( true == isset( $this->m_intTotalErrorCount ) ) ? ( string ) $this->m_intTotalErrorCount : '0';
	}

	public function setAuditLogDatetime( $strAuditLogDatetime ) {
		$this->set( 'm_strAuditLogDatetime', CStrings::strTrimDef( $strAuditLogDatetime, -1, NULL, true ) );
	}

	public function getAuditLogDatetime() {
		return $this->m_strAuditLogDatetime;
	}

	public function sqlAuditLogDatetime() {
		return ( true == isset( $this->m_strAuditLogDatetime ) ) ? '\'' . $this->m_strAuditLogDatetime . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, diagnostic_id, task_id, diagnostic_log_details, total_error_count, audit_log_datetime, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDiagnosticId() . ', ' .
 						$this->sqlTaskId() . ', ' .
 						$this->sqlDiagnosticLogDetails() . ', ' .
 						$this->sqlTotalErrorCount() . ', ' .
 						$this->sqlAuditLogDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' diagnostic_id = ' . $this->sqlDiagnosticId() . ','; } elseif( true == array_key_exists( 'DiagnosticId', $this->getChangedColumns() ) ) { $strSql .= ' diagnostic_id = ' . $this->sqlDiagnosticId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' diagnostic_log_details = ' . $this->sqlDiagnosticLogDetails() . ','; } elseif( true == array_key_exists( 'DiagnosticLogDetails', $this->getChangedColumns() ) ) { $strSql .= ' diagnostic_log_details = ' . $this->sqlDiagnosticLogDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_error_count = ' . $this->sqlTotalErrorCount() . ','; } elseif( true == array_key_exists( 'TotalErrorCount', $this->getChangedColumns() ) ) { $strSql .= ' total_error_count = ' . $this->sqlTotalErrorCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_log_datetime = ' . $this->sqlAuditLogDatetime() . ','; } elseif( true == array_key_exists( 'AuditLogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' audit_log_datetime = ' . $this->sqlAuditLogDatetime() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'diagnostic_id' => $this->getDiagnosticId(),
			'task_id' => $this->getTaskId(),
			'diagnostic_log_details' => $this->getDiagnosticLogDetails(),
			'total_error_count' => $this->getTotalErrorCount(),
			'audit_log_datetime' => $this->getAuditLogDatetime(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>