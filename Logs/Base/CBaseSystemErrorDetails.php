<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSystemErrorDetails
 * Do not add any new functions to this class.
 */

class CBaseSystemErrorDetails extends CEosPluralBase {

	/**
	 * @return CSystemErrorDetail[]
	 */
	public static function fetchSystemErrorDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSystemErrorDetail::class, $objDatabase );
	}

	/**
	 * @return CSystemErrorDetail
	 */
	public static function fetchSystemErrorDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSystemErrorDetail::class, $objDatabase );
	}

	public static function fetchSystemErrorDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_error_details', $objDatabase );
	}

	public static function fetchSystemErrorDetailById( $intId, $objDatabase ) {
		return self::fetchSystemErrorDetail( sprintf( 'SELECT * FROM system_error_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSystemErrorDetailsBySystemErrorId( $intSystemErrorId, $objDatabase ) {
		return self::fetchSystemErrorDetails( sprintf( 'SELECT * FROM system_error_details WHERE system_error_id = %d', ( int ) $intSystemErrorId ), $objDatabase );
	}

	public static function fetchSystemErrorDetailsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchSystemErrorDetails( sprintf( 'SELECT * FROM system_error_details WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchSystemErrorDetailsByPsProductModuleId( $intPsProductModuleId, $objDatabase ) {
		return self::fetchSystemErrorDetails( sprintf( 'SELECT * FROM system_error_details WHERE ps_product_module_id = %d', ( int ) $intPsProductModuleId ), $objDatabase );
	}

	public static function fetchSystemErrorDetailsByPsProductOptionId( $intPsProductOptionId, $objDatabase ) {
		return self::fetchSystemErrorDetails( sprintf( 'SELECT * FROM system_error_details WHERE ps_product_option_id = %d', ( int ) $intPsProductOptionId ), $objDatabase );
	}

}
?>