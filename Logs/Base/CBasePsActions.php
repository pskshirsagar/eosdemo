<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CPsActions
 * Do not add any new functions to this class.
 */

class CBasePsActions extends CEosPluralBase {

	/**
	 * @return CPsAction[]
	 */
	public static function fetchPsActions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsAction', $objDatabase );
	}

	/**
	 * @return CPsAction
	 */
	public static function fetchPsAction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsAction', $objDatabase );
	}

	public static function fetchPsActionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_actions', $objDatabase );
	}

	public static function fetchPsActionById( $intId, $objDatabase ) {
		return self::fetchPsAction( sprintf( 'SELECT * FROM ps_actions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsActionsByReviewTaskId( $intReviewTaskId, $objDatabase ) {
		return self::fetchPsActions( sprintf( 'SELECT * FROM ps_actions WHERE review_task_id = %d', ( int ) $intReviewTaskId ), $objDatabase );
	}

}
?>