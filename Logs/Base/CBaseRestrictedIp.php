<?php

class CBaseRestrictedIp extends CEosSingularBase {

	const TABLE_NAME = 'public.restricted_ips';

	protected $m_intId;
	protected $m_strIpAddress;
	protected $m_intPsCompetitorId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( stripcslashes( $arrValues['ip_address'] ) ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ip_address'] ) : $arrValues['ip_address'] );
		if( isset( $arrValues['ps_competitor_id'] ) && $boolDirectSet ) $this->set( 'm_intPsCompetitorId', trim( $arrValues['ps_competitor_id'] ) ); elseif( isset( $arrValues['ps_competitor_id'] ) ) $this->setPsCompetitorId( $arrValues['ps_competitor_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 40, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? '\'' . addslashes( $this->m_strIpAddress ) . '\'' : 'NULL';
	}

	public function setPsCompetitorId( $intPsCompetitorId ) {
		$this->set( 'm_intPsCompetitorId', CStrings::strToIntDef( $intPsCompetitorId, NULL, false ) );
	}

	public function getPsCompetitorId() {
		return $this->m_intPsCompetitorId;
	}

	public function sqlPsCompetitorId() {
		return ( true == isset( $this->m_intPsCompetitorId ) ) ? ( string ) $this->m_intPsCompetitorId : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ip_address' => $this->getIpAddress(),
			'ps_competitor_id' => $this->getPsCompetitorId()
		);
	}

}
?>