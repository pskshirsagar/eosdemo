<?php

class CBaseEcsMessageLog extends CEosSingularBase {

	const TABLE_NAME = 'public.ecs_message_logs';

	protected $m_intId;
	protected $m_intEcsMessageId;
	protected $m_strStatus;
	protected $m_strSentTo;
	protected $m_strSentOn;
	protected $m_strReceivedOn;
	protected $m_strUpdatedOn;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ecs_message_id'] ) && $boolDirectSet ) $this->set( 'm_intEcsMessageId', trim( $arrValues['ecs_message_id'] ) ); elseif( isset( $arrValues['ecs_message_id'] ) ) $this->setEcsMessageId( $arrValues['ecs_message_id'] );
		if( isset( $arrValues['status'] ) && $boolDirectSet ) $this->set( 'm_strStatus', trim( stripcslashes( $arrValues['status'] ) ) ); elseif( isset( $arrValues['status'] ) ) $this->setStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status'] ) : $arrValues['status'] );
		if( isset( $arrValues['sent_to'] ) && $boolDirectSet ) $this->set( 'm_strSentTo', trim( stripcslashes( $arrValues['sent_to'] ) ) ); elseif( isset( $arrValues['sent_to'] ) ) $this->setSentTo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sent_to'] ) : $arrValues['sent_to'] );
		if( isset( $arrValues['sent_on'] ) && $boolDirectSet ) $this->set( 'm_strSentOn', trim( $arrValues['sent_on'] ) ); elseif( isset( $arrValues['sent_on'] ) ) $this->setSentOn( $arrValues['sent_on'] );
		if( isset( $arrValues['received_on'] ) && $boolDirectSet ) $this->set( 'm_strReceivedOn', trim( $arrValues['received_on'] ) ); elseif( isset( $arrValues['received_on'] ) ) $this->setReceivedOn( $arrValues['received_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEcsMessageId( $intEcsMessageId ) {
		$this->set( 'm_intEcsMessageId', CStrings::strToIntDef( $intEcsMessageId, NULL, false ) );
	}

	public function getEcsMessageId() {
		return $this->m_intEcsMessageId;
	}

	public function sqlEcsMessageId() {
		return ( true == isset( $this->m_intEcsMessageId ) ) ? ( string ) $this->m_intEcsMessageId : 'NULL';
	}

	public function setStatus( $strStatus ) {
		$this->set( 'm_strStatus', CStrings::strTrimDef( $strStatus, 20, NULL, true ) );
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function sqlStatus() {
		return ( true == isset( $this->m_strStatus ) ) ? '\'' . addslashes( $this->m_strStatus ) . '\'' : 'NULL';
	}

	public function setSentTo( $strSentTo ) {
		$this->set( 'm_strSentTo', CStrings::strTrimDef( $strSentTo, 30, NULL, true ) );
	}

	public function getSentTo() {
		return $this->m_strSentTo;
	}

	public function sqlSentTo() {
		return ( true == isset( $this->m_strSentTo ) ) ? '\'' . addslashes( $this->m_strSentTo ) . '\'' : 'NULL';
	}

	public function setSentOn( $strSentOn ) {
		$this->set( 'm_strSentOn', CStrings::strTrimDef( $strSentOn, -1, NULL, true ) );
	}

	public function getSentOn() {
		return $this->m_strSentOn;
	}

	public function sqlSentOn() {
		return ( true == isset( $this->m_strSentOn ) ) ? '\'' . $this->m_strSentOn . '\'' : 'NOW()';
	}

	public function setReceivedOn( $strReceivedOn ) {
		$this->set( 'm_strReceivedOn', CStrings::strTrimDef( $strReceivedOn, -1, NULL, true ) );
	}

	public function getReceivedOn() {
		return $this->m_strReceivedOn;
	}

	public function sqlReceivedOn() {
		return ( true == isset( $this->m_strReceivedOn ) ) ? '\'' . $this->m_strReceivedOn . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ecs_message_id, status, sent_to, sent_on, received_on, updated_on, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEcsMessageId() . ', ' .
 						$this->sqlStatus() . ', ' .
 						$this->sqlSentTo() . ', ' .
 						$this->sqlSentOn() . ', ' .
 						$this->sqlReceivedOn() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ecs_message_id = ' . $this->sqlEcsMessageId() . ','; } elseif( true == array_key_exists( 'EcsMessageId', $this->getChangedColumns() ) ) { $strSql .= ' ecs_message_id = ' . $this->sqlEcsMessageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; } elseif( true == array_key_exists( 'Status', $this->getChangedColumns() ) ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_to = ' . $this->sqlSentTo() . ','; } elseif( true == array_key_exists( 'SentTo', $this->getChangedColumns() ) ) { $strSql .= ' sent_to = ' . $this->sqlSentTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; } elseif( true == array_key_exists( 'SentOn', $this->getChangedColumns() ) ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' received_on = ' . $this->sqlReceivedOn() . ','; } elseif( true == array_key_exists( 'ReceivedOn', $this->getChangedColumns() ) ) { $strSql .= ' received_on = ' . $this->sqlReceivedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ecs_message_id' => $this->getEcsMessageId(),
			'status' => $this->getStatus(),
			'sent_to' => $this->getSentTo(),
			'sent_on' => $this->getSentOn(),
			'received_on' => $this->getReceivedOn(),
			'updated_on' => $this->getUpdatedOn(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>