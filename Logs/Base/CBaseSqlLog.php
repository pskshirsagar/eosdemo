<?php

class CBaseSqlLog extends CEosSingularBase {

	const TABLE_NAME = 'public.sql_logs';

	protected $m_intDatabaseId;
	protected $m_intPsProductId;
	protected $m_intSqlStatementId;
	protected $m_strModule;
	protected $m_strAction;
	protected $m_strMonth;
	protected $m_intHitCount;
	protected $m_fltTotalExecutionSeconds;
	protected $m_fltMinExecutionSeconds;
	protected $m_fltMaxExecutionSeconds;
	protected $m_intMaxRowsReturned;

	public function __construct() {
		parent::__construct();

		$this->m_intHitCount = '1';
		$this->m_fltTotalExecutionSeconds = '0';
		$this->m_fltMinExecutionSeconds = '0';
		$this->m_fltMaxExecutionSeconds = '0';
		$this->m_intMaxRowsReturned = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['database_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseId', trim( $arrValues['database_id'] ) ); elseif( isset( $arrValues['database_id'] ) ) $this->setDatabaseId( $arrValues['database_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['sql_statement_id'] ) && $boolDirectSet ) $this->set( 'm_intSqlStatementId', trim( $arrValues['sql_statement_id'] ) ); elseif( isset( $arrValues['sql_statement_id'] ) ) $this->setSqlStatementId( $arrValues['sql_statement_id'] );
		if( isset( $arrValues['module'] ) && $boolDirectSet ) $this->set( 'm_strModule', trim( stripcslashes( $arrValues['module'] ) ) ); elseif( isset( $arrValues['module'] ) ) $this->setModule( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['module'] ) : $arrValues['module'] );
		if( isset( $arrValues['action'] ) && $boolDirectSet ) $this->set( 'm_strAction', trim( stripcslashes( $arrValues['action'] ) ) ); elseif( isset( $arrValues['action'] ) ) $this->setAction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action'] ) : $arrValues['action'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->set( 'm_strMonth', trim( $arrValues['month'] ) ); elseif( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['hit_count'] ) && $boolDirectSet ) $this->set( 'm_intHitCount', trim( $arrValues['hit_count'] ) ); elseif( isset( $arrValues['hit_count'] ) ) $this->setHitCount( $arrValues['hit_count'] );
		if( isset( $arrValues['total_execution_seconds'] ) && $boolDirectSet ) $this->set( 'm_fltTotalExecutionSeconds', trim( $arrValues['total_execution_seconds'] ) ); elseif( isset( $arrValues['total_execution_seconds'] ) ) $this->setTotalExecutionSeconds( $arrValues['total_execution_seconds'] );
		if( isset( $arrValues['min_execution_seconds'] ) && $boolDirectSet ) $this->set( 'm_fltMinExecutionSeconds', trim( $arrValues['min_execution_seconds'] ) ); elseif( isset( $arrValues['min_execution_seconds'] ) ) $this->setMinExecutionSeconds( $arrValues['min_execution_seconds'] );
		if( isset( $arrValues['max_execution_seconds'] ) && $boolDirectSet ) $this->set( 'm_fltMaxExecutionSeconds', trim( $arrValues['max_execution_seconds'] ) ); elseif( isset( $arrValues['max_execution_seconds'] ) ) $this->setMaxExecutionSeconds( $arrValues['max_execution_seconds'] );
		if( isset( $arrValues['max_rows_returned'] ) && $boolDirectSet ) $this->set( 'm_intMaxRowsReturned', trim( $arrValues['max_rows_returned'] ) ); elseif( isset( $arrValues['max_rows_returned'] ) ) $this->setMaxRowsReturned( $arrValues['max_rows_returned'] );
		$this->m_boolInitialized = true;
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->set( 'm_intDatabaseId', CStrings::strToIntDef( $intDatabaseId, NULL, false ) );
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function sqlDatabaseId() {
		return ( true == isset( $this->m_intDatabaseId ) ) ? ( string ) $this->m_intDatabaseId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setSqlStatementId( $intSqlStatementId ) {
		$this->set( 'm_intSqlStatementId', CStrings::strToIntDef( $intSqlStatementId, NULL, false ) );
	}

	public function getSqlStatementId() {
		return $this->m_intSqlStatementId;
	}

	public function sqlSqlStatementId() {
		return ( true == isset( $this->m_intSqlStatementId ) ) ? ( string ) $this->m_intSqlStatementId : 'NULL';
	}

	public function setModule( $strModule ) {
		$this->set( 'm_strModule', CStrings::strTrimDef( $strModule, 100, NULL, true ) );
	}

	public function getModule() {
		return $this->m_strModule;
	}

	public function sqlModule() {
		return ( true == isset( $this->m_strModule ) ) ? '\'' . addslashes( $this->m_strModule ) . '\'' : 'NULL';
	}

	public function setAction( $strAction ) {
		$this->set( 'm_strAction', CStrings::strTrimDef( $strAction, 100, NULL, true ) );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function sqlAction() {
		return ( true == isset( $this->m_strAction ) ) ? '\'' . addslashes( $this->m_strAction ) . '\'' : 'NULL';
	}

	public function setMonth( $strMonth ) {
		$this->set( 'm_strMonth', CStrings::strTrimDef( $strMonth, -1, NULL, true ) );
	}

	public function getMonth() {
		return $this->m_strMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_strMonth ) ) ? '\'' . $this->m_strMonth . '\'' : 'NOW()';
	}

	public function setHitCount( $intHitCount ) {
		$this->set( 'm_intHitCount', CStrings::strToIntDef( $intHitCount, NULL, false ) );
	}

	public function getHitCount() {
		return $this->m_intHitCount;
	}

	public function sqlHitCount() {
		return ( true == isset( $this->m_intHitCount ) ) ? ( string ) $this->m_intHitCount : '1';
	}

	public function setTotalExecutionSeconds( $fltTotalExecutionSeconds ) {
		$this->set( 'm_fltTotalExecutionSeconds', CStrings::strToFloatDef( $fltTotalExecutionSeconds, NULL, false, 0 ) );
	}

	public function getTotalExecutionSeconds() {
		return $this->m_fltTotalExecutionSeconds;
	}

	public function sqlTotalExecutionSeconds() {
		return ( true == isset( $this->m_fltTotalExecutionSeconds ) ) ? ( string ) $this->m_fltTotalExecutionSeconds : '0';
	}

	public function setMinExecutionSeconds( $fltMinExecutionSeconds ) {
		$this->set( 'm_fltMinExecutionSeconds', CStrings::strToFloatDef( $fltMinExecutionSeconds, NULL, false, 0 ) );
	}

	public function getMinExecutionSeconds() {
		return $this->m_fltMinExecutionSeconds;
	}

	public function sqlMinExecutionSeconds() {
		return ( true == isset( $this->m_fltMinExecutionSeconds ) ) ? ( string ) $this->m_fltMinExecutionSeconds : '0';
	}

	public function setMaxExecutionSeconds( $fltMaxExecutionSeconds ) {
		$this->set( 'm_fltMaxExecutionSeconds', CStrings::strToFloatDef( $fltMaxExecutionSeconds, NULL, false, 0 ) );
	}

	public function getMaxExecutionSeconds() {
		return $this->m_fltMaxExecutionSeconds;
	}

	public function sqlMaxExecutionSeconds() {
		return ( true == isset( $this->m_fltMaxExecutionSeconds ) ) ? ( string ) $this->m_fltMaxExecutionSeconds : '0';
	}

	public function setMaxRowsReturned( $intMaxRowsReturned ) {
		$this->set( 'm_intMaxRowsReturned', CStrings::strToIntDef( $intMaxRowsReturned, NULL, false ) );
	}

	public function getMaxRowsReturned() {
		return $this->m_intMaxRowsReturned;
	}

	public function sqlMaxRowsReturned() {
		return ( true == isset( $this->m_intMaxRowsReturned ) ) ? ( string ) $this->m_intMaxRowsReturned : '0';
	}

	public function toArray() {
		return array(
			'database_id' => $this->getDatabaseId(),
			'ps_product_id' => $this->getPsProductId(),
			'sql_statement_id' => $this->getSqlStatementId(),
			'module' => $this->getModule(),
			'action' => $this->getAction(),
			'month' => $this->getMonth(),
			'hit_count' => $this->getHitCount(),
			'total_execution_seconds' => $this->getTotalExecutionSeconds(),
			'min_execution_seconds' => $this->getMinExecutionSeconds(),
			'max_execution_seconds' => $this->getMaxExecutionSeconds(),
			'max_rows_returned' => $this->getMaxRowsReturned()
		);
	}

}
?>