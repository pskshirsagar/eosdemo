<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSystemErrors
 * Do not add any new functions to this class.
 */

class CBaseSystemErrors extends CEosPluralBase {

	/**
	 * @return CSystemError[]
	 */
	public static function fetchSystemErrors( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSystemError::class, $objDatabase );
	}

	/**
	 * @return CSystemError
	 */
	public static function fetchSystemError( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSystemError::class, $objDatabase );
	}

	public static function fetchSystemErrorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_errors', $objDatabase );
	}

	public static function fetchSystemErrorById( $intId, $objDatabase ) {
		return self::fetchSystemError( sprintf( 'SELECT * FROM system_errors WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSystemErrorsBySystemErrorTypeId( $intSystemErrorTypeId, $objDatabase ) {
		return self::fetchSystemErrors( sprintf( 'SELECT * FROM system_errors WHERE system_error_type_id = %d', ( int ) $intSystemErrorTypeId ), $objDatabase );
	}

	public static function fetchSystemErrorsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchSystemErrors( sprintf( 'SELECT * FROM system_errors WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchSystemErrorsByClusterId( $intClusterId, $objDatabase ) {
		return self::fetchSystemErrors( sprintf( 'SELECT * FROM system_errors WHERE cluster_id = %d', ( int ) $intClusterId ), $objDatabase );
	}

}
?>