<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSiteTabletUsageLogs
 * Do not add any new functions to this class.
 */

class CBaseSiteTabletUsageLogs extends CEosPluralBase {

	/**
	 * @return CSiteTabletUsageLog[]
	 */
	public static function fetchSiteTabletUsageLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSiteTabletUsageLog', $objDatabase );
	}

	/**
	 * @return CSiteTabletUsageLog
	 */
	public static function fetchSiteTabletUsageLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSiteTabletUsageLog', $objDatabase );
	}

	public static function fetchSiteTabletUsageLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'site_tablet_usage_logs', $objDatabase );
	}

	public static function fetchSiteTabletUsageLogById( $intId, $objDatabase ) {
		return self::fetchSiteTabletUsageLog( sprintf( 'SELECT * FROM site_tablet_usage_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSiteTabletUsageLogsByCid( $intCid, $objDatabase ) {
		return self::fetchSiteTabletUsageLogs( sprintf( 'SELECT * FROM site_tablet_usage_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSiteTabletUsageLogsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchSiteTabletUsageLogs( sprintf( 'SELECT * FROM site_tablet_usage_logs WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>