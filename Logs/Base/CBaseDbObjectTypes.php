<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CDbObjectTypes
 * Do not add any new functions to this class.
 */

class CBaseDbObjectTypes extends CEosPluralBase {

	/**
	 * @return CDbObjectType[]
	 */
	public static function fetchDbObjectTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDbObjectType', $objDatabase );
	}

	/**
	 * @return CDbObjectType
	 */
	public static function fetchDbObjectType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDbObjectType', $objDatabase );
	}

	public static function fetchDbObjectTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'db_object_types', $objDatabase );
	}

	public static function fetchDbObjectTypeById( $intId, $objDatabase ) {
		return self::fetchDbObjectType( sprintf( 'SELECT * FROM db_object_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>