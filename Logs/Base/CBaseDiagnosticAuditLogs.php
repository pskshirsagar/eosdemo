<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CDiagnosticAuditLogs
 * Do not add any new functions to this class.
 */

class CBaseDiagnosticAuditLogs extends CEosPluralBase {

	/**
	 * @return CDiagnosticAuditLog[]
	 */
	public static function fetchDiagnosticAuditLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDiagnosticAuditLog::class, $objDatabase );
	}

	/**
	 * @return CDiagnosticAuditLog
	 */
	public static function fetchDiagnosticAuditLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDiagnosticAuditLog::class, $objDatabase );
	}

	public static function fetchDiagnosticAuditLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'diagnostic_audit_logs', $objDatabase );
	}

	public static function fetchDiagnosticAuditLogById( $intId, $objDatabase ) {
		return self::fetchDiagnosticAuditLog( sprintf( 'SELECT * FROM diagnostic_audit_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDiagnosticAuditLogsByDiagnosticId( $intDiagnosticId, $objDatabase ) {
		return self::fetchDiagnosticAuditLogs( sprintf( 'SELECT * FROM diagnostic_audit_logs WHERE diagnostic_id = %d', ( int ) $intDiagnosticId ), $objDatabase );
	}

	public static function fetchDiagnosticAuditLogsByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchDiagnosticAuditLogs( sprintf( 'SELECT * FROM diagnostic_audit_logs WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

}
?>