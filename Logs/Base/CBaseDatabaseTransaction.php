<?php

class CBaseDatabaseTransaction extends CEosSingularBase {

	const TABLE_NAME = 'public.database_transactions';

	protected $m_intId;
	protected $m_intClusterId;
	protected $m_intDatabaseId;
	protected $m_intDatabaseTransactionTypeId;
	protected $m_intScriptId;
	protected $m_intHardwareId;
	protected $m_intDatabaseTransactionId;
	protected $m_strSummary;
	protected $m_strBeginDatetime;
	protected $m_strEndDatetime;
	protected $m_strDescription;
	protected $m_strFailedOn;
	protected $m_strResolvedOn;
	protected $m_intDismissedBy;
	protected $m_strDismissedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intClusterId = '1';
		$this->m_intDatabaseTransactionTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['database_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseId', trim( $arrValues['database_id'] ) ); elseif( isset( $arrValues['database_id'] ) ) $this->setDatabaseId( $arrValues['database_id'] );
		if( isset( $arrValues['database_transaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseTransactionTypeId', trim( $arrValues['database_transaction_type_id'] ) ); elseif( isset( $arrValues['database_transaction_type_id'] ) ) $this->setDatabaseTransactionTypeId( $arrValues['database_transaction_type_id'] );
		if( isset( $arrValues['script_id'] ) && $boolDirectSet ) $this->set( 'm_intScriptId', trim( $arrValues['script_id'] ) ); elseif( isset( $arrValues['script_id'] ) ) $this->setScriptId( $arrValues['script_id'] );
		if( isset( $arrValues['hardware_id'] ) && $boolDirectSet ) $this->set( 'm_intHardwareId', trim( $arrValues['hardware_id'] ) ); elseif( isset( $arrValues['hardware_id'] ) ) $this->setHardwareId( $arrValues['hardware_id'] );
		if( isset( $arrValues['database_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseTransactionId', trim( $arrValues['database_transaction_id'] ) ); elseif( isset( $arrValues['database_transaction_id'] ) ) $this->setDatabaseTransactionId( $arrValues['database_transaction_id'] );
		if( isset( $arrValues['summary'] ) && $boolDirectSet ) $this->set( 'm_strSummary', trim( stripcslashes( $arrValues['summary'] ) ) ); elseif( isset( $arrValues['summary'] ) ) $this->setSummary( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['summary'] ) : $arrValues['summary'] );
		if( isset( $arrValues['begin_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBeginDatetime', trim( $arrValues['begin_datetime'] ) ); elseif( isset( $arrValues['begin_datetime'] ) ) $this->setBeginDatetime( $arrValues['begin_datetime'] );
		if( isset( $arrValues['end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndDatetime', trim( $arrValues['end_datetime'] ) ); elseif( isset( $arrValues['end_datetime'] ) ) $this->setEndDatetime( $arrValues['end_datetime'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['failed_on'] ) && $boolDirectSet ) $this->set( 'm_strFailedOn', trim( $arrValues['failed_on'] ) ); elseif( isset( $arrValues['failed_on'] ) ) $this->setFailedOn( $arrValues['failed_on'] );
		if( isset( $arrValues['resolved_on'] ) && $boolDirectSet ) $this->set( 'm_strResolvedOn', trim( $arrValues['resolved_on'] ) ); elseif( isset( $arrValues['resolved_on'] ) ) $this->setResolvedOn( $arrValues['resolved_on'] );
		if( isset( $arrValues['dismissed_by'] ) && $boolDirectSet ) $this->set( 'm_intDismissedBy', trim( $arrValues['dismissed_by'] ) ); elseif( isset( $arrValues['dismissed_by'] ) ) $this->setDismissedBy( $arrValues['dismissed_by'] );
		if( isset( $arrValues['dismissed_on'] ) && $boolDirectSet ) $this->set( 'm_strDismissedOn', trim( $arrValues['dismissed_on'] ) ); elseif( isset( $arrValues['dismissed_on'] ) ) $this->setDismissedOn( $arrValues['dismissed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : '1';
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->set( 'm_intDatabaseId', CStrings::strToIntDef( $intDatabaseId, NULL, false ) );
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function sqlDatabaseId() {
		return ( true == isset( $this->m_intDatabaseId ) ) ? ( string ) $this->m_intDatabaseId : 'NULL';
	}

	public function setDatabaseTransactionTypeId( $intDatabaseTransactionTypeId ) {
		$this->set( 'm_intDatabaseTransactionTypeId', CStrings::strToIntDef( $intDatabaseTransactionTypeId, NULL, false ) );
	}

	public function getDatabaseTransactionTypeId() {
		return $this->m_intDatabaseTransactionTypeId;
	}

	public function sqlDatabaseTransactionTypeId() {
		return ( true == isset( $this->m_intDatabaseTransactionTypeId ) ) ? ( string ) $this->m_intDatabaseTransactionTypeId : '1';
	}

	public function setScriptId( $intScriptId ) {
		$this->set( 'm_intScriptId', CStrings::strToIntDef( $intScriptId, NULL, false ) );
	}

	public function getScriptId() {
		return $this->m_intScriptId;
	}

	public function sqlScriptId() {
		return ( true == isset( $this->m_intScriptId ) ) ? ( string ) $this->m_intScriptId : 'NULL';
	}

	public function setHardwareId( $intHardwareId ) {
		$this->set( 'm_intHardwareId', CStrings::strToIntDef( $intHardwareId, NULL, false ) );
	}

	public function getHardwareId() {
		return $this->m_intHardwareId;
	}

	public function sqlHardwareId() {
		return ( true == isset( $this->m_intHardwareId ) ) ? ( string ) $this->m_intHardwareId : 'NULL';
	}

	public function setDatabaseTransactionId( $intDatabaseTransactionId ) {
		$this->set( 'm_intDatabaseTransactionId', CStrings::strToIntDef( $intDatabaseTransactionId, NULL, false ) );
	}

	public function getDatabaseTransactionId() {
		return $this->m_intDatabaseTransactionId;
	}

	public function sqlDatabaseTransactionId() {
		return ( true == isset( $this->m_intDatabaseTransactionId ) ) ? ( string ) $this->m_intDatabaseTransactionId : 'NULL';
	}

	public function setSummary( $strSummary ) {
		$this->set( 'm_strSummary', CStrings::strTrimDef( $strSummary, -1, NULL, true ) );
	}

	public function getSummary() {
		return $this->m_strSummary;
	}

	public function sqlSummary() {
		return ( true == isset( $this->m_strSummary ) ) ? '\'' . addslashes( $this->m_strSummary ) . '\'' : 'NULL';
	}

	public function setBeginDatetime( $strBeginDatetime ) {
		$this->set( 'm_strBeginDatetime', CStrings::strTrimDef( $strBeginDatetime, -1, NULL, true ) );
	}

	public function getBeginDatetime() {
		return $this->m_strBeginDatetime;
	}

	public function sqlBeginDatetime() {
		return ( true == isset( $this->m_strBeginDatetime ) ) ? '\'' . $this->m_strBeginDatetime . '\'' : 'NOW()';
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->set( 'm_strEndDatetime', CStrings::strTrimDef( $strEndDatetime, -1, NULL, true ) );
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function sqlEndDatetime() {
		return ( true == isset( $this->m_strEndDatetime ) ) ? '\'' . $this->m_strEndDatetime . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setFailedOn( $strFailedOn ) {
		$this->set( 'm_strFailedOn', CStrings::strTrimDef( $strFailedOn, -1, NULL, true ) );
	}

	public function getFailedOn() {
		return $this->m_strFailedOn;
	}

	public function sqlFailedOn() {
		return ( true == isset( $this->m_strFailedOn ) ) ? '\'' . $this->m_strFailedOn . '\'' : 'NULL';
	}

	public function setResolvedOn( $strResolvedOn ) {
		$this->set( 'm_strResolvedOn', CStrings::strTrimDef( $strResolvedOn, -1, NULL, true ) );
	}

	public function getResolvedOn() {
		return $this->m_strResolvedOn;
	}

	public function sqlResolvedOn() {
		return ( true == isset( $this->m_strResolvedOn ) ) ? '\'' . $this->m_strResolvedOn . '\'' : 'NULL';
	}

	public function setDismissedBy( $intDismissedBy ) {
		$this->set( 'm_intDismissedBy', CStrings::strToIntDef( $intDismissedBy, NULL, false ) );
	}

	public function getDismissedBy() {
		return $this->m_intDismissedBy;
	}

	public function sqlDismissedBy() {
		return ( true == isset( $this->m_intDismissedBy ) ) ? ( string ) $this->m_intDismissedBy : 'NULL';
	}

	public function setDismissedOn( $strDismissedOn ) {
		$this->set( 'm_strDismissedOn', CStrings::strTrimDef( $strDismissedOn, -1, NULL, true ) );
	}

	public function getDismissedOn() {
		return $this->m_strDismissedOn;
	}

	public function sqlDismissedOn() {
		return ( true == isset( $this->m_strDismissedOn ) ) ? '\'' . $this->m_strDismissedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cluster_id, database_id, database_transaction_type_id, script_id, hardware_id, database_transaction_id, summary, begin_datetime, end_datetime, description, failed_on, resolved_on, dismissed_by, dismissed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlClusterId() . ', ' .
 						$this->sqlDatabaseId() . ', ' .
 						$this->sqlDatabaseTransactionTypeId() . ', ' .
 						$this->sqlScriptId() . ', ' .
 						$this->sqlHardwareId() . ', ' .
 						$this->sqlDatabaseTransactionId() . ', ' .
 						$this->sqlSummary() . ', ' .
 						$this->sqlBeginDatetime() . ', ' .
 						$this->sqlEndDatetime() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlFailedOn() . ', ' .
 						$this->sqlResolvedOn() . ', ' .
 						$this->sqlDismissedBy() . ', ' .
 						$this->sqlDismissedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; } elseif( true == array_key_exists( 'DatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_transaction_type_id = ' . $this->sqlDatabaseTransactionTypeId() . ','; } elseif( true == array_key_exists( 'DatabaseTransactionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' database_transaction_type_id = ' . $this->sqlDatabaseTransactionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' script_id = ' . $this->sqlScriptId() . ','; } elseif( true == array_key_exists( 'ScriptId', $this->getChangedColumns() ) ) { $strSql .= ' script_id = ' . $this->sqlScriptId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hardware_id = ' . $this->sqlHardwareId() . ','; } elseif( true == array_key_exists( 'HardwareId', $this->getChangedColumns() ) ) { $strSql .= ' hardware_id = ' . $this->sqlHardwareId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_transaction_id = ' . $this->sqlDatabaseTransactionId() . ','; } elseif( true == array_key_exists( 'DatabaseTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' database_transaction_id = ' . $this->sqlDatabaseTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' summary = ' . $this->sqlSummary() . ','; } elseif( true == array_key_exists( 'Summary', $this->getChangedColumns() ) ) { $strSql .= ' summary = ' . $this->sqlSummary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_datetime = ' . $this->sqlBeginDatetime() . ','; } elseif( true == array_key_exists( 'BeginDatetime', $this->getChangedColumns() ) ) { $strSql .= ' begin_datetime = ' . $this->sqlBeginDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; } elseif( true == array_key_exists( 'EndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; } elseif( true == array_key_exists( 'FailedOn', $this->getChangedColumns() ) ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_on = ' . $this->sqlResolvedOn() . ','; } elseif( true == array_key_exists( 'ResolvedOn', $this->getChangedColumns() ) ) { $strSql .= ' resolved_on = ' . $this->sqlResolvedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dismissed_by = ' . $this->sqlDismissedBy() . ','; } elseif( true == array_key_exists( 'DismissedBy', $this->getChangedColumns() ) ) { $strSql .= ' dismissed_by = ' . $this->sqlDismissedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dismissed_on = ' . $this->sqlDismissedOn() . ','; } elseif( true == array_key_exists( 'DismissedOn', $this->getChangedColumns() ) ) { $strSql .= ' dismissed_on = ' . $this->sqlDismissedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cluster_id' => $this->getClusterId(),
			'database_id' => $this->getDatabaseId(),
			'database_transaction_type_id' => $this->getDatabaseTransactionTypeId(),
			'script_id' => $this->getScriptId(),
			'hardware_id' => $this->getHardwareId(),
			'database_transaction_id' => $this->getDatabaseTransactionId(),
			'summary' => $this->getSummary(),
			'begin_datetime' => $this->getBeginDatetime(),
			'end_datetime' => $this->getEndDatetime(),
			'description' => $this->getDescription(),
			'failed_on' => $this->getFailedOn(),
			'resolved_on' => $this->getResolvedOn(),
			'dismissed_by' => $this->getDismissedBy(),
			'dismissed_on' => $this->getDismissedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>