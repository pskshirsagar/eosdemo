<?php

class CBaseSessionLog extends CEosSingularBase {

	const TABLE_NAME = 'public.session_logs';

	protected $m_intId;
	protected $m_strClientName;
	protected $m_strPropertyName;
	protected $m_strCustomerNameFull;
	protected $m_strWebsiteName;
	protected $m_intPsProductId;
	protected $m_strSessionId;
	protected $m_strServerIp;
	protected $m_strRequesterIp;
	protected $m_strModule;
	protected $m_strAction;
	protected $m_strRequestUrl;
	protected $m_strRequestDatetime;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['client_name'] ) && $boolDirectSet ) $this->set( 'm_strClientName', trim( stripcslashes( $arrValues['client_name'] ) ) ); elseif( isset( $arrValues['client_name'] ) ) $this->setClientName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_name'] ) : $arrValues['client_name'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( stripcslashes( $arrValues['property_name'] ) ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_name'] ) : $arrValues['property_name'] );
		if( isset( $arrValues['customer_name_full'] ) && $boolDirectSet ) $this->set( 'm_strCustomerNameFull', trim( stripcslashes( $arrValues['customer_name_full'] ) ) ); elseif( isset( $arrValues['customer_name_full'] ) ) $this->setCustomerNameFull( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_name_full'] ) : $arrValues['customer_name_full'] );
		if( isset( $arrValues['website_name'] ) && $boolDirectSet ) $this->set( 'm_strWebsiteName', trim( stripcslashes( $arrValues['website_name'] ) ) ); elseif( isset( $arrValues['website_name'] ) ) $this->setWebsiteName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['website_name'] ) : $arrValues['website_name'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['session_id'] ) && $boolDirectSet ) $this->set( 'm_strSessionId', trim( stripcslashes( $arrValues['session_id'] ) ) ); elseif( isset( $arrValues['session_id'] ) ) $this->setSessionId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['session_id'] ) : $arrValues['session_id'] );
		if( isset( $arrValues['server_ip'] ) && $boolDirectSet ) $this->set( 'm_strServerIp', trim( stripcslashes( $arrValues['server_ip'] ) ) ); elseif( isset( $arrValues['server_ip'] ) ) $this->setServerIp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['server_ip'] ) : $arrValues['server_ip'] );
		if( isset( $arrValues['requester_ip'] ) && $boolDirectSet ) $this->set( 'm_strRequesterIp', trim( stripcslashes( $arrValues['requester_ip'] ) ) ); elseif( isset( $arrValues['requester_ip'] ) ) $this->setRequesterIp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['requester_ip'] ) : $arrValues['requester_ip'] );
		if( isset( $arrValues['module'] ) && $boolDirectSet ) $this->set( 'm_strModule', trim( stripcslashes( $arrValues['module'] ) ) ); elseif( isset( $arrValues['module'] ) ) $this->setModule( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['module'] ) : $arrValues['module'] );
		if( isset( $arrValues['action'] ) && $boolDirectSet ) $this->set( 'm_strAction', trim( stripcslashes( $arrValues['action'] ) ) ); elseif( isset( $arrValues['action'] ) ) $this->setAction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action'] ) : $arrValues['action'] );
		if( isset( $arrValues['request_url'] ) && $boolDirectSet ) $this->set( 'm_strRequestUrl', trim( stripcslashes( $arrValues['request_url'] ) ) ); elseif( isset( $arrValues['request_url'] ) ) $this->setRequestUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['request_url'] ) : $arrValues['request_url'] );
		if( isset( $arrValues['request_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRequestDatetime', trim( $arrValues['request_datetime'] ) ); elseif( isset( $arrValues['request_datetime'] ) ) $this->setRequestDatetime( $arrValues['request_datetime'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setClientName( $strClientName ) {
		$this->set( 'm_strClientName', CStrings::strTrimDef( $strClientName, 100, NULL, true ) );
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function sqlClientName() {
		return ( true == isset( $this->m_strClientName ) ) ? '\'' . addslashes( $this->m_strClientName ) . '\'' : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 50, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? '\'' . addslashes( $this->m_strPropertyName ) . '\'' : 'NULL';
	}

	public function setCustomerNameFull( $strCustomerNameFull ) {
		$this->set( 'm_strCustomerNameFull', CStrings::strTrimDef( $strCustomerNameFull, 200, NULL, true ) );
	}

	public function getCustomerNameFull() {
		return $this->m_strCustomerNameFull;
	}

	public function sqlCustomerNameFull() {
		return ( true == isset( $this->m_strCustomerNameFull ) ) ? '\'' . addslashes( $this->m_strCustomerNameFull ) . '\'' : 'NULL';
	}

	public function setWebsiteName( $strWebsiteName ) {
		$this->set( 'm_strWebsiteName', CStrings::strTrimDef( $strWebsiteName, 50, NULL, true ) );
	}

	public function getWebsiteName() {
		return $this->m_strWebsiteName;
	}

	public function sqlWebsiteName() {
		return ( true == isset( $this->m_strWebsiteName ) ) ? '\'' . addslashes( $this->m_strWebsiteName ) . '\'' : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setSessionId( $strSessionId ) {
		$this->set( 'm_strSessionId', CStrings::strTrimDef( $strSessionId, 250, NULL, true ) );
	}

	public function getSessionId() {
		return $this->m_strSessionId;
	}

	public function sqlSessionId() {
		return ( true == isset( $this->m_strSessionId ) ) ? '\'' . addslashes( $this->m_strSessionId ) . '\'' : 'NULL';
	}

	public function setServerIp( $strServerIp ) {
		$this->set( 'm_strServerIp', CStrings::strTrimDef( $strServerIp, 50, NULL, true ) );
	}

	public function getServerIp() {
		return $this->m_strServerIp;
	}

	public function sqlServerIp() {
		return ( true == isset( $this->m_strServerIp ) ) ? '\'' . addslashes( $this->m_strServerIp ) . '\'' : 'NULL';
	}

	public function setRequesterIp( $strRequesterIp ) {
		$this->set( 'm_strRequesterIp', CStrings::strTrimDef( $strRequesterIp, 150, NULL, true ) );
	}

	public function getRequesterIp() {
		return $this->m_strRequesterIp;
	}

	public function sqlRequesterIp() {
		return ( true == isset( $this->m_strRequesterIp ) ) ? '\'' . addslashes( $this->m_strRequesterIp ) . '\'' : 'NULL';
	}

	public function setModule( $strModule ) {
		$this->set( 'm_strModule', CStrings::strTrimDef( $strModule, 250, NULL, true ) );
	}

	public function getModule() {
		return $this->m_strModule;
	}

	public function sqlModule() {
		return ( true == isset( $this->m_strModule ) ) ? '\'' . addslashes( $this->m_strModule ) . '\'' : 'NULL';
	}

	public function setAction( $strAction ) {
		$this->set( 'm_strAction', CStrings::strTrimDef( $strAction, 250, NULL, true ) );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function sqlAction() {
		return ( true == isset( $this->m_strAction ) ) ? '\'' . addslashes( $this->m_strAction ) . '\'' : 'NULL';
	}

	public function setRequestUrl( $strRequestUrl ) {
		$this->set( 'm_strRequestUrl', CStrings::strTrimDef( $strRequestUrl, 2083, NULL, true ) );
	}

	public function getRequestUrl() {
		return $this->m_strRequestUrl;
	}

	public function sqlRequestUrl() {
		return ( true == isset( $this->m_strRequestUrl ) ) ? '\'' . addslashes( $this->m_strRequestUrl ) . '\'' : 'NULL';
	}

	public function setRequestDatetime( $strRequestDatetime ) {
		$this->set( 'm_strRequestDatetime', CStrings::strTrimDef( $strRequestDatetime, -1, NULL, true ) );
	}

	public function getRequestDatetime() {
		return $this->m_strRequestDatetime;
	}

	public function sqlRequestDatetime() {
		return ( true == isset( $this->m_strRequestDatetime ) ) ? '\'' . $this->m_strRequestDatetime . '\'' : 'NOW()';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'client_name' => $this->getClientName(),
			'property_name' => $this->getPropertyName(),
			'customer_name_full' => $this->getCustomerNameFull(),
			'website_name' => $this->getWebsiteName(),
			'ps_product_id' => $this->getPsProductId(),
			'session_id' => $this->getSessionId(),
			'server_ip' => $this->getServerIp(),
			'requester_ip' => $this->getRequesterIp(),
			'module' => $this->getModule(),
			'action' => $this->getAction(),
			'request_url' => $this->getRequestUrl(),
			'request_datetime' => $this->getRequestDatetime()
		);
	}

}
?>