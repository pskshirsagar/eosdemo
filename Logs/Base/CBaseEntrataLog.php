<?php

class CBaseEntrataLog extends CEosSingularBase {

	const TABLE_NAME = 'public.entrata_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_strModuleName;
	protected $m_strActionName;
	protected $m_intHits;
	protected $m_strLogDate;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strFilterKey;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['module_name'] ) && $boolDirectSet ) $this->set( 'm_strModuleName', trim( stripcslashes( $arrValues['module_name'] ) ) ); elseif( isset( $arrValues['module_name'] ) ) $this->setModuleName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['module_name'] ) : $arrValues['module_name'] );
		if( isset( $arrValues['action_name'] ) && $boolDirectSet ) $this->set( 'm_strActionName', trim( stripcslashes( $arrValues['action_name'] ) ) ); elseif( isset( $arrValues['action_name'] ) ) $this->setActionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action_name'] ) : $arrValues['action_name'] );
		if( isset( $arrValues['hits'] ) && $boolDirectSet ) $this->set( 'm_intHits', trim( $arrValues['hits'] ) ); elseif( isset( $arrValues['hits'] ) ) $this->setHits( $arrValues['hits'] );
		if( isset( $arrValues['log_date'] ) && $boolDirectSet ) $this->set( 'm_strLogDate', trim( $arrValues['log_date'] ) ); elseif( isset( $arrValues['log_date'] ) ) $this->setLogDate( $arrValues['log_date'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['filter_key'] ) && $boolDirectSet ) $this->set( 'm_strFilterKey', trim( stripcslashes( $arrValues['filter_key'] ) ) ); elseif( isset( $arrValues['filter_key'] ) ) $this->setFilterKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['filter_key'] ) : $arrValues['filter_key'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setModuleName( $strModuleName ) {
		$this->set( 'm_strModuleName', CStrings::strTrimDef( $strModuleName, 100, NULL, true ) );
	}

	public function getModuleName() {
		return $this->m_strModuleName;
	}

	public function sqlModuleName() {
		return ( true == isset( $this->m_strModuleName ) ) ? '\'' . addslashes( $this->m_strModuleName ) . '\'' : 'NULL';
	}

	public function setActionName( $strActionName ) {
		$this->set( 'm_strActionName', CStrings::strTrimDef( $strActionName, 100, NULL, true ) );
	}

	public function getActionName() {
		return $this->m_strActionName;
	}

	public function sqlActionName() {
		return ( true == isset( $this->m_strActionName ) ) ? '\'' . addslashes( $this->m_strActionName ) . '\'' : 'NULL';
	}

	public function setHits( $intHits ) {
		$this->set( 'm_intHits', CStrings::strToIntDef( $intHits, NULL, false ) );
	}

	public function getHits() {
		return $this->m_intHits;
	}

	public function sqlHits() {
		return ( true == isset( $this->m_intHits ) ) ? ( string ) $this->m_intHits : 'NULL';
	}

	public function setLogDate( $strLogDate ) {
		$this->set( 'm_strLogDate', CStrings::strTrimDef( $strLogDate, -1, NULL, true ) );
	}

	public function getLogDate() {
		return $this->m_strLogDate;
	}

	public function sqlLogDate() {
		return ( true == isset( $this->m_strLogDate ) ) ? '\'' . $this->m_strLogDate . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setFilterKey( $strFilterKey ) {
		$this->set( 'm_strFilterKey', CStrings::strTrimDef( $strFilterKey, 100, NULL, true ) );
	}

	public function getFilterKey() {
		return $this->m_strFilterKey;
	}

	public function sqlFilterKey() {
		return ( true == isset( $this->m_strFilterKey ) ) ? '\'' . addslashes( $this->m_strFilterKey ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, module_name, action_name, hits, log_date, created_by, created_on, filter_key )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlModuleName() . ', ' .
 						$this->sqlActionName() . ', ' .
 						$this->sqlHits() . ', ' .
 						$this->sqlLogDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlFilterKey() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' module_name = ' . $this->sqlModuleName() . ','; } elseif( true == array_key_exists( 'ModuleName', $this->getChangedColumns() ) ) { $strSql .= ' module_name = ' . $this->sqlModuleName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_name = ' . $this->sqlActionName() . ','; } elseif( true == array_key_exists( 'ActionName', $this->getChangedColumns() ) ) { $strSql .= ' action_name = ' . $this->sqlActionName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hits = ' . $this->sqlHits() . ','; } elseif( true == array_key_exists( 'Hits', $this->getChangedColumns() ) ) { $strSql .= ' hits = ' . $this->sqlHits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_date = ' . $this->sqlLogDate() . ','; } elseif( true == array_key_exists( 'LogDate', $this->getChangedColumns() ) ) { $strSql .= ' log_date = ' . $this->sqlLogDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' filter_key = ' . $this->sqlFilterKey() . ','; } elseif( true == array_key_exists( 'FilterKey', $this->getChangedColumns() ) ) { $strSql .= ' filter_key = ' . $this->sqlFilterKey() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'module_name' => $this->getModuleName(),
			'action_name' => $this->getActionName(),
			'hits' => $this->getHits(),
			'log_date' => $this->getLogDate(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'filter_key' => $this->getFilterKey()
		);
	}

}
?>