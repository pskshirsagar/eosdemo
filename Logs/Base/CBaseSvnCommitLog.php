<?php

class CBaseSvnCommitLog extends CEosSingularBase {

	const TABLE_NAME = 'public.svn_commit_logs';

	protected $m_intId;
	protected $m_intUserId;
	protected $m_intSvnRepositoryId;
	protected $m_strRevisionId;
	protected $m_intCodeAuditorId;
	protected $m_strSvnUsername;
	protected $m_strCommitMessage;
	protected $m_strCommitDatetime;
	protected $m_intSvnCommitFilesCount;
	protected $m_intFilesAdded;
	protected $m_intFilesDeleted;
	protected $m_intTotalLineInsertions;
	protected $m_intTotalLineDeletions;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intRepositoryId;
	protected $m_intGitClusterRepositoryId;

	public function __construct() {
		parent::__construct();

		$this->m_intFilesAdded = '0';
		$this->m_intFilesDeleted = '0';
		$this->m_intTotalLineInsertions = '0';
		$this->m_intTotalLineDeletions = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['svn_repository_id'] ) && $boolDirectSet ) $this->set( 'm_intSvnRepositoryId', trim( $arrValues['svn_repository_id'] ) ); elseif( isset( $arrValues['svn_repository_id'] ) ) $this->setSvnRepositoryId( $arrValues['svn_repository_id'] );
		if( isset( $arrValues['revision_id'] ) && $boolDirectSet ) $this->set( 'm_strRevisionId', trim( $arrValues['revision_id'] ) ); elseif( isset( $arrValues['revision_id'] ) ) $this->setRevisionId( $arrValues['revision_id'] );
		if( isset( $arrValues['code_auditor_id'] ) && $boolDirectSet ) $this->set( 'm_intCodeAuditorId', trim( $arrValues['code_auditor_id'] ) ); elseif( isset( $arrValues['code_auditor_id'] ) ) $this->setCodeAuditorId( $arrValues['code_auditor_id'] );
		if( isset( $arrValues['svn_username'] ) && $boolDirectSet ) $this->set( 'm_strSvnUsername', trim( $arrValues['svn_username'] ) ); elseif( isset( $arrValues['svn_username'] ) ) $this->setSvnUsername( $arrValues['svn_username'] );
		if( isset( $arrValues['commit_message'] ) && $boolDirectSet ) $this->set( 'm_strCommitMessage', trim( $arrValues['commit_message'] ) ); elseif( isset( $arrValues['commit_message'] ) ) $this->setCommitMessage( $arrValues['commit_message'] );
		if( isset( $arrValues['commit_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCommitDatetime', trim( $arrValues['commit_datetime'] ) ); elseif( isset( $arrValues['commit_datetime'] ) ) $this->setCommitDatetime( $arrValues['commit_datetime'] );
		if( isset( $arrValues['svn_commit_files_count'] ) && $boolDirectSet ) $this->set( 'm_intSvnCommitFilesCount', trim( $arrValues['svn_commit_files_count'] ) ); elseif( isset( $arrValues['svn_commit_files_count'] ) ) $this->setSvnCommitFilesCount( $arrValues['svn_commit_files_count'] );
		if( isset( $arrValues['files_added'] ) && $boolDirectSet ) $this->set( 'm_intFilesAdded', trim( $arrValues['files_added'] ) ); elseif( isset( $arrValues['files_added'] ) ) $this->setFilesAdded( $arrValues['files_added'] );
		if( isset( $arrValues['files_deleted'] ) && $boolDirectSet ) $this->set( 'm_intFilesDeleted', trim( $arrValues['files_deleted'] ) ); elseif( isset( $arrValues['files_deleted'] ) ) $this->setFilesDeleted( $arrValues['files_deleted'] );
		if( isset( $arrValues['total_line_insertions'] ) && $boolDirectSet ) $this->set( 'm_intTotalLineInsertions', trim( $arrValues['total_line_insertions'] ) ); elseif( isset( $arrValues['total_line_insertions'] ) ) $this->setTotalLineInsertions( $arrValues['total_line_insertions'] );
		if( isset( $arrValues['total_line_deletions'] ) && $boolDirectSet ) $this->set( 'm_intTotalLineDeletions', trim( $arrValues['total_line_deletions'] ) ); elseif( isset( $arrValues['total_line_deletions'] ) ) $this->setTotalLineDeletions( $arrValues['total_line_deletions'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['repository_id'] ) && $boolDirectSet ) $this->set( 'm_intRepositoryId', trim( $arrValues['repository_id'] ) ); elseif( isset( $arrValues['repository_id'] ) ) $this->setRepositoryId( $arrValues['repository_id'] );
		if( isset( $arrValues['git_cluster_repository_id'] ) && $boolDirectSet ) $this->set( 'm_intGitClusterRepositoryId', trim( $arrValues['git_cluster_repository_id'] ) ); elseif( isset( $arrValues['git_cluster_repository_id'] ) ) $this->setGitClusterRepositoryId( $arrValues['git_cluster_repository_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setSvnRepositoryId( $intSvnRepositoryId ) {
		$this->set( 'm_intSvnRepositoryId', CStrings::strToIntDef( $intSvnRepositoryId, NULL, false ) );
	}

	public function getSvnRepositoryId() {
		return $this->m_intSvnRepositoryId;
	}

	public function sqlSvnRepositoryId() {
		return ( true == isset( $this->m_intSvnRepositoryId ) ) ? ( string ) $this->m_intSvnRepositoryId : 'NULL';
	}

	public function setRevisionId( $strRevisionId ) {
		$this->set( 'm_strRevisionId', CStrings::strTrimDef( $strRevisionId, 50, NULL, true ) );
	}

	public function getRevisionId() {
		return $this->m_strRevisionId;
	}

	public function sqlRevisionId() {
		return ( true == isset( $this->m_strRevisionId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRevisionId ) : '\'' . addslashes( $this->m_strRevisionId ) . '\'' ) : 'NULL';
	}

	public function setCodeAuditorId( $intCodeAuditorId ) {
		$this->set( 'm_intCodeAuditorId', CStrings::strToIntDef( $intCodeAuditorId, NULL, false ) );
	}

	public function getCodeAuditorId() {
		return $this->m_intCodeAuditorId;
	}

	public function sqlCodeAuditorId() {
		return ( true == isset( $this->m_intCodeAuditorId ) ) ? ( string ) $this->m_intCodeAuditorId : 'NULL';
	}

	public function setSvnUsername( $strSvnUsername ) {
		$this->set( 'm_strSvnUsername', CStrings::strTrimDef( $strSvnUsername, 240, NULL, true ) );
	}

	public function getSvnUsername() {
		return $this->m_strSvnUsername;
	}

	public function sqlSvnUsername() {
		return ( true == isset( $this->m_strSvnUsername ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSvnUsername ) : '\'' . addslashes( $this->m_strSvnUsername ) . '\'' ) : 'NULL';
	}

	public function setCommitMessage( $strCommitMessage ) {
		$this->set( 'm_strCommitMessage', CStrings::strTrimDef( $strCommitMessage, -1, NULL, true ) );
	}

	public function getCommitMessage() {
		return $this->m_strCommitMessage;
	}

	public function sqlCommitMessage() {
		return ( true == isset( $this->m_strCommitMessage ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCommitMessage ) : '\'' . addslashes( $this->m_strCommitMessage ) . '\'' ) : 'NULL';
	}

	public function setCommitDatetime( $strCommitDatetime ) {
		$this->set( 'm_strCommitDatetime', CStrings::strTrimDef( $strCommitDatetime, -1, NULL, true ) );
	}

	public function getCommitDatetime() {
		return $this->m_strCommitDatetime;
	}

	public function sqlCommitDatetime() {
		return ( true == isset( $this->m_strCommitDatetime ) ) ? '\'' . $this->m_strCommitDatetime . '\'' : 'NOW()';
	}

	public function setSvnCommitFilesCount( $intSvnCommitFilesCount ) {
		$this->set( 'm_intSvnCommitFilesCount', CStrings::strToIntDef( $intSvnCommitFilesCount, NULL, false ) );
	}

	public function getSvnCommitFilesCount() {
		return $this->m_intSvnCommitFilesCount;
	}

	public function sqlSvnCommitFilesCount() {
		return ( true == isset( $this->m_intSvnCommitFilesCount ) ) ? ( string ) $this->m_intSvnCommitFilesCount : 'NULL';
	}

	public function setFilesAdded( $intFilesAdded ) {
		$this->set( 'm_intFilesAdded', CStrings::strToIntDef( $intFilesAdded, NULL, false ) );
	}

	public function getFilesAdded() {
		return $this->m_intFilesAdded;
	}

	public function sqlFilesAdded() {
		return ( true == isset( $this->m_intFilesAdded ) ) ? ( string ) $this->m_intFilesAdded : '0';
	}

	public function setFilesDeleted( $intFilesDeleted ) {
		$this->set( 'm_intFilesDeleted', CStrings::strToIntDef( $intFilesDeleted, NULL, false ) );
	}

	public function getFilesDeleted() {
		return $this->m_intFilesDeleted;
	}

	public function sqlFilesDeleted() {
		return ( true == isset( $this->m_intFilesDeleted ) ) ? ( string ) $this->m_intFilesDeleted : '0';
	}

	public function setTotalLineInsertions( $intTotalLineInsertions ) {
		$this->set( 'm_intTotalLineInsertions', CStrings::strToIntDef( $intTotalLineInsertions, NULL, false ) );
	}

	public function getTotalLineInsertions() {
		return $this->m_intTotalLineInsertions;
	}

	public function sqlTotalLineInsertions() {
		return ( true == isset( $this->m_intTotalLineInsertions ) ) ? ( string ) $this->m_intTotalLineInsertions : '0';
	}

	public function setTotalLineDeletions( $intTotalLineDeletions ) {
		$this->set( 'm_intTotalLineDeletions', CStrings::strToIntDef( $intTotalLineDeletions, NULL, false ) );
	}

	public function getTotalLineDeletions() {
		return $this->m_intTotalLineDeletions;
	}

	public function sqlTotalLineDeletions() {
		return ( true == isset( $this->m_intTotalLineDeletions ) ) ? ( string ) $this->m_intTotalLineDeletions : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setRepositoryId( $intRepositoryId ) {
		$this->set( 'm_intRepositoryId', CStrings::strToIntDef( $intRepositoryId, NULL, false ) );
	}

	public function getRepositoryId() {
		return $this->m_intRepositoryId;
	}

	public function sqlRepositoryId() {
		return ( true == isset( $this->m_intRepositoryId ) ) ? ( string ) $this->m_intRepositoryId : 'NULL';
	}

	public function setGitClusterRepositoryId( $intGitClusterRepositoryId ) {
		$this->set( 'm_intGitClusterRepositoryId', CStrings::strToIntDef( $intGitClusterRepositoryId, NULL, false ) );
	}

	public function getGitClusterRepositoryId() {
		return $this->m_intGitClusterRepositoryId;
	}

	public function sqlGitClusterRepositoryId() {
		return ( true == isset( $this->m_intGitClusterRepositoryId ) ) ? ( string ) $this->m_intGitClusterRepositoryId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, user_id, svn_repository_id, revision_id, code_auditor_id, svn_username, commit_message, commit_datetime, svn_commit_files_count, files_added, files_deleted, total_line_insertions, total_line_deletions, created_by, created_on, repository_id, git_cluster_repository_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlUserId() . ', ' .
						$this->sqlSvnRepositoryId() . ', ' .
						$this->sqlRevisionId() . ', ' .
						$this->sqlCodeAuditorId() . ', ' .
						$this->sqlSvnUsername() . ', ' .
						$this->sqlCommitMessage() . ', ' .
						$this->sqlCommitDatetime() . ', ' .
						$this->sqlSvnCommitFilesCount() . ', ' .
						$this->sqlFilesAdded() . ', ' .
						$this->sqlFilesDeleted() . ', ' .
						$this->sqlTotalLineInsertions() . ', ' .
						$this->sqlTotalLineDeletions() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlRepositoryId() . ', ' .
						$this->sqlGitClusterRepositoryId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId(). ',' ; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_repository_id = ' . $this->sqlSvnRepositoryId(). ',' ; } elseif( true == array_key_exists( 'SvnRepositoryId', $this->getChangedColumns() ) ) { $strSql .= ' svn_repository_id = ' . $this->sqlSvnRepositoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' revision_id = ' . $this->sqlRevisionId(). ',' ; } elseif( true == array_key_exists( 'RevisionId', $this->getChangedColumns() ) ) { $strSql .= ' revision_id = ' . $this->sqlRevisionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' code_auditor_id = ' . $this->sqlCodeAuditorId(). ',' ; } elseif( true == array_key_exists( 'CodeAuditorId', $this->getChangedColumns() ) ) { $strSql .= ' code_auditor_id = ' . $this->sqlCodeAuditorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_username = ' . $this->sqlSvnUsername(). ',' ; } elseif( true == array_key_exists( 'SvnUsername', $this->getChangedColumns() ) ) { $strSql .= ' svn_username = ' . $this->sqlSvnUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commit_message = ' . $this->sqlCommitMessage(). ',' ; } elseif( true == array_key_exists( 'CommitMessage', $this->getChangedColumns() ) ) { $strSql .= ' commit_message = ' . $this->sqlCommitMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commit_datetime = ' . $this->sqlCommitDatetime(). ',' ; } elseif( true == array_key_exists( 'CommitDatetime', $this->getChangedColumns() ) ) { $strSql .= ' commit_datetime = ' . $this->sqlCommitDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_commit_files_count = ' . $this->sqlSvnCommitFilesCount(). ',' ; } elseif( true == array_key_exists( 'SvnCommitFilesCount', $this->getChangedColumns() ) ) { $strSql .= ' svn_commit_files_count = ' . $this->sqlSvnCommitFilesCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' files_added = ' . $this->sqlFilesAdded(). ',' ; } elseif( true == array_key_exists( 'FilesAdded', $this->getChangedColumns() ) ) { $strSql .= ' files_added = ' . $this->sqlFilesAdded() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' files_deleted = ' . $this->sqlFilesDeleted(). ',' ; } elseif( true == array_key_exists( 'FilesDeleted', $this->getChangedColumns() ) ) { $strSql .= ' files_deleted = ' . $this->sqlFilesDeleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_line_insertions = ' . $this->sqlTotalLineInsertions(). ',' ; } elseif( true == array_key_exists( 'TotalLineInsertions', $this->getChangedColumns() ) ) { $strSql .= ' total_line_insertions = ' . $this->sqlTotalLineInsertions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_line_deletions = ' . $this->sqlTotalLineDeletions(). ',' ; } elseif( true == array_key_exists( 'TotalLineDeletions', $this->getChangedColumns() ) ) { $strSql .= ' total_line_deletions = ' . $this->sqlTotalLineDeletions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repository_id = ' . $this->sqlRepositoryId(). ',' ; } elseif( true == array_key_exists( 'RepositoryId', $this->getChangedColumns() ) ) { $strSql .= ' repository_id = ' . $this->sqlRepositoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' git_cluster_repository_id = ' . $this->sqlGitClusterRepositoryId() ; } elseif( true == array_key_exists( 'GitClusterRepositoryId', $this->getChangedColumns() ) ) { $strSql .= ' git_cluster_repository_id = ' . $this->sqlGitClusterRepositoryId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'user_id' => $this->getUserId(),
			'svn_repository_id' => $this->getSvnRepositoryId(),
			'revision_id' => $this->getRevisionId(),
			'code_auditor_id' => $this->getCodeAuditorId(),
			'svn_username' => $this->getSvnUsername(),
			'commit_message' => $this->getCommitMessage(),
			'commit_datetime' => $this->getCommitDatetime(),
			'svn_commit_files_count' => $this->getSvnCommitFilesCount(),
			'files_added' => $this->getFilesAdded(),
			'files_deleted' => $this->getFilesDeleted(),
			'total_line_insertions' => $this->getTotalLineInsertions(),
			'total_line_deletions' => $this->getTotalLineDeletions(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'repository_id' => $this->getRepositoryId(),
			'git_cluster_repository_id' => $this->getGitClusterRepositoryId()
		);
	}

}
?>