<?php

class CBaseSystemErrorDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.system_error_details';

	protected $m_intId;
	protected $m_intSystemErrorId;
	protected $m_intPsProductId;
	protected $m_intPsProductModuleId;
	protected $m_intPsProductOptionId;
	protected $m_strClient;
	protected $m_strBrowser;
	protected $m_strServer;
	protected $m_strModule;
	protected $m_strAction;
	protected $m_strDescription;
	protected $m_strBacktrace;
	protected $m_intErrorCount;
	protected $m_intIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intErrorCount = '1';
		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['system_error_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemErrorId', trim( $arrValues['system_error_id'] ) ); elseif( isset( $arrValues['system_error_id'] ) ) $this->setSystemErrorId( $arrValues['system_error_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ps_product_module_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductModuleId', trim( $arrValues['ps_product_module_id'] ) ); elseif( isset( $arrValues['ps_product_module_id'] ) ) $this->setPsProductModuleId( $arrValues['ps_product_module_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductOptionId', trim( $arrValues['ps_product_option_id'] ) ); elseif( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['client'] ) && $boolDirectSet ) $this->set( 'm_strClient', trim( stripcslashes( $arrValues['client'] ) ) ); elseif( isset( $arrValues['client'] ) ) $this->setClient( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client'] ) : $arrValues['client'] );
		if( isset( $arrValues['browser'] ) && $boolDirectSet ) $this->set( 'm_strBrowser', trim( stripcslashes( $arrValues['browser'] ) ) ); elseif( isset( $arrValues['browser'] ) ) $this->setBrowser( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['browser'] ) : $arrValues['browser'] );
		if( isset( $arrValues['server'] ) && $boolDirectSet ) $this->set( 'm_strServer', trim( stripcslashes( $arrValues['server'] ) ) ); elseif( isset( $arrValues['server'] ) ) $this->setServer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['server'] ) : $arrValues['server'] );
		if( isset( $arrValues['module'] ) && $boolDirectSet ) $this->set( 'm_strModule', trim( stripcslashes( $arrValues['module'] ) ) ); elseif( isset( $arrValues['module'] ) ) $this->setModule( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['module'] ) : $arrValues['module'] );
		if( isset( $arrValues['action'] ) && $boolDirectSet ) $this->set( 'm_strAction', trim( stripcslashes( $arrValues['action'] ) ) ); elseif( isset( $arrValues['action'] ) ) $this->setAction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action'] ) : $arrValues['action'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['backtrace'] ) && $boolDirectSet ) $this->set( 'm_strBacktrace', trim( stripcslashes( $arrValues['backtrace'] ) ) ); elseif( isset( $arrValues['backtrace'] ) ) $this->setBacktrace( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['backtrace'] ) : $arrValues['backtrace'] );
		if( isset( $arrValues['error_count'] ) && $boolDirectSet ) $this->set( 'm_intErrorCount', trim( $arrValues['error_count'] ) ); elseif( isset( $arrValues['error_count'] ) ) $this->setErrorCount( $arrValues['error_count'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSystemErrorId( $intSystemErrorId ) {
		$this->set( 'm_intSystemErrorId', CStrings::strToIntDef( $intSystemErrorId, NULL, false ) );
	}

	public function getSystemErrorId() {
		return $this->m_intSystemErrorId;
	}

	public function sqlSystemErrorId() {
		return ( true == isset( $this->m_intSystemErrorId ) ) ? ( string ) $this->m_intSystemErrorId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsProductModuleId( $intPsProductModuleId ) {
		$this->set( 'm_intPsProductModuleId', CStrings::strToIntDef( $intPsProductModuleId, NULL, false ) );
	}

	public function getPsProductModuleId() {
		return $this->m_intPsProductModuleId;
	}

	public function sqlPsProductModuleId() {
		return ( true == isset( $this->m_intPsProductModuleId ) ) ? ( string ) $this->m_intPsProductModuleId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->set( 'm_intPsProductOptionId', CStrings::strToIntDef( $intPsProductOptionId, NULL, false ) );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setClient( $strClient ) {
		$this->set( 'm_strClient', CStrings::strTrimDef( $strClient, -1, NULL, true ) );
	}

	public function getClient() {
		return $this->m_strClient;
	}

	public function sqlClient() {
		return ( true == isset( $this->m_strClient ) ) ? '\'' . addslashes( $this->m_strClient ) . '\'' : 'NULL';
	}

	public function setBrowser( $strBrowser ) {
		$this->set( 'm_strBrowser', CStrings::strTrimDef( $strBrowser, 240, NULL, true ) );
	}

	public function getBrowser() {
		return $this->m_strBrowser;
	}

	public function sqlBrowser() {
		return ( true == isset( $this->m_strBrowser ) ) ? '\'' . addslashes( $this->m_strBrowser ) . '\'' : 'NULL';
	}

	public function setServer( $strServer ) {
		$this->set( 'm_strServer', CStrings::strTrimDef( $strServer, -1, NULL, true ) );
	}

	public function getServer() {
		return $this->m_strServer;
	}

	public function sqlServer() {
		return ( true == isset( $this->m_strServer ) ) ? '\'' . addslashes( $this->m_strServer ) . '\'' : 'NULL';
	}

	public function setModule( $strModule ) {
		$this->set( 'm_strModule', CStrings::strTrimDef( $strModule, -1, NULL, true ) );
	}

	public function getModule() {
		return $this->m_strModule;
	}

	public function sqlModule() {
		return ( true == isset( $this->m_strModule ) ) ? '\'' . addslashes( $this->m_strModule ) . '\'' : 'NULL';
	}

	public function setAction( $strAction ) {
		$this->set( 'm_strAction', CStrings::strTrimDef( $strAction, -1, NULL, true ) );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function sqlAction() {
		return ( true == isset( $this->m_strAction ) ) ? '\'' . addslashes( $this->m_strAction ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setBacktrace( $strBacktrace ) {
		$this->set( 'm_strBacktrace', CStrings::strTrimDef( $strBacktrace, -1, NULL, true ) );
	}

	public function getBacktrace() {
		return $this->m_strBacktrace;
	}

	public function sqlBacktrace() {
		return ( true == isset( $this->m_strBacktrace ) ) ? '\'' . addslashes( $this->m_strBacktrace ) . '\'' : 'NULL';
	}

	public function setErrorCount( $intErrorCount ) {
		$this->set( 'm_intErrorCount', CStrings::strToIntDef( $intErrorCount, NULL, false ) );
	}

	public function getErrorCount() {
		return $this->m_intErrorCount;
	}

	public function sqlErrorCount() {
		return ( true == isset( $this->m_intErrorCount ) ) ? ( string ) $this->m_intErrorCount : '1';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, system_error_id, ps_product_id, ps_product_module_id, ps_product_option_id, client, browser, server, module, action, description, backtrace, error_count, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSystemErrorId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlPsProductModuleId() . ', ' .
 						$this->sqlPsProductOptionId() . ', ' .
 						$this->sqlClient() . ', ' .
 						$this->sqlBrowser() . ', ' .
 						$this->sqlServer() . ', ' .
 						$this->sqlModule() . ', ' .
 						$this->sqlAction() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlBacktrace() . ', ' .
 						$this->sqlErrorCount() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_error_id = ' . $this->sqlSystemErrorId() . ','; } elseif( true == array_key_exists( 'SystemErrorId', $this->getChangedColumns() ) ) { $strSql .= ' system_error_id = ' . $this->sqlSystemErrorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_module_id = ' . $this->sqlPsProductModuleId() . ','; } elseif( true == array_key_exists( 'PsProductModuleId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_module_id = ' . $this->sqlPsProductModuleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; } elseif( true == array_key_exists( 'PsProductOptionId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client = ' . $this->sqlClient() . ','; } elseif( true == array_key_exists( 'Client', $this->getChangedColumns() ) ) { $strSql .= ' client = ' . $this->sqlClient() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' browser = ' . $this->sqlBrowser() . ','; } elseif( true == array_key_exists( 'Browser', $this->getChangedColumns() ) ) { $strSql .= ' browser = ' . $this->sqlBrowser() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' server = ' . $this->sqlServer() . ','; } elseif( true == array_key_exists( 'Server', $this->getChangedColumns() ) ) { $strSql .= ' server = ' . $this->sqlServer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' module = ' . $this->sqlModule() . ','; } elseif( true == array_key_exists( 'Module', $this->getChangedColumns() ) ) { $strSql .= ' module = ' . $this->sqlModule() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action = ' . $this->sqlAction() . ','; } elseif( true == array_key_exists( 'Action', $this->getChangedColumns() ) ) { $strSql .= ' action = ' . $this->sqlAction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' backtrace = ' . $this->sqlBacktrace() . ','; } elseif( true == array_key_exists( 'Backtrace', $this->getChangedColumns() ) ) { $strSql .= ' backtrace = ' . $this->sqlBacktrace() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_count = ' . $this->sqlErrorCount() . ','; } elseif( true == array_key_exists( 'ErrorCount', $this->getChangedColumns() ) ) { $strSql .= ' error_count = ' . $this->sqlErrorCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'system_error_id' => $this->getSystemErrorId(),
			'ps_product_id' => $this->getPsProductId(),
			'ps_product_module_id' => $this->getPsProductModuleId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'client' => $this->getClient(),
			'browser' => $this->getBrowser(),
			'server' => $this->getServer(),
			'module' => $this->getModule(),
			'action' => $this->getAction(),
			'description' => $this->getDescription(),
			'backtrace' => $this->getBacktrace(),
			'error_count' => $this->getErrorCount(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>