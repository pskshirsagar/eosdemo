<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CTableFkeys
 * Do not add any new functions to this class.
 */

class CBaseTableFkeys extends CEosPluralBase {

	/**
	 * @return CTableFkey[]
	 */
	public static function fetchTableFkeys( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTableFkey', $objDatabase );
	}

	/**
	 * @return CTableFkey
	 */
	public static function fetchTableFkey( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTableFkey', $objDatabase );
	}

	public static function fetchTableFkeyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'table_fkeys', $objDatabase );
	}

	public static function fetchTableFkeyById( $intId, $objDatabase ) {
		return self::fetchTableFkey( sprintf( 'SELECT * FROM table_fkeys WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTableFkeysByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchTableFkeys( sprintf( 'SELECT * FROM table_fkeys WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

}
?>