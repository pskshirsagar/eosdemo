<?php

class CBaseDiagnostic extends CEosSingularBase {

	const TABLE_NAME = 'public.diagnostics';

	protected $m_intId;
	protected $m_intDiagnosticTypeId;
	protected $m_intFrequencyId;
	protected $m_intTaskPriorityId;
	protected $m_intLastErrorTaskId;
	protected $m_intPsProductId;
	protected $m_intPsProductOptionId;
	protected $m_intDeveloperEmployeeId;
	protected $m_intQaEmployeeId;
	protected $m_intTableId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strRapidViewSql;
	protected $m_strRapidRepairSql;
	protected $m_strStandardViewSql;
	protected $m_strStandardRepairSql;
	protected $m_strBeginDate;
	protected $m_intLastRepairedBy;
	protected $m_strLastRepairedOn;
	protected $m_intAlwaysShow;
	protected $m_intEnableAutoRepair;
	protected $m_boolIsPropertySpecific;
	protected $m_intIsClientSpecific;
	protected $m_boolRunForTestDb;
	protected $m_boolRunOnMasterDb;
	protected $m_intIsWarning;
	protected $m_intIsPhp;
	protected $m_intIsDisabled;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsMigration;
	protected $m_boolIsAudit;
	protected $m_intSdmEmployeeId;
	protected $m_arrintFrequencyDetails;
	protected $m_boolIsMigrationRepair;
	protected $m_arrintStakeholders;
	protected $m_intReplicaLagTime;
	protected $m_strFieldName;

	public function __construct() {
		parent::__construct();

		$this->m_intTaskPriorityId = '1';
		$this->m_intAlwaysShow = '0';
		$this->m_intEnableAutoRepair = '0';
		$this->m_boolIsPropertySpecific = false;
		$this->m_intIsClientSpecific = '0';
		$this->m_boolRunForTestDb = false;
		$this->m_boolRunOnMasterDb = false;
		$this->m_intIsWarning = '1';
		$this->m_intIsPhp = '0';
		$this->m_intIsDisabled = '0';
		$this->m_intOrderNum = '0';
		$this->m_boolIsMigration = false;
		$this->m_boolIsAudit = false;
		$this->m_boolIsMigrationRepair = false;
		$this->m_intReplicaLagTime = '720';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['diagnostic_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDiagnosticTypeId', trim( $arrValues['diagnostic_type_id'] ) ); elseif( isset( $arrValues['diagnostic_type_id'] ) ) $this->setDiagnosticTypeId( $arrValues['diagnostic_type_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['task_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskPriorityId', trim( $arrValues['task_priority_id'] ) ); elseif( isset( $arrValues['task_priority_id'] ) ) $this->setTaskPriorityId( $arrValues['task_priority_id'] );
		if( isset( $arrValues['last_error_task_id'] ) && $boolDirectSet ) $this->set( 'm_intLastErrorTaskId', trim( $arrValues['last_error_task_id'] ) ); elseif( isset( $arrValues['last_error_task_id'] ) ) $this->setLastErrorTaskId( $arrValues['last_error_task_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductOptionId', trim( $arrValues['ps_product_option_id'] ) ); elseif( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['developer_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intDeveloperEmployeeId', trim( $arrValues['developer_employee_id'] ) ); elseif( isset( $arrValues['developer_employee_id'] ) ) $this->setDeveloperEmployeeId( $arrValues['developer_employee_id'] );
		if( isset( $arrValues['qa_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intQaEmployeeId', trim( $arrValues['qa_employee_id'] ) ); elseif( isset( $arrValues['qa_employee_id'] ) ) $this->setQaEmployeeId( $arrValues['qa_employee_id'] );
		if( isset( $arrValues['table_id'] ) && $boolDirectSet ) $this->set( 'm_intTableId', trim( $arrValues['table_id'] ) ); elseif( isset( $arrValues['table_id'] ) ) $this->setTableId( $arrValues['table_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['rapid_view_sql'] ) && $boolDirectSet ) $this->set( 'm_strRapidViewSql', trim( $arrValues['rapid_view_sql'] ) ); elseif( isset( $arrValues['rapid_view_sql'] ) ) $this->setRapidViewSql( $arrValues['rapid_view_sql'] );
		if( isset( $arrValues['rapid_repair_sql'] ) && $boolDirectSet ) $this->set( 'm_strRapidRepairSql', trim( $arrValues['rapid_repair_sql'] ) ); elseif( isset( $arrValues['rapid_repair_sql'] ) ) $this->setRapidRepairSql( $arrValues['rapid_repair_sql'] );
		if( isset( $arrValues['standard_view_sql'] ) && $boolDirectSet ) $this->set( 'm_strStandardViewSql', trim( $arrValues['standard_view_sql'] ) ); elseif( isset( $arrValues['standard_view_sql'] ) ) $this->setStandardViewSql( $arrValues['standard_view_sql'] );
		if( isset( $arrValues['standard_repair_sql'] ) && $boolDirectSet ) $this->set( 'm_strStandardRepairSql', trim( $arrValues['standard_repair_sql'] ) ); elseif( isset( $arrValues['standard_repair_sql'] ) ) $this->setStandardRepairSql( $arrValues['standard_repair_sql'] );
		if( isset( $arrValues['begin_date'] ) && $boolDirectSet ) $this->set( 'm_strBeginDate', trim( $arrValues['begin_date'] ) ); elseif( isset( $arrValues['begin_date'] ) ) $this->setBeginDate( $arrValues['begin_date'] );
		if( isset( $arrValues['last_repaired_by'] ) && $boolDirectSet ) $this->set( 'm_intLastRepairedBy', trim( $arrValues['last_repaired_by'] ) ); elseif( isset( $arrValues['last_repaired_by'] ) ) $this->setLastRepairedBy( $arrValues['last_repaired_by'] );
		if( isset( $arrValues['last_repaired_on'] ) && $boolDirectSet ) $this->set( 'm_strLastRepairedOn', trim( $arrValues['last_repaired_on'] ) ); elseif( isset( $arrValues['last_repaired_on'] ) ) $this->setLastRepairedOn( $arrValues['last_repaired_on'] );
		if( isset( $arrValues['always_show'] ) && $boolDirectSet ) $this->set( 'm_intAlwaysShow', trim( $arrValues['always_show'] ) ); elseif( isset( $arrValues['always_show'] ) ) $this->setAlwaysShow( $arrValues['always_show'] );
		if( isset( $arrValues['enable_auto_repair'] ) && $boolDirectSet ) $this->set( 'm_intEnableAutoRepair', trim( $arrValues['enable_auto_repair'] ) ); elseif( isset( $arrValues['enable_auto_repair'] ) ) $this->setEnableAutoRepair( $arrValues['enable_auto_repair'] );
		if( isset( $arrValues['is_property_specific'] ) && $boolDirectSet ) $this->set( 'm_boolIsPropertySpecific', trim( stripcslashes( $arrValues['is_property_specific'] ) ) ); elseif( isset( $arrValues['is_property_specific'] ) ) $this->setIsPropertySpecific( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_property_specific'] ) : $arrValues['is_property_specific'] );
		if( isset( $arrValues['is_client_specific'] ) && $boolDirectSet ) $this->set( 'm_intIsClientSpecific', trim( $arrValues['is_client_specific'] ) ); elseif( isset( $arrValues['is_client_specific'] ) ) $this->setIsClientSpecific( $arrValues['is_client_specific'] );
		if( isset( $arrValues['run_for_test_db'] ) && $boolDirectSet ) $this->set( 'm_boolRunForTestDb', trim( stripcslashes( $arrValues['run_for_test_db'] ) ) ); elseif( isset( $arrValues['run_for_test_db'] ) ) $this->setRunForTestDb( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['run_for_test_db'] ) : $arrValues['run_for_test_db'] );
		if( isset( $arrValues['run_on_master_db'] ) && $boolDirectSet ) $this->set( 'm_boolRunOnMasterDb', trim( stripcslashes( $arrValues['run_on_master_db'] ) ) ); elseif( isset( $arrValues['run_on_master_db'] ) ) $this->setRunOnMasterDb( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['run_on_master_db'] ) : $arrValues['run_on_master_db'] );
		if( isset( $arrValues['is_warning'] ) && $boolDirectSet ) $this->set( 'm_intIsWarning', trim( $arrValues['is_warning'] ) ); elseif( isset( $arrValues['is_warning'] ) ) $this->setIsWarning( $arrValues['is_warning'] );
		if( isset( $arrValues['is_php'] ) && $boolDirectSet ) $this->set( 'm_intIsPhp', trim( $arrValues['is_php'] ) ); elseif( isset( $arrValues['is_php'] ) ) $this->setIsPhp( $arrValues['is_php'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_migration'] ) && $boolDirectSet ) $this->set( 'm_boolIsMigration', trim( stripcslashes( $arrValues['is_migration'] ) ) ); elseif( isset( $arrValues['is_migration'] ) ) $this->setIsMigration( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_migration'] ) : $arrValues['is_migration'] );
		if( isset( $arrValues['is_audit'] ) && $boolDirectSet ) $this->set( 'm_boolIsAudit', trim( stripcslashes( $arrValues['is_audit'] ) ) ); elseif( isset( $arrValues['is_audit'] ) ) $this->setIsAudit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_audit'] ) : $arrValues['is_audit'] );
		if( isset( $arrValues['sdm_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSdmEmployeeId', trim( $arrValues['sdm_employee_id'] ) ); elseif( isset( $arrValues['sdm_employee_id'] ) ) $this->setSdmEmployeeId( $arrValues['sdm_employee_id'] );
		if( isset( $arrValues['frequency_details'] ) && $boolDirectSet ) $this->set( 'm_arrintFrequencyDetails', trim( $arrValues['frequency_details'] ) ); elseif( isset( $arrValues['frequency_details'] ) ) $this->setFrequencyDetails( $arrValues['frequency_details'] );
		if( isset( $arrValues['is_migration_repair'] ) && $boolDirectSet ) $this->set( 'm_boolIsMigrationRepair', trim( stripcslashes( $arrValues['is_migration_repair'] ) ) ); elseif( isset( $arrValues['is_migration_repair'] ) ) $this->setIsMigrationRepair( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_migration_repair'] ) : $arrValues['is_migration_repair'] );
		if( isset( $arrValues['stakeholders'] ) && $boolDirectSet ) $this->set( 'm_arrintStakeholders', trim( $arrValues['stakeholders'] ) ); elseif( isset( $arrValues['stakeholders'] ) ) $this->setStakeholders( $arrValues['stakeholders'] );
		if( isset( $arrValues['replica_lag_time'] ) && $boolDirectSet ) $this->set( 'm_intReplicaLagTime', trim( $arrValues['replica_lag_time'] ) ); elseif( isset( $arrValues['replica_lag_time'] ) ) $this->setReplicaLagTime( $arrValues['replica_lag_time'] );
		if( isset( $arrValues['field_name'] ) && $boolDirectSet ) $this->set( 'm_strFieldName', trim( $arrValues['field_name'] ) ); elseif( isset( $arrValues['field_name'] ) ) $this->setFieldName( $arrValues['field_name'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDiagnosticTypeId( $intDiagnosticTypeId ) {
		$this->set( 'm_intDiagnosticTypeId', CStrings::strToIntDef( $intDiagnosticTypeId, NULL, false ) );
	}

	public function getDiagnosticTypeId() {
		return $this->m_intDiagnosticTypeId;
	}

	public function sqlDiagnosticTypeId() {
		return ( true == isset( $this->m_intDiagnosticTypeId ) ) ? ( string ) $this->m_intDiagnosticTypeId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setTaskPriorityId( $intTaskPriorityId ) {
		$this->set( 'm_intTaskPriorityId', CStrings::strToIntDef( $intTaskPriorityId, NULL, false ) );
	}

	public function getTaskPriorityId() {
		return $this->m_intTaskPriorityId;
	}

	public function sqlTaskPriorityId() {
		return ( true == isset( $this->m_intTaskPriorityId ) ) ? ( string ) $this->m_intTaskPriorityId : '1';
	}

	public function setLastErrorTaskId( $intLastErrorTaskId ) {
		$this->set( 'm_intLastErrorTaskId', CStrings::strToIntDef( $intLastErrorTaskId, NULL, false ) );
	}

	public function getLastErrorTaskId() {
		return $this->m_intLastErrorTaskId;
	}

	public function sqlLastErrorTaskId() {
		return ( true == isset( $this->m_intLastErrorTaskId ) ) ? ( string ) $this->m_intLastErrorTaskId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->set( 'm_intPsProductOptionId', CStrings::strToIntDef( $intPsProductOptionId, NULL, false ) );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setDeveloperEmployeeId( $intDeveloperEmployeeId ) {
		$this->set( 'm_intDeveloperEmployeeId', CStrings::strToIntDef( $intDeveloperEmployeeId, NULL, false ) );
	}

	public function getDeveloperEmployeeId() {
		return $this->m_intDeveloperEmployeeId;
	}

	public function sqlDeveloperEmployeeId() {
		return ( true == isset( $this->m_intDeveloperEmployeeId ) ) ? ( string ) $this->m_intDeveloperEmployeeId : 'NULL';
	}

	public function setQaEmployeeId( $intQaEmployeeId ) {
		$this->set( 'm_intQaEmployeeId', CStrings::strToIntDef( $intQaEmployeeId, NULL, false ) );
	}

	public function getQaEmployeeId() {
		return $this->m_intQaEmployeeId;
	}

	public function sqlQaEmployeeId() {
		return ( true == isset( $this->m_intQaEmployeeId ) ) ? ( string ) $this->m_intQaEmployeeId : 'NULL';
	}

	public function setTableId( $intTableId ) {
		$this->set( 'm_intTableId', CStrings::strToIntDef( $intTableId, NULL, false ) );
	}

	public function getTableId() {
		return $this->m_intTableId;
	}

	public function sqlTableId() {
		return ( true == isset( $this->m_intTableId ) ) ? ( string ) $this->m_intTableId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setRapidViewSql( $strRapidViewSql ) {
		$this->set( 'm_strRapidViewSql', CStrings::strTrimDef( $strRapidViewSql, -1, NULL, true ) );
	}

	public function getRapidViewSql() {
		return $this->m_strRapidViewSql;
	}

	public function sqlRapidViewSql() {
		return ( true == isset( $this->m_strRapidViewSql ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRapidViewSql ) : '\'' . addslashes( $this->m_strRapidViewSql ) . '\'' ) : 'NULL';
	}

	public function setRapidRepairSql( $strRapidRepairSql ) {
		$this->set( 'm_strRapidRepairSql', CStrings::strTrimDef( $strRapidRepairSql, -1, NULL, true ) );
	}

	public function getRapidRepairSql() {
		return $this->m_strRapidRepairSql;
	}

	public function sqlRapidRepairSql() {
		return ( true == isset( $this->m_strRapidRepairSql ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRapidRepairSql ) : '\'' . addslashes( $this->m_strRapidRepairSql ) . '\'' ) : 'NULL';
	}

	public function setStandardViewSql( $strStandardViewSql ) {
		$this->set( 'm_strStandardViewSql', CStrings::strTrimDef( $strStandardViewSql, -1, NULL, true ) );
	}

	public function getStandardViewSql() {
		return $this->m_strStandardViewSql;
	}

	public function sqlStandardViewSql() {
		return ( true == isset( $this->m_strStandardViewSql ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStandardViewSql ) : '\'' . addslashes( $this->m_strStandardViewSql ) . '\'' ) : 'NULL';
	}

	public function setStandardRepairSql( $strStandardRepairSql ) {
		$this->set( 'm_strStandardRepairSql', CStrings::strTrimDef( $strStandardRepairSql, -1, NULL, true ) );
	}

	public function getStandardRepairSql() {
		return $this->m_strStandardRepairSql;
	}

	public function sqlStandardRepairSql() {
		return ( true == isset( $this->m_strStandardRepairSql ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStandardRepairSql ) : '\'' . addslashes( $this->m_strStandardRepairSql ) . '\'' ) : 'NULL';
	}

	public function setBeginDate( $strBeginDate ) {
		$this->set( 'm_strBeginDate', CStrings::strTrimDef( $strBeginDate, -1, NULL, true ) );
	}

	public function getBeginDate() {
		return $this->m_strBeginDate;
	}

	public function sqlBeginDate() {
		return ( true == isset( $this->m_strBeginDate ) ) ? '\'' . $this->m_strBeginDate . '\'' : 'NOW()';
	}

	public function setLastRepairedBy( $intLastRepairedBy ) {
		$this->set( 'm_intLastRepairedBy', CStrings::strToIntDef( $intLastRepairedBy, NULL, false ) );
	}

	public function getLastRepairedBy() {
		return $this->m_intLastRepairedBy;
	}

	public function sqlLastRepairedBy() {
		return ( true == isset( $this->m_intLastRepairedBy ) ) ? ( string ) $this->m_intLastRepairedBy : 'NULL';
	}

	public function setLastRepairedOn( $strLastRepairedOn ) {
		$this->set( 'm_strLastRepairedOn', CStrings::strTrimDef( $strLastRepairedOn, -1, NULL, true ) );
	}

	public function getLastRepairedOn() {
		return $this->m_strLastRepairedOn;
	}

	public function sqlLastRepairedOn() {
		return ( true == isset( $this->m_strLastRepairedOn ) ) ? '\'' . $this->m_strLastRepairedOn . '\'' : 'NULL';
	}

	public function setAlwaysShow( $intAlwaysShow ) {
		$this->set( 'm_intAlwaysShow', CStrings::strToIntDef( $intAlwaysShow, NULL, false ) );
	}

	public function getAlwaysShow() {
		return $this->m_intAlwaysShow;
	}

	public function sqlAlwaysShow() {
		return ( true == isset( $this->m_intAlwaysShow ) ) ? ( string ) $this->m_intAlwaysShow : '0';
	}

	public function setEnableAutoRepair( $intEnableAutoRepair ) {
		$this->set( 'm_intEnableAutoRepair', CStrings::strToIntDef( $intEnableAutoRepair, NULL, false ) );
	}

	public function getEnableAutoRepair() {
		return $this->m_intEnableAutoRepair;
	}

	public function sqlEnableAutoRepair() {
		return ( true == isset( $this->m_intEnableAutoRepair ) ) ? ( string ) $this->m_intEnableAutoRepair : '0';
	}

	public function setIsPropertySpecific( $boolIsPropertySpecific ) {
		$this->set( 'm_boolIsPropertySpecific', CStrings::strToBool( $boolIsPropertySpecific ) );
	}

	public function getIsPropertySpecific() {
		return $this->m_boolIsPropertySpecific;
	}

	public function sqlIsPropertySpecific() {
		return ( true == isset( $this->m_boolIsPropertySpecific ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPropertySpecific ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsClientSpecific( $intIsClientSpecific ) {
		$this->set( 'm_intIsClientSpecific', CStrings::strToIntDef( $intIsClientSpecific, NULL, false ) );
	}

	public function getIsClientSpecific() {
		return $this->m_intIsClientSpecific;
	}

	public function sqlIsClientSpecific() {
		return ( true == isset( $this->m_intIsClientSpecific ) ) ? ( string ) $this->m_intIsClientSpecific : '0';
	}

	public function setRunForTestDb( $boolRunForTestDb ) {
		$this->set( 'm_boolRunForTestDb', CStrings::strToBool( $boolRunForTestDb ) );
	}

	public function getRunForTestDb() {
		return $this->m_boolRunForTestDb;
	}

	public function sqlRunForTestDb() {
		return ( true == isset( $this->m_boolRunForTestDb ) ) ? '\'' . ( true == ( bool ) $this->m_boolRunForTestDb ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRunOnMasterDb( $boolRunOnMasterDb ) {
		$this->set( 'm_boolRunOnMasterDb', CStrings::strToBool( $boolRunOnMasterDb ) );
	}

	public function getRunOnMasterDb() {
		return $this->m_boolRunOnMasterDb;
	}

	public function sqlRunOnMasterDb() {
		return ( true == isset( $this->m_boolRunOnMasterDb ) ) ? '\'' . ( true == ( bool ) $this->m_boolRunOnMasterDb ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsWarning( $intIsWarning ) {
		$this->set( 'm_intIsWarning', CStrings::strToIntDef( $intIsWarning, NULL, false ) );
	}

	public function getIsWarning() {
		return $this->m_intIsWarning;
	}

	public function sqlIsWarning() {
		return ( true == isset( $this->m_intIsWarning ) ) ? ( string ) $this->m_intIsWarning : '1';
	}

	public function setIsPhp( $intIsPhp ) {
		$this->set( 'm_intIsPhp', CStrings::strToIntDef( $intIsPhp, NULL, false ) );
	}

	public function getIsPhp() {
		return $this->m_intIsPhp;
	}

	public function sqlIsPhp() {
		return ( true == isset( $this->m_intIsPhp ) ) ? ( string ) $this->m_intIsPhp : '0';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsMigration( $boolIsMigration ) {
		$this->set( 'm_boolIsMigration', CStrings::strToBool( $boolIsMigration ) );
	}

	public function getIsMigration() {
		return $this->m_boolIsMigration;
	}

	public function sqlIsMigration() {
		return ( true == isset( $this->m_boolIsMigration ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMigration ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAudit( $boolIsAudit ) {
		$this->set( 'm_boolIsAudit', CStrings::strToBool( $boolIsAudit ) );
	}

	public function getIsAudit() {
		return $this->m_boolIsAudit;
	}

	public function sqlIsAudit() {
		return ( true == isset( $this->m_boolIsAudit ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAudit ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSdmEmployeeId( $intSdmEmployeeId ) {
		$this->set( 'm_intSdmEmployeeId', CStrings::strToIntDef( $intSdmEmployeeId, NULL, false ) );
	}

	public function getSdmEmployeeId() {
		return $this->m_intSdmEmployeeId;
	}

	public function sqlSdmEmployeeId() {
		return ( true == isset( $this->m_intSdmEmployeeId ) ) ? ( string ) $this->m_intSdmEmployeeId : 'NULL';
	}

	public function setFrequencyDetails( $arrintFrequencyDetails ) {
		$this->set( 'm_arrintFrequencyDetails', CStrings::strToArrIntDef( $arrintFrequencyDetails, NULL ) );
	}

	public function getFrequencyDetails() {
		return $this->m_arrintFrequencyDetails;
	}

	public function sqlFrequencyDetails() {
		return ( true == isset( $this->m_arrintFrequencyDetails ) && true == valArr( $this->m_arrintFrequencyDetails ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintFrequencyDetails, NULL ) . '\'' : 'NULL';
	}

	public function setIsMigrationRepair( $boolIsMigrationRepair ) {
		$this->set( 'm_boolIsMigrationRepair', CStrings::strToBool( $boolIsMigrationRepair ) );
	}

	public function getIsMigrationRepair() {
		return $this->m_boolIsMigrationRepair;
	}

	public function sqlIsMigrationRepair() {
		return ( true == isset( $this->m_boolIsMigrationRepair ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMigrationRepair ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setStakeholders( $arrintStakeholders ) {
		$this->set( 'm_arrintStakeholders', CStrings::strToArrIntDef( $arrintStakeholders, NULL ) );
	}

	public function getStakeholders() {
		return $this->m_arrintStakeholders;
	}

	public function sqlStakeholders() {
		return ( true == isset( $this->m_arrintStakeholders ) && true == valArr( $this->m_arrintStakeholders ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintStakeholders, NULL ) . '\'' : 'NULL';
	}

	public function setReplicaLagTime( $intReplicaLagTime ) {
		$this->set( 'm_intReplicaLagTime', CStrings::strToIntDef( $intReplicaLagTime, NULL, false ) );
	}

	public function getReplicaLagTime() {
		return $this->m_intReplicaLagTime;
	}

	public function sqlReplicaLagTime() {
		return ( true == isset( $this->m_intReplicaLagTime ) ) ? ( string ) $this->m_intReplicaLagTime : '720';
	}

	public function setFieldName( $strFieldName ) {
		$this->set( 'm_strFieldName', CStrings::strTrimDef( $strFieldName, -1, NULL, true ) );
	}

	public function getFieldName() {
		return $this->m_strFieldName;
	}

	public function sqlFieldName() {
		return ( true == isset( $this->m_strFieldName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFieldName ) : '\'' . addslashes( $this->m_strFieldName ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, diagnostic_type_id, frequency_id, task_priority_id, last_error_task_id, ps_product_id, ps_product_option_id, developer_employee_id, qa_employee_id, table_id, name, description, rapid_view_sql, rapid_repair_sql, standard_view_sql, standard_repair_sql, begin_date, last_repaired_by, last_repaired_on, always_show, enable_auto_repair, is_property_specific, is_client_specific, run_for_test_db, run_on_master_db, is_warning, is_php, is_disabled, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, is_migration, is_audit, sdm_employee_id, frequency_details, is_migration_repair, stakeholders, replica_lag_time, field_name )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlDiagnosticTypeId() . ', ' .
		          $this->sqlFrequencyId() . ', ' .
		          $this->sqlTaskPriorityId() . ', ' .
		          $this->sqlLastErrorTaskId() . ', ' .
		          $this->sqlPsProductId() . ', ' .
		          $this->sqlPsProductOptionId() . ', ' .
		          $this->sqlDeveloperEmployeeId() . ', ' .
		          $this->sqlQaEmployeeId() . ', ' .
		          $this->sqlTableId() . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlDescription() . ', ' .
		          $this->sqlRapidViewSql() . ', ' .
		          $this->sqlRapidRepairSql() . ', ' .
		          $this->sqlStandardViewSql() . ', ' .
		          $this->sqlStandardRepairSql() . ', ' .
		          $this->sqlBeginDate() . ', ' .
		          $this->sqlLastRepairedBy() . ', ' .
		          $this->sqlLastRepairedOn() . ', ' .
		          $this->sqlAlwaysShow() . ', ' .
		          $this->sqlEnableAutoRepair() . ', ' .
		          $this->sqlIsPropertySpecific() . ', ' .
		          $this->sqlIsClientSpecific() . ', ' .
		          $this->sqlRunForTestDb() . ', ' .
		          $this->sqlRunOnMasterDb() . ', ' .
		          $this->sqlIsWarning() . ', ' .
		          $this->sqlIsPhp() . ', ' .
		          $this->sqlIsDisabled() . ', ' .
		          $this->sqlOrderNum() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlIsMigration() . ', ' .
		          $this->sqlIsAudit() . ', ' .
		          $this->sqlSdmEmployeeId() . ', ' .
		          $this->sqlFrequencyDetails() . ', ' .
		          $this->sqlIsMigrationRepair() . ', ' .
		          $this->sqlStakeholders() . ', ' .
		          $this->sqlReplicaLagTime() . ', ' .
		          $this->sqlFieldName() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' diagnostic_type_id = ' . $this->sqlDiagnosticTypeId(). ',' ; } elseif( true == array_key_exists( 'DiagnosticTypeId', $this->getChangedColumns() ) ) { $strSql .= ' diagnostic_type_id = ' . $this->sqlDiagnosticTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId(). ',' ; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_priority_id = ' . $this->sqlTaskPriorityId(). ',' ; } elseif( true == array_key_exists( 'TaskPriorityId', $this->getChangedColumns() ) ) { $strSql .= ' task_priority_id = ' . $this->sqlTaskPriorityId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_error_task_id = ' . $this->sqlLastErrorTaskId(). ',' ; } elseif( true == array_key_exists( 'LastErrorTaskId', $this->getChangedColumns() ) ) { $strSql .= ' last_error_task_id = ' . $this->sqlLastErrorTaskId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId(). ',' ; } elseif( true == array_key_exists( 'PsProductOptionId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' developer_employee_id = ' . $this->sqlDeveloperEmployeeId(). ',' ; } elseif( true == array_key_exists( 'DeveloperEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' developer_employee_id = ' . $this->sqlDeveloperEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId(). ',' ; } elseif( true == array_key_exists( 'QaEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' table_id = ' . $this->sqlTableId(). ',' ; } elseif( true == array_key_exists( 'TableId', $this->getChangedColumns() ) ) { $strSql .= ' table_id = ' . $this->sqlTableId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rapid_view_sql = ' . $this->sqlRapidViewSql(). ',' ; } elseif( true == array_key_exists( 'RapidViewSql', $this->getChangedColumns() ) ) { $strSql .= ' rapid_view_sql = ' . $this->sqlRapidViewSql() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rapid_repair_sql = ' . $this->sqlRapidRepairSql(). ',' ; } elseif( true == array_key_exists( 'RapidRepairSql', $this->getChangedColumns() ) ) { $strSql .= ' rapid_repair_sql = ' . $this->sqlRapidRepairSql() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' standard_view_sql = ' . $this->sqlStandardViewSql(). ',' ; } elseif( true == array_key_exists( 'StandardViewSql', $this->getChangedColumns() ) ) { $strSql .= ' standard_view_sql = ' . $this->sqlStandardViewSql() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' standard_repair_sql = ' . $this->sqlStandardRepairSql(). ',' ; } elseif( true == array_key_exists( 'StandardRepairSql', $this->getChangedColumns() ) ) { $strSql .= ' standard_repair_sql = ' . $this->sqlStandardRepairSql() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_date = ' . $this->sqlBeginDate(). ',' ; } elseif( true == array_key_exists( 'BeginDate', $this->getChangedColumns() ) ) { $strSql .= ' begin_date = ' . $this->sqlBeginDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_repaired_by = ' . $this->sqlLastRepairedBy(). ',' ; } elseif( true == array_key_exists( 'LastRepairedBy', $this->getChangedColumns() ) ) { $strSql .= ' last_repaired_by = ' . $this->sqlLastRepairedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_repaired_on = ' . $this->sqlLastRepairedOn(). ',' ; } elseif( true == array_key_exists( 'LastRepairedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_repaired_on = ' . $this->sqlLastRepairedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' always_show = ' . $this->sqlAlwaysShow(). ',' ; } elseif( true == array_key_exists( 'AlwaysShow', $this->getChangedColumns() ) ) { $strSql .= ' always_show = ' . $this->sqlAlwaysShow() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enable_auto_repair = ' . $this->sqlEnableAutoRepair(). ',' ; } elseif( true == array_key_exists( 'EnableAutoRepair', $this->getChangedColumns() ) ) { $strSql .= ' enable_auto_repair = ' . $this->sqlEnableAutoRepair() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_property_specific = ' . $this->sqlIsPropertySpecific(). ',' ; } elseif( true == array_key_exists( 'IsPropertySpecific', $this->getChangedColumns() ) ) { $strSql .= ' is_property_specific = ' . $this->sqlIsPropertySpecific() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_client_specific = ' . $this->sqlIsClientSpecific(). ',' ; } elseif( true == array_key_exists( 'IsClientSpecific', $this->getChangedColumns() ) ) { $strSql .= ' is_client_specific = ' . $this->sqlIsClientSpecific() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' run_for_test_db = ' . $this->sqlRunForTestDb(). ',' ; } elseif( true == array_key_exists( 'RunForTestDb', $this->getChangedColumns() ) ) { $strSql .= ' run_for_test_db = ' . $this->sqlRunForTestDb() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' run_on_master_db = ' . $this->sqlRunOnMasterDb(). ',' ; } elseif( true == array_key_exists( 'RunOnMasterDb', $this->getChangedColumns() ) ) { $strSql .= ' run_on_master_db = ' . $this->sqlRunOnMasterDb() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_warning = ' . $this->sqlIsWarning(). ',' ; } elseif( true == array_key_exists( 'IsWarning', $this->getChangedColumns() ) ) { $strSql .= ' is_warning = ' . $this->sqlIsWarning() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_php = ' . $this->sqlIsPhp(). ',' ; } elseif( true == array_key_exists( 'IsPhp', $this->getChangedColumns() ) ) { $strSql .= ' is_php = ' . $this->sqlIsPhp() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_migration = ' . $this->sqlIsMigration(). ',' ; } elseif( true == array_key_exists( 'IsMigration', $this->getChangedColumns() ) ) { $strSql .= ' is_migration = ' . $this->sqlIsMigration() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_audit = ' . $this->sqlIsAudit(). ',' ; } elseif( true == array_key_exists( 'IsAudit', $this->getChangedColumns() ) ) { $strSql .= ' is_audit = ' . $this->sqlIsAudit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sdm_employee_id = ' . $this->sqlSdmEmployeeId(). ',' ; } elseif( true == array_key_exists( 'SdmEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sdm_employee_id = ' . $this->sqlSdmEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_details = ' . $this->sqlFrequencyDetails(). ',' ; } elseif( true == array_key_exists( 'FrequencyDetails', $this->getChangedColumns() ) ) { $strSql .= ' frequency_details = ' . $this->sqlFrequencyDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_migration_repair = ' . $this->sqlIsMigrationRepair(). ',' ; } elseif( true == array_key_exists( 'IsMigrationRepair', $this->getChangedColumns() ) ) { $strSql .= ' is_migration_repair = ' . $this->sqlIsMigrationRepair() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' stakeholders = ' . $this->sqlStakeholders(). ',' ; } elseif( true == array_key_exists( 'Stakeholders', $this->getChangedColumns() ) ) { $strSql .= ' stakeholders = ' . $this->sqlStakeholders() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' replica_lag_time = ' . $this->sqlReplicaLagTime(). ',' ; } elseif( true == array_key_exists( 'ReplicaLagTime', $this->getChangedColumns() ) ) { $strSql .= ' replica_lag_time = ' . $this->sqlReplicaLagTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' field_name = ' . $this->sqlFieldName(). ',' ; } elseif( true == array_key_exists( 'FieldName', $this->getChangedColumns() ) ) { $strSql .= ' field_name = ' . $this->sqlFieldName() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'diagnostic_type_id' => $this->getDiagnosticTypeId(),
			'frequency_id' => $this->getFrequencyId(),
			'task_priority_id' => $this->getTaskPriorityId(),
			'last_error_task_id' => $this->getLastErrorTaskId(),
			'ps_product_id' => $this->getPsProductId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'developer_employee_id' => $this->getDeveloperEmployeeId(),
			'qa_employee_id' => $this->getQaEmployeeId(),
			'table_id' => $this->getTableId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'rapid_view_sql' => $this->getRapidViewSql(),
			'rapid_repair_sql' => $this->getRapidRepairSql(),
			'standard_view_sql' => $this->getStandardViewSql(),
			'standard_repair_sql' => $this->getStandardRepairSql(),
			'begin_date' => $this->getBeginDate(),
			'last_repaired_by' => $this->getLastRepairedBy(),
			'last_repaired_on' => $this->getLastRepairedOn(),
			'always_show' => $this->getAlwaysShow(),
			'enable_auto_repair' => $this->getEnableAutoRepair(),
			'is_property_specific' => $this->getIsPropertySpecific(),
			'is_client_specific' => $this->getIsClientSpecific(),
			'run_for_test_db' => $this->getRunForTestDb(),
			'run_on_master_db' => $this->getRunOnMasterDb(),
			'is_warning' => $this->getIsWarning(),
			'is_php' => $this->getIsPhp(),
			'is_disabled' => $this->getIsDisabled(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_migration' => $this->getIsMigration(),
			'is_audit' => $this->getIsAudit(),
			'sdm_employee_id' => $this->getSdmEmployeeId(),
			'frequency_details' => $this->getFrequencyDetails(),
			'is_migration_repair' => $this->getIsMigrationRepair(),
			'stakeholders' => $this->getStakeholders(),
			'replica_lag_time' => $this->getReplicaLagTime(),
			'field_name' => $this->getFieldName()
		);
	}

}
?>