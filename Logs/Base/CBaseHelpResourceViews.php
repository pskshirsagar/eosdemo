<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CHelpResourceViews
 * Do not add any new functions to this class.
 */

class CBaseHelpResourceViews extends CEosPluralBase {

	/**
	 * @return CHelpResourceView[]
	 */
	public static function fetchHelpResourceViews( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CHelpResourceView', $objDatabase );
	}

	/**
	 * @return CHelpResourceView
	 */
	public static function fetchHelpResourceView( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CHelpResourceView', $objDatabase );
	}

	public static function fetchHelpResourceViewCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'help_resource_views', $objDatabase );
	}

	public static function fetchHelpResourceViewById( $intId, $objDatabase ) {
		return self::fetchHelpResourceView( sprintf( 'SELECT * FROM help_resource_views WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchHelpResourceViewsByCid( $intCid, $objDatabase ) {
		return self::fetchHelpResourceViews( sprintf( 'SELECT * FROM help_resource_views WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchHelpResourceViewsByHelpResourceId( $intHelpResourceId, $objDatabase ) {
		return self::fetchHelpResourceViews( sprintf( 'SELECT * FROM help_resource_views WHERE help_resource_id = %d', ( int ) $intHelpResourceId ), $objDatabase );
	}

}
?>