<?php

class CBaseRestrictedIpRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.restricted_ip_requests';

	protected $m_intId;
	protected $m_intRestrictedIpId;
	protected $m_strRequestDatetime;
	protected $m_strUserAgentInfo;
	protected $m_strUrl;
	protected $m_strRequestData;
	protected $m_intIsReviewed;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['restricted_ip_id'] ) && $boolDirectSet ) $this->set( 'm_intRestrictedIpId', trim( $arrValues['restricted_ip_id'] ) ); elseif( isset( $arrValues['restricted_ip_id'] ) ) $this->setRestrictedIpId( $arrValues['restricted_ip_id'] );
		if( isset( $arrValues['request_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRequestDatetime', trim( $arrValues['request_datetime'] ) ); elseif( isset( $arrValues['request_datetime'] ) ) $this->setRequestDatetime( $arrValues['request_datetime'] );
		if( isset( $arrValues['user_agent_info'] ) && $boolDirectSet ) $this->set( 'm_strUserAgentInfo', trim( stripcslashes( $arrValues['user_agent_info'] ) ) ); elseif( isset( $arrValues['user_agent_info'] ) ) $this->setUserAgentInfo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['user_agent_info'] ) : $arrValues['user_agent_info'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( stripcslashes( $arrValues['url'] ) ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url'] ) : $arrValues['url'] );
		if( isset( $arrValues['request_data'] ) && $boolDirectSet ) $this->set( 'm_strRequestData', trim( stripcslashes( $arrValues['request_data'] ) ) ); elseif( isset( $arrValues['request_data'] ) ) $this->setRequestData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['request_data'] ) : $arrValues['request_data'] );
		if( isset( $arrValues['is_reviewed'] ) && $boolDirectSet ) $this->set( 'm_intIsReviewed', trim( $arrValues['is_reviewed'] ) ); elseif( isset( $arrValues['is_reviewed'] ) ) $this->setIsReviewed( $arrValues['is_reviewed'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRestrictedIpId( $intRestrictedIpId ) {
		$this->set( 'm_intRestrictedIpId', CStrings::strToIntDef( $intRestrictedIpId, NULL, false ) );
	}

	public function getRestrictedIpId() {
		return $this->m_intRestrictedIpId;
	}

	public function sqlRestrictedIpId() {
		return ( true == isset( $this->m_intRestrictedIpId ) ) ? ( string ) $this->m_intRestrictedIpId : 'NULL';
	}

	public function setRequestDatetime( $strRequestDatetime ) {
		$this->set( 'm_strRequestDatetime', CStrings::strTrimDef( $strRequestDatetime, -1, NULL, true ) );
	}

	public function getRequestDatetime() {
		return $this->m_strRequestDatetime;
	}

	public function sqlRequestDatetime() {
		return ( true == isset( $this->m_strRequestDatetime ) ) ? '\'' . $this->m_strRequestDatetime . '\'' : 'NOW()';
	}

	public function setUserAgentInfo( $strUserAgentInfo ) {
		$this->set( 'm_strUserAgentInfo', CStrings::strTrimDef( $strUserAgentInfo, 40, NULL, true ) );
	}

	public function getUserAgentInfo() {
		return $this->m_strUserAgentInfo;
	}

	public function sqlUserAgentInfo() {
		return ( true == isset( $this->m_strUserAgentInfo ) ) ? '\'' . addslashes( $this->m_strUserAgentInfo ) . '\'' : 'NULL';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, 120, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? '\'' . addslashes( $this->m_strUrl ) . '\'' : 'NULL';
	}

	public function setRequestData( $strRequestData ) {
		$this->set( 'm_strRequestData', CStrings::strTrimDef( $strRequestData, -1, NULL, true ) );
	}

	public function getRequestData() {
		return $this->m_strRequestData;
	}

	public function sqlRequestData() {
		return ( true == isset( $this->m_strRequestData ) ) ? '\'' . addslashes( $this->m_strRequestData ) . '\'' : 'NULL';
	}

	public function setIsReviewed( $intIsReviewed ) {
		$this->set( 'm_intIsReviewed', CStrings::strToIntDef( $intIsReviewed, NULL, false ) );
	}

	public function getIsReviewed() {
		return $this->m_intIsReviewed;
	}

	public function sqlIsReviewed() {
		return ( true == isset( $this->m_intIsReviewed ) ) ? ( string ) $this->m_intIsReviewed : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, restricted_ip_id, request_datetime, user_agent_info, url, request_data, is_reviewed, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlRestrictedIpId() . ', ' .
 						$this->sqlRequestDatetime() . ', ' .
 						$this->sqlUserAgentInfo() . ', ' .
 						$this->sqlUrl() . ', ' .
 						$this->sqlRequestData() . ', ' .
 						$this->sqlIsReviewed() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' restricted_ip_id = ' . $this->sqlRestrictedIpId() . ','; } elseif( true == array_key_exists( 'RestrictedIpId', $this->getChangedColumns() ) ) { $strSql .= ' restricted_ip_id = ' . $this->sqlRestrictedIpId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime() . ','; } elseif( true == array_key_exists( 'RequestDatetime', $this->getChangedColumns() ) ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_agent_info = ' . $this->sqlUserAgentInfo() . ','; } elseif( true == array_key_exists( 'UserAgentInfo', $this->getChangedColumns() ) ) { $strSql .= ' user_agent_info = ' . $this->sqlUserAgentInfo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; } elseif( true == array_key_exists( 'Url', $this->getChangedColumns() ) ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_data = ' . $this->sqlRequestData() . ','; } elseif( true == array_key_exists( 'RequestData', $this->getChangedColumns() ) ) { $strSql .= ' request_data = ' . $this->sqlRequestData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reviewed = ' . $this->sqlIsReviewed() . ','; } elseif( true == array_key_exists( 'IsReviewed', $this->getChangedColumns() ) ) { $strSql .= ' is_reviewed = ' . $this->sqlIsReviewed() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'restricted_ip_id' => $this->getRestrictedIpId(),
			'request_datetime' => $this->getRequestDatetime(),
			'user_agent_info' => $this->getUserAgentInfo(),
			'url' => $this->getUrl(),
			'request_data' => $this->getRequestData(),
			'is_reviewed' => $this->getIsReviewed(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>