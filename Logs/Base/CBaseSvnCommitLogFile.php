<?php

class CBaseSvnCommitLogFile extends CEosSingularBase {

	const TABLE_NAME = 'public.svn_commit_log_files';

	protected $m_intId;
	protected $m_intSvnCommitLogId;
	protected $m_strFilePath;
	protected $m_strChangeDescription;
	protected $m_boolIsCriticalCommit;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsCriticalCommit = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['svn_commit_log_id'] ) && $boolDirectSet ) $this->set( 'm_intSvnCommitLogId', trim( $arrValues['svn_commit_log_id'] ) ); elseif( isset( $arrValues['svn_commit_log_id'] ) ) $this->setSvnCommitLogId( $arrValues['svn_commit_log_id'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['change_description'] ) && $boolDirectSet ) $this->set( 'm_strChangeDescription', trim( stripcslashes( $arrValues['change_description'] ) ) ); elseif( isset( $arrValues['change_description'] ) ) $this->setChangeDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['change_description'] ) : $arrValues['change_description'] );
		if( isset( $arrValues['is_critical_commit'] ) && $boolDirectSet ) $this->set( 'm_boolIsCriticalCommit', trim( stripcslashes( $arrValues['is_critical_commit'] ) ) ); elseif( isset( $arrValues['is_critical_commit'] ) ) $this->setIsCriticalCommit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_critical_commit'] ) : $arrValues['is_critical_commit'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSvnCommitLogId( $intSvnCommitLogId ) {
		$this->set( 'm_intSvnCommitLogId', CStrings::strToIntDef( $intSvnCommitLogId, NULL, false ) );
	}

	public function getSvnCommitLogId() {
		return $this->m_intSvnCommitLogId;
	}

	public function sqlSvnCommitLogId() {
		return ( true == isset( $this->m_intSvnCommitLogId ) ) ? ( string ) $this->m_intSvnCommitLogId : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, -1, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setChangeDescription( $strChangeDescription ) {
		$this->set( 'm_strChangeDescription', CStrings::strTrimDef( $strChangeDescription, -1, NULL, true ) );
	}

	public function getChangeDescription() {
		return $this->m_strChangeDescription;
	}

	public function sqlChangeDescription() {
		return ( true == isset( $this->m_strChangeDescription ) ) ? '\'' . addslashes( $this->m_strChangeDescription ) . '\'' : 'NULL';
	}

	public function setIsCriticalCommit( $boolIsCriticalCommit ) {
		$this->set( 'm_boolIsCriticalCommit', CStrings::strToBool( $boolIsCriticalCommit ) );
	}

	public function getIsCriticalCommit() {
		return $this->m_boolIsCriticalCommit;
	}

	public function sqlIsCriticalCommit() {
		return ( true == isset( $this->m_boolIsCriticalCommit ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCriticalCommit ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, svn_commit_log_id, file_path, change_description, is_critical_commit, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSvnCommitLogId() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlChangeDescription() . ', ' .
 						$this->sqlIsCriticalCommit() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_commit_log_id = ' . $this->sqlSvnCommitLogId() . ','; } elseif( true == array_key_exists( 'SvnCommitLogId', $this->getChangedColumns() ) ) { $strSql .= ' svn_commit_log_id = ' . $this->sqlSvnCommitLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' change_description = ' . $this->sqlChangeDescription() . ','; } elseif( true == array_key_exists( 'ChangeDescription', $this->getChangedColumns() ) ) { $strSql .= ' change_description = ' . $this->sqlChangeDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_critical_commit = ' . $this->sqlIsCriticalCommit() . ','; } elseif( true == array_key_exists( 'IsCriticalCommit', $this->getChangedColumns() ) ) { $strSql .= ' is_critical_commit = ' . $this->sqlIsCriticalCommit() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'svn_commit_log_id' => $this->getSvnCommitLogId(),
			'file_path' => $this->getFilePath(),
			'change_description' => $this->getChangeDescription(),
			'is_critical_commit' => $this->getIsCriticalCommit(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>