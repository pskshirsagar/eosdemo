<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CModuleAccessLogs
 * Do not add any new functions to this class.
 */

class CBaseModuleAccessLogs extends CEosPluralBase {

	/**
	 * @return CModuleAccessLog[]
	 */
	public static function fetchModuleAccessLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CModuleAccessLog', $objDatabase );
	}

	/**
	 * @return CModuleAccessLog
	 */
	public static function fetchModuleAccessLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CModuleAccessLog', $objDatabase );
	}

	public static function fetchModuleAccessLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'module_access_logs', $objDatabase );
	}

	public static function fetchModuleAccessLogById( $intId, $objDatabase ) {
		return self::fetchModuleAccessLog( sprintf( 'SELECT * FROM module_access_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchModuleAccessLogsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchModuleAccessLogs( sprintf( 'SELECT * FROM module_access_logs WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

}
?>