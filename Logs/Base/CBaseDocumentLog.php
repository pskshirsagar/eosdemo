<?php

class CBaseDocumentLog extends CEosSingularBase {

	const TABLE_NAME = 'public.document_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyUserId;
	protected $m_intFileId;
	protected $m_intAuditlogId;
	protected $m_intAuditTypeId;
	protected $m_strAuditTypeName;
	protected $m_strAuditFilePath;
	protected $m_strIpAddress;
	protected $m_strContentSha1;
	protected $m_strMetaData;
	protected $m_intIsSuccess;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsSuccess = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['file_id'] ) && $boolDirectSet ) $this->set( 'm_intFileId', trim( $arrValues['file_id'] ) ); elseif( isset( $arrValues['file_id'] ) ) $this->setFileId( $arrValues['file_id'] );
		if( isset( $arrValues['auditlog_id'] ) && $boolDirectSet ) $this->set( 'm_intAuditlogId', trim( $arrValues['auditlog_id'] ) ); elseif( isset( $arrValues['auditlog_id'] ) ) $this->setAuditlogId( $arrValues['auditlog_id'] );
		if( isset( $arrValues['audit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAuditTypeId', trim( $arrValues['audit_type_id'] ) ); elseif( isset( $arrValues['audit_type_id'] ) ) $this->setAuditTypeId( $arrValues['audit_type_id'] );
		if( isset( $arrValues['audit_type_name'] ) && $boolDirectSet ) $this->set( 'm_strAuditTypeName', trim( stripcslashes( $arrValues['audit_type_name'] ) ) ); elseif( isset( $arrValues['audit_type_name'] ) ) $this->setAuditTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['audit_type_name'] ) : $arrValues['audit_type_name'] );
		if( isset( $arrValues['audit_file_path'] ) && $boolDirectSet ) $this->set( 'm_strAuditFilePath', trim( stripcslashes( $arrValues['audit_file_path'] ) ) ); elseif( isset( $arrValues['audit_file_path'] ) ) $this->setAuditFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['audit_file_path'] ) : $arrValues['audit_file_path'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( stripcslashes( $arrValues['ip_address'] ) ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ip_address'] ) : $arrValues['ip_address'] );
		if( isset( $arrValues['content_sha1'] ) && $boolDirectSet ) $this->set( 'm_strContentSha1', trim( stripcslashes( $arrValues['content_sha1'] ) ) ); elseif( isset( $arrValues['content_sha1'] ) ) $this->setContentSha1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['content_sha1'] ) : $arrValues['content_sha1'] );
		if( isset( $arrValues['meta_data'] ) && $boolDirectSet ) $this->set( 'm_strMetaData', trim( stripcslashes( $arrValues['meta_data'] ) ) ); elseif( isset( $arrValues['meta_data'] ) ) $this->setMetaData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['meta_data'] ) : $arrValues['meta_data'] );
		if( isset( $arrValues['is_success'] ) && $boolDirectSet ) $this->set( 'm_intIsSuccess', trim( $arrValues['is_success'] ) ); elseif( isset( $arrValues['is_success'] ) ) $this->setIsSuccess( $arrValues['is_success'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setFileId( $intFileId ) {
		$this->set( 'm_intFileId', CStrings::strToIntDef( $intFileId, NULL, false ) );
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function sqlFileId() {
		return ( true == isset( $this->m_intFileId ) ) ? ( string ) $this->m_intFileId : 'NULL';
	}

	public function setAuditlogId( $intAuditlogId ) {
		$this->set( 'm_intAuditlogId', CStrings::strToIntDef( $intAuditlogId, NULL, false ) );
	}

	public function getAuditlogId() {
		return $this->m_intAuditlogId;
	}

	public function sqlAuditlogId() {
		return ( true == isset( $this->m_intAuditlogId ) ) ? ( string ) $this->m_intAuditlogId : 'NULL';
	}

	public function setAuditTypeId( $intAuditTypeId ) {
		$this->set( 'm_intAuditTypeId', CStrings::strToIntDef( $intAuditTypeId, NULL, false ) );
	}

	public function getAuditTypeId() {
		return $this->m_intAuditTypeId;
	}

	public function sqlAuditTypeId() {
		return ( true == isset( $this->m_intAuditTypeId ) ) ? ( string ) $this->m_intAuditTypeId : 'NULL';
	}

	public function setAuditTypeName( $strAuditTypeName ) {
		$this->set( 'm_strAuditTypeName', CStrings::strTrimDef( $strAuditTypeName, 250, NULL, true ) );
	}

	public function getAuditTypeName() {
		return $this->m_strAuditTypeName;
	}

	public function sqlAuditTypeName() {
		return ( true == isset( $this->m_strAuditTypeName ) ) ? '\'' . addslashes( $this->m_strAuditTypeName ) . '\'' : 'NULL';
	}

	public function setAuditFilePath( $strAuditFilePath ) {
		$this->set( 'm_strAuditFilePath', CStrings::strTrimDef( $strAuditFilePath, 4096, NULL, true ) );
	}

	public function getAuditFilePath() {
		return $this->m_strAuditFilePath;
	}

	public function sqlAuditFilePath() {
		return ( true == isset( $this->m_strAuditFilePath ) ) ? '\'' . addslashes( $this->m_strAuditFilePath ) . '\'' : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 100, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? '\'' . addslashes( $this->m_strIpAddress ) . '\'' : 'NULL';
	}

	public function setContentSha1( $strContentSha1 ) {
		$this->set( 'm_strContentSha1', CStrings::strTrimDef( $strContentSha1, 100, NULL, true ) );
	}

	public function getContentSha1() {
		return $this->m_strContentSha1;
	}

	public function sqlContentSha1() {
		return ( true == isset( $this->m_strContentSha1 ) ) ? '\'' . addslashes( $this->m_strContentSha1 ) . '\'' : 'NULL';
	}

	public function setMetaData( $strMetaData ) {
		$this->set( 'm_strMetaData', CStrings::strTrimDef( $strMetaData, -1, NULL, true ) );
	}

	public function getMetaData() {
		return $this->m_strMetaData;
	}

	public function sqlMetaData() {
		return ( true == isset( $this->m_strMetaData ) ) ? '\'' . addslashes( $this->m_strMetaData ) . '\'' : 'NULL';
	}

	public function setIsSuccess( $intIsSuccess ) {
		$this->set( 'm_intIsSuccess', CStrings::strToIntDef( $intIsSuccess, NULL, false ) );
	}

	public function getIsSuccess() {
		return $this->m_intIsSuccess;
	}

	public function sqlIsSuccess() {
		return ( true == isset( $this->m_intIsSuccess ) ) ? ( string ) $this->m_intIsSuccess : '0';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_user_id, file_id, auditlog_id, audit_type_id, audit_type_name, audit_file_path, ip_address, content_sha1, meta_data, is_success, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCompanyUserId() . ', ' .
 						$this->sqlFileId() . ', ' .
 						$this->sqlAuditlogId() . ', ' .
 						$this->sqlAuditTypeId() . ', ' .
 						$this->sqlAuditTypeName() . ', ' .
 						$this->sqlAuditFilePath() . ', ' .
 						$this->sqlIpAddress() . ', ' .
 						$this->sqlContentSha1() . ', ' .
 						$this->sqlMetaData() . ', ' .
 						$this->sqlIsSuccess() . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; } elseif( true == array_key_exists( 'FileId', $this->getChangedColumns() ) ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auditlog_id = ' . $this->sqlAuditlogId() . ','; } elseif( true == array_key_exists( 'AuditlogId', $this->getChangedColumns() ) ) { $strSql .= ' auditlog_id = ' . $this->sqlAuditlogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_type_id = ' . $this->sqlAuditTypeId() . ','; } elseif( true == array_key_exists( 'AuditTypeId', $this->getChangedColumns() ) ) { $strSql .= ' audit_type_id = ' . $this->sqlAuditTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_type_name = ' . $this->sqlAuditTypeName() . ','; } elseif( true == array_key_exists( 'AuditTypeName', $this->getChangedColumns() ) ) { $strSql .= ' audit_type_name = ' . $this->sqlAuditTypeName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_file_path = ' . $this->sqlAuditFilePath() . ','; } elseif( true == array_key_exists( 'AuditFilePath', $this->getChangedColumns() ) ) { $strSql .= ' audit_file_path = ' . $this->sqlAuditFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content_sha1 = ' . $this->sqlContentSha1() . ','; } elseif( true == array_key_exists( 'ContentSha1', $this->getChangedColumns() ) ) { $strSql .= ' content_sha1 = ' . $this->sqlContentSha1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meta_data = ' . $this->sqlMetaData() . ','; } elseif( true == array_key_exists( 'MetaData', $this->getChangedColumns() ) ) { $strSql .= ' meta_data = ' . $this->sqlMetaData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_success = ' . $this->sqlIsSuccess() . ','; } elseif( true == array_key_exists( 'IsSuccess', $this->getChangedColumns() ) ) { $strSql .= ' is_success = ' . $this->sqlIsSuccess() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_user_id' => $this->getCompanyUserId(),
			'file_id' => $this->getFileId(),
			'auditlog_id' => $this->getAuditlogId(),
			'audit_type_id' => $this->getAuditTypeId(),
			'audit_type_name' => $this->getAuditTypeName(),
			'audit_file_path' => $this->getAuditFilePath(),
			'ip_address' => $this->getIpAddress(),
			'content_sha1' => $this->getContentSha1(),
			'meta_data' => $this->getMetaData(),
			'is_success' => $this->getIsSuccess(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>