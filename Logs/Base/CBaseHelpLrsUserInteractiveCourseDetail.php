<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseHelpLrsUserInteractiveCourseDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.help_lrs_user_interactive_course_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intUserId;
	protected $m_intHelpResourceId;
	protected $m_intCompanyUserAssessmentId;
	protected $m_intCourseProgress;
	protected $m_intCourseAssignedCount;
	protected $m_intCourseAttemptCount;
	protected $m_fltCourseScore;
	protected $m_strUsername;
	protected $m_strEmailAddress;
	protected $m_strCourseDueDate;
	protected $m_strCourseAssignedOn;
	protected $m_strAttemptOn;
	protected $m_strHighestScoreOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strInitialPassedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCourseAssignedCount = '1';
		$this->m_intCourseAttemptCount = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['help_resource_id'] ) && $boolDirectSet ) $this->set( 'm_intHelpResourceId', trim( $arrValues['help_resource_id'] ) ); elseif( isset( $arrValues['help_resource_id'] ) ) $this->setHelpResourceId( $arrValues['help_resource_id'] );
		if( isset( $arrValues['company_user_assessment_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserAssessmentId', trim( $arrValues['company_user_assessment_id'] ) ); elseif( isset( $arrValues['company_user_assessment_id'] ) ) $this->setCompanyUserAssessmentId( $arrValues['company_user_assessment_id'] );
		if( isset( $arrValues['course_progress'] ) && $boolDirectSet ) $this->set( 'm_intCourseProgress', trim( $arrValues['course_progress'] ) ); elseif( isset( $arrValues['course_progress'] ) ) $this->setCourseProgress( $arrValues['course_progress'] );
		if( isset( $arrValues['course_assigned_count'] ) && $boolDirectSet ) $this->set( 'm_intCourseAssignedCount', trim( $arrValues['course_assigned_count'] ) ); elseif( isset( $arrValues['course_assigned_count'] ) ) $this->setCourseAssignedCount( $arrValues['course_assigned_count'] );
		if( isset( $arrValues['course_attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intCourseAttemptCount', trim( $arrValues['course_attempt_count'] ) ); elseif( isset( $arrValues['course_attempt_count'] ) ) $this->setCourseAttemptCount( $arrValues['course_attempt_count'] );
		if( isset( $arrValues['course_score'] ) && $boolDirectSet ) $this->set( 'm_fltCourseScore', trim( $arrValues['course_score'] ) ); elseif( isset( $arrValues['course_score'] ) ) $this->setCourseScore( $arrValues['course_score'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( stripcslashes( $arrValues['username'] ) ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username'] ) : $arrValues['username'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['course_due_date'] ) && $boolDirectSet ) $this->set( 'm_strCourseDueDate', trim( $arrValues['course_due_date'] ) ); elseif( isset( $arrValues['course_due_date'] ) ) $this->setCourseDueDate( $arrValues['course_due_date'] );
		if( isset( $arrValues['course_assigned_on'] ) && $boolDirectSet ) $this->set( 'm_strCourseAssignedOn', trim( $arrValues['course_assigned_on'] ) ); elseif( isset( $arrValues['course_assigned_on'] ) ) $this->setCourseAssignedOn( $arrValues['course_assigned_on'] );
		if( isset( $arrValues['attempt_on'] ) && $boolDirectSet ) $this->set( 'm_strAttemptOn', trim( $arrValues['attempt_on'] ) ); elseif( isset( $arrValues['attempt_on'] ) ) $this->setAttemptOn( $arrValues['attempt_on'] );
		if( isset( $arrValues['highest_score_on'] ) && $boolDirectSet ) $this->set( 'm_strHighestScoreOn', trim( $arrValues['highest_score_on'] ) ); elseif( isset( $arrValues['highest_score_on'] ) ) $this->setHighestScoreOn( $arrValues['highest_score_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['initial_passed_on'] ) && $boolDirectSet ) $this->set( 'm_strInitialPassedOn', trim( $arrValues['initial_passed_on'] ) ); elseif( isset( $arrValues['initial_passed_on'] ) ) $this->setInitialPassedOn( $arrValues['initial_passed_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setHelpResourceId( $intHelpResourceId ) {
		$this->set( 'm_intHelpResourceId', CStrings::strToIntDef( $intHelpResourceId, NULL, false ) );
	}

	public function getHelpResourceId() {
		return $this->m_intHelpResourceId;
	}

	public function sqlHelpResourceId() {
		return ( true == isset( $this->m_intHelpResourceId ) ) ? ( string ) $this->m_intHelpResourceId : 'NULL';
	}

	public function setCompanyUserAssessmentId( $intCompanyUserAssessmentId ) {
		$this->set( 'm_intCompanyUserAssessmentId', CStrings::strToIntDef( $intCompanyUserAssessmentId, NULL, false ) );
	}

	public function getCompanyUserAssessmentId() {
		return $this->m_intCompanyUserAssessmentId;
	}

	public function sqlCompanyUserAssessmentId() {
		return ( true == isset( $this->m_intCompanyUserAssessmentId ) ) ? ( string ) $this->m_intCompanyUserAssessmentId : 'NULL';
	}

	public function setCourseProgress( $intCourseProgress ) {
		$this->set( 'm_intCourseProgress', CStrings::strToIntDef( $intCourseProgress, NULL, false ) );
	}

	public function getCourseProgress() {
		return $this->m_intCourseProgress;
	}

	public function sqlCourseProgress() {
		return ( true == isset( $this->m_intCourseProgress ) ) ? ( string ) $this->m_intCourseProgress : 'NULL';
	}

	public function setCourseAssignedCount( $intCourseAssignedCount ) {
		$this->set( 'm_intCourseAssignedCount', CStrings::strToIntDef( $intCourseAssignedCount, NULL, false ) );
	}

	public function getCourseAssignedCount() {
		return $this->m_intCourseAssignedCount;
	}

	public function sqlCourseAssignedCount() {
		return ( true == isset( $this->m_intCourseAssignedCount ) ) ? ( string ) $this->m_intCourseAssignedCount : '1';
	}

	public function setCourseAttemptCount( $intCourseAttemptCount ) {
		$this->set( 'm_intCourseAttemptCount', CStrings::strToIntDef( $intCourseAttemptCount, NULL, false ) );
	}

	public function getCourseAttemptCount() {
		return $this->m_intCourseAttemptCount;
	}

	public function sqlCourseAttemptCount() {
		return ( true == isset( $this->m_intCourseAttemptCount ) ) ? ( string ) $this->m_intCourseAttemptCount : '1';
	}

	public function setCourseScore( $fltCourseScore ) {
		$this->set( 'm_fltCourseScore', CStrings::strToFloatDef( $fltCourseScore, NULL, false, 0 ) );
	}

	public function getCourseScore() {
		return $this->m_fltCourseScore;
	}

	public function sqlCourseScore() {
		return ( true == isset( $this->m_fltCourseScore ) ) ? ( string ) $this->m_fltCourseScore : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 240, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? '\'' . addslashes( $this->m_strUsername ) . '\'' : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setCourseDueDate( $strCourseDueDate ) {
		$this->set( 'm_strCourseDueDate', CStrings::strTrimDef( $strCourseDueDate, -1, NULL, true ) );
	}

	public function getCourseDueDate() {
		return $this->m_strCourseDueDate;
	}

	public function sqlCourseDueDate() {
		return ( true == isset( $this->m_strCourseDueDate ) ) ? '\'' . $this->m_strCourseDueDate . '\'' : 'NULL';
	}

	public function setCourseAssignedOn( $strCourseAssignedOn ) {
		$this->set( 'm_strCourseAssignedOn', CStrings::strTrimDef( $strCourseAssignedOn, -1, NULL, true ) );
	}

	public function getCourseAssignedOn() {
		return $this->m_strCourseAssignedOn;
	}

	public function sqlCourseAssignedOn() {
		return ( true == isset( $this->m_strCourseAssignedOn ) ) ? '\'' . $this->m_strCourseAssignedOn . '\'' : 'NOW()';
	}

	public function setAttemptOn( $strAttemptOn ) {
		$this->set( 'm_strAttemptOn', CStrings::strTrimDef( $strAttemptOn, -1, NULL, true ) );
	}

	public function getAttemptOn() {
		return $this->m_strAttemptOn;
	}

	public function sqlAttemptOn() {
		return ( true == isset( $this->m_strAttemptOn ) ) ? '\'' . $this->m_strAttemptOn . '\'' : 'NOW()';
	}

	public function setHighestScoreOn( $strHighestScoreOn ) {
		$this->set( 'm_strHighestScoreOn', CStrings::strTrimDef( $strHighestScoreOn, -1, NULL, true ) );
	}

	public function getHighestScoreOn() {
		return $this->m_strHighestScoreOn;
	}

	public function sqlHighestScoreOn() {
		return ( true == isset( $this->m_strHighestScoreOn ) ) ? '\'' . $this->m_strHighestScoreOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setInitialPassedOn( $strInitialPassedOn ) {
		$this->set( 'm_strInitialPassedOn', CStrings::strTrimDef( $strInitialPassedOn, -1, NULL, true ) );
	}

	public function getInitialPassedOn() {
		return $this->m_strInitialPassedOn;
	}

	public function sqlInitialPassedOn() {
		return ( true == isset( $this->m_strInitialPassedOn ) ) ? '\'' . $this->m_strInitialPassedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, user_id, help_resource_id, company_user_assessment_id, course_progress, course_assigned_count, course_attempt_count, course_score, username, email_address, course_due_date, course_assigned_on, attempt_on, highest_score_on, updated_by, updated_on, created_by, created_on, initial_passed_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlUserId() . ', ' .
 						$this->sqlHelpResourceId() . ', ' .
 						$this->sqlCompanyUserAssessmentId() . ', ' .
 						$this->sqlCourseProgress() . ', ' .
 						$this->sqlCourseAssignedCount() . ', ' .
 						$this->sqlCourseAttemptCount() . ', ' .
 						$this->sqlCourseScore() . ', ' .
 						$this->sqlUsername() . ', ' .
 						$this->sqlEmailAddress() . ', ' .
 						$this->sqlCourseDueDate() . ', ' .
 						$this->sqlCourseAssignedOn() . ', ' .
 						$this->sqlAttemptOn() . ', ' .
 						$this->sqlHighestScoreOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlInitialPassedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' help_resource_id = ' . $this->sqlHelpResourceId() . ','; } elseif( true == array_key_exists( 'HelpResourceId', $this->getChangedColumns() ) ) { $strSql .= ' help_resource_id = ' . $this->sqlHelpResourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_assessment_id = ' . $this->sqlCompanyUserAssessmentId() . ','; } elseif( true == array_key_exists( 'CompanyUserAssessmentId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_assessment_id = ' . $this->sqlCompanyUserAssessmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' course_progress = ' . $this->sqlCourseProgress() . ','; } elseif( true == array_key_exists( 'CourseProgress', $this->getChangedColumns() ) ) { $strSql .= ' course_progress = ' . $this->sqlCourseProgress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' course_assigned_count = ' . $this->sqlCourseAssignedCount() . ','; } elseif( true == array_key_exists( 'CourseAssignedCount', $this->getChangedColumns() ) ) { $strSql .= ' course_assigned_count = ' . $this->sqlCourseAssignedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' course_attempt_count = ' . $this->sqlCourseAttemptCount() . ','; } elseif( true == array_key_exists( 'CourseAttemptCount', $this->getChangedColumns() ) ) { $strSql .= ' course_attempt_count = ' . $this->sqlCourseAttemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' course_score = ' . $this->sqlCourseScore() . ','; } elseif( true == array_key_exists( 'CourseScore', $this->getChangedColumns() ) ) { $strSql .= ' course_score = ' . $this->sqlCourseScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' course_due_date = ' . $this->sqlCourseDueDate() . ','; } elseif( true == array_key_exists( 'CourseDueDate', $this->getChangedColumns() ) ) { $strSql .= ' course_due_date = ' . $this->sqlCourseDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' course_assigned_on = ' . $this->sqlCourseAssignedOn() . ','; } elseif( true == array_key_exists( 'CourseAssignedOn', $this->getChangedColumns() ) ) { $strSql .= ' course_assigned_on = ' . $this->sqlCourseAssignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attempt_on = ' . $this->sqlAttemptOn() . ','; } elseif( true == array_key_exists( 'AttemptOn', $this->getChangedColumns() ) ) { $strSql .= ' attempt_on = ' . $this->sqlAttemptOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' highest_score_on = ' . $this->sqlHighestScoreOn() . ','; } elseif( true == array_key_exists( 'HighestScoreOn', $this->getChangedColumns() ) ) { $strSql .= ' highest_score_on = ' . $this->sqlHighestScoreOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initial_passed_on = ' . $this->sqlInitialPassedOn() . ','; } elseif( true == array_key_exists( 'InitialPassedOn', $this->getChangedColumns() ) ) { $strSql .= ' initial_passed_on = ' . $this->sqlInitialPassedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'user_id' => $this->getUserId(),
			'help_resource_id' => $this->getHelpResourceId(),
			'company_user_assessment_id' => $this->getCompanyUserAssessmentId(),
			'course_progress' => $this->getCourseProgress(),
			'course_assigned_count' => $this->getCourseAssignedCount(),
			'course_attempt_count' => $this->getCourseAttemptCount(),
			'course_score' => $this->getCourseScore(),
			'username' => $this->getUsername(),
			'email_address' => $this->getEmailAddress(),
			'course_due_date' => $this->getCourseDueDate(),
			'course_assigned_on' => $this->getCourseAssignedOn(),
			'attempt_on' => $this->getAttemptOn(),
			'highest_score_on' => $this->getHighestScoreOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'initial_passed_on' => $this->getInitialPassedOn()
		);
	}

}
?>