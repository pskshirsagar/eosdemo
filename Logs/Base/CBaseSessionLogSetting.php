<?php

class CBaseSessionLogSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.session_log_settings';

	protected $m_intId;
	protected $m_intPsProductId;
	protected $m_intEnableSessionLogging;

	public function __construct() {
		parent::__construct();

		$this->m_intEnableSessionLogging = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['enable_session_logging'] ) && $boolDirectSet ) $this->set( 'm_intEnableSessionLogging', trim( $arrValues['enable_session_logging'] ) ); elseif( isset( $arrValues['enable_session_logging'] ) ) $this->setEnableSessionLogging( $arrValues['enable_session_logging'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setEnableSessionLogging( $intEnableSessionLogging ) {
		$this->set( 'm_intEnableSessionLogging', CStrings::strToIntDef( $intEnableSessionLogging, NULL, false ) );
	}

	public function getEnableSessionLogging() {
		return $this->m_intEnableSessionLogging;
	}

	public function sqlEnableSessionLogging() {
		return ( true == isset( $this->m_intEnableSessionLogging ) ) ? ( string ) $this->m_intEnableSessionLogging : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_product_id' => $this->getPsProductId(),
			'enable_session_logging' => $this->getEnableSessionLogging()
		);
	}

}
?>