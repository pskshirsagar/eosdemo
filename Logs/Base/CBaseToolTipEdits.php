<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CToolTipEdits
 * Do not add any new functions to this class.
 */

class CBaseToolTipEdits extends CEosPluralBase {

	/**
	 * @return CToolTipEdit[]
	 */
	public static function fetchToolTipEdits( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CToolTipEdit', $objDatabase );
	}

	/**
	 * @return CToolTipEdit
	 */
	public static function fetchToolTipEdit( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CToolTipEdit', $objDatabase );
	}

	public static function fetchToolTipEditCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tool_tip_edits', $objDatabase );
	}

	public static function fetchToolTipEditById( $intId, $objDatabase ) {
		return self::fetchToolTipEdit( sprintf( 'SELECT * FROM tool_tip_edits WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchToolTipEditsByToolTipId( $intToolTipId, $objDatabase ) {
		return self::fetchToolTipEdits( sprintf( 'SELECT * FROM tool_tip_edits WHERE tool_tip_id = %d', ( int ) $intToolTipId ), $objDatabase );
	}

}
?>