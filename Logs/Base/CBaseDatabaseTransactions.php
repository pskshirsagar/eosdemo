<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CDatabaseTransactions
 * Do not add any new functions to this class.
 */

class CBaseDatabaseTransactions extends CEosPluralBase {

	/**
	 * @return CDatabaseTransaction[]
	 */
	public static function fetchDatabaseTransactions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDatabaseTransaction', $objDatabase );
	}

	/**
	 * @return CDatabaseTransaction
	 */
	public static function fetchDatabaseTransaction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDatabaseTransaction', $objDatabase );
	}

	public static function fetchDatabaseTransactionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'database_transactions', $objDatabase );
	}

	public static function fetchDatabaseTransactionById( $intId, $objDatabase ) {
		return self::fetchDatabaseTransaction( sprintf( 'SELECT * FROM database_transactions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDatabaseTransactionsByClusterId( $intClusterId, $objDatabase ) {
		return self::fetchDatabaseTransactions( sprintf( 'SELECT * FROM database_transactions WHERE cluster_id = %d', ( int ) $intClusterId ), $objDatabase );
	}

	public static function fetchDatabaseTransactionsByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchDatabaseTransactions( sprintf( 'SELECT * FROM database_transactions WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

	public static function fetchDatabaseTransactionsByDatabaseTransactionTypeId( $intDatabaseTransactionTypeId, $objDatabase ) {
		return self::fetchDatabaseTransactions( sprintf( 'SELECT * FROM database_transactions WHERE database_transaction_type_id = %d', ( int ) $intDatabaseTransactionTypeId ), $objDatabase );
	}

	public static function fetchDatabaseTransactionsByScriptId( $intScriptId, $objDatabase ) {
		return self::fetchDatabaseTransactions( sprintf( 'SELECT * FROM database_transactions WHERE script_id = %d', ( int ) $intScriptId ), $objDatabase );
	}

	public static function fetchDatabaseTransactionsByHardwareId( $intHardwareId, $objDatabase ) {
		return self::fetchDatabaseTransactions( sprintf( 'SELECT * FROM database_transactions WHERE hardware_id = %d', ( int ) $intHardwareId ), $objDatabase );
	}

	public static function fetchDatabaseTransactionsByDatabaseTransactionId( $intDatabaseTransactionId, $objDatabase ) {
		return self::fetchDatabaseTransactions( sprintf( 'SELECT * FROM database_transactions WHERE database_transaction_id = %d', ( int ) $intDatabaseTransactionId ), $objDatabase );
	}

}
?>