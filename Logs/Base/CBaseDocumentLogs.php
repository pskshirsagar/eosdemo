<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CDocumentLogs
 * Do not add any new functions to this class.
 */

class CBaseDocumentLogs extends CEosPluralBase {

	/**
	 * @return CDocumentLog[]
	 */
	public static function fetchDocumentLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDocumentLog', $objDatabase );
	}

	/**
	 * @return CDocumentLog
	 */
	public static function fetchDocumentLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDocumentLog', $objDatabase );
	}

	public static function fetchDocumentLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_logs', $objDatabase );
	}

	public static function fetchDocumentLogById( $intId, $objDatabase ) {
		return self::fetchDocumentLog( sprintf( 'SELECT * FROM document_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDocumentLogsByCid( $intCid, $objDatabase ) {
		return self::fetchDocumentLogs( sprintf( 'SELECT * FROM document_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDocumentLogsByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return self::fetchDocumentLogs( sprintf( 'SELECT * FROM document_logs WHERE company_user_id = %d', ( int ) $intCompanyUserId ), $objDatabase );
	}

	public static function fetchDocumentLogsByFileId( $intFileId, $objDatabase ) {
		return self::fetchDocumentLogs( sprintf( 'SELECT * FROM document_logs WHERE file_id = %d', ( int ) $intFileId ), $objDatabase );
	}

	public static function fetchDocumentLogsByAuditlogId( $intAuditlogId, $objDatabase ) {
		return self::fetchDocumentLogs( sprintf( 'SELECT * FROM document_logs WHERE auditlog_id = %d', ( int ) $intAuditlogId ), $objDatabase );
	}

	public static function fetchDocumentLogsByAuditTypeId( $intAuditTypeId, $objDatabase ) {
		return self::fetchDocumentLogs( sprintf( 'SELECT * FROM document_logs WHERE audit_type_id = %d', ( int ) $intAuditTypeId ), $objDatabase );
	}

}
?>