<?php

class CBaseTableFkey extends CEosSingularBase {

	const TABLE_NAME = 'public.table_fkeys';

	protected $m_intId;
	protected $m_intDatabaseId;
	protected $m_strTableName;
	protected $m_strFieldName;
	protected $m_strFkeyTableName;
	protected $m_strFkeyFieldName;
	protected $m_strFkeyName;
	protected $m_strNotes;
	protected $m_intBadDataCount;
	protected $m_intIsBadFkey;
	protected $m_intHasFkey;
	protected $m_intDismissedBy;
	protected $m_strDismissedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intBadDataCount = '0';
		$this->m_intIsBadFkey = '0';
		$this->m_intHasFkey = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['database_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseId', trim( $arrValues['database_id'] ) ); elseif( isset( $arrValues['database_id'] ) ) $this->setDatabaseId( $arrValues['database_id'] );
		if( isset( $arrValues['table_name'] ) && $boolDirectSet ) $this->set( 'm_strTableName', trim( stripcslashes( $arrValues['table_name'] ) ) ); elseif( isset( $arrValues['table_name'] ) ) $this->setTableName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['table_name'] ) : $arrValues['table_name'] );
		if( isset( $arrValues['field_name'] ) && $boolDirectSet ) $this->set( 'm_strFieldName', trim( stripcslashes( $arrValues['field_name'] ) ) ); elseif( isset( $arrValues['field_name'] ) ) $this->setFieldName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['field_name'] ) : $arrValues['field_name'] );
		if( isset( $arrValues['fkey_table_name'] ) && $boolDirectSet ) $this->set( 'm_strFkeyTableName', trim( stripcslashes( $arrValues['fkey_table_name'] ) ) ); elseif( isset( $arrValues['fkey_table_name'] ) ) $this->setFkeyTableName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['fkey_table_name'] ) : $arrValues['fkey_table_name'] );
		if( isset( $arrValues['fkey_field_name'] ) && $boolDirectSet ) $this->set( 'm_strFkeyFieldName', trim( stripcslashes( $arrValues['fkey_field_name'] ) ) ); elseif( isset( $arrValues['fkey_field_name'] ) ) $this->setFkeyFieldName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['fkey_field_name'] ) : $arrValues['fkey_field_name'] );
		if( isset( $arrValues['fkey_name'] ) && $boolDirectSet ) $this->set( 'm_strFkeyName', trim( stripcslashes( $arrValues['fkey_name'] ) ) ); elseif( isset( $arrValues['fkey_name'] ) ) $this->setFkeyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['fkey_name'] ) : $arrValues['fkey_name'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['bad_data_count'] ) && $boolDirectSet ) $this->set( 'm_intBadDataCount', trim( $arrValues['bad_data_count'] ) ); elseif( isset( $arrValues['bad_data_count'] ) ) $this->setBadDataCount( $arrValues['bad_data_count'] );
		if( isset( $arrValues['is_bad_fkey'] ) && $boolDirectSet ) $this->set( 'm_intIsBadFkey', trim( $arrValues['is_bad_fkey'] ) ); elseif( isset( $arrValues['is_bad_fkey'] ) ) $this->setIsBadFkey( $arrValues['is_bad_fkey'] );
		if( isset( $arrValues['has_fkey'] ) && $boolDirectSet ) $this->set( 'm_intHasFkey', trim( $arrValues['has_fkey'] ) ); elseif( isset( $arrValues['has_fkey'] ) ) $this->setHasFkey( $arrValues['has_fkey'] );
		if( isset( $arrValues['dismissed_by'] ) && $boolDirectSet ) $this->set( 'm_intDismissedBy', trim( $arrValues['dismissed_by'] ) ); elseif( isset( $arrValues['dismissed_by'] ) ) $this->setDismissedBy( $arrValues['dismissed_by'] );
		if( isset( $arrValues['dismissed_on'] ) && $boolDirectSet ) $this->set( 'm_strDismissedOn', trim( $arrValues['dismissed_on'] ) ); elseif( isset( $arrValues['dismissed_on'] ) ) $this->setDismissedOn( $arrValues['dismissed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->set( 'm_intDatabaseId', CStrings::strToIntDef( $intDatabaseId, NULL, false ) );
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function sqlDatabaseId() {
		return ( true == isset( $this->m_intDatabaseId ) ) ? ( string ) $this->m_intDatabaseId : 'NULL';
	}

	public function setTableName( $strTableName ) {
		$this->set( 'm_strTableName', CStrings::strTrimDef( $strTableName, 512, NULL, true ) );
	}

	public function getTableName() {
		return $this->m_strTableName;
	}

	public function sqlTableName() {
		return ( true == isset( $this->m_strTableName ) ) ? '\'' . addslashes( $this->m_strTableName ) . '\'' : 'NULL';
	}

	public function setFieldName( $strFieldName ) {
		$this->set( 'm_strFieldName', CStrings::strTrimDef( $strFieldName, 512, NULL, true ) );
	}

	public function getFieldName() {
		return $this->m_strFieldName;
	}

	public function sqlFieldName() {
		return ( true == isset( $this->m_strFieldName ) ) ? '\'' . addslashes( $this->m_strFieldName ) . '\'' : 'NULL';
	}

	public function setFkeyTableName( $strFkeyTableName ) {
		$this->set( 'm_strFkeyTableName', CStrings::strTrimDef( $strFkeyTableName, 512, NULL, true ) );
	}

	public function getFkeyTableName() {
		return $this->m_strFkeyTableName;
	}

	public function sqlFkeyTableName() {
		return ( true == isset( $this->m_strFkeyTableName ) ) ? '\'' . addslashes( $this->m_strFkeyTableName ) . '\'' : 'NULL';
	}

	public function setFkeyFieldName( $strFkeyFieldName ) {
		$this->set( 'm_strFkeyFieldName', CStrings::strTrimDef( $strFkeyFieldName, 512, NULL, true ) );
	}

	public function getFkeyFieldName() {
		return $this->m_strFkeyFieldName;
	}

	public function sqlFkeyFieldName() {
		return ( true == isset( $this->m_strFkeyFieldName ) ) ? '\'' . addslashes( $this->m_strFkeyFieldName ) . '\'' : 'NULL';
	}

	public function setFkeyName( $strFkeyName ) {
		$this->set( 'm_strFkeyName', CStrings::strTrimDef( $strFkeyName, 1024, NULL, true ) );
	}

	public function getFkeyName() {
		return $this->m_strFkeyName;
	}

	public function sqlFkeyName() {
		return ( true == isset( $this->m_strFkeyName ) ) ? '\'' . addslashes( $this->m_strFkeyName ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setBadDataCount( $intBadDataCount ) {
		$this->set( 'm_intBadDataCount', CStrings::strToIntDef( $intBadDataCount, NULL, false ) );
	}

	public function getBadDataCount() {
		return $this->m_intBadDataCount;
	}

	public function sqlBadDataCount() {
		return ( true == isset( $this->m_intBadDataCount ) ) ? ( string ) $this->m_intBadDataCount : '0';
	}

	public function setIsBadFkey( $intIsBadFkey ) {
		$this->set( 'm_intIsBadFkey', CStrings::strToIntDef( $intIsBadFkey, NULL, false ) );
	}

	public function getIsBadFkey() {
		return $this->m_intIsBadFkey;
	}

	public function sqlIsBadFkey() {
		return ( true == isset( $this->m_intIsBadFkey ) ) ? ( string ) $this->m_intIsBadFkey : '0';
	}

	public function setHasFkey( $intHasFkey ) {
		$this->set( 'm_intHasFkey', CStrings::strToIntDef( $intHasFkey, NULL, false ) );
	}

	public function getHasFkey() {
		return $this->m_intHasFkey;
	}

	public function sqlHasFkey() {
		return ( true == isset( $this->m_intHasFkey ) ) ? ( string ) $this->m_intHasFkey : '0';
	}

	public function setDismissedBy( $intDismissedBy ) {
		$this->set( 'm_intDismissedBy', CStrings::strToIntDef( $intDismissedBy, NULL, false ) );
	}

	public function getDismissedBy() {
		return $this->m_intDismissedBy;
	}

	public function sqlDismissedBy() {
		return ( true == isset( $this->m_intDismissedBy ) ) ? ( string ) $this->m_intDismissedBy : 'NULL';
	}

	public function setDismissedOn( $strDismissedOn ) {
		$this->set( 'm_strDismissedOn', CStrings::strTrimDef( $strDismissedOn, -1, NULL, true ) );
	}

	public function getDismissedOn() {
		return $this->m_strDismissedOn;
	}

	public function sqlDismissedOn() {
		return ( true == isset( $this->m_strDismissedOn ) ) ? '\'' . $this->m_strDismissedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, database_id, table_name, field_name, fkey_table_name, fkey_field_name, fkey_name, notes, bad_data_count, is_bad_fkey, has_fkey, dismissed_by, dismissed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDatabaseId() . ', ' .
 						$this->sqlTableName() . ', ' .
 						$this->sqlFieldName() . ', ' .
 						$this->sqlFkeyTableName() . ', ' .
 						$this->sqlFkeyFieldName() . ', ' .
 						$this->sqlFkeyName() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlBadDataCount() . ', ' .
 						$this->sqlIsBadFkey() . ', ' .
 						$this->sqlHasFkey() . ', ' .
 						$this->sqlDismissedBy() . ', ' .
 						$this->sqlDismissedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; } elseif( true == array_key_exists( 'DatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' table_name = ' . $this->sqlTableName() . ','; } elseif( true == array_key_exists( 'TableName', $this->getChangedColumns() ) ) { $strSql .= ' table_name = ' . $this->sqlTableName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' field_name = ' . $this->sqlFieldName() . ','; } elseif( true == array_key_exists( 'FieldName', $this->getChangedColumns() ) ) { $strSql .= ' field_name = ' . $this->sqlFieldName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fkey_table_name = ' . $this->sqlFkeyTableName() . ','; } elseif( true == array_key_exists( 'FkeyTableName', $this->getChangedColumns() ) ) { $strSql .= ' fkey_table_name = ' . $this->sqlFkeyTableName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fkey_field_name = ' . $this->sqlFkeyFieldName() . ','; } elseif( true == array_key_exists( 'FkeyFieldName', $this->getChangedColumns() ) ) { $strSql .= ' fkey_field_name = ' . $this->sqlFkeyFieldName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fkey_name = ' . $this->sqlFkeyName() . ','; } elseif( true == array_key_exists( 'FkeyName', $this->getChangedColumns() ) ) { $strSql .= ' fkey_name = ' . $this->sqlFkeyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bad_data_count = ' . $this->sqlBadDataCount() . ','; } elseif( true == array_key_exists( 'BadDataCount', $this->getChangedColumns() ) ) { $strSql .= ' bad_data_count = ' . $this->sqlBadDataCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_bad_fkey = ' . $this->sqlIsBadFkey() . ','; } elseif( true == array_key_exists( 'IsBadFkey', $this->getChangedColumns() ) ) { $strSql .= ' is_bad_fkey = ' . $this->sqlIsBadFkey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_fkey = ' . $this->sqlHasFkey() . ','; } elseif( true == array_key_exists( 'HasFkey', $this->getChangedColumns() ) ) { $strSql .= ' has_fkey = ' . $this->sqlHasFkey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dismissed_by = ' . $this->sqlDismissedBy() . ','; } elseif( true == array_key_exists( 'DismissedBy', $this->getChangedColumns() ) ) { $strSql .= ' dismissed_by = ' . $this->sqlDismissedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dismissed_on = ' . $this->sqlDismissedOn() . ','; } elseif( true == array_key_exists( 'DismissedOn', $this->getChangedColumns() ) ) { $strSql .= ' dismissed_on = ' . $this->sqlDismissedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'database_id' => $this->getDatabaseId(),
			'table_name' => $this->getTableName(),
			'field_name' => $this->getFieldName(),
			'fkey_table_name' => $this->getFkeyTableName(),
			'fkey_field_name' => $this->getFkeyFieldName(),
			'fkey_name' => $this->getFkeyName(),
			'notes' => $this->getNotes(),
			'bad_data_count' => $this->getBadDataCount(),
			'is_bad_fkey' => $this->getIsBadFkey(),
			'has_fkey' => $this->getHasFkey(),
			'dismissed_by' => $this->getDismissedBy(),
			'dismissed_on' => $this->getDismissedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>