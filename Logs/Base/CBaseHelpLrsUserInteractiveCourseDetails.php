<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CHelpLrsUserInteractiveCourseDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseHelpLrsUserInteractiveCourseDetails extends CEosPluralBase {

	/**
	 * @return CHelpLrsUserInteractiveCourseDetail[]
	 */
	public static function fetchHelpLrsUserInteractiveCourseDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CHelpLrsUserInteractiveCourseDetail::class, $objDatabase );
	}

	/**
	 * @return CHelpLrsUserInteractiveCourseDetail
	 */
	public static function fetchHelpLrsUserInteractiveCourseDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CHelpLrsUserInteractiveCourseDetail::class, $objDatabase );
	}

	public static function fetchHelpLrsUserInteractiveCourseDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'help_lrs_user_interactive_course_details', $objDatabase );
	}

	public static function fetchHelpLrsUserInteractiveCourseDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchHelpLrsUserInteractiveCourseDetail( sprintf( 'SELECT * FROM help_lrs_user_interactive_course_details WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchHelpLrsUserInteractiveCourseDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchHelpLrsUserInteractiveCourseDetails( sprintf( 'SELECT * FROM help_lrs_user_interactive_course_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchHelpLrsUserInteractiveCourseDetailsByUserIdByCid( $intUserId, $intCid, $objDatabase ) {
		return self::fetchHelpLrsUserInteractiveCourseDetails( sprintf( 'SELECT * FROM help_lrs_user_interactive_course_details WHERE user_id = %d AND cid = %d', ( int ) $intUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchHelpLrsUserInteractiveCourseDetailsByHelpResourceIdByCid( $intHelpResourceId, $intCid, $objDatabase ) {
		return self::fetchHelpLrsUserInteractiveCourseDetails( sprintf( 'SELECT * FROM help_lrs_user_interactive_course_details WHERE help_resource_id = %d AND cid = %d', ( int ) $intHelpResourceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchHelpLrsUserInteractiveCourseDetailsByCompanyUserAssessmentIdByCid( $intCompanyUserAssessmentId, $intCid, $objDatabase ) {
		return self::fetchHelpLrsUserInteractiveCourseDetails( sprintf( 'SELECT * FROM help_lrs_user_interactive_course_details WHERE company_user_assessment_id = %d AND cid = %d', ( int ) $intCompanyUserAssessmentId, ( int ) $intCid ), $objDatabase );
	}

}
?>