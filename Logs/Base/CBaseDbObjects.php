<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CDbObjects
 * Do not add any new functions to this class.
 */

class CBaseDbObjects extends CEosPluralBase {

	/**
	 * @return CDbObject[]
	 */
	public static function fetchDbObjects( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDbObject', $objDatabase );
	}

	/**
	 * @return CDbObject
	 */
	public static function fetchDbObject( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDbObject', $objDatabase );
	}

	public static function fetchDbObjectCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'db_objects', $objDatabase );
	}

	public static function fetchDbObjectById( $intId, $objDatabase ) {
		return self::fetchDbObject( sprintf( 'SELECT * FROM db_objects WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDbObjectsByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchDbObjects( sprintf( 'SELECT * FROM db_objects WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

	public static function fetchDbObjectsByDbObjectTypeId( $intDbObjectTypeId, $objDatabase ) {
		return self::fetchDbObjects( sprintf( 'SELECT * FROM db_objects WHERE db_object_type_id = %d', ( int ) $intDbObjectTypeId ), $objDatabase );
	}

	public static function fetchDbObjectsByTableId( $intTableId, $objDatabase ) {
		return self::fetchDbObjects( sprintf( 'SELECT * FROM db_objects WHERE table_id = %d', ( int ) $intTableId ), $objDatabase );
	}

}
?>