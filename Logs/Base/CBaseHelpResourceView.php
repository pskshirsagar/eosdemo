<?php

class CBaseHelpResourceView extends CEosSingularBase {

	const TABLE_NAME = 'public.help_resource_views';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intHelpResourceId;
	protected $m_strViewByCompanyUserName;
	protected $m_strCompanyUserRole;
	protected $m_strCompanyName;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsDownloaded;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';
		$this->m_boolIsDownloaded = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['help_resource_id'] ) && $boolDirectSet ) $this->set( 'm_intHelpResourceId', trim( $arrValues['help_resource_id'] ) ); elseif( isset( $arrValues['help_resource_id'] ) ) $this->setHelpResourceId( $arrValues['help_resource_id'] );
		if( isset( $arrValues['view_by_company_user_name'] ) && $boolDirectSet ) $this->set( 'm_strViewByCompanyUserName', trim( $arrValues['view_by_company_user_name'] ) ); elseif( isset( $arrValues['view_by_company_user_name'] ) ) $this->setViewByCompanyUserName( $arrValues['view_by_company_user_name'] );
		if( isset( $arrValues['company_user_role'] ) && $boolDirectSet ) $this->set( 'm_strCompanyUserRole', trim( $arrValues['company_user_role'] ) ); elseif( isset( $arrValues['company_user_role'] ) ) $this->setCompanyUserRole( $arrValues['company_user_role'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( $arrValues['company_name'] ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( $arrValues['company_name'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_downloaded'] ) && $boolDirectSet ) $this->set( 'm_boolIsDownloaded', trim( stripcslashes( $arrValues['is_downloaded'] ) ) ); elseif( isset( $arrValues['is_downloaded'] ) ) $this->setIsDownloaded( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_downloaded'] ) : $arrValues['is_downloaded'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setHelpResourceId( $intHelpResourceId ) {
		$this->set( 'm_intHelpResourceId', CStrings::strToIntDef( $intHelpResourceId, NULL, false ) );
	}

	public function getHelpResourceId() {
		return $this->m_intHelpResourceId;
	}

	public function sqlHelpResourceId() {
		return ( true == isset( $this->m_intHelpResourceId ) ) ? ( string ) $this->m_intHelpResourceId : 'NULL';
	}

	public function setViewByCompanyUserName( $strViewByCompanyUserName ) {
		$this->set( 'm_strViewByCompanyUserName', CStrings::strTrimDef( $strViewByCompanyUserName, 240, NULL, true ) );
	}

	public function getViewByCompanyUserName() {
		return $this->m_strViewByCompanyUserName;
	}

	public function sqlViewByCompanyUserName() {
		return ( true == isset( $this->m_strViewByCompanyUserName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strViewByCompanyUserName ) : '\'' . addslashes( $this->m_strViewByCompanyUserName ) . '\'' ) : 'NULL';
	}

	public function setCompanyUserRole( $strCompanyUserRole ) {
		$this->set( 'm_strCompanyUserRole', CStrings::strTrimDef( $strCompanyUserRole, 240, NULL, true ) );
	}

	public function getCompanyUserRole() {
		return $this->m_strCompanyUserRole;
	}

	public function sqlCompanyUserRole() {
		return ( true == isset( $this->m_strCompanyUserRole ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyUserRole ) : '\'' . addslashes( $this->m_strCompanyUserRole ) . '\'' ) : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 240, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyName ) : '\'' . addslashes( $this->m_strCompanyName ) . '\'' ) : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsDownloaded( $boolIsDownloaded ) {
		$this->set( 'm_boolIsDownloaded', CStrings::strToBool( $boolIsDownloaded ) );
	}

	public function getIsDownloaded() {
		return $this->m_boolIsDownloaded;
	}

	public function sqlIsDownloaded() {
		return ( true == isset( $this->m_boolIsDownloaded ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDownloaded ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, help_resource_id, view_by_company_user_name, company_user_role, company_name, created_by, created_on, is_downloaded )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlHelpResourceId() . ', ' .
		          $this->sqlViewByCompanyUserName() . ', ' .
		          $this->sqlCompanyUserRole() . ', ' .
		          $this->sqlCompanyName() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlIsDownloaded() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' help_resource_id = ' . $this->sqlHelpResourceId(). ',' ; } elseif( true == array_key_exists( 'HelpResourceId', $this->getChangedColumns() ) ) { $strSql .= ' help_resource_id = ' . $this->sqlHelpResourceId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' view_by_company_user_name = ' . $this->sqlViewByCompanyUserName(). ',' ; } elseif( true == array_key_exists( 'ViewByCompanyUserName', $this->getChangedColumns() ) ) { $strSql .= ' view_by_company_user_name = ' . $this->sqlViewByCompanyUserName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_role = ' . $this->sqlCompanyUserRole(). ',' ; } elseif( true == array_key_exists( 'CompanyUserRole', $this->getChangedColumns() ) ) { $strSql .= ' company_user_role = ' . $this->sqlCompanyUserRole() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName(). ',' ; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_downloaded = ' . $this->sqlIsDownloaded() ; } elseif( true == array_key_exists( 'IsDownloaded', $this->getChangedColumns() ) ) { $strSql .= ' is_downloaded = ' . $this->sqlIsDownloaded() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'help_resource_id' => $this->getHelpResourceId(),
			'view_by_company_user_name' => $this->getViewByCompanyUserName(),
			'company_user_role' => $this->getCompanyUserRole(),
			'company_name' => $this->getCompanyName(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_downloaded' => $this->getIsDownloaded()
		);
	}

}
?>