<?php

class CBaseDbObjectStat extends CEosSingularBase {

	const TABLE_NAME = 'public.db_object_stats';

	protected $m_intId;
	protected $m_intDatabaseTransactionId;
	protected $m_intDbObjectId;
	protected $m_intCount;
	protected $m_intSize;
	protected $m_strLogDatetime;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCount = '0';
		$this->m_intSize = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['database_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseTransactionId', trim( $arrValues['database_transaction_id'] ) ); elseif( isset( $arrValues['database_transaction_id'] ) ) $this->setDatabaseTransactionId( $arrValues['database_transaction_id'] );
		if( isset( $arrValues['db_object_id'] ) && $boolDirectSet ) $this->set( 'm_intDbObjectId', trim( $arrValues['db_object_id'] ) ); elseif( isset( $arrValues['db_object_id'] ) ) $this->setDbObjectId( $arrValues['db_object_id'] );
		if( isset( $arrValues['count'] ) && $boolDirectSet ) $this->set( 'm_intCount', trim( $arrValues['count'] ) ); elseif( isset( $arrValues['count'] ) ) $this->setCount( $arrValues['count'] );
		if( isset( $arrValues['size'] ) && $boolDirectSet ) $this->set( 'm_intSize', trim( $arrValues['size'] ) ); elseif( isset( $arrValues['size'] ) ) $this->setSize( $arrValues['size'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDatabaseTransactionId( $intDatabaseTransactionId ) {
		$this->set( 'm_intDatabaseTransactionId', CStrings::strToIntDef( $intDatabaseTransactionId, NULL, false ) );
	}

	public function getDatabaseTransactionId() {
		return $this->m_intDatabaseTransactionId;
	}

	public function sqlDatabaseTransactionId() {
		return ( true == isset( $this->m_intDatabaseTransactionId ) ) ? ( string ) $this->m_intDatabaseTransactionId : 'NULL';
	}

	public function setDbObjectId( $intDbObjectId ) {
		$this->set( 'm_intDbObjectId', CStrings::strToIntDef( $intDbObjectId, NULL, false ) );
	}

	public function getDbObjectId() {
		return $this->m_intDbObjectId;
	}

	public function sqlDbObjectId() {
		return ( true == isset( $this->m_intDbObjectId ) ) ? ( string ) $this->m_intDbObjectId : 'NULL';
	}

	public function setCount( $intCount ) {
		$this->m_intCount = CStrings::strToIntDef( $intCount, NULL, false );
		$this->set( 'm_intCount', CStrings::strToIntDef( $intCount, NULL, false ) );
	}

	public function getCount() {
		return $this->m_intCount;
	}

	public function sqlCount() {
		return ( true == isset( $this->m_intCount ) ) ? ( string ) $this->m_intCount : '0';
	}

	public function setSize( $intSize ) {
		$this->m_intSize = CStrings::strToIntDef( $intSize, NULL, false );
		$this->set( 'm_intSize', CStrings::strToIntDef( $intSize, NULL, false ) );
	}

	public function getSize() {
		return $this->m_intSize;
	}

	public function sqlSize() {
		return ( true == isset( $this->m_intSize ) ) ? ( string ) $this->m_intSize : '0';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, database_transaction_id, db_object_id, count, size, log_datetime, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDatabaseTransactionId() . ', ' .
 						$this->sqlDbObjectId() . ', ' .
 						$this->sqlCount() . ', ' .
 						$this->sqlSize() . ', ' .
 						$this->sqlLogDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_transaction_id = ' . $this->sqlDatabaseTransactionId() . ','; } elseif( true == array_key_exists( 'DatabaseTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' database_transaction_id = ' . $this->sqlDatabaseTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' db_object_id = ' . $this->sqlDbObjectId() . ','; } elseif( true == array_key_exists( 'DbObjectId', $this->getChangedColumns() ) ) { $strSql .= ' db_object_id = ' . $this->sqlDbObjectId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' count = ' . $this->sqlCount() . ','; } elseif( true == array_key_exists( 'Count', $this->getChangedColumns() ) ) { $strSql .= ' count = ' . $this->sqlCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' size = ' . $this->sqlSize() . ','; } elseif( true == array_key_exists( 'Size', $this->getChangedColumns() ) ) { $strSql .= ' size = ' . $this->sqlSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'database_transaction_id' => $this->getDatabaseTransactionId(),
			'db_object_id' => $this->getDbObjectId(),
			'count' => $this->getCount(),
			'size' => $this->getSize(),
			'log_datetime' => $this->getLogDatetime(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>