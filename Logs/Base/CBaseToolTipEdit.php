<?php

class CBaseToolTipEdit extends CEosSingularBase {

	const TABLE_NAME = 'public.tool_tip_edits';

	protected $m_intId;
	protected $m_intToolTipId;
	protected $m_strFieldName;
	protected $m_strOldData;
	protected $m_strNewData;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['tool_tip_id'] ) && $boolDirectSet ) $this->set( 'm_intToolTipId', trim( $arrValues['tool_tip_id'] ) ); elseif( isset( $arrValues['tool_tip_id'] ) ) $this->setToolTipId( $arrValues['tool_tip_id'] );
		if( isset( $arrValues['field_name'] ) && $boolDirectSet ) $this->set( 'm_strFieldName', trim( stripcslashes( $arrValues['field_name'] ) ) ); elseif( isset( $arrValues['field_name'] ) ) $this->setFieldName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['field_name'] ) : $arrValues['field_name'] );
		if( isset( $arrValues['old_data'] ) && $boolDirectSet ) $this->set( 'm_strOldData', trim( stripcslashes( $arrValues['old_data'] ) ) ); elseif( isset( $arrValues['old_data'] ) ) $this->setOldData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['old_data'] ) : $arrValues['old_data'] );
		if( isset( $arrValues['new_data'] ) && $boolDirectSet ) $this->set( 'm_strNewData', trim( stripcslashes( $arrValues['new_data'] ) ) ); elseif( isset( $arrValues['new_data'] ) ) $this->setNewData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_data'] ) : $arrValues['new_data'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setToolTipId( $intToolTipId ) {
		$this->set( 'm_intToolTipId', CStrings::strToIntDef( $intToolTipId, NULL, false ) );
	}

	public function getToolTipId() {
		return $this->m_intToolTipId;
	}

	public function sqlToolTipId() {
		return ( true == isset( $this->m_intToolTipId ) ) ? ( string ) $this->m_intToolTipId : 'NULL';
	}

	public function setFieldName( $strFieldName ) {
		$this->set( 'm_strFieldName', CStrings::strTrimDef( $strFieldName, 30, NULL, true ) );
	}

	public function getFieldName() {
		return $this->m_strFieldName;
	}

	public function sqlFieldName() {
		return ( true == isset( $this->m_strFieldName ) ) ? '\'' . addslashes( $this->m_strFieldName ) . '\'' : 'NULL';
	}

	public function setOldData( $strOldData ) {
		$this->set( 'm_strOldData', CStrings::strTrimDef( $strOldData, -1, NULL, true ) );
	}

	public function getOldData() {
		return $this->m_strOldData;
	}

	public function sqlOldData() {
		return ( true == isset( $this->m_strOldData ) ) ? '\'' . addslashes( $this->m_strOldData ) . '\'' : 'NULL';
	}

	public function setNewData( $strNewData ) {
		$this->set( 'm_strNewData', CStrings::strTrimDef( $strNewData, -1, NULL, true ) );
	}

	public function getNewData() {
		return $this->m_strNewData;
	}

	public function sqlNewData() {
		return ( true == isset( $this->m_strNewData ) ) ? '\'' . addslashes( $this->m_strNewData ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, tool_tip_id, field_name, old_data, new_data, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlToolTipId() . ', ' .
 						$this->sqlFieldName() . ', ' .
 						$this->sqlOldData() . ', ' .
 						$this->sqlNewData() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tool_tip_id = ' . $this->sqlToolTipId() . ','; } elseif( true == array_key_exists( 'ToolTipId', $this->getChangedColumns() ) ) { $strSql .= ' tool_tip_id = ' . $this->sqlToolTipId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' field_name = ' . $this->sqlFieldName() . ','; } elseif( true == array_key_exists( 'FieldName', $this->getChangedColumns() ) ) { $strSql .= ' field_name = ' . $this->sqlFieldName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_data = ' . $this->sqlOldData() . ','; } elseif( true == array_key_exists( 'OldData', $this->getChangedColumns() ) ) { $strSql .= ' old_data = ' . $this->sqlOldData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_data = ' . $this->sqlNewData() . ','; } elseif( true == array_key_exists( 'NewData', $this->getChangedColumns() ) ) { $strSql .= ' new_data = ' . $this->sqlNewData() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'tool_tip_id' => $this->getToolTipId(),
			'field_name' => $this->getFieldName(),
			'old_data' => $this->getOldData(),
			'new_data' => $this->getNewData(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>