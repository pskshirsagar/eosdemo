<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSessionLogSettings
 * Do not add any new functions to this class.
 */

class CBaseSessionLogSettings extends CEosPluralBase {

	/**
	 * @return CSessionLogSetting[]
	 */
	public static function fetchSessionLogSettings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSessionLogSetting', $objDatabase );
	}

	/**
	 * @return CSessionLogSetting
	 */
	public static function fetchSessionLogSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSessionLogSetting', $objDatabase );
	}

	public static function fetchSessionLogSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'session_log_settings', $objDatabase );
	}

	public static function fetchSessionLogSettingById( $intId, $objDatabase ) {
		return self::fetchSessionLogSetting( sprintf( 'SELECT * FROM session_log_settings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSessionLogSettingsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchSessionLogSettings( sprintf( 'SELECT * FROM session_log_settings WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

}
?>