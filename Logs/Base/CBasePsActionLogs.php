<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CPsActionLogs
 * Do not add any new functions to this class.
 */

class CBasePsActionLogs extends CEosPluralBase {

	/**
	 * @return CPsActionLog[]
	 */
	public static function fetchPsActionLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsActionLog', $objDatabase );
	}

	/**
	 * @return CPsActionLog
	 */
	public static function fetchPsActionLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsActionLog', $objDatabase );
	}

	public static function fetchPsActionLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_action_logs', $objDatabase );
	}

	public static function fetchPsActionLogById( $intId, $objDatabase ) {
		return self::fetchPsActionLog( sprintf( 'SELECT * FROM ps_action_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsActionLogsByCid( $intCid, $objDatabase ) {
		return self::fetchPsActionLogs( sprintf( 'SELECT * FROM ps_action_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPsActionLogsByWebsiteId( $intWebsiteId, $objDatabase ) {
		return self::fetchPsActionLogs( sprintf( 'SELECT * FROM ps_action_logs WHERE website_id = %d', ( int ) $intWebsiteId ), $objDatabase );
	}

	public static function fetchPsActionLogsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchPsActionLogs( sprintf( 'SELECT * FROM ps_action_logs WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

}
?>