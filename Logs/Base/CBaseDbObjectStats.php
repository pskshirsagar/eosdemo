<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CDbObjectStats
 * Do not add any new functions to this class.
 */

class CBaseDbObjectStats extends CEosPluralBase {

	/**
	 * @return CDbObjectStat[]
	 */
	public static function fetchDbObjectStats( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDbObjectStat', $objDatabase );
	}

	/**
	 * @return CDbObjectStat
	 */
	public static function fetchDbObjectStat( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDbObjectStat', $objDatabase );
	}

	public static function fetchDbObjectStatCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'db_object_stats', $objDatabase );
	}

	public static function fetchDbObjectStatById( $intId, $objDatabase ) {
		return self::fetchDbObjectStat( sprintf( 'SELECT * FROM db_object_stats WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDbObjectStatsByDatabaseTransactionId( $intDatabaseTransactionId, $objDatabase ) {
		return self::fetchDbObjectStats( sprintf( 'SELECT * FROM db_object_stats WHERE database_transaction_id = %d', ( int ) $intDatabaseTransactionId ), $objDatabase );
	}

	public static function fetchDbObjectStatsByDbObjectId( $intDbObjectId, $objDatabase ) {
		return self::fetchDbObjectStats( sprintf( 'SELECT * FROM db_object_stats WHERE db_object_id = %d', ( int ) $intDbObjectId ), $objDatabase );
	}

}
?>