<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSqlLogs
 * Do not add any new functions to this class.
 */

class CBaseSqlLogs extends CEosPluralBase {

	/**
	 * @return CSqlLog[]
	 */
	public static function fetchSqlLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSqlLog', $objDatabase );
	}

	/**
	 * @return CSqlLog
	 */
	public static function fetchSqlLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSqlLog', $objDatabase );
	}

	public static function fetchSqlLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sql_logs', $objDatabase );
	}

	public static function fetchSqlLogById( $intId, $objDatabase ) {
		return self::fetchSqlLog( sprintf( 'SELECT * FROM sql_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSqlLogsByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchSqlLogs( sprintf( 'SELECT * FROM sql_logs WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

	public static function fetchSqlLogsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchSqlLogs( sprintf( 'SELECT * FROM sql_logs WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchSqlLogsBySqlStatementId( $intSqlStatementId, $objDatabase ) {
		return self::fetchSqlLogs( sprintf( 'SELECT * FROM sql_logs WHERE sql_statement_id = %d', ( int ) $intSqlStatementId ), $objDatabase );
	}

}
?>