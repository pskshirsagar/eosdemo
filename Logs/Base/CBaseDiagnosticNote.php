<?php

class CBaseDiagnosticNote extends CEosSingularBase {

	const TABLE_NAME = 'public.diagnostic_notes';

	protected $m_intId;
	protected $m_intDiagnosticNoteTypeId;
	protected $m_intDiagnosticId;
	protected $m_intEmployeeId;
	protected $m_strName;
	protected $m_strDiagnosticNote;
	protected $m_strSql;
	protected $m_strDiagnosticNoteDatetime;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intDiagnosticNoteTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['diagnostic_note_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDiagnosticNoteTypeId', trim( $arrValues['diagnostic_note_type_id'] ) ); elseif( isset( $arrValues['diagnostic_note_type_id'] ) ) $this->setDiagnosticNoteTypeId( $arrValues['diagnostic_note_type_id'] );
		if( isset( $arrValues['diagnostic_id'] ) && $boolDirectSet ) $this->set( 'm_intDiagnosticId', trim( $arrValues['diagnostic_id'] ) ); elseif( isset( $arrValues['diagnostic_id'] ) ) $this->setDiagnosticId( $arrValues['diagnostic_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['diagnostic_note'] ) && $boolDirectSet ) $this->set( 'm_strDiagnosticNote', trim( stripcslashes( $arrValues['diagnostic_note'] ) ) ); elseif( isset( $arrValues['diagnostic_note'] ) ) $this->setDiagnosticNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['diagnostic_note'] ) : $arrValues['diagnostic_note'] );
		if( isset( $arrValues['sql'] ) && $boolDirectSet ) $this->set( 'm_strSql', trim( stripcslashes( $arrValues['sql'] ) ) ); elseif( isset( $arrValues['sql'] ) ) $this->setSql( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sql'] ) : $arrValues['sql'] );
		if( isset( $arrValues['diagnostic_note_datetime'] ) && $boolDirectSet ) $this->set( 'm_strDiagnosticNoteDatetime', trim( $arrValues['diagnostic_note_datetime'] ) ); elseif( isset( $arrValues['diagnostic_note_datetime'] ) ) $this->setDiagnosticNoteDatetime( $arrValues['diagnostic_note_datetime'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDiagnosticNoteTypeId( $intDiagnosticNoteTypeId ) {
		$this->set( 'm_intDiagnosticNoteTypeId', CStrings::strToIntDef( $intDiagnosticNoteTypeId, NULL, false ) );
	}

	public function getDiagnosticNoteTypeId() {
		return $this->m_intDiagnosticNoteTypeId;
	}

	public function sqlDiagnosticNoteTypeId() {
		return ( true == isset( $this->m_intDiagnosticNoteTypeId ) ) ? ( string ) $this->m_intDiagnosticNoteTypeId : '1';
	}

	public function setDiagnosticId( $intDiagnosticId ) {
		$this->set( 'm_intDiagnosticId', CStrings::strToIntDef( $intDiagnosticId, NULL, false ) );
	}

	public function getDiagnosticId() {
		return $this->m_intDiagnosticId;
	}

	public function sqlDiagnosticId() {
		return ( true == isset( $this->m_intDiagnosticId ) ) ? ( string ) $this->m_intDiagnosticId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDiagnosticNote( $strDiagnosticNote ) {
		$this->set( 'm_strDiagnosticNote', CStrings::strTrimDef( $strDiagnosticNote, -1, NULL, true ) );
	}

	public function getDiagnosticNote() {
		return $this->m_strDiagnosticNote;
	}

	public function sqlDiagnosticNote() {
		return ( true == isset( $this->m_strDiagnosticNote ) ) ? '\'' . addslashes( $this->m_strDiagnosticNote ) . '\'' : 'NULL';
	}

	public function setSql( $strSql ) {
		$this->set( 'm_strSql', CStrings::strTrimDef( $strSql, -1, NULL, true ) );
	}

	public function getSql() {
		return $this->m_strSql;
	}

	public function sqlSql() {
		return ( true == isset( $this->m_strSql ) ) ? '\'' . addslashes( $this->m_strSql ) . '\'' : 'NULL';
	}

	public function setDiagnosticNoteDatetime( $strDiagnosticNoteDatetime ) {
		$this->set( 'm_strDiagnosticNoteDatetime', CStrings::strTrimDef( $strDiagnosticNoteDatetime, -1, NULL, true ) );
	}

	public function getDiagnosticNoteDatetime() {
		return $this->m_strDiagnosticNoteDatetime;
	}

	public function sqlDiagnosticNoteDatetime() {
		return ( true == isset( $this->m_strDiagnosticNoteDatetime ) ) ? '\'' . $this->m_strDiagnosticNoteDatetime . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, diagnostic_note_type_id, diagnostic_id, employee_id, name, diagnostic_note, sql, diagnostic_note_datetime, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDiagnosticNoteTypeId() . ', ' .
 						$this->sqlDiagnosticId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDiagnosticNote() . ', ' .
 						$this->sqlSql() . ', ' .
 						$this->sqlDiagnosticNoteDatetime() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' diagnostic_note_type_id = ' . $this->sqlDiagnosticNoteTypeId() . ','; } elseif( true == array_key_exists( 'DiagnosticNoteTypeId', $this->getChangedColumns() ) ) { $strSql .= ' diagnostic_note_type_id = ' . $this->sqlDiagnosticNoteTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' diagnostic_id = ' . $this->sqlDiagnosticId() . ','; } elseif( true == array_key_exists( 'DiagnosticId', $this->getChangedColumns() ) ) { $strSql .= ' diagnostic_id = ' . $this->sqlDiagnosticId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' diagnostic_note = ' . $this->sqlDiagnosticNote() . ','; } elseif( true == array_key_exists( 'DiagnosticNote', $this->getChangedColumns() ) ) { $strSql .= ' diagnostic_note = ' . $this->sqlDiagnosticNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sql = ' . $this->sqlSql() . ','; } elseif( true == array_key_exists( 'Sql', $this->getChangedColumns() ) ) { $strSql .= ' sql = ' . $this->sqlSql() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' diagnostic_note_datetime = ' . $this->sqlDiagnosticNoteDatetime() . ','; } elseif( true == array_key_exists( 'DiagnosticNoteDatetime', $this->getChangedColumns() ) ) { $strSql .= ' diagnostic_note_datetime = ' . $this->sqlDiagnosticNoteDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'diagnostic_note_type_id' => $this->getDiagnosticNoteTypeId(),
			'diagnostic_id' => $this->getDiagnosticId(),
			'employee_id' => $this->getEmployeeId(),
			'name' => $this->getName(),
			'diagnostic_note' => $this->getDiagnosticNote(),
			'sql' => $this->getSql(),
			'diagnostic_note_datetime' => $this->getDiagnosticNoteDatetime(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>