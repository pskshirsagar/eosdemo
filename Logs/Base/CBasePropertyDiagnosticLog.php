<?php

class CBasePropertyDiagnosticLog extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.property_diagnostic_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_strRequestKey;
	protected $m_intDiagnosticId;
	protected $m_intDatabaseId;
	protected $m_intDiagnosticResultTypeId;
	protected $m_strLogDatetime;
	protected $m_fltItemCount;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltItemCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['request_key'] ) && $boolDirectSet ) $this->set( 'm_strRequestKey', trim( stripcslashes( $arrValues['request_key'] ) ) ); elseif( isset( $arrValues['request_key'] ) ) $this->setRequestKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['request_key'] ) : $arrValues['request_key'] );
		if( isset( $arrValues['diagnostic_id'] ) && $boolDirectSet ) $this->set( 'm_intDiagnosticId', trim( $arrValues['diagnostic_id'] ) ); elseif( isset( $arrValues['diagnostic_id'] ) ) $this->setDiagnosticId( $arrValues['diagnostic_id'] );
		if( isset( $arrValues['database_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseId', trim( $arrValues['database_id'] ) ); elseif( isset( $arrValues['database_id'] ) ) $this->setDatabaseId( $arrValues['database_id'] );
		if( isset( $arrValues['diagnostic_result_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDiagnosticResultTypeId', trim( $arrValues['diagnostic_result_type_id'] ) ); elseif( isset( $arrValues['diagnostic_result_type_id'] ) ) $this->setDiagnosticResultTypeId( $arrValues['diagnostic_result_type_id'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['item_count'] ) && $boolDirectSet ) $this->set( 'm_fltItemCount', trim( $arrValues['item_count'] ) ); elseif( isset( $arrValues['item_count'] ) ) $this->setItemCount( $arrValues['item_count'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setRequestKey( $strRequestKey ) {
		$this->set( 'm_strRequestKey', CStrings::strTrimDef( $strRequestKey, 200, NULL, true ) );
	}

	public function getRequestKey() {
		return $this->m_strRequestKey;
	}

	public function sqlRequestKey() {
		return ( true == isset( $this->m_strRequestKey ) ) ? '\'' . addslashes( $this->m_strRequestKey ) . '\'' : 'NULL';
	}

	public function setDiagnosticId( $intDiagnosticId ) {
		$this->set( 'm_intDiagnosticId', CStrings::strToIntDef( $intDiagnosticId, NULL, false ) );
	}

	public function getDiagnosticId() {
		return $this->m_intDiagnosticId;
	}

	public function sqlDiagnosticId() {
		return ( true == isset( $this->m_intDiagnosticId ) ) ? ( string ) $this->m_intDiagnosticId : 'NULL';
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->set( 'm_intDatabaseId', CStrings::strToIntDef( $intDatabaseId, NULL, false ) );
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function sqlDatabaseId() {
		return ( true == isset( $this->m_intDatabaseId ) ) ? ( string ) $this->m_intDatabaseId : 'NULL';
	}

	public function setDiagnosticResultTypeId( $intDiagnosticResultTypeId ) {
		$this->set( 'm_intDiagnosticResultTypeId', CStrings::strToIntDef( $intDiagnosticResultTypeId, NULL, false ) );
	}

	public function getDiagnosticResultTypeId() {
		return $this->m_intDiagnosticResultTypeId;
	}

	public function sqlDiagnosticResultTypeId() {
		return ( true == isset( $this->m_intDiagnosticResultTypeId ) ) ? ( string ) $this->m_intDiagnosticResultTypeId : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setItemCount( $fltItemCount ) {
		$this->set( 'm_fltItemCount', CStrings::strToFloatDef( $fltItemCount, NULL, false, 2 ) );
	}

	public function getItemCount() {
		return $this->m_fltItemCount;
	}

	public function sqlItemCount() {
		return ( true == isset( $this->m_fltItemCount ) ) ? ( string ) $this->m_fltItemCount : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, request_key, diagnostic_id, database_id, diagnostic_result_type_id, log_datetime, item_count, details, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlRequestKey() . ', ' .
						$this->sqlDiagnosticId() . ', ' .
						$this->sqlDatabaseId() . ', ' .
						$this->sqlDiagnosticResultTypeId() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlItemCount() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_key = ' . $this->sqlRequestKey(). ',' ; } elseif( true == array_key_exists( 'RequestKey', $this->getChangedColumns() ) ) { $strSql .= ' request_key = ' . $this->sqlRequestKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' diagnostic_id = ' . $this->sqlDiagnosticId(). ',' ; } elseif( true == array_key_exists( 'DiagnosticId', $this->getChangedColumns() ) ) { $strSql .= ' diagnostic_id = ' . $this->sqlDiagnosticId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId(). ',' ; } elseif( true == array_key_exists( 'DatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' diagnostic_result_type_id = ' . $this->sqlDiagnosticResultTypeId(). ',' ; } elseif( true == array_key_exists( 'DiagnosticResultTypeId', $this->getChangedColumns() ) ) { $strSql .= ' diagnostic_result_type_id = ' . $this->sqlDiagnosticResultTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_count = ' . $this->sqlItemCount(). ',' ; } elseif( true == array_key_exists( 'ItemCount', $this->getChangedColumns() ) ) { $strSql .= ' item_count = ' . $this->sqlItemCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'request_key' => $this->getRequestKey(),
			'diagnostic_id' => $this->getDiagnosticId(),
			'database_id' => $this->getDatabaseId(),
			'diagnostic_result_type_id' => $this->getDiagnosticResultTypeId(),
			'log_datetime' => $this->getLogDatetime(),
			'item_count' => $this->getItemCount(),
			'details' => $this->getDetails(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>