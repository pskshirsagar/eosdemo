<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CHelpResourceLogDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseHelpResourceLogDetails extends CEosPluralBase {

	/**
	 * @return CHelpResourceLogDetail[]
	 */
	public static function fetchHelpResourceLogDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CHelpResourceLogDetail', $objDatabase );
	}

	/**
	 * @return CHelpResourceLogDetail
	 */
	public static function fetchHelpResourceLogDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CHelpResourceLogDetail', $objDatabase );
	}

	public static function fetchHelpResourceLogDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'help_resource_log_details', $objDatabase );
	}

	public static function fetchHelpResourceLogDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchHelpResourceLogDetail( sprintf( 'SELECT * FROM help_resource_log_details WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchHelpResourceLogDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchHelpResourceLogDetails( sprintf( 'SELECT * FROM help_resource_log_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchHelpResourceLogDetailsByHelpResourceIdByCid( $intHelpResourceId, $intCid, $objDatabase ) {
		return self::fetchHelpResourceLogDetails( sprintf( 'SELECT * FROM help_resource_log_details WHERE help_resource_id = %d AND cid = %d', ( int ) $intHelpResourceId, ( int ) $intCid ), $objDatabase );
	}

}
?>