<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSessionLogs
 * Do not add any new functions to this class.
 */

class CBaseSessionLogs extends CEosPluralBase {

	/**
	 * @return CSessionLog[]
	 */
	public static function fetchSessionLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSessionLog', $objDatabase );
	}

	/**
	 * @return CSessionLog
	 */
	public static function fetchSessionLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSessionLog', $objDatabase );
	}

	public static function fetchSessionLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'session_logs', $objDatabase );
	}

	public static function fetchSessionLogById( $intId, $objDatabase ) {
		return self::fetchSessionLog( sprintf( 'SELECT * FROM session_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSessionLogsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchSessionLogs( sprintf( 'SELECT * FROM session_logs WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchSessionLogsBySessionId( $strSessionId, $objDatabase ) {
		return self::fetchSessionLogs( sprintf( 'SELECT * FROM session_logs WHERE session_id = \'%s\'', $strSessionId ), $objDatabase );
	}

}
?>