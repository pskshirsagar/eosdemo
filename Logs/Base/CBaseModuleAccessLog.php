<?php

class CBaseModuleAccessLog extends CEosSingularBase {

	const TABLE_NAME = 'public.module_access_logs';

	protected $m_intPsProductId;
	protected $m_strModule;
	protected $m_strAction;
	protected $m_strDay;
	protected $m_intHitCount;
	protected $m_fltTotalExecutionSeconds;
	protected $m_fltMinExecutionSeconds;
	protected $m_fltMaxExecutionSeconds;
	protected $m_intTotalSqlStatements;
	protected $m_fltSlowestQuerySeconds;
	protected $m_intMaxDuplicateSqlCount;
	protected $m_intMaxRowsReturned;

	public function __construct() {
		parent::__construct();

		$this->m_intHitCount = '0';
		$this->m_fltTotalExecutionSeconds = '0';
		$this->m_fltMinExecutionSeconds = '0';
		$this->m_fltMaxExecutionSeconds = '0';
		$this->m_intTotalSqlStatements = '0';
		$this->m_fltSlowestQuerySeconds = '0';
		$this->m_intMaxDuplicateSqlCount = '0';
		$this->m_intMaxRowsReturned = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['module'] ) && $boolDirectSet ) $this->set( 'm_strModule', trim( stripcslashes( $arrValues['module'] ) ) ); elseif( isset( $arrValues['module'] ) ) $this->setModule( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['module'] ) : $arrValues['module'] );
		if( isset( $arrValues['action'] ) && $boolDirectSet ) $this->set( 'm_strAction', trim( stripcslashes( $arrValues['action'] ) ) ); elseif( isset( $arrValues['action'] ) ) $this->setAction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action'] ) : $arrValues['action'] );
		if( isset( $arrValues['day'] ) && $boolDirectSet ) $this->set( 'm_strDay', trim( $arrValues['day'] ) ); elseif( isset( $arrValues['day'] ) ) $this->setDay( $arrValues['day'] );
		if( isset( $arrValues['hit_count'] ) && $boolDirectSet ) $this->set( 'm_intHitCount', trim( $arrValues['hit_count'] ) ); elseif( isset( $arrValues['hit_count'] ) ) $this->setHitCount( $arrValues['hit_count'] );
		if( isset( $arrValues['total_execution_seconds'] ) && $boolDirectSet ) $this->set( 'm_fltTotalExecutionSeconds', trim( $arrValues['total_execution_seconds'] ) ); elseif( isset( $arrValues['total_execution_seconds'] ) ) $this->setTotalExecutionSeconds( $arrValues['total_execution_seconds'] );
		if( isset( $arrValues['min_execution_seconds'] ) && $boolDirectSet ) $this->set( 'm_fltMinExecutionSeconds', trim( $arrValues['min_execution_seconds'] ) ); elseif( isset( $arrValues['min_execution_seconds'] ) ) $this->setMinExecutionSeconds( $arrValues['min_execution_seconds'] );
		if( isset( $arrValues['max_execution_seconds'] ) && $boolDirectSet ) $this->set( 'm_fltMaxExecutionSeconds', trim( $arrValues['max_execution_seconds'] ) ); elseif( isset( $arrValues['max_execution_seconds'] ) ) $this->setMaxExecutionSeconds( $arrValues['max_execution_seconds'] );
		if( isset( $arrValues['total_sql_statements'] ) && $boolDirectSet ) $this->set( 'm_intTotalSqlStatements', trim( $arrValues['total_sql_statements'] ) ); elseif( isset( $arrValues['total_sql_statements'] ) ) $this->setTotalSqlStatements( $arrValues['total_sql_statements'] );
		if( isset( $arrValues['slowest_query_seconds'] ) && $boolDirectSet ) $this->set( 'm_fltSlowestQuerySeconds', trim( $arrValues['slowest_query_seconds'] ) ); elseif( isset( $arrValues['slowest_query_seconds'] ) ) $this->setSlowestQuerySeconds( $arrValues['slowest_query_seconds'] );
		if( isset( $arrValues['max_duplicate_sql_count'] ) && $boolDirectSet ) $this->set( 'm_intMaxDuplicateSqlCount', trim( $arrValues['max_duplicate_sql_count'] ) ); elseif( isset( $arrValues['max_duplicate_sql_count'] ) ) $this->setMaxDuplicateSqlCount( $arrValues['max_duplicate_sql_count'] );
		if( isset( $arrValues['max_rows_returned'] ) && $boolDirectSet ) $this->set( 'm_intMaxRowsReturned', trim( $arrValues['max_rows_returned'] ) ); elseif( isset( $arrValues['max_rows_returned'] ) ) $this->setMaxRowsReturned( $arrValues['max_rows_returned'] );
		$this->m_boolInitialized = true;
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setModule( $strModule ) {
		$this->set( 'm_strModule', CStrings::strTrimDef( $strModule, 100, NULL, true ) );
	}

	public function getModule() {
		return $this->m_strModule;
	}

	public function sqlModule() {
		return ( true == isset( $this->m_strModule ) ) ? '\'' . addslashes( $this->m_strModule ) . '\'' : 'NULL';
	}

	public function setAction( $strAction ) {
		$this->set( 'm_strAction', CStrings::strTrimDef( $strAction, 100, NULL, true ) );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function sqlAction() {
		return ( true == isset( $this->m_strAction ) ) ? '\'' . addslashes( $this->m_strAction ) . '\'' : 'NULL';
	}

	public function setDay( $strDay ) {
		$this->set( 'm_strDay', CStrings::strTrimDef( $strDay, -1, NULL, true ) );
	}

	public function getDay() {
		return $this->m_strDay;
	}

	public function sqlDay() {
		return ( true == isset( $this->m_strDay ) ) ? '\'' . $this->m_strDay . '\'' : 'NOW()';
	}

	public function setHitCount( $intHitCount ) {
		$this->set( 'm_intHitCount', CStrings::strToIntDef( $intHitCount, NULL, false ) );
	}

	public function getHitCount() {
		return $this->m_intHitCount;
	}

	public function sqlHitCount() {
		return ( true == isset( $this->m_intHitCount ) ) ? ( string ) $this->m_intHitCount : '0';
	}

	public function setTotalExecutionSeconds( $fltTotalExecutionSeconds ) {
		$this->set( 'm_fltTotalExecutionSeconds', CStrings::strToFloatDef( $fltTotalExecutionSeconds, NULL, false, 0 ) );
	}

	public function getTotalExecutionSeconds() {
		return $this->m_fltTotalExecutionSeconds;
	}

	public function sqlTotalExecutionSeconds() {
		return ( true == isset( $this->m_fltTotalExecutionSeconds ) ) ? ( string ) $this->m_fltTotalExecutionSeconds : '0';
	}

	public function setMinExecutionSeconds( $fltMinExecutionSeconds ) {
		$this->set( 'm_fltMinExecutionSeconds', CStrings::strToFloatDef( $fltMinExecutionSeconds, NULL, false, 0 ) );
	}

	public function getMinExecutionSeconds() {
		return $this->m_fltMinExecutionSeconds;
	}

	public function sqlMinExecutionSeconds() {
		return ( true == isset( $this->m_fltMinExecutionSeconds ) ) ? ( string ) $this->m_fltMinExecutionSeconds : '0';
	}

	public function setMaxExecutionSeconds( $fltMaxExecutionSeconds ) {
		$this->set( 'm_fltMaxExecutionSeconds', CStrings::strToFloatDef( $fltMaxExecutionSeconds, NULL, false, 0 ) );
	}

	public function getMaxExecutionSeconds() {
		return $this->m_fltMaxExecutionSeconds;
	}

	public function sqlMaxExecutionSeconds() {
		return ( true == isset( $this->m_fltMaxExecutionSeconds ) ) ? ( string ) $this->m_fltMaxExecutionSeconds : '0';
	}

	public function setTotalSqlStatements( $intTotalSqlStatements ) {
		$this->set( 'm_intTotalSqlStatements', CStrings::strToIntDef( $intTotalSqlStatements, NULL, false ) );
	}

	public function getTotalSqlStatements() {
		return $this->m_intTotalSqlStatements;
	}

	public function sqlTotalSqlStatements() {
		return ( true == isset( $this->m_intTotalSqlStatements ) ) ? ( string ) $this->m_intTotalSqlStatements : '0';
	}

	public function setSlowestQuerySeconds( $fltSlowestQuerySeconds ) {
		$this->set( 'm_fltSlowestQuerySeconds', CStrings::strToFloatDef( $fltSlowestQuerySeconds, NULL, false, 0 ) );
	}

	public function getSlowestQuerySeconds() {
		return $this->m_fltSlowestQuerySeconds;
	}

	public function sqlSlowestQuerySeconds() {
		return ( true == isset( $this->m_fltSlowestQuerySeconds ) ) ? ( string ) $this->m_fltSlowestQuerySeconds : '0';
	}

	public function setMaxDuplicateSqlCount( $intMaxDuplicateSqlCount ) {
		$this->set( 'm_intMaxDuplicateSqlCount', CStrings::strToIntDef( $intMaxDuplicateSqlCount, NULL, false ) );
	}

	public function getMaxDuplicateSqlCount() {
		return $this->m_intMaxDuplicateSqlCount;
	}

	public function sqlMaxDuplicateSqlCount() {
		return ( true == isset( $this->m_intMaxDuplicateSqlCount ) ) ? ( string ) $this->m_intMaxDuplicateSqlCount : '0';
	}

	public function setMaxRowsReturned( $intMaxRowsReturned ) {
		$this->set( 'm_intMaxRowsReturned', CStrings::strToIntDef( $intMaxRowsReturned, NULL, false ) );
	}

	public function getMaxRowsReturned() {
		return $this->m_intMaxRowsReturned;
	}

	public function sqlMaxRowsReturned() {
		return ( true == isset( $this->m_intMaxRowsReturned ) ) ? ( string ) $this->m_intMaxRowsReturned : '0';
	}

	public function toArray() {
		return array(
			'ps_product_id' => $this->getPsProductId(),
			'module' => $this->getModule(),
			'action' => $this->getAction(),
			'day' => $this->getDay(),
			'hit_count' => $this->getHitCount(),
			'total_execution_seconds' => $this->getTotalExecutionSeconds(),
			'min_execution_seconds' => $this->getMinExecutionSeconds(),
			'max_execution_seconds' => $this->getMaxExecutionSeconds(),
			'total_sql_statements' => $this->getTotalSqlStatements(),
			'slowest_query_seconds' => $this->getSlowestQuerySeconds(),
			'max_duplicate_sql_count' => $this->getMaxDuplicateSqlCount(),
			'max_rows_returned' => $this->getMaxRowsReturned()
		);
	}

}
?>