<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSystemErrorTypes
 * Do not add any new functions to this class.
 */

class CBaseSystemErrorTypes extends CEosPluralBase {

	/**
	 * @return CSystemErrorType[]
	 */
	public static function fetchSystemErrorTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSystemErrorType', $objDatabase );
	}

	/**
	 * @return CSystemErrorType
	 */
	public static function fetchSystemErrorType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSystemErrorType', $objDatabase );
	}

	public static function fetchSystemErrorTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_error_types', $objDatabase );
	}

	public static function fetchSystemErrorTypeById( $intId, $objDatabase ) {
		return self::fetchSystemErrorType( sprintf( 'SELECT * FROM system_error_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>