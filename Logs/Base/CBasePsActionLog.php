<?php

class CBasePsActionLog extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_action_logs';

	protected $m_intCid;
	protected $m_intWebsiteId;
	protected $m_intPsProductId;
	protected $m_strModuleName;
	protected $m_strActionName;
	protected $m_strClickDate;
	protected $m_intClickStarts;
	protected $m_intClickCompletes;
	protected $m_intTotalDuplicateCount;
	protected $m_intTotalSqlCount;
	protected $m_fltTotalSqlTime;
	protected $m_fltTotalPhpTime;
	protected $m_fltTotalRunTime;
	protected $m_intIsAdministrator;

	public function __construct() {
		parent::__construct();

		$this->m_intClickStarts = '0';
		$this->m_intClickCompletes = '0';
		$this->m_intTotalDuplicateCount = '0';
		$this->m_intTotalSqlCount = '0';
		$this->m_fltTotalSqlTime = '0';
		$this->m_fltTotalPhpTime = '0';
		$this->m_fltTotalRunTime = '0';
		$this->m_intIsAdministrator = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['module_name'] ) && $boolDirectSet ) $this->set( 'm_strModuleName', trim( stripcslashes( $arrValues['module_name'] ) ) ); elseif( isset( $arrValues['module_name'] ) ) $this->setModuleName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['module_name'] ) : $arrValues['module_name'] );
		if( isset( $arrValues['action_name'] ) && $boolDirectSet ) $this->set( 'm_strActionName', trim( stripcslashes( $arrValues['action_name'] ) ) ); elseif( isset( $arrValues['action_name'] ) ) $this->setActionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action_name'] ) : $arrValues['action_name'] );
		if( isset( $arrValues['click_date'] ) && $boolDirectSet ) $this->set( 'm_strClickDate', trim( $arrValues['click_date'] ) ); elseif( isset( $arrValues['click_date'] ) ) $this->setClickDate( $arrValues['click_date'] );
		if( isset( $arrValues['click_starts'] ) && $boolDirectSet ) $this->set( 'm_intClickStarts', trim( $arrValues['click_starts'] ) ); elseif( isset( $arrValues['click_starts'] ) ) $this->setClickStarts( $arrValues['click_starts'] );
		if( isset( $arrValues['click_completes'] ) && $boolDirectSet ) $this->set( 'm_intClickCompletes', trim( $arrValues['click_completes'] ) ); elseif( isset( $arrValues['click_completes'] ) ) $this->setClickCompletes( $arrValues['click_completes'] );
		if( isset( $arrValues['total_duplicate_count'] ) && $boolDirectSet ) $this->set( 'm_intTotalDuplicateCount', trim( $arrValues['total_duplicate_count'] ) ); elseif( isset( $arrValues['total_duplicate_count'] ) ) $this->setTotalDuplicateCount( $arrValues['total_duplicate_count'] );
		if( isset( $arrValues['total_sql_count'] ) && $boolDirectSet ) $this->set( 'm_intTotalSqlCount', trim( $arrValues['total_sql_count'] ) ); elseif( isset( $arrValues['total_sql_count'] ) ) $this->setTotalSqlCount( $arrValues['total_sql_count'] );
		if( isset( $arrValues['total_sql_time'] ) && $boolDirectSet ) $this->set( 'm_fltTotalSqlTime', trim( $arrValues['total_sql_time'] ) ); elseif( isset( $arrValues['total_sql_time'] ) ) $this->setTotalSqlTime( $arrValues['total_sql_time'] );
		if( isset( $arrValues['total_php_time'] ) && $boolDirectSet ) $this->set( 'm_fltTotalPhpTime', trim( $arrValues['total_php_time'] ) ); elseif( isset( $arrValues['total_php_time'] ) ) $this->setTotalPhpTime( $arrValues['total_php_time'] );
		if( isset( $arrValues['total_run_time'] ) && $boolDirectSet ) $this->set( 'm_fltTotalRunTime', trim( $arrValues['total_run_time'] ) ); elseif( isset( $arrValues['total_run_time'] ) ) $this->setTotalRunTime( $arrValues['total_run_time'] );
		if( isset( $arrValues['is_administrator'] ) && $boolDirectSet ) $this->set( 'm_intIsAdministrator', trim( $arrValues['is_administrator'] ) ); elseif( isset( $arrValues['is_administrator'] ) ) $this->setIsAdministrator( $arrValues['is_administrator'] );
		$this->m_boolInitialized = true;
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setModuleName( $strModuleName ) {
		$this->set( 'm_strModuleName', CStrings::strTrimDef( $strModuleName, 250, NULL, true ) );
	}

	public function getModuleName() {
		return $this->m_strModuleName;
	}

	public function sqlModuleName() {
		return ( true == isset( $this->m_strModuleName ) ) ? '\'' . addslashes( $this->m_strModuleName ) . '\'' : 'NULL';
	}

	public function setActionName( $strActionName ) {
		$this->set( 'm_strActionName', CStrings::strTrimDef( $strActionName, 250, NULL, true ) );
	}

	public function getActionName() {
		return $this->m_strActionName;
	}

	public function sqlActionName() {
		return ( true == isset( $this->m_strActionName ) ) ? '\'' . addslashes( $this->m_strActionName ) . '\'' : 'NULL';
	}

	public function setClickDate( $strClickDate ) {
		$this->set( 'm_strClickDate', CStrings::strTrimDef( $strClickDate, -1, NULL, true ) );
	}

	public function getClickDate() {
		return $this->m_strClickDate;
	}

	public function sqlClickDate() {
		return ( true == isset( $this->m_strClickDate ) ) ? '\'' . $this->m_strClickDate . '\'' : 'NOW()';
	}

	public function setClickStarts( $intClickStarts ) {
		$this->set( 'm_intClickStarts', CStrings::strToIntDef( $intClickStarts, NULL, false ) );
	}

	public function getClickStarts() {
		return $this->m_intClickStarts;
	}

	public function sqlClickStarts() {
		return ( true == isset( $this->m_intClickStarts ) ) ? ( string ) $this->m_intClickStarts : '0';
	}

	public function setClickCompletes( $intClickCompletes ) {
		$this->set( 'm_intClickCompletes', CStrings::strToIntDef( $intClickCompletes, NULL, false ) );
	}

	public function getClickCompletes() {
		return $this->m_intClickCompletes;
	}

	public function sqlClickCompletes() {
		return ( true == isset( $this->m_intClickCompletes ) ) ? ( string ) $this->m_intClickCompletes : '0';
	}

	public function setTotalDuplicateCount( $intTotalDuplicateCount ) {
		$this->set( 'm_intTotalDuplicateCount', CStrings::strToIntDef( $intTotalDuplicateCount, NULL, false ) );
	}

	public function getTotalDuplicateCount() {
		return $this->m_intTotalDuplicateCount;
	}

	public function sqlTotalDuplicateCount() {
		return ( true == isset( $this->m_intTotalDuplicateCount ) ) ? ( string ) $this->m_intTotalDuplicateCount : '0';
	}

	public function setTotalSqlCount( $intTotalSqlCount ) {
		$this->set( 'm_intTotalSqlCount', CStrings::strToIntDef( $intTotalSqlCount, NULL, false ) );
	}

	public function getTotalSqlCount() {
		return $this->m_intTotalSqlCount;
	}

	public function sqlTotalSqlCount() {
		return ( true == isset( $this->m_intTotalSqlCount ) ) ? ( string ) $this->m_intTotalSqlCount : '0';
	}

	public function setTotalSqlTime( $fltTotalSqlTime ) {
		$this->set( 'm_fltTotalSqlTime', CStrings::strToFloatDef( $fltTotalSqlTime, NULL, false, 4 ) );
	}

	public function getTotalSqlTime() {
		return $this->m_fltTotalSqlTime;
	}

	public function sqlTotalSqlTime() {
		return ( true == isset( $this->m_fltTotalSqlTime ) ) ? ( string ) $this->m_fltTotalSqlTime : '0';
	}

	public function setTotalPhpTime( $fltTotalPhpTime ) {
		$this->set( 'm_fltTotalPhpTime', CStrings::strToFloatDef( $fltTotalPhpTime, NULL, false, 4 ) );
	}

	public function getTotalPhpTime() {
		return $this->m_fltTotalPhpTime;
	}

	public function sqlTotalPhpTime() {
		return ( true == isset( $this->m_fltTotalPhpTime ) ) ? ( string ) $this->m_fltTotalPhpTime : '0';
	}

	public function setTotalRunTime( $fltTotalRunTime ) {
		$this->set( 'm_fltTotalRunTime', CStrings::strToFloatDef( $fltTotalRunTime, NULL, false, 4 ) );
	}

	public function getTotalRunTime() {
		return $this->m_fltTotalRunTime;
	}

	public function sqlTotalRunTime() {
		return ( true == isset( $this->m_fltTotalRunTime ) ) ? ( string ) $this->m_fltTotalRunTime : '0';
	}

	public function setIsAdministrator( $intIsAdministrator ) {
		$this->set( 'm_intIsAdministrator', CStrings::strToIntDef( $intIsAdministrator, NULL, false ) );
	}

	public function getIsAdministrator() {
		return $this->m_intIsAdministrator;
	}

	public function sqlIsAdministrator() {
		return ( true == isset( $this->m_intIsAdministrator ) ) ? ( string ) $this->m_intIsAdministrator : '0';
	}

	public function toArray() {
		return array(
			'cid' => $this->getCid(),
			'website_id' => $this->getWebsiteId(),
			'ps_product_id' => $this->getPsProductId(),
			'module_name' => $this->getModuleName(),
			'action_name' => $this->getActionName(),
			'click_date' => $this->getClickDate(),
			'click_starts' => $this->getClickStarts(),
			'click_completes' => $this->getClickCompletes(),
			'total_duplicate_count' => $this->getTotalDuplicateCount(),
			'total_sql_count' => $this->getTotalSqlCount(),
			'total_sql_time' => $this->getTotalSqlTime(),
			'total_php_time' => $this->getTotalPhpTime(),
			'total_run_time' => $this->getTotalRunTime(),
			'is_administrator' => $this->getIsAdministrator()
		);
	}

}
?>