<?php

class CBaseLeaseExecutionLog extends CEosSingularBase {

	const TABLE_NAME = 'public.lease_execution_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApplicationId;
	protected $m_intLeaseId;
	protected $m_intLeaseDocumentId;
	protected $m_intLeaseVersion;
	protected $m_strRequestedIpAddress;
	protected $m_strTargetIpAddress;
	protected $m_intTotalDocumentCount;
	protected $m_strErrorDescription;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intTotalDocumentCount = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['lease_document_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseDocumentId', trim( $arrValues['lease_document_id'] ) ); elseif( isset( $arrValues['lease_document_id'] ) ) $this->setLeaseDocumentId( $arrValues['lease_document_id'] );
		if( isset( $arrValues['lease_version'] ) && $boolDirectSet ) $this->set( 'm_intLeaseVersion', trim( $arrValues['lease_version'] ) ); elseif( isset( $arrValues['lease_version'] ) ) $this->setLeaseVersion( $arrValues['lease_version'] );
		if( isset( $arrValues['requested_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strRequestedIpAddress', trim( stripcslashes( $arrValues['requested_ip_address'] ) ) ); elseif( isset( $arrValues['requested_ip_address'] ) ) $this->setRequestedIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['requested_ip_address'] ) : $arrValues['requested_ip_address'] );
		if( isset( $arrValues['target_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strTargetIpAddress', trim( stripcslashes( $arrValues['target_ip_address'] ) ) ); elseif( isset( $arrValues['target_ip_address'] ) ) $this->setTargetIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['target_ip_address'] ) : $arrValues['target_ip_address'] );
		if( isset( $arrValues['total_document_count'] ) && $boolDirectSet ) $this->set( 'm_intTotalDocumentCount', trim( $arrValues['total_document_count'] ) ); elseif( isset( $arrValues['total_document_count'] ) ) $this->setTotalDocumentCount( $arrValues['total_document_count'] );
		if( isset( $arrValues['error_description'] ) && $boolDirectSet ) $this->set( 'm_strErrorDescription', trim( stripcslashes( $arrValues['error_description'] ) ) ); elseif( isset( $arrValues['error_description'] ) ) $this->setErrorDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['error_description'] ) : $arrValues['error_description'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setLeaseDocumentId( $intLeaseDocumentId ) {
		$this->set( 'm_intLeaseDocumentId', CStrings::strToIntDef( $intLeaseDocumentId, NULL, false ) );
	}

	public function getLeaseDocumentId() {
		return $this->m_intLeaseDocumentId;
	}

	public function sqlLeaseDocumentId() {
		return ( true == isset( $this->m_intLeaseDocumentId ) ) ? ( string ) $this->m_intLeaseDocumentId : 'NULL';
	}

	public function setLeaseVersion( $intLeaseVersion ) {
		$this->set( 'm_intLeaseVersion', CStrings::strToIntDef( $intLeaseVersion, NULL, false ) );
	}

	public function getLeaseVersion() {
		return $this->m_intLeaseVersion;
	}

	public function sqlLeaseVersion() {
		return ( true == isset( $this->m_intLeaseVersion ) ) ? ( string ) $this->m_intLeaseVersion : 'NULL';
	}

	public function setRequestedIpAddress( $strRequestedIpAddress ) {
		$this->set( 'm_strRequestedIpAddress', CStrings::strTrimDef( $strRequestedIpAddress, 15, NULL, true ) );
	}

	public function getRequestedIpAddress() {
		return $this->m_strRequestedIpAddress;
	}

	public function sqlRequestedIpAddress() {
		return ( true == isset( $this->m_strRequestedIpAddress ) ) ? '\'' . addslashes( $this->m_strRequestedIpAddress ) . '\'' : 'NULL';
	}

	public function setTargetIpAddress( $strTargetIpAddress ) {
		$this->set( 'm_strTargetIpAddress', CStrings::strTrimDef( $strTargetIpAddress, 15, NULL, true ) );
	}

	public function getTargetIpAddress() {
		return $this->m_strTargetIpAddress;
	}

	public function sqlTargetIpAddress() {
		return ( true == isset( $this->m_strTargetIpAddress ) ) ? '\'' . addslashes( $this->m_strTargetIpAddress ) . '\'' : 'NULL';
	}

	public function setTotalDocumentCount( $intTotalDocumentCount ) {
		$this->set( 'm_intTotalDocumentCount', CStrings::strToIntDef( $intTotalDocumentCount, NULL, false ) );
	}

	public function getTotalDocumentCount() {
		return $this->m_intTotalDocumentCount;
	}

	public function sqlTotalDocumentCount() {
		return ( true == isset( $this->m_intTotalDocumentCount ) ) ? ( string ) $this->m_intTotalDocumentCount : '1';
	}

	public function setErrorDescription( $strErrorDescription ) {
		$this->set( 'm_strErrorDescription', CStrings::strTrimDef( $strErrorDescription, 50, NULL, true ) );
	}

	public function getErrorDescription() {
		return $this->m_strErrorDescription;
	}

	public function sqlErrorDescription() {
		return ( true == isset( $this->m_strErrorDescription ) ) ? '\'' . addslashes( $this->m_strErrorDescription ) . '\'' : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, application_id, lease_id, lease_document_id, lease_version, requested_ip_address, target_ip_address, total_document_count, error_description, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApplicationId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlLeaseDocumentId() . ', ' .
 						$this->sqlLeaseVersion() . ', ' .
 						$this->sqlRequestedIpAddress() . ', ' .
 						$this->sqlTargetIpAddress() . ', ' .
 						$this->sqlTotalDocumentCount() . ', ' .
 						$this->sqlErrorDescription() . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_document_id = ' . $this->sqlLeaseDocumentId() . ','; } elseif( true == array_key_exists( 'LeaseDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' lease_document_id = ' . $this->sqlLeaseDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_version = ' . $this->sqlLeaseVersion() . ','; } elseif( true == array_key_exists( 'LeaseVersion', $this->getChangedColumns() ) ) { $strSql .= ' lease_version = ' . $this->sqlLeaseVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_ip_address = ' . $this->sqlRequestedIpAddress() . ','; } elseif( true == array_key_exists( 'RequestedIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' requested_ip_address = ' . $this->sqlRequestedIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' target_ip_address = ' . $this->sqlTargetIpAddress() . ','; } elseif( true == array_key_exists( 'TargetIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' target_ip_address = ' . $this->sqlTargetIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_document_count = ' . $this->sqlTotalDocumentCount() . ','; } elseif( true == array_key_exists( 'TotalDocumentCount', $this->getChangedColumns() ) ) { $strSql .= ' total_document_count = ' . $this->sqlTotalDocumentCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_description = ' . $this->sqlErrorDescription() . ','; } elseif( true == array_key_exists( 'ErrorDescription', $this->getChangedColumns() ) ) { $strSql .= ' error_description = ' . $this->sqlErrorDescription() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'application_id' => $this->getApplicationId(),
			'lease_id' => $this->getLeaseId(),
			'lease_document_id' => $this->getLeaseDocumentId(),
			'lease_version' => $this->getLeaseVersion(),
			'requested_ip_address' => $this->getRequestedIpAddress(),
			'target_ip_address' => $this->getTargetIpAddress(),
			'total_document_count' => $this->getTotalDocumentCount(),
			'error_description' => $this->getErrorDescription(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>