<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSvnCommitLogFiles
 * Do not add any new functions to this class.
 */

class CBaseSvnCommitLogFiles extends CEosPluralBase {

	/**
	 * @return CSvnCommitLogFile[]
	 */
	public static function fetchSvnCommitLogFiles( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSvnCommitLogFile', $objDatabase );
	}

	/**
	 * @return CSvnCommitLogFile
	 */
	public static function fetchSvnCommitLogFile( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSvnCommitLogFile', $objDatabase );
	}

	public static function fetchSvnCommitLogFileCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'svn_commit_log_files', $objDatabase );
	}

	public static function fetchSvnCommitLogFileById( $intId, $objDatabase ) {
		return self::fetchSvnCommitLogFile( sprintf( 'SELECT * FROM svn_commit_log_files WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSvnCommitLogFilesBySvnCommitLogId( $intSvnCommitLogId, $objDatabase ) {
		return self::fetchSvnCommitLogFiles( sprintf( 'SELECT * FROM svn_commit_log_files WHERE svn_commit_log_id = %d', ( int ) $intSvnCommitLogId ), $objDatabase );
	}

}
?>