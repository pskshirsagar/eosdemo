<?php

class CBaseApiRequestLog extends CEosSingularBase {

	const TABLE_NAME = 'public.api_request_logs';

	protected $m_intId;
	protected $m_intClusterId;
	protected $m_intDatabaseId;
	protected $m_intCid;
	protected $m_intServiceId;
	protected $m_strMinorApiVersion;
	protected $m_strMajorApiVersion;
	protected $m_intServiceConsumerId;
	protected $m_intServiceConsumerReferenceId;
	protected $m_intServiceAuthenticationTypeId;
	protected $m_arrintPropertyIds;
	protected $m_intIntegrationResultId;
	protected $m_strServiceConsumerReferenceNumber;
	protected $m_strUserName;
	protected $m_strEntrataReportName;
	protected $m_strServiceToken;
	protected $m_strServiceUrl;
	protected $m_strContentType;
	protected $m_strResponseCode;
	protected $m_strErrorDescription;
	protected $m_strUserAgent;
	protected $m_strRequestId;
	protected $m_strRequestForwardedTo;
	protected $m_boolIsInternal;
	protected $m_strIpAddress;
	protected $m_strAccessedOn;
	protected $m_strCompletedOn;
	protected $m_fltTimeConsumed;
	protected $m_fltMemoryConsumed;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strAuthenticationMethod;
	protected $m_strReportData;
	protected $m_jsonReportData;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['database_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseId', trim( $arrValues['database_id'] ) ); elseif( isset( $arrValues['database_id'] ) ) $this->setDatabaseId( $arrValues['database_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['service_id'] ) && $boolDirectSet ) $this->set( 'm_intServiceId', trim( $arrValues['service_id'] ) ); elseif( isset( $arrValues['service_id'] ) ) $this->setServiceId( $arrValues['service_id'] );
		if( isset( $arrValues['minor_api_version'] ) && $boolDirectSet ) $this->set( 'm_strMinorApiVersion', trim( $arrValues['minor_api_version'] ) ); elseif( isset( $arrValues['minor_api_version'] ) ) $this->setMinorApiVersion( $arrValues['minor_api_version'] );
		if( isset( $arrValues['major_api_version'] ) && $boolDirectSet ) $this->set( 'm_strMajorApiVersion', trim( $arrValues['major_api_version'] ) ); elseif( isset( $arrValues['major_api_version'] ) ) $this->setMajorApiVersion( $arrValues['major_api_version'] );
		if( isset( $arrValues['service_consumer_id'] ) && $boolDirectSet ) $this->set( 'm_intServiceConsumerId', trim( $arrValues['service_consumer_id'] ) ); elseif( isset( $arrValues['service_consumer_id'] ) ) $this->setServiceConsumerId( $arrValues['service_consumer_id'] );
		if( isset( $arrValues['service_consumer_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intServiceConsumerReferenceId', trim( $arrValues['service_consumer_reference_id'] ) ); elseif( isset( $arrValues['service_consumer_reference_id'] ) ) $this->setServiceConsumerReferenceId( $arrValues['service_consumer_reference_id'] );
		if( isset( $arrValues['service_authentication_type_id'] ) && $boolDirectSet ) $this->set( 'm_intServiceAuthenticationTypeId', trim( $arrValues['service_authentication_type_id'] ) ); elseif( isset( $arrValues['service_authentication_type_id'] ) ) $this->setServiceAuthenticationTypeId( $arrValues['service_authentication_type_id'] );
		if( isset( $arrValues['property_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintPropertyIds', trim( $arrValues['property_ids'] ) ); elseif( isset( $arrValues['property_ids'] ) ) $this->setPropertyIds( $arrValues['property_ids'] );
		if( isset( $arrValues['integration_result_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationResultId', trim( $arrValues['integration_result_id'] ) ); elseif( isset( $arrValues['integration_result_id'] ) ) $this->setIntegrationResultId( $arrValues['integration_result_id'] );
		if( isset( $arrValues['service_consumer_reference_number'] ) && $boolDirectSet ) $this->set( 'm_strServiceConsumerReferenceNumber', trim( $arrValues['service_consumer_reference_number'] ) ); elseif( isset( $arrValues['service_consumer_reference_number'] ) ) $this->setServiceConsumerReferenceNumber( $arrValues['service_consumer_reference_number'] );
		if( isset( $arrValues['user_name'] ) && $boolDirectSet ) $this->set( 'm_strUserName', trim( $arrValues['user_name'] ) ); elseif( isset( $arrValues['user_name'] ) ) $this->setUserName( $arrValues['user_name'] );
		if( isset( $arrValues['entrata_report_name'] ) && $boolDirectSet ) $this->set( 'm_strEntrataReportName', trim( $arrValues['entrata_report_name'] ) ); elseif( isset( $arrValues['entrata_report_name'] ) ) $this->setEntrataReportName( $arrValues['entrata_report_name'] );
		if( isset( $arrValues['service_token'] ) && $boolDirectSet ) $this->set( 'm_strServiceToken', trim( $arrValues['service_token'] ) ); elseif( isset( $arrValues['service_token'] ) ) $this->setServiceToken( $arrValues['service_token'] );
		if( isset( $arrValues['service_url'] ) && $boolDirectSet ) $this->set( 'm_strServiceUrl', trim( $arrValues['service_url'] ) ); elseif( isset( $arrValues['service_url'] ) ) $this->setServiceUrl( $arrValues['service_url'] );
		if( isset( $arrValues['content_type'] ) && $boolDirectSet ) $this->set( 'm_strContentType', trim( $arrValues['content_type'] ) ); elseif( isset( $arrValues['content_type'] ) ) $this->setContentType( $arrValues['content_type'] );
		if( isset( $arrValues['response_code'] ) && $boolDirectSet ) $this->set( 'm_strResponseCode', trim( $arrValues['response_code'] ) ); elseif( isset( $arrValues['response_code'] ) ) $this->setResponseCode( $arrValues['response_code'] );
		if( isset( $arrValues['error_description'] ) && $boolDirectSet ) $this->set( 'm_strErrorDescription', trim( $arrValues['error_description'] ) ); elseif( isset( $arrValues['error_description'] ) ) $this->setErrorDescription( $arrValues['error_description'] );
		if( isset( $arrValues['user_agent'] ) && $boolDirectSet ) $this->set( 'm_strUserAgent', trim( $arrValues['user_agent'] ) ); elseif( isset( $arrValues['user_agent'] ) ) $this->setUserAgent( $arrValues['user_agent'] );
		if( isset( $arrValues['request_id'] ) && $boolDirectSet ) $this->set( 'm_strRequestId', trim( $arrValues['request_id'] ) ); elseif( isset( $arrValues['request_id'] ) ) $this->setRequestId( $arrValues['request_id'] );
		if( isset( $arrValues['request_forwarded_to'] ) && $boolDirectSet ) $this->set( 'm_strRequestForwardedTo', trim( $arrValues['request_forwarded_to'] ) ); elseif( isset( $arrValues['request_forwarded_to'] ) ) $this->setRequestForwardedTo( $arrValues['request_forwarded_to'] );
		if( isset( $arrValues['is_internal'] ) && $boolDirectSet ) $this->set( 'm_boolIsInternal', trim( stripcslashes( $arrValues['is_internal'] ) ) ); elseif( isset( $arrValues['is_internal'] ) ) $this->setIsInternal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_internal'] ) : $arrValues['is_internal'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( $arrValues['ip_address'] ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( $arrValues['ip_address'] );
		if( isset( $arrValues['accessed_on'] ) && $boolDirectSet ) $this->set( 'm_strAccessedOn', trim( $arrValues['accessed_on'] ) ); elseif( isset( $arrValues['accessed_on'] ) ) $this->setAccessedOn( $arrValues['accessed_on'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['time_consumed'] ) && $boolDirectSet ) $this->set( 'm_fltTimeConsumed', trim( $arrValues['time_consumed'] ) ); elseif( isset( $arrValues['time_consumed'] ) ) $this->setTimeConsumed( $arrValues['time_consumed'] );
		if( isset( $arrValues['memory_consumed'] ) && $boolDirectSet ) $this->set( 'm_fltMemoryConsumed', trim( $arrValues['memory_consumed'] ) ); elseif( isset( $arrValues['memory_consumed'] ) ) $this->setMemoryConsumed( $arrValues['memory_consumed'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['authentication_method'] ) && $boolDirectSet ) $this->set( 'm_strAuthenticationMethod', trim( $arrValues['authentication_method'] ) ); elseif( isset( $arrValues['authentication_method'] ) ) $this->setAuthenticationMethod( $arrValues['authentication_method'] );
		if( isset( $arrValues['report_data'] ) ) $this->set( 'm_strReportData', trim( $arrValues['report_data'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : 'NULL';
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->set( 'm_intDatabaseId', CStrings::strToIntDef( $intDatabaseId, NULL, false ) );
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function sqlDatabaseId() {
		return ( true == isset( $this->m_intDatabaseId ) ) ? ( string ) $this->m_intDatabaseId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setServiceId( $intServiceId ) {
		$this->set( 'm_intServiceId', CStrings::strToIntDef( $intServiceId, NULL, false ) );
	}

	public function getServiceId() {
		return $this->m_intServiceId;
	}

	public function sqlServiceId() {
		return ( true == isset( $this->m_intServiceId ) ) ? ( string ) $this->m_intServiceId : 'NULL';
	}

	public function setMinorApiVersion( $strMinorApiVersion ) {
		$this->set( 'm_strMinorApiVersion', CStrings::strTrimDef( $strMinorApiVersion, 3, NULL, true ) );
	}

	public function getMinorApiVersion() {
		return $this->m_strMinorApiVersion;
	}

	public function sqlMinorApiVersion() {
		return ( true == isset( $this->m_strMinorApiVersion ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMinorApiVersion ) : '\'' . addslashes( $this->m_strMinorApiVersion ) . '\'' ) : 'NULL';
	}

	public function setMajorApiVersion( $strMajorApiVersion ) {
		$this->set( 'm_strMajorApiVersion', CStrings::strTrimDef( $strMajorApiVersion, 3, NULL, true ) );
	}

	public function getMajorApiVersion() {
		return $this->m_strMajorApiVersion;
	}

	public function sqlMajorApiVersion() {
		return ( true == isset( $this->m_strMajorApiVersion ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMajorApiVersion ) : '\'' . addslashes( $this->m_strMajorApiVersion ) . '\'' ) : 'NULL';
	}

	public function setServiceConsumerId( $intServiceConsumerId ) {
		$this->set( 'm_intServiceConsumerId', CStrings::strToIntDef( $intServiceConsumerId, NULL, false ) );
	}

	public function getServiceConsumerId() {
		return $this->m_intServiceConsumerId;
	}

	public function sqlServiceConsumerId() {
		return ( true == isset( $this->m_intServiceConsumerId ) ) ? ( string ) $this->m_intServiceConsumerId : 'NULL';
	}

	public function setServiceConsumerReferenceId( $intServiceConsumerReferenceId ) {
		$this->set( 'm_intServiceConsumerReferenceId', CStrings::strToIntDef( $intServiceConsumerReferenceId, NULL, false ) );
	}

	public function getServiceConsumerReferenceId() {
		return $this->m_intServiceConsumerReferenceId;
	}

	public function sqlServiceConsumerReferenceId() {
		return ( true == isset( $this->m_intServiceConsumerReferenceId ) ) ? ( string ) $this->m_intServiceConsumerReferenceId : 'NULL';
	}

	public function setServiceAuthenticationTypeId( $intServiceAuthenticationTypeId ) {
		$this->set( 'm_intServiceAuthenticationTypeId', CStrings::strToIntDef( $intServiceAuthenticationTypeId, NULL, false ) );
	}

	public function getServiceAuthenticationTypeId() {
		return $this->m_intServiceAuthenticationTypeId;
	}

	public function sqlServiceAuthenticationTypeId() {
		return ( true == isset( $this->m_intServiceAuthenticationTypeId ) ) ? ( string ) $this->m_intServiceAuthenticationTypeId : 'NULL';
	}

	public function setPropertyIds( $arrintPropertyIds ) {
		$this->set( 'm_arrintPropertyIds', CStrings::strToArrIntDef( $arrintPropertyIds, NULL ) );
	}

	public function getPropertyIds() {
		return $this->m_arrintPropertyIds;
	}

	public function sqlPropertyIds() {
		return ( true == isset( $this->m_arrintPropertyIds ) && true == valArr( $this->m_arrintPropertyIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintPropertyIds, NULL ) . '\'' : 'NULL';
	}

	public function setIntegrationResultId( $intIntegrationResultId ) {
		$this->set( 'm_intIntegrationResultId', CStrings::strToIntDef( $intIntegrationResultId, NULL, false ) );
	}

	public function getIntegrationResultId() {
		return $this->m_intIntegrationResultId;
	}

	public function sqlIntegrationResultId() {
		return ( true == isset( $this->m_intIntegrationResultId ) ) ? ( string ) $this->m_intIntegrationResultId : 'NULL';
	}

	public function setServiceConsumerReferenceNumber( $strServiceConsumerReferenceNumber ) {
		$this->set( 'm_strServiceConsumerReferenceNumber', CStrings::strTrimDef( $strServiceConsumerReferenceNumber, 30, NULL, true ) );
	}

	public function getServiceConsumerReferenceNumber() {
		return $this->m_strServiceConsumerReferenceNumber;
	}

	public function sqlServiceConsumerReferenceNumber() {
		return ( true == isset( $this->m_strServiceConsumerReferenceNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServiceConsumerReferenceNumber ) : '\'' . addslashes( $this->m_strServiceConsumerReferenceNumber ) . '\'' ) : 'NULL';
	}

	public function setUserName( $strUserName ) {
		$this->set( 'm_strUserName', CStrings::strTrimDef( $strUserName, 100, NULL, true ) );
	}

	public function getUserName() {
		return $this->m_strUserName;
	}

	public function sqlUserName() {
		return ( true == isset( $this->m_strUserName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUserName ) : '\'' . addslashes( $this->m_strUserName ) . '\'' ) : 'NULL';
	}

	public function setEntrataReportName( $strEntrataReportName ) {
		$this->set( 'm_strEntrataReportName', CStrings::strTrimDef( $strEntrataReportName, 100, NULL, true ) );
	}

	public function getEntrataReportName() {
		return $this->m_strEntrataReportName;
	}

	public function sqlEntrataReportName() {
		return ( true == isset( $this->m_strEntrataReportName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEntrataReportName ) : '\'' . addslashes( $this->m_strEntrataReportName ) . '\'' ) : 'NULL';
	}

	public function setServiceToken( $strServiceToken ) {
		$this->set( 'm_strServiceToken', CStrings::strTrimDef( $strServiceToken, 275, NULL, true ) );
	}

	public function getServiceToken() {
		return $this->m_strServiceToken;
	}

	public function sqlServiceToken() {
		return ( true == isset( $this->m_strServiceToken ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServiceToken ) : '\'' . addslashes( $this->m_strServiceToken ) . '\'' ) : 'NULL';
	}

	public function setServiceUrl( $strServiceUrl ) {
		$this->set( 'm_strServiceUrl', CStrings::strTrimDef( $strServiceUrl, 255, NULL, true ) );
	}

	public function getServiceUrl() {
		return $this->m_strServiceUrl;
	}

	public function sqlServiceUrl() {
		return ( true == isset( $this->m_strServiceUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServiceUrl ) : '\'' . addslashes( $this->m_strServiceUrl ) . '\'' ) : 'NULL';
	}

	public function setContentType( $strContentType ) {
		$this->set( 'm_strContentType', CStrings::strTrimDef( $strContentType, 100, NULL, true ) );
	}

	public function getContentType() {
		return $this->m_strContentType;
	}

	public function sqlContentType() {
		return ( true == isset( $this->m_strContentType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strContentType ) : '\'' . addslashes( $this->m_strContentType ) . '\'' ) : 'NULL';
	}

	public function setResponseCode( $strResponseCode ) {
		$this->set( 'm_strResponseCode', CStrings::strTrimDef( $strResponseCode, 10, NULL, true ) );
	}

	public function getResponseCode() {
		return $this->m_strResponseCode;
	}

	public function sqlResponseCode() {
		return ( true == isset( $this->m_strResponseCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResponseCode ) : '\'' . addslashes( $this->m_strResponseCode ) . '\'' ) : 'NULL';
	}

	public function setErrorDescription( $strErrorDescription ) {
		$this->set( 'm_strErrorDescription', CStrings::strTrimDef( $strErrorDescription, 255, NULL, true ) );
	}

	public function getErrorDescription() {
		return $this->m_strErrorDescription;
	}

	public function sqlErrorDescription() {
		return ( true == isset( $this->m_strErrorDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strErrorDescription ) : '\'' . addslashes( $this->m_strErrorDescription ) . '\'' ) : 'NULL';
	}

	public function setUserAgent( $strUserAgent ) {
		$this->set( 'm_strUserAgent', CStrings::strTrimDef( $strUserAgent, 255, NULL, true ) );
	}

	public function getUserAgent() {
		return $this->m_strUserAgent;
	}

	public function sqlUserAgent() {
		return ( true == isset( $this->m_strUserAgent ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUserAgent ) : '\'' . addslashes( $this->m_strUserAgent ) . '\'' ) : 'NULL';
	}

	public function setRequestId( $strRequestId ) {
		$this->set( 'm_strRequestId', CStrings::strTrimDef( $strRequestId, 50, NULL, true ) );
	}

	public function getRequestId() {
		return $this->m_strRequestId;
	}

	public function sqlRequestId() {
		return ( true == isset( $this->m_strRequestId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRequestId ) : '\'' . addslashes( $this->m_strRequestId ) . '\'' ) : 'NULL';
	}

	public function setRequestForwardedTo( $strRequestForwardedTo ) {
		$this->set( 'm_strRequestForwardedTo', CStrings::strTrimDef( $strRequestForwardedTo, 10, NULL, true ) );
	}

	public function getRequestForwardedTo() {
		return $this->m_strRequestForwardedTo;
	}

	public function sqlRequestForwardedTo() {
		return ( true == isset( $this->m_strRequestForwardedTo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRequestForwardedTo ) : '\'' . addslashes( $this->m_strRequestForwardedTo ) . '\'' ) : 'NULL';
	}

	public function setIsInternal( $boolIsInternal ) {
		$this->set( 'm_boolIsInternal', CStrings::strToBool( $boolIsInternal ) );
	}

	public function getIsInternal() {
		return $this->m_boolIsInternal;
	}

	public function sqlIsInternal() {
		return ( true == isset( $this->m_boolIsInternal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInternal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 40, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIpAddress ) : '\'' . addslashes( $this->m_strIpAddress ) . '\'' ) : 'NULL';
	}

	public function setAccessedOn( $strAccessedOn ) {
		$this->set( 'm_strAccessedOn', CStrings::strTrimDef( $strAccessedOn, -1, NULL, true ) );
	}

	public function getAccessedOn() {
		return $this->m_strAccessedOn;
	}

	public function sqlAccessedOn() {
		return ( true == isset( $this->m_strAccessedOn ) ) ? '\'' . $this->m_strAccessedOn . '\'' : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setTimeConsumed( $fltTimeConsumed ) {
		$this->set( 'm_fltTimeConsumed', CStrings::strToFloatDef( $fltTimeConsumed, NULL, false, 4 ) );
	}

	public function getTimeConsumed() {
		return $this->m_fltTimeConsumed;
	}

	public function sqlTimeConsumed() {
		return ( true == isset( $this->m_fltTimeConsumed ) ) ? ( string ) $this->m_fltTimeConsumed : 'NULL';
	}

	public function setMemoryConsumed( $fltMemoryConsumed ) {
		$this->set( 'm_fltMemoryConsumed', CStrings::strToFloatDef( $fltMemoryConsumed, NULL, false, 4 ) );
	}

	public function getMemoryConsumed() {
		return $this->m_fltMemoryConsumed;
	}

	public function sqlMemoryConsumed() {
		return ( true == isset( $this->m_fltMemoryConsumed ) ) ? ( string ) $this->m_fltMemoryConsumed : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAuthenticationMethod( $strAuthenticationMethod ) {
		$this->set( 'm_strAuthenticationMethod', CStrings::strTrimDef( $strAuthenticationMethod, 50, NULL, true ) );
	}

	public function getAuthenticationMethod() {
		return $this->m_strAuthenticationMethod;
	}

	public function sqlAuthenticationMethod() {
		return ( true == isset( $this->m_strAuthenticationMethod ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAuthenticationMethod ) : '\'' . addslashes( $this->m_strAuthenticationMethod ) . '\'' ) : 'NULL';
	}

	public function setReportData( $jsonReportData ) {
		if( true == valObj( $jsonReportData, 'stdClass' ) ) {
			$this->set( 'm_jsonReportData', $jsonReportData );
		} elseif( true == valJsonString( $jsonReportData ) ) {
			$this->set( 'm_jsonReportData', CStrings::strToJson( $jsonReportData ) );
		} else {
			$this->set( 'm_jsonReportData', NULL ); 
		}
		unset( $this->m_strReportData );
	}

	public function getReportData() {
		if( true == isset( $this->m_strReportData ) ) {
			$this->m_jsonReportData = CStrings::strToJson( $this->m_strReportData );
			unset( $this->m_strReportData );
		}
		return $this->m_jsonReportData;
	}

	public function sqlReportData() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getReportData() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getReportData() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getReportData() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cluster_id, database_id, cid, service_id, minor_api_version, major_api_version, service_consumer_id, service_consumer_reference_id, service_authentication_type_id, property_ids, integration_result_id, service_consumer_reference_number, user_name, entrata_report_name, service_token, service_url, content_type, response_code, error_description, user_agent, request_id, request_forwarded_to, is_internal, ip_address, accessed_on, completed_on, time_consumed, memory_consumed, updated_by, updated_on, created_by, created_on, authentication_method, report_data )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlClusterId() . ', ' .
						$this->sqlDatabaseId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlServiceId() . ', ' .
						$this->sqlMinorApiVersion() . ', ' .
						$this->sqlMajorApiVersion() . ', ' .
						$this->sqlServiceConsumerId() . ', ' .
						$this->sqlServiceConsumerReferenceId() . ', ' .
						$this->sqlServiceAuthenticationTypeId() . ', ' .
						$this->sqlPropertyIds() . ', ' .
						$this->sqlIntegrationResultId() . ', ' .
						$this->sqlServiceConsumerReferenceNumber() . ', ' .
						$this->sqlUserName() . ', ' .
						$this->sqlEntrataReportName() . ', ' .
						$this->sqlServiceToken() . ', ' .
						$this->sqlServiceUrl() . ', ' .
						$this->sqlContentType() . ', ' .
						$this->sqlResponseCode() . ', ' .
						$this->sqlErrorDescription() . ', ' .
						$this->sqlUserAgent() . ', ' .
						$this->sqlRequestId() . ', ' .
						$this->sqlRequestForwardedTo() . ', ' .
						$this->sqlIsInternal() . ', ' .
						$this->sqlIpAddress() . ', ' .
						$this->sqlAccessedOn() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						$this->sqlTimeConsumed() . ', ' .
						$this->sqlMemoryConsumed() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlAuthenticationMethod() . ', ' .
						$this->sqlReportData() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId(). ',' ; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId(). ',' ; } elseif( true == array_key_exists( 'DatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_id = ' . $this->sqlServiceId(). ',' ; } elseif( true == array_key_exists( 'ServiceId', $this->getChangedColumns() ) ) { $strSql .= ' service_id = ' . $this->sqlServiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minor_api_version = ' . $this->sqlMinorApiVersion(). ',' ; } elseif( true == array_key_exists( 'MinorApiVersion', $this->getChangedColumns() ) ) { $strSql .= ' minor_api_version = ' . $this->sqlMinorApiVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' major_api_version = ' . $this->sqlMajorApiVersion(). ',' ; } elseif( true == array_key_exists( 'MajorApiVersion', $this->getChangedColumns() ) ) { $strSql .= ' major_api_version = ' . $this->sqlMajorApiVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_consumer_id = ' . $this->sqlServiceConsumerId(). ',' ; } elseif( true == array_key_exists( 'ServiceConsumerId', $this->getChangedColumns() ) ) { $strSql .= ' service_consumer_id = ' . $this->sqlServiceConsumerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_consumer_reference_id = ' . $this->sqlServiceConsumerReferenceId(). ',' ; } elseif( true == array_key_exists( 'ServiceConsumerReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' service_consumer_reference_id = ' . $this->sqlServiceConsumerReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_authentication_type_id = ' . $this->sqlServiceAuthenticationTypeId(). ',' ; } elseif( true == array_key_exists( 'ServiceAuthenticationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' service_authentication_type_id = ' . $this->sqlServiceAuthenticationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_ids = ' . $this->sqlPropertyIds(). ',' ; } elseif( true == array_key_exists( 'PropertyIds', $this->getChangedColumns() ) ) { $strSql .= ' property_ids = ' . $this->sqlPropertyIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_result_id = ' . $this->sqlIntegrationResultId(). ',' ; } elseif( true == array_key_exists( 'IntegrationResultId', $this->getChangedColumns() ) ) { $strSql .= ' integration_result_id = ' . $this->sqlIntegrationResultId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_consumer_reference_number = ' . $this->sqlServiceConsumerReferenceNumber(). ',' ; } elseif( true == array_key_exists( 'ServiceConsumerReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' service_consumer_reference_number = ' . $this->sqlServiceConsumerReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_name = ' . $this->sqlUserName(). ',' ; } elseif( true == array_key_exists( 'UserName', $this->getChangedColumns() ) ) { $strSql .= ' user_name = ' . $this->sqlUserName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_report_name = ' . $this->sqlEntrataReportName(). ',' ; } elseif( true == array_key_exists( 'EntrataReportName', $this->getChangedColumns() ) ) { $strSql .= ' entrata_report_name = ' . $this->sqlEntrataReportName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_token = ' . $this->sqlServiceToken(). ',' ; } elseif( true == array_key_exists( 'ServiceToken', $this->getChangedColumns() ) ) { $strSql .= ' service_token = ' . $this->sqlServiceToken() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_url = ' . $this->sqlServiceUrl(). ',' ; } elseif( true == array_key_exists( 'ServiceUrl', $this->getChangedColumns() ) ) { $strSql .= ' service_url = ' . $this->sqlServiceUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content_type = ' . $this->sqlContentType(). ',' ; } elseif( true == array_key_exists( 'ContentType', $this->getChangedColumns() ) ) { $strSql .= ' content_type = ' . $this->sqlContentType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_code = ' . $this->sqlResponseCode(). ',' ; } elseif( true == array_key_exists( 'ResponseCode', $this->getChangedColumns() ) ) { $strSql .= ' response_code = ' . $this->sqlResponseCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_description = ' . $this->sqlErrorDescription(). ',' ; } elseif( true == array_key_exists( 'ErrorDescription', $this->getChangedColumns() ) ) { $strSql .= ' error_description = ' . $this->sqlErrorDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_agent = ' . $this->sqlUserAgent(). ',' ; } elseif( true == array_key_exists( 'UserAgent', $this->getChangedColumns() ) ) { $strSql .= ' user_agent = ' . $this->sqlUserAgent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_id = ' . $this->sqlRequestId(). ',' ; } elseif( true == array_key_exists( 'RequestId', $this->getChangedColumns() ) ) { $strSql .= ' request_id = ' . $this->sqlRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_forwarded_to = ' . $this->sqlRequestForwardedTo(). ',' ; } elseif( true == array_key_exists( 'RequestForwardedTo', $this->getChangedColumns() ) ) { $strSql .= ' request_forwarded_to = ' . $this->sqlRequestForwardedTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_internal = ' . $this->sqlIsInternal(). ',' ; } elseif( true == array_key_exists( 'IsInternal', $this->getChangedColumns() ) ) { $strSql .= ' is_internal = ' . $this->sqlIsInternal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accessed_on = ' . $this->sqlAccessedOn(). ',' ; } elseif( true == array_key_exists( 'AccessedOn', $this->getChangedColumns() ) ) { $strSql .= ' accessed_on = ' . $this->sqlAccessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_consumed = ' . $this->sqlTimeConsumed(). ',' ; } elseif( true == array_key_exists( 'TimeConsumed', $this->getChangedColumns() ) ) { $strSql .= ' time_consumed = ' . $this->sqlTimeConsumed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memory_consumed = ' . $this->sqlMemoryConsumed(). ',' ; } elseif( true == array_key_exists( 'MemoryConsumed', $this->getChangedColumns() ) ) { $strSql .= ' memory_consumed = ' . $this->sqlMemoryConsumed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' authentication_method = ' . $this->sqlAuthenticationMethod(). ',' ; } elseif( true == array_key_exists( 'AuthenticationMethod', $this->getChangedColumns() ) ) { $strSql .= ' authentication_method = ' . $this->sqlAuthenticationMethod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_data = ' . $this->sqlReportData(). ',' ; } elseif( true == array_key_exists( 'ReportData', $this->getChangedColumns() ) ) { $strSql .= ' report_data = ' . $this->sqlReportData() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cluster_id' => $this->getClusterId(),
			'database_id' => $this->getDatabaseId(),
			'cid' => $this->getCid(),
			'service_id' => $this->getServiceId(),
			'minor_api_version' => $this->getMinorApiVersion(),
			'major_api_version' => $this->getMajorApiVersion(),
			'service_consumer_id' => $this->getServiceConsumerId(),
			'service_consumer_reference_id' => $this->getServiceConsumerReferenceId(),
			'service_authentication_type_id' => $this->getServiceAuthenticationTypeId(),
			'property_ids' => $this->getPropertyIds(),
			'integration_result_id' => $this->getIntegrationResultId(),
			'service_consumer_reference_number' => $this->getServiceConsumerReferenceNumber(),
			'user_name' => $this->getUserName(),
			'entrata_report_name' => $this->getEntrataReportName(),
			'service_token' => $this->getServiceToken(),
			'service_url' => $this->getServiceUrl(),
			'content_type' => $this->getContentType(),
			'response_code' => $this->getResponseCode(),
			'error_description' => $this->getErrorDescription(),
			'user_agent' => $this->getUserAgent(),
			'request_id' => $this->getRequestId(),
			'request_forwarded_to' => $this->getRequestForwardedTo(),
			'is_internal' => $this->getIsInternal(),
			'ip_address' => $this->getIpAddress(),
			'accessed_on' => $this->getAccessedOn(),
			'completed_on' => $this->getCompletedOn(),
			'time_consumed' => $this->getTimeConsumed(),
			'memory_consumed' => $this->getMemoryConsumed(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'authentication_method' => $this->getAuthenticationMethod(),
			'report_data' => $this->getReportData()
		);
	}

}
?>