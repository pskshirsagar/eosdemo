<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CEcsMessageLogs
 * Do not add any new functions to this class.
 */

class CBaseEcsMessageLogs extends CEosPluralBase {

	/**
	 * @return CEcsMessageLog[]
	 */
	public static function fetchEcsMessageLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEcsMessageLog', $objDatabase );
	}

	/**
	 * @return CEcsMessageLog
	 */
	public static function fetchEcsMessageLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEcsMessageLog', $objDatabase );
	}

	public static function fetchEcsMessageLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ecs_message_logs', $objDatabase );
	}

	public static function fetchEcsMessageLogById( $intId, $objDatabase ) {
		return self::fetchEcsMessageLog( sprintf( 'SELECT * FROM ecs_message_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEcsMessageLogsByEcsMessageId( $intEcsMessageId, $objDatabase ) {
		return self::fetchEcsMessageLogs( sprintf( 'SELECT * FROM ecs_message_logs WHERE ecs_message_id = %d', ( int ) $intEcsMessageId ), $objDatabase );
	}

}
?>