<?php

class CBaseDiagnosticChange extends CEosSingularBase {

	const TABLE_NAME = 'public.diagnostic_changes';

	protected $m_intId;
	protected $m_intDiagnosticId;
	protected $m_strChangeDatetime;
	protected $m_strOldRapidViewSql;
	protected $m_strNewRapidViewSql;
	protected $m_strOldRapidRepairSql;
	protected $m_strNewRapidRepairSql;
	protected $m_strOldStandardViewSql;
	protected $m_strNewStandardViewSql;
	protected $m_strOldStandardRepairSql;
	protected $m_strNewStandardRepairSql;
	protected $m_intOldDeletedBy;
	protected $m_intNewDeletedBy;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['diagnostic_id'] ) && $boolDirectSet ) $this->set( 'm_intDiagnosticId', trim( $arrValues['diagnostic_id'] ) ); elseif( isset( $arrValues['diagnostic_id'] ) ) $this->setDiagnosticId( $arrValues['diagnostic_id'] );
		if( isset( $arrValues['change_datetime'] ) && $boolDirectSet ) $this->set( 'm_strChangeDatetime', trim( $arrValues['change_datetime'] ) ); elseif( isset( $arrValues['change_datetime'] ) ) $this->setChangeDatetime( $arrValues['change_datetime'] );
		if( isset( $arrValues['old_rapid_view_sql'] ) && $boolDirectSet ) $this->set( 'm_strOldRapidViewSql', trim( stripcslashes( $arrValues['old_rapid_view_sql'] ) ) ); elseif( isset( $arrValues['old_rapid_view_sql'] ) ) $this->setOldRapidViewSql( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['old_rapid_view_sql'] ) : $arrValues['old_rapid_view_sql'] );
		if( isset( $arrValues['new_rapid_view_sql'] ) && $boolDirectSet ) $this->set( 'm_strNewRapidViewSql', trim( stripcslashes( $arrValues['new_rapid_view_sql'] ) ) ); elseif( isset( $arrValues['new_rapid_view_sql'] ) ) $this->setNewRapidViewSql( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_rapid_view_sql'] ) : $arrValues['new_rapid_view_sql'] );
		if( isset( $arrValues['old_rapid_repair_sql'] ) && $boolDirectSet ) $this->set( 'm_strOldRapidRepairSql', trim( stripcslashes( $arrValues['old_rapid_repair_sql'] ) ) ); elseif( isset( $arrValues['old_rapid_repair_sql'] ) ) $this->setOldRapidRepairSql( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['old_rapid_repair_sql'] ) : $arrValues['old_rapid_repair_sql'] );
		if( isset( $arrValues['new_rapid_repair_sql'] ) && $boolDirectSet ) $this->set( 'm_strNewRapidRepairSql', trim( stripcslashes( $arrValues['new_rapid_repair_sql'] ) ) ); elseif( isset( $arrValues['new_rapid_repair_sql'] ) ) $this->setNewRapidRepairSql( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_rapid_repair_sql'] ) : $arrValues['new_rapid_repair_sql'] );
		if( isset( $arrValues['old_standard_view_sql'] ) && $boolDirectSet ) $this->set( 'm_strOldStandardViewSql', trim( stripcslashes( $arrValues['old_standard_view_sql'] ) ) ); elseif( isset( $arrValues['old_standard_view_sql'] ) ) $this->setOldStandardViewSql( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['old_standard_view_sql'] ) : $arrValues['old_standard_view_sql'] );
		if( isset( $arrValues['new_standard_view_sql'] ) && $boolDirectSet ) $this->set( 'm_strNewStandardViewSql', trim( stripcslashes( $arrValues['new_standard_view_sql'] ) ) ); elseif( isset( $arrValues['new_standard_view_sql'] ) ) $this->setNewStandardViewSql( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_standard_view_sql'] ) : $arrValues['new_standard_view_sql'] );
		if( isset( $arrValues['old_standard_repair_sql'] ) && $boolDirectSet ) $this->set( 'm_strOldStandardRepairSql', trim( stripcslashes( $arrValues['old_standard_repair_sql'] ) ) ); elseif( isset( $arrValues['old_standard_repair_sql'] ) ) $this->setOldStandardRepairSql( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['old_standard_repair_sql'] ) : $arrValues['old_standard_repair_sql'] );
		if( isset( $arrValues['new_standard_repair_sql'] ) && $boolDirectSet ) $this->set( 'm_strNewStandardRepairSql', trim( stripcslashes( $arrValues['new_standard_repair_sql'] ) ) ); elseif( isset( $arrValues['new_standard_repair_sql'] ) ) $this->setNewStandardRepairSql( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_standard_repair_sql'] ) : $arrValues['new_standard_repair_sql'] );
		if( isset( $arrValues['old_deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intOldDeletedBy', trim( $arrValues['old_deleted_by'] ) ); elseif( isset( $arrValues['old_deleted_by'] ) ) $this->setOldDeletedBy( $arrValues['old_deleted_by'] );
		if( isset( $arrValues['new_deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intNewDeletedBy', trim( $arrValues['new_deleted_by'] ) ); elseif( isset( $arrValues['new_deleted_by'] ) ) $this->setNewDeletedBy( $arrValues['new_deleted_by'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDiagnosticId( $intDiagnosticId ) {
		$this->set( 'm_intDiagnosticId', CStrings::strToIntDef( $intDiagnosticId, NULL, false ) );
	}

	public function getDiagnosticId() {
		return $this->m_intDiagnosticId;
	}

	public function sqlDiagnosticId() {
		return ( true == isset( $this->m_intDiagnosticId ) ) ? ( string ) $this->m_intDiagnosticId : 'NULL';
	}

	public function setChangeDatetime( $strChangeDatetime ) {
		$this->set( 'm_strChangeDatetime', CStrings::strTrimDef( $strChangeDatetime, -1, NULL, true ) );
	}

	public function getChangeDatetime() {
		return $this->m_strChangeDatetime;
	}

	public function sqlChangeDatetime() {
		return ( true == isset( $this->m_strChangeDatetime ) ) ? '\'' . $this->m_strChangeDatetime . '\'' : 'NOW()';
	}

	public function setOldRapidViewSql( $strOldRapidViewSql ) {
		$this->set( 'm_strOldRapidViewSql', CStrings::strTrimDef( $strOldRapidViewSql, -1, NULL, true ) );
	}

	public function getOldRapidViewSql() {
		return $this->m_strOldRapidViewSql;
	}

	public function sqlOldRapidViewSql() {
		return ( true == isset( $this->m_strOldRapidViewSql ) ) ? '\'' . addslashes( $this->m_strOldRapidViewSql ) . '\'' : 'NULL';
	}

	public function setNewRapidViewSql( $strNewRapidViewSql ) {
		$this->set( 'm_strNewRapidViewSql', CStrings::strTrimDef( $strNewRapidViewSql, -1, NULL, true ) );
	}

	public function getNewRapidViewSql() {
		return $this->m_strNewRapidViewSql;
	}

	public function sqlNewRapidViewSql() {
		return ( true == isset( $this->m_strNewRapidViewSql ) ) ? '\'' . addslashes( $this->m_strNewRapidViewSql ) . '\'' : 'NULL';
	}

	public function setOldRapidRepairSql( $strOldRapidRepairSql ) {
		$this->set( 'm_strOldRapidRepairSql', CStrings::strTrimDef( $strOldRapidRepairSql, -1, NULL, true ) );
	}

	public function getOldRapidRepairSql() {
		return $this->m_strOldRapidRepairSql;
	}

	public function sqlOldRapidRepairSql() {
		return ( true == isset( $this->m_strOldRapidRepairSql ) ) ? '\'' . addslashes( $this->m_strOldRapidRepairSql ) . '\'' : 'NULL';
	}

	public function setNewRapidRepairSql( $strNewRapidRepairSql ) {
		$this->set( 'm_strNewRapidRepairSql', CStrings::strTrimDef( $strNewRapidRepairSql, -1, NULL, true ) );
	}

	public function getNewRapidRepairSql() {
		return $this->m_strNewRapidRepairSql;
	}

	public function sqlNewRapidRepairSql() {
		return ( true == isset( $this->m_strNewRapidRepairSql ) ) ? '\'' . addslashes( $this->m_strNewRapidRepairSql ) . '\'' : 'NULL';
	}

	public function setOldStandardViewSql( $strOldStandardViewSql ) {
		$this->set( 'm_strOldStandardViewSql', CStrings::strTrimDef( $strOldStandardViewSql, -1, NULL, true ) );
	}

	public function getOldStandardViewSql() {
		return $this->m_strOldStandardViewSql;
	}

	public function sqlOldStandardViewSql() {
		return ( true == isset( $this->m_strOldStandardViewSql ) ) ? '\'' . addslashes( $this->m_strOldStandardViewSql ) . '\'' : 'NULL';
	}

	public function setNewStandardViewSql( $strNewStandardViewSql ) {
		$this->set( 'm_strNewStandardViewSql', CStrings::strTrimDef( $strNewStandardViewSql, -1, NULL, true ) );
	}

	public function getNewStandardViewSql() {
		return $this->m_strNewStandardViewSql;
	}

	public function sqlNewStandardViewSql() {
		return ( true == isset( $this->m_strNewStandardViewSql ) ) ? '\'' . addslashes( $this->m_strNewStandardViewSql ) . '\'' : 'NULL';
	}

	public function setOldStandardRepairSql( $strOldStandardRepairSql ) {
		$this->set( 'm_strOldStandardRepairSql', CStrings::strTrimDef( $strOldStandardRepairSql, -1, NULL, true ) );
	}

	public function getOldStandardRepairSql() {
		return $this->m_strOldStandardRepairSql;
	}

	public function sqlOldStandardRepairSql() {
		return ( true == isset( $this->m_strOldStandardRepairSql ) ) ? '\'' . addslashes( $this->m_strOldStandardRepairSql ) . '\'' : 'NULL';
	}

	public function setNewStandardRepairSql( $strNewStandardRepairSql ) {
		$this->set( 'm_strNewStandardRepairSql', CStrings::strTrimDef( $strNewStandardRepairSql, -1, NULL, true ) );
	}

	public function getNewStandardRepairSql() {
		return $this->m_strNewStandardRepairSql;
	}

	public function sqlNewStandardRepairSql() {
		return ( true == isset( $this->m_strNewStandardRepairSql ) ) ? '\'' . addslashes( $this->m_strNewStandardRepairSql ) . '\'' : 'NULL';
	}

	public function setOldDeletedBy( $intOldDeletedBy ) {
		$this->set( 'm_intOldDeletedBy', CStrings::strToIntDef( $intOldDeletedBy, NULL, false ) );
	}

	public function getOldDeletedBy() {
		return $this->m_intOldDeletedBy;
	}

	public function sqlOldDeletedBy() {
		return ( true == isset( $this->m_intOldDeletedBy ) ) ? ( string ) $this->m_intOldDeletedBy : 'NULL';
	}

	public function setNewDeletedBy( $intNewDeletedBy ) {
		$this->set( 'm_intNewDeletedBy', CStrings::strToIntDef( $intNewDeletedBy, NULL, false ) );
	}

	public function getNewDeletedBy() {
		return $this->m_intNewDeletedBy;
	}

	public function sqlNewDeletedBy() {
		return ( true == isset( $this->m_intNewDeletedBy ) ) ? ( string ) $this->m_intNewDeletedBy : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, diagnostic_id, change_datetime, old_rapid_view_sql, new_rapid_view_sql, old_rapid_repair_sql, new_rapid_repair_sql, old_standard_view_sql, new_standard_view_sql, old_standard_repair_sql, new_standard_repair_sql, old_deleted_by, new_deleted_by, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDiagnosticId() . ', ' .
 						$this->sqlChangeDatetime() . ', ' .
 						$this->sqlOldRapidViewSql() . ', ' .
 						$this->sqlNewRapidViewSql() . ', ' .
 						$this->sqlOldRapidRepairSql() . ', ' .
 						$this->sqlNewRapidRepairSql() . ', ' .
 						$this->sqlOldStandardViewSql() . ', ' .
 						$this->sqlNewStandardViewSql() . ', ' .
 						$this->sqlOldStandardRepairSql() . ', ' .
 						$this->sqlNewStandardRepairSql() . ', ' .
 						$this->sqlOldDeletedBy() . ', ' .
 						$this->sqlNewDeletedBy() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' diagnostic_id = ' . $this->sqlDiagnosticId() . ','; } elseif( true == array_key_exists( 'DiagnosticId', $this->getChangedColumns() ) ) { $strSql .= ' diagnostic_id = ' . $this->sqlDiagnosticId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' change_datetime = ' . $this->sqlChangeDatetime() . ','; } elseif( true == array_key_exists( 'ChangeDatetime', $this->getChangedColumns() ) ) { $strSql .= ' change_datetime = ' . $this->sqlChangeDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_rapid_view_sql = ' . $this->sqlOldRapidViewSql() . ','; } elseif( true == array_key_exists( 'OldRapidViewSql', $this->getChangedColumns() ) ) { $strSql .= ' old_rapid_view_sql = ' . $this->sqlOldRapidViewSql() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_rapid_view_sql = ' . $this->sqlNewRapidViewSql() . ','; } elseif( true == array_key_exists( 'NewRapidViewSql', $this->getChangedColumns() ) ) { $strSql .= ' new_rapid_view_sql = ' . $this->sqlNewRapidViewSql() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_rapid_repair_sql = ' . $this->sqlOldRapidRepairSql() . ','; } elseif( true == array_key_exists( 'OldRapidRepairSql', $this->getChangedColumns() ) ) { $strSql .= ' old_rapid_repair_sql = ' . $this->sqlOldRapidRepairSql() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_rapid_repair_sql = ' . $this->sqlNewRapidRepairSql() . ','; } elseif( true == array_key_exists( 'NewRapidRepairSql', $this->getChangedColumns() ) ) { $strSql .= ' new_rapid_repair_sql = ' . $this->sqlNewRapidRepairSql() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_standard_view_sql = ' . $this->sqlOldStandardViewSql() . ','; } elseif( true == array_key_exists( 'OldStandardViewSql', $this->getChangedColumns() ) ) { $strSql .= ' old_standard_view_sql = ' . $this->sqlOldStandardViewSql() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_standard_view_sql = ' . $this->sqlNewStandardViewSql() . ','; } elseif( true == array_key_exists( 'NewStandardViewSql', $this->getChangedColumns() ) ) { $strSql .= ' new_standard_view_sql = ' . $this->sqlNewStandardViewSql() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_standard_repair_sql = ' . $this->sqlOldStandardRepairSql() . ','; } elseif( true == array_key_exists( 'OldStandardRepairSql', $this->getChangedColumns() ) ) { $strSql .= ' old_standard_repair_sql = ' . $this->sqlOldStandardRepairSql() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_standard_repair_sql = ' . $this->sqlNewStandardRepairSql() . ','; } elseif( true == array_key_exists( 'NewStandardRepairSql', $this->getChangedColumns() ) ) { $strSql .= ' new_standard_repair_sql = ' . $this->sqlNewStandardRepairSql() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_deleted_by = ' . $this->sqlOldDeletedBy() . ','; } elseif( true == array_key_exists( 'OldDeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' old_deleted_by = ' . $this->sqlOldDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_deleted_by = ' . $this->sqlNewDeletedBy() . ','; } elseif( true == array_key_exists( 'NewDeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' new_deleted_by = ' . $this->sqlNewDeletedBy() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'diagnostic_id' => $this->getDiagnosticId(),
			'change_datetime' => $this->getChangeDatetime(),
			'old_rapid_view_sql' => $this->getOldRapidViewSql(),
			'new_rapid_view_sql' => $this->getNewRapidViewSql(),
			'old_rapid_repair_sql' => $this->getOldRapidRepairSql(),
			'new_rapid_repair_sql' => $this->getNewRapidRepairSql(),
			'old_standard_view_sql' => $this->getOldStandardViewSql(),
			'new_standard_view_sql' => $this->getNewStandardViewSql(),
			'old_standard_repair_sql' => $this->getOldStandardRepairSql(),
			'new_standard_repair_sql' => $this->getNewStandardRepairSql(),
			'old_deleted_by' => $this->getOldDeletedBy(),
			'new_deleted_by' => $this->getNewDeletedBy(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>