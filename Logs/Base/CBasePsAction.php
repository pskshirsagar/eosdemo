<?php

class CBasePsAction extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_actions';

	protected $m_intId;
	protected $m_intReviewTaskId;
	protected $m_strModuleName;
	protected $m_strActionName;
	protected $m_fltRecentLoadTime;
	protected $m_intRecentDailyClicks;
	protected $m_fltAverageLoadTime;
	protected $m_intAverageDailyClicks;
	protected $m_intIgnoreWarnings;

	public function __construct() {
		parent::__construct();

		$this->m_intIgnoreWarnings = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['review_task_id'] ) && $boolDirectSet ) $this->set( 'm_intReviewTaskId', trim( $arrValues['review_task_id'] ) ); elseif( isset( $arrValues['review_task_id'] ) ) $this->setReviewTaskId( $arrValues['review_task_id'] );
		if( isset( $arrValues['module_name'] ) && $boolDirectSet ) $this->set( 'm_strModuleName', trim( stripcslashes( $arrValues['module_name'] ) ) ); elseif( isset( $arrValues['module_name'] ) ) $this->setModuleName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['module_name'] ) : $arrValues['module_name'] );
		if( isset( $arrValues['action_name'] ) && $boolDirectSet ) $this->set( 'm_strActionName', trim( stripcslashes( $arrValues['action_name'] ) ) ); elseif( isset( $arrValues['action_name'] ) ) $this->setActionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action_name'] ) : $arrValues['action_name'] );
		if( isset( $arrValues['recent_load_time'] ) && $boolDirectSet ) $this->set( 'm_fltRecentLoadTime', trim( $arrValues['recent_load_time'] ) ); elseif( isset( $arrValues['recent_load_time'] ) ) $this->setRecentLoadTime( $arrValues['recent_load_time'] );
		if( isset( $arrValues['recent_daily_clicks'] ) && $boolDirectSet ) $this->set( 'm_intRecentDailyClicks', trim( $arrValues['recent_daily_clicks'] ) ); elseif( isset( $arrValues['recent_daily_clicks'] ) ) $this->setRecentDailyClicks( $arrValues['recent_daily_clicks'] );
		if( isset( $arrValues['average_load_time'] ) && $boolDirectSet ) $this->set( 'm_fltAverageLoadTime', trim( $arrValues['average_load_time'] ) ); elseif( isset( $arrValues['average_load_time'] ) ) $this->setAverageLoadTime( $arrValues['average_load_time'] );
		if( isset( $arrValues['average_daily_clicks'] ) && $boolDirectSet ) $this->set( 'm_intAverageDailyClicks', trim( $arrValues['average_daily_clicks'] ) ); elseif( isset( $arrValues['average_daily_clicks'] ) ) $this->setAverageDailyClicks( $arrValues['average_daily_clicks'] );
		if( isset( $arrValues['ignore_warnings'] ) && $boolDirectSet ) $this->set( 'm_intIgnoreWarnings', trim( $arrValues['ignore_warnings'] ) ); elseif( isset( $arrValues['ignore_warnings'] ) ) $this->setIgnoreWarnings( $arrValues['ignore_warnings'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setReviewTaskId( $intReviewTaskId ) {
		$this->set( 'm_intReviewTaskId', CStrings::strToIntDef( $intReviewTaskId, NULL, false ) );
	}

	public function getReviewTaskId() {
		return $this->m_intReviewTaskId;
	}

	public function sqlReviewTaskId() {
		return ( true == isset( $this->m_intReviewTaskId ) ) ? ( string ) $this->m_intReviewTaskId : 'NULL';
	}

	public function setModuleName( $strModuleName ) {
		$this->set( 'm_strModuleName', CStrings::strTrimDef( $strModuleName, 150, NULL, true ) );
	}

	public function getModuleName() {
		return $this->m_strModuleName;
	}

	public function sqlModuleName() {
		return ( true == isset( $this->m_strModuleName ) ) ? '\'' . addslashes( $this->m_strModuleName ) . '\'' : 'NULL';
	}

	public function setActionName( $strActionName ) {
		$this->set( 'm_strActionName', CStrings::strTrimDef( $strActionName, 150, NULL, true ) );
	}

	public function getActionName() {
		return $this->m_strActionName;
	}

	public function sqlActionName() {
		return ( true == isset( $this->m_strActionName ) ) ? '\'' . addslashes( $this->m_strActionName ) . '\'' : 'NULL';
	}

	public function setRecentLoadTime( $fltRecentLoadTime ) {
		$this->set( 'm_fltRecentLoadTime', CStrings::strToFloatDef( $fltRecentLoadTime, NULL, false, 2 ) );
	}

	public function getRecentLoadTime() {
		return $this->m_fltRecentLoadTime;
	}

	public function sqlRecentLoadTime() {
		return ( true == isset( $this->m_fltRecentLoadTime ) ) ? ( string ) $this->m_fltRecentLoadTime : 'NULL';
	}

	public function setRecentDailyClicks( $intRecentDailyClicks ) {
		$this->set( 'm_intRecentDailyClicks', CStrings::strToIntDef( $intRecentDailyClicks, NULL, false ) );
	}

	public function getRecentDailyClicks() {
		return $this->m_intRecentDailyClicks;
	}

	public function sqlRecentDailyClicks() {
		return ( true == isset( $this->m_intRecentDailyClicks ) ) ? ( string ) $this->m_intRecentDailyClicks : 'NULL';
	}

	public function setAverageLoadTime( $fltAverageLoadTime ) {
		$this->set( 'm_fltAverageLoadTime', CStrings::strToFloatDef( $fltAverageLoadTime, NULL, false, 2 ) );
	}

	public function getAverageLoadTime() {
		return $this->m_fltAverageLoadTime;
	}

	public function sqlAverageLoadTime() {
		return ( true == isset( $this->m_fltAverageLoadTime ) ) ? ( string ) $this->m_fltAverageLoadTime : 'NULL';
	}

	public function setAverageDailyClicks( $intAverageDailyClicks ) {
		$this->set( 'm_intAverageDailyClicks', CStrings::strToIntDef( $intAverageDailyClicks, NULL, false ) );
	}

	public function getAverageDailyClicks() {
		return $this->m_intAverageDailyClicks;
	}

	public function sqlAverageDailyClicks() {
		return ( true == isset( $this->m_intAverageDailyClicks ) ) ? ( string ) $this->m_intAverageDailyClicks : 'NULL';
	}

	public function setIgnoreWarnings( $intIgnoreWarnings ) {
		$this->set( 'm_intIgnoreWarnings', CStrings::strToIntDef( $intIgnoreWarnings, NULL, false ) );
	}

	public function getIgnoreWarnings() {
		return $this->m_intIgnoreWarnings;
	}

	public function sqlIgnoreWarnings() {
		return ( true == isset( $this->m_intIgnoreWarnings ) ) ? ( string ) $this->m_intIgnoreWarnings : '1';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'review_task_id' => $this->getReviewTaskId(),
			'module_name' => $this->getModuleName(),
			'action_name' => $this->getActionName(),
			'recent_load_time' => $this->getRecentLoadTime(),
			'recent_daily_clicks' => $this->getRecentDailyClicks(),
			'average_load_time' => $this->getAverageLoadTime(),
			'average_daily_clicks' => $this->getAverageDailyClicks(),
			'ignore_warnings' => $this->getIgnoreWarnings()
		);
	}

}
?>