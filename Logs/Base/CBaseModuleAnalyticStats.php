<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CModuleAnalyticStats
 * Do not add any new functions to this class.
 */

class CBaseModuleAnalyticStats extends CEosPluralBase {

	/**
	 * @return CModuleAnalyticStat[]
	 */
	public static function fetchModuleAnalyticStats( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CModuleAnalyticStat::class, $objDatabase );
	}

	/**
	 * @return CModuleAnalyticStat
	 */
	public static function fetchModuleAnalyticStat( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CModuleAnalyticStat::class, $objDatabase );
	}

	public static function fetchModuleAnalyticStatCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'module_analytic_stats', $objDatabase );
	}

	public static function fetchModuleAnalyticStatById( $intId, $objDatabase ) {
		return self::fetchModuleAnalyticStat( sprintf( 'SELECT * FROM module_analytic_stats WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchModuleAnalyticStatsByCid( $intCid, $objDatabase ) {
		return self::fetchModuleAnalyticStats( sprintf( 'SELECT * FROM module_analytic_stats WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchModuleAnalyticStatsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchModuleAnalyticStats( sprintf( 'SELECT * FROM module_analytic_stats WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchModuleAnalyticStatsByAppId( $intAppId, $objDatabase ) {
		return self::fetchModuleAnalyticStats( sprintf( 'SELECT * FROM module_analytic_stats WHERE app_id = %d', ( int ) $intAppId ), $objDatabase );
	}

}
?>