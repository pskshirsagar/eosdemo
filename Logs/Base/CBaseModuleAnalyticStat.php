<?php

class CBaseModuleAnalyticStat extends CEosSingularBase {

	const TABLE_NAME = 'public.module_analytic_stats';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intAppId;
	protected $m_strUserHitDetails;
	protected $m_jsonUserHitDetails;
	protected $m_strModuleName;
	protected $m_strModuleHitDate;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['app_id'] ) && $boolDirectSet ) $this->set( 'm_intAppId', trim( $arrValues['app_id'] ) ); elseif( isset( $arrValues['app_id'] ) ) $this->setAppId( $arrValues['app_id'] );
		if( isset( $arrValues['user_hit_details'] ) ) $this->set( 'm_strUserHitDetails', trim( $arrValues['user_hit_details'] ) );
		if( isset( $arrValues['module_name'] ) && $boolDirectSet ) $this->set( 'm_strModuleName', trim( stripcslashes( $arrValues['module_name'] ) ) ); elseif( isset( $arrValues['module_name'] ) ) $this->setModuleName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['module_name'] ) : $arrValues['module_name'] );
		if( isset( $arrValues['module_hit_date'] ) && $boolDirectSet ) $this->set( 'm_strModuleHitDate', trim( $arrValues['module_hit_date'] ) ); elseif( isset( $arrValues['module_hit_date'] ) ) $this->setModuleHitDate( $arrValues['module_hit_date'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setAppId( $intAppId ) {
		$this->set( 'm_intAppId', CStrings::strToIntDef( $intAppId, NULL, false ) );
	}

	public function getAppId() {
		return $this->m_intAppId;
	}

	public function sqlAppId() {
		return ( true == isset( $this->m_intAppId ) ) ? ( string ) $this->m_intAppId : 'NULL';
	}

	public function setUserHitDetails( $jsonUserHitDetails ) {
		if( true == valObj( $jsonUserHitDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonUserHitDetails', $jsonUserHitDetails );
		} elseif( true == valJsonString( $jsonUserHitDetails ) ) {
			$this->set( 'm_jsonUserHitDetails', CStrings::strToJson( $jsonUserHitDetails ) );
		} else {
			$this->set( 'm_jsonUserHitDetails', NULL ); 
		}
		unset( $this->m_strUserHitDetails );
	}

	public function getUserHitDetails() {
		if( true == isset( $this->m_strUserHitDetails ) ) {
			$this->m_jsonUserHitDetails = CStrings::strToJson( $this->m_strUserHitDetails );
			unset( $this->m_strUserHitDetails );
		}
		return $this->m_jsonUserHitDetails;
	}

	public function sqlUserHitDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getUserHitDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getUserHitDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setModuleName( $strModuleName ) {
		$this->set( 'm_strModuleName', CStrings::strTrimDef( $strModuleName, -1, NULL, true ) );
	}

	public function getModuleName() {
		return $this->m_strModuleName;
	}

	public function sqlModuleName() {
		return ( true == isset( $this->m_strModuleName ) ) ? '\'' . addslashes( $this->m_strModuleName ) . '\'' : 'NULL';
	}

	public function setModuleHitDate( $strModuleHitDate ) {
		$this->set( 'm_strModuleHitDate', CStrings::strTrimDef( $strModuleHitDate, -1, NULL, true ) );
	}

	public function getModuleHitDate() {
		return $this->m_strModuleHitDate;
	}

	public function sqlModuleHitDate() {
		return ( true == isset( $this->m_strModuleHitDate ) ) ? '\'' . $this->m_strModuleHitDate . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, app_id, user_hit_details, module_name, module_hit_date, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlAppId() . ', ' .
 						$this->sqlUserHitDetails() . ', ' .
 						$this->sqlModuleName() . ', ' .
 						$this->sqlModuleHitDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' app_id = ' . $this->sqlAppId() . ','; } elseif( true == array_key_exists( 'AppId', $this->getChangedColumns() ) ) { $strSql .= ' app_id = ' . $this->sqlAppId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_hit_details = ' . $this->sqlUserHitDetails() . ','; } elseif( true == array_key_exists( 'UserHitDetails', $this->getChangedColumns() ) ) { $strSql .= ' user_hit_details = ' . $this->sqlUserHitDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' module_name = ' . $this->sqlModuleName() . ','; } elseif( true == array_key_exists( 'ModuleName', $this->getChangedColumns() ) ) { $strSql .= ' module_name = ' . $this->sqlModuleName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' module_hit_date = ' . $this->sqlModuleHitDate() . ','; } elseif( true == array_key_exists( 'ModuleHitDate', $this->getChangedColumns() ) ) { $strSql .= ' module_hit_date = ' . $this->sqlModuleHitDate() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'app_id' => $this->getAppId(),
			'user_hit_details' => $this->getUserHitDetails(),
			'module_name' => $this->getModuleName(),
			'module_hit_date' => $this->getModuleHitDate(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>