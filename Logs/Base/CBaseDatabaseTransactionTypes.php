<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CDatabaseTransactionTypes
 * Do not add any new functions to this class.
 */

class CBaseDatabaseTransactionTypes extends CEosPluralBase {

	/**
	 * @return CDatabaseTransactionType[]
	 */
	public static function fetchDatabaseTransactionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDatabaseTransactionType', $objDatabase );
	}

	/**
	 * @return CDatabaseTransactionType
	 */
	public static function fetchDatabaseTransactionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDatabaseTransactionType', $objDatabase );
	}

	public static function fetchDatabaseTransactionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'database_transaction_types', $objDatabase );
	}

	public static function fetchDatabaseTransactionTypeById( $intId, $objDatabase ) {
		return self::fetchDatabaseTransactionType( sprintf( 'SELECT * FROM database_transaction_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>