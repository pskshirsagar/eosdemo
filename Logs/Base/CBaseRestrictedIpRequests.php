<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CRestrictedIpRequests
 * Do not add any new functions to this class.
 */

class CBaseRestrictedIpRequests extends CEosPluralBase {

	/**
	 * @return CRestrictedIpRequest[]
	 */
	public static function fetchRestrictedIpRequests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CRestrictedIpRequest', $objDatabase );
	}

	/**
	 * @return CRestrictedIpRequest
	 */
	public static function fetchRestrictedIpRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRestrictedIpRequest', $objDatabase );
	}

	public static function fetchRestrictedIpRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'restricted_ip_requests', $objDatabase );
	}

	public static function fetchRestrictedIpRequestById( $intId, $objDatabase ) {
		return self::fetchRestrictedIpRequest( sprintf( 'SELECT * FROM restricted_ip_requests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchRestrictedIpRequestsByRestrictedIpId( $intRestrictedIpId, $objDatabase ) {
		return self::fetchRestrictedIpRequests( sprintf( 'SELECT * FROM restricted_ip_requests WHERE restricted_ip_id = %d', ( int ) $intRestrictedIpId ), $objDatabase );
	}

}
?>