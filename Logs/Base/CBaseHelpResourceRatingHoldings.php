<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CHelpResourceRatingHoldings
 * Do not add any new functions to this class.
 */

class CBaseHelpResourceRatingHoldings extends CEosPluralBase {

	/**
	 * @return CHelpResourceRatingHolding[]
	 */
	public static function fetchHelpResourceRatingHoldings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CHelpResourceRatingHolding', $objDatabase );
	}

	/**
	 * @return CHelpResourceRatingHolding
	 */
	public static function fetchHelpResourceRatingHolding( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CHelpResourceRatingHolding', $objDatabase );
	}

	public static function fetchHelpResourceRatingHoldingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'help_resource_rating_holdings', $objDatabase );
	}

	public static function fetchHelpResourceRatingHoldingById( $intId, $objDatabase ) {
		return self::fetchHelpResourceRatingHolding( sprintf( 'SELECT * FROM help_resource_rating_holdings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchHelpResourceRatingHoldingsByCid( $intCid, $objDatabase ) {
		return self::fetchHelpResourceRatingHoldings( sprintf( 'SELECT * FROM help_resource_rating_holdings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchHelpResourceRatingHoldingsByHelpResourceId( $intHelpResourceId, $objDatabase ) {
		return self::fetchHelpResourceRatingHoldings( sprintf( 'SELECT * FROM help_resource_rating_holdings WHERE help_resource_id = %d', ( int ) $intHelpResourceId ), $objDatabase );
	}

}
?>