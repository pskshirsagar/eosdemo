<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CEntrataLogs
 * Do not add any new functions to this class.
 */

class CBaseEntrataLogs extends CEosPluralBase {

	/**
	 * @return CEntrataLog[]
	 */
	public static function fetchEntrataLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEntrataLog', $objDatabase );
	}

	/**
	 * @return CEntrataLog
	 */
	public static function fetchEntrataLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEntrataLog', $objDatabase );
	}

	public static function fetchEntrataLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'entrata_logs', $objDatabase );
	}

	public static function fetchEntrataLogById( $intId, $objDatabase ) {
		return self::fetchEntrataLog( sprintf( 'SELECT * FROM entrata_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEntrataLogsByCid( $intCid, $objDatabase ) {
		return self::fetchEntrataLogs( sprintf( 'SELECT * FROM entrata_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>