<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSvnCommitLogTasks
 * Do not add any new functions to this class.
 */

class CBaseSvnCommitLogTasks extends CEosPluralBase {

	/**
	 * @return CSvnCommitLogTask[]
	 */
	public static function fetchSvnCommitLogTasks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSvnCommitLogTask', $objDatabase );
	}

	/**
	 * @return CSvnCommitLogTask
	 */
	public static function fetchSvnCommitLogTask( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSvnCommitLogTask', $objDatabase );
	}

	public static function fetchSvnCommitLogTaskCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'svn_commit_log_tasks', $objDatabase );
	}

	public static function fetchSvnCommitLogTaskById( $intId, $objDatabase ) {
		return self::fetchSvnCommitLogTask( sprintf( 'SELECT * FROM svn_commit_log_tasks WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSvnCommitLogTasksBySvnCommitLogId( $intSvnCommitLogId, $objDatabase ) {
		return self::fetchSvnCommitLogTasks( sprintf( 'SELECT * FROM svn_commit_log_tasks WHERE svn_commit_log_id = %d', ( int ) $intSvnCommitLogId ), $objDatabase );
	}

	public static function fetchSvnCommitLogTasksByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchSvnCommitLogTasks( sprintf( 'SELECT * FROM svn_commit_log_tasks WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

}
?>