<?php

class CBaseSvnCommitLogTask extends CEosSingularBase {

	const TABLE_NAME = 'public.svn_commit_log_tasks';

	protected $m_intId;
	protected $m_intSvnCommitLogId;
	protected $m_intTaskId;
	protected $m_intIsFlagged;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsFlagged = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['svn_commit_log_id'] ) && $boolDirectSet ) $this->set( 'm_intSvnCommitLogId', trim( $arrValues['svn_commit_log_id'] ) ); elseif( isset( $arrValues['svn_commit_log_id'] ) ) $this->setSvnCommitLogId( $arrValues['svn_commit_log_id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['is_flagged'] ) && $boolDirectSet ) $this->set( 'm_intIsFlagged', trim( $arrValues['is_flagged'] ) ); elseif( isset( $arrValues['is_flagged'] ) ) $this->setIsFlagged( $arrValues['is_flagged'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSvnCommitLogId( $intSvnCommitLogId ) {
		$this->set( 'm_intSvnCommitLogId', CStrings::strToIntDef( $intSvnCommitLogId, NULL, false ) );
	}

	public function getSvnCommitLogId() {
		return $this->m_intSvnCommitLogId;
	}

	public function sqlSvnCommitLogId() {
		return ( true == isset( $this->m_intSvnCommitLogId ) ) ? ( string ) $this->m_intSvnCommitLogId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setIsFlagged( $intIsFlagged ) {
		$this->set( 'm_intIsFlagged', CStrings::strToIntDef( $intIsFlagged, NULL, false ) );
	}

	public function getIsFlagged() {
		return $this->m_intIsFlagged;
	}

	public function sqlIsFlagged() {
		return ( true == isset( $this->m_intIsFlagged ) ) ? ( string ) $this->m_intIsFlagged : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, svn_commit_log_id, task_id, is_flagged, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSvnCommitLogId() . ', ' .
 						$this->sqlTaskId() . ', ' .
 						$this->sqlIsFlagged() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_commit_log_id = ' . $this->sqlSvnCommitLogId() . ','; } elseif( true == array_key_exists( 'SvnCommitLogId', $this->getChangedColumns() ) ) { $strSql .= ' svn_commit_log_id = ' . $this->sqlSvnCommitLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_flagged = ' . $this->sqlIsFlagged() . ','; } elseif( true == array_key_exists( 'IsFlagged', $this->getChangedColumns() ) ) { $strSql .= ' is_flagged = ' . $this->sqlIsFlagged() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'svn_commit_log_id' => $this->getSvnCommitLogId(),
			'task_id' => $this->getTaskId(),
			'is_flagged' => $this->getIsFlagged(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>