<?php

class CBaseSystemError extends CEosSingularBase {

	const TABLE_NAME = 'public.system_errors';

	protected $m_intId;
	protected $m_intSystemErrorTypeId;
	protected $m_intEmployeeId;
	protected $m_intClusterId;
	protected $m_strFilePath;
	protected $m_intLine;
	protected $m_strDescription;
	protected $m_strBacktrace;
	protected $m_intErrorCount;
	protected $m_intIsPublished;
	protected $m_strResolveNote;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intClusterId = '1';
		$this->m_intErrorCount = '1';
		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['system_error_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemErrorTypeId', trim( $arrValues['system_error_type_id'] ) ); elseif( isset( $arrValues['system_error_type_id'] ) ) $this->setSystemErrorTypeId( $arrValues['system_error_type_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['line'] ) && $boolDirectSet ) $this->set( 'm_intLine', trim( $arrValues['line'] ) ); elseif( isset( $arrValues['line'] ) ) $this->setLine( $arrValues['line'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['backtrace'] ) && $boolDirectSet ) $this->set( 'm_strBacktrace', trim( stripcslashes( $arrValues['backtrace'] ) ) ); elseif( isset( $arrValues['backtrace'] ) ) $this->setBacktrace( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['backtrace'] ) : $arrValues['backtrace'] );
		if( isset( $arrValues['error_count'] ) && $boolDirectSet ) $this->set( 'm_intErrorCount', trim( $arrValues['error_count'] ) ); elseif( isset( $arrValues['error_count'] ) ) $this->setErrorCount( $arrValues['error_count'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['resolve_note'] ) && $boolDirectSet ) $this->set( 'm_strResolveNote', trim( stripcslashes( $arrValues['resolve_note'] ) ) ); elseif( isset( $arrValues['resolve_note'] ) ) $this->setResolveNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['resolve_note'] ) : $arrValues['resolve_note'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSystemErrorTypeId( $intSystemErrorTypeId ) {
		$this->set( 'm_intSystemErrorTypeId', CStrings::strToIntDef( $intSystemErrorTypeId, NULL, false ) );
	}

	public function getSystemErrorTypeId() {
		return $this->m_intSystemErrorTypeId;
	}

	public function sqlSystemErrorTypeId() {
		return ( true == isset( $this->m_intSystemErrorTypeId ) ) ? ( string ) $this->m_intSystemErrorTypeId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : '1';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setLine( $intLine ) {
		$this->set( 'm_intLine', CStrings::strToIntDef( $intLine, NULL, false ) );
	}

	public function getLine() {
		return $this->m_intLine;
	}

	public function sqlLine() {
		return ( true == isset( $this->m_intLine ) ) ? ( string ) $this->m_intLine : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setBacktrace( $strBacktrace ) {
		$this->set( 'm_strBacktrace', CStrings::strTrimDef( $strBacktrace, -1, NULL, true ) );
	}

	public function getBacktrace() {
		return $this->m_strBacktrace;
	}

	public function sqlBacktrace() {
		return ( true == isset( $this->m_strBacktrace ) ) ? '\'' . addslashes( $this->m_strBacktrace ) . '\'' : 'NULL';
	}

	public function setErrorCount( $intErrorCount ) {
		$this->set( 'm_intErrorCount', CStrings::strToIntDef( $intErrorCount, NULL, false ) );
	}

	public function getErrorCount() {
		return $this->m_intErrorCount;
	}

	public function sqlErrorCount() {
		return ( true == isset( $this->m_intErrorCount ) ) ? ( string ) $this->m_intErrorCount : '1';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setResolveNote( $strResolveNote ) {
		$this->set( 'm_strResolveNote', CStrings::strTrimDef( $strResolveNote, -1, NULL, true ) );
	}

	public function getResolveNote() {
		return $this->m_strResolveNote;
	}

	public function sqlResolveNote() {
		return ( true == isset( $this->m_strResolveNote ) ) ? '\'' . addslashes( $this->m_strResolveNote ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, system_error_type_id, employee_id, cluster_id, file_path, line, description, backtrace, error_count, is_published, resolve_note, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSystemErrorTypeId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlClusterId() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlLine() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlBacktrace() . ', ' .
 						$this->sqlErrorCount() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlResolveNote() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_error_type_id = ' . $this->sqlSystemErrorTypeId() . ','; } elseif( true == array_key_exists( 'SystemErrorTypeId', $this->getChangedColumns() ) ) { $strSql .= ' system_error_type_id = ' . $this->sqlSystemErrorTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' line = ' . $this->sqlLine() . ','; } elseif( true == array_key_exists( 'Line', $this->getChangedColumns() ) ) { $strSql .= ' line = ' . $this->sqlLine() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' backtrace = ' . $this->sqlBacktrace() . ','; } elseif( true == array_key_exists( 'Backtrace', $this->getChangedColumns() ) ) { $strSql .= ' backtrace = ' . $this->sqlBacktrace() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_count = ' . $this->sqlErrorCount() . ','; } elseif( true == array_key_exists( 'ErrorCount', $this->getChangedColumns() ) ) { $strSql .= ' error_count = ' . $this->sqlErrorCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolve_note = ' . $this->sqlResolveNote() . ','; } elseif( true == array_key_exists( 'ResolveNote', $this->getChangedColumns() ) ) { $strSql .= ' resolve_note = ' . $this->sqlResolveNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'system_error_type_id' => $this->getSystemErrorTypeId(),
			'employee_id' => $this->getEmployeeId(),
			'cluster_id' => $this->getClusterId(),
			'file_path' => $this->getFilePath(),
			'line' => $this->getLine(),
			'description' => $this->getDescription(),
			'backtrace' => $this->getBacktrace(),
			'error_count' => $this->getErrorCount(),
			'is_published' => $this->getIsPublished(),
			'resolve_note' => $this->getResolveNote(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>