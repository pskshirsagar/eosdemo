<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CToolTipViews
 * Do not add any new functions to this class.
 */

class CBaseToolTipViews extends CEosPluralBase {

	/**
	 * @return CToolTipView[]
	 */
	public static function fetchToolTipViews( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CToolTipView', $objDatabase );
	}

	/**
	 * @return CToolTipView
	 */
	public static function fetchToolTipView( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CToolTipView', $objDatabase );
	}

	public static function fetchToolTipViewCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tool_tip_views', $objDatabase );
	}

	public static function fetchToolTipViewById( $intId, $objDatabase ) {
		return self::fetchToolTipView( sprintf( 'SELECT * FROM tool_tip_views WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchToolTipViewsByToolTipId( $intToolTipId, $objDatabase ) {
		return self::fetchToolTipViews( sprintf( 'SELECT * FROM tool_tip_views WHERE tool_tip_id = %d', ( int ) $intToolTipId ), $objDatabase );
	}

	public static function fetchToolTipViewsByCid( $intCid, $objDatabase ) {
		return self::fetchToolTipViews( sprintf( 'SELECT * FROM tool_tip_views WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>