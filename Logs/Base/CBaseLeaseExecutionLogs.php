<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CLeaseExecutionLogs
 * Do not add any new functions to this class.
 */

class CBaseLeaseExecutionLogs extends CEosPluralBase {

	/**
	 * @return CLeaseExecutionLog[]
	 */
	public static function fetchLeaseExecutionLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CLeaseExecutionLog', $objDatabase );
	}

	/**
	 * @return CLeaseExecutionLog
	 */
	public static function fetchLeaseExecutionLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLeaseExecutionLog', $objDatabase );
	}

	public static function fetchLeaseExecutionLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_execution_logs', $objDatabase );
	}

	public static function fetchLeaseExecutionLogById( $intId, $objDatabase ) {
		return self::fetchLeaseExecutionLog( sprintf( 'SELECT * FROM lease_execution_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchLeaseExecutionLogsByCid( $intCid, $objDatabase ) {
		return self::fetchLeaseExecutionLogs( sprintf( 'SELECT * FROM lease_execution_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseExecutionLogsByApplicationId( $intApplicationId, $objDatabase ) {
		return self::fetchLeaseExecutionLogs( sprintf( 'SELECT * FROM lease_execution_logs WHERE application_id = %d', ( int ) $intApplicationId ), $objDatabase );
	}

	public static function fetchLeaseExecutionLogsByLeaseId( $intLeaseId, $objDatabase ) {
		return self::fetchLeaseExecutionLogs( sprintf( 'SELECT * FROM lease_execution_logs WHERE lease_id = %d', ( int ) $intLeaseId ), $objDatabase );
	}

	public static function fetchLeaseExecutionLogsByLeaseDocumentId( $intLeaseDocumentId, $objDatabase ) {
		return self::fetchLeaseExecutionLogs( sprintf( 'SELECT * FROM lease_execution_logs WHERE lease_document_id = %d', ( int ) $intLeaseDocumentId ), $objDatabase );
	}

}
?>