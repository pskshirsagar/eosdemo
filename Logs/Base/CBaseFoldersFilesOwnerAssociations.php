<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CFoldersFilesOwnerAssociations
 * Do not add any new functions to this class.
 */

class CBaseFoldersFilesOwnerAssociations extends CEosPluralBase {

	/**
	 * @return CFoldersFilesOwnerAssociation[]
	 */
	public static function fetchFoldersFilesOwnerAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CFoldersFilesOwnerAssociation::class, $objDatabase );
	}

	/**
	 * @return CFoldersFilesOwnerAssociation
	 */
	public static function fetchFoldersFilesOwnerAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFoldersFilesOwnerAssociation::class, $objDatabase );
	}

	public static function fetchFoldersFilesOwnerAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'folders_files_owner_associations', $objDatabase );
	}

	public static function fetchFoldersFilesOwnerAssociationById( $intId, $objDatabase ) {
		return self::fetchFoldersFilesOwnerAssociation( sprintf( 'SELECT * FROM folders_files_owner_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchFoldersFilesOwnerAssociationsByFoldersFilesOwnerAssociationsId( $intFoldersFilesOwnerAssociationsId, $objDatabase ) {
		return self::fetchFoldersFilesOwnerAssociations( sprintf( 'SELECT * FROM folders_files_owner_associations WHERE folders_files_owner_associations_id = %d', ( int ) $intFoldersFilesOwnerAssociationsId ), $objDatabase );
	}

	public static function fetchFoldersFilesOwnerAssociationsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchFoldersFilesOwnerAssociations( sprintf( 'SELECT * FROM folders_files_owner_associations WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchFoldersFilesOwnerAssociationsByPsProductOptionId( $intPsProductOptionId, $objDatabase ) {
		return self::fetchFoldersFilesOwnerAssociations( sprintf( 'SELECT * FROM folders_files_owner_associations WHERE ps_product_option_id = %d', ( int ) $intPsProductOptionId ), $objDatabase );
	}

	public static function fetchFoldersFilesOwnerAssociationsBySdmEmployeeId( $intSdmEmployeeId, $objDatabase ) {
		return self::fetchFoldersFilesOwnerAssociations( sprintf( 'SELECT * FROM folders_files_owner_associations WHERE sdm_employee_id = %d', ( int ) $intSdmEmployeeId ), $objDatabase );
	}

	public static function fetchFoldersFilesOwnerAssociationsByArchitectEmployeeId( $intArchitectEmployeeId, $objDatabase ) {
		return self::fetchFoldersFilesOwnerAssociations( sprintf( 'SELECT * FROM folders_files_owner_associations WHERE architect_employee_id = %d', ( int ) $intArchitectEmployeeId ), $objDatabase );
	}

	public static function fetchFoldersFilesOwnerAssociationsByDevEmployeeId( $intDevEmployeeId, $objDatabase ) {
		return self::fetchFoldersFilesOwnerAssociations( sprintf( 'SELECT * FROM folders_files_owner_associations WHERE dev_employee_id = %d', ( int ) $intDevEmployeeId ), $objDatabase );
	}

	public static function fetchFoldersFilesOwnerAssociationsByQaEmployeeId( $intQaEmployeeId, $objDatabase ) {
		return self::fetchFoldersFilesOwnerAssociations( sprintf( 'SELECT * FROM folders_files_owner_associations WHERE qa_employee_id = %d', ( int ) $intQaEmployeeId ), $objDatabase );
	}

}
?>