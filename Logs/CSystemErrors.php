<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSystemErrors
 * Do not add any new functions to this class.
 */

class CSystemErrors extends CBaseSystemErrors {

	public static function fetchSystemErrorsByIds( $arrintSystemErrorsIds, $objDatabase ) {

		if( false == valArr( $arrintSystemErrorsIds ) ) return NULL;

		return self::fetchSystemErrors( sprintf( 'SELECT * FROM %s WHERE id IN ( %s )', 'system_errors', implode( ',', $arrintSystemErrorsIds ) ), $objDatabase );
    }

	public static function fetchConflictingSystemError( $objSystemError, $boolIsCheckDescription, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						system_errors
					WHERE
						system_error_type_id = ' . ( int ) $objSystemError->getSystemErrorTypeId() . '
						AND cluster_id = ' . ( int ) $objSystemError->getClusterId() . '
						AND deleted_on IS NULL
						AND file_path = ' . $objSystemError->sqlFilePath() . '
						AND line = ' . ( int ) $objSystemError->getLine();

		if( true == $boolIsCheckDescription ) {
			$strSql .= ' AND description = ' . $objSystemError->sqlDescription();
		}

		$strSql .= ' LIMIT 1';

		return self::fetchSystemError( $strSql, $objDatabase );
	}

	public static function fetchPaginatedSystemErrorsCountBySearchFilter( $objSystemErrorsFilter, $objDatabase ) {

		$strProductCondition        = '';
		$arrstrAndSearchParameters = self::getSearchCriteria( $objSystemErrorsFilter );

		$strFromClause = ' FROM
								system_errors se
								LEFT JOIN system_error_details sed ON ( se.id = sed.system_error_id )';

		if( true == valObj( $objSystemErrorsFilter, 'CSystemErrorsFilter' ) ) {
			$arrstrAndSearchParameters = self::getSearchCriteria( $objSystemErrorsFilter );
			if( true == valArr( $objSystemErrorsFilter->getPsProductIds() ) ) {
				$strProductCondition = ' AND NOT exists (
															SELECT
																system_error_id
															FROM
																system_error_details sed1
															WHERE 
																sed1.system_error_id = se.id AND sed1.ps_product_id NOT IN ( ' . implode( ',', $objSystemErrorsFilter->getPsProductIds() ) . ' )
														) ';
			}
		}

		$strSql = 'SELECT '
					. ' count( distinct( se.id ) ) as count ' . $strFromClause
					. ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' WHERE ' : '' )
					. ( ( true == valArr( $arrstrAndSearchParameters ) ) ? implode( ' AND ', $arrstrAndSearchParameters ) : '' )
					. $strProductCondition;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchPaginatedSystemErrorsBySearchFilter( $objSystemErrorsFilter = NULL, $objDatabase ) {

		$intOffset	= ( 0 < $objSystemErrorsFilter->getPageNo() ) ? $objSystemErrorsFilter->getPageSize() * ( $objSystemErrorsFilter->getPageNo() - 1 ) : 0;
		$intLimit	= ( int ) $objSystemErrorsFilter->getPageSize();

		$strFromClause = ' FROM
								system_errors se
								JOIN system_error_details sed ON ( se.id = sed.system_error_id )';

		$strProductCondition        = '';
		$strOrderClause			= '';
		$strGroupByClause		= ' GROUP BY se.id';

		$arrstrAndSearchParameters = array();

		if( true == valObj( $objSystemErrorsFilter, 'CSystemErrorsFilter' ) ) {
			$arrstrAndSearchParameters = self::getSearchCriteria( $objSystemErrorsFilter );
			if( true == valArr( $objSystemErrorsFilter->getPsProductIds() ) ) {
				$strProductCondition = ' AND NOT exists (
															SELECT
																system_error_id
															FROM
																system_error_details sed1
															WHERE 
																sed1.system_error_id = se.id AND sed1.ps_product_id NOT IN ( ' . implode( ',', $objSystemErrorsFilter->getPsProductIds() ) . ' )
														) ';
			}
		}

		if( false == is_null( $objSystemErrorsFilter ) && false == is_null( $objSystemErrorsFilter->getOrderByField() ) && false == is_null( $objSystemErrorsFilter->getOrderByType() ) ) {

			$strOrder = ( 1 == $objSystemErrorsFilter->getOrderByType() ) ? ' DESC ' : ' ASC ';

			switch( $objSystemErrorsFilter->getOrderByField() ) {
				case NULL:
					break;

				case 'system_error_id':
					$strOrderClause			= ' ORDER BY se.id ' . $strOrder;
					break;

				case 'employee_id':
					$strOrderClause			= ' ORDER BY se.employee_id ' . $strOrder;
					break;

				case 'system_error_type_id':
					$strFromClause			.= ' JOIN system_error_types st ON ( se.system_error_type_id = st.id ) ';
					$strOrderClause			 = ' ORDER BY lower( st.name )' . $strOrder;
					$strGroupByClause		.= ', st.name ';
					break;

				case 'error_count':
					 $strOrderClause 		 = ' ORDER BY se.error_count ' . $strOrder;
					break;

				case 'created_on':
					$strOrderClause			 = ' ORDER BY se.created_on ' . $strOrder;
					break;

				case 'updated_on':
					$strOrderClause			 = ' ORDER BY se.updated_on ' . $strOrder;
					break;

				default:
					$strOrderClause 		 = ' ORDER BY se.' . $objSystemErrorsFilter->getOrderByField() . $strOrder;
			}
		}

		$strSql = 'SELECT'
						. ' se.*' . $strFromClause
						. ( ( false == is_null( $arrstrAndSearchParameters ) && true == valArr( $arrstrAndSearchParameters ) ) ? ' WHERE ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' )
						. $strProductCondition
						. $strGroupByClause
						. ( ( 0 < strlen( $strOrderClause ) ) ? $strOrderClause : ' ORDER BY se.updated_on DESC ' ) . '
					OFFSET
						' . ( int ) $intOffset;

		if( true == isset( $intLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSystemErrorsCountBySearchFilter( $objSystemErrorsFilter = NULL, $objDatabase ) {
		$arrstrAndSearchParameters		= self::getSearchCriteria( $objSystemErrorsFilter );
		$arrstrProductcondition			= preg_grep( '/sed.ps_product_id IN(.*)/', $arrstrAndSearchParameters );
		$arrstrProductOptioncondition	= preg_grep( '/sed.ps_product_option_id IN(.*)/', $arrstrAndSearchParameters );

		$strCondition = '';
		if( true == valArr( $arrstrAndSearchParameters ) ) {
			$strCondition = implode( ' AND ', $arrstrAndSearchParameters );
			if( ( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) && true == in_array( 'se.deleted_by IS NULL', $arrstrAndSearchParameters ) ) || ( true == valArr( $arrstrProductcondition ) && false == valArr( $arrstrProductOptioncondition ) ) ) {
				$strCondition .= ' AND sed.ps_product_option_id IS NULL AND sed.ps_product_id != 0
								AND
								se.id in ( SELECT
									system_error_id
								FROM
								system_error_details
								GROUP BY system_error_id HAVING COUNT( DISTINCT(ps_product_id) ) = 1 )';
			}
		}

		$strSql = 'SELECT
						sed.ps_product_id,
						count( DISTINCT sed.system_error_id )
					FROM
						system_errors se
						LEFT JOIN system_error_details sed ON ( se.id = sed.system_error_id )
					WHERE '
						 . $strCondition . '
					GROUP BY
					    sed.ps_product_id
					ORDER BY
					    sed.ps_product_id ASC ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSystemErrorsByGivenDateSpanByProductIds( $strFromReleaseDate, $strToReleaseDate, $arrintPsProductIds, $objDatabase ) {

		if( false == valArr( $arrintPsProductIds ) ) return NULL;

		$strSql = 'SELECT
							sed.ps_product_id,
							se.id,
							se.created_on
						FROM
							system_errors se
							LEFT JOIN system_error_details sed ON ( se.id = sed.system_error_id )

						WHERE
							date ( se.created_on ) <= date \'' . $strToReleaseDate . '\' and
							date ( se.created_on ) > date \'' . $strFromReleaseDate . '\' and
							sed.ps_product_id in(' . implode( ',', $arrintPsProductIds ) . ' )
						GROUP BY
							sed.ps_product_id, se.id, se.created_on
						ORDER BY
							sed.ps_product_id ASC ';

		$arrmixSystemErrorsCountByGivenDateSpan = fetchData( $strSql, $objDatabase );

		return $arrmixSystemErrorsCountByGivenDateSpan;
	}

	public static function fetchPaginatedSystemErrorsByGivenDateSpanByProductIds( $intPageNo, $intPageSize, $arrintPsProductIds, $strFromReleaseDate, $strToReleaseDate, $objDatabase, $strField=NULL, $strFieldType=NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		switch( $strField ) {
			case NULL:
			case 'count':
				$strField = 'error_count';
				break;

			case 'system_error_type':
				$strField = 'system_error_type_name';
				break;

			case 'module':
				$strField = 'module';
				break;

			case 'logged_on':
				$strField = 'created_on';
				break;

			case 'updated_on':
				$strField = 'updated_on';
				break;

			case 'is_resolved':
				$strField = 'deleted_by';
				break;

			case 'system_error_id':
				$strField = 'id';
				break;

			default:
				$strField = 'error_count';
		}

		if( NULL == $strFieldType ) {
			$strFieldType = 'DESC';
		}

		$strSql = 'SELECT
							sert.name AS system_error_type_name,
							sed.module as module,
							sed.ps_product_id as ps_product_id,
							sum(se.error_count) as error_count,
							MIN(to_char( se.created_on, \'mm/d/yy hh:mi:ss\') ) as created_on,
							MAX(to_char( se.updated_on, \'mm/d/yy hh:mi:ss\') ) as updated_on,
							se.deleted_by AS deleted_by,
							MAX( se.id ) AS id
						FROM system_errors se
							JOIN system_error_types sert ON ( se.system_error_type_id = sert.id )
							LEFT JOIN system_error_details sed ON ( se.id = sed.system_error_id )
						WHERE
							se.created_on ::date <= \'' . $strToReleaseDate . '\' ::date AND
							se.created_on ::date > \'' . $strFromReleaseDate . '\' ::date AND
							sed.ps_product_id in( ' . implode( ',', $arrintPsProductIds ) . ' )
						GROUP BY
							se.system_error_type_id,
							sert.name,
							sed.module,
							sed.ps_product_id,
							se.deleted_by
						ORDER BY '
							. $strField . ' ' . $strFieldType . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return self::fetchSystemErrors( $strSql, $objDatabase );
	}

	public static function fetchSystemErrorCountByGivenDateSpanByProductIds( $strFromReleaseDate, $strToReleaseDate, $arrintPsProductIds, $objDatabase ) {

		$strSql = 'SELECT
							sert.name AS system_error_type_name,
							sed.module as module,
							sum(se.error_count) as error_count,
							MIN(se.created_on) as created_on,
							MAX(se.updated_on) as updated_on
						FROM system_errors se
							JOIN system_error_types sert ON ( se.system_error_type_id = sert.id )
							LEFT JOIN system_error_details sed ON ( se.id = sed.system_error_id )
						WHERE
							se.created_on::date <= \'' . $strToReleaseDate . '\'::date AND
							se.created_on::date > \'' . $strFromReleaseDate . '\'::date AND
							 sed.ps_product_id in( ' . implode( ',', $arrintPsProductIds ) . ' )
						GROUP BY
							se.system_error_type_id,
							sert.name,
							sed.module';

		$arrmixSystemErrorsCountByGivenDateSpan = \Psi\Libraries\UtilFunctions\count( fetchData( $strSql, $objDatabase ) );

		return $arrmixSystemErrorsCountByGivenDateSpan;
	}

	public static function fetchSearchedSystemErrorIds( $intFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
						se.id,
						st.name AS system_error_type_name,
						se.file_path,
						se.description
					FROM
						system_errors se
						JOIN system_error_types st ON ( se.system_error_type_id = st.id )
						LEFT JOIN system_error_details sed ON ( se.id = sed.system_error_id )
					WHERE
						to_char( se.id, \'99999999\' )  LIKE \'%' . $intFilteredExplodedSearch . '%\'
					ORDER BY
						se.id
					LIMIT
						10';

		return self::fetchSystemErrors( $strSql, $objDatabase );

	}

	public static function getSearchCriteria( $objSystemErrorsFilter ) {

 		$arrstrWhereParameters = array();

		if( true == isset( $objSystemErrorsFilter ) && true == valArr( $objSystemErrorsFilter->getPsProductIds() ) && false == valArr( $objSystemErrorsFilter->getPsProductOptions() ) ) {
			array_push( $arrstrWhereParameters, 'sed.ps_product_id IN ( ' . implode( ',', $objSystemErrorsFilter->getPsProductIds() ) . ' ) AND sed.ps_product_option_id IS NULL' );
		} elseif( true == isset( $objSystemErrorsFilter ) && true == valArr( $objSystemErrorsFilter->getPsProductOptions() ) && true == valArr( $objSystemErrorsFilter->getPsProductIds() ) ) {
			array_push( $arrstrWhereParameters, '( sed.ps_product_id IN ( ' . implode( ',', $objSystemErrorsFilter->getPsProductIds() ) . ' ) AND sed.ps_product_option_id IS NULL OR sed.ps_product_option_id IN ( ' . implode( ',', $objSystemErrorsFilter->getPsProductOptions() ) . ' ) )' );
		} elseif( true == isset( $objSystemErrorsFilter ) && true == valArr( $objSystemErrorsFilter->getPsProductOptions() ) && false == valArr( $objSystemErrorsFilter->getPsProductIds() ) ) {
			array_push( $arrstrWhereParameters, 'sed.ps_product_option_id IN ( ' . implode( ',', $objSystemErrorsFilter->getPsProductOptions() ) . ' )' );
		}

		if( 0 < strlen( $objSystemErrorsFilter->getEmployeeIds() ) ) $arrstrWhereParameters[] = 'se.employee_id IN( ' . $objSystemErrorsFilter->getEmployeeIds() . ' )';
		if( 0 < strlen( $objSystemErrorsFilter->getSystemErrorIds() ) ) $arrstrWhereParameters[] = 'se.id IN( ' . $objSystemErrorsFilter->getSystemErrorIds() . ' )';

		if( true == is_numeric( $objSystemErrorsFilter->getDeletedBy() ) ) {
			$arrstrWhereParameters[] = 'se.deleted_by IS NOT NULL';
		} else {
			$arrstrWhereParameters[] = 'se.deleted_by IS NULL';
		}

		if( false == is_null( $objSystemErrorsFilter->getFilePath() ) && 0 < strlen( $objSystemErrorsFilter->getFilePath() ) )			$arrstrWhereParameters[] = 'se.file_path ILIKE \'%' . trim( addslashes( $objSystemErrorsFilter->getFilePath() ) ) . '%\'';
		if( 0 < strlen( $objSystemErrorsFilter->getLineNumber() ) )																		$arrstrWhereParameters[] = 'se.line = ' . ( int ) trim( $objSystemErrorsFilter->getLineNumber() );
		if( false == is_null( $objSystemErrorsFilter->getClient() ) && 0 < strlen( $objSystemErrorsFilter->getClient() ) ) 				$arrstrWhereParameters[] = 'sed.client ILIKE \'%' . trim( addslashes( $objSystemErrorsFilter->getClient() ) ) . '%\'';
		if( false == is_null( $objSystemErrorsFilter->getBrowser() ) && 0 < strlen( $objSystemErrorsFilter->getBrowser() ) ) 			$arrstrWhereParameters[] = 'sed.browser ILIKE \'%' . trim( addslashes( $objSystemErrorsFilter->getBrowser() ) ) . '%\'';
		if( false == is_null( $objSystemErrorsFilter->getServer() ) && 0 < strlen( $objSystemErrorsFilter->getServer() ) ) 				$arrstrWhereParameters[] = 'sed.server ILIKE \'%' . trim( addslashes( $objSystemErrorsFilter->getServer() ) ) . '%\'';
		if( false == is_null( $objSystemErrorsFilter->getModule() ) && 0 < strlen( $objSystemErrorsFilter->getModule() ) ) 				$arrstrWhereParameters[] = 'sed.module IN ( ' . trim( '\'' . implode( '\',\'', explode( ',', $objSystemErrorsFilter->getModule() ) ) . '\'' ) . ' )';
		if( false == is_null( $objSystemErrorsFilter->getAction() ) && 0 < strlen( $objSystemErrorsFilter->getAction() ) ) 				$arrstrWhereParameters[] = 'sed.action ILIKE \'%' . trim( addslashes( $objSystemErrorsFilter->getAction() ) ) . '%\'';
		if( false == is_null( $objSystemErrorsFilter->getDescription() ) && 0 < strlen( $objSystemErrorsFilter->getDescription() ) ) 	$arrstrWhereParameters[] = ' ( se.description ILIKE \'%' . trim( addslashes( $objSystemErrorsFilter->getDescription() ) ) . '%\' OR se.backtrace ILIKE \'%' . trim( addslashes( $objSystemErrorsFilter->getDescription() ) ) . '%\' )';
		if( false == is_null( $objSystemErrorsFilter->getUserId() ) && 0 < strlen( $objSystemErrorsFilter->getUserId() ) ) 				$arrstrWhereParameters[] = 'se.employee_id = ' . ( int ) $objSystemErrorsFilter->getUserId();
		if( 0 < strlen( $objSystemErrorsFilter->getSystemErrorTypeIds() ) )																$arrstrWhereParameters[] = 'se.system_error_type_id IN ( ' . $objSystemErrorsFilter->getSystemErrorTypeIds() . ')';
		if( 0 < strlen( $objSystemErrorsFilter->getClusterIds() ) )																		$arrstrWhereParameters[] = 'se.cluster_id IN ( ' . $objSystemErrorsFilter->getClusterIds() . ')';
		if( 0 < strlen( $objSystemErrorsFilter->getFromDate() ) )  																		$arrstrWhereParameters[] = 'se.created_on >= \'' . date( 'Y-m-d', strtotime( $objSystemErrorsFilter->getFromDate() ) ) . ' 00:00:00\'';
		if( 0 < strlen( $objSystemErrorsFilter->getToDate() ) )																			$arrstrWhereParameters[] = 'se.created_on <= \'' . date( 'Y-m-d', strtotime( $objSystemErrorsFilter->getToDate() ) ) . ' 23:59:59\'';

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrWhereParameters ) ) {
			return $arrstrWhereParameters;
		} else {
			return NULL;
		}
	}

	public static function fetchTodaysUndeletedSystemErrors( $objDatabase ) {

		$strSql = ' SELECT
					    se.id,
					    sed.module,
					    sed.backtrace
					FROM
					    system_errors se
					    LEFT JOIN system_error_details sed ON ( sed.system_error_id = se.id )
					WHERE
					    deleted_on IS NULL
					    AND deleted_by IS NULL
					    AND DATE ( sed.created_on ) = DATE ( now ( ) )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSystemErrorsByMonth( $strDate, $objDatabase, $intPageNo= NULL, $intPageSize =NULL,$strOrderByField = NULL, $strOrderByType = NULL ) {

		if( false == is_null( $strOrderByField ) ) {

			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY se.updated_on DESC';
		}

		if( true != is_null( $intPageNo ) && true != is_null( $intPageSize ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$strPageCondition = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;
		}

		$strSql = '	SELECT
						*
					FROM
						system_errors se
					WHERE
   						to_char ( se.created_on, \'YYYY-MM-DD\' ) >= to_char ( \'' . $strDate . '\' ::DATE, \'YYYY-MM-DD\' )
					AND
   						to_char ( se.created_on, \'YYYY-MM-DD\' ) <= to_char ( \'' . $strDate . '\' ::DATE, \'YYYY-MM-DD\' )
					AND
						se.description like \'%no more connections allowed%\'
					';

		$strSql .= $strOrderBy . $strPageCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMaxConnectionErrorCountPerMonth( $strFromCreatedDate, $strToCreatedDate, $objLogDatabase ) {

	$strSql = 'SELECT
					DISTINCT (date( created_on ) ),
					count(id) as count
				FROM
					system_errors se
				WHERE
					se.description like \'%no more connections allowed%\'
				AND 
					to_char ( se.created_on, \'YYYY-MM-DD\' ) >= to_char ( \'' . $strFromCreatedDate . '\' ::DATE, \'YYYY-MM-DD\' )
				AND 
					to_char ( se.created_on, \'YYYY-MM-DD\' ) <= to_char ( \'' . $strToCreatedDate . '\' ::DATE, \'YYYY-MM-DD\' )
   				GROUP BY
   					date (created_on)
   				ORDER BY
   					date (created_on)';

	return fetchData( $strSql, $objLogDatabase );

	}

	public static function fetchSystemErrorCountByProductBYMonth( $arrintPsProduct, $intClusterId, $objLogDatabase ) {

		if( true == isset( $arrintPsProduct['ps_product_option_id'] ) ) {
			$strImplodeConditionPsProductOption = 'sed.ps_product_option_id IN (' . implode( ',', $arrintPsProduct['ps_product_option_id'] ) . ')';
		}

		if( true == isset( $arrintPsProduct['ps_product_id'] ) ) {
			$strOrCondition = '';
			if( false == is_null( $strImplodeConditionPsProductOption ) ) {

				$strOrCondition = ' OR ( sed.ps_product_option_id IS NULL AND ';
				$strImplodeConditionPsProduct = $strOrCondition . 'sed.ps_product_id IN (' . implode( ',', $arrintPsProduct['ps_product_id'] ) . ') )';
			} else {
				$strImplodeConditionPsProduct = $strOrCondition . 'sed.ps_product_id IN (' . implode( ',', $arrintPsProduct['ps_product_id'] ) . ') ';
			}

		}

		$strSql = 'SELECT subq.ps_product_id,
							       subq.ps_product_option_id,
							       count(subq.ps_product_id),
							       to_char( to_timestamp(to_char( subq.mon, \'99\'), \'MM\'), \'Mon\') as mon,
							       subq.yr

							FROM (
							       SELECT sed.ps_product_id,
							              sed.ps_product_option_id,
							              extract(year FROM sed.created_on) as yr,
							       		 extract(month FROM sed.created_on) as mon,
							            row_number() OVER(partition by sed.system_error_id
							       ORDER BY sed.id ASC) as sed1,
							                se.id,
							                se.cluster_id
							       FROM system_errors se
							            JOIN system_error_details sed ON (se.id = sed.system_error_id)
							       WHERE se.deleted_by IS NULL AND
							             sed.ps_product_id IS NOT NULL AND
							             se.cluster_id = ' . ( int ) $intClusterId . ' AND
							             se.created_on >(current_date - INTERVAL \'12 months\')
										AND (' . $strImplodeConditionPsProductOption . $strImplodeConditionPsProduct . ' )
							       GROUP BY se.id,
							                sed.ps_product_id,
							                sed.ps_product_option_id,
							                sed.system_error_id,
							                sed.id,
							               extract(year
							       FROM sed.created_on),
							       extract(month
							       FROM sed.created_on)

							     ) AS subq
							WHERE subq.sed1 = 1
							GROUP BY subq.ps_product_id,
							         subq.ps_product_option_id,
							         subq.mon,
							         subq.yr
							ORDER BY subq.yr ASC,
							         subq.mon ASC
								';
		return fetchData( $strSql, $objLogDatabase );
	}

	public static function fetchTopSystemErrorsByProductIdsBySubProductIdsByClusterIdsBySystemErrorTypeIds( $arrintProductIds, $arrintClusterIds, $arrintSystemErrorTypeIds, $objLogDatabase, $intLimit = 10, $arrintSubProductIds = array() ) {

		$strOrCondition = '';

		if( true == valArr( $arrintSubProductIds ) ) {
			$strOrCondition = 'OR sed.ps_product_option_id IN ( ' . implode( ',', $arrintSubProductIds ) . ' )';
		}

		$strSql = 'SELECT
					DISTINCT se.id,
					set.name,
					se.description,
					se.file_path,
					se.line,
					CASE
						WHEN se.cluster_id = ' . CCluster::RAPID . ' THEN \'Rapid\'
						WHEN se.cluster_id = ' . CCluster::STANDARD . ' THEN \'Standard\'
					END AS cluster_name,
					se.error_count,
					se.created_on,
					se.updated_on
				FROM
					system_errors se
					JOIN system_error_details sed ON ( se.id = sed.system_error_id AND ( sed.ps_product_id IN ( ' . implode( ',', $arrintProductIds ) . ' ) AND ( sed.ps_product_option_id IS NULL ' . $strOrCondition . ' ) ) )
					JOIN system_error_types set ON ( set.id = se.system_error_type_id )
				WHERE
					se.system_error_type_id IN ( ' . implode( ',', $arrintSystemErrorTypeIds ) . ' )
					AND se.deleted_by IS NULL
					AND se.description NOT LIKE \'execute_db ERROR:%\'
					AND se.cluster_id IN ( ' . implode( ', ', $arrintClusterIds ) . ' )
				ORDER BY
					se.error_count DESC
				OFFSET 0
				LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objLogDatabase );
	}

	public static function fetchSystemErrorsCount( $objLogDatabase, $arrintProductId = array(), $strPsProductOptionIds = NULL ) {

		$strProductCondition = $strJoinCondition = '';

		if( true == valArr( $arrintProductId ) && false == empty( $arrintProductId ) && false == is_null( $strPsProductOptionIds ) ) {
			$strJoinCondition = ' LEFT JOIN system_error_details sed ON (se.id = sed.system_error_id) ';
			$strProductCondition = ' AND ( sed.ps_product_id IN ( ' . implode( ',', $arrintProductId ) . ' ) AND sed.ps_product_option_id IS NULL OR sed.ps_product_option_id IN ( ' . $strPsProductOptionIds . ' ) ) ';
		}

		$strSql = '	SELECT
						COUNT( DISTINCT ( se.id ) ) as count
					FROM
						system_errors se ' . $strJoinCondition . '
					WHERE se.deleted_by IS NULL' . $strProductCondition;

		return fetchData( $strSql, $objLogDatabase );
	}

	public static function fetchQaFileOwnerEmployeeIdById( $intSystemErrorId, $objLogDatabase ) {

		if( true == is_null( $intSystemErrorId ) ) return NULL;

		$strSql = ' SELECT
						se.file_path
					FROM
						system_errors se
					WHERE
						se.id = ' . ( int ) $intSystemErrorId;

		$arrstrSystemErrorDetials = fetchData( $strSql, $objLogDatabase );

		if( true == valArr( $arrstrSystemErrorDetials ) ) {

			$strSql = ' SELECT
							ffoa.qa_employee_id AS qa_employee_id
						FROM
						    folders_files_owner_associations ffoa
						WHERE
							ffoa.file_path like( \'%' . $arrstrSystemErrorDetials[0]['file_path'] . '%\' )
							AND ffoa.deleted_by IS NULL
							AND ffoa.qa_employee_id IS NOT NULL';

			$arrintQaEmployeeIds = fetchData( $strSql, $objLogDatabase );

			if( false == empty( $arrintQaEmployeeIds[0]['qa_employee_id'] ) ) return $arrintQaEmployeeIds[0]['qa_employee_id'];
		}

		return NULL;

	}

}
?>