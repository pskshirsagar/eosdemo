<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CDbObjects
 * Do not add any new functions to this class.
 */

class CDbObjects extends CBaseDbObjects {

	public static function fetchDbObjectsByDbObjectTypeIdByDatabaseId( $intDbObjectTypeId, $intDatabaseId, $objDatabase ) {

		$strSql = ' SELECT
						*
						FROM
						db_objects
						WHERE
						db_object_type_id = ' . ( int ) $intDbObjectTypeId . ' 
						AND database_id = ' . ( int ) $intDatabaseId;

		return parent::fetchDbObjects( $strSql, $objDatabase );
	}

	public static function fetchDbObjectCountByDbObjectTypeIdByDatabaseId( $intDbObjectType, $intDatabaseId, $objDatabase ) {

		$strWhere = ' WHERE db_object_type_id = ' . ( int ) $intDbObjectType . 'AND database_id = ' . ( int ) $intDatabaseId;

		return self::fetchDbObjectCount( $strWhere, $objDatabase );
	}

	public static function fetchDbObjectStatsByDbObjectTypeIdByDatabaseIdForPagination( $intDbObjectTypeId, $intDatabaseId = NULL, $objDatabase, $intLimit = 0, $intOffset = 0 ) {

		$strCheck = '';

		if( false == is_null( $intDatabaseId ) ) {
			$strCheck .= ' AND database_id = ' . ( int ) $intDatabaseId;
		}

		$strSql = '	SELECT
					    d.*
					FROM
					    db_objects d,
					    db_object_stats dos
					WHERE
						d.id = dos.db_object_id
    					AND db_object_type_id = ' . ( int ) $intDbObjectTypeId . $strCheck;

		if( 0 < $intLimit )
			$strSql .= ' ORDER BY size DESC LIMIT ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset;

		return self::fetchDbObjects( $strSql, $objDatabase );
	}

	public static function fetchDatabaseTableStats( $objDatabase, $intDatabaseId = NULL ) {

		$strWhere = '';
		if( false == is_null( $intDatabaseId ) ) {
			$strWhere = 'AND t.database_id = ' . ( int ) $intDatabaseId;
		}

		$strSql = 'SELECT
					      sub.database_id,
					      count(*) AS table_count
					    FROM
					      (
					        SELECT
					          DISTINCT t.database_id,
					          t.id AS table_id
					        FROM
					          db_objects t
					        WHERE
					          t.db_object_type_id = ' . CDbObjectType::TABLE . $strWhere . '
					        ORDER BY
					          t.database_id, t.id
					      ) AS sub
					    GROUP BY
					      sub.database_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDatabaseFunctionStats( $objDatabase ) {

		$strSql = 'SELECT
						sub.database_id,
						count(*) AS function_count
					FROM
						(
						SELECT
							DISTINCT
								database_id,
								name
						FROM
							db_objects
						WHERE
							db_object_type_id = ' . CDbObjectType::DB_OBJECT_TYPE_FUNCTION . '
						ORDER BY
							database_id, name
						)AS sub
					GROUP BY
						sub.database_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDbObjectsByDbObjectTypeIds( $arrintDbObjectTypeIds, $objDatabase ) {

		if( true == valArr( $arrintDbObjectTypeIds ) ) {
			$arrintDbObjectTypeIds = implode( ',', $arrintDbObjectTypeIds );
		}

		$strSql = '	SELECT
					    *
					FROM
					    db_objects
					WHERE
    					db_object_type_id IN (' . $arrintDbObjectTypeIds . ' )
    				ORDER BY
    					db_object_type_id';

		return self::fetchDbObjects( $strSql, $objDatabase );
	}

}
?>