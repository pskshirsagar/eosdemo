<?php

class CEcsMessageLog extends CBaseEcsMessageLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEcsMessageId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStatus() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSentTo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSentOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReceivedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>