<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CDatabaseTransactionTypes
 * Do not add any new functions to this class.
 */

class CDatabaseTransactionTypes extends CBaseDatabaseTransactionTypes {

	public static function fetchDatabaseTransactionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDatabaseTransactionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDatabaseTransactionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDatabaseTransactionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllDatabaseTransactionTypes( $objDatabase ) {

    	$strSql = 'SELECT * FROM database_transaction_types WHERE is_published = 1';

    	return parent::fetchDatabaseTransactionTypes( $strSql, $objDatabase );
	}
}
?>