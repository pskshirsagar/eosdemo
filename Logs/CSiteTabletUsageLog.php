<?php

class CSiteTabletUsageLog extends CBaseSiteTabletUsageLog {

	const DEVICE_IOS = 'iOS';
	const DEVICE_ANDROID = 'android';

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeviceName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAccessedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function insert( $objDatabase, $boolReturnSqlOnly = false ) {

    	$strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.site_tablet_usage_logs_id_seq\' )' : ( int ) $this->m_intId;

    	$strSql = 'INSERT INTO
					  public.site_tablet_usage_logs
					VALUES ( ' .
    					$strId . ', ' .
    					$this->sqlCid() . ', ' .
    					$this->sqlPropertyId() . ', ' .
    					$this->sqlDeviceName() . ', ' .
    					$this->sqlAccessedOn() . ' ) ' . ' RETURNING id;';

    	if( true == $boolReturnSqlOnly ) {
    		return $strSql;
    	} else {
    		return $this->executeSql( $strSql, $this, $objDatabase );
    	}
    }

    public function delete( $objDatabase, $boolReturnSqlOnly = false ) {

    	$strSql = 'DELETE FROM public.site_tablet_usage_logs WHERE id = ' . ( int ) $this->sqlId() . ';';

    	if( true == $boolReturnSqlOnly ) {
    		return $strSql;
    	} else {
    		return $this->executeSql( $strSql, $this, $objDatabase );
    	}
    }

}
?>