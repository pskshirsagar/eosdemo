<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSessionLogs
 * Do not add any new functions to this class.
 */

class CSessionLogs extends CBaseSessionLogs {

	public static function logSessionInfo( $intPsProductId, $objLogsDatabase, $strClientName = NULL, $strPropertyName = NULL, $strCustomerNameFull = NULL, $strWebsiteName = NULL ) {

		if( 'production' != CONFIG_ENVIRONMENT ) {
			return;
		}

		// Check whether logging is enabled
		$objSessionLogSetting = CSessionLogSettings::fetchSessionLogSettingByPsProductId( $intPsProductId, $objLogsDatabase );

		if( true == valObj( $objSessionLogSetting, 'CSessionLogSetting' ) && 0 < $objSessionLogSetting->getEnableSessionLogging() ) {

			// Create session log entry
			$objSessionLog = new CSessionLog();

			// Gather request data
			if( true == isset( $_REQUEST[REQUEST_MODULE] ) ) {
				$objSessionLog->setModule( $_REQUEST[REQUEST_MODULE] );
			}

			if( true == isset( $_REQUEST[REQUEST_ACTION] ) ) {
				$objSessionLog->setAction( $_REQUEST[REQUEST_ACTION] );
			}

			$strSessionName = ini_get( 'session.name' );

			if( true == isset( $_COOKIE[$strSessionName] ) ) {
				$objSessionLog->setSessionId( $_COOKIE[$strSessionName] );
			} else {
				$objSessionLog->setSessionId( '' );
			}

			$strPrefix	= ( true === CONFIG_IS_SECURE_URL ) ? 'https://' : 'http://';

			$objSessionLog->setRequestUrl( $strPrefix . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );

			$objSessionLog->setServerIp( $_SERVER['SERVER_ADDR'] . ( isset( $_COOKIE['BALANCEID'] ) ? ' [' . $_COOKIE['BALANCEID'] . ']' : '' ) );

			if( true == isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
				$strRequestorIp = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$strRequestorIp = $_SERVER['REMOTE_ADDR'];
			}

			$objSessionLog->setRequesterIp( $strRequestorIp );

			// Gather arguments
			$objSessionLog->setPsProductId( $intPsProductId );
			$objSessionLog->setClientName( $strClientName );
			$objSessionLog->setPropertyName( $strPropertyName );
			$objSessionLog->setCustomerNameFull( $strCustomerNameFull );
			$objSessionLog->setWebsiteName( $strWebsiteName );

			// Insert record
			$objSessionLog->insertOrUpdate( SYSTEM_USER_ID, $objLogsDatabase );
		}
	}

}
?>