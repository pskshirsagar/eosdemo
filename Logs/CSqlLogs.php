<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSqlLogs
 * Do not add any new functions to this class.
 */

class CSqlLogs extends CBaseSqlLogs {

	public static function fetchSqlLogs( $strSql, $objDatabase ) {
		$arrobjSqlLogs = NULL;
		$objSqlLog = NULL;

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$arrobjSqlLogs = array();

			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();

				$objSqlLog = new CSqlLog();
				$objSqlLog->setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );
				$objSqlLog->setSerializedOriginalValues( serialize( $arrmixValues ) );

				if( false == is_null( $objSqlLog->getDatabaseId() ) ) {
					$arrobjSqlLogs[$objSqlLog->getDatabaseId()] = $objSqlLog;
				} else {
					$arrobjSqlLogs[] = $objSqlLog;
				}

				$objDataset->next();
			}
		}

		$objDataset->cleanup();

		return $arrobjSqlLogs;
	}

	public static function fetchSqlLog( $strSql, $objDatabase ) {
		$objSqlLog = NULL;

		$arrobjSqlLogs = self::fetchSqlLogs( $strSql, $objDatabase );
		if( true == valArr( $arrobjSqlLogs ) ) {
			if( 1 < \Psi\Libraries\UtilFunctions\count( $arrobjSqlLogs ) ) {
				trigger_error( 'Expecting a single record when multiple records returned. Sql = ' . $strSql, E_USER_WARNING );
				return NULL;
			}

			$objSqlLog = array_shift( $arrobjSqlLogs );
		}

		return $objSqlLog;
	}

	public static function fetchDuplicateSqlLogRecords( $intPsProductId, $intDatabaseId, $intSqlStatementId, $strModule, $strAction, $strDate, $objDatabase ) {

		$strSql	= 'SELECT
					    *
					FROM
					    sql_logs sl
					WHERE
					    sl.database_id = ' . ( int ) $intDatabaseId . '
					    AND sl.ps_product_id = ' . ( int ) $intPsProductId . '
					    AND sl.sql_statement_id = ' . ( int ) $intSqlStatementId . '
					    AND sl.module LIKE \'' . $strModule . '\'
					    AND sl.action LIKE \'' . $strAction . '\'
					    AND date_part ( \'year\', sl.month ) = ' . date( 'Y', strtotime( $strDate ) ) . '
					    AND date_part ( \'month\', sl.month ) = ' . date( 'm', strtotime( $strDate ) ) . '
					LIMIT 1;';

		return fetchData( $strSql, $objDatabase );
	}
}
?>