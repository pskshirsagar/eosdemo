<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CDatabaseTransactions
 * Do not add any new functions to this class.
 */

class CDatabaseTransactions extends CBaseDatabaseTransactions {

	public static function fetchPaginatedDatabaseTransactionsByDatabaseId( $intPageNo, $intPageSize, $intDatabaseId, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = ' SELECT
						*
					FROM
						database_transactions
					WHERE
						database_id = ' . ( int ) $intDatabaseId . '
					ORDER BY id DESC
					OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchDatabaseTransactions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedDatabasesTransactionsCountByDatabaseId( $intDatabaseId, $objDatabase ) {
		$strWhere = ' WHERE database_id = ' . ( int ) $intDatabaseId;
		return self::fetchDatabaseTransactionCount( $strWhere, $objDatabase );
	}

	public static function fetchPaginatedDatabaseTransactionsByScriptIdByDatabaseId( $intPageNo, $intPageSize, $intScriptId, $intDatabaseId, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = ' SELECT
						*
					FROM
						database_transactions
					WHERE
						script_id = ' . ( int ) $intScriptId . '
						AND database_id = ' . ( int ) $intDatabaseId . '
					ORDER BY id DESC
					OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchDatabaseTransactions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedDatabasesTransactionsCountByScriptId( $intScriptId, $objDatabase ) {
		$strSql = 'SELECT COUNT( * )
							FROM (
									select *,
									row_number() OVER(PARTITION BY parent_transaction_id
											ORDER BY dt_ids DESC)
									from (
											SELECT CASE
											WHEN MIN(dt.database_transaction_id) IS NULL
											THEN min(dt.id)
											ELSE min(dt.database_transaction_id)
											END as parent_transaction_id,
											array_to_string(array_agg(dt.id), \',\') as
											child_transaction_ids,
											SUM(1) AS child_database_transaction_count,
											array_agg(dt.id) as dt_ids,
											count(CASE
													WHEN dt.end_datetime IS NOT NULL THEN 1
													END) AS completed_database_transaction_count,
											count(CASE
													WHEN dt.failed_on IS NOT NULL THEN 1
													END) AS failed_database_transaction_count
											FROM database_transactions dt
											WHERE dt.script_id = ' . ( int ) $intScriptId . '
											GROUP BY (COALESCE((database_transaction_id)::double precision,
													date_part( \'epoch\'::text, func_date_trunc_minute(begin_datetime)
													)))
											ORDER BY (COALESCE((database_transaction_id)::double precision,
													date_part( \'epoch\'::text, func_date_trunc_minute(begin_datetime)
													)))
									) as sub
							) AS sub_query2
							JOIN database_transactions dt1 ON (dt1.id = sub_query2.parent_transaction_id)
							where row_number = 1';

		$arrintCount = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrintCount[0]['count'] ) ) ? ( int ) $arrintCount[0]['count'] : 0;
	}

	public static function fetchDatabaseTransactionsByDayByScriptIds( $strDay, $arrintScriptIds, $objDatabase ) {
		if( false == valArr( $arrintScriptIds ) ) return NULL;

		$strSql = 'SELECT * FROM database_transactions
						WHERE
							func_date_trunc_day( begin_datetime ) = \'' . $strDay . '\'
							AND script_id IN ( ' . implode( ',', $arrintScriptIds ) . ' )
						ORDER BY begin_datetime';

		return self::fetchDatabaseTransactions( $strSql, $objDatabase );
	}

	public static function fetchDatabaseTransactionsByDateByScriptIds( $strDay, $arrintScriptIds, $objDatabase ) {

		if( $strDay == date( 'm/d/Y' ) ) {
			$strDateCondition = 'func_date_trunc_day(begin_datetime) > \'' . $strDay . '\'::date - INTERVAL \'10 day\'';
		} else {
			$strDateCondition = 'func_date_trunc_day( begin_datetime ) = \'' . $strDay . '\'';
		}
		$strSql = 'SELECT
						script_id,
						max(begin_datetime) AS begin_datetime
					FROM
						database_transactions
					WHERE '
						. $strDateCondition . ' AND script_id IN (' . implode( ',', $arrintScriptIds ) . ' )
					GROUP BY
						script_id';

		return self::fetchDatabaseTransactions( $strSql, $objDatabase );
	}

	public static function fetchLatestDatabaseTransancetionByScriptId( $intScriptId, $objDatabase ) {

		$strSql = ' SELECT
						dt.script_id,
						dt.begin_datetime,
						dt.end_datetime
					FROM
						database_transactions dt
					WHERE
						dt.begin_datetime IS NOT NULL
						AND dt.end_datetime IS NOT NULL
						AND script_id = ' . ( int ) $intScriptId . '
					 LIMIT 1';

		return self::fetchDatabaseTransactions( $strSql, $objDatabase );
	}

	public static function fetchDatabaseTransactionsByIds( $arrintIds, $objDatabase ) {

		$strSql = ' SELECT
						dt.*
					FROM
						database_transactions dt
					WHERE
						id IN ( ' . implode( ',', $arrintIds ) . ' )';

		return self::fetchDatabaseTransactions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedParentDatabaseTransactionsByDatabaseId( $intPageNo, $intPageSize, $intDatabaseId, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = ' SELECT
						*
					FROM
						database_transactions
					WHERE
						database_id = ' . ( int ) $intDatabaseId . '
					ORDER BY
						id DESC
					OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedParentDatabaseTransactionsByScriptId( $intPageNo, $intPageSize, $intScriptId, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = 'SELECT *
							FROM (
									select *,
									row_number() OVER(PARTITION BY parent_transaction_id
											ORDER BY dt_ids DESC)
									from (
											SELECT CASE
											WHEN MIN(dt.database_transaction_id) IS NULL
											THEN min(dt.id)
											ELSE min(dt.database_transaction_id)
											END as parent_transaction_id,
											array_to_string(array_agg(dt.id), \',\') as
											child_transaction_ids,
											SUM(1) AS child_database_transaction_count,
											array_agg(dt.id) as dt_ids,
											count(CASE
													WHEN dt.end_datetime IS NOT NULL THEN 1
													END) AS completed_database_transaction_count,
											count(CASE
													WHEN dt.failed_on IS NOT NULL THEN 1
													END) AS failed_database_transaction_count,
											count(CASE
													WHEN dt.dismissed_on IS NOT NULL THEN 1
													END) AS dismiss_database_transaction_count
											FROM database_transactions dt
											WHERE dt.script_id = ' . ( int ) $intScriptId . '
											GROUP BY (COALESCE((database_transaction_id)::double precision,
													date_part( \'epoch\'::text, func_date_trunc_minute(begin_datetime)
													)))
											ORDER BY (COALESCE((database_transaction_id)::double precision,
													date_part( \'epoch\'::text, func_date_trunc_minute(begin_datetime)
													))) DESC
											LIMIT ' . ( int ) $intLimit * $intPageNo * 2 . '
									) as sub
							) AS sub_query2
							JOIN database_transactions dt1 ON (dt1.id = sub_query2.parent_transaction_id)
							WHERE row_number = 1
				ORDER BY dt1.begin_Datetime DESC
				OFFSET ' . ( int ) $intOffset . '
				LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveDatabaseTransactionsByHardwareId( $intHardwarId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						database_transactions
					WHERE
						end_datetime IS NULL
						AND database_transaction_type_id != 2
						AND hardware_id = ' . ( int ) $intHardwarId . '
						AND begin_datetime > \'' . date( 'm/d/Y H:i:s' ) . '\'::timestamp - interval \'48 hour\'';

		return self::fetchDatabaseTransactions( $strSql, $objDatabase );
	}

	public static function fetchActiveDatabaseTransactionById( $intId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						database_transactions
					WHERE
						id = ' . ( int ) $intId . '
						AND end_datetime IS NULL';

		return self::fetchDatabaseTransaction( $strSql, $objDatabase );
	}

	public static function fetchChildDatabaseTransactionsByIds( $arrintIds, $objDatabase ) {

		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = ' SELECT
						dt.*
					FROM
						database_transactions dt
					WHERE
						dt.id IN ( ' . implode( ',', $arrintIds ) . ' )
					ORDER BY
						dt.database_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLatestDatabaseTransactionByScriptIdByEndDatetime( $intScriptId, $strEndDatetime, $objDatabase ) {

		$strSql = 'SELECT
						id
					FROM
						database_transactions
					WHERE
						script_id = ' . ( int ) $intScriptId . '
						AND end_datetime::DATE = \'' . $strEndDatetime . '\'
					ORDER BY
							id DESC
					LIMIT 1';

		return self::fetchDatabaseTransaction( $strSql, $objDatabase );
	}

	public static function fetchFailedDatabaseTransactionsByFailedOnByScriptIds( $strDay, $arrintScriptIds, $objDatabase ) {
		$strSql = 'SELECT
                        Distinct script_id
					FROM
						database_transactions
					WHERE
						func_date_trunc_day( begin_datetime ) = \'' . $strDay . '\'
		                AND func_date_trunc_day( failed_on ) = \'' . $strDay . '\'
		                AND failed_on IS NOT NULL
		                AND script_id IN (' . implode( ',', $arrintScriptIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchStartedDatabaseTransactionsByBeginDatetime( $strDay, $arrintScriptIds, $objDatabase ) {
		$strSql = 'SELECT 
						Distinct script_id
					FROM 
						database_transactions
					WHERE 
						func_date_trunc_day( begin_datetime ) = \'' . $strDay . '\' AND script_id IN ( ' . implode( ',', $arrintScriptIds ) . ' )';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDatabaseTransactionsByScriptIdByDatabaseTransactionTypeId( $intScriptId, $intDatabaseTransactionTypeId, $objDatabase ) {

		$strSql = 'SELECT
						script_id,
                        begin_datetime,
                        failed_on,
                        end_datetime
					FROM
                        database_transactions
					WHERE
                        script_id = ' . ( int ) $intScriptId . '
                        AND database_transaction_type_id = ' . ( int ) $intDatabaseTransactionTypeId . '
                        LIMIT 3';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomDatabaseTransactionsByScriptId( $intScriptId, $objDatabase ) {

		if( CScript::SCREENING_CHECK_STATUS == $intScriptId ) {
			$strTimeInterval = 'INTERVAL \'150\' MINUTE';
			$strExtractHour = 'EXTRACT ( \'hour\'
								FROM
								begin_datetime ) AS data_capture_hour';
		} else {
			$strTimeInterval = 'INTERVAL \'72\' HOUR';
			$strExtractHour = 'EXTRACT ( \'day\'
								FROM
								begin_datetime ) AS data_capture_hour';
		}

		$strSql = ' SELECT
						script_id,
						database_id,
						begin_datetime,
						failed_on,
						CASE
						WHEN failed_on IS NOT NULL THEN \'Error\'
						ELSE \'Success\'
						END AS script_status,
						' . $strExtractHour . '
					FROM
						(
							SELECT
								MAX ( begin_datetime ) AS created_on_date
							FROM
								database_transactions
							WHERE
								script_id = ' . ( int ) $intScriptId . '
								AND database_transaction_type_id = ' . CDatabaseTransactionType::CONFIG_SCHEDULED_SCRIPT . '
						) AS max_created_on
					JOIN database_transactions dt ON dt.begin_datetime BETWEEN created_on_date - ' . $strTimeInterval . ' AND created_on_date
					WHERE
						script_id = ' . ( int ) $intScriptId . '
						AND database_transaction_type_id = ' . CDatabaseTransactionType::CONFIG_SCHEDULED_SCRIPT . '
					ORDER BY
						begin_datetime DESC';

		return fetchData( $strSql, $objDatabase );
	}

}
?>