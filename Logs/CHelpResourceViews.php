<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CHelpResourceViews
 * Do not add any new functions to this class.
 */

class CHelpResourceViews extends CBaseHelpResourceViews {

	public static function fetchHelpResourceViewsCountByDate( $strTodayDate, $objDatabase ) {

		$strWhereCondition = '';

		if( true == isset( $strTodayDate ) ) {
			$strWhereCondition = ' WHERE \'' . $strTodayDate . '\' = date_trunc( \'day\', hrv.created_on )';
		}

		$strSql = 'SELECT
						DISTINCT( hrv.help_resource_id ) as help_resource_id,
						COUNT( hrv.help_resource_id ) as views_count
					FROM
						help_resource_views hrv' . $strWhereCondition . '
					GROUP BY
						hrv.help_resource_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchHelpResourceViewsByHelpResourceIdByFilter( $intHelpResourceId, $arrstrViewsFilter, $objDatabase ) {

		$intOffset	= ( 0 < $arrstrViewsFilter['page_no'] ) ? $arrstrViewsFilter['page_size'] * ( $arrstrViewsFilter['page_no'] - 1 ) : 0;
		$intLimit	= $arrstrViewsFilter['page_size'];
		$strOrderByClause = ' hrv.' . $arrstrViewsFilter['order_by_field'];
		$strWhereCondition = ' AND date_trunc( \'day\', hrv.created_on ) < NOW() - INTERVAL \' 1 days\'';

		if( 'company_name' == $arrstrViewsFilter['order_by_field'] ) {
			$strOrderByClause = ' c.' . $arrstrViewsFilter['order_by_field'];
		}

		if( true == valStr( $arrstrViewsFilter['from_date'] ) && true == valStr( $arrstrViewsFilter['to_date'] ) ) {
			$strWhereCondition = ' AND hrv.created_on BETWEEN \' ' . $arrstrViewsFilter['from_date'] . ' 00:00:00\' AND \' ' . $arrstrViewsFilter['to_date'] . ' 23:59:59\' 
									AND c.company_status_type_id IN (' . CCompanyStatusType::CLIENT . ')';
		}

		if( true == valArr( $arrstrViewsFilter['clients'] ) ) {
			$strWhereCondition .= ' AND hrv.cid IN (' . implode( ',', $arrstrViewsFilter['clients'] ) . ')';
		}

		$strSql = 'SELECT
						hrv.created_on,
						hrv.view_by_company_user_name,
						hrv.company_user_role,
						c.company_name
					FROM
						help_resource_views hrv
						LEFT JOIN clients c ON c.id = hrv.cid
					WHERE
						hrv.help_resource_id = ' . ( int ) $intHelpResourceId . '
						' . $strWhereCondition . '
					Order by ' . $strOrderByClause . ' ' . $arrstrViewsFilter['order_by_type'];

		if( false == is_null( $intLimit ) ) {
			$strSql .= ' OFFSET	' . ( int ) $intOffset . ' LIMIT	' . ( int ) $intLimit;
		}

		$arrmixHelpResourceViews['data'] = fetchData( $strSql, $objDatabase );

		$strCountSql = 'SELECT
						count( hrv.* )
					FROM
						help_resource_views hrv
						LEFT JOIN clients c ON c.id = hrv.cid
					WHERE
						hrv.help_resource_id = ' . ( int ) $intHelpResourceId . '
						' . $strWhereCondition;

		$arrmixHelpResourceViews['count'] = parent::fetchColumn( $strCountSql, 'count', $objDatabase );
		return $arrmixHelpResourceViews;
	}

	public static function fetchHelpResourceViewsByFilter( $boolIsDownload, $arrmixLmsReportFilter, $strOrderBy = NULL, $strOrderByField = NULL, $objDatabase ) {
		if( true == is_null( $arrmixLmsReportFilter['from_date'] ) || true == is_null( $arrmixLmsReportFilter['to_date'] ) ) {
			return false;
		}

		$strWhereCondition = 'AND c.company_status_type_id IN (' . CCompanyStatusType::CLIENT . ')';
		$strInnerWhereCondition = 'AND c.company_status_type_id IN (' . CCompanyStatusType::CLIENT . ')';
		$strOrderClause = ' ORDER BY  ' . $strOrderByField . ' ' . $strOrderBy;

		$intOffset = ( 0 < $arrmixLmsReportFilter['page_no'] ) ? $arrmixLmsReportFilter['page_size'] * ( $arrmixLmsReportFilter['page_no'] - 1 ) : 0;
		$intLimit  = ( int ) $arrmixLmsReportFilter['page_size'];

		if( true == valArr( $arrmixLmsReportFilter['help_resource_ids'] ) ) {
			$strWhereCondition .= 'AND hrv.help_resource_id IN (' . implode( ',', $arrmixLmsReportFilter['help_resource_ids'] ) . ')';
		}

		if( false == is_null( $arrmixLmsReportFilter['clients'] ) ) {
			$strWhereCondition .= ' AND hrv.cid IN (' . implode( ',', $arrmixLmsReportFilter['clients'] ) . ')';
			$strInnerWhereCondition = ' AND hrc.cid IN (' . implode( ',', $arrmixLmsReportFilter['clients'] ) . ')';
		}

		$strSql = 'SELECT
					hrv.help_resource_id,
					count(*) as view_count,
					CASE
						WHEN hrc1.comment_count > 0 THEN hrc1.comment_count
						ELSE 0
					END AS comment_count
				FROM
					help_resource_views hrv 
					LEFT JOIN clients c ON c.id = hrv.cid
					LEFT JOIN 
					(
					 SELECT  hrc.help_resource_id,
							count(*) as comment_count
						FROM
							help_resource_comments hrc LEFT JOIN clients c ON c.id = hrc.cid
						WHERE
							hrc.created_on BETWEEN \' ' . $arrmixLmsReportFilter['from_date'] . ' 00:00:00 \' AND \' ' . $arrmixLmsReportFilter['to_date'] . ' 23:59:59 \'' . $strInnerWhereCondition . '
						group by hrc.help_resource_id 
					)hrc1 ON hrv.help_resource_id = hrc1.help_resource_id
				WHERE
					hrv.created_on BETWEEN \' ' . $arrmixLmsReportFilter['from_date'] . ' 00:00:00 \' AND \' ' . $arrmixLmsReportFilter['to_date'] . ' 23:59:59 \'' . $strWhereCondition . '
				group by hrv.help_resource_id,hrc1.comment_count'
				. $strOrderClause;

		if( false == $boolIsDownload ) {
			$strSql .= ' OFFSET	' . ( int ) $intOffset . ' LIMIT	' . ( int ) $intLimit;
		}
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchHelpResourceViewsCountByFilter( $arrmixLmsReportFilter, $objDatabase ) {
		if( true == is_null( $arrmixLmsReportFilter['from_date'] ) || true == is_null( $arrmixLmsReportFilter['to_date'] ) ) {
			return false;
		}

		$strWhereCondition = 'AND c.company_status_type_id IN (' . CCompanyStatusType::CLIENT . ')';

		if( true == valArr( $arrmixLmsReportFilter['help_resource_ids'] ) ) {
			$strWhereCondition .= 'AND hrv.help_resource_id IN (' . implode( ',', $arrmixLmsReportFilter['help_resource_ids'] ) . ')';
		}

		if( false == is_null( $arrmixLmsReportFilter['clients'] ) ) {
			$strWhereCondition .= ' AND hrv.cid IN (' . implode( ',', $arrmixLmsReportFilter['clients'] ) . ')';
		}

		if( false == is_null( $arrmixLmsReportFilter['designations'] ) ) {
			$strWhereCondition .= ' AND hrv.company_user_role IN (\'' . implode( "','", $arrmixLmsReportFilter['designations'] ) . '\')';
		}

		$strSql = 'SELECT 
						COUNT( lms_records.* )
					FROM
						(SELECT
							hrv.help_resource_id
						FROM
							help_resource_views hrv
							LEFT JOIN clients c ON c.id = hrv.cid
						WHERE
							hrv.created_on BETWEEN \' ' . $arrmixLmsReportFilter['from_date'] . ' 00:00:00 \' AND \' ' . $arrmixLmsReportFilter['to_date'] . ' 23:59:59 \'' . $strWhereCondition . '
							group by hrv.help_resource_id
						)lms_records';

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

}
?>