<?php

class CModuleAnalyticStat extends CBaseModuleAnalyticStat {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAppId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUserHitDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valModuleName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valModuleHitDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>