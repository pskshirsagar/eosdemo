<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CHelpResourceRatingHoldings
 * Do not add any new functions to this class.
 */

class CHelpResourceRatingHoldings extends CBaseHelpResourceRatingHoldings {

	public static function fetchHelpResourceRatingHoldingsToSync( $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT( hrrh.help_resource_id ) as help_resource_id,
						count( CASE WHEN hrrh.rating_value = 1 THEN hrrh.id END ) as yes_count,
						count( CASE WHEN hrrh.rating_value = 0 THEN hrrh.id END ) as no_count,
						count( hrrh.comment ) as comments_count
					FROM
						help_resource_rating_holdings hrrh
					GROUP BY
						hrrh.help_resource_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchHelpResourceFeedbackByHelpResourceIds( $arrintHelpResourceids, $objDatabase ) {

		if( false == valArr( $arrintHelpResourceids ) ) return NULL;

		$strSql = 'SELECT
						hrrh.cid,
						hrrh.help_resource_id,
						hrrh.rating_value,
						hrrh.comment,
						hrrh.comment_by_company_user_name,
						hrrh.company_user_role,
						hrrh.company_name,
						hrrh.created_on,
						hrrh.created_by
					FROM
						help_resource_rating_holdings hrrh
					WHERE
						hrrh.help_resource_id IN ( ' . implode( ', ', $arrintHelpResourceids ) . ' )
						AND hrrh.comment IS NOT NULL';

		return parent::fetchHelpResourceRatingHoldings( $strSql, $objDatabase );
	}

	public static function buildDeleteSqlForHelpResourceRatingHoldingsByHelpResourceIds( $arrintHelpResourceids ) {

		if( false == valArr( $arrintHelpResourceids ) ) return NULL;

		$strSql = 'DELETE
					FROM
						help_resource_rating_holdings hrrh
					WHERE
						hrrh.help_resource_id IN ( ' . implode( ', ', $arrintHelpResourceids ) . ' )';

		return $strSql;
	}

}
?>