<?php

class CRestrictedIp extends CBaseRestrictedIp {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIpAddress() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsCompetitorId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	public function insert( $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.restricted_ips_id_seq\' )' : ( int ) $this->m_intId;

		$strSql = 'INSERT INTO
					  public.restricted_ips
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlIpAddress() . ', ' .
						$this->sqlPsCompetitorId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						public.restricted_ips
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' ip_address = ' . $this->sqlIpAddress() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlIpAddress() ) != $this->getOriginalValueByFieldName( 'ip_address' ) ) {
			$arrstrOriginalValueChanges['ip_address'] = $this->sqlIpAddress();
			$strSql .= ' ip_address = ' . $this->sqlIpAddress() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' ps_competitor_id = ' . $this->sqlPsCompetitorId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlPsCompetitorId() ) != $this->getOriginalValueByFieldName( 'ps_competitor_id' ) ) {
			$arrstrOriginalValueChanges['ps_competitor_id'] = $this->sqlPsCompetitorId();
			$strSql .= ' ps_competitor_id = ' . $this->sqlPsCompetitorId() . ',';
			$boolUpdate = true;
		}

		$strSql = \Psi\CStringService::singleton()->substr( $strSql, 0, -1 );

		$strSql .= ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM public.restricted_ips WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

}
?>