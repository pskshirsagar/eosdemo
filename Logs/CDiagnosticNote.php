<?php

class CDiagnosticNote extends CBaseDiagnosticNote {

	protected $m_strDiagnosticNoteTypeName;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['diagnostic_note_type_name'] ) )		$this->setDiagnosticNoteTypeName( $arrmixValues['diagnostic_note_type_name'] );
		return;
	}

	public function setDiagnosticNoteTypeName( $strDiagnosticNoteTypeName ) {
		$this->m_strDiagnosticNoteTypeName = $strDiagnosticNoteTypeName;
	}

	public function getDiagnosticNoteTypeName() {
		return $this->m_strDiagnosticNoteTypeName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDiagnosticId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {

		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDiagnosticNote() {

		$boolIsValid = true;

		if( true == is_null( $this->getDiagnosticNote() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'diagnostic_note', 'Diagnostic note is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSql() {

		$boolIsValid = true;

		if( true == is_null( $this->getSql() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sql', 'SQL is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDiagnosticNoteDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDiagnosticNote();
				break;

			case VALIDATE_UPDATE:
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>