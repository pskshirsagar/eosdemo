<?php

class CRestrictedIpRequest extends CBaseRestrictedIpRequest {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRestrictedIpId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRequestDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUserAgentInfo() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUrl() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRequestData() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsReviewed() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>