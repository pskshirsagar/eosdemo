<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CHelpLrsUserInteractiveCourseDetails
 * Do not add any new functions to this class.
 */

class CHelpLrsUserInteractiveCourseDetails extends CBaseHelpLrsUserInteractiveCourseDetails {

	public static function fetchHelpLrsUserInteractiveCourseDetailsByUserIdsByHelpResourceIdsByCids( $arrintUserIds, $arrintHelpResourceIds, $arrintCids, $objDatabase ) {
		$strSql = 'SELECT * FROM help_lrs_user_interactive_course_details WHERE cid IN (' . implode( ',', $arrintCids ) . ') AND  user_id IN (' . implode( ',', $arrintUserIds ) . ') AND help_resource_id IN (' . implode( ',', $arrintHelpResourceIds ) . ')';

		return self::fetchObjects( $strSql, CHelpLrsUserInteractiveCourseDetail::class, $objDatabase );
	}

	public static function fetchHelpLrsUserInteractiveCourseDetailsByEmailAddresses( $arrstrEmailAddresses, $objDatabase ) {
		$strSql = 'SELECT * FROM help_lrs_user_interactive_course_details WHERE email_address IN (\'' . implode( '\',\'', $arrstrEmailAddresses ) . '\') AND username IS NULL';

		return self::fetchObjects( $strSql, CHelpLrsUserInteractiveCourseDetail::class, $objDatabase );
	}

}
?>