<?php

class CDiagnosticResultType extends CBaseDiagnosticResultType {

	const SUCCESS 		= 1;
	const FAILURE 		= 2;
	const SQL_ERROR 	= 3;
	const TIME_OUT 		= 4;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public static function getDiagnosticResultTypeId( $intCount, $arrstrResults ) {

		if( true == isset ( $arrstrResults['failed'] ) && true == isset ( $arrstrResults['failure_type'] ) && 1 == $arrstrResults['failed'] && false !== \Psi\CStringService::singleton()->stristr( $arrstrResults['failure_type'], 'statement timeout' ) ) {
			return self::TIME_OUT;
		} elseif( true == isset( $arrstrResults['failed'] ) && 1 == $arrstrResults['failed'] ) {
			return  self::SQL_ERROR;
		} elseif( 0 == $intCount ) {
			return self::SUCCESS;
		} else {
			return self::FAILURE;
		}

	}

}
?>