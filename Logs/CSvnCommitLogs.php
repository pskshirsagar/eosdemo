<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSvnCommitLogs
 * Do not add any new functions to this class.
 */

class CSvnCommitLogs extends CBaseSvnCommitLogs {

	public static function fetchLatestSvnCommitLogsBySvnRepositoryIds( $arrintSvnRepositoryIds, $objDatabase ) {
		$strSql = 'SELECT DISTINCT svn_repository_id, MAX( revision_id::INTEGER ) OVER ( PARTITION BY svn_repository_id ) AS revision_id FROM svn_commit_logs WHERE svn_repository_id IN ( \'' . implode( '\',\'', $arrintSvnRepositoryIds ) . '\' )';

		return parent::fetchSvnCommitLogs( $strSql, $objDatabase );
	}

	public static function fetchSvnCommitLogCountByUserIdsByDate( $arrintUserIds, $strStartDate, $strEndDate, $objDatabase ) {

		if( false == valArr( $arrintUserIds ) || false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) return NULL;

		$strSql = 'SELECT user_id, count( id ) FROM svn_commit_logs WHERE user_id IN ( ' . implode( ',', $arrintUserIds ) . ' ) AND date_trunc( \'day\', commit_datetime ) BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\' GROUP BY user_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSvnCommitLogsByIdsByUserIds( $arrintIds, $arrintUserIds, $objDatabase ) {

		if( false == valArr( $arrintIds ) || false == valArr( $arrintUserIds ) ) return NULL;

		$strSql = 'SELECT
						scl.user_id,
						scl.id
					FROM
						svn_commit_logs scl
					WHERE
						scl.id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND scl.user_id IN ( ' . implode( ',', $arrintUserIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSvnCommitDateByUserIdsByDateRange( $arrintUserIds, $strStartDate, $strEndDate, $objDatabase ) {

		if( false == valArr( $arrintUserIds ) || false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) return NULL;

		$strSql = 'SELECT commit_datetime::date FROM svn_commit_logs WHERE user_id IN ( ' . implode( ',', $arrintUserIds ) . ' ) AND date_trunc( \'day\', commit_datetime ) BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\' ORDER BY revision_id';

		return parent::fetchSvnCommitLogs( $strSql, $objDatabase );
	}

	public static function fetchSvnCommitLogsByTaskIdBySvnRepositoryIds( $arrintTaskIds, $arrintSvnRepositoryIds, $objDatabase, $boolIsCodeAuditorSet = true ) {

		if( false == valArr( $arrintSvnRepositoryIds ) || false == valArr( $arrintTaskIds ) ) return NULL;

		$strWhereClause = '';

		if( false == $boolIsCodeAuditorSet ) {
			$strWhereClause = ' AND code_auditor_id IS NULL
			  						AND task_id NOT IN (	SELECT
																task_id
															FROM
																svn_commit_logs scl
																JOIN svn_commit_log_tasks sclt ON ( scl.id = sclt.svn_commit_log_id )
															WHERE
																code_auditor_id IS NOT NULL
																AND task_id IN ( ' . implode( ',', $arrintTaskIds ) . ')
															GROUP BY
																task_id
														)';
		}

		$strSql = 'SELECT
						scl.*, task_id
					FROM
						svn_commit_logs scl
						JOIN svn_commit_log_tasks sclt ON ( scl.id = sclt.svn_commit_log_id )
					WHERE
						svn_repository_id IN ( ' . implode( ',', $arrintSvnRepositoryIds ) . ')
						AND user_id IS NOT NULL
						AND task_id IN ( ' . implode( ',', $arrintTaskIds ) . ')'
						. $strWhereClause . '
					ORDER BY
						svn_commit_files_count DESC';

		return parent::fetchSvnCommitLogs( $strSql, $objDatabase );
	}

	public static function fetchSvnCommitLogsBySvnRepositoryIdByCommitDatetime( $intRepositoryId, $intRevisionId, $strStartDate, $strEndDate, $strCondition, $objDatabase ) {

		$strDateCondition = 'scl.revision_id::INTEGER >' . ( int ) $intRevisionId;

		if( true == is_null( $intRevisionId ) ) {
			$strDateCondition = 'scl.commit_datetime::date <=\'' . $strEndDate . '\'::date AND scl.commit_datetime::date >=\'' . $strStartDate . '\'::date';
		}

		$strSql = 'SELECT
					    scl.*, sclt.task_id
					FROM
					    svn_commit_logs scl
					    LEFT JOIN svn_commit_log_tasks AS sclt ON ( scl.id = sclt.svn_commit_log_id )
					WHERE
                    	' . $strDateCondition . '
					    AND scl.svn_repository_id = ' . ( int ) $intRepositoryId . '
					    AND scl.svn_username != \'release\'
					ORDER BY
					    scl.id DESC ' . $strCondition;

		return parent::fetchSvnCommitLogs( $strSql, $objDatabase );
	}

	public static function fetchSvnCommitLogsBySvnRepositoryIdByCommitDatetimeCount( $intRepositoryId, $intRevisionId, $strStartDate, $strEndDate, $objDatabase ) {

		$strDateCondition = 'scl.revision_id::INTEGER >' . ( int ) $intRevisionId;

		if( true == is_null( $intRevisionId ) ) {
			$strDateCondition = 'scl.commit_datetime::date <=\'' . $strEndDate . '\'::date AND scl.commit_datetime::date >=\'' . $strStartDate . '\'::date';
		}

		$strSql = 'SELECT
					    scl.*, sclt.task_id
					FROM
					    svn_commit_logs scl
					    LEFT JOIN svn_commit_log_tasks AS sclt ON ( scl.id = sclt.svn_commit_log_id )
					WHERE
                    	' . $strDateCondition . '
					    AND scl.svn_repository_id = ' . ( int ) $intRepositoryId . '
					    AND scl.svn_username != \'release\'
					ORDER BY
					    scl.id DESC ';

		return parent::fetchSvnCommitLogs( $strSql, $objDatabase );
	}

	public static function fetchSvnCommitLogsBySvnRepositoryIdByCommitDatetimeByTaskIds( $intRepositoryId, $intRevisionId, $strStartDate, $strEndDate, $arrintTaskids, $strCondition, $objDatabase ) {

		$strDateCondition = 'scl.revision_id::INTEGER >' . ( int ) $intRevisionId;

		if( true == is_null( $intRevisionId ) ) {
			$strDateCondition = 'scl.commit_datetime::date <=\'' . $strEndDate . '\'::date AND scl.commit_datetime::date >=\'' . $strStartDate . '\'::date';
		}

		$strSql = 'SELECT
					    scl.*, sclt.task_id
					FROM
					    svn_commit_logs scl
					    LEFT JOIN svn_commit_log_tasks AS sclt ON ( scl.id = sclt.svn_commit_log_id )
					WHERE
                    	' . $strDateCondition . '
					    AND scl.svn_repository_id = ' . ( int ) $intRepositoryId . '
					    AND scl.svn_username != \'release\'
					    AND sclt.task_id IN(  ' . implode( ',', $arrintTaskids ) . ' )
					ORDER BY
					    scl.id DESC ' . $strCondition;
		return parent::fetchSvnCommitLogs( $strSql, $objDatabase );
	}

	public static function fetchSvnCommitLogsBySvnRepositoryIdByCommitDatetimeByTaskIdsCount( $intRepositoryId, $intRevisionId, $strStartDate, $strEndDate, $arrintTaskids, $objDatabase ) {

		$strDateCondition = 'scl.revision_id::INTEGER >' . ( int ) $intRevisionId;

		if( true == is_null( $intRevisionId ) ) {
			$strDateCondition = 'scl.commit_datetime::date <=\'' . $strEndDate . '\'::date AND scl.commit_datetime::date >=\'' . $strStartDate . '\'::date';
		}

		$strSql = 'SELECT
					    scl.*, sclt.task_id
					FROM
					    svn_commit_logs scl
					    LEFT JOIN svn_commit_log_tasks AS sclt ON ( scl.id = sclt.svn_commit_log_id )
					WHERE
                    	' . $strDateCondition . '
					    AND scl.svn_repository_id = ' . ( int ) $intRepositoryId . '
					    AND scl.svn_username != \'release\'
					    AND sclt.task_id IN(  ' . implode( ',', $arrintTaskids ) . ' )
					ORDER BY
					    scl.id DESC ';
		return parent::fetchSvnCommitLogs( $strSql, $objDatabase );
	}

	public static function fetchWeeklySvnCommitLogs( $objDatabase ) {

		$strSql = ' SELECT
						scl.svn_repository_id,
						MIN( scl.revision_id ) as revision_id
					FROM
						svn_commit_logs scl
					WHERE
						scl.commit_datetime >= NOW() - INTERVAL \'1 week\'
					GROUP BY
						scl.svn_repository_id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>