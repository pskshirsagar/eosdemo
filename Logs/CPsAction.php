<?php

class CPsAction extends CBasePsAction {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReviewTaskId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valModuleName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valActionName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRecentLoadTime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRecentDailyClicks() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAverageLoadTime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAverageDailyClicks() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIgnoreWarnings() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>