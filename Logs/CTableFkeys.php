<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CTableFkeys
 * Do not add any new functions to this class.
 */

class CTableFkeys extends CBaseTableFkeys {

	public static function fetchAllTableFkeys( $objDatabase ) {

		$strSql = 'SELECT * FROM table_fkeys;';

		return parent::fetchTableFkeys( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedTableFkeysByDatabaseIdsByTableName( $arrintDatabaseIds, $strTableName, $boolIsBadFkeys, $intPageNo, $intPageSize, $strOrderByField = NULL, $strOrderByType = NULL , $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		if( false == is_null( $strOrderByField ) ) {

			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY updated_on DESC';
		}

		$strWhere = ' WHERE database_id IN ( ' . implode( ',', $arrintDatabaseIds ) . ')';

		if( false == is_null( $strTableName ) ) {
			$strWhere .= ' AND table_name = \'' . $strTableName . '\'';
		}

		if( true == $boolIsBadFkeys ) {
			$strWhere .= ' AND is_bad_fkey = 1';
		}

		$strSql = 'SELECT * FROM table_fkeys ' . $strWhere . $strOrderBy . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return parent::fetchTableFkeys( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedTableFkeysCountByDatabaseIdsByTableName( $arrintDatabaseIds, $strTableName, $boolIsBadFkeys, $objDatabase ) {

		$strWhere = ' WHERE database_id IN ( ' . implode( ',', $arrintDatabaseIds ) . ')';

		if( false == is_null( $strTableName ) ) {
			$strWhere .= ' AND table_name = \'' . $strTableName . '\'';
		}

		if( true == $boolIsBadFkeys ) {
			$strWhere .= ' AND is_bad_fkey = 1';
		}

		return parent::fetchTableFkeyCount( $strWhere, $objDatabase );
	}

	public static function fetchTableFkeyByIds( $arrintTableFkeyIds, $objDatabase ) {

		if( false == valArr( $arrintTableFkeyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM table_fkeys WHERE id IN ( ' . implode( ',', $arrintTableFkeyIds ) . ' )';
		return parent::fetchTableFkeys( $strSql, $objDatabase );
	}
}
?>