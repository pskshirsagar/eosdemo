<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSystemErrorDetails
 * Do not add any new functions to this class.
 */

class CSystemErrorDetails extends CBaseSystemErrorDetails {

	public static function fetchConflictingSystemErrorDetails( $objSystemErrorDetail, $objDatabase ) {

		$intPsProductId = $objSystemErrorDetail->getPsProductId();
		$strAction 		= $objSystemErrorDetail->getAction();

		$strSql = 'SELECT
						*
					FROM
						system_error_details
					WHERE
						system_error_id = ' . ( int ) $objSystemErrorDetail->getSystemErrorId() . '
						' . ( false == empty( $intPsProductId ) ? ' AND ps_product_id = ' . ( int ) $intPsProductId : '' ) . '
						AND module = \'' . $objSystemErrorDetail->getModule() . '\'
						AND action ' . ( false == empty( $strAction ) ? ' = \'' . $objSystemErrorDetail->getAction() . '\'' : ' IS NULL' ) . '
						AND description = ' . $objSystemErrorDetail->sqlDescription() . '
					LIMIT 1';

		return self::fetchSystemErrorDetail( $strSql, $objDatabase );
	}

	public static function fetchPaginatedSystemErrorDetailsBySearchFilter( $intSystemErrorId, $objSystemErrorsFilter, $objDatabase ) {

		$arrstrAndSearchParameters 	= array();

		if( true == valObj( $objSystemErrorsFilter, 'CSystemErrorsFilter' ) ) {
			$arrstrAndSearchParameters = CSystemErrors::getSearchCriteria( $objSystemErrorsFilter );
		}

		$intKey = array_search( 'se.deleted_by IS NULL', $arrstrAndSearchParameters );

		if( true == is_numeric( $intKey ) ) {
			unset( $arrstrAndSearchParameters[$intKey] );
		}

		$intOffset 		= ( 0 < $objSystemErrorsFilter->getPageNo() ) ? $objSystemErrorsFilter->getPageSize() * ( $objSystemErrorsFilter->getPageNo() - 1 ) : 0;
		$intLimit 		= ( int ) $objSystemErrorsFilter->getPageSize();

		$strOrderClause 			= '';
		$strDistinctOnClause 		= '';

		if( false == is_null( $objSystemErrorsFilter ) && false == is_null( $objSystemErrorsFilter->getOrderByField() ) && false == is_null( $objSystemErrorsFilter->getOrderByType() ) ) {

			$strOrder = ( 1 == $objSystemErrorsFilter->getOrderByType() ) ? ' DESC ' : ' ASC ';

			switch( $objSystemErrorsFilter->getOrderByField() ) {
				case NULL:
					break;

				case 'id':
					$strOrderClause = ' ORDER BY sed.id ' . $strOrder;
					break;

				case 'ps_product_id':
					$strOrderClause 	= ' ORDER BY sed.ps_product_id ' . $strOrder;
					break;

				case 'module':
					$strOrderClause 	= ' ORDER BY sed.module ' . $strOrder;
					break;

				case 'action':
					$strOrderClause 	= ' ORDER BY sed.action ' . $strOrder;
					break;

				case 'employee_id':
					$strOrderClause 	= ' ORDER BY se.employee_id ' . $strOrder;
					break;

				case 'cluster_id':
					$strOrderClause 	= ' ORDER BY se.cluster_id ' . $strOrder;
					break;

				case 'server':
					$strOrderClause 	= ' ORDER BY sed.server ' . $strOrder;
					break;

				case 'error_count':
					$strOrderClause 	= ' ORDER BY  sed.error_count ' . $strOrder;
					break;

				case 'created_on':
					$strOrderClause		= ' ORDER BY sed.created_on ' . $strOrder;
					break;

				case 'updated_on':
					$strOrderClause		= ' ORDER BY sed.updated_on ' . $strOrder;
					break;

				default:
					$strOrderClause 		 = ' ORDER BY sed.' . $objSystemErrorsFilter->getOrderByField() . $strOrder;
			}
		}

		$strSql = 'SELECT '
					. ' sed.*, se.employee_id, se.cluster_id
					FROM
    					system_error_details sed
    					LEFT JOIN system_errors se ON ( sed.system_error_id = se.id ) '
						. ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' WHERE ' : '' )
						. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? implode( ' AND ', $arrstrAndSearchParameters ) . ' AND ' : '' )
						. ' sed.system_error_id = ' . ( int ) $intSystemErrorId
						. ( ( 0 < strlen( $strOrderClause ) ) ? $strOrderClause : '  ORDER BY sed.updated_on DESC ' ) . '
    		   	   OFFSET
    		   	   		' . ( int ) $intOffset;

		if( true == isset( $intLimit ) ) {
			$strSql .= ' LIMIT	' . $intLimit;
		}
		return self::fetchSystemErrorDetails( $strSql, $objDatabase );
	}

	public static function fetchPaginatedSystemErrorDetailsCountBySearchFilter( $intSystemErrorId, $objSystemErrorsFilter, $objDatabase ) {

		$arrstrAndSearchParameters 	= array();

		if( true == valObj( $objSystemErrorsFilter, 'CSystemErrorsFilter' ) ) {
			$arrstrAndSearchParameters = CSystemErrors::getSearchCriteria( $objSystemErrorsFilter );
		}

		$intKey = array_search( 'se.deleted_by IS NULL', $arrstrAndSearchParameters );

		if( true == is_numeric( $intKey ) ) {
			unset( $arrstrAndSearchParameters[$intKey] );
		}

		$strSql = 'SELECT '
					. ' count( sed.id ) as count
					FROM
    					system_error_details sed
    					LEFT JOIN system_errors se ON ( sed.system_error_id = se.id ) '
						. ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' WHERE ' : '' )
						. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? implode( ' AND ', $arrstrAndSearchParameters ) . ' AND ' : '' )
						. ' sed.system_error_id = ' . ( int ) $intSystemErrorId;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchLatestSystemErrorDetailBySystemErrorId( $intSystemErrorId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						system_error_details
					WHERE
						system_error_id = ' . ( int ) $intSystemErrorId . '
					ORDER BY
						updated_on DESC
					LIMIT 1';

		return self::fetchSystemErrorDetail( $strSql, $objDatabase );

	}

}
?>