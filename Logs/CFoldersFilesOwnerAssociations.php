<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CFoldersFilesOwnerAssociations
 * Do not add any new functions to this class.
 */

class CFoldersFilesOwnerAssociations extends CBaseFoldersFilesOwnerAssociations {

	public static function fetchDefaultFoldersOwnerAssociations( $objDatabase ) {

		$strSql = ' SELECT
						ffoa.id,
						ffoa.name,
						ffoa.file_path,
						ffoa.ps_product_id,
						ffoa.ps_product_option_id,
						ffoa.dev_employee_id,
						ffoa.qa_employee_id,
						( CASE WHEN ffoa.is_multilingual_support = \'f\' THEN NULL ELSE 1 END ) is_multilingual_support
					FROM
						folders_files_owner_associations ffoa
					WHERE
						ffoa.folders_files_owner_associations_id IS NULL
						AND ffoa.deleted_by IS NULL
						AND ffoa.deleted_on IS NULL
					ORDER BY
						ffoa.name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFoldersFilesOwnerAssociationsByParentFolderId( $intParentFolderId, $objDatabase ) {

		if( false == valId( $intParentFolderId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						ffoa.id,
						ffoa.name,
						ffoa.module_name,
						ffoa.file_path,
						ffoa.ps_product_id,
						ffoa.ps_product_option_id,
						ffoa.sdm_employee_id,
						ffoa.architect_employee_id,
						ffoa.folders_files_owner_associations_id as parent_folder_id,
						ffoa.dev_employee_id,
						ffoa.qa_employee_id,
						( CASE WHEN ffoa.is_multilingual_support = \'f\' THEN NULL ELSE 1 END ) is_multilingual_support,
						CASE
							WHEN ffoa.name LIKE \'%.%\'
							THEN 1
							ELSE NULL
						END as is_file
					FROM
						folders_files_owner_associations ffoa
					WHERE
						ffoa.folders_files_owner_associations_id = ' . $intParentFolderId . '
						AND ffoa.deleted_by IS NULL
						AND ffoa.deleted_on IS NULL
					ORDER BY
						ffoa.name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDeletedFoldersFilesOwnerAssociationCountById( $intFolderId, $objDatabase ) {

		if( false == valId( $intFolderId ) ) return NULL;

		$strSql = ' SELECT
						count(ffoa.id)
					FROM
						folders_files_owner_associations ffoa
					WHERE
						ffoa.folders_files_owner_associations_id = ' . $intFolderId . '
						AND ffoa.deleted_by IS NOT NULL
						AND ffoa.deleted_on IS NOT NULL
					';
		$arrintCount = current( fetchData( $strSql, $objDatabase ) );
		return $arrintCount['count'];

	}

	public static function fetchFoldersAssociationsByParentFoldersFilesOwnerAssociationIds( $arrintParentFoldersFilesOwnerAssociationId, $objDatabase ) {
		if( false == valArr( $arrintParentFoldersFilesOwnerAssociationId ) ) return NULL;

		$strSql = ' SELECT
						ffoa.id,
						ffoa.name,
						ffoa.file_path,
						ffoa.folders_files_owner_associations_id as parent_folder_id,
						ffoa.dev_employee_id,
						ffoa.qa_employee_id,
						count( CASE
							WHEN ffoa_child.name NOT LIKE \'%.%\' AND ffoa_child.deleted_on IS NULL
							THEN 1
							ELSE NULL
						END )as child_folders_count
					FROM
						folders_files_owner_associations ffoa
						LEFT JOIN folders_files_owner_associations ffoa_child ON (ffoa.id = ffoa_child.folders_files_owner_associations_id)
					WHERE
						ffoa.folders_files_owner_associations_id IN (' . implode( ',', $arrintParentFoldersFilesOwnerAssociationId ) . ' )
						AND ffoa.name NOT LIKE \'%.%\'
						AND ffoa.deleted_by IS NULL
						AND ffoa.deleted_on IS NULL
					GROUP BY
						ffoa.id,
						ffoa.name,
						ffoa.file_path,
						ffoa.folders_files_owner_associations_id,
						ffoa.dev_employee_id,
						ffoa.qa_employee_id
					ORDER BY
						ffoa.name ASC';

		$arrmixData = fetchData( $strSql, $objDatabase );

		return $arrmixData;
	}

	public static function fetchSearchedFoldersFilesOwnerAssociations( $strFilteredExplodedSearch, $objDatabase ) {
		if( false == valStr( $strFilteredExplodedSearch ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						ffoa.id,
						ffoa.name,
						ffoa.module_name,
						ffoa.file_path,
						ffoa.folders_files_owner_associations_id as parent_folder_id,
						ffoa.sdm_employee_id,
						ffoa.architect_employee_id,
						ffoa.dev_employee_id,
						ffoa.qa_employee_id
					FROM
						folders_files_owner_associations ffoa
					WHERE
						 ( ( ffoa.module_name ILIKE \'%' . $strFilteredExplodedSearch . '%\' OR to_tsvector( \'english\', ffoa.module_name ) @@ plainto_tsquery( \'english\', \'' . $strFilteredExplodedSearch . '\' ) )
							OR ( ffoa.name ILIKE \'%' . $strFilteredExplodedSearch . '%\' OR to_tsvector( \'english\', ffoa.name ) @@ plainto_tsquery( \'english\', \'' . $strFilteredExplodedSearch . '\' ) )
						)
						AND ffoa.deleted_by IS NULL
						AND ffoa.deleted_on IS NULL
					GROUP BY
						ffoa.id,
						ffoa.name,
						ffoa.folders_files_owner_associations_id,
						ffoa.dev_employee_id,
						ffoa.qa_employee_id
					ORDER BY
						ffoa.name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFoldersFilesOwnerAssociationsByIds( $arrintFoldersFilesOwnerAssociationIds, $objDatabase ) {
		if( false == valArr( $arrintFoldersFilesOwnerAssociationIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						folders_files_owner_associations ffoa
					WHERE
						ffoa.id IN (' . implode( ',', $arrintFoldersFilesOwnerAssociationIds ) . ' )
						AND ffoa.deleted_by IS NULL
						AND ffoa.deleted_on IS NULL';

		return self::fetchFoldersFilesOwnerAssociations( $strSql, $objDatabase );
	}

	public static function fetchFoldersFilesOwnerAssociationsByFoldersFilesOwnerAssociationsIdByName( $intFolderFileAssociationId, $strName, $objDatabase ) {
		if( false == is_numeric( $intFolderFileAssociationId ) || false == valStr( $strName ) ) return NULL;

		$strSql = ' SELECT
						count(ffoa.id)
					FROM
						folders_files_owner_associations ffoa
					WHERE
						ffoa.folders_files_owner_associations_id = ' . $intFolderFileAssociationId . '
						AND ffoa.name = \'' . $strName . '\'
						AND ffoa.deleted_by IS NULL
						AND ffoa.deleted_on IS NULL';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrmixData[0]['count'] ) ) return $arrmixData[0]['count'];

		return 0;
	}

	public static function fetchFoldersFilesOwnerAssociationsByFilePath( $strFilePath, $objDatabase ) {
		if( false == valStr( $strFilePath ) ) return NULL;

		$strSql = ' SELECT
						ffoa.id,
						ffoa.ps_product_id,
						ffoa.ps_product_option_id,
						ffoa.dev_employee_id,
						ffoa.qa_employee_id,
						ffoa.name,
						ffoa.file_path
					FROM
						folders_files_owner_associations ffoa
					WHERE
						ffoa.file_path ILIKE \'%' . $strFilePath . '\'
						AND ffoa.deleted_by IS NULL
						AND ffoa.deleted_on IS NULL';

		$arrmixData = fetchData( $strSql, $objDatabase );

		return $arrmixData;
	}

	public static function fetchAllFoldersFilesOwnerAssociationsByFilePath( $strFilePath, $objDatabase ) {
		if( false == valStr( $strFilePath ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						folders_files_owner_associations ffoa
					WHERE
						ffoa.file_path ILIKE \'%' . $strFilePath . '\'
						AND ffoa.deleted_by IS NULL
						AND ffoa.deleted_on IS NULL';

		return self::fetchFoldersFilesOwnerAssociation( $strSql, $objDatabase );
	}

	public static function fetchFoldersFilesOwnerAssociationsByDeletedFilePath( $strFilePath, $objDatabase ) {
		if( false == valStr( $strFilePath ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						folders_files_owner_associations ffoa
					WHERE
						ffoa.file_path ILIKE \'%' . $strFilePath . '\'';

		return self::fetchFoldersFilesOwnerAssociations( $strSql, $objDatabase );
	}

	public static function fetchAllActiveChildIdsById( $intFolderFileId, $objDatabase, $boolIsUpdateOwner = false ) {

		if( false == is_numeric( $intFolderFileId ) ) return NULL;

		$strOuterSelectSql		= '';
		$strEndOuterSelectSql	= '';

		if( true == $boolIsUpdateOwner ) {
			$strOuterSelectSql		= ' SELECT
											*
										FROM
											folders_files_owner_associations
										WHERE
											id IN (
													SELECT
														subSql.id
													FROM
														( ';
			$strEndOuterSelectSql	= ' )as subSql )';

		}

		$strSql = $strOuterSelectSql . 'WITH recursive
													all_folder_file_association(id, folders_files_owner_associations_id, depth)
														AS (
																SELECT
																	ffoa.id,
																	ffoa.folders_files_owner_associations_id,
																	1
																FROM
																	folders_files_owner_associations ffoa
																WHERE
																	(ffoa.id = ' . ( int ) $intFolderFileId . '
																	OR ffoa.folders_files_owner_associations_id = ' . ( int ) $intFolderFileId . ')
																	AND ffoa.deleted_by IS NULL
																	AND ffoa.deleted_on IS NULL
																UNION
																SELECT
																	ffoa.id,
																	ffoa.folders_files_owner_associations_id,
																	affa.depth + 1
																FROM
																	folders_files_owner_associations ffoa,
																	all_folder_file_association affa
																WHERE
																	ffoa.folders_files_owner_associations_id = affa.id
																	AND affa.id <> ' . ( int ) $intFolderFileId . '
																	AND ffoa.deleted_by IS NULL
																	AND ffoa.deleted_on IS NULL
															)
															SELECT
																affa.id
															FROM
																all_folder_file_association affa
																JOIN folders_files_owner_associations ffoa ON (affa.id = ffoa.id)
															ORDER BY
																affa.id DESC' . $strEndOuterSelectSql;

		return self::fetchFoldersFilesOwnerAssociations( $strSql, $objDatabase );

	}

	public static function fetchFileOwnerDetailsByFilePath( $strFilePath, $objLogsDatabase ) {

		// Need this path on live, if working on other local or stage environment need to change this path accordingly.
		$strFilePath = str_replace( PATH_VHOSTS, '', $strFilePath );

		$arrstrPaths = array();

		// Check if its a base file
		if( false !== \Psi\CStringService::singleton()->strpos( $strFilePath, 'Base/CBase' ) ) {
			$strCustomFilePath = str_replace( 'Base/CBase', 'C', $strFilePath );
			$arrstrPaths[] = $strFilePath;
			$strFilePath = $strCustomFilePath;
		}

		$arrstrFilePaths = explode( '/', $strFilePath );

		while( \Psi\Libraries\UtilFunctions\count( $arrstrFilePaths ) > 0 ) {
			$strPath = implode( $arrstrFilePaths, '/' );
			array_pop( $arrstrFilePaths );
			$arrstrPaths[] = $strPath;
		}

		$arrstrFoldersFilesOwnerAssocition = array();

		foreach( $arrstrPaths as $strPath ) {
			$arrstrFoldersFilesOwnerAssocition = self::fetchFoldersFilesOwnerAssociationsByFilePath( $strPath, $objLogsDatabase );
			if( true == valArr( $arrstrFoldersFilesOwnerAssocition ) ) {
				return $arrstrFoldersFilesOwnerAssocition;
			}
		}

		return $arrstrFoldersFilesOwnerAssocition;
	}

}
?>