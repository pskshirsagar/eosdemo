<?php

class CDiagnostic extends CBaseDiagnostic {

	const IMPROPERLY_FORMATTED_POST_MONTH	 = 44;
	const DISABLED_TRIGGERS					 = 79;

	const DIAGNOSTIC_FREQUENCY_DAILY		 = 2;
	const DIAGNOSTIC_FREQUENCY_WEEKLY		 = 3;
	const DIAGNOSTIC_FREQUENCY_MONTHLY		 = 4;
	const DIAGNOSTIC_FREQUENCY_CUSTOM		 = 5;
	const DIAGNOSTIC_FREQUENCY_MANUAL		 = 6;

	const DIAGNOSTIC_FREQUENCY_WEEKLY_MON	= 1;
	const DIAGNOSTIC_FREQUENCY_WEEKLY_TUE	= 2;
	const DIAGNOSTIC_FREQUENCY_WEEKLY_WED	= 3;
	const DIAGNOSTIC_FREQUENCY_WEEKLY_THU	= 4;
	const DIAGNOSTIC_FREQUENCY_WEEKLY_FRI	= 5;
	const DIAGNOSTIC_FREQUENCY_WEEKLY_SAT	= 6;
	const DIAGNOSTIC_FREQUENCY_WEEKLY_SUN	= 7;

	const DIAGNOSTIC_AUDIT_DAY				= 14;

	public static $c_arrstrDiagnosticFrequencies		= [ self::DIAGNOSTIC_FREQUENCY_DAILY => 'daily', self::DIAGNOSTIC_FREQUENCY_WEEKLY => 'weekly', self::DIAGNOSTIC_FREQUENCY_MONTHLY => 'monthly', self::DIAGNOSTIC_FREQUENCY_CUSTOM => 'custom', self::DIAGNOSTIC_FREQUENCY_MANUAL => 'manual' ];
	public static $c_arrstrDiagnosticFrequenciesWeekly	= [ 'Mon' => self::DIAGNOSTIC_FREQUENCY_WEEKLY_MON, 'tue' => self::DIAGNOSTIC_FREQUENCY_WEEKLY_TUE, 'wed' => self::DIAGNOSTIC_FREQUENCY_WEEKLY_WED, 'Thu' => self::DIAGNOSTIC_FREQUENCY_WEEKLY_THU, 'fri' => self::DIAGNOSTIC_FREQUENCY_WEEKLY_FRI, 'sat' => self::DIAGNOSTIC_FREQUENCY_WEEKLY_SAT, 'sun' => self::DIAGNOSTIC_FREQUENCY_WEEKLY_SUN ];


	public $m_objAdminDatabase;

	private $m_objEmployee;
	private $m_arrobjLoggedInUserRoles;

	protected $m_intRepairStatus;
	protected $m_intTestClientErrorCount;

	protected $m_arrmixCustomVariables;

	public function __construct() {
		parent::__construct();

		$this->m_arrmixCustomVariables = [];
		$this->setRepairStatus( -1 );

		return;
	}

	public function setDefaults() {
		parent::setDefaults();
		$this->setFrequencyId( CFrequency::DAILY );
		$this->setIsClientSpecific( 1 );
	}

	public function getRepairStatus() {
		return $this->m_intRepairStatus;
	}

	public function getTestClientErrorCount() {
		return $this->m_intTestClientErrorCount;
	}

	public function setTestClientErrorCount( $intTestClientErrorCount ) {
		$this->m_intTestClientErrorCount = $intTestClientErrorCount;
	}

	public function setRepairStatus( $intRepairStatus ) {
		$this->m_intRepairStatus = $intRepairStatus;
	}

	public function createDiagnosticLog( $intCid ) {

		$objDiagnosticLog = new CDiagnosticLog();
		$objDiagnosticLog->setCid( $intCid );
		$objDiagnosticLog->setDiagnosticId( $this->getId() );
		$objDiagnosticLog->setLogDatetime( date( 'm/d/Y H:i:s' ) );

		return $objDiagnosticLog;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = false, $boolDirectSet );

		$this->m_arrmixCustomVariables['item_count']				 = ( isset( $arrmixValues['item_count'] ) ) ? trim( $arrmixValues['item_count'] ) : '';
		$this->m_arrmixCustomVariables['message']					 = ( isset( $arrmixValues['message'] ) ) ? trim( $arrmixValues['message'] ) : '';
		$this->m_arrmixCustomVariables['seconds']					 = ( isset( $arrmixValues['seconds'] ) ) ? trim( $arrmixValues['seconds'] ) : '';
		$this->m_arrmixCustomVariables['is_error']					 = ( isset( $arrmixValues['is_error'] ) ) ? trim( $arrmixValues['is_error'] ) : '';
		$this->m_arrmixCustomVariables['log_datetime']				 = ( isset( $arrmixValues['log_datetime'] ) ) ? trim( $arrmixValues['log_datetime'] ) : '';
		$this->m_arrmixCustomVariables['diagnostic_type']			 = ( isset( $arrmixValues['diagnostic_type'] ) ) ? trim( $arrmixValues['diagnostic_type'] ) : '';
		$this->m_arrmixCustomVariables['error_count']				 = ( isset( $arrmixValues['error_count'] ) ) ? trim( ( int ) $arrmixValues['error_count'] ) : '';
		$this->m_arrmixCustomVariables['test_client_error_count']	 = ( isset( $arrmixValues['test_client_error_count'] ) ) ? trim( ( int ) $arrmixValues['test_client_error_count'] ) : '';
		$this->m_arrmixCustomVariables['repair_count']				 = ( isset( $arrmixValues['error_count'] ) ) ? trim( ( int ) $arrmixValues['repair_count'] ) : '';
		$this->m_arrmixCustomVariables['employee_name']				 = ( isset( $arrmixValues['employee_name'] ) ) ? trim( $arrmixValues['employee_name'] ) : '';
		$this->m_arrmixCustomVariables['database_type_id']			 = ( isset( $arrmixValues['database_type_id'] ) ) ? trim( $arrmixValues['database_type_id'] ) : '';
		$this->m_arrmixCustomVariables['diagnostic_result_type_id']	 = ( isset( $arrmixValues['diagnostic_result_type_id'] ) ) ? trim( $arrmixValues['diagnostic_result_type_id'] ) : '';
		$this->m_arrmixCustomVariables['diagnostic_note']			 = ( isset( $arrmixValues['diagnostic_note'] ) ) ? trim( $arrmixValues['diagnostic_note'] ) : '';
		$this->m_arrmixCustomVariables['diagnostic_note_created_on'] = ( isset( $arrmixValues['diagnostic_note_created_on'] ) ) ? trim( $arrmixValues['diagnostic_note_created_on'] ) : '';
		$this->m_arrmixCustomVariables['diagnostic_note_employee_id'] = ( isset( $arrmixValues['diagnostic_note_employee_id'] ) ) ? trim( $arrmixValues['diagnostic_note_employee_id'] ) : '';
		$this->m_arrmixCustomVariables['table_name']				 = ( isset( $arrmixValues['table_name'] ) ) ? trim( $arrmixValues['table_name'] ) : '';
		$this->m_arrmixCustomVariables['total_diagnostics']			 = ( isset( $arrmixValues['total_diagnostics'] ) ) ? trim( $arrmixValues['total_diagnostics'] ) : '';

		return;
	}

	public function getCustomVariableByKey( $strKey ) {
		return ( true == array_key_exists( $strKey, $this->m_arrmixCustomVariables ) ) ? $this->m_arrmixCustomVariables[$strKey] : NULL;
	}

	private function getOrFetchLoggedInUserRoles( $objUser ) {
		if( true == empty( $this->m_arrobjLoggedInUserRoles ) ) {
			$this->m_arrobjLoggedInUserRoles = CRoles::fetchRolesByUserId( $objUser->getId(), $this->m_objAdminDatabase );
		}

		return $this->m_arrobjLoggedInUserRoles;
	}

	private function getOrFetchEmployee( $objUser ) {
		if( true == empty( $this->m_objEmployee ) ) {
			$this->m_objEmployee = CEmployees::fetchEmployeeByUserId( $objUser->getId(), $this->m_objAdminDatabase );
		}

		return $this->m_objEmployee;
	}

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDiagnosticTypeId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getDiagnosticTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'diagnostic_type_id', 'Diagnostic type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTableId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getTableId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'table_id', 'Related table is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSdmEmployeeId() {
		$boolIsValid = true;

		/** if( false == is_numeric( $this->getSdmEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sdm_employee_id', 'SDM assignment is required.' ) );
		} */

		return $boolIsValid;
	}

	public function valDeveloperEmployeeId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getDeveloperEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'developer_employee_id', 'Developer assignment is required.' ) );
		}

		return $boolIsValid;
	}

	public function valQAEmployeeId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getQAEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'qa_employee_id', 'QA assignment is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductId() {

		$boolIsValid = true;

		if( true == is_numeric( $this->getPsProductId() ) && 0 == $this->getIsClientSpecific() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product should never be selected for a non-client-specific diagnostic.' ) );
		}

		return $boolIsValid;
	}

	public function valTaskPriorityId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getTaskPriorityId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_priority_id', 'Priority is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBeginDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getBeginDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_date', 'Begin date is required.' ) );
		} elseif( false == is_null( $this->getBeginDate() ) && ( false == CValidation::validateDate( $this->getBeginDate() ) || false == preg_match( '#^(((0?[0-9])|(1[012]))/(0?[0-9]|[0-2][0-9]|3[0-1])/[0-9]{4})$#', $this->getBeginDate() ) ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_date', 'Begin date should be in mm/dd/yyyy format.' ) );
		}

		return $boolIsValid;
	}

	public function valRapidViewSql() {
		$boolIsValid = true;

		if( true == is_null( $this->getRapidViewSql() ) && true == is_null( $this->getStandardViewSql() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_view_sql', 'View SQL is required.' ) );
		}

		if( false == is_null( $this->getRapidViewSql() ) ) {

			if( 1 == $this->getIsClientSpecific() && false === \Psi\CStringService::singleton()->strpos( $this->getRapidViewSql(), '#CID#' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_view_sql', 'Rapid view SQL must have #CID# variable when flagged as client specific.' ) );
			}

			if( true == $this->getIsPropertySpecific() && false === \Psi\CStringService::singleton()->strpos( $this->getRapidViewSql(), '#PID#' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_view_sql', 'Rapid view SQL must have #PID# variable when flagged as property specific.' ) );
			}

			if( 1 == $this->getIsClientSpecific() && 1 == preg_match( '/[ \r\t]*in[ \r\t]+\([ \r\t]+#CID#[ \r\t]+\)/i', $this->getRapidViewSql() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_repair_sql', 'Rapid view SQL may not use "in (#CID#)", please use load_properties()' ) );
			}

			if( ( 1 == $this->getIsClientSpecific() || true == $this->getIsPropertySpecific() ) && 1 == preg_match( '/property_id/i', $this->getRapidViewSql() ) && 0 == preg_match( '/load_properties/i', $this->getRapidViewSql() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_repair_sql', 'When property_id exists in SQL, please use load_properties( ... )' ) );
			}

			if( 0 == $this->getIsClientSpecific() && false !== \Psi\CStringService::singleton()->strpos( $this->getRapidViewSql(), '#CID#' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_view_sql', 'Rapid view SQL must not have a #CID# variable when not flagged as client specific.' ) );
			}

			if( false == $this->getIsPropertySpecific() && false !== \Psi\CStringService::singleton()->strpos( $this->getRapidViewSql(), '#PID#' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_view_sql', 'Rapid view SQL must not have a #PID# variable when not flagged as property specific.' ) );
			}

			if( false !== \Psi\CStringService::singleton()->strpos( $this->getRapidViewSql(), 'DIAGNOSTIC_COUNT_WRAPPER' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_view_sql', 'It is no longer required to include DIAGNOSTIC_COUNT_WRAPPER_START and DIAGNOSTIC_COUNT_WRAPPER_END variables. Please remove.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valStandardViewSql() {
		$boolIsValid = true;

		if( false == is_null( $this->getStandardViewSql() ) ) {

			if( 1 == $this->getIsClientSpecific() && false === \Psi\CStringService::singleton()->strpos( $this->getStandardViewSql(), '#CID#' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'standard_view_sql', 'Standard view SQL must have #CID# variable when flagged as client specific.' ) );
			}

			if( true == $this->getIsPropertySpecific() && false === \Psi\CStringService::singleton()->strpos( $this->getStandardViewSql(), '#PID#' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'standard_view_sql', 'Standard view SQL must have #PID# variable when flagged as property specific.' ) );
			}

			if( 1 == $this->getIsClientSpecific() && 1 == preg_match( '/[ \r\t]*in[ \r\t]+\([ \r\t]+#CID#[ \r\t]+\)/i', $this->getStandardViewSql() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'standard_view_sql', 'Standard view SQL may not use "in (#CID#)", please use load_properties()' ) );
			}

			if( ( 1 == $this->getIsClientSpecific() || true == $this->getIsPropertySpecific() ) && 1 == preg_match( '/property_id/i', $this->getStandardViewSql() ) && 0 == preg_match( '/load_properties/i', $this->getStandardViewSql() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'standard_view_sql', 'When property_id exists in SQL, please use load_properties( ... )' ) );
			}

			if( 0 == $this->getIsClientSpecific() && false !== \Psi\CStringService::singleton()->strpos( $this->getStandardViewSql(), '#CID#' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'standard_view_sql', 'Standard View SQL must not have a #CID# variable when not flagged as client specific.' ) );
			}

			if( false == $this->getIsPropertySpecific() && false !== \Psi\CStringService::singleton()->strpos( $this->getStandardViewSql(), '#PID#' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'standard_view_sql', 'Standard View SQL must not have a #PID# variable when not flagged as property specific.' ) );
			}

			if( false !== \Psi\CStringService::singleton()->strpos( $this->getStandardViewSql(), 'DIAGNOSTIC_COUNT_WRAPPER' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'standard_view_sql', 'It is no longer required to include DIAGNOSTIC_COUNT_WRAPPER_START and DIAGNOSTIC_COUNT_WRAPPER_END variables. Please remove.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valRapidRepairSql() {
		$boolIsValid = true;

		if( true == is_null( $this->getRapidRepairSql() ) ) {
			return $boolIsValid;
		}

		if( 1 == $this->getIsClientSpecific() && false === \Psi\CStringService::singleton()->strpos( $this->getRapidRepairSql(), '#CID#' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_repair_sql', 'Repair SQL must include a #CID# variable when flagged as client specific.' ) );

		} elseif( 0 == $this->getIsClientSpecific() && false !== \Psi\CStringService::singleton()->strpos( $this->getRapidRepairSql(), '#CID#' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_repair_sql', 'Repair SQL must not have a #CID# variable when not flagged as client specific.' ) );
		}

		if( true == $this->getIsPropertySpecific() && false === \Psi\CStringService::singleton()->strpos( $this->getRapidRepairSql(), '#PID#' ) && 1 == preg_match( '/load_properties/i', $this->getRapidRepairSql() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_repair_sql', 'Repair SQL must include a #PID# variable when flagged as property specific.' ) );
		}

		if( false == $this->getIsPropertySpecific() && false !== \Psi\CStringService::singleton()->strpos( $this->getRapidRepairSql(), '#PID#' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_repair_sql', 'Repair SQL must not have a #PID# variable when not flagged as property specific.' ) );
		}

		if( 1 == preg_match( '/drop[ \r\t]+table/i', $this->getRapidRepairSql() ) || 1 == preg_match( '/drop[ \r\t]+database/i', $this->getRapidRepairSql() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_repair_sql', 'Rapid repair SQL may not drop a table or database' ) );
		}

		return $boolIsValid;
	}

	public function valStandardRepairSql() {
		$boolIsValid = true;

		if( true == is_null( $this->getStandardRepairSql() ) ) {
			return $boolIsValid;
		}

		if( 1 == $this->getIsClientSpecific() && false === \Psi\CStringService::singleton()->strpos( $this->getStandardRepairSql(), '#CID#' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'standard_repair_sql', 'Standard Repair SQL must include a #CID# variable when flagged as client specific.' ) );

		} elseif( 0 == $this->getIsClientSpecific() && false !== \Psi\CStringService::singleton()->strpos( $this->getStandardRepairSql(), '#CID#' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'standard_repair_sql', 'Standard Repair SQL must not have a #CID# variable when not flagged as client specific.' ) );
		}

		if( true == $this->getIsPropertySpecific() && false === \Psi\CStringService::singleton()->strpos( $this->getStandardRepairSql(), '#PID#' ) && 1 == preg_match( '/load_properties/i', $this->getStandardRepairSql() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'standard_repair_sql', 'Standard Repair SQL must include a #PID# variable when flagged as property specific.' ) );
		}

		if( false == $this->getIsPropertySpecific() && false !== \Psi\CStringService::singleton()->strpos( $this->getStandardRepairSql(), '#PID#' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'standard_repair_sql', 'Standard Repair SQL must not have a #PID# variable when not flagged as property specific.' ) );
		}

		if( 1 == preg_match( '/drop[ \r\t]+table/i', $this->getStandardRepairSql() ) || 1 == preg_match( '/drop[ \r\t]+database/i', $this->getStandardRepairSql() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'standard_repair_sql', 'Standard repair SQL may not drop a table or database' ) );
		}

		return $boolIsValid;
	}

	public function valRequiresGlRebuild() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valAutoRepair() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsRepairable() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsWarning() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsPhp() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		if( true == is_null( $this->getOrderNum() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', 'Order num is required.' ) );
		} elseif( false == is_numeric( $this->getOrderNum() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', 'Invalid order num.' ) );
		}

		return $boolIsValid;
	}

	public function valFrequencyDetails() {
		$boolIsValid = true;
		if( $this->getFrequencyId() != self::DIAGNOSTIC_FREQUENCY_DAILY && $this->getFrequencyId() != self::DIAGNOSTIC_FREQUENCY_MANUAL && ( true == is_null( $this->getFrequencyDetails() ) || false == valArr( array_filter( ( array ) $this->getFrequencyDetails() ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_details', 'Diagnostic Run On Day(s) is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsMigrationRepair() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStakeholders() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReplicaLagTime() {
		$boolIsValid = true;

		if( false == valId( $this->getReplicaLagTime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'replica_lag_time', 'RO DB sync lag threshold needs to be an integer value between 1 to 720.' ) );
		} elseif( 0 >= $this->getReplicaLagTime() || 720 < $this->getReplicaLagTime() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'replica_lag_time', 'RO DB sync lag threshold needs to be between 1 to 720.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDiagnosticTypeId();
				$boolIsValid &= $this->valTableId();
				$boolIsValid &= $this->valTaskPriorityId();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valSdmEmployeeId();
				$boolIsValid &= $this->valDeveloperEmployeeId();
				$boolIsValid &= $this->valQAEmployeeId();
				$boolIsValid &= $this->valBeginDate();
				$boolIsValid &= $this->valRapidViewSql();
				$boolIsValid &= $this->valRapidRepairSql();
				$boolIsValid &= $this->valStandardViewSql();
				$boolIsValid &= $this->valStandardRepairSql();
				$boolIsValid &= $this->valOrderNum();
				$boolIsValid &= $this->valFrequencyDetails();
				$boolIsValid &= $this->valReplicaLagTime();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function runDiagnosticRepairPermitted( $objUser, $objTargetDatabasePsDba, $arrobjLoggedInUserRoles, $objEmployee ) {

		return false;

		if( false == valObj( $objTargetDatabasePsDba, 'CDatabase' ) ) {
			return false;
		}

		// Between 12:00 AM and 6:00 AM
		$intCurrentHour = date( 'G' );
		$this->m_boolBetweenTwelveAmAndSixAm = false;
		if( 0 <= $intCurrentHour && 6 >= $intCurrentHour ) {
			$this->m_boolBetweenTwelveAmAndSixAm = true;
		}

		$this->m_boolHasDisableTrigger = false;

		if( 1 == preg_match( '/disable[ \r\t]+trigger/i', $this->getRapidRepairSql() ) ) {
			$this->m_boolHasDisableTrigger = true;
		}

		if( 1 == preg_match( '/disable[ \r\t]+trigger/i', $this->getStandardRepairSql() ) ) {
			$this->m_boolHasDisableTrigger = true;
		}

		// CDiagnosticsScripts
		if( SYSTEM_USER_ID == $objUser->getId() ) {
			if( true == $this->getEnableAutoRepair() && false == $this->m_boolHasDisableTrigger ) {
				return true;
			}

			return false;

			// interface users
		} elseif( SYSTEM_USER_ID != $objUser->getId() ) {

			if( true == $this->m_boolHasDisableTrigger && false == $this->m_boolBetweenTwelveAmAndSixAm ) {
				return false;
			}

			$this->m_boolAllowDiagnosticRepair = false;

			if( true == valArr( $arrobjLoggedInUserRoles ) ) {
				$this->m_boolAllowDiagnosticRepair = array_key_exists( CRole::ALLOW_DIAGNOSTIC_REPAIR, $arrobjLoggedInUserRoles );
			}

			if( $this->getDeveloperEmployeeId() == $objEmployee->getId() ) {
				$this->m_boolIsDiagnosticDeveloper = true;
			}

			if( true == $this->m_boolIsDiagnosticDeveloper || true == $objUser->getIsSuperUser() || true == $this->m_boolAllowDiagnosticRepair ) {
				return true;
			}

			return false;
		}
	}

	public function runDiagnostic( $objUser, $arrintDiagnosticPermissions = [], $arrobjTargetDatabases, $arrobjTargetDatabasesPsDba, $objLogsDatabase, $intPropertyIds = NULL, $objAdminDatabase = NULL, $intScriptReplicaLagTime = NULL, $boolIsDebugging = false ) {

		if( false == valArr( $arrobjTargetDatabases ) ) {
			echo 'Minimally, target database(s) must be passed.';
			exit;
		}

		$arrstrDiagnosticLogs = [];

		if( true == valObj( $objAdminDatabase, 'CDatabase' ) ) {
			$this->m_objAdminDatabase = $objAdminDatabase;
		}

		// Todo: Admin database should be passed from caller class
		if( false == valObj( $this->m_objAdminDatabase, 'CDatabase' ) ) {
			$this->m_objAdminDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::ADMIN, CDatabaseUserType::PS_DEVELOPER );
		}

		$this->m_objAdminDatabase->open();

		$arrobjLoggedInUserRoles 	= $this->getOrFetchLoggedInUserRoles( $objUser );
		$objEmployee 				= $this->getOrFetchEmployee( $objUser );

		$this->m_objAdminDatabase->close();

		foreach( $arrobjTargetDatabases as $objTargetDatabase ) {

			if( false == valObj( $objTargetDatabase, 'CDatabase' ) ) {
				echo 'Unable to load the Database.';
				exit;
			}

			$intDatabaseId						= $objTargetDatabase->getId();
			$boolRunDiagnosticRepairPermitted	= false;
			$objTargetDatabasePsDba				= NULL;

			if( true == isset( $arrobjTargetDatabasesPsDba ) && true == array_key_exists( $intDatabaseId, $arrobjTargetDatabasesPsDba ) ) {
				$objTargetDatabasePsDba				= $arrobjTargetDatabasesPsDba[$intDatabaseId];
				$boolRunDiagnosticRepairPermitted	= $this->runDiagnosticRepairPermitted( $objUser, $objTargetDatabasePsDba, $arrobjLoggedInUserRoles, $objEmployee );
			}

			// Make sure the database_user_type_id = diagnostics
			/* if( CDatabaseUserType::DIAGNOSTICS != $objTargetDatabase->getDatabaseUserTypeId() ) {
				echo 'Only the diagnostics database user can run a diagnostic view SQL.';
				exit;
			} */

			// Make sure we don't run against ineligible clusters.
			if( ( false == is_null( $this->getRapidViewSql() ) && CCluster::RAPID == $objTargetDatabase->getClusterId() ) || ( false == is_null( $this->getStandardViewSql() ) && CCluster::STANDARD == $objTargetDatabase->getClusterId() ) ) {

				$objTargetDatabase->open();

				if( true == $boolRunDiagnosticRepairPermitted ) {
					$objTargetDatabasePsDba->open();
				}

				$fltTimeStart = microtime( true );

				// get RO DB Lag Time passing through schedule script only else use set over diagnostic
				$intReplicaLagTime = $intScriptReplicaLagTime ?? $this->getReplicaLagTime();

				// Diagnostics are processed and formatted differently depending on whether they are client-specific or not.
				if( 1 == $this->getIsClientSpecific() ) {
					// Allowed for all products, as when all products is selected in diagnostics settings it picks up the 0th index of diagnostic permission
					if( true == isset( $arrintDiagnosticPermissions[$intDatabaseId][0] ) ) {
						$arrintCids = array_keys( ( array ) $arrintDiagnosticPermissions[$intDatabaseId][0] );
					} else {
						$arrintCids = [];
					}

					// Do no run diagnostic for test client, if Run for Test DB is not checked.
					if( false == $this->getRunForTestDb() && CDatabaseType::CLIENT == $objTargetDatabase->getDatabaseTypeId() ) {
						$arrintClientIds	= ( array ) CClients::fetchClientsIdsByCompanyStatusTypeId( CCompanyStatusType::CLIENT, $objTargetDatabase );
						$arrintCids			= array_intersect( array_column( $arrintClientIds, 'id' ), $arrintCids );
					}

					if( false == valArr( $arrintCids ) ) {
						echo 'No Clients found.';
						continue;
					}

					$strViewSql = $this->loadViewSql( $objTargetDatabase->getClusterId(), implode( ',', $arrintCids ), $intPropertyIds, $objLogsDatabase );
					$strViewSql = 'SET STATEMENT_TIMEOUT = \'600s\';' . $strViewSql;

					if( true == $boolIsDebugging ) {
						display( $strViewSql );
					}

					// We are assuming that ; will always be used in case of Master DB. As we are creating some temp table.
					if( 1 < substr_count( $strViewSql, ';' ) || true == $this->getRunOnMasterDb() ) {
						$objTargetDatabase->open();
						$arrstrPreRepairResults = executeSql( $strViewSql, $objTargetDatabase );
						$objTargetDatabase->close();
					} else {
						$objTargetDatabase->setUseReplica( true, $intReplicaLagTime );
						$arrstrPreRepairResults = executeSql( $strViewSql, $objTargetDatabase, $boolUseMasterDatabase = false );
					}

					if( true == $boolRunDiagnosticRepairPermitted ) {

						switch( NULL ) {
							default:
								$objTargetDatabasePsDba->begin();

								if( false == $objTargetDatabasePsDba->execute( $this->loadRepairSql( $objTargetDatabasePsDba->getClusterId(), implode( ',', $arrintCids ), $intPropertyIds ) ) ) {
									// the normal ajax_messages.tpl won't work because pattern.js will intercept and display its own error message for AJAX calls.
									$this->m_strErrorMessage = pg_last_error();
									$objTargetDatabasePsDba->rollback();
									break;
								} else {
									// If repair is occurring through the interface, then do this...
									if( SYSTEM_USER_ID != $objUser->getId() ) {
										$this->setLastRepairedBy( $objUser->getId() );
										$this->setLastRepairedOn( 'now()' );
										$this->update( $objUser->getId(), $objLogsDatabase );
									}
								}

								$objTargetDatabasePsDba->commit();
						}

						$arrstrPostRepairResults		= executeSql( $strViewSql, $objTargetDatabasePsDba );
						$arrstrRekeyedPostRepairResults = rekeyArray( 'cid', $arrstrPostRepairResults['data'], $boolMakeKeyLowerCase = false, $boolHasMultipleArraysWithSameKey = true );
					}

					$arrstrRekeyedResults	= rekeyArray( 'cid', $arrstrPreRepairResults['data'], $boolMakeKeyLowerCase = false, $boolHasMultipleArraysWithSameKey = true );
					$fltProcessingTime		= ( ( microtime( true ) - $fltTimeStart ) / \Psi\Libraries\UtilFunctions\count( $arrintCids ) );

					foreach( $arrintCids as $intCid ) {
						$intCount			= ( true == isset ( $arrstrRekeyedResults[$intCid][0]['count'] ) ) ? ( int ) $arrstrRekeyedResults[$intCid][0]['count'] : 0;
						$intPostRepairCount = ( true == isset ( $arrstrRekeyedPostRepairResults[$intCid][0]['count'] ) ) ? ( int ) $arrstrRekeyedPostRepairResults[$intCid][0]['count'] : 0;

						$arrstrDiagnosticLogs[] = [ 'cid' => $intCid, 'count' => $intCount, 'post_repair_count' => $intPostRepairCount, 'diagnostic_result_type_id' => CDiagnosticResultType::getDiagnosticResultTypeId( $intCount, $arrstrPreRepairResults ), 'processing_time' => $fltProcessingTime, 'database_id' => $intDatabaseId ];
					}
				} else {

					$strViewSql = $this->loadViewSql( $objTargetDatabase->getClusterId() );
					if( 1 < substr_count( $strViewSql, ';' ) || true == $this->getRunOnMasterDb() ) {
						$objTargetDatabase->open();
						$arrstrPreRepairResults = executeSql( $strViewSql, $objTargetDatabase );
						$objTargetDatabase->close();
					} else {
						$objTargetDatabase->setUseReplica( true, $intReplicaLagTime );
						$arrstrPreRepairResults = executeSql( $strViewSql, $objTargetDatabase, $boolUseMasterDatabase = false );
					}

					if( true == $boolRunDiagnosticRepairPermitted ) {
						switch( NULL ) {
							default:
								$objTargetDatabasePsDba->begin();
								if( false == $objTargetDatabasePsDba->execute( $this->loadRepairSql( $objTargetDatabasePsDba->getClusterId() ) ) ) {
									$this->m_strErrorMessage = pg_last_error();
									$objTargetDatabasePsDba->rollback();
									break;
								}
								$objTargetDatabasePsDba->commit();
						}

						$arrstrPostRepairResults = executeSql( $strViewSql, $objTargetDatabasePsDba );
					}

					$intCount			 = ( true == isset ( $arrstrPreRepairResults['data'][0]['count'] ) ) ? $arrstrPreRepairResults['data'][0]['count'] : 0;
					$intPostRepairCount	 = ( true == isset ( $arrstrPostRepairResults['data'][0]['count'] ) ) ? $arrstrPostRepairResults['data'][0]['count'] : 0;
					$fltProcessingTime	 = microtime( true ) - $fltTimeStart;

					$arrstrDiagnosticLogs[] = [ 'count' => $intCount, 'post_repair_count' => $intPostRepairCount, 'diagnostic_result_type_id' => CDiagnosticResultType::getDiagnosticResultTypeId( $intCount, $arrstrPreRepairResults ), 'processing_time' => $fltProcessingTime, 'database_id' => $intDatabaseId ];
				}

				$objTargetDatabase->close();
				if( true == $boolRunDiagnosticRepairPermitted ) {
					$objTargetDatabasePsDba->close();
				}
			}
		}

		$intTotalErrorCount		 = 0;
		$fltTotalProcessingTime	 = 0;
		$intSqlFailureCount		 = 0;
		$intRepairCount			 = 0;
		$intTotalRepairCount	 = 0;

		$arrobjDiagnosticLogs = [];

		if( true == valArr( $arrstrDiagnosticLogs ) ) {

			$strSql = '';

			foreach( $arrstrDiagnosticLogs as $arrstrDiagnosticLog ) {

				$intCid			 = ( true == isset ( $arrstrDiagnosticLog['cid'] ) ) ? $arrstrDiagnosticLog['cid'] : NULL;
				$intDatabaseId	 = ( true == isset ( $arrstrDiagnosticLog['database_id'] ) ) ? $arrstrDiagnosticLog['database_id'] : NULL;
				$intCount		 = ( true == isset ( $arrstrDiagnosticLog['count'] ) ) ? $arrstrDiagnosticLog['count'] : 0;
				$intPostRepairCount = ( true == isset ( $arrstrDiagnosticLog['post_repair_count'] ) ) ? $arrstrDiagnosticLog['post_repair_count'] : 0;

				if( true == $boolRunDiagnosticRepairPermitted ) {
					$intRepairCount = $intCount - $intPostRepairCount;
				}

				$intDiagnosticResultTypeId	 = ( true == isset ( $arrstrDiagnosticLog['diagnostic_result_type_id'] ) ) ? $arrstrDiagnosticLog['diagnostic_result_type_id'] : CDiagnosticResultType::FAILURE;
				$fltProcessingTime			 = ( true == isset ( $arrstrDiagnosticLog['processing_time'] ) ) ? $arrstrDiagnosticLog['processing_time'] : 0;

				$intTotalErrorCount += $intCount;
				$intTotalRepairCount += $intRepairCount;
				$fltTotalProcessingTime += $fltProcessingTime;
				$intSqlFailureCount += ( CDiagnosticResultType::SQL_ERROR == $intDiagnosticResultTypeId ) ? 1 : 0;

				$objDiagnosticLog = $this->createDiagnosticLog( $intCid );
				$objDiagnosticLog->setDiagnosticResultTypeId( $intDiagnosticResultTypeId );
				$objDiagnosticLog->setItemCount( $intCount );
				if( true == $boolRunDiagnosticRepairPermitted ) {
					$objDiagnosticLog->setRepairCount( $intRepairCount );
				}

				$objDiagnosticLog->setSeconds( $fltProcessingTime );
				$objDiagnosticLog->setDatabaseId( $intDatabaseId );
				$strSql .= $objDiagnosticLog->insert( $objUser->getId(), $objLogsDatabase, $boolReturnSqlOnly = true );

				$arrobjDiagnosticLogs[] = $objDiagnosticLog;
			}

			if( false == valStr( $intPropertyIds ) && false === fetchData( $strSql, $objLogsDatabase ) ) {
				trigger_error( 'Diagnostic logs failed to insert.', E_USER_WARNING );
			}
		}

		return [ 'error_count' => $intTotalErrorCount, 'repair_count' => $intTotalRepairCount, 'diagnostic_logs' => $arrobjDiagnosticLogs, 'processing_time' => $fltTotalProcessingTime, 'sql_failures' => $intSqlFailureCount ];
	}

	public function runPropertyLevelDiagnostic( $intCid, $arrobjTargetDatabases, $intPropertyIds, $objLogsDatabase, $intScriptReplicaLagTime = NULL ) {

		if( false == valArr( $arrobjTargetDatabases ) ) {
			echo 'Minimally, target diagnostics database(s) must be passed.';
			exit;
		}

		$intTotalErrorCount		 = 0;
		$fltTotalProcessingTime	 = 0;
		$intSqlFailureCount		 = 0;
		$intTotalRepairCount	 = 0;

		foreach( $arrobjTargetDatabases as $objTargetDatabase ) {

			if( false == valObj( $objTargetDatabase, 'CDatabase' ) ) {
				echo 'Unable to load the Database.';
				exit;
			}

			// Make sure the database_user_type_id = diagnostics
			/* if( CDatabaseUserType::DIAGNOSTICS != $objTargetDatabase->getDatabaseUserTypeId() ) {
				echo 'Only the diagnostics database user can run a diagnostic view SQL.';
				exit;
			} */

			// Make sure we don't run against ineligible clusters.
			if( ( false == is_null( $this->getRapidViewSql() ) && CCluster::RAPID == $objTargetDatabase->getClusterId() ) || ( false == is_null( $this->getStandardViewSql() ) && CCluster::STANDARD == $objTargetDatabase->getClusterId() ) ) {

				$fltTimeStart = microtime( true );

				// get RO DB Lag Time passing through schedule script only else use set over diagnostic
				$intReplicaLagTime = $intScriptReplicaLagTime ?? $this->getReplicaLagTime();

				$strViewSql = $this->loadViewSql( $objTargetDatabase->getClusterId(), $intCid, $intPropertyIds, $objLogsDatabase );

				if( 1 < substr_count( $strViewSql, ';' ) || true == $this->getRunOnMasterDb() ) {
					$objTargetDatabase->open();
					$arrstrPreRepairResults = executeSql( $strViewSql, $objTargetDatabase );
					$objTargetDatabase->close();
				} else {
					$objTargetDatabase->setUseReplica( true, $intReplicaLagTime );
					$arrstrPreRepairResults = executeSql( $strViewSql, $objTargetDatabase, $boolUseMasterDatabase = false );
				}
			}

			$intTotalErrorCount	 = ( true == isset ( $arrstrPreRepairResults['data'][0]['count'] ) ) ? $arrstrPreRepairResults['data'][0]['count'] : 0;
			$fltTotalProcessingTime = microtime( true ) - $fltTimeStart;
		}

		return [ 'error_count' => $intTotalErrorCount, 'repair_count' => $intTotalRepairCount, 'seconds' => $fltTotalProcessingTime, 'sql_failures' => $intSqlFailureCount, 'log_datetime' => date( 'm/d/Y H:i:s' ) ];

	}

	public function loadDiagnosticData( $arrintDiagnosticPermissions = [], $arrobjTargetDatabases, $intPropertyIds = NULL ) {

		if( false == valArr( $arrobjTargetDatabases ) ) {
			trigger_error( 'Target database must be passed.', E_USER_ERROR );
			exit;
		}

		$arrintTotalData = [];

		foreach( $arrobjTargetDatabases as $objTargetDatabase ) {

			// Make sure the database_user_type_id = diagnostics
			/* if( CDatabaseUserType::DIAGNOSTICS != $objTargetDatabase->getDatabaseUserTypeId() ) {
				trigger_error( 'Only the diagnostics database user can run a diagnostic.', E_USER_ERROR );
				exit;
			} */

			// Make sure we don't run against ineligible clusters.
			if( ( false == is_null( $this->getRapidViewSql() ) && CCluster::RAPID == $objTargetDatabase->getClusterId() ) || ( false == is_null( $this->getStandardViewSql() ) && CCluster::STANDARD == $objTargetDatabase->getClusterId() ) ) {
				$objTargetDatabase->open();

				$fltTimeStart = microtime( true );

				// Diagnostics are processed and formatted differently depending on whether they are client-specific or not.
				if( 1 == $this->getIsClientSpecific() ) {

					if( true == isset ( $arrintDiagnosticPermissions[$objTargetDatabase->getId()][0] ) ) {
						$arrintCids = array_keys( ( array ) $arrintDiagnosticPermissions[$objTargetDatabase->getId()][0] );
					} else {
						$arrintCids = [];
					}

					if( false == valArr( $arrintCids ) ) {
						continue;
					}

					$arrstrData = ( array ) fetchData( $this->loadViewDataSql( $objTargetDatabase->getClusterId(), implode( ',', $arrintCids ), NULL, $intPropertyIds ), $objTargetDatabase );
				} else {

					$arrstrData = ( array ) fetchData( $this->loadViewDataSql( $objTargetDatabase->getClusterId(), NULL, NULL, $intPropertyIds ), $objTargetDatabase );
				}

				$arrintTotalData = array_merge( $arrintTotalData, $arrstrData );

				$objTargetDatabase->close();
			}
		}

		return $arrintTotalData;
	}

	public function loadViewDataSql( $intClusterId, $strCids = NULL, $intLimit = NULL, $intPropertyIds = NULL, $objLogsDatabase = NULL ) {
		if( CCluster::STANDARD == $intClusterId ) {
			$strSql = $this->getStandardViewSql();
		} else {
			$strSql = $this->getRapidViewSql();
		}

		$strSql = ( true == valStr( $strCids ) ) ? str_replace( '#CID#', $strCids, $strSql ) : $strSql;
		$strSql = ( true == valStr( $intPropertyIds ) ) ? str_replace( '#PID#', $intPropertyIds, $strSql ) : str_replace( '#PID#', '', $strSql );

		$strSql = preg_replace( '/\;$/m', '', $strSql );

		if( false == empty( $intLimit ) ) {
			$strSql .= ' limit ' . ( int ) $intLimit . ';';
		}
		// fetch exceptions ids and append those in SQL
		if( true == valStr( $strCids ) && true == valObj( $objLogsDatabase, 'CDatabase' ) ) {
			$arrobjDiagnosticExceptions = $this->fetchDiagnosticExceptions( $strCids, $objLogsDatabase );
			$strExceptionIds = NULL;
			if( true == valArr( $arrobjDiagnosticExceptions ) ) {
				foreach( $arrobjDiagnosticExceptions as $objDiagnosticException ) {
					$intExceptionCid = $objDiagnosticException->getCid();
					foreach( $objDiagnosticException->getExceptionIds() as $intExceptionId ) {
						$strExceptionIds .= ( ( true == valStr( $strExceptionIds ) ) ? ',' : NULL ) . '(' . $intExceptionCid . ',' . $intExceptionId . ')';
					}
				}
				$strSql	= str_replace( '#EXCEPTIONS#', $strExceptionIds, $strSql );
			} else {
				$strExceptionIds = '(0,0)';
				$strSql	= str_replace( '#EXCEPTIONS#', $strExceptionIds, $strSql );
			}
		}
		return $strSql;
	}

	public function loadViewSql( $intClusterId, $strCids = NULL, $intPropertyIds = NULL, $objLogsDatabase = NULL ) {

		if( CCluster::STANDARD == $intClusterId ) {
			$strSql = $this->getStandardViewSql();
		} else {
			$strSql = $this->getRapidViewSql();
		}

		$strOuterSql = '';

		$arrstrViewSubSqls = array_filter( explode( ';', trim( $strSql ) ) );
		$intSubSqlCount    = \Psi\Libraries\UtilFunctions\count( $arrstrViewSubSqls );

		if( 1 < $intSubSqlCount ) {
			$strSubSql = array_pop( $arrstrViewSubSqls );
			$strOuterSql = implode( ';', $arrstrViewSubSqls ) . ';';
		} else {
			$strSubSql = array_pop( $arrstrViewSubSqls );
		}

		$strSubSql		= ( true == valStr( $intPropertyIds ) ) ? str_replace( '#PID#', $intPropertyIds, $strSubSql ) : str_replace( '#PID#', '', $strSubSql );
		if( true == valStr( $strOuterSql ) ) {
			$strOuterSql	= ( true == valStr( $intPropertyIds ) ) ? str_replace( '#PID#', $intPropertyIds, $strOuterSql ) : str_replace( '#PID#', '', $strOuterSql );
		}

		// fetch exceptions ids and append those in SQL
		if( true == valStr( $strCids ) && true == valObj( $objLogsDatabase, 'CDatabase' ) ) {
			$arrobjDiagnosticExceptions = $this->fetchDiagnosticExceptions( $strCids, $objLogsDatabase );
			$strExceptionIds = NULL;
			if( true == valArr( $arrobjDiagnosticExceptions ) ) {
				foreach( $arrobjDiagnosticExceptions as $objDiagnosticExceptions ) {
					$intExceptionCid = $objDiagnosticExceptions->getCid();
					foreach( $objDiagnosticExceptions->getExceptionIds() as $intExceptionId ) {
						$strExceptionIds .= ( ( true == valStr( $strExceptionIds ) ) ? ',' : NULL ) . '(' . $intExceptionCid . ',' . $intExceptionId . ')';
					}
				}
				$strSubSql	= str_replace( '#EXCEPTIONS#', $strExceptionIds, $strSubSql );
			} else {
				$strExceptionIds = '(0,0)';
				$strSubSql	= str_replace( '#EXCEPTIONS#', $strExceptionIds, $strSubSql );
			}
		}

		if( 1 == $this->getIsClientSpecific() ) {
			$strSubSql		= ( true == valStr( $strCids ) ) ? str_replace( '#CID#', $strCids, $strSubSql ) : $strSubSql;
			$strOuterSql	= ( true == valStr( $strCids ) ) ? str_replace( '#CID#', $strCids, $strOuterSql ) : $strOuterSql;
			$strReturnSql	= $strOuterSql . ' SELECT cid, count(*) FROM ( ' . $strSubSql . ' ) AS sub GROUP BY cid;';
		} else {
			$strReturnSql	= $strOuterSql . ' SELECT count(*) FROM ( ' . $strSubSql . ' ) AS sub;';
		}

		return $strReturnSql;
	}

	public function loadRepairSql( $intClusterId, $strCids = NULL, $intPropertyIds = NULL ) {

		if( CCluster::STANDARD == $intClusterId ) {
			$strSql = $this->getStandardRepairSql();
		} else {
			$strSql = $this->getRapidRepairSql();
		}

		$strSql = ( false == is_null( $strCids ) ) ? str_replace( '#CID#', $strCids, $strSql ) : $strSql;
		$strSql = ( true == valStr( $intPropertyIds ) ) ? str_replace( '#PID#', $intPropertyIds, $strSql ) : str_replace( '#PID#', '', $strSql );

		return $strSql;
	}

	public function fetchClientErrorCountsCount( $intCid = NULL, $objLogsDatabase ) {
		return \Psi\Eos\Logs\CDiagnostics::createService()->fetchClientErrorCountsCountByDiagnosticId( $this->getId(), $intCid, $objLogsDatabase );
	}

	public function fetchClientErrorCounts( $strSortBy = 'database_id', $intCid = NULL, $objPagination, $objLogsDatabase, $objAdminDatabase, $objConnectDatabase ) {
		return \Psi\Eos\Logs\CDiagnostics::createService()->fetchClientErrorCountsByDiagnosticId( $this->getId(), $intCid, $strSortBy, $objPagination, $objLogsDatabase, $objAdminDatabase, $objConnectDatabase );
	}

	public function fetchDatabaseErrorCounts( $arrintDatabaseIds, $objLogsDatabase, $objConnectDatabase ) {
		return \Psi\Eos\Logs\CDiagnostics::createService()->fetchDatabaseErrorCountsByDiagnosticId( $this->getId(), $arrintDatabaseIds, $objLogsDatabase, $objConnectDatabase );
	}

	public function fetchDiagnosticType( $objDatabase ) {
		return \Psi\Eos\Logs\CDiagnosticTypes::createService()->fetchDiagnosticTypeById( $this->getDiagnosticTypeId(), $objDatabase );
	}

	public function fetchTargetDatabases( $intClientDatabaseId, $objConnectDatabase, $objLogsDatabase ) {

		$objDiagnosticType = $this->fetchDiagnosticType( $objLogsDatabase );

		assertObj( $objDiagnosticType, 'CDiagnosticType' );

		if( CDatabaseType::CLIENT == $objDiagnosticType->getDatabaseTypeId() ) {
			$objDatabase = CDatabases::fetchDatabaseByIdByDatabaseUserTypeIdByDatabaseTypeId( $intClientDatabaseId, CDatabaseUserType::DIAGNOSTICS, $objDiagnosticType->getDatabaseTypeId(), $objConnectDatabase );

			return [ $objDatabase->getId() => $objDatabase ];
		} else {
			return CDatabases::fetchDatabasesByDatabaseUserTypeIdByDatabaseTypeId( $objDiagnosticType->getDatabaseTypeId(), CDatabaseUserType::DIAGNOSTICS, $objConnectDatabase, $boolFetchMasterDatabases = true );
		}
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );

		return $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function fetchDiagnosticExceptions( $strCids, $objLogsDatabase ) {
		return \Psi\Eos\Logs\CDiagnosticExceptions::createService()->fetchActiveDiagnosticExceptionByCidByDiagnosticId( $strCids, $this->getId(), $objLogsDatabase );
	}

}

?>