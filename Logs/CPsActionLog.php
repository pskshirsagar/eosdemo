<?php

class CPsActionLog extends CBasePsActionLog {

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valWebsiteId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valModuleName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valActionName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valClickDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valClickStarts() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valClickCompletes() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTotalDuplicateCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTotalSqlCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTotalSqlTime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTotalPhpTime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTotalRunTime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsAdministrator() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>