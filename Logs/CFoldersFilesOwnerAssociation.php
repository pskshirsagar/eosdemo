<?php

use Psi\Eos\Admin\CPsProductOptions;

class CFoldersFilesOwnerAssociation extends CBaseFoldersFilesOwnerAssociation {

	const FOLDER_ENTRATA_APPLICATIONS  = 220;
	const FOLDER_SCRIPTS        = 6307;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFoldersFilesOwnerAssociationsId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase, $boolIsFile = true ) {
		$boolIsValid = true;

		if( 0 == strlen( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Folder / File Name required. ' ) );
		} else {
			if( false == $boolIsFile ) {

				// Folder Name Validation
				if( false == preg_match( '/^[a-zA-Z0-9-_]+$/', $this->getName() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Invalid Folder Name. ' ) );
				}
			} else {

				$arrstrFileDeatils = pathinfo( $this->getName() );

				// File Name Validation
				if( 'php' !== $arrstrFileDeatils['extension'] && 'tpl' !== $arrstrFileDeatils['extension'] ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Invalid File Type. ' ) );
				}

				if( false == preg_match( '/^[a-zA-Z0-9-._]+$/', $arrstrFileDeatils['filename'] ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Invalid File Name. ' ) );
				}
			}
		}

		// Check if alredy present in system
		if( true == $boolIsValid ) {
			$intCount = CFoldersFilesOwnerAssociations::fetchFoldersFilesOwnerAssociationsByFoldersFilesOwnerAssociationsIdByName( $this->getFoldersFilesOwnerAssociationsId(), $this->getName(), $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				if( true == $boolIsFile ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'File Name already exists. ' ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Folder Name already exists. ' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valFilePath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDevEmployeeId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getDevEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'dev_employee_id', 'Developer is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valQaEmployeeId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getQaEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'qa_employee_id', 'QA is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getPsProductId() ) && false == is_numeric( $this->getPsProductOptionId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product is required. ' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolIsFile = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'insert_file_assocition':
			case 'insert_folder_assocition':
				$boolIsValid &= $this->valName( $objDatabase, $boolIsFile );
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valDevEmployeeId();
				$boolIsValid &= $this->valQaEmployeeId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>