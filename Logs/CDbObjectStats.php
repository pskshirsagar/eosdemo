<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CDbObjectStats
 * Do not add any new functions to this class.
 */

class CDbObjectStats extends CBaseDbObjectStats {

	const TABLE_ROW_COUNT_SIZE_MINIMUM	= 1000000;
	const OBJECT_SIZE_BYTE_MINIMUM		= 100000000;

	public static function fetchDbObjectStats( $strSql, $objDatabase ) {
		return self::fetchObjects( $strSql, 'CDbObjectStat', $objDatabase, $boolIsReturnKeyedArray = true );

	}

	public static function fetchDbObjectStat( $strSql, $objDatabase ) {
		return self::fetchObject( $strSql, 'CDbObjectStat', $objDatabase );
	}

	public static function fetchDatabaseTableStatsByDatabaseId( $intDatabaseId, $objDatabase, $intDatabaseTransactionLimit = 8 ) {

		$strSql = '
			SELECT
				sub.*,
				CASE
					WHEN sub.rank != 1 AND sub.old_count::DECIMAL !=0 THEN
						round( ( ( sub.new_count::DECIMAL - sub.old_count::DECIMAL ) / sub.old_count::DECIMAL ) * 100, 3 )
					WHEN sub.rank != 1 AND sub.old_count::DECIMAL = 0 THEN
						0
					ELSE NULL
				END AS percent_change
			FROM (
				WITH database_transaction_limit_set AS (
					SELECT
						DISTINCT dos.database_transaction_id
					FROM
						db_object_stats dos
					ORDER BY
						dos.database_transaction_id DESC
					LIMIT  ' . ( int ) $intDatabaseTransactionLimit . '
				)
				SELECT
					rank() OVER (partition by t.name order by t.name ASC, dos.database_transaction_id ASC),
					lag(count) OVER (order by t.name ASC, dos.database_transaction_id ASC) as old_count,
					dos.database_transaction_id,
					t.name,
					dos.count as new_count,
					dos.log_datetime
				FROM
					db_object_stats dos
					LEFT JOIN db_objects t ON t.id = dos.db_object_id
				WHERE
					t.database_id = ' . ( int ) $intDatabaseId . '
					AND t.db_object_type_id = ' . ( int ) CDbObjectType::TABLE . '
					AND dos.database_transaction_id in (select database_transaction_id from database_transaction_limit_set)
					AND dos.count >= ' . ( int ) self::TABLE_ROW_COUNT_SIZE_MINIMUM . '
			) as sub';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDatabaseFunctionStatsByDatabaseId( $intDatabaseId, $objDatabase, $intDatabaseTransactionLimit = 8 ) {

		$strSql = '
			SELECT
				sub.*,
				CASE WHEN sub.rank != 1 AND sub.old_count::DECIMAL !=0 THEN
					round( ( ( sub.new_count::DECIMAL - sub.old_count::DECIMAL ) / sub.old_count::DECIMAL ) * 100, 3 )
				ELSE NULL
				END AS percent_change
			FROM (
				WITH database_transaction_limit_set AS (
					SELECT
						DISTINCT dos.database_transaction_id
					FROM
						db_object_stats dos
					ORDER BY
						dos.database_transaction_id DESC
					LIMIT  ' . ( int ) $intDatabaseTransactionLimit . '
				)
				SELECT
					rank() OVER (partition by dbo.name order by dbo.name ASC, dos.database_transaction_id ASC),
					lag(count) OVER (order by dbo.name ASC, dos.database_transaction_id ASC) as old_count,
					dos.database_transaction_id,
					dbo.name AS function_name,
					dos.count as new_count,
					dos.log_datetime
				FROM
					db_object_stats dos
					JOIN db_objects dbo ON dbo.id = dos.db_object_id 
				WHERE
					dbo.database_id = ' . ( int ) $intDatabaseId . '
					AND dbo.db_object_type_id = ' . ( int ) CDbObjectType::DB_OBJECT_TYPE_FUNCTION . '
				AND dos.database_transaction_id in (select database_transaction_id from database_transaction_limit_set)

			) as sub';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTableStats( $objDatabase, $objDatabaseFilter, $boolIsPagination = false, $intLimit = 0, $intOffset = 0 ) {

		$strSql = '
			SELECT
				sub.table_name,
				sub.database_type_id,
				count ( sub.database_id ) AS count
			FROM
				(
				SELECT
					DISTINCT t.name as table_name,
					d.database_type_id,
					t.database_id
				FROM
					db_objects t
					JOIN databases d ON d.id = t.database_id
				WHERE
					d.database_type_id = ' . ( int ) $objDatabaseFilter->getDatabaseTypeId() . '
					AND t.db_object_type_id = ' . CDbObjectType::TABLE . '
				) AS sub
			GROUP BY
				sub.table_name,
				sub.database_type_id
			ORDER BY
				sub.table_name';

		if( true == $boolIsPagination )
			$strSql .= ' LIMIT ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchTableDatabaseStatsByTableId( $intTableId, $intDbObjectTypeId, $objDatabase, $intDatabaseTransactionLimit = 6 ) {

			$strSql = '
						SELECT
						   sub.*,
						   CASE
						     WHEN sub.rank != 1 AND sub.old_count::DECIMAL != 0 THEN round ( ( ( sub.new_count::DECIMAL - sub.old_count::DECIMAL ) / sub.old_count::DECIMAL ) * 100, 2 )
						     ELSE NULL
						   END AS percent_change
						FROM
						   (
						     SELECT
						         rank ( ) OVER ( partition BY d.database_name ORDER BY t.name ASC, dos.log_datetime ASC ),
						         lag ( count ) OVER ( PARTITION BY dos.db_object_id, t.database_id ORDER BY dos.db_object_id, t.database_id, dos.log_datetime ASC, dos.database_transaction_id ASC ) AS old_count,
						         d.id AS database_id,
						         d.database_name,
						         dos.database_transaction_id,
						         t.name,
						         t.id AS table_id,
						         dos.count AS new_count,
						         dos.size,
						         dos.log_datetime
						     FROM
						         db_object_stats dos
						         LEFT JOIN db_objects t ON t.id = dos.db_object_id
						         LEFT JOIN databases d ON d.id = t.database_id
						     WHERE
						         t.db_object_type_id = ' . ( int ) CDbObjectType::TABLE . '
						         AND t.table_id = ' . ( int ) $intTableId . '
						         AND date( dos.log_datetime ) IN ( SELECT
						                                                 DISTINCT ( date( log_datetime ) ) as log_datetime
						                                                FROM
						                                                 db_object_stats
						                                                ORDER BY
						                                                 log_datetime DESC
						                                                LIMIT
						                                                 ' . ( int ) $intDatabaseTransactionLimit . '
						                                         )
						) as sub';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFunctionStats( $objDatabase, $objDatabaseFilter ) {

		$strSql = '
			WITH max_database_transaction_id AS (
				select max(dos.database_transaction_id) from db_object_stats dos
			)
			SELECT
				sub.function_name,
				d.database_type_id,
				count( d.id ) as count,
				sub2.latest_call_count AS latest_total_database_call_count
			FROM
				databases d
				JOIN
				(
					SELECT
						DISTINCT d.id as database_id, dbo.name as function_name
					FROM
						db_object_stats dos
						JOIN db_objects dbo ON dbo.id = dos.db_object_id
						JOIN databases d ON d.id = dbo.database_id
					WHERE
						dbo.db_object_type_id = ' . ( int ) CDbObjectType::DB_OBJECT_TYPE_FUNCTION . '
					ORDER BY
						d.id ASC,
						dbo.name ASC
				) AS sub ON sub.database_id = d.id
				JOIN
				(
					SELECT
						dbo.name as function_name,
						sum( dos.count ) as latest_call_count
					FROM
						db_object_stats dos
						JOIN db_objects dbo ON dbo.id = dos.db_object_id
						JOIN databases d on d.id = dbo.database_id
					WHERE
						dos.database_transaction_id IN (select * from max_database_transaction_id)
						AND dbo.db_object_type_id = ' . ( int ) CDbObjectType::DB_OBJECT_TYPE_FUNCTION . '
					GROUP BY
						dbo.name
				) AS sub2 on sub2.function_name = sub.function_name
			WHERE d.database_type_id = ' . ( int ) $objDatabaseFilter->getDatabaseTypeId() . '
			GROUP BY
				sub.function_name,
				d.database_type_id,
				sub2.latest_call_count
			ORDER BY
				d.database_type_id ASC,
				sub.function_name ASC,
				sub2.latest_call_count';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchFunctionDatabaseStatsByFunctionName( $strFunctionName, $objDatabase, $intDatabaseTransactionLimit = 8 ) {

		$strSql = '
			SELECT
					sub.*,
					CASE WHEN sub.rank != 1 AND sub.old_count::DECIMAL !=0 THEN
						round( ( ( sub.new_count::DECIMAL - sub.old_count::DECIMAL ) / sub.old_count::DECIMAL ) * 100, 3 )
					ELSE NULL
					END AS percent_change
			FROM (
				WITH database_transaction_limit_set AS (
					SELECT
						DISTINCT dos.database_transaction_id
					FROM
						db_object_stats dos
					ORDER BY
						dos.database_transaction_id DESC
					LIMIT ' . ( int ) $intDatabaseTransactionLimit . '
				)
				SELECT
					rank() OVER ( PARTITION BY d.database_name ORDER BY dbo.name ASC, dbo.database_id ASC, dos.database_transaction_id ASC ) as rank,
					dbo.database_id,
					d.database_name,
					dos.database_transaction_id,
					dbo.name as function_name,
					lag(count) OVER (ORDER BY substring ( d.database_name FROM \'[0-9]+\' )::integer ASC, dos.database_transaction_id ASC) as old_count,
					dos.count as new_count,
					dos.log_datetime
				FROM
					db_object_stats dos
					JOIN db_objects dbo ON dbo.id = dos.db_object_id
					LEFT JOIN databases d ON d.id = dbo.database_id
				WHERE
					dbo.db_object_type_id = 3
					AND dbo.name = \'' . $strFunctionName . '\'
			) AS sub';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchDropFunctionSql( $strFunctionName, $objDatabase ) {

		$strSql = '
			SELECT
				\'DROP FUNCTION IF EXISTS \' || \'' . $strFunctionName . '\' || \'(\' || pg_get_function_identity_arguments( pp.oid ) || \');\' AS sql
			FROM
				pg_proc pp
			WHERE
				pp.proname = \'' . $strFunctionName . '\'
		';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchTableSizeStatsByTableIdAndDatabaseId( $intTableId, $intDatabaseId, $objDatabase, $intLogDatetimeLimit = 6 ) {

		$strSql = '	SELECT
						rank() OVER (partition by d.database_name order by dbo.name ASC, dos.database_transaction_id ASC),
						lag(size) OVER (ORDER BY substring ( d.database_name FROM \'[0-9]+\' )::integer ASC, dos.database_transaction_id ASC) as old_size,
						d.id as database_id,
						d.database_name,
						dos.database_transaction_id,
						dbo.name as table_name,
						dbo.id as table_id,
						dos.size as new_size,
						dos.count,
						dos.log_datetime
					FROM
						db_object_stats dos
					LEFT JOIN
						db_objects dbo ON dbo.id = dos.db_object_id
					LEFT JOIN
						databases d ON d.id = dbo.database_id
					WHERE
						dbo.id = ' . ( int ) $intTableId . '
						AND dbo.database_id = ' . ( int ) $intDatabaseId . '
						AND dbo.db_object_type_id = ' . ( int ) CDbObjectType::TABLE . '
					ORDER BY
						log_datetime ASC
					LIMIT
						' . ( int ) $intLogDatetimeLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTableSizeStatsByDatabaseIds( $arrintDatabases, $objDatabase, $intLogDatetimeLimit = 6 ) {

		if( true == valArr( $arrintDatabases ) ) {
			$arrintDatabases = implode( ',', $arrintDatabases );
		}

		$strSql	= '	SELECT
					  sub.*,
					  CASE
					    WHEN sub.rank != 1 AND sub.old_size::DECIMAL != 0 THEN round(((sub.new_size
					      ::DECIMAL - sub.old_size::DECIMAL) / sub.old_size::DECIMAL) * 100, 2)
					    ELSE NULL
					  END AS percent_change
					FROM
					  (
					    SELECT
					      rank() OVER(partition by d.database_name order by t.name ASC, dos.database_transaction_id ASC),
					      lag(size) OVER(PARTITION BY dos.db_object_id, t.database_id ORDER BY dos.db_object_id, t.database_id, dos.log_datetime ASC, dos.database_transaction_id ASC) as old_size,
					      d.id as database_id,
					      d.database_name,
					      dos.database_transaction_id,
					      t.name,
						  t.id as table_id,
					      dos.size as new_size,
						  dos.count,
					      dos.log_datetime
					    FROM
					      db_object_stats dos
					      LEFT JOIN db_objects t ON t.id = dos.db_object_id
					      LEFT JOIN databases d ON d.id = t.database_id
					    WHERE
					      t.db_object_type_id = ' . ( int ) CDbObjectType::TABLE . '
					      AND t.name NOT LIKE \'%types\'
					      AND t.database_id IN ( ' . $arrintDatabases . ' )
					      AND date( dos.log_datetime ) IN (
					                            SELECT
					                              DISTINCT ( date( log_datetime ) ) as log_datetime
					                            FROM
					                              db_object_stats
					                            ORDER BY
					                              log_datetime DESC
					                            LIMIT
					                              ' . ( int ) $intLogDatetimeLimit . '
					      					  )
					  ) as sub';

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchSearchedTables( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = '	SELECT
						DISTINCT t.table_id as table_id,
						t.name as table_name
					FROM
						db_object_stats dos
						JOIN db_objects t ON t.id = dos.db_object_id
						JOIN databases d ON d.id = t.database_id
					WHERE
						t.name ILIKE \'%' . implode( '%\' AND t.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						AND db_object_type_id = ' . CDbObjectType::TABLE . '
					GROUP BY
						t.id,
						t.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDbObjectStatRecentLogDate( $objDatabase ) {

		$strSql	= '	SELECT
						date( log_datetime ) as log_datetime
					FROM
						db_object_stats
					ORDER BY
						id DESC
					LIMIT 1';

		return self::fetchDbObjectStat( $strSql, $objDatabase );
	}

	public static function fetchDbObjectStatsByDbObjectIds( $arrintDbObjectIds, $objDatabase ) {

		if( true == empty( $arrintDbObjectIds ) ) return NULL;

		$strSql = 'SELECT 
						*
					FROM
						db_object_stats
					WHERE
						db_object_id IN ( ' . implode( ',', $arrintDbObjectIds ) . ' )';

		return self::fetchDbObjectStats( $strSql, $objDatabase );
	}

}
?>