<?php

class CSvnCommitLog extends CBaseSvnCommitLog {

	protected $m_intTaskId;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSvnRepositoryId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRevisionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCodeAuditorId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSvnUsername() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTaskIds() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCommitMessage() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCommitDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSvnCommitFilesCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Set Functions
     *
     */

    public function setTaskId( $intTaskId ) {
    	$this->m_intTaskId = $intTaskId;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );
    	if( true == isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
    }

    /**
     * get Functions
     *
     */

    public function getTaskId() {
    	 return $this->m_intTaskId;
    }

}
?>