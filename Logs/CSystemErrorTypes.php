<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSystemErrorTypes
 * Do not add any new functions to this class.
 */

class CSystemErrorTypes extends CBaseSystemErrorTypes {

	public static function fetchSystemErrorTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSystemErrorType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSystemErrorType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSystemErrorType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllSystemErrorTypes( $objDatabase ) {
		return self::fetchSystemErrorTypes( 'SELECT * FROM system_error_types WHERE is_published = 1', $objDatabase );
	}
}
?>