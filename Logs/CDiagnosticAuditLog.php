<?php

class CDiagnosticAuditLog extends CBaseDiagnosticAuditLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDiagnosticId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDiagnosticLogDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalErrorCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAuditLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>