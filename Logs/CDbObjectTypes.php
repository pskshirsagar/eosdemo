<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CDbObjectTypes
 * Do not add any new functions to this class.
 */

class CDbObjectTypes extends CBaseDbObjectTypes {

    public static function fetchDbObjectTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CDbObjectType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchDbObjectType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CDbObjectType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchAllDbObjectTypes( $objDatabase ) {
    	$strSql = 'SELECT * FROM db_object_types';
    	return self::fetchDbObjectTypes( $strSql, $objDatabase );
    }
}
?>