<?php

class CDiagnosticException extends CBaseDiagnosticException {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {

		$boolIsValid = true;

		if( true == empty( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Client Id', 'Client Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDiagnosticId() {
		$boolIsValid = true;

		if( true == empty( $this->getDiagnosticId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Diagnostic Id', 'Diagnostic Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valExceptionIds() {
		$boolIsValid = true;

		if( true == empty( $this->getExceptionIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Exception Id(s)', 'Exception Id(s) is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;

		if( true == empty( $this->getNotes() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Reason', 'Reason is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDiagnosticId();
				$boolIsValid &= $this->valExceptionIds();
				$boolIsValid &= $this->valNotes();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDiagnosticId();
				$boolIsValid &= $this->valExceptionIds();
				$boolIsValid &= $this->valNotes();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>