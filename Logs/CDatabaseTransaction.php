<?php

class CDatabaseTransaction extends CBaseDatabaseTransaction {

	public function valDatabaseId() {
		$boolIsValid = true;

		if( true == is_null( $this->getDatabaseId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'database_id', 'Database id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDatabaseTransactionTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getDatabaseTransactionTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'database_transaction_type_id', 'Database transaction type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valScriptId() {
		$boolIsValid = true;

		if( true == is_null( $this->getScriptId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'script_id', 'Script id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBeginDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->getBeginDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_datetime', 'Begin date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEndDatetime() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valFailedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valResolvedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDatabaseId();
				$boolIsValid &= $this->valDatabaseTransactionTypeId();
				$boolIsValid &= $this->valScriptId();
				$boolIsValid &= $this->valBeginDatetime();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function insertParentDatabaseTransaction( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'SELECT
						*
					FROM
						public.insert_parent_database_transaction(' .
							$this->sqlClusterId() . ', ' .
							$this->sqlDatabaseId() . ', ' .
							$this->sqlDatabaseTransactionTypeId() . ', ' .
							$this->sqlScriptId() . ', ' .
							$this->sqlHardwareId() . ', ' .
							$this->sqlDatabaseTransactionId() . ', ' .
							$this->sqlSummary() . ', ' .
							$this->sqlBeginDatetime() . ', ' .
							$this->sqlEndDatetime() . ', ' .
							$this->sqlDescription() . ', ' .
							$this->sqlFailedOn() . ', ' .
							$this->sqlResolvedOn() . ', ' .
							$this->sqlDismissedBy() . ', ' .
							$this->sqlDismissedOn() . ', ' .
							( int ) $intCurrentUserId . ', ' .
							$this->sqlUpdatedOn() . ', ' .
							$this->sqlCreatedOn() .
						' ) as id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			$arrmixData = fetchData( $strSql, $objDatabase );
			if( true == valArr( $arrmixData ) && true == isset( $arrmixData[0]['id'] ) ) {
				$this->setId( $arrmixData[0]['id'] );
				return true;
			} else {
				return false;
			}
		}
	}
}
?>