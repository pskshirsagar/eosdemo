<?php

class CDiagnosticType extends CBaseDiagnosticType {

	const ACCOUNTS_PAYABLE 		= 1;
	const RESIDENTS 			= 2;
	const PAYMENTS 				= 3;
	const LOGS 					= 4;
	const INSURANCE 			= 5;
	const SCREENING 			= 6;
	const CLIENT_ADMIN 			= 7;
	const UTILITIES 			= 8;
	const EMAIL 				= 9;
	const LEADS 				= 10;
	const RESIDENT_VERIFY 		= 11;
	const INSPECTIONS 		    = 12;
	const AFFORDABLE 			= 15;
	const COMPETITOR 			= 16;
	const HA 					= 17;

	protected $m_arrobjDiagnostics;

    public function addDiagnostic( $objDiagnostic ) {
    	$this->m_arrobjDiagnostics[$objDiagnostic->getId()] = $objDiagnostic;
    }

    public function getDiagnostics() {
    	return $this->m_arrobjDiagnostics;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsClientSpecific() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public static function getDiagnosticTypeNameById( $intDiagnosticTypeId ) {
    	$strDiagnosticTypeName = NULL;

    	switch( $intDiagnosticTypeId ) {
    		case self::ACCOUNTS_PAYABLE:
    			$strDiagnosticTypeName = 'Accounts Payable';
    			break;

    		case self::RESIDENTS:
    			$strDiagnosticTypeName = 'Residents';
    			break;

    		case self::PAYMENTS:
    			$strDiagnosticTypeName = 'Payments';
    			break;

    		case self::LOGS:
    			$strDiagnosticTypeName = 'Logs';
    			break;

    		case self::INSURANCE:
    			$strDiagnosticTypeName = 'Insurance';
    			break;

    		case self::SCREENING:
    			$strDiagnosticTypeName = 'Screening';
    			break;

    		case self::CLIENT_ADMIN:
    			$strDiagnosticTypeName = 'Client Admin';
    			break;

    		case self::UTILITIES:
    			$strDiagnosticTypeName = 'Utilities';
    			break;

    		case self::EMAIL:
    			$strDiagnosticTypeName = 'Email';
    			break;

    		case self::LEADS:
    			$strDiagnosticTypeName = 'Leads';
    			break;

    		case self::RESIDENT_VERIFY:
    			$strDiagnosticTypeName = 'Resident Verify';
    			break;

			case self::INSPECTIONS:
    			$strDiagnosticTypeName = 'Inspections';
    			break;

			case self::AFFORDABLE:
				$strDiagnosticTypeName = 'Affordable';
				break;

			case self::COMPETITOR:
			    $strDiagnosticTypeName = 'Competitor';
			    break;

		    case self::HA:
			    $strDiagnosticTypeName = 'HA';
			    break;

		    default:
    			$strDiagnosticTypeName = NULL;
    			break;
    	}

    	return $strDiagnosticTypeName;
    }

}
?>