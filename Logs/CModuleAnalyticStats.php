<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CModuleAnalyticStats
 * Do not add any new functions to this class.
 */

class CModuleAnalyticStats extends CBaseModuleAnalyticStats {

	public static function fetchLatestHitDate( $objDatabase ) {

		$strSql = 'SELECT module_hit_date FROM module_analytic_stats ORDER BY module_hit_date DESC LIMIT 1';

		return self::fetchColumn( $strSql, 'module_hit_date', $objDatabase );
	}

}
?>