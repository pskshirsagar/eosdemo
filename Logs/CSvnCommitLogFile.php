<?php

class CSvnCommitLogFile extends CBaseSvnCommitLogFile {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSvnCommitLogId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFilePath() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChangeDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>