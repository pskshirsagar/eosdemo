<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSvnCommitLogTasks
 * Do not add any new functions to this class.
 */

class CSvnCommitLogTasks extends CBaseSvnCommitLogTasks {

	public static function fetchSvnCommitLogTasksByLatestSvnCommitLogId( $intLatestSvnCommitLogId, $objDatabase ) {

		if( false == is_numeric( $intLatestSvnCommitLogId ) ) return;

		$strSql = 'SELECT
						*
					FROM
						svn_commit_log_tasks
					WHERE
						svn_commit_log_id > ' . ( int ) $intLatestSvnCommitLogId . '
					ORDER BY
						svn_commit_log_id';

		return parent::fetchSvnCommitLogTasks( $strSql, $objDatabase );
	}
}
?>