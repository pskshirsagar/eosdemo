<?php

class CDocumentLog extends CBaseDocumentLog {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAuditlogId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAuditTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAuditTypeName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAuditFilePath() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIpAddress() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valContentSha1() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMetaData() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSuccess() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>