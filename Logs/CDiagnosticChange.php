<?php

class CDiagnosticChange extends CBaseDiagnosticChange {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDiagnosticId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChangeDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOldViewSql() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNewViewSql() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOldRepairSql() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNewRepairSql() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOldDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNewDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>