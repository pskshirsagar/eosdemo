<?php

class CSystemErrorType extends CBaseSystemErrorType {

	const ERROR								= 1;
	const WARNING							= 2;
	const CATCHABLE_FATAL_ERROR				= 4096;

	public static $c_arrintAllErrorTypeIds = array( 1, 4, 16, 64, 256, 4096 );

}
?>