<?php

class CSystemErrorDetail extends CBaseSystemErrorDetail {

    public function __construct() {
        parent::__construct();

        $this->m_boolAllowDifferentialUpdate = true;

        return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSystemErrorId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsProductModuleId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsProductOptionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valClient() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBrowser() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valServer() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valModule() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAction() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBacktrace() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valErrorCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function determineAndSetPsProductId() {

    	if( false == is_null( $this->getPsProductId() ) ) return;

    	if( false !== \Psi\CStringService::singleton()->stripos( $this->getServer(), 'clientadmin.' . CONFIG_COMPANY_BASE_DOMAIN ) ) {
    		$this->setPsProductId( CPsProduct::CLIENT_ADMIN );
    	} elseif( false !== \Psi\CStringService::singleton()->stripos( $this->getServer(), 'sms.' . CONFIG_COMPANY_BASE_DOMAIN ) ) {
    		$this->setPsProductId( CPsProduct::MESSAGE_CENTER );
    	} elseif( false !== \Psi\CStringService::singleton()->stripos( $this->getServer(), 'residentsync.' . CONFIG_COMPANY_BASE_DOMAIN ) ) {
    		$this->setPsProductId( CPsProduct::API_SERVICES );
    	} elseif( false !== \Psi\CStringService::singleton()->stripos( $this->getServer(), 'prospectportal.com' ) ) {
    		$this->setPsProductId( CPsProduct::PROSPECT_PORTAL );
    	} elseif( false !== \Psi\CStringService::singleton()->stripos( $this->getServer(), 'residentportal.com' ) ) {
    		$this->setPsProductId( CPsProduct::RESIDENT_PORTAL );
    	} elseif( false !== \Psi\CStringService::singleton()->stripos( $this->getServer(), '.entrata.com' ) ) {
    		$this->setPsProductId( CPsProduct::ENTRATA );
    	} elseif( false !== \Psi\CStringService::singleton()->stripos( $this->getServer(), 'vacancy.com' ) ) {
    		$this->setPsProductId( CPsProduct::VACANCY );
    	} elseif( false !== \Psi\CStringService::singleton()->stripos( $this->getServer(), 'entrata.com' ) ) {
    		$this->setPsProductId( CPsProduct::ENTRATA_COM );
    	} elseif( false !== \Psi\CStringService::singleton()->stripos( $this->getServer(), 'residentinsure.com' ) ) {
    		$this->setPsProductId( CPsProduct::RESIDENT_INSURE );
    	} elseif( false !== \Psi\CStringService::singleton()->stripos( $this->getServer(), 'jobs.' . CONFIG_COMPANY_BASE_DOMAIN ) ) {
    		$this->setPsProductId( CPsProduct::JOBS );
    	} else {
    		$this->setPsProductId( CPsProduct::PROSPECT_PORTAL );
    	}

    }

}
?>