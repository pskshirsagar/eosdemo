<?php

use Psi\Eos\Admin\CPsProducts;

class CSystemError extends CBaseSystemError {

	protected $m_intUserId;
	protected $m_strEmployeeNameFirst;
	protected $m_strEmployeeNameLast;
	protected $m_strEmployeeEmailAddress;
	protected $m_strSystemErrorTypeName;

	protected $m_intPsProductId;
	protected $m_intPsProductModuleId;
	protected $m_intPsProductOptionId;
	protected $m_strClient;
	protected $m_strBrowser;
	protected $m_strServer;
	protected $m_strModule;
	protected $m_strAction;

	const ID_GENERIC_SYSTEM_ERROR = 1111;

	public function __construct() {
		parent::__construct();

		$this->m_boolAllowDifferentialUpdate = true;

		return;
	}

	/**
	 * Create Functions
	 */

	public function createTask( $objAdminDatabase, $objLogsDatabase, $objDeployDatabase = NULL ) {

		$objSystemErrorType		= CSystemErrorTypes::fetchSystemErrorTypeById( $this->getSystemErrorTypeId(), $objLogsDatabase );

		$objSystemErrorDetail	= CSystemErrorDetails::fetchLatestSystemErrorDetailBySystemErrorId( $this->getId(), $objLogsDatabase );

		if( false == valObj( $objSystemErrorDetail, 'CSystemErrorDetail' ) ) {
			return false;
		}

		$objTask = new CTask();

		$objTask->setDefaults();

		$arrstrFoldersFilesOwnerAssocition = CFoldersFilesOwnerAssociations::fetchFileOwnerDetailsByFilePath( $this->getFilePath(), $objLogsDatabase );

		if( true == valArr( $arrstrFoldersFilesOwnerAssocition ) ) {
			$intResponsibleQaEmployeeId = $arrstrFoldersFilesOwnerAssocition[0]['qa_employee_id'];

			// Set Responsible QA Employee, Department Id, SDM Employee Id
			if( true == isset( $intResponsibleQaEmployeeId ) && false == is_null( $intResponsibleQaEmployeeId ) ) {

				$arrmixEmployeeData = CEmployees::fetchAssociatedSystemErrorEmployeesById( $intResponsibleQaEmployeeId, $objAdminDatabase );

				if( true == ValArr( $arrmixEmployeeData ) ) {
					$objTask->setUserId( $arrmixEmployeeData[0]['user_id'] );
					$objTask->setDepartmentId( $arrmixEmployeeData[0]['department_id'] );
					$objTask->setProjectManagerId( $arrmixEmployeeData[0]['sdm_employee_id'] );
				}
			}
		}

		// Set Responsible QA Employee, Department Id, Ps Product Option Id , SDM Employee Id (if not got in folders files owner associations)
		if( true == valObj( $objSystemErrorDetail, 'CSystemErrorDetail' ) ) {

			if( false == is_null( $objSystemErrorDetail->getPsProductId() ) && false == is_null( $objSystemErrorDetail->getModule() ) ) {

				if( CPsProduct::JOBS == $objSystemErrorDetail->getPsProductId() && true == is_null( $objTask->getUserId() ) ) {

					// Initialize the deploy database.
					if( false == valObj( $objDeployDatabase, 'CDatabase' ) ) {
						$objDeployDatabase = CDatabases::createDeployDatabase( $boolRandomizeConnections = false );
					}

					// Check the script owner for that particular script, and get the user id of that and assign task to him.
					$objScript = CScripts::fetchScriptByName( $objSystemErrorDetail->getModule() . '.class.php', $objDeployDatabase );

					if( true == valObj( $objScript, 'CScript' ) ) {

						if( CUser::ID_ADMIN == $objScript->getCreatedBy() || CUser::ID_DAVID_BATEMAN == $objScript->getCreatedBy() ) {
							$objTask->setUserId( CUser::ID_SANJAY_SINGH );

						} else {
							$objTask->setUserId( $objScript->getCreatedBy() );
						}
					}

				}
			}
		}

		$objPsProduct = CPsProducts::createService()->fetchPsProductById( $objSystemErrorDetail->getPsProductId(), $objAdminDatabase );

		if( true == is_null( $objTask->getUserId() ) && false == is_null( $objSystemErrorDetail->getPsProductId() ) ) {

			if( true == valObj( $objPsProduct, 'CPsProduct' ) && false == is_null( $objPsProduct->getQaEmployeeId() ) ) {

				$objPsProductUser = CUsers::fetchUserByEmployeeId( $objPsProduct->getQaEmployeeId(), $objAdminDatabase );
				$intResponsibleQAEmployee = $objPsProductUser->getId();
				$objTask->setUserId( $intResponsibleQAEmployee );
			}
		}

		if( true == is_null( $objTask->getProjectManagerId() ) && true == valObj( $objPsProduct, 'CPsProduct' ) ) {
			if( CPsProduct::JOBS == $objSystemErrorDetail->getPsProductId() ) {
				if( true == valObj( $objScript, 'CScript' ) ) $objTeam = CTeams::fetchPrimaryTeamByUserId( $objScript->getCreatedBy(), $objAdminDatabase );
				if( true == valObj( $objTeam, 'CTeam' ) && true == valId( $objTeam->getSdmEmployeeId() ) ) {
					$objTask->setProjectManagerId( $objTeam->getSdmEmployeeId() );
				} else {
					$objTask->setProjectManagerId( CUser::ID_SANDEEP_GARUD );
				}
			} else {
				$objPsProductUser = CUsers::fetchUserByEmployeeId( $objPsProduct->getSdmEmployeeId(), $objAdminDatabase );
				if( true == valObj( $objPsProductUser, 'CUser' ) ) {
					$objTask->setProjectManagerId( $objPsProduct->getSdmEmployeeId() );
					$objTask->setProjectManagerName( $objPsProductUser->getNameFull() );
				}
			}
		}

		if( true == is_null( $objTask->getDepartmentId() ) ) {
			$objEmployee = CEmployees::fetchEmployeeByUserId( $objTask->getUserId(), $objAdminDatabase );

			if( true == valObj( $objEmployee, 'CEmployee' ) ) {
				$objTask->setDepartmentId( $objEmployee->getDepartmentId() );
			}
		}

		if( true == valObj( $objSystemErrorType, 'CSystemErrorType' ) && ( CSystemErrorType::ERROR == $objSystemErrorType->getId() || CSystemErrorType::CATCHABLE_FATAL_ERROR == $objSystemErrorType->getId() ) ) {
			// Set the task priority high.
			$objTask->setTaskPriorityId( CTaskPriority::URGENT );
		}
		$arrstrFilePath = explode( '/', $this->getFilePath() );
		$strTaskTitle = $objSystemErrorType->getName() . ' in ' . call_user_func_array( 'end',  array( &$arrstrFilePath ) );
		if( false == is_null( $objSystemErrorDetail->getModule() ) ) {
			$strTaskTitle .= ' for module=' . $objSystemErrorDetail->getModule();
		}

		$objTask->setTaskTypeId( CTaskType::BUG );
		( 0 != $objSystemErrorDetail->getPsProductId() ) ? $objTask->setPsProductId( $objSystemErrorDetail->getPsProductId() ) : $objTask->setPsProductId( NULL );
		( 0 != $objSystemErrorDetail->getPsProductOptionId() ) ? $objTask->setPsProductOptionId( $objSystemErrorDetail->getPsProductOptionId() ) : $objTask->setPsProductOptionId( NULL );
		$objTask->setClusterId( $this->getClusterId() );
		$objTask->setClusterName( CCluster::getClusterNameByClusterId( $this->getClusterId() ) );
		$objTask->setCid( NULL );
		$objTask->setDescription( nl2br( $objSystemErrorDetail->getBacktrace() ) );
		$objTask->setTitle( $strTaskTitle );
		$objTask->setUrl( $this->getFilePath() );
		$objTask->setTaskDatetime( $objSystemErrorDetail->getCreatedOn() );
		$objTask->setDueDate( date( 'm-d-Y H:i:s', strtotime( '+1 day' ) ) );
		$objTask->setReferenceNumber( $this->getId() );

		if( true == is_null( $objTask->getUserId() ) ) {

			$intDeveloperEmployeeId = $this->getEmployeeId();
			if( true == isset( $intDeveloperEmployeeId ) && false == is_null( $intDeveloperEmployeeId ) ) {
				$arrmixEmployeeData = CEmployees::fetchAssociatedSystemErrorEmployeesById( $intDeveloperEmployeeId, $objAdminDatabase );
				if( true == ValArr( $arrmixEmployeeData ) ) {
					$objTask->setUserId( $arrmixEmployeeData[0]['user_id'] );
				}
			}
		}

		return $objTask;
	}

	public function setPsProductId( $intPsProductId ) {
		$this->m_intPsProductId = CStrings::strToIntDef( $intPsProductId, NULL, false );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->m_intPsProductOptionId = CStrings::strToIntDef( $intPsProductOptionId, NULL, false );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setClient( $strClient ) {
		$this->m_strClient = CStrings::strTrimDef( $strClient, -1, NULL, true );
	}

	public function getClient() {
		return $this->m_strClient;
	}

	public function sqlClient() {
		return ( true == isset( $this->m_strClient ) ) ? '\'' . addslashes( $this->m_strClient ) . '\'' : 'NULL';
	}

	public function setBrowser( $strBrowser ) {
		$this->m_strBrowser = CStrings::strTrimDef( $strBrowser, 240, NULL, true );
	}

	public function getBrowser() {
		return $this->m_strBrowser;
	}

	public function sqlBrowser() {
		return ( true == isset( $this->m_strBrowser ) ) ? '\'' . addslashes( $this->m_strBrowser ) . '\'' : 'NULL';
	}

	public function setServer( $strServer ) {
		$this->m_strServer = CStrings::strTrimDef( $strServer, -1, NULL, true );
	}

	public function getServer() {
		return $this->m_strServer;
	}

	public function sqlServer() {
		return ( true == isset( $this->m_strServer ) ) ? '\'' . addslashes( $this->m_strServer ) . '\'' : 'NULL';
	}

	public function setModule( $strModule ) {
		$this->m_strModule = CStrings::strTrimDef( $strModule, -1, NULL, true );
	}

	public function getModule() {
		return $this->m_strModule;
	}

	public function sqlModule() {
		return ( true == isset( $this->m_strModule ) ) ? '\'' . addslashes( $this->m_strModule ) . '\'' : 'NULL';
	}

	public function setAction( $strAction ) {
		$this->m_strAction = CStrings::strTrimDef( $strAction, -1, NULL, true );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function sqlAction() {
		return ( true == isset( $this->m_strAction ) ) ? '\'' . addslashes( $this->m_strAction ) . '\'' : 'NULL';
	}

	/**
	 * Set Functions
	 */

 	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
 		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['employee_id'] ) ) $this->setEmployeeId( $arrmixValues['employee_id'] );
		if( true == isset( $arrmixValues['employee_name_first'] ) ) $this->setEmployeeNameFirst( $arrmixValues['employee_name_first'] );
		if( true == isset( $arrmixValues['employee_name_last'] ) ) $this->setEmployeeNameLast( $arrmixValues['employee_name_last'] );
		if( true == isset( $arrmixValues['system_error_type_name'] ) ) $this->setSystemErrorTypeName( $arrmixValues['system_error_type_name'] );
		if( true == isset( $arrmixValues['ps_product_id'] ) ) $this->setPsProductId( $arrmixValues['ps_product_id'] );
		if( true == isset( $arrmixValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrmixValues['ps_product_option_id'] );
		if( true == isset( $arrmixValues['client'] ) ) $this->setClient( $arrmixValues['client'] );
		if( true == isset( $arrmixValues['browser'] ) ) $this->setBrowser( $arrmixValues['browser'] );
		if( true == isset( $arrmixValues['module'] ) ) $this->setModule( $arrmixValues['module'] );
		if( true == isset( $arrmixValues['action'] ) ) $this->setAction( $arrmixValues['action'] );

		return;
	}

	/**
	 * Get Functions
	 */

	public function getEmployeeNameFirst() {
		return $this->m_strEmployeeNameFirst;
	}

	public function getEmployeeNameLast() {
		return $this->m_strEmployeeNameLast;
	}

	public function getEmployeeEmailAddress() {
		return $this->m_strEmployeeEmailAddress;
	}

	public function getSystemErrorTypeName() {
		return $this->m_strSystemErrorTypeName;
	}

	/**
	 * Set Functions
	 */

	public function setEmployeeNameFirst( $strNameFirst ) {
		$this->m_strEmployeeNameFirst = $strNameFirst;
	}

	public function setEmployeeNameLast( $strNameLast ) {
		$this->m_strEmployeeNameLast = $strNameLast;
	}

	public function setEmployeeEmailAddress( $strEmployeeEmailAddress ) {
		$this->m_strEmployeeEmailAddress = $strEmployeeEmailAddress;
	}

	public function setSystemErrorTypeName( $strSystemErrorTypeName ) {
		$this->m_strSystemErrorTypeName = $strSystemErrorTypeName;
	}

	/**
	 * Validation Functions
	 */

	public function valSystemErrorTypeId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valFilePath() {
		$boolIsValid = true;

		if( true == is_null( $this->getFilePath() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'filepath', 'Filepath is required' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objLogsDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valConflictingError( $objLogsDatabase );
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valFilePath();
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * validate functions
	 */

	public function valConflictingError( $objLogsDatabase ) {

		$boolIsValid			= true;
		$boolIsCheckDescription = true;

		if( true == valObj( $objLogsDatabase, 'CDatabase' ) ) {
			$objConflictingSystemError	= CSystemErrors::fetchConflictingSystemError( $this, $boolIsCheckDescription, $objLogsDatabase );

			if( true == valObj( $objConflictingSystemError, 'CSystemError' ) ) {
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	/**
	 * Insert & Update & Delete
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( 0 == $this->getSystemErrorTypeId() ) {
			$this->setSystemErrorTypeId( CSystemErrorType::WARNING );
		}

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( 0 == $this->getSystemErrorTypeId() ) {
			$this->setSystemErrorTypeId( CSystemErrorType::WARNING );
		}

		$this->setDescription( preg_replace( '/\\0/', '', $this->getDescription() ) );
		$this->setBacktrace( preg_replace( '/\\0/', '', $this->getBacktrace() ) );

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false );
	}

	public function determineAndSetData( $objSystemErrorDetail, $objAdminDatabase = NULL ) {

		if( true == is_null( $objSystemErrorDetail->getPsProductId() ) ) {
			$objSystemErrorDetail->determineAndSetPsProductId();
		}

		if( true == is_null( $objSystemErrorDetail->getModule() ) && true == is_null( $this->getEmployeeId() ) ) {
			$this->determineAndSetUserIdByPsProductId( $objSystemErrorDetail );

			// This will hit the admin DB, do we really require this or will add constant for employees also if require will changes the dev_user_id to dev_emp_id and same for Qa in ps_product_modules table
			if( false == is_null( $this->m_intUserId ) && true == is_numeric( $this->m_intUserId ) ) {
				$this->determineAndSetEmployeeIdAndEmployeeAddressByUserId( $this->m_intUserId, $objAdminDatabase );
			}
		}

		if( true == is_null( $this->getEmployeeId() ) ) {
			$this->setEmployeeId( CEmployee::ID_SANDEEP_GARUD );
		}

	}

	public function determineAndSetUserIdByPsProductId( $objSystemErrorDetail ) {

		if( true == is_null( $objSystemErrorDetail->getPsProductId() ) ) {
			$objSystemErrorDetail->determineAndSetPsProductId();
		}

		switch( $objSystemErrorDetail->getPsProductId() ) {

			case CPsProduct::ENTRATA:
				$this->m_intUserId = CUser::ID_PUNEET_KATHURIA;
				break;

			case CPsProduct::PROSPECT_PORTAL:
				$this->m_intUserId = CUser::ID_YUSUF_POONAWALA;
				break;

			case CPsProduct::RESIDENT_PORTAL:
				$this->m_intUserId = CUser::ID_ROHITSING_PARDESHI;
				break;

			case CPsProduct::RESIDENT_PAY:
				$this->m_intUserId = CUser::ID_ROHITSING_PARDESHI;
				break;

			case CPsProduct::CLIENT_ADMIN:
				$this->m_intUserId = CUser::ID_LOKESH_AGRAWAL;
				break;

			case CPsProduct::LOBBY_DISPLAY:
				$this->m_intUserId = CUser::ID_ROHITSING_PARDESHI;
				break;

			case CPsProduct::API_SERVICES:
				$this->m_intUserId = CUser::ID_SANDEEP_CHAVAN;
				break;

			case CPsProduct::ENTRATA_COM:
				$this->m_intUserId = CUser::ID_TAPAN_BHATT;
				break;

			case CPsProduct::GMAIL_EMAIL:
				$this->m_intUserId = CUser::ID_SANDEEP_GARUD;
				break;

			case CPsProduct::VACANCY:
				$this->m_intUserId = CUser::ID_AMITESHWAR_PRASAD;
				break;

			case CPsProduct::RESIDENT_PAY_DESKTOP:
				$this->m_intUserId = CUser::ID_ROHITSING_PARDESHI;
				break;

			case CPsProduct::PMS_INTEGRATION:
				$this->m_intUserId = CUser::ID_SANDEEP_CHAVAN;
				break;

			case CPsProduct::ILS_PORTAL:
				$this->m_intUserId = CUser::ID_RAHUL_WAGHMARE;
				break;

			case CPsProduct::GUEST_CARD_PARSING:
				$this->m_intUserId = CUser::ID_RAHUL_WAGHMARE;
				break;

			case CPsProduct::BLOG:
				$this->m_intUserId = CUser::ID_AMITESHWAR_PRASAD;
				break;

			case CPsProduct::SEO_SERVICES:
				$this->m_intUserId = CUser::ID_YUSUF_POONAWALA;
				break;

			case CPsProduct::LEASE_EXECUTION:
				$this->m_intUserId = CUser::ID_KEDAR_LANGADE;
				break;

			case CPsProduct::MESSAGE_CENTER:
				$this->m_intUserId = CUser::ID_SACHINKUMAR_DURGE;
				break;

			case CPsProduct::CRAIGSLIST_POSTING:
				$this->m_intUserId = CUser::ID_RAHUL_WAGHMARE;
				break;

			case CPsProduct::SITE_TABLET:
				$this->m_intUserId = CUser::ID_MAHEK_JOSHIPURA;
				break;

			case CPsProduct::CALL_TRACKER:
				$this->m_intUserId = CUser::ID_SACHINKUMAR_DURGE;
				break;

			case CPsProduct::FACEBOOK_INTEGRATION:
				$this->m_intUserId = CUser::ID_RAHUL_WAGHMARE;
				break;

			case CPsProduct::RESIDENT_UTILITY:
				$this->m_intUserId = CUser::ID_VARUN_KAMBLE;
				break;

			case CPsProduct::RESIDENT_INSURE:
				$this->m_intUserId = CUser::ID_AMOL_DESHPANDE;
				break;

			case CPsProduct::CUSTOM_DEVELOPMENT:
				$this->m_intUserId = CUser::ID_YUSUF_POONAWALA;
				break;

			case CPsProduct::PRICING:
				$this->m_intUserId = CUser::ID_SANDEEP_GARUD;
				break;

			case CPsProduct::LEAD_MANAGEMENT:
				$this->m_intUserId = CUser::ID_KEDAR_LANGADE;
				break;

			case CPsProduct::PRINT_BROCHURE:
				$this->m_intUserId = CUser::ID_YUSUF_POONAWALA;
				break;

			case CPsProduct::LEASING_CENTER:
				$this->m_intUserId = CUser::ID_SACHINKUMAR_DURGE;
				break;

			case CPsProduct::PARCEL_ALERT:
				$this->m_intUserId = CUser::ID_ROHITSING_PARDESHI;
				break;

			case CPsProduct::CALL_ANALYSIS:
				$this->m_intUserId = CUser::ID_SACHINKUMAR_DURGE;
				break;

			default:
				// default case
				break;
		}
	}

	public function determineAndSetEmployeeIdAndEmployeeAddressByUserId( $intUserId, $objAdminDatabase ) {

		$objUser = CUsers::fetchUserInformationById( $intUserId, $objAdminDatabase );

		if( true == valObj( $objUser, 'CUser' ) ) {
			$this->setEmployeeId( $objUser->getEmployeeId() );
			$this->setEmployeeEmailAddress( $objUser->getEmailAddress() );
		}
	}

	public function determineAndSetFileAssociations( $objSystemErrorDetail, $strMessageFilePath, $objLogsDatabase ) {

		$strFilePath = $this->getFilePath();

		if( false !== \Psi\CStringService::singleton()->strpos( $strFilePath, '.tpl' ) ) {
			$intCharactersPosition = 6;

			$strFilePath	= str_replace( PATH_VHOSTS . 'Interfaces/', '', $strFilePath );
			$strTrimmedPath = str_replace( '.php', '', $strFilePath );
			$strTplFileName = \Psi\CStringService::singleton()->substr( $strTrimmedPath, \Psi\CStringService::singleton()->strpos( $strTrimmedPath, '.file.' ) + $intCharactersPosition );

			$arrmixFolderFileOwnerAssociations = CFoldersFilesOwnerAssociations::fetchFileOwnerDetailsByFilePath( $strTplFileName, $objLogsDatabase );
		} else {
			$arrmixFolderFileOwnerAssociations = CFoldersFilesOwnerAssociations::fetchFileOwnerDetailsByFilePath( $strFilePath, $objLogsDatabase );
		}

		if( false == valArr( $arrmixFolderFileOwnerAssociations ) ) {
			return;
		}

		if( true == valArr( $arrmixFolderFileOwnerAssociations, 1, true ) ) {

			if( false !== \Psi\CStringService::singleton()->strpos( $strFilePath, '.tpl' ) ) {
				$this->setFilePath( PATH_VHOSTS . $arrmixFolderFileOwnerAssociations[0]['file_path'] );
			}

			if( CPsProduct::JOBS != $objSystemErrorDetail->getPsProductId() && false == is_null( $arrmixFolderFileOwnerAssociations[0]['ps_product_id'] ) && $objSystemErrorDetail->getPsProductId() != $arrmixFolderFileOwnerAssociations[0]['ps_product_id'] ) {
				$objSystemErrorDetail->setPsProductId( $arrmixFolderFileOwnerAssociations[0]['ps_product_id'] );
			}

			if( CPsProduct::JOBS != $objSystemErrorDetail->getPsProductId() && false == is_null( $arrmixFolderFileOwnerAssociations[0]['ps_product_option_id'] ) ) {
				$objSystemErrorDetail->setPsProductOptionId( $arrmixFolderFileOwnerAssociations[0]['ps_product_option_id'] );
			}

			if( true == is_null( $this->getEmployeeId() ) ) {
				$this->setEmployeeId( $arrmixFolderFileOwnerAssociations[0]['dev_employee_id'] );
			}
		} elseif( false !== \Psi\CStringService::singleton()->strpos( $strFilePath, '.tpl' ) ) {

			// For TPL file, if found multiple records for same file name in folders_files_owner_associations table.
			if( true == file_exists( PATH_COMPILED_INTERFACES . $strFilePath ) ) {
				$objFilePointer = CFileIo::fileOpen( PATH_COMPILED_INTERFACES . $strFilePath, 'r' );

				if( true == $objFilePointer ) {
					while( false !== ( $strLine = \Psi\CStringService::singleton()->htmlentities( fgets( $objFilePointer ) ) ) ) {
						$strLine = html_entity_decode( $strLine );

						if( false !== \Psi\CStringService::singleton()->strpos( $strLine, '*/ ?>' ) ) {
							$intPosition		= \Psi\CStringService::singleton()->strpos( $strLine, '"' );
							$strActualFilePath	= \Psi\CStringService::singleton()->substr( $strLine, $intPosition + 1, \Psi\CStringService::singleton()->strrpos( $strLine, '"' ) - $intPosition - 1 );
							$strTplFilePath		= str_replace( PATH_VHOSTS, '', $strActualFilePath );

							$this->setFilePath( $strActualFilePath );

							$arrmixFolderFileOwnerAssociations = rekeyArray( 'file_path', $arrmixFolderFileOwnerAssociations );
							if( true == array_key_exists( $strTplFilePath, $arrmixFolderFileOwnerAssociations ) ) {
								if( false == is_null( $arrmixFolderFileOwnerAssociations[$strTplFilePath]['ps_product_id'] ) && $objSystemErrorDetail->getPsProductId() != $arrmixFolderFileOwnerAssociations[$strTplFilePath]['ps_product_id'] ) {
									$objSystemErrorDetail->setPsProductId( $arrmixFolderFileOwnerAssociations[$strTplFilePath]['ps_product_id'] );
								}

								if( false == is_null( $arrmixFolderFileOwnerAssociations[$strTplFilePath]['ps_product_option_id'] ) ) {
									$objSystemErrorDetail->setPsProductOptionId( $arrmixFolderFileOwnerAssociations[$strTplFilePath]['ps_product_option_id'] );
								}

								if( true == is_null( $this->getEmployeeId() ) ) {
									$this->setEmployeeId( $arrmixFolderFileOwnerAssociations[$strTplFilePath]['dev_employee_id'] );
								}
							}

							break;
						}
					}
				}
			}
		} else {
			$strPhpFilePath = str_replace( PATH_VHOSTS, '', $strMessageFilePath );

			$arrmixFolderFileOwnerAssociations = rekeyArray( 'file_path', $arrmixFolderFileOwnerAssociations );
			if( true == array_key_exists( $strPhpFilePath, $arrmixFolderFileOwnerAssociations ) ) {
				if( CPsProduct::JOBS != $objSystemErrorDetail->getPsProductId() && false == is_null( $arrmixFolderFileOwnerAssociations[$strPhpFilePath]['ps_product_id'] ) && $objSystemErrorDetail->getPsProductId() != $arrmixFolderFileOwnerAssociations[$strPhpFilePath]['ps_product_id'] ) {
					$objSystemErrorDetail->setPsProductId( $arrmixFolderFileOwnerAssociations[$strPhpFilePath]['ps_product_id'] );
				}

				if( CPsProduct::JOBS != $objSystemErrorDetail->getPsProductId() && false == is_null( $arrmixFolderFileOwnerAssociations[$strPhpFilePath]['ps_product_option_id'] ) ) {
					$objSystemErrorDetail->setPsProductOptionId( $arrmixFolderFileOwnerAssociations[$strPhpFilePath]['ps_product_option_id'] );
				}

				if( true == is_null( $this->getEmployeeId() ) ) {
					$this->setEmployeeId( $arrmixFolderFileOwnerAssociations[$strPhpFilePath]['dev_employee_id'] );
				}
			}
		}

		return;
	}

}
?>