<?php

class CHelpResourceView extends CBaseHelpResourceView {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpResourceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViewByCompanyUserName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserRole() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDownloaded() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>