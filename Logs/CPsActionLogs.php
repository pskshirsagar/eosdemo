<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CPsActionLogs
 * Do not add any new functions to this class.
 */

class CPsActionLogs extends CBasePsActionLogs {

	public static function fetchPsActionLogs( $strSql, $objDatabase ) {
		$arrobjPsActionLogs = NULL;
		$objPsActionLog = NULL;

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$arrobjPsActionLogs = array();

			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();

				$objPsActionLog = new CPsActionLog();
				$objPsActionLog->setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );
				$objPsActionLog->setSerializedOriginalValues( serialize( $arrmixValues ) );

				if( false == is_null( $objPsActionLog->getCid() ) ) {
					$arrobjPsActionLogs[$objPsActionLog->getCid()] = $objPsActionLog;
				} else {
					$arrobjPsActionLogs[] = $objPsActionLog;
				}

				$objDataset->next();
			}
		}

		$objDataset->cleanup();

		return $arrobjPsActionLogs;
	}

	public static function fetchPsActionLog( $strSql, $objDatabase ) {
		$objPsActionLog = NULL;

		$arrobjPsActionLogs = self::fetchPsActionLogs( $strSql, $objDatabase );
		if( true == valArr( $arrobjPsActionLogs ) ) {
			if( 1 < \Psi\Libraries\UtilFunctions\count( $arrobjPsActionLogs ) ) {
				trigger_error( 'Expecting a single record when multiple records returned. Sql = ' . $strSql, E_USER_WARNING );
				return NULL;
			}

			$objPsActionLog = array_shift( $arrobjPsActionLogs );
		}

		return $objPsActionLog;
	}

}
?>