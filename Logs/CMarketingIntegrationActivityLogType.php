<?php

class CMarketingIntegrationActivityLogType extends CBaseMarketingIntegrationActivityLogType {

	const SUBSCRIPTION_REQUEST                 = 1;
	const CANCELLED_SUBSCRIPTION               = 2;
	const FEED_TYPE_CHANGE_REQUEST             = 3;
	const PROPERTY_EXCLUDED                    = 4;
	const SUBSCRIPTION_REQUEST_CANCELLED       = 5;
	const OCCUPANCY_TYPE_CHANGE                = 6;
	const FEED_TYPE_CHANGE_DENIED              = 7;
	const FEED_TYPE_CHANGE_APPROVED            = 8;
	const FEED_TYPE_CHANGE_REQUEST_FAILED      = 9;
	const SUBSCRIPTION_APPROVED                = 10;
	const SUBSCRIPTION_DENIED                  = 11;
	const SUBSCRIPTION_REQUEST_FAILED          = 12;
	const RETRY_SUBSCRIPTION_REQUEST           = 13;
	const DUPLICATE_SUPPRESSED                 = 14;
	const DUPLICATE_UNAVAILABLE                = 15;
	const DUPLICATE_INSERTED                   = 16;
	const DUPLICATE_DECLINED                   = 17;
	const DUPLICATE_SUPPRESSION_REQUESTED      = 18;
	const CHANGE_PUBLISHER_SUBSCRIPTION_STATUS = 19;
	const UNEXPECTED_ACTIVITY_OCCURED          = 20;

	public static $c_arrintActivityLogTypeIdsForSubscriptionLog = [
		self::SUBSCRIPTION_REQUEST,
		self::CANCELLED_SUBSCRIPTION,
		self::RETRY_SUBSCRIPTION_REQUEST
	];

	public static $c_arrintActivityLogTypesForIlsSubscriptionLog = [
		self::SUBSCRIPTION_REQUEST,
		self::CANCELLED_SUBSCRIPTION,
		self::FEED_TYPE_CHANGE_REQUEST,
		self::PROPERTY_EXCLUDED,
		self::SUBSCRIPTION_REQUEST_CANCELLED,
		self::FEED_TYPE_CHANGE_DENIED,
		self::FEED_TYPE_CHANGE_APPROVED,
		self::FEED_TYPE_CHANGE_REQUEST_FAILED,
		self::SUBSCRIPTION_APPROVED,
		self::SUBSCRIPTION_DENIED,
		self::SUBSCRIPTION_REQUEST_FAILED,
		self::OCCUPANCY_TYPE_CHANGE
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function loadMarketingIntegrationActivityLogTypeIdByName( $strActivityLogType ) : int {
		return ( true == defined( "self::" . strtoupper( $strActivityLogType ) ) ) ? constant( "self::" . strtoupper( $strActivityLogType ) ) : self::UNEXPECTED_ACTIVITY_OCCURED;
	}

}
?>