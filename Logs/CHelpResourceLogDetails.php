<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CHelpResourceLogDetails
 * Do not add any new functions to this class.
 */

class CHelpResourceLogDetails extends CBaseHelpResourceLogDetails {

	public static function fetchHelpResourceLogDetailByHelpResourceIdByCid( $intHelpResourceId, $intCid, $objDatabase ) {

		if( false == valId( $intHelpResourceId ) ) {
			return false;
		}

		$strSql = 'SELECT
						*
					FROM
						help_resource_log_details hrld
					WHERE
						hrld.cid = ' . ( int ) $intCid . '
						AND  hrld.help_resource_id = ' . ( int ) $intHelpResourceId;

		return parent::fetchHelpResourceLogDetail( $strSql, $objDatabase );

	}

	public static function fetchHelpResourceLogDetailsByFilter( $arrmixLmsReportFilter, $boolDownloadReport, $strOrderBy, $strOrderByField, $objDatabase ) {
		$strWhereCondition	= 'WHERE c.company_status_type_id IN (' . CCompanyStatusType::CLIENT . ')';
		$strOrderClause		= '';
		$intOffset			= ( 0 < $arrmixLmsReportFilter['page_no'] ) ? $arrmixLmsReportFilter['page_size'] * ( $arrmixLmsReportFilter['page_no'] - 1 ) : 0;
		$intLimit			= $arrmixLmsReportFilter['page_size'];
		if( true == valStr( $strOrderBy ) && true == valStr( $strOrderByField ) ) {
			$strOrderClause	= ' ORDER BY  ' . $strOrderByField . ' ' . $strOrderBy;
		}

		if( true == valArr( $arrmixLmsReportFilter['help_resource_ids'] ) ) {
			$strWhereCondition .= ' AND hrld.help_resource_id IN (' . implode( ',', $arrmixLmsReportFilter['help_resource_ids'] ) . ')';
		}
		if( true == valArr( $arrmixLmsReportFilter['clients'] ) ) {
			$strWhereCondition .= 'AND hrld.cid IN (' . implode( ',', $arrmixLmsReportFilter['clients'] ) . ')';
		}

		$strSql = 'SELECT 
						course_details.*,
						(course_details.attempted_count - course_details.completed_count ) AS failed_count
					FROM 
						(
							SELECT
								hrld.help_resource_id,
								SUM ( CASE WHEN hrld.course_attempted_count IS NOT NULL THEN hrld.course_attempted_count ELSE 0 END ) AS attempted_count,
								SUM ( CASE WHEN hrld.course_completed_count IS NOT NULL THEN hrld.course_completed_count ELSE 0 END ) AS completed_count
							FROM
								help_resource_log_details hrld
								LEFT JOIN clients c ON c.id = hrld.cid
							' . $strWhereCondition . '
							GROUP BY hrld.help_resource_id 
						)course_details
					' . $strOrderClause;

		if( false == $boolDownloadReport && false == is_null( $intLimit ) ) {
			$strSql .= ' OFFSET	' . ( int ) $intOffset . ' LIMIT	' . ( int ) $intLimit;
		}

		$arrintCourseViewsStatistics['data'] = fetchData( $strSql, $objDatabase );

		$strCountSql = 'SELECT 
						count( course_details.* )
					FROM 
						(
							SELECT
								hrld.help_resource_id
							FROM
								help_resource_log_details hrld
								LEFT JOIN clients c ON c.id = hrld.cid
							' . $strWhereCondition . '  
							GROUP BY hrld.help_resource_id 
						)course_details';

		$arrintCourseViewsStatistics['count'] = self::fetchColumn( $strCountSql, 'count', $objDatabase );
		return $arrintCourseViewsStatistics;
	}

}
?>
