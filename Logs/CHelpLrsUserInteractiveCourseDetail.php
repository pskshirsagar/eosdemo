<?php

class CHelpLrsUserInteractiveCourseDetail extends CBaseHelpLrsUserInteractiveCourseDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpResourceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserAssessmentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCourseProgress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCourseAssignedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCourseAttemptCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCourseScore() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUsername() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCourseDueDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCourseAssignedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAttemptOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHighestScoreOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInitialPassedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>