<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CSessionLogSettings
 * Do not add any new functions to this class.
 */

class CSessionLogSettings extends CBaseSessionLogSettings {

	public static function fetchSessionLogSettingByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchObject( sprintf( 'SELECT * FROM session_log_settings sls WHERE sls.ps_product_id = %d', $intPsProductId ), 'CSessionLogSetting', $objDatabase );
	}

}
?>