<?php

class CDatabaseTransactionType extends CBaseDatabaseTransactionType {

	const CONFIG_SCHEDULED_SCRIPT 		= 1;
	const CONFIG_SQL_SCRIPT 			= 2;
	const CONFIG_COMPANY_MIGRATION 		= 3;
	const CONFIG_COMPANY_DELETION 		= 4;
	const CONFIG_MANUAL 				= 5;

}
?>