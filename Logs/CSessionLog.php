<?php

class CSessionLog extends CBaseSessionLog {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valClientName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerNameFull() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valWebsiteName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSessionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valServerIp() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRequesterIp() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valModule() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAction() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRequestUrl() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRequestDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'INSERT INTO
						public.session_logs
					VALUES ( ' .
						' nextval(\'session_logs_id_seq\'), ' .
						$this->sqlClientName() . ', ' .
						$this->sqlPropertyName() . ', ' .
						$this->sqlCustomerNameFull() . ', ' .
						$this->sqlWebsiteName() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlSessionId() . ', ' .
						$this->sqlServerIp() . ', ' .
						$this->sqlRequesterIp() . ', ' .
						$this->sqlModule() . ', ' .
						$this->sqlAction() . ', ' .
						$this->sqlRequestUrl() . ', ' .
						$this->sqlRequestDatetime() . ' ) ;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}
}
?>