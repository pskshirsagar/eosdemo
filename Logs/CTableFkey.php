<?php

class CTableFkey extends CBaseTableFkey {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDatabaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTableName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFieldName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFkeyTableName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFkeyFieldName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFkeyName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNotes() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBadDataCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valHasFkey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDismissedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDismissedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>