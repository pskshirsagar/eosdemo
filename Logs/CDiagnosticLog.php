<?php

class CDiagnosticLog extends CBaseDiagnosticLog {

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	$this->m_arrmixCustomVariables['diagnostic_name'] 			= ( isset( $arrmixValues['diagnostic_name'] ) ) ? trim( $arrmixValues['diagnostic_name'] ) : '';
    	$this->m_arrmixCustomVariables['is_error'] 					= ( isset( $arrmixValues['is_error'] ) ) ? trim( $arrmixValues['is_error'] ) : '';

    	return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDiagnosticId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valStartDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEndDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valItemCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMessage() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOutput() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAutoRepairSucceeded() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function delete( $arrintDiagnosticIds, $objDatabase, $boolReturnSqlOnly = false ) {

    	if( false == valArr( $arrintDiagnosticIds ) ) return false;

    	return fetchData( 'DELETE FROM diagnostic_logs WHERE diagnostic_id IN ( ' . implode( ',', $arrintDiagnosticIds ) . ' )', $objDatabase );
    }
}
?>