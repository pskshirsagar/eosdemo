<?php

class CDbObjectStat extends CBaseDbObjectStat {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDbObjectTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDatabaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDatabaseTransactionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTableId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSize() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLogDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>