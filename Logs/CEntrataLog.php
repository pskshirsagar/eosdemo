<?php

class CEntrataLog extends CBaseEntrataLog {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valModuleName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valActionName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valHits() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLogDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFilterKey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>