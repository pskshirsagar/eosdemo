<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CModuleAccessLogs
 * Do not add any new functions to this class.
 */

class CModuleAccessLogs extends CBaseModuleAccessLogs {

    public static function fetchDuplicateModuleAccessLogRecords( $intPsProductId, $strModule, $strAction, $strDate, $objDatabase ) {

    	$strSql	= 'SELECT
						*
					FROM
					    module_access_logs mal
					WHERE
					    mal.ps_product_id = ' . ( int ) $intPsProductId . '
					    AND mal.module LIKE \'' . $strModule . '\'
					    AND mal.action LIKE \'' . $strAction . '\'
					    AND date_part ( \'year\', mal.day ) = ' . date( 'Y', strtotime( $strDate ) ) . '
					    AND date_part ( \'month\', mal.day ) = ' . date( 'm', strtotime( $strDate ) ) . '
					    AND date_part ( \'day\', mal.day ) = ' . date( 'd', strtotime( $strDate ) ) . '
					LIMIT 1;';

    	return fetchData( $strSql, $objDatabase );
    }

    public static function fetchSimpleModuleAccessLogs( $strOrderBy = NULL, $strSortDirection = NULL, $intPageSize = NULL, $intOffset = NULL, $intNumberOfDay, $objModuleLogFilter = NULL, $objDatabase ) {

    	$strWhere = ' WHERE mal.hit_count >= 1 AND day IS NOT NULL';

    	if( false == is_null( $objModuleLogFilter ) ) {

    		if( false == is_null( $objModuleLogFilter->getProductIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objModuleLogFilter->getProductIds() ) ) {
    			$strWhere .= ' AND mal.ps_product_id IN ( ' . implode( ', ', $objModuleLogFilter->getProductIds() ) . ' )';
    		}

    		$strModule = $objModuleLogFilter->getModule();

    		if( false == is_null( $strModule ) && false == empty( $strModule ) ) {
    			$strWhere .= ' AND mal.module ILIKE \'%' . $strModule . '%\'';
    		}

    		$strAction = $objModuleLogFilter->getAction();

    		if( false == empty( $strAction ) ) {
    			$strWhere .= ' AND mal.action ILIKE \'%' . $strAction . '%\'';
    		}

			$strFromDate	= $objModuleLogFilter->getFromDate();
    		$strToDate		= $objModuleLogFilter->getToDate();

    		if( false == is_null( $strFromDate ) && false == empty( $strFromDate ) ) {
    			$strWhere .= ' AND mal.day >= \'' . $strFromDate . '\'';
    		}

    		if( false == is_null( $strToDate ) && false == empty( $strToDate ) ) {
    			$strWhere .= ' AND mal.day <= \'' . $strToDate . '\'';
    		}

    		if( true == is_null( $strFromDate ) && true == is_null( $strToDate ) && false == is_null( $intNumberOfDay ) ) {
    			$strWhere .= ' AND mal.day >= ( NOW() - INTERVAL \'' . $intNumberOfDay . ' days\' ) ';
    		}
    	}

    	$strSql	= 'SELECT
						mal.*
					FROM
					    module_access_logs mal
    				' . $strWhere . '
					ORDER BY ' . $strOrderBy . ' ' . $strSortDirection . '
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

    	return fetchData( $strSql, $objDatabase );
    }

    public function loadSimpleModuleAccessLogsCount( $intNumberOfDay, $objModuleLogFilter = NULL, $objDatabase ) {

    	$strWhere = ' WHERE mal.hit_count >= 1';

    	if( false == is_null( $objModuleLogFilter ) ) {

    		if( false == is_null( $objModuleLogFilter->getProductIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objModuleLogFilter->getProductIds() ) ) {
    			$strWhere .= ' AND mal.ps_product_id IN ( ' . implode( ', ', $objModuleLogFilter->getProductIds() ) . ' )';
    		}

    		$strModule = $objModuleLogFilter->getModule();

    		if( false == is_null( $strModule ) && false == empty( $strModule ) ) {
    			$strWhere .= ' AND mal.module ILIKE \'%' . $strModule . '%\'';
    		}

    		$strAction = $objModuleLogFilter->getAction();

    		if( false == empty( $strAction ) ) {
    			$strWhere .= ' AND mal.action ILIKE \'%' . $strAction . '%\'';
    		}

    		$strFromDate = $objModuleLogFilter->getFromDate();

    		if( false == is_null( $strFromDate ) && false == empty( $strFromDate ) ) {
    			$strWhere .= ' AND mal.day >= \'' . $strFromDate . '\'';
    		}

    		$strToDate = $objModuleLogFilter->getToDate();

    		if( false == is_null( $strToDate ) && false == empty( $strToDate ) ) {
    			$strWhere .= ' AND mal.day <= \'' . $strToDate . '\'';
    		}
    	}

    	if( true == is_null( $strFromDate ) && true == is_null( $strToDate ) && false == is_null( $intNumberOfDay ) ) {
    		$strWhere .= ' AND mal.day >= ( NOW() - INTERVAL \'' . $intNumberOfDay . ' days\' ) ';
    	}

    	$strSql = 'SELECT
					    count ( mal.* )
					FROM
					    module_access_logs mal ' . $strWhere;

    	return fetchData( $strSql, $objDatabase );
    }

}
?>