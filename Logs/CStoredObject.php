<?php

class CStoredObject extends CBaseStoredObject {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceTable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceTag() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVendor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContainer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContentLength() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEtag() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createGatewayRequest( $arrmixArgs = [] ) {
		$arrmixRequest = [
			'cid'                   => $this->getCid(),
			'container'             => $this->getContainer(),
			'objectId'              => $this->getReferenceId(),
			'key'                   => $this->getKey(),
			'storageGateway'        => $this->getVendor(),
			'legacyStorageGateway'  => $this->getDetailsField( [ 'legacy_persistence', 'storageGateway' ] )
		];

		if( true == valStr( $this->getDetailsField( [ 'legacy_persistence', 'container' ] ) ) ) {
			$arrmixRequest['legacyContainer'] = $this->getDetailsField( [ 'legacy_persistence', 'container' ] );
		}

		if( true == valStr( $this->getDetailsField( [ 'legacy_persistence', 'key' ] ) ) ) {
			$arrmixRequest['legacyKey'] = $this->getDetailsField( [ 'legacy_persistence', 'key' ] );
		}

		foreach( $arrmixArgs as $strKey => $strVal ) {
			$arrmixRequest[$strKey] = $strVal;
		}

		if( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_MULTI_SOURCED == $arrmixRequest['storageGateway'] ) {
			unset( $arrmixRequest['storageGateway'] );
		}

		if( 1 == $this->getCid() ) unset( $arrmixRequest['cid'] );

		return $arrmixRequest;
	}

}
?>