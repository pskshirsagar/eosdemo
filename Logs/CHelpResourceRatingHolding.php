<?php

class CHelpResourceRatingHolding extends CBaseHelpResourceRatingHolding {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpResourceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRatingValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valComment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCommentByCompanyUserName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserRole() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>