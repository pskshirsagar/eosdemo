<?php

class CDiagnosticEmployee extends CBaseDiagnosticEmployee {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDiagnosticId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmployeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>