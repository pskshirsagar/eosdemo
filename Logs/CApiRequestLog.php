<?php

class CApiRequestLog extends CBaseApiRequestLog {

	use \Psi\Libraries\EosFoundation\TEosStoredObject;
	const AUTHENTICATION_METHOD_BASIC_HEADER  = 'header-basic';
	const AUTHENTICATION_METHOD_BEARER_HEADER = 'header-bearer';
	const AUTHENTICATION_METHOD_REQUEST_AUTH  = 'request-auth';
	const AUTHENTICATION_METHOD_NO_AUTH       = 'no-auth';

	protected $m_strTitle;

	public static $c_arrstrServiceAuthenticationMethods = [
		self::AUTHENTICATION_METHOD_BASIC_HEADER  => 'Basic Header',
		self::AUTHENTICATION_METHOD_BEARER_HEADER => 'Bearer Header',
		self::AUTHENTICATION_METHOD_REQUEST_AUTH  => 'Request Auth',
		self::AUTHENTICATION_METHOD_NO_AUTH       => 'No Auth'
	];

	public static $_REQUEST_FORM_API_REQUEST_LOG = [
		'id'                                => NULL,
		'cluster_id'                        => NULL,
		'database_id'                       => NULL,
		'cid'                               => NULL,
		'service_id'                        => NULL,
		'minor_api_version'                 => NULL,
		'major_api_version'                 => NULL,
		'service_consumer_id'               => NULL,
		'service_consumer_reference_id'     => NULL,
		'service_authentication_type_id'    => NULL,
		'property_ids'                      => NULL,
		'integration_result_id'             => NULL,
		'service_consumer_reference_number' => NULL,
		'user_name'                         => NULL,
		'entrata_report_name'               => NULL,
		'service_token'                     => NULL,
		'service_url'                       => NULL,
		'content_type'                      => NULL,
		'response_code'                     => NULL,
		'error_description'                 => NULL,
		'user_agent'                        => NULL,
		'request_id'                        => NULL,
		'request_forwarded_to'              => NULL,
		'is_internal'                       => NULL,
		'ip_address'                        => NULL,
		'accessed_on'                       => NULL,
		'completed_on'                      => NULL,
		'time_consumed'                     => NULL,
		'memory_consumed'                   => NULL,
		'updated_by'                        => NULL,
		'updated_on'                        => NULL,
		'created_by'                        => NULL,
		'created_on'                        => NULL,
		'authentication_method'             => NULL,
		'report_data'                       => NULL
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valServiceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valServiceConsumerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valServiceConsumerReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valServiceAuthenticationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIntegrationResultId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valServiceConsumerReferenceNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUserName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valServiceToken() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valServiceUrl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContentType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResponseCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valErrorDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUserAgent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestForwardedTo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsInternal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIpAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccessedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = CStrings::strTrimDef( $strTitle, -1, NULL, true );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function getStorageGateWay() {
		return CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3;
	}

	public function getObjectStorageGateway() {
		$objStorageGateway           = CObjectStorageGatewayFactory::createObjectStorageGateway( $this->getStorageGateWay() );
		$objStorageGateway->initialize();
		return $objStorageGateway;
	}

	public function calcStorageKey() {
		$strDate = ( false != valStr( $this->getCreatedOn() ) ? date( 'Y/m/d', strtotime( $this->getCreatedOn() ) ) : date( 'Y/m/d' ) );
		if( false != valId( $this->getCid() ) ) {
			return $this->getCid() . '/' . 'api_logs/' . $strDate . '/' . $this->getId() . '/' . $this->getId() . '_result.txt';
		}
		return 'api_logs/' . $strDate . '/' . $this->getId() . '/' . $this->getId() . '_result.txt';
	}

	public function calcStorageContainer( $strVendor = NULL ) {
		return CConfig::get( 'OSG_BUCKET_API' );
	}

	public function createStoredObject( $strReferenceTag = NULL ) {
		$intApiRequestLogId = $this->getId();
		if( false == valStr( $strReferenceTag ) ) {
			$strReferenceTag = $this->calcStorageKey( );
		}
		$objStoredObject = new \CStoredObject();
		$objStoredObject->setCid( $this->getCid() );
		$objStoredObject->setReferenceTable( self::TABLE_NAME );
		$objStoredObject->setReferenceId( $intApiRequestLogId );
		$objStoredObject->setReferenceTag( $this->getId() . '_result.txt' );
		$objStoredObject->setVendor( \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 );
		$objStoredObject->setContainer( $this->calcStorageContainer() );
		$objStoredObject->setKey( $strReferenceTag );
		$objStoredObject->setDetailsField( [ 'legacy_persistence', 'storageGateway' ], \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 );
		$objStoredObject->setDetailsField( [ 'legacy_persistence', 'container' ], $this->calcStorageContainer() );
		$objStoredObject->setDetailsField( [ 'legacy_persistence', 'key' ], $strReferenceTag );

		return $objStoredObject;
	}

	public function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Logs\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), self::TABLE_NAME, $strReferenceTag, $objDatabase );
	}
	/**
	 * @param      $strReferenceTag : value for referenceTag is sftp_keys or pgp_keys
	 * @param bool $boolRawData
	 * @return object|null
	 */
	public function getKeyResponse( $strReferenceTag ) {
		$strFileName               = $this->calcStorageKey( $strReferenceTag );
		$objStoredObject           = $this->fetchStoredObject( $this->m_objDatabase, $strFileName );
		$arrmixObjectConfiguration = $objStoredObject->createGatewayRequest( [ 'storageGateway' => $this->getStorageGateWay(), 'noEncrypt' => true, 'mountSystem' => 'NON_BACKUP_MOUNTS', 'outputFile' => 'temp' ] );

		$arrmixGatewayResponse = $this->getObjectStorageGateway( $objStoredObject->getVendor() )->getObject( $arrmixObjectConfiguration );
		if( false == $arrmixGatewayResponse->hasErrors() || false != valStr( $arrmixGatewayResponse['outputFile'] ) ) {
			return $arrmixGatewayResponse['outputFile'];
		}

		return NULL;

	}
	/**
	 * @param CApiRequestLog $objApiRequestLog
	 * @param $strApiLog
	 * @param $intApiRequestLogId
	 * @param $intCurrentUserId
	 * @return bool
	 */
	public function storeResultFiles( $strApiLog, $intCurrentUserId, $objDatabase ) {
		$strFileName               = $this->calcStorageKey();
		$objStoredObject           = $this->createStoredObject( $strFileName );
		$arrmixObjectConfiguration = $this->createPutGatewayRequest( [ 'storageGateway' => $this->getStorageGateWay(), 'data' => $strApiLog, 'noEncrypt' => true, 'mountSystem' => 'NON_BACKUP_MOUNTS' ], $strFileName );
		$arrmixPutObjectResponse   = $this->getObjectStorageGateway()->putObject( $arrmixObjectConfiguration );
		if( false != is_null( $arrmixPutObjectResponse ) || false == $arrmixPutObjectResponse->isSuccessful() ) {
			// On live if it failed to store on S3 try to store on Mounts
			$arrmixObjectConfiguration['storageGateway'] = CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM;
			$arrmixPutObjectResponse                     = $this->getObjectStorageGateway()->putObject( $arrmixObjectConfiguration );
			if( false != is_null( $arrmixPutObjectResponse ) || false == $arrmixPutObjectResponse->isSuccessful() ) {
				return false;
			}
		}
		$arrmixPutObjectResponse['referenceId'] = $this->getId();
		$this->setTitle( pg_escape_literal( $objDatabase->getHandle(), 'Result File:' ) . $this->getId() );
		$this->setObjectStorageGatewayResponse( $arrmixPutObjectResponse );
		if( false == $this->insertOrUpdateStoredObject( $intCurrentUserId, $objDatabase, $objStoredObject, $strFileName ) ) {
			return false;
		}

		return true;

	}
}
?>