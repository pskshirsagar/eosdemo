<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CEcsMessageLogs
 * Do not add any new functions to this class.
 */

class CEcsMessageLogs extends CBaseEcsMessageLogs {

	public static function fetchAllEcsMessageLogs( $intMessageId, $objDatabase ) {

		$strSql = 'Select * from ecs_message_logs WHERE ecs_message_id =' . $intMessageId;

		return self::fetchEcsMessageLogs( $strSql, $objDatabase );
	}

}
?>