<?php

class CDiagnosticPermission extends CBaseDiagnosticPermission {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDatabaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $arrobjDiagnosticPermissions = NULL, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.diagnostic_permissions_id_seq\' )' : ( int ) $this->m_intId;

		$strSql = 'INSERT INTO
					  public.diagnostic_permissions
					VALUES';

		foreach( $arrobjDiagnosticPermissions as $objDiagnosticPermission ) {
			$strSql .= '(' .
				$strId . ',' .
				$objDiagnosticPermission->getCid() . ',' .
				$objDiagnosticPermission->getDatabaseId() . ',' .
				$objDiagnosticPermission->getPsProductId() . ',' .
				( int ) $intCurrentUserId . ',now()),';
		}

		$strSql = rtrim( $strSql, ',' );

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

}
?>