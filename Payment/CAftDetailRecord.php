<?php

class CAftDetailRecord extends CBaseAftDetailRecord {

	public function setAccountNumber( $strAccountNumber ) {
		if( !valStr( $strAccountNumber ) ) {
			return;
		}

		$this->setAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
	}

	public function getAccountNumber() {
		if( !valStr( $this->getAccountNumberEncrypted() ) ) {
			return NULL;
		}

		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER );
	}

	public function getAccountNumberMasked() {
		return 'XXX' . substr( $this->getAccountNumber(), -4 );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAftFileId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAftFileBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSettlementDistributionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEftInstructionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogicalRecordTypeCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountNumberEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameOnAccount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIdentificationNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsReconPrepped() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>