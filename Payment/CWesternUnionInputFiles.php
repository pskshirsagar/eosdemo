<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CWesternUnionInputFiles
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CWesternUnionInputFiles extends CBaseWesternUnionInputFiles {

	public static function fetchLastWesternUnionInputFile( $objPaymentDatabase ) {
    	$strSql = 'SELECT * FROM western_union_input_files f WHERE f.id = ( SELECT max(id) FROM western_union_input_files )	LIMIT 1;';
    	return self::fetchWesternUnionInputFile( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnsentWesternUnionInputFiles( $objPaymentDatabase ) {
    	$strSql = 'SELECT * FROM western_union_input_files f WHERE sent_on IS NULL AND file_name IS NOT NULL AND file_path IS NOT NULL;';
    	return self::fetchWesternUnionInputFiles( $strSql, $objPaymentDatabase );
    }

}
?>