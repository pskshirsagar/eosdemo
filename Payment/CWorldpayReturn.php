<?php

class CWorldpayReturn extends CBaseWorldpayReturn {

	protected $m_objArPayment;
	protected $m_objReturnType;

	public function createReconciliationEntry() {
		$objReconciliationEntry = new CReconciliationEntry();
		$objReconciliationEntry->setDefaults();
		$objReconciliationEntry->setWorldpayReturnId( $this->getId() );
		$objReconciliationEntry->setReconciliationEntryTypeId( CReconciliationEntryType::WORLDPAY_RETURN );
		$objReconciliationEntry->setTraceNumber( $this->getWorldpayCaseId() );
		$objReconciliationEntry->setTotal( - 1 * $this->getAmount() );
		$objReconciliationEntry->setMovementDatetime( $this->getReturnDate() );

		return $objReconciliationEntry;
	}

	public function getOrFetchArPayment( $objPaymentDatabase ) : CArPayment {
		if( !valId( $this->getArPaymentId() ) ) {
			return NULL;
		}

		if( valObj( $this->m_objArPayment, CArPayment::class ) ) {
			return $this->m_objArPayment;
		}

		return \Psi\Eos\Payment\CArPayments::createService()->fetchArPaymentById( $this->getArPaymentId(), $objPaymentDatabase );
	}

	/**
	 * @param $objPaymentDatabase
	 * @return CReturnType|null
	 */
	public function getOrFetchReturnType( $objPaymentDatabase ) {
		if( valObj( $this->m_objReturnType, CReturnType::class ) ) {
			return $this->m_objReturnType;
		}

		if( is_null( $this->getReasonCode() ) ) {
			return NULL;
		}

		$objReturnType = \Psi\Eos\Payment\CReturnTypes::createService()->fetchReturnTypeByCode( $this->getReasonCode(), $objPaymentDatabase );

		if( !valObj( $objReturnType, CReturnType::class ) ) {
			return NULL;
		}

		$this->setReturnTypeId( $objReturnType->getId() );
		$this->m_objReturnType = $objReturnType;

		return $this->getReturnTypeId();
	}

	public function populateRequiresReturnFee( $objPaymentDatabase ) {

		// Default return fee requirement to off
		$this->setRequiresReturnFee( false );

		$arrintEligiblePaymentTypeIds = array( CPaymentType::PAD );
		$arrintEligibleReturnTypeIds = array( CReturnType::NFS_DIRECT_DEBIT );
		$objCompanyMerchantAccount = NULL;

		if( is_null( $this->getArPaymentId() ) || 0 >= $this->getArPaymentId() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'You can only apply return fees to Ar Payments.', NULL ) );
			return;
		}

		$objArPayment = $this->getOrFetchArPayment( $objPaymentDatabase );

		if( !valObj( $objArPayment, CArPayment::class ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to load Ar Payment object from database.', NULL ) );
			return;
		}

		$arrobjNachaReturnAddendaDetailRecords = \Psi\Eos\Payment\CNachaReturnAddendaDetailRecords::createService()->fetchNachaReturnAddendaDetailRecordsByArPaymentId( $this->getArPaymentId(), $objPaymentDatabase );

		// the current object already exists in the database so check for more than 1 result to see if there are pre-existing returns
		if( false == is_null( $arrobjNachaReturnAddendaDetailRecords ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrobjNachaReturnAddendaDetailRecords ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'This is not the first return for this ar_payment.', NULL ) );
			return;
		}

		if( false == in_array( $objArPayment->getPaymentTypeId(), $arrintEligiblePaymentTypeIds ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'You can only apply return fees for PAD transactions.', NULL ) );
			return;
		}

		if( NULL != $objArPayment->getArPaymentId() ) {
			$objAssociatedArPayment = \Psi\Eos\Payment\CArPayments::createService()->fetchArPaymentByIdByCid( $objArPayment->getArPaymentId(), $objArPayment->getCid(), $objPaymentDatabase );
			if( valObj( $objAssociatedArPayment, CArPayment::class ) ) {
				if( CPaymentStatusType::REVERSED == $objAssociatedArPayment->getPaymentStatusTypeId() || 1 == $objArPayment->getIsReversed() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Do not charge NSF fee when reversal is already initiated on payment.', NULL ) );
					return;
				}
			}
		}

		$objCompanyMerchantAccount = $objArPayment->getOrFetchCompanyMerchantAccount( $objPaymentDatabase );

		if( !valObj( $objCompanyMerchantAccount, CCompanyMerchantAccount::class ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to load Merchant Account object from database.', NULL ) );
			return;
		}

		if( !in_array( $this->getReturnTypeId(), $arrintEligibleReturnTypeIds, true ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Return type is ineligible for applying return fees: ' . $this->getReturnTypeId(), NULL ) );
			return;
		}

		if( true == is_null( $objCompanyMerchantAccount->getResidentReturnFee( $objArPayment->getPaymentTypeId() ) ) || 0 >= $objCompanyMerchantAccount->getResidentReturnFee( $objArPayment->getPaymentTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Resident return fee must be greater than $0.', NULL ) );
			return;
		}

		$this->setRequiresReturnFee( true );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWorldpayCaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReasonCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReasonDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArPaymentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEftInstructionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>