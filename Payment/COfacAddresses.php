<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\COfacAddresses
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class COfacAddresses extends CBaseOfacAddresses {

	public static function fetchOfacAddresseCountries( $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT
						country
					FROM
						ofac_addresses
					WHERE
						country IS NOT NULL
					ORDER BY
						country';

		return fetchData( $strSql, $objDatabase );
	}

}
?>