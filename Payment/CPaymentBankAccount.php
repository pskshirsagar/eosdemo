<?php

class CPaymentBankAccount extends CBasePaymentBankAccount {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentBankAccountTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCurrencyCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'uuid_generate_v4()' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, payment_bank_account_type_id, country_code, currency_code, details, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
			$strId . ', ' .
			$this->sqlCid() . ', ' .
			$this->sqlPaymentBankAccountTypeId() . ', ' .
			$this->sqlCountryCode() . ', ' .
			$this->sqlCurrencyCode() . ', ' .
			$this->sqlDetails() . ', ' .
			$this->sqlDeletedBy() . ', ' .
			$this->sqlDeletedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlUpdatedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function executeSql( $strSql, $objBaseTarget, $objDatabase ) {
		$strType = NULL;

		if( true == \Psi\CStringService::singleton()->stristr( \Psi\CStringService::singleton()->strtolower( $strSql ), 'insert' ) ) {
			$strType = 'insert';
		} else {
			if( true == \Psi\CStringService::singleton()->stristr( \Psi\CStringService::singleton()->strtolower( $strSql ), 'update' ) ) {
				$strType = 'update';
			} else {
				if( true == \Psi\CStringService::singleton()->stristr( \Psi\CStringService::singleton()->strtolower( $strSql ), 'delete' ) ) {
					$strType = 'delete';
				}
			}
		}

		switch( $strType ) {
			case 'insert':
			case 'update':
			case 'delete':
				$objDatabase->logWriteQueriesCount();
				break;

			default:
				// For other SQL operation, system will not check permissions.
				break;
		}

		$objDataset = $objDatabase->getMasterDatabase()->createDataset();

		if( false == $objDataset->execute( $strSql, true ) ) {
			$objBaseTarget->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to ' . $strType . ' ' . get_class( $objBaseTarget ) . ' record. The following error was reported.' ) );
			$objBaseTarget->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;

		} else {
			// On insert, we need to catch the id and assign it to the variable.
			if( 0 < $objDataset->getRecordCount() ) {
				while( false == $objDataset->eof() ) {
					$arrmixValues = $objDataset->fetchArray();
					if( true == isset ( $arrmixValues['id'] ) ) {
						$objBaseTarget->setId( $arrmixValues['id'] );
					}
					$objDataset->next();
				}
			}
		}

		if( true == $this->isStoredProc( $strSql ) ) {
			if( 0 < $objDataset->getRecordCount() ) {
				$objBaseTarget->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to ' . $strType . ' ' . get_class( $objBaseTarget ) . ' record. The following error was reported.' ) );
				while( false == $objDataset->eof() ) {
					$arrmixValues = $objDataset->fetchArray();
					$objBaseTarget->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
					$objDataset->next();
				}

				$objDataset->cleanup();

				return false;
			}
		}

		$objDataset->cleanup();

		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'payment_bank_account_type_id' => $this->getPaymentBankAccountTypeId(),
			'country_code' => $this->getCountryCode(),
			'currency_code' => $this->getCurrencyCode(),
			'details' => json_encode( $this->getDetails() ),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>