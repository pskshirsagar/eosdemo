<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CCompanyMerchantAccounts
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CCompanyMerchantAccounts extends CBaseCompanyMerchantAccounts {

	const COMPANY_MERCHANT_ACCOUNTS							= 'company_merchant_accounts';
	const VIEW_COMPANY_MERCHANT_ACCOUNTS 					= 'view_company_merchant_accounts';
	const VIEW_RECURRING_COMPANY_MERCHANT_ACCOUNTS 			= 'view_recurring_company_merchant_accounts';
	const VIEW_TERMINAL_COMPANY_MERCHANT_ACCOUNTS 			= 'view_terminal_company_merchant_accounts';

	/**
	 * @param CCompanyMerchantAccount $objCompanyMerchantAccount
	 * @param $intPaymentMediumId
	 * @param CDatabase $objPaymentDatabase
	 */
	public static function setPaymentMediumOnObject( $objCompanyMerchantAccount, $intPaymentMediumId = NULL, $objPaymentDatabase = NULL ) {
		if( false == valObj( $objCompanyMerchantAccount, CCompanyMerchantAccount::class ) ) {
			return;
		}

		if( true == is_null( $intPaymentMediumId ) ) {
			$intPaymentMediumId = CPaymentMedium::WEB;
		}

		if( false == is_null( $objPaymentDatabase ) ) {
			$objCompanyMerchantAccount->setPaymentDatabase( $objPaymentDatabase );
		}

		$objCompanyMerchantAccount->setPaymentMediumId( $intPaymentMediumId );
	}

	/**
	 * @param CCompanyMerchantAccount[] $arrobjCompanyMerchantAccounts
	 * @param int $intPaymentMediumId
	 * @param CDatabase $objPaymentDatabase
	 */
	public static function setPaymentMediumOnObjects( $arrobjCompanyMerchantAccounts, $intPaymentMediumId, $objPaymentDatabase = NULL ) {
		if( false == valArr( $arrobjCompanyMerchantAccounts ) ) {
			return;
		}

		foreach( $arrobjCompanyMerchantAccounts as $objCompanyMerchantAccount ) {
			self::setPaymentMediumOnObject( $objCompanyMerchantAccount, $intPaymentMediumId, $objPaymentDatabase );
		}
	}

	public static function fetchCompanyMerchantAccountsByCid( $intCid, $objPaymentDatabase, $intPaymentMediumId = NULL ) {
		$arrobjCompanyMerchantAccounts = self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM %s WHERE cid = %d ORDER BY account_name', self::COMPANY_MERCHANT_ACCOUNTS, ( int ) $intCid ), $objPaymentDatabase );

		self::setPaymentMediumOnObjects( $arrobjCompanyMerchantAccounts, $intPaymentMediumId, $objPaymentDatabase );

		return $arrobjCompanyMerchantAccounts;
	}

	public static function fetchCompanyMerchantAccountsByDistributionAccountId( $intAccountId, $objPaymentDatabase ) {
		$arrobjMerchantAccounts = self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE distribution_account_id = %d', ( int ) $intAccountId ), $objPaymentDatabase );
		self::setPaymentMediumOnObjects( $arrobjMerchantAccounts, NULL, $objPaymentDatabase );
		return $arrobjMerchantAccounts;
	}

	public static function fetchCompanyMerchantAccountById( $intId, $objPaymentDatabase, $intPaymentMediumId = NULL ) {
		if ( false == is_numeric( $intPaymentMediumId ) ) $intPaymentMediumId = CPaymentMedium::WEB;
		$objCompanyMerchantAccount = self::fetchCompanyMerchantAccount( sprintf( 'SELECT * FROM %s WHERE id = %d', self::COMPANY_MERCHANT_ACCOUNTS, ( int ) $intId ), $objPaymentDatabase );

		self::setPaymentMediumOnObject( $objCompanyMerchantAccount, $intPaymentMediumId, $objPaymentDatabase );

		return $objCompanyMerchantAccount;
	}

	public static function fetchCompanyMerchantAccountByIds( $arrintCompanyMerchantAccountIds, $objPaymentDatabase, $intPaymentMediumId = NULL ) {
		if( true == is_null( $arrintCompanyMerchantAccountIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintCompanyMerchantAccountIds ) ) return NULL;

		$arrobjCompanyMerchantAccounts = self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM %s WHERE is_disabled != 1 AND id IN (' . implode( ',', $arrintCompanyMerchantAccountIds ) . ')', self::COMPANY_MERCHANT_ACCOUNTS ), $objPaymentDatabase );

		self::setPaymentMediumOnObjects( $arrobjCompanyMerchantAccounts, $intPaymentMediumId, $objPaymentDatabase );

		return $arrobjCompanyMerchantAccounts;
	}

	public static function fetchCompanyMerchantAccountNameByIds( $arrintCompanyMerchantAccountIds, $objPaymentDatabase ) {

		if( false == valArr( $arrintCompanyMerchantAccountIds ) ) return NULL;

		$strSql = 'SELECT
						cma.id,
						cma.cid,
						cma.account_name
					FROM company_merchant_accounts cma
					WHERE cma.id IN( ' . implode( ',', $arrintCompanyMerchantAccountIds ) . ' )';

		return fetchData( $strSql, $objPaymentDatabase );
	}

	public static function fetchLastCompanyMerchantAccountByPaymentMediumIdCid( $intPaymentMediumId, $intCid, $objPaymentDatabase ) {
		$objCompanyMerchantAccount = self::fetchCompanyMerchantAccount( sprintf( 'SELECT * FROM %s WHERE cid = %d AND is_disabled <> 1 ORDER BY created_on DESC LIMIT 1', self::COMPANY_MERCHANT_ACCOUNTS, ( int ) $intCid ), $objPaymentDatabase );

		self::setPaymentMediumOnObject( $objCompanyMerchantAccount, $intPaymentMediumId, $objPaymentDatabase );

		return $objCompanyMerchantAccount;
	}

	public static function fetchActiveCompanyMerchantAccountsByCid( $intCid, $objPaymentDatabase, $intPaymentMediumId = NULL, $arrstrCountryCodes = NULL ) {
		$strSql = 'SELECT * FROM %s WHERE cid = %d AND is_disabled != 1';
		if( is_null( $arrstrCountryCodes ) == false ) $strSql .= ' AND country_code IN (\'' . implode( '\',\'', $arrstrCountryCodes ) . '\')';
		$strSql .= ' ORDER BY account_name';
		$arrobjCompanyMerchantAccounts = self::fetchCompanyMerchantAccounts( sprintf( $strSql, self::COMPANY_MERCHANT_ACCOUNTS, ( int ) $intCid ), $objPaymentDatabase );

		self::setPaymentMediumOnObjects( $arrobjCompanyMerchantAccounts, $intPaymentMediumId, $objPaymentDatabase );

		return $arrobjCompanyMerchantAccounts;
	}

	public static function fetchActiveCompanyMerchantAccountsByCids( $arrintCids, $objPaymentDatabase, $intPaymentMediumId = NULL ) {
		$arrobjCompanyMerchantAccounts = self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM %s WHERE cid IN ( ' . implode( ',', $arrintCids ) . ' ) AND is_disabled != 1 ORDER BY account_name', self::COMPANY_MERCHANT_ACCOUNTS, ( int ) $arrintCids ), $objPaymentDatabase );

		self::setPaymentMediumOnObjects( $arrobjCompanyMerchantAccounts, $intPaymentMediumId, $objPaymentDatabase );

		return $arrobjCompanyMerchantAccounts;
	}

	public static function fetchActiveCompanyMerchantAccounts( $objPaymentDatabase, $intPaymentMediumId = NULL ) {
		$arrobjCompanyMerchantAccounts = self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM %s WHERE is_disabled != 1 ORDER BY cid, id', self::COMPANY_MERCHANT_ACCOUNTS ), $objPaymentDatabase );

		self::setPaymentMediumOnObjects( $arrobjCompanyMerchantAccounts, $intPaymentMediumId, $objPaymentDatabase );

		return $arrobjCompanyMerchantAccounts;
	}

	public static function fetchCompanyMerchantAccountsByIds( $arrintCompanyMerchantAccountIds, $objPaymentDatabase, $intPaymentMediumId = NULL ) {
		if( false == valArr( $arrintCompanyMerchantAccountIds ) ) return NULL;
		$strSql = 'SELECT * FROM ' . self::COMPANY_MERCHANT_ACCOUNTS . ' WHERE id IN ( ' . implode( ',', $arrintCompanyMerchantAccountIds ) . ' )';
		$arrobjCompanyMerchantAccounts = self::fetchCompanyMerchantAccounts( $strSql, $objPaymentDatabase );

		self::setPaymentMediumOnObjects( $arrobjCompanyMerchantAccounts, $intPaymentMediumId, $objPaymentDatabase );

		return $arrobjCompanyMerchantAccounts;
	}

	public static function fetchCompanyMerchantAccountsByIdsSortedByCompanyName( $arrintCompanyMerchantAccountIds, $objPaymentDatabase, $intPaymentMediumId = NULL ) {

		if( false == valArr( $arrintCompanyMerchantAccountIds ) ) return NULL;

		$strSql = 'SELECT
    					vcma.*
    				FROM
    					' . self::COMPANY_MERCHANT_ACCOUNTS . ' vcma,
    					clients c
    				WHERE
    					vcma.cid = c.id
    					AND vcma.id IN ( ' . implode( ',', $arrintCompanyMerchantAccountIds ) . ' )
    				ORDER BY
    					c.company_name,
    					vcma.account_name';

		$arrobjCompanyMerchantAccounts = self::fetchCompanyMerchantAccounts( $strSql, $objPaymentDatabase );

		self::setPaymentMediumOnObjects( $arrobjCompanyMerchantAccounts, $intPaymentMediumId, $objPaymentDatabase );

		return $arrobjCompanyMerchantAccounts;
	}

	public static function fetchCompanyMerchantAccountsByCidByIds( $intCid, $arrintCompanyMerchantAccountIds, $objPaymentDatabase, $intPaymentMediumId = NULL ) {
		if( false == valArr( $arrintCompanyMerchantAccountIds ) ) return NULL;
		$strSql = 'SELECT * FROM ' . self::COMPANY_MERCHANT_ACCOUNTS . ' WHERE cid = ' . ( int ) $intCid . ' AND id IN ( ' . implode( ',', $arrintCompanyMerchantAccountIds ) . ' )';
		$arrobjCompanyMerchantAccounts = self::fetchCompanyMerchantAccounts( $strSql, $objPaymentDatabase );

		self::setPaymentMediumOnObjects( $arrobjCompanyMerchantAccounts, $intPaymentMediumId, $objPaymentDatabase );

		return $arrobjCompanyMerchantAccounts;
	}

	public static function fetchCompanyMerchantAccountByCompanyMerchantAccountIdPaymentMediumId( $intPaymentMediumId, $intCompanyMerchantAccountId, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM ' . self::COMPANY_MERCHANT_ACCOUNTS . ' WHERE id = ' . ( int ) $intCompanyMerchantAccountId;
		$objCompanyMerchantAccount = self::fetchCompanyMerchantAccount( $strSql, $objPaymentDatabase );

		self::setPaymentMediumOnObject( $objCompanyMerchantAccount, $intPaymentMediumId, $objPaymentDatabase );

		return $objCompanyMerchantAccount;
	}

	public static function fetchCompanyMerchantAccountByCompanyMerchantAccountIdByCidByPaymentMediumId( $intCompanyMerchantAccountId, $intCid, $intPaymentMediumId, $objPaymentDatabase ) {
		if( false == is_numeric( $intCompanyMerchantAccountId ) ) return NULL;
		$strSql = 'SELECT * FROM ' . self::COMPANY_MERCHANT_ACCOUNTS . ' WHERE id = ' . ( int ) $intCompanyMerchantAccountId . ' AND cid = ' . ( int ) $intCid;
		$objCompanyMerchantAccount = self::fetchCompanyMerchantAccount( $strSql, $objPaymentDatabase );

		self::setPaymentMediumOnObject( $objCompanyMerchantAccount, $intPaymentMediumId, $objPaymentDatabase );

		return $objCompanyMerchantAccount;
	}

	public static function fetchActiveCompanyMerchantAccountByIdByCidByPaymentMediumId( $intCompanyMerchantAccountId, $intCid, $intPaymentMediumId, $objPaymentDatabase ) {
		if( false == is_numeric( $intCompanyMerchantAccountId ) ) return NULL;
		$strSql = 'SELECT * FROM ' . self::COMPANY_MERCHANT_ACCOUNTS . ' WHERE id = ' . ( int ) $intCompanyMerchantAccountId . ' AND cid = ' . ( int ) $intCid . ' AND is_disabled <> 1';
		$objCompanyMerchantAccount = self::fetchCompanyMerchantAccount( $strSql, $objPaymentDatabase );

		self::setPaymentMediumOnObject( $objCompanyMerchantAccount, $intPaymentMediumId, $objPaymentDatabase );

		return $objCompanyMerchantAccount;
	}

	// fetch all company merchant account which are requested to underwrite.

	public static function fetchSimpleUnderwritingRequiredCompanyMerchantAccounts( $arrstrMerchantAccountUnderwritingFilter, $objDatabase ) {

		$intOffset 				= ( true == isset( $arrstrMerchantAccountUnderwritingFilter['page_no'] ) && true == isset( $arrstrMerchantAccountUnderwritingFilter['page_size'] ) ) ? $arrstrMerchantAccountUnderwritingFilter['page_size'] * ( $arrstrMerchantAccountUnderwritingFilter['page_no'] - 1 ) : 0;
		$intLimit 				= ( true == isset( $arrstrMerchantAccountUnderwritingFilter['page_size'] ) ) ? $arrstrMerchantAccountUnderwritingFilter['page_size'] : '';
		$strOrderByField 		= ' subq.company_name';
		$strOrderByType			= ' ASC';

		if( true == valArr( $arrstrMerchantAccountUnderwritingFilter ) ) {
			if( true == isset( $arrstrMerchantAccountUnderwritingFilter['order_by_field'] ) ) {

				switch( $arrstrMerchantAccountUnderwritingFilter['order_by_field'] ) {

					case 'company_name':
						$strOrderByField = ' company_name';
						break;

					case 'account_name':
						$strOrderByField = ' account_name';
						break;

					case 'status':
						$strOrderByField = ' underwritten_on';
						break;

					default:
						$strOrderByField = ' company_name';
						break;
				}
			}

			if( true == isset( $arrstrMerchantAccountUnderwritingFilter['order_by_type'] ) ) {
				$strOrderByType = ( 'asc' == $arrstrMerchantAccountUnderwritingFilter['order_by_type'] ) ? ' ASC' : ' DESC';
			}
		}

		$strSql = 'SELECT *
						FROM ( SELECT
						            DISTINCT ON(cma.cid ) cid,
						            cma.id AS id,
						            cma.account_name,
						            cma.underwriting_request_date,
						            cma.underwriting_sent_on,
						            cma.underwritten_on,
						            c.company_name
						        FROM
						            company_merchant_accounts cma
						            JOIN clients c ON ( cma.cid = c.id AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
						        WHERE
						            cma.underwriting_request_date IS NOT NULL
						        GROUP BY
						            cma.cid,
						            cma.id,
						            cma.account_name,
						            cma.underwriting_request_date,
						            cma.underwriting_sent_on,
						            cma.underwritten_on,
						            c.company_name
						      ) as subq
						ORDER BY
							' . $strOrderByField . $strOrderByType . '
						OFFSET ' . ( int ) $intOffset;

		if( false == is_null( $intLimit ) ) {
			$strSql .= ' LIMIT	' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnderwritingRequiredCompanyMerchantAccountsCount( $objDatabase ) {

		$strSql = 'SELECT
       					count(*) as count
					FROM (
					        SELECT
					            DISTINCT ON(cma.cid ) cid
					        FROM
					            company_merchant_accounts cma
					            JOIN clients c ON ( cma.cid = c.id AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
					        WHERE
					            cma.underwriting_request_date IS NOT NULL
					        GROUP BY
					            cma.cid,
					            cma.id,
					            cma.account_name,
					            cma.underwriting_request_date,
					            cma.underwriting_sent_on,
					            cma.underwritten_on,
					            c.company_name
					       ) as subq';

		$arrstrCount = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrstrCount[0]['count'] ) ) ? $arrstrCount[0]['count'] : 0;
	}

	// fetch all company merchant account ids which goes over $100K in the current year.

	public static function fetchRequiringUnderwritingMerchantAccounts( $objDatabase ) {

		$strSql = 'SELECT
						ap.company_merchant_account_id,
						ap.payment_type_id,
						sum( ap.total_amount ) AS payment_amount
					FROM
						ar_payments as ap
						JOIN company_merchant_accounts as cma ON ( ap.company_merchant_account_id = cma.id AND ap.cid = cma.cid )
						JOIN clients as c ON ( ap.cid = c.id AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
					WHERE
						ap.merchant_gateway_id IN ( ' . CMerchantGateway::VANTIV_CREDIT_CARD . ', ' . CMerchantGateway::LITLE_CREDIT_CARD . ' )
						AND ap.payment_type_id IN ( ' . CPaymentType::VISA . ', ' . CPaymentType::MASTERCARD . ' )
						AND ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . '
						AND DATE_TRUNC( \'year\', ap.payment_datetime ) > DATE_TRUNC( \'year\', NOW() - INTERVAL \' 3 year \' )
					    AND DATE_TRUNC( \'year\', ap.payment_datetime ) >= \'2013/01/01\'
						AND cma.underwriting_request_date IS NULL
					GROUP BY
						ap.company_merchant_account_id,
						ap.payment_type_id,
						DATE_TRUNC( \'year\', ap.payment_datetime )
					HAVING
						sum( ap.total_amount ) >= 100000
					ORDER BY
						payment_amount DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyMerchantAccountBySettlementDistributionId( $intSettlementDistributionId, $objPaymentDatabase ) {

		$strSql = 'SELECT
						 cma.*
    				FROM
						 settlement_distributions sd
        				 JOIN company_merchant_accounts cma ON (cma.id = sd.company_merchant_account_id)
  				  WHERE
						sd.id = ' . ( int ) $intSettlementDistributionId;

		$objCompanyMerchantAccount = self::fetchCompanyMerchantAccount( $strSql, $objPaymentDatabase );
		self::setPaymentMediumOnObject( $objCompanyMerchantAccount, NULL, $objPaymentDatabase );

		return $objCompanyMerchantAccount;
	}

	public static function fetchAllCompanyMerchantAccountsByIds( $arrintCompanyMerchantAccountIds, $objPaymentDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						company_merchant_accounts cma
					WHERE
						cma.id IN (  ' . implode( ',', $arrintCompanyMerchantAccountIds ) . ' )';

		$arrobjMerchantAccounts = self::fetchCompanyMerchantAccounts( $strSql, $objPaymentDatabase );
		self::setPaymentMediumOnObjects( $arrobjMerchantAccounts, NULL, $objPaymentDatabase );
		return $arrobjMerchantAccounts;
	}

	public static function fetchSearchMerchantAccounts( $arrmixFilteredExplodedSearch, $objDatabase ) {

		$strMerchantAccountIdCondition = ( true == isset( $arrmixFilteredExplodedSearch[0] ) && is_numeric( $arrmixFilteredExplodedSearch[0] ) ) ? ' to_char( cma.id, \'99999999\' ) LIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' OR ' : '';

		$strSql = 'SELECT
						cma.id,
						cma.account_name,
						c.id as company_id,
						c.company_name,
						c.ps_lead_id
					FROM
						company_merchant_accounts cma
						JOIN clients c ON ( c.id = cma.cid )
					WHERE
						(' . $strMerchantAccountIdCondition . ' cma.account_name ILIKE E\'%' . implode( '%\' AND cma.account_name ILIKE E\'%', $arrmixFilteredExplodedSearch ) . '%\') AND cma.is_disabled = 0
					ORDER BY
						cma.id
					LIMIT
						10';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedEnabledOrDisabledCompanyMerchantAccountsOrCountByCid( $intCid, $objPaymentDatabase, $objPagination = NULL, $boolReturnCount = false, $boolIsDisabled = false ) {

		if( false == is_numeric( $intCid ) )  return NULL;

		$strSql = ( false == $boolReturnCount ) ? 'SELECT vcma.*' : 'SELECT count( vcma.id )';

		$strSql .= ' FROM
						company_merchant_accounts vcma
					 WHERE
						vcma.cid = ' . ( int ) $intCid;

		if( false == $boolIsDisabled ) {
			$strSql .= ' AND is_disabled != 1 ';
		}

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) && false == $boolReturnCount ) {
			$strSql .= 'ORDER BY
							LOWER( account_name ) ASC
							OFFSET ' . ( int ) $objPagination->getOffset() . '
							LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == $boolReturnCount ) {
			$arrintResponse = fetchData( $strSql, $objPaymentDatabase );
			if( true == isset ( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
			else return 0;
		}

		$arrobjMerchantAccounts = self::fetchCompanyMerchantAccounts( $strSql, $objPaymentDatabase );
		self::setPaymentMediumOnObjects( $arrobjMerchantAccounts, NULL, $objPaymentDatabase );
		return $arrobjMerchantAccounts;
	}

	public static function fetchSimpleLegalEntityIdByTaxNumberByTaxLegalNameByCid( $strTaxNumber, $strTaxLegalName, $intCid, $intAccountId, $objPaymentDatabase ) {

		$strSql = 'SELECT
						 cma.external_legal_entity_id
    				FROM
						 company_merchant_accounts cma
  				   WHERE
						cma.tax_number_encrypted = \'' . $strTaxNumber . '\'
						 AND cma.tax_legal_name = \'' . pg_escape_string( $strTaxLegalName ) . '\'
						 AND cma.id != ' . ( int ) $intAccountId . '
						 AND cma.cid = ' . ( int ) $intCid . '
						 AND ( cma.external_legal_entity_id IS NOT NULL OR cma.external_legal_entity_id = \'-1\' )
					LIMIT 1';

		$arrstrData = fetchData( $strSql, $objPaymentDatabase );
		return ( true == isset ( $arrstrData[0]['external_legal_entity_id'] ) ) ? $arrstrData[0]['external_legal_entity_id'] : NULL;
	}

	public static function fetchAllCompanyMerchantAccountsByTaxNumberByTaxLegalName( $strTaxNumber, $strTaxLegalName, $intCid, $intAccountId, $objPaymentDatabase ) {

		$strSql = 'SELECT
						 *
    				FROM
						 company_merchant_accounts cma
  				   WHERE
						cma.tax_number_encrypted = \'' . $strTaxNumber . '\'
						 AND cma.tax_legal_name = \'' . pg_escape_string( $strTaxLegalName ) . '\'
						 AND cma.id != ' . ( int ) $intAccountId . '
						 AND cma.cid = ' . ( int ) $intCid . '
						 AND ( cma.external_legal_entity_id IS NULL OR cma.external_legal_entity_id = \'-1\' ) ';

		$arrobjMerchantAccounts = self::fetchCompanyMerchantAccounts( $strSql, $objPaymentDatabase );
		self::setPaymentMediumOnObjects( $arrobjMerchantAccounts, NULL, $objPaymentDatabase );
		return $arrobjMerchantAccounts;
	}

	public static function fetchSimpleCompanyMerchantAccountByIdByCid( $intCompanyMerchantAccountId, $intCid, $objPaymentDatabase ) {

		$strSql = 'SELECT
						cma.account_name
					FROM
						company_merchant_accounts cma
					WHERE
						cma.id = ' . ( int ) $intCompanyMerchantAccountId . '
						AND cma.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objPaymentDatabase );

	}

	public static function fetchQuickSearchMerchantAccountsByCid( $intCid, $arrstrFilteredExplodedSearch, $objDatabase = NULL, $boolIsDisabled = false ) {
		if( false == valArr( $arrstrFilteredExplodedSearch ) ) return NULL;

		$strSearchCriteria = ' cma.account_name ILIKE E\'%' . implode( '%\' AND cma.account_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' ';
		$strSearchCriteria	.= ( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrFilteredExplodedSearch ) && is_numeric( $arrstrFilteredExplodedSearch[0] ) ) ? ' OR to_char( cma.id, \'99999999\' ) LIKE \'%' . $arrstrFilteredExplodedSearch[0] . '%\' ' : '';

		$strSqlWhere = '';
		if( false == $boolIsDisabled ) {
			$strSqlWhere .= ' AND is_disabled != 1 ';
		}

		$strSql = 'SELECT
					cma.id,
					cma.cid,
					cma.account_name
				FROM
					company_merchant_accounts cma
				WHERE
					cma.cid = ' . ( int ) $intCid . $strSqlWhere . '
					AND ( ' . $strSearchCriteria . ' ) LIMIT 10 ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCommercialCompanyMerchantAccountNamesByCid( $intCid, $objPaymentDatabase ) {
		$strSql = 'SELECT id, account_name FROM company_merchant_accounts WHERE merchant_processing_type_id = ' . CMerchantProcessingType::COMMERCIAL . ' AND cid = ' . ( int ) $intCid;

		$arrmixResults = fetchData( $strSql, $objPaymentDatabase );

		if( false == valArr( $arrmixResults ) ) {
			return array();
		}

		$arrstrAccounts = array();

		foreach( $arrmixResults as $arrmixResult ) {
			$arrstrAccounts[$arrmixResult['id']] = $arrmixResult['account_name'];
		}

		return $arrstrAccounts;
	}

	public static function fetchCompanyMerchantAccountsWithArPaymentsByDateRange( $strStartDatetime, $strEndDatetime, $objPaymentDatabase ) {
		if( false == valStr( $strStartDatetime ) || false == valStr( $strEndDatetime ) ) {
			return NULL;
		}

		$strSql = '
			SELECT
				sub.cid,
				sub.entity_id,
				sub.billing_account_id,
				count( sub.id ) AS item_count,
				array_agg( sub.id ) AS account_ids,
				sum( acquire_processing_fee ) AS acquire_processing_fee
			FROM (
				SELECT
					cma.cid,
					cma.id,
					cma.acquire_processing_fee,
					cma.billing_account_id,
					c.entity_id
				FROM
					company_merchant_accounts cma
					JOIN clients c ON c.id = cma.cid AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
				WHERE
					EXISTS(
						SELECT
							id
						FROM
							ar_payments ap
						WHERE
							ap.cid = cma.cid
							AND ap.company_merchant_account_id = cma.id
							AND ap.payment_datetime BETWEEN \'' . $strStartDatetime . '\' AND \'' . $strEndDatetime . '\'
							AND ap.payment_type_id IN( ' . implode( ',', CPaymentType::$c_arrintElectronicPaymentTypeIds ) . ' )
					) AND
					c.id NOT IN ( ' . CClient::ID_BATMAN_RENTALS . ', ' . CClient::ID_ROBIN_RENTALS . ' )
			) sub
			GROUP BY
				sub.cid,
				sub.entity_id,
				sub.billing_account_id
			;
		';

		return fetchData( $strSql, $objPaymentDatabase );
	}

	public static function fetchCompanyMerchantAccountAllowedPaymentTypesByIdByCid( $intCompanyMerchantAccountId, $intCid, $objPaymentDatabase ) {

		if( false == valId( $intCompanyMerchantAccountId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$objCompanyMerchantAccount = self::fetchCompanyMerchantAccountByCompanyMerchantAccountIdByCidByPaymentMediumId( $intCompanyMerchantAccountId, $intCid, \CPaymentMedium::WEB, $objPaymentDatabase );

		return [
			[
				'id' => $objCompanyMerchantAccount->getId(),
				'ach_is_enabled' => $objCompanyMerchantAccount->methodIsEnabled( \CPaymentType::ACH ),
				'vi_is_enabled' => $objCompanyMerchantAccount->methodIsEnabled( \CPaymentType::VISA ),
				'mc_is_enabled' => $objCompanyMerchantAccount->methodIsEnabled( \CPaymentType::MASTERCARD ),
				'di_is_enabled' => $objCompanyMerchantAccount->methodIsEnabled( \CPaymentType::DISCOVER ),
				'ae_is_enabled' => $objCompanyMerchantAccount->methodIsEnabled( \CPaymentType::AMEX ),
				'emo_is_enabled' => $objCompanyMerchantAccount->methodIsEnabled( \CPaymentType::EMONEY_ORDER ),
				'ch21_is_enabled' => $objCompanyMerchantAccount->methodIsEnabled( \CPaymentType::CHECK_21 )
			]
		];
	}

	public static function fetchNextTabaPayClientId( $objPaymentDatabase ) {
		$strSql = 'SELECT CASE
							WHEN MAX(details->>\'taba_pay_client_id\') IS NULL OR MAX(details->>\'taba_pay_client_id\') = \'\' THEN 1
							ELSE MAX(details->>\'taba_pay_client_id\')::integer + 1 
						END AS client_id
						FROM company_merchant_accounts';

		$arrmixResult = fetchData( $strSql, $objPaymentDatabase );

		return $arrmixResult;
	}

}
?>