<?php

class CNachaReturnDetailRecord extends CBaseNachaReturnDetailRecord {

	protected $m_objNachaReturnAddendaDetailRecord;
	protected $m_objNachaReturnFileBatch;

	protected $m_arrobjNachaReturnAddendaDetailRecords;

    public function __construct() {
        parent::__construct();

        $this->m_arrobjNachaReturnAddendaDetailRecords = array();

        return;
    }

	/**
	 * Add Functions
	 */

	public function addNachaReturnAddendaDetailRecord( $objNachaReturnAddendaDetailRecord ) {
		$this->m_arrobjNachaReturnAddendaDetailRecords[$objNachaReturnAddendaDetailRecord->getId()] = $objNachaReturnAddendaDetailRecord;
	}

	/**
	 * Set Functions
	 */

    public function setDefaults() {
    	$this->m_strReturnedOn = date( 'm/d/Y' );
    }

    public function setDfiAccountNumber( $strDfiAccountNumber ) {
		if ( true == stristr( $strDfiAccountNumber, 'XXXX' ) ) return;
		$strDfiAccountNumber = CStrings::strTrimDef( $strDfiAccountNumber, 20, NULL, true );
	    if( false == valStr( $strDfiAccountNumber ) ) {
		    return NULL;
	    }
		$this->setCheckAccountNumberEncrypted( trim( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strDfiAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) ) );
        $this->m_strDfiAccountNumber = CStrings::strTrimDef( $strDfiAccountNumber, 17, NULL, true );
    }

    public function setNachaReturnFileBatch( $objNachaReturnFileBatch ) {
        $this->m_objNachaReturnFileBatch = $objNachaReturnFileBatch;
    }

    public function setNachaReturnAddendaDetailRecord( $objNachaReturnAddendaDetailRecord ) {
    	$this->m_objNachaReturnAddendaDetailRecord = $objNachaReturnAddendaDetailRecord;
	}

	/**
	 * Get Functions
	 */

    public function getNachaReturnFileBatch() {
        return $this->m_objNachaReturnFileBatch;
    }

    public function getNachaReturnAddendaDetailRecord() {
        return $this->m_objNachaReturnAddendaDetailRecord;
    }

	public function getCheckAccountNumber() {
		if( false == valStr( $this->getCheckAccountNumberEncrypted() ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	public function getFormattedAmount() {
		return abs( round( ( floatval( $this->m_strAmount ) / 100 ), 2 ) );
	}

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	/**
	 * Other Functions
	 */

    public function populateData( $strNachaReturnDetailRecord, $objPaymentDatabase ) {

    	$strNachaReturnDetailRecord = trim( $strNachaReturnDetailRecord );

    	if( false == isset ( $strNachaReturnDetailRecord ) || 94 != strlen( $strNachaReturnDetailRecord ) ) {
    		trigger_error( 'Nacha Return Detail Record was not the property number of characters.', E_USER_WARNING );
    		return false;
    	}

    	$this->m_strRecordTypeCode 					= substr( $strNachaReturnDetailRecord, 0, 1 );
    	$this->m_strRecordTransactionCode 			= substr( $strNachaReturnDetailRecord, 1, 2 );
    	$this->m_strReceivingDfiIdentification 		= substr( $strNachaReturnDetailRecord, 3, 8 );
    	$this->m_strCheckDigit 						= substr( $strNachaReturnDetailRecord, 11, 1 );
    	$strDfiAccountNumber 						= substr( $strNachaReturnDetailRecord, 12, 17 );
    	$this->m_strAmount 							= substr( $strNachaReturnDetailRecord, 29, 10 );
    	$this->m_strIdentificationNumber 			= substr( $strNachaReturnDetailRecord, 39, 15 );
    	$this->m_strReceivingCompanyName 			= substr( $strNachaReturnDetailRecord, 54, 22 );
    	$this->m_strDiscretionaryData 				= substr( $strNachaReturnDetailRecord, 76, 2 );
    	$this->m_strAddendaRecordIndicator 			= substr( $strNachaReturnDetailRecord, 78, 1 );
    	$this->m_strTraceNumber 					= substr( $strNachaReturnDetailRecord, 79, 15 );

    	$this->setDfiAccountNumber( $strDfiAccountNumber );
    	$this->m_strCheckRoutingNumber 	= $this->m_strReceivingDfiIdentification;
    	$this->m_intNachaReturnFileId 	= $this->m_objNachaReturnFileBatch->getNachaReturnFileId();

    	return true;
    }

    public function createNachaReturnAddendaDetailRecord( $strNachaReturnAddendaDetailRecord, $strHeaderEffectiveEntryDate, $objPaymentDatabase ) {

    	$strNachaReturnAddendaDetailRecord = trim( $strNachaReturnAddendaDetailRecord );

    	if( false == isset ( $strNachaReturnAddendaDetailRecord ) ) return false;
    	if( false == isset ( $strNachaReturnAddendaDetailRecord ) || 94 != strlen( $strNachaReturnAddendaDetailRecord ) ) {
    		trigger_error( 'Nacha Return Detail Record was not the property number of characters.', E_USER_WARNING );
    		return false;
    	}

		$this->m_objNachaReturnAddendaDetailRecord = new CNachaReturnAddendaDetailRecord();
		$this->m_objNachaReturnAddendaDetailRecord->setId( $this->m_objNachaReturnAddendaDetailRecord->fetchNextId( $objPaymentDatabase ) );
		$this->m_objNachaReturnAddendaDetailRecord->setNachaReturnDetailRecordId( $this->m_intId );
		$this->m_objNachaReturnAddendaDetailRecord->setNachaReturnDetailRecord( $this );

		if( false == $this->m_objNachaReturnAddendaDetailRecord->populateData( $strNachaReturnAddendaDetailRecord, $objPaymentDatabase ) ) {
			return false;
		}

		if( false == $this->m_objNachaReturnAddendaDetailRecord->loadOriginalNachaEntryDetailRecord( $strHeaderEffectiveEntryDate, $objPaymentDatabase ) ) {
			$objNachaReturnDetailRecord = $this;
			$objNachaReturnAddendaDetailRecord = $this->m_objNachaReturnAddendaDetailRecord;

			// Set the entry detail record type based on the transaction code.
			if( true == in_array( $objNachaReturnDetailRecord->getRecordTransactionCode(), array( '21', '31', '41', '51' ) ) ) {
				$objNachaReturnAddendaDetailRecord->setEntryDetailRecordTypeId( CEntryDetailRecordType::CREDIT );
			} else {
				$objNachaReturnAddendaDetailRecord->setEntryDetailRecordTypeId( CEntryDetailRecordType::DEBIT );
			}

			$strError = sprintf( 'Failed to find original NEDR for [%s]: NRDR #%d | NRADR #%d | NRF #%d | Trace number %s | Amount $%s (%s) | Receiving name \'%s\' | Effective entry date \'%s\'',
					( 1 == $this->m_objNachaReturnAddendaDetailRecord->getIsNoticeOfChange() ? 'NOTICE OF CHANGE ' : 'RETURN' ),
					$objNachaReturnDetailRecord->getId(),
					$objNachaReturnAddendaDetailRecord->getId(),
					$objNachaReturnDetailRecord->getNachaReturnFileId(),
					$objNachaReturnAddendaDetailRecord->getOriginalEntryTraceNumber(),
					$objNachaReturnDetailRecord->getFormattedAmount(),
					( CEntryDetailRecordType::CREDIT == $objNachaReturnAddendaDetailRecord->getEntryDetailRecordTypeId() ? 'CREDIT' : 'DEBIT' ),
					$objNachaReturnDetailRecord->getReceivingCompanyName(),
					$strHeaderEffectiveEntryDate );
			trigger_error( $strError, E_USER_WARNING );
			$objNachaReturnDetailRecord->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, 'is_unmatched_return', $strError ) );
		}

		// Call logic to popluate flag to determine whether or not a return fee should be charged
		$this->m_objNachaReturnAddendaDetailRecord->populateRequiresReturnFee( $objPaymentDatabase );

		$this->m_objNachaReturnFileBatch->setCid( $this->m_objNachaReturnAddendaDetailRecord->getCid() );

		return true;
    }

	public function insert( $intCurrentUserId, $objPaymentDatabase, $boolReturnSqlOnly = false ) {
	    $objDataset = $objPaymentDatabase->createDataset();

	    $intId = $this->getId();

	    if( true == is_null( $intId ) ) {
	        $strSql = "SELECT nextval( 'nacha_return_detail_records_id_seq'::text ) AS id";

	        if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
	            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert nacha return detail record record. The following error was reported.' ) );
	            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objPaymentDatabase->errorMsg() ) );

	            $objDataset->cleanup();
	            return false;
	        }

	        $arrValues = $objDataset->fetchArray();
	        $this->setId( $arrValues['id'] );

	        $objDataset->cleanup();
	    }

	    $strSql = 'SELECT * ' .
	                'FROM nacha_return_detail_records_insert( ' .
	                 $this->sqlId() . ', ' .
	                 $this->sqlNachaReturnFileId() . ', ' .
	                 $this->sqlNachaReturnFileBatchId() . ', ' .
	                 $this->sqlCheckAccountTypeId() . ', ' .
	                 $this->sqlCheckRoutingNumber() . ', ' .
	                 $this->sqlCheckAccountNumberEncrypted() . ', ' .
	                 $this->sqlRecordTypeCode() . ', ' .
	                 $this->sqlRecordTransactionCode() . ', ' .
	                 $this->sqlReceivingDfiIdentification() . ', ' .
	                 $this->sqlCheckDigit() . ', ' .
	                 'NULL, ' .
	                 $this->sqlAmount() . ', ' .
	                 $this->sqlIdentificationNumber() . ', ' .
	                 $this->sqlReceivingCompanyName() . ', ' .
	                 $this->sqlDiscretionaryData() . ', ' .
	                 $this->sqlAddendaRecordIndicator() . ', ' .
	                 $this->sqlTraceNumber() . ', ' .
	                 $this->sqlReturnedOn() . ', ' .
	                 $this->sqlSentToReturnsAccountOn() . ', ' .
	                 ( int ) $intCurrentUserId . ' ) AS result;';

	    if( false == $objDataset->execute( $strSql ) ) {
	        // Reset id on error
	        $this->setId( $intId );

	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert nacha return detail record record. The following error was reported.' ) );
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objPaymentDatabase->errorMsg() ) );

	        $objDataset->cleanup();

	        return false;
	    }

	    if( 0 < $objDataset->getRecordCount() ) {
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert nacha return detail record record. The following error was reported.' ) );
	        // Reset id on error
	        $this->setId( $intId );

	        while( false == $objDataset->eof() ) {
	            $arrValues = $objDataset->fetchArray();
	            $this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
	            $objDataset->next();
	        }

	        $objDataset->cleanup();

	        return false;
	    }

	    $objDataset->cleanup();

	    return true;
	}

	public function update( $intCurrentUserId, $objPaymentDatabase, $boolReturnSqlOnly = false ) {

		$objDataset = $objPaymentDatabase->createDataset();

		$strSql = 'SELECT * ' .
		            'FROM nacha_return_detail_records_update( ' .
		             $this->sqlId() . ', ' .
		             $this->sqlNachaReturnFileId() . ', ' .
		             $this->sqlNachaReturnFileBatchId() . ', ' .
		             $this->sqlCheckAccountTypeId() . ', ' .
		             $this->sqlCheckRoutingNumber() . ', ' .
		             $this->sqlCheckAccountNumberEncrypted() . ', ' .
		             $this->sqlRecordTypeCode() . ', ' .
		             $this->sqlRecordTransactionCode() . ', ' .
		             $this->sqlReceivingDfiIdentification() . ', ' .
		             $this->sqlCheckDigit() . ', ' .
		             'NULL, ' .
		             $this->sqlAmount() . ', ' .
		             $this->sqlIdentificationNumber() . ', ' .
		             $this->sqlReceivingCompanyName() . ', ' .
		             $this->sqlDiscretionaryData() . ', ' .
		             $this->sqlAddendaRecordIndicator() . ', ' .
		             $this->sqlTraceNumber() . ', ' .
		             $this->sqlReturnedOn() . ', ' .
		             $this->sqlSentToReturnsAccountOn() . ', ' .
		             ( int ) $intCurrentUserId . ' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update nacha return detail record record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objPaymentDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update nacha return detail record record. The following error was reported.' ) );
			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

		$objDataset->cleanup();

		return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function loadNachaReturnAddendaDetailRecords( $strHeaderEffectiveEntryDate, $objDatabase ) {
		$arrobjNachaReturnAddendaDetailRecords = \Psi\Eos\Payment\CNachaReturnAddendaDetailRecords::createService()->fetchNachaReturnAddendaDetailRecordsByNachaReturnDetailRecordId( $this->getId(), $objDatabase );
		if( true == valArr( $arrobjNachaReturnAddendaDetailRecords ) ) {
			foreach( $arrobjNachaReturnAddendaDetailRecords as $objNachaReturnAddendaDetailRecord ) {
				if( false != valObj( $objNachaReturnAddendaDetailRecord, 'CNachaReturnAddendaDetailRecord' ) ) {
					$objNachaReturnAddendaDetailRecord->loadOriginalNachaEntryDetailRecord( $strHeaderEffectiveEntryDate, $objDatabase );
					$this->m_objNachaReturnAddendaDetailRecord = $objNachaReturnAddendaDetailRecord;
				}
			}
		}
	}

}
?>