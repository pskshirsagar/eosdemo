<?php

class CReconciliationEntryType extends CBaseReconciliationEntryType {

	// Entry Types From Nacha File Types
	const AR_PAYMENTS									= 1;
	const COMPANY_PAYMENTS								= 2;
	const SETTLEMENT_DISTRIBUTIONS 						= 3;
	const ACH_RETURNS 									= 4;
	// const CLIENT_FUND_DISTRIBUTIONS                   = 5;
	const NACHA_RETURN_ADDENDA_DETAIL_RECORDS			= 6;
	const ACH_CHECK21_INTERMEDIARY_DISTRIBUTIONS		= 7;
	const ACH_CHECK21_RETURN_INTERMEDIARY_DEBITS		= 8;
	const WESTERN_UNION_INTERMEDIARY_CREDITS			= 9;

	// Entry Types From Eft Batch Types
	const COMPANY_PAYMENT_CREDIT_CARDS_VI_MC		= 10;
	const COMPANY_PAYMENT_CREDIT_CARDS_DISCOVER		= 11;
	const COMPANY_PAYMENT_CREDIT_CARDS_AMEX			= 12;
	const AR_PAYMENT_CREDIT_CARDS_VI_MC				= 13;
	const AR_PAYMENT_CREDIT_CARDS_DISCOVER			= 14;
	const AR_PAYMENT_CREDIT_CARDS_AMEX				= 15;

	// Entry Types From X937 File Types
	const AR_PAYMENTS_CHECK_21 						= 16;
	const COMPANY_PAYMENTS_CHECK_21 				= 17;

	// Charge Backs
	const AR_PAYMENTS_CHARGE_BACKS						= 18;

	// Entry Types From Western Union Output Files
	const WESTERN_UNION_EMONEY_ORDER					= 19;

	const ACH_ADJUSTMENT		 						= 20;
	const ECHO_ACH_DEPOSIT	 							= 21;
	const ECHO_ACH_RETURN	 					    	= 22;
	const OLD_ECHO_ACH_SETTLEMENT				    	= 23;

	const OLD_ACH_COMPANY_PAYMENT				    	= 24;
	// const OLD_ACH_COMPANY_PAYMENT_BATCH               = 25;

	const RECONCILIATION_ENTRY_MANUAL_AR_PAYMENT_X937_RETURN_OR_ADJUSTMENT  	= 25;
	const CENTRAL_NACHA_RETURN_ADDENDA_DETAIL_RECORDS							= 26;
	const ACH_CONVENIENCE_FEE_CLEARING											= 27;
	const RECONCILIATION_ENTRY_MANUAL_AR_PAYMENT_NACHA_RETURN				  	= 28;
	const ACH_CHARITY_DONATION_CLEARING											= 29;

	// Company Charge Backs
	const COMPANY_PAYMENTS_CHARGE_BACKS					= 30;
	const UNBALANCED_NACHA_FILE_BANK_BALANCING_ENTRY	= 31;
	const BAI2_BANK_RECONCILIATION_FILE					= 32;
	const CREDIT_CARD									= 33;

	// Money Gram
	const MONEY_GRAM									= 34;

	const EFT_INSTRUCTIONS								= 35;

	const WORLDPAY_RETURN								= 36;
}
?>