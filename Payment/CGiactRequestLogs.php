<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CGiactRequestLogs
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CGiactRequestLogs extends CBaseGiactRequestLogs {

	public static function fetchGiactRequestLogs( $strSql, $objDatabase ) {
		return self::fetchObjects( $strSql, CGiactRequestLog::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchGiactRequestLog( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CGiactRequestLog::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDeclinedGiactRequestLogsByCustomerIdByPropertyIdByCid( $intCustomerId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intCustomerId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) || false == valObj( $objDatabase, 'CDatabase' ) ) return false;

		$strSql = 'SELECT
						*
					FROM
						giact_request_logs
					WHERE
						account_response_code IN ( \'' . implode( '\',\'', CGiactRequestLog::$c_arrstrBlockingResponseCodes ) . '\' )
						AND product_id IN ( ' . CPsProduct::RESIDENT_PAY . ', ' . CPsProduct::RESIDENT_PORTAL . ' )
						AND details -> \'customer_id\' = \'' . $intCustomerId . '\'
						AND details -> \'propery_id\' = \'' . $intPropertyId . '\'
						AND details -> \'cid\' = \'' . $intCid . '\'
						AND request_timestamp > ( NOW() - INTERVAL \'24 hours\' )
					ORDER BY
						request_timestamp DESC';

		return self::fetchGiactRequestLogs( $strSql, $objDatabase );
	}

	public static function fetchGiactRequestLogRequestTimestampById( $intId, $objDatabase ) {
		$strSql = 'SELECT
						request_timestamp
					FROM
						giact_request_logs
					WHERE
						id = ' . ( int ) $intId;
		return parent::fetchColumn( $strSql, 'request_timestamp', $objDatabase );
	}

	public static function fetchGiactRequestLogsWithAllDetails( $objDatabase ) {
		$strSql = 'SELECT
						*,
						details -> \'cid\' as cid
					FROM
						giact_request_logs
					WHERE
						details IS NOT NULL
						AND (details ->> \'customer_id\')::int > 0
						AND (details ->> \'propery_id\')::int > 0
						AND (details ->> \'cid\')::int > 0
						AND (details ->> \'company_merchant_account_id\')::int > 0';

		return fetchData( $strSql, $objDatabase );
	}

}
?>