<?php

class CNachaFileType extends CBaseNachaFileType {

	const AR_PAYMENTS								= 1;
	const COMPANY_PAYMENTS							= 2;
	const SETTLEMENT_DISTRIBUTIONS 					= 3;
	const ACH_RETURNS 								= 4;

	// #5 Deleted
	const NACHA_RETURN_ADDENDA_DETAIL_RECORDS		= 6;
	const ACH_CHECK21_INTERMEDIARY_DISTRIBUTIONS	= 7;
	const ACH_CHECK21_RETURN_INTERMEDIARY_DEBITS	= 8;
	const WESTERN_UNION_INTERMEDIARY_CREDITS		= 9;
	const CONVENIENCE_FEE_CLEARING					= 10;
	const CHARITY_DONATION_CLEARING					= 11;
	const EFTS										= 12;
	const AP_PAYMENTS								= 13;
	const AP_DISTRIBUTIONS							= 14;
	const INSURANCE_PAYMENTS						= 15;
	const INTERNAL_TRANSFERS						= 16;

}

?>