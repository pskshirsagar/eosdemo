<?php

class CNachaReturnFileBatch extends CBaseNachaReturnFileBatch {

	protected $m_arrobjNachaReturnDetailRecords;
	protected $m_objNachaReturnFile;

    public function __construct() {
        parent::__construct();

        $this->m_arrobjNachaReturnDetailRecords = array();

        return;
    }

	/**
	 * Add Functions
	 */

	public function addNachaReturnDetailRecord( $objNachaReturnDetailRecord ) {
		$this->m_arrobjNachaReturnDetailRecords[$objNachaReturnDetailRecord->getId()] = $objNachaReturnDetailRecord;
	}

	/**
	 * Set Functions
	 */

    public function setNachaReturnFile( $objNachaReturnFile ) {
        $this->m_objNachaReturnFile = $objNachaReturnFile;
    }

	/**
	 * Get Functions
	 */

    public function getNachaReturnFile() {
        return $this->m_objNachaReturnFile;
    }

    public function getNachaReturnDetailRecords() {
        return $this->m_arrobjNachaReturnDetailRecords;
    }

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	/**
	 * Other Functions
	 */

    public function populateData( $strBatchHeaderData ) {

    	$strBatchHeaderData = trim( $strBatchHeaderData );

    	if( true == is_null( $strBatchHeaderData ) ) {
    		trigger_error( 'Header data was not set.', E_USER_WARNING );
			return false;

    	} elseif( 94 != strlen( $strBatchHeaderData ) ) {
    		trigger_error( 'Header data string length was not 94 characters.', E_USER_WARNING );
			return false;
		}

		$this->m_strRecordTypeCode 					= substr( $strBatchHeaderData, 0, 1 );
		$this->m_strServiceClassCode 				= substr( $strBatchHeaderData, 1, 3 );
		$this->m_strCompanyName 					= substr( $strBatchHeaderData, 4, 16 );
		$this->m_strCompanyDiscretionaryData 		= substr( $strBatchHeaderData, 20, 20 );
		$this->m_strCompanyIdentification			= substr( $strBatchHeaderData, 40, 10 );
		$this->m_strStandardEntryClassCode			= substr( $strBatchHeaderData, 50, 3 );
		$this->m_strCompanyEntryDescription			= substr( $strBatchHeaderData, 53, 10 );
		$this->m_strCompanyDescriptiveDate			= substr( $strBatchHeaderData, 63, 6 );
		$this->m_strEffectiveEntryDate				= substr( $strBatchHeaderData, 69, 6 );
		$this->m_strSettlementDate					= substr( $strBatchHeaderData, 75, 3 );
		$this->m_strOriginatorStatusCode			= substr( $strBatchHeaderData, 78, 1 );
		$this->m_strOriginatingDfiIdentification	= substr( $strBatchHeaderData, 79, 8 );
		$this->m_strBatchNumber						= substr( $strBatchHeaderData, 87, 7 );

		return true;
    }

    public function loadEntryDetailAndAddendaRecords( $arrstrEntryDetailAndAddendaRecords, $strHeaderEffectiveEntryDate, $objPaymentDatabase ) {
		$boolIsValid = true;

		foreach( $arrstrEntryDetailAndAddendaRecords as $arrstrEntryDetailAndAddendaRecord ) {

			if( false == ( $objNachaReturnDetailRecord = $this->createNachaReturnDetailRecord( $arrstrEntryDetailAndAddendaRecord['entry_detail_record'], $objPaymentDatabase ) ) ) {
				return false;
			}

			$boolIsValid = $objNachaReturnDetailRecord->createNachaReturnAddendaDetailRecord( $arrstrEntryDetailAndAddendaRecord['addenda_record'], $strHeaderEffectiveEntryDate, $objPaymentDatabase );

			if( false == $boolIsValid ) {
				return false;
			}

			$this->m_arrobjNachaReturnDetailRecords[] = $objNachaReturnDetailRecord;
		}

		return $boolIsValid;
    }

    public function createNachaReturnDetailRecord( $strNachaReturnDetailRecord, $objPaymentDatabase ) {

    	$strNachaReturnDetailRecord = trim( $strNachaReturnDetailRecord );
    	if ( false == isset ( $strNachaReturnDetailRecord ) ) return false;

		$objNachaReturnDetailRecord = new CNachaReturnDetailRecord();
		$objNachaReturnDetailRecord->setDefaults();
		$objNachaReturnDetailRecord->setId( $objNachaReturnDetailRecord->fetchNextId( $objPaymentDatabase ) );
		$objNachaReturnDetailRecord->setNachaReturnFileBatchId( $this->m_intId );
		$objNachaReturnDetailRecord->setNachaReturnFileBatch( $this );

		if( false == $objNachaReturnDetailRecord->populateData( $strNachaReturnDetailRecord, $objPaymentDatabase ) ) {
			return false;
		}

		return $objNachaReturnDetailRecord;
    }

	public function loadNachaReturnDetailRecords( $strHeaderEffectiveEntryDate, $objPaymentDatabase ) {
		$arrobjNachaReturnDetailRecords = \Psi\Eos\Payment\CNachaReturnDetailRecords::createService()->fetchNachaReturnDetailRecordsByNachaReturnFileBatchId( $this->getId(), $objPaymentDatabase );
		if( true == valArr( $arrobjNachaReturnDetailRecords ) ) {
			foreach( $arrobjNachaReturnDetailRecords as $objNachaReturnDetailRecord ) {
				if( false != valObj( $objNachaReturnDetailRecord, 'CNachaReturnDetailRecord' ) ) {
					$objNachaReturnDetailRecord->loadNachaReturnAddendaDetailRecords( $strHeaderEffectiveEntryDate, $objPaymentDatabase );
					$this->addNachaReturnDetailRecord( $objNachaReturnDetailRecord );
				}
			}
		}
	}

	public function unsetNachaReturnDetailRecord( $intNachaReturnDetailRecordId ) {
		$arrobjNachaReturnDetailRecords = rekeyObjects( 'id', $this->m_arrobjNachaReturnDetailRecords );

		if( true == array_key_exists( $intNachaReturnDetailRecordId, $arrobjNachaReturnDetailRecords ) ) {
			unset( $arrobjNachaReturnDetailRecords[$intNachaReturnDetailRecordId] );
			$this->m_arrobjNachaReturnDetailRecords = $arrobjNachaReturnDetailRecords;
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $this->m_arrobjNachaReturnDetailRecords ) ) {
			return true;
		}

		return false;
	}

}
?>