<?php

class CReconciliationEntry extends CBaseReconciliationEntry {

	protected $m_intCid;
	protected $m_objClient;
	protected $m_objReturnType;
	protected $m_strGatewayUsername;
	protected $m_strReconciliationEntryTypeName;
	protected $m_strPropertyName;

   /**
    * Get Functions
    */

	public function getCid() {
		return $this->m_intCid;
	}

	public function getClient() {
		return $this->m_objClient;
	}

	public function getReturnType() {
		return $this->m_objReturnType;
	}

	public function getGatewayUsername() {
		return $this->m_strGatewayUsername;
	}

	public function getReconciliationEntryTypeName() {
		return $this->m_strReconciliationEntryTypeName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	/**
	 * Set Functions
	 */

	public function setCid( $intCid ) {
        $this->m_intCid = $intCid;
	}

	public function setClient( $objClient ) {
        $this->m_objClient = $objClient;
	}

	public function setReturnType( $objReturnType ) {
        $this->m_objReturnType = $objReturnType;
	}

	public function setGatewayUsername( $strGatewayUsername ) {
        $this->m_strGatewayUsername = $strGatewayUsername;
	}

    public function setReconciliationEntryTypeName( $strReconciliationEntryTypeName ) {
    	$this->m_strReconciliationEntryTypeName = $strReconciliationEntryTypeName;
    }

    public function setPropertyName( $strPropertyName ) {
    	$this->m_strPropertyName = $strPropertyName;
    }

    public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

    	parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

    	if( isset( $arrstrValues['reconciliation_entry_type_name'] ) && $boolDirectSet ) {
			$this->m_strReconciliationEntryTypeName = trim( $arrstrValues['reconciliation_entry_type_name'] );
		} elseif( isset( $arrstrValues['reconciliation_entry_type_name'] ) ) {
			$this->setReconciliationEntryTypeName( $arrstrValues['reconciliation_entry_type_name'] );
		}

		if( true == isset( $arrstrValues['cid'] ) ) {
			$this->setCid( $arrstrValues['cid'] );
		}

        return;
    }

	/**
	 * Validate Functions
	 */

    public function valProcessingBankAccountId() {
        $boolIsValid = true;

        if( true == is_null( $this->getProcessingBankAccountId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_bank_account_id', 'Processing bank account required.' ) );
        }

        return $boolIsValid;
    }

    public function valReconciliationId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getReconciliationId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reconciliation_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valReconciliationEntryTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getReconciliationEntryTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reconciliation_entry_type_id', 'Reconciliation entry type required. ' ) );
        }

        return $boolIsValid;
    }

    public function valTotal() {
        $boolIsValid = true;

        if( true == is_null( $this->getTotal() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total', 'Amount required.' ) );
        }

        // if( false == is_null( $this->getTotal()) && 0 == ( float )$this->getTotal() ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total', 'Amount cannot be zero' ));
        // }

        return $boolIsValid;
    }

	public function valMovementDatetime() {

		$boolIsValid = true;
		if( true == is_null( $this->getMovementDatetime() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'movement_date_time', 'Date required.' ) );
		}
		return $boolIsValid;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

		switch( $strAction ) {
		    case VALIDATE_INSERT:
		    case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valProcessingBankAccountId();
            	$boolIsValid &= $this->valReconciliationEntryTypeId();
            	$boolIsValid &= $this->valTotal();
            	$boolIsValid &= $this->valMovementDatetime();
            	break;

		    case VALIDATE_DELETE:
		        break;

		    default:
		    	$boolIsValid = true;
		}

        return $boolIsValid;
    }

	public function loadDataFromBai2FileLineData( $arrstrLineData, $objPaymentDatabase ) {

		// Load total

		if( false == valArr( $arrstrLineData ) ) {
			trigger_error( 'Bai2 Line data is null', E_USER_ERROR );
			exit;
		}

		$intTypeCodeArrayIndex 			= 1;
		$intAmountArrayIndex			= 2;
		$strAmount 						= $arrstrLineData[$intAmountArrayIndex] / 100;
		$intTypeCode					= $arrstrLineData[$intTypeCodeArrayIndex];

		// This tells us that the amount in this line item is positive
		$intPositiveAmountTypeCodeLimit	= 399;

		if( $intPositiveAmountTypeCodeLimit < $intTypeCode ) {
			$strAmount = - ( $strAmount );
		}

		$this->setTotal( $strAmount );

		$strFundsType  = $arrstrLineData[$intFundsTypePosition = 3];

		//  0 = immediate availability (zero).
		// 	1 = one-day availability.
		// 	2 = two-or-more days availability.
		// 	S = distributed availability.
		// 	V = value dated.
		// 	D = distributed availability.
		// 	Z = unknown (default).

		// 	If funds type = S, the next three fields are immediate availability
		// 	amount, one-day availability amount, and more than one-day
		// 	availability amount.  See section "Funds Type."

		// 	If funds type = V, the next two fields are value date (YYMMDD) 7
		// 	and value time in military format (2400).

		// 	If funds type = D, the next field indicates the number of availability
		// 	distributions.  Multiplying the number of distrubtions by 2 gives the additional fields in the array.

		switch( $strFundsType ) {
		    case 'V':
		    	$intExtraFields = 2;
		        break;

		    case 'D':
				$intExtraFields = 1 + ( $arrstrLineData[$intNumberOfDistributionsPosition = 4] * 2 );
		        break;

		    case 'S':
		    	$intExtraFields = 3;
		        break;

		    default:
		    	$intExtraFields = 1;
		        break;
		}

		$strCustomerReference = $arrstrLineData[$intStandardCustomerReferencePosition = ( 5 + $intExtraFields )];
		$this->setTransactionDetails( $strCustomerReference );

		if( false == isset( $arrstrLineData[$intStandardCustomerReferencePosition = ( 6 + $intExtraFields )] ) || '/' == $arrstrLineData[$intStandardCustomerReferencePosition = ( 5 + $intExtraFields )] ) {
			// if there is no memo, it's either a Fifth Third BAI entry, or or a Zions entry where the trace number is not needed.
			$this->setMemo( NULL );
			$this->setTraceNumber( NULL );

			// In a Fifth Third BAII file, the customer reference is formatted as follows:
			// <companyname> <discretionary data> <entry desc> <NEDR ID> <NEDR Desc> <entry date>
			// Prop Solutions 9688883 SetlD istro 31095439 PBA 22 Debit 102814
			$strDiscretionaryDataRegex = '/^[A-Za-z\s]+\s[\d]+\s[A-Za-z\s]+\s([\d]+)+\s.*$/i';
			// Fifth Third returns have the following format
			// RETURN SETTLE RETURN <nedr id> <entry date>
			// RETURN SETTLE RETURN 31370356 110414
			$strSettlementReturnRegex = '/^[A-za-z\s]+\s([\d]+)\s[\d]+.*$/i';

			$boolDiscretionaryResult = preg_match( $strDiscretionaryDataRegex, $strCustomerReference, $arrstrMatches );
			$boolReturnResult = preg_match( $strSettlementReturnRegex, $strCustomerReference, $arrstrReturnMatches );

			if( '/' == $strCustomerReference || ( false == $boolDiscretionaryResult && false == $boolReturnResult ) || ( false == isset( $arrstrMatches[1] ) && false == isset( $arrstrReturnMatches[1] ) ) ) {
				return;
			}

			if( false != $boolDiscretionaryResult ) {
				$intNachaEntryDetailRecordId = $arrstrMatches[1];
			} elseif( false != $boolReturnResult ) {
				$intNachaEntryDetailRecordId = $arrstrReturnMatches[1];
			} else {
				return;
			}

			$this->setTraceNumber( \Psi\Eos\Payment\CNachaEntryDetailRecords::createService()->fetchTraceNumberById( $intNachaEntryDetailRecordId, $objPaymentDatabase ) );

			return;
		}

		$strMemo = 'Bank Ref: ' . $arrstrLineData[$intStandardBankReferencePosition = ( 4 + $intExtraFields )] . ' - Memo:' . $arrstrLineData[$intStandardTextReferencePosition = ( 6 + $intExtraFields )];

		$this->setMemo( $strMemo );

		// Extract the trace number

		if( false == is_null( $strCustomerReference ) ) {

			$arrstrMatches 			= NULL;
			$intTraceNumberPrefix	= NULL;
			$intTraceNumberPostfix	= NULL;
			$intTraceNumber			= NULL;

			if( false != preg_match_all( '(\d+)', $strCustomerReference, $arrintMatches, PREG_OFFSET_CAPTURE ) ) {

				foreach( $arrintMatches as $arrintMatch ) {
					foreach( $arrintMatch as $arrintTraceFields ) {
						if( 16 == $arrintTraceFields[1] ) {
							$intTraceNumberPrefix = $arrintTraceFields[0];
						} elseif( 39 == $arrintTraceFields[1] ) {
							$intTraceNumberPostfix = $arrintTraceFields[0];
						}
					}
				}
			}

			$intDefaultNumberOfCharactersToAppend = 7;
			if( strlen( $intTraceNumberPostfix ) < $intDefaultNumberOfCharactersToAppend ) {
				$intDefaultNumberOfCharactersToAppend = strlen( $intTraceNumberPostfix );
			}

			$intTraceNumberPostfix = substr( ( string ) $intTraceNumberPostfix, ( $intDefaultNumberOfCharactersToAppend * -1 ) );
			if( false == is_null( $intTraceNumberPrefix ) && 7 < strlen( $intTraceNumberPrefix ) ) {
				$intTraceNumber	= substr( $intTraceNumberPrefix, 0, 8 ) . str_pad( $intTraceNumberPostfix, 7, '0', STR_PAD_LEFT );
			}

			$this->setTraceNumber( $intTraceNumber );
		}
	}

}
?>