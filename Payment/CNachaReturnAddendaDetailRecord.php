<?php

require_once( PATH_LIBRARIES_PSI . 'Ach/CAch.defines.php' );

class CNachaReturnAddendaDetailRecord extends CBaseNachaReturnAddendaDetailRecord {

	protected $m_objReturnType;
	/** @var  CNachaEntryDetailRecord */
	protected $m_objOriginalNachaEntryDetailRecord;
	protected $m_objNachaReturnDetailRecord;
	protected $m_objClient;

	const ROUTING_NUMBER_LENGTH 		= 9;
	const ACCOUNT_NUMBER_LENGTH 		= 17;
	const TRANSACTION_CODE_LENGTH		= 2;

	const NOC_CODE_INCORRECT_ACCOUNT_NUMBER 										= 'C01';
	const NOC_CODE_INCORRECT_ROUTING_NUMBER 					 					= 'C02';
	const NOC_CODE_INCORRECT_ROUTING_NUMBER_AND_ACCOUNT_NUMBER 	 					= 'C03';
	const NOC_CODE_INCORRECT_NAME 								 					= 'C04';
	const NOC_CODE_INCORRECT_TRANSACTION_CODE 					 					= 'C05';
	const NOC_CODE_INCORRECT_ACCOUNT_NUMBER_AND_TRANSACTION_CODE 					= 'C06';
	const NOC_CODE_INCORRECT_ROUTING_NUMBER_AND_ACCOUNT_NUMBER_AND_TRANSACTION_CODE = 'C07';
	const NOC_CODE_INCORRECT_RECEIVING_BANK_ID 										= 'C08';
	const NOC_CODE_INCORRECT_INDIVIDUAL_ID_NUMBER 									= 'C09';

	const COMPANY_PAYMENT_RETURN_CHANGE_DATE = '2020-04-15 00:00:00';

	private $m_arrstrNocCodeDescriptions = array(
		self::NOC_CODE_INCORRECT_ACCOUNT_NUMBER => array(
			'The bank has informed us that this routing number and account number combination is incorrect. Please contact your bank to get the correct information.'
		),
		self::NOC_CODE_INCORRECT_ROUTING_NUMBER => array(
			'The routing number used was incorrect. The correct routing number is: %s.'
		),
		self::NOC_CODE_INCORRECT_ROUTING_NUMBER_AND_ACCOUNT_NUMBER => array(
			'The routing number used was incorrect. The correct routing number is: %s.',
			'The bank has informed us that this routing number and account number combination is incorrect. Please contact your bank to get the correct information.'
		),
		self::NOC_CODE_INCORRECT_NAME => array(
			'The name on the bank account does not match the name provided. Please contact your bank if you need assistance.'
		),
		self::NOC_CODE_INCORRECT_TRANSACTION_CODE => array(
			'The account type did not match the account number (i.e. the account number is a checking account and a savings account type was specified or vice versa).'
		),
		self::NOC_CODE_INCORRECT_ACCOUNT_NUMBER_AND_TRANSACTION_CODE => array(
			'The bank has informed us that this routing number and account number combination is incorrect. Please contact your bank to get the correct information.',
			'The account type did not match the account number (i.e. the account number is a checking account and a savings account type was specified or vice versa).'
		),
		self::NOC_CODE_INCORRECT_ROUTING_NUMBER_AND_ACCOUNT_NUMBER_AND_TRANSACTION_CODE => array(
			'The routing number used was incorrect. The correct routing number is: %s.',
			'The bank has informed us that this routing number and account number combination is incorrect. Please contact your bank to get the correct information.',
			'The account type did not match the account number (i.e. the account number is a checking account and a savings account type was specified or vice versa).'
		),
		self::NOC_CODE_INCORRECT_RECEIVING_BANK_ID => array(
			'The receiving bank account information is invalid.'
		),
		self::NOC_CODE_INCORRECT_INDIVIDUAL_ID_NUMBER => array(
			'The individual ID number is incorrect.'
		)
	);

	/**
	 * Get Functions
	 */

    public function getOriginalNachaEntryDetailRecord() {
    	return $this->m_objOriginalNachaEntryDetailRecord;
    }

    public function getNachaReturnDetailRecord() {
    	return $this->m_objNachaReturnDetailRecord;
    }

    public function getClient() {
    	return $this->m_objClient;
    }

	/**
	 * Set Functions
	 */

    public function setOriginalNachaEntryDetailRecord( $objNachaEntryDetailRecord ) {
    	$this->m_objOriginalNachaEntryDetailRecord = $objNachaEntryDetailRecord;
    }

    public function setNachaReturnDetailRecord( $objNachaReturnDetailRecord ) {
    	$this->m_objNachaReturnDetailRecord = $objNachaReturnDetailRecord;
    }

	// new setter- getter function for new field added (corrected_data_encrypted) will change the name in phase two when corrected_data field will be deleted

	public function setCorrectedDataNew( $strCorrectedData ) {
		// use the \Psi\Libraries\UtilEncryption\CEncryption package because the data is originally encrypted in PsPayments
		if( true == valStr( $strCorrectedData ) ) {
			$this->setCorrectedDataEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCorrectedData, CONFIG_SODIUM_KEY_NACHA_CORRECTED_DATA ) );
		}
	}

	public function getCorrectedDataNew() {
		// use the \Psi\Libraries\UtilEncryption\CEncryption package because the data is originally encrypted in PsPayments
		if( false == valStr( $this->getCorrectedDataEncrypted() ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCorrectedDataEncrypted(), CONFIG_SODIUM_KEY_NACHA_CORRECTED_DATA, [ 'legacy_secret_key' => CONFIG_KEY_NACHA_CORRECTED_DATA, 'mode' => MCRYPT_MODE_CBC ] );
	}

	/**
	 * Fetch Functions
	 */

    public function fetchOriginalNachaEntryDetailRecord( $objPaymentDatabase ) {
		$this->m_objOriginalNachaEntryDetailRecord = \Psi\Eos\Payment\CNachaEntryDetailRecords::createService()->fetchNachaEntryDetailRecordById( $this->m_intNachaEntryDetailRecordId, $objPaymentDatabase );
    	return $this->m_objOriginalNachaEntryDetailRecord;
    }

    public function fetchNachaReturnDetailRecord( $objPaymentDatabase ) {
		$this->m_objNachaReturnDetailRecord = \Psi\Eos\Payment\CNachaReturnDetailRecords::createService()->fetchNachaReturnDetailRecordById( $this->m_intNachaReturnDetailRecordId, $objPaymentDatabase );
    	return $this->m_objNachaReturnDetailRecord;
    }

    public function fetchClient( $objPaymentDatabase ) {
		$this->m_objClient = CClients::fetchClientById( $this->m_intCid, $objPaymentDatabase );
    	return $this->m_objClient;
    }

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	/**
	 * Other Functions
	 */

    public function hasAlreadyPosted( $objPaymentDatabase ) {

		$objNachaReturnAddendaDetailRecord = \Psi\Eos\Payment\CNachaReturnAddendaDetailRecords::createService()->fetchReturnNachaReturnAddendaDetailRecordByOriginalEntryTraceNumberByNachaEntryDetailRecordId( $this->m_strOriginalEntryTraceNumber, $this->m_intNachaEntryDetailRecordId, $objPaymentDatabase );

		if( true == valObj( $objNachaReturnAddendaDetailRecord, 'CNachaReturnAddendaDetailRecord' ) ) {
			return true;
		}

		return false;
	}

    public function processReturn( $intUserId, $objAdminDatabase, $objPaymentDatabase, $objClientDatabase, $objEmailDatabase ) {

    	if( 1 == $this->m_intIsNoticeOfChange ) return true;

    	/// NOTE: If we reach this point and we have no original NEDR loaded, return true. We don't want to fail the entire file
		//       Our handler script will send an email alert, and this will need to be dealt with manually.
    	if( false == valObj( $this->m_objOriginalNachaEntryDetailRecord, 'CNachaEntryDetailRecord' ) ) {
			$this->loadReturnTypeId( $objPaymentDatabase );
			return true;

		} else {
			if( true == in_array( $this->m_objOriginalNachaEntryDetailRecord->getNachaFileId(), array( 7397, 7396 ) ) ) {

				$objNachaReturnDetailRecord = $this->fetchNachaReturnDetailRecord( $objPaymentDatabase );

				if( true == valObj( $objNachaReturnDetailRecord, 'CNachaReturnDetailRecord' ) && 0 == strlen( trim( $objNachaReturnDetailRecord->getCheckAccountNumber() ) ) ) {
					$this->loadReturnTypeId( $objPaymentDatabase );
					return true;
				}
			}
		}

		if( true == is_null( $this->m_objOriginalNachaEntryDetailRecord->getReturnedOn() ) ) $this->m_objOriginalNachaEntryDetailRecord->setReturnedOn( date( 'Y-m-d H:i:s' ) );

		$this->m_objOriginalNachaEntryDetailRecord->setReturnTypeId( $this->m_intReturnTypeId );
		$this->m_objOriginalNachaEntryDetailRecord->setReturnType( $this->m_objReturnType );

		if( false == $this->m_objOriginalNachaEntryDetailRecord->update( $intUserId, $objPaymentDatabase ) ) {
			trigger_error( 'Nacha Entry Detail Record failed to update.', E_USER_ERROR );
			return false;
		}

    	if( false == $this->m_objOriginalNachaEntryDetailRecord->processReturn( $intUserId, $this, $objAdminDatabase, $objPaymentDatabase, $objClientDatabase, $objEmailDatabase ) ) {
			$this->addErrorMsgs( $this->m_objOriginalNachaEntryDetailRecord->getErrorMsgs() );
			return false;
		}

		return true;
    }

    public function processNoticeOfChange( $intUserId, $objAdminDatabase, $objPaymentDatabase, $objClientDatabase, $objEmailDatabase ) {

    	if( 1 != $this->m_intIsNoticeOfChange ) return true;

    	if( true == is_null( $this->getChangedOn() ) ) {

			$this->setChangedOn( date( 'Y-m-d H:i:s' ) );

			if( false == $this->update( $intUserId, $objPaymentDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'Nacha Return Addenda Detail Record failed to update.' ) );
				return false;
			}

			// NOTE: If we reach this point and we have no original NEDR loaded, return true. We don't want to fail the entire file
			//       Our handler script will send an email alert, and this will need to be dealt with manually.
			if( false == valObj( $this->m_objOriginalNachaEntryDetailRecord, 'CNachaEntryDetailRecord' ) ) {
				return true;
			}

			$this->m_objOriginalNachaEntryDetailRecord->setReturnTypeId( $this->m_intReturnTypeId );
			$this->m_objOriginalNachaEntryDetailRecord->setReturnType( $this->m_objReturnType );

	    	if( false == $this->m_objOriginalNachaEntryDetailRecord->processNoticeOfChange( $intUserId, $this, $objAdminDatabase, $objPaymentDatabase, $objClientDatabase, $objEmailDatabase ) ) {
				$this->addErrorMsgs( $this->m_objOriginalNachaEntryDetailRecord->getErrorMsgs() );
				return false;
			} else {
				$this->addErrorMsgs( $this->m_objOriginalNachaEntryDetailRecord->getErrorMsgs() );
			}
    	}

		return true;
    }

    public function loadReturnTypeId( $objPaymentDatabase ) {

    	$this->m_objReturnType = CReturnTypes::fetchReturnTypeByCode( $this->m_strReturnReasonCode, $objPaymentDatabase );

    	if( true == valObj( $this->m_objReturnType, 'CReturnType' ) ) {
			$this->m_intReturnTypeId = $this->m_objReturnType->getId();

    	} else {
    		trigger_error( 'Return type failed to load.', E_USER_ERROR );
    	}

    	return $this->m_objReturnType;
    }

    // ***********************************************************************************************************
    // This is the key query used to identify the original transaction that has come back
    // returned. All automated return processing hinges on this function locating the original NEDR.
    // ***********************************************************************************************************

    public function loadOriginalNachaEntryDetailRecord( $strHeaderEffectiveEntryDate, $objPaymentDatabase ) {

    	$this->m_objOriginalNachaEntryDetailRecord = \Psi\Eos\Payment\CNachaEntryDetailRecords::createService()->fetchNachaEntryDetailRecordByTraceNumberByHeaderEffectiveEntryDate( $this->m_strOriginalEntryTraceNumber, $strHeaderEffectiveEntryDate, $objPaymentDatabase );

    	if( true == valObj( $this->m_objOriginalNachaEntryDetailRecord, 'CNachaEntryDetailRecord' ) ) {
			$this->mapOriginalNachaEntryDetailRecordData();
    	} else {
    		return false;
    	}

    	return $this->m_objOriginalNachaEntryDetailRecord;
    }

    public function mapOriginalNachaEntryDetailRecordData() {

		if( true == valObj( $this->m_objOriginalNachaEntryDetailRecord, 'CNachaEntryDetailRecord' ) ) {

			$this->m_intCid 		= $this->m_objOriginalNachaEntryDetailRecord->getCid();
			$this->m_intArPaymentId 				= $this->m_objOriginalNachaEntryDetailRecord->getArPaymentId();
			$this->m_intSettlementDistributionId 	= $this->m_objOriginalNachaEntryDetailRecord->getSettlementDistributionId();
			$this->m_intCompanyPaymentId 			= $this->m_objOriginalNachaEntryDetailRecord->getCompanyPaymentId();
			$this->m_intClearingBatchId 			= $this->m_objOriginalNachaEntryDetailRecord->getClearingBatchId();
			$this->m_intEftInstructionId			= $this->m_objOriginalNachaEntryDetailRecord->getEftInstructionId();
			$this->m_intNachaEntryDetailRecordId 	= $this->m_objOriginalNachaEntryDetailRecord->getId();
			$this->m_intNachaFileBatchId 			= $this->m_objOriginalNachaEntryDetailRecord->getNachaFileBatchId();
			$this->m_intNachaFileId 				= $this->m_objOriginalNachaEntryDetailRecord->getNachaFileId();
			$this->m_intEntryDetailRecordTypeId		= $this->m_objOriginalNachaEntryDetailRecord->getEntryDetailRecordTypeId();
    	}
    }

    public function populateData( $strNachaReturnAddendaDetailRecord, $objPaymentDatabase ) {

    	$strNachaReturnAddendaDetailRecord = trim( $strNachaReturnAddendaDetailRecord );

    	if( false == isset ( $strNachaReturnAddendaDetailRecord ) || 94 != strlen( $strNachaReturnAddendaDetailRecord ) ) {
    		trigger_error( 'Nacha Return Detail Addenda Record was not the property number of characters.', E_USER_WARNING );
    		return false;
    	}

    	$this->m_strRecordTypeCode 					= substr( $strNachaReturnAddendaDetailRecord, 0, 1 );
    	$this->m_strTypeCode 						= substr( $strNachaReturnAddendaDetailRecord, 1, 2 );
    	$this->m_strReturnReasonCode 				= substr( $strNachaReturnAddendaDetailRecord, 3, 3 );
    	$this->m_strOriginalEntryTraceNumber 		= substr( $strNachaReturnAddendaDetailRecord, 6, 15 );
    	$this->m_strDateOfDeath 					= substr( $strNachaReturnAddendaDetailRecord, 21, 6 );
    	$this->m_strReceivingDfiIdentification 		= substr( $strNachaReturnAddendaDetailRecord, 27, 8 );
    	$this->m_strTraceNumber 					= substr( $strNachaReturnAddendaDetailRecord, 79, 15 );

		if( 'COR' != $this->m_objNachaReturnDetailRecord->getNachaReturnFileBatch()->getStandardEntryClassCode() ) {
			// This information gets set if this is a Return
			$this->m_strInformation = substr( $strNachaReturnAddendaDetailRecord, 35, 44 );

		} else {
			// This information gets set if it is a Notice of Change.
			$this->m_strCorrectedData 				= substr( $strNachaReturnAddendaDetailRecord, 35, 29 );
			// The corrected data should be saved in encrypted format
			$this->setCorrectedDataNew( substr( $strNachaReturnAddendaDetailRecord, 35, 29 ) );

			$this->m_strReserved 					= substr( $strNachaReturnAddendaDetailRecord, 64, 15 );
			$this->m_intIsNoticeOfChange			= 1;
		}

    	$this->loadReturnTypeId( $objPaymentDatabase );

    	return true;
    }

	public function populateRequiresReturnFee( $objPaymentDatabase ) {

		// Default return fee requirement to off
		$this->setRequiresReturnFee( 0 );

		// ******************************************************************************************************
		// The following rules need to all be true in order for a return to be eligible for generating return fees:
		//	* Return must be for a valid ar_payment (NOTE: This will automatically disqualify returns on return fee items since they're company payments)
		//  * This must be the FIRST return for this ar_payment
		//	* Payment type must be ACH
		//  * Do not charge NSF fee when reversal is already initiated on payment
		//  * Return type must be in list of eligible types
		//	* Merchant account's resident return fee must be > $0
		// ******************************************************************************************************

		$arrintEligiblePaymentTypeIds = array( CPaymentType::ACH );
    	$arrintEligibleReturnTypeIds = array( CReturnType::INSUFFICIENT_FUNDS, CReturnType::NSF_UNCOLLECTED );
    	$objCompanyMerchantAccount = NULL;

    	switch( NULL ) {
    		default:

				if( true == is_null( $this->getArPaymentId() ) || 0 >= $this->getArPaymentId() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'You can only apply return fees to Ar Payments.', NULL ) );
					break;
				}

				$objArPayment = CArPayments::fetchArPaymentByIdByCid( $this->getArPaymentId(), $this->getCid(), $objPaymentDatabase );

		    	if( false == valObj( $objArPayment, 'CArPayment' ) ) {
		    		$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to load Ar Payment object from database.', NULL ) );
					break;
				}

				$arrobjNachaReturnAddendaDetailRecords = \Psi\Eos\Payment\CNachaReturnAddendaDetailRecords::createService()->fetchNachaReturnAddendaDetailRecordsByArPaymentId( $this->getArPaymentId(), $objPaymentDatabase );

		    	// the current object already exists in the database so check for more than 1 result to see if there are pre-existing returns
				if( false == is_null( $arrobjNachaReturnAddendaDetailRecords ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrobjNachaReturnAddendaDetailRecords ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'This is not the first return for this ar_payment.', NULL ) );
					break;
				}

				if( false == in_array( $objArPayment->getPaymentTypeId(), $arrintEligiblePaymentTypeIds ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'You can only apply return fees for ACH transactions.', NULL ) );
					break;
				}
				if( true == valObj( $objArPayment, 'CArPayment' ) && NULL != $objArPayment->getArPaymentId() ) {
					$objAssociatedArPayment = CArPayments::fetchArPaymentByIdByCid( $objArPayment->getArPaymentId(), $this->getCid(), $objPaymentDatabase );
					if( true == valObj( $objAssociatedArPayment, 'CArPayment' ) ) {
						if( CPaymentStatusType::REVERSED == $objAssociatedArPayment->getPaymentStatusTypeId() || 1 == $objArPayment->getIsReversed() ) {
							$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Do not charge NSF fee when reversal is already initiated on payment.', NULL ) );
							break;
						}
					}
				}

				$objCompanyMerchantAccount = $objArPayment->getOrFetchCompanyMerchantAccount( $objPaymentDatabase );

				if( false == valObj( $objCompanyMerchantAccount, 'CCompanyMerchantAccount' ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to load Merchant Account object from database.', NULL ) );
					break;
				}

		    	if( false == in_array( $this->getReturnTypeId(), $arrintEligibleReturnTypeIds, true ) ) {
		    		$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Return type is ineligible for applying return fees: ' . $this->getReturnTypeId(), NULL ) );
					break;
				}

				if( true == is_null( $objCompanyMerchantAccount->getResidentReturnFee( $objArPayment->getPaymentTypeId() ) ) || 0 >= $objCompanyMerchantAccount->getResidentReturnFee( $objArPayment->getPaymentTypeId() ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Resident return fee must be greater than $0.', NULL ) );
					break;
				}

				$this->setRequiresReturnFee( 1 );
    	}
    }

	public function createReconciliationEntry( $objPaymentDatabase ) {

		$objReconciliationEntry = new CReconciliationEntry();
		$objReconciliationEntry->setDefaults();
		$objReconciliationEntry->setNachaReturnAddendaDetailRecordId( $this->getId() );
		$objReconciliationEntry->setReconciliationEntryTypeId( CReconciliationEntryType::CENTRAL_NACHA_RETURN_ADDENDA_DETAIL_RECORDS );
		$objReconciliationEntry->setTraceNumber( $this->getOriginalEntryTraceNumber() );

		// We need to find out the amount of the original return.
		$objNachaEntryDetailRecord = $this->fetchOriginalNachaEntryDetailRecord( $objPaymentDatabase );

		// If original NEDR cannot be loaded, fetch the NRDR for creating rec entry
		if( false == valObj( $objNachaEntryDetailRecord, 'CNachaEntryDetailRecord' ) ) {

			$objNachaReturnDetailRecord = $this->fetchNachaReturnDetailRecord( $objPaymentDatabase );

			if( false == valObj( $objNachaReturnDetailRecord, 'CNachaReturnDetailRecord' ) ) {
				trigger_error( 'Nacha Return Detail Record could not be loaded for this addenda record #' . $this->getId(), E_USER_WARNING );
				return false;
			}

			if( CEntryDetailRecordType::CREDIT == $this->getEntryDetailRecordTypeId() ) {
				$objReconciliationEntry->setTotal( $objNachaReturnDetailRecord->getFormattedAmount() );

			} elseif( CEntryDetailRecordType::DEBIT == $this->getEntryDetailRecordTypeId() ) {
				$objReconciliationEntry->setTotal( ( -1 * abs( $objNachaReturnDetailRecord->getFormattedAmount() ) ) );
			}

			$objReconciliationEntry->setMemo( 'Original NEDR not found' );
		} else {
			if( CEntryDetailRecordType::CREDIT == $objNachaEntryDetailRecord->getEntryDetailRecordTypeId() ) {
				$objReconciliationEntry->setTotal( $objNachaEntryDetailRecord->getFormattedAmount() );

			} elseif( CEntryDetailRecordType::DEBIT == $objNachaEntryDetailRecord->getEntryDetailRecordTypeId() ) {
				$objReconciliationEntry->setTotal( ( -1 * abs( $objNachaEntryDetailRecord->getFormattedAmount() ) ) );
			}
		}

		// Make sure that the return date is not in the common.transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.

		$intCreatedOn = strtotime( $this->getCreatedOn() );

		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y', $intCreatedOn ), date( 'm/d/Y', ( $intCreatedOn + ( 21 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = array();

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intCreatedOn = strtotime( '+1 day', $intCreatedOn );

		while( 'Sun' == date( 'D', $intCreatedOn ) || 'Sat' == date( 'D', $intCreatedOn ) || true == in_array( $intCreatedOn, $arrintProhibitedTransactionHolidays ) ) {
			$intCreatedOn = strtotime( '+1 day', $intCreatedOn );
		}

		$objReconciliationEntry->setMovementDatetime( date( 'm/d/Y', $intCreatedOn ) );

		return $objReconciliationEntry;
	}

	public function getCorrectedAccountNumber( $strNocCode = NULL, $strCorrectedData = NULL ) {

		if( is_null( $strNocCode ) && false == $this->getIsNoticeOfChange() ) {
			return '';
		}

		if( is_null( $strNocCode ) ) {
			$strNocCode = $this->getReturnReasonCode();
		}

		if( is_null( $strCorrectedData ) ) {
			$strCorrectedData = $this->getCorrectedDataNew();
		}

		if( is_null( $strNocCode ) || is_null( $strCorrectedData ) ) {
			return '';
		}

		$strCorrectedAccountNumber = '';

		switch( $strNocCode ) {
			case self::NOC_CODE_INCORRECT_ACCOUNT_NUMBER:
			case self::NOC_CODE_INCORRECT_ACCOUNT_NUMBER_AND_TRANSACTION_CODE:
				// Correct DFI account number appears in the first 17 positions of the corrected data field
				$strCorrectedAccountNumber = substr( $strCorrectedData, 0, self::ACCOUNT_NUMBER_LENGTH );
				break;

			case self::NOC_CODE_INCORRECT_ROUTING_NUMBER_AND_ACCOUNT_NUMBER:
				// Correct DFI account number appears in the 13th through the 29th positions
				$strCorrectedAccountNumber = substr( $strCorrectedData, 12, self::ACCOUNT_NUMBER_LENGTH );
				break;

			case self::NOC_CODE_INCORRECT_ROUTING_NUMBER_AND_ACCOUNT_NUMBER_AND_TRANSACTION_CODE:
				// Correct DFI account number appears in the 10th through the 26th positions
				$strCorrectedAccountNumber = substr( $strCorrectedData, 9, self::ACCOUNT_NUMBER_LENGTH );
				break;

			default:
				// Account number does not appear in corrected data
				break;
		}
		return trim( $strCorrectedAccountNumber );
	}

	public function getCorrectedRoutingNumber( $strNocCode = NULL, $strCorrectedData = NULL ) {

		if( is_null( $strNocCode ) && false == $this->getIsNoticeOfChange() ) {
			return '';
		}

		if( is_null( $strNocCode ) ) {
			$strNocCode = $this->getReturnReasonCode();
		}

		if( is_null( $strCorrectedData ) ) {
			$strCorrectedData = $this->getCorrectedDataNew();
		}

		if( is_null( $strNocCode ) || is_null( $strCorrectedData ) ) {
			return '';
		}

		$strCorrectRoutingNumber = '';
		switch( $strNocCode ) {
			case self::NOC_CODE_INCORRECT_ROUTING_NUMBER:
			case self::NOC_CODE_INCORRECT_ROUTING_NUMBER_AND_ACCOUNT_NUMBER:
			case self::NOC_CODE_INCORRECT_ROUTING_NUMBER_AND_ACCOUNT_NUMBER_AND_TRANSACTION_CODE:
				// Correct Routing Number (including Check Digit) appears in first nine positions
				// of the Corrected Data Field
				if( strlen( $strCorrectedData ) >= self::ROUTING_NUMBER_LENGTH ) {
					$strCorrectRoutingNumber = substr( $strCorrectedData, 0, self::ROUTING_NUMBER_LENGTH );
				}
				break;

			default:
				// Nothing to correct
				break;
		}

		return $strCorrectRoutingNumber;
	}

	public function getCorrectedTransactionCode( $strNocCode = NULL, $strCorrectedData = NULL ) {
		if( is_null( $strNocCode ) && false == $this->getIsNoticeOfChange() ) {
			return '';
		}

		if( is_null( $strNocCode ) ) {
			$strNocCode = $this->getReturnReasonCode();
		}

		if( is_null( $strCorrectedData ) ) {
			$strCorrectedData = $this->getCorrectedDataNew();
		}

		if( is_null( $strNocCode ) || is_null( $strCorrectedData ) ) {
			return '';
		}

		$strCorrectTransactionCode = '';
		switch( $strNocCode ) {
			case self::NOC_CODE_INCORRECT_TRANSACTION_CODE:
				// Correct Transaction Code appears in first two positions
				$strCorrectTransactionCode = substr( $strCorrectedData, 0, self::TRANSACTION_CODE_LENGTH );
				break;

			case self::NOC_CODE_INCORRECT_ACCOUNT_NUMBER_AND_TRANSACTION_CODE:
				// Correct Transaction Code appears in the 21st and 22nd positions
				if( 21 < strlen( $strCorrectedData ) ) {
					$strCorrectTransactionCode = substr( $strCorrectedData, 20, self::TRANSACTION_CODE_LENGTH );
				}
				break;

			case self::NOC_CODE_INCORRECT_ROUTING_NUMBER_AND_ACCOUNT_NUMBER_AND_TRANSACTION_CODE:
				// Correct Transaction Code appears in the 27th and 28th positions

				if( 27 < strlen( $strCorrectedData ) ) {
					$strCorrectTransactionCode = substr( $strCorrectedData, 26, self::TRANSACTION_CODE_LENGTH );
				}
				break;

			default:
				// Incorrect code
				break;
		}

		return $strCorrectTransactionCode;
	}

	public function getCorrectedAccountTypeId( $strNocCode = NULL, $strCorrectedData = NULL ) {

		$strCorrectTransactionCode = $this->getCorrectedTransactionCode( $strNocCode, $strCorrectedData );

		$intCorrectAccountTypeId = 0;
		switch( trim( ( string ) $strCorrectTransactionCode ) ) {
			case ACH_TRANSACTION_CODE_DEBIT_DDA_PAYMENT:
			case ACH_TRANSACTION_CODE_CREDIT_DDA_DEPOSIT:
				$intCorrectAccountTypeId = CCheckAccountType::PERSONAL_CHECKING;
				break;

			case ACH_TRANSACTION_CODE_DEBIT_SAVINGS_PAYMENT:
			case ACH_TRANSACTION_CODE_CREDIT_SAVINGS_DEPOSIT:
				$intCorrectAccountTypeId = CCheckAccountType::PERSONAL_SAVINGS;
				break;

			default:
				// Unhandled transaction code
				break;
		}

		return $intCorrectAccountTypeId;
	}

	public function getNocCodeDescription( $strNocCode = NULL, $strCorrectedData = NULL ) {

		if( is_null( $strNocCode ) && false == $this->getIsNoticeOfChange() ) {
			return '';
		}

		if( is_null( $strNocCode ) ) {
			$strNocCode = $this->getReturnReasonCode();
		}

		if( is_null( $strCorrectedData ) ) {
			$strCorrectedData = $this->getCorrectedDataNew();
		}

		if( is_null( $strNocCode ) || is_null( $strCorrectedData ) ) {
			return '';
		}

		$strDescription = '';
		if( array_key_exists( $strNocCode, $this->m_arrstrNocCodeDescriptions ) ) {
			$strDescription = implode( ' ', $this->m_arrstrNocCodeDescriptions[$strNocCode] );
		}

		$strCorrectRoutingNumber = $this->getCorrectedRoutingNumber( $strNocCode, $strCorrectedData );

		$strDescription = sprintf( $strDescription, $strCorrectRoutingNumber );

		return $strDescription;
	}

	public function getNocCodeDescriptions( $strNocCode = NULL, $strCorrectedData = NULL ) {

		if( is_null( $strNocCode ) && false == $this->getIsNoticeOfChange() ) {
			return '';
		}

		if( is_null( $strNocCode ) ) {
			$strNocCode = $this->getReturnReasonCode();
		}

		if( is_null( $strCorrectedData ) ) {
			$strCorrectedData = $this->getCorrectedDataNew();
		}

		if( is_null( $strNocCode ) || is_null( $strCorrectedData ) ) {
			return '';
		}

		$strCorrectRoutingNumber = $this->getCorrectedRoutingNumber( $strNocCode, $strCorrectedData );

		$arrstrDescriptions = array();
		if( array_key_exists( $strNocCode, $this->m_arrstrNocCodeDescriptions ) ) {
			foreach( $this->m_arrstrNocCodeDescriptions[$strNocCode] as $strDescription ) {
				$arrstrDescriptions[] = sprintf( $strDescription, $strCorrectRoutingNumber );
			}
		}

		return $arrstrDescriptions;
	}

	public function addErrorMsgs( $arrobjErrorMsgs ) {
		if( true == valArr( $arrobjErrorMsgs ) ) {
			foreach( $arrobjErrorMsgs as $objErrorMsg ) {
				if( true == valObj( $objErrorMsg, 'CErrorMsg' ) ) {
					$this->addErrorMsg( $objErrorMsg );
				}
			}
		}
	}

}

?>