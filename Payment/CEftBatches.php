<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CEftBatches
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CEftBatches extends CBaseEftBatches {

    const EFT_TYPE_COMPANY_PAYMENT = 3;

	public static function fetchEftBatchByX937FileId( $intX937FileId, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM eft_batches WHERE x937_file_id = ' . ( int ) $intX937FileId;
		return self::fetchEftBatch( $strSql, $objPaymentDatabase );
	}

    /**
     * This would normally be handled by the EFT system,
     * however, we pulled this method in to perform direct lookups
     * temporarily
     *
     * @param int[] $arrintCompanyPaymentIds
     * @param \Psi\Libraries\Db\CDatabase $objPaymentDatabase
     * @return mixed[]
     */
    public function fetchEftBatchDetailsByCompanyPaymentIds( $arrintCompanyPaymentIds, $objPaymentDatabase ) {
        if ( false == \Psi\Libraries\UtilFunctions\valArr( $arrintCompanyPaymentIds ) ) {
            return [];
        }

        $strSql = 'SELECT eb.batch_amount, eb.id AS eft_batch_id, eb.batch_datetime, ei.id AS eft_instruction_id, e.remote_payment_number AS company_payment_id
					FROM eft_batches eb
					JOIN eft_instructions ei ON ei.eft_batch_id = eb.id
					JOIN efts e ON e.id = ei.eft_id
					WHERE
						e.eft_type_id = ' . self::EFT_TYPE_COMPANY_PAYMENT . '
						AND e.remote_payment_number IN ( \'' . implode("','", $arrintCompanyPaymentIds) . '\')
						AND ei.amount < 0';

        $arrmixResults = fetchData( $strSql, $objPaymentDatabase );

        if ( false == \Psi\Libraries\UtilFunctions\valArr( $arrmixResults ) ) {
            return [];
        }

        return \Psi\Libraries\UtilArray\CArray::createService()->rekeyArray( 'company_payment_id', $arrmixResults );
    }

    /**
     * This would normally be handled by the EFT system,
     * however, we pulled this method in to perform direct lookups
     * temporarily
     *
     * @param int[] $arrintCompanyPaymentIds
     * @param \Psi\Libraries\Db\CDatabase $objPaymentDatabase
     * @return mixed[]
     */
    public function fetchEftBatchDetailsByReversalCompanyPaymentIds( $arrintCompanyPaymentIds, $objPaymentDatabase ) {
        if( false == \Psi\Libraries\UtilFunctions\valArr( $arrintCompanyPaymentIds ) ) {
            return [];
        }

        $strSql = 'SELECT eb.batch_amount, eb.id AS eft_batch_id, eb.batch_datetime, ei.id AS eft_instruction_id, e.remote_reference_number_3 AS company_payment_id
					FROM eft_batches eb
					JOIN eft_instructions ei ON ei.eft_batch_id = eb.id
					JOIN efts e ON e.id = ei.eft_id
					WHERE
						e.eft_type_id = ' . self::EFT_TYPE_COMPANY_PAYMENT . '
						AND e.remote_reference_number_3 IN ( \'' . implode( "','", $arrintCompanyPaymentIds ) . '\')
						AND ei.amount > 0';

        $arrmixResults = fetchData( $strSql, $objPaymentDatabase );

        if( false == \Psi\Libraries\UtilFunctions\valArr( $arrmixResults ) ) {
            return [];
        }

        return \Psi\Libraries\UtilArray\CArray::createService()->rekeyArray( 'company_payment_id', $arrmixResults );
    }

	public static function fetchUnReconPreppedEftBatches( $objPaymentDatabase ) {

		$strSql = 'SELECT
							eb.*
						FROM
							eft_batches eb
						WHERE
							eb.is_recon_prepped <> 1
							AND eb.aggregate_eft_batch_id IS NULL
							--AND date_trunc( \'day\', eb.created_on ) < date_trunc( \'day\', NOW() )
						ORDER BY
							eb.batch_amount DESC
					LIMIT 10000';

		return self::fetchEftBatches( $strSql, $objPaymentDatabase );
	}

}
?>