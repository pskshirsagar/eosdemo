<?php

class CBatchedCompanyPayment extends CBaseBatchedCompanyPayment {

	protected $m_objCompanyPayment;

    /**
     * Get Functions
     */

	public function getCompanyPayment() {
		return $this->m_objCompanyPayment;
	}

	/**
	 * Set Functions
	 */

	public function setCompanyPayment( $objCompanyPayment ) {
		$this->m_objCompanyPayment = $objCompanyPayment;
	}

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>