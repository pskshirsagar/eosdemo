<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CProcessingBanks
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CProcessingBanks extends CBaseProcessingBanks {

	public static function fetchProcessingBanks( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CProcessingBank', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchProcessingBank( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CProcessingBank', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllProcessingBanks( $objPaymentDatabase ) {
		return self::fetchProcessingBanks( 'SELECT * FROM processing_banks ORDER BY order_num', $objPaymentDatabase );
	}

	public static function fetchAllSimpleProcessingBanks( $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM processing_banks ORDER BY order_num';

		return fetchData( $strSql, $objPaymentDatabase );
	}

}
?>