<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMerchantAccountMethods
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CMerchantAccountMethods extends CBaseMerchantAccountMethods {

	public static function fetchMerchantAccountMethodsByCompanyMerchantAccountIdPaymentMediumId( $intCompanyMerchantAccountId, $intPaymentMediumId, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM merchant_account_methods WHERE company_merchant_account_id = ' . ( int ) $intCompanyMerchantAccountId . ' AND payment_medium_id = ' . ( int ) $intPaymentMediumId;

		return self::fetchMerchantAccountMethods( $strSql, $objPaymentDatabase );
	}

	public static function fetchMerchantAccountMethodsByCompanyMerchantAccountIdPaymentMediumIdPaymentTypeId( $intCompanyMerchantAccountId, $intPaymentMediumId, $intPaymentTypeId, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM merchant_account_methods WHERE company_merchant_account_id = ' . ( int ) $intCompanyMerchantAccountId . ' AND payment_medium_id = ' . ( int ) $intPaymentMediumId . ' AND payment_type_id = ' . ( int ) $intPaymentTypeId . ' AND is_enabled = 1';

		return self::fetchMerchantAccountMethod( $strSql, $objPaymentDatabase );
	}

	public static function fetchMerchantAccountMethodsByCompanyMerchantAccountIdPaymentTypeIdByPaymentMediumId( $intCompanyMerchantAccountId, $intPaymentMediumId, $intPaymentTypeId, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM merchant_account_methods WHERE company_merchant_account_id = ' . ( int ) $intCompanyMerchantAccountId . ' AND payment_medium_id = ' . ( int ) $intPaymentMediumId . ' AND payment_type_id = ' . ( int ) $intPaymentTypeId;

		return self::fetchMerchantAccountMethod( $strSql, $objPaymentDatabase );
	}

	public static function fetchMerchantAccountMethodsByCompanyMerchantAccountId( $intCompanyMerchantAccountId, $objDatabase ) {

		$strSql = 'SELECT * FROM merchant_account_methods WHERE company_merchant_account_id = ' . ( int ) $intCompanyMerchantAccountId;
		return self::fetchMerchantAccountMethods( $strSql, $objDatabase );
	}

	public static function fetchMerchantAccountMethodsByProcessingBankAccountIdsByPaymentMediumIdByCompanyStatusTypeIds( $arrintProcessingBankAccountIds, $intPaymentMediumId, $arrintCompanyStatusTypeIds, $objDatabase ) {
		$strSql = 'SELECT
						mam.id,
						mam.cid,
						mam.company_merchant_account_id,
						c.database_id
					FROM
						merchant_account_methods mam
						JOIN company_merchant_accounts cma ON cma.id = mam.company_merchant_account_id
						JOIN clients c ON c.id = mam.cid
					WHERE
						mam.processing_bank_account_id IN ( ' . implode( ',', $arrintProcessingBankAccountIds ) . ' )
						AND mam.payment_medium_id = ' . ( int ) $intPaymentMediumId . '
						AND c.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
						AND cma.is_company_charge_incentives_activated = 1
					ORDER BY
						c.database_id DESC, mam.cid';

		return fetchData( $strSql, $objDatabase );
	}

}
?>