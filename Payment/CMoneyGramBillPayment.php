<?php

class CMoneyGramBillPayment extends CBaseMoneyGramBillPayment {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMoneyGramAccountId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAgentId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMgReferenceNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerFee() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPaymentAmount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFirstName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMiddleName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLastName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAddress1() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCity() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valState() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostalCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPhoneNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAgentZip() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valProcessedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArPaymentId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>