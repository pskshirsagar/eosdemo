<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CReconciliations
 * Do not add any new functions to this class.
 */

class CReconciliations extends CBaseReconciliations {

	public static function fetchLastReconciliationByProcessingBankAccountId( $intProcessingBankAccountId, $objPaymentDatabase ) {
		$strSql = 'SELECT rec.* FROM reconciliations rec WHERE rec.processing_bank_account_id = ' . ( int ) $intProcessingBankAccountId . ' ORDER BY rec.created_on DESC LIMIT 1';
		return self::fetchReconciliation( $strSql, $objPaymentDatabase );
	}

	public static function fetchReconciliationByProcessingBankAccountIdByReconciliationId( $intProcessingBankAccountId, $intReconcliationId, $objPaymentDatabase ) {
		$strSql = 'SELECT rec.* FROM reconciliations rec WHERE rec.processing_bank_account_id = ' . ( int ) $intProcessingBankAccountId . ' AND rec.id = ' . ( int ) $intReconcliationId;
		return self::fetchReconciliation( $strSql, $objPaymentDatabase );
	}

	public static function fetchReconciliationsByProcessingBankAccountId( $intProcessingBankAccountId, $objPaymentDatabase ) {
		$strSql = 'SELECT rec.* FROM reconciliations rec WHERE rec.processing_bank_account_id = ' . ( int ) $intProcessingBankAccountId . ' ORDER BY rec.id';
		return self::fetchReconciliations( $strSql, $objPaymentDatabase );
	}

}
?>