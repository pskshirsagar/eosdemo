<?php

class CGiactRequestLog extends CBaseGiactRequestLog {

	const DECLINED = 'Declined';

	// Request Failed Response codes
	const GS01	= 'GS01';
	const GS02	= 'GS02';
	const GS03	= 'GS03';
	const GS04	= 'GS04';
	const GP01	= 'GP01';
	const RT00	= 'RT00';
	const RT01	= 'RT01';
	const RT02	= 'RT02';
	const RT04	= 'RT04';
	const GN01	= 'GN01';
	const GN05	= 'GN05';

	// Request Passed Response codes
	const ND00	= 'ND00';
	const ND01	= 'ND01';
	const _1111	= '_1111';
	const _2222	= '_2222';
	const _3333	= '_3333';
	const _5555	= '_5555';
	const _7777	= '_7777';
	const _8888	= '_8888';
	const _9999	= '_9999';
	const RT03	= 'RT03';
	const RT05	= 'RT05';

	protected $m_arrmixGiactRejectionStatuses;

	public static $c_arrstrGiactPassedStatuses = [ self::_1111, self::_2222, self::_3333, self::_5555, self::_7777, self::_8888, self::_9999, self::RT03, self::RT05, self::ND00, self::ND01 ];

	public static $c_arrstrGiactInvalidAcctResponseCodes = [ self::GS01, self::GS02, self::RT00 ];

	public static $c_arrmixGiactRejectionStatuses = [
		self::GN01	=> 'Negative information was found in accounts history',
		self::GN05	=> 'Routing number is not assigned to a financial institution',
		self::GP01	=> 'Account found in your API users Private Bad Check list',
		self::GS01	=> 'Invalid/Unassigned routing number',
		self::GS02	=> 'Invalid Account Number',
		self::GS03	=> 'Invalid Check Number',
		self::GS04	=> 'Invalid Amount',
		self::RT00	=> 'Routing number in participating bank, but account number not located',
		self::RT01	=> 'Account should be declined based on the risk factor reported',
		self::RT02	=> 'Item (Check Number) should be declined based on the risk factor reported',
		self::RT04	=> 'Non-Demand Deposit Account'
	];

	public function getGiactRejectionStatuses() {

		if( true == valArr( $this->m_arrmixGiactRejectionStatuses ) )
			return $this->m_arrmixGiactRejectionStatuses;

		$this->m_arrmixGiactRejectionStatuses = [
			self::GN01	=> __( 'Negative information was found in accounts history' ),
			self::GN05	=> __( 'Routing number is not assigned to a financial institution' ),
			self::GP01	=> __( 'Account found in your API users Private Bad Check list' ),
			self::GS01	=> __( 'Invalid/Unassigned routing number' ),
			self::GS02	=> __( 'Invalid Account Number' ),
			self::GS03	=> __( 'Invalid Check Number' ),
			self::GS04	=> __( 'Invalid Amount' ),
			self::RT00	=> __( 'Routing number in participating bank, but account number not located' ),
			self::RT01	=> __( 'Account should be declined based on the risk factor reported' ),
			self::RT02	=> __( 'Item (Check Number) should be declined based on the risk factor reported' ),
			self::RT04	=> __( 'Non-Demand Deposit Account' )
		];

		return $this->m_arrmixGiactRejectionStatuses;
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	const ALLOWEDRETRYLIMIT = 3;

	public static $c_arrstrBlockingResponseCodes = [ self::ND01, self::GS01, self::GS02, self::GS03, self::GS04, self::GP01, self::RT00, self::RT01, self::RT02, self::GN01, self::GN05 ];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>