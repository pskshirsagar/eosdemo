<?php

class CPaymentFileType extends CBasePaymentFileType {

	const NACHA_FILES						= 1;
	const NACHA_CONFIRMATION_FILES 			= 2;
	const NACHA_RETURN_FILES 				= 3;
	const NACHA_PARTICIPANT_FILES 			= 4;
	const X937_FILES 						= 5;
	const X937_RETURN_FILES 				= 6;
	const X937_IMAGES_FILES 				= 7;
	const BAII_FILES 						= 8;
	const WORLDPAY_SETTLEMENT_FILES 		= 9;
	const WORLDPAY_AMEX_SETTLEMENT_FILES 	= 10;
	const WORLDPAY_CHARGEBACK_FILES 		= 11;
	const MONEYGRAM_LOCATIONS 				= 12;
	const MONEYGRAM_TRANSACTION_REPORTS 	= 13;
	const TABAPAY_EXCEPTIONS_REPORTS 		= 14;
	const TABAPAY_CHARGEBACK_REPORTS 		= 15;
	const TABAPAY_TRANSACTION_REPORTS 		= 16;
	const TABAPAY_ISOSUMMARY_REPORTS 		= 17;
	const TABAPAY_STATEMENTS 				= 18;
	const TABAPAY_SUMMARY_REPORTS 			= 19;
	const TABAPAY_INTERCHANGE_REPORTS 		= 20;
	const ZION_ROUTING_NUMBERS 				= 21;
	const X937_CONFIRMATION_FILES			= 22;
	const AMEX_SETTLEMENT_FILES				= 23;
	const NACHA_AUTHORIZATION_FILES			= 24;
	const X937_AUTHORIZATION_FILES			= 25;
	const EFT_RETURN_FILES					= 26;
	const EFT_NOC_FILES						= 27;
	const EFT_FILES							= 28;
	const WORLDPAY_NOC_FILES				= 29;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriority() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>