<?php

use Psi\Libraries\Cryptography\CCrypto;

class CSettlementDistribution extends CBaseSettlementDistribution {

	protected $m_arrobjSettledArPayments;
	protected $m_arrobjReturnedArPayments;
	protected $m_arrobjReversedArPayments;
	protected $m_arrobjReversalReturnedArPayments;
	protected $m_arrobjArPaymentDistributions;
	protected $m_arrobjNachaEntryDetailRecords;

	protected $m_objCompanyMerchantAccount;
	protected $m_objAccount;
	protected $m_objClient;

	protected $m_strTraceNumber;
	protected $m_intAttemptCount;

	public function __construct() {
		parent::__construct();

		$this->m_fltDistributionAmount = 0;

		return;
	}

	/**
	 *
	 * Get Functions
	 */

	public function getTraceNumber() {
		return $this->m_strTraceNumber;
	}

	public function getNachaEntryDetailRecords() {
		return $this->m_arrobjNachaEntryDetailRecords;
	}

	public function getAccount() {
		return $this->m_objAccount;
	}

	public function getSettledArPayments() {
		return $this->m_arrobjSettledArPayments;
	}

	public function getReturnedArPayments() {
		return $this->m_arrobjReturnedArPayments;
	}

	public function getReversedArPayments() {
		return $this->m_arrobjReversedArPayments;
	}

	public function getCheckAccountNumber() {
		if( false == valStr( $this->getCheckAccountNumberEncrypted() ) ) {
			return NULL;
		}
		return CCrypto::createService()->decrypt( $this->m_strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getArPaymentDistributions() {
		return $this->m_arrobjArPaymentDistributions;
	}

	public function getClient() {
		return $this->m_objClient;
	}

	public function getCompanyMerchantAccount() {
		return $this->m_objCompanyMerchantAccount;
	}

	public function getBilltoNameFull() {
		return trim( $this->m_strBilltoNameFirst ) . ' ' . trim( $this->m_strBilltoNameLast );
	}

	public function getAttemptCount() {
		return $this->m_intAttemptCount;
	}

	public function getCheckAccountNumberMasked() {
		$strCheckAccountNumber = $this->getCheckAccountNumber();
		if( true == empty( $strCheckAccountNumber ) ) {
			return NULL;
		}

		$intStringLength = \Psi\CStringService::singleton()->strlen( $strCheckAccountNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, -4, $intStringLength );

		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	/**
	 *
	 * Add Functions
	 */

	public function addArPaymentDistribution( $objArPaymentDistribution ) {
		$this->m_arrobjArPaymentDistributions[$objArPaymentDistribution->getId()] = $objArPaymentDistribution;
	}

	/**
	 *
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['trace_number'] ) )	$this->setTraceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['trace_number'] ) : $arrmixValues['trace_number'] );
	}

	public function setNachaEntryDetailRecords( $arrobjNachaEntryDetailRecords ) {
		$this->m_arrobjNachaEntryDetailRecords = $arrobjNachaEntryDetailRecords;
	}

	public function setTraceNumber( $strTraceNumber ) {
		$this->m_strTraceNumber = CStrings::strTrimDef( $strTraceNumber, 15, NULL, true );
	}

	public function setAccount( $objAccount ) {
		return $this->m_objAccount = $objAccount;
	}

	public function setClient( $objClient ) {
		$this->m_objClient = $objClient;
	}

	public function setSettledArPayments( $arrobjArPayments ) {
		if( false == is_null( $arrobjArPayments ) && true == valArr( $arrobjArPayments ) ) {
			$this->m_arrobjSettledArPayments = $arrobjArPayments;
		}
	}

	public function setReturnedArPayments( $arrobjArPayments ) {
		if( false == is_null( $arrobjArPayments ) && true == valArr( $arrobjArPayments ) ) {
			$this->m_arrobjReturnedArPayments = $arrobjArPayments;
		}
	}

	public function setReversedArPayments( $arrobjArPayments ) {
		if( false == is_null( $arrobjArPayments ) && true == valArr( $arrobjArPayments ) ) {
			$this->m_arrobjReversedArPayments = $arrobjArPayments;
		}
	}

	public function setCheckAccountNumber( $strPlainCheckAccountNumber ) {
		if( true == valStr( $strPlainCheckAccountNumber ) ) {
			$this->setCheckAccountNumberEncrypted( CCrypto::createService()->encrypt( $strPlainCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
		}
	}

	public function setArPaymentDistributions( $arrobjArPaymentDistributions ) {

		$this->m_fltDistributionAmount = 0;

		// Calculate the settlement total here
		if( true == valArr( $arrobjArPaymentDistributions ) ) {
			foreach( $arrobjArPaymentDistributions as $objArPaymentDistribution ) {
				$this->m_fltDistributionAmount += $objArPaymentDistribution->getDistributionAmount();
			}
		}

		$this->m_arrobjArPaymentDistributions = $arrobjArPaymentDistributions;
	}

	public function setCompanyMerchantAccount( $objCompanyMerchantAccount ) {
		$this->m_objCompanyMerchantAccount = $objCompanyMerchantAccount;
	}

	public function setAttemptCount( $intAttemptCount ) {
		return $this->m_intAttemptCount = $intAttemptCount;
	}

	/**
	 *
	 * Create Functions
	 */

	public function createDistributionTransaction() {

		$objDistributionTransaction = new CDistributionTransaction();
		$objDistributionTransaction->setCid( $this->getCid() );
		$objDistributionTransaction->setAccountId( $this->getAccountId() );
		$objDistributionTransaction->setCompanyMerchantAccountId( $this->getCompanyMerchantAccountId() );
		$objDistributionTransaction->setSettlementDistributionId( $this->getId() );
		$objDistributionTransaction->setTransactionDatetime( $this->getDistributionDatetime() );
		$objDistributionTransaction->setTransactionAmount( $this->getDistributionAmount() );

		return $objDistributionTransaction;
	}

	/**
	 *
	 * Get Or Fetch Functions
	 */

	public function getOrFetchNachaEntryDetailRecords( $objPaymentDatabase ) {

		if( false == isset( $this->m_arrobjNachaEntryDetailRecords ) ) {

			$arrobjNachaEntryDetailRecords = \Psi\Eos\Payment\CNachaEntryDetailRecords::createService()->fetchNachaEntryDetailRecordsBySettlementDistributionId( $this->m_intId, $objPaymentDatabase );

			$this->setNachaEntryDetailRecords( $arrobjNachaEntryDetailRecords );
		}

		return $this->getNachaEntryDetailRecords();
	}

	/**
	 *
	 * Fetch Functions
	 */

	public function fetchProcessingBankAccountTransactions( $objPaymentDatabase ) {

		$strSql = 'SELECT
						pba.id as processing_bank_account_id,
						pba.routing_number,
						pba.account_number_encrypted,
						sum( ap.payment_amount ) as payment_amount
					FROM
						settlement_distributions sd,
						ar_payment_distributions apd,
						ar_payments ap,
						processing_bank_accounts pba
					WHERE
						sd.id = apd.settlement_distribution_id
						AND apd.ar_payment_id = ap.id
						AND ap.processing_bank_account_id = pba.id
						AND sd.id = ' . ( int ) $this->getId() . '
					GROUP BY
						pba.id,
						pba.routing_number,
						pba.account_number_encrypted';

		return fetchData( $strSql, $objPaymentDatabase );
	}

	public function fetchClient( $objPaymentDatabase ) {
		$this->m_objClient = CClients::fetchClientById( $this->m_intCid, $objPaymentDatabase );
		return $this->m_objClient;
	}

	public function fetchSettledArPayments( $objPaymentDatabase ) {
		return $this->m_arrobjSettledArPayments = CArPayments::fetchDistributedArPaymentsBySettlementDistributionIdPaymentDistributionTypeIdByCid( $this->m_intId, CPaymentDistributionType::SETTLEMENT, $this->getCid(), $objPaymentDatabase );
	}

	public function fetchAlreadyExportedArPaymentCount( $objPaymentDatabase ) {
		return $this->m_arrobjSettledArPayments = CArPayments::fetchAlreadyExportedArPaymentCountBySettlementDistributionIdByCid( $this->m_intId, $this->getCid(), $objPaymentDatabase );
	}

	public function fetchArPayments( $objPaymentDatabase ) {
		return $this->m_arrobjSettledArPayments = CArPayments::fetchArPaymentsBySettlementDistributionIdByCid( $this->m_intId, $this->getCid(), $objPaymentDatabase );
	}

	public function fetchArPaymentCount( $objPaymentDatabase ) {
		return $this->m_arrobjSettledArPayments = CArPayments::fetchArPaymentCountBySettlementDistributionIdByCid( $this->m_intId, $this->getCid(), $objPaymentDatabase );
	}

	public function fetchCompanyMerchantAccount( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CCompanyMerchantAccounts::createService()->fetchCompanyMerchantAccountById( $this->m_intCompanyMerchantAccountId, $objPaymentDatabase );
	}

	public function fetchMerchantAccount( $objDatabase ) {
		return CMerchantAccounts::fetchCustomMerchantAccountByIdByCid( $this->m_intCompanyMerchantAccountId, $this->getCid(), $objDatabase );
	}

	public function fetchAccount( $objAdminDatabase ) {
		return \Psi\Eos\Admin\CAccounts::createService()->fetchAccountById( $this->m_intAccountId, $objAdminDatabase );
	}

	public function fetchArPaymentDistributions( $objPaymentDatabase ) {
		$this->m_arrobjArPaymentDistributions = \Psi\Eos\Payment\CArPaymentDistributions::createService()->fetchArPaymentDistributionsBySettlementDistributionId( $this->m_intId, $objPaymentDatabase );
		return $this->m_arrobjArPaymentDistributions;
	}

	public function fetchComplexArPaymentDistributions( $objPaymentDatabase ) {
		$this->m_arrobjArPaymentDistributions = \Psi\Eos\Payment\CArPaymentDistributions::createService()->fetchComplexArPaymentDistributionsBySettlementDistributionId( $this->m_intId, $objPaymentDatabase );
		return $this->m_arrobjArPaymentDistributions;
	}

	public function fetchDistributionTransactions( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CDistributionTransactions::createService()->fetchDistributionTransactionsBySettlementDistributionId( $this->m_intId, $objPaymentDatabase );
	}

	/**
	 *
	 * Validation Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 *
	 * Other Functions
	 */

	public function processClosePaymentBatchIntegrationClient( $intCompanyUserId, $objClientDatabase, $objPaymentDatabase, $boolQueueSyncOverride = false, $intIntegrationClientType = NULL, $intIntegrationSyncTypeId = NULL ) {

		if( false == is_null( $this->getExportedOn() ) ) {
			return true;
		}

		// We only call this on distributions that are positive.  Negative distributions are reversed in external systems individually through the reverse or return methods.
		switch( $intIntegrationClientType ) {

			case CIntegrationClientType::REAL_PAGE:
				if( false == in_array( $this->getDistributionStatusTypeId(), [ CDistributionStatusType::CREDITED, CDistributionStatusType::DEBITED ] ) ) {
					return true;
				}
				break;

			default:
				if( CDistributionStatusType::CREDITED != $this->getDistributionStatusTypeId() ) {
					return true;
				}
		}

		$intPropertyId	= $this->fetchFirstPropertyInBatch( $objPaymentDatabase );
		$objProperty 	= \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $intPropertyId, $this->getCid(), $objClientDatabase );

		if( false == valObj( $objProperty, 'CProperty' ) ) {
			trigger_error( 'Client Id[' . $this->getCid() . '] Property Id[' . $intPropertyId . '] Settlement Distribution Id[' . $this->m_intId . '] Database Id[' . $objClientDatabase->getId() . '] Database Name[' . $objClientDatabase->getDatabaseName() . '] failed to load in batch integration export', E_USER_ERROR );
			exit;
		}

		if( true == is_null( $objProperty->getRemotePrimaryKey() ) || 0 == strlen( $objProperty->getRemotePrimaryKey() ) ) {
			return true;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $intPropertyId, $this->m_intCid, CIntegrationService::CLOSE_PAYMENT_BATCH, $intCompanyUserId, $objClientDatabase );
		$objGenericWorker->setClient( $this->fetchClient( $objClientDatabase ) );
		$objGenericWorker->setSettlementDistribution( $this );
		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setPaymentDatabase( $objPaymentDatabase );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		$boolIsValid = $objGenericWorker->process();

		if( false == $boolIsValid ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function fetchFirstPropertyInBatch( $objPaymentDatabase ) {

		$strSql = 'SELECT cp.property_id FROM ar_payments cp, ar_payment_distributions cpd
						WHERE cp.id = cpd.ar_payment_id AND cpd.cid = ' . $this->getCid() . ' AND cpd.settlement_distribution_id = ' . $this->m_intId . ' LIMIT 1';

		$arrintPropertyId = fetchData( $strSql, $objPaymentDatabase );

		if( true == valArr( $arrintPropertyId ) && true == isset( $arrintPropertyId[0]['property_id'] ) ) {
			return $arrintPropertyId[0]['property_id'];
		} else {
			return 0;
		}
	}

	public function fetchFirstPaymentInBatch( $objPaymentDatabase ) {
		return CArPayments::fetchFirstPaymentInBatchBySettlementDistributionIdByCid( $this->getId(), $this->getCid(), $objPaymentDatabase );
	}

	public function generateAndEmailSettlementDistributionNotice( $arrobjPaymentStatusTypes, $arrobjPaymentTypes, $objClientDatabase, $objAdminDatabase, $objPaymentDatabase, $objEmailDatabase, $boolIsFailure = false, $boolIsFromScript = false ) {

		$arrobjReturnTypes = array();
		if( true == $boolIsFailure ) {
			$arrobjReturnTypes = CReturnTypes::fetchAllReturnTypes( $objAdminDatabase );
		}

		// Generate a list of all property ids associated to payments in this settlement.
		$arrintPropertyIds = array();

		if( true == valArr( $this->m_arrobjArPaymentDistributions ) ) {
			foreach( $this->m_arrobjArPaymentDistributions as $objArPaymentDistribution ) {
				$objArPayment = $objArPaymentDistribution->getArPayment();
				if( true == valObj( $objArPayment, 'CArPayment' ) ) {
					array_push( $arrintPropertyIds, $objArPayment->getPropertyId() );
				}
			}
		}

		if( true == valArr( $arrintPropertyIds ) ) {
			$arrintPropertyIds = array_unique( $arrintPropertyIds );
			$arrobjProperties = \Psi\Eos\Admin\CProperties::createService()->fetchAllPropertiesByIdsByCid( $arrintPropertyIds, $this->getCid(), $objAdminDatabase );
		} else {
			$arrobjProperties = array();
		}
		if( false == valArr( $arrobjProperties ) ) {
			return NULL;
		}

		$objCompanyMerchantAccount = $this->fetchCompanyMerchantAccount( $objPaymentDatabase );

		$this->m_objPaymentEmailer = new CPaymentEmailer();
		return $this->m_objPaymentEmailer->emailSettlementDistributionNotices( $this, $arrobjProperties, $objCompanyMerchantAccount, $arrobjPaymentStatusTypes, $arrobjPaymentTypes, $arrobjReturnTypes, $objClientDatabase, $objEmailDatabase, $boolIsFailure, $objAdminDatabase, $boolIsFromScript );
	}

	public function populateGateway( $objGateway ) {

		require_once( PATH_EOS_DEFINES . 'TraceCodes.defines.php' );

		switch( $this->getPaymentTypeId() ) {
			case CPaymentType::ACH:
				$objGateway->setPaymentMethod( CGateway::PAYMENT_METHOD_ECHECK );
				break;

			default:
				return false;
				break;
		}

		$objGateway->setPaymentDatetime( $this->getDistributionDatetime() );
		$objGateway->setPaymentAmount( round( abs( $this->getDistributionAmount() ), 2 ) );
		$objGateway->setPaymentMemo( $this->getDistributionMemo() );
		$objGateway->setBilltoIpAddress( $this->getBilltoIpAddress() );
		$objGateway->setBilltoAccountNumber( $this->getBilltoAccountNumber() );
		$objGateway->setBilltoCompanyName( $this->getBilltoCompanyName() );
		$objGateway->setBilltoNameFirst( $this->getBilltoNameFirst() );
		$objGateway->setBilltoNameMiddle( $this->getBilltoNameMiddle() );
		$objGateway->setBilltoNameLast( $this->getBilltoNameLast() );
		$objGateway->setBilltoNameFull( $this->getBilltoNameFull() );
		$objGateway->setBilltoStreetLine1( $this->getBilltoStreetLine1() );
		$objGateway->setBilltoStreetLine2( $this->getBilltoStreetLine2() );
		$objGateway->setBilltoStreetLine3( $this->getBilltoStreetLine3() );
		$objGateway->setBilltoCity( $this->getBilltoCity() );
		$objGateway->setBilltoStateCode( $this->getBilltoStateCode() );
		$objGateway->setBilltoProvince( $this->getBilltoProvince() );
		$objGateway->setBilltoPostalCode( $this->getBilltoPostalCode() );
		$objGateway->setBilltoCountryCode( $this->getBilltoCountryCode() );
		$objGateway->setBilltoPhoneNumber( $this->getBilltoPhoneNumber() );
		$objGateway->setBilltoEmailAddress( $this->getBilltoEmailAddress() );
		$objGateway->setAchBankName( $this->getCheckBankName() );
		$objGateway->setAchAccountType( $this->getCheckAccountTypeId() );
		$objGateway->setAchAccountNumber( $this->getCheckAccountNumber() );
		$objGateway->setAchRoutingNumber( $this->getCheckRoutingNumber() );
		$objGateway->setAchNameOnAccount( $this->getCheckNameOnAccount() );

		// Set a duplicate value so that Echo will process our request
		$objGateway->setDuplicateCounter( rand( 1, 999999999 ) );
		$objGateway->setMerchantTraceNumber( CTraceCodes::SETTLEMENT_DISTRIBUTION . '=' . $this->getId() );

		// Set the ach check number to 000 because Echo will fail if there is a non-zero ec_serial_number on a credit - MST 20060202
		$objGateway->setCheckNumber( '000' );

		return true;
	}

	public function loadArTransactionByArPaymentDistribution( $objArPaymentDistribution, $arrobjNegativeArTransactions, $arrobjPositiveArTransactions ) {

		if( true == in_array( $objArPaymentDistribution->getPaymentDistributionTypeId(), array( CPaymentDistributionType::SETTLEMENT, CPaymentDistributionType::REVERSAL_RETURN ) ) ) {
			return ( true == isset( $arrobjPositiveArTransactions[$objArPaymentDistribution->getArPaymentId()] ) ) ? $arrobjPositiveArTransactions[$objArPaymentDistribution->getArPaymentId()] : NULL;

		} elseif( true == in_array( $objArPaymentDistribution->getPaymentDistributionTypeId(), array( CPaymentDistributionType::RECALL, CPaymentDistributionType::REVERSAL ) ) ) {
			return  ( true == isset( $arrobjNegativeArTransactions[$objArPaymentDistribution->getArPaymentId()] ) ) ? $arrobjNegativeArTransactions[$objArPaymentDistribution->getArPaymentId()] : NULL;
		}

		return NULL;
	}

	public function insertArDeposit( $intUserId, $objClientDatabase, $objPaymentDatabase ) {

		// Find out which bank account each properties deposits should go into.
		$objBankAccount = CBankAccounts::fetchBankAccountByCompanyMerchantAccountIdByCid( $this->getCompanyMerchantAccountId(), $this->getCid(), $objClientDatabase );

		if( false == valObj( $objBankAccount, 'CBankAccount' ) ) return 'Missing Bank Account';
		if( 1 == $objBankAccount->getIsDisabled() ) return 'Disabled Bank Account';
		if( 1 == $this->getIsPostedToArDeposits() ) return 'Already Posted';

		// Get all the payment ids related to this deposit.
		$arrobjArPaymentDistributions = $this->fetchComplexArPaymentDistributions( $objPaymentDatabase );

		// Find all the property ids on those payments
		$arrintPropertyIds = array();
		$arrintArPaymentIds = array();
		if( true == valArr( $arrobjArPaymentDistributions ) ) {
			foreach( $arrobjArPaymentDistributions as $objArPaymentDistribution ) {
				$arrintPropertyIds[$objArPaymentDistribution->getPropertyId()] = $objArPaymentDistribution->getPropertyId();
				$arrintArPaymentIds[$objArPaymentDistribution->getArPaymentId()] = $objArPaymentDistribution->getArPaymentId();
			}
		}

		if( false == valArr( $arrintPropertyIds ) ) return 'No Property Ids Loaded';

		$arrobjTempPositiveArTransactions = \Psi\Eos\Entrata\CArTransactions::createService()->fetchUndepositedPositiveArTransactionsByArPaymentIdsByCid( $arrintArPaymentIds, $this->getCid(), $objClientDatabase );
		$arrobjTempNegativeArTransactions = \Psi\Eos\Entrata\CArTransactions::createService()->fetchUndepositedNegativeArTransactionsByArPaymentIdsByCid( $arrintArPaymentIds, $this->getCid(), $objClientDatabase );

		$arrobjPositiveArTransactions = $arrobjNegativeArTransactions = array();

		if( true == valArr( $arrobjTempPositiveArTransactions ) ) {
			foreach( $arrobjTempPositiveArTransactions as $objArTransaction ) {
				$arrobjPositiveArTransactions[$objArTransaction->getArPaymentId()][$objArTransaction->getId()] = $objArTransaction;
			}
		}

		if( true == valArr( $arrobjTempNegativeArTransactions ) ) {
			foreach( $arrobjTempNegativeArTransactions as $objArTransaction ) {
				$arrobjNegativeArTransactions[$objArTransaction->getArPaymentId()][$objArTransaction->getId()] = $objArTransaction;
			}
		}

		// Divide up the ar payment distributions by ar post month from property gl settings.
		$arrobjPropertyGlSettings = CPropertyGlSettings::fetchPropertyGlSettingsByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objClientDatabase );
		$arrobjPropertyGlSettings = rekeyObjects( 'PropertyId', $arrobjPropertyGlSettings );

		// Nest the ar payment distributions by post month (we will create one ar deposit per post month)

		$arrobjNestedArPaymentDistributions = array();

		if( true == valArr( $arrobjArPaymentDistributions ) ) {
			foreach( $arrobjArPaymentDistributions as $objArPaymentDistribution ) {

				// We need to get the ar_transactions associated with this ar payment distribution.  It should be found in either $arrobjPositiveArTransactions or $arrobjNegativeArTransactions;
				$arrobjArTransactions = ( array ) $this->loadArTransactionByArPaymentDistribution( $objArPaymentDistribution, $arrobjNegativeArTransactions, $arrobjPositiveArTransactions );

				if( false == valArr( $arrobjArTransactions ) ) continue;

				$objPropertyGlSetting = $arrobjPropertyGlSettings[$objArPaymentDistribution->getPropertyId()];

				if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
					trigger_error( 'Property GL Setting failed to load.', E_USER_ERROR );
					exit;
				}

				$arrobjNestedArPaymentDistributions[$objPropertyGlSetting->getArPostMonth()][] = $objArPaymentDistribution;
			}
		}

		if( false == valArr( $arrobjNestedArPaymentDistributions ) ) {

			$this->setIsPostedToArDeposits( 1 );

			if( false == $this->update( $intUserId, $objPaymentDatabase ) ) {
				return 'Settlement Distro failed to update.';
			}

			return array( $this->getId() );
		}

		// Now let's find out what properties this bank account can use.  We have to ignore payments associated to properties not associated to the associated bank account.

		$arrintPropertyIdsWithABankAccountReference = array();

		if( true == valObj( $objBankAccount, 'CBankAccount' ) ) {
			$arrobjPropertyBankAccounts = $objBankAccount->fetchPropertyBankAccounts( $objClientDatabase );

			// Find out which properties are not associated to this bank account.
			if( true == valArr( $arrobjPropertyBankAccounts ) ) {
				foreach( $arrobjPropertyBankAccounts as $objPropertyBankAccount ) {
					if( true == in_array( $objPropertyBankAccount->getPropertyId(), $arrintPropertyIds ) ) {
						$arrintPropertyIdsWithABankAccountReference[$objPropertyBankAccount->getPropertyId()] = $objPropertyBankAccount->getPropertyId();
					}
				}
			}
		}

		// Make sure that none of the properties that we're about to create a deposit for are in migration mode.
		$arrintArDepositIds = array();

		$arrobjMigratedPropertyGLSettings = CPropertyGlSettings::fetchMigratedPropertyGlSettingsByPropertyIdsByCid( $arrintPropertyIdsWithABankAccountReference, $this->getCid(), $objClientDatabase );

		// This is a potential problem.  If someone turns on migration mode, none of the payments will get put into deposits automatically.
		if( true == valArr( $arrobjMigratedPropertyGLSettings ) ) {
			return 'Migration Mode';
		}

		// Create a deposit for all the payments with properties attached to this bank account
		if( true == valArr( $arrintPropertyIdsWithABankAccountReference ) && true == valArr( $arrobjNestedArPaymentDistributions ) ) {

			foreach( $arrobjNestedArPaymentDistributions as $strArPostMonth => $arrobjSubNestedArPaymentDistributions ) {

				$objArDeposit = $objBankAccount->createArDeposit( $objClientDatabase );
				$objArDeposit->setArDepositTypeId( CArDepositType::ELECTRONIC );
				$objArDeposit->setSettlementDistributionId( $this->getId() );
				$objArDeposit->setIsDeleted( 0 );
				$objArDeposit->setIsPosted( 0 );
				$objArDeposit->setGlTransactionTypeId( CGlTransactionType::AR_DEPOSIT );
				$objArDeposit->setDetailsInserted( 0 );

				$fltTotalDepositAmount 		= 0;
				$intAssociationCount 		= 0;
				$arrstrPostDates			= array();
				$arrstrPostMonths			= array();
				$arrintArTransactionIds 	= array();

				foreach( $arrobjSubNestedArPaymentDistributions as $objArPaymentDistribution ) {
					$objArTransaction = NULL;

					if( false == in_array( $objArPaymentDistribution->getPropertyId(), $arrintPropertyIdsWithABankAccountReference ) ) {
						echo 'No bank account refence could be found.';
						continue;
					}

					$arrobjArTransactions = ( array ) $this->loadArTransactionByArPaymentDistribution( $objArPaymentDistribution, $arrobjNegativeArTransactions, $arrobjPositiveArTransactions );

					if( false == valArr( $arrobjArTransactions ) ) {
						$this->setIsPostedToArDeposits( 1 );
						if( false == $this->update( $intUserId, $objPaymentDatabase ) ) {
							return 'Settlement Distro failed to update.';
						}
						return array( $this->getId() );
					}

					foreach( $arrobjArTransactions as $intArTransactionId => $objArTransaction ) {

						if( false == in_array( $objArTransaction->getPropertyId(), $arrintPropertyIdsWithABankAccountReference ) ) {
							echo 'No bank account refence could be found.';
							continue;
						}

						$arrintArTransactionIds[] = $objArTransaction->getId();
						$fltTotalDepositAmount = bcadd( $fltTotalDepositAmount, $objArTransaction->getTransactionAmount() * -1, 2 );
						$arrstrPostDates[strtotime( $objArTransaction->getPostDate() )] = $objArTransaction->getPostDate();
						$arrstrPostMonths[strtotime( $objArTransaction->getPostMonth() )] = $objArTransaction->getPostMonth();
						$intAssociationCount++;
					}
				}

				$strDepositDate = $this->determineDepositDate( $objPaymentDatabase );
				$arrstrPostDates[strtotime( $strDepositDate )] = $strDepositDate;
				$arrstrPostMonths[strtotime( $strArPostMonth )] = $strArPostMonth;

				$objArDeposit->setPostDate( $arrstrPostDates[max( array_keys( $arrstrPostDates ) )] );
				$objArDeposit->setPostMonth( $arrstrPostMonths[max( array_keys( $arrstrPostMonths ) )] );

				if( 0 < $intAssociationCount ) {

					$objArDeposit->setDepositAmount( $fltTotalDepositAmount );
					$objArDeposit->setArTransactionIds( $arrintArTransactionIds );

					if( false == $objArDeposit->insert( $intUserId, $objClientDatabase ) ) {
						return 'Ar deposit failed to insert.';
					}

					$this->setIsPostedToArDeposits( 1 );
					if( false == $this->update( $intUserId, $objPaymentDatabase ) ) {
						return 'Settlement Distro failed to update.';
					}

					$arrintArDepositIds[$objArDeposit->getId()] = $objArDeposit->getId();

				} else {
					return 'No payments could be associated to this deposit.';
				}
			}
		}

		return $arrintArDepositIds;
	}

	private function postFailedDistributionFeeToCompanyLedger( $objAdminDatabase ) {

		$objCompanyMerchantAccount = CCompanyMerchantAccounts::fetchCompanyMerchantAccountByIdAndCid( $this->getCompanyMerchantAccountId(), $this->getCid(), $objAdminDatabase );

		if( false == valObj( $objCompanyMerchantAccount, 'CCompanyMerchantAccount' ) ) {
			return false;
		}

		$fltFailedDistributionFee = $objCompanyMerchantAccount->getFailedDistributionFee( $this->getPaymentTypeId() );
		if( true == is_null( $fltFailedDistributionFee ) || 0 == $fltFailedDistributionFee ) return true;

		// Load client to get entity_id
		$objClient = $objCompanyMerchantAccount->fetchClient( $objAdminDatabase );

		if( false == valObj( $objClient, 'CClient' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to load client.', NULL ) );
			return false;
		}

		$objTransaction = new CTransaction();
		$objTransaction->setCid( $objCompanyMerchantAccount->getCid() );
		$objTransaction->setEntityId( $objClient->getEntityId() );
		$objTransaction->setChargeCodeId( CChargeCode::ACH_MANAGEMENT_FAILED_FEE );
		$objTransaction->setAccountId( $objCompanyMerchantAccount->getAccountId() );
		$objTransaction->setCompanyMerchantAccountId( $objCompanyMerchantAccount->getId() );

		$objTransaction->setTransactionDatetime( date( 'Y-m-d H:i:s' ) );
		$objTransaction->setTransactionAmount( $objCompanyMerchantAccount->getFailedDistributionFee( $this->getPaymentTypeId() ) );
		$objTransaction->setMemo( 'Failed distribution fee for distribution of $' . number_format( $this->getDistributionAmount(), 2 ) . '  Settlement  ' . $this->getId() );

		if( false == $objTransaction->postCharge( self::SYSTEM_USER_ID, $objAdminDatabase ) ) {
			$arrobjErrorMsgs = $objTransaction->getErrorMsgs();
			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjErrorMsgs ) ) {
				$this->m_arrobjErrorMsgs = array_merge( $this->m_arrobjErrorMsgs, $arrobjErrorMsgs );
			}

			return false;
		}

		return true;
	}

	// * Prepares the payment as NSF processed. An update() will need to be called after this method returns successfully by the caller.
	// * This should also be surrounded by a database transaction, since it includes modifications to multiple objects, and we have an
	// * all-or-nothing situation for accuracy of records and so that the process can be retried upon failure.

	public function processReturn( $intCurrentUserId, $objAdminDatabase, $objPaymentdatabase, $objEmailDatabse ) {

		if( CPaymentType::ACH != $this->m_intPaymentTypeId && CPaymentType::PAD != $this->m_intPaymentTypeId ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Cannot continue processing. Non-ACH type distribution ( Type ID ' . $this->getPaymentTypeId() . ' ) found.', NULL ) );
			return false;
		}

		if( false == in_array( $this->m_intDistributionStatusTypeId, array( CDistributionStatusType::CREDITED, CDistributionStatusType::DEBITED ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Distribution status type ( ID ' . $this->m_intDistributionStatusTypeId . ' ) is not "Credited" or "Debited". Processing will not continue.', NULL ) );
			return false;
		}

		if( false == isset( $this->m_strReturnedOn ) ) {
			$this->setReturnedOn( date( 'Y-m-d H:i:s' ) );
		}

		if( CDistributionStatusType::CREDITED == $this->m_intDistributionStatusTypeId ) {
			$this->setDistributionStatusTypeId( CDistributionStatusType::CREDIT_RETURN );

		} elseif( CDistributionStatusType::DEBITED == $this->m_intDistributionStatusTypeId ) {
			$this->setDistributionStatusTypeId( CDistributionStatusType::DEBIT_RETURN );
		}

		// Create a distribution_transaction to keep a history of this happening.
		$objDistributionTransaction = $this->createDistributionTransaction();
		$objDistributionTransaction->setPaymentTransactionTypeId( CPaymentTransactionType::RECALL );
		$objDistributionTransaction->setGatewayResponseCode( -1 );
		$objDistributionTransaction->setGatewayResponseText( $this->m_intReturnTypeId );

		// Save the settlement distribution transaction
		if( false == $objDistributionTransaction->validate( VALIDATE_INSERT ) || false == $objDistributionTransaction->insert( $intCurrentUserId, $objPaymentdatabase ) ) {
			$this->addErrorMsgs( $objDistributionTransaction->getErrorMsgs() );
			return false;
		}

//		if( CDistributionStatusType::DEBIT_RETURN == $this->getDistributionStatusTypeId() && false == $this->applyDistributionFailureFee( $intCurrentUserId, $objAdminDatabase, $objPaymentdatabase ) ) {
//			trigger_error( 'Distribution Failure Fee failed to apply. Id: ' . $this->getId(), E_USER_WARNING );
//			return false;
//		}
//
//		$this->setReturnFeePostedOn( date( 'Y-m-d H:i:s' ) );

		if( false == $this->update( $intCurrentUserId, $objPaymentdatabase ) ) {
			trigger_error( 'Returning settlement distribution failed to update.', E_USER_WARNING );
			return false;
		}

		$arrobjPaymentTypes						= CPaymentTypes::fetchAllPaymentTypes( $objPaymentdatabase );
		$arrobjPaymentStatusTypes				= CPaymentStatusTypes::fetchAllPaymentStatusTypes( $objPaymentdatabase );
		$this->m_arrobjArPaymentDistributions	= $this->fetchArPaymentDistributions( $objPaymentdatabase );

		// THIS IS CRAPPY CODE (I THINK).  CAN'T WE LOAD ALL AR PAYMENTS WITH ONE QUERY AND ASSOCIATE
		if( true == valArr( $this->m_arrobjArPaymentDistributions ) ) {
			foreach( $this->m_arrobjArPaymentDistributions as $objArPaymentDistribution ) {
				$objArPaymentDistribution->fetchArPayment( $objPaymentdatabase );
			}
		}

		// here we should fetch Appropriate client database to reflect this return payment.
		$objClient = $this->fetchClient( $objPaymentdatabase );

		if( false == valObj( $objClient, 'CClient' ) ) {
			trigger_error( 'Failed to load client object.', E_USER_ERROR );
			exit;
		}

		$this->setAttemptCount( \Psi\Eos\Payment\CArPaymentDistributions::createService()->fetchAttemptCountBySettlementDistributionId( $this->getId(), $objPaymentdatabase ) );

		$objClientDatabase = $objClient->loadDatabase( CDatabaseUserType::PS_PROPERTYMANAGER );

		$this->generateAndEmailSettlementDistributionNotice( $arrobjPaymentStatusTypes, $arrobjPaymentTypes, $objClientDatabase, $objAdminDatabase, $objPaymentdatabase, $objEmailDatabse, true );

		return true;
	}

	public function addErrorMsgs( $arrobjErrorMsgs ) {
		if( true == valArr( $arrobjErrorMsgs ) ) {
			foreach( $arrobjErrorMsgs as $objErrorMsg ) {
				if( true == valObj( $objErrorMsg, 'CErrorMsg' ) ) {
					$this->addErrorMsg( $objErrorMsg );
				}
			}
		}
	}

	public function determineDepositDate( $objPaymentDatabase ) {

		if( true == is_null( $this->getDistributionDatetime() ) ) {
			trigger_error( 'Distibute on date is null.', E_USER_ERROR );
			exit;
		}

		// Make sure that the settlement date is not in the transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.
		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( $this->getDistributionDatetime(), date( 'm/d/Y', ( strtotime( $this->getDistributionDatetime() ) + ( 30 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = array();

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intDepositDatetime = strtotime( $this->getDistributionDatetime() );

		while( true ) {
			$intDepositDatetime += ( 3600 * 24 );
			if( 'Sun' != date( 'D', $intDepositDatetime ) && 'Sat' != date( 'D', $intDepositDatetime ) && false == in_array( $intDepositDatetime, $arrintProhibitedTransactionHolidays ) ) {
				$strDepositDate = date( 'm/d/Y', $intDepositDatetime );
				break;
			}
		}

		return $strDepositDate;
	}

}
?>