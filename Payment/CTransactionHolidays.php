<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CTransactionHolidays
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CTransactionHolidays extends CBaseTransactionHolidays {

	public static function fetchTransactionHolidays( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CTransactionHoliday', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = 86400, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTransactionHoliday( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CTransactionHoliday', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = 86400, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( $strOnOrAfterDate, $strBeforeDate, $objPaymentDatabase ) {
		$strSql  = 'SELECT * FROM transaction_holidays WHERE holiday_date >= \'' . $strOnOrAfterDate . '\' AND holiday_date < \'' . addslashes( $strBeforeDate ) . '\'';
		return self::fetchTransactionHolidays( $strSql, $objPaymentDatabase );
	}

	public static function fetchTransactionHolidayByDate( $strDate, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM transaction_holidays WHERE holiday_date = \'' . $strDate . '\' LIMIT 1 ';
		return self::fetchTransactionHoliday( $strSql, $objPaymentDatabase );
	}

	public static function fetchTransactionHolidaysAfterDate( $strAfterDate, $objPaymentDatabase ) {
		$strSql  = 'SELECT * FROM transaction_holidays WHERE holiday_date > \'' . $strAfterDate . '\'';
		return self::fetchTransactionHolidays( $strSql, $objPaymentDatabase );
	}

}

?>