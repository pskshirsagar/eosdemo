<?php

class CPaymentFile extends CBasePaymentFile {

	public function valId() {
		$boolIsValid = true;

		if( false == \Psi\Libraries\UtilFunctions\valId( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Payment File Id', 'Invalid Payment File Id', NULL ) );
		}

		return $boolIsValid;
	}

	public function valPaymentFileTypeId() {
		$boolIsValid = true;

		if( false == \Psi\Libraries\UtilFunctions\valId( $this->getPaymentFileTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Payment File Type Id', 'Invalid Payment File Type Id', NULL ) );
		}

		return $boolIsValid;
	}

	public function valRemoteFileId() {
		$boolIsValid = true;

		if( true == isset( $this->m_intRemoteFileId ) && false == \Psi\Libraries\UtilFunctions\valId( $this->getRemoteFileId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Remote File Id', 'Invalid Remote File Id', NULL ) );
		}

		return $boolIsValid;
	}

	public function valProcessBankId() {
		$boolIsValid = true;

		if( true == isset( $this->m_intProcessingBankId ) && false == \Psi\Libraries\UtilFunctions\valId( $this->getProcessingBankId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Remote File Id', 'Invalid Remote File Id', NULL ) );
		}

		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;

		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getFileName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'File Name', 'Invalid File Name', NULL ) );
		}

		return $boolIsValid;
	}

	public function valFilePath() {
		$boolIsValid = true;

		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getFilePath() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'File Path', 'Invalid File Path', NULL ) );
		}

		return $boolIsValid;
	}

	public function valFileContainer() {
		$boolIsValid = true;

		if( false == isset( $this->m_strFileContainer ) || 0 == strlen( $this->getFileContainer() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'File Container', 'Invalid File Container', NULL ) );
		}

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;

		if( true == isset( $this->m_jsonDetails ) && false == is_object( $this->getDetails() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'details', 'Invalid json string for Payment File item.' ) );
		}

		return $boolIsValid;
	}

	public function valProcessedOn() {
		$boolIsValid = true;

		if( true == isset( $this->m_strProcessedOn ) && false == \Psi\Libraries\UtilFunctions\valStr( $this->getProcessedOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processed_on', 'Processed on is required.' ) );
		}

		return $boolIsValid;
	}

	public function valFileDownloadedOn() {
		$boolIsValid = true;

		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getFileDownloadedOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'downloaded_on', 'Processed on is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPaymentFileTypeId();
				$boolIsValid &= $this->valRemoteFileId();
				$boolIsValid &= $this->valProcessBankId();
				$boolIsValid &= $this->valFileContainer();
				$boolIsValid &= $this->valFilePath();
				$boolIsValid &= $this->valFileName();
				$boolIsValid &= $this->valFileDownloadedOn();
				$boolIsValid &= $this->valDetails();
				$boolIsValid &= $this->valProcessedOn();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>