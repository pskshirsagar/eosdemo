<?php

class CBinFlag extends CBaseBinFlag {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCommercialCard() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsInternationalCard() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCreditCard() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPrepaidCard() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsGiftCard() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCheckCard() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDebitCard() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>