<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CSettlementDistributions
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CSettlementDistributions extends CBaseSettlementDistributions {

	public static function fetchRecentUnArDepositedSettlementDistributionsByCidsByPropertyIds( $arrintCids, $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintCids ) && false == valArr( $arrintPropertyIds ) ) return;

		$strSql = 'SELECT
						DISTINCT ON (sd.id)
						sd.*
					FROM
						settlement_distributions sd,
						ar_payment_distributions apd,
						ar_payments ap
				   WHERE
				   		sd.id = apd.settlement_distribution_id
						AND apd.ar_payment_id = ap.id
						AND sd.cid = apd.cid
						AND apd.cid = ap.cid
						AND sd.is_posted_to_ar_deposits <> 1
						AND sd.distribution_datetime > ( NOW() - INTERVAL \'60 days\' ) ';

		if( true == valArr( $arrintCids ) ) {
			$strSql .= ' AND sd.cid IN ( ' . implode( ',', $arrintCids ) . ' )';
		}

		if( true == valArr( $arrintPropertyIds ) ) {
			$strSql .= ' AND ap.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		}

		return self::fetchSettlementDistributions( $strSql, $objDatabase );
	}

	public static function fetchSettlementDistributionsByArPaymentIdByCid( $intArPaymentId, $intCid, $objPaymentDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (sd.id)
						sd.*
					FROM
						settlement_distributions sd,
						ar_payment_distributions apd
				   WHERE
				   		sd.id = apd.settlement_distribution_id
				   		AND sd.cid = ' . ( int ) $intCid . '
				   		AND apd.ar_payment_id = ' . ( int ) $intArPaymentId;

		return self::fetchSettlementDistributions( $strSql, $objPaymentDatabase );
	}

	public static function fetchSettlementDistributionsByCidByArPaymentIds( $intCid, $arrintArPaymentIds, $objPaymentDatabase ) {
		$arrintArPaymentIds = array_filter( $arrintArPaymentIds );

		if ( false == valArr( $arrintArPaymentIds ) ) return;

		$strSql = 'SELECT
						DISTINCT ON (sd.id)
						sd.*
					FROM
						settlement_distributions sd,
						ar_payment_distributions apd
				   WHERE
				   		sd.id = apd.settlement_distribution_id
				   		AND sd.cid = ' . ( int ) $intCid . '
				   		AND apd.ar_payment_id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' )';

		return self::fetchSettlementDistributions( $strSql, $objPaymentDatabase );
	}

	public static function fetchSuccessfulSettlementDistributionsByArPaymentIdByCid( $intArPaymentId, $intCid, $objPaymentDatabase ) {

		$arrintSuccessfulDistributionStatusTypeIds = array( CDistributionStatusType::CREDITED, CDistributionStatusType::CREDITING, CDistributionStatusType::DEBITED, CDistributionStatusType::DEBITING );

		$strSql = 'SELECT
						DISTINCT ON (sd.id)
						sd.*
					FROM
						settlement_distributions sd,
						ar_payment_distributions apd
				   WHERE
				   		sd.id = apd.settlement_distribution_id
				   		AND sd.cid = ' . ( int ) $intCid . '
				   		AND sd.distribution_status_type_id IN ( ' . implode( ',', $arrintSuccessfulDistributionStatusTypeIds ) . ' )
				   		AND apd.ar_payment_id = ' . ( int ) $intArPaymentId;

		return self::fetchSettlementDistributions( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnExportedSettlementDistributionsByDatabaseId( $intDatabaseId, $arrintIntegratedPropertyIds, $objPaymentDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (sd.id) sd.*
					FROM
						settlement_distributions sd,
						clients c,
						ar_payment_distributions apd,
						ar_payments ap
					WHERE
						sd.cid = c.id
						AND c.database_id = ' . ( int ) $intDatabaseId . '
						AND sd.id = apd.settlement_distribution_id
						AND apd.ar_payment_id = ap.id ';

		if( true == valArr( $arrintIntegratedPropertyIds ) ) {
			$strSql .= ' AND ap.property_id IN ( ' . implode( ',', $arrintIntegratedPropertyIds ) . ' ) ';
		}

		$strSql .= ' AND sd.exported_on IS NULL
					AND sd.lock_sequence IS NULL
					AND sd.created_on > ( NOW() - INTERVAL \'15 days\' ) ';

		return self::fetchSettlementDistributions( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnEmailedSettlementDistributionsByDatabaseId( $intDatabaseId, $objPaymentDatabase ) {

		$strSql = 'SELECT
						sd.*
					FROM
						settlement_distributions sd
						JOIN clients c ON( sd.cid = c.id )
					WHERE
						c.database_id = ' . ( int ) $intDatabaseId . '
						AND sd.emailed_on IS NULL';

		return self::fetchSettlementDistributions( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnEmailedSettlementDistributionsTotalCountByDatabaseIds( $arrintDatabaseIds, $objPaymentDatabase ) {
		if( false == valArr( $arrintDatabaseIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT(sd.id)
					FROM
						settlement_distributions sd
						JOIN clients c ON( sd.cid = c.id )
					WHERE
						sd.emailed_on IS NULL
						AND c.database_id IN( ' . implode( ',', $arrintDatabaseIds ) . ' );';

		$arrmixData = fetchData( $strSql, $objPaymentDatabase );

		if( true == valArr( $arrmixData ) && true == isset( $arrmixData[0]['count'] ) ) {
			return $arrmixData[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchPaginatedSettlementDistributionsByCid( $intCid, $intPageNo, $intPageSize, $objPaymentDatabase, $objSettlementDistributionsFilter = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    	$intLimit = ( int ) $intPageSize;

    	$strTables = '';
		$strParameters = '';

    	if( true == valObj( $objSettlementDistributionsFilter, 'CSettlementDistributionsFilter' ) ) {

    		if( 0 < strlen( $objSettlementDistributionsFilter->getPropertyIds() ) ) {
				$strTables		 .= ' , ar_payment_distributions apd, ar_payments ap ';
    		}

    		$arrstrSearchParameters = self::fetchSettlementDistributionsSearchCriteria( $objSettlementDistributionsFilter );
    		$strParameters .= ( false == is_null( $arrstrSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrSearchParameters ) : '';
     	}

    	$strSql = 'SELECT
    					DISTINCT (sd.id), sd.*
    				FROM
    					settlement_distributions sd' . $strTables . '
    				WHERE
    					sd.cid = ' . ( int ) $intCid . $strParameters . '
    				ORDER BY
    					sd.distribution_datetime DESC,
    					sd.created_on DESC
    				OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

    	return self::fetchSettlementDistributions( $strSql, $objPaymentDatabase );
	}

	public static function fetchPaginatedSettlementDistributionsCountByCid( $intCid, $objPaymentDatabase, $objSettlementDistributionsFilter = NULL ) {

		$strTables = '';
		$strParameters = '';

    	if( true == valObj( $objSettlementDistributionsFilter, 'CSettlementDistributionsFilter' ) ) {

    		if( 0 < strlen( $objSettlementDistributionsFilter->getPropertyIds() ) ) {
				$strTables		 .= ' , ar_payment_distributions apd, ar_payments ap ';
    		}

    		$arrstrSearchParameters = self::fetchSettlementDistributionsSearchCriteria( $objSettlementDistributionsFilter );
    		$strParameters .= ( false == is_null( $arrstrSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrSearchParameters ) : '';
     	}

		$strSql = 'SELECT count(distinct(sd.id) )
					FROM
						settlement_distributions sd ' . $strTables . '
					WHERE
						sd.cid = ' . ( int ) $intCid . $strParameters;

		$arrintResponse = fetchData( $strSql, $objPaymentDatabase );

    	if ( true == isset ( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

    	return 0;
	}

	public static function fetchPaginatedSettlementDistributionsByCidByPropertyIds( $intCid, $arrintPropertyIds, $objPaymentDatabase, $objSettlementDistributionsFilter = NULL, $boolIsAdministrator = false ) {

    	if( false == valArr( $arrintPropertyIds ) ) return NULL;

    	if( true == valObj( $objSettlementDistributionsFilter, 'CSettlementDistributionsFilter' ) && true == $objSettlementDistributionsFilter->getIsShowSuccessfulOnly() ) {

			$strSql	= 'SELECT
							 sd.*
						FROM
							settlement_distributions sd
							JOIN company_merchant_accounts cma ON ( cma.id = sd.company_merchant_account_id AND cma.cid = sd.cid )
							JOIN
							(
								SELECT
									DISTINCT ( apd.settlement_distribution_id ),
									apd.cid
								FROM
									ar_payment_distributions apd
									JOIN ar_payments ap ON ( ap.id = apd.ar_payment_id AND ap.cid = apd.cid )
								WHERE
									ap.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
							) as sub_query ON ( sub_query.settlement_distribution_id = sd.id AND sub_query.cid = sd.cid )
						WHERE
							sd.cid = ' . ( int ) $intCid . '
							AND sd.distribution_status_type_id IN( ' . CDistributionStatusType::CREDITED . ',' . CDistributionStatusType::DEBITED . ' ) ';

		if( false == is_null( $objSettlementDistributionsFilter->getCompanyMerchantAccountIds() ) ) {
			$strSql .= ' AND cma.id IN ( ' . $objSettlementDistributionsFilter->getCompanyMerchantAccountIds() . ' ) ';
		}

			$strSql .= ' ORDER BY
							sd.company_merchant_account_id,
							sd.distribution_datetime DESC,
							sd.created_on ASC ';
		} else {

	    	if( true == valObj( $objSettlementDistributionsFilter, 'CSettlementDistributionsFilter' ) ) {

	    		$arrstrAndSearchParameters = self::fetchSettlementDistributionsSearchCriteriaNew( $objSettlementDistributionsFilter );

	    	}
	    	$strShowUnsuccessfulCondition = '';
	    	if( true == valObj( $objSettlementDistributionsFilter, 'CSettlementDistributionsFilter' ) && true == $objSettlementDistributionsFilter->getIsShowUnsuccessfulOnly() ) {
					$strShowUnsuccessfulCondition = 'AND sd.exported_on IS NULL
													AND sd.distribution_status_type_id IN( ' . CDistributionStatusType::CREDITED . ')';
	    	}
	    	$strSql = 'SELECT
	    					 sd.*
	    				FROM
							settlement_distributions sd
							JOIN company_merchant_accounts cma ON ( cma.id =  sd.company_merchant_account_id AND cma.cid = sd.cid )
						WHERE
							sd.cid = ' . ( int ) $intCid . '
							' . $strShowUnsuccessfulCondition . '
							' . ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' ) . '

						ORDER BY
							sd.company_merchant_account_id ASC,
							sd.distribution_datetime DESC,
							sd.created_on ASC ';
    	}

    	return self::fetchSettlementDistributions( $strSql, $objPaymentDatabase );
	}

	public static function fetchPaginatedSettlementDistributions( $intPageNo, $intPageSize, $objPaymentDatabase, $objSettlementDistributionsFilter = NULL, $boolIsSearch = false ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    	$intLimit = ( int ) $intPageSize;

    	$strTables = '';
    	$strParameters = '';

    	if( true == valObj( $objSettlementDistributionsFilter, 'CSettlementDistributionsFilter' ) ) {

    		if( 0 < strlen( $objSettlementDistributionsFilter->getPropertyIds() ) ) {
				$strTables		 .= ' , ar_payment_distributions apd, ar_payments ap ';
    		}

    		$arrstrSearchParameters = self::fetchSettlementDistributionsSearchCriteria( $objSettlementDistributionsFilter, $boolIsSearch );
    		$strParameters .= ( false == is_null( $arrstrSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrSearchParameters ) : '';
     	}

		$strSql = 'SELECT
						sd.*, c.company_name
				    FROM
						settlement_distributions sd,
						clients c ' . $strTables . '
				    WHERE
				   		sd.cid = c.id ' . $strParameters . '
				    ORDER BY
				   		sd.id DESC
				   	OFFSET ' . ( int ) $intOffset . '
				   	LIMIT ' . $intLimit;

    	return self::fetchSettlementDistributions( $strSql, $objPaymentDatabase );
	}

	public static function fetchPaginatedSettlementDistributionsCount( $objPaymentDatabase, $objSettlementDistributionsFilter = NULL, $boolIsSearch = false ) {

    	$strSql = 'SELECT count(DISTINCT(sd.id) ) FROM settlement_distributions sd ';

    	if( true == valObj( $objSettlementDistributionsFilter, 'CSettlementDistributionsFilter' ) ) {

    		if( 0 < strlen( $objSettlementDistributionsFilter->getPropertyIds() ) ) {
				$strSql	.= ' , ar_payment_distributions apd, ar_payments ap ';
    		}

    		$arrstrSearchParameters = self::fetchSettlementDistributionsSearchCriteria( $objSettlementDistributionsFilter, $boolIsSearch );
    		$strSql .= ( false == is_null( $arrstrSearchParameters ) ) ? ' WHERE ' . implode( ' AND ', $arrstrSearchParameters ) : '';
		}

    	$arrintResponse = fetchData( $strSql, $objPaymentDatabase );

    	if ( true == isset ( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

    	return 0;
	}

	public static function fetchSettlementDistributionsByIds( $arrintSettlementDistributionIds, $objPaymentDatabase ) {
		if( false == valArr( $arrintSettlementDistributionIds ) ) return NULL;

		$strSql = 'SELECT * FROM settlement_distributions WHERE id IN (' . implode( ',', $arrintSettlementDistributionIds ) . ')';
		return self::fetchSettlementDistributions( $strSql, $objPaymentDatabase );
	}

	public static function fetchSettlementDistributionsSearchCriteria( $objSettlementDistributionsFilter, $boolIsSearch = false ) {

		$arrstrAndSearchParameters = array();

		// Create SQL parameters.
		if( false == is_null( $objSettlementDistributionsFilter->getCompanyMerchantAccountIds() )
			&& 'all' != $objSettlementDistributionsFilter->getCompanyMerchantAccountIds()
			&& 0 < strlen( trim( $objSettlementDistributionsFilter->getCompanyMerchantAccountIds() ) ) ) {
			$arrstrAndSearchParameters[] = ' sd.company_merchant_account_id IN ( ' . $objSettlementDistributionsFilter->getCompanyMerchantAccountIds() . ' )';
		}

		if( false == is_null( $objSettlementDistributionsFilter->getStartDistributionDate() ) ) {
			$arrstrAndSearchParameters[] = " sd.distribution_datetime >= '" . date( 'Y-m-d', strtotime( $objSettlementDistributionsFilter->getStartDistributionDate() ) ) . " 00:00:00'";
		}

		if( false == is_null( $objSettlementDistributionsFilter->getEndDistributionDate() ) ) {
			$arrstrAndSearchParameters[] = " sd.distribution_datetime <= '" . date( 'Y-m-d', strtotime( $objSettlementDistributionsFilter->getEndDistributionDate() ) ) . " 23:59:59'";
		}

		if( false == is_null( trim( $objSettlementDistributionsFilter->getTraceNumber() ) )
			&& 0 < strlen( trim( $objSettlementDistributionsFilter->getTraceNumber() ) ) ) {
			$arrstrAndSearchParameters[] = " nedr.trace_number = '" . addslashes( trim( $objSettlementDistributionsFilter->getTraceNumber() ) ) . "'";
		}

		if( false == is_null( $objSettlementDistributionsFilter->getIsShowDisabledCompanyMerchantAccounts() )
			&& 1 != $objSettlementDistributionsFilter->getIsShowDisabledCompanyMerchantAccounts() ) {
			$arrstrAndSearchParameters[] = ' cma.is_disabled <> 1 ';
		}

		if( 0 < $objSettlementDistributionsFilter->getCid() ) {
			$arrstrAndSearchParameters[] = ' sd.cid = ' . ( int ) $objSettlementDistributionsFilter->getCid();
		}

		if( 0 < strlen( $objSettlementDistributionsFilter->getPropertyIds() ) ) {
			$arrstrAndSearchParameters[] = ' sd.id = apd.settlement_distribution_id AND apd.ar_payment_id = ap.id AND ap.property_id IN ( ' . $objSettlementDistributionsFilter->getPropertyIds() . ' ) ';
		}

		if( false == is_null( $objSettlementDistributionsFilter->getId() ) && true == is_numeric( $objSettlementDistributionsFilter->getId() ) ) {
			if( true == $boolIsSearch ) {
				$arrstrAndSearchParameters[] = ' to_char( sd.id, \'999999999\' ) LIKE \'%' . $objSettlementDistributionsFilter->getId() . '%\'';
			} else {
				$arrstrAndSearchParameters[] = 'sd.id = ' . ( int ) $objSettlementDistributionsFilter->getId();
			}
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) {
    		return $arrstrAndSearchParameters;
    	} else {
    		return NULL;
    	}
	}

	public static function fetchSettlementDistributionsSearchCriteriaNew( $objSettlementDistributionsFilter ) {

		$arrstrAndSearchParameters = array();

		// Create SQL parameters.
		if( false == is_null( $objSettlementDistributionsFilter->getCompanyMerchantAccountIds() )
			&& 'all' != $objSettlementDistributionsFilter->getCompanyMerchantAccountIds()
			&& 0 < strlen( trim( $objSettlementDistributionsFilter->getCompanyMerchantAccountIds() ) ) ) {
			$arrstrAndSearchParameters[] = ' sd.company_merchant_account_id IN ( ' . $objSettlementDistributionsFilter->getCompanyMerchantAccountIds() . ' )';
		}

		if( false == is_null( $objSettlementDistributionsFilter->getStartDistributionDate() ) ) {
			$arrstrAndSearchParameters[] = " sd.distribution_datetime >= '" . date( 'Y-m-d', strtotime( $objSettlementDistributionsFilter->getStartDistributionDate() ) ) . " 00:00:00'";
		}

		if( false == is_null( $objSettlementDistributionsFilter->getEndDistributionDate() ) ) {
			$arrstrAndSearchParameters[] = " sd.distribution_datetime <= '" . date( 'Y-m-d', strtotime( $objSettlementDistributionsFilter->getEndDistributionDate() ) ) . " 23:59:59'";
		}

		if( true == is_numeric( $objSettlementDistributionsFilter->getId() ) ) {
			$arrstrAndSearchParameters[] = ' sd.id = ' . ( int ) $objSettlementDistributionsFilter->getId();
		}

		if( false == is_null( $objSettlementDistributionsFilter->getIsShowDisabledCompanyMerchantAccounts() )
			&& 1 != $objSettlementDistributionsFilter->getIsShowDisabledCompanyMerchantAccounts() ) {
			$arrstrAndSearchParameters[] = ' ( cma.is_disabled = 0 OR cma.is_disabled IS NULL )';
		}

		if( 0 < $objSettlementDistributionsFilter->getCid() ) {
			$arrstrAndSearchParameters[] = ' sd.cid = ' . ( int ) $objSettlementDistributionsFilter->getCid();
		}

		if( 0 < strlen( $objSettlementDistributionsFilter->getPropertyIds() ) ) {
			$arrstrAndSearchParameters[] = ' sd.id IN( SELECT DISTINCT ON( apd.settlement_distribution_id ) apd.settlement_distribution_id FROM ar_payment_distributions apd, ar_payments ap WHERE sd.id = apd.settlement_distribution_id AND apd.ar_payment_id = ap.id AND ap.property_id IN ( ' . $objSettlementDistributionsFilter->getPropertyIds() . ' ) )';
		}

		if( false == is_null( trim( $objSettlementDistributionsFilter->getTraceNumber() ) )
			&& 0 < strlen( trim( $objSettlementDistributionsFilter->getTraceNumber() ) ) ) {
			$arrstrAndSearchParameters[] = " sd.id IN( SELECT settlement_distribution_id FROM nacha_entry_detail_records WHERE trace_number = '" . addslashes( trim( $objSettlementDistributionsFilter->getTraceNumber() ) ) . "' )";
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) {
    		return $arrstrAndSearchParameters;
    	} else {
    		return NULL;
    	}
	}

	public static function updateSettlementDistributionConfirmedOnByPropertyId( $intUserId, $intPropertyId, $objPaymentDatabase ) {
		$strSql = 'UPDATE
							settlement_distributions
						SET
							confirmed_on = NOW(),
							updated_by = ' . ( int ) $intUserId . ',
							updated_on = NOW()
						WHERE
							id IN (
								SELECT
									sd.id
								FROM
									settlement_distributions sd
									JOIN ar_payment_distributions apd ON apd.settlement_distribution_id = sd.id
									JOIN ar_payments ap ON ap.id = apd.ar_payment_id
								WHERE
									ap.property_id = ' . ( int ) $intPropertyId . '
									AND sd.confirmed_on IS NULL
							)
						RETURNING id';

		$arrmixResult = fetchData( $strSql, $objPaymentDatabase );

		if( false == $arrmixResult ) {
			return false;
		}

		return \Psi\Libraries\UtilFunctions\count( $arrmixResult );
	}

}
?>