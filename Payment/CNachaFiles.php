<?php

/**
 * Class CNachaFiles
 * @deprecated Please use \Psi\Eos\Payment\CNachaFiles
 */
class CNachaFiles extends CBaseNachaFiles {

	/**
	 * @deprecated Please use \Psi\Eos\Payment\CNachaFiles instead
	 * @throws Exception
	 */
	public static function fetchPaginatedNachaFiles( $intPageNo, $intPageSize, $objPaymentDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    	$intLimit = ( int ) $intPageSize;

    	$strSql = 'SELECT * FROM (
						SELECT nf.*,
							CASE
					    		WHEN confirmed_on IS NULL
					        	THEN 0
					        	ELSE 1
					    	END AS confirmed_status
					 	FROM
					    nacha_files nf
					ORDER BY
					nf.created_on DESC ) AS sub
					ORDER BY confirmed_status, created_on DESC
			   		OFFSET ' . ( int ) $intOffset . '
			   		LIMIT ' . $intLimit;

    	return self::fetchNachaFiles( $strSql, $objPaymentDatabase );
		// throw new Exception( 'Kindly use \Psi\Eos\Payment\CNachaFiles instead' );
	}

	/**
	 * @param $objPaymentDatabase
	 * @return int
	 * @deprecated Use \Psi\Eos\Payment\CNachaFiles
	 */
	public static function fetchPaginatedNachaFilesCount( $objPaymentDatabase ) {

    	$strSql = 'SELECT count(id) FROM nacha_files';

    	$arrintResponse = fetchData( $strSql, $objPaymentDatabase );

    	if ( true == isset ( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

    	return 0;
	}

	public static function fetchDailyNachaFilesByDate( $strDate, $objPaymentDatabase ) {

	    $strSql = 'SELECT nf.*, ( nf.control_total_credit_entry_amount::numeric / 100::numeric ) AS total_amount FROM
    					nacha_files nf
    			   WHERE
    			   		DATE_TRUNC( \'day\', created_on ) = \'' . $strDate . '\'
    			   ORDER BY
    			   		nf.created_on DESC';

	    return self::fetchNachaFiles( $strSql, $objPaymentDatabase );
	}

	public static function fetchDailyNachaFilesByProcessingBankIdByDate( $intProcessingBankId, $strDate, $objPaymentDatabase ) {

	    $strSql = 'SELECT nf.*, ( nf.control_total_credit_entry_amount::numeric / 100::numeric ) AS total_amount FROM
    					nacha_files nf
    			   WHERE
    			   		DATE_TRUNC( \'day\', created_on ) = \'' . $strDate . '\'
    			   		AND processing_bank_id = ' . ( int ) $intProcessingBankId . '
    			   ORDER BY
    			   		nf.created_on DESC';

	    return self::fetchNachaFiles( $strSql, $objPaymentDatabase );
	}

	public static function fetchNachaFilesByProcessingBankIdWithNullTransmittedOn( $intProcessingBankId, $objPaymentDatabase ) {

	    $strSql = 'SELECT
	    				nf.*
	    		   FROM
    					nacha_files nf
    			   WHERE
    			   		transmitted_on IS NULL
    			   		AND processing_bank_id = ' . ( int ) $intProcessingBankId . '
    			   ORDER BY
    			   		nf.id';

	    return self::fetchNachaFiles( $strSql, $objPaymentDatabase );
	}

	public static function fetchNachaFilesByProcessingBankIdWithNullConfirmedOn( $intProcessingBankId, $objPaymentDatabase ) {

		$strSql = '	SELECT
						nf.*
					FROM
						nacha_files nf
					WHERE
						nf.confirmed_on IS NULL
						AND nf.processing_bank_id = ' . ( int ) $intProcessingBankId . '
					ORDER BY
						nf.id';

		return self::fetchNachaFiles( $strSql, $objPaymentDatabase );
	}

	public static function fetchNachaFilesByProcessingBankIdByCreatedOnDate( $intProcessingBankId, $strDate, $boolIsFetchConfirmedOn, $objPaymentDatabase ) {

		 $strSql = 'SELECT';
				if( true === $boolIsFetchConfirmedOn ) {
				 	$strSql .= ' count(nf.id) as count';
				} else {
					$strSql .= ' nf.*';
				}

				$strSql .= ' FROM
					nacha_files nf
				WHERE
					DATE_TRUNC( \'day\', created_on ) = \'' . $strDate . '\'
					AND nf.processing_bank_id = ' . ( int ) $intProcessingBankId;

				if( true === $boolIsFetchConfirmedOn ) {
				 	$strSql .= ' AND nf.confirmed_on IS NULL ';
				}

				if( true === $boolIsFetchConfirmedOn ) {
					$strSql .= ' GROUP BY
					nf.id';
				}

				$strSql .= ' ORDER BY
					nf.id';

		if( true === $boolIsFetchConfirmedOn ) {
			$arrintResponse = fetchData( $strSql, $objPaymentDatabase );
    		if ( true == isset ( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
    		return 0;
		} else {
			return self::fetchNachaFiles( $strSql, $objPaymentDatabase );
		}
	}

}
?>