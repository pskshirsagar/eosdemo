<?php

use Psi\Libraries\Cryptography\CCrypto;

class CApplicationKeyValue extends CBaseApplicationKeyValue {

	const SECURE 		= 1;
	const END_USER_ID 	= 1;

	const PERSONAL_GUARANTEE 	= 1;
	const ENTITY_GUARANTEE 		= 2;

    /**
     * Validation Functions
     */

    public function valValue() {
        $boolIsValid = true;

        if( 'DOB' == $this->getKey() ) {

        	if( true == is_null( $this->getValue() ) ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Officer birth date', 'Officer birth date is required.' ) );
        	} elseif( false == CValidation::validateDate( $this->getValue() ) ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Officer birth date', 'Officer birth date is not a valid date.' ) );
        	} elseif( strtotime( $this->getValue() ) >= strtotime( date( 'm/d/Y' ) ) ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Officer birth date should be earlier than current date.' ) );
        	}
        }

        if( true == is_null( $this->getValue() ) ) {
        	$boolIsValid = false;

        	switch( $this->getKey() ) {
        		case 'QUESTION1_PRODUCTS_SERVICES':
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Question1\'s answer is required.' ) );
        			break;

        		case 'QUESTION2_HOW_ADVERTISE':
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Question2\'s answer is required.' ) );
        			break;

        		case 'QUESTION3_WHAT_CHARGE_FOR':
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Question3\'s answer is required.' ) );
        			break;

        		case 'QUESTION4_BILLING_POLICIES':
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Question4\'s answer is required.' ) );
        			break;

        		case 'QUESTION5_CHARGED_IN_ADVANCE':
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Question5\'s answer is required.' ) );
        			break;

        		case 'QUESTION6_LEASE_PERIOD':
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Question6\'s answer is required.' ) );
        			break;

        		case 'QUESTION7_REFUND_POLICY':
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Question7\'s answer is required.' ) );
        			break;

        		case 'OFFICER_NAME':
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Officer name is required.' ) );
        			break;

        		case 'SS':
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Officer SS# is required.' ) );
        			break;

        		case 'OFFICER_NAME_OPTION':
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Officer title is required.' ) );
        			break;

        		case 'DOB':
        			// $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Officer birth date is required.' ) );
        			break;

        		case 'HOME_ADDRESS':
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Officer home address is required.' ) );
        			break;

        		case 'OFFICER_CITY':
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Officer city is required.' ) );
        			break;

        		case 'OFFICER_STATE_CODE':
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Officer state is required.' ) );
        			break;

        		case 'OFFICER_POSTAL_CODE':
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Officer zip code is required.' ) );
        			break;

        		case 'PHONE_NUMBER':
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Officer phone number is required.' ) );
        			break;

        		default:
        			$boolIsValid = true;
        			break;
        	}
        } elseif( 'SS' == $this->getKey() && 9 != strlen( $this->getValue() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Enter valid SS#.' ) );
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function valIsSecure() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function validate( $strAction, $strFormName = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	switch( $strFormName ) {
            		case 'form1':
            		case 'form2':
            			break;

            		case 'form3':
            			$boolIsValid   &= $this->valValue();
            			break;

            		case 'form4':
            			break;

            		default:
            			// validate nothing
            			break;
            	}
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valValue();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	/**
	 * @return null|string
	 * @throws \Psi\Libraries\Cryptography\Exceptions\CCryptoErrorException
	 * @throws \Psi\Libraries\Cryptography\Exceptions\CCryptoInvalidException
	 */
    public function getEncryptedValue() {
    	if( !valStr( $this->m_strValue ) ) {
    		return NULL;
	    }
	    return CCrypto::createService()->decrypt( $this->m_strValue, CONFIG_SODIUM_KEY_APPLICATION_KEY_VALUE, [ 'legacy_secret_key' => CONFIG_KEY_APPLICATION_KEY_VALUE ] );
    }

	/**
	 * @param $strValue
	 * @throws \Psi\Libraries\Cryptography\Exceptions\CCryptoErrorException
	 * @throws \Psi\Libraries\Cryptography\Exceptions\CCryptoInvalidException
	 */
	public function setEncryptedValue( $strValue ) {
		if( !valStr( $strValue ) ) {
			return;
		}
        $this->setValue( CCrypto::createService()->encrypt( $strValue, CONFIG_SODIUM_KEY_APPLICATION_KEY_VALUE ) );
    }

}
?>