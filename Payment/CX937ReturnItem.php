<?php

class CX937ReturnItem extends CBaseX937ReturnItem {

	const RETURN_DATA_FIELD_PROCESSING_DATE = 0;
	const RETURN_DATA_FIELD_PAYMENT_NUMBER 	= 1;
	const RETURN_DATA_FIELD_AMOUNT 			= 2;
	const RETURN_DATA_FIELD_ROUTING_NUMBER 	= 3;
	const RETURN_DATA_FIELD_ACCOUNT_NUMBER 	= 4;
	const RETURN_DATA_FIELD_CHECK_NUMBER 	= 5;
	const RETURN_DATA_FIELD_RETURN_TYPE 	= 8;

    protected $m_strReturnReason;

	protected $m_objCompanyPayment;
	protected $m_objArPayment;
	protected $m_objReturnType;

	protected $m_intProcessingBankAccountId;
	protected $m_intIntermediaryAccountId;

	// Zions Return Reason - PSI Return Type Id association
	protected $m_arrintReturnTypesMapping = array(
		'1' => '183',
		'2' => '207',
		'3' => '209',
		'4' => '208',
		'5' => '210',
		'6' => '215',
		'8' => '214',
		'10' => '217',
		'11' => '216',
		'12' => '184',
		'13' => '217',
		'14' => '212',
		'16' => '219',
		'17' => '181',
		'26' => '219',
		'27' => '173',
		'28' => '219',
		'31' => '222',
		'32' => '211',
		'33' => '213',
		'34' => '218',
		'35' => '225',
		'36' => '221',
		'37' => '223',
		'38' => '184',
		'39' => '225',
		'40' => '179',
		'41' => '226',
		'42' => '227',
		'43' => '184',
		'44' => '184',
		'45' => '184',
		'46' => '184',
		'47' => '219',
		'48' => '210',
		'51' => '183',
		'52' => '207',
		'53' => '209',
		'54' => '208',
		'55' => '210',
		'56' => '215',
		'58' => '216',
		'60' => '217',
		'61' => '212',
		'62' => '184',
	);

    /**
     * Get Functions
     */

    public function getReturnReason() {
    	return $this->m_strReturnReason;
    }

    public function getProcessingDate() {
    	return $this->getProcessedOn();
    }

	/**
	 * @return CArPayment
	 */
    public function getArPayment() {
    	return $this->m_objArPayment;
    }

    public function getProcessingBankAccountId() {
    	return $this->m_intProcessingBankAccountId;
    }

    public function getIntermediaryAccountId() {
    	return $this->m_intIntermediaryAccountId;
    }

	public function getCheckAccountNumber() {
    	if( !\Psi\Libraries\UtilFunctions\valStr( $this->getCheckAccountNumberEncrypted() ) ) {
    		return '';
		}

    	return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
    }

    /**
     * Set Functions
     */

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrValues['intermediary_account_id'] ) ) $this->setIntermediaryAccountId( $arrValues['intermediary_account_id'] );
        if( true == isset( $arrValues['processing_bank_account_id'] ) ) $this->setProcessingBankAccountId( $arrValues['processing_bank_account_id'] );
		if( true == isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );

        return;
    }

    public function setArPayment( $objArPayment ) {
    	$this->m_objArPayment = $objArPayment;
    }

    public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
    	$this->m_intProcessingBankAccountId = $intProcessingBankAccountId;
    }

    public function setReturnReason( $strReturnReason ) {
    	$this->m_strReturnReason = $strReturnReason;
    }

	public function setProcessingDate( $strProcessingDate ) {
    	$objDateTime = \DateTime::createFromFormat( 'Ymd', $strProcessingDate );
    	$this->setProcessedOn( $objDateTime->format( 'Y-m-d' ) );
    }

    public function setIntermediaryAccountId( $intIntermediaryAccountId ) {
    	$this->m_intIntermediaryAccountId = $intIntermediaryAccountId;
    }

	public function setCheckAccountNumber( $strCheckAccountNumber ) {
    	if( !\Psi\Libraries\UtilFunctions\valStr( $strCheckAccountNumber ) ) {
    		return NULL;
		}

    	$strCheckAccountNumberEncrypted = \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER );
    	$this->setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted );
    }

    /**
     * Create Functions
     */

	public function createX937ReturnImage() {

		$objX937ReturnImage = new CX937ReturnImage();
		$objX937ReturnImage->setX937ReturnFileId( $this->getX937ReturnFileId() );
		$objX937ReturnImage->setX937ReturnItemId( $this->getId() );

		return $objX937ReturnImage;
	}

	public function getCheckAccountNumberMasked() {
		$strCheckAccountNumber = $this->getCheckAccountNumber();
		$intStringLength = strlen( $strCheckAccountNumber );
		$strLastFour = substr( $strCheckAccountNumber, -4 );
		return str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getCheckRoutingNumberMasked() {
		$intStringLength = strlen( $this->getCheckRoutingNumber() );
		$strLastFour = substr( $this->getCheckRoutingNumber(), -4 );
		return str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	/**
	 * Fetch Functions
	 */

	public function fetchX937ReturnImages( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CX937ReturnImages::createService()->fetchX937ReturnImagesByX937ReturnItemIdByX937ReturnFileId( $this->m_intId, $this->m_intX937ReturnFileId, $objPaymentDatabase );
	}

	public function fetchCompanyPayment( $objAdminDatabase ) {
		$this->m_objCompanyPayment = \Psi\Eos\Admin\CCompanyPayments::createService()->fetchCompanyPaymentById( $this->m_intCompanyPaymentId, $objAdminDatabase );
		return $this->m_objCompanyPayment;
	}

	public function fetchArPayment( $objPaymentDatabase ) {
		$this->m_objArPayment = CArPayments::fetchArPaymentByIdByCid( $this->m_intArPaymentId, $this->getCid(), $objPaymentDatabase );
		return $this->m_objArPayment;
	}

	public function fetchReturnType( $objPaymentDatabase ) {
		$this->m_objReturnType = CReturnTypes::fetchReturnTypeById( $this->m_intReturnTypeId, $objPaymentDatabase );
		return $this->m_objReturnType;
	}

	/**
	 * Get Or Fetch Functions
	 */

	public function getOrFetchArPayment( $objPaymentDatabase ) {

		if( true == valObj( $this->m_objArPayment, 'CArPayment' ) ) {
			return $this->m_objArPayment;
		} else {
			return $this->fetchArPayment( $objPaymentDatabase );
		}
	}

	public function getOrFetchReturnType( $objPaymentDatabase ) {

		if( true == valObj( $this->m_objReturnType, 'CReturnType' ) ) {
			return $this->m_objReturnType;
		} else {
			return $this->fetchReturnType( $objPaymentDatabase );
		}
	}

	/**
	 * Validation Functions
	 */

    public function valX937ReturnFileId() {
        $boolIsValid = true;

        if( true == is_null( $this->getX937ReturnFileId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'x937_return_file_id', 'X937 return file id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valReturnTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getReturnTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'return_type_id', 'X937 return type is required.' ) );
        }

        return $boolIsValid;
    }

    public function valProcessedOn() {
        $boolIsValid = true;

        if( true == is_null( $this->getProcessedOn() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processed_on', 'Processed On is Required' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valX937ReturnFileId();
            	$boolIsValid &= $this->valReturnTypeId();
            	break;

			case 'process_return':
            	$boolIsValid &= $this->valX937ReturnFileId();
            	$boolIsValid &= $this->valReturnTypeId();
            	$boolIsValid &= $this->valProcessedOn();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Other Functions
     */

    public function processReturn( $intUserId, $objAdminDatabase, $objClientDatabase = NULL, $objPaymentDatabase = NULL, $objEmailDatabase = NULL, $objArPayment = NULL ) {

    	if( true == is_numeric( $this->m_intArPaymentId ) ) {

			$this->m_objArPayment = $objArPayment;

			if( false == valObj( $this->m_objArPayment, 'CArPayment' ) ) {
				trigger_error( 'Invalid ArPayment object provided for return processing', E_USER_WARNING );
				return false;
			}

    		if( true == isset ( $this->m_objArPayment )
    				&& CPaymentStatusType::CAPTURED == $this->m_objArPayment->getPaymentStatusTypeId()
    				&& true == is_null( $this->m_objArPayment->getReturnedOn() )
    				&& CPaymentType::CHECK_21 == $this->m_objArPayment->getPaymentTypeId() ) {

    			$this->m_objArPayment->setReturnTypeId( $this->m_intReturnTypeId );

    			$this->fetchReturnType( $objAdminDatabase );

    			if( true == valObj( $this->m_objReturnType, 'CReturnType' ) ) {
    				$strReturnReason = $this->m_objReturnType->getName();
    			}

    			if( false == $this->m_objArPayment->processPayment( CArPayment::PROCESS_METHOD_RETURN, $intUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase = NULL, $boolForceIntegration = false, $boolSendEmails = false, $strReturnReason ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, getConsolidatedErrorMsg( $this->m_objArPayment ) ) );
					return false;
				}
    		}

    	} elseif( true == is_numeric( $this->m_intCompanyPaymentId ) ) {

    		$this->fetchCompanyPayment( $objAdminDatabase );

    		if( true == isset ( $this->m_objCompanyPayment ) && CPaymentStatusType::CAPTURED == $this->m_objCompanyPayment->getPaymentStatusTypeId() ) {

				$this->m_objCompanyPayment->setReturnTypeId( $this->m_intReturnTypeId );

    			if( false == $this->m_objCompanyPayment->processReturn( $intUserId, $objAdminDatabase, $objPaymentDatabase, $objEmailDatabase ) ) {
					return false;
	    		}
    		}

    	} else {
    		return false;
    	}

    	return true;
    }

	public function setAECReturnItemData( $objCheckItem, $objAdminDatabase ) {

		$arrmixReturnData = explode( '-', $objCheckItem['Data2'] );

		if( true == valArr( $arrmixReturnData ) ) {
			$strX937ReturnCode = '300' . trim( $arrmixReturnData[0] );
		}

		$objReturnType = CReturnTypes::fetchReturnTypeByCode( $strX937ReturnCode, $objAdminDatabase );

		if( true == valObj( $objReturnType, 'CReturnType' ) ) {
			$this->setReturnTypeId( $objReturnType->getId() );
		} else {
			$this->setReturnTypeId( CReturnType::GENERIC_CHECK21_RETURN_TYPE );
		}

		$objCheckItem['MerchantTransId'] = trim( $objCheckItem['MerchantTransId'] );
		$objCheckItem['Amt']  	= trim( $objCheckItem['Amt'] );
		$objCheckItem['Amt'] 	= substr( $objCheckItem['Amt'], 0, ( strlen( $objCheckItem['Amt'] ) - 2 ) ) . '.' . substr( $objCheckItem['Amt'], -2, 2 );
		$objCheckItem['AcctNum'] = trim( $objCheckItem['AcctNum'] );
		$objCheckItem['RTN'] 	= trim( $objCheckItem['RTN'] );
		$objCheckItem['ChkNum']  = trim( $objCheckItem['ChkNum'] );

		if( 0 < strlen( $objCheckItem['MerchantTransId'] ) && 99 < $objCheckItem['MerchantTransId'] ) {
			$this->setPaymentNumber( ( string ) $objCheckItem['MerchantTransId'] );
		} else {
			$this->setPaymentNumber( NULL );
		}

		if( 0 < strlen( $objCheckItem['Amt'] ) ) {
			$this->setAmount( ( float ) $objCheckItem['Amt'] );
		} else {
			$this->setAmount( NULL );
		}

		if( 0 < strlen( $objCheckItem['AcctNum'] ) ) {
			$this->setCheckAccountNumber( ( string ) $objCheckItem['AcctNum'] );
		} else {
			$this->setCheckAccountNumber( NULL );
		}

		if( 0 < strlen( $objCheckItem['RTN'] ) ) {
			$this->setCheckRoutingNumber( ( string ) $objCheckItem['RTN'] );
		} else {
			$this->setCheckRoutingNumber( NULL );
		}

		if( 0 < strlen( $objCheckItem['ChkNum'] ) ) {
			$this->setCheckNumber( ( string ) $objCheckItem['ChkNum'] );
		} else {
			$this->setCheckNumber( NULL );
		}
    }

	public function setBankReturnItemData( $arrmixReturnData, $objAdminDatabase ) {

		if( true == isset( $this->m_arrintReturnTypesMapping[$arrmixReturnData[self::RETURN_DATA_FIELD_RETURN_TYPE]] ) ) {
			$objReturnType = CReturnTypes::fetchReturnTypeById( ( string ) $this->m_arrintReturnTypesMapping[$arrmixReturnData[self::RETURN_DATA_FIELD_RETURN_TYPE]], $objAdminDatabase );

			if( true == valObj( $objReturnType, 'CReturnType' ) ) {
				$this->setReturnTypeId( $objReturnType->getId() );
			}
		}

		if( true == is_null( $this->getReturnTypeId() ) ) {
			$this->setReturnTypeId( CReturnType::GENERIC_CHECK21_RETURN_TYPE );
		}

		if( 0 < strlen( $arrmixReturnData[self::RETURN_DATA_FIELD_PROCESSING_DATE] ) ) {
			$this->setProcessingDate( ( string ) $arrmixReturnData[self::RETURN_DATA_FIELD_PROCESSING_DATE] );
		}

		if( 0 < strlen( $arrmixReturnData[self::RETURN_DATA_FIELD_PAYMENT_NUMBER] ) ) {
			$this->setPaymentNumber( ( string ) $arrmixReturnData[self::RETURN_DATA_FIELD_PAYMENT_NUMBER] );
		}

		if( 0 < strlen( $arrmixReturnData[self::RETURN_DATA_FIELD_AMOUNT] ) ) {
			$this->setAmount( ( float ) $arrmixReturnData[self::RETURN_DATA_FIELD_AMOUNT] );
		}

		if( 0 < strlen( $arrmixReturnData[self::RETURN_DATA_FIELD_ROUTING_NUMBER] ) ) {
			$this->setCheckRoutingNumber( ( string ) $arrmixReturnData[self::RETURN_DATA_FIELD_ROUTING_NUMBER] );
		}

		if( 0 < strlen( $arrmixReturnData[self::RETURN_DATA_FIELD_ACCOUNT_NUMBER] ) ) {
			$this->setCheckAccountNumber( ( string ) $arrmixReturnData[self::RETURN_DATA_FIELD_ACCOUNT_NUMBER] );
		}

		if( 0 < strlen( $arrmixReturnData[self::RETURN_DATA_FIELD_CHECK_NUMBER] ) ) {
			$this->setCheckNumber( ( string ) $arrmixReturnData[self::RETURN_DATA_FIELD_CHECK_NUMBER] );
		}
	}

	public function associateArPayment( $objPaymentDatabase ) {

		if( false == is_null( $this->getPaymentNumber() ) ) {
			$objArPayment = CArPayments::fetchArPayment( 'SELECT * FROM view_ar_payments  AS vcp WHERE vcp.payment_status_type_id IN ( ' . CPaymentStatusType::CAPTURED . ', ' . CPaymentStatusType::REVERSED . ') AND vcp.payment_type_id = ' . CPaymentType::CHECK_21 . ' AND vcp.id = ' . ( int ) $this->getPaymentNumber(), $objPaymentDatabase );
		} else {
			if( !valStr( $this->getCheckAccountNumber() ) ) {
				return false;
			}

			$strCheckAccountNumberBindex = \Psi\Libraries\Cryptography\CCrypto::createService()->blindindex( $this->getCheckAccountNumber(), CONFIG_SODIUM_KEY_BLIND_INDEX );

			$strSql = 'SELECT
								*
			           FROM
			           		view_ar_payments AS vcp
			           WHERE
			           		vcp.payment_type_id = ' . CPaymentType::CHECK_21 . '
			           AND  vcp.payment_status_type_id IN ( ' . CPaymentStatusType::CAPTURED . ', ' . CPaymentStatusType::REVERSED . ')
					   AND 	vcp.check_account_number_bindex = \'' . $strCheckAccountNumberBindex . '\'
					   AND  vcp.check_routing_number LIKE \'' . $this->getCheckRoutingNumber() . '\'
					   AND  vcp.payment_amount = \'' . $this->getAmount() . '\'
					   ORDER BY vcp.created_on
					   LIMIT 2';

			$arrobjArPayments = CArPayments::fetchArPayments( $strSql, $objPaymentDatabase );

			if( true == valArr( $arrobjArPayments, 2 ) ) {
				return false;
			} elseif( true == valArr( $arrobjArPayments ) && 1 == \Psi\Libraries\UtilFunctions\count( $arrobjArPayments ) ) {
				$objArPayment = array_pop( $arrobjArPayments );
			}
		}

		if( false == valObj( $objArPayment, 'CArPayment' ) ) {
			return false;
		}

		$this->setArPayment( $objArPayment );
		$this->setArPaymentId( $objArPayment->getId() );
		$this->setCid( $objArPayment->getCid() );
		$this->setConfirmedOn( date( 'm/d/Y H:i:s' ) );

		return true;
	}

	public function createReconciliationEntry( $objPaymentDatabase ) {

		$objReconciliationEntry = new CReconciliationEntry();
		$objReconciliationEntry->setProcessingBankAccountId( $this->getProcessingBankAccountId() );
		$objReconciliationEntry->setX937ReturnFileId( $this->getX937ReturnFileId() );
		$objReconciliationEntry->setArPaymentId( $this->getArPaymentId() );
		$objReconciliationEntry->setTotal( -1 * abs( $this->getAmount() ) );
		$objReconciliationEntry->setReconciliationEntryTypeId( CReconciliationEntryType::AR_PAYMENTS_CHECK_21 );

		// Make sure that the return date is not in the common.transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.

		$intCreatedOn = strtotime( $this->getCreatedOn() );

		$arrobjTransactionHolidays = ( array ) \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y', $intCreatedOn ), date( 'm/d/Y', ( $intCreatedOn + ( 21 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = array();

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intCreatedOn = strtotime( '+1 day', $intCreatedOn );

		while( 'Sun' == date( 'D', $intCreatedOn ) || 'Sat' == date( 'D', $intCreatedOn ) || true == in_array( $intCreatedOn, $arrintProhibitedTransactionHolidays ) ) {
			$intCreatedOn = strtotime( '+1 day', $intCreatedOn );
		}

		$objReconciliationEntry->setMovementDatetime( date( 'm/d/Y', $intCreatedOn ) );

		return $objReconciliationEntry;
	}

}

?>