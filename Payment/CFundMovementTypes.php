<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CFundMovementTypes
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CFundMovementTypes extends CBaseFundMovementTypes {

	public static function fetchFundMovementTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CFundMovementType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchFundMovementType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CFundMovementType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllFundMovementTypes( $objPaymentDatabase, $boolPublished = true ) {

		$strSql = 'SELECT * FROM fund_movement_types';

		if( true == $boolPublished )	$strSql .= ' WHERE is_published = 1 ';

		$strSql	.= ' ORDER BY order_num';

		return parent::fetchFundMovementTypes( $strSql, $objPaymentDatabase );
	}

}
?>