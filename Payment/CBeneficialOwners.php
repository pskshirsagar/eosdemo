<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CBeneficialOwners
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CBeneficialOwners extends CBaseBeneficialOwners {

	// TODO eventually -soon- we should store application_id in beneficial_owners table, and rewrite this query

	public function fetchBeneficialOwnersByApplicationId( $intCid, $intMerchantAccountApplicationId, $objDatabase ) {
			$strSql = 'SELECT bo.* FROM beneficial_owners bo
					WHERE bo.id IN 
					( SELECT akv.value::int
						FROM
							application_key_values akv
						WHERE
							akv.cid = ' . ( int ) $intCid . '
							AND akv.merchant_account_application_id = ' . ( int ) $intMerchantAccountApplicationId . '
							AND akv.key LIKE \'BENEFICIAL_OWNER_LINKING_ID%\'
							AND akv.value IS NOT NULL
					)
					AND bo.deleted_on IS NULL
					ORDER BY updated_on';
			return $this->fetchBeneficialOwners( $strSql, $objDatabase );

	}

}
?>