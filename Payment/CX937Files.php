<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937Files
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */
class CX937Files extends CBaseX937Files {

	public static function fetchPaginatedX937Files( $intPageNo, $intPageSize, $objPaymentDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    	$intLimit = ( int ) $intPageSize;

    	$strSql = 'SELECT nf.* FROM
    					x937_files nf
    			   ORDER BY
    			   		nf.created_on DESC
			   		OFFSET ' . ( int ) $intOffset . '
			   		LIMIT ' . $intLimit;

    	return self::fetchX937Files( $strSql, $objPaymentDatabase );
	}

	public static function fetchPaginatedX937FilesCount( $objPaymentDatabase ) {

    	$strSql = 'SELECT count(id) FROM x937_files';

    	$arrintResponse = fetchData( $strSql, $objPaymentDatabase );

    	if ( true == isset ( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

    	return 0;
	}

	public static function fetchDailyX937FilesByDate( $strDate, $objPaymentDatabase ) {

	    $strSql = 'SELECT xf.*, ( xf.control_file_total_amount::numeric / 100::numeric ) AS total_amount
	   				FROM
    					x937_files xf
    			   WHERE
    			   		DATE_TRUNC( \'day\', created_on ) = \'' . $strDate . '\'
    			   ORDER BY
    			   		xf.created_on DESC';

	    return self::fetchX937Files( $strSql, $objPaymentDatabase );
	}

	public static function fetchDailyX937FilesByProcessingBankIdByDate( $intProcessingBankId, $strDate, $objPaymentDatabase ) {

	    $strSql = 'SELECT
	    				xf.*,
	    				( xf.control_file_total_amount::numeric / 100::numeric ) AS total_amount
	    			FROM
    					x937_files xf
    			   WHERE
    			   		DATE_TRUNC( \'day\', created_on ) = \'' . $strDate . '\'
    			   		AND processing_bank_id = ' . ( int ) $intProcessingBankId . '
    			   ORDER BY
    			   		xf.created_on DESC';

	    return self::fetchX937Files( $strSql, $objPaymentDatabase );
	}

	public static function fetchSingleX937FileById( $intX937FileId, $objPaymentDatabase ) {

	    $strSql = 'SELECT
	    				xf.*,
	    				( xf.control_file_total_amount::numeric / 100::numeric ) AS total_amount
	    			FROM
    					x937_files xf
    			   	WHERE
    			   		id = ' . ( int ) $intX937FileId . '
    			   	ORDER BY
    			   		xf.created_on DESC';

	    return self::fetchX937File( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnReconPreppedX937Files( $objPaymentDatabase ) {

		$strSql = 'SELECT
							xf.*,
							( xf.control_file_total_amount::numeric / 100::numeric ) AS total_amount
						FROM
							x937_files xf
						WHERE
							 xf.is_recon_prepped <> 1
							AND xf.confirmed_on IS NOT NULL
					LIMIT 10000';

		return self::fetchX937Files( $strSql, $objPaymentDatabase );
	}

	public static function fetchX937FilesByProcessingBankIdWithNullTransmittedOn( $intProcessingBankId, $objPaymentDatabase ) {

	    $strSql = 'SELECT
	    				xf.*
	    			FROM
    					x937_files xf
    			   WHERE
    			   		transmitted_on IS NULL
    			   		AND processing_bank_id = ' . ( int ) $intProcessingBankId . '
    			   ORDER BY
    			   		xf.id';

	    return self::fetchX937Files( $strSql, $objPaymentDatabase );
	}

	public static function fetchX937FilesByProcessingBankIdWithNullConfirmedOn( $intProcessingBankId, $objPaymentDatabase ) {

		$strSql = 'SELECT
	    				xf.*
	    			FROM
    					x937_files xf
    			   WHERE
    			   		confirmed_on IS NULL
    			   		AND processing_bank_id = ' . ( int ) $intProcessingBankId . '
    			   ORDER BY
    			   		xf.id';

		return self::fetchX937Files( $strSql, $objPaymentDatabase );
	}

	public static function fetchX937FilesByProcessingBankIdByCreatedOnDate( $intProcessingBankId, $strDate, $boolIsFetchConfirmedOn, $objPaymentDatabase ) {

		$strSql = 'SELECT';
				if( true === $boolIsFetchConfirmedOn ) {
					$strSql .= ' count(xf.id) as count';
				} else {
					$strSql .= ' xf.*';
				}

				$strSql .= ' FROM
								x937_files xf
							WHERE
								DATE_TRUNC( \'day\', created_on ) = \'' . $strDate . '\'
								AND xf.processing_bank_id = ' . ( int ) $intProcessingBankId;

				if( true === $boolIsFetchConfirmedOn ) {
					$strSql .= ' AND xf.confirmed_on IS NULL ';
				}

				if( true === $boolIsFetchConfirmedOn ) {
					$strSql .= ' GROUP BY
								xf.id';
				}

				$strSql .= ' ORDER BY
								xf.id';

		if( true === $boolIsFetchConfirmedOn ) {
			$arrintResponse = fetchData( $strSql, $objPaymentDatabase );
			if ( true == isset ( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
			return 0;
		} else {
			return self::fetchX937Files( $strSql, $objPaymentDatabase );
		}
	}

}
?>