<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937CheckDetailAddendumBRecords
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CX937CheckDetailAddendumBRecords extends CBaseX937CheckDetailAddendumBRecords {

    public static function fetchX937CheckDetailAddendumBRecordsByX937FileId( $intX937FileId, $objPaymentDatabase ) {

		$strSql = '	SELECT
						xcdabr.*
					FROM
						x937_check_detail_addendum_b_records xcdabr
    					INNER JOIN x937_check_detail_records xcdr ON xcdabr.x937_check_detail_record_id = xcdr.id
					WHERE
						xcdr.x937_file_id = ' . ( int ) $intX937FileId;

		return self::fetchX937CheckDetailAddendumBRecords( $strSql, $objPaymentDatabase );
	}

}
?>