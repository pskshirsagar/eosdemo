<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937ViewDetailRecords
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CX937ViewDetailRecords extends CBaseX937ViewDetailRecords {

	public static function fetchX937ViewDetailRecordsByX937CheckDetailRecordId( $intX937CheckDetailRecordId, $objDatabase ) {
		return self::fetchX937ViewDetailRecords( sprintf( 'SELECT * FROM x937_view_detail_records WHERE x937_check_detail_record_id = %d', ( int ) $intX937CheckDetailRecordId ), $objDatabase );
	}

}
?>