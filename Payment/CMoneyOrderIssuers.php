<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMoneyOrderIssuers
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CMoneyOrderIssuers extends CBaseMoneyOrderIssuers {

	public static function fetchRecurringMoneyOrderIssuers( $intLowerCountThreshold, $objPaymentDatabase ) {

    		$strSql = 'SELECT
    						moi.id AS id,
    						pymts.check_account_number_encrypted,
    						pymts.check_routing_number,
    						pymts.occurrences,
    						CASE
    							WHEN moi.is_excluded = 1
    							THEN 1
    							ELSE 0
    						END AS is_excluded
    					FROM
    						( SELECT
								count(id) AS occurrences,
								check_account_number_encrypted,
							    check_routing_number
							FROM
								view_ar_payments arp
							WHERE
								check_is_money_order = 1
							    AND payment_type_id = ' . ( int ) CPaymentType::CHECK_21 . '
							    AND payment_status_type_id = ' . ( int ) CPaymentStatusType::CAPTURED . '
							GROUP BY
								check_account_number_encrypted,
							    check_routing_number
							) AS pymts
							LEFT OUTER JOIN money_order_issuers moi ON ( pymts.check_account_number_encrypted = moi.check_account_number_encrypted AND pymts.check_routing_number = moi.check_routing_number )
						WHERE
							pymts.occurrences >= ' . ( int ) $intLowerCountThreshold . '
						ORDER BY
							pymts.occurrences DESC';

    		return self::fetchMoneyOrderIssuers( $strSql, $objPaymentDatabase );
    }

}
?>