<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937CheckDetailAddendumARecords
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CX937CheckDetailAddendumARecords extends CBaseX937CheckDetailAddendumARecords {

    public static function fetchX937CheckDetailAddendumARecordsByX937FileId( $intX937FileId, $objPaymentDatabase ) {

		$strSql = '	SELECT
						xcdaar.*
					FROM
						x937_check_detail_addendum_a_records xcdaar
    					INNER JOIN x937_check_detail_records xcdr ON xcdaar.x937_check_detail_record_id = xcdr.id
					WHERE
						xcdr.x937_file_id = ' . ( int ) $intX937FileId;

		return self::fetchX937CheckDetailAddendumARecords( $strSql, $objPaymentDatabase );
	}

}
?>