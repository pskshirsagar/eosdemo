<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937ReturnImages
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CX937ReturnImages extends CBaseX937ReturnImages {

	public static function fetchX937ReturnImagesByX937ReturnItemIdByX937ReturnFileId( $intX937ReturnItemId, $intX937ReturnFileId, $objPaymentDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						x937_return_images
					WHERE
						x937_return_item_id = ' . ( int ) $intX937ReturnItemId . '
						AND x937_return_file_id	= ' . ( int ) $intX937ReturnFileId;

		return self::fetchX937ReturnImages( $strSql, $objPaymentDatabase );
	}

}

?>