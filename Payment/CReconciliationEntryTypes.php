<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CReconciliationEntryTypes
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CReconciliationEntryTypes extends CBaseReconciliationEntryTypes {

	public static function fetchReconciliationEntryTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CReconciliationEntryType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchReconciliationEntryType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CReconciliationEntryType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllReconciliationEntryTypes( $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM reconciliation_entry_types ORDER BY order_num';
		return self::fetchReconciliationEntryTypes( $strSql, $objPaymentDatabase );
	}

}
?>