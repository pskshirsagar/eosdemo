<?php

use Psi\Libraries\Cryptography\CCrypto;
use Psi\Eos\Admin\CAccounts;

class CCompanyMerchantAccount extends CBaseCompanyMerchantAccount {

	use TMerchantAccount;

	const CLIENT_BRE_PHONE	 	= 8888171539;
	const CLIENT_PSI_PHONE	 	= 8018775491;

	const PRIMARY_CONTACT_PHONE	 			= 8013755522;
	const MERCHANT_CATEGORY_CODE	 		= 6513;

	const ACCOUNT_VERIFICATION_TYPE_STORED_BILLING = 1;
	const ACCOUNT_VERIFICATION_TYPE_EVERY_PAYMENT = 2;

	const CANADA_DEBIT_DISCOUNT_RATE_CAP     = 0.0075;
	const CANADA_CREDIT_DISCOUNT_RATE_CAP     = 0.0175;

	const MAX_ID_LINDON = 99999999;  // 100 million - 1
	const MIN_ID_IRELAND = 100000000; // 100 million
	const MAX_ID_IRELAND = 199999999; // 200 million - 1

	protected $m_objClient;
	protected $m_objDistributionAccount;
	protected $m_objIntermediaryAccount;
	protected $m_objClearingAccount;
	protected $m_objDonationAccount;
	protected $m_objRebateAccount;
	protected $m_objReturnsAccount;
	protected $m_objCommercialMerchantAccount;

	protected $m_arrintEnabledGatewayTypeIds;

	protected $m_arrobjArTransactions;

	protected $m_fltAmountDue;

	protected $m_strUsePreexisting;

	// For use in Ach Customer Payments Script
	protected $m_arrobjPositiveArPayments;
	protected $m_arrobjNegativeArPayments;

	// For use in Ach Settlement Distributions Script
	protected $m_arrobjSettlingArPayments;
	protected $m_arrobjReturningArPayments;
	protected $m_arrobjReversingArPayments;
	protected $m_arrobjReturningReversedArPayments;

	// For use in Conveniece Fee Clearing Script
	protected $m_arrobjClearingTransactions;

	// This variable will get used while creating account
	protected $m_intEntityId;

	// This variable will be return when we are fetching all charge code merchant account types for a property
	protected $m_intArCodeId;

	// This variable is used to store the order of the default customer payment objects.  We have
	// to load the value on to the merchant account and then assign it over to the customer payment
	// while created in $this->createArPayment.
	protected $m_intOrderNum;

	// This variable is used while mapping / creating customer payments during dynamic payment splitting process.
	protected $m_intCaptureDelayDays;

	protected $m_intIsOverrideFeeAmountValidation;

	protected $m_strRoutingNumber;
	protected $m_strAccountNumberEncrypted;

	protected $m_objPaymentDatabase;
	protected $m_intPaymentMediumId;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjErrorMsgs = array();
		$this->m_fltAmountDue = 0;
		$this->m_intIsOverrideFeeAmountValidation = 0;

		$this->m_boolAllowDifferentialUpdate = false;
	}

	// region Add Functions

	public function addPositiveArPayment( $objPositiveArPayment ) {
		$this->m_arrobjPositiveArPayments[$objPositiveArPayment->getId()] = $objPositiveArPayment;
	}

	public function addNegativeArPayment( $objNegativeArPayment ) {
		$this->m_arrobjNegativeArPayments[$objNegativeArPayment->getId()] = $objNegativeArPayment;
	}

	public function addSettlingArPayment( $objSettlingArPayment ) {
		$this->m_arrobjSettlingArPayments[$objSettlingArPayment->getId()] = $objSettlingArPayment;
	}

	public function addReturningArPayment( $objReturningArPayment ) {
		$this->m_arrobjReturningArPayments[$objReturningArPayment->getId()] = $objReturningArPayment;
	}

	public function addReversingArPayment( $objReversingArPayment ) {
		$this->m_arrobjReversingArPayments[$objReversingArPayment->getId()] = $objReversingArPayment;
	}

	public function addReturningReversedArPayment( $objReturningReversedArPayment ) {
		$this->m_arrobjReturningReversedArPayments[$objReturningReversedArPayment->getId()] = $objReturningReversedArPayment;
	}

	public function addClearingTransaction( $objClearingTransaction ) {
		$this->m_arrobjClearingTransactions[$objClearingTransaction->getId()] = $objClearingTransaction;
	}

	public function addArTransaction( $objArTransaction ) {
		$this->m_arrobjArTransactions[$objArTransaction->getId()] = $objArTransaction;
	}

	// endregion

	// region Get or Fetch Functions

	public function getOrFetchIntermediaryAccount( $objAdminDatabase ) {

		if( true == valObj( $this->m_objIntermediaryAccount, 'CAccount' ) ) {
			return $this->m_objIntermediaryAccount;
		} else {
			return $this->fetchIntermediaryAccount( $objAdminDatabase );
		}
	}

	public function getOrFetchReturnsAccount( $objAdminDatabase ) {

		if( true == valObj( $this->m_objReturnsAccount, 'CAccount' ) ) {
			return $this->m_objReturnsAccount;
		} else {
			return $this->fetchReturnsAccount( $objAdminDatabase );
		}
	}

	public function getOrFetchCommercialMerchantAccount( $objAdminDatabase ) {

		if( true == valObj( $this->m_objCommercialMerchantAccount, 'CCompanyMerchantAccount' ) ) {
			return $this->m_objCommercialMerchantAccount;
		} else {
			return $this->fetchCommercialMerchantAccount( $objAdminDatabase );
		}
	}

	// endregion

	// region Get Functions

	public function getCaptureDelayDays() {
		return $this->m_intCaptureDelayDays;
	}

	public function getPositiveArPayments() {
		return $this->m_arrobjPositiveArPayments;
	}

	public function getNegativeArPayments() {
		return $this->m_arrobjNegativeArPayments;
	}

	public function getSettlingArPayments() {
		return $this->m_arrobjSettlingArPayments;
	}

	public function getReturningArPayments() {
		return $this->m_arrobjReturningArPayments;
	}

	public function getReversingArPayments() {
		return $this->m_arrobjReversingArPayments;
	}

	public function getReturningReversedArPayments() {
		return $this->m_arrobjReturningReversedArPayments;
	}

	public function getClearingTransactions() {
		return $this->m_arrobjClearingTransactions;
	}

	public function getMaxMonthlyProcessAmount() {
		$intPaymentTypeId = ( $this->getCountryCode() == CCountry::CODE_CANADA ) ? CPaymentType::PAD : CPaymentType::ACH;
		return ( $this->methodIsEnabled( $intPaymentTypeId ) ) ? $this->getMethodByAmount( $intPaymentTypeId )->getMaxMonthlyProcessAmount() : 0.00;
	}

	public function getUsePreexisting() {
		return $this->m_strUsePreexisting;
	}

	public function getAmountDue() {
		return $this->m_fltAmountDue;
	}

	public function getClient() {
		return $this->m_objClient;
	}

	public function setPaymentDatabase( $objPaymentDatabase ) {
		$this->m_objPaymentDatabase = $objPaymentDatabase;
	}

	public function getPaymentDatabase() {
		return $this->m_objPaymentDatabase;
	}

	public function valPaymentDatabase() {
		return valObj( $this->m_objPaymentDatabase, CDatabase::class );
	}

	public function setPaymentMediumId( $intPaymentMediumId ) {
		$this->m_intPaymentMediumId = $intPaymentMediumId;
	}

	public function getPaymentMediumId() {
		if( is_null( $this->m_intPaymentMediumId ) ) {
			$this->setPaymentMediumId( CPaymentMedium::WEB );
		}

		return $this->m_intPaymentMediumId;
	}

	public function valPaymentMediumId() {
		return in_array( $this->getPaymentMediumId(), [ CPaymentMedium::WEB, CPaymentMedium::RECURRING, CPaymentMedium::TERMINAL ] );
	}

	public function setId( $intId ) {
		parent::setId( $intId );

		if( !valArr( $arrobjMerchantAccountMethods = $this->getMerchantMethods() ) ) return;

		array_walk_recursive( $arrobjMerchantAccountMethods, function( CMerchantAccountMethod $objMethod ) {
			$objMethod->setCompanyMerchantAccountId( $this->getId() );
		} );
	}

	public function getIsControlledDistribution() {
		$boolIsControlledDistribution = false;
		$arrobjMerchantAccountMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMerchantAccountMethods, function( CMerchantAccountMethod $objMethod ) use( &$boolIsControlledDistribution ) {
			$intPaymentTypeId = $this->getCountryCode() == CCountry::CODE_CANADA ? CPaymentType::PAD : CPaymentType::ACH;
			if( $intPaymentTypeId == $objMethod->getPaymentTypeId() ) {
				$boolIsControlledDistribution = $objMethod->getIsControlledDistribution();
			}
		} );
		return $boolIsControlledDistribution;
	}

	public function getCcDistributionDelayDays() {
		return ( $this->methodIsEnabled( CPaymentType::MASTERCARD ) ) ? $this->getMethodByAmount( CPaymentType::MASTERCARD )->getDistributionDelayDays() : 0;
	}

	public function getMaxPaymentAmount() {
		if( !in_array( $this->getCountryCode(), CCountry::$c_arrstrPaymentFacilitatorCountryCodes ) && CCurrency::CURRENCY_CODE_EUR == $this->getCurrencyCode() && $this->methodIsEnabled( CPaymentType::SEPA_DIRECT_DEBIT ) ) {
			return $this->getTopTierMethod( CPaymentType::SEPA_DIRECT_DEBIT )->getMaxPaymentAmount();
		}
		$intPaymentTypeId = ( $this->getCountryCode() == CCountry::CODE_CANADA ) ? CPaymentType::PAD : CPaymentType::ACH;
		return $this->getTopTierMethod( $intPaymentTypeId )->getMaxPaymentAmount();
	}

	public function getUndelayedSettlementCeiling() {
		$intPaymentTypeId = ( $this->getCountryCode() == CCountry::CODE_CANADA ) ? CPaymentType::PAD : CPaymentType::ACH;
		return ( $this->methodIsEnabled( $intPaymentTypeId ) ) ? $this->getMethodByAmount( $intPaymentTypeId )->getUndelayedSettlementCeiling() : 0;
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function getRoutingNumber() {
		return $this->m_strRoutingNumber;
	}

	public function getAccountNumberEncrypted() {
		return $this->m_strAccountNumberEncrypted;
	}

	public function getResidentReturnFee( $intPaymentTypeId ) {
		return ( $this->methodIsEnabled( $intPaymentTypeId ) ) ? $this->getMethodByAmount( $intPaymentTypeId )->getResidentReturnFee() : 0.00;
	}

	public function getMerchantGatewayId( $intPaymentTypeId ) {
		return ( $this->methodIsEnabled( $intPaymentTypeId ) ) ? $this->getMethodByAmount( $intPaymentTypeId )->getMerchantGatewayId() : 0;
	}

	public function getAcceptedPaymentTypeIds( $fltPaymentAmount = 0.0 ) {
		// Return empty result set if merchant account has been disabled.
		if( 1 == $this->getIsDisabled() ) {
			return NULL;
		}

		$arrintPaymentTypeIds = NULL;

		foreach( CPaymentType::$c_arrintMerchantAccountPaymentTypeIds as $intPaymentId ) {
			$objMethod = $this->getMethodByAmount( $intPaymentId, $fltPaymentAmount );
			if( is_null( $objMethod ) ) {
				continue;
			}
			if( true == valId( $objMethod->getId() ) && true == empty( $objMethod->getDeletedOn() ) ) {
				$arrintPaymentTypeIds[] = $intPaymentId;
			}
		}

		return $arrintPaymentTypeIds;
	}

	public function getCid() {
		if( true == valObj( $this->m_objClient, CClient::class ) ) {
			return $this->m_objClient->getId();
		} else {
			return parent::getCid();
		}
	}

	public function getCcReversalDelayDays() {
		// TODO: This needs an update either in functionality or how it is used, as reversal delay days can vary by amount
		return ( $this->methodIsEnabled( CPaymentType::MASTERCARD ) ) ? $this->getMethodByAmount( CPaymentType::MASTERCARD )->getReversalDelayDays() : 0;
	}

	public function getDistributionAccount() {
		return $this->m_objDistributionAccount;
	}

	public function getIntermediaryAccount() {
		return $this->m_objIntermediaryAccount;
	}

	public function getReturnsAccount() {
		return $this->m_objReturnsAccount;
	}

	public function getEntityId() {
		return $this->m_intEntityId;
	}

	public function getIsOverrideFeeAmountValidation() {
		return $this->m_intIsOverrideFeeAmountValidation;
	}

	public function getTaxNumber() {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getTaxNumberEncrypted() ) ) {
			return NULL;
		}

		return CCrypto::createService()->decrypt( $this->getTaxNumberEncrypted(), CONFIG_SODIUM_KEY_CHECK_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CHECK_TAX_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getEveryPaymentFee() {
		if( $this->methodIsEnabled( CPaymentType::ACH ) ) {
			return $this->getMethodByAmount( CPaymentType::ACH )->getEveryPaymentFee();
		}
		return 0;
	}

	public function getStoredBillingFee() {
		if( $this->methodIsEnabled( CPaymentType::ACH ) ) {
			return $this->getMethodByAmount( CPaymentType::ACH )->getStoredBillingFee();
		}
		return 0;
	}

	// endregion

	// region Set Functions

	public function setFundMovementTypeId( $intFundMovementTypeId ) {
		$this->m_intFundMovementTypeId = $intFundMovementTypeId;
	}

	public function setCaptureDelayDays( $intCaptureDelayDays ) {
		$this->m_intCaptureDelayDays = $intCaptureDelayDays;
	}

	public function setArTransactions( $arrobjArTransactions ) {
		$this->m_arrobjArTransactions = $arrobjArTransactions;
	}

	public function setDistributionAccount( $objDistributionAccount ) {
		$this->m_objDistributionAccount = $objDistributionAccount;
	}

	public function setCommercialMerchantAccount( $objCommercialMerchantAccount ) {
		$this->m_objCommercialMerchantAccount = $objCommercialMerchantAccount;
	}

	/**
	 * @return CCompanyMerchantAccount
	 */
	public function getCommercialMerchantAccount() {
		return $this->m_objCommercialMerchantAccount;
	}

	public function setIntermediaryAccount( $objIntermediaryAccount ) {
		$this->m_objIntermediaryAccount = $objIntermediaryAccount;
	}

	public function setReturnsAccount( $objReturnsAccount ) {
		$this->m_objReturnsAccount = $objReturnsAccount;
	}

	public function setEntityId( $intEntityId ) {
		$this->m_intEntityId = $intEntityId;
	}

	public function setRoutingNumber( $strRoutingNumber ) {
		$this->m_strRoutingNumber = $strRoutingNumber;
	}

	public function setAccountNumberEncrypted( $strAccountNumberEncrypted ) {
		$this->m_strAccountNumberEncrypted = $strAccountNumberEncrypted;
	}

	public function setTaxNumber( $strTaxNumber ) {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $strTaxNumber ) || true == stristr( $strTaxNumber, 'X' ) ) return;

		$strTaxNumber = CStrings::strTrimDef( $strTaxNumber, 20, NULL, true );
		$this->setTaxNumberEncrypted( CCrypto::createService()->encrypt( $strTaxNumber, CONFIG_SODIUM_KEY_CHECK_TAX_NUMBER ) );
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['cc_reversal_delay_days'] ) ) 				$this->setCcReversalDelayDays( $arrstrValues['cc_reversal_delay_days'] );
		if( true == isset( $arrstrValues['cc_cut_off_time'] ) ) 					$this->setCcCutOffTime( $arrstrValues['cc_cut_off_time'] );
		if( true == isset( $arrstrValues['cc_distribution_delay_days'] ) )			$this->setCcDistributionDelayDays( $arrstrValues['cc_distribution_delay_days'] );

		if( true == isset( $arrstrValues['undelayed_settlement_ceiling'] ) ) 		$this->setUndelayedSettlementCeiling( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['undelayed_settlement_ceiling'] ) : $arrstrValues['undelayed_settlement_ceiling'] );
		if( true == isset( $arrstrValues['max_monthly_process_amount'] ) ) 			$this->setMaxMonthlyProcessAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['max_monthly_process_amount'] ) : $arrstrValues['max_monthly_process_amount'] );

		if( true == isset( $arrstrValues['use_preexisting'] ) )    					$this->setUsePreexisting( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['use_preexisting'] ) : $arrstrValues['use_preexisting'] );
		if( true == isset( $arrstrValues['amount_due'] ) ) 							$this->setAmountDue( $arrstrValues['amount_due'] );
		if( true == isset( $arrstrValues['ar_code_id'] ) ) 							$this->setArCodeId( $arrstrValues['ar_code_id'] );
		if( true == isset( $arrstrValues['is_controlled_distribution'] ) ) 			$this->setIsControlledDistribution( $arrstrValues['is_controlled_distribution'] );
		if( true == isset( $arrstrValues['override_fee_amount_validation'] ) )      $this->setIsOverrideFeeAmountValidation( $arrstrValues['override_fee_amount_validation'] );

		if( true == isset( $arrstrValues['routing_number'] ) )						$this->setRoutingNumber( $arrstrValues['routing_number'] );
		if( true == isset( $arrstrValues['account_number_encrypted'] ) )			$this->setAccountNumberEncrypted( $arrstrValues['account_number_encrypted'] );
		if( true == isset( $arrstrValues['tax_number'] ) ) 						$this->setTaxNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['tax_number'] ) : $arrstrValues['tax_number'] );

		if( true == isset( $arrstrValues['every_payment_fee'] ) )				$this->setEveryPaymentFee( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['every_payment_fee'] ) : $arrstrValues['every_payment_fee'] );
		if( true == isset( $arrstrValues['stored_billing_fee'] ) )				$this->setStoredBillingFee( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['stored_billing_fee'] ) : $arrstrValues['stored_billing_fee'] );

		if( true == isset( $arrstrValues['taba_pay_client_id'] ) ) $this->setTabaPayClientId( $arrstrValues['taba_pay_client_id'] );
		if( true == isset( $arrstrValues['taba_pay_convenience_fee'] ) ) $this->setTabaPayConvenienceFee( $arrstrValues['taba_pay_convenience_fee'] );
		if( true == isset( $arrstrValues['taba_pay_address_line_1'] ) ) $this->setTabaPayAddressLine1( $arrstrValues['taba_pay_address_line_1'] );
		if( true == isset( $arrstrValues['taba_pay_address_line_2'] ) ) $this->setTabaPayAddressLine2( $arrstrValues['taba_pay_address_line_2'] );
		if( true == isset( $arrstrValues['taba_pay_city'] ) ) $this->setTabaPayCity( $arrstrValues['taba_pay_city'] );
		if( true == isset( $arrstrValues['taba_pay_state'] ) ) $this->setTabaPayState( $arrstrValues['taba_pay_state'] );
		if( true == isset( $arrstrValues['taba_pay_postal_code'] ) ) $this->setTabaPayPostalCode( $arrstrValues['taba_pay_postal_code'] );
		if( true == isset( $arrstrValues['taba_pay_county_code'] ) ) $this->setTabaPayCountyCode( $arrstrValues['taba_pay_county_code'] );
		if( true == isset( $arrstrValues['worldpay_sub_merchant_id'] ) ) $this->setWorldpaySubMerchantId( $arrstrValues['worldpay_sub_merchant_id'] );
		if( true == isset( $arrstrValues['amex_seller_id'] ) ) $this->setAmexSellerId( $arrstrValues['amex_seller_id'] );
		$intPlaidAccountVerificationVal = ( 5 == ( $arrstrValues['merchant_processing_type_id'] ?? NULL ) || 7 == ( $arrstrValues['merchant_processing_type_id'] ?? NULL ) ) ? 0 : $arrstrValues['plaid_account_verification'] ?? NULL;
		if( true == isset( $arrstrValues['plaid_account_verification'] ) ) $this->setPlaidAccountVerification( $intPlaidAccountVerificationVal );
		$intPlaidFundVerificationVal = ( $intPlaidAccountVerificationVal && $arrstrValues['plaid_fund_verification'] ) ? $arrstrValues['plaid_fund_verification'] : 0;
		if( true == isset( $arrstrValues['plaid_fund_verification'] ) ) $this->setPlaidFundVerification( $intPlaidFundVerificationVal );
		if( true == isset( $arrstrValues['delay_predicted_payment_returns'] ) ) $this->setDelayPredictedPaymentReturns( ( bool ) $arrstrValues['delay_predicted_payment_returns'] );
		if( true == isset( $arrstrValues['enable_gateway_timeout_retries'] ) ) $this->setEnableGatewayTimeoutRetries( ( bool ) $arrstrValues['enable_gateway_timeout_retries'] );

		// set values on merchant account methods
		$arrintUpdatedPaymentTypeIds = [];
		foreach( $arrstrValues as $strKey => $strValue ) {
			$strPaymentType = explode( '_', $strKey )[0];
			$intPaymentTypeId = CPaymentType::stringToId( ucfirst( $strPaymentType ) );
			if( false == is_null( $intPaymentTypeId ) && false == in_array( $intPaymentTypeId, $arrintUpdatedPaymentTypeIds ) ) {
				$arrintUpdatedPaymentTypeIds[] = $intPaymentTypeId;
			}
		}

		$arrobjMethods = $this->getMerchantMethods();

		/**
		 * Sets up dynamic gateway data based on gateway to payment type mapping
		 */
		foreach( CGatewayType::$c_arrintMerchantAccountGatewayTypes as $intGatewayTypeId ) {
			$arrintPaymentTypeIds = CPaymentType::$c_arrmixPaymentTypesByGatewayTypeId[$intGatewayTypeId];
			$strGatewayTypePrefix = CGatewayType::idToString( $intGatewayTypeId );

			array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( $arrintUpdatedPaymentTypeIds, $arrintPaymentTypeIds, $strGatewayTypePrefix, $arrstrValues, $boolStripSlashes ) {
				if( !in_array( $objMethod->getPaymentTypeId(), $arrintUpdatedPaymentTypeIds ) || !in_array( $objMethod->getPaymentTypeId(), $arrintPaymentTypeIds ) ) return;

				if( true == isset( $arrstrValues[$strGatewayTypePrefix . '_gateway_username'] ) ) 				$objMethod->setGatewayUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues[$strGatewayTypePrefix . '_gateway_username'] ) : $arrstrValues[$strGatewayTypePrefix . '_gateway_username'] );
				if( true == isset( $arrstrValues[$strGatewayTypePrefix . '_gateway_password'] ) ) 				$objMethod->setGatewayPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues[$strGatewayTypePrefix . '_gateway_password'] ) : $arrstrValues[$strGatewayTypePrefix . '_gateway_password'] );
				if( true == isset( $arrstrValues[$strGatewayTypePrefix . '_login_username'] ) ) 				$objMethod->setLoginUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues[$strGatewayTypePrefix . '_login_username'] ) : $arrstrValues[$strGatewayTypePrefix . '_login_username'] );
				if( true == isset( $arrstrValues[$strGatewayTypePrefix . '_login_password'] ) ) 				$objMethod->setLoginPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues[$strGatewayTypePrefix . '_login_password'] ) : $arrstrValues[$strGatewayTypePrefix . '_login_password'] );
				if( true == isset( $arrstrValues[$strGatewayTypePrefix . '_max_payment_amount'] ) ) 			$objMethod->setMaxPaymentAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues[$strGatewayTypePrefix . '_max_payment_amount'] ) : $arrstrValues[$strGatewayTypePrefix . '_max_payment_amount'] );
			} );
		}
	}

	public function setOrderNum( $intOrderNum ) {
		$this->m_intOrderNum = $intOrderNum;
	}

	public function setAchMaxPaymentAmount( $fltMaxPaymentAmount ) {
		if( $this->methodIsEnabled( CPaymentType::ACH ) ) {
			$this->getMethodByAmount( CPaymentType::ACH )->setMaxPaymentAmount( $fltMaxPaymentAmount );
		}
	}

	public function setCh21MaxPaymentAmount( $fltMaxPaymentAmount ) {
		if( $this->methodIsEnabled( CPaymentType::CHECK_21 ) ) {
			$this->getMethodByAmount( CPaymentType::CHECK_21 )->setMaxPaymentAmount( $fltMaxPaymentAmount );
		}
	}

	public function setEmoMaxPaymentAmount( $fltEmoMaxPaymentAmount ) {
		if( $this->methodIsEnabled( CPaymentType::EMONEY_ORDER ) ) {
			$this->getMethodByAmount( CPaymentType::EMONEY_ORDER )->setMaxPaymentAmount( $fltEmoMaxPaymentAmount );
		}
	}

	public function setAchWaivedDebitDiscountRate( $fltAchWaivedDebitDiscountRate ) {
		if( $this->methodIsEnabled( CPaymentType::ACH ) ) {
			$this->getMethodByAmount( CPaymentType::ACH )->setWaivedDebitDiscountRate( $fltAchWaivedDebitDiscountRate );
		}
	}

	public function setCh21WaivedDebitDiscountRate( $fltCh21WaivedDebitDiscountRate ) {
		if( $this->methodIsEnabled( CPaymentType::CHECK_21 ) ) {
			$this->getMethodByAmount( CPaymentType::CHECK_21 )->setWaivedDebitDiscountRate( $fltCh21WaivedDebitDiscountRate );
		}
	}

	public function setEmoWaivedDebitDiscountRate( $fltEmoWaivedDebitDiscountRate ) {
		if( $this->methodIsEnabled( CPaymentType::EMONEY_ORDER ) ) {
			$this->getMethodByAmount( CPaymentType::EMONEY_ORDER )->setWaivedDebitDiscountRate( $fltEmoWaivedDebitDiscountRate );
		}
	}

	public function setCcMaxPaymentAmount( $fltCcMaxPaymentAmount ) {
		// Visa is purposely not set here.  It is set independently
		//  unless visa pilot is enabled
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( $fltCcMaxPaymentAmount ) {
			if( in_array( $objMethod->getPaymentTypeId(), [ CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX ] ) ) {
				$objMethod->setMaxPaymentAmount( $fltCcMaxPaymentAmount );
			}
		} );
	}

	public function setUsePreexisting( $strUsePreexisting ) {
		$this->m_strUsePreexisting = CStrings::strTrimDef( $strUsePreexisting, 20, NULL, true );
	}

	public function setAmountDue( $fltAmountDue ) {
		$this->m_fltAmountDue = CStrings::strToFloatDef( $fltAmountDue, NULL, true, 4 );
	}

	public function setArCodeId( $intArCodeId ) {
		$this->m_intArCodeId = $intArCodeId;
	}

	public function setClient( $objClient ) {
		if( false == valObj( $objClient, CClient::class ) ) {
			trigger_error( 'Incompatible types: \'CClient\' and \'' . get_class( $objClient ) . '\' - CCompanyMerchantAccount::setClient()', E_USER_ERROR );
			return false;
		}

		$this->m_objClient = $objClient;
		return true;
	}

	public function setIsControlledDistribution( $boolIsControlledDistribution ) {
		$arrobjMerchantAccountMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMerchantAccountMethods, function( CMerchantAccountMethod $objMethod ) use( $boolIsControlledDistribution ) {
			$arrintPaymentTypes = [ CPaymentType::ACH, CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX, CPaymentType::CHECK_21, CPaymentType::EMONEY_ORDER, CPaymentType::PAD ];
			if( in_array( $objMethod->getPaymentTypeId(), $arrintPaymentTypes, true ) ) {
				$objMethod->setIsControlledDistribution( $boolIsControlledDistribution );
			}
		} );
	}

	public function setCcDistributionDelayDays( $intDistributionDelayDays ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( $intDistributionDelayDays ) {
			if( in_array( $objMethod->getPaymentTypeId(), [CPaymentType::VISA,CPaymentType::MASTERCARD,CPaymentType::DISCOVER,CPaymentType::AMEX] ) ) {
				$objMethod->setDistributionDelayDays( $intDistributionDelayDays );
			}
		} );
	}

	public function setNullNonCopiedFields() {
		$this->setId( NULL );
		$this->setDistributionAccountId( NULL );
		$this->setBillingAccountId( NULL );
		$this->setDonationAccountId( NULL );
		$this->setRebateAccountId( NULL );
		$this->setReturnsAccountId( NULL );
		$this->setAccountName( NULL );
		$this->setDescription( NULL );
		$this->setNotes( NULL );
		$this->setIsDisabled( NULL );
		$this->setExternalLegalEntityId( NULL );
		$this->setAePin( NULL );
	}

	public function setCid( $intCid ) {
		parent::setCid( $intCid );
		// If we are setting the client id directly we unbind the
		// client object to avoid any concurrency issues
		$this->m_objClient = NULL;
	}

	public function setCcTransactionFee( $fltCcTransactionFee ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( $fltCcTransactionFee ) {
			if( in_array( $objMethod->getPaymentTypeId(), [CPaymentType::VISA,CPaymentType::MASTERCARD,CPaymentType::DISCOVER,CPaymentType::AMEX] ) ) {
				$objMethod->setTransactionFee( $fltCcTransactionFee );
			}
		} );
	}

	public function setCcGatewayUsername( $strGatewayUsername ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( $strGatewayUsername ) {
			if( in_array( $objMethod->getPaymentTypeId(), [CPaymentType::VISA,CPaymentType::MASTERCARD,CPaymentType::DISCOVER,CPaymentType::AMEX] ) ) {
				$objMethod->setGatewayUsername( $strGatewayUsername );
			}
		} );

		// store the decrypted mid in details for reporting
		$this->setCcGatewayMid( $strGatewayUsername );
	}

	public function setMaxPaymentAmount( $fltMaxPaymentAmount ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( $fltMaxPaymentAmount ) {
			if( in_array( $objMethod->getPaymentTypeId(), [CPaymentType::ACH,CPaymentType::PAD,CPaymentType::MASTERCARD,CPaymentType::DISCOVER,CPaymentType::AMEX,CPaymentType::CHECK_21,CPaymentType::EMONEY_ORDER] ) ) {
				$objMethod->setMaxPaymentAmount( $fltMaxPaymentAmount );
			}
		} );
	}

	public function setUndelayedSettlementCeiling( $fltUndelayedSettlementCeiling ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( $fltUndelayedSettlementCeiling ) {
			if( in_array( $objMethod->getPaymentTypeId(), [ CPaymentType::ACH, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX, CPaymentType::CHECK_21, CPaymentType::EMONEY_ORDER, CPaymentType::PAD ] ) ) {
				$objMethod->setUndelayedSettlementCeiling( $fltUndelayedSettlementCeiling );
			}
		} );
	}

	public function setMaxMonthlyProcessAmount( $fltMaxMonthlyProcessAmount ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( $fltMaxMonthlyProcessAmount ) {
			if( in_array( $objMethod->getPaymentTypeId(), CPaymentType::$c_arrintMerchantAccountPaymentTypeIds ) ) {
				$objMethod->setMaxMonthlyProcessAmount( $fltMaxMonthlyProcessAmount );
			}
		} );
	}

	public function setCcReversalDelayDays( $intCcReversalDelayDays ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( $intCcReversalDelayDays ) {
			if( in_array( $objMethod->getPaymentTypeId(), [CPaymentType::VISA,CPaymentType::MASTERCARD,CPaymentType::DISCOVER,CPaymentType::AMEX] ) ) {
				$objMethod->setReversalDelayDays( $intCcReversalDelayDays );
			}
		} );
	}

	public function setCcCutOffTime( $fltCcCutOffTime ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( $fltCcCutOffTime ) {
			if( in_array( $objMethod->getPaymentTypeId(), [CPaymentType::VISA,CPaymentType::MASTERCARD,CPaymentType::DISCOVER,CPaymentType::AMEX] ) ) {
				$objMethod->setCutOffTime( $fltCcCutOffTime );
			}
		} );
	}

	public function setEveryPaymentFee( $fltEveryPaymentFee ) {
		if( $this->methodIsEnabled( CPaymentType::ACH ) ) {
			$this->getMethodByAmount( CPaymentType::ACH )->setEveryPaymentFee( $fltEveryPaymentFee );
		}
	}

	public function setStoredBillingFee( $fltStoredBillingFee ) {
		if( $this->methodIsEnabled( CPaymentType::ACH ) ) {
			$this->getMethodByAmount( CPaymentType::ACH )->setStoredBillingFee( $fltStoredBillingFee );
		}
	}

	public function setDefaults( $intPaymentMediumId = CPaymentMedium::WEB, $objManageMerchantAccounts = NULL ) {

		$this->setDistributionGroupTypeId( CDistributionGroupType::ALL_TOGETHER );
		$this->setDistributionFailureFee( '25.00' );
		// This should be default now. PBY  04/20/2010
		$this->setProcessingBankId( CProcessingBank::ZIONS_BANK );
		$this->setFundMovementTypeId( CFundMovementType::BANK_INTERMEDIARY_MOVEMENT );
		$this->setIsOverrideFeeAmountValidation( 0 );
		$arrobjMerchantAccountMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMerchantAccountMethods, function( CMerchantAccountMethod $objMethod ) {
			$objMethod->applyDefaults();
		} );

		if( is_a( $objManageMerchantAccounts, 'CManageMerchantAccounts' ) ) {

			$objApplicationBankAccount		= $objManageMerchantAccounts->getApplicationBankAccount();
			$objMerchantAccountApplication	= $objManageMerchantAccounts->getMerchantAccountApplication();

			if( is_a( $objApplicationBankAccount, 'CApplicationBankAccount' ) ) {
				$this->setAccountName( 'CD - ' . $objApplicationBankAccount->getPropertyName() );
				array_walk_recursive( $arrobjMerchantAccountMethods, function( CMerchantAccountMethod $objMethod ) use ( $objApplicationBankAccount ) {
					if( $objMethod->getPaymentTypeId() == CPaymentType::AMEX ) {
						$objMethod->setPin( $objApplicationBankAccount->getAePin() );
					}
					if( $objMethod->getPaymentTypeId() == CPaymentType::DISCOVER ) {
						$objMethod->setPin( $objApplicationBankAccount->getDiPin() );
					}
				} );
			}

			if( is_a( $objMerchantAccountApplication, 'CMerchantAccountApplication' ) ) {
				$this->setMaxPaymentAmount( $objMerchantAccountApplication->getAchMaxAmountPerTransaction() );
				$this->setMaxMonthlyProcessAmount( $objMerchantAccountApplication->getCcMaxAmountPerMonth() + $objMerchantAccountApplication->getAchMaxAmountPerMonth() );
			}
		}
	}

	// This is a temperory function to set default values for existing records.

	public function setEMoneyOrderDefaults( $intPaymentMediumId = CPaymentMedium::WEB ) {
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( $intPaymentMediumId ) {
			if( CPaymentType::EMONEY_ORDER != $objMethod->getPaymentTypeId() ) return;
			$objMethod->setDistributionDelayDays( 2 );
			$objMethod->setReversalDelayDays( 7 );
			$objMethod->setCutOffTime( '15:30' );
			$objMethod->setAuthSuccessFee( .25 );
			$objMethod->setAuthFailureFee( 0 );
			$objMethod->setGatewayTransactionFee( .25 );
			$objMethod->setResidentTransactionFee( 0.00 );
			$objMethod->setTransactionFee( 1.50 );
			$objMethod->setReverseTransactionFee( 0.00 );
			$objMethod->setWaivedTransactionFee( 1.50 );
			$objMethod->setCompanyReturnFee( 0.00 );
			$objMethod->setCompanyAdjustmentFee( 0.00 );
			$objMethod->setDowngradeTransactionFee( 0 );
			$objMethod->setInternationalTransactionFee( 0 );
			$objMethod->setResidentReturnFee( 0.00 );
			$objMethod->setUnauthorizedReturnFee( 0.00 );
			$objMethod->setProviderDiscountRate( 0.00 );
			$objMethod->setResidentDiscountRate( 0.00 );
			$objMethod->setCompanyDiscountRate( 0.00 );
			$objMethod->setWaivedDiscountRate( 0 );
			$objMethod->setDowngradeDiscountRate( 0 );
			$objMethod->setInternationalDiscountRate( 0 );

			switch( $intPaymentMediumId ) {

				case CPaymentMedium::TERMINAL:
					$objMethod->setIsEnabled( 1 );
					break;

				case CPaymentMedium::RECURRING:
				default:
					$objMethod->setIsEnabled( 0 );
			}
		} );
	}

	// This function SHOULD  called after apply request form is done and before insert and update of company merchant account.

	public function setProcessingBankAccount() {

		if( false == in_array( $this->getFundMovementTypeId(), [ CFundMovementType::BANK_INTERMEDIARY_MOVEMENT, CFundMovementType::BANK_INTERMEDIARY_ANTICIPATORY ] ) ) {
			trigger_error( 'We only allows bank intermediary processing bank accounts.', E_USER_ERROR );
			exit;
		}

		$arrobjMerchantAccountMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMerchantAccountMethods, function( CMerchantAccountMethod $objMethod ) {
			switch( $this->getProcessingBankId() ) {
				case CProcessingBank::FIFTH_THIRD:
					if( CPaymentType::ACH == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( CProcessingBankAccount::FIFTH_THIRD_ACH );
					if( CPaymentType::CHECK_21 == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( CProcessingBankAccount::FIFTH_THIRD_CHECK21 );
					if( CPaymentType::VISA == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), CProcessingBank::ZIONS_BANK, $boolIsAnticipatory = false ) );
					if( CPaymentType::MASTERCARD == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), CProcessingBank::ZIONS_BANK, $boolIsAnticipatory = false ) );
					if( CPaymentType::DISCOVER == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), CProcessingBank::ZIONS_BANK, $boolIsAnticipatory = false ) );
					if( CPaymentType::AMEX == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), CProcessingBank::ZIONS_BANK, $boolIsAnticipatory = false ) );
					if( CPaymentType::EMONEY_ORDER == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( CProcessingBankAccount::ZIONS_CENTRAL_MONEYGRAM );
					break;

				case CProcessingBank::ZIONS_BANK:
					if( CPaymentType::ACH == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( CMerchantGateway::ZIONS_ACH, $boolIsAnticipatory = false ) );
					if( CPaymentType::CHECK_21 == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( CMerchantGateway::ZIONS_CHECK21, $boolIsAnticipatory = false ) );
					if( CPaymentType::VISA == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), $this->getProcessingBankId(), $boolIsAnticipatory = false ) );
					if( CPaymentType::MASTERCARD == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), $this->getProcessingBankId(), $boolIsAnticipatory = false ) );
					if( CPaymentType::DISCOVER == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), $this->getProcessingBankId(), $boolIsAnticipatory = false ) );
					if( CPaymentType::AMEX == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), $this->getProcessingBankId(), $boolIsAnticipatory = false ) );
					if( CPaymentType::EMONEY_ORDER == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( CMerchantGateway::MONEY_GRAM, $this->getProcessingBankId(), $boolIsAnticipatory = false ) );
					break;

				case CProcessingBank::BANK_OF_MONTREAL:
					if( CPaymentType::PAD == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( CMerchantGateway::WORLDPAY_DIRECT_DEBIT, CProcessingBank::BANK_OF_MONTREAL, $boolIsAnticipatory = false ) );
					if( CPaymentType::VISA == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), $this->getProcessingBankId(), $boolIsAnticipatory = false ) );
					if( CPaymentType::MASTERCARD == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), $this->getProcessingBankId(), $boolIsAnticipatory = false ) );
					if( CPaymentType::DISCOVER == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), $this->getProcessingBankId(), $boolIsAnticipatory = false ) );
					if( CPaymentType::AMEX == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), $this->getProcessingBankId(), $boolIsAnticipatory = false ) );
					break;

				case CProcessingBank::INTERNATIONAL:
					if( CPaymentType::VISA == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), $this->getProcessingBankId(), $boolIsAnticipatory = false ) );
					if( CPaymentType::MASTERCARD == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), $this->getProcessingBankId(), $boolIsAnticipatory = false ) );
					if( CPaymentType::DISCOVER == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), $this->getProcessingBankId(), $boolIsAnticipatory = false ) );
					if( CPaymentType::AMEX == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountIdForMerchantGatewayId( $objMethod->getMerchantGatewayId(), $this->getProcessingBankId(), $boolIsAnticipatory = false ) );
					if( CPaymentType::SEPA_DIRECT_DEBIT == $objMethod->getPaymentTypeId() ) $objMethod->setProcessingBankAccountId( CProcessingBankAccount::INTERNATIONAL );
					break;

				case CProcessingBank::FIRST_REGIONAL:
				default:
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Please choose another Processing Bank as we are no longer working with First Regional.', NULL ) );
					break;
			}
		} );
	}

	public function setIsOverrideFeeAmountValidation( $intIsOverrideFeeAmountValidation ) {
		$this->m_intIsOverrideFeeAmountValidation = $intIsOverrideFeeAmountValidation;
	}

	// endregion

	// region Create Functions

	public function createArPayment( $intPaymentTypeId = NULL ) {

		$objArPayment = new CArPayment();
		$objArPayment->setDefaults();
		$objArPayment->setCid( $this->getCid() );
		$objArPayment->setCompanyMerchantAccount( $this );
		$objArPayment->setPaymentStatusTypeId( CPaymentStatusType::PENDING );
		$objArPayment->setArCodeId( $this->getArCodeId() );
		$objArPayment->setCompanyMerchantAccountId( $this->getId() );
		$objArPayment->setCurrencyCode( $this->getCurrencyCode() );

		if( true == $this->getIsDisabled() ) {
			$objArPayment->addErrorMsg( new CErrorMsg( NULL, '', 'The merchant account through which this payment would normally process is not active.  Please try back later.' ) );
			return $objArPayment;
		}

		$intMerchantGatewayId = NULL;
		if( false == is_null( $intPaymentTypeId ) ) {
			$intMerchantGatewayId = ( $this->methodIsEnabled( $intPaymentTypeId ) ) ? $this->getMethodByAmount( $intPaymentTypeId )->getMerchantGatewayId() : NULL;
		}

		if( false == is_null( $intMerchantGatewayId ) ) {
			$objArPayment->setMerchantGatewayId( $intMerchantGatewayId );
		}

		if( true == is_numeric( $this->m_intOrderNum ) ) {
			$objArPayment->setOrderNum( $this->m_intOrderNum );
		}

		if( true == is_numeric( $this->m_fltAmountDue ) ) {
			$objArPayment->setPaymentAmount( $this->m_fltAmountDue );
		}

		if( true == isset( $this->m_arrobjArTransactions ) ) {
			$objArPayment->setArTransactions( $this->m_arrobjArTransactions );
		}

		// Add logic to calculate capture_delay_days date.
		if( true == is_numeric( $this->getCaptureDelayDays() ) && 0 < $this->getCaptureDelayDays() ) {
			$objArPayment->setAutoCaptureOn( date( 'm/d/Y H:i:s', strtotime( '+' . ( string ) $this->getCaptureDelayDays() . ' days' ) ) );
		}

		return $objArPayment;
	}

	/**
	 * @param CArPayment $objArPayment
	 * @param bool $boolIsOverrideGateway
	 * @return bool|CFirstRegionalGateway|CLitleGateway|CVantivGateway|CVitalGateway|CWorldpayWPGCcGateway|CZionsGateway|null
	 */
	public function createGateway( $objArPayment, $boolIsOverrideGateway = true ) {

		$objGateway = NULL;

		if( true == $boolIsOverrideGateway ) {
			$this->populateMerchantGatewayId( $objArPayment );
		}

		$strMerchantName = '';
		if( false == is_null( $this->getMerchantName() ) ) {
			$strMerchantName = $this->getMerchantName();
		} else {
			$strMerchantName = $this->getAccountName();
		}

		switch( $objArPayment->getMerchantGatewayId() ) {

			case CMerchantGateway::WORLDPAY_INTL_CREDIT_CARD:
				$objGateway = new CWorldpayWPGCcGateway();
				$objGateway->setMerchantName( $strMerchantName );
				// make sure the currency object is loaded before populating the gateway
				$objArPayment->getOrFetchCurrency( $this->getPaymentDatabase() );
				break;

			case CMerchantGateway::VANTIV_CREDIT_CARD:
				$objGateway = new CVantivGateway();
				$objGateway->setMerchantName( $strMerchantName );
				break;

			case CMerchantGateway::LITLE_CREDIT_CARD:
				$objGateway = new CLitleGateway();
				$objGateway->setMerchantName( $strMerchantName );
				break;

			case CMerchantGateway::VITAL:
				$objGateway = $this->generateVitalGateway();
				break;

			case CMerchantGateway::FIRST_REGIONAL_ACH:
			case CMerchantGateway::FIRST_REGIONAL_CHECK21:
				$objGateway = $this->generateFirstRegionalGateway();
				break;

			case CMerchantGateway::ZIONS_ACH:
			case CMerchantGateway::ZIONS_CHECK21:
				$objGateway = $this->generateZionsGateway();
				break;

			case CMerchantGateway::FIFTH_THIRD_ACH:
			case CMerchantGateway::FIFTH_THIRD_CHECK21:
				$objGateway = $this->generateFifthThirdGateway();
				break;

			case CMerchantGateway::FIRST_REGIONAL_WU_EMO:
				$objGateway = $this->generateWesternUnionGateway();
				break;

			case CMerchantGateway::MONEY_GRAM:
				$objGateway = $this->generateMoneyGramGateway();
				break;

			case CMerchantGateway::SEPA_DIRECT_DEBIT:
				$objGateway = \Psi\Core\Payment\Gateways\WorldpayWPG\CSepaDirectDebit::create();
				$objGateway->setMerchantAccount( $this );
				$objArPayment->getOrFetchCurrency( $this->getPaymentDatabase() );
				break;

			case CMerchantGateway::WORLDPAY_DIRECT_DEBIT:
				$objGateway = \Psi\Core\Payment\Gateways\WorldPay\CDirectDebit::create();
				break;

			default:
				trigger_error( 'Customer payment did not contain a valid merchant gateway id.', E_USER_ERROR );
				return false;
		}

		if( false == is_null( $objArPayment->getPaymentTypeId() ) ) {
			$objGateway = $this->determineMerchantGatewayCredentials( $objGateway, $objArPayment );
			if( $this->methodIsEnabled( $objArPayment->getPaymentTypeId() ) ) {
				$objGateway->setProcessingBankAccountId( $this->getMethodByAmount( $objArPayment->getPaymentTypeId() )->getProcessingBankAccountId() );
			}
		}

		if( true == isset ( $objGateway ) ) {
			$objGateway->populateGateway( $objArPayment );
		}

		return $objGateway;
	}

	public function determineMerchantGatewayCredentials( $objGateway, CArPayment $objArPayment ) {

		$objMethod = $this->getMethodByAmount( $objArPayment->getPaymentTypeId(), $objArPayment->getPaymentAmount() );

		if( CMerchantGateway::WORLDPAY_INTL_CREDIT_CARD != $objArPayment->getMerchantGatewayId() ) {
			if( !is_null( $objMethod ) ) {
				$objGateway->setGatewayUsername( $objMethod->getGatewayUsername() );
				$objGateway->setGatewayPassword( $objMethod->getGatewayPassword() );
			}

			return $objGateway;
		}

		$boolIsEntrata = ( CPsProduct::ENTRATA == $objArPayment->getPsProductId() ? true : false );
		$boolIsScheduledPayment = ( false == is_null( $objArPayment->getScheduledPaymentId() ) ? true : false );

		if( true == $boolIsScheduledPayment ) {
			if( CMerchantGateway::SEPA_DIRECT_DEBIT == $objArPayment->getMerchantGatewayId() && CPaymentType::SEPA_DIRECT_DEBIT == $objArPayment->getPaymentTypeId() ) {
				if( !is_null( $objMethod ) ) {
					$objGateway->setGatewayUsername( $objMethod->getWorldpayRecurringUsername() );
					$objGateway->setGatewayPassword( $objMethod->getWorldpayRecurringPassword() );
					$objGateway->setMerchantCode( $objMethod->getWorldpayRecurringMerchantCode() );
				}
				return $objGateway;
			}

			if( !is_null( $objMethod ) ) {
				$objGateway->setGatewayUsername( $objMethod->getWorldpayCcRecurringUsername() );
				$objGateway->setGatewayPassword( $objMethod->getWorldpayCcRecurringPassword() );
				$objGateway->setMerchantCode( $objMethod->getWorldpayCcRecurringMerchantCode() );
				$objGateway->setInteractionType( CWorldpayWPGCcGateway::RECR );
			}
			return $objGateway;
		}

		if( true == $boolIsEntrata || true == $objArPayment->getForceWorldpayMoto() ) {
			if( !is_null( $objMethod ) ) {
				$objGateway->setGatewayUsername( $objMethod->getWorldpayCcMotoUsername() );
				$objGateway->setGatewayPassword( $objMethod->getWorldpayCcMotoPassword() );
				$objGateway->setMerchantCode( $objMethod->getWorldpayCcMotoMerchantCode() );
				$objGateway->setInteractionType( CWorldpayWPGCcGateway::MOTO );
			}
			return $objGateway;
		}

		if( CMerchantGateway::SEPA_DIRECT_DEBIT == $objArPayment->getMerchantGatewayId() && CPaymentType::SEPA_DIRECT_DEBIT == $objArPayment->getPaymentTypeId() ) {
			if( !is_null( $objMethod ) ) {
				$objGateway->setGatewayUsername( $objMethod->getWorldpayEcomUsername() );
				$objGateway->setGatewayPassword( $objMethod->getWorldpayEcomPassword() );
				$objGateway->setMerchantCode( $objMethod->getWorldpayEcomMerchantCode() );
			}
			return $objGateway;
		}

		if( !is_null( $objMethod ) ) {
			$objGateway->setGatewayUsername( $objMethod->getWorldpayCcEcomUsername() );
			$objGateway->setGatewayPassword( $objMethod->getWorldpayCcEcomPassword() );
			$objGateway->setMerchantCode( $objMethod->getWorldpayCcEcomMerchantCode() );
			$objGateway->setInteractionType( CWorldpayWPGCcGateway::ECOM );
			$objGateway->set3dsFlexEnabled( $this->getWorldpay3dsFlexIsEnabled() );
		}

		return $objGateway;
	}

	public function getWorldpayCcGatewayCredentials() {
		$arrmixWorldpayCredentials = [];
		$objCcMethod = NULL;
		foreach( CPaymentType::$c_arrintCreditCardPaymentTypes as $intPaymentType ) {
			if( $this->methodIsEnabled( $intPaymentType ) ) {
				$objCcMethod = $this->getMethodByAmount( $intPaymentType );
				break;
			}
		}

		if( true == is_null( $objCcMethod ) ) {
			return [];
		}

		$arrmixWorldpayCredentials['worldpay_cc_moto_username'] = $objCcMethod->getWorldpayCcMotoUsername();
		$arrmixWorldpayCredentials['worldpay_cc_moto_password'] = $objCcMethod->getWorldpayCcMotoPassword();
		$arrmixWorldpayCredentials['worldpay_cc_moto_merchant_code'] = $objCcMethod->getWorldpayCcMotoMerchantCode();
		$arrmixWorldpayCredentials['worldpay_cc_ecom_username'] = $objCcMethod->getWorldpayCcEcomUsername();
		$arrmixWorldpayCredentials['worldpay_cc_ecom_password'] = $objCcMethod->getWorldpayCcEcomPassword();
		$arrmixWorldpayCredentials['worldpay_cc_ecom_merchant_code'] = $objCcMethod->getWorldpayCcEcomMerchantCode();
		$arrmixWorldpayCredentials['worldpay_cc_recurring_username'] = $objCcMethod->getWorldpayCcRecurringUsername();
		$arrmixWorldpayCredentials['worldpay_cc_recurring_password'] = $objCcMethod->getWorldpayCcRecurringPassword();
		$arrmixWorldpayCredentials['worldpay_cc_recurring_merchant_code'] = $objCcMethod->getWorldpayCcRecurringMerchantCode();

		return $arrmixWorldpayCredentials;
	}

	public function getSepaInfo() {
		$objSepaMethod = NULL;
		$arrmixSepaInfo = [];

		$objSepaMethod = $this->getMethodByAmount( CPaymentType::SEPA_DIRECT_DEBIT );

		if( true == is_null( $objSepaMethod ) ) {
			return $arrmixSepaInfo;
		}

		$arrmixSepaInfo['sepa_bic'] = $objSepaMethod->getSepaBic();
		$arrmixSepaInfo['sepa_iban'] = $objSepaMethod->getSepaIban();
		$arrmixSepaInfo['sepa_iban_masked'] = $objSepaMethod->getSepaIbanMasked();
		$arrmixSepaInfo['sepa_creditor_id'] = $objSepaMethod->getSepaCreditorId();
		$arrmixSepaInfo['sepa_company_name'] = $objSepaMethod->getSepaCompanyName();
		$arrmixSepaInfo['sepa_company_address'] = $objSepaMethod->getSepaCompanyAddress();

		return $arrmixSepaInfo;
	}

	public function createDistributionAccount( $objAdminDatabase = NULL ) {

		$this->m_objDistributionAccount = new CAccount();
		$this->m_objDistributionAccount->setDefaults( $this->getCid(), $objAdminDatabase );
		$this->m_objDistributionAccount->setCid( $this->getCid() );
		$this->m_objDistributionAccount->setEntityId( $this->m_intEntityId );
		$this->m_objDistributionAccount->setInvoiceMethodId( CInvoiceMethod::TRACK_2 );
		$this->m_objDistributionAccount->setAccountTypeId( CAccountType::MERCHANT );
		$this->m_objDistributionAccount->setInvoicesEmailed( 1 );
		$this->m_objDistributionAccount->setInvoicesMailed( NULL );

		return $this->m_objDistributionAccount;
	}

	public function createIntermediaryAccount( $objPaymentDatabase ) {

		$objProcessingBank = \Psi\Eos\Payment\CProcessingBanks::createService()->fetchProcessingBankById( $this->getProcessingBankId(), $objPaymentDatabase );

		$this->m_objIntermediaryAccount = new CAccount();
		$this->m_objIntermediaryAccount->setDefaults();
		$this->m_objIntermediaryAccount->setCid( $this->getCid() );
		$this->m_objIntermediaryAccount->setEntityId( $this->m_intEntityId );
		$this->m_objIntermediaryAccount->setAccountTypeId( CAccountType::INTERMEDIARY );
		$this->m_objIntermediaryAccount->setInvoiceMethodId( CInvoiceMethod::TRACK_2 );

		$this->m_objIntermediaryAccount->setInvoicesEmailed( 0 );
		$this->m_objIntermediaryAccount->setInvoicesMailed( 0 );

		if( true == valObj( $objProcessingBank, CProcessingBank::class ) ) {
			$this->m_objIntermediaryAccount->setCheckBankName( $objProcessingBank->getName() );
			$this->m_objIntermediaryAccount->setCheckRoutingNumber( $objProcessingBank->getEndrsRtrnsRoutingNum() );
		}

		$this->m_objIntermediaryAccount->setBilltoStreetLine1( 'PO Box 196' );
		$this->m_objIntermediaryAccount->setBillToCity( 'Provo' );
		$this->m_objIntermediaryAccount->setBilltoStateCode( 'UT' );
		$this->m_objIntermediaryAccount->setBilltoPostalCode( '84603' );

		return $this->m_objIntermediaryAccount;
	}

	public function createClearingAccount( $objDistributionAccount = NULL ) {

		$this->m_objClearingAccount = new CAccount();
		$this->m_objClearingAccount->setDefaults();
		$this->m_objClearingAccount->setCid( $this->getCid() );
		$this->m_objClearingAccount->setEntityId( $this->m_intEntityId );
		$this->m_objClearingAccount->setAccountTypeId( CAccountType::CLEARING );
		$this->m_objClearingAccount->setInvoiceMethodId( CInvoiceMethod::TRACK_2 );
		$this->m_objClearingAccount->setInvoicesEmailed( 0 );
		$this->m_objClearingAccount->setInvoicesMailed( 0 );

		if( true == valObj( $objDistributionAccount, CAccount::class ) ) {
			$this->m_objClearingAccount->setAccountName( $objDistributionAccount->getAccountName() );
			$this->m_objClearingAccount->setCheckBankName( $objDistributionAccount->getCheckBankName() );
			$this->m_objClearingAccount->setCheckRoutingNumber( $objDistributionAccount->getCheckRoutingNumber() );
			$this->m_objClearingAccount->setCheckAccountNumber( $objDistributionAccount->getCheckAccountNumber() );
			$this->m_objClearingAccount->setBilltoStreetLine1( $objDistributionAccount->getBilltoStreetLine1() );
			$this->m_objClearingAccount->setBilltoStreetLine2( $objDistributionAccount->getBilltoStreetLine2() );
			$this->m_objClearingAccount->setBillToCity( $objDistributionAccount->getBillToCity() );
			$this->m_objClearingAccount->setBilltoStateCode( $objDistributionAccount->getBilltoStateCode() );
			$this->m_objClearingAccount->setBilltoPostalCode( $objDistributionAccount->getBilltoPostalCode() );
			$this->m_objClearingAccount->setBilltoNameFirst( $objDistributionAccount->getBilltoNameFirst() );
			$this->m_objClearingAccount->setBilltoNameLast( $objDistributionAccount->getBilltoNameLast() );
			$this->m_objClearingAccount->setBilltoEmailAddress( $objDistributionAccount->getBilltoEmailAddress() );
			$this->m_objClearingAccount->setBilltoPhoneNumber( $objDistributionAccount->getBilltoPhoneNumber() );
		}

		$this->m_objClearingAccount->setInvoicesEmailed( 1 );

		return $this->m_objClearingAccount;
	}

	public function createDonationAccount( $objDistributionAccount = NULL ) {

		$this->m_objDonationAccount = new CAccount();
		$this->m_objDonationAccount->setDefaults();
		$this->m_objDonationAccount->setCid( $this->getCid() );
		$this->m_objDonationAccount->setEntityId( $this->m_intEntityId );
		$this->m_objDonationAccount->setAccountTypeId( CAccountType::CLEARING );
		$this->m_objDonationAccount->setInvoiceMethodId( CInvoiceMethod::TRACK_2 );
		$this->m_objDonationAccount->setInvoicesEmailed( 0 );
		$this->m_objDonationAccount->setInvoicesMailed( 0 );

		if( true == valObj( $objDistributionAccount, CAccount::class ) ) {
			$this->m_objDonationAccount->setAccountName( $objDistributionAccount->getAccountName() );
			$this->m_objDonationAccount->setCheckBankName( $objDistributionAccount->getCheckBankName() );
			$this->m_objDonationAccount->setCheckRoutingNumber( $objDistributionAccount->getCheckRoutingNumber() );
			$this->m_objDonationAccount->setCheckAccountNumber( $objDistributionAccount->getCheckAccountNumber() );
			$this->m_objDonationAccount->setBilltoStreetLine1( $objDistributionAccount->getBilltoStreetLine1() );
			$this->m_objDonationAccount->setBilltoStreetLine2( $objDistributionAccount->getBilltoStreetLine2() );
			$this->m_objDonationAccount->setBillToCity( $objDistributionAccount->getBillToCity() );
			$this->m_objDonationAccount->setBilltoStateCode( $objDistributionAccount->getBilltoStateCode() );
			$this->m_objDonationAccount->setBilltoPostalCode( $objDistributionAccount->getBilltoPostalCode() );
			$this->m_objDonationAccount->setBilltoNameFirst( $objDistributionAccount->getBilltoNameFirst() );
			$this->m_objDonationAccount->setBilltoNameLast( $objDistributionAccount->getBilltoNameLast() );
			$this->m_objDonationAccount->setBilltoEmailAddress( $objDistributionAccount->getBilltoEmailAddress() );
			$this->m_objDonationAccount->setBilltoPhoneNumber( $objDistributionAccount->getBilltoPhoneNumber() );
		}

		$this->m_objDonationAccount->setInvoicesEmailed( 1 );

		return $this->m_objDonationAccount;
	}

	public function createRebateAccount( $objDistributionAccount = NULL ) {

		$this->m_objRebateAccount = new CAccount();
		$this->m_objRebateAccount->setDefaults();
		$this->m_objRebateAccount->setCid( $this->getCid() );
		$this->m_objRebateAccount->setEntityId( $this->m_intEntityId );
		$this->m_objRebateAccount->setAccountTypeId( CAccountType::CLEARING );
		$this->m_objRebateAccount->setInvoiceMethodId( CInvoiceMethod::TRACK_2 );
		$this->m_objRebateAccount->setInvoicesEmailed( 0 );
		$this->m_objRebateAccount->setInvoicesMailed( 0 );

		if( true == valObj( $objDistributionAccount, CAccount::class ) ) {
			$this->m_objRebateAccount->setAccountName( $objDistributionAccount->getAccountName() );
			$this->m_objRebateAccount->setCheckBankName( $objDistributionAccount->getCheckBankName() );
			$this->m_objRebateAccount->setCheckRoutingNumber( $objDistributionAccount->getCheckRoutingNumber() );
			$this->m_objRebateAccount->setCheckAccountNumber( $objDistributionAccount->getCheckAccountNumber() );
			$this->m_objRebateAccount->setBilltoStreetLine1( $objDistributionAccount->getBilltoStreetLine1() );
			$this->m_objRebateAccount->setBilltoStreetLine2( $objDistributionAccount->getBilltoStreetLine2() );
			$this->m_objRebateAccount->setBillToCity( $objDistributionAccount->getBillToCity() );
			$this->m_objRebateAccount->setBilltoStateCode( $objDistributionAccount->getBilltoStateCode() );
			$this->m_objRebateAccount->setBilltoPostalCode( $objDistributionAccount->getBilltoPostalCode() );
			$this->m_objRebateAccount->setBilltoNameFirst( $objDistributionAccount->getBilltoNameFirst() );
			$this->m_objRebateAccount->setBilltoNameLast( $objDistributionAccount->getBilltoNameLast() );
			$this->m_objRebateAccount->setBilltoEmailAddress( $objDistributionAccount->getBilltoEmailAddress() );
			$this->m_objRebateAccount->setBilltoPhoneNumber( $objDistributionAccount->getBilltoPhoneNumber() );
		}

		$this->m_objRebateAccount->setInvoicesEmailed( 1 );

		return $this->m_objRebateAccount;
	}

	public function createSettlementDistribution( $objAdminDatabase, $boolIsPositiveDistribution = true ) {

		$boolIsCopiedFromReturnsAccount = false;

		$objDistributionAccount 	= CAccounts::createService()->fetchAccountById( $this->getDistributionAccountId(), $objAdminDatabase );
		$objIntermediaryAccount 	= CAccounts::createService()->fetchAccountById( $this->getIntermediaryAccountId(), $objAdminDatabase );
		$objReturnsAccount			= CAccounts::createService()->fetchAccountById( $this->getReturnsAccountId(), $objAdminDatabase );

		$this->setIntermediaryAccount( $objIntermediaryAccount );
		$this->setDistributionAccount( $objDistributionAccount );
		$this->setReturnsAccount( $objReturnsAccount );

		if( true == $boolIsPositiveDistribution || false == valObj( $objReturnsAccount, CAccount::class ) || $objReturnsAccount->getId() == $objDistributionAccount->getId() ) {
			$objCopyAccount = $objDistributionAccount;
			$intCopyAccountId = $this->getDistributionAccountId();

		} else {
			$objCopyAccount = $objReturnsAccount;
			$intCopyAccountId = $this->getReturnsAccountId();
			$boolIsCopiedFromReturnsAccount = true;
		}

		// Make sure that a valid billing account was returned
		if( false == valObj( $objCopyAccount, CAccount::class ) ) {
			trigger_error( 'Cannot find a valid billing account for this merchant account', E_USER_ERROR );
			return NULL;
		}

		// Make sure that the ach information exists and is correct
		if( false == $objCopyAccount->validate( 'convert_to_settlement_distribution', $objAdminDatabase ) ) {
			trigger_error( 'Billing account for this merchant account is invalid.', E_USER_ERROR );
			exit;

			if( 0 < \Psi\Libraries\UtilFunctions\count( $objCopyAccount->getErrorMsgs() ) ) {
				$this->m_arrobjErrorMsgs = array_merge( $this->m_arrobjErrorMsgs, $objCopyAccount->getErrorMsgs() );
			}

			return false;
		}

		$objSettlementDistribution = new CSettlementDistribution();
		$objSettlementDistribution->setCompanyMerchantAccount( $this );
		$objSettlementDistribution->setAccount( $objCopyAccount );
		$objSettlementDistribution->setAccountId( $intCopyAccountId );
		$objSettlementDistribution->setCid( $this->m_intCid );
		$objSettlementDistribution->setPaymentTypeId( ( CCurrency::CURRENCY_CODE_CAD == $this->getCurrencyCode() ? CPaymentType::PAD : CPaymentType::ACH ) );
		$objSettlementDistribution->setCompanyMerchantAccountId( $this->m_intId );
		$objSettlementDistribution->setDistributionStatusTypeId( CDistributionStatusType::PENDING );
		$objSettlementDistribution->setDistributionDatetime( date( 'Y-m-d H:i:s' ) );
		$objSettlementDistribution->setDistributionAmount( 0.00 );
		$objSettlementDistribution->setDistributionMemo( NULL );

		$objSettlementDistribution->setBilltoNameFirst( $objCopyAccount->getBilltoNameFirst() );
		$objSettlementDistribution->setBilltoNameLast( $objCopyAccount->getBilltoNameLast() );
		$objSettlementDistribution->setBilltoNameFull( $objCopyAccount->getBilltoNameFull() );
		$objSettlementDistribution->setBilltoStreetLine1( $objCopyAccount->getBilltoStreetLine1() );
		$objSettlementDistribution->setBilltoStreetLine2( $objCopyAccount->getBilltoStreetLine2() );
		$objSettlementDistribution->setBilltoStreetLine3( $objCopyAccount->getBilltoStreetLine3() );
		$objSettlementDistribution->setBilltoCity( $objCopyAccount->getBilltoCity() );
		$objSettlementDistribution->setBilltoStateCode( $objCopyAccount->getBilltoStateCode() );
		$objSettlementDistribution->setBilltoPostalCode( $objCopyAccount->getBilltoPostalCode() );
		$objSettlementDistribution->setBilltoEmailAddress( $objCopyAccount->getBilltoEmailAddress() );

		$objSettlementDistribution->setCheckBankName( $objCopyAccount->getCheckBankName() );
		$objSettlementDistribution->setCheckAccountTypeId( $objCopyAccount->getCheckAccountTypeId() );
		$objSettlementDistribution->setCheckAccountNumberEncrypted( $objCopyAccount->getCheckAccountNumberEncrypted() );
		$objSettlementDistribution->setCheckRoutingNumber( $objCopyAccount->getCheckRoutingNumber() );
		$objSettlementDistribution->setCheckNameOnAccount( $objCopyAccount->getCheckNameOnAccount() );

		return $objSettlementDistribution;
	}

	// endregion

	// region Fetch Functions

	public function fetchDistributionAccount( $objAdminDatabase ) {
		return CAccounts::createService()->fetchAccountByIdAndCid( $this->m_intDistributionAccountId, $this->m_intCid, $objAdminDatabase );
	}

	public function fetchIntermediaryAccount( $objAdminDatabase ) {
		$this->m_objIntermediaryAccount = CAccounts::createService()->fetchAccountByIdAndCid( $this->m_intIntermediaryAccountId, $this->m_intCid, $objAdminDatabase );
		return $this->m_objIntermediaryAccount;
	}

	public function fetchClearingAccount( $objAdminDatabase ) {
		return CAccounts::createService()->fetchAccountByIdAndCid( $this->getClearingAccountId(), $this->m_intCid, $objAdminDatabase );
	}

	public function fetchDonationAccount( $objAdminDatabase ) {
		$this->m_objDonationAccount = CAccounts::createService()->fetchAccountByIdAndCid( $this->m_intDonationAccountId, $this->m_intCid, $objAdminDatabase );
		return $this->m_objDonationAccount;
	}

	public function fetchRebateAccount( $objAdminDatabase ) {
		$this->m_objRebateAccount = CAccounts::createService()->fetchAccountByIdAndCid( $this->m_intRebateAccountId, $this->m_intCid, $objAdminDatabase );
		return $this->m_objRebateAccount;
	}

	public function fetchReturnsAccount( $objAdminDatabase ) {
		$this->m_objReturnsAccount = CAccounts::createService()->fetchAccountByIdAndCid( $this->getReturnsAccountId(), $this->m_intCid, $objAdminDatabase );
		return $this->m_objReturnsAccount;
	}

	public function fetchBillingAccount( $objAdminDatabase ) {
		return CAccounts::createService()->fetchAccountByIdAndCid( $this->m_intBillingAccountId, $this->m_intCid, $objAdminDatabase );
	}

	public function fetchCommercialMerchantAccount( $objPaymentDatabase ) {
		if( true == is_null( $this->m_intCommercialMerchantAccountId ) ) {
			return NULL;
		}
		return \Psi\Eos\Payment\CCompanyMerchantAccounts::createService()->fetchCompanyMerchantAccountById( $this->m_intCommercialMerchantAccountId, $objPaymentDatabase );
	}

	public function fetchClient( $objAdminDatabase ) {
		$objClient = CClients::fetchClientById( $this->getCid(), $objAdminDatabase );

		return $objClient;
	}

	public function fetchArCode( $objAdminDatabase ) {
		$objArCode = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByIdByCid( $this->getArCodeId(), $this->getCid(), $objAdminDatabase );

		return $objArCode;
	}

	/**
	 * Returns an array of CPaymentType that are allowed to be processed by this merchant account.
	 * Returns NULL if the merchant account is disabled.
	 * For each payment type accepted the convenience fee and company charge are calculated and store
	 * with the payment type.
	 *
	 */

	public function fetchAcceptedPaymentTypes( $fltPaymentAmount = 0.0, $objAdminDatabase ) {

		$arrintPaymentTypeIds = $this->getAcceptedPaymentTypeIds( $fltPaymentAmount );

		if( true == is_null( $arrintPaymentTypeIds ) ) {
			return NULL;
		}

		return CPaymentTypes::fetchPaymentTypesByIds( $arrintPaymentTypeIds, $objAdminDatabase );
	}

	// endregion

	// region Validation Functions

	public function valDistributionFee() {
		$boolIsValid = true;

		if( true == $boolIsValid && 5 < $this->getDistributionFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'distribution_fee', 'Distribution fee cannot exceed $5.' ) );
		}

		return $boolIsValid;
	}

	public function valDistributionFailureFee() {
		$boolIsValid = true;

		if( true == $boolIsValid && 50 < $this->getDistributionFailureFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'distribution_failure_fee', 'Distribution Failure fee cannot exceed $50.' ) );
		}

		return $boolIsValid;
	}

	public function valAcquireProcessingFee() {
		$boolIsValid = true;

		if( true == $boolIsValid && 10 < $this->getAcquireProcessingFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'acquire_processing_fee', 'Acquirer Processing Fee cannot exceed $10.' ) );
		}

		return $boolIsValid;
	}

	public function valDistributionAccountId( $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		if( false == isset( $this->m_intDistributionAccountId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'distribution_account_id', 'Distribution account is required.' ) );
		} elseif( true == valObj( $objAdminDatabase, CDatabase::class ) ) {

			$strSql = 'SELECT count( a.id )  FROM accounts a
					   WHERE a.id = ' . $this->m_intDistributionAccountId . '
					   AND a.payment_type_id IN (' . CPaymentType::CHECK . ', ' . CPaymentType::ACH . ', ' . CPaymentType::VISA . ', ' . CPaymentType::MASTERCARD . ', ' . CPaymentType::DISCOVER . ', ' . CPaymentType::AMEX . ', ' . CPaymentType::PAD . ')';

			$arrintResponse = fetchData( $strSql, $objAdminDatabase );

			if( true == isset( $arrintResponse[0]['count'] ) && 0 == $arrintResponse[0]['count'] ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'distribution_account_id', 'Distribution account must be billed electronically.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valProcessingBankId() {
		$boolIsValid = true;

		if( ( false == isset( $this->m_intProcessingBankId ) || 0 == $this->m_intProcessingBankId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_bank_id', 'Processing bank does not exist.' ) );
		}

		// Now validate that the selected processing bank is supported under the selected country code
		switch( $this->getProcessingBankId() ) {
			case CProcessingBank::ZIONS_BANK:
			case CProcessingBank::FIFTH_THIRD:
				if( CCountry::CODE_USA != $this->getCountryCode() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_bank_id', 'The selected processing bank is not supported under the selected country code \'' . $this->getCountryCode() . '\'' ) );
					$boolIsValid = false;
				}
				break;

			case CProcessingBank::BANK_OF_MONTREAL:
				if( CCountry::CODE_CANADA != $this->getCountryCode() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_bank_id', 'Bank of Montreal is not supported under the selected country code \'' . $this->getCountryCode() . '\'' ) );
					$boolIsValid = false;
				}
				break;

			case CProcessingBank::INTERNATIONAL:
				if( in_array( $this->getCountryCode(), CCountry::$c_arrstrPaymentFacilitatorCountryCodes ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_bank_id', 'The International processing bank is not supported under the selected country code \'' . $this->getCountryCode() . '\'' ) );
					$boolIsValid = false;
					break;
				}
				break;

			default:
				return $boolIsValid;
		}

//		if( CProcessingBank::ZIONS_BANK != $this->m_intProcessingBankId && CProcessingBank::FIFTH_THIRD != $this->m_intProcessingBankId ) {
//			$boolIsValid = false;
//			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_bank_id', 'Zions or Fifth Third must be set as the default processing bank.' ) );
//		}

		return $boolIsValid;
	}

	public function valFundMovementTypeId() {
		$boolIsValid = true;

		if( ( false == isset( $this->m_intFundMovementTypeId ) || 0 == $this->m_intFundMovementTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fund_movement_type_id', 'Fund movement type does not exist.' ) );
		}

		return $boolIsValid;
	}

	public function valTaxLegalName() {
		$boolIsValid = true;
		if( false == preg_match( '/^[a-z0-9\-\.\&\: ]*$/i', trim( $this->m_strTaxLegalName ) ) && true == $this->m_boolIsNewUi ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_legal_name', 'Special characters are not allowed in Tax legal  name.' ) );
		}

		return $boolIsValid;
	}

	public function valIntermediaryAccountId() {
		$boolIsValid = true;

		if( ( false == isset( $this->m_intIntermediaryAccountId ) || 0 == $this->m_intIntermediaryAccountId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'intermediary_account_id', 'Intermediary account does not exist.' ) );
		}

		return $boolIsValid;
	}

	public function valClearingAccountId() {
		$boolIsValid = true;

		if( CMerchantProcessingType::CLEARING == $this->getMerchantProcessingTypeId() && false == is_numeric( $this->getClearingAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'clearing_account_id', 'Clearing account must be set, if "Clearing" is selected as the merchant processing type.' ) );
		}

		return $boolIsValid;
	}

	public function valDonationAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRebateAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaxNumberEncrypted() {
		$boolIsValid = true;

		if( false == is_null( $this->getTaxNumberEncrypted() ) && 0 === preg_match( '/^[0-9-]+$/', $this->getTaxNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_encrypted', 'Tax Id Number must be number.' ) );
		}

		return $boolIsValid;
	}

	public function valExternalLegalEntityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMerchantProcessingTypeId() {
		$boolIsValid = true;

		if( ( true == is_null( $this->getMerchantProcessingTypeId() ) || 0 == $this->getMerchantProcessingTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'merchant_processing_type_id', 'Merchant processing type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valFixedConvenienceFee() {

		$boolIsValid = true;

		$arrintMerchantProcessingTypeIds = array(
			CMerchantProcessingType::STANDARD,
			CMerchantProcessingType::CLEARING,
			CMerchantProcessingType::VARIABLE_VISA,
			CMerchantProcessingType::COMMERCIAL
		);

		if( CMerchantProcessingType::COMMERCIAL != $this->getMerchantProcessingTypeId() && CMerchantProcessingType::STANDARD != $this->getMerchantProcessingTypeId() && CMerchantProcessingType::CLEARING != $this->getMerchantProcessingTypeId() && ( true == is_null( $this->getFixedConvenienceFee() ) || 0 >= $this->getFixedConvenienceFee() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fixed_convenience_fee', 'Fixed convenience fee must be set.' ) );
		}

		if( true == $boolIsValid && CMerchantProcessingType::COMMERCIAL != $this->getMerchantProcessingTypeId() && CMerchantProcessingType::STANDARD != $this->getMerchantProcessingTypeId() && CMerchantProcessingType::CLEARING != $this->getMerchantProcessingTypeId() && 1 > $this->getFixedConvenienceFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fixed_convenience_fee', 'Fixed convenience must be at least $1.' ) );
		}

		if( true == $boolIsValid && false == in_array( $this->getMerchantProcessingTypeId(), $arrintMerchantProcessingTypeIds ) && 150 < $this->getFixedConvenienceFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fixed_convenience_fee', 'Fixed convenience must be less than $150.' ) );
		}

		return $boolIsValid;
	}

	public function valTargetDiscountRate() {

		$boolIsValid = true;

		if( ( CMerchantProcessingType::VARIABLE_FEES == $this->getMerchantProcessingTypeId() || CMerchantProcessingType::VARIABLE_VISA == $this->getMerchantProcessingTypeId() ) && ( true == is_null( $this->getTargetDiscountRate() ) || 0 >= $this->getTargetDiscountRate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'target_discount_rate', 'Target discount rate must be set.' ) );
		}

		if( ( CMerchantProcessingType::VARIABLE_FEES == $this->getMerchantProcessingTypeId() || CMerchantProcessingType::VARIABLE_VISA == $this->getMerchantProcessingTypeId() ) && 0.05 < $this->getTargetDiscountRate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'target_discount_rate', 'Target discount rate cannot be greater than 5%.' ) );
		}

		if( ( CMerchantProcessingType::VARIABLE_FEES == $this->getMerchantProcessingTypeId() || CMerchantProcessingType::VARIABLE_VISA == $this->getMerchantProcessingTypeId() ) && 0.02 > $this->getTargetDiscountRate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'target_discount_rate', 'Target discount rate cannot be less than 2%.' ) );
		}

		return $boolIsValid;
	}

	/**
	 * @return bool
	 */
	public function valCountryCode() {
		$boolIsValid = true;

		switch( $this->getMerchantProcessingTypeId() ) {
			case CMerchantProcessingType::STANDARD:
				if( !in_array( $this->getCountryCode(), CMerchantProcessingTypes::$c_arrstrCountryCodesByMerchantProcessingType[CMerchantProcessingType::STANDARD] ) ) {
					$strMerchantProcessingType = 'Standard';
					$boolIsValid = false;
				}
				break;

			case CMerchantProcessingType::INTERNATIONAL:
				if( !in_array( $this->getCountryCode(), CMerchantProcessingTypes::$c_arrstrCountryCodesByMerchantProcessingType[CMerchantProcessingType::INTERNATIONAL] ) ) {
					$strMerchantProcessingType = 'International';
					$boolIsValid = false;
				}
				break;

			default:
				$boolIsValid = true;
				break;
		}

		if( !$boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_code', 'The selected country code \'' . $this->getCountryCode() . '\' cannot be associated with ' . $strMerchantProcessingType . ' accounts.' ) );
		}

		return $boolIsValid;
	}

	public function valCommercialMerchantAccount() {
		$boolIsValid = true;

		if( !valId( $this->getCommercialMerchantAccountId() ) || !valObj( $this->getCommercialMerchantAccount(), CCompanyMerchantAccount::class ) ) {
			return $boolIsValid;
		}

		if( $this->getCountryCode() != $this->getCommercialMerchantAccount()->getCountryCode() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commercial_merchant_account_id', 'Cannot associate a Commercial Merchant Account with a different country code \'' . $this->getCommercialMerchantAccount()->getCountryCode() . '\'' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	// For very rare cases, we allow clients to be paper invoiced for transaction fees, so we commented out validation on requirement of ACH type on the billing account.
	// If we can get Waterton on Auto Debit, we can probably switch this back.

	public function valBillingAccountId() {
		$boolIsValid = true;

		if( ( false == isset( $this->m_intBillingAccountId ) || 0 == $this->m_intBillingAccountId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_account_id', 'Billing account is not selected.' ) );

		}// else if( ( $this->m_intBillingAccountId != $this->m_intDistributionAccountId ) && ( true == valObj( $objAdminDatabase, 'CDatabase' ) ) ) {

		// $strSql = 'SELECT count( a.id )  FROM accounts a
		//		   WHERE a.id = ' . $this->m_intBillingAccountId . '
		//		   AND a.payment_type_id IN (' . CPaymentType::ACH . ')';

		//	$arrintResponse = fetchData( $strSql, $objAdminDatabase );

		//	if( true == isset( $arrintResponse[0]['count'] ) && 0 == $arrintResponse[0]['count'] ) {
		//		$boolIsValid = false;
		//		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_account_id', 'Billing account must be billed electronically.' ) );
		//	}
		// }

		return $boolIsValid;
	}

	public function valReturnsAccountId( $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		if( ( false == isset( $this->m_intReturnsAccountId ) || 0 == $this->m_intReturnsAccountId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'returns_account_id', 'Returns account is not selected.' ) );

		} elseif( 0 < $this->m_intReturnsAccountId && $this->m_intReturnsAccountId != $this->m_intDistributionAccountId && valObj( $objAdminDatabase, CDatabase::class ) ) {

			$strSql = 'SELECT count( a.id ) FROM accounts a
					   WHERE a.id = ' . $this->m_intReturnsAccountId . '
					   AND a.payment_type_id IN (' . CPaymentType::ACH . ')';

			$arrintResponse = fetchData( $strSql, $objAdminDatabase );

			if( true == isset( $arrintResponse[0]['count'] ) && 0 == $arrintResponse[0]['count'] ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'returns_account_id', 'Returns account must be billed electronically.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valAccountName() {
		$boolIsValid = true;

		// Validation example
		if( false == isset( $this->m_strAccountName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_name', 'Account name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMerchantName() {
		$boolIsValid = true;

		if( false == isset( $this->m_strMerchantName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'merchant_name', "'Appears on Statement as' is required." ) );
		}

		return $boolIsValid;
	}

	public function valNotificationEmail() {

		$boolIsValid = true;

		if( false == is_null( $this->m_strNotificationEmail ) && false == CValidation::validateEmailAddresses( trim( $this->m_strNotificationEmail ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notification_email', 'Notification Email "' . $this->m_strNotificationEmail . '" is not valid.' ) );
		}

		return $boolIsValid;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function valCcMinimumConvenienceFee() {
		return true;
	}

	public function valCcCalcFeesOnMaxPayment() {
		return true;
	}

	public function valCh21IsEnabled( $boolIsArPaymentValidation = false ) {
		$boolIsValid = true;

		if( true == $boolIsArPaymentValidation && !$this->methodIsEnabled( CPaymentType::CHECK_21 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ch21_enabled', 'Configuration Error: Check 21 is not an active payment option.' ) );
		}

		return $boolIsValid;
	}

	public function valEmoIsEnabled( $boolIsArPaymentValidation = false ) {
		$boolIsValid = true;

		// TODO: Reevaluate the logic for this method and fix implementation
		// ??? If Emo enablement setting is null, should we default it to allowed?
		if( is_null( $this->getMethodByAmount( CPaymentType::EMONEY_ORDER )->getIsEnabled() ) ) {
			$this->getMethodByAmount( CPaymentType::EMONEY_ORDER )->setIsEnabled( 1 );
		}

		// I think Below if condition seems to be not in use and can be removed.
		if( $boolIsArPaymentValidation && 1 != $this->getMethodByAmount( CPaymentType::EMONEY_ORDER )->getIsEnabled() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'emoney_order_enabled', 'Configuration Error: E-Money Order is not an active payment option.' ) );
		}

		return $boolIsValid;
	}

	public function valIsDisabled( $objClientDatabase = NULL ) {
		$boolIsValid = true;

		if( 1 == $this->getIsDisabled() && ( true == valObj( $objClientDatabase, CDatabase::class ) ) ) {

			$arrobjPropertyMerchantAccounts = CPropertyMerchantAccounts::fetchCustomPropertyMerchantAccountsByCompanyMerchantAccountIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );

			if( false == is_null( $arrobjPropertyMerchantAccounts ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrobjPropertyMerchantAccounts ) ) {
				$arrintPropertyIds = array();

				if( false == is_null( $arrobjPropertyMerchantAccounts ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrobjPropertyMerchantAccounts ) ) {
					$arrobjPropertyMerchantAccounts = rekeyObjects( 'PropertyId', $arrobjPropertyMerchantAccounts );
					$arrintPropertyIds 				= array_keys( $arrobjPropertyMerchantAccounts );
				}

				if( false == is_null( $arrintPropertyIds ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) {
					$arrobjProperties = \Psi\Eos\Entrata\CProperties::createService()->fetchCustomPropertiesByIdsByCid( $arrintPropertyIds, $this->getCid(), $objClientDatabase );
				}

				$strPropertyNames = '';
				if( false == is_null( $arrobjProperties ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrobjProperties ) ) {

					$strPropertyNames .= "<div style='padding-top:10px; padding-left:20px;text-align:left;' id='error' >";

					foreach( $arrobjProperties as $objProperty ) {

						$strPropertyNames .= '&#149;' . $objProperty->getPropertyName() . '<br>';
					}
					$strPropertyNames .= '</div>';

					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_disabled', 'Merchant Account cannot be disabled.  This Merchant Account is used by the following Properties.' . $strPropertyNames ) );
					$boolIsValid = false;
				}
			}
		}
		return $boolIsValid;
	}

	public function valProcessingBankAccount() {

		$boolIsValid = true;

		if( false == in_array( $this->getFundMovementTypeId(), [ CFundMovementType::BANK_INTERMEDIARY_MOVEMENT, CFundMovementType::BANK_INTERMEDIARY_ANTICIPATORY ] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_bank_id', 'Invalid processing bank and fund movement type. Only bank intermediary is allowed' ) );
			return false;
		}

		switch( $this->getProcessingBankId() ) {
			case CProcessingBank::FIFTH_THIRD:
				if( $this->methodIsEnabled( CPaymentType::ACH ) && CProcessingBankAccount::FIFTH_THIRD_ACH != $this->getMethodByAmount( CPaymentType::ACH )->getProcessingBankAccountId() ) {
					$boolIsValid = false;
				}
				if( $this->methodIsEnabled( CPaymentType::CHECK_21 ) && CProcessingBankAccount::FIFTH_THIRD_CHECK21 != $this->getMethodByAmount( CPaymentType::CHECK_21 )->getProcessingBankAccountId() ) {
					$boolIsValid = false;
				}
				if( $this->methodIsEnabled( CPaymentType::EMONEY_ORDER ) && CProcessingBankAccount::ZIONS_CENTRAL_MONEYGRAM != $this->getMethodByAmount( CPaymentType::EMONEY_ORDER )->getProcessingBankAccountId() ) {
					$boolIsValid = false;
				}
				foreach( CPaymentType::$c_arrintCreditCardPaymentTypes as $intPaymentType ) {
					if( !$this->methodIsEnabled( $intPaymentType ) ) {
						continue;
					}

					if( CProcessingBankAccount::ZIONS_CREDIT_CARD != $this->getMethodByAmount( $intPaymentType )->getProcessingBankAccountId() && CProcessingBankAccount::ZIONS_CREDIT_CARD_LITLE != $this->getMethodByAmount( $intPaymentType )->getProcessingBankAccountId() ) {
						$boolIsValid = false;

						break;
					}
				}
				if( !$boolIsValid ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_bank_id', 'Invalid processing bank and fund movement type. ( Fifth Third & Bank Intermediary )' ) );
				}
				break;

			case CProcessingBank::BANK_OF_MONTREAL:
				if( $this->methodIsEnabled( CPaymentType::PAD ) && CProcessingBankAccount::BANK_OF_MONTREAL_ACH_CREDIT_CARD != $this->getMethodByAmount( CPaymentType::ACH )->getProcessingBankAccountId() ) {
					$boolIsValid = false;
				}

				foreach( CPaymentType::$c_arrintCreditCardPaymentTypes as $intPaymentType ) {
					if( !$this->methodIsEnabled( $intPaymentType ) ) {
						continue;
					}

					if( CProcessingBankAccount::BANK_OF_MONTREAL_ACH_CREDIT_CARD != $this->getMethodByAmount( $intPaymentType )->getProcessingBankAccountId() ) {
						$boolIsValid = false;

						break;
					}
				}
				if( !$boolIsValid ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_bank_id', 'Invalid processing bank and fund movement type. ( Bank of Montreal )' ) );
				}
				break;

			case CProcessingBank::INTERNATIONAL:
				if( CMerchantProcessingType::INTERNATIONAL != $this->getMerchantProcessingTypeId() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_bank_id', 'Only International merchant accounts can support the International processing bank.' ) );
					$boolIsValid = false;
				}

				if( $this->methodIsEnabled( CPaymentType::SEPA_DIRECT_DEBIT ) && CProcessingBankAccount::INTERNATIONAL != $this->getMethodByAmount( CPaymentType::SEPA_DIRECT_DEBIT )->getProcessingBankAccountId() ) {
					$boolIsValid = false;
				}

				foreach( CPaymentType::$c_arrintCreditCardPaymentTypes as $intPaymentType ) {
					if( !$this->methodIsEnabled( $intPaymentType ) ) {
						continue;
					}

					if( CProcessingBankAccount::INTERNATIONAL != $this->getMethodByAmount( $intPaymentType )->getProcessingBankAccountId() ) {
						$boolIsValid = false;

						break;
					}
				}
				if( !$boolIsValid ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_bank_id', 'Invalid processing bank and fund movement type. ( International )' ) );
				}
				break;

			case CProcessingBank::ZIONS_BANK:
			default:
				if( $this->methodIsEnabled( CPaymentType::ACH ) && CProcessingBankAccount::ZIONS_CENTRAL_ACH != $this->getMethodByAmount( CPaymentType::ACH )->getProcessingBankAccountId() ) {
					$boolIsValid = false;
				}

				if( $this->methodIsEnabled( CPaymentType::CHECK_21 ) && CProcessingBankAccount::ZIONS_CENTRAL_CHECK21 != $this->getMethodByAmount( CPaymentType::CHECK_21 )->getProcessingBankAccountId() ) {
					$boolIsValid = false;
				}

				if( $this->methodIsEnabled( CPaymentType::EMONEY_ORDER ) && CProcessingBankAccount::ZIONS_CENTRAL_MONEYGRAM != $this->getMethodByAmount( CPaymentType::EMONEY_ORDER )->getProcessingBankAccountId() ) {
					$boolIsValid = false;
				}

				foreach( CPaymentType::$c_arrintCreditCardPaymentTypes as $intPaymentType ) {
					if( !$this->getMethodByAmount( $intPaymentType ) ) {
						continue;
					}

					if( CProcessingBankAccount::ZIONS_CREDIT_CARD != $this->getMethodByAmount( $intPaymentType )->getProcessingBankAccountId() && CProcessingBankAccount::ZIONS_CREDIT_CARD_LITLE != $this->getMethodByAmount( $intPaymentType )->getProcessingBankAccountId() ) {
						$boolIsValid = false;

						break;
					}
				}
				if( !$boolIsValid ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_bank_id', 'Invalid processing bank and fund movement type. ( Zion & Bank Intermediary )' ) );
				}
				break;
		}

		return $boolIsValid;
	}

	public function valDebitPricingMatch() {
		$boolIsValid = true;
		foreach( $this->getMerchantMethods() as $intPaymentMediumId => $arrobjTiers ) {
			foreach( $arrobjTiers as $intTier => $arrobjMethods ) {
				$boolHasAchFee = false;
				$fltAchFee = 0;

				$arrfltTransactionFees = [];

				// if ach is enabled, all of the debit fees for credit cards must match the resident ach fee (there is no such thing as ach debit)
				if( ( $objAchMethod = $arrobjMethods[CPaymentType::ACH] ?? NULL ) && 1 == $objAchMethod->getIsEnabled() && empty( $objAchMethod->getDeletedOn() ) && CPaymentMedium::TERMINAL != $intPaymentMediumId && !is_null( $objAchMethod->getResidentTransactionFee() ) ) {
					$objAchMethod->getResidentTransactionFee();
					$boolHasAchFee = true;
				}

				if( ( $objViMethod = $arrobjMethods[CPaymentType::VISA] ?? NULL ) && 1 == $objViMethod->getIsEnabled() && empty( $objViMethod->getDeletedOn() ) ) {
					$arrfltTransactionFees[] = $objViMethod->getResidentDebitTransactionFee();
				}

				if( ( $objMcMethod = $arrobjMethods[CPaymentType::MASTERCARD] ?? NULL ) && 1 == $objMcMethod->getIsEnabled() && empty( $objMcMethod->getDeletedOn() ) ) {
					$arrfltTransactionFees[] = $objMcMethod->getResidentDebitTransactionFee();
				}

				if( ( $objDiMethod = $arrobjMethods[CPaymentType::DISCOVER] ?? NULL ) && 1 == $objDiMethod->getIsEnabled() && empty( $objDiMethod->getDeletedOn() ) ) {
					$arrfltTransactionFees[] = $objDiMethod->getResidentDebitTransactionFee();
				}

				if( ( $objAeMethod = $arrobjMethods[CPaymentType::AMEX] ?? NULL ) && 1 == $objAeMethod->getIsEnabled() && empty( $objAeMethod->getDeletedOn() ) ) {
					$arrfltTransactionFees[] = $objAeMethod->getResidentDebitTransactionFee();
				}

				//In all cases, we want to ensure that all CC resident debit transaction fees match if the resident is absorbing any CC debit transaction fees
				$fltPreviousFee = NULL;
				foreach( $arrfltTransactionFees as $fltTransactionFee ) {
					if( !is_null( $fltPreviousFee ) && 0 != bccomp( $fltPreviousFee, $fltTransactionFee, 2 ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, implode( '.', [ 'merchant_account_methods', $intPaymentMediumId, $intTier, 0, 'resident_debit_transaction_fee' ] ), 'Visa, Mastercard, Discover, and AmEx resident debit transaction fees must match if the resident is absorbing CC any debit fees.' ) );
						$boolIsValid = false;
						break;
					}
					$fltPreviousFee = $fltTransactionFee;
				}

				//If separate pricing is enabled and Visa Pilot disabled, we want to ensure that the resident ACH fee matches the CC fees
				if( true == $boolIsValid && true == $boolHasAchFee && 0 == $this->getIsOverrideFeeAmountValidation() && 1 == $this->getEnableSeparateDebitPricing() && 0 == $this->getEnableVisaPilot() ) {
					foreach( $arrfltTransactionFees as $fltTransactionFee ) {
						if( false == is_null( $fltAchFee ) && 0 != bccomp( $fltAchFee, $fltTransactionFee, 2 ) ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, implode( '.', [ 'merchant_account_methods', $intPaymentMediumId, $intTier, 0, 'resident_debit_transaction_fee' ] ), 'ACH resident transaction fees must match Visa, Mastercard, Discover, and AmEx resident debit transaction fees when separate debit pricing is enabled.' ) );
							$boolIsValid = false;
							break;
						}
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valCreditPricingMatch() {
		$boolIsValid = true;

		foreach( $this->getMerchantMethods() as $intPaymentMediumId => $arrobjTiers ) {
			foreach( $arrobjTiers as $intTier => $arrobjMethods ) {
				$arrfltTransactionFees = [];
				$arrfltDiscountRates = [];

				if( ( $objViMethod = $arrobjMethods[CPaymentType::VISA] ?? NULL ) && 1 == $objViMethod->getIsEnabled() && empty( $objViMethod->getDeletedOn() ) ) {
					$arrfltTransactionFees[] = $objViMethod->getResidentTransactionFee();
					$arrfltDiscountRates[] = $objViMethod->getResidentDiscountRate();
				}

				if( ( $objMcMethod = $arrobjMethods[CPaymentType::MASTERCARD] ?? NULL ) && 1 == $objMcMethod->getIsEnabled() && empty( $objMcMethod->getDeletedOn() ) ) {
					$arrfltTransactionFees[] = $objMcMethod->getResidentTransactionFee();
					$arrfltDiscountRates[] = $objMcMethod->getResidentDiscountRate();
				}

				if( ( $objDiMethod = $arrobjMethods[CPaymentType::DISCOVER] ?? NULL ) && 1 == $objDiMethod->getIsEnabled() && empty( $objDiMethod->getDeletedOn() ) ) {
					$arrfltTransactionFees[] = $objDiMethod->getResidentTransactionFee();
					$arrfltDiscountRates[] = $objDiMethod->getResidentDiscountRate();
				}

				if( ( $objAeMethod = $arrobjMethods[CPaymentType::AMEX] ?? NULL ) && 1 == $objAeMethod->getIsEnabled() && empty( $objAeMethod->getDeletedOn() ) ) {
					$arrfltTransactionFees[] = $objAeMethod->getResidentTransactionFee();
					$arrfltDiscountRates[] = $objAeMethod->getResidentDiscountRate();
				}

				$fltPreviousFee = NULL;

				foreach( $arrfltTransactionFees as $fltTransactionFee ) {
					if( false == is_null( $fltPreviousFee ) && 0 != bccomp( $fltPreviousFee, $fltTransactionFee, 2 ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ae_resident_transaction_fee', 'Visa, mastercard, discover, and amex resident transaction fees must match if the resident is absorbing CC transaction fees.' ) );
						$boolIsValid = false;
						break;
					}
					$fltPreviousFee = $fltTransactionFee;
				}

				$fltPreviousRate = NULL;

				foreach( $arrfltDiscountRates as $fltDiscountRate ) {

					if( false == is_null( $fltPreviousRate ) && 0 != bccomp( $fltPreviousRate, $fltDiscountRate, 3 ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ae_resident_discount_rate', 'Visa, mastercard, discover, and amex resident discount rates must match.' ) );
						$boolIsValid = false;
						break;
					}

					$fltPreviousRate = $fltDiscountRate;
				}
			}
		}

		return $boolIsValid;
	}

	/**
	 * @return bool
	 */
	private function valInternationalAccount() {
		$boolIsValid = true;

		if( CPaymentMedium::TERMINAL == $this->getPaymentMediumId() ) {
			return $boolIsValid;
		}

		$boolIsValid &= $this->valAccountName();

		if( false == \Psi\Libraries\UtilFunctions\valArr( $this->getMerchantMethods() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vi_transaction_fee', __( 'At least one payment type must be associated with this merchant account.' ) ) );
			return false;
		}

		$boolAllMethodsDeleted = true;
		$arrobjMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( &$boolAllMethodsDeleted ) {
			if( empty( $objMethod->getDeletedOn() ) ) {
				$boolAllMethodsDeleted = false;
			}
		} );

		if( $boolAllMethodsDeleted ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vi_transaction_fee', __( 'At least one payment type must be associated with this merchant account.' ) ) );
			return false;
		}

		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( &$boolIsValid ) {
			if( !valId( $objMethod->getId() ) || !empty( $objMethod->getDeletedOn() ) || 0 == $objMethod->getIsEnabled() ) {
				return;
			}
			$boolIsValid &= $objMethod->validate( $this->getClient(), $this->getMerchantProcessingTypeId(), $this->getCountryCode(), $this->getEnableSeparateDebitPricing(), $this->getEnableVisaPilot() );
		} );

		$this->collectMethodErrors();

		return $boolIsValid;
	}

	/**
	 * We need to validate that all 9 credential fields are present for all CC type payments
	 *
	 * @return bool
	 */
	private function validateWorldpayCredentials() {
		$boolIsValid = true;

		if( CMerchantGateway::WORLDPAY_INTL_CREDIT_CARD != $this->getCcMerchantGatewayId() ) {
			return $boolIsValid;
		}

		$arrobjMethods = $this->getMerchantMethods();

		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( &$boolIsValid ) {
			if( !in_array( $objMethod->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) || !$objMethod->getIsEnabled() || !empty( $objMethod->getDeletedOn() ) ) {
				return;
			}

			if( empty( $objMethod->getWorldpayCcEcomUsername() ) || empty( $objMethod->getWorldpayCcMotoUsername() ) || empty( $objMethod->getWorldpayCcRecurringUsername() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'worldpay_cc_ecom_username', __( 'Gateway username required.' ) ) );
				$boolIsValid &= false;
			}

			if( empty( $objMethod->getWorldpayCcEcomPassword() ) || empty( $objMethod->getWorldpayCcMotoPassword() ) || empty( $objMethod->getWorldpayCcRecurringPassword() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'worldpay_cc_ecom_password', __( 'Gateway password required.' ) ) );
				$boolIsValid &= false;
			}

			if( empty( $objMethod->getWorldpayCcEcomMerchantCode() ) || empty( $objMethod->getWorldpayCcMotoMerchantCode() ) || empty( $objMethod->getWorldpayCcRecurringMerchantCode() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'worldpay_cc_ecom_merchant_code', __( 'Gateway merchant code required.' ) ) );
				$boolIsValid &= false;
			}
		} );

		return $boolIsValid;
	}

	private function validateSepaWorldpayCredentials() {

		$boolIsValid = true;

		$arrobjMethods = $this->getMerchantMethods();

		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( &$boolIsValid ) {

			if( $objMethod->getPaymentTypeId() != CPaymentType::SEPA_DIRECT_DEBIT || !$objMethod->getIsEnabled() || false == empty( $objMethod->getDeletedOn() ) ) {
				return;
			}

			if( CMerchantGateway::SEPA_DIRECT_DEBIT != $objMethod->getMerchantGatewayId() ) {
				return;
			}

			if( true == empty( $objMethod->getWorldpayEcomUsername() ) || true == empty( $objMethod->getWorldpayRecurringUsername() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sepa_worldpay_ecom_password', __( '{%s, 0} Gateway username required.', [ 'SEPA' ] ) ) );
				$boolIsValid &= false;
			}

			if( true == empty( $objMethod->getWorldpayEcomPassword() ) || true == empty( $objMethod->getWorldpayRecurringPassword() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sepa_worldpay_ecom_password', __( '{%s, 0} Gateway password required.', [ 'SEPA' ] ) ) );
				$boolIsValid &= false;
			}

			if( true == empty( $objMethod->getWorldpayEcomMerchantCode() ) || true == empty( $objMethod->getWorldpayRecurringMerchantCode() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sepa_worldpay_ecom_merchant_code', __( '{%s, 0} Gateway merchant code required.', [ 'SEPA' ] ) ) );
				$boolIsValid &= false;
			}
		} );

		return $boolIsValid;
	}

	private function validateSepaInfo() {

		$boolIsValid = true;

		$arrobjMethods = $this->getMerchantMethods();

		array_walk_recursive( $arrobjMethods, function( CMerchantAccountMethod $objMethod ) use( &$boolIsValid ) {

			if( $objMethod->getPaymentTypeId() != CPaymentType::SEPA_DIRECT_DEBIT || !$objMethod->getIsEnabled() || false == empty( $objMethod->getDeletedOn() ) ) {
				return;
			}

			if( true == empty( $objMethod->getSepaCompanyName() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sepa_company_name', __( '{%s, 0} is required.', [ 'SEPA Company Name' ] ) ) );
				$boolIsValid &= false;
			}

			if( true == empty( $objMethod->getWorldpayMandatePrefix() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sepa_worldpay_mandate_prefix', __( '{%s, 0} is required.', [ 'SEPA e-Mandate Prefix' ] ) ) );
				$boolIsValid &= false;
			}
		} );

		$boolIsValid &= $this->validateSepaWorldpayCredentials();

		return $boolIsValid;
	}

	private function val3dsFlexDetails() {
		$objCcMethod = NULL;

		/**
		 * If 3DS Flex is enabled, then we need verify all the details
		 */
		if( ( bool ) $this->getWorldpay3dsFlexIsEnabled() ) {
			if( !\Psi\Libraries\UtilFunctions\valStr( $this->getWorldpay3dsOrganisationUnitId() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'worldpay_3ds_organisation_unit_id', __( 'Organisational Unit ID is required when {%s, 0} is enabled.', [ '3DS Flex' ] ) ) );
				return false;
			}

			if( !\Psi\Libraries\UtilFunctions\valStr( $this->getWorldpay3dsJwtIssuerId() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sepa_worldpay_mandate_prefix', __( 'API ID is required when {%s, 0} is enabled.', [ '3DS Flex' ] ) ) );
				return false;
			}

			if( !\Psi\Libraries\UtilFunctions\valStr( $this->getWorldpay3dsJwtMacKey() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sepa_worldpay_mandate_prefix', __( 'API Key is required when {%s, 0} is enabled.', [ '3DS Flex' ] ) ) );
				return false;
			}
		}

		return true;
	}

	/**
	 * @return bool
	 */
	private function validateSepaIban() {
		$boolIsValid = true;
		$objIban = \Psi\Libraries\Iban\CIban::create( $this->getMethodByAmount( \CPaymentType::SEPA_DIRECT_DEBIT )->getSepaIban() );

		if( false == $objIban->verify() ) {
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, NULL, __( '{%s, 0} is not valid.', [ 'IBAN' ] ) ) );
			$boolIsValid &= false;
		}

		if( false == $objIban->isSepa() ) {
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, NULL, __( '{%s, 0} cannot be used for direct debit.', [ 'IBAN' ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	/**
	 * @return bool
	 */
	private function validateSepaBic() {
		$boolIsValid = true;
		$strBic = $this->getMethodByAmount( \CPaymentType::SEPA_DIRECT_DEBIT )->getSepaBic();

		if( 8 != mb_strlen( $strBic ) && 11 != mb_strlen( $strBic ) ) {
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, NULL, __( '{%s, 0} must be {%d, 1} or {%d, 2} characters..', [ 'BIC', 8, 11 ] ) ) );
			$boolIsValid = false;
		}

		if( false == preg_match( '/^[A-Z]{6,6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3,3}){0,1}$/i', $strBic ) ) {
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, NULL, __( '{%s, 0} is not valid.', [ 'BIC' ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 *
	 */
	public function valTabaPayCountyCode() {
		$boolIsValid = true;

		if( empty( $this->getTabaPayCountyCode() ) ) {
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, NULL, __( 'Tabapay County Code is required.' ) ) );
			$boolIsValid = false;
		}

		if( $boolIsValid && 1 !== \Psi\CStringService::singleton()->preg_match( '/^\d{3}$/', $this->getTabaPayCountyCode() ) ) {
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, NULL, __( 'Tabapay County Code must be a 3-digit numerical code.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valTabaPayClientId() {
		$boolIsValid = true;

		if( empty( $this->getTabaPayClientId() ) ) {
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, NULL, __( 'Invalid Tabapay Client ID' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				// $boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valProcessingBankId();
				$boolIsValid &= $this->valFundMovementTypeId();
				$boolIsValid &= $this->valProcessingBankAccount();
				$boolIsValid &= $this->valDistributionAccountId();
				$boolIsValid &= $this->valDistributionFee();
				$boolIsValid &= $this->valDistributionFailureFee();
				$boolIsValid &= $this->valAcquireProcessingFee();
				$boolIsValid &= $this->valIntermediaryAccountId();
				$boolIsValid &= $this->valClearingAccountId();
				$boolIsValid &= $this->valDonationAccountId();
				$boolIsValid &= $this->valRebateAccountId();
				$boolIsValid &= $this->valBillingAccountId();
				$boolIsValid &= $this->valReturnsAccountId( $objAdminDatabase );

				$boolIsValid &= $this->valAccountName();
				$boolIsValid &= $this->valMerchantName();
				$boolIsValid &= $this->valMerchantProcessingTypeId();
				$boolIsValid &= $this->valFixedConvenienceFee();
				$boolIsValid &= $this->valTargetDiscountRate();
				$boolIsValid &= $this->valCountryCode();
				$boolIsValid &= $this->valCommercialMerchantAccount();

				if( 0 == $this->getIsOverrideFeeAmountValidation() ) {
					$boolIsValid &= $this->valDebitPricingMatch();
					$boolIsValid &= $this->valCreditPricingMatch();
				}

				$boolIsValid &= $this->valNotificationEmail();
				break;

			case VALIDATE_UPDATE:
				// $boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valProcessingBankId();
				$boolIsValid &= $this->valFundMovementTypeId();
				$boolIsValid &= $this->valProcessingBankAccount();
				if( 0 == $this->getIsDisabled() ) {
					$boolIsValid &= $this->valDistributionAccountId( $objAdminDatabase );
					$boolIsValid &= $this->valReturnsAccountId( $objAdminDatabase );
				}
				$boolIsValid &= $this->valDistributionFee();
				$boolIsValid &= $this->valDistributionFailureFee();
				$boolIsValid &= $this->valAcquireProcessingFee();
				$boolIsValid &= $this->valIntermediaryAccountId();
				$boolIsValid &= $this->valClearingAccountId();
				$boolIsValid &= $this->valDonationAccountId();
				$boolIsValid &= $this->valRebateAccountId();
				$boolIsValid &= $this->valBillingAccountId();
				$boolIsValid &= $this->valCountryCode();
				$boolIsValid &= $this->valCommercialMerchantAccount();

				$boolIsValid &= $this->valTaxNumberEncrypted();

				$boolIsValid &= $this->valAccountName();

				if( CMerchantProcessingType::ACCOUNTS_PAYABLE == $this->getMerchantProcessingTypeId() ) {
					$boolIsValid &= $this->valTabaPayCountyCode();
				}

				if( CMerchantProcessingType::INTERNATIONAL != $this->getMerchantProcessingTypeId() ) {
					$boolIsValid &= $this->valMerchantName();
					$boolIsValid &= $this->valFixedConvenienceFee();
				}

				$boolIsValid &= $this->valMerchantProcessingTypeId();
				$boolIsValid &= $this->valTargetDiscountRate();

				if( 0 == $this->getIsOverrideFeeAmountValidation() ) {
					$boolIsValid &= $this->valDebitPricingMatch();
					$boolIsValid &= $this->valCreditPricingMatch();
				}

				$boolIsValid &= $this->valNotificationEmail();

				$boolIsValid &= $this->valIsDisabled( $objClientDatabase );
				break;

			case 'validate_tabapay_onboard_update':
				$boolIsValid &= $this->valTabaPayClientId();
				break;

			case 'validate_new_international_account':
				if( CMerchantProcessingType::INTERNATIONAL != $this->getMerchantProcessingTypeId() ) {
					return false;
				}
				$boolIsValid &= $this->valInternationalAccount();
				$boolIsValid &= $this->valMerchantProcessingTypeId();
				$boolIsValid &= $this->valProcessingBankAccount();
				$boolIsValid &= $this->validateWorldpayCredentials();
				$boolIsValid &= $this->validateSepaInfo();
				$boolIsValid &= $this->valCommercialMerchantAccount();
				$boolIsValid &= $this->val3dsFlexDetails();
				break;

			case 'validate_international_account':
				if( CMerchantProcessingType::INTERNATIONAL != $this->getMerchantProcessingTypeId() ) {
					return false;
				}
				$boolIsValid &= $this->valInternationalAccount();
				$boolIsValid &= $this->valMerchantProcessingTypeId();
				$boolIsValid &= $this->valProcessingBankAccount();
				$boolIsValid &= $this->validateWorldpayCredentials();
				$boolIsValid &= $this->valCommercialMerchantAccount();
				$boolIsValid &= $this->val3dsFlexDetails();
				break;

			case 'validate_international_merchant_account_approval':
				if( CMerchantProcessingType::INTERNATIONAL != $this->getMerchantProcessingTypeId() ) {
					return false;
				}
				$boolIsValid &= $this->valInternationalAccount();
				$boolIsValid &= $this->valMerchantProcessingTypeId();

				return $boolIsValid;

			case VALIDATE_DELETE:
			default:
				// default case
				return true;
		}

		$boolIsValid &= $this->validateMethods();

		$this->collectMethodErrors();

		return $boolIsValid;
	}

	public function validateRecurring( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				if( 0 == $this->getIsOverrideFeeAmountValidation() ) {
					$boolIsValid &= $this->valDebitPricingMatch();

					foreach( $this->getMerchantMethods() as $objMerchantAccountMethod ) {
						if( CMerchantProcessingType::ACCOUNTS_PAYABLE == $this->getMerchantProcessingTypeId() && CPaymentType::ACH != $objMerchantAccountMethod->getPaymentTypeId() ) {
							continue;
						}

						$boolIsValid &= $objMerchantAccountMethod->validate( $this->getClient(), $this->getMerchantProcessingTypeId(), $this->getCountryCode(), $this->getEnableSeparateDebitPricing(), $this->getEnableVisaPilot() );
					}
				}
				break;

			case VALIDATE_DELETE:
			default:
				// default case
				$boolIsValid = true;
				break;
		}

		$this->collectMethodErrors();

		return $boolIsValid;
	}

	public function validateTerminal( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case 'validate_terminal_check_21_payment_insert':
				$boolIsValid &= $this->valCh21IsEnabled( true );
				break;

			case 'validate_terminal_emoney_order_payment_insert':
				$boolIsValid &= $this->valEmoIsEnabled( true );
				break;

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				if( 0 == $this->getIsOverrideFeeAmountValidation() ) {
					$boolIsValid &= $this->valDebitPricingMatch();

					foreach( $this->getMerchantMethods() as $objMerchantAccountMethod ) {
						if( CMerchantProcessingType::ACCOUNTS_PAYABLE == $this->getMerchantProcessingTypeId() && CPaymentType::ACH != $objMerchantAccountMethod->getPaymentTypeId() ) {
							continue;
						}

						$boolIsValid &= $objMerchantAccountMethod->validate( $this->getClient(), $this->getMerchantProcessingTypeId(), $this->getCountryCode(), $this->getEnableSeparateDebitPricing(), $this->getEnableVisaPilot() );
					}
				}
				break;

			case VALIDATE_DELETE:
			default:
				// default case
				$boolIsValid = true;
				break;
		}

		$this->collectMethodErrors();

		return $boolIsValid;
	}

	public function valIsDuplicateMerchantAccountForMerchantRequest( $intMerchantChangeRequestId, $objPaymentDatabase ) {

		$boolIsValid = true;

		$strSql = 'SELECT
					 count (cma.id)
				FROM
					merchant_change_request_accounts mcra
					JOIN company_merchant_accounts cma ON ( cma.cid = mcra.cid AND mcra.company_merchant_account_id = cma.id )
				WHERE
					mcra.merchant_change_request_id = ' . ( int ) $intMerchantChangeRequestId . '
					AND cma.account_name = \'' . $this->getAccountName() . '\';';

		$arrintResponse = fetchData( $strSql, $objPaymentDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) && 0 < $arrintResponse[0]['count'] ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valMerchantAccountMethodTiers() {
		// TODO: Ensure there are no collisions between MA-Method-Type-Max
	}

	// endregion

	// region Database Functions

	public function insert( $intCurrentUserId, $objAdminDatabase, $objPaymentDatabase = NULL, $boolOverrideInternationalMethodInsert = false ) {
		// If there is no valid id, fetch a new one to use (takes client's cloud into account)
		$this->setId( valId( $this->getId() ) ? $this->getId() : $this->fetchNextId( $objPaymentDatabase ) );

		if( !valId( $this->getId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert company merchant account record. Failed to fetch next company merchant account id.' ) );
			return false;
		}

		// Let the base class insert the merchant account
		if( !parent::insert( $intCurrentUserId, $objPaymentDatabase ) ) {
			return false;
		}

		$objMethod = $this->getMethodByAmount( CPaymentType::VISA );
		// Check here to see if a variable convenience fee exists on visa.  If so, set requires phone auth.
		if( false == is_null( $objMethod ) && false == is_null( $objMethod->getResidentDiscountRate() ) && 0 < $objMethod->getResidentDiscountRate() ) {
			$objMethod->setPhoneAuthRequired( 1 );
			$objMethod->setCalcFeesOnMaxPayment( 1 );
		}

		if( !$this->updateMerchantAccountMethods( $intCurrentUserId, $objPaymentDatabase ) ) {
			return false;
		}

		$this->sendNewMerchantAccountEmailToSupportEmployee( $objAdminDatabase );

		return true;
	}

	public function update( $intCurrentUserId, $objPaymentDatabase, $boolUpdateMethods = true, $boolOverrideMethodUpdate = false ) {
		if( !parent::update( $intCurrentUserId, $objPaymentDatabase ) ) {
			return false;
		}

		if( !$boolUpdateMethods ) return true;

		return $this->updateMerchantAccountMethods( $intCurrentUserId, $objPaymentDatabase );
	}

	public function delete( $intCurrentUserId, $objPaymentDatabase, $boolReturnSqlOnly = false ) {
		$objDataset = $objPaymentDatabase->createDataset();

		// Delete the methods first
		$this->deleteMerchantMethods( $intCurrentUserId, $objPaymentDatabase, $objDataset );

		$objDataset->cleanup();

		if( !parent::delete( $intCurrentUserId, $objPaymentDatabase ) ) {
			return false;
		}

		return true;
	}

	// This function is intentionally overridden in order to jump some range of ids from payments.ar_payments table.
	// Do not move this function from here.

	public function fetchNextId( $objDatabase, $strTableName = NULL ) {

		$this->updateIdSequence( $objDatabase );

		return parent::fetchNextId( $objDatabase );
	}

	public function updateIdSequence( $objDatabase, $intSequenceIncrementValue = 1 ) {
		$strSql = 'SELECT last_value AS id FROM company_merchant_accounts_id_seq';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( false != valArr( $arrmixData ) && false != isset( $arrmixData[0]['id'] ) ) {
			$intStartingId = $arrmixData[0]['id'];
			$intEndingId = $intStartingId + $intSequenceIncrementValue;

			if( CONFIG_CLOUD_ID == CCloud::LINDON_ID && self::MAX_ID_LINDON < $intEndingId ) {
				// we're in the Lindon cloud and the payment id is greater than 100 million
				$strSql = 'SELECT id FROM company_merchant_accounts WHERE id < ' . self::MAX_ID_LINDON . ' ORDER BY id DESC LIMIT 1';
				$intMinIdForCloud = 1;
			} elseif( CONFIG_CLOUD_ID == CCloud::IRELAND_ID && ( self::MAX_ID_IRELAND < $intEndingId || self::MIN_ID_IRELAND > $intStartingId ) ) {
				// we're in the Ireland cloud and the payment id is not between 100 and 200 million
				$strSql = 'SELECT id FROM company_merchant_accounts WHERE id BETWEEN ' . self::MIN_ID_IRELAND . ' AND ' . self::MAX_ID_IRELAND . ' ORDER BY id DESC LIMIT 1';
				$intMinIdForCloud = self::MIN_ID_IRELAND;
			} else {
				// no adjustment necessary
				return $intStartingId;
			}

			$arrmixResult = fetchData( $strSql, $objDatabase );
			if( !valArr( $arrmixResult ) ) {
				$intMaxId = $intMinIdForCloud;
			} else {
				$intMaxId = $arrmixResult[0]['id'];
			}
			$intNextId = $intMaxId + 1;
			$strSql = 'SELECT setval( \'company_merchant_accounts_id_seq\', ' . ( int ) $intNextId . ', FALSE )';
			fetchData( $strSql, $objDatabase );

			return $intNextId;
		}
	}

	protected function updateMerchantAccountMethods( $intCurrentUserId, $objPaymentDatabase ) {
		$objDataset = $objPaymentDatabase->createDataset();

		// Delete all methods first
		$this->deleteMerchantMethods( $intCurrentUserId, $objPaymentDatabase, $objDataset );

		$boolMethodsUpdated = true;
		$arrobjMerchantAccountMethods = $this->getMerchantMethods();
		$arrintEnabledGatewayTypeIds = $this->getEnabledGatewayTypeIds( $objPaymentDatabase );
		array_walk_recursive( $arrobjMerchantAccountMethods, function( CMerchantAccountMethod $objMerchantAccountMethod ) use( $intCurrentUserId, $objPaymentDatabase, $objDataset, $arrintEnabledGatewayTypeIds, &$boolMethodsUpdated ){
			// Do not insert methods that would not be supported by this merchant account
			if( $this->shouldSkipUnsupportedMethod( $objMerchantAccountMethod, $arrintEnabledGatewayTypeIds ) ) {
				return;
			}

			$objMerchantAccountMethod->setId( NULL );
			$objMerchantAccountMethod->setCompanyMerchantAccountId( $this->getId() );
			$objMerchantAccountMethod->setCid( $this->getCid() );

			// If the method has been soft deleted previously, restore it
			$objMerchantAccountMethod->setDeletedBy( NULL );
			$objMerchantAccountMethod->setDeletedOn( NULL );

			if( false == $objMerchantAccountMethod->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert merchant account method. Medium: ' . $objMerchantAccountMethod->getPaymentMediumId() . ' PaymentType: ' . $objMerchantAccountMethod->getPaymentTypeId() ) );
				$this->addErrorMsgs( $objMerchantAccountMethod->getErrorMsgs() );
				$this->addErrorMsgs( $objPaymentDatabase->getErrorMsgs() );

				$objDataset->cleanup();
				$boolMethodsUpdated = false;
			}
		} );

		$objDataset->cleanup();

		return $boolMethodsUpdated;
	}

	protected function deleteMerchantMethods( $intCurrentUserId, $objPaymentDatabase, $objDataset ) {
		$arrobjMerchantAccountMethods = \Psi\Eos\Payment\CMerchantAccountMethods::createService()->fetchMerchantAccountMethodsByCompanyMerchantAccountId( $this->getId(), $objPaymentDatabase ) ?: [];
		foreach( $arrobjMerchantAccountMethods as $objMerchantAccountMethod ) {
			if( !$objMerchantAccountMethod->delete( $intCurrentUserId, $objPaymentDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete merchant account method. Medium: ' . $objMerchantAccountMethod->getPaymentMediumId() . ' PaymentType: ' . $objMerchantAccountMethod->getPaymentTypeId() ) );
				$this->addErrorMsgs( $objMerchantAccountMethod->getErrorMsgs() );
				$this->addErrorMsgs( $objPaymentDatabase->getErrorMsgs() );
				$objDataset->cleanup();
				return false;
			}
		}
		return true;
	}

	// endregion

	// region Gateway Functions

	public function setEnabledGatewayTypeids( $arrintEnabledGatewayTypeIds ) {
		$this->m_arrintEnabledGatewayTypeIds = $arrintEnabledGatewayTypeIds;
		return $this;
	}

	public function loadPaymentGateways( $objPaymentDatabase ) {
		$arrobjGatewayTypes = \Psi\Eos\Payment\CGatewayTypes::createService()->fetchAllGatewayTypes( $objPaymentDatabase );
		$arrobjMerchantGateways = \Psi\Eos\Payment\CMerchantGateways::createService()->fetchMerchantGatewaysByIsPublished( 1, $objPaymentDatabase );
		$arrobjMerchantGatewaysByGatewayTypeId = rekeyObjects( 'GatewayTypeId', $arrobjMerchantGateways, true );

		// Flatten the tiers into a single array
		$arrobjMerchantAccountMethods = $this->getFlattenedMethods();

		$arrmixPaymentGateways = [];
		foreach( $arrobjGatewayTypes as $intId => $objGatewayType ) {
			$arrobjMerchantGateways = rekeyObjects( 'Id', $arrobjMerchantGatewaysByGatewayTypeId[$intId] );
			$boolIsDeleted = valId( $this->getId() ) ? true : !in_array( $intId, CGatewayType::$c_arrintGatewayIdsByProcessingBank[$this->getProcessingBankId()] );
			$arrmixPaymentGateways[$intId] = [
				'name'									=> $objGatewayType->getName(),
				'merchant_gateway_id'					=> 0,
				'merchant_gateways'						=> $arrobjMerchantGateways,
				'cut_off_time'							=> '',
				'is_deleted'							=> $boolIsDeleted,

				'gateway_username'						=> '',
				'gateway_password'						=> '',
				'login_username'						=> '',
				'login_password'						=> '',
				'minimum_convenience_fee'				=> '',

				'worldpay_cc_moto_username'				=> '',
				'worldpay_cc_moto_password'				=> '',
				'worldpay_cc_moto_merchant_code'		=> '',
				'worldpay_cc_ecom_username'				=> '',
				'worldpay_cc_ecom_password'				=> '',
				'worldpay_cc_ecom_merchant_code'		=> '',
				'worldpay_cc_recurring_username'		=> '',
				'worldpay_cc_recurring_password'		=> '',
				'worldpay_cc_recurring_merchant_code'	=> '',

				'sepa_creditor_id'						=> '',
				'sepa_bic'								=> '',
				'sepa_company_address'					=> '',
				'sepa_iban'								=> '',
				'sepa_iban_masked'						=> '',
				'sepa_company_name'						=> '',
			];

			// Loop over methods
			foreach( $arrobjMerchantAccountMethods as $objMerchantAccountMethod ) {
				//   Get the merchant gateway matching the method's merchant_gateway_id
				$objMerchantGateway = $arrobjMerchantGateways[$objMerchantAccountMethod->getMerchantGatewayId()] ?? NULL;
				if( is_null( $objMerchantGateway ) ) continue;

				// If the merchant gateway's type matches the current gateway type:
				// Set the merchant_gateway_id on the gateway type.
				$arrmixPaymentGateways[$intId]['merchant_gateway_id'] = $objMerchantAccountMethod->getMerchantGatewayId();

				// Get the cutoff time and deleted status
				$arrmixPaymentGateways[$intId]['cut_off_time'] = $objMerchantAccountMethod->getCutOffTime();
				$arrmixPaymentGateways[$intId]['is_deleted'] &= !is_null( $objMerchantAccountMethod->getDeletedOn() );

				// Harvest the additional info
				$arrmixPaymentGateways[$intId]['gateway_username']						= $objMerchantAccountMethod->getGatewayUsername();
				$arrmixPaymentGateways[$intId]['gateway_password']						= $objMerchantAccountMethod->getGatewayPassword();
				$arrmixPaymentGateways[$intId]['login_username']						= $objMerchantAccountMethod->getLoginUsername();
				$arrmixPaymentGateways[$intId]['login_password']						= $objMerchantAccountMethod->getLoginPassword();
				$arrmixPaymentGateways[$intId]['minimum_convenience_fee']				= $objMerchantAccountMethod->getMinimumConvenienceFee();

				$arrmixPaymentGateways[$intId]['worldpay_cc_moto_username']				= $objMerchantAccountMethod->getWorldpayCcMotoUsername();
				$arrmixPaymentGateways[$intId]['worldpay_cc_moto_password']				= $objMerchantAccountMethod->getWorldpayCcMotoPassword();
				$arrmixPaymentGateways[$intId]['worldpay_cc_moto_merchant_code']		= $objMerchantAccountMethod->getWorldpayCcMotoMerchantCode();
				$arrmixPaymentGateways[$intId]['worldpay_cc_ecom_username']				= $objMerchantAccountMethod->getWorldpayCcEcomUsername();
				$arrmixPaymentGateways[$intId]['worldpay_cc_ecom_password']				= $objMerchantAccountMethod->getWorldpayCcEcomPassword();
				$arrmixPaymentGateways[$intId]['worldpay_cc_ecom_merchant_code']		= $objMerchantAccountMethod->getWorldpayCcEcomMerchantCode();
				$arrmixPaymentGateways[$intId]['worldpay_cc_recurring_username']		= $objMerchantAccountMethod->getWorldpayCcRecurringUsername();
				$arrmixPaymentGateways[$intId]['worldpay_cc_recurring_password']		= $objMerchantAccountMethod->getWorldpayCcRecurringPassword();
				$arrmixPaymentGateways[$intId]['worldpay_cc_recurring_merchant_code']	= $objMerchantAccountMethod->getWorldpayCcRecurringMerchantCode();

				$arrmixPaymentGateways[$intId]['sepa_creditor_id']						= $objMerchantAccountMethod->getSepaCreditorId();
				$arrmixPaymentGateways[$intId]['sepa_bic']								= $objMerchantAccountMethod->getSepaBic();
				$arrmixPaymentGateways[$intId]['sepa_company_address']					= $objMerchantAccountMethod->getSepaCompanyAddress();
				$arrmixPaymentGateways[$intId]['sepa_iban']								= $objMerchantAccountMethod->getSepaIban();
				$arrmixPaymentGateways[$intId]['sepa_iban_masked']						= $objMerchantAccountMethod->getSepaIbanMasked();
				$arrmixPaymentGateways[$intId]['sepa_company_name']						= $objMerchantAccountMethod->getSepaCompanyName();

				// All methods that share this gateway should have the same info, so break out of the loop to avoid doing the work multiple times
				break;
			}
		}
		return $arrmixPaymentGateways;
	}

	public function getEnabledGatewayTypeIds( $objPaymentDatabase ) {
		$arrintResult = array_keys( array_filter( array_map( function( $arrmixPaymentGateway ) {
			return !$arrmixPaymentGateway['is_deleted'];
		}, $this->loadPaymentGateways( $objPaymentDatabase ) ) ) );

		if( valArr( $this->m_arrintEnabledGatewayTypeIds ) ) {
			$arrintResult = array_intersect( $arrintResult, $this->m_arrintEnabledGatewayTypeIds );
		}

		return $arrintResult;
	}

	// endregion

	// region Merchant Method Functions

	/**
	 * Magic function to replace all of the direct get/set that were
	 * part of the base class when we used the old view_company_merchant_accounts
	 * to load merchant accounts and methods in one giant, bloated object.
	 * We've tried to update everything to use $this->getOrFetchMethod but
	 * this is here just in case we missed any calling code that still uses
	 * the old getters and setters (e.g. getViMaxPaymentAmount)
	 *
	 * @param string $strMethod
	 * @param mixed[] $arrmixArguments
	 * @return mixed
	 */
	public function __call( $strMethod, $arrmixArguments ) {
		$arrmixMatches = [];
		if( false == preg_match( CPaymentType::GETTER_SETTER_REGEX, $strMethod, $arrmixMatches ) ) {
			return parent::__call( $strMethod, $arrmixArguments );
		}

		$intPaymentTypeId = CPaymentType::stringToId( $arrmixMatches[2] );
		$strMethodName = $arrmixMatches[1] . $arrmixMatches[3];

		if( 0 === strpos( $strMethod, 'get' ) || 0 === strpos( $strMethod, 'set' ) ) {
			$objMethod = $this->getMethodByAmount( $intPaymentTypeId );
			return $objMethod ? $objMethod->{$strMethodName}( ...$arrmixArguments ) : NULL;
		}

		return NULL;
	}

	/**
	 * @param $intPaymentTypeId
	 * @param int $fltPaymentAmount
	 * @return CMerchantAccountMethod
	 * @deprecated Please refactor to use {@see getMethodByAmount}, {@see methodExists}, {@see methodIsEnabled}, etc instead
	 */
	public function getOrFetchMethod( $intPaymentTypeId, $fltPaymentAmount = 0 ) {
		if( !in_array( $intPaymentTypeId, CPaymentType::$c_arrintMerchantAccountPaymentTypeIds ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Invalid payment type Id for merchant account.' ) );
			return NULL;
		}

		if( !$this->valPaymentMediumId() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Invalid payment medium Id.' ) );
			return NULL;
		}

		// Load methods for this payment type if they aren't cached in memory
		if( !isset( $this->getMerchantMethods()[$this->getPaymentMediumId()] ) ) {
			$this->loadMethods();
		}

		// Find the method matching the payment type with the smallest max_payment_amount greater than $fltPaymentAmount
		/** @var CMerchantAccountMethod[] $arrobjMethodAccountMethod */
		foreach( $this->getMerchantMethods()[$this->getPaymentMediumId()] ?? [] as $intTier => $arrobjMethodAccountMethods ) {
			$objMethod = $arrobjMethodAccountMethods[$intPaymentTypeId] ?? NULL;
			if( $objMethod && $objMethod->getMaxPaymentAmount() ?: PHP_INT_MAX >= $fltPaymentAmount ) {
				return $objMethod;
			}
		}

		// If we reach this point, there are no tiers with the
		return NULL;
	}

	public function loadMethods() {
		if( !$this->valPaymentDatabase() && valObj( $this->getDatabase(), 'CDatabase' ) ) {
			$this->setPaymentDatabase( $this->getDatabase() );
		} elseif( !$this->valPaymentDatabase() && !valObj( $this->getDatabase(), 'CDatabase' ) ) {
			$this->setPaymentDatabase( CDatabases::createDatabase( CDatabaseUserType::PS_PROPERTYMANAGER, CDatabaseType::PAYMENT ) );
		}

		$this->setMerchantMethods( \Psi\Eos\Payment\CMerchantAccountMethods::createService()->fetchMerchantAccountMethodTiers( $this->getId(), $this->getPaymentDatabase() ) );
	}

	public function createMethod() {
		return new CMerchantAccountMethod();
	}

	public function getFlattenedMethods() {
		$arrobjMerchantAccountMethodTiers = $this->getMerchantMethods();

		/** @var CMerchantAccountMethod[] $arrobjMerchantAccountMethods */
		$arrobjMerchantAccountMethods = [];
		array_walk_recursive( $arrobjMerchantAccountMethodTiers, function( $objMerchantMethod ) use( &$arrobjMerchantAccountMethods ) {
			$arrobjMerchantAccountMethods[] = $objMerchantMethod;
		} );

		return $arrobjMerchantAccountMethods;
	}

	// endregion

	// region Other Functions

	public function applyRequestForm( $arrstrRequestForm, $arrstrFormFields = NULL ) {

		if( true == isset( $arrstrRequestForm['target_discount_rate'] ) ) {
			$arrstrRequestForm['target_discount_rate'] = ( $arrstrRequestForm['target_discount_rate'] ?: 0 ) / 100;
		}
		if( true == isset( $arrstrRequestForm['every_payment_fee'] ) ) {
			$arrstrRequestForm['every_payment_fee'];
		}
		if( true == isset( $arrstrRequestForm['stored_billing_fee'] ) ) {
			$arrstrRequestForm['stored_billing_fee'];
		}

		foreach( CPaymentType::$c_arrintMerchantAccountPaymentTypeIds as $intPaymentTypeId ) {
			$strPaymentType = CPaymentType::idToString( $intPaymentTypeId );

			if( true == isset( $arrstrRequestForm[$strPaymentType . '_provider_discount_rate'] ) && 0 < $arrstrRequestForm[$strPaymentType . '_provider_discount_rate'] ) {
				$arrstrRequestForm[$strPaymentType . '_provider_discount_rate'] = $arrstrRequestForm[$strPaymentType . '_provider_discount_rate'] / 100;
			}
			if( true == isset( $arrstrRequestForm[$strPaymentType . '_company_discount_rate'] ) && 0 < $arrstrRequestForm[$strPaymentType . '_company_discount_rate'] ) {
				$arrstrRequestForm[$strPaymentType . '_company_discount_rate'] = $arrstrRequestForm[$strPaymentType . '_company_discount_rate'] / 100;
			}
			if( true == isset( $arrstrRequestForm[$strPaymentType . '_company_debit_discount_rate'] ) && 0 < $arrstrRequestForm[$strPaymentType . '_company_debit_discount_rate'] ) {
				$arrstrRequestForm[$strPaymentType . '_company_debit_discount_rate'] = $arrstrRequestForm[$strPaymentType . '_company_debit_discount_rate'] / 100;
			}
			if( true == isset( $arrstrRequestForm[$strPaymentType . '_resident_discount_rate'] ) && 0 < $arrstrRequestForm[$strPaymentType . '_resident_discount_rate'] ) {
				$arrstrRequestForm[$strPaymentType . '_resident_discount_rate'] = $arrstrRequestForm[$strPaymentType . '_resident_discount_rate'] / 100;
			}
			if( true == isset( $arrstrRequestForm[$strPaymentType . '_resident_debit_discount_rate'] ) && 0 < $arrstrRequestForm[$strPaymentType . '_resident_debit_discount_rate'] ) {
				$arrstrRequestForm[$strPaymentType . '_resident_debit_discount_rate'] = $arrstrRequestForm[$strPaymentType . '_resident_debit_discount_rate'] / 100;
			}
			if( true == isset( $arrstrRequestForm[$strPaymentType . '_waived_discount_rate'] ) && 0 < $arrstrRequestForm[$strPaymentType . '_waived_discount_rate'] ) {
				$arrstrRequestForm[$strPaymentType . '_waived_discount_rate'] = $arrstrRequestForm[$strPaymentType . '_waived_discount_rate'] / 100;
			}
			if( true == isset( $arrstrRequestForm[$strPaymentType . '_waived_debit_discount_rate'] ) && 0 < $arrstrRequestForm[$strPaymentType . '_waived_debit_discount_rate'] ) {
				$arrstrRequestForm[$strPaymentType . '_waived_debit_discount_rate'] = $arrstrRequestForm[$strPaymentType . '_waived_debit_discount_rate'] / 100;
			}
			if( true == isset( $arrstrRequestForm[$strPaymentType . '_downgrade_discount_rate'] ) && 0 < $arrstrRequestForm[$strPaymentType . '_downgrade_discount_rate'] ) {
				$arrstrRequestForm[$strPaymentType . '_downgrade_discount_rate'] = $arrstrRequestForm[$strPaymentType . '_downgrade_discount_rate'] / 100;
			}
			if( true == isset( $arrstrRequestForm[$strPaymentType . '_international_discount_rate'] ) && 0 < $arrstrRequestForm[$strPaymentType . '_international_discount_rate'] ) {
				$arrstrRequestForm[$strPaymentType . '_international_discount_rate'] = $arrstrRequestForm[$strPaymentType . '_international_discount_rate'] / 100;
			}
			if( true == isset( $arrstrRequestForm[$strPaymentType . '_rebate_discount_rate'] ) && 0 < $arrstrRequestForm[$strPaymentType . '_rebate_discount_rate'] ) {
				$arrstrRequestForm[$strPaymentType . '_rebate_discount_rate'] = $arrstrRequestForm[$strPaymentType . '_rebate_discount_rate'] / 100;
			}
			if( true == isset( $arrstrRequestForm[$strPaymentType . '_resident_commercial_discount_rate'] ) && 0 < $arrstrRequestForm[$strPaymentType . '_resident_commercial_discount_rate'] ) {
				$arrstrRequestForm[$strPaymentType . '_resident_commercial_discount_rate'] = $arrstrRequestForm[$strPaymentType . '_resident_commercial_discount_rate'] / 100;
			}
			if( true == isset( $arrstrRequestForm[$strPaymentType . '_resident_international_discount_rate'] ) && 0 < $arrstrRequestForm[$strPaymentType . '_resident_international_discount_rate'] ) {
				$arrstrRequestForm[$strPaymentType . '_resident_international_discount_rate'] = $arrstrRequestForm[$strPaymentType . '_resident_international_discount_rate'] / 100;
			}
		}

		if( true == isset( $arrstrRequestForm['cc_max_payment_amount'] ) ) {
			$this->setCcMaxPaymentAmount( $arrstrRequestForm['cc_max_payment_amount'] );
		}

		if( true == isset( $arrstrRequestForm['waived_distribution_fee'] ) && 0 == $arrstrRequestForm['waived_distribution_fee'] && true == isset( $arrstrRequestForm['distribution_fee'] ) && 'varies' == strtolower( $arrstrRequestForm['distribution_fee'] ) ) {
			$arrstrRequestForm['distribution_fee'] = $this->getDistributionFee();
		}

		parent::applyRequestForm( $arrstrRequestForm, $arrstrFormFields );

		if( CMerchantProcessingType::INTERNATIONAL == $this->getMerchantProcessingTypeId() ) {
			if( CMerchantGateway::WORLDPAY_INTL_CREDIT_CARD == $this->getCcMerchantGatewayId() ) {
				if( true == isset( $arrstrRequestForm['worldpay_3ds_organisation_unit_id'] ) ) {
					$this->setWorldpay3dsOrganisationUnitId( $arrstrRequestForm['worldpay_3ds_organisation_unit_id'] );
				}
				if( true == isset( $arrstrRequestForm['worldpay_3ds_jwt_issuer_id'] ) ) {
					$this->setWorldpay3dsJwtIssuerId( $arrstrRequestForm['worldpay_3ds_jwt_issuer_id'] );
				}
				if( true == isset( $arrstrRequestForm['worldpay_3ds_jwt_mac_key'] ) ) {
					$this->setWorldpay3dsJwtMacKey( $arrstrRequestForm['worldpay_3ds_jwt_mac_key'] );
				}
				if( true == isset( $arrstrRequestForm['worldpay_3ds_flex_is_enabled'] ) ) {
					$this->setWorldpay3dsFlexIsEnabled( ( bool ) $arrstrRequestForm['worldpay_3ds_flex_is_enabled'] );
				}

				$this->setWorldpayCcCredentials( $arrstrRequestForm );
			}

			$this->setSepaFields( $arrstrRequestForm );
		}
	}

	/**
	 * @param $arrstrRequestForm
	 */
	private function setWorldpayCcCredentials( $arrstrRequestForm ) {

		$arrobjMethods = $this->getMerchantMethods();

		array_walk_recursive( $arrobjMethods, function ( CMerchantAccountMethod $objMethod ) use ( &$arrstrRequestForm ) {

			if( in_array( $objMethod->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) ) {

				$strPaymentType = CPaymentType::idToString( $objMethod->getPaymentTypeId() );

				if( true == isset( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_moto_username'] ) ) {
					$objMethod->setWorldpayCcMotoUsername( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_moto_username'] );
				}
				if( true == isset( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_moto_password'] ) ) {
					$objMethod->setWorldpayCcMotoPassword( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_moto_password'] );
				}
				if( true == isset( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_moto_merchant_code'] ) ) {
					$objMethod->setWorldpayCcMotoMerchantCode( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_moto_merchant_code'] );
				}
				if( true == isset( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_ecom_username'] ) ) {
					$objMethod->setWorldpayCcEcomUsername( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_ecom_username'] );
				}
				if( true == isset( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_ecom_password'] ) ) {
					$objMethod->setWorldpayCcEcomPassword( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_ecom_password'] );
				}
				if( true == isset( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_ecom_merchant_code'] ) ) {
					$objMethod->setWorldpayCcEcomMerchantCode( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_ecom_merchant_code'] );
				}
				if( true == isset( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_recurring_username'] ) ) {
					$objMethod->setWorldpayCcRecurringUsername( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_recurring_username'] );
				}
				if( true == isset( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_recurring_password'] ) ) {
					$objMethod->setWorldpayCcRecurringPassword( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_recurring_password'] );
				}
				if( true == isset( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_recurring_merchant_code'] ) ) {
					$objMethod->setWorldpayCcRecurringMerchantCode( $arrstrRequestForm[$strPaymentType . '_worldpay_cc_recurring_merchant_code'] );
				}
			}
		} );
	}

	/**
	 * @param $arrstrRequestForm
	 * @throws \Psi\Libraries\Cryptography\Exceptions\CCryptoErrorException
	 * @throws \Psi\Libraries\Cryptography\Exceptions\CCryptoInvalidException
	 */
	private function setSepaFields( $arrstrRequestForm ) {

		$arrobjMethods = $this->getMerchantMethods();

		array_walk_recursive( $arrobjMethods, function ( CMerchantAccountMethod $objMethod ) use ( &$arrstrRequestForm ) {

			if( \CPaymentType::SEPA_DIRECT_DEBIT == $objMethod->getPaymentTypeId() ) {

				if( true == isset( $arrstrRequestForm['sepa_company_name'] ) ) {
					$objMethod->setSepaCompanyName( $arrstrRequestForm['sepa_company_name'] );
				}
				if( true == isset( $arrstrRequestForm['sepa_worldpay_mandate_prefix'] ) ) {
					$objMethod->setWorldpayMandatePrefix( $arrstrRequestForm['sepa_worldpay_mandate_prefix'] );
				}

				$objMethod->setProcessingBankAccountId( \CProcessingBankAccount::INTERNATIONAL );
				$objMethod->setMerchantGatewayId( \CMerchantGateway::SEPA_DIRECT_DEBIT );

				if( true == isset( $arrstrRequestForm['sepa_worldpay_ecom_username'] ) ) {
					$objMethod->setWorldpayEcomUsername( $arrstrRequestForm['sepa_worldpay_ecom_username'] );
				}
				if( true == isset( $arrstrRequestForm['sepa_worldpay_ecom_password'] ) ) {
					$objMethod->setWorldpayEcomPassword( $arrstrRequestForm['sepa_worldpay_ecom_password'] );
				}
				if( true == isset( $arrstrRequestForm['sepa_worldpay_ecom_merchant_code'] ) ) {
					$objMethod->setWorldpayEcomMerchantCode( $arrstrRequestForm['sepa_worldpay_ecom_merchant_code'] );
				}
				if( true == isset( $arrstrRequestForm['sepa_worldpay_recurring_username'] ) ) {
					$objMethod->setWorldpayRecurringUsername( $arrstrRequestForm['sepa_worldpay_recurring_username'] );
				}
				if( true == isset( $arrstrRequestForm['sepa_worldpay_recurring_password'] ) ) {
					$objMethod->setWorldpayRecurringPassword( $arrstrRequestForm['sepa_worldpay_recurring_password'] );
				}
				if( true == isset( $arrstrRequestForm['sepa_worldpay_recurring_merchant_code'] ) ) {
					$objMethod->setWorldpayRecurringMerchantCode( $arrstrRequestForm['sepa_worldpay_recurring_merchant_code'] );
				}

			}
		} );
	}

	public function enforceNullRates() {
		$arrobjMerchantAccountMethods = $this->getMerchantMethods();
		array_walk_recursive( $arrobjMerchantAccountMethods, function( CMerchantAccountMethod $objMethod ) {
			if( CPaymentType::ACH != $objMethod->getPaymentTypeId() ) return;
			$objMethod->setResidentDiscountRate( 0 );
			$objMethod->setCompanyDiscountRate( 0 );
			$objMethod->setWaivedDiscountRate( 0 );
			if( CPaymentMedium::TERMINAL == $objMethod->getPaymentMediumId() ) {
				$objMethod->setResidentTransactionFee( 0 );
			}
		} );
	}

	public function increaseAmountDue( $fltIncreaseAmount ) {
		if( false == is_numeric( $this->m_fltAmountDue ) ) $this->m_fltAmountDue = 0;
		if( false == is_numeric( $fltIncreaseAmount ) ) return;

		$this->m_fltAmountDue += $fltIncreaseAmount;
	}

	public function sqlCid() {
		if( true == valObj( $this->m_objClient, CClient::class ) ) {
			return $this->m_objClient->sqlId();
		} else {
			return parent::sqlCid();
		}
	}

	public function generateFirstRegionalGateway() {

		$objGateway = new CFirstRegionalGateway();
		$objGateway->initialize();

		if( false == is_null( $this->getMerchantName() ) ) {
			$objGateway->setMerchantName( $this->getMerchantName() );
		} else {
			$objGateway->setMerchantName( $this->getAccountName() );
		}

		return $objGateway;
	}

	public function generateZionsGateway() {

		$objGateway = new CZionsGateway();
		$objGateway->initialize();

		if( false == is_null( $this->getMerchantName() ) ) {
			$objGateway->setMerchantName( $this->getMerchantName() );
		} else {
			$objGateway->setMerchantName( $this->getAccountName() );
		}

		return $objGateway;
	}

	public function generateFifthThirdGateway() {

		$objGateway = new CFifthThirdGateway();
		$objGateway->initialize();

		if( false == is_null( $this->getMerchantName() ) ) {
			$objGateway->setMerchantName( $this->getMerchantName() );
		} else {
			$objGateway->setMerchantName( $this->getAccountName() );
		}

		return $objGateway;
	}

	public function generateWesternUnionGateway() {

		$objGateway = new CWesternUnionGateway();
		$objGateway->initialize();

		if( false == is_null( $this->getMerchantName() ) ) {
			$objGateway->setMerchantName( $this->getMerchantName() );
		} else {
			$objGateway->setMerchantName( $this->getAccountName() );
		}

		return $objGateway;
	}

	public function generateMoneyGramGateway() {

		$objGateway = new CMoneyGramGateway();
		$objGateway->initialize();

		if( false == is_null( $this->getMerchantName() ) ) {
			$objGateway->setMerchantName( $this->getMerchantName() );
		} else {
			$objGateway->setMerchantName( $this->getAccountName() );
		}

		return $objGateway;
	}

	public function generateVitalGateway() {

		$objGateway = new CVitalGateway();
		$objGateway->initialize();

		if( false == is_null( $this->getMerchantName() ) ) {
			$objGateway->setMerchantName( $this->getMerchantName() );
		} else {
			$objGateway->setMerchantName( $this->getAccountName() );
		}

		return $objGateway;
	}

	public function generateMyEcheckGateway() {

		$objGateway = new CMyEcheckGateway();
		$objGateway->initialize();

		if( false == is_null( $this->getMerchantName() ) ) {
			$objGateway->setMerchantName( $this->getMerchantName() );
		} else {
			$objGateway->setMerchantName( $this->getAccountName() );
		}

		return $objGateway;
	}

	public function clearErrorMsgs() {
		$this->m_arrobjErrorMsgs = NULL;
	}

	public function releaseConflictingEndorsements( $intCurrentUserId, $objPaymentDatabase ) {

		$objOldCompanyMerchantAccount = \Psi\Eos\Payment\CCompanyMerchantAccounts::createService()->fetchCompanyMerchantAccountById( $this->getId(), $objPaymentDatabase );

		if( false == valObj( $objOldCompanyMerchantAccount, CCompanyMerchantAccount::class ) ) {
			return false;
		}

		if( $this->getProcessingBankId() == $objOldCompanyMerchantAccount->getProcessingBankId() ) {
			return true;
		}

		$arrobjArPaymentImages = ( array ) CArPaymentImages::fetchUnBatchedEndorsedArPaymentImagesByCompanyMerchantAccountId( $this->getId(), $objPaymentDatabase );

		foreach( $arrobjArPaymentImages as $objArPaymentImage ) {

			if( false === stristr( $objArPaymentImage->getImageName(), 'endorsed_' ) ) {
				continue;
			}

			$objArPaymentImage->setImageName( str_replace( 'endorsed_', '', $objArPaymentImage->getImageName() ) );
			$objArPaymentImage->setEndorsedOn( NULL );

			if( false == $objArPaymentImage->update( $intCurrentUserId, $objPaymentDatabase ) ) {
				$this->addErrorMsgs( $objArPaymentImage->getErrorMsgs() );
				return false;
			}
		}

		// Look for ar payment images that are locked in last hour but not endorsed
		$arrobjArPaymentImages = CArPaymentImages::fetchUnEndorsedLockedArPaymentImagesByCompanyMerchantAccountId( $this->getId(), $objPaymentDatabase );

		if( false == valArr( $arrobjArPaymentImages ) ) return true;

		$objPaymentEmailer		= new CPaymentEmailer();
		$arrstrEmailAddresses	= array( CSystemEmail::BBLAKER_EMAIL_ADDRESS, CSystemEmail::RJENSEN_EMAIL_ADDRESS, CSystemEmail::XENTO_RPARDESHI_EMAIL_ADDRESS );

		$strSubject = 'Ar Payment Images Incorrectly Endorsed';
		$strMessage = 'Ar Payment Images may not have been endorsed with correct processing bank Id(' . $this->getProcessingBankId() . ') for ' . PHP_EOL;
		$strMessage .= 'Client Id: ' . $this->getCid() . PHP_EOL;
		$strMessage .= 'Merchant Account Name: ' . $this->getAccountName() . PHP_EOL;
		$strMessage .= 'Ar Payment Image Ids: ' . implode( ', ', array_keys( $arrobjArPaymentImages ) );

		$objPaymentEmailer->emailEmergencyTeam( $strMessage, $strSubject, $arrstrEmailAddresses );

		return true;
	}

	/**
	 * If the MA is of international type, then we need to skip the insertion/updating of non-supported payment methods
	 * Supported international methods => CPaymentType::$c_arrintInternationalPaymentTypes
	 *
	 * @param CMerchantAccountMethod $objMerchantAccountMethod
	 * @return bool
	 */
	private function shouldSkipUnsupportedMethod( CMerchantAccountMethod $objMerchantAccountMethod, $arrintEnabledGatewayTypeIds ) {
		$arrintPaymentTypeIds = [];
		foreach( $arrintEnabledGatewayTypeIds as $intGatewayTypeId ) {
			$arrintPaymentTypeIds = array_merge( $arrintPaymentTypeIds, CPaymentType::$c_arrmixPaymentTypesByGatewayTypeId[$intGatewayTypeId] );
		}
		$arrintPaymentTypeIds = array_intersect( $arrintPaymentTypeIds, \Psi\Eos\Entrata\CPaymentTypes::createService()->getSupportedPaymentTypeIdsByCountryCode( $this->getCountryCode() ) );

		if( !in_array( $objMerchantAccountMethod->getPaymentTypeId(), $arrintPaymentTypeIds ) ) {
			return true;
		}

		return false;
	}

	private function sendNewMerchantAccountEmailToSupportEmployee( $objAdminDatabase ) {
		// Send support person an email to let them know a new merchant account has been set up for one of their clients.
		$objClient  = $this->fetchClient( $objAdminDatabase );

		if( !valObj( $objClient, CClient::class ) ) {
			trigger_error( 'client failed to load.', E_USER_WARNING );
			return;
		}

		$objPsLeadDetail = $objClient->fetchPsLeadDetail( $objAdminDatabase );

		if( !valObj( $objPsLeadDetail, CPsLeadDetail::class ) ) {
			trigger_error( 'Company detail failed to load.', E_USER_WARNING );
			return;
		}

		if( defined( 'CONFIG_ENVIRONMENT' ) && 'production' == CONFIG_ENVIRONMENT ) {
			$objSupportEmployee = $objPsLeadDetail->fetchSupportEmployee( $objAdminDatabase );

			if( valObj( $objSupportEmployee, CEmployee::class ) && $objSupportEmployee->getEmailAddress() ) {
				mail( $objSupportEmployee->getEmailAddress(), 'New merchant account', 'A new merchant account has been set up for your client (' . $objClient->getCompanyName() . ').  The merchant account name is (' . $this->getAccountName() . ').  If this is an integrated client, you may need to configure integration elements (e.g. bank books for eSite).', 'from:' . CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
			}
		}
	}

	// endregion

}
