<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937CheckDetailAddendumCRecords
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CX937CheckDetailAddendumCRecords extends CBaseX937CheckDetailAddendumCRecords {

    public static function fetchX937CheckDetailAddendumCRecordsByX937FileId( $intX937FileId, $objPaymentDatabase ) {

		$strSql = '	SELECT
						xcdacr.*
					FROM
						x937_check_detail_addendum_c_records xcdacr
    					INNER JOIN x937_check_detail_records xcdr ON xcdacr.x937_check_detail_record_id = xcdr.id
					WHERE
						xcdr.x937_file_id = ' . ( int ) $intX937FileId;

		return self::fetchX937CheckDetailAddendumCRecords( $strSql, $objPaymentDatabase );
	}

}
?>