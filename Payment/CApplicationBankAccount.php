<?php

class CApplicationBankAccount extends CBaseApplicationBankAccount {

	const MERCHANT_POSSIBLE_BANK_ACCOUNTS = 100;

    /**
     * Get Functions
     */

    public function getBankAccountNumber() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_APPLICATION_BANK_ACCOUNTS ];
        return( true == isset( $this->m_strBankAccountNumberEncrypted ) ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strBankAccountNumberEncrypted, CONFIG_SODIUM_KEY_APPLICATION_BANK_ACCOUNTS, $arrmixOptions ) : NULL;
    }

    public function getBankRoutingNumber() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_APPLICATION_BANK_ACCOUNTS ];
		return( true == isset( $this->m_strBankRoutingNumberEncrypted ) ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strBankRoutingNumberEncrypted, CONFIG_SODIUM_KEY_APPLICATION_BANK_ACCOUNTS, $arrmixOptions ) : NULL;
    }

	/**
	 * Set Functions
	 */

	public function setBankAccountNumber( $strBankAccountNumber ) {
		if( false == valStr( $strBankAccountNumber ) ) {
			$this->setBankAccountNumberEncrypted( NULL );

			return NULL;
		}

        $this->setBankAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strBankAccountNumber, CONFIG_SODIUM_KEY_APPLICATION_BANK_ACCOUNTS ) );

    }

    public function setBankRoutingNumber( $strBankRoutingNumber ) {
		if( false == valStr( $strBankRoutingNumber ) ) {
			$this->setBankRoutingNumberEncrypted( NULL );
			$this->setBankRoutingNumberMasked( NULL );

			return NULL;
		}

        $this->setBankRoutingNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strBankRoutingNumber, CONFIG_SODIUM_KEY_APPLICATION_BANK_ACCOUNTS ) );
		$this->setBankRoutingNumberMasked( \Psi\Libraries\UtilEncryption\CEncryption::createService()->maskText( $strBankRoutingNumber ) );
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrValues['bank_routing_number'] ) ) $this->setBankRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_routing_number'] ) : $arrValues['bank_routing_number'] );
        if( true == isset( $arrValues['bank_account_number'] ) ) $this->setBankAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_account_number'] ) : $arrValues['bank_account_number'] );

        return;
    }

    /**
     * Validation Functions
     */

    public function valPropertyName() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', 'Property name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valCompletedOn() {
        $boolIsValid = true;

        if( true == isset( $this->m_strCompletedOn ) && 1 != CValidation::validateDate( $this->m_strCompletedOn ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_name', 'Completed on date is not valid.' ) );
        }

        return $boolIsValid;
    }

    public function valBankName() {
        $boolIsValid = true;

        if( true == is_null( $this->getBankName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_name', 'Bank name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valBankAccountNumber() {
        $boolIsValid = true;

        if( 0 == strlen( trim( $this->getBankAccountNumber() ) ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_account_number', 'Bank account number is required.' ) );
        } elseif( false == is_numeric( trim( $this->getBankAccountNumber() ) ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_account_number', 'Bank account number not a valid number.' ) );
        }

        return $boolIsValid;
    }

    public function valBankRoutingNumber( $objPaymentDatabase = NULL ) {
        $boolIsValid = true;

        if( 0 == strlen( trim( $this->getBankRoutingNumber() ) ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_routing_number', 'Routing number is required.' ) );
        } elseif( false == is_numeric( $this->getBankRoutingNumber() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_routing_number', 'Routing number not a valid number.', 623 ) );
        } elseif( 9 != strlen( trim( $this->getBankRoutingNumber() ) ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_routing_number', 'Routing number must be 9 digits long.', 623 ) );
        }

		if( false == is_null( $objPaymentDatabase ) ) {
			// If it is 9 numeric characters see if it is a fed ach participant

			$objFedAchParticipant = \Psi\Eos\Payment\CFedAchParticipants::createService()->fetchFedAchParticipantByRoutingNumber( trim( $this->getBankRoutingNumber() ), $objPaymentDatabase );

			if( true == is_null( $objFedAchParticipant ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_routing_number', 'Routing number was not found.', 624 ) );
			}
		}

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valPropertyName();
            	$boolIsValid &= $this->valBankName();
            	$boolIsValid &= $this->valBankAccountNumber();
            	$boolIsValid &= $this->valBankRoutingNumber();
				break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCompletedOn();
            	$boolIsValid &= $this->valBankAccountNumber();
            	$boolIsValid &= $this->valBankRoutingNumber();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Other Functions
     */

    public function applyRequestForm( $arrRequestForm, $arrFormFields = NULL ) {

        if( true == valArr( $arrFormFields ) ) {
            $arrRequestForm = mergeIntersectArray( $arrFormFields, $arrRequestForm );
        }

        $this->setValues( $arrRequestForm, false );
    }

}
?>