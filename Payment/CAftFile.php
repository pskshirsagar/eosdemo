<?php

class CAftFile extends CBaseAftFile {

	/** @var CAftFileBatch[] */
	protected $m_arrobjAftFileBatches;
	protected $m_fltCreditTotal;
	protected $m_fltDebitTotal;

	public function loadAssociatedBatchesAndDetailRecords( $objPaymentDatabase ) {
		$this->m_arrobjAftFileBatches = \Psi\Eos\Payment\CAftFileBatches::createService()->fetchAftFileBatchesByAftFileId( $this->getId(), $objPaymentDatabase );

		foreach( $this->m_arrobjAftFileBatches as $objAftFileBatch ) {
			$objAftFileBatch->loadAftDetailRecords( $objPaymentDatabase );
		}
	}

	public function getAftFileBatches() {
		return $this->m_arrobjAftFileBatches;
	}

	/**
	 * @return CAftDetailRecord[]
	 */
	public function getAftDetailRecords() {
		$arrobjAftDetailRecords = [];

		foreach( $this->m_arrobjAftFileBatches as $objAftFileBatch ) {
			$arrobjAftDetailRecords = array_merge( $objAftFileBatch->getAftDetailRecords(), $arrobjAftDetailRecords );
		}

		return $arrobjAftDetailRecords;
	}

	public function getCreditTotal() {
		return $this->m_fltCreditTotal;
	}

	public function setCreditTotal( $fltCreditTotal ) {
		$this->m_fltCreditTotal = $fltCreditTotal;
	}

	public function getDebitTotal() {
		return $this->m_fltDebitTotal;
	}

	public function setDebitTotal( $fltDebitTotal ) {
		$this->m_fltDebitTotal = $fltDebitTotal;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['debit_total'] ) ) $this->setDebitTotal( $arrmixValues['debit_total'] );
		if( true == isset( $arrmixValues['credit_total'] ) ) $this->setCreditTotal( $arrmixValues['credit_total'] );

		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentFileId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAftFileTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessingBankId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMerchantGatewayId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginatorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileCreationNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileCreationDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDestinationDataCentre() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalValueDebit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalValueCredit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalCountDebit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalCountCredit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransmittedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConfirmedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>