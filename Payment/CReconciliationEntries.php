<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CReconciliationEntries
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CReconciliationEntries extends CBaseReconciliationEntries {

	public static function fetchAllUnreconciledReconciliationEntriesByProcessingBankAccountId( $intProcessingBankAccountId, $objPaymentDatabase, $strToDate = NULL ) {

		$strSql = 'SELECT
						re.*,
						ret.name as reconciliation_entry_type_name,
						ap.cid
					FROM
						reconciliation_entries re
						LEFT JOIN reconciliation_entry_types ret ON ( re.reconciliation_entry_type_id = ret.id )
						LEFT JOIN ar_payments ap ON ( re.ar_payment_id = ap.id )
					WHERE
						re.reconciliation_id IS NULL
						AND re.processing_bank_account_id = ' . ( int ) $intProcessingBankAccountId;

		if( false == is_null( $strToDate ) ) {
			$strSql	.= ' AND movement_datetime <= \'' . $strToDate . ' 23:59:00\'';
		}

		$strSql	.= ' ORDER BY re.movement_datetime DESC, re.total DESC, re.transaction_details ASC';

		return self::fetchReconciliationEntries( $strSql, $objPaymentDatabase );
	}

	public static function fetchAllReconciledReconciliationEntriesByReconciliationId( $intReconciliationId, $objPaymentDatabase, $strToDate = NULL ) {

		$strSql = 'SELECT
						re.*,
						ap.cid
					FROM
						reconciliation_entries re
						LEFT JOIN ar_payments ap ON ( re.ar_payment_id = ap.id )
					WHERE
						re.reconciliation_id = ' . ( int ) $intReconciliationId;

		if( false == is_null( $strToDate ) ) {
			$strSql	.= ' AND movement_datetime <= \'' . $strToDate . ' 23:59:00\'';
		}

		$strSql	.= ' ORDER BY re.movement_datetime DESC, re.total DESC, re.transaction_details ASC';

		return self::fetchReconciliationEntries( $strSql, $objPaymentDatabase );
	}

	public static function fetchReconciliationEntriesByProcessingBankAccountIdByIds( $intProcessingBankAccountId, $arrintReconciliationEntriesIds, $objPaymentDatabase ) {
		$strSql = 'SELECT re.*	FROM reconciliation_entries re	WHERE re.id IN (' . implode( ',', $arrintReconciliationEntriesIds ) . ') AND re.processing_bank_account_id = ' . ( int ) $intProcessingBankAccountId;
		return self::fetchReconciliationEntries( $strSql, $objPaymentDatabase );
	}

	public static function fetchReconciliationEntryByProcessingBankAccountIdById( $intProcessingBankAccountId, $intReconciliationEntryId, $objPaymentDatabase ) {
		$strSql = 'SELECT re.* FROM	reconciliation_entries re WHERE	re.id = ' . ( int ) $intReconciliationEntryId . ' AND re.processing_bank_account_id = ' . ( int ) $intProcessingBankAccountId;
		return self::fetchReconciliationEntry( $strSql, $objPaymentDatabase );
	}

    public static function fetchReconciliationEntriesByReconciliationId( $intReconciliationId, $objPaymentDatabase ) {
        return self::fetchReconciliationEntries( 'SELECT * FROM reconciliation_entries WHERE reconciliation_id = ' . ( int ) $intReconciliationId . ' ORDER BY movement_datetime ASC', $objPaymentDatabase );
    }

    public static function fetchReconciliationEntriesByProcessingBankAccountIdByReconciliationEntryTypeId( $intProcessingBankAccountId, $intReconciliationEntryTypeId, $objPaymentDatabase ) {
		$strSql = 'SELECT re.* FROM reconciliation_entries re WHERE re.processing_bank_account_id = ' . ( int ) $intProcessingBankAccountId . '	AND	re.reconciliation_entry_type_id = ' . ( int ) $intReconciliationEntryTypeId . ' ORDER BY re.movement_datetime';
		return self::fetchReconciliationEntries( $strSql, $objPaymentDatabase );
    }

    public static function fetchPaginatedUnReconciledReconciliations( $intPageNo, $intPageSize, $strStartDate, $objDatabase ) {

    	$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    	$intLimit = ( int ) $intPageSize;

    	$strSql = 'SELECT
    					re.*
    				FROM
				    	reconciliation_entries re
				    	JOIN processing_bank_accounts pba ON (
														    	re.processing_bank_account_id = pba.id
														    	AND re.reconciliation_id IS NULL
														    	AND pba.last_reconciled_on IS NULL
			    											 )
			    	WHERE
				    	DATE_TRUNC(\'day\', re.created_on) < \'' . $strStartDate . '\'
				    ORDER BY
			    		re.created_on DESC
    				OFFSET ' . ( int ) $intOffset . '
    				LIMIT ' . $intLimit;

    	return self::fetchReconciliationEntries( $strSql, $objDatabase );
    }

    public static function fetchUnReconciledReconciliationsCount( $strStartDate, $objDatabase ) {

    	$strSql = 'SELECT
    					count(re.id) as count
    				FROM
    					reconciliation_entries re
    				JOIN processing_bank_accounts pba ON (
    														re.processing_bank_account_id = pba.id
    														AND re.reconciliation_id IS NULL
    														AND pba.last_reconciled_on IS NULL
    													 )
    				WHERE
    					DATE_TRUNC(\'day\', re.created_on) < \'' . $strStartDate . '\'';

    	$arrstrData = fetchData( $strSql, $objDatabase );

    	return ( true == valArr( $arrstrData ) )? $arrstrData[0]['count']:0;
    }

    public static function fetchReconciliationEntriesByReconciliationEntryTypeId( $intReconciliationEntryTypeId, $objDatabase ) {

    	$strSql = 'SELECT * FROM reconciliation_entries WHERE reconciliation_entry_type_id = ' . ( int ) $intReconciliationEntryTypeId;
    	return self::fetchReconciliationEntries( $strSql, $objDatabase );
    }

    public static function fetchReconciliationEntriesByBai2FileId( $intBai2FileId, $objDatabase ) {
    	return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE bai2_file_id = %d', ( int ) $intBai2FileId ), $objDatabase );
    }

    public static function fetchReconciliationEntriesDataByIds( $arrintReconciliationEntryIds, $objDatabase ) {
	    $strSql = '	SELECT
						re.id,
						re.company_payment_id,
						re.ar_payment_id,
						COALESCE(
							nedr.cid,
							nradr.cid,
							cb.cid,
							sd.cid,
							ap.cid,
							( SELECT
									MAX( bap.cid )
								FROM
									batched_ar_payments bap
								WHERE
								bap.eft_batch_id = eb.id )
							) cid,
						nradr.return_type_id,
						nradr.return_reason_code,
						eb.gateway_username_encrypted
					FROM
						reconciliation_entries re
						LEFT JOIN nacha_entry_detail_records nedr ON re.nacha_entry_detail_record_id = nedr.id
						LEFT JOIN nacha_return_addenda_detail_records nradr ON re.nacha_return_addenda_detail_record_id = nradr.id
						LEFT JOIN charge_backs cb ON re.charge_back_id = cb.id
						LEFT JOIN eft_batches eb ON re.eft_batch_id = eb.id
						LEFT JOIN x937_files xf ON re.x937_file_id = xf.id
						LEFT JOIN x937_return_files xrf ON re.x937_return_file_id = xrf.id
						LEFT JOIN nacha_return_files nrf ON re.nacha_return_file_id = nrf.id
						LEFT JOIN settlement_distributions sd ON re.settlement_distribution_id = sd.id
						LEFT JOIN western_union_output_files wuof ON re.western_union_output_file_id = wuof.id
						LEFT JOIN ar_payments ap ON re.ar_payment_id = ap.id
					WHERE
						re.id IN ( ' . implode( ',', $arrintReconciliationEntryIds ) . ')';

	    return fetchData( $strSql, $objDatabase );
    }

}
?>