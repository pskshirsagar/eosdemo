<?php

class CClearingBatch extends CBaseClearingBatch {

	protected $m_strNotificationEmailAddress;

	public function getNotificationEmailAddress() {
		return $this->m_strNotificationEmailAddress;
	}

	public function setNotificationEmailAddress( $strNotificationEmailAddress ) {
		$this->m_strNotificationEmailAddress = $strNotificationEmailAddress;
	}

	/**
	 * Validation Functions
	 */

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intCid ) && 0 >= ( int ) $this->m_intCid ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required for clearing batch.' ) );
		}

		return $boolIsValid;
	}

	public function valAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intAccountId ) && 0 >= ( int ) $this->m_intAccountId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_id', 'A account is required for a clearing batch.' ) );
		}

		return $boolIsValid;
	}

	public function valProcessingBankId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intProcessingBankId ) && 0 >= ( int ) $this->m_intProcessingBankId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_bank_id', 'A processing bank is required for a clearing batch.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyMerchantAccountId() {
		$boolIsValid = true;

		if( true == isset( $this->m_intCompanyMerchantAccountId ) && 0 >= ( int ) $this->m_intCompanyMerchantAccountId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_merchant_account_id', 'A company merchant account is required for a clearing batch.' ) );
		}

		return $boolIsValid;
	}

	public function valClearingAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->m_fltClearingAmount ) || 0 == bccomp( $this->m_fltClearingAmount, 0, 2 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', 'Payment amount is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valProcessingBankId();
				$boolIsValid &= $this->valCompanyMerchantAccountId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

    /**
     * Other Functions
     */

	public function processReturn( $intCurrentUserId, $objPaymentDatabase ) {
		$this->setReturnedOn( date( 'm/d/Y' ) );

		if( false == $this->update( $intCurrentUserId, $objPaymentDatabase ) ) {
			trigger_error( 'Returning clearing batch failed to update.', E_USER_WARNING );
			return false;
		}

		return true;
	}

}

?>