<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CWesternUnionOutputRecords
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */
class CWesternUnionOutputRecords extends CBaseWesternUnionOutputRecords {

	public static function fetchWesternUnionOutputRecordsByFileId( $intFileId, $objPaymentDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						western_union_output_records
					WHERE
						file_id = ' . ( int ) $intFileId;

		return self::fetchWesternUnionOutputRecords( $strSql, $objPaymentDatabase );
	}

}
?>