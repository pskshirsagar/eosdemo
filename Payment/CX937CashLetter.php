<?php

class CX937CashLetter extends CBaseX937CashLetter {

	protected $m_objPaymentDatabase;

	protected $m_arrobjX937Bundles;

	protected $m_intBundleHeaderSequenceNumberPosition;

    public function __construct() {
        parent::__construct();

        $this->m_intBundleHeaderSequenceNumberPosition = 1;
        $this->m_arrobjX937Bundles = array();

        return;
    }

	/**
	 * Add Functions
	 */

	public function addX937Bundle( $objX937Bundle ) {
		$this->m_arrobjX937Bundles[$objX937Bundle->getId()] = $objX937Bundle;
	}

	/**
	 * Create Functions
	 */

	public function createX937Bundle() {

		$objX937Bundle = new CX937Bundle();
		$objX937Bundle->setPaymentDatabase( $this->m_objPaymentDatabase );
		$objX937Bundle->setDefaults();

		$objX937Bundle->setX937FileId( $this->m_intX937FileId );
		$objX937Bundle->setX937CashLetterId( $this->m_intId );

		// A code used to identify the type of bundle. [03-04] (Bundle has same value as cash letter)
		$objX937Bundle->setHeaderCollectionTypeIndicator( $this->m_strHeaderCollectionTypeIndicator );

		// A number used to identify the institution that receives and processes the cash letter or the bundle. [05-13] (Bundle has same value as cash letter)
		$objX937Bundle->setHeaderDestinationRoutingNumber( $this->m_strHeaderDestinationRoutingNumber );

		// A number used to identify the institution that creates the bundle header record. [14-22]  (Bundle has same value as cash letter)
		$objX937Bundle->setHeaderEceInstitutionRoutingNumber( $this->m_strHeaderEceInstitutionRoutingNumber );

		// The year, month, and day that designates the business date of the bundle.  The date typically assigned to research this bundle. [23-30] (Bundle has same value as cash letter)
		$objX937Bundle->setHeaderBusinessDate( $this->m_strHeaderBusinessDate );

		// The year, month, and day that designates the date the bundle is created.  The default should be based on GMT. [31-38] (Bundle has same value as cash letter)
    	$objX937Bundle->setHeaderCreationDate( $this->m_strHeaderCreationDate );

    	// A number assigned by the institution that creates the bundle.  Usually denotes the relative position of the bundle within the cash letter. [49-52]
    	$objX937Bundle->setHeaderSequenceNumber( $this->m_intBundleHeaderSequenceNumberPosition );
    	$objX937Bundle->setHeaderCycleNumber( $this->m_intBundleHeaderSequenceNumberPosition );
		$this->m_intBundleHeaderSequenceNumberPosition++;

		$this->m_arrobjX937Bundles[] = $objX937Bundle;

		return $objX937Bundle;
	}

	/**
	 * Set Functions
	 */

	public function setDefaults() {

    	$this->setId( $this->fetchNextId( $this->m_objPaymentDatabase ) );

    	// Identifies type of record.  For image cash letter header use '10' [01-02]
    	$this->setHeaderRecordType( '10' );

    	// A code used to identify the type of cash letter.  Must be the same as the collection type indicator in the bundle header record in that cash letter. [03-04]
    	$this->setHeaderCollectionTypeIndicator( '01' );

		// A number used to identify the institution that receives and processes the cash letter or the bundle.  This is the bank recieving the cash letter.
		// So each residents bank will receive a cash letter who processed a payment. This should be the Federal Reserve Right?[5-13]
    	//$this->setHeaderDestinationRoutingNumber(); // (set in CX937File::createX937CashLetter)

		// A number to identify the institution that creates the cash letter header record. [14-22]
    	//$this->setHeaderEceInstitutionRoutingNumber(); // (set in CX937File::createX937CashLetter)

    	// The year, month, and day that designates the business date of the cash letter.  The date typically assigned to research this cash letter. [23-30]
    	$this->setHeaderBusinessDate( date( 'Ymd' ) );

    	// The year, month, and day the cash letter is created.  The default date shall be based on GMT. The local time zone or a specific time zone may be used under clearing arrangements. [31-38]
    	$this->setHeaderCreationDate( date( 'Ymd' ) );

    	// The time the cash letter is created.  The default time shall be GMT.  The local time zone or a specific time zone may be used under clearing arrangements. [39-42]
    	$this->setHeaderCreationTime( date( 'Hi' ) );

    	// This  indicates the presence of records or the type of records contained in the cash letter.  If an image is associated to any (even one) Check Detail Record (Type 25),
    	// the cash letter must have a Cash Letter Record Type Indicator of Defined Value of "I" or "F". [43]
    	$this->setHeaderRecordTypeIndicator( 'I' );

		// A code that indicates the type of documentation that supports all check records in the cash letter.  This code indicates whether or not the items contained int eh cash letter
		// are the same time. [44] (I think we should use G 'Image included', no paper provided)
    	$this->setHeaderDocumenationTypeIndicator( 'G' );

    	// This is an ID assigned by Entrata to identify the cash letter. Only 8 digits are allowed. [45-52] (When we rearch our 100,000,000 cash letter, numbers will start over.
    	$this->setHeaderId( $this->m_intId );

		// A contact at the institution that creates the cash letter.[53-66]
    	// $this->setHeaderOriginatorContactName( 'Char Arrindell' );(set in CX937File::createX937CashLetter)

    	// The phone number used to contact the institution that creates the cash letter.[67-76]
    	// $this->setHeaderOriginatorContactPhoneNumber( '8007770929' );(set in CX937File::createX937CashLetter)

    	// A code used to specify the Federal Reserve work type. I don't think we will have to specify this. [77]
    	$this->setHeaderFedWorkType( 'C' );

    	// A field used at the discretion of users of the standard. [78-79]
    	$this->setHeaderUserField( '  ' );

    	// A field used at the discretion of users of the standard. [80]
    	$this->setHeaderReserved( ' ' );

    	// A code used to identify the type of record [01-02]
    	$this->setControlRecordType( '90' );

		// The number used to indicate the total number of bundles within the cash letter. [03-08]
    	// $this->setControlBundleCount( '' ); // Set in CX937File.class.php

		// The total number of items sent within a cash letter [09-16]
    	// $this->setControlItemsCount(); // Set in CX937File.class.php

    	// The amount sum value of the items within the cash letter. [17-30]
    	// $this->setControlTotalAmount(); // Set in CX937File.class.php

    	// The total number of images views within a cash letter.  Each image is represented by an Image View Detail Record (Type 50) and an Image View Data Record (Type 52) pair. [31-39]
    	// $this->setControlImagesCount(); // Set in CX937File.class.php

		// The short name used to identify the institution that creates the Cash Letter Control Record (Type 90). [40-57]
    	// $this->setControlEceInstitutionName( 'FIRST REGIONAL BAN' );(set in CX937File::createX937CashLetter)

		// The year, month and day that the institution creating the cash letter expects the settlement to occur. [58-65]  Should it be today or tomorrow?  I'm assuming today.
    	$this->setControlSettlementDate( '        ' );

    	// A field reserved for future use by the Accredited Standards Committe X9. [66-80]
    	$this->setControlReserved( '               ' );
	}

    public function setPaymentDatabase( $objPaymentDatabase ) {
    	$this->m_objPaymentDatabase = $objPaymentDatabase;
    }

	public function setHeaderRecordType( $strHeaderRecordType ) {
	    $this->m_strHeaderRecordType = CStrings::strNachaDef( $strHeaderRecordType, 2, true, true, '0' );
	}

	public function setX937File( $objX937File ) {
	    $this->m_objX937File = $objX937File;
	}

	public function setHeaderCollectionTypeIndicator( $strHeaderCollectionTypeIndicator ) {
	    $this->m_strHeaderCollectionTypeIndicator = CStrings::strNachaDef( $strHeaderCollectionTypeIndicator, 2, true, true, '0' );
	}

	public function setHeaderDestinationRoutingNumber( $strHeaderDestinationRoutingNumber ) {
	    $this->m_strHeaderDestinationRoutingNumber = CStrings::strNachaDef( $strHeaderDestinationRoutingNumber, 9, true, true, '0' );
	}

	public function setHeaderEceInstitutionRoutingNumber( $strHeaderEceInstitutionRoutingNumber ) {
	    $this->m_strHeaderEceInstitutionRoutingNumber = CStrings::strNachaDef( $strHeaderEceInstitutionRoutingNumber, 9, true, true, '0' );
	}

	public function setHeaderBusinessDate( $strHeaderBusinessDate ) {
	    $this->m_strHeaderBusinessDate = CStrings::strNachaDef( $strHeaderBusinessDate, 8, true, true, '0' );
	}

	public function setHeaderCreationDate( $strHeaderCreationDate ) {
	    $this->m_strHeaderCreationDate = CStrings::strNachaDef( $strHeaderCreationDate, 8, true, true, '0' );
	}

	public function setHeaderCreationTime( $strHeaderCreationTime ) {
	    $this->m_strHeaderCreationTime = CStrings::strNachaDef( $strHeaderCreationTime, 4, true, true, '0' );
	}

	public function setHeaderRecordTypeIndicator( $strHeaderRecordTypeIndicator ) {
	    $this->m_strHeaderRecordTypeIndicator = CStrings::strNachaDef( $strHeaderRecordTypeIndicator, 1, false, false );
	}

	public function setHeaderDocumenationTypeIndicator( $strHeaderDocumenationTypeIndicator ) {
	    $this->m_strHeaderDocumenationTypeIndicator = CStrings::strNachaDef( $strHeaderDocumenationTypeIndicator, 1, false, false );
	}

	public function setHeaderId( $strHeaderId ) {
	    $this->m_strHeaderId = CStrings::strNachaDef( $strHeaderId, 8, false, false );
	}

	public function setHeaderOriginatorContactName( $strHeaderOriginatorContactName ) {
	    $this->m_strHeaderOriginatorContactName = CStrings::strNachaDef( $strHeaderOriginatorContactName, 14, false, false );
	}

	public function setHeaderOriginatorContactPhoneNumber( $strHeaderOriginatorContactPhoneNumber ) {
	    $this->m_strHeaderOriginatorContactPhoneNumber = CStrings::strNachaDef( $strHeaderOriginatorContactPhoneNumber, 10, true, true, '0' );
	}

	public function setHeaderFedWorkType( $strHeaderFedWorkType ) {
	    $this->m_strHeaderFedWorkType = CStrings::strNachaDef( $strHeaderFedWorkType, 1, false, false );
	}

	public function setHeaderUserField( $strHeaderUserField ) {
	    $this->m_strHeaderUserField = CStrings::strNachaDef( $strHeaderUserField, 2, false, false );
	}

	public function setHeaderReserved( $strHeaderReserved ) {
	    $this->m_strHeaderReserved = CStrings::strNachaDef( $strHeaderReserved, 1, false, false );
	}

	public function setControlRecordType( $strControlRecordType ) {
	    $this->m_strControlRecordType = CStrings::strNachaDef( $strControlRecordType, 2, true, true, '0' );
	}

	public function setControlBundleCount( $strControlBundleCount ) {
	    $this->m_strControlBundleCount = CStrings::strNachaDef( $strControlBundleCount, 6, true, true, '0' );
	}

	public function setControlItemsCount( $strControlItemsCount ) {
	    $this->m_strControlItemsCount = CStrings::strNachaDef( $strControlItemsCount, 8, true, true, '0' );
	}

	public function setFormattedControlTotalAmount( $strFormattedControlTotalAmount ) {

    	// Validate to make sure amount is in $$.cc format
    	if( false == stristr( $strFormattedControlTotalAmount, '.' ) ) {
    		trigger_error( 'Total Control Cash Letter Total Amount is not in $$.cc format.', E_USER_ERROR );
    		exit;
    	}

	    $this->m_strControlTotalAmount = CStrings::strNachaDef( $strFormattedControlTotalAmount, 14, true, true, '0' );
	}

	public function setControlImagesCount( $strControlImagesCount ) {
	    $this->m_strControlImagesCount = CStrings::strNachaDef( $strControlImagesCount, 9, true, true, '0' );
	}

	public function setControlEceInstitutionName( $strControlEceInstitutionName ) {
	    $this->m_strControlEceInstitutionName = CStrings::strNachaDef( $strControlEceInstitutionName, 18, false, false );
	}

	public function setControlSettlementDate( $strControlSettlementDate ) {
	    $this->m_strControlSettlementDate = CStrings::strNachaDef( $strControlSettlementDate, 8, true, true );
	}

	public function setControlReserved( $strControlReserved ) {
	    $this->m_strControlReserved = CStrings::strNachaDef( $strControlReserved, 15, false, false );
	}

	/**
	 * Get Functions
	 */

	public function getX937Bundles() {
		return $this->m_arrobjX937Bundles;
	}

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	/**
	 * Other Functions
	 */

	public function buildHeaderRecord( $boolConvertToEbcdic = false ) {

		$strRecord = '';

		$strRecord .= $this->getHeaderRecordType();
		$strRecord .= $this->getHeaderCollectionTypeIndicator();
		$strRecord .= $this->getHeaderDestinationRoutingNumber();
		$strRecord .= $this->getHeaderEceInstitutionRoutingNumber();
		$strRecord .= $this->getHeaderBusinessDate();
		$strRecord .= $this->getHeaderCreationDate();
		$strRecord .= $this->getHeaderCreationTime();
		$strRecord .= $this->getHeaderRecordTypeIndicator();
		$strRecord .= $this->getHeaderDocumenationTypeIndicator();
		$strRecord .= $this->getHeaderId();
		$strRecord .= $this->getHeaderOriginatorContactName();
		$strRecord .= $this->getHeaderOriginatorContactPhoneNumber();
		$strRecord .= $this->getHeaderFedWorkType();
		$strRecord .= $this->getHeaderUserField();
		$strRecord .= $this->getHeaderReserved();

		if( true == $boolConvertToEbcdic ) {
			$strRecord = \Psi\Libraries\UtilStrings\CStrings::createService()->asciiToEbcdic( $strRecord );
		}

		return $strRecord;
	}

	public function buildControlRecord( $boolConvertToEbcdic = false ) {

		$strRecord = '';

		$strRecord .= $this->getControlRecordType();
		$strRecord .= $this->getControlBundleCount();
		$strRecord .= $this->getControlItemsCount();
		$strRecord .= $this->getControlTotalAmount();
		$strRecord .= $this->getControlImagesCount();
		$strRecord .= $this->getControlEceInstitutionName();
		$strRecord .= $this->getControlSettlementDate();
		$strRecord .= $this->getControlReserved();

		if( true == $boolConvertToEbcdic ) {
			$strRecord = \Psi\Libraries\UtilStrings\CStrings::createService()->asciiToEbcdic( $strRecord );
		}

		return $strRecord;
	}
}
?>