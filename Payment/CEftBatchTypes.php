<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CEftBatchTypes
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CEftBatchTypes extends CBaseEftBatchTypes {

	public static function fetchEftBatchTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEftBatchType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEftBatchType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEftBatchType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>