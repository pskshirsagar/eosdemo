<?php

class CArPaymentNote extends CBaseArPaymentNote {

	/**
	 * Set Functions
	 */

	public function setNote( $strNote ) {
		$this->m_strNote = CStrings::strTrimDef( $strNote, -1, NULL, true, false, true );
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getId())) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ));
		// }

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getCid())) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ));
		// }

		return $boolIsValid;
	}

	public function valArPaymentId() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getArPaymentId())) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_payment_id', '' ));
		// }

		return $boolIsValid;
	}

	public function valNote() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		if( true == is_null( $this->getNote() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Note is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNoteDatetime() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getNoteDatetime())) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note_datetime', '' ));
		// }

		return $boolIsValid;
	}

	public function valUpdatedBy() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getUpdatedBy())) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ));
		// }

		return $boolIsValid;
	}

	public function valUpdatedOn() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getUpdatedOn())) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ));
		// }

		return $boolIsValid;
	}

	public function valCreatedBy() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getCreatedBy())) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ));
		// }

		return $boolIsValid;
	}

	public function valCreatedOn() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getCreatedOn())) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ));
		// }

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valNote();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>