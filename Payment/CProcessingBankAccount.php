<?php

class CProcessingBankAccount extends CBaseProcessingBankAccount {

	const FRB_CENTRAL_ACH						= 1;
	const FRB_CENTRAL_CHECK21					= 2;
	const FRB_CENTRAL_CREDIT_CARD				= 3;
	const FRB_CENTRAL_MONEY_ORDER				= 6;
	const ZIONS_CENTRAL_ACH						= 7;

	// NOTE: Our Zions C21 Anticipatory funding if the also the Zions ACH account
	//  In the future we may add additional C21 anticipatory accounts
	const ZIONS_CENTRAL_CHECK21_ANTICIPATORY	= 7;
	const ZIONS_CENTRAL_CHECK21					= 8;
	const FRB_PSI_OPERATING						= 9;
	const FRB_CENTRAL_ACH_RETURNS				= 10;
	const ZIONS_CENTRAL_ACH_RETURNS				= 11;
	const ZIONS_PSI_OPERATING					= 12;
	const ZIONS_CHECK21_RETURNS_ACCOUNT			= 13;
	const ZIONS_PSI_ELECTRONIC_RECEIPTS			= 14;
	const ZIONS_CREDIT_CARD						= 15;
	const FIFTH_THIRD_CENTRAL_INTERMEDIARY		= 17;
	const ZIONS_UTILITY_BILLING					= 18;
	const ZIONS_CENTRAL_MONEYGRAM				= 19;

	// Insurance Bank Accounts
	const ZIONS_MARKEL_PSIA_INSURANCE			= 16;
	const ZIONS_KEMPER_PSIA_INSURANCE			= 20;
	const ZIONS_QBE_PSIA_INSURANCE				= 26;
	const ZIONS_IDTHEFT_PSIA_INSURANCE			= 28;
	const ZIONS_MASTER_POLICY_PSIA_INSURANCE	= 29;

	const ZIONS_CREDIT_CARD_LITLE				= 21;
	const ZIONS_TABAPAY							= 30;

	// Fifth Third bank accounts
	const FIFTH_THIRD_ACH						= 22;
	const FIFTH_THIRD_CHECK21					= 23;
	const FIFTH_THIRD_CREDIT_CARD				= 24;
	const FIFTH_THIRD_MONEYGRAM					= 25;

	const BANK_OF_MONTREAL_OPERATING			= 34;
	const BANK_OF_MONTREAL_ACH_CREDIT_CARD		= 32;
	const BANK_OF_MONTREAL_REVENUE				= 31;

	const INTERNATIONAL							= 33;

	public static $c_arrintAccountsRequiringDeposits = array(
		self::ZIONS_PSI_OPERATING,
		self::ZIONS_PSI_ELECTRONIC_RECEIPTS,
		self::ZIONS_MARKEL_PSIA_INSURANCE,
		self::ZIONS_KEMPER_PSIA_INSURANCE,
		self::ZIONS_QBE_PSIA_INSURANCE,
		self::ZIONS_IDTHEFT_PSIA_INSURANCE,
		self::ZIONS_MASTER_POLICY_PSIA_INSURANCE
	);

	// For use in Ach Customer Payments Script
	protected $m_arrobjPositiveArPayments;
	protected $m_arrobjNegativeArPayments;
	protected $m_arrobjX937CheckDetailRecords;
	protected $m_arrobjX937ReturnItems;
	protected $m_arrobjSettlementDistributions;

	protected $m_objClient;

	protected $m_fltPositiveArPaymentsTotalAmount;
	protected $m_fltNegativeArPaymentsTotalAmount;

	protected $m_strUsePreexisting;
	// For reconciliations
	protected $m_strLastReconciledOn;
	protected $m_strBalance;

	// For reconciliations
	protected $m_intPendingReconciliationId;

	// For use in Ach Settlement Distributions Script
	protected $m_arrobjSettlingArPayments;
	protected $m_arrobjReturningArPayments;
	protected $m_arrobjReversingArPayments;
	protected $m_arrobjReturningReversedArPayments;

	protected $m_fltClearingAmount;

    public function __construct() {
        parent::__construct();

	    $this->m_fltBalance = 0;
	    $this->m_fltPositiveArPaymentsTotalAmount = 0;
	    $this->m_fltNegativeArPaymentsTotalAmount = 0;
		$this->m_fltClearingAmount = 0;

        $this->m_arrobjPositiveArPayments 		        = array();
        $this->m_arrobjNegativeArPayments 		        = array();
        $this->m_arrobjReversalArPayments 		        = array();

        $this->m_arrobjSettlingArPayments               = array();
        $this->m_arrobjReturningArPayments              = array();
        $this->m_arrobjReversingArPayments              = array();
        $this->m_arrobjReturningReversedArPayments      = array();

        $this->m_arrobjX937CheckDetailRecords 			= array();
        $this->m_arrobjX937ReturnItems 					= array();

        $this->m_arrobjSettlementDistributions 			= array();

        return;
    }

	/**
	 * Get or Fetch Functions
	 */

	public function getOrFetchClient( $objPaymentDatabase ) {

		if( false == valObj( $this->m_objClient, 'CClient' ) ) {
			$this->m_objClient = $this->fetchClient( $objPaymentDatabase );
		}

		return $this->m_objClient;
	}

	public function getAccountNumberMasked() {

		$strAccountNumber = $this->getAccountNumber();
		$intStringLength  = strlen( $strAccountNumber );
		$strLastFour      = substr( $strAccountNumber, -4 );
		return str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	/**
	 * Add Functions
	 */

	public function addPositiveArPayment( $objPositiveArPayment ) {
		$this->m_arrobjPositiveArPayments[$objPositiveArPayment->getId()] = $objPositiveArPayment;
	}

	public function addNegativeArPayment( $objNegativeArPayment ) {
		$this->m_arrobjNegativeArPayments[$objNegativeArPayment->getId()] = $objNegativeArPayment;
	}

    public function addX937CheckDetailRecord( $objX937CheckDetailRecord ) {
    	$this->m_arrobjX937CheckDetailRecords[$objX937CheckDetailRecord->getId()] = $objX937CheckDetailRecord;
    }

    public function addX937ReturnItem( $objX937ReturnItem ) {
    	$this->m_arrobjX937ReturnItems[$objX937ReturnItem->getId()] = $objX937ReturnItem;
    }

	public function addSettlingArPayment( $objSettlingArPayment ) {
		$this->m_arrobjSettlingArPayments[$objSettlingArPayment->getId()] = $objSettlingArPayment;
	}

	public function addReturningArPayment( $objReturningArPayment ) {
		$this->m_arrobjReturningArPayments[$objReturningArPayment->getId()] = $objReturningArPayment;
	}

	public function addReversingArPayment( $objReversingArPayment ) {
		$this->m_arrobjReversingArPayments[$objReversingArPayment->getId()] = $objReversingArPayment;
	}

	public function addReturningReversedArPayment( $objReturningReversedArPayment ) {
		$this->m_arrobjReturningReversedArPayments[$objReturningReversedArPayment->getId()] = $objReturningReversedArPayment;
	}

	public function addSettlementDistribution( $objSettlementDistribution ) {
		$this->m_arrobjSettlementDistributions[$objSettlementDistribution->getId()] = $objSettlementDistribution;
	}

	/**
	 * Get Functions
	 */

	public function getPositiveArPayments() {
		return $this->m_arrobjPositiveArPayments;
	}

	public function getNegativeArPayments() {
		return $this->m_arrobjNegativeArPayments;
	}

	public function getClient() {
		return $this->m_objClient;
	}

	public function getPositiveArPaymentsTotalAmount() {
		return $this->m_fltPositiveArPaymentsTotalAmount;
	}

	public function getNegativeArPaymentsTotalAmount() {
		return $this->m_fltNegativeArPaymentsTotalAmount;
	}

	public function getSettlingArPayments() {
		return $this->m_arrobjSettlingArPayments;
	}

	public function getReturningArPayments() {
		return $this->m_arrobjReturningArPayments;
	}

	public function getReversingArPayments() {
		return $this->m_arrobjReversingArPayments;
	}

	public function getReturningReversedArPayments() {
		return $this->m_arrobjReturningReversedArPayments;
	}

	public function getAccountNumber() {

		if( false == valStr( $this->getAccountNumberEncrypted() ) ) {
			return NULL;
		}

		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	public function getX937CheckDetailRecords() {
		return $this->m_arrobjX937CheckDetailRecords;
	}

    public function getX937ReturnItems() {
        return $this->m_arrobjX937ReturnItems;
    }

	public function getUsePreexisting() {
        return $this->m_strUsePreexisting;
	}

    public function getLastReconciledOn() {
		return $this->m_strLastReconciledOn;
    }

    public function getSettlementDistributions() {
		return $this->m_arrobjSettlementDistributions;
    }

    public function getBalance() {
		return $this->m_fltBalance;
    }

	public function getClearingAmount() {
		return $this->m_fltClearingAmount;
	}

	public function getPendingReconciliationId() {
		return $this->m_intPendingReconciliationId;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['last_reconciled_on'] ) ) $this->setLastReconciledOn( $arrmixValues['last_reconciled_on'] );
		if( true == isset( $arrmixValues['balance'] ) ) $this->setBalance( $arrmixValues['balance'] );
		if( true == isset( $arrmixValues['pending_reconciliation_id'] ) ) 			$this->setPendingReconciliationId( $arrmixValues['pending_reconciliation_id'] );
		return;
	}

	public function setClient( $objClient ) {
	    $this->m_objClient = $objClient;
	}

	public function setAccountNumber( $strPlainAccountNumber ) {
		if( true == valStr( $strPlainAccountNumber ) ) {
			$objCrypto = \Psi\Libraries\Cryptography\CCrypto::createService();
			$this->setAccountNumberEncrypted( $objCrypto->encrypt( $strPlainAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
			$this->setAccountNumberBindex( $objCrypto->blindIndex( $strPlainAccountNumber, CONFIG_SODIUM_KEY_BLIND_INDEX ) );
		}
	}

	public function setUsePreexisting( $intUsePreexisting ) {
        $this->m_strUsePreexisting = CStrings::strTrimDef( $intUsePreexisting, 20, NULL, true );
	}

	public function setLastReconciledOn( $strLastReconciledOn ) {
		$this->m_strLastReconciledOn = $strLastReconciledOn;
	}

	public function setBalance( $fltBalance ) {
		$this->m_fltBalance = $fltBalance;
	}

	public function setClearingAmount( $fltClearingAmount ) {
		$this->m_fltClearingAmount = $fltClearingAmount;
	}

	public function setPendingReconciliationId( $intPendingReconciliationId ) {
		$this->m_intPendingReconciliationId = $intPendingReconciliationId;
	}

	/**
	 * Create Functions
	 */

	public function createReconciliation() {

		$objReconciliation = new CReconciliation();
		$objReconciliation->setProcessingBankAccountId( $this->getId() );

		return $objReconciliation;
	}

	/**
	 * Other Functions
	 */

	public function calculatePositiveArPaymentsTotalAmount() {
        $this->m_fltPositiveArPaymentsTotalAmount = 0;
        if( true == valArr( $this->m_arrobjPositiveArPayments ) ) {
            foreach( $this->m_arrobjPositiveArPayments as $objPositiveArPayment ) {
                $this->m_fltPositiveArPaymentsTotalAmount += $objPositiveArPayment->getTotalAmount();
            }
        }
	}

	public function calculateNegativeArPaymentsTotalAmount() {
        $this->m_fltNegativeArPaymentsTotalAmount = 0;
        if( true == valArr( $this->m_arrobjNegativeArPayments ) ) {
            foreach( $this->m_arrobjNegativeArPayments as $objNegativeArPayment ) {
                $this->m_fltNegativeArPaymentsTotalAmount += $objNegativeArPayment->getTotalAmount();
            }
        }
	}

	public function fetchClient( $objPaymentDatabase ) {
		if ( false == is_numeric( $this->getCid() ) ) return NULL;
		return CClients::fetchClientById( $this->getCid(), $objPaymentDatabase );
	}

    public function fetchLastReconciliation( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CReconciliations::createService()->fetchLastReconciliationByProcessingBankAccountId( $this->getId(), $objPaymentDatabase );
    }

	public function fetchAllUnreconciledReconciliationEntries( $objPaymentDatabase, $strToDate = NULL ) {
		return \Psi\Eos\Payment\CReconciliationEntries::createService()->fetchAllUnreconciledReconciliationEntriesByProcessingBankAccountId( $this->getId(), $objPaymentDatabase, $strToDate );
	}

	public function fetchReconciliations( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CReconciliations::createService()->fetchReconciliationsByProcessingBankAccountId( $this->getId(), $objPaymentDatabase );
	}

	public function fetchReconciliationByReconciliationId( $intReconcliationId, $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CReconciliations::createService()->fetchReconciliationByProcessingBankAccountIdByReconciliationId( $this->getId(), $intReconcliationId, $objPaymentDatabase );
	}

	public function  fetchReconciliationEntriesByIds( $arrintReconciliationEntriesIds, $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CReconciliationEntries::createService()->fetchReconciliationEntriesByProcessingBankAccountIdByIds( $this->getId(), $arrintReconciliationEntriesIds, $objPaymentDatabase );
	}

	public function  fetchReconciliationEntryById( $intReconciliationEntryId, $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CReconciliationEntries::createService()->fetchReconciliationEntryByProcessingBankAccountIdById( $this->getId(), $intReconciliationEntryId, $objPaymentDatabase );
	}

	public function fetchReconciliationEntriesByReconciliationEntryTypeId( $intReconciliationEntryTypeId, $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CReconciliationEntries::createService()->fetchReconciliationEntriesByProcessingBankAccountIdByReconciliationEntryTypeId( $this->getId(), $intReconciliationEntryTypeId, $objPaymentDatabase );
	}

	/**
	 * Validate Functions
	 */

    public function valId() {
        $boolIsValid = true;

        if( true == is_null( $this->getId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Processing bank account id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valProcessingBankId() {
		$boolIsValid = true;

		if( ( false == isset( $this->m_intProcessingBankId ) || 0 == $this->m_intProcessingBankId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_bank_id', 'Processing bank does not exist.' ) );
		}

		return $boolIsValid;
    }

	public function valFundMovementTypeId() {
		$boolIsValid = true;

		if( ( false == isset( $this->m_intFundMovementTypeId ) || 0 == $this->m_intFundMovementTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fund_movement_type_id', 'Fund movement type does not exist.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
        $boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_name', 'Processing bank account name is required.' ) );
        }

        return $boolIsValid;
	}

    public function valDescription() {
        $boolIsValid = true;

        if( true == is_null( $this->getDescription() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_name', 'Processing bank account description is required.' ) );
        }

        return $boolIsValid;
    }

    public function valRoutingNumber( $objPaymentDatabase = NULL ) {
        $boolIsValid = true;

        if( false == isset( $objPaymentDatabase ) ) {
			trigger_error( 'DATABASE IS REQUIRED IN ACH ROUTING NUMBER VALIDATION.', E_USER_WARNING );
        }
        $intRoutingNumber = $this->getRoutingNumber();
        if( true == empty( $intRoutingNumber ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'routing_number', 'Routing number is required.', 605 ) );
        }

        if( 0 !== preg_match( '/[^0-9]/', $this->getRoutingNumber() ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'routing_number', 'Routing number must be a number.', 622 ) );
			return false;
        }

        if( 9 != strlen( $this->getRoutingNumber() ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'routing_number', 'Routing number must be 9 digits long.', 623 ) );
			return false;
        }

		if( false == is_null( $objPaymentDatabase ) ) {
			// If it is 9 numeric characters see if it is a fed ach participant

			$objFedAchParticipant = \Psi\Eos\Payment\CFedAchParticipants::createService()->fetchFedAchParticipantByRoutingNumber( $this->getRoutingNumber(), $objPaymentDatabase );

			if( true == is_null( $objFedAchParticipant ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'routing_number', 'Routing number is not valid.', 624 ) );
			}
		}

        return $boolIsValid;
    }

    public function valAccountNumber() {
        $boolIsValid = true;
        $intAccountNumber = $this->getAccountNumber();
        if( true == empty( $intAccountNumber ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', 'Account number is required.', 602 ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objPaymentDatabase = NULL ) {
        $boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				// fall-through to do other validations
		    case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valProcessingBankId();
            	$boolIsValid &= $this->valFundMovementTypeId();
            	$boolIsValid &= $this->valName();
            	$boolIsValid &= $this->valDescription();
            	$boolIsValid &= $this->valRoutingNumber( $objPaymentDatabase );
            	$boolIsValid &= $this->valAccountNumber();
		        break;

		    case VALIDATE_DELETE:
            	$boolIsValid &= $this->valId();
		        break;

		    default:
               	// default case
              	$boolIsValid = true;
		        break;
		}

        return $boolIsValid;
    }

}
?>