<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMerchantAccountApplicationTypes
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CMerchantAccountApplicationTypes extends CBaseMerchantAccountApplicationTypes {

	public static function fetchMerchantAccountApplicationTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMerchantAccountApplicationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMerchantAccountApplicationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMerchantAccountApplicationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllMerchantAccountApplicationTypes( $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM merchant_account_application_types ORDER BY order_num';

		return self::fetchMerchantAccountApplicationTypes( $strSql, $objPaymentDatabase );
	}

	public static function fetchArrayAllMerchantAccountApplicationTypes( $objPaymentDatabase ) {

		$arrobjApplicationTypes = CMerchantAccountApplicationTypes::fetchAllMerchantAccountApplicationTypes( $objPaymentDatabase );

		// build new array for application types with id as key and name as value.
		$arrstrApplicationTypesNames = array();
		foreach( $arrobjApplicationTypes as $objApplicationTypes ) {
			$arrstrApplicationTypesNames[$objApplicationTypes->getId()] = $objApplicationTypes->getName();
		}

		return $arrstrApplicationTypesNames;
	}

}
?>