<?php

use Psi\Libraries\UtilDates\CDates;
use Psi\Libraries\UtilObjectModifiers\CObjectModifiers;

/**
 * Class CNachaFile
 * @deprecated Use \Psi\Eos\Payment\CNachaFile
 */
class CNachaFile extends CBaseNachaFile {

	const ACH_AR_PAYMENT_COMPANY_ID						= '1861072180';
	const ACH_RETURN_ADDENDA_COMPANY_ID 					= '3861072180';
	const ACH_COMPANY_PAYMENTS_RESIDENT_INSURE_COMPANY_ID 	= '4861072180';
	const ACH_OFFSET_COMPANY_ID								= '7861072180';
	const ACH_PSI_COMPANY_ID								= '9861072180';
	const PATH_NACHA_FILES									= 'nacha_files/';

	protected $m_resNachaFile;

	protected $m_arrobjNachaFileBatches;
	protected $m_arrobjEftBatches;

	protected $m_arrobjNachaEntryDetailRecords;

	protected $m_intTotalFileLines;
	protected $m_intTotalNachaEntryDetailRecords;

	protected $m_objPaymentDatabase;
	protected $m_objProcessingBank;

	protected $m_fltTotalAmount;

	protected $m_strFile;

	public function getFilePath() {
		return PATH_MOUNTS_NACHA_FILES . $this->m_strFilePath;
	}

	/**
	 * Add Functions
	 */

	public function addNachaFileBatch( $objNachaFileBatch ) {
		$this->m_arrobjNachaFileBatches[$objNachaFileBatch->getId()] = $objNachaFileBatch;
	}

	/**
	 * Create Functions
	 */

	public function createNachaFileBatch() {

		$objNachaFileBatch = new CNachaFileBatch();
		$objNachaFileBatch->setDefaults();
		$objNachaFileBatch->setNachaFileId( $this->m_intId );

		// Set identification values from the PBA
		// $objNachaFileBatch->setHeaderCompanyIdentification( $this->m_objProcessingBank->getAchImmOrigRoutingNumber() );
		$objNachaFileBatch->setHeaderCompanyIdentification( self::ACH_OFFSET_COMPANY_ID );
		$objNachaFileBatch->setHeaderOriginatingDfiIdentification( $this->m_objProcessingBank->getAchImmDestRoutingNumber() );
		// $objNachaFileBatch->setControlCompanyIdentification( $this->m_objProcessingBank->getAchImmOrigRoutingNumber() );
		$objNachaFileBatch->setControlCompanyIdentification( self::ACH_OFFSET_COMPANY_ID );
		$objNachaFileBatch->setControlOriginatingDfiIdentification( $this->m_objProcessingBank->getAchImmDestRoutingNumber() );

		$objNachaFileBatch->setHeaderEffectiveEntryDate( $this->determineHeaderEffectiveEntryDate( $this->m_objPaymentDatabase ) );
		// $objNachaFileBatch->setHeaderEffectiveEntryDate( date( 'ymd' ) );

		return $objNachaFileBatch;
	}

	private function createBalancingEftBatch( $intProcessingBankAccountId, $fltSignedBatchAmount ) {

		$objDates = new CDates();
		$arrintStartEndTimes = $objDates->getStartEndTimeByDays( 1 );

		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y', $arrintStartEndTimes['start_time'] ), date( 'm/d/Y', $arrintStartEndTimes['end_time'] ), $this->m_objPaymentDatabase );
		$arrintNonBusinessTimes = [];

		if( true == \Psi\Libraries\UtilFunctions\valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				$arrintNonBusinessTimes[] = strtotime( $objTransactionHoliday->getHolidayDate() );
			}
		}

		$objEftBatch = new CEftBatch();
		$objEftBatch->setDefaults();
		$objEftBatch->setProcessingBankId( $this->m_objProcessingBank->getId() );
		$objEftBatch->setProcessingBankAccountId( $intProcessingBankAccountId );
		$objEftBatch->setMerchantGatewayId( $this->getMerchantGatewayId() );
		$objEftBatch->setEftBatchTypeId( CEftBatchType::UNBALANCED_NACHA_FILE_BANK_BALANCING_ENTRY );
		$objEftBatch->setNachaFileId( $this->m_intId );
		$objEftBatch->setBatchAmount( $fltSignedBatchAmount );
		$objEftBatch->setBatchMemo( sprintf( 'Bank posted entry for unbalanced Nacha File #%d, PBA#%d', $this->m_intId, $intProcessingBankAccountId ) );
		$objEftBatch->setBatchDatetime( date( 'm/d/Y' ) );
		$objEftBatch->setScheduledProcessDatetime( date( 'm/d/Y' ) );
		$objEftBatch->setSettlementDate( $objDates->addBusinessDays( 1, $arrintNonBusinessTimes, date( 'm/d/Y' ) ) );
		$objEftBatch->setProcessedOn( date( 'm/d/Y' ) );

		return $objEftBatch;
	}

	/**
	 * Get Functions
	 */

	public function getNachaFileBatches() {
		return $this->m_arrobjNachaFileBatches;
	}

	public function getNachaEntryDetailRecords() {
		return $this->m_arrobjNachaEntryDetailRecords;
	}

	public function getRebuiltAmount() {

		$strRebuiltAmount = $this->m_strControlTotalCreditEntryAmount;
		while( '0' == $strRebuiltAmount[0] ) {
			$strRebuiltAmount = substr( $strRebuiltAmount, 1 );
		}

		$strRebultAmount = ( $strRebuiltAmount / 100 );

		return $strRebultAmount;
	}

	public function getTotalAmount() {
		return $this->m_fltTotalAmount;
	}

	public function getFile() {
		return $this->m_strFile;
	}

	/**
	 * Set Functions
	 */

	public function setDefaults() {

		if( false == valObj( $this->m_objProcessingBank, 'CProcessingBank' ) ) {
			trigger_error( 'Processing bank not properly loaded.', E_USER_ERROR );
		}

		$this->setMerchantGatewayId( $this->m_objProcessingBank->getAchMerchantGatewayId() );

		$this->m_strNachaFileDatetime = date( 'm/d/Y h:i:s' );

		// Set Default Header Data

		// Size = 1 : Identifies this as the file header.
		$this->setHeaderRecordTypeCode( '1' );

		// Size = 2 : Specifies the priority. (Constant -- always set to 01)
		$this->setHeaderPriorityCode( '01' );

		// Size = 10 : Specifies what bank the funds are going to. (This is the first regional routing number) this might be the ODFI (always 1st regional)
		$this->setHeaderImmediateDestination( $this->m_objProcessingBank->getAchImmDestRoutingNumber() );

		// Size = 10 : This is the ach operator or sending point. (Which is 1st Regional Bank) -- this should show up on the trace number (12224259)
		$this->setHeaderImmediateOrigin( $this->m_objProcessingBank->getAchImmOrigRoutingNumber() );

		// Size = 6	: YYMMDD 070105 is Jan 5 2007
		$this->setHeaderFileCreationDate( date( 'ymd' ) );

		// Size = 4 : HHMM
		$this->setHeaderFileCreationTime( date( 'Hi' ) );

		// Size = 1 : This field contains the number of file sent during the current day to ODFI
		$this->setHeaderFileIdModifier( 'A' );

		// Size = 3 : This field contains the number of characters on a row in this file
		$this->setHeaderRecordSize( '094' );

		// Size = 2 : 10 must always be used -- See page OR 79
		$this->setHeaderBlockingFactor( '10' );

		// Size = 1 : Always should equal "1"... can be used someday to differentiate between different versions of NACHA files
		$this->setHeaderFormatCode( '1' );

		// Size = 23 : This is the name of the bank where funds are headed
		$this->setHeaderImmediateDestinationName( $this->m_objProcessingBank->getAchImmDestName() );

		// Size = 23 : This is the name of the originator
		$this->setHeaderImmediateOriginName( $this->m_objProcessingBank->getAchImmOrigName() );

		// Size = 8 : Not required
		$this->setHeaderReferenceCode( '        ' );

		// Set Default Footer Data

		// Size = 1 : Identifies this record as a file control record
		$this->setControlRecordTypeCode( '9' );

		// Size = 6 	: Must be equal to the number of batch header records in the file
		// $this->setControlBatchCount();

		// Size = 6 	: Contains all of the blocks in the file.  (number of lines including the file header and file control records)
		// $this->setControlBlockCount();

		// Size = 8 	: This count is a tally of each Entry Detail Record and each Addenda Record processed, within either the batch or file as appropriate.
		// $this->setControlEntryAddendaCount();

		// Size = 10 	: Batch control record.  What is the entry hash and do I have to provide it. See OR 83 Same thing as batch control but for entire file.  We just have to add up the routing numbers.  Add up 8 digits (pos 4 - 11).   Only use 8 most digits in routing number.  Pad with zeros to the left.
		// $this->setControlEntryHash();

		// Size = 12 	: Total Debit
		// $this->setControlTotalDebitEntryAmount();

		// Size = 12 	: Total Credit
		// $this->setControlTotalCreditEntryAmount();

		// Size = 39 : Leave Blank
		$this->setControlReserved( '                                       ' );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['total_amount'] ) ) $this->setTotalAmount( $arrmixValues['total_amount'] );

		return;
	}

	public function setDatabase( $objPaymentDatabase ) {
		$this->m_objPaymentDatabase = $objPaymentDatabase;
	}

	public function setHeaderRecordTypeCode( $strHeaderRecordTypeCode ) {
		$this->m_strHeaderRecordTypeCode = CStrings::strNachaDef( $strHeaderRecordTypeCode, 1, true, false );
	}

	public function setHeaderPriorityCode( $strHeaderPriorityCode ) {
		$this->m_strHeaderPriorityCode = CStrings::strNachaDef( $strHeaderPriorityCode, 2, true, false );
	}

	public function setHeaderImmediateDestination( $strHeaderImmediateDestination ) {
		$this->m_strHeaderImmediateDestination = CStrings::strNachaDef( $strHeaderImmediateDestination, 10, true, false );
	}

	public function setHeaderImmediateOrigin( $strHeaderImmediateOrigin ) {
		$this->m_strHeaderImmediateOrigin = CStrings::strNachaDef( $strHeaderImmediateOrigin, 10, true, false );
	}

	public function setHeaderFileCreationDate( $strHeaderFileCreationDate ) {
		$this->m_strHeaderFileCreationDate = CStrings::strNachaDef( $strHeaderFileCreationDate, 6, true, false );
	}

	public function setHeaderFileCreationTime( $strHeaderFileCreationTime ) {
		$this->m_strHeaderFileCreationTime = CStrings::strNachaDef( $strHeaderFileCreationTime, 4, true, false );
	}

	public function setHeaderFileIdModifier( $strHeaderFileIdModifier ) {
		$this->m_strHeaderFileIdModifier = CStrings::strNachaDef( $strHeaderFileIdModifier, 1, true, false );
	}

	public function setHeaderRecordSize( $strHeaderRecordSize ) {
		$this->m_strHeaderRecordSize = CStrings::strNachaDef( $strHeaderRecordSize, 3, true, false );
	}

	public function setHeaderBlockingFactor( $strHeaderBlockingFactor ) {
		$this->m_strHeaderBlockingFactor = CStrings::strNachaDef( $strHeaderBlockingFactor, 2, true, false );
	}

	public function setHeaderFormatCode( $strHeaderFormatCode ) {
		$this->m_strHeaderFormatCode = CStrings::strNachaDef( $strHeaderFormatCode, 1, true, false );
	}

	public function setHeaderImmediateDestinationName( $strHeaderImmediateDestinationName ) {
		$this->m_strHeaderImmediateDestinationName = CStrings::strNachaDef( $strHeaderImmediateDestinationName, 23, false, false );
	}

	public function setHeaderImmediateOriginName( $strHeaderImmediateOriginName ) {
		$this->m_strHeaderImmediateOriginName = CStrings::strNachaDef( $strHeaderImmediateOriginName, 23, false, false );
	}

	public function setHeaderReferenceCode( $strHeaderReferenceCode ) {
		$this->m_strHeaderReferenceCode = CStrings::strNachaDef( $strHeaderReferenceCode, 8, true, false );
	}

	public function setControlRecordTypeCode( $strControlRecordTypeCode ) {
		$this->m_strControlRecordTypeCode = CStrings::strNachaDef( $strControlRecordTypeCode, 1, true, false );
	}

	public function setControlBatchCount( $strControlBatchCount ) {
		$this->m_strControlBatchCount = CStrings::strNachaDef( $strControlBatchCount, 6, true, false, '0' );
	}

	public function setControlBlockCount( $strControlBlockCount ) {
		$this->m_strControlBlockCount = CStrings::strNachaDef( $strControlBlockCount, 6, true, false, '0' );
	}

	public function setControlEntryAddendaCount( $strControlEntryAddendaCount ) {
		$this->m_strControlEntryAddendaCount = CStrings::strNachaDef( $strControlEntryAddendaCount, 8, true, false, '0' );
	}

	public function setControlEntryHash( $strControlEntryHash ) {
		$this->m_strControlEntryHash = CStrings::strNachaDef( $strControlEntryHash, 10, true, true, '0' );
	}

	public function setFormattedControlTotalDebitEntryAmount( $strControlTotalDebitEntryAmount ) {
		// Validate to make sure amount is in $$.cc format
		if( false == stristr( $strControlTotalDebitEntryAmount, '.' ) ) {
			trigger_error( 'Total Debit Entry Amount is not in $$.cc format.', E_USER_ERROR );
			exit;
		}

		$this->m_strControlTotalDebitEntryAmount = CStrings::strNachaDef( $strControlTotalDebitEntryAmount, 12, true, false, '0' );
	}

	public function setFormattedControlTotalCreditEntryAmount( $strControlTotalCreditEntryAmount ) {
		// Validate to make sure amount is in $$.cc format
		if( false == stristr( $strControlTotalCreditEntryAmount, '.' ) ) {
			trigger_error( 'Total Credit Entry Amount is not in $$.cc format.', E_USER_ERROR );
			exit;
		}

		$this->m_strControlTotalCreditEntryAmount = CStrings::strNachaDef( $strControlTotalCreditEntryAmount, 12, true, false, '0' );
	}

	public function setControlReserved( $strControlReserved ) {
		$this->m_strControlReserved = CStrings::strNachaDef( $strControlReserved, 39, true, false );
	}

	public function setTotalAmount( $fltTotalAmount ) {
		$this->m_fltTotalAmount = $fltTotalAmount;
	}

	public function setProcessingBank( $objProcessingBank ) {
		$this->m_objProcessingBank = $objProcessingBank;
		$this->m_intProcessingBankId = $objProcessingBank->getId();
	}

	public function setFile( $strFile ) {
		$this->m_strFile = $strFile;
	}

	/**
	 * Validate Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function loadAssociatedBatchesAndEntryControlDetailRecords( $objPaymentDatabase ) {
		$this->fetchNachaFileBatches( $objPaymentDatabase );
		$this->fetchNachaEntryDetailRecords( $objPaymentDatabase );

		// Nest the nacha entry detail records to the batches
		CObjectModifiers::createService()->nestObjects( $this->m_arrobjNachaEntryDetailRecords, $this->m_arrobjNachaFileBatches );

		return true;
	}

	public function fetchNachaFileBatches( $objPaymentDatabase ) {
		$this->m_arrobjNachaFileBatches = \Psi\Eos\Payment\CNachaFileBatches::createService()->fetchNachaFileBatchesByNachaFileId( $this->m_intId, $objPaymentDatabase );
		return $this->m_arrobjNachaFileBatches;
	}

	public function fetchNachaEntryDetailRecords( $objPaymentDatabase ) {
		$this->m_arrobjNachaEntryDetailRecords = \Psi\Eos\Payment\CNachaEntryDetailRecords::createService()->fetchNachaEntryDetailRecordsByNachaFileId( $this->m_intId, $objPaymentDatabase );
		return $this->m_arrobjNachaEntryDetailRecords;
	}

	public function fetchAndSetHeaderFileIdModifier() {

		// HeaderFileIdModifier needs to be set differently for each processing bank

		switch( $this->getProcessingBankId() ) {

			case CProcessingBank::FIRST_REGIONAL:
				// Retrieve Last Nacha File Id from the database.
				$strLastFileIdModiferSql 	= 'SELECT
													header_file_id_modifier
												FROM
													nacha_files
												WHERE
													date_trunc ( \'day\', nacha_file_datetime ) = \'' . date( 'm/d/Y' ) . '\'
													AND processing_bank_id = ' . ( int ) CProcessingBank::FIRST_REGIONAL . '
												ORDER BY
													header_file_id_modifier DESC';

				$arrintLastFileIdModifierToday = fetchData( $strLastFileIdModiferSql, $this->m_objPaymentDatabase );

				$intLastFileIdModifierToday = $arrintLastFileIdModifierToday[0]['header_file_id_modifier'];

				if( false == is_numeric( $intLastFileIdModifierToday ) ) {
					$intLastFileIdModifierToday = 0;
				}

				$intLastFileIdModifierToday++;

				$this->setHeaderFileIdModifier( $intLastFileIdModifierToday );
				break;

			case CProcessingBank::ZIONS_BANK:
				$this->setHeaderFileIdModifier( 'A' );
				break;

			case CProcessingBank::FIFTH_THIRD:
				// Retrieve file modifiers from the database in most to least recent order
				//  5/3: Requires file modifiers ordered from: A-Z, 1-9, 0
				//       This provides 36 unique modifiers for a given day. After 36, we'll set every file to '0'
				$strLastFileIdModiferSql 	= 'SELECT
													UPPER(header_file_id_modifier) header_file_id_modifier
												FROM
													nacha_files
												WHERE
													id <> ' . ( int ) $this->getId() . '
													AND date_trunc ( \'day\', nacha_file_datetime ) = \'' . date( 'm/d/Y' ) . '\'
													AND processing_bank_id = ' . ( int ) CProcessingBank::FIFTH_THIRD . '
												ORDER BY
													CASE
														WHEN header_file_id_modifier ~ \'^0\' THEN 0
														WHEN header_file_id_modifier ~ \'^[1-9]\' THEN 1
														WHEN header_file_id_modifier ~* \'^[A-Z]\' THEN 2
														ELSE 99
													END,
													UPPER(header_file_id_modifier) DESC';

				$arrstrLastFileIdModifierToday = fetchData( $strLastFileIdModiferSql, $this->m_objPaymentDatabase );

				$strLastFileIdModifierToday = $arrstrLastFileIdModifierToday[0]['header_file_id_modifier'];

				// Increment to to next value
				//   ASCII Values:  A-Z => 65-90, 1-9 => 49-57, 0 => 48
				if( false == valStr( $strLastFileIdModifierToday ) ) {
					$strLastFileIdModifierToday = 'A';
				} elseif( true == preg_match( '/^[A-Y]|[1-8]/i', $strLastFileIdModifierToday ) ) {
					$strLastFileIdModifierToday = chr( ord( $strLastFileIdModifierToday ) + 1 );

				} elseif( true == preg_match( '/^Z/i', $strLastFileIdModifierToday ) ) {
					$strLastFileIdModifierToday = '1';

				} elseif( true == preg_match( '/^9/', $strLastFileIdModifierToday ) ) {
					$strLastFileIdModifierToday = '0';

				} elseif( true == preg_match( '/^0/', $strLastFileIdModifierToday ) ) {
					$strLastFileIdModifierToday = 'A';
				}

				$this->setHeaderFileIdModifier( $strLastFileIdModifierToday );
				break;

			default:
				trigger_error( 'Processing bank is not set while trying to calculate file id modifier', E_USER_ERROR );
		}
	}

	/**
	 * Other Functions
	 */

	public function loadControlEntryHashes() {

		$intFileControlEntryHash = 0;

		if( true == valArr( $this->m_arrobjNachaFileBatches ) ) {
			foreach( $this->m_arrobjNachaFileBatches as $objNachaFileBatch ) {

				$intBatchControlEntryHash = 0;

				if( true == valArr( $objNachaFileBatch->getNachaEntryDetailRecords() ) ) {
					foreach( $objNachaFileBatch->getNachaEntryDetailRecords() as $objNachaEntryDetailRecord ) {
						$intBatchControlEntryHash += $objNachaEntryDetailRecord->getReceivingDfiIdentification();
					}
				}

				// Size = 10 : ? What is the entry hash and do I have to provide it. See OR 83 add DFI routing number for each detail record.  Use right most 10 ten digits.  Add up routing numbers in the rounting numbers.  Add all routing numbers in detail records.   Only use 8 most digits in routing number.  Pad with zeros to the left.
				// I commented the set because I am already setting the batch control entry hash in the batching process in CArPaymentNachaFileController.class.php
				$objNachaFileBatch->setControlEntryHash( $intBatchControlEntryHash );
				$intFileControlEntryHash += $intBatchControlEntryHash;
			}
		}

		// Size = 10 : Batch control record.  What is the entry hash and do I have to provide it. See OR 83 Same thing as batch control but for entire file.  We just have to add up the routing numbers.  Add up 8 digits (pos 4 - 11).   Only use 8 most digits in routing number.  Pad with zeros to the left.
		$this->setControlEntryHash( $intFileControlEntryHash );

		return;
	}

	public function loadFileNameAndFilePath() {

		$arrobjMerchantGateways = \Psi\Eos\Payment\CMerchantGateways::createService()->fetchAllMerchantGateways( $this->m_objPaymentDatabase );
		$arrobjNachaFileTypes 	= \Psi\Eos\Payment\CNachaFileTypes::createService()->fetchAllNachaFileTypes( $this->m_objPaymentDatabase );
		$arrobjProcessingBanks 	= \Psi\Eos\Payment\CProcessingBanks::createService()->fetchAllProcessingBanks( $this->m_objPaymentDatabase );

		$strFileName = NULL;

		// Filename needs to be formatted differently for each processing bank

		switch( $this->getProcessingBankId() ) {
			case CProcessingBank::ZIONS_BANK:
				// Example: ACHPRSL_20100104_2530.txt
				$strFileName = 'ACHPRSL_' . date( 'Ymd' ) . '_' . $this->getId() . '.txt';
				break;

			case CProcessingBank::FIFTH_THIRD:
				// Example Dev/Test: TESTACH53_20111031_0003244.txt 	( Format: TESTACH53_{YYYYMMDD}_{NNNNNNN}.txt, where NNNNNNN is file id left zero padded to 7 digits)
				// Example Prod: 	 ACH53_20111031_0029887.txt 		( Format: ACH53_{YYYYMMDD}_{NNNNNNN}.txt where NNNNNNN is file id left zero padded to 7 digits)
				$strFileName = '';

				if( CONFIG_ENVIRONMENT != 'production' ) {
					$strFileName .= 'TEST';
				}

				$strFileName .= sprintf( 'ACH53_%s_%07s.txt', date( 'Ymd' ), $this->getId() );
				break;

			case CProcessingBank::FIRST_REGIONAL:
				// Example: 20100104_PSI_FirstRegional_ARPayments_Daily_Nacha_File_No_4_2530.txt
				$strFileName = date( 'Ymd' ) . '_PSI_' . preg_replace( '/[^a-zA-Z0-9]/', '', trim( $arrobjProcessingBanks[$this->m_intProcessingBankId]->getName() ) ) . '_' . preg_replace( '/[^a-zA-Z0-9]/', '', trim( $arrobjNachaFileTypes[$this->m_intNachaFileTypeId]->getName() ) ) . '_Daily_Nacha_File_No_' . $this->m_strHeaderFileIdModifier . '_' . $this->getId() . '.txt';
				break;

			default:
				// default case
				break;
		}

		if( true == is_null( $strFileName ) ) {
			trigger_error( 'File name could not be set.', E_USER_ERROR );
			exit;
		}

		$this->setFileName( $strFileName );
		$this->setFilePath( 'Outgoing/' . preg_replace( '/[^a-zA-Z0-9]/', '', trim( $arrobjMerchantGateways[$this->m_intMerchantGatewayId]->getName() ) ) . '/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . preg_replace( '/[^a-zA-Z0-9]/', '', trim( $arrobjNachaFileTypes[$this->m_intNachaFileTypeId]->getName() ) ) . '/' );
	}

	public function loadControlBlockCount() {

		// Size = 6 : Contains all of the blocks in the file.  (number of lines including the file header and file control records)
		$intTotalBatchesCount 				= ( \Psi\Libraries\UtilFunctions\count( $this->getNachaFileBatches() ) * 2 ); // batch headers and control records
		$intTotalEntryDetailRecordsCount 	= 0;

		if( true == valArr( $this->getNachaFileBatches() ) ) {
			foreach( $this->getNachaFileBatches() as $objNachaFileBatch ) {
				$intTotalEntryDetailRecordsCount += \Psi\Libraries\UtilFunctions\count( $objNachaFileBatch->getNachaEntryDetailRecords() );
			}
		}

		$intTotalRecords = $intTotalBatchesCount + $intTotalEntryDetailRecordsCount + 2; // 2 lines for file control record and file header record

		$intControlBlockCount = ceil( ( $intTotalRecords / 10 ) );

		$this->m_intTotalFileLines 					= $intTotalRecords;
		$this->m_intTotalNachaEntryDetailRecords 	= $intTotalEntryDetailRecordsCount;

		$this->setControlBlockCount( $intControlBlockCount );

		return;
	}

	public function convertNachaFileToUnbalanced() {
		if( true == valArr( $this->m_arrobjNachaFileBatches ) ) {

			$arrintIntermediaryNachaFileBatchKeys = array();
			$this->m_arrobjEftBatches = array();

			foreach( $this->m_arrobjNachaFileBatches as $intBatchKey => $objNachaFileBatch ) {
				if( true == valArr( $objNachaFileBatch->getNachaEntryDetailRecords() ) ) {
					foreach( $objNachaFileBatch->getNachaEntryDetailRecords() as $objNachaEntryDetailRecord ) {

						// Build an EFT Batch that will act as a replacement for the intermediary NEDR
						if( true == $objNachaEntryDetailRecord->getIsIntermediary() ) {

							$arrintIntermediaryNachaFileBatchKeys[$intBatchKey] = $intBatchKey;

							// Get amount and make it signed
							$fltAmount = $objNachaEntryDetailRecord->getFormattedAmount();
							$fltSignedAmount = ( CEntryDetailRecordType::DEBIT == $objNachaEntryDetailRecord->getEntryDetailRecordTypeId() ? ( -1 * $fltAmount ) : $fltAmount );

							// Create an EFT batch
							$objEftBatch = $this->createBalancingEftBatch( $objNachaEntryDetailRecord->getProcessingBankAccountId(), $fltSignedAmount );
							$this->m_arrobjEftBatches[] = $objEftBatch;
						}
					}
				}
			}

			$arrobjUnsetBatches = array();

			// Unset the intermediary batches from the member array, so they won't  be included in the file
			foreach( $arrintIntermediaryNachaFileBatchKeys as $intBatchKey ) {
				$arrobjUnsetBatches[$intBatchKey] = $this->m_arrobjNachaFileBatches[$intBatchKey];
				unset( $this->m_arrobjNachaFileBatches[$intBatchKey] );
			}

			// Insert EFT batches for each of the intermediary NEDR that were removed.
			foreach( $this->m_arrobjEftBatches as $objEftBatch ) {
				if( false == $objEftBatch->insert( SYSTEM_USER_ID, $this->m_objPaymentDatabase ) ) {
					trigger_error( 'EFT Batch failed to insert!', E_USER_ERROR );
					exit;
				}
			}
		}
	}

	// This function loads all of the data that is consistent across every nacha file ever generated

	public function loadCoreFileData() {

		// *************************************************************************************************
		// If PB is configured for unbalanced files, we'll need to modify the batches/entries
		// *************************************************************************************************

		if( false == $this->m_objProcessingBank->getIsBalancedAchFiles() ) {
			$this->convertNachaFileToUnbalanced();
		}

		// This number reflects the sequence of ach files to sent to the specific merchant gateway for a given day.
		$this->fetchAndSetHeaderFileIdModifier();

		$intTotalFileEntryDetailRecords 	= 0;
		$fltTotalFileCreditAmount 			= 0;
		$fltTotalFileDebitAmount 			= 0;

		if( true == valArr( $this->m_arrobjNachaFileBatches ) ) {
			foreach( $this->m_arrobjNachaFileBatches as $objNachaFileBatch ) {

				// Size = 7 : This should be an ascending number (payment_batch id ).  This has to be the same number on the batch control record.
				// I think this may not work.
				// This will fail when we get to 10 million batches.  This should take a very long time.
				$objNachaFileBatch->setHeaderBatchNumber( $objNachaFileBatch->getId() );
				// Size = 7 : This should be an ascending number (payment_batch id ) and should match the opening batch control line
				$objNachaFileBatch->setControlBatchNumber( $objNachaFileBatch->getId() );

				//************** Insert Nacha Batch *************//

				if( false == $objNachaFileBatch->insert( SYSTEM_USER_ID, $this->m_objPaymentDatabase ) ) {
					$this->m_objPaymentDatabase->rollback();
					trigger_error( 'Nacha file batch failed to insert.', E_USER_ERROR );
					exit;
				}

				$intTotalBatchEntryDetailRecords 	= 0;
				$fltTotalBatchDebitAmount 			= 0;
				$fltTotalBatchCreditAmount 			= 0;

				if( true == valArr( $objNachaFileBatch->getNachaEntryDetailRecords() ) ) {
					foreach( $objNachaFileBatch->getNachaEntryDetailRecords() as $objNachaEntryDetailRecord ) {

						// Size = 1 : This is a funky calculated Modulus 10 number based on the routing number.  Really Weird.  It is used by Thompson's publishing to determine the receivers' bank routing number.
						$objNachaEntryDetailRecord->buildCheckDigit();

						if( CEntryDetailRecordType::CREDIT == $objNachaEntryDetailRecord->getEntryDetailRecordTypeId() ) {
							$fltTotalBatchCreditAmount += ( float ) $objNachaEntryDetailRecord->getAmount();
						} else {
							$fltTotalBatchDebitAmount += ( float ) $objNachaEntryDetailRecord->getAmount();
						}

						// Size = 15 : This should be the check number for BOC headers and ID of the payment (cust_pa_322323), etc. for WEB
						// if it's a BOC header, the check number was already set in ArPaymentNachaFileController.class.php
						if( 'BOC' != $objNachaFileBatch->getHeaderStandardEntryClassCode() || true == is_null( $objNachaFileBatch->getHeaderStandardEntryClassCode() ) ) {
							$objNachaEntryDetailRecord->setIdentificationNumber( $objNachaEntryDetailRecord->getId() );
						}

						// Size = 15 : Trace number is prefix (8 digits) + position (7 digits). (TRACE NUMBER prefix is typically ACH immediate destination *OR* ACH immediate origin defined for processing bank)
						switch( $this->getProcessingBankId() ) {
							case CProcessingBank::FIFTH_THIRD:
								$objNachaEntryDetailRecord->buildTraceNumber( $this->m_strHeaderImmediateDestination );
								break;

							case CProcessingBank::ZIONS_BANK:
							default:
								$objNachaEntryDetailRecord->buildTraceNumber( $this->m_strHeaderImmediateOrigin );
								break;
						}

						// Insert entry detail record
						if( false == $objNachaEntryDetailRecord->insert( SYSTEM_USER_ID, $this->m_objPaymentDatabase ) ) {
							$this->m_objPaymentDatabase->rollback();
							trigger_error( 'Nacha entry detail record failed to insert.', E_USER_ERROR );
							exit;
						}

						$intTotalBatchEntryDetailRecords++;

						if( true == valArr( $objNachaEntryDetailRecord->getNachaDetailRecordContents() ) ) {
							foreach( $objNachaEntryDetailRecord->getNachaDetailRecordContents() as $objNachaDetailRecordContent ) {
								$objNachaDetailRecordContent->setNachaFileId( $this->getId() );
								$objNachaDetailRecordContent->setNachaEntryDetailRecordId( $objNachaEntryDetailRecord->getId() );

								if( false == $objNachaDetailRecordContent->insert( SYSTEM_USER_ID, $this->m_objPaymentDatabase ) ) {
									$this->m_objPaymentDatabase->rollback();
									trigger_error( 'Nacha entry detail record failed to insert.', E_USER_ERROR );
									exit;
								}
							}
						}
					}
				}

				//************** Update Batch Totals *************//

				// Size = 6 : This looks like the numbers of records in the batch.  Technical definition: This count is a tally of each Entry Detail Record and each Addenda Record processed, within either the batch or file as appropriate.
				$objNachaFileBatch->setControlEntryAddendaCount( $intTotalBatchEntryDetailRecords );

				// Size = 12 : Debit Amount
				$objNachaFileBatch->setFormattedControlTotalDebitAmount( number_format( ( $fltTotalBatchDebitAmount / 100 ), 2 ) );

				// Size = 12 : Credit Amount
				$objNachaFileBatch->setFormattedControlTotalCreditAmount( number_format( ( $fltTotalBatchCreditAmount / 100 ), 2 ) );

				$fltTotalFileDebitAmount += $fltTotalBatchDebitAmount;
				$fltTotalFileCreditAmount += $fltTotalBatchCreditAmount;

				//************** Insert Batch *************//

				if( false == $objNachaFileBatch->update( SYSTEM_USER_ID, $this->m_objPaymentDatabase ) ) {
					$this->m_objPaymentDatabase->rollback();
					trigger_error( 'Nacha file batch failed to update.', E_USER_ERROR );
					exit;
				}

				$intTotalFileEntryDetailRecords += $intTotalBatchEntryDetailRecords;
			}
		}

		//************** Set Custom File Fields *************//

		// Size = 6 : Must be equal to the number of batch header records in the file
		$this->setControlBatchCount( \Psi\Libraries\UtilFunctions\count( $this->getNachaFileBatches() ) );

		// Size = 8 : This count is a tally of each Entry Detail Record and each Addenda Record processed, within either the batch or file as appropriate.
		$this->setControlEntryAddendaCount( $intTotalFileEntryDetailRecords );

		// Size = 12 : Total Debit
		$this->setFormattedControlTotalDebitEntryAmount( number_format( ( $fltTotalFileDebitAmount / 100 ), 2 ) );

		// Size = 12 : Total Credit
		$this->setFormattedControlTotalCreditEntryAmount( number_format( ( $fltTotalFileCreditAmount / 100 ), 2 ) );

		$this->loadControlEntryHashes();

		$this->loadFileNameAndFilePath();

		$this->loadControlBlockCount();

		return true;
	}

	public function rebuildAndSaveFileFromDatabaseData( $objPaymentDatabase ) {

		$this->m_objPaymentDatabase = $objPaymentDatabase;

		// Load rows related to the file
		$objProcessingBank = \Psi\Eos\Payment\CProcessingBanks::createService()->fetchProcessingBankById( $this->getProcessingBankId(), $objPaymentDatabase );
		$arrobjNachaFileBatches = $this->fetchNachaFileBatches( $objPaymentDatabase );
		$arrobjNachaEntryDetailRecords = $this->fetchNachaEntryDetailRecords( $objPaymentDatabase );

		// Set processing bank
		$this->setProcessingBank( $objProcessingBank );

		// Sort records by batch
		$arrobjNachaDetailRecordsByBatchId = array();
		foreach( $arrobjNachaEntryDetailRecords as $objNachaEntryDetailRecord ) {
			$arrobjBatchEntries = $arrobjNachaDetailRecordsByBatchId[$objNachaEntryDetailRecord->getNachaFileBatchId()];

			if( NULL == $arrobjBatchEntries ) {
				$arrobjBatchEntries = array();
				$arrobjNachaDetailRecordsByBatchId[$objNachaEntryDetailRecord->getNachaFileBatchId()] = $arrobjBatchEntries;
			}
			array_push( $arrobjNachaDetailRecordsByBatchId[$objNachaEntryDetailRecord->getNachaFileBatchId()], $objNachaEntryDetailRecord );
		}

		// Reformat data (the data stored in the db is not formatted the way it is meant to be in the file, the set methods can reformat the data, so passing current data back into the set for reformatting)
		// and assign records to their batches

		$intI = 0;
		$intCount = \Psi\Libraries\UtilFunctions\count( $arrobjNachaFileBatches );

		foreach( $arrobjNachaFileBatches as $objNachaFileBatch ) {
			$objNachaFileBatch->setControlBatchNumber( $objNachaFileBatch->getControlBatchNumber() );
			$objNachaFileBatch->setControlCompanyIdentification( $objNachaFileBatch->getControlCompanyIdentification() );
			$objNachaFileBatch->setControlEntryAddendaCount( $objNachaFileBatch->getControlEntryAddendaCount() );
			$objNachaFileBatch->setControlEntryHash( $objNachaFileBatch->getControlEntryHash() );
			$objNachaFileBatch->setControlMessageAuthenticationCode( $objNachaFileBatch->getControlMessageAuthenticationCode() );
			$objNachaFileBatch->setControlOriginatingDfiIdentification( $objNachaFileBatch->getControlOriginatingDfiIdentification() );
			$objNachaFileBatch->setControlRecordTypeCode( $objNachaFileBatch->getControlRecordTypeCode() );
			$objNachaFileBatch->setControlReserved( $objNachaFileBatch->getControlReserved() );
			$objNachaFileBatch->setControlServiceClassCode( $objNachaFileBatch->getControlServiceClassCode() );
			$objNachaFileBatch->setControlTotalCreditAmount( $objNachaFileBatch->getControlTotalCreditAmount() );
			$objNachaFileBatch->setControlTotalDebitAmount( $objNachaFileBatch->getControlTotalDebitAmount() );
			$objNachaFileBatch->setHeaderBatchNumber( $objNachaFileBatch->getHeaderBatchNumber() );
			$objNachaFileBatch->setHeaderCompanyIdentification( $objNachaFileBatch->getHeaderCompanyIdentification() );
			$objNachaFileBatch->setHeaderOriginatingDfiIdentification( $objNachaFileBatch->getHeaderOriginatingDfiIdentification() );
			$objNachaFileBatch->setHeaderRecordTypeCode( $objNachaFileBatch->getHeaderRecordTypeCode() );
			$objNachaFileBatch->setHeaderServiceClassCode( $objNachaFileBatch->getHeaderServiceClassCode() );
			$objNachaFileBatch->setHeaderBatchNumber( $objNachaFileBatch->getHeaderBatchNumber() );

			foreach( $arrobjNachaDetailRecordsByBatchId[$objNachaFileBatch->getId()] as $objNachaEntryDetailRecord ) {
				$objNachaEntryDetailRecord->setAddendaRecordIndicator( $objNachaEntryDetailRecord->getAddendaRecordIndicator() );
				$objNachaEntryDetailRecord->setAmount( $objNachaEntryDetailRecord->getAmount() );
				$objNachaEntryDetailRecord->setCheckAccountNumberEncrypted( $objNachaEntryDetailRecord->getCheckAccountNumberEncrypted() );
				$objNachaEntryDetailRecord->setCheckDigit( $objNachaEntryDetailRecord->getCheckDigit() );
				$objNachaEntryDetailRecord->setCheckRoutingNumber( $objNachaEntryDetailRecord->getCheckRoutingNumber() );
				// THIS IS A BIG DEAL HERE
				$objNachaEntryDetailRecord->setDfiAccountNumber( $objNachaEntryDetailRecord->getCheckAccountNumber() );
				$objNachaEntryDetailRecord->setDiscretionaryData( $objNachaEntryDetailRecord->getDiscretionaryData() );
				$objNachaEntryDetailRecord->setFormattedAmount( $objNachaEntryDetailRecord->getFormattedAmount() );
				$objNachaEntryDetailRecord->setIdentificationNumber( $objNachaEntryDetailRecord->getIdentificationNumber() );
				$objNachaEntryDetailRecord->setOffsettingAccountNumber( $objNachaEntryDetailRecord->getOffsettingAccountNumber() );
				$objNachaEntryDetailRecord->setOffsettingAccountNumberEncrypted( $objNachaEntryDetailRecord->getOffsettingAccountNumberEncrypted() );
				$objNachaEntryDetailRecord->setOffsettingRoutingNumber( $objNachaEntryDetailRecord->getOffsettingRoutingNumber() );
				$objNachaEntryDetailRecord->setOriginalEntryTraceNumber( $objNachaEntryDetailRecord->getOriginalEntryTraceNumber() );
				$objNachaEntryDetailRecord->setReceivingCompanyName( $objNachaEntryDetailRecord->getReceivingCompanyName() );
				$objNachaEntryDetailRecord->setReceivingDfiIdentification( $objNachaEntryDetailRecord->getReceivingDfiIdentification() );
				$objNachaEntryDetailRecord->setRecordTransactionCode( $objNachaEntryDetailRecord->getRecordTransactionCode() );
				$objNachaEntryDetailRecord->setRecordTypeCode( $objNachaEntryDetailRecord->getRecordTypeCode() );
				$objNachaEntryDetailRecord->setTraceNumber( $objNachaEntryDetailRecord->getTraceNumber() );
				$objNachaFileBatch->addNachaEntryDetailRecord( $objNachaEntryDetailRecord );
			}

			$intI++;
		}

		return $this->writeFileToDisk();
	}

	public function buildAndSaveFile() {

		$this->loadCoreFileData();

		return $this->writeFileToDisk();
	}

	public function writeFileToDisk() {

		$intNumberOfBufferLines = ( 10 - ( $this->m_intTotalFileLines % 10 ) );

		$boolIsSuccessful = true;

		CFileIo::recursiveMakeDir( $this->getFilePath() );

		// Truncate file to zero length
		$resHandle = CFileIo::fileOpen( $this->getFilePath() . $this->getFileName(), 'w' );

		$strFile = '';

		// Add file header
		$strFileHeader = '';
		$strFileHeader .= $this->m_strHeaderRecordTypeCode;
		$strFileHeader .= $this->m_strHeaderPriorityCode;
		$strFileHeader .= $this->m_strHeaderImmediateDestination;
		$strFileHeader .= $this->m_strHeaderImmediateOrigin;
		$strFileHeader .= $this->m_strHeaderFileCreationDate;
		$strFileHeader .= $this->m_strHeaderFileCreationTime;
		$strFileHeader .= $this->m_strHeaderFileIdModifier;
		$strFileHeader .= $this->m_strHeaderRecordSize;
		$strFileHeader .= $this->m_strHeaderBlockingFactor;
		$strFileHeader .= $this->m_strHeaderFormatCode;
		$strFileHeader .= $this->m_strHeaderImmediateDestinationName;
		$strFileHeader .= $this->m_strHeaderImmediateOriginName;
		$strFileHeader .= $this->m_strHeaderReferenceCode;

		$strFile .= $strFileHeader . chr( 13 ) . chr( 10 );

		// Loop on batches
		if( true == valArr( $this->m_arrobjNachaFileBatches ) ) {
			foreach( $this->m_arrobjNachaFileBatches as $objNachaFileBatch ) {
				$strBatchHeader = '';
				$strBatchHeader .= $objNachaFileBatch->getHeaderRecordTypeCode();
				$strBatchHeader .= $objNachaFileBatch->getHeaderServiceClassCode();
				$strBatchHeader .= $objNachaFileBatch->getHeaderCompanyName();
				$strBatchHeader .= $objNachaFileBatch->getHeaderCompanyDiscretionaryData();
				$strBatchHeader .= $objNachaFileBatch->getHeaderCompanyIdentification();
				$strBatchHeader .= $objNachaFileBatch->getHeaderStandardEntryClassCode();
				$strBatchHeader .= $objNachaFileBatch->getHeaderCompanyEntryDescription();
				$strBatchHeader .= $objNachaFileBatch->getHeaderCompanyDescriptiveDate();
				$strBatchHeader .= $objNachaFileBatch->getHeaderEffectiveEntryDate();
				$strBatchHeader .= $objNachaFileBatch->getHeaderSettlementDate();
				$strBatchHeader .= $objNachaFileBatch->getHeaderOriginatorStatusCode();
				$strBatchHeader .= $objNachaFileBatch->getHeaderOriginatingDfiIdentification();
				$strBatchHeader .= $objNachaFileBatch->getHeaderBatchNumber();

				$strFile .= $strBatchHeader . chr( 13 ) . chr( 10 );

				if( true == valArr( $objNachaFileBatch->getNachaEntryDetailRecords() ) ) {
					foreach( $objNachaFileBatch->getNachaEntryDetailRecords() as $objNachaEntryDetailRecord ) {

						$strDetailRecord = '';
						$strDetailRecord .= $objNachaEntryDetailRecord->getRecordTypeCode();
						$strDetailRecord .= $objNachaEntryDetailRecord->getRecordTransactionCode();
						$strDetailRecord .= $objNachaEntryDetailRecord->getReceivingDfiIdentification();
						$strDetailRecord .= $objNachaEntryDetailRecord->getCheckDigit();
						$strDetailRecord .= $objNachaEntryDetailRecord->getDfiAccountNumber();
						$strDetailRecord .= $objNachaEntryDetailRecord->getAmount();
						$strDetailRecord .= $objNachaEntryDetailRecord->getIdentificationNumber();
						$strDetailRecord .= $objNachaEntryDetailRecord->getReceivingCompanyName();
						$strDetailRecord .= $objNachaEntryDetailRecord->getDiscretionaryData();
						$strDetailRecord .= $objNachaEntryDetailRecord->getAddendaRecordIndicator();
						$strDetailRecord .= $objNachaEntryDetailRecord->getTraceNumber();

						$strFile .= $strDetailRecord . chr( 13 ) . chr( 10 );
					}
				}

				$strBatchControl = '';
				$strBatchControl .= $objNachaFileBatch->getControlRecordTypeCode();
				$strBatchControl .= $objNachaFileBatch->getControlServiceClassCode();
				$strBatchControl .= $objNachaFileBatch->getControlEntryAddendaCount();
				$strBatchControl .= $objNachaFileBatch->getControlEntryHash();
				$strBatchControl .= $objNachaFileBatch->getControlTotalDebitAmount();
				$strBatchControl .= $objNachaFileBatch->getControlTotalCreditAmount();
				$strBatchControl .= $objNachaFileBatch->getControlCompanyIdentification();
				$strBatchControl .= $objNachaFileBatch->getControlMessageAuthenticationCode();
				$strBatchControl .= $objNachaFileBatch->getControlReserved();
				$strBatchControl .= $objNachaFileBatch->getControlOriginatingDfiIdentification();
				$strBatchControl .= $objNachaFileBatch->getControlBatchNumber();

				$strFile .= $strBatchControl . chr( 13 ) . chr( 10 );
			}
		}

		$strFileControl = '';
		$strFileControl .= $this->m_strControlRecordTypeCode;
		$strFileControl .= $this->m_strControlBatchCount;
		$strFileControl .= $this->m_strControlBlockCount;
		$strFileControl .= $this->m_strControlEntryAddendaCount;
		$strFileControl .= $this->m_strControlEntryHash;
		$strFileControl .= $this->m_strControlTotalDebitEntryAmount;
		$strFileControl .= $this->m_strControlTotalCreditEntryAmount;
		$strFileControl .= $this->m_strControlReserved;

		if( 0 != $intNumberOfBufferLines ) {
			$strFileControl .= chr( 13 ) . chr( 10 );
		}

		$strFile .= $strFileControl;

		// Now we need to 9 fill the rest of the records up to 10 * the file control block count.
		for( $intX = 0; $intX < $intNumberOfBufferLines; $intX ++ ) {
			$strFile .= str_pad( '', 94, '9' ) . chr( 13 ) . chr( 10 );
		}

		/**
		 * Until we change the ZionsFileTransfer script in PsPayments to read from objectstore,
		 * we will need to keep this here as the existing file transfer script upload handler still pulls from mounts
		 */
		if( false === fwrite( $resHandle, $strFile ) ) {
			trigger_error( 'File could not be saved to file system.', E_USER_ERROR );
			exit;
		}

		$objPutNachaFile = \Psi\Core\Payment\PaymentFiles\CPutNachaFile::create();

		/**
		 * This will encrypt the content and store the object in the payments system objectstore
		 */
		try {
			$objPutNachaFile->setNachaContent( $strFile )
				->setObjectStorageKey( self::PATH_NACHA_FILES . $this->m_strFilePath . $this->getFileName() );

			$objPutNachaFile->paymentSystemPutObject( $this, CPaymentFileType::NACHA_FILES );
		} catch( \Throwable $objThrowable ) {
			trigger_error( 'Nacha file could not be saved to the object store. Nacha File ID: ' . $this->getId(), E_USER_NOTICE );
		}

		// FifthThird bank does ACH Authorization by creating and uploading an AUTH file. So here is that file created.
		if( $this->getProcessingBankId() == CProcessingBank::FIFTH_THIRD ) {
			$strAuthFile = sprintf( 'A%d %012d %012d %d%s', $this->m_strHeaderImmediateOrigin, $this->m_strControlTotalDebitEntryAmount, $this->m_strControlTotalCreditEntryAmount, '8778269700', str_pad( '', 46 ) );

			$resHandle = CFileIo::fileOpen( PATH_MOUNTS_NACHA_FILES . 'Outgoing/FifthThirdACH/Authorizations/' . $this->getFileName(), 'w' );

			/**
			 * Continue to write the authorization file for FifthThird until we get the transfer script in PsPayments
			 * migrated over to the new data handler script
			 */
			if( false === fwrite( $resHandle, $strAuthFile ) ) {
				trigger_error( 'Nacha authentication failed to save to file system.', E_USER_ERROR );
				exit;
			}

			/**
			 * This will encrypt the content and store the object in the payments system objectstore
			 */
			try {
				$objPutNachaFile->setNachaContent( $strAuthFile )
					->setObjectStorageKey( self::PATH_NACHA_FILES . 'Outgoing/FifthThirdACH/Authorizations/' . $this->getFileName() );

				$objPutNachaFile->paymentSystemPutObject( $this, CPaymentFileType::NACHA_AUTHORIZATION_FILES );
			} catch( \Throwable $objThrowable ) {
				trigger_error( 'Nacha Authorization File could not be saved to the object store. Nacha Auth File ID: ' . $this->getId(), E_USER_NOTICE );
			}
		}

		if( false == $this->update( SYSTEM_USER_ID, $this->m_objPaymentDatabase ) ) {
			$this->m_objPaymentDatabase->rollback();
			trigger_error( 'Customer payment nacha file failed to update.', E_USER_ERROR );
			exit;
		}

		$this->m_strFile = $strFile;

		return $boolIsSuccessful;
	}

	public function verifyFile() {

		// **********************************************************************************************************************
		// This function parses the actual file that has been generated.  It asserts that all of the actual totals/counts agree
		// with the actual totals/counts. It supports both unbalanced and balanced Nacha files.
		//   NOTE: We are using bccomp( val1, val2 ), with no "scale" specified in the 3rd parameter.
		//         The default scale is 0 (ignore anything after the decimal) for bccomp and this is correct. Since all values
		//         in a Nacha formatted file are either 10 or 12 digit zero padded whole numbers.  For example $208.20 will
		//         look like 0000020820 (entry detail record ) or 000000020820 (control records). We cast these strings to a float
		//         using floatval() and perform whole number comparisons.  Its important that we use bccomp() as floats
		//         have inexact precision.  Its also important because its quite easy to overrun a 32 bit integer. Since we're
		//         handling the values as whole numbers, anything greater than $21,474,836.47 will overrun the max value
		//         of a 32 bit integer 2,147,483,647!
		// **********************************************************************************************************************

		$boolIsVerified = true;

		$this->addOutput( 'Verifying Nacha File...' );
		$this->addOutput( '  PB: {' . $this->m_objProcessingBank->getName() . '}' );
		$this->addOutput( '  PB Requires balanced Nacha: ' . ( false == $this->m_objProcessingBank->getIsBalancedAchFiles() ? 'NO' : 'YES' ) );

		// Don't output this in production as the nacha file has all of the unencrypted account numbers in it
		if( CONFIG_ENVIRONMENT != 'production' ) {
			$this->addOutput( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%' );
			$this->addOutput( $this->m_strFile );
			$this->addOutput( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%' );
		}

		$fltCreditFileTotal = 0;
		$fltDebitFileTotal = 0;
		$intBatchesFileCount = 0;

		$fltCreditBatchTotal = 0;
		$fltDebitBatchTotal = 0;
		$intAddendaBatchCount = 0;

		$arrstrLines = explode( "\r\n", $this->m_strFile );
		$intNumRecords = \Psi\Libraries\UtilFunctions\count( $arrstrLines ) - 1;

		// Verify file lines is a multiple of 10
		if( 0 != $intNumRecords % 10 ) {
			$this->addOutput( 'File lines ' . ( int ) $intNumRecords . ' not a multiple of 10', ERROR_TYPE_VALIDATION );
			$boolIsVerified = false;
		}

		// Process each line of file
		for( $intI = 0; $intI < $intNumRecords; $intI++ ) {

			// Verify record length is 96 (94 + CR + LF)
			if( 94 != strlen( $arrstrLines[$intI] ) ) {
				$this->addOutput( 'Record ' . ( int ) $intI . ' has invalid length: ' . strlen( $arrstrLines[$intI] . "\n" . $arrstrLines[$intI] ), ERROR_TYPE_VALIDATION );
				$boolIsVerified = false;
			}

			switch( substr( $arrstrLines[$intI], 0, 1 ) ) {
				// File Header
				case '1':
					$strRecordSize = substr( $arrstrLines[$intI], 34, 3 );
					$strBlockingFactor = substr( $arrstrLines[$intI], 37, 2 );

					// Verify we have a 94 character record size specified
					if( '094' != $strRecordSize ) {
						$this->addOutput( 'Record ' . ( int ) $intI . ' has invalid File Header Record Size: $strRecordSize' . "\n" . $arrstrLines[$intI], ERROR_TYPE_VALIDATION );
						$boolIsVerified = false;
					}

					// Verify we have a 10 record blocking factor (number of records will be a multiple of 10)
					if( '10' != $strBlockingFactor ) {
						$this->addOutput( 'Record ' . ( int ) $intI . ' has invalid File Header Blocking Factor: $strBlockingFactor' . "\n" . $arrstrLines[$intI], ERROR_TYPE_VALIDATION );
						$boolIsVerified = false;
					}
					break;

					// Batch Header
				case '5':
					$fltCreditBatchTotal = 0;
					$fltDebitBatchTotal = 0;
					$intAddendaBatchCount = 0;
					break;

					// Entry Detail Record
				case '6':
					$strTransactionCode = substr( $arrstrLines[$intI], 1, 2 );
					$strAmount = substr( $arrstrLines[$intI], 29, 10 );

					$fltAmount = floatval( $strAmount );

					// Verify amount is > 0
					if( 0 <= bccomp( 0, $fltAmount ) ) {
						$this->addOutput( 'Record ' . ( int ) $intI . ' has invalid Detail Record Amount: $fltAmount' . "\n" . $arrstrLines[$intI], ERROR_TYPE_VALIDATION );
						$boolIsVerified = false;
					}

					// Tally batch/file totals.  Verify Transaction code
					if( '22' == $strTransactionCode || '32' == $strTransactionCode ) {
						$fltCreditFileTotal += $fltAmount;
						$fltCreditBatchTotal += $fltAmount;
					} elseif( '27' == $strTransactionCode || '37' == $strTransactionCode ) {
						$fltDebitFileTotal += $fltAmount;
						$fltDebitBatchTotal += $fltAmount;
					} else {
						$this->addOutput( 'Record ' . ( int ) $intI . ' has invalid Detail Record Txn Code: ' . $strTransactionCode . "\n" . $arrstrLines[$intI], ERROR_TYPE_VALIDATION );
						$boolIsVerified = false;
					}

					$intAddendaBatchCount++;
					break;

					// Batch Control Record
				case '8':
					$strAddendaCount = substr( $arrstrLines[$intI], 4, 6 );
					$strDebitAmount = substr( $arrstrLines[$intI], 20, 12 );
					$strCreditAmount = substr( $arrstrLines[$intI], 32, 12 );

					$intAddendaCount = intval( $strAddendaCount );
					$fltDebitAmount = floatval( $strDebitAmount );
					$fltCreditAmount = floatval( $strCreditAmount );

					// Verify either debit or credit is > zero
					if( 0 <= bccomp( 0, $fltDebitAmount ) && 0 <= bccomp( 0, $fltCreditAmount ) ) {
						$this->addOutput( 'Record ' . ( int ) $intI . ' has invalid Batch Control - Debit/Credit both zero' . "\n" . $arrstrLines[$intI], ERROR_TYPE_VALIDATION );
						$boolIsVerified = false;
					}

					// Verify specified debit for batch matches the amount tallied from detail records
					if( 0 != bccomp( $fltDebitAmount, $fltDebitBatchTotal ) ) {
						$this->addOutput( 'Record ' . ( int ) $intI . ' has Batch Control Debit ' . $fltDebitAmount . ' unmatched with total record Debits ' . $fltDebitBatchTotal . "\n" . $arrstrLines[$intI], ERROR_TYPE_VALIDATION );
						$boolIsVerified = false;
					}

					// Verify specified credit for batch matches the amount tallied from detail records
					if( 0 != bccomp( $fltCreditAmount, $fltCreditBatchTotal ) ) {
						$this->addOutput( 'Record ' . ( int ) $intI . ' has Batch Control Credit ' . $fltCreditAmount . ' unmatched with total record Credits ' . $fltCreditBatchTotal . "\n" . $arrstrLines[$intI], ERROR_TYPE_VALIDATION );
						$boolIsVerified = false;
					}

					// Verify specified record count matches count of actual detail records
					if( $intAddendaCount != $intAddendaBatchCount ) {
						$this->addOutput( 'Record ' . ( int ) $intI . ' has Batch Control Addenda Count ' . ( int ) $intAddendaCount . ' unmatched with total record count ' . ( int ) $intAddendaBatchCount . "\n" . $arrstrLines[$intI], ERROR_TYPE_VALIDATION );
						$boolIsVerified = false;
					}

					$intBatchesFileCount++;
					break;

					// File Control Record
				case '9':
					// **********************************************************************************************************************
					// File control record is the final record in the file (aside from filler records).  This is where we assert the
					//   all of the tallied counts and amounts against the values in the control records
					// **********************************************************************************************************************

					$strBatchCount = substr( $arrstrLines[$intI], 1, 6 );
					$strBlockCount = substr( $arrstrLines[$intI], 7, 6 );
					$strDebitAmount = substr( $arrstrLines[$intI], 31, 12 );
					$strCreditAmount = substr( $arrstrLines[$intI], 43, 12 );

					$intBatchCount = intval( $strBatchCount );
					$intBlockCount = intval( $strBlockCount );
					$fltDebitAmount = floatval( $strDebitAmount );
					$fltCreditAmount = floatval( $strCreditAmount );

					// If all '9's, this is a filler record (not a File Control record ), so break completely out of loop;
					if( '999999' == $strBatchCount && '999999' == $strBlockCount ) {
						break 2;
					}

					// Here we are handling unbalanced files. When unbalanced, there will be an array of EFT batches in place of the PBA debits/credits
					$fltBalancingCreditAmount = 0;
					$fltBalancingDebitAmount = 0;

					if( true == valArr( $this->m_arrobjEftBatches ) ) {
						foreach( $this->m_arrobjEftBatches as $objEftBatch ) {
							if( 0 < $objEftBatch->getBatchAmount() ) {
								$fltBalancingCreditAmount = bcadd( $fltBalancingCreditAmount, $objEftBatch->getBatchAmount(), 2 );
							} else {
								$fltBalancingDebitAmount = bcadd( $fltBalancingDebitAmount, abs( $objEftBatch->getBatchAmount() ), 2 );
							}
						}
					}

					$this->addOutput( '' );
					$this->addOutput( 'Credit amount in file control       : ' . $fltCreditAmount );
					$this->addOutput( 'Credit amount totalled              : ' . $fltCreditFileTotal );
					if( 0 < $fltBalancingCreditAmount ) {
						$this->addOutput( 'Credit balancing amount totalled  : ' . $fltBalancingCreditAmount );
					}

					$this->addOutput( 'Debit amount in file control        : ' . $fltDebitAmount );
					$this->addOutput( 'Debit amount totalled               : ' . $fltDebitFileTotal );
					if( 0 < $fltBalancingDebitAmount ) {
						$this->addOutput( 'Debit balancing amount totalled : ' . $fltBalancingDebitAmount );
					}

					$this->addOutput( 'Batch count in file control         : ' . ( int ) $intBatchCount );
					$this->addOutput( 'Batches counted                     : ' . ( int ) $intBatchesFileCount );
					$this->addOutput( 'Block count in file control         : ' . ( int ) $intBlockCount . ' (total_records/10)' );
					$this->addOutput( 'Records counted                     : ' . ( int ) $intNumRecords );
					$this->addOutput( '' );

					// Verify either debit or credit is > zero
					if( 0 <= bccomp( 0, $fltDebitAmount ) && 0 <= bccomp( 0, $fltCreditAmount ) ) {
						$this->addOutput( 'Record ' . ( int ) $intI . ' has invalid File Control - Debit/Credit both zero' . "\n" . $arrstrLines[$intI], ERROR_TYPE_VALIDATION );
						$boolIsVerified = false;
					}

					// Verify specified debit for file matches the amount tallied from detail records
					if( 0 != bccomp( $fltDebitAmount, $fltDebitFileTotal ) ) {
						$this->addOutput( 'Record ' . ( int ) $intI . ' has File Control Debit ' . $fltDebitAmount . ' unmatched with total record Debits ' . $fltDebitFileTotal . "\n" . $arrstrLines[$intI], ERROR_TYPE_VALIDATION );
						$boolIsVerified = false;
					}

					// Verify specified credit for file matches the amount tallied from detail records
					if( 0 != bccomp( $fltCreditAmount, $fltCreditFileTotal ) ) {
						$this->addOutput( 'Record ' . ( int ) $intI . ' has File Control Credit ' . $fltCreditAmount . ' unmatched with total record Credits ' . $fltCreditFileTotal . "\n" . $arrstrLines[$intI], ERROR_TYPE_VALIDATION );
						$boolIsVerified = false;
					}

					// Verify debit and credit amounts are "balanced"
					//  NOTE: We do allow for unbalanced Nacha files.  Unbalanced Nacha files have no transactions against PSI intermediary accounts.
					//        So we require pseduo "balancing" entries in the system to track these intermediary movements
					//        We need to verify that the (file credits + balancing credits) == (file debits + balancing debits)
					if( 0 != bccomp( $fltDebitAmount + round( $fltBalancingDebitAmount * 100 ), $fltCreditAmount + round( $fltBalancingCreditAmount * 100 ) ) ) {
						$this->addOutput( 'Record ' . ( int ) $intI . ' has File Control Debit ' . $fltDebitAmount . '+ Balancing Debit ' . round( $fltBalancingDebitAmount * 100 ) . ' unmatched with File Control Credit ' . $fltCreditAmount . ' + Balancing Credit ' . round( $fltBalancingCreditAmount * 100 ) . "\n" . $arrstrLines[$intI], ERROR_TYPE_VALIDATION );
						$boolIsVerified = false;
					}

					// Verify specified batch count matches count of actual batches
					if( $intBatchCount != $intBatchesFileCount ) {
						$this->addOutput( 'Record ' . ( int ) $intI . ' has File Control Addenda Count ' . ( int ) $intBatchCount . ' unmatched with total record count ' . ( int ) $intBatchesFileCount . "\n" . $arrstrLines[$intI], ERROR_TYPE_VALIDATION );
						$boolIsVerified = false;
					}

					// Verify specified block count matches count of actual blocks
					if( $intBlockCount != intval( $intNumRecords / 10 ) && $intBlockCount != ( intval( $intNumRecords / 10 ) - 1 ) ) {
						$this->addOutput( 'Record ' . ( int ) $intI . ' has File Control Block Count ' . ( int ) $intBlockCount . ' unmatched with total record count ' . ( int ) $intNumRecords . "\n" . $arrstrLines[$intI], ERROR_TYPE_VALIDATION );
						$boolIsVerified = false;
					}
					break;

					// Unsupported
				default:
					$this->addOutput( 'Record ' . ( int ) $intI . ' has unsupported File Record Type: ' . substr( $arrstrLines[$intI], 0, 1 ) . "\n" . $arrstrLines[$intI], ERROR_TYPE_VALIDATION );
					$boolIsVerified = false;
					break;
			}
		}

		if( true == $boolIsVerified ) {
			$this->addOutput( 'SUCCESS VERIFY', ERROR_TYPE_VALIDATION );
		} else {
			$this->addOutput( 'FAILED VERIFY', ERROR_TYPE_VALIDATION );
		}

		return $boolIsVerified;
	}

	// This function is to reload all the nacha file data back into the objects for validation & other manipulation when required

	public function restoreDatabaseData( $objPaymentDatabase ) {

		$this->m_arrobjNachaFileBatches = \Psi\Eos\Payment\CNachaFileBatches::createService()->fetchNachaFileBatchesByNachaFileId( $this->getId(), $objPaymentDatabase );
		$this->m_arrobjNachaEntryDetailRecords = \Psi\Eos\Payment\CNachaEntryDetailRecords::createService()->fetchNachaEntryDetailRecordsByNachaFileId( $this->getId(), $objPaymentDatabase );

		if( true == valArr( $this->m_arrobjNachaEntryDetailRecords ) ) {
			foreach( $this->m_arrobjNachaEntryDetailRecords as $objNachaEntryDetailRecord ) {
				$objNachaEntryDetailRecord->setDfiAccountNumber( $objNachaEntryDetailRecord->getCheckAccountNumber() );
			}
		}

		CObjectModifiers::createService()->nestObjects( $this->m_arrobjNachaEntryDetailRecords, $this->m_arrobjNachaFileBatches );

		// We need to reset the DFI account number here, because it never gets loaded in raw format into the database.

		// I don't need this function yet, but I got it started in case we need to iterate through the database version of an ACH file and do something with it
		// For example, at some point we may need the ability to re-write an ACH file, with some bug fixed.

		return $this->rewriteFileToFileSystem();
	}

	public function determineHeaderEffectiveEntryDate( $objPaymentDatabase ) {

		// *********************************************************************************************************
		// We want to calculate the next business day after today
		//   We'll start at tomorrow and continue to increment the days until we find a non-holiday, non-weekend day
		// *********************************************************************************************************

		// Get holidays that occur in the next 30 days
		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y', '+1 day' ), date( 'm/d/Y', strtotime( '+30 days' ) ), $objPaymentDatabase );

		// Create an array of all prohibited holiday dates
		$arrintProhibitedTransactionHolidays = array();

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intEffectiveEntryDate = strtotime( '+1 day' );

		// Now we loop until we find a non-holiday, non-weekend day
		while( 'Sun' == date( 'D', $intEffectiveEntryDate ) || 'Sat' == date( 'D', $intEffectiveEntryDate ) || true == in_array( strtotime( date( 'm/d/Y', $intEffectiveEntryDate ) ), $arrintProhibitedTransactionHolidays ) ) {
			$intEffectiveEntryDate = strtotime( '+1 day', $intEffectiveEntryDate );
		}

		return date( 'ymd', $intEffectiveEntryDate );
	}

	public function rewriteFileToFileSystem() {
		trigger_error( 'Function rewriteFileToFileSystem() not implemented', E_USER_ERROR );
		exit;
	}

	// Add an info or error message to the message array

	private function addOutput( $strMessage, $intErrorType = NULL ) {
		$this->addErrorMsg( new CErrorMsg( $intErrorType, NULL, $strMessage, NULL ) );
	}

}
?>