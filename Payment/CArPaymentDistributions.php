<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentDistributions
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CArPaymentDistributions extends CBaseArPaymentDistributions {

	public static function fetchArPaymentDistributionsBySettlementDistributionIds( $arrintSettlementDistributionIds, $objPaymentDatabase ) {
		if( false == valArr( $arrintSettlementDistributionIds ) ) return;
		$strSql = 'SELECT * FROM ar_payment_distributions WHERE settlement_distribution_id IN( ' . implode( ',', $arrintSettlementDistributionIds ) . ') ';
		return self::fetchArPaymentDistributions( $strSql, $objPaymentDatabase );
	}

	public static function fetchArPaymentDistributionsByArPaymentId( $intArPaymentId, $objPaymentDatabase ) {
        return self::fetchArPaymentDistributions( sprintf( 'SELECT * FROM %s WHERE ar_payment_id = %d ORDER BY created_on DESC', 'ar_payment_distributions', ( int ) $intArPaymentId ), $objPaymentDatabase );
    }

    public static function fetchComplexArPaymentDistributionsBySettlementDistributionId( $intSettlementDistributionId, $objPaymentDatabase ) {

    	$strSql = 'SELECT DISTINCT ON ( apd.id ) apd.*,
    						ap.property_id,
    						ap.is_split AS is_split_ar_payment
    					FROM
    						ar_payment_distributions apd,
    						ar_payments ap
    					WHERE
    						apd.ar_payment_id = ap.id
    						AND apd.settlement_distribution_id = ' . ( int ) $intSettlementDistributionId;

    	return self::fetchArPaymentDistributions( $strSql, $objPaymentDatabase );
    }

	public static function fetchAttemptCountBySettlementDistributionId( $intSettlementDistributionId, $objPaymentDatabase ) {
		$strSql = 'SELECT
						( MAX(previous_distributions) + 1 ) attempt_count
					FROM (
					  SELECT
					      apd.ar_payment_id,
					      COUNT(apd2.id) previous_distributions
					  FROM
					      ar_payment_distributions apd
					      LEFT JOIN ar_payment_distributions apd2 ON
					          apd.ar_payment_id = apd2.ar_payment_id
					          AND apd.payment_distribution_type_id = apd2.payment_distribution_type_id
					          AND apd.id > apd2.id
					  WHERE
					      apd.settlement_distribution_id =' . ( int ) $intSettlementDistributionId . '
					      AND apd.created_on > ( NOW() - INTERVAL \'12 months\' )
					  GROUP BY
					      apd.ar_payment_id
					) x';

		$arrintSettlementDistributionAttemptCount = fetchData( $strSql, $objPaymentDatabase );

		if( true == valArr( $arrintSettlementDistributionAttemptCount ) && true == isset( $arrintSettlementDistributionAttemptCount[0]['attempt_count'] ) ) {
			return $arrintSettlementDistributionAttemptCount[0]['attempt_count'];
		} else {
			return 0;
		}
	}

	public static function fetchNotMatchingArPaymentsWithBatchedArPaymentsBetweenDateRangeByProcessingBankAccountIdByPaymentDistributionTypeIdByEftBatchTypeid( $strStartDate, $strEndDate, $intProcessingBankAccountId, $intEftBatchTypeId, $intPaymentDistributionTypeId, $objPaymentDatabase ) {

		$strSql = ' SELECT apd.ar_payment_id
					FROM ar_payment_distributions apd
					     LEFT JOIN batched_ar_payments bap ON bap.id = apd.ar_payment_id
					     LEFT JOIN eft_batches eb ON (bap.eft_batch_id = eb.id AND
					     eb.processing_bank_account_id = ' . ( int ) $intProcessingBankAccountId . ' AND
						 eb.eft_batch_type_id = ' . ( int ) $intEftBatchTypeId . ' )
					WHERE (apd.created_on BETWEEN \'' . $strStartDate . '\' AND
					      \'' . $strEndDate . '\'       ) AND
					      apd.processing_bank_account_id = ' . ( int ) $intProcessingBankAccountId . ' AND
					      apd.payment_distribution_type_id = ' . ( int ) $intPaymentDistributionTypeId . ' AND
					      bap.id IS NULL';

		return fetchData( $strSql, $objPaymentDatabase );
	}

	public static function fetchSettlementDistributionAmountBySettlementDistributionIds( $arrintSettlementDistributionId, $objPaymentDatabase ) {
		$strSql = 'SELECT
						apd.settlement_distribution_id, sum( apd.distribution_amount ) as amount, apd.cid
					FROM
                        ar_payment_distributions apd
				    WHERE
                        apd.settlement_distribution_id IN ' . $arrintSettlementDistributionId . '
                    GROUP BY
                        apd.settlement_distribution_id, apd.cid';

		return fetchData( $strSql, $objPaymentDatabase );
	}

}
?>