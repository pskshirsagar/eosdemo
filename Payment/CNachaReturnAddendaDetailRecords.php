<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaReturnAddendaDetailRecords
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CNachaReturnAddendaDetailRecords extends CBaseNachaReturnAddendaDetailRecords {

    public static function fetchReturnNachaReturnAddendaDetailRecordByOriginalEntryTraceNumberByNachaEntryDetailRecordId( $strOriginalEntryTraceNumber, $intNachaEntryDetailRecordId, $objPaymentDatabase ) {

        $strSql = 'SELECT * FROM
        				nacha_return_addenda_detail_records
        			WHERE
        				nacha_entry_detail_record_id = ' . ( int ) $intNachaEntryDetailRecordId . '
        				AND original_entry_trace_number = \'' . addslashes( $strOriginalEntryTraceNumber ) . '\'
        			ORDER BY
        				created_on DESC
        			LIMIT 1';

        return self::fetchNachaReturnAddendaDetailRecord( $strSql, $objPaymentDatabase );
    }

	public static function fetchUnTransferredNachaReturnAddendaDetailRecordsByNachaReturnFileId( $intNachaReturnFileId, $objPaymentDatabase ) {

		$strSql = ' SELECT
						nradr.*
					FROM
						nacha_return_addenda_detail_records nradr,
						nacha_return_detail_records nrdr,
						nacha_files nf
					WHERE
						nradr.nacha_file_id = nf.id
						AND nradr.nacha_return_detail_record_id = nrdr.id
						AND nradr.returns_account_transferred_on IS NULL
						AND nradr.is_notice_of_change <> 1
						AND nrdr.nacha_return_file_id = ' . ( int ) $intNachaReturnFileId;

		return self::fetchNachaReturnAddendaDetailRecords( $strSql, $objPaymentDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsByNachaReturnFileId( $intNachaReturnFileId, $objPaymentDatabase ) {

		$strSql = ' SELECT
						nradr.*
					FROM
						nacha_return_addenda_detail_records nradr,
						nacha_return_detail_records nrdr,
						nacha_files nf
					WHERE
						nradr.nacha_file_id = nf.id
						AND nradr.nacha_return_detail_record_id = nrdr.id
						AND nrdr.nacha_return_file_id = ' . ( int ) $intNachaReturnFileId;

		return self::fetchNachaReturnAddendaDetailRecords( $strSql, $objPaymentDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsByArPaymentId( $intArPaymentId, $objPaymentDatabase ) {

		$strSql = ' SELECT
						nradr.*
					FROM
						nacha_return_addenda_detail_records nradr
					WHERE
						nradr.ar_payment_id = ' . ( int ) $intArPaymentId;

		return self::fetchNachaReturnAddendaDetailRecords( $strSql, $objPaymentDatabase );
	}

	// This function gathers the returns that eligible for ACH return fees.  The query looks for
	//  records that have the requires_return_fee flag asserted, have never been returned, or
	//  the ACH transaction associated with the return has came back as NSF.  We also safeguard against
	//  an ar_payment being returned twice; In this case only the first return is eligible for return fees.

	public static function fetchFeeEligibleNachaReturnAddendaDetailRecords( $intMaxAttempts, $objPaymentDatabase ) {

		$strSql = ' SELECT
						nradr.*
					FROM
						nacha_return_addenda_detail_records nradr
                        INNER JOIN ar_payments ap ON nradr.ar_payment_id = ap.id
					WHERE
						nradr.created_on > ( NOW() - INTERVAL \'90 days\' )
						AND	nradr.requires_return_fee = 1
						AND COALESCE ( nradr.return_fee_attempt_count, 0 ) < ' . ( int ) $intMaxAttempts . '
                        AND ap.batched_on <= ( NOW() - INTERVAL \'7 days\' )
						AND (
							nradr.last_return_fee_attempt_on IS NULL
                            OR (
                            	nradr.last_return_fee_attempt_on < nradr.last_return_fee_failed_on
                            	AND nradr.last_return_fee_failed_on <= ( NOW() - INTERVAL \'10 days\' )
                            )
						)
						AND nradr.id = (
							SELECT
								MIN(id)
							FROM
								nacha_return_addenda_detail_records
							WHERE
								ar_payment_id = nradr.ar_payment_id
						)';

		return self::fetchNachaReturnAddendaDetailRecords( $strSql, $objPaymentDatabase );
	}

	// This function gathers the returns that have charged residents fees that we now want to refund. The way we are
	// Identifying these returns is if they have attempted to charge the fee but are no longer configured to charge
	// a fee.

	public static function fetchRecreditFeeEligibleNachaReturnAddendaDetailRecords( $objPaymentDatabase ) {
		$strSql = ' SELECT
						nradr.*
					FROM
						nacha_return_addenda_detail_records nradr
                        INNER JOIN ar_payments ap ON nradr.ar_payment_id = ap.id
					WHERE
						nradr.requires_return_fee = 0
						AND nradr.return_fee_attempt_count >= 1
                        AND nradr.last_return_fee_attempt_on IS NOT NULL
						AND nradr.last_return_fee_failed_on IS NULL';

		return self::fetchNachaReturnAddendaDetailRecords( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnReconPreppedNachaReturnAddendaDetailRecords( $objPaymentDatabase, $intProcessingBankId ) {

		$strSql = ' SELECT
						nradr.*
					FROM
						nacha_return_addenda_detail_records nradr,
						nacha_return_detail_records nrdr,
						nacha_return_files nrf
					WHERE
						nradr.nacha_return_detail_record_id = nrdr.id
						AND nrdr.nacha_return_file_id = nrf.id
						AND nrf.processing_bank_id = ' . ( int ) $intProcessingBankId . '
						AND nradr.is_notice_of_change = 0
						AND nradr.is_recon_prepped = 0 ';

		return self::fetchNachaReturnAddendaDetailRecords( $strSql, $objPaymentDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsByNachaReturnDetailRecordIds( $arrintIds, $objDatabase ) {
		if( false == \Psi\Libraries\UtilFunctions\valArr( $arrintIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT * FROM nacha_return_addenda_detail_records WHERE nacha_return_detail_record_id IN ( ' . implode( ',', $arrintIds ) . ' )';
		return self::fetchNachaReturnAddendaDetailRecords( $strSql, $objDatabase );
	}

}
?>