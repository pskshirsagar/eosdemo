<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CWesternUnionInputRecords
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CWesternUnionInputRecords extends CBaseWesternUnionInputRecords {

	public static function fetchWesternUnionInputRecordByClientOrderNumber( $strTransactionIdNumber, $objPaymentDatabase ) {

		$strSql = '	SELECT
						*
					FROM
						western_union_input_records r
					WHERE
						r.client_order_number = \'' . addslashes( $strTransactionIdNumber ) . '\'
						AND r.cancelled_on IS NULL
					ORDER BY
						r.id DESC
					LIMIT 1';

		return self::fetchWesternUnionInputRecord( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnsentWesternUnionInputRecords( $objPaymentDatabase ) {
		return self::fetchWesternUnionInputRecords( sprintf( 'SELECT * FROM %s WHERE file_id IS NULL ORDER BY id', 'western_union_input_records' ), $objPaymentDatabase );
	}

}
?>