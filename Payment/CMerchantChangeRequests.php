<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMerchantChangeRequests
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CMerchantChangeRequests extends CBaseMerchantChangeRequests {

	public static function fetchActiveMerchantChangeRequestsByCid( $intCid, $objPaymentDatabase, $boolProcessed = false ) {
		$strCondition = '';
		if( false == $boolProcessed ) {
			$strCondition = ' AND processed_on IS NULL';
		} else {
			$strCondition = ' AND processed_on IS NOT NULL';
		}
		$strSql = 'SELECT * FROM merchant_change_requests WHERE cid = ' . ( int ) $intCid . ' AND deleted_by IS NULL ' . $strCondition . ' ORDER BY created_on DESC ';
		return self::fetchMerchantChangeRequests( $strSql, $objPaymentDatabase );
	}

	public static function fetchPaginatedMerchantChangeRequestsByCompanyStatusTypeId( $intPageNumber, $intPageSize, $intCompanyStatusTypeId, $objMerchantChangeRequestFilter, $objPaymentDatabase ) {

		$intOffset = ( 0 < $intPageNumber ) ? $intPageSize * ( $intPageNumber - 1 ) : 0;
    	$intLimit = ( int ) $intPageSize;

    	$strSqlCondition = self::genereateSqlFromFilter( $objMerchantChangeRequestFilter );

    	$strSql = 'SELECT
    					mcr.*
    				FROM
    					merchant_change_requests mcr,
    					clients c
    				WHERE
    					mcr.cid = c.id
    					AND c.company_status_type_id = ' . ( int ) $intCompanyStatusTypeId . '
    					AND deleted_by IS NULL ' . $strSqlCondition . '
    				ORDER BY
    					 processed_on desc, created_on desc
    				OFFSET
    					' . ( int ) $intOffset . '
    				LIMIT
    					' . $intLimit;

		return self::fetchMerchantChangeRequests( $strSql, $objPaymentDatabase );
	}

	public static function fetchTotalMerchantChangeRequestsCountByCompanyStatusTypeId( $intCompanyStatusTypeId, $objMerchantChangeRequestFilter, $objPaymentDatabase ) {

		$strSqlCondition = self::genereateSqlFromFilter( $objMerchantChangeRequestFilter );

		$strSql = 'SELECT
						count( mcr.id ) AS count
					FROM
    					merchant_change_requests mcr,
    					clients c
					WHERE
    					mcr.cid = c.id
    					AND c.company_status_type_id = ' . ( int ) $intCompanyStatusTypeId . '
    					AND deleted_by IS NULL' . $strSqlCondition;

		$arrintResponse = fetchData( $strSql, $objPaymentDatabase );

    	if ( true == isset ( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

    	return 0;
	}

	public static function genereateSqlFromFilter( $objMerchantChangeRequestFilter ) {

		$strSqlCondition = '';

		if( false == is_null( $objMerchantChangeRequestFilter ) ) {
			$arrstrAndConditions = array();

			if( 0 < $objMerchantChangeRequestFilter->getCid() ) 		$arrstrAndConditions[] = ' c.id = ' . ( int ) $objMerchantChangeRequestFilter->getCid();
			if( 1 == $objMerchantChangeRequestFilter->getIsProcessed() ) 			$arrstrAndConditions[] = ' mcr.processed_on IS NOT NULL ';
			if( 1 == $objMerchantChangeRequestFilter->getIsCloseAccountRequest() ) 	$arrstrAndConditions[] = ' mcr.is_close_account_request = 1 ';

			if( true == valArr( $arrstrAndConditions ) ) {
				$strSqlCondition = implode( ' AND ', $arrstrAndConditions );
			}

			if( 0 < strlen( $strSqlCondition ) ) {
				$strSqlCondition = ' AND' . $strSqlCondition;
			}
		}

		return $strSqlCondition;
	}

	public static function fetchPaginatedActiveNonSignedMerchantChangeRequestsByCid( $intCid, $intOffset, $intPageLimit, $strSortBy, $objPaymentDatabase ) {

		$strSql = 'SELECT 
			id,
			requestor_name,
			request_datetime,
			request_description 
		FROM 
			merchant_change_requests 
		WHERE 
			cid = ' . ( int ) $intCid . ' AND 
			deleted_by IS NULL AND
			deleted_on IS NULL AND 
			processed_on IS NULL AND
			signature_verified_on IS NULL';

		$strSql .= ' ORDER BY
		' . $strSortBy . '
		OFFSET ' . ( int ) $intOffset . '
		LIMIT ' . ( int ) $intPageLimit;

		$arrintResult = fetchData( $strSql, $objPaymentDatabase );

		return $arrintResult;

	}

	public static function fetchActiveNonSignedMerchantChangeRequestsCountByCid( $intCid, $objPaymentDatabase ) {

		$strSql = 'SELECT 
					 count(id)
					FROM 
						merchant_change_requests 
					WHERE 
						cid = ' . ( int ) $intCid . ' AND 
						deleted_by IS NULL AND
						deleted_on IS NULL AND 
						processed_on IS NULL AND
						signature_verified_on IS NULL';

		$arrintResult = fetchData( $strSql, $objPaymentDatabase );

		return $arrintResult;
	}

	public static function fetchMerchantChangeRequestsByIdsByCid( $arrintIds, $intCid, $objPaymentDatabase ) {
		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM merchant_change_requests WHERE cid = ' . ( int ) $intCid . ' AND id IN ( ' . implode( ',', $arrintIds ) . ' )';

		return self::fetchMerchantChangeRequests( $strSql, $objPaymentDatabase );
	}

	public static function fetchInternationalMerchantChangeRequestsCount( $objPaymentDatabase, $intCompanyStatusTypeId, $boolIsProcessed = false ) {
		$strWhereCondition = '';
		if( true == $boolIsProcessed ) {
			$strWhereCondition = ' AND mcr.processed_on IS NOT NULL';
		}

		$strSql = 'SELECT COUNT( mcr.id ) AS count FROM merchant_change_requests mcr
    				INNER JOIN clients c ON(c.id = mcr.cid)
					INNER JOIN merchant_change_request_accounts mcra ON(mcra.merchant_change_request_id = mcr.id AND mcra.merchant_processing_type_id = ' . CMerchantProcessingType::INTERNATIONAL . ')
						WHERE c.company_status_type_id = ' . ( int ) $intCompanyStatusTypeId . ' AND mcr.deleted_by IS NULL ' . $strWhereCondition . ' 
						GROUP BY mcr.id HAVING COUNT(mcra.id) > 0 ORDER BY mcr.id';

		$arrintResponse = fetchData( $strSql, $objPaymentDatabase );

		if ( true == isset ( $arrintResponse[0]['count'] ) ) return ( int ) $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchInternationalMerchantChangeRequests( $objPaymentDatabase, $intCompanyStatusTypeId, $boolIsProcessed = false, $intPageNo = NULL, $intCountPerPage = NULL ) {
		$strWhereCondition = '';
		if( true == $boolIsProcessed ) {
			$strWhereCondition = ' AND mcr.processed_on IS NOT NULL';
		}

		$strOffset = '';
		if( false == is_null( $intPageNo ) && false == is_null( $intCountPerPage ) ) {
			$strOffset = ' OFFSET ' . ( int ) $intPageNo . ' LIMIT ' . ( int ) $intCountPerPage;
		}

		$strSql = 'SELECT mcr.* FROM merchant_change_requests mcr
    				INNER JOIN clients c ON(c.id = mcr.cid)
					INNER JOIN merchant_change_request_accounts mcra ON(mcra.merchant_change_request_id = mcr.id AND mcra.merchant_processing_type_id = ' . CMerchantProcessingType::INTERNATIONAL . ')
						WHERE c.company_status_type_id = ' . ( int ) $intCompanyStatusTypeId . ' AND mcr.deleted_by IS NULL ' . $strWhereCondition . ' 
						GROUP BY mcr.id HAVING COUNT(mcra.id) > 0 ORDER BY mcr.id' . $strOffset;

		return self::fetchMerchantChangeRequests( $strSql, $objPaymentDatabase );
	}

}
?>