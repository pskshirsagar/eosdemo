<?php

class CChargeBack extends CBaseChargeBack {

	protected $m_intProcessingBankAccountId;
	protected $m_fltTotal;

    public function createReconciliationEntry( $objPaymentDatabase ) {

		$objReconciliationEntry = new CReconciliationEntry();
		$objReconciliationEntry->setProcessingBankAccountId( $this->getProcessingBankAccountId() );
		$objReconciliationEntry->setReconciliationEntryTypeId( CReconciliationEntryType::AR_PAYMENTS_CHARGE_BACKS );
		$objReconciliationEntry->setChargeBackId( $this->getId() );

		if( 1 == $this->getIsRefund() ) {
			$objReconciliationEntry->setTotal( abs( $this->getTotal() ) );
		} else {
			$objReconciliationEntry->setTotal( -1 * abs( $this->getTotal() ) );
		}

		// Make sure that the return date is not in the common.transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.

		$intCreatedOn = strtotime( $this->getCreatedOn() );

		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y', $intCreatedOn ), date( 'm/d/Y', ( $intCreatedOn + ( 21 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = array();

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intCreatedOn = strtotime( '+1 day', $intCreatedOn );

		while( 'Sun' == date( 'D', $intCreatedOn ) || 'Sat' == date( 'D', $intCreatedOn ) || true == in_array( $intCreatedOn, $arrintProhibitedTransactionHolidays ) ) {
			$intCreatedOn = strtotime( '+1 day', $intCreatedOn );
		}

		$objReconciliationEntry->setMovementDatetime( date( 'm/d/Y', $intCreatedOn ) );

		return $objReconciliationEntry;
	}

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['processing_bank_account_id'] ) ) $this->setProcessingBankAccountId( $arrmixValues['processing_bank_account_id'] );
        if( true == isset( $arrmixValues['total'] ) ) $this->setTotal( $arrmixValues['total'] );

        return;
    }

	public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
		$this->m_intProcessingBankAccountId = $intProcessingBankAccountId;
	}

	public function setTotal( $fltTotal ) {
		$this->m_fltTotal = $fltTotal;
	}

	public function getProcessingBankAccountId() {
		return $this->m_intProcessingBankAccountId;
	}

	public function getTotal() {
		return $this->m_fltTotal;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>