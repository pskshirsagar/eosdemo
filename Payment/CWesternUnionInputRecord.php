<?php

require_once( PATH_PHP_APPLICATIONS . 'Applications.defines.php' );
require_once( PATH_PHP_FRAMEWORKS . 'Frameworks.defines.php' );
require_once( PATH_PHP_LIBRARIES . 'Libraries.defines.php' );

use Psi\Libraries\HtmlMimeMail\CHtmlMimeMail;

class CWesternUnionInputRecord extends CBaseWesternUnionInputRecord {

	protected $m_objCompanyMerchantAccount;

    public function insert( $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly = false ) {

    	// Only set the id and calculate/set the TID (client order #) if the values are not
    	// * already set.  We are reusing the same TID across multiple records, so this is
    	// *  very important

    	if( false == isset( $this->m_intId ) || 0 >= intval( $this->m_intId ) ) {
    		$this->setId( $this->fetchNextId( $objAdminDatabase ) );
    	}

    	if( false == isset( $this->m_strClientOrderNumber ) || 0 >= strlen( $this->m_strClientOrderNumber ) ) {
    		$this->setClientOrderNumber( CWesternUnionController::calcTransactionId( $this->m_intId, false ) );
    	}

    	return parent::insert( $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly );
    }

	/**
	 * Get or Fetch Functions
	 */

	public function getOrFetchProperty( $objClientDatabase ) {

		if( true == valObj( $this->m_objProperty, 'CProperty' ) ) {
			return $this->m_objProperty;
		} else {
			return $this->fetchProperty( $objClientDatabase );
		}
	}

	public function getOrFetchCustomer( $objClientDatabase ) {

		if( true == valObj( $this->m_objCustomer, 'CCustomer' ) ) {
			return $this->m_objCustomer;
		} else {
			return $this->fetchCustomer( $objClientDatabase );
		}
	}

	public function getOrFetchLease( $objClientDatabase ) {

		if( true == valObj( $this->m_objLease, 'CLease' ) ) {
			return $this->m_objLease;
		} else {
			return $this->fetchLease( $objClientDatabase );
		}
	}

	public function getOrFetchCompanyMerchantAccount( $objPaymentDatabase ) {

		if( true == valObj( $this->m_objCompanyMerchantAccount, 'CCompanyMerchantAccount' ) ) {
			return $this->m_objCompanyMerchantAccount;
		} else {
			return $this->fetchCompanyMerchantAccount( $objPaymentDatabase );
		}
	}

	/**
	 * Set Functions
	 */

	public function setPurchaseAmount( $strPurchaseAmount ) {
        $this->m_strPurchaseAmount = ( float ) CStrings::strToFloatDef( $strPurchaseAmount, NULL, false, 4 );
    }

	public function setDefaults() {

		$this->setRecordType( 'D' );
		$this->setTransactionType( 'A' );  // "A" is for add, "S" is for cancel
		$this->setMoneyTransferType( '' );
		$this->setNameFormatCode( 'D' );
		$this->setCustomerCountry( 'US' );
		$this->setCodeCity( 'ASF045677' );
		$this->setCodeState( '' );
		$this->setOptInFlag( '' );
		$this->setClientDate( date( 'Ymd' ) );
		$this->setClientTime( date( 'His' ) );
		$this->setExpirationDate( date( 'Ymd', mktime( 0, 0, 0, date( 'm' ) + 2, date( 'd' ), date( 'Y' ) ) ) );
		$this->setExpirationTime( '235959' );

		return;
	}

	public function setForeignKeys( $intFileId, $intCid, $intPropertyId, $intCustomerId, $intLeaseId ) {

		$this->setFileId( $intFileId );
		$this->setCid( $intCid );
		$this->setPropertyId( $intPropertyId );
		$this->setCustomerId( $intCustomerId );
		$this->setLeaseId( $intLeaseId );
		$this->setClientReference( 'lease_id:$intLeaseId' );
		$this->setClientName( 'cid:$intCid' );
	}

	/**
	 * Validation Functions
	 */

    public function valId() {
        $boolIsValid = true;

        if( true == is_null( $this->getId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', '' ) );
        }

        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;

        if( true == is_null( $this->getCustomerId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', '' ) );
        }

        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;

        if( true == is_null( $this->getLeaseId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', '' ) );
        }

        return $boolIsValid;
    }

    public function valCustomerFirstName() {
        $boolIsValid = true;

        if( true == is_null( $this->getCustomerFirstName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_first_name', 'Customer first name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valCustomerLastName() {
        $boolIsValid = true;

        if( true == is_null( $this->getCustomerLastName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_last_name', 'Customer last name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valCustomerStreetAddress() {
        $boolIsValid = true;

        if( true == is_null( $this->getCustomerStreetAddress() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_street_address', 'Customer street address is required.' ) );
        }

        return $boolIsValid;
    }

    public function valCustomerCity() {
        $boolIsValid = true;

        if( true == is_null( $this->getCustomerCity() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_city', 'Customer city is required' ) );
        }

        return $boolIsValid;
    }

    public function valCustomerStateprovince() {
        $boolIsValid = true;

        if( true == is_null( $this->getCustomerStateprovince() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_stateprovince', 'Customer state province is required.' ) );
        }

        return $boolIsValid;
    }

    public function valCustomerPostalCode() {
        $boolIsValid = true;

        if( true == is_null( $this->getCustomerPostalCode() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_postal_code', 'Customer postal code is required.' ) );
        }

        return $boolIsValid;
    }

    public function valCustomerTelephone() {
        $boolIsValid = true;

        if( true == is_null( $this->getCustomerTelephone() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_telephone', 'Customer telephone is required.' ) );
        }

        return $boolIsValid;
    }

    public function valCustomerCountry() {
        $boolIsValid = true;

        if( true == is_null( $this->getCustomerCountry() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_country', 'Customer country name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valCustomerEmailAddress() {
        $boolIsValid = true;

        if( true == is_null( $this->getCustomerEmailAddress() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_email_address', 'Customer email address is required.' ) );
        }

        return $boolIsValid;
    }

    public function valClientOrderNumber() {
        $boolIsValid = true;

        if( true == is_null( $this->getClientOrderNumber() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_order_number', 'Client order number is required.' ) );
        }

        return $boolIsValid;
    }

    public function valClientReference() {
        $boolIsValid = true;

        if( true == is_null( $this->getClientReference() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_reference', 'Client reference is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPurchaseAmount( $objCompanyMerchantAccount ) {
        $boolIsValid = true;

        if( true == is_null( $this->getPurchaseAmount() ) || 0 >= ( float ) $this->getPurchaseAmount() ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'purchase_amount', 'Valid purchase amount is required.' ) );
        }

        if( true == $boolIsValid && ( float ) 950 < ( float ) $this->getPurchaseAmount() ) {
        	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'purchase_amount', 'Maximum Western Union purchase amount is $950.' ) );
        }

        if( true == $boolIsValid && true == valObj( $objCompanyMerchantAccount, 'CCompanyMerchantAccount' ) ) {
			if( $objCompanyMerchantAccount->getEmoMaxPaymentAmount() <= ( float ) $this->getPurchaseAmount() ) {
				$boolIsValid = false;
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'purchase_amount', 'Purchase amount exceeds the max amount set for Merchant Account.' ) );
			}
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objCompanyMerchantAccount = NULL ) {
    	$boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
               	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valCustomerId();
            	$boolIsValid &= $this->valLeaseId();
            	$boolIsValid &= $this->valCustomerFirstName();
            	$boolIsValid &= $this->valCustomerLastName();
            	$boolIsValid &= $this->valCustomerStreetAddress();
            	$boolIsValid &= $this->valCustomerCity();
            	$boolIsValid &= $this->valCustomerStateprovince();
            	$boolIsValid &= $this->valCustomerPostalCode();
            	$boolIsValid &= $this->valCustomerTelephone();
            	// $boolIsValid &= $this->valCustomerCountry();
            	$boolIsValid &= $this->valCustomerEmailAddress();
            	// $boolIsValid &= $this->valClientOrderNumber();
            	$boolIsValid &= $this->valClientReference();
            	$boolIsValid &= $this->valPurchaseAmount( $objCompanyMerchantAccount );
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Other Functions
     */

    public function cloneAndPrepNewObject( $objPaymentDatabase, $intUserId = 2 ) {

    	// Clone object and reset fields to prepare item for insert
		$objNewWesternUnionInputRecord = clone $this;
		$objNewWesternUnionInputRecord->setDefaults();
        $objNewWesternUnionInputRecord->setFileId( NULL );
        $objNewWesternUnionInputRecord->setCancelledOn( NULL );
        $objNewWesternUnionInputRecord->setUpdatedBy( $intUserId );
        $objNewWesternUnionInputRecord->setUpdatedOn( date( 'Ymd His' ) );

        // Get next WU input file Id
		$objNewWesternUnionInputRecord->setId( $objNewWesternUnionInputRecord->fetchNextId( $objPaymentDatabase ) );

		return $objNewWesternUnionInputRecord;
    }

    public function sendWesternUnionInputRecordDetails( $arrobjProperties, $objClient, $objClientDatabase, $objEmailDatabase ) {

		$objProperty = $arrobjProperties[$this->getPropertyId()];
		$objPropertyEmailAddress = $objProperty->fetchOfficeEmailAddress( $objClientDatabase );
		$arrstrPaymentNotificationEmails = $objProperty->fetchPaymentNotificationEmails( $objClientDatabase );

		$strHtmlEmailOutput = $this->buildHtmlWesternUnionInputRecordDetailsContent( $arrobjProperties, $objClient, $objClientDatabase );
		$strSubject 		= 'Western Union Customer Transaction Details';

		$objSystemEmail = new CSystemEmail();

		$objWesternUnionEmailPropertyPreference = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'FROM_PAYMENT_NOTIFICATION_EMAIL', $this->getPropertyId(), $this->getCid(), $objClientDatabase );

		if( false == is_null( $objWesternUnionEmailPropertyPreference ) && true == valObj( $objWesternUnionEmailPropertyPreference, 'CPropertyPreference' ) && false == is_null( $objWesternUnionEmailPropertyPreference->getValue() ) ) {
			$objSystemEmail->setFromEmailAddress( $objWesternUnionEmailPropertyPreference->getValue() );
		}

		$objSystemEmail->setToEmailAddress( $this->getCustomerEmailAddress() );
		$objSystemEmail->setHtmlContent( $strHtmlEmailOutput );
		$objSystemEmail->setSubject( $strSubject );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::SCHEDULED_EMAIL );
		$objSystemEmail->setCid( $this->getCid() );
		$objSystemEmail->setPropertyId( $this->getPropertyId() );
		$arrobjSystemEmails = array();
		$arrobjSystemEmails[] = clone $objSystemEmail;

    	if( true == valArr( $arrstrPaymentNotificationEmails ) ) {

			$arrstrPaymentNotificationEmails = array_unique( $arrstrPaymentNotificationEmails );
		} elseif( false == is_null( $objPropertyEmailAddress->getEmailAddress() ) && 0 < strlen( $objPropertyEmailAddress->getEmailAddress() ) ) {

			$arrstrPaymentNotificationEmails[] = $objPropertyEmailAddress->getEmailAddress();
		}

		foreach( $arrstrPaymentNotificationEmails as  $strPaymentNotificationEmail ) {
			$objSystemEmail->setToEmailAddress( $strPaymentNotificationEmail );
			$arrobjSystemEmails[] = clone $objSystemEmail;
		}

		foreach( $arrobjSystemEmails as $objSystemEmail ) {

			if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {

				$objMimeEmail = new CHtmlMimeMail();

				$objMimeEmail->setFrom( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
				$objMimeEmail->setHtml( $strHtmlEmailOutput );
				$objMimeEmail->setSubject( $strSubject );

				$objMimeEmail->send( $objSystemEmail->getToEmailAddress() );
			}
		}

		return true;
	}

	public function buildHtmlWesternUnionInputRecordDetailsContent( $arrobjProperties, $objClient, $objClientDatabase ) {
		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objProperty = $arrobjProperties[$this->getPropertyId()];
		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$objMarketingMediaAssociation			= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $this->m_objDatabase, true );
			$objNsfCustomTextPropertyNotification	= $objProperty->fetchPropertyNotificationByKey( 'NSF_CUSTOM_TEXT', $objClientDatabase );
    	}
		if( true == valObj( $objClient, 'CClient' ) ) {

			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objClientDatabase );
    	}

    	$objSmarty = new CPsSmarty( PATH_INTERFACES_RESIDENT_WORKS, false );

		$objSmarty->assign_by_ref( 'properties', 					$arrobjProperties );
		$objSmarty->assign_by_ref( 'western_union_input_record', 	$this );
		$objSmarty->assign_by_ref( 'marketing_media_association', 	$objMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'default_company_media_file', 	$objLogoMarketingMediaAssociation );

		$objSmarty->assign( 'media_library_uri', 		CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'is_view_type', 			false );
		$objSmarty->assign( 'terms_and_condition_path', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/Common/legal/resident_center_terms_conditions.html' );
		$objSmarty->assign( 'image_url',	            CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$strHtmlRequestContent = $objSmarty->nestedFetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/western_union_input_record/western_union_input_record_notification.tpl' );
		return $strHtmlRequestContent;
	}

}
?>