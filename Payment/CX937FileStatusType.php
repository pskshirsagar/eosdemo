<?php

class CX937FileStatusType extends CBaseX937FileStatusType {

	const BUILDING 			= 1;
	const TRANSMITTING 		= 2;
	const TRANSMITTED 		= 3;
	const RETURNS_PARSING 	= 4;
	const RETURNS_PARSED 	= 5;
}
?>