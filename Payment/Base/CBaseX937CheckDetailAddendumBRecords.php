<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937CheckDetailAddendumBRecords
 * Do not add any new functions to this class.
 */

class CBaseX937CheckDetailAddendumBRecords extends CEosPluralBase {

	/**
	 * @return CX937CheckDetailAddendumBRecord[]
	 */
	public static function fetchX937CheckDetailAddendumBRecords( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CX937CheckDetailAddendumBRecord::class, $objDatabase );
	}

	/**
	 * @return CX937CheckDetailAddendumBRecord
	 */
	public static function fetchX937CheckDetailAddendumBRecord( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CX937CheckDetailAddendumBRecord::class, $objDatabase );
	}

	public static function fetchX937CheckDetailAddendumBRecordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'x937_check_detail_addendum_b_records', $objDatabase );
	}

	public static function fetchX937CheckDetailAddendumBRecordById( $intId, $objDatabase ) {
		return self::fetchX937CheckDetailAddendumBRecord( sprintf( 'SELECT * FROM x937_check_detail_addendum_b_records WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchX937CheckDetailAddendumBRecordsByX937CheckDetailRecordId( $intX937CheckDetailRecordId, $objDatabase ) {
		return self::fetchX937CheckDetailAddendumBRecords( sprintf( 'SELECT * FROM x937_check_detail_addendum_b_records WHERE x937_check_detail_record_id = %d', ( int ) $intX937CheckDetailRecordId ), $objDatabase );
	}

}
?>