<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937ReturnFiles
 * Do not add any new functions to this class.
 */

class CBaseX937ReturnFiles extends CEosPluralBase {

	/**
	 * @return CX937ReturnFile[]
	 */
	public static function fetchX937ReturnFiles( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CX937ReturnFile::class, $objDatabase );
	}

	/**
	 * @return CX937ReturnFile
	 */
	public static function fetchX937ReturnFile( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CX937ReturnFile::class, $objDatabase );
	}

	public static function fetchX937ReturnFileCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'x937_return_files', $objDatabase );
	}

	public static function fetchX937ReturnFileById( $intId, $objDatabase ) {
		return self::fetchX937ReturnFile( sprintf( 'SELECT * FROM x937_return_files WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchX937ReturnFilesByProcessingBankId( $intProcessingBankId, $objDatabase ) {
		return self::fetchX937ReturnFiles( sprintf( 'SELECT * FROM x937_return_files WHERE processing_bank_id = %d', ( int ) $intProcessingBankId ), $objDatabase );
	}

	public static function fetchX937ReturnFilesByProcessingBankAccountId( $intProcessingBankAccountId, $objDatabase ) {
		return self::fetchX937ReturnFiles( sprintf( 'SELECT * FROM x937_return_files WHERE processing_bank_account_id = %d', ( int ) $intProcessingBankAccountId ), $objDatabase );
	}

	public static function fetchX937ReturnFilesByX937FileTypeId( $intX937FileTypeId, $objDatabase ) {
		return self::fetchX937ReturnFiles( sprintf( 'SELECT * FROM x937_return_files WHERE x937_file_type_id = %d', ( int ) $intX937FileTypeId ), $objDatabase );
	}

	public static function fetchX937ReturnFilesByX937FileStatusTypeId( $intX937FileStatusTypeId, $objDatabase ) {
		return self::fetchX937ReturnFiles( sprintf( 'SELECT * FROM x937_return_files WHERE x937_file_status_type_id = %d', ( int ) $intX937FileStatusTypeId ), $objDatabase );
	}

	public static function fetchX937ReturnFilesByMerchantGatewayId( $intMerchantGatewayId, $objDatabase ) {
		return self::fetchX937ReturnFiles( sprintf( 'SELECT * FROM x937_return_files WHERE merchant_gateway_id = %d', ( int ) $intMerchantGatewayId ), $objDatabase );
	}

}
?>