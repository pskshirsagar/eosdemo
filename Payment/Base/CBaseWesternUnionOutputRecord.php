<?php

class CBaseWesternUnionOutputRecord extends CEosSingularBase {

	const TABLE_NAME = 'public.western_union_output_records';

	protected $m_intId;
	protected $m_intFileId;
	protected $m_intCid;
	protected $m_intArPaymentId;
	protected $m_intPropertyId;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_strRecordType;
	protected $m_strTransactionIdNumber;
	protected $m_strFirstName;
	protected $m_strLastName;
	protected $m_strAddress;
	protected $m_strCity;
	protected $m_strStateProvince;
	protected $m_strPostalCode;
	protected $m_strTelephone;
	protected $m_strCountry;
	protected $m_strMtcn;
	protected $m_strAgentZip;
	protected $m_strPaymentAmount;
	protected $m_strTransactionDatetime;
	protected $m_strCommentText;
	protected $m_strClientIdentifier;
	protected $m_strClientReference;
	protected $m_strCustomerFee;
	protected $m_strProcessedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['file_id'] ) && $boolDirectSet ) $this->set( 'm_intFileId', trim( $arrValues['file_id'] ) ); elseif( isset( $arrValues['file_id'] ) ) $this->setFileId( $arrValues['file_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['record_type'] ) && $boolDirectSet ) $this->set( 'm_strRecordType', trim( stripcslashes( $arrValues['record_type'] ) ) ); elseif( isset( $arrValues['record_type'] ) ) $this->setRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_type'] ) : $arrValues['record_type'] );
		if( isset( $arrValues['transaction_id_number'] ) && $boolDirectSet ) $this->set( 'm_strTransactionIdNumber', trim( stripcslashes( $arrValues['transaction_id_number'] ) ) ); elseif( isset( $arrValues['transaction_id_number'] ) ) $this->setTransactionIdNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['transaction_id_number'] ) : $arrValues['transaction_id_number'] );
		if( isset( $arrValues['first_name'] ) && $boolDirectSet ) $this->set( 'm_strFirstName', trim( stripcslashes( $arrValues['first_name'] ) ) ); elseif( isset( $arrValues['first_name'] ) ) $this->setFirstName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['first_name'] ) : $arrValues['first_name'] );
		if( isset( $arrValues['last_name'] ) && $boolDirectSet ) $this->set( 'm_strLastName', trim( stripcslashes( $arrValues['last_name'] ) ) ); elseif( isset( $arrValues['last_name'] ) ) $this->setLastName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['last_name'] ) : $arrValues['last_name'] );
		if( isset( $arrValues['address'] ) && $boolDirectSet ) $this->set( 'm_strAddress', trim( stripcslashes( $arrValues['address'] ) ) ); elseif( isset( $arrValues['address'] ) ) $this->setAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['address'] ) : $arrValues['address'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state_province'] ) && $boolDirectSet ) $this->set( 'm_strStateProvince', trim( stripcslashes( $arrValues['state_province'] ) ) ); elseif( isset( $arrValues['state_province'] ) ) $this->setStateProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_province'] ) : $arrValues['state_province'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['telephone'] ) && $boolDirectSet ) $this->set( 'm_strTelephone', trim( stripcslashes( $arrValues['telephone'] ) ) ); elseif( isset( $arrValues['telephone'] ) ) $this->setTelephone( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['telephone'] ) : $arrValues['telephone'] );
		if( isset( $arrValues['country'] ) && $boolDirectSet ) $this->set( 'm_strCountry', trim( stripcslashes( $arrValues['country'] ) ) ); elseif( isset( $arrValues['country'] ) ) $this->setCountry( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country'] ) : $arrValues['country'] );
		if( isset( $arrValues['mtcn'] ) && $boolDirectSet ) $this->set( 'm_strMtcn', trim( stripcslashes( $arrValues['mtcn'] ) ) ); elseif( isset( $arrValues['mtcn'] ) ) $this->setMtcn( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mtcn'] ) : $arrValues['mtcn'] );
		if( isset( $arrValues['agent_zip'] ) && $boolDirectSet ) $this->set( 'm_strAgentZip', trim( stripcslashes( $arrValues['agent_zip'] ) ) ); elseif( isset( $arrValues['agent_zip'] ) ) $this->setAgentZip( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['agent_zip'] ) : $arrValues['agent_zip'] );
		if( isset( $arrValues['payment_amount'] ) && $boolDirectSet ) $this->set( 'm_strPaymentAmount', trim( stripcslashes( $arrValues['payment_amount'] ) ) ); elseif( isset( $arrValues['payment_amount'] ) ) $this->setPaymentAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payment_amount'] ) : $arrValues['payment_amount'] );
		if( isset( $arrValues['transaction_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDatetime', trim( stripcslashes( $arrValues['transaction_datetime'] ) ) ); elseif( isset( $arrValues['transaction_datetime'] ) ) $this->setTransactionDatetime( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['transaction_datetime'] ) : $arrValues['transaction_datetime'] );
		if( isset( $arrValues['comment_text'] ) && $boolDirectSet ) $this->set( 'm_strCommentText', trim( stripcslashes( $arrValues['comment_text'] ) ) ); elseif( isset( $arrValues['comment_text'] ) ) $this->setCommentText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['comment_text'] ) : $arrValues['comment_text'] );
		if( isset( $arrValues['client_identifier'] ) && $boolDirectSet ) $this->set( 'm_strClientIdentifier', trim( stripcslashes( $arrValues['client_identifier'] ) ) ); elseif( isset( $arrValues['client_identifier'] ) ) $this->setClientIdentifier( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_identifier'] ) : $arrValues['client_identifier'] );
		if( isset( $arrValues['client_reference'] ) && $boolDirectSet ) $this->set( 'm_strClientReference', trim( stripcslashes( $arrValues['client_reference'] ) ) ); elseif( isset( $arrValues['client_reference'] ) ) $this->setClientReference( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_reference'] ) : $arrValues['client_reference'] );
		if( isset( $arrValues['customer_fee'] ) && $boolDirectSet ) $this->set( 'm_strCustomerFee', trim( stripcslashes( $arrValues['customer_fee'] ) ) ); elseif( isset( $arrValues['customer_fee'] ) ) $this->setCustomerFee( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_fee'] ) : $arrValues['customer_fee'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setFileId( $intFileId ) {
		$this->set( 'm_intFileId', CStrings::strToIntDef( $intFileId, NULL, false ) );
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function sqlFileId() {
		return ( true == isset( $this->m_intFileId ) ) ? ( string ) $this->m_intFileId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setRecordType( $strRecordType ) {
		$this->set( 'm_strRecordType', CStrings::strTrimDef( $strRecordType, 1, NULL, true ) );
	}

	public function getRecordType() {
		return $this->m_strRecordType;
	}

	public function sqlRecordType() {
		return ( true == isset( $this->m_strRecordType ) ) ? '\'' . addslashes( $this->m_strRecordType ) . '\'' : 'NULL';
	}

	public function setTransactionIdNumber( $strTransactionIdNumber ) {
		$this->set( 'm_strTransactionIdNumber', CStrings::strTrimDef( $strTransactionIdNumber, 24, NULL, true ) );
	}

	public function getTransactionIdNumber() {
		return $this->m_strTransactionIdNumber;
	}

	public function sqlTransactionIdNumber() {
		return ( true == isset( $this->m_strTransactionIdNumber ) ) ? '\'' . addslashes( $this->m_strTransactionIdNumber ) . '\'' : 'NULL';
	}

	public function setFirstName( $strFirstName ) {
		$this->set( 'm_strFirstName', CStrings::strTrimDef( $strFirstName, 33, NULL, true ) );
	}

	public function getFirstName() {
		return $this->m_strFirstName;
	}

	public function sqlFirstName() {
		return ( true == isset( $this->m_strFirstName ) ) ? '\'' . addslashes( $this->m_strFirstName ) . '\'' : 'NULL';
	}

	public function setLastName( $strLastName ) {
		$this->set( 'm_strLastName', CStrings::strTrimDef( $strLastName, 41, NULL, true ) );
	}

	public function getLastName() {
		return $this->m_strLastName;
	}

	public function sqlLastName() {
		return ( true == isset( $this->m_strLastName ) ) ? '\'' . addslashes( $this->m_strLastName ) . '\'' : 'NULL';
	}

	public function setAddress( $strAddress ) {
		$this->set( 'm_strAddress', CStrings::strTrimDef( $strAddress, 40, NULL, true ) );
	}

	public function getAddress() {
		return $this->m_strAddress;
	}

	public function sqlAddress() {
		return ( true == isset( $this->m_strAddress ) ) ? '\'' . addslashes( $this->m_strAddress ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 24, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setStateProvince( $strStateProvince ) {
		$this->set( 'm_strStateProvince', CStrings::strTrimDef( $strStateProvince, 2, NULL, true ) );
	}

	public function getStateProvince() {
		return $this->m_strStateProvince;
	}

	public function sqlStateProvince() {
		return ( true == isset( $this->m_strStateProvince ) ) ? '\'' . addslashes( $this->m_strStateProvince ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 9, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setTelephone( $strTelephone ) {
		$this->set( 'm_strTelephone', CStrings::strTrimDef( $strTelephone, 10, NULL, true ) );
	}

	public function getTelephone() {
		return $this->m_strTelephone;
	}

	public function sqlTelephone() {
		return ( true == isset( $this->m_strTelephone ) ) ? '\'' . addslashes( $this->m_strTelephone ) . '\'' : 'NULL';
	}

	public function setCountry( $strCountry ) {
		$this->set( 'm_strCountry', CStrings::strTrimDef( $strCountry, 3, NULL, true ) );
	}

	public function getCountry() {
		return $this->m_strCountry;
	}

	public function sqlCountry() {
		return ( true == isset( $this->m_strCountry ) ) ? '\'' . addslashes( $this->m_strCountry ) . '\'' : 'NULL';
	}

	public function setMtcn( $strMtcn ) {
		$this->set( 'm_strMtcn', CStrings::strTrimDef( $strMtcn, 16, NULL, true ) );
	}

	public function getMtcn() {
		return $this->m_strMtcn;
	}

	public function sqlMtcn() {
		return ( true == isset( $this->m_strMtcn ) ) ? '\'' . addslashes( $this->m_strMtcn ) . '\'' : 'NULL';
	}

	public function setAgentZip( $strAgentZip ) {
		$this->set( 'm_strAgentZip', CStrings::strTrimDef( $strAgentZip, 9, NULL, true ) );
	}

	public function getAgentZip() {
		return $this->m_strAgentZip;
	}

	public function sqlAgentZip() {
		return ( true == isset( $this->m_strAgentZip ) ) ? '\'' . addslashes( $this->m_strAgentZip ) . '\'' : 'NULL';
	}

	public function setPaymentAmount( $strPaymentAmount ) {
		$this->set( 'm_strPaymentAmount', CStrings::strTrimDef( $strPaymentAmount, 13, NULL, true ) );
	}

	public function getPaymentAmount() {
		return $this->m_strPaymentAmount;
	}

	public function sqlPaymentAmount() {
		return ( true == isset( $this->m_strPaymentAmount ) ) ? '\'' . addslashes( $this->m_strPaymentAmount ) . '\'' : 'NULL';
	}

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->set( 'm_strTransactionDatetime', CStrings::strTrimDef( $strTransactionDatetime, 24, NULL, true ) );
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function sqlTransactionDatetime() {
		return ( true == isset( $this->m_strTransactionDatetime ) ) ? '\'' . addslashes( $this->m_strTransactionDatetime ) . '\'' : 'NULL';
	}

	public function setCommentText( $strCommentText ) {
		$this->set( 'm_strCommentText', CStrings::strTrimDef( $strCommentText, 70, NULL, true ) );
	}

	public function getCommentText() {
		return $this->m_strCommentText;
	}

	public function sqlCommentText() {
		return ( true == isset( $this->m_strCommentText ) ) ? '\'' . addslashes( $this->m_strCommentText ) . '\'' : 'NULL';
	}

	public function setClientIdentifier( $strClientIdentifier ) {
		$this->set( 'm_strClientIdentifier', CStrings::strTrimDef( $strClientIdentifier, 9, NULL, true ) );
	}

	public function getClientIdentifier() {
		return $this->m_strClientIdentifier;
	}

	public function sqlClientIdentifier() {
		return ( true == isset( $this->m_strClientIdentifier ) ) ? '\'' . addslashes( $this->m_strClientIdentifier ) . '\'' : 'NULL';
	}

	public function setClientReference( $strClientReference ) {
		$this->set( 'm_strClientReference', CStrings::strTrimDef( $strClientReference, 24, NULL, true ) );
	}

	public function getClientReference() {
		return $this->m_strClientReference;
	}

	public function sqlClientReference() {
		return ( true == isset( $this->m_strClientReference ) ) ? '\'' . addslashes( $this->m_strClientReference ) . '\'' : 'NULL';
	}

	public function setCustomerFee( $strCustomerFee ) {
		$this->set( 'm_strCustomerFee', CStrings::strTrimDef( $strCustomerFee, 6, NULL, true ) );
	}

	public function getCustomerFee() {
		return $this->m_strCustomerFee;
	}

	public function sqlCustomerFee() {
		return ( true == isset( $this->m_strCustomerFee ) ) ? '\'' . addslashes( $this->m_strCustomerFee ) . '\'' : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, file_id, cid, ar_payment_id, property_id, customer_id, lease_id, record_type, transaction_id_number, first_name, last_name, address, city, state_province, postal_code, telephone, country, mtcn, agent_zip, payment_amount, transaction_datetime, comment_text, client_identifier, client_reference, customer_fee, processed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlFileId() . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlArPaymentId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlRecordType() . ', ' .
 						$this->sqlTransactionIdNumber() . ', ' .
 						$this->sqlFirstName() . ', ' .
 						$this->sqlLastName() . ', ' .
 						$this->sqlAddress() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlStateProvince() . ', ' .
 						$this->sqlPostalCode() . ', ' .
 						$this->sqlTelephone() . ', ' .
 						$this->sqlCountry() . ', ' .
 						$this->sqlMtcn() . ', ' .
 						$this->sqlAgentZip() . ', ' .
 						$this->sqlPaymentAmount() . ', ' .
 						$this->sqlTransactionDatetime() . ', ' .
 						$this->sqlCommentText() . ', ' .
 						$this->sqlClientIdentifier() . ', ' .
 						$this->sqlClientReference() . ', ' .
 						$this->sqlCustomerFee() . ', ' .
 						$this->sqlProcessedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; } elseif( true == array_key_exists( 'FileId', $this->getChangedColumns() ) ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; } elseif( true == array_key_exists( 'RecordType', $this->getChangedColumns() ) ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_id_number = ' . $this->sqlTransactionIdNumber() . ','; } elseif( true == array_key_exists( 'TransactionIdNumber', $this->getChangedColumns() ) ) { $strSql .= ' transaction_id_number = ' . $this->sqlTransactionIdNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_name = ' . $this->sqlFirstName() . ','; } elseif( true == array_key_exists( 'FirstName', $this->getChangedColumns() ) ) { $strSql .= ' first_name = ' . $this->sqlFirstName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_name = ' . $this->sqlLastName() . ','; } elseif( true == array_key_exists( 'LastName', $this->getChangedColumns() ) ) { $strSql .= ' last_name = ' . $this->sqlLastName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address = ' . $this->sqlAddress() . ','; } elseif( true == array_key_exists( 'Address', $this->getChangedColumns() ) ) { $strSql .= ' address = ' . $this->sqlAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_province = ' . $this->sqlStateProvince() . ','; } elseif( true == array_key_exists( 'StateProvince', $this->getChangedColumns() ) ) { $strSql .= ' state_province = ' . $this->sqlStateProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' telephone = ' . $this->sqlTelephone() . ','; } elseif( true == array_key_exists( 'Telephone', $this->getChangedColumns() ) ) { $strSql .= ' telephone = ' . $this->sqlTelephone() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country = ' . $this->sqlCountry() . ','; } elseif( true == array_key_exists( 'Country', $this->getChangedColumns() ) ) { $strSql .= ' country = ' . $this->sqlCountry() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mtcn = ' . $this->sqlMtcn() . ','; } elseif( true == array_key_exists( 'Mtcn', $this->getChangedColumns() ) ) { $strSql .= ' mtcn = ' . $this->sqlMtcn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agent_zip = ' . $this->sqlAgentZip() . ','; } elseif( true == array_key_exists( 'AgentZip', $this->getChangedColumns() ) ) { $strSql .= ' agent_zip = ' . $this->sqlAgentZip() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_amount = ' . $this->sqlPaymentAmount() . ','; } elseif( true == array_key_exists( 'PaymentAmount', $this->getChangedColumns() ) ) { $strSql .= ' payment_amount = ' . $this->sqlPaymentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; } elseif( true == array_key_exists( 'TransactionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' comment_text = ' . $this->sqlCommentText() . ','; } elseif( true == array_key_exists( 'CommentText', $this->getChangedColumns() ) ) { $strSql .= ' comment_text = ' . $this->sqlCommentText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_identifier = ' . $this->sqlClientIdentifier() . ','; } elseif( true == array_key_exists( 'ClientIdentifier', $this->getChangedColumns() ) ) { $strSql .= ' client_identifier = ' . $this->sqlClientIdentifier() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_reference = ' . $this->sqlClientReference() . ','; } elseif( true == array_key_exists( 'ClientReference', $this->getChangedColumns() ) ) { $strSql .= ' client_reference = ' . $this->sqlClientReference() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_fee = ' . $this->sqlCustomerFee() . ','; } elseif( true == array_key_exists( 'CustomerFee', $this->getChangedColumns() ) ) { $strSql .= ' customer_fee = ' . $this->sqlCustomerFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'file_id' => $this->getFileId(),
			'cid' => $this->getCid(),
			'ar_payment_id' => $this->getArPaymentId(),
			'property_id' => $this->getPropertyId(),
			'customer_id' => $this->getCustomerId(),
			'lease_id' => $this->getLeaseId(),
			'record_type' => $this->getRecordType(),
			'transaction_id_number' => $this->getTransactionIdNumber(),
			'first_name' => $this->getFirstName(),
			'last_name' => $this->getLastName(),
			'address' => $this->getAddress(),
			'city' => $this->getCity(),
			'state_province' => $this->getStateProvince(),
			'postal_code' => $this->getPostalCode(),
			'telephone' => $this->getTelephone(),
			'country' => $this->getCountry(),
			'mtcn' => $this->getMtcn(),
			'agent_zip' => $this->getAgentZip(),
			'payment_amount' => $this->getPaymentAmount(),
			'transaction_datetime' => $this->getTransactionDatetime(),
			'comment_text' => $this->getCommentText(),
			'client_identifier' => $this->getClientIdentifier(),
			'client_reference' => $this->getClientReference(),
			'customer_fee' => $this->getCustomerFee(),
			'processed_on' => $this->getProcessedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>