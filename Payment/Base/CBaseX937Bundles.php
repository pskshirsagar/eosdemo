<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937Bundles
 * Do not add any new functions to this class.
 */

class CBaseX937Bundles extends CEosPluralBase {

	/**
	 * @return CX937Bundle[]
	 */
	public static function fetchX937Bundles( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CX937Bundle::class, $objDatabase );
	}

	/**
	 * @return CX937Bundle
	 */
	public static function fetchX937Bundle( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CX937Bundle::class, $objDatabase );
	}

	public static function fetchX937BundleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'x937_bundles', $objDatabase );
	}

	public static function fetchX937BundleById( $intId, $objDatabase ) {
		return self::fetchX937Bundle( sprintf( 'SELECT * FROM x937_bundles WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchX937BundlesByCid( $intCid, $objDatabase ) {
		return self::fetchX937Bundles( sprintf( 'SELECT * FROM x937_bundles WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchX937BundlesByX937FileId( $intX937FileId, $objDatabase ) {
		return self::fetchX937Bundles( sprintf( 'SELECT * FROM x937_bundles WHERE x937_file_id = %d', ( int ) $intX937FileId ), $objDatabase );
	}

	public static function fetchX937BundlesByX937CashLetterId( $intX937CashLetterId, $objDatabase ) {
		return self::fetchX937Bundles( sprintf( 'SELECT * FROM x937_bundles WHERE x937_cash_letter_id = %d', ( int ) $intX937CashLetterId ), $objDatabase );
	}

	public static function fetchX937BundlesByHeaderId( $strHeaderId, $objDatabase ) {
		return self::fetchX937Bundles( sprintf( 'SELECT * FROM x937_bundles WHERE header_id = \'%s\'', $strHeaderId ), $objDatabase );
	}

}
?>