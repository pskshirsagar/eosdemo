<?php

class CBaseX937ViewDetailRecord extends CEosSingularBase {

	const TABLE_NAME = 'public.x937_view_detail_records';

	protected $m_intId;
	protected $m_intX937CheckDetailRecordId;
	protected $m_strRecordType;
	protected $m_strImageIndicator;
	protected $m_strImageCreatorRoutingNumber;
	protected $m_strImageCreatorDate;
	protected $m_strImageViewFormatIndicator;
	protected $m_strImageViewCompressionAlgorithmIdentifier;
	protected $m_strImageViewDataSize;
	protected $m_strViewSideIndicator;
	protected $m_strViewDescriptor;
	protected $m_strDigitalSignatureIndicator;
	protected $m_strDigitalSignatureMethod;
	protected $m_strSecurityKeySize;
	protected $m_strStartOfProtectedData;
	protected $m_strLengthOfProtectedData;
	protected $m_strImageRecreateIndicator;
	protected $m_strUserField;
	protected $m_strReserved;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['x937_check_detail_record_id'] ) && $boolDirectSet ) $this->set( 'm_intX937CheckDetailRecordId', trim( $arrValues['x937_check_detail_record_id'] ) ); elseif( isset( $arrValues['x937_check_detail_record_id'] ) ) $this->setX937CheckDetailRecordId( $arrValues['x937_check_detail_record_id'] );
		if( isset( $arrValues['record_type'] ) && $boolDirectSet ) $this->set( 'm_strRecordType', trim( stripcslashes( $arrValues['record_type'] ) ) ); elseif( isset( $arrValues['record_type'] ) ) $this->setRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_type'] ) : $arrValues['record_type'] );
		if( isset( $arrValues['image_indicator'] ) && $boolDirectSet ) $this->set( 'm_strImageIndicator', trim( stripcslashes( $arrValues['image_indicator'] ) ) ); elseif( isset( $arrValues['image_indicator'] ) ) $this->setImageIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_indicator'] ) : $arrValues['image_indicator'] );
		if( isset( $arrValues['image_creator_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strImageCreatorRoutingNumber', trim( stripcslashes( $arrValues['image_creator_routing_number'] ) ) ); elseif( isset( $arrValues['image_creator_routing_number'] ) ) $this->setImageCreatorRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_creator_routing_number'] ) : $arrValues['image_creator_routing_number'] );
		if( isset( $arrValues['image_creator_date'] ) && $boolDirectSet ) $this->set( 'm_strImageCreatorDate', trim( stripcslashes( $arrValues['image_creator_date'] ) ) ); elseif( isset( $arrValues['image_creator_date'] ) ) $this->setImageCreatorDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_creator_date'] ) : $arrValues['image_creator_date'] );
		if( isset( $arrValues['image_view_format_indicator'] ) && $boolDirectSet ) $this->set( 'm_strImageViewFormatIndicator', trim( stripcslashes( $arrValues['image_view_format_indicator'] ) ) ); elseif( isset( $arrValues['image_view_format_indicator'] ) ) $this->setImageViewFormatIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_view_format_indicator'] ) : $arrValues['image_view_format_indicator'] );
		if( isset( $arrValues['image_view_compression_algorithm_identifier'] ) && $boolDirectSet ) $this->set( 'm_strImageViewCompressionAlgorithmIdentifier', trim( stripcslashes( $arrValues['image_view_compression_algorithm_identifier'] ) ) ); elseif( isset( $arrValues['image_view_compression_algorithm_identifier'] ) ) $this->setImageViewCompressionAlgorithmIdentifier( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_view_compression_algorithm_identifier'] ) : $arrValues['image_view_compression_algorithm_identifier'] );
		if( isset( $arrValues['image_view_data_size'] ) && $boolDirectSet ) $this->set( 'm_strImageViewDataSize', trim( stripcslashes( $arrValues['image_view_data_size'] ) ) ); elseif( isset( $arrValues['image_view_data_size'] ) ) $this->setImageViewDataSize( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_view_data_size'] ) : $arrValues['image_view_data_size'] );
		if( isset( $arrValues['view_side_indicator'] ) && $boolDirectSet ) $this->set( 'm_strViewSideIndicator', trim( stripcslashes( $arrValues['view_side_indicator'] ) ) ); elseif( isset( $arrValues['view_side_indicator'] ) ) $this->setViewSideIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['view_side_indicator'] ) : $arrValues['view_side_indicator'] );
		if( isset( $arrValues['view_descriptor'] ) && $boolDirectSet ) $this->set( 'm_strViewDescriptor', trim( stripcslashes( $arrValues['view_descriptor'] ) ) ); elseif( isset( $arrValues['view_descriptor'] ) ) $this->setViewDescriptor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['view_descriptor'] ) : $arrValues['view_descriptor'] );
		if( isset( $arrValues['digital_signature_indicator'] ) && $boolDirectSet ) $this->set( 'm_strDigitalSignatureIndicator', trim( stripcslashes( $arrValues['digital_signature_indicator'] ) ) ); elseif( isset( $arrValues['digital_signature_indicator'] ) ) $this->setDigitalSignatureIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['digital_signature_indicator'] ) : $arrValues['digital_signature_indicator'] );
		if( isset( $arrValues['digital_signature_method'] ) && $boolDirectSet ) $this->set( 'm_strDigitalSignatureMethod', trim( stripcslashes( $arrValues['digital_signature_method'] ) ) ); elseif( isset( $arrValues['digital_signature_method'] ) ) $this->setDigitalSignatureMethod( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['digital_signature_method'] ) : $arrValues['digital_signature_method'] );
		if( isset( $arrValues['security_key_size'] ) && $boolDirectSet ) $this->set( 'm_strSecurityKeySize', trim( stripcslashes( $arrValues['security_key_size'] ) ) ); elseif( isset( $arrValues['security_key_size'] ) ) $this->setSecurityKeySize( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['security_key_size'] ) : $arrValues['security_key_size'] );
		if( isset( $arrValues['start_of_protected_data'] ) && $boolDirectSet ) $this->set( 'm_strStartOfProtectedData', trim( stripcslashes( $arrValues['start_of_protected_data'] ) ) ); elseif( isset( $arrValues['start_of_protected_data'] ) ) $this->setStartOfProtectedData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['start_of_protected_data'] ) : $arrValues['start_of_protected_data'] );
		if( isset( $arrValues['length_of_protected_data'] ) && $boolDirectSet ) $this->set( 'm_strLengthOfProtectedData', trim( stripcslashes( $arrValues['length_of_protected_data'] ) ) ); elseif( isset( $arrValues['length_of_protected_data'] ) ) $this->setLengthOfProtectedData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['length_of_protected_data'] ) : $arrValues['length_of_protected_data'] );
		if( isset( $arrValues['image_recreate_indicator'] ) && $boolDirectSet ) $this->set( 'm_strImageRecreateIndicator', trim( stripcslashes( $arrValues['image_recreate_indicator'] ) ) ); elseif( isset( $arrValues['image_recreate_indicator'] ) ) $this->setImageRecreateIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_recreate_indicator'] ) : $arrValues['image_recreate_indicator'] );
		if( isset( $arrValues['user_field'] ) && $boolDirectSet ) $this->set( 'm_strUserField', trim( stripcslashes( $arrValues['user_field'] ) ) ); elseif( isset( $arrValues['user_field'] ) ) $this->setUserField( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['user_field'] ) : $arrValues['user_field'] );
		if( isset( $arrValues['reserved'] ) && $boolDirectSet ) $this->set( 'm_strReserved', trim( stripcslashes( $arrValues['reserved'] ) ) ); elseif( isset( $arrValues['reserved'] ) ) $this->setReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reserved'] ) : $arrValues['reserved'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setX937CheckDetailRecordId( $intX937CheckDetailRecordId ) {
		$this->set( 'm_intX937CheckDetailRecordId', CStrings::strToIntDef( $intX937CheckDetailRecordId, NULL, false ) );
	}

	public function getX937CheckDetailRecordId() {
		return $this->m_intX937CheckDetailRecordId;
	}

	public function sqlX937CheckDetailRecordId() {
		return ( true == isset( $this->m_intX937CheckDetailRecordId ) ) ? ( string ) $this->m_intX937CheckDetailRecordId : 'NULL';
	}

	public function setRecordType( $strRecordType ) {
		$this->set( 'm_strRecordType', CStrings::strTrimDef( $strRecordType, 2, NULL, true ) );
	}

	public function getRecordType() {
		return $this->m_strRecordType;
	}

	public function sqlRecordType() {
		return ( true == isset( $this->m_strRecordType ) ) ? '\'' . addslashes( $this->m_strRecordType ) . '\'' : 'NULL';
	}

	public function setImageIndicator( $strImageIndicator ) {
		$this->set( 'm_strImageIndicator', CStrings::strTrimDef( $strImageIndicator, 1, NULL, true ) );
	}

	public function getImageIndicator() {
		return $this->m_strImageIndicator;
	}

	public function sqlImageIndicator() {
		return ( true == isset( $this->m_strImageIndicator ) ) ? '\'' . addslashes( $this->m_strImageIndicator ) . '\'' : 'NULL';
	}

	public function setImageCreatorRoutingNumber( $strImageCreatorRoutingNumber ) {
		$this->set( 'm_strImageCreatorRoutingNumber', CStrings::strTrimDef( $strImageCreatorRoutingNumber, 9, NULL, true ) );
	}

	public function getImageCreatorRoutingNumber() {
		return $this->m_strImageCreatorRoutingNumber;
	}

	public function sqlImageCreatorRoutingNumber() {
		return ( true == isset( $this->m_strImageCreatorRoutingNumber ) ) ? '\'' . addslashes( $this->m_strImageCreatorRoutingNumber ) . '\'' : 'NULL';
	}

	public function setImageCreatorDate( $strImageCreatorDate ) {
		$this->set( 'm_strImageCreatorDate', CStrings::strTrimDef( $strImageCreatorDate, 8, NULL, true ) );
	}

	public function getImageCreatorDate() {
		return $this->m_strImageCreatorDate;
	}

	public function sqlImageCreatorDate() {
		return ( true == isset( $this->m_strImageCreatorDate ) ) ? '\'' . addslashes( $this->m_strImageCreatorDate ) . '\'' : 'NULL';
	}

	public function setImageViewFormatIndicator( $strImageViewFormatIndicator ) {
		$this->set( 'm_strImageViewFormatIndicator', CStrings::strTrimDef( $strImageViewFormatIndicator, 2, NULL, true ) );
	}

	public function getImageViewFormatIndicator() {
		return $this->m_strImageViewFormatIndicator;
	}

	public function sqlImageViewFormatIndicator() {
		return ( true == isset( $this->m_strImageViewFormatIndicator ) ) ? '\'' . addslashes( $this->m_strImageViewFormatIndicator ) . '\'' : 'NULL';
	}

	public function setImageViewCompressionAlgorithmIdentifier( $strImageViewCompressionAlgorithmIdentifier ) {
		$this->set( 'm_strImageViewCompressionAlgorithmIdentifier', CStrings::strTrimDef( $strImageViewCompressionAlgorithmIdentifier, 2, NULL, true ) );
	}

	public function getImageViewCompressionAlgorithmIdentifier() {
		return $this->m_strImageViewCompressionAlgorithmIdentifier;
	}

	public function sqlImageViewCompressionAlgorithmIdentifier() {
		return ( true == isset( $this->m_strImageViewCompressionAlgorithmIdentifier ) ) ? '\'' . addslashes( $this->m_strImageViewCompressionAlgorithmIdentifier ) . '\'' : 'NULL';
	}

	public function setImageViewDataSize( $strImageViewDataSize ) {
		$this->set( 'm_strImageViewDataSize', CStrings::strTrimDef( $strImageViewDataSize, 7, NULL, true ) );
	}

	public function getImageViewDataSize() {
		return $this->m_strImageViewDataSize;
	}

	public function sqlImageViewDataSize() {
		return ( true == isset( $this->m_strImageViewDataSize ) ) ? '\'' . addslashes( $this->m_strImageViewDataSize ) . '\'' : 'NULL';
	}

	public function setViewSideIndicator( $strViewSideIndicator ) {
		$this->set( 'm_strViewSideIndicator', CStrings::strTrimDef( $strViewSideIndicator, 1, NULL, true ) );
	}

	public function getViewSideIndicator() {
		return $this->m_strViewSideIndicator;
	}

	public function sqlViewSideIndicator() {
		return ( true == isset( $this->m_strViewSideIndicator ) ) ? '\'' . addslashes( $this->m_strViewSideIndicator ) . '\'' : 'NULL';
	}

	public function setViewDescriptor( $strViewDescriptor ) {
		$this->set( 'm_strViewDescriptor', CStrings::strTrimDef( $strViewDescriptor, 2, NULL, true ) );
	}

	public function getViewDescriptor() {
		return $this->m_strViewDescriptor;
	}

	public function sqlViewDescriptor() {
		return ( true == isset( $this->m_strViewDescriptor ) ) ? '\'' . addslashes( $this->m_strViewDescriptor ) . '\'' : 'NULL';
	}

	public function setDigitalSignatureIndicator( $strDigitalSignatureIndicator ) {
		$this->set( 'm_strDigitalSignatureIndicator', CStrings::strTrimDef( $strDigitalSignatureIndicator, 1, NULL, true ) );
	}

	public function getDigitalSignatureIndicator() {
		return $this->m_strDigitalSignatureIndicator;
	}

	public function sqlDigitalSignatureIndicator() {
		return ( true == isset( $this->m_strDigitalSignatureIndicator ) ) ? '\'' . addslashes( $this->m_strDigitalSignatureIndicator ) . '\'' : 'NULL';
	}

	public function setDigitalSignatureMethod( $strDigitalSignatureMethod ) {
		$this->set( 'm_strDigitalSignatureMethod', CStrings::strTrimDef( $strDigitalSignatureMethod, 2, NULL, true ) );
	}

	public function getDigitalSignatureMethod() {
		return $this->m_strDigitalSignatureMethod;
	}

	public function sqlDigitalSignatureMethod() {
		return ( true == isset( $this->m_strDigitalSignatureMethod ) ) ? '\'' . addslashes( $this->m_strDigitalSignatureMethod ) . '\'' : 'NULL';
	}

	public function setSecurityKeySize( $strSecurityKeySize ) {
		$this->set( 'm_strSecurityKeySize', CStrings::strTrimDef( $strSecurityKeySize, 5, NULL, true ) );
	}

	public function getSecurityKeySize() {
		return $this->m_strSecurityKeySize;
	}

	public function sqlSecurityKeySize() {
		return ( true == isset( $this->m_strSecurityKeySize ) ) ? '\'' . addslashes( $this->m_strSecurityKeySize ) . '\'' : 'NULL';
	}

	public function setStartOfProtectedData( $strStartOfProtectedData ) {
		$this->set( 'm_strStartOfProtectedData', CStrings::strTrimDef( $strStartOfProtectedData, 7, NULL, true ) );
	}

	public function getStartOfProtectedData() {
		return $this->m_strStartOfProtectedData;
	}

	public function sqlStartOfProtectedData() {
		return ( true == isset( $this->m_strStartOfProtectedData ) ) ? '\'' . addslashes( $this->m_strStartOfProtectedData ) . '\'' : 'NULL';
	}

	public function setLengthOfProtectedData( $strLengthOfProtectedData ) {
		$this->set( 'm_strLengthOfProtectedData', CStrings::strTrimDef( $strLengthOfProtectedData, 7, NULL, true ) );
	}

	public function getLengthOfProtectedData() {
		return $this->m_strLengthOfProtectedData;
	}

	public function sqlLengthOfProtectedData() {
		return ( true == isset( $this->m_strLengthOfProtectedData ) ) ? '\'' . addslashes( $this->m_strLengthOfProtectedData ) . '\'' : 'NULL';
	}

	public function setImageRecreateIndicator( $strImageRecreateIndicator ) {
		$this->set( 'm_strImageRecreateIndicator', CStrings::strTrimDef( $strImageRecreateIndicator, 1, NULL, true ) );
	}

	public function getImageRecreateIndicator() {
		return $this->m_strImageRecreateIndicator;
	}

	public function sqlImageRecreateIndicator() {
		return ( true == isset( $this->m_strImageRecreateIndicator ) ) ? '\'' . addslashes( $this->m_strImageRecreateIndicator ) . '\'' : 'NULL';
	}

	public function setUserField( $strUserField ) {
		$this->set( 'm_strUserField', CStrings::strTrimDef( $strUserField, 8, NULL, true ) );
	}

	public function getUserField() {
		return $this->m_strUserField;
	}

	public function sqlUserField() {
		return ( true == isset( $this->m_strUserField ) ) ? '\'' . addslashes( $this->m_strUserField ) . '\'' : 'NULL';
	}

	public function setReserved( $strReserved ) {
		$this->set( 'm_strReserved', CStrings::strTrimDef( $strReserved, 15, NULL, true ) );
	}

	public function getReserved() {
		return $this->m_strReserved;
	}

	public function sqlReserved() {
		return ( true == isset( $this->m_strReserved ) ) ? '\'' . addslashes( $this->m_strReserved ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, x937_check_detail_record_id, record_type, image_indicator, image_creator_routing_number, image_creator_date, image_view_format_indicator, image_view_compression_algorithm_identifier, image_view_data_size, view_side_indicator, view_descriptor, digital_signature_indicator, digital_signature_method, security_key_size, start_of_protected_data, length_of_protected_data, image_recreate_indicator, user_field, reserved, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlX937CheckDetailRecordId() . ', ' .
 						$this->sqlRecordType() . ', ' .
 						$this->sqlImageIndicator() . ', ' .
 						$this->sqlImageCreatorRoutingNumber() . ', ' .
 						$this->sqlImageCreatorDate() . ', ' .
 						$this->sqlImageViewFormatIndicator() . ', ' .
 						$this->sqlImageViewCompressionAlgorithmIdentifier() . ', ' .
 						$this->sqlImageViewDataSize() . ', ' .
 						$this->sqlViewSideIndicator() . ', ' .
 						$this->sqlViewDescriptor() . ', ' .
 						$this->sqlDigitalSignatureIndicator() . ', ' .
 						$this->sqlDigitalSignatureMethod() . ', ' .
 						$this->sqlSecurityKeySize() . ', ' .
 						$this->sqlStartOfProtectedData() . ', ' .
 						$this->sqlLengthOfProtectedData() . ', ' .
 						$this->sqlImageRecreateIndicator() . ', ' .
 						$this->sqlUserField() . ', ' .
 						$this->sqlReserved() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_check_detail_record_id = ' . $this->sqlX937CheckDetailRecordId() . ','; } elseif( true == array_key_exists( 'X937CheckDetailRecordId', $this->getChangedColumns() ) ) { $strSql .= ' x937_check_detail_record_id = ' . $this->sqlX937CheckDetailRecordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; } elseif( true == array_key_exists( 'RecordType', $this->getChangedColumns() ) ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_indicator = ' . $this->sqlImageIndicator() . ','; } elseif( true == array_key_exists( 'ImageIndicator', $this->getChangedColumns() ) ) { $strSql .= ' image_indicator = ' . $this->sqlImageIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_creator_routing_number = ' . $this->sqlImageCreatorRoutingNumber() . ','; } elseif( true == array_key_exists( 'ImageCreatorRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' image_creator_routing_number = ' . $this->sqlImageCreatorRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_creator_date = ' . $this->sqlImageCreatorDate() . ','; } elseif( true == array_key_exists( 'ImageCreatorDate', $this->getChangedColumns() ) ) { $strSql .= ' image_creator_date = ' . $this->sqlImageCreatorDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_view_format_indicator = ' . $this->sqlImageViewFormatIndicator() . ','; } elseif( true == array_key_exists( 'ImageViewFormatIndicator', $this->getChangedColumns() ) ) { $strSql .= ' image_view_format_indicator = ' . $this->sqlImageViewFormatIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_view_compression_algorithm_identifier = ' . $this->sqlImageViewCompressionAlgorithmIdentifier() . ','; } elseif( true == array_key_exists( 'ImageViewCompressionAlgorithmIdentifier', $this->getChangedColumns() ) ) { $strSql .= ' image_view_compression_algorithm_identifier = ' . $this->sqlImageViewCompressionAlgorithmIdentifier() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_view_data_size = ' . $this->sqlImageViewDataSize() . ','; } elseif( true == array_key_exists( 'ImageViewDataSize', $this->getChangedColumns() ) ) { $strSql .= ' image_view_data_size = ' . $this->sqlImageViewDataSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' view_side_indicator = ' . $this->sqlViewSideIndicator() . ','; } elseif( true == array_key_exists( 'ViewSideIndicator', $this->getChangedColumns() ) ) { $strSql .= ' view_side_indicator = ' . $this->sqlViewSideIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' view_descriptor = ' . $this->sqlViewDescriptor() . ','; } elseif( true == array_key_exists( 'ViewDescriptor', $this->getChangedColumns() ) ) { $strSql .= ' view_descriptor = ' . $this->sqlViewDescriptor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' digital_signature_indicator = ' . $this->sqlDigitalSignatureIndicator() . ','; } elseif( true == array_key_exists( 'DigitalSignatureIndicator', $this->getChangedColumns() ) ) { $strSql .= ' digital_signature_indicator = ' . $this->sqlDigitalSignatureIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' digital_signature_method = ' . $this->sqlDigitalSignatureMethod() . ','; } elseif( true == array_key_exists( 'DigitalSignatureMethod', $this->getChangedColumns() ) ) { $strSql .= ' digital_signature_method = ' . $this->sqlDigitalSignatureMethod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' security_key_size = ' . $this->sqlSecurityKeySize() . ','; } elseif( true == array_key_exists( 'SecurityKeySize', $this->getChangedColumns() ) ) { $strSql .= ' security_key_size = ' . $this->sqlSecurityKeySize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_of_protected_data = ' . $this->sqlStartOfProtectedData() . ','; } elseif( true == array_key_exists( 'StartOfProtectedData', $this->getChangedColumns() ) ) { $strSql .= ' start_of_protected_data = ' . $this->sqlStartOfProtectedData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' length_of_protected_data = ' . $this->sqlLengthOfProtectedData() . ','; } elseif( true == array_key_exists( 'LengthOfProtectedData', $this->getChangedColumns() ) ) { $strSql .= ' length_of_protected_data = ' . $this->sqlLengthOfProtectedData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_recreate_indicator = ' . $this->sqlImageRecreateIndicator() . ','; } elseif( true == array_key_exists( 'ImageRecreateIndicator', $this->getChangedColumns() ) ) { $strSql .= ' image_recreate_indicator = ' . $this->sqlImageRecreateIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_field = ' . $this->sqlUserField() . ','; } elseif( true == array_key_exists( 'UserField', $this->getChangedColumns() ) ) { $strSql .= ' user_field = ' . $this->sqlUserField() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reserved = ' . $this->sqlReserved() . ','; } elseif( true == array_key_exists( 'Reserved', $this->getChangedColumns() ) ) { $strSql .= ' reserved = ' . $this->sqlReserved() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'x937_check_detail_record_id' => $this->getX937CheckDetailRecordId(),
			'record_type' => $this->getRecordType(),
			'image_indicator' => $this->getImageIndicator(),
			'image_creator_routing_number' => $this->getImageCreatorRoutingNumber(),
			'image_creator_date' => $this->getImageCreatorDate(),
			'image_view_format_indicator' => $this->getImageViewFormatIndicator(),
			'image_view_compression_algorithm_identifier' => $this->getImageViewCompressionAlgorithmIdentifier(),
			'image_view_data_size' => $this->getImageViewDataSize(),
			'view_side_indicator' => $this->getViewSideIndicator(),
			'view_descriptor' => $this->getViewDescriptor(),
			'digital_signature_indicator' => $this->getDigitalSignatureIndicator(),
			'digital_signature_method' => $this->getDigitalSignatureMethod(),
			'security_key_size' => $this->getSecurityKeySize(),
			'start_of_protected_data' => $this->getStartOfProtectedData(),
			'length_of_protected_data' => $this->getLengthOfProtectedData(),
			'image_recreate_indicator' => $this->getImageRecreateIndicator(),
			'user_field' => $this->getUserField(),
			'reserved' => $this->getReserved(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>