<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CGiactRequestLogs
 * Do not add any new functions to this class.
 */

class CBaseGiactRequestLogs extends CEosPluralBase {

	/**
	 * @return CGiactRequestLog[]
	 */
	public static function fetchGiactRequestLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGiactRequestLog::class, $objDatabase );
	}

	/**
	 * @return CGiactRequestLog
	 */
	public static function fetchGiactRequestLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGiactRequestLog::class, $objDatabase );
	}

	public static function fetchGiactRequestLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'giact_request_logs', $objDatabase );
	}

	public static function fetchGiactRequestLogById( $intId, $objDatabase ) {
		return self::fetchGiactRequestLog( sprintf( 'SELECT * FROM giact_request_logs WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchGiactRequestLogsByProductId( $intProductId, $objDatabase ) {
		return self::fetchGiactRequestLogs( sprintf( 'SELECT * FROM giact_request_logs WHERE product_id = %d', $intProductId ), $objDatabase );
	}

	public static function fetchGiactRequestLogsByPsProductOptionId( $intPsProductOptionId, $objDatabase ) {
		return self::fetchGiactRequestLogs( sprintf( 'SELECT * FROM giact_request_logs WHERE ps_product_option_id = %d', $intPsProductOptionId ), $objDatabase );
	}

	public static function fetchGiactRequestLogsByGiactReferenceId( $strGiactReferenceId, $objDatabase ) {
		return self::fetchGiactRequestLogs( sprintf( 'SELECT * FROM giact_request_logs WHERE giact_reference_id = \'%s\'', $strGiactReferenceId ), $objDatabase );
	}

}
?>