<?php

class CBasePaymentFile extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.payment_files';

	protected $m_intId;
	protected $m_intPaymentFileTypeId;
	protected $m_intRemoteFileId;
	protected $m_intProcessingBankId;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strFileContainer;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strProcessedOn;
	protected $m_strFileDownloadedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['payment_file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentFileTypeId', trim( $arrValues['payment_file_type_id'] ) ); elseif( isset( $arrValues['payment_file_type_id'] ) ) $this->setPaymentFileTypeId( $arrValues['payment_file_type_id'] );
		if( isset( $arrValues['remote_file_id'] ) && $boolDirectSet ) $this->set( 'm_intRemoteFileId', trim( $arrValues['remote_file_id'] ) ); elseif( isset( $arrValues['remote_file_id'] ) ) $this->setRemoteFileId( $arrValues['remote_file_id'] );
		if( isset( $arrValues['processing_bank_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankId', trim( $arrValues['processing_bank_id'] ) ); elseif( isset( $arrValues['processing_bank_id'] ) ) $this->setProcessingBankId( $arrValues['processing_bank_id'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( $arrValues['file_name'] ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( $arrValues['file_path'] ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( $arrValues['file_path'] );
		if( isset( $arrValues['file_container'] ) && $boolDirectSet ) $this->set( 'm_strFileContainer', trim( $arrValues['file_container'] ) ); elseif( isset( $arrValues['file_container'] ) ) $this->setFileContainer( $arrValues['file_container'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['file_downloaded_on'] ) && $boolDirectSet ) $this->set( 'm_strFileDownloadedOn', trim( $arrValues['file_downloaded_on'] ) ); elseif( isset( $arrValues['file_downloaded_on'] ) ) $this->setFileDownloadedOn( $arrValues['file_downloaded_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPaymentFileTypeId( $intPaymentFileTypeId ) {
		$this->set( 'm_intPaymentFileTypeId', CStrings::strToIntDef( $intPaymentFileTypeId, NULL, false ) );
	}

	public function getPaymentFileTypeId() {
		return $this->m_intPaymentFileTypeId;
	}

	public function sqlPaymentFileTypeId() {
		return ( true == isset( $this->m_intPaymentFileTypeId ) ) ? ( string ) $this->m_intPaymentFileTypeId : 'NULL';
	}

	public function setRemoteFileId( $intRemoteFileId ) {
		$this->set( 'm_intRemoteFileId', CStrings::strToIntDef( $intRemoteFileId, NULL, false ) );
	}

	public function getRemoteFileId() {
		return $this->m_intRemoteFileId;
	}

	public function sqlRemoteFileId() {
		return ( true == isset( $this->m_intRemoteFileId ) ) ? ( string ) $this->m_intRemoteFileId : 'NULL';
	}

	public function setProcessingBankId( $intProcessingBankId ) {
		$this->set( 'm_intProcessingBankId', CStrings::strToIntDef( $intProcessingBankId, NULL, false ) );
	}

	public function getProcessingBankId() {
		return $this->m_intProcessingBankId;
	}

	public function sqlProcessingBankId() {
		return ( true == isset( $this->m_intProcessingBankId ) ) ? ( string ) $this->m_intProcessingBankId : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 500, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileName ) : '\'' . addslashes( $this->m_strFileName ) . '\'' ) : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 500, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFilePath ) : '\'' . addslashes( $this->m_strFilePath ) . '\'' ) : 'NULL';
	}

	public function setFileContainer( $strFileContainer ) {
		$this->set( 'm_strFileContainer', CStrings::strTrimDef( $strFileContainer, 50, NULL, true ) );
	}

	public function getFileContainer() {
		return $this->m_strFileContainer;
	}

	public function sqlFileContainer() {
		return ( true == isset( $this->m_strFileContainer ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileContainer ) : '\'' . addslashes( $this->m_strFileContainer ) . '\'' ) : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setFileDownloadedOn( $strFileDownloadedOn ) {
		$this->set( 'm_strFileDownloadedOn', CStrings::strTrimDef( $strFileDownloadedOn, -1, NULL, true ) );
	}

	public function getFileDownloadedOn() {
		return $this->m_strFileDownloadedOn;
	}

	public function sqlFileDownloadedOn() {
		return ( true == isset( $this->m_strFileDownloadedOn ) ) ? '\'' . $this->m_strFileDownloadedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, payment_file_type_id, remote_file_id, processing_bank_id, file_name, file_path, file_container, details, processed_on, file_downloaded_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPaymentFileTypeId() . ', ' .
						$this->sqlRemoteFileId() . ', ' .
						$this->sqlProcessingBankId() . ', ' .
						$this->sqlFileName() . ', ' .
						$this->sqlFilePath() . ', ' .
						$this->sqlFileContainer() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlProcessedOn() . ', ' .
						$this->sqlFileDownloadedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_file_type_id = ' . $this->sqlPaymentFileTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentFileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_file_type_id = ' . $this->sqlPaymentFileTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_file_id = ' . $this->sqlRemoteFileId(). ',' ; } elseif( true == array_key_exists( 'RemoteFileId', $this->getChangedColumns() ) ) { $strSql .= ' remote_file_id = ' . $this->sqlRemoteFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId(). ',' ; } elseif( true == array_key_exists( 'ProcessingBankId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath(). ',' ; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_container = ' . $this->sqlFileContainer(). ',' ; } elseif( true == array_key_exists( 'FileContainer', $this->getChangedColumns() ) ) { $strSql .= ' file_container = ' . $this->sqlFileContainer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn(). ',' ; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_downloaded_on = ' . $this->sqlFileDownloadedOn(). ',' ; } elseif( true == array_key_exists( 'FileDownloadedOn', $this->getChangedColumns() ) ) { $strSql .= ' file_downloaded_on = ' . $this->sqlFileDownloadedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'payment_file_type_id' => $this->getPaymentFileTypeId(),
			'remote_file_id' => $this->getRemoteFileId(),
			'processing_bank_id' => $this->getProcessingBankId(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'file_container' => $this->getFileContainer(),
			'details' => $this->getDetails(),
			'processed_on' => $this->getProcessedOn(),
			'file_downloaded_on' => $this->getFileDownloadedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>