<?php

class CBaseEftBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.eft_batches';

	protected $m_intId;
	protected $m_intProcessingBankId;
	protected $m_intProcessingBankAccountId;
	protected $m_strGatewayUsernameEncrypted;
	protected $m_intMerchantGatewayId;
	protected $m_intEftBatchTypeId;
	protected $m_intEftBatchStatusTypeId;
	protected $m_intAggregateEftBatchId;
	protected $m_intNachaFileId;
	protected $m_intX937FileId;
	protected $m_strBatchDatetime;
	protected $m_strScheduledProcessDatetime;
	protected $m_fltBatchAmount;
	protected $m_fltConfirmationAmount;
	protected $m_strBatchMemo;
	protected $m_strSettlementDate;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strProcessedOn;
	protected $m_strConfirmedOn;
	protected $m_intIsReconPrepped;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsReconPrepped = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['processing_bank_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankId', trim( $arrValues['processing_bank_id'] ) ); elseif( isset( $arrValues['processing_bank_id'] ) ) $this->setProcessingBankId( $arrValues['processing_bank_id'] );
		if( isset( $arrValues['processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankAccountId', trim( $arrValues['processing_bank_account_id'] ) ); elseif( isset( $arrValues['processing_bank_account_id'] ) ) $this->setProcessingBankAccountId( $arrValues['processing_bank_account_id'] );
		if( isset( $arrValues['gateway_username_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strGatewayUsernameEncrypted', trim( stripcslashes( $arrValues['gateway_username_encrypted'] ) ) ); elseif( isset( $arrValues['gateway_username_encrypted'] ) ) $this->setGatewayUsernameEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gateway_username_encrypted'] ) : $arrValues['gateway_username_encrypted'] );
		if( isset( $arrValues['merchant_gateway_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantGatewayId', trim( $arrValues['merchant_gateway_id'] ) ); elseif( isset( $arrValues['merchant_gateway_id'] ) ) $this->setMerchantGatewayId( $arrValues['merchant_gateway_id'] );
		if( isset( $arrValues['eft_batch_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEftBatchTypeId', trim( $arrValues['eft_batch_type_id'] ) ); elseif( isset( $arrValues['eft_batch_type_id'] ) ) $this->setEftBatchTypeId( $arrValues['eft_batch_type_id'] );
		if( isset( $arrValues['eft_batch_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEftBatchStatusTypeId', trim( $arrValues['eft_batch_status_type_id'] ) ); elseif( isset( $arrValues['eft_batch_status_type_id'] ) ) $this->setEftBatchStatusTypeId( $arrValues['eft_batch_status_type_id'] );
		if( isset( $arrValues['aggregate_eft_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intAggregateEftBatchId', trim( $arrValues['aggregate_eft_batch_id'] ) ); elseif( isset( $arrValues['aggregate_eft_batch_id'] ) ) $this->setAggregateEftBatchId( $arrValues['aggregate_eft_batch_id'] );
		if( isset( $arrValues['nacha_file_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaFileId', trim( $arrValues['nacha_file_id'] ) ); elseif( isset( $arrValues['nacha_file_id'] ) ) $this->setNachaFileId( $arrValues['nacha_file_id'] );
		if( isset( $arrValues['x937_file_id'] ) && $boolDirectSet ) $this->set( 'm_intX937FileId', trim( $arrValues['x937_file_id'] ) ); elseif( isset( $arrValues['x937_file_id'] ) ) $this->setX937FileId( $arrValues['x937_file_id'] );
		if( isset( $arrValues['batch_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBatchDatetime', trim( $arrValues['batch_datetime'] ) ); elseif( isset( $arrValues['batch_datetime'] ) ) $this->setBatchDatetime( $arrValues['batch_datetime'] );
		if( isset( $arrValues['scheduled_process_datetime'] ) && $boolDirectSet ) $this->set( 'm_strScheduledProcessDatetime', trim( $arrValues['scheduled_process_datetime'] ) ); elseif( isset( $arrValues['scheduled_process_datetime'] ) ) $this->setScheduledProcessDatetime( $arrValues['scheduled_process_datetime'] );
		if( isset( $arrValues['batch_amount'] ) && $boolDirectSet ) $this->set( 'm_fltBatchAmount', trim( $arrValues['batch_amount'] ) ); elseif( isset( $arrValues['batch_amount'] ) ) $this->setBatchAmount( $arrValues['batch_amount'] );
		if( isset( $arrValues['confirmation_amount'] ) && $boolDirectSet ) $this->set( 'm_fltConfirmationAmount', trim( $arrValues['confirmation_amount'] ) ); elseif( isset( $arrValues['confirmation_amount'] ) ) $this->setConfirmationAmount( $arrValues['confirmation_amount'] );
		if( isset( $arrValues['batch_memo'] ) && $boolDirectSet ) $this->set( 'm_strBatchMemo', trim( stripcslashes( $arrValues['batch_memo'] ) ) ); elseif( isset( $arrValues['batch_memo'] ) ) $this->setBatchMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['batch_memo'] ) : $arrValues['batch_memo'] );
		if( isset( $arrValues['settlement_date'] ) && $boolDirectSet ) $this->set( 'm_strSettlementDate', trim( $arrValues['settlement_date'] ) ); elseif( isset( $arrValues['settlement_date'] ) ) $this->setSettlementDate( $arrValues['settlement_date'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strConfirmedOn', trim( $arrValues['confirmed_on'] ) ); elseif( isset( $arrValues['confirmed_on'] ) ) $this->setConfirmedOn( $arrValues['confirmed_on'] );
		if( isset( $arrValues['is_recon_prepped'] ) && $boolDirectSet ) $this->set( 'm_intIsReconPrepped', trim( $arrValues['is_recon_prepped'] ) ); elseif( isset( $arrValues['is_recon_prepped'] ) ) $this->setIsReconPrepped( $arrValues['is_recon_prepped'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setProcessingBankId( $intProcessingBankId ) {
		$this->set( 'm_intProcessingBankId', CStrings::strToIntDef( $intProcessingBankId, NULL, false ) );
	}

	public function getProcessingBankId() {
		return $this->m_intProcessingBankId;
	}

	public function sqlProcessingBankId() {
		return ( true == isset( $this->m_intProcessingBankId ) ) ? ( string ) $this->m_intProcessingBankId : 'NULL';
	}

	public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
		$this->set( 'm_intProcessingBankAccountId', CStrings::strToIntDef( $intProcessingBankAccountId, NULL, false ) );
	}

	public function getProcessingBankAccountId() {
		return $this->m_intProcessingBankAccountId;
	}

	public function sqlProcessingBankAccountId() {
		return ( true == isset( $this->m_intProcessingBankAccountId ) ) ? ( string ) $this->m_intProcessingBankAccountId : 'NULL';
	}

	public function setGatewayUsernameEncrypted( $strGatewayUsernameEncrypted ) {
		$this->set( 'm_strGatewayUsernameEncrypted', CStrings::strTrimDef( $strGatewayUsernameEncrypted, 240, NULL, true ) );
	}

	public function getGatewayUsernameEncrypted() {
		return $this->m_strGatewayUsernameEncrypted;
	}

	public function sqlGatewayUsernameEncrypted() {
		return ( true == isset( $this->m_strGatewayUsernameEncrypted ) ) ? '\'' . addslashes( $this->m_strGatewayUsernameEncrypted ) . '\'' : 'NULL';
	}

	public function setMerchantGatewayId( $intMerchantGatewayId ) {
		$this->set( 'm_intMerchantGatewayId', CStrings::strToIntDef( $intMerchantGatewayId, NULL, false ) );
	}

	public function getMerchantGatewayId() {
		return $this->m_intMerchantGatewayId;
	}

	public function sqlMerchantGatewayId() {
		return ( true == isset( $this->m_intMerchantGatewayId ) ) ? ( string ) $this->m_intMerchantGatewayId : 'NULL';
	}

	public function setEftBatchTypeId( $intEftBatchTypeId ) {
		$this->set( 'm_intEftBatchTypeId', CStrings::strToIntDef( $intEftBatchTypeId, NULL, false ) );
	}

	public function getEftBatchTypeId() {
		return $this->m_intEftBatchTypeId;
	}

	public function sqlEftBatchTypeId() {
		return ( true == isset( $this->m_intEftBatchTypeId ) ) ? ( string ) $this->m_intEftBatchTypeId : 'NULL';
	}

	public function setEftBatchStatusTypeId( $intEftBatchStatusTypeId ) {
		$this->set( 'm_intEftBatchStatusTypeId', CStrings::strToIntDef( $intEftBatchStatusTypeId, NULL, false ) );
	}

	public function getEftBatchStatusTypeId() {
		return $this->m_intEftBatchStatusTypeId;
	}

	public function sqlEftBatchStatusTypeId() {
		return ( true == isset( $this->m_intEftBatchStatusTypeId ) ) ? ( string ) $this->m_intEftBatchStatusTypeId : 'NULL';
	}

	public function setAggregateEftBatchId( $intAggregateEftBatchId ) {
		$this->set( 'm_intAggregateEftBatchId', CStrings::strToIntDef( $intAggregateEftBatchId, NULL, false ) );
	}

	public function getAggregateEftBatchId() {
		return $this->m_intAggregateEftBatchId;
	}

	public function sqlAggregateEftBatchId() {
		return ( true == isset( $this->m_intAggregateEftBatchId ) ) ? ( string ) $this->m_intAggregateEftBatchId : 'NULL';
	}

	public function setNachaFileId( $intNachaFileId ) {
		$this->set( 'm_intNachaFileId', CStrings::strToIntDef( $intNachaFileId, NULL, false ) );
	}

	public function getNachaFileId() {
		return $this->m_intNachaFileId;
	}

	public function sqlNachaFileId() {
		return ( true == isset( $this->m_intNachaFileId ) ) ? ( string ) $this->m_intNachaFileId : 'NULL';
	}

	public function setX937FileId( $intX937FileId ) {
		$this->set( 'm_intX937FileId', CStrings::strToIntDef( $intX937FileId, NULL, false ) );
	}

	public function getX937FileId() {
		return $this->m_intX937FileId;
	}

	public function sqlX937FileId() {
		return ( true == isset( $this->m_intX937FileId ) ) ? ( string ) $this->m_intX937FileId : 'NULL';
	}

	public function setBatchDatetime( $strBatchDatetime ) {
		$this->set( 'm_strBatchDatetime', CStrings::strTrimDef( $strBatchDatetime, -1, NULL, true ) );
	}

	public function getBatchDatetime() {
		return $this->m_strBatchDatetime;
	}

	public function sqlBatchDatetime() {
		return ( true == isset( $this->m_strBatchDatetime ) ) ? '\'' . $this->m_strBatchDatetime . '\'' : 'NOW()';
	}

	public function setScheduledProcessDatetime( $strScheduledProcessDatetime ) {
		$this->set( 'm_strScheduledProcessDatetime', CStrings::strTrimDef( $strScheduledProcessDatetime, -1, NULL, true ) );
	}

	public function getScheduledProcessDatetime() {
		return $this->m_strScheduledProcessDatetime;
	}

	public function sqlScheduledProcessDatetime() {
		return ( true == isset( $this->m_strScheduledProcessDatetime ) ) ? '\'' . $this->m_strScheduledProcessDatetime . '\'' : 'NOW()';
	}

	public function setBatchAmount( $fltBatchAmount ) {
		$this->set( 'm_fltBatchAmount', CStrings::strToFloatDef( $fltBatchAmount, NULL, false, 4 ) );
	}

	public function getBatchAmount() {
		return $this->m_fltBatchAmount;
	}

	public function sqlBatchAmount() {
		return ( true == isset( $this->m_fltBatchAmount ) ) ? ( string ) $this->m_fltBatchAmount : 'NULL';
	}

	public function setConfirmationAmount( $fltConfirmationAmount ) {
		$this->set( 'm_fltConfirmationAmount', CStrings::strToFloatDef( $fltConfirmationAmount, NULL, false, 4 ) );
	}

	public function getConfirmationAmount() {
		return $this->m_fltConfirmationAmount;
	}

	public function sqlConfirmationAmount() {
		return ( true == isset( $this->m_fltConfirmationAmount ) ) ? ( string ) $this->m_fltConfirmationAmount : 'NULL';
	}

	public function setBatchMemo( $strBatchMemo ) {
		$this->set( 'm_strBatchMemo', CStrings::strTrimDef( $strBatchMemo, 2000, NULL, true ) );
	}

	public function getBatchMemo() {
		return $this->m_strBatchMemo;
	}

	public function sqlBatchMemo() {
		return ( true == isset( $this->m_strBatchMemo ) ) ? '\'' . addslashes( $this->m_strBatchMemo ) . '\'' : 'NULL';
	}

	public function setSettlementDate( $strSettlementDate ) {
		$this->set( 'm_strSettlementDate', CStrings::strTrimDef( $strSettlementDate, -1, NULL, true ) );
	}

	public function getSettlementDate() {
		return $this->m_strSettlementDate;
	}

	public function sqlSettlementDate() {
		return ( true == isset( $this->m_strSettlementDate ) ) ? '\'' . $this->m_strSettlementDate . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setConfirmedOn( $strConfirmedOn ) {
		$this->set( 'm_strConfirmedOn', CStrings::strTrimDef( $strConfirmedOn, -1, NULL, true ) );
	}

	public function getConfirmedOn() {
		return $this->m_strConfirmedOn;
	}

	public function sqlConfirmedOn() {
		return ( true == isset( $this->m_strConfirmedOn ) ) ? '\'' . $this->m_strConfirmedOn . '\'' : 'NULL';
	}

	public function setIsReconPrepped( $intIsReconPrepped ) {
		$this->set( 'm_intIsReconPrepped', CStrings::strToIntDef( $intIsReconPrepped, NULL, false ) );
	}

	public function getIsReconPrepped() {
		return $this->m_intIsReconPrepped;
	}

	public function sqlIsReconPrepped() {
		return ( true == isset( $this->m_intIsReconPrepped ) ) ? ( string ) $this->m_intIsReconPrepped : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, processing_bank_id, processing_bank_account_id, gateway_username_encrypted, merchant_gateway_id, eft_batch_type_id, eft_batch_status_type_id, aggregate_eft_batch_id, nacha_file_id, x937_file_id, batch_datetime, scheduled_process_datetime, batch_amount, confirmation_amount, batch_memo, settlement_date, file_name, file_path, processed_on, confirmed_on, is_recon_prepped, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlProcessingBankId() . ', ' .
 						$this->sqlProcessingBankAccountId() . ', ' .
 						$this->sqlGatewayUsernameEncrypted() . ', ' .
 						$this->sqlMerchantGatewayId() . ', ' .
 						$this->sqlEftBatchTypeId() . ', ' .
 						$this->sqlEftBatchStatusTypeId() . ', ' .
 						$this->sqlAggregateEftBatchId() . ', ' .
 						$this->sqlNachaFileId() . ', ' .
 						$this->sqlX937FileId() . ', ' .
 						$this->sqlBatchDatetime() . ', ' .
 						$this->sqlScheduledProcessDatetime() . ', ' .
 						$this->sqlBatchAmount() . ', ' .
 						$this->sqlConfirmationAmount() . ', ' .
 						$this->sqlBatchMemo() . ', ' .
 						$this->sqlSettlementDate() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlProcessedOn() . ', ' .
 						$this->sqlConfirmedOn() . ', ' .
 						$this->sqlIsReconPrepped() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; } elseif( true == array_key_exists( 'ProcessingBankId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; } elseif( true == array_key_exists( 'ProcessingBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gateway_username_encrypted = ' . $this->sqlGatewayUsernameEncrypted() . ','; } elseif( true == array_key_exists( 'GatewayUsernameEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' gateway_username_encrypted = ' . $this->sqlGatewayUsernameEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; } elseif( true == array_key_exists( 'MerchantGatewayId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eft_batch_type_id = ' . $this->sqlEftBatchTypeId() . ','; } elseif( true == array_key_exists( 'EftBatchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' eft_batch_type_id = ' . $this->sqlEftBatchTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eft_batch_status_type_id = ' . $this->sqlEftBatchStatusTypeId() . ','; } elseif( true == array_key_exists( 'EftBatchStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' eft_batch_status_type_id = ' . $this->sqlEftBatchStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' aggregate_eft_batch_id = ' . $this->sqlAggregateEftBatchId() . ','; } elseif( true == array_key_exists( 'AggregateEftBatchId', $this->getChangedColumns() ) ) { $strSql .= ' aggregate_eft_batch_id = ' . $this->sqlAggregateEftBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_file_id = ' . $this->sqlNachaFileId() . ','; } elseif( true == array_key_exists( 'NachaFileId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_file_id = ' . $this->sqlNachaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_file_id = ' . $this->sqlX937FileId() . ','; } elseif( true == array_key_exists( 'X937FileId', $this->getChangedColumns() ) ) { $strSql .= ' x937_file_id = ' . $this->sqlX937FileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime() . ','; } elseif( true == array_key_exists( 'BatchDatetime', $this->getChangedColumns() ) ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_process_datetime = ' . $this->sqlScheduledProcessDatetime() . ','; } elseif( true == array_key_exists( 'ScheduledProcessDatetime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_process_datetime = ' . $this->sqlScheduledProcessDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_amount = ' . $this->sqlBatchAmount() . ','; } elseif( true == array_key_exists( 'BatchAmount', $this->getChangedColumns() ) ) { $strSql .= ' batch_amount = ' . $this->sqlBatchAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmation_amount = ' . $this->sqlConfirmationAmount() . ','; } elseif( true == array_key_exists( 'ConfirmationAmount', $this->getChangedColumns() ) ) { $strSql .= ' confirmation_amount = ' . $this->sqlConfirmationAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_memo = ' . $this->sqlBatchMemo() . ','; } elseif( true == array_key_exists( 'BatchMemo', $this->getChangedColumns() ) ) { $strSql .= ' batch_memo = ' . $this->sqlBatchMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' settlement_date = ' . $this->sqlSettlementDate() . ','; } elseif( true == array_key_exists( 'SettlementDate', $this->getChangedColumns() ) ) { $strSql .= ' settlement_date = ' . $this->sqlSettlementDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; } elseif( true == array_key_exists( 'ConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; } elseif( true == array_key_exists( 'IsReconPrepped', $this->getChangedColumns() ) ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'processing_bank_id' => $this->getProcessingBankId(),
			'processing_bank_account_id' => $this->getProcessingBankAccountId(),
			'gateway_username_encrypted' => $this->getGatewayUsernameEncrypted(),
			'merchant_gateway_id' => $this->getMerchantGatewayId(),
			'eft_batch_type_id' => $this->getEftBatchTypeId(),
			'eft_batch_status_type_id' => $this->getEftBatchStatusTypeId(),
			'aggregate_eft_batch_id' => $this->getAggregateEftBatchId(),
			'nacha_file_id' => $this->getNachaFileId(),
			'x937_file_id' => $this->getX937FileId(),
			'batch_datetime' => $this->getBatchDatetime(),
			'scheduled_process_datetime' => $this->getScheduledProcessDatetime(),
			'batch_amount' => $this->getBatchAmount(),
			'confirmation_amount' => $this->getConfirmationAmount(),
			'batch_memo' => $this->getBatchMemo(),
			'settlement_date' => $this->getSettlementDate(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'processed_on' => $this->getProcessedOn(),
			'confirmed_on' => $this->getConfirmedOn(),
			'is_recon_prepped' => $this->getIsReconPrepped(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>