<?php

class CBaseNachaReturnFile extends CEosSingularBase {

	const TABLE_NAME = 'public.nacha_return_files';

	protected $m_intId;
	protected $m_intProcessingBankId;
	protected $m_intProcessingBankAccountId;
	protected $m_intNachaFileTypeId;
	protected $m_intNachaFileStatusTypeId;
	protected $m_intMerchantGatewayId;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strNachaFileDatetime;
	protected $m_strProcessedOn;
	protected $m_strReturnsProcessedOn;
	protected $m_strCentralReturnsProcessedOn;
	protected $m_strConfirmedOn;
	protected $m_strRepairedOn;
	protected $m_intIsReconPrepped;
	protected $m_intIsCorrection;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsReconPrepped = '0';
		$this->m_intIsCorrection = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['processing_bank_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankId', trim( $arrValues['processing_bank_id'] ) ); elseif( isset( $arrValues['processing_bank_id'] ) ) $this->setProcessingBankId( $arrValues['processing_bank_id'] );
		if( isset( $arrValues['processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankAccountId', trim( $arrValues['processing_bank_account_id'] ) ); elseif( isset( $arrValues['processing_bank_account_id'] ) ) $this->setProcessingBankAccountId( $arrValues['processing_bank_account_id'] );
		if( isset( $arrValues['nacha_file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaFileTypeId', trim( $arrValues['nacha_file_type_id'] ) ); elseif( isset( $arrValues['nacha_file_type_id'] ) ) $this->setNachaFileTypeId( $arrValues['nacha_file_type_id'] );
		if( isset( $arrValues['nacha_file_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaFileStatusTypeId', trim( $arrValues['nacha_file_status_type_id'] ) ); elseif( isset( $arrValues['nacha_file_status_type_id'] ) ) $this->setNachaFileStatusTypeId( $arrValues['nacha_file_status_type_id'] );
		if( isset( $arrValues['merchant_gateway_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantGatewayId', trim( $arrValues['merchant_gateway_id'] ) ); elseif( isset( $arrValues['merchant_gateway_id'] ) ) $this->setMerchantGatewayId( $arrValues['merchant_gateway_id'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['nacha_file_datetime'] ) && $boolDirectSet ) $this->set( 'm_strNachaFileDatetime', trim( $arrValues['nacha_file_datetime'] ) ); elseif( isset( $arrValues['nacha_file_datetime'] ) ) $this->setNachaFileDatetime( $arrValues['nacha_file_datetime'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['returns_processed_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnsProcessedOn', trim( $arrValues['returns_processed_on'] ) ); elseif( isset( $arrValues['returns_processed_on'] ) ) $this->setReturnsProcessedOn( $arrValues['returns_processed_on'] );
		if( isset( $arrValues['central_returns_processed_on'] ) && $boolDirectSet ) $this->set( 'm_strCentralReturnsProcessedOn', trim( $arrValues['central_returns_processed_on'] ) ); elseif( isset( $arrValues['central_returns_processed_on'] ) ) $this->setCentralReturnsProcessedOn( $arrValues['central_returns_processed_on'] );
		if( isset( $arrValues['confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strConfirmedOn', trim( $arrValues['confirmed_on'] ) ); elseif( isset( $arrValues['confirmed_on'] ) ) $this->setConfirmedOn( $arrValues['confirmed_on'] );
		if( isset( $arrValues['repaired_on'] ) && $boolDirectSet ) $this->set( 'm_strRepairedOn', trim( $arrValues['repaired_on'] ) ); elseif( isset( $arrValues['repaired_on'] ) ) $this->setRepairedOn( $arrValues['repaired_on'] );
		if( isset( $arrValues['is_recon_prepped'] ) && $boolDirectSet ) $this->set( 'm_intIsReconPrepped', trim( $arrValues['is_recon_prepped'] ) ); elseif( isset( $arrValues['is_recon_prepped'] ) ) $this->setIsReconPrepped( $arrValues['is_recon_prepped'] );
		if( isset( $arrValues['is_correction'] ) && $boolDirectSet ) $this->set( 'm_intIsCorrection', trim( $arrValues['is_correction'] ) ); elseif( isset( $arrValues['is_correction'] ) ) $this->setIsCorrection( $arrValues['is_correction'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setProcessingBankId( $intProcessingBankId ) {
		$this->set( 'm_intProcessingBankId', CStrings::strToIntDef( $intProcessingBankId, NULL, false ) );
	}

	public function getProcessingBankId() {
		return $this->m_intProcessingBankId;
	}

	public function sqlProcessingBankId() {
		return ( true == isset( $this->m_intProcessingBankId ) ) ? ( string ) $this->m_intProcessingBankId : 'NULL';
	}

	public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
		$this->set( 'm_intProcessingBankAccountId', CStrings::strToIntDef( $intProcessingBankAccountId, NULL, false ) );
	}

	public function getProcessingBankAccountId() {
		return $this->m_intProcessingBankAccountId;
	}

	public function sqlProcessingBankAccountId() {
		return ( true == isset( $this->m_intProcessingBankAccountId ) ) ? ( string ) $this->m_intProcessingBankAccountId : 'NULL';
	}

	public function setNachaFileTypeId( $intNachaFileTypeId ) {
		$this->set( 'm_intNachaFileTypeId', CStrings::strToIntDef( $intNachaFileTypeId, NULL, false ) );
	}

	public function getNachaFileTypeId() {
		return $this->m_intNachaFileTypeId;
	}

	public function sqlNachaFileTypeId() {
		return ( true == isset( $this->m_intNachaFileTypeId ) ) ? ( string ) $this->m_intNachaFileTypeId : 'NULL';
	}

	public function setNachaFileStatusTypeId( $intNachaFileStatusTypeId ) {
		$this->set( 'm_intNachaFileStatusTypeId', CStrings::strToIntDef( $intNachaFileStatusTypeId, NULL, false ) );
	}

	public function getNachaFileStatusTypeId() {
		return $this->m_intNachaFileStatusTypeId;
	}

	public function sqlNachaFileStatusTypeId() {
		return ( true == isset( $this->m_intNachaFileStatusTypeId ) ) ? ( string ) $this->m_intNachaFileStatusTypeId : 'NULL';
	}

	public function setMerchantGatewayId( $intMerchantGatewayId ) {
		$this->set( 'm_intMerchantGatewayId', CStrings::strToIntDef( $intMerchantGatewayId, NULL, false ) );
	}

	public function getMerchantGatewayId() {
		return $this->m_intMerchantGatewayId;
	}

	public function sqlMerchantGatewayId() {
		return ( true == isset( $this->m_intMerchantGatewayId ) ) ? ( string ) $this->m_intMerchantGatewayId : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setNachaFileDatetime( $strNachaFileDatetime ) {
		$this->set( 'm_strNachaFileDatetime', CStrings::strTrimDef( $strNachaFileDatetime, -1, NULL, true ) );
	}

	public function getNachaFileDatetime() {
		return $this->m_strNachaFileDatetime;
	}

	public function sqlNachaFileDatetime() {
		return ( true == isset( $this->m_strNachaFileDatetime ) ) ? '\'' . $this->m_strNachaFileDatetime . '\'' : 'NOW()';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NOW()';
	}

	public function setReturnsProcessedOn( $strReturnsProcessedOn ) {
		$this->set( 'm_strReturnsProcessedOn', CStrings::strTrimDef( $strReturnsProcessedOn, -1, NULL, true ) );
	}

	public function getReturnsProcessedOn() {
		return $this->m_strReturnsProcessedOn;
	}

	public function sqlReturnsProcessedOn() {
		return ( true == isset( $this->m_strReturnsProcessedOn ) ) ? '\'' . $this->m_strReturnsProcessedOn . '\'' : 'NULL';
	}

	public function setCentralReturnsProcessedOn( $strCentralReturnsProcessedOn ) {
		$this->set( 'm_strCentralReturnsProcessedOn', CStrings::strTrimDef( $strCentralReturnsProcessedOn, -1, NULL, true ) );
	}

	public function getCentralReturnsProcessedOn() {
		return $this->m_strCentralReturnsProcessedOn;
	}

	public function sqlCentralReturnsProcessedOn() {
		return ( true == isset( $this->m_strCentralReturnsProcessedOn ) ) ? '\'' . $this->m_strCentralReturnsProcessedOn . '\'' : 'NULL';
	}

	public function setConfirmedOn( $strConfirmedOn ) {
		$this->set( 'm_strConfirmedOn', CStrings::strTrimDef( $strConfirmedOn, -1, NULL, true ) );
	}

	public function getConfirmedOn() {
		return $this->m_strConfirmedOn;
	}

	public function sqlConfirmedOn() {
		return ( true == isset( $this->m_strConfirmedOn ) ) ? '\'' . $this->m_strConfirmedOn . '\'' : 'NULL';
	}

	public function setRepairedOn( $strRepairedOn ) {
		$this->set( 'm_strRepairedOn', CStrings::strTrimDef( $strRepairedOn, -1, NULL, true ) );
	}

	public function getRepairedOn() {
		return $this->m_strRepairedOn;
	}

	public function sqlRepairedOn() {
		return ( true == isset( $this->m_strRepairedOn ) ) ? '\'' . $this->m_strRepairedOn . '\'' : 'NULL';
	}

	public function setIsReconPrepped( $intIsReconPrepped ) {
		$this->set( 'm_intIsReconPrepped', CStrings::strToIntDef( $intIsReconPrepped, NULL, false ) );
	}

	public function getIsReconPrepped() {
		return $this->m_intIsReconPrepped;
	}

	public function sqlIsReconPrepped() {
		return ( true == isset( $this->m_intIsReconPrepped ) ) ? ( string ) $this->m_intIsReconPrepped : '0';
	}

	public function setIsCorrection( $intIsCorrection ) {
		$this->set( 'm_intIsCorrection', CStrings::strToIntDef( $intIsCorrection, NULL, false ) );
	}

	public function getIsCorrection() {
		return $this->m_intIsCorrection;
	}

	public function sqlIsCorrection() {
		return ( true == isset( $this->m_intIsCorrection ) ) ? ( string ) $this->m_intIsCorrection : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, processing_bank_id, processing_bank_account_id, nacha_file_type_id, nacha_file_status_type_id, merchant_gateway_id, file_name, file_path, nacha_file_datetime, processed_on, returns_processed_on, central_returns_processed_on, confirmed_on, repaired_on, is_recon_prepped, is_correction, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlProcessingBankId() . ', ' .
 						$this->sqlProcessingBankAccountId() . ', ' .
 						$this->sqlNachaFileTypeId() . ', ' .
 						$this->sqlNachaFileStatusTypeId() . ', ' .
 						$this->sqlMerchantGatewayId() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlNachaFileDatetime() . ', ' .
 						$this->sqlProcessedOn() . ', ' .
 						$this->sqlReturnsProcessedOn() . ', ' .
 						$this->sqlCentralReturnsProcessedOn() . ', ' .
 						$this->sqlConfirmedOn() . ', ' .
 						$this->sqlRepairedOn() . ', ' .
 						$this->sqlIsReconPrepped() . ', ' .
 						$this->sqlIsCorrection() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; } elseif( true == array_key_exists( 'ProcessingBankId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; } elseif( true == array_key_exists( 'ProcessingBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_file_type_id = ' . $this->sqlNachaFileTypeId() . ','; } elseif( true == array_key_exists( 'NachaFileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_file_type_id = ' . $this->sqlNachaFileTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_file_status_type_id = ' . $this->sqlNachaFileStatusTypeId() . ','; } elseif( true == array_key_exists( 'NachaFileStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_file_status_type_id = ' . $this->sqlNachaFileStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; } elseif( true == array_key_exists( 'MerchantGatewayId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_file_datetime = ' . $this->sqlNachaFileDatetime() . ','; } elseif( true == array_key_exists( 'NachaFileDatetime', $this->getChangedColumns() ) ) { $strSql .= ' nacha_file_datetime = ' . $this->sqlNachaFileDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returns_processed_on = ' . $this->sqlReturnsProcessedOn() . ','; } elseif( true == array_key_exists( 'ReturnsProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' returns_processed_on = ' . $this->sqlReturnsProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' central_returns_processed_on = ' . $this->sqlCentralReturnsProcessedOn() . ','; } elseif( true == array_key_exists( 'CentralReturnsProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' central_returns_processed_on = ' . $this->sqlCentralReturnsProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; } elseif( true == array_key_exists( 'ConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repaired_on = ' . $this->sqlRepairedOn() . ','; } elseif( true == array_key_exists( 'RepairedOn', $this->getChangedColumns() ) ) { $strSql .= ' repaired_on = ' . $this->sqlRepairedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; } elseif( true == array_key_exists( 'IsReconPrepped', $this->getChangedColumns() ) ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_correction = ' . $this->sqlIsCorrection() . ','; } elseif( true == array_key_exists( 'IsCorrection', $this->getChangedColumns() ) ) { $strSql .= ' is_correction = ' . $this->sqlIsCorrection() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'processing_bank_id' => $this->getProcessingBankId(),
			'processing_bank_account_id' => $this->getProcessingBankAccountId(),
			'nacha_file_type_id' => $this->getNachaFileTypeId(),
			'nacha_file_status_type_id' => $this->getNachaFileStatusTypeId(),
			'merchant_gateway_id' => $this->getMerchantGatewayId(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'nacha_file_datetime' => $this->getNachaFileDatetime(),
			'processed_on' => $this->getProcessedOn(),
			'returns_processed_on' => $this->getReturnsProcessedOn(),
			'central_returns_processed_on' => $this->getCentralReturnsProcessedOn(),
			'confirmed_on' => $this->getConfirmedOn(),
			'repaired_on' => $this->getRepairedOn(),
			'is_recon_prepped' => $this->getIsReconPrepped(),
			'is_correction' => $this->getIsCorrection(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>