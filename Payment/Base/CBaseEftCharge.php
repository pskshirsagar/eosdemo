<?php

class CBaseEftCharge extends CEosSingularBase {

	const TABLE_NAME = 'public.eft_charges';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intEftChargeTypeId;
	protected $m_intCompanyMerchantAccountId;
	protected $m_intEftInstructionId;
	protected $m_intArPaymentId;
	protected $m_intSettlementDistributionId;
	protected $m_intCompanyTransactionId;
	protected $m_intTransactionId;
	protected $m_intClearingTransactionId;
	protected $m_intAccountId;
	protected $m_intChargeCodeId;
	protected $m_intEftChargeId;
	protected $m_intGiactRequestLogId;
	protected $m_strChargeDatetime;
	protected $m_fltChargeAmount;
	protected $m_fltCostAmount;
	protected $m_strMemo;
	protected $m_strPostedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strCurrencyCode;

	public function __construct() {
		parent::__construct();

		$this->m_fltCostAmount = '0';
		$this->m_strCurrencyCode = 'USD';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['eft_charge_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEftChargeTypeId', trim( $arrValues['eft_charge_type_id'] ) ); elseif( isset( $arrValues['eft_charge_type_id'] ) ) $this->setEftChargeTypeId( $arrValues['eft_charge_type_id'] );
		if( isset( $arrValues['company_merchant_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMerchantAccountId', trim( $arrValues['company_merchant_account_id'] ) ); elseif( isset( $arrValues['company_merchant_account_id'] ) ) $this->setCompanyMerchantAccountId( $arrValues['company_merchant_account_id'] );
		if( isset( $arrValues['eft_instruction_id'] ) && $boolDirectSet ) $this->set( 'm_intEftInstructionId', trim( $arrValues['eft_instruction_id'] ) ); elseif( isset( $arrValues['eft_instruction_id'] ) ) $this->setEftInstructionId( $arrValues['eft_instruction_id'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['settlement_distribution_id'] ) && $boolDirectSet ) $this->set( 'm_intSettlementDistributionId', trim( $arrValues['settlement_distribution_id'] ) ); elseif( isset( $arrValues['settlement_distribution_id'] ) ) $this->setSettlementDistributionId( $arrValues['settlement_distribution_id'] );
		if( isset( $arrValues['company_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyTransactionId', trim( $arrValues['company_transaction_id'] ) ); elseif( isset( $arrValues['company_transaction_id'] ) ) $this->setCompanyTransactionId( $arrValues['company_transaction_id'] );
		if( isset( $arrValues['transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionId', trim( $arrValues['transaction_id'] ) ); elseif( isset( $arrValues['transaction_id'] ) ) $this->setTransactionId( $arrValues['transaction_id'] );
		if( isset( $arrValues['clearing_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intClearingTransactionId', trim( $arrValues['clearing_transaction_id'] ) ); elseif( isset( $arrValues['clearing_transaction_id'] ) ) $this->setClearingTransactionId( $arrValues['clearing_transaction_id'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeCodeId', trim( $arrValues['charge_code_id'] ) ); elseif( isset( $arrValues['charge_code_id'] ) ) $this->setChargeCodeId( $arrValues['charge_code_id'] );
		if( isset( $arrValues['eft_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intEftChargeId', trim( $arrValues['eft_charge_id'] ) ); elseif( isset( $arrValues['eft_charge_id'] ) ) $this->setEftChargeId( $arrValues['eft_charge_id'] );
		if( isset( $arrValues['giact_request_log_id'] ) && $boolDirectSet ) $this->set( 'm_intGiactRequestLogId', trim( $arrValues['giact_request_log_id'] ) ); elseif( isset( $arrValues['giact_request_log_id'] ) ) $this->setGiactRequestLogId( $arrValues['giact_request_log_id'] );
		if( isset( $arrValues['charge_datetime'] ) && $boolDirectSet ) $this->set( 'm_strChargeDatetime', trim( $arrValues['charge_datetime'] ) ); elseif( isset( $arrValues['charge_datetime'] ) ) $this->setChargeDatetime( $arrValues['charge_datetime'] );
		if( isset( $arrValues['charge_amount'] ) && $boolDirectSet ) $this->set( 'm_fltChargeAmount', trim( $arrValues['charge_amount'] ) ); elseif( isset( $arrValues['charge_amount'] ) ) $this->setChargeAmount( $arrValues['charge_amount'] );
		if( isset( $arrValues['cost_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCostAmount', trim( $arrValues['cost_amount'] ) ); elseif( isset( $arrValues['cost_amount'] ) ) $this->setCostAmount( $arrValues['cost_amount'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( stripcslashes( $arrValues['memo'] ) ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['memo'] ) : $arrValues['memo'] );
		if( isset( $arrValues['posted_on'] ) && $boolDirectSet ) $this->set( 'm_strPostedOn', trim( $arrValues['posted_on'] ) ); elseif( isset( $arrValues['posted_on'] ) ) $this->setPostedOn( $arrValues['posted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( stripcslashes( $arrValues['currency_code'] ) ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['currency_code'] ) : $arrValues['currency_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setEftChargeTypeId( $intEftChargeTypeId ) {
		$this->set( 'm_intEftChargeTypeId', CStrings::strToIntDef( $intEftChargeTypeId, NULL, false ) );
	}

	public function getEftChargeTypeId() {
		return $this->m_intEftChargeTypeId;
	}

	public function sqlEftChargeTypeId() {
		return ( true == isset( $this->m_intEftChargeTypeId ) ) ? ( string ) $this->m_intEftChargeTypeId : 'NULL';
	}

	public function setCompanyMerchantAccountId( $intCompanyMerchantAccountId ) {
		$this->set( 'm_intCompanyMerchantAccountId', CStrings::strToIntDef( $intCompanyMerchantAccountId, NULL, false ) );
	}

	public function getCompanyMerchantAccountId() {
		return $this->m_intCompanyMerchantAccountId;
	}

	public function sqlCompanyMerchantAccountId() {
		return ( true == isset( $this->m_intCompanyMerchantAccountId ) ) ? ( string ) $this->m_intCompanyMerchantAccountId : 'NULL';
	}

	public function setEftInstructionId( $intEftInstructionId ) {
		$this->set( 'm_intEftInstructionId', CStrings::strToIntDef( $intEftInstructionId, NULL, false ) );
	}

	public function getEftInstructionId() {
		return $this->m_intEftInstructionId;
	}

	public function sqlEftInstructionId() {
		return ( true == isset( $this->m_intEftInstructionId ) ) ? ( string ) $this->m_intEftInstructionId : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setSettlementDistributionId( $intSettlementDistributionId ) {
		$this->set( 'm_intSettlementDistributionId', CStrings::strToIntDef( $intSettlementDistributionId, NULL, false ) );
	}

	public function getSettlementDistributionId() {
		return $this->m_intSettlementDistributionId;
	}

	public function sqlSettlementDistributionId() {
		return ( true == isset( $this->m_intSettlementDistributionId ) ) ? ( string ) $this->m_intSettlementDistributionId : 'NULL';
	}

	public function setCompanyTransactionId( $intCompanyTransactionId ) {
		$this->set( 'm_intCompanyTransactionId', CStrings::strToIntDef( $intCompanyTransactionId, NULL, false ) );
	}

	public function getCompanyTransactionId() {
		return $this->m_intCompanyTransactionId;
	}

	public function sqlCompanyTransactionId() {
		return ( true == isset( $this->m_intCompanyTransactionId ) ) ? ( string ) $this->m_intCompanyTransactionId : 'NULL';
	}

	public function setTransactionId( $intTransactionId ) {
		$this->set( 'm_intTransactionId', CStrings::strToIntDef( $intTransactionId, NULL, false ) );
	}

	public function getTransactionId() {
		return $this->m_intTransactionId;
	}

	public function sqlTransactionId() {
		return ( true == isset( $this->m_intTransactionId ) ) ? ( string ) $this->m_intTransactionId : 'NULL';
	}

	public function setClearingTransactionId( $intClearingTransactionId ) {
		$this->set( 'm_intClearingTransactionId', CStrings::strToIntDef( $intClearingTransactionId, NULL, false ) );
	}

	public function getClearingTransactionId() {
		return $this->m_intClearingTransactionId;
	}

	public function sqlClearingTransactionId() {
		return ( true == isset( $this->m_intClearingTransactionId ) ) ? ( string ) $this->m_intClearingTransactionId : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setChargeCodeId( $intChargeCodeId ) {
		$this->set( 'm_intChargeCodeId', CStrings::strToIntDef( $intChargeCodeId, NULL, false ) );
	}

	public function getChargeCodeId() {
		return $this->m_intChargeCodeId;
	}

	public function sqlChargeCodeId() {
		return ( true == isset( $this->m_intChargeCodeId ) ) ? ( string ) $this->m_intChargeCodeId : 'NULL';
	}

	public function setEftChargeId( $intEftChargeId ) {
		$this->set( 'm_intEftChargeId', CStrings::strToIntDef( $intEftChargeId, NULL, false ) );
	}

	public function getEftChargeId() {
		return $this->m_intEftChargeId;
	}

	public function sqlEftChargeId() {
		return ( true == isset( $this->m_intEftChargeId ) ) ? ( string ) $this->m_intEftChargeId : 'NULL';
	}

	public function setGiactRequestLogId( $intGiactRequestLogId ) {
		$this->set( 'm_intGiactRequestLogId', CStrings::strToIntDef( $intGiactRequestLogId, NULL, false ) );
	}

	public function getGiactRequestLogId() {
		return $this->m_intGiactRequestLogId;
	}

	public function sqlGiactRequestLogId() {
		return ( true == isset( $this->m_intGiactRequestLogId ) ) ? ( string ) $this->m_intGiactRequestLogId : 'NULL';
	}

	public function setChargeDatetime( $strChargeDatetime ) {
		$this->set( 'm_strChargeDatetime', CStrings::strTrimDef( $strChargeDatetime, -1, NULL, true ) );
	}

	public function getChargeDatetime() {
		return $this->m_strChargeDatetime;
	}

	public function sqlChargeDatetime() {
		return ( true == isset( $this->m_strChargeDatetime ) ) ? '\'' . $this->m_strChargeDatetime . '\'' : 'NOW()';
	}

	public function setChargeAmount( $fltChargeAmount ) {
		$this->set( 'm_fltChargeAmount', CStrings::strToFloatDef( $fltChargeAmount, NULL, false, 2 ) );
	}

	public function getChargeAmount() {
		return $this->m_fltChargeAmount;
	}

	public function sqlChargeAmount() {
		return ( true == isset( $this->m_fltChargeAmount ) ) ? ( string ) $this->m_fltChargeAmount : 'NULL';
	}

	public function setCostAmount( $fltCostAmount ) {
		$this->set( 'm_fltCostAmount', CStrings::strToFloatDef( $fltCostAmount, NULL, false, 2 ) );
	}

	public function getCostAmount() {
		return $this->m_fltCostAmount;
	}

	public function sqlCostAmount() {
		return ( true == isset( $this->m_fltCostAmount ) ) ? ( string ) $this->m_fltCostAmount : '0';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, 2000, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? '\'' . addslashes( $this->m_strMemo ) . '\'' : 'NULL';
	}

	public function setPostedOn( $strPostedOn ) {
		$this->set( 'm_strPostedOn', CStrings::strTrimDef( $strPostedOn, -1, NULL, true ) );
	}

	public function getPostedOn() {
		return $this->m_strPostedOn;
	}

	public function sqlPostedOn() {
		return ( true == isset( $this->m_strPostedOn ) ) ? '\'' . $this->m_strPostedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' : '\'USD\'';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, eft_charge_type_id, company_merchant_account_id, eft_instruction_id, ar_payment_id, settlement_distribution_id, company_transaction_id, transaction_id, clearing_transaction_id, account_id, charge_code_id, eft_charge_id, giact_request_log_id, charge_datetime, charge_amount, cost_amount, memo, posted_on, updated_by, updated_on, created_by, created_on, currency_code )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlEftChargeTypeId() . ', ' .
						$this->sqlCompanyMerchantAccountId() . ', ' .
						$this->sqlEftInstructionId() . ', ' .
						$this->sqlArPaymentId() . ', ' .
						$this->sqlSettlementDistributionId() . ', ' .
						$this->sqlCompanyTransactionId() . ', ' .
						$this->sqlTransactionId() . ', ' .
						$this->sqlClearingTransactionId() . ', ' .
						$this->sqlAccountId() . ', ' .
						$this->sqlChargeCodeId() . ', ' .
						$this->sqlEftChargeId() . ', ' .
						$this->sqlGiactRequestLogId() . ', ' .
						$this->sqlChargeDatetime() . ', ' .
						$this->sqlChargeAmount() . ', ' .
						$this->sqlCostAmount() . ', ' .
						$this->sqlMemo() . ', ' .
						$this->sqlPostedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCurrencyCode() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eft_charge_type_id = ' . $this->sqlEftChargeTypeId() . ','; } elseif( true == array_key_exists( 'EftChargeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' eft_charge_type_id = ' . $this->sqlEftChargeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_merchant_account_id = ' . $this->sqlCompanyMerchantAccountId() . ','; } elseif( true == array_key_exists( 'CompanyMerchantAccountId', $this->getChangedColumns() ) ) { $strSql .= ' company_merchant_account_id = ' . $this->sqlCompanyMerchantAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eft_instruction_id = ' . $this->sqlEftInstructionId() . ','; } elseif( true == array_key_exists( 'EftInstructionId', $this->getChangedColumns() ) ) { $strSql .= ' eft_instruction_id = ' . $this->sqlEftInstructionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId() . ','; } elseif( true == array_key_exists( 'SettlementDistributionId', $this->getChangedColumns() ) ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_transaction_id = ' . $this->sqlCompanyTransactionId() . ','; } elseif( true == array_key_exists( 'CompanyTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' company_transaction_id = ' . $this->sqlCompanyTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; } elseif( true == array_key_exists( 'TransactionId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clearing_transaction_id = ' . $this->sqlClearingTransactionId() . ','; } elseif( true == array_key_exists( 'ClearingTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' clearing_transaction_id = ' . $this->sqlClearingTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_code_id = ' . $this->sqlChargeCodeId() . ','; } elseif( true == array_key_exists( 'ChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' charge_code_id = ' . $this->sqlChargeCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eft_charge_id = ' . $this->sqlEftChargeId() . ','; } elseif( true == array_key_exists( 'EftChargeId', $this->getChangedColumns() ) ) { $strSql .= ' eft_charge_id = ' . $this->sqlEftChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' giact_request_log_id = ' . $this->sqlGiactRequestLogId() . ','; } elseif( true == array_key_exists( 'GiactRequestLogId', $this->getChangedColumns() ) ) { $strSql .= ' giact_request_log_id = ' . $this->sqlGiactRequestLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_datetime = ' . $this->sqlChargeDatetime() . ','; } elseif( true == array_key_exists( 'ChargeDatetime', $this->getChangedColumns() ) ) { $strSql .= ' charge_datetime = ' . $this->sqlChargeDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_amount = ' . $this->sqlChargeAmount() . ','; } elseif( true == array_key_exists( 'ChargeAmount', $this->getChangedColumns() ) ) { $strSql .= ' charge_amount = ' . $this->sqlChargeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cost_amount = ' . $this->sqlCostAmount() . ','; } elseif( true == array_key_exists( 'CostAmount', $this->getChangedColumns() ) ) { $strSql .= ' cost_amount = ' . $this->sqlCostAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_on = ' . $this->sqlPostedOn() . ','; } elseif( true == array_key_exists( 'PostedOn', $this->getChangedColumns() ) ) { $strSql .= ' posted_on = ' . $this->sqlPostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode(). ',' ; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'eft_charge_type_id' => $this->getEftChargeTypeId(),
			'company_merchant_account_id' => $this->getCompanyMerchantAccountId(),
			'eft_instruction_id' => $this->getEftInstructionId(),
			'ar_payment_id' => $this->getArPaymentId(),
			'settlement_distribution_id' => $this->getSettlementDistributionId(),
			'company_transaction_id' => $this->getCompanyTransactionId(),
			'transaction_id' => $this->getTransactionId(),
			'clearing_transaction_id' => $this->getClearingTransactionId(),
			'account_id' => $this->getAccountId(),
			'charge_code_id' => $this->getChargeCodeId(),
			'eft_charge_id' => $this->getEftChargeId(),
			'giact_request_log_id' => $this->getGiactRequestLogId(),
			'charge_datetime' => $this->getChargeDatetime(),
			'charge_amount' => $this->getChargeAmount(),
			'cost_amount' => $this->getCostAmount(),
			'memo' => $this->getMemo(),
			'posted_on' => $this->getPostedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'currency_code' => $this->getCurrencyCode()
		);
	}

}
?>