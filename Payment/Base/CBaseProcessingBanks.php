<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CProcessingBanks
 * Do not add any new functions to this class.
 */

class CBaseProcessingBanks extends CEosPluralBase {

	/**
	 * @return CProcessingBank[]
	 */
	public static function fetchProcessingBanks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CProcessingBank::class, $objDatabase );
	}

	/**
	 * @return CProcessingBank
	 */
	public static function fetchProcessingBank( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CProcessingBank::class, $objDatabase );
	}

	public static function fetchProcessingBankCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'processing_banks', $objDatabase );
	}

	public static function fetchProcessingBankById( $intId, $objDatabase ) {
		return self::fetchProcessingBank( sprintf( 'SELECT * FROM processing_banks WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchProcessingBanksByAchMerchantGatewayId( $intAchMerchantGatewayId, $objDatabase ) {
		return self::fetchProcessingBanks( sprintf( 'SELECT * FROM processing_banks WHERE ach_merchant_gateway_id = %d', ( int ) $intAchMerchantGatewayId ), $objDatabase );
	}

	public static function fetchProcessingBanksByCcMerchantGatewayId( $intCcMerchantGatewayId, $objDatabase ) {
		return self::fetchProcessingBanks( sprintf( 'SELECT * FROM processing_banks WHERE cc_merchant_gateway_id = %d', ( int ) $intCcMerchantGatewayId ), $objDatabase );
	}

	public static function fetchProcessingBanksByX937MerchantGatewayId( $intX937MerchantGatewayId, $objDatabase ) {
		return self::fetchProcessingBanks( sprintf( 'SELECT * FROM processing_banks WHERE x937_merchant_gateway_id = %d', ( int ) $intX937MerchantGatewayId ), $objDatabase );
	}

}
?>