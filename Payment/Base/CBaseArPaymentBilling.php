<?php

class CBaseArPaymentBilling extends CEosSingularBase {

	const TABLE_NAME = 'public.ar_payment_billings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intArPaymentId;
	protected $m_strCcCardNumberEncrypted;
	protected $m_strCcExpDateMonth;
	protected $m_strCcExpDateYear;
	protected $m_strCcNameOnCard;
	protected $m_strCcBinNumber;
	protected $m_strCheckDate;
	protected $m_strCheckPayableTo;
	protected $m_strCheckBankName;
	protected $m_strCheckNameOnAccount;
	protected $m_intCheckAccountTypeId;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckAccountNumberEncrypted;
	protected $m_strCheckAccountNumberLastfourEncrypted;
	protected $m_strCheckAuxillaryOnUs;
	protected $m_strCheckEpc;
	protected $m_intCheckIsMoneyOrder;
	protected $m_intCheckIsConverted;
	protected $m_intBlockAutoAssociation;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strPaymentBankAccountId;
	protected $m_strCheckAccountNumberBindex;
	protected $m_strCheckAccountNumberLastfour;

	public function __construct() {
		parent::__construct();

		$this->m_intCheckIsMoneyOrder = '0';
		$this->m_intCheckIsConverted = '0';
		$this->m_intBlockAutoAssociation = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['cc_card_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCcCardNumberEncrypted', trim( stripcslashes( $arrValues['cc_card_number_encrypted'] ) ) ); elseif( isset( $arrValues['cc_card_number_encrypted'] ) ) $this->setCcCardNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_card_number_encrypted'] ) : $arrValues['cc_card_number_encrypted'] );
		if( isset( $arrValues['cc_exp_date_month'] ) && $boolDirectSet ) $this->set( 'm_strCcExpDateMonth', trim( stripcslashes( $arrValues['cc_exp_date_month'] ) ) ); elseif( isset( $arrValues['cc_exp_date_month'] ) ) $this->setCcExpDateMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_exp_date_month'] ) : $arrValues['cc_exp_date_month'] );
		if( isset( $arrValues['cc_exp_date_year'] ) && $boolDirectSet ) $this->set( 'm_strCcExpDateYear', trim( stripcslashes( $arrValues['cc_exp_date_year'] ) ) ); elseif( isset( $arrValues['cc_exp_date_year'] ) ) $this->setCcExpDateYear( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_exp_date_year'] ) : $arrValues['cc_exp_date_year'] );
		if( isset( $arrValues['cc_name_on_card'] ) && $boolDirectSet ) $this->set( 'm_strCcNameOnCard', trim( stripcslashes( $arrValues['cc_name_on_card'] ) ) ); elseif( isset( $arrValues['cc_name_on_card'] ) ) $this->setCcNameOnCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_name_on_card'] ) : $arrValues['cc_name_on_card'] );
		if( isset( $arrValues['cc_bin_number'] ) && $boolDirectSet ) $this->set( 'm_strCcBinNumber', trim( stripcslashes( $arrValues['cc_bin_number'] ) ) ); elseif( isset( $arrValues['cc_bin_number'] ) ) $this->setCcBinNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_bin_number'] ) : $arrValues['cc_bin_number'] );
		if( isset( $arrValues['check_date'] ) && $boolDirectSet ) $this->set( 'm_strCheckDate', trim( $arrValues['check_date'] ) ); elseif( isset( $arrValues['check_date'] ) ) $this->setCheckDate( $arrValues['check_date'] );
		if( isset( $arrValues['check_payable_to'] ) && $boolDirectSet ) $this->set( 'm_strCheckPayableTo', trim( stripcslashes( $arrValues['check_payable_to'] ) ) ); elseif( isset( $arrValues['check_payable_to'] ) ) $this->setCheckPayableTo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_payable_to'] ) : $arrValues['check_payable_to'] );
		if( isset( $arrValues['check_bank_name'] ) && $boolDirectSet ) $this->set( 'm_strCheckBankName', trim( stripcslashes( $arrValues['check_bank_name'] ) ) ); elseif( isset( $arrValues['check_bank_name'] ) ) $this->setCheckBankName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_bank_name'] ) : $arrValues['check_bank_name'] );
		if( isset( $arrValues['check_name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strCheckNameOnAccount', trim( stripcslashes( $arrValues['check_name_on_account'] ) ) ); elseif( isset( $arrValues['check_name_on_account'] ) ) $this->setCheckNameOnAccount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_name_on_account'] ) : $arrValues['check_name_on_account'] );
		if( isset( $arrValues['check_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckAccountTypeId', trim( $arrValues['check_account_type_id'] ) ); elseif( isset( $arrValues['check_account_type_id'] ) ) $this->setCheckAccountTypeId( $arrValues['check_account_type_id'] );
		if( isset( $arrValues['check_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckRoutingNumber', trim( stripcslashes( $arrValues['check_routing_number'] ) ) ); elseif( isset( $arrValues['check_routing_number'] ) ) $this->setCheckRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_routing_number'] ) : $arrValues['check_routing_number'] );
		if( isset( $arrValues['check_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberEncrypted', trim( stripcslashes( $arrValues['check_account_number_encrypted'] ) ) ); elseif( isset( $arrValues['check_account_number_encrypted'] ) ) $this->setCheckAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_account_number_encrypted'] ) : $arrValues['check_account_number_encrypted'] );
		if( isset( $arrValues['check_account_number_lastfour_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberLastfourEncrypted', trim( stripcslashes( $arrValues['check_account_number_lastfour_encrypted'] ) ) ); elseif( isset( $arrValues['check_account_number_lastfour_encrypted'] ) ) $this->setCheckAccountNumberLastfourEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_account_number_lastfour_encrypted'] ) : $arrValues['check_account_number_lastfour_encrypted'] );
		if( isset( $arrValues['check_auxillary_on_us'] ) && $boolDirectSet ) $this->set( 'm_strCheckAuxillaryOnUs', trim( stripcslashes( $arrValues['check_auxillary_on_us'] ) ) ); elseif( isset( $arrValues['check_auxillary_on_us'] ) ) $this->setCheckAuxillaryOnUs( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_auxillary_on_us'] ) : $arrValues['check_auxillary_on_us'] );
		if( isset( $arrValues['check_epc'] ) && $boolDirectSet ) $this->set( 'm_strCheckEpc', trim( stripcslashes( $arrValues['check_epc'] ) ) ); elseif( isset( $arrValues['check_epc'] ) ) $this->setCheckEpc( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_epc'] ) : $arrValues['check_epc'] );
		if( isset( $arrValues['check_is_money_order'] ) && $boolDirectSet ) $this->set( 'm_intCheckIsMoneyOrder', trim( $arrValues['check_is_money_order'] ) ); elseif( isset( $arrValues['check_is_money_order'] ) ) $this->setCheckIsMoneyOrder( $arrValues['check_is_money_order'] );
		if( isset( $arrValues['check_is_converted'] ) && $boolDirectSet ) $this->set( 'm_intCheckIsConverted', trim( $arrValues['check_is_converted'] ) ); elseif( isset( $arrValues['check_is_converted'] ) ) $this->setCheckIsConverted( $arrValues['check_is_converted'] );
		if( isset( $arrValues['block_auto_association'] ) && $boolDirectSet ) $this->set( 'm_intBlockAutoAssociation', trim( $arrValues['block_auto_association'] ) ); elseif( isset( $arrValues['block_auto_association'] ) ) $this->setBlockAutoAssociation( $arrValues['block_auto_association'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['payment_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_strPaymentBankAccountId', trim( $arrValues['payment_bank_account_id'] ) ); elseif( isset( $arrValues['payment_bank_account_id'] ) ) $this->setPaymentBankAccountId( $arrValues['payment_bank_account_id'] );
		if( isset( $arrValues['check_account_number_bindex'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberBindex', trim( $arrValues['check_account_number_bindex'] ) ); elseif( isset( $arrValues['check_account_number_bindex'] ) ) $this->setCheckAccountNumberBindex( $arrValues['check_account_number_bindex'] );
		if( isset( $arrValues['check_account_number_lastfour'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberLastfour', trim( $arrValues['check_account_number_lastfour'] ) ); elseif( isset( $arrValues['check_account_number_lastfour'] ) ) $this->setCheckAccountNumberLastfour( $arrValues['check_account_number_lastfour'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setCcCardNumberEncrypted( $strCcCardNumberEncrypted ) {
		$this->set( 'm_strCcCardNumberEncrypted', CStrings::strTrimDef( $strCcCardNumberEncrypted, 240, NULL, true ) );
	}

	public function getCcCardNumberEncrypted() {
		return $this->m_strCcCardNumberEncrypted;
	}

	public function sqlCcCardNumberEncrypted() {
		return ( true == isset( $this->m_strCcCardNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCcCardNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setCcExpDateMonth( $strCcExpDateMonth ) {
		$this->set( 'm_strCcExpDateMonth', CStrings::strTrimDef( $strCcExpDateMonth, 2, NULL, true ) );
	}

	public function getCcExpDateMonth() {
		return $this->m_strCcExpDateMonth;
	}

	public function sqlCcExpDateMonth() {
		return ( true == isset( $this->m_strCcExpDateMonth ) ) ? '\'' . addslashes( $this->m_strCcExpDateMonth ) . '\'' : 'NULL';
	}

	public function setCcExpDateYear( $strCcExpDateYear ) {
		$this->set( 'm_strCcExpDateYear', CStrings::strTrimDef( $strCcExpDateYear, 4, NULL, true ) );
	}

	public function getCcExpDateYear() {
		return $this->m_strCcExpDateYear;
	}

	public function sqlCcExpDateYear() {
		return ( true == isset( $this->m_strCcExpDateYear ) ) ? '\'' . addslashes( $this->m_strCcExpDateYear ) . '\'' : 'NULL';
	}

	public function setCcNameOnCard( $strCcNameOnCard ) {
		$this->set( 'm_strCcNameOnCard', CStrings::strTrimDef( $strCcNameOnCard, 50, NULL, true ) );
	}

	public function getCcNameOnCard() {
		return $this->m_strCcNameOnCard;
	}

	public function sqlCcNameOnCard() {
		return ( true == isset( $this->m_strCcNameOnCard ) ) ? '\'' . addslashes( $this->m_strCcNameOnCard ) . '\'' : 'NULL';
	}

	public function setCcBinNumber( $strCcBinNumber ) {
		$this->set( 'm_strCcBinNumber', CStrings::strTrimDef( $strCcBinNumber, 6, NULL, true ) );
	}

	public function getCcBinNumber() {
		return $this->m_strCcBinNumber;
	}

	public function sqlCcBinNumber() {
		return ( true == isset( $this->m_strCcBinNumber ) ) ? '\'' . addslashes( $this->m_strCcBinNumber ) . '\'' : 'NULL';
	}

	public function setCheckDate( $strCheckDate ) {
		$this->set( 'm_strCheckDate', CStrings::strTrimDef( $strCheckDate, -1, NULL, true ) );
	}

	public function getCheckDate() {
		return $this->m_strCheckDate;
	}

	public function sqlCheckDate() {
		return ( true == isset( $this->m_strCheckDate ) ) ? '\'' . $this->m_strCheckDate . '\'' : 'NULL';
	}

	public function setCheckPayableTo( $strCheckPayableTo ) {
		$this->set( 'm_strCheckPayableTo', CStrings::strTrimDef( $strCheckPayableTo, 240, NULL, true ) );
	}

	public function getCheckPayableTo() {
		return $this->m_strCheckPayableTo;
	}

	public function sqlCheckPayableTo() {
		return ( true == isset( $this->m_strCheckPayableTo ) ) ? '\'' . addslashes( $this->m_strCheckPayableTo ) . '\'' : 'NULL';
	}

	public function setCheckBankName( $strCheckBankName ) {
		$this->set( 'm_strCheckBankName', CStrings::strTrimDef( $strCheckBankName, 100, NULL, true ) );
	}

	public function getCheckBankName() {
		return $this->m_strCheckBankName;
	}

	public function sqlCheckBankName() {
		return ( true == isset( $this->m_strCheckBankName ) ) ? '\'' . addslashes( $this->m_strCheckBankName ) . '\'' : 'NULL';
	}

	public function setCheckNameOnAccount( $strCheckNameOnAccount ) {
		$this->set( 'm_strCheckNameOnAccount', CStrings::strTrimDef( $strCheckNameOnAccount, 50, NULL, true ) );
	}

	public function getCheckNameOnAccount() {
		return $this->m_strCheckNameOnAccount;
	}

	public function sqlCheckNameOnAccount() {
		return ( true == isset( $this->m_strCheckNameOnAccount ) ) ? '\'' . addslashes( $this->m_strCheckNameOnAccount ) . '\'' : 'NULL';
	}

	public function setCheckAccountTypeId( $intCheckAccountTypeId ) {
		$this->set( 'm_intCheckAccountTypeId', CStrings::strToIntDef( $intCheckAccountTypeId, NULL, false ) );
	}

	public function getCheckAccountTypeId() {
		return $this->m_intCheckAccountTypeId;
	}

	public function sqlCheckAccountTypeId() {
		return ( true == isset( $this->m_intCheckAccountTypeId ) ) ? ( string ) $this->m_intCheckAccountTypeId : 'NULL';
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->set( 'm_strCheckRoutingNumber', CStrings::strTrimDef( $strCheckRoutingNumber, 240, NULL, true ) );
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function sqlCheckRoutingNumber() {
		return ( true == isset( $this->m_strCheckRoutingNumber ) ) ? '\'' . addslashes( $this->m_strCheckRoutingNumber ) . '\'' : 'NULL';
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->set( 'm_strCheckAccountNumberEncrypted', CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function sqlCheckAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCheckAccountNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setCheckAccountNumberLastfourEncrypted( $strCheckAccountNumberLastfourEncrypted ) {
		$this->set( 'm_strCheckAccountNumberLastfourEncrypted', CStrings::strTrimDef( $strCheckAccountNumberLastfourEncrypted, 240, NULL, true ) );
	}

	public function getCheckAccountNumberLastfourEncrypted() {
		return $this->m_strCheckAccountNumberLastfourEncrypted;
	}

	public function sqlCheckAccountNumberLastfourEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberLastfourEncrypted ) ) ? '\'' . addslashes( $this->m_strCheckAccountNumberLastfourEncrypted ) . '\'' : 'NULL';
	}

	public function setCheckAuxillaryOnUs( $strCheckAuxillaryOnUs ) {
		$this->set( 'm_strCheckAuxillaryOnUs', CStrings::strTrimDef( $strCheckAuxillaryOnUs, 240, NULL, true ) );
	}

	public function getCheckAuxillaryOnUs() {
		return $this->m_strCheckAuxillaryOnUs;
	}

	public function sqlCheckAuxillaryOnUs() {
		return ( true == isset( $this->m_strCheckAuxillaryOnUs ) ) ? '\'' . addslashes( $this->m_strCheckAuxillaryOnUs ) . '\'' : 'NULL';
	}

	public function setCheckEpc( $strCheckEpc ) {
		$this->set( 'm_strCheckEpc', CStrings::strTrimDef( $strCheckEpc, 240, NULL, true ) );
	}

	public function getCheckEpc() {
		return $this->m_strCheckEpc;
	}

	public function sqlCheckEpc() {
		return ( true == isset( $this->m_strCheckEpc ) ) ? '\'' . addslashes( $this->m_strCheckEpc ) . '\'' : 'NULL';
	}

	public function setCheckIsMoneyOrder( $intCheckIsMoneyOrder ) {
		$this->set( 'm_intCheckIsMoneyOrder', CStrings::strToIntDef( $intCheckIsMoneyOrder, NULL, false ) );
	}

	public function getCheckIsMoneyOrder() {
		return $this->m_intCheckIsMoneyOrder;
	}

	public function sqlCheckIsMoneyOrder() {
		return ( true == isset( $this->m_intCheckIsMoneyOrder ) ) ? ( string ) $this->m_intCheckIsMoneyOrder : '0';
	}

	public function setCheckIsConverted( $intCheckIsConverted ) {
		$this->set( 'm_intCheckIsConverted', CStrings::strToIntDef( $intCheckIsConverted, NULL, false ) );
	}

	public function getCheckIsConverted() {
		return $this->m_intCheckIsConverted;
	}

	public function sqlCheckIsConverted() {
		return ( true == isset( $this->m_intCheckIsConverted ) ) ? ( string ) $this->m_intCheckIsConverted : '0';
	}

	public function setBlockAutoAssociation( $intBlockAutoAssociation ) {
		$this->set( 'm_intBlockAutoAssociation', CStrings::strToIntDef( $intBlockAutoAssociation, NULL, false ) );
	}

	public function getBlockAutoAssociation() {
		return $this->m_intBlockAutoAssociation;
	}

	public function sqlBlockAutoAssociation() {
		return ( true == isset( $this->m_intBlockAutoAssociation ) ) ? ( string ) $this->m_intBlockAutoAssociation : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPaymentBankAccountId( $strPaymentBankAccountId ) {
		$this->set( 'm_strPaymentBankAccountId', CStrings::strTrimDef( $strPaymentBankAccountId, NULL, NULL, true ) );
	}

	public function getPaymentBankAccountId() {
		return $this->m_strPaymentBankAccountId;
	}

	public function sqlPaymentBankAccountId() {
		return ( true == isset( $this->m_strPaymentBankAccountId ) ) ? '\'' . addslashes( $this->m_strPaymentBankAccountId ) . '\'' : 'NULL';
	}

	public function setCheckAccountNumberBindex( $strCheckAccountNumberBindex ) {
		$this->set( 'm_strCheckAccountNumberBindex', CStrings::strTrimDef( $strCheckAccountNumberBindex, 240, NULL, true ) );
	}

	public function getCheckAccountNumberBindex() {
		return $this->m_strCheckAccountNumberBindex;
	}

	public function sqlCheckAccountNumberBindex() {
		return ( true == isset( $this->m_strCheckAccountNumberBindex ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckAccountNumberBindex ) : '\'' . addslashes( $this->m_strCheckAccountNumberBindex ) . '\'' ) : 'NULL';
	}

	public function setCheckAccountNumberLastfour( $strCheckAccountNumberLastfour ) {
		$this->set( 'm_strCheckAccountNumberLastfour', CStrings::strTrimDef( $strCheckAccountNumberLastfour, 25, NULL, true ) );
	}

	public function getCheckAccountNumberLastfour() {
		return $this->m_strCheckAccountNumberLastfour;
	}

	public function sqlCheckAccountNumberLastfour() {
		return ( true == isset( $this->m_strCheckAccountNumberLastfour ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckAccountNumberLastfour ) : '\'' . addslashes( $this->m_strCheckAccountNumberLastfour ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ar_payment_id, cc_card_number_encrypted, cc_exp_date_month, cc_exp_date_year, cc_name_on_card, cc_bin_number, check_date, check_payable_to, check_bank_name, check_name_on_account, check_account_type_id, check_routing_number, check_account_number_encrypted, check_account_number_lastfour_encrypted, check_auxillary_on_us, check_epc, check_is_money_order, check_is_converted, block_auto_association, updated_by, updated_on, created_by, created_on, payment_bank_account_id, check_account_number_bindex, check_account_number_lastfour )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlArPaymentId() . ', ' .
						$this->sqlCcCardNumberEncrypted() . ', ' .
						$this->sqlCcExpDateMonth() . ', ' .
						$this->sqlCcExpDateYear() . ', ' .
						$this->sqlCcNameOnCard() . ', ' .
						$this->sqlCcBinNumber() . ', ' .
						$this->sqlCheckDate() . ', ' .
						$this->sqlCheckPayableTo() . ', ' .
						$this->sqlCheckBankName() . ', ' .
						$this->sqlCheckNameOnAccount() . ', ' .
						$this->sqlCheckAccountTypeId() . ', ' .
						$this->sqlCheckRoutingNumber() . ', ' .
						$this->sqlCheckAccountNumberEncrypted() . ', ' .
						$this->sqlCheckAccountNumberLastfourEncrypted() . ', ' .
						$this->sqlCheckAuxillaryOnUs() . ', ' .
						$this->sqlCheckEpc() . ', ' .
						$this->sqlCheckIsMoneyOrder() . ', ' .
						$this->sqlCheckIsConverted() . ', ' .
						$this->sqlBlockAutoAssociation() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlPaymentBankAccountId() . ', ' .
						$this->sqlCheckAccountNumberBindex() . ', ' .
						$this->sqlCheckAccountNumberLastfour() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId(). ',' ; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_card_number_encrypted = ' . $this->sqlCcCardNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'CcCardNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' cc_card_number_encrypted = ' . $this->sqlCcCardNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_exp_date_month = ' . $this->sqlCcExpDateMonth(). ',' ; } elseif( true == array_key_exists( 'CcExpDateMonth', $this->getChangedColumns() ) ) { $strSql .= ' cc_exp_date_month = ' . $this->sqlCcExpDateMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_exp_date_year = ' . $this->sqlCcExpDateYear(). ',' ; } elseif( true == array_key_exists( 'CcExpDateYear', $this->getChangedColumns() ) ) { $strSql .= ' cc_exp_date_year = ' . $this->sqlCcExpDateYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_name_on_card = ' . $this->sqlCcNameOnCard(). ',' ; } elseif( true == array_key_exists( 'CcNameOnCard', $this->getChangedColumns() ) ) { $strSql .= ' cc_name_on_card = ' . $this->sqlCcNameOnCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_bin_number = ' . $this->sqlCcBinNumber(). ',' ; } elseif( true == array_key_exists( 'CcBinNumber', $this->getChangedColumns() ) ) { $strSql .= ' cc_bin_number = ' . $this->sqlCcBinNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_date = ' . $this->sqlCheckDate(). ',' ; } elseif( true == array_key_exists( 'CheckDate', $this->getChangedColumns() ) ) { $strSql .= ' check_date = ' . $this->sqlCheckDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_payable_to = ' . $this->sqlCheckPayableTo(). ',' ; } elseif( true == array_key_exists( 'CheckPayableTo', $this->getChangedColumns() ) ) { $strSql .= ' check_payable_to = ' . $this->sqlCheckPayableTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName(). ',' ; } elseif( true == array_key_exists( 'CheckBankName', $this->getChangedColumns() ) ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount(). ',' ; } elseif( true == array_key_exists( 'CheckNameOnAccount', $this->getChangedColumns() ) ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId(). ',' ; } elseif( true == array_key_exists( 'CheckAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber(). ',' ; } elseif( true == array_key_exists( 'CheckRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'CheckAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_lastfour_encrypted = ' . $this->sqlCheckAccountNumberLastfourEncrypted(). ',' ; } elseif( true == array_key_exists( 'CheckAccountNumberLastfourEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_lastfour_encrypted = ' . $this->sqlCheckAccountNumberLastfourEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_auxillary_on_us = ' . $this->sqlCheckAuxillaryOnUs(). ',' ; } elseif( true == array_key_exists( 'CheckAuxillaryOnUs', $this->getChangedColumns() ) ) { $strSql .= ' check_auxillary_on_us = ' . $this->sqlCheckAuxillaryOnUs() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_epc = ' . $this->sqlCheckEpc(). ',' ; } elseif( true == array_key_exists( 'CheckEpc', $this->getChangedColumns() ) ) { $strSql .= ' check_epc = ' . $this->sqlCheckEpc() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_is_money_order = ' . $this->sqlCheckIsMoneyOrder(). ',' ; } elseif( true == array_key_exists( 'CheckIsMoneyOrder', $this->getChangedColumns() ) ) { $strSql .= ' check_is_money_order = ' . $this->sqlCheckIsMoneyOrder() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_is_converted = ' . $this->sqlCheckIsConverted(). ',' ; } elseif( true == array_key_exists( 'CheckIsConverted', $this->getChangedColumns() ) ) { $strSql .= ' check_is_converted = ' . $this->sqlCheckIsConverted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_auto_association = ' . $this->sqlBlockAutoAssociation(). ',' ; } elseif( true == array_key_exists( 'BlockAutoAssociation', $this->getChangedColumns() ) ) { $strSql .= ' block_auto_association = ' . $this->sqlBlockAutoAssociation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_bank_account_id = ' . $this->sqlPaymentBankAccountId(). ',' ; } elseif( true == array_key_exists( 'PaymentBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' payment_bank_account_id = ' . $this->sqlPaymentBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_bindex = ' . $this->sqlCheckAccountNumberBindex(). ',' ; } elseif( true == array_key_exists( 'CheckAccountNumberBindex', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_bindex = ' . $this->sqlCheckAccountNumberBindex() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_lastfour = ' . $this->sqlCheckAccountNumberLastfour(). ',' ; } elseif( true == array_key_exists( 'CheckAccountNumberLastfour', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_lastfour = ' . $this->sqlCheckAccountNumberLastfour() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ar_payment_id' => $this->getArPaymentId(),
			'cc_card_number_encrypted' => $this->getCcCardNumberEncrypted(),
			'cc_exp_date_month' => $this->getCcExpDateMonth(),
			'cc_exp_date_year' => $this->getCcExpDateYear(),
			'cc_name_on_card' => $this->getCcNameOnCard(),
			'cc_bin_number' => $this->getCcBinNumber(),
			'check_date' => $this->getCheckDate(),
			'check_payable_to' => $this->getCheckPayableTo(),
			'check_bank_name' => $this->getCheckBankName(),
			'check_name_on_account' => $this->getCheckNameOnAccount(),
			'check_account_type_id' => $this->getCheckAccountTypeId(),
			'check_routing_number' => $this->getCheckRoutingNumber(),
			'check_account_number_encrypted' => $this->getCheckAccountNumberEncrypted(),
			'check_account_number_lastfour_encrypted' => $this->getCheckAccountNumberLastfourEncrypted(),
			'check_auxillary_on_us' => $this->getCheckAuxillaryOnUs(),
			'check_epc' => $this->getCheckEpc(),
			'check_is_money_order' => $this->getCheckIsMoneyOrder(),
			'check_is_converted' => $this->getCheckIsConverted(),
			'block_auto_association' => $this->getBlockAutoAssociation(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'payment_bank_account_id' => $this->getPaymentBankAccountId(),
			'check_account_number_bindex' => $this->getCheckAccountNumberBindex(),
			'check_account_number_lastfour' => $this->getCheckAccountNumberLastfour()
		);
	}

}
?>