<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CApplicationKeyValues
 * Do not add any new functions to this class.
 */

class CBaseApplicationKeyValues extends CEosPluralBase {

	/**
	 * @return CApplicationKeyValue[]
	 */
	public static function fetchApplicationKeyValues( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApplicationKeyValue::class, $objDatabase );
	}

	/**
	 * @return CApplicationKeyValue
	 */
	public static function fetchApplicationKeyValue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplicationKeyValue::class, $objDatabase );
	}

	public static function fetchApplicationKeyValueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_key_values', $objDatabase );
	}

	public static function fetchApplicationKeyValueById( $intId, $objDatabase ) {
		return self::fetchApplicationKeyValue( sprintf( 'SELECT * FROM application_key_values WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchApplicationKeyValuesByCid( $intCid, $objDatabase ) {
		return self::fetchApplicationKeyValues( sprintf( 'SELECT * FROM application_key_values WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationKeyValuesByMerchantAccountApplicationId( $intMerchantAccountApplicationId, $objDatabase ) {
		return self::fetchApplicationKeyValues( sprintf( 'SELECT * FROM application_key_values WHERE merchant_account_application_id = %d', ( int ) $intMerchantAccountApplicationId ), $objDatabase );
	}

}
?>