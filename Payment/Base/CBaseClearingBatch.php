<?php

class CBaseClearingBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.clearing_batches';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intProcessingBankId;
	protected $m_intCompanyMerchantAccountId;
	protected $m_intAccountId;
	protected $m_intClearingBatchTypeId;
	protected $m_intInvoiceId;
	protected $m_intReturnTypeId;
	protected $m_fltClearingAmount;
	protected $m_strBatchedOn;
	protected $m_strReturnedOn;
	protected $m_strNotifiedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltClearingAmount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['processing_bank_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankId', trim( $arrValues['processing_bank_id'] ) ); elseif( isset( $arrValues['processing_bank_id'] ) ) $this->setProcessingBankId( $arrValues['processing_bank_id'] );
		if( isset( $arrValues['company_merchant_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMerchantAccountId', trim( $arrValues['company_merchant_account_id'] ) ); elseif( isset( $arrValues['company_merchant_account_id'] ) ) $this->setCompanyMerchantAccountId( $arrValues['company_merchant_account_id'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['clearing_batch_type_id'] ) && $boolDirectSet ) $this->set( 'm_intClearingBatchTypeId', trim( $arrValues['clearing_batch_type_id'] ) ); elseif( isset( $arrValues['clearing_batch_type_id'] ) ) $this->setClearingBatchTypeId( $arrValues['clearing_batch_type_id'] );
		if( isset( $arrValues['invoice_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceId', trim( $arrValues['invoice_id'] ) ); elseif( isset( $arrValues['invoice_id'] ) ) $this->setInvoiceId( $arrValues['invoice_id'] );
		if( isset( $arrValues['return_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReturnTypeId', trim( $arrValues['return_type_id'] ) ); elseif( isset( $arrValues['return_type_id'] ) ) $this->setReturnTypeId( $arrValues['return_type_id'] );
		if( isset( $arrValues['clearing_amount'] ) && $boolDirectSet ) $this->set( 'm_fltClearingAmount', trim( $arrValues['clearing_amount'] ) ); elseif( isset( $arrValues['clearing_amount'] ) ) $this->setClearingAmount( $arrValues['clearing_amount'] );
		if( isset( $arrValues['batched_on'] ) && $boolDirectSet ) $this->set( 'm_strBatchedOn', trim( $arrValues['batched_on'] ) ); elseif( isset( $arrValues['batched_on'] ) ) $this->setBatchedOn( $arrValues['batched_on'] );
		if( isset( $arrValues['returned_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnedOn', trim( $arrValues['returned_on'] ) ); elseif( isset( $arrValues['returned_on'] ) ) $this->setReturnedOn( $arrValues['returned_on'] );
		if( isset( $arrValues['notified_on'] ) && $boolDirectSet ) $this->set( 'm_strNotifiedOn', trim( $arrValues['notified_on'] ) ); elseif( isset( $arrValues['notified_on'] ) ) $this->setNotifiedOn( $arrValues['notified_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setProcessingBankId( $intProcessingBankId ) {
		$this->set( 'm_intProcessingBankId', CStrings::strToIntDef( $intProcessingBankId, NULL, false ) );
	}

	public function getProcessingBankId() {
		return $this->m_intProcessingBankId;
	}

	public function sqlProcessingBankId() {
		return ( true == isset( $this->m_intProcessingBankId ) ) ? ( string ) $this->m_intProcessingBankId : 'NULL';
	}

	public function setCompanyMerchantAccountId( $intCompanyMerchantAccountId ) {
		$this->set( 'm_intCompanyMerchantAccountId', CStrings::strToIntDef( $intCompanyMerchantAccountId, NULL, false ) );
	}

	public function getCompanyMerchantAccountId() {
		return $this->m_intCompanyMerchantAccountId;
	}

	public function sqlCompanyMerchantAccountId() {
		return ( true == isset( $this->m_intCompanyMerchantAccountId ) ) ? ( string ) $this->m_intCompanyMerchantAccountId : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setClearingBatchTypeId( $intClearingBatchTypeId ) {
		$this->set( 'm_intClearingBatchTypeId', CStrings::strToIntDef( $intClearingBatchTypeId, NULL, false ) );
	}

	public function getClearingBatchTypeId() {
		return $this->m_intClearingBatchTypeId;
	}

	public function sqlClearingBatchTypeId() {
		return ( true == isset( $this->m_intClearingBatchTypeId ) ) ? ( string ) $this->m_intClearingBatchTypeId : 'NULL';
	}

	public function setInvoiceId( $intInvoiceId ) {
		$this->set( 'm_intInvoiceId', CStrings::strToIntDef( $intInvoiceId, NULL, false ) );
	}

	public function getInvoiceId() {
		return $this->m_intInvoiceId;
	}

	public function sqlInvoiceId() {
		return ( true == isset( $this->m_intInvoiceId ) ) ? ( string ) $this->m_intInvoiceId : 'NULL';
	}

	public function setReturnTypeId( $intReturnTypeId ) {
		$this->set( 'm_intReturnTypeId', CStrings::strToIntDef( $intReturnTypeId, NULL, false ) );
	}

	public function getReturnTypeId() {
		return $this->m_intReturnTypeId;
	}

	public function sqlReturnTypeId() {
		return ( true == isset( $this->m_intReturnTypeId ) ) ? ( string ) $this->m_intReturnTypeId : 'NULL';
	}

	public function setClearingAmount( $fltClearingAmount ) {
		$this->set( 'm_fltClearingAmount', CStrings::strToFloatDef( $fltClearingAmount, NULL, false, 4 ) );
	}

	public function getClearingAmount() {
		return $this->m_fltClearingAmount;
	}

	public function sqlClearingAmount() {
		return ( true == isset( $this->m_fltClearingAmount ) ) ? ( string ) $this->m_fltClearingAmount : '0';
	}

	public function setBatchedOn( $strBatchedOn ) {
		$this->set( 'm_strBatchedOn', CStrings::strTrimDef( $strBatchedOn, -1, NULL, true ) );
	}

	public function getBatchedOn() {
		return $this->m_strBatchedOn;
	}

	public function sqlBatchedOn() {
		return ( true == isset( $this->m_strBatchedOn ) ) ? '\'' . $this->m_strBatchedOn . '\'' : 'NULL';
	}

	public function setReturnedOn( $strReturnedOn ) {
		$this->set( 'm_strReturnedOn', CStrings::strTrimDef( $strReturnedOn, -1, NULL, true ) );
	}

	public function getReturnedOn() {
		return $this->m_strReturnedOn;
	}

	public function sqlReturnedOn() {
		return ( true == isset( $this->m_strReturnedOn ) ) ? '\'' . $this->m_strReturnedOn . '\'' : 'NULL';
	}

	public function setNotifiedOn( $strNotifiedOn ) {
		$this->set( 'm_strNotifiedOn', CStrings::strTrimDef( $strNotifiedOn, -1, NULL, true ) );
	}

	public function getNotifiedOn() {
		return $this->m_strNotifiedOn;
	}

	public function sqlNotifiedOn() {
		return ( true == isset( $this->m_strNotifiedOn ) ) ? '\'' . $this->m_strNotifiedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, processing_bank_id, company_merchant_account_id, account_id, clearing_batch_type_id, invoice_id, return_type_id, clearing_amount, batched_on, returned_on, notified_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlProcessingBankId() . ', ' .
 						$this->sqlCompanyMerchantAccountId() . ', ' .
 						$this->sqlAccountId() . ', ' .
 						$this->sqlClearingBatchTypeId() . ', ' .
 						$this->sqlInvoiceId() . ', ' .
 						$this->sqlReturnTypeId() . ', ' .
 						$this->sqlClearingAmount() . ', ' .
 						$this->sqlBatchedOn() . ', ' .
 						$this->sqlReturnedOn() . ', ' .
 						$this->sqlNotifiedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; } elseif( true == array_key_exists( 'ProcessingBankId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_merchant_account_id = ' . $this->sqlCompanyMerchantAccountId() . ','; } elseif( true == array_key_exists( 'CompanyMerchantAccountId', $this->getChangedColumns() ) ) { $strSql .= ' company_merchant_account_id = ' . $this->sqlCompanyMerchantAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clearing_batch_type_id = ' . $this->sqlClearingBatchTypeId() . ','; } elseif( true == array_key_exists( 'ClearingBatchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' clearing_batch_type_id = ' . $this->sqlClearingBatchTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_id = ' . $this->sqlInvoiceId() . ','; } elseif( true == array_key_exists( 'InvoiceId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_id = ' . $this->sqlInvoiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; } elseif( true == array_key_exists( 'ReturnTypeId', $this->getChangedColumns() ) ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clearing_amount = ' . $this->sqlClearingAmount() . ','; } elseif( true == array_key_exists( 'ClearingAmount', $this->getChangedColumns() ) ) { $strSql .= ' clearing_amount = ' . $this->sqlClearingAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batched_on = ' . $this->sqlBatchedOn() . ','; } elseif( true == array_key_exists( 'BatchedOn', $this->getChangedColumns() ) ) { $strSql .= ' batched_on = ' . $this->sqlBatchedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; } elseif( true == array_key_exists( 'ReturnedOn', $this->getChangedColumns() ) ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notified_on = ' . $this->sqlNotifiedOn() . ','; } elseif( true == array_key_exists( 'NotifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' notified_on = ' . $this->sqlNotifiedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'processing_bank_id' => $this->getProcessingBankId(),
			'company_merchant_account_id' => $this->getCompanyMerchantAccountId(),
			'account_id' => $this->getAccountId(),
			'clearing_batch_type_id' => $this->getClearingBatchTypeId(),
			'invoice_id' => $this->getInvoiceId(),
			'return_type_id' => $this->getReturnTypeId(),
			'clearing_amount' => $this->getClearingAmount(),
			'batched_on' => $this->getBatchedOn(),
			'returned_on' => $this->getReturnedOn(),
			'notified_on' => $this->getNotifiedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>