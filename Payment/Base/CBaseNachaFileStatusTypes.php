<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaFileStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseNachaFileStatusTypes extends CEosPluralBase {

	/**
	 * @return CNachaFileStatusType[]
	 */
	public static function fetchNachaFileStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CNachaFileStatusType::class, $objDatabase );
	}

	/**
	 * @return CNachaFileStatusType
	 */
	public static function fetchNachaFileStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CNachaFileStatusType::class, $objDatabase );
	}

	public static function fetchNachaFileStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'nacha_file_status_types', $objDatabase );
	}

	public static function fetchNachaFileStatusTypeById( $intId, $objDatabase ) {
		return self::fetchNachaFileStatusType( sprintf( 'SELECT * FROM nacha_file_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>