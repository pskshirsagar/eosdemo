<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMerchantProcessingTypes
 * Do not add any new functions to this class.
 */

class CBaseMerchantProcessingTypes extends CEosPluralBase {

	/**
	 * @return CMerchantProcessingType[]
	 */
	public static function fetchMerchantProcessingTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMerchantProcessingType::class, $objDatabase );
	}

	/**
	 * @return CMerchantProcessingType
	 */
	public static function fetchMerchantProcessingType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMerchantProcessingType::class, $objDatabase );
	}

	public static function fetchMerchantProcessingTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'merchant_processing_types', $objDatabase );
	}

	public static function fetchMerchantProcessingTypeById( $intId, $objDatabase ) {
		return self::fetchMerchantProcessingType( sprintf( 'SELECT * FROM merchant_processing_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>