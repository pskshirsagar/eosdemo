<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CReconciliationEntryTypes
 * Do not add any new functions to this class.
 */

class CBaseReconciliationEntryTypes extends CEosPluralBase {

	/**
	 * @return CReconciliationEntryType[]
	 */
	public static function fetchReconciliationEntryTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReconciliationEntryType::class, $objDatabase );
	}

	/**
	 * @return CReconciliationEntryType
	 */
	public static function fetchReconciliationEntryType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReconciliationEntryType::class, $objDatabase );
	}

	public static function fetchReconciliationEntryTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reconciliation_entry_types', $objDatabase );
	}

	public static function fetchReconciliationEntryTypeById( $intId, $objDatabase ) {
		return self::fetchReconciliationEntryType( sprintf( 'SELECT * FROM reconciliation_entry_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>