<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937CheckDetailRecords
 * Do not add any new functions to this class.
 */

class CBaseX937CheckDetailRecords extends CEosPluralBase {

	/**
	 * @return CX937CheckDetailRecord[]
	 */
	public static function fetchX937CheckDetailRecords( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CX937CheckDetailRecord::class, $objDatabase );
	}

	/**
	 * @return CX937CheckDetailRecord
	 */
	public static function fetchX937CheckDetailRecord( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CX937CheckDetailRecord::class, $objDatabase );
	}

	public static function fetchX937CheckDetailRecordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'x937_check_detail_records', $objDatabase );
	}

	public static function fetchX937CheckDetailRecordById( $intId, $objDatabase ) {
		return self::fetchX937CheckDetailRecord( sprintf( 'SELECT * FROM x937_check_detail_records WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchX937CheckDetailRecordsByCid( $intCid, $objDatabase ) {
		return self::fetchX937CheckDetailRecords( sprintf( 'SELECT * FROM x937_check_detail_records WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchX937CheckDetailRecordsByProcessingBankAccountId( $intProcessingBankAccountId, $objDatabase ) {
		return self::fetchX937CheckDetailRecords( sprintf( 'SELECT * FROM x937_check_detail_records WHERE processing_bank_account_id = %d', ( int ) $intProcessingBankAccountId ), $objDatabase );
	}

	public static function fetchX937CheckDetailRecordsByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchX937CheckDetailRecords( sprintf( 'SELECT * FROM x937_check_detail_records WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

	public static function fetchX937CheckDetailRecordsBySettlementDistributionId( $intSettlementDistributionId, $objDatabase ) {
		return self::fetchX937CheckDetailRecords( sprintf( 'SELECT * FROM x937_check_detail_records WHERE settlement_distribution_id = %d', ( int ) $intSettlementDistributionId ), $objDatabase );
	}

	public static function fetchX937CheckDetailRecordsByCompanyPaymentId( $intCompanyPaymentId, $objDatabase ) {
		return self::fetchX937CheckDetailRecords( sprintf( 'SELECT * FROM x937_check_detail_records WHERE company_payment_id = %d', ( int ) $intCompanyPaymentId ), $objDatabase );
	}

	public static function fetchX937CheckDetailRecordsByX937FileId( $intX937FileId, $objDatabase ) {
		return self::fetchX937CheckDetailRecords( sprintf( 'SELECT * FROM x937_check_detail_records WHERE x937_file_id = %d', ( int ) $intX937FileId ), $objDatabase );
	}

	public static function fetchX937CheckDetailRecordsByX937CashLetterId( $intX937CashLetterId, $objDatabase ) {
		return self::fetchX937CheckDetailRecords( sprintf( 'SELECT * FROM x937_check_detail_records WHERE x937_cash_letter_id = %d', ( int ) $intX937CashLetterId ), $objDatabase );
	}

	public static function fetchX937CheckDetailRecordsByX937BundleId( $intX937BundleId, $objDatabase ) {
		return self::fetchX937CheckDetailRecords( sprintf( 'SELECT * FROM x937_check_detail_records WHERE x937_bundle_id = %d', ( int ) $intX937BundleId ), $objDatabase );
	}

	public static function fetchX937CheckDetailRecordsByReturnTypeId( $intReturnTypeId, $objDatabase ) {
		return self::fetchX937CheckDetailRecords( sprintf( 'SELECT * FROM x937_check_detail_records WHERE return_type_id = %d', ( int ) $intReturnTypeId ), $objDatabase );
	}

}
?>