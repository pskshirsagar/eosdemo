<?php

class CBaseX937CashLetter extends CEosSingularBase {

	const TABLE_NAME = 'public.x937_cash_letters';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intX937FileId;
	protected $m_strHeaderRecordType;
	protected $m_strHeaderCollectionTypeIndicator;
	protected $m_strHeaderDestinationRoutingNumber;
	protected $m_strHeaderEceInstitutionRoutingNumber;
	protected $m_strHeaderBusinessDate;
	protected $m_strHeaderCreationDate;
	protected $m_strHeaderCreationTime;
	protected $m_strHeaderRecordTypeIndicator;
	protected $m_strHeaderDocumenationTypeIndicator;
	protected $m_strHeaderId;
	protected $m_strHeaderOriginatorContactName;
	protected $m_strHeaderOriginatorContactPhoneNumber;
	protected $m_strHeaderFedWorkType;
	protected $m_strHeaderUserField;
	protected $m_strHeaderReserved;
	protected $m_strControlRecordType;
	protected $m_strControlBundleCount;
	protected $m_strControlItemsCount;
	protected $m_strControlTotalAmount;
	protected $m_strControlImagesCount;
	protected $m_strControlEceInstitutionName;
	protected $m_strControlSettlementDate;
	protected $m_strControlReserved;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['x937_file_id'] ) && $boolDirectSet ) $this->set( 'm_intX937FileId', trim( $arrValues['x937_file_id'] ) ); elseif( isset( $arrValues['x937_file_id'] ) ) $this->setX937FileId( $arrValues['x937_file_id'] );
		if( isset( $arrValues['header_record_type'] ) && $boolDirectSet ) $this->set( 'm_strHeaderRecordType', trim( stripcslashes( $arrValues['header_record_type'] ) ) ); elseif( isset( $arrValues['header_record_type'] ) ) $this->setHeaderRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_record_type'] ) : $arrValues['header_record_type'] );
		if( isset( $arrValues['header_collection_type_indicator'] ) && $boolDirectSet ) $this->set( 'm_strHeaderCollectionTypeIndicator', trim( stripcslashes( $arrValues['header_collection_type_indicator'] ) ) ); elseif( isset( $arrValues['header_collection_type_indicator'] ) ) $this->setHeaderCollectionTypeIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_collection_type_indicator'] ) : $arrValues['header_collection_type_indicator'] );
		if( isset( $arrValues['header_destination_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strHeaderDestinationRoutingNumber', trim( stripcslashes( $arrValues['header_destination_routing_number'] ) ) ); elseif( isset( $arrValues['header_destination_routing_number'] ) ) $this->setHeaderDestinationRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_destination_routing_number'] ) : $arrValues['header_destination_routing_number'] );
		if( isset( $arrValues['header_ece_institution_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strHeaderEceInstitutionRoutingNumber', trim( stripcslashes( $arrValues['header_ece_institution_routing_number'] ) ) ); elseif( isset( $arrValues['header_ece_institution_routing_number'] ) ) $this->setHeaderEceInstitutionRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_ece_institution_routing_number'] ) : $arrValues['header_ece_institution_routing_number'] );
		if( isset( $arrValues['header_business_date'] ) && $boolDirectSet ) $this->set( 'm_strHeaderBusinessDate', trim( stripcslashes( $arrValues['header_business_date'] ) ) ); elseif( isset( $arrValues['header_business_date'] ) ) $this->setHeaderBusinessDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_business_date'] ) : $arrValues['header_business_date'] );
		if( isset( $arrValues['header_creation_date'] ) && $boolDirectSet ) $this->set( 'm_strHeaderCreationDate', trim( stripcslashes( $arrValues['header_creation_date'] ) ) ); elseif( isset( $arrValues['header_creation_date'] ) ) $this->setHeaderCreationDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_creation_date'] ) : $arrValues['header_creation_date'] );
		if( isset( $arrValues['header_creation_time'] ) && $boolDirectSet ) $this->set( 'm_strHeaderCreationTime', trim( stripcslashes( $arrValues['header_creation_time'] ) ) ); elseif( isset( $arrValues['header_creation_time'] ) ) $this->setHeaderCreationTime( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_creation_time'] ) : $arrValues['header_creation_time'] );
		if( isset( $arrValues['header_record_type_indicator'] ) && $boolDirectSet ) $this->set( 'm_strHeaderRecordTypeIndicator', trim( stripcslashes( $arrValues['header_record_type_indicator'] ) ) ); elseif( isset( $arrValues['header_record_type_indicator'] ) ) $this->setHeaderRecordTypeIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_record_type_indicator'] ) : $arrValues['header_record_type_indicator'] );
		if( isset( $arrValues['header_documenation_type_indicator'] ) && $boolDirectSet ) $this->set( 'm_strHeaderDocumenationTypeIndicator', trim( stripcslashes( $arrValues['header_documenation_type_indicator'] ) ) ); elseif( isset( $arrValues['header_documenation_type_indicator'] ) ) $this->setHeaderDocumenationTypeIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_documenation_type_indicator'] ) : $arrValues['header_documenation_type_indicator'] );
		if( isset( $arrValues['header_id'] ) && $boolDirectSet ) $this->set( 'm_strHeaderId', trim( stripcslashes( $arrValues['header_id'] ) ) ); elseif( isset( $arrValues['header_id'] ) ) $this->setHeaderId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_id'] ) : $arrValues['header_id'] );
		if( isset( $arrValues['header_originator_contact_name'] ) && $boolDirectSet ) $this->set( 'm_strHeaderOriginatorContactName', trim( stripcslashes( $arrValues['header_originator_contact_name'] ) ) ); elseif( isset( $arrValues['header_originator_contact_name'] ) ) $this->setHeaderOriginatorContactName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_originator_contact_name'] ) : $arrValues['header_originator_contact_name'] );
		if( isset( $arrValues['header_originator_contact_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strHeaderOriginatorContactPhoneNumber', trim( stripcslashes( $arrValues['header_originator_contact_phone_number'] ) ) ); elseif( isset( $arrValues['header_originator_contact_phone_number'] ) ) $this->setHeaderOriginatorContactPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_originator_contact_phone_number'] ) : $arrValues['header_originator_contact_phone_number'] );
		if( isset( $arrValues['header_fed_work_type'] ) && $boolDirectSet ) $this->set( 'm_strHeaderFedWorkType', trim( stripcslashes( $arrValues['header_fed_work_type'] ) ) ); elseif( isset( $arrValues['header_fed_work_type'] ) ) $this->setHeaderFedWorkType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_fed_work_type'] ) : $arrValues['header_fed_work_type'] );
		if( isset( $arrValues['header_user_field'] ) && $boolDirectSet ) $this->set( 'm_strHeaderUserField', trim( stripcslashes( $arrValues['header_user_field'] ) ) ); elseif( isset( $arrValues['header_user_field'] ) ) $this->setHeaderUserField( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_user_field'] ) : $arrValues['header_user_field'] );
		if( isset( $arrValues['header_reserved'] ) && $boolDirectSet ) $this->set( 'm_strHeaderReserved', trim( stripcslashes( $arrValues['header_reserved'] ) ) ); elseif( isset( $arrValues['header_reserved'] ) ) $this->setHeaderReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_reserved'] ) : $arrValues['header_reserved'] );
		if( isset( $arrValues['control_record_type'] ) && $boolDirectSet ) $this->set( 'm_strControlRecordType', trim( stripcslashes( $arrValues['control_record_type'] ) ) ); elseif( isset( $arrValues['control_record_type'] ) ) $this->setControlRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_record_type'] ) : $arrValues['control_record_type'] );
		if( isset( $arrValues['control_bundle_count'] ) && $boolDirectSet ) $this->set( 'm_strControlBundleCount', trim( stripcslashes( $arrValues['control_bundle_count'] ) ) ); elseif( isset( $arrValues['control_bundle_count'] ) ) $this->setControlBundleCount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_bundle_count'] ) : $arrValues['control_bundle_count'] );
		if( isset( $arrValues['control_items_count'] ) && $boolDirectSet ) $this->set( 'm_strControlItemsCount', trim( stripcslashes( $arrValues['control_items_count'] ) ) ); elseif( isset( $arrValues['control_items_count'] ) ) $this->setControlItemsCount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_items_count'] ) : $arrValues['control_items_count'] );
		if( isset( $arrValues['control_total_amount'] ) && $boolDirectSet ) $this->set( 'm_strControlTotalAmount', trim( stripcslashes( $arrValues['control_total_amount'] ) ) ); elseif( isset( $arrValues['control_total_amount'] ) ) $this->setControlTotalAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_total_amount'] ) : $arrValues['control_total_amount'] );
		if( isset( $arrValues['control_images_count'] ) && $boolDirectSet ) $this->set( 'm_strControlImagesCount', trim( stripcslashes( $arrValues['control_images_count'] ) ) ); elseif( isset( $arrValues['control_images_count'] ) ) $this->setControlImagesCount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_images_count'] ) : $arrValues['control_images_count'] );
		if( isset( $arrValues['control_ece_institution_name'] ) && $boolDirectSet ) $this->set( 'm_strControlEceInstitutionName', trim( stripcslashes( $arrValues['control_ece_institution_name'] ) ) ); elseif( isset( $arrValues['control_ece_institution_name'] ) ) $this->setControlEceInstitutionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_ece_institution_name'] ) : $arrValues['control_ece_institution_name'] );
		if( isset( $arrValues['control_settlement_date'] ) && $boolDirectSet ) $this->set( 'm_strControlSettlementDate', trim( stripcslashes( $arrValues['control_settlement_date'] ) ) ); elseif( isset( $arrValues['control_settlement_date'] ) ) $this->setControlSettlementDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_settlement_date'] ) : $arrValues['control_settlement_date'] );
		if( isset( $arrValues['control_reserved'] ) && $boolDirectSet ) $this->set( 'm_strControlReserved', trim( stripcslashes( $arrValues['control_reserved'] ) ) ); elseif( isset( $arrValues['control_reserved'] ) ) $this->setControlReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_reserved'] ) : $arrValues['control_reserved'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setX937FileId( $intX937FileId ) {
		$this->set( 'm_intX937FileId', CStrings::strToIntDef( $intX937FileId, NULL, false ) );
	}

	public function getX937FileId() {
		return $this->m_intX937FileId;
	}

	public function sqlX937FileId() {
		return ( true == isset( $this->m_intX937FileId ) ) ? ( string ) $this->m_intX937FileId : 'NULL';
	}

	public function setHeaderRecordType( $strHeaderRecordType ) {
		$this->set( 'm_strHeaderRecordType', CStrings::strTrimDef( $strHeaderRecordType, 2, NULL, true ) );
	}

	public function getHeaderRecordType() {
		return $this->m_strHeaderRecordType;
	}

	public function sqlHeaderRecordType() {
		return ( true == isset( $this->m_strHeaderRecordType ) ) ? '\'' . addslashes( $this->m_strHeaderRecordType ) . '\'' : 'NULL';
	}

	public function setHeaderCollectionTypeIndicator( $strHeaderCollectionTypeIndicator ) {
		$this->set( 'm_strHeaderCollectionTypeIndicator', CStrings::strTrimDef( $strHeaderCollectionTypeIndicator, 2, NULL, true ) );
	}

	public function getHeaderCollectionTypeIndicator() {
		return $this->m_strHeaderCollectionTypeIndicator;
	}

	public function sqlHeaderCollectionTypeIndicator() {
		return ( true == isset( $this->m_strHeaderCollectionTypeIndicator ) ) ? '\'' . addslashes( $this->m_strHeaderCollectionTypeIndicator ) . '\'' : 'NULL';
	}

	public function setHeaderDestinationRoutingNumber( $strHeaderDestinationRoutingNumber ) {
		$this->set( 'm_strHeaderDestinationRoutingNumber', CStrings::strTrimDef( $strHeaderDestinationRoutingNumber, 9, NULL, true ) );
	}

	public function getHeaderDestinationRoutingNumber() {
		return $this->m_strHeaderDestinationRoutingNumber;
	}

	public function sqlHeaderDestinationRoutingNumber() {
		return ( true == isset( $this->m_strHeaderDestinationRoutingNumber ) ) ? '\'' . addslashes( $this->m_strHeaderDestinationRoutingNumber ) . '\'' : 'NULL';
	}

	public function setHeaderEceInstitutionRoutingNumber( $strHeaderEceInstitutionRoutingNumber ) {
		$this->set( 'm_strHeaderEceInstitutionRoutingNumber', CStrings::strTrimDef( $strHeaderEceInstitutionRoutingNumber, 9, NULL, true ) );
	}

	public function getHeaderEceInstitutionRoutingNumber() {
		return $this->m_strHeaderEceInstitutionRoutingNumber;
	}

	public function sqlHeaderEceInstitutionRoutingNumber() {
		return ( true == isset( $this->m_strHeaderEceInstitutionRoutingNumber ) ) ? '\'' . addslashes( $this->m_strHeaderEceInstitutionRoutingNumber ) . '\'' : 'NULL';
	}

	public function setHeaderBusinessDate( $strHeaderBusinessDate ) {
		$this->set( 'm_strHeaderBusinessDate', CStrings::strTrimDef( $strHeaderBusinessDate, 8, NULL, true ) );
	}

	public function getHeaderBusinessDate() {
		return $this->m_strHeaderBusinessDate;
	}

	public function sqlHeaderBusinessDate() {
		return ( true == isset( $this->m_strHeaderBusinessDate ) ) ? '\'' . addslashes( $this->m_strHeaderBusinessDate ) . '\'' : 'NULL';
	}

	public function setHeaderCreationDate( $strHeaderCreationDate ) {
		$this->set( 'm_strHeaderCreationDate', CStrings::strTrimDef( $strHeaderCreationDate, 8, NULL, true ) );
	}

	public function getHeaderCreationDate() {
		return $this->m_strHeaderCreationDate;
	}

	public function sqlHeaderCreationDate() {
		return ( true == isset( $this->m_strHeaderCreationDate ) ) ? '\'' . addslashes( $this->m_strHeaderCreationDate ) . '\'' : 'NULL';
	}

	public function setHeaderCreationTime( $strHeaderCreationTime ) {
		$this->set( 'm_strHeaderCreationTime', CStrings::strTrimDef( $strHeaderCreationTime, 4, NULL, true ) );
	}

	public function getHeaderCreationTime() {
		return $this->m_strHeaderCreationTime;
	}

	public function sqlHeaderCreationTime() {
		return ( true == isset( $this->m_strHeaderCreationTime ) ) ? '\'' . addslashes( $this->m_strHeaderCreationTime ) . '\'' : 'NULL';
	}

	public function setHeaderRecordTypeIndicator( $strHeaderRecordTypeIndicator ) {
		$this->set( 'm_strHeaderRecordTypeIndicator', CStrings::strTrimDef( $strHeaderRecordTypeIndicator, 1, NULL, true ) );
	}

	public function getHeaderRecordTypeIndicator() {
		return $this->m_strHeaderRecordTypeIndicator;
	}

	public function sqlHeaderRecordTypeIndicator() {
		return ( true == isset( $this->m_strHeaderRecordTypeIndicator ) ) ? '\'' . addslashes( $this->m_strHeaderRecordTypeIndicator ) . '\'' : 'NULL';
	}

	public function setHeaderDocumenationTypeIndicator( $strHeaderDocumenationTypeIndicator ) {
		$this->set( 'm_strHeaderDocumenationTypeIndicator', CStrings::strTrimDef( $strHeaderDocumenationTypeIndicator, 1, NULL, true ) );
	}

	public function getHeaderDocumenationTypeIndicator() {
		return $this->m_strHeaderDocumenationTypeIndicator;
	}

	public function sqlHeaderDocumenationTypeIndicator() {
		return ( true == isset( $this->m_strHeaderDocumenationTypeIndicator ) ) ? '\'' . addslashes( $this->m_strHeaderDocumenationTypeIndicator ) . '\'' : 'NULL';
	}

	public function setHeaderId( $strHeaderId ) {
		$this->set( 'm_strHeaderId', CStrings::strTrimDef( $strHeaderId, 8, NULL, true ) );
	}

	public function getHeaderId() {
		return $this->m_strHeaderId;
	}

	public function sqlHeaderId() {
		return ( true == isset( $this->m_strHeaderId ) ) ? '\'' . addslashes( $this->m_strHeaderId ) . '\'' : 'NULL';
	}

	public function setHeaderOriginatorContactName( $strHeaderOriginatorContactName ) {
		$this->set( 'm_strHeaderOriginatorContactName', CStrings::strTrimDef( $strHeaderOriginatorContactName, 14, NULL, true ) );
	}

	public function getHeaderOriginatorContactName() {
		return $this->m_strHeaderOriginatorContactName;
	}

	public function sqlHeaderOriginatorContactName() {
		return ( true == isset( $this->m_strHeaderOriginatorContactName ) ) ? '\'' . addslashes( $this->m_strHeaderOriginatorContactName ) . '\'' : 'NULL';
	}

	public function setHeaderOriginatorContactPhoneNumber( $strHeaderOriginatorContactPhoneNumber ) {
		$this->set( 'm_strHeaderOriginatorContactPhoneNumber', CStrings::strTrimDef( $strHeaderOriginatorContactPhoneNumber, 10, NULL, true ) );
	}

	public function getHeaderOriginatorContactPhoneNumber() {
		return $this->m_strHeaderOriginatorContactPhoneNumber;
	}

	public function sqlHeaderOriginatorContactPhoneNumber() {
		return ( true == isset( $this->m_strHeaderOriginatorContactPhoneNumber ) ) ? '\'' . addslashes( $this->m_strHeaderOriginatorContactPhoneNumber ) . '\'' : 'NULL';
	}

	public function setHeaderFedWorkType( $strHeaderFedWorkType ) {
		$this->set( 'm_strHeaderFedWorkType', CStrings::strTrimDef( $strHeaderFedWorkType, 1, NULL, true ) );
	}

	public function getHeaderFedWorkType() {
		return $this->m_strHeaderFedWorkType;
	}

	public function sqlHeaderFedWorkType() {
		return ( true == isset( $this->m_strHeaderFedWorkType ) ) ? '\'' . addslashes( $this->m_strHeaderFedWorkType ) . '\'' : 'NULL';
	}

	public function setHeaderUserField( $strHeaderUserField ) {
		$this->set( 'm_strHeaderUserField', CStrings::strTrimDef( $strHeaderUserField, 2, NULL, true ) );
	}

	public function getHeaderUserField() {
		return $this->m_strHeaderUserField;
	}

	public function sqlHeaderUserField() {
		return ( true == isset( $this->m_strHeaderUserField ) ) ? '\'' . addslashes( $this->m_strHeaderUserField ) . '\'' : 'NULL';
	}

	public function setHeaderReserved( $strHeaderReserved ) {
		$this->set( 'm_strHeaderReserved', CStrings::strTrimDef( $strHeaderReserved, 1, NULL, true ) );
	}

	public function getHeaderReserved() {
		return $this->m_strHeaderReserved;
	}

	public function sqlHeaderReserved() {
		return ( true == isset( $this->m_strHeaderReserved ) ) ? '\'' . addslashes( $this->m_strHeaderReserved ) . '\'' : 'NULL';
	}

	public function setControlRecordType( $strControlRecordType ) {
		$this->set( 'm_strControlRecordType', CStrings::strTrimDef( $strControlRecordType, 2, NULL, true ) );
	}

	public function getControlRecordType() {
		return $this->m_strControlRecordType;
	}

	public function sqlControlRecordType() {
		return ( true == isset( $this->m_strControlRecordType ) ) ? '\'' . addslashes( $this->m_strControlRecordType ) . '\'' : 'NULL';
	}

	public function setControlBundleCount( $strControlBundleCount ) {
		$this->set( 'm_strControlBundleCount', CStrings::strTrimDef( $strControlBundleCount, 6, NULL, true ) );
	}

	public function getControlBundleCount() {
		return $this->m_strControlBundleCount;
	}

	public function sqlControlBundleCount() {
		return ( true == isset( $this->m_strControlBundleCount ) ) ? '\'' . addslashes( $this->m_strControlBundleCount ) . '\'' : 'NULL';
	}

	public function setControlItemsCount( $strControlItemsCount ) {
		$this->set( 'm_strControlItemsCount', CStrings::strTrimDef( $strControlItemsCount, 8, NULL, true ) );
	}

	public function getControlItemsCount() {
		return $this->m_strControlItemsCount;
	}

	public function sqlControlItemsCount() {
		return ( true == isset( $this->m_strControlItemsCount ) ) ? '\'' . addslashes( $this->m_strControlItemsCount ) . '\'' : 'NULL';
	}

	public function setControlTotalAmount( $strControlTotalAmount ) {
		$this->set( 'm_strControlTotalAmount', CStrings::strTrimDef( $strControlTotalAmount, 14, NULL, true ) );
	}

	public function getControlTotalAmount() {
		return $this->m_strControlTotalAmount;
	}

	public function sqlControlTotalAmount() {
		return ( true == isset( $this->m_strControlTotalAmount ) ) ? '\'' . addslashes( $this->m_strControlTotalAmount ) . '\'' : 'NULL';
	}

	public function setControlImagesCount( $strControlImagesCount ) {
		$this->set( 'm_strControlImagesCount', CStrings::strTrimDef( $strControlImagesCount, 9, NULL, true ) );
	}

	public function getControlImagesCount() {
		return $this->m_strControlImagesCount;
	}

	public function sqlControlImagesCount() {
		return ( true == isset( $this->m_strControlImagesCount ) ) ? '\'' . addslashes( $this->m_strControlImagesCount ) . '\'' : 'NULL';
	}

	public function setControlEceInstitutionName( $strControlEceInstitutionName ) {
		$this->set( 'm_strControlEceInstitutionName', CStrings::strTrimDef( $strControlEceInstitutionName, 18, NULL, true ) );
	}

	public function getControlEceInstitutionName() {
		return $this->m_strControlEceInstitutionName;
	}

	public function sqlControlEceInstitutionName() {
		return ( true == isset( $this->m_strControlEceInstitutionName ) ) ? '\'' . addslashes( $this->m_strControlEceInstitutionName ) . '\'' : 'NULL';
	}

	public function setControlSettlementDate( $strControlSettlementDate ) {
		$this->set( 'm_strControlSettlementDate', CStrings::strTrimDef( $strControlSettlementDate, 8, NULL, true ) );
	}

	public function getControlSettlementDate() {
		return $this->m_strControlSettlementDate;
	}

	public function sqlControlSettlementDate() {
		return ( true == isset( $this->m_strControlSettlementDate ) ) ? '\'' . addslashes( $this->m_strControlSettlementDate ) . '\'' : 'NULL';
	}

	public function setControlReserved( $strControlReserved ) {
		$this->set( 'm_strControlReserved', CStrings::strTrimDef( $strControlReserved, 15, NULL, true ) );
	}

	public function getControlReserved() {
		return $this->m_strControlReserved;
	}

	public function sqlControlReserved() {
		return ( true == isset( $this->m_strControlReserved ) ) ? '\'' . addslashes( $this->m_strControlReserved ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, x937_file_id, header_record_type, header_collection_type_indicator, header_destination_routing_number, header_ece_institution_routing_number, header_business_date, header_creation_date, header_creation_time, header_record_type_indicator, header_documenation_type_indicator, header_id, header_originator_contact_name, header_originator_contact_phone_number, header_fed_work_type, header_user_field, header_reserved, control_record_type, control_bundle_count, control_items_count, control_total_amount, control_images_count, control_ece_institution_name, control_settlement_date, control_reserved, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlX937FileId() . ', ' .
 						$this->sqlHeaderRecordType() . ', ' .
 						$this->sqlHeaderCollectionTypeIndicator() . ', ' .
 						$this->sqlHeaderDestinationRoutingNumber() . ', ' .
 						$this->sqlHeaderEceInstitutionRoutingNumber() . ', ' .
 						$this->sqlHeaderBusinessDate() . ', ' .
 						$this->sqlHeaderCreationDate() . ', ' .
 						$this->sqlHeaderCreationTime() . ', ' .
 						$this->sqlHeaderRecordTypeIndicator() . ', ' .
 						$this->sqlHeaderDocumenationTypeIndicator() . ', ' .
 						$this->sqlHeaderId() . ', ' .
 						$this->sqlHeaderOriginatorContactName() . ', ' .
 						$this->sqlHeaderOriginatorContactPhoneNumber() . ', ' .
 						$this->sqlHeaderFedWorkType() . ', ' .
 						$this->sqlHeaderUserField() . ', ' .
 						$this->sqlHeaderReserved() . ', ' .
 						$this->sqlControlRecordType() . ', ' .
 						$this->sqlControlBundleCount() . ', ' .
 						$this->sqlControlItemsCount() . ', ' .
 						$this->sqlControlTotalAmount() . ', ' .
 						$this->sqlControlImagesCount() . ', ' .
 						$this->sqlControlEceInstitutionName() . ', ' .
 						$this->sqlControlSettlementDate() . ', ' .
 						$this->sqlControlReserved() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_file_id = ' . $this->sqlX937FileId() . ','; } elseif( true == array_key_exists( 'X937FileId', $this->getChangedColumns() ) ) { $strSql .= ' x937_file_id = ' . $this->sqlX937FileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_record_type = ' . $this->sqlHeaderRecordType() . ','; } elseif( true == array_key_exists( 'HeaderRecordType', $this->getChangedColumns() ) ) { $strSql .= ' header_record_type = ' . $this->sqlHeaderRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_collection_type_indicator = ' . $this->sqlHeaderCollectionTypeIndicator() . ','; } elseif( true == array_key_exists( 'HeaderCollectionTypeIndicator', $this->getChangedColumns() ) ) { $strSql .= ' header_collection_type_indicator = ' . $this->sqlHeaderCollectionTypeIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_destination_routing_number = ' . $this->sqlHeaderDestinationRoutingNumber() . ','; } elseif( true == array_key_exists( 'HeaderDestinationRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' header_destination_routing_number = ' . $this->sqlHeaderDestinationRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_ece_institution_routing_number = ' . $this->sqlHeaderEceInstitutionRoutingNumber() . ','; } elseif( true == array_key_exists( 'HeaderEceInstitutionRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' header_ece_institution_routing_number = ' . $this->sqlHeaderEceInstitutionRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_business_date = ' . $this->sqlHeaderBusinessDate() . ','; } elseif( true == array_key_exists( 'HeaderBusinessDate', $this->getChangedColumns() ) ) { $strSql .= ' header_business_date = ' . $this->sqlHeaderBusinessDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_creation_date = ' . $this->sqlHeaderCreationDate() . ','; } elseif( true == array_key_exists( 'HeaderCreationDate', $this->getChangedColumns() ) ) { $strSql .= ' header_creation_date = ' . $this->sqlHeaderCreationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_creation_time = ' . $this->sqlHeaderCreationTime() . ','; } elseif( true == array_key_exists( 'HeaderCreationTime', $this->getChangedColumns() ) ) { $strSql .= ' header_creation_time = ' . $this->sqlHeaderCreationTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_record_type_indicator = ' . $this->sqlHeaderRecordTypeIndicator() . ','; } elseif( true == array_key_exists( 'HeaderRecordTypeIndicator', $this->getChangedColumns() ) ) { $strSql .= ' header_record_type_indicator = ' . $this->sqlHeaderRecordTypeIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_documenation_type_indicator = ' . $this->sqlHeaderDocumenationTypeIndicator() . ','; } elseif( true == array_key_exists( 'HeaderDocumenationTypeIndicator', $this->getChangedColumns() ) ) { $strSql .= ' header_documenation_type_indicator = ' . $this->sqlHeaderDocumenationTypeIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_id = ' . $this->sqlHeaderId() . ','; } elseif( true == array_key_exists( 'HeaderId', $this->getChangedColumns() ) ) { $strSql .= ' header_id = ' . $this->sqlHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_originator_contact_name = ' . $this->sqlHeaderOriginatorContactName() . ','; } elseif( true == array_key_exists( 'HeaderOriginatorContactName', $this->getChangedColumns() ) ) { $strSql .= ' header_originator_contact_name = ' . $this->sqlHeaderOriginatorContactName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_originator_contact_phone_number = ' . $this->sqlHeaderOriginatorContactPhoneNumber() . ','; } elseif( true == array_key_exists( 'HeaderOriginatorContactPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' header_originator_contact_phone_number = ' . $this->sqlHeaderOriginatorContactPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_fed_work_type = ' . $this->sqlHeaderFedWorkType() . ','; } elseif( true == array_key_exists( 'HeaderFedWorkType', $this->getChangedColumns() ) ) { $strSql .= ' header_fed_work_type = ' . $this->sqlHeaderFedWorkType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_user_field = ' . $this->sqlHeaderUserField() . ','; } elseif( true == array_key_exists( 'HeaderUserField', $this->getChangedColumns() ) ) { $strSql .= ' header_user_field = ' . $this->sqlHeaderUserField() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_reserved = ' . $this->sqlHeaderReserved() . ','; } elseif( true == array_key_exists( 'HeaderReserved', $this->getChangedColumns() ) ) { $strSql .= ' header_reserved = ' . $this->sqlHeaderReserved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_record_type = ' . $this->sqlControlRecordType() . ','; } elseif( true == array_key_exists( 'ControlRecordType', $this->getChangedColumns() ) ) { $strSql .= ' control_record_type = ' . $this->sqlControlRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_bundle_count = ' . $this->sqlControlBundleCount() . ','; } elseif( true == array_key_exists( 'ControlBundleCount', $this->getChangedColumns() ) ) { $strSql .= ' control_bundle_count = ' . $this->sqlControlBundleCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_items_count = ' . $this->sqlControlItemsCount() . ','; } elseif( true == array_key_exists( 'ControlItemsCount', $this->getChangedColumns() ) ) { $strSql .= ' control_items_count = ' . $this->sqlControlItemsCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_total_amount = ' . $this->sqlControlTotalAmount() . ','; } elseif( true == array_key_exists( 'ControlTotalAmount', $this->getChangedColumns() ) ) { $strSql .= ' control_total_amount = ' . $this->sqlControlTotalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_images_count = ' . $this->sqlControlImagesCount() . ','; } elseif( true == array_key_exists( 'ControlImagesCount', $this->getChangedColumns() ) ) { $strSql .= ' control_images_count = ' . $this->sqlControlImagesCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_ece_institution_name = ' . $this->sqlControlEceInstitutionName() . ','; } elseif( true == array_key_exists( 'ControlEceInstitutionName', $this->getChangedColumns() ) ) { $strSql .= ' control_ece_institution_name = ' . $this->sqlControlEceInstitutionName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_settlement_date = ' . $this->sqlControlSettlementDate() . ','; } elseif( true == array_key_exists( 'ControlSettlementDate', $this->getChangedColumns() ) ) { $strSql .= ' control_settlement_date = ' . $this->sqlControlSettlementDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_reserved = ' . $this->sqlControlReserved() . ','; } elseif( true == array_key_exists( 'ControlReserved', $this->getChangedColumns() ) ) { $strSql .= ' control_reserved = ' . $this->sqlControlReserved() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'x937_file_id' => $this->getX937FileId(),
			'header_record_type' => $this->getHeaderRecordType(),
			'header_collection_type_indicator' => $this->getHeaderCollectionTypeIndicator(),
			'header_destination_routing_number' => $this->getHeaderDestinationRoutingNumber(),
			'header_ece_institution_routing_number' => $this->getHeaderEceInstitutionRoutingNumber(),
			'header_business_date' => $this->getHeaderBusinessDate(),
			'header_creation_date' => $this->getHeaderCreationDate(),
			'header_creation_time' => $this->getHeaderCreationTime(),
			'header_record_type_indicator' => $this->getHeaderRecordTypeIndicator(),
			'header_documenation_type_indicator' => $this->getHeaderDocumenationTypeIndicator(),
			'header_id' => $this->getHeaderId(),
			'header_originator_contact_name' => $this->getHeaderOriginatorContactName(),
			'header_originator_contact_phone_number' => $this->getHeaderOriginatorContactPhoneNumber(),
			'header_fed_work_type' => $this->getHeaderFedWorkType(),
			'header_user_field' => $this->getHeaderUserField(),
			'header_reserved' => $this->getHeaderReserved(),
			'control_record_type' => $this->getControlRecordType(),
			'control_bundle_count' => $this->getControlBundleCount(),
			'control_items_count' => $this->getControlItemsCount(),
			'control_total_amount' => $this->getControlTotalAmount(),
			'control_images_count' => $this->getControlImagesCount(),
			'control_ece_institution_name' => $this->getControlEceInstitutionName(),
			'control_settlement_date' => $this->getControlSettlementDate(),
			'control_reserved' => $this->getControlReserved(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>