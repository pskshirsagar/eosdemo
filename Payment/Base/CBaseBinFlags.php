<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CBinFlags
 * Do not add any new functions to this class.
 */

class CBaseBinFlags extends CEosPluralBase {

	/**
	 * @return CBinFlag[]
	 */
	public static function fetchBinFlags( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBinFlag::class, $objDatabase );
	}

	/**
	 * @return CBinFlag
	 */
	public static function fetchBinFlag( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBinFlag::class, $objDatabase );
	}

	public static function fetchBinFlagCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'bin_flags', $objDatabase );
	}

	public static function fetchBinFlagById( $intId, $objDatabase ) {
		return self::fetchBinFlag( sprintf( 'SELECT * FROM bin_flags WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>