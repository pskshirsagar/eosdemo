<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CEftBatches
 * Do not add any new functions to this class.
 */

class CBaseEftBatches extends CEosPluralBase {

	/**
	 * @return CEftBatch[]
	 */
	public static function fetchEftBatches( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEftBatch::class, $objDatabase );
	}

	/**
	 * @return CEftBatch
	 */
	public static function fetchEftBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEftBatch::class, $objDatabase );
	}

	public static function fetchEftBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'eft_batches', $objDatabase );
	}

	public static function fetchEftBatchById( $intId, $objDatabase ) {
		return self::fetchEftBatch( sprintf( 'SELECT * FROM eft_batches WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEftBatchesByProcessingBankId( $intProcessingBankId, $objDatabase ) {
		return self::fetchEftBatches( sprintf( 'SELECT * FROM eft_batches WHERE processing_bank_id = %d', ( int ) $intProcessingBankId ), $objDatabase );
	}

	public static function fetchEftBatchesByProcessingBankAccountId( $intProcessingBankAccountId, $objDatabase ) {
		return self::fetchEftBatches( sprintf( 'SELECT * FROM eft_batches WHERE processing_bank_account_id = %d', ( int ) $intProcessingBankAccountId ), $objDatabase );
	}

	public static function fetchEftBatchesByMerchantGatewayId( $intMerchantGatewayId, $objDatabase ) {
		return self::fetchEftBatches( sprintf( 'SELECT * FROM eft_batches WHERE merchant_gateway_id = %d', ( int ) $intMerchantGatewayId ), $objDatabase );
	}

	public static function fetchEftBatchesByEftBatchTypeId( $intEftBatchTypeId, $objDatabase ) {
		return self::fetchEftBatches( sprintf( 'SELECT * FROM eft_batches WHERE eft_batch_type_id = %d', ( int ) $intEftBatchTypeId ), $objDatabase );
	}

	public static function fetchEftBatchesByEftBatchStatusTypeId( $intEftBatchStatusTypeId, $objDatabase ) {
		return self::fetchEftBatches( sprintf( 'SELECT * FROM eft_batches WHERE eft_batch_status_type_id = %d', ( int ) $intEftBatchStatusTypeId ), $objDatabase );
	}

	public static function fetchEftBatchesByAggregateEftBatchId( $intAggregateEftBatchId, $objDatabase ) {
		return self::fetchEftBatches( sprintf( 'SELECT * FROM eft_batches WHERE aggregate_eft_batch_id = %d', ( int ) $intAggregateEftBatchId ), $objDatabase );
	}

	public static function fetchEftBatchesByNachaFileId( $intNachaFileId, $objDatabase ) {
		return self::fetchEftBatches( sprintf( 'SELECT * FROM eft_batches WHERE nacha_file_id = %d', ( int ) $intNachaFileId ), $objDatabase );
	}

	public static function fetchEftBatchesByX937FileId( $intX937FileId, $objDatabase ) {
		return self::fetchEftBatches( sprintf( 'SELECT * FROM eft_batches WHERE x937_file_id = %d', ( int ) $intX937FileId ), $objDatabase );
	}

}
?>