<?php

class CBaseReconciliationEntry extends CEosSingularBase {

	const TABLE_NAME = 'public.reconciliation_entries';

	protected $m_intId;
	protected $m_intProcessingBankAccountId;
	protected $m_intReconciliationId;
	protected $m_intReconciliationEntryTypeId;
	protected $m_intEftInstructionId;
	protected $m_intNachaEntryDetailRecordId;
	protected $m_intNachaReturnAddendaDetailRecordId;
	protected $m_intChargeBackId;
	protected $m_intEftBatchId;
	protected $m_intX937FileId;
	protected $m_intX937ReturnFileId;
	protected $m_intNachaReturnFileId;
	protected $m_intSettlementDistributionId;
	protected $m_intWesternUnionOutputFileId;
	protected $m_intCompanyPaymentId;
	protected $m_intArPaymentId;
	protected $m_intBai2FileId;
	protected $m_strTraceNumber;
	protected $m_strMovementDatetime;
	protected $m_fltTotal;
	protected $m_strMemo;
	protected $m_strNotes;
	protected $m_strTransactionDetails;
	protected $m_intIsManual;
	protected $m_intIsApproved;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intWorldpayReturnId;
	protected $m_intAftFileBatchId;

	public function __construct() {
		parent::__construct();

		$this->m_intIsManual = '0';
		$this->m_intIsApproved = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankAccountId', trim( $arrValues['processing_bank_account_id'] ) ); elseif( isset( $arrValues['processing_bank_account_id'] ) ) $this->setProcessingBankAccountId( $arrValues['processing_bank_account_id'] );
		if( isset( $arrValues['reconciliation_id'] ) && $boolDirectSet ) $this->set( 'm_intReconciliationId', trim( $arrValues['reconciliation_id'] ) ); elseif( isset( $arrValues['reconciliation_id'] ) ) $this->setReconciliationId( $arrValues['reconciliation_id'] );
		if( isset( $arrValues['reconciliation_entry_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReconciliationEntryTypeId', trim( $arrValues['reconciliation_entry_type_id'] ) ); elseif( isset( $arrValues['reconciliation_entry_type_id'] ) ) $this->setReconciliationEntryTypeId( $arrValues['reconciliation_entry_type_id'] );
		if( isset( $arrValues['eft_instruction_id'] ) && $boolDirectSet ) $this->set( 'm_intEftInstructionId', trim( $arrValues['eft_instruction_id'] ) ); elseif( isset( $arrValues['eft_instruction_id'] ) ) $this->setEftInstructionId( $arrValues['eft_instruction_id'] );
		if( isset( $arrValues['nacha_entry_detail_record_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaEntryDetailRecordId', trim( $arrValues['nacha_entry_detail_record_id'] ) ); elseif( isset( $arrValues['nacha_entry_detail_record_id'] ) ) $this->setNachaEntryDetailRecordId( $arrValues['nacha_entry_detail_record_id'] );
		if( isset( $arrValues['nacha_return_addenda_detail_record_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaReturnAddendaDetailRecordId', trim( $arrValues['nacha_return_addenda_detail_record_id'] ) ); elseif( isset( $arrValues['nacha_return_addenda_detail_record_id'] ) ) $this->setNachaReturnAddendaDetailRecordId( $arrValues['nacha_return_addenda_detail_record_id'] );
		if( isset( $arrValues['charge_back_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeBackId', trim( $arrValues['charge_back_id'] ) ); elseif( isset( $arrValues['charge_back_id'] ) ) $this->setChargeBackId( $arrValues['charge_back_id'] );
		if( isset( $arrValues['eft_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intEftBatchId', trim( $arrValues['eft_batch_id'] ) ); elseif( isset( $arrValues['eft_batch_id'] ) ) $this->setEftBatchId( $arrValues['eft_batch_id'] );
		if( isset( $arrValues['x937_file_id'] ) && $boolDirectSet ) $this->set( 'm_intX937FileId', trim( $arrValues['x937_file_id'] ) ); elseif( isset( $arrValues['x937_file_id'] ) ) $this->setX937FileId( $arrValues['x937_file_id'] );
		if( isset( $arrValues['x937_return_file_id'] ) && $boolDirectSet ) $this->set( 'm_intX937ReturnFileId', trim( $arrValues['x937_return_file_id'] ) ); elseif( isset( $arrValues['x937_return_file_id'] ) ) $this->setX937ReturnFileId( $arrValues['x937_return_file_id'] );
		if( isset( $arrValues['nacha_return_file_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaReturnFileId', trim( $arrValues['nacha_return_file_id'] ) ); elseif( isset( $arrValues['nacha_return_file_id'] ) ) $this->setNachaReturnFileId( $arrValues['nacha_return_file_id'] );
		if( isset( $arrValues['settlement_distribution_id'] ) && $boolDirectSet ) $this->set( 'm_intSettlementDistributionId', trim( $arrValues['settlement_distribution_id'] ) ); elseif( isset( $arrValues['settlement_distribution_id'] ) ) $this->setSettlementDistributionId( $arrValues['settlement_distribution_id'] );
		if( isset( $arrValues['western_union_output_file_id'] ) && $boolDirectSet ) $this->set( 'm_intWesternUnionOutputFileId', trim( $arrValues['western_union_output_file_id'] ) ); elseif( isset( $arrValues['western_union_output_file_id'] ) ) $this->setWesternUnionOutputFileId( $arrValues['western_union_output_file_id'] );
		if( isset( $arrValues['company_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyPaymentId', trim( $arrValues['company_payment_id'] ) ); elseif( isset( $arrValues['company_payment_id'] ) ) $this->setCompanyPaymentId( $arrValues['company_payment_id'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['bai2_file_id'] ) && $boolDirectSet ) $this->set( 'm_intBai2FileId', trim( $arrValues['bai2_file_id'] ) ); elseif( isset( $arrValues['bai2_file_id'] ) ) $this->setBai2FileId( $arrValues['bai2_file_id'] );
		if( isset( $arrValues['trace_number'] ) && $boolDirectSet ) $this->set( 'm_strTraceNumber', trim( stripcslashes( $arrValues['trace_number'] ) ) ); elseif( isset( $arrValues['trace_number'] ) ) $this->setTraceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['trace_number'] ) : $arrValues['trace_number'] );
		if( isset( $arrValues['movement_datetime'] ) && $boolDirectSet ) $this->set( 'm_strMovementDatetime', trim( $arrValues['movement_datetime'] ) ); elseif( isset( $arrValues['movement_datetime'] ) ) $this->setMovementDatetime( $arrValues['movement_datetime'] );
		if( isset( $arrValues['total'] ) && $boolDirectSet ) $this->set( 'm_fltTotal', trim( $arrValues['total'] ) ); elseif( isset( $arrValues['total'] ) ) $this->setTotal( $arrValues['total'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( stripcslashes( $arrValues['memo'] ) ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['memo'] ) : $arrValues['memo'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['transaction_details'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDetails', trim( stripcslashes( $arrValues['transaction_details'] ) ) ); elseif( isset( $arrValues['transaction_details'] ) ) $this->setTransactionDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['transaction_details'] ) : $arrValues['transaction_details'] );
		if( isset( $arrValues['is_manual'] ) && $boolDirectSet ) $this->set( 'm_intIsManual', trim( $arrValues['is_manual'] ) ); elseif( isset( $arrValues['is_manual'] ) ) $this->setIsManual( $arrValues['is_manual'] );
		if( isset( $arrValues['is_approved'] ) && $boolDirectSet ) $this->set( 'm_intIsApproved', trim( $arrValues['is_approved'] ) ); elseif( isset( $arrValues['is_approved'] ) ) $this->setIsApproved( $arrValues['is_approved'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['worldpay_return_id'] ) && $boolDirectSet ) $this->set( 'm_intWorldpayReturnId', trim( $arrValues['worldpay_return_id'] ) ); elseif( isset( $arrValues['worldpay_return_id'] ) ) $this->setWorldpayReturnId( $arrValues['worldpay_return_id'] );
		if( isset( $arrValues['aft_file_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intAftFileBatchId', trim( $arrValues['aft_file_batch_id'] ) ); elseif( isset( $arrValues['aft_file_batch_id'] ) ) $this->setAftFileBatchId( $arrValues['aft_file_batch_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
		$this->set( 'm_intProcessingBankAccountId', CStrings::strToIntDef( $intProcessingBankAccountId, NULL, false ) );
	}

	public function getProcessingBankAccountId() {
		return $this->m_intProcessingBankAccountId;
	}

	public function sqlProcessingBankAccountId() {
		return ( true == isset( $this->m_intProcessingBankAccountId ) ) ? ( string ) $this->m_intProcessingBankAccountId : 'NULL';
	}

	public function setReconciliationId( $intReconciliationId ) {
		$this->set( 'm_intReconciliationId', CStrings::strToIntDef( $intReconciliationId, NULL, false ) );
	}

	public function getReconciliationId() {
		return $this->m_intReconciliationId;
	}

	public function sqlReconciliationId() {
		return ( true == isset( $this->m_intReconciliationId ) ) ? ( string ) $this->m_intReconciliationId : 'NULL';
	}

	public function setReconciliationEntryTypeId( $intReconciliationEntryTypeId ) {
		$this->set( 'm_intReconciliationEntryTypeId', CStrings::strToIntDef( $intReconciliationEntryTypeId, NULL, false ) );
	}

	public function getReconciliationEntryTypeId() {
		return $this->m_intReconciliationEntryTypeId;
	}

	public function sqlReconciliationEntryTypeId() {
		return ( true == isset( $this->m_intReconciliationEntryTypeId ) ) ? ( string ) $this->m_intReconciliationEntryTypeId : 'NULL';
	}

	public function setEftInstructionId( $intEftInstructionId ) {
		$this->set( 'm_intEftInstructionId', CStrings::strToIntDef( $intEftInstructionId, NULL, false ) );
	}

	public function getEftInstructionId() {
		return $this->m_intEftInstructionId;
	}

	public function sqlEftInstructionId() {
		return ( true == isset( $this->m_intEftInstructionId ) ) ? ( string ) $this->m_intEftInstructionId : 'NULL';
	}

	public function setNachaEntryDetailRecordId( $intNachaEntryDetailRecordId ) {
		$this->set( 'm_intNachaEntryDetailRecordId', CStrings::strToIntDef( $intNachaEntryDetailRecordId, NULL, false ) );
	}

	public function getNachaEntryDetailRecordId() {
		return $this->m_intNachaEntryDetailRecordId;
	}

	public function sqlNachaEntryDetailRecordId() {
		return ( true == isset( $this->m_intNachaEntryDetailRecordId ) ) ? ( string ) $this->m_intNachaEntryDetailRecordId : 'NULL';
	}

	public function setNachaReturnAddendaDetailRecordId( $intNachaReturnAddendaDetailRecordId ) {
		$this->set( 'm_intNachaReturnAddendaDetailRecordId', CStrings::strToIntDef( $intNachaReturnAddendaDetailRecordId, NULL, false ) );
	}

	public function getNachaReturnAddendaDetailRecordId() {
		return $this->m_intNachaReturnAddendaDetailRecordId;
	}

	public function sqlNachaReturnAddendaDetailRecordId() {
		return ( true == isset( $this->m_intNachaReturnAddendaDetailRecordId ) ) ? ( string ) $this->m_intNachaReturnAddendaDetailRecordId : 'NULL';
	}

	public function setChargeBackId( $intChargeBackId ) {
		$this->set( 'm_intChargeBackId', CStrings::strToIntDef( $intChargeBackId, NULL, false ) );
	}

	public function getChargeBackId() {
		return $this->m_intChargeBackId;
	}

	public function sqlChargeBackId() {
		return ( true == isset( $this->m_intChargeBackId ) ) ? ( string ) $this->m_intChargeBackId : 'NULL';
	}

	public function setEftBatchId( $intEftBatchId ) {
		$this->set( 'm_intEftBatchId', CStrings::strToIntDef( $intEftBatchId, NULL, false ) );
	}

	public function getEftBatchId() {
		return $this->m_intEftBatchId;
	}

	public function sqlEftBatchId() {
		return ( true == isset( $this->m_intEftBatchId ) ) ? ( string ) $this->m_intEftBatchId : 'NULL';
	}

	public function setX937FileId( $intX937FileId ) {
		$this->set( 'm_intX937FileId', CStrings::strToIntDef( $intX937FileId, NULL, false ) );
	}

	public function getX937FileId() {
		return $this->m_intX937FileId;
	}

	public function sqlX937FileId() {
		return ( true == isset( $this->m_intX937FileId ) ) ? ( string ) $this->m_intX937FileId : 'NULL';
	}

	public function setX937ReturnFileId( $intX937ReturnFileId ) {
		$this->set( 'm_intX937ReturnFileId', CStrings::strToIntDef( $intX937ReturnFileId, NULL, false ) );
	}

	public function getX937ReturnFileId() {
		return $this->m_intX937ReturnFileId;
	}

	public function sqlX937ReturnFileId() {
		return ( true == isset( $this->m_intX937ReturnFileId ) ) ? ( string ) $this->m_intX937ReturnFileId : 'NULL';
	}

	public function setNachaReturnFileId( $intNachaReturnFileId ) {
		$this->set( 'm_intNachaReturnFileId', CStrings::strToIntDef( $intNachaReturnFileId, NULL, false ) );
	}

	public function getNachaReturnFileId() {
		return $this->m_intNachaReturnFileId;
	}

	public function sqlNachaReturnFileId() {
		return ( true == isset( $this->m_intNachaReturnFileId ) ) ? ( string ) $this->m_intNachaReturnFileId : 'NULL';
	}

	public function setSettlementDistributionId( $intSettlementDistributionId ) {
		$this->set( 'm_intSettlementDistributionId', CStrings::strToIntDef( $intSettlementDistributionId, NULL, false ) );
	}

	public function getSettlementDistributionId() {
		return $this->m_intSettlementDistributionId;
	}

	public function sqlSettlementDistributionId() {
		return ( true == isset( $this->m_intSettlementDistributionId ) ) ? ( string ) $this->m_intSettlementDistributionId : 'NULL';
	}

	public function setWesternUnionOutputFileId( $intWesternUnionOutputFileId ) {
		$this->set( 'm_intWesternUnionOutputFileId', CStrings::strToIntDef( $intWesternUnionOutputFileId, NULL, false ) );
	}

	public function getWesternUnionOutputFileId() {
		return $this->m_intWesternUnionOutputFileId;
	}

	public function sqlWesternUnionOutputFileId() {
		return ( true == isset( $this->m_intWesternUnionOutputFileId ) ) ? ( string ) $this->m_intWesternUnionOutputFileId : 'NULL';
	}

	public function setCompanyPaymentId( $intCompanyPaymentId ) {
		$this->set( 'm_intCompanyPaymentId', CStrings::strToIntDef( $intCompanyPaymentId, NULL, false ) );
	}

	public function getCompanyPaymentId() {
		return $this->m_intCompanyPaymentId;
	}

	public function sqlCompanyPaymentId() {
		return ( true == isset( $this->m_intCompanyPaymentId ) ) ? ( string ) $this->m_intCompanyPaymentId : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setBai2FileId( $intBai2FileId ) {
		$this->set( 'm_intBai2FileId', CStrings::strToIntDef( $intBai2FileId, NULL, false ) );
	}

	public function getBai2FileId() {
		return $this->m_intBai2FileId;
	}

	public function sqlBai2FileId() {
		return ( true == isset( $this->m_intBai2FileId ) ) ? ( string ) $this->m_intBai2FileId : 'NULL';
	}

	public function setTraceNumber( $strTraceNumber ) {
		$this->set( 'm_strTraceNumber', CStrings::strTrimDef( $strTraceNumber, 15, NULL, true ) );
	}

	public function getTraceNumber() {
		return $this->m_strTraceNumber;
	}

	public function sqlTraceNumber() {
		return ( true == isset( $this->m_strTraceNumber ) ) ? '\'' . addslashes( $this->m_strTraceNumber ) . '\'' : 'NULL';
	}

	public function setMovementDatetime( $strMovementDatetime ) {
		$this->set( 'm_strMovementDatetime', CStrings::strTrimDef( $strMovementDatetime, -1, NULL, true ) );
	}

	public function getMovementDatetime() {
		return $this->m_strMovementDatetime;
	}

	public function sqlMovementDatetime() {
		return ( true == isset( $this->m_strMovementDatetime ) ) ? '\'' . $this->m_strMovementDatetime . '\'' : 'NOW()';
	}

	public function setTotal( $fltTotal ) {
		$this->set( 'm_fltTotal', CStrings::strToFloatDef( $fltTotal, NULL, false, 2 ) );
	}

	public function getTotal() {
		return $this->m_fltTotal;
	}

	public function sqlTotal() {
		return ( true == isset( $this->m_fltTotal ) ) ? ( string ) $this->m_fltTotal : 'NULL';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, -1, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? '\'' . addslashes( $this->m_strMemo ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 1028, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setTransactionDetails( $strTransactionDetails ) {
		$this->set( 'm_strTransactionDetails', CStrings::strTrimDef( $strTransactionDetails, 1028, NULL, true ) );
	}

	public function getTransactionDetails() {
		return $this->m_strTransactionDetails;
	}

	public function sqlTransactionDetails() {
		return ( true == isset( $this->m_strTransactionDetails ) ) ? '\'' . addslashes( $this->m_strTransactionDetails ) . '\'' : 'NULL';
	}

	public function setIsManual( $intIsManual ) {
		$this->set( 'm_intIsManual', CStrings::strToIntDef( $intIsManual, NULL, false ) );
	}

	public function getIsManual() {
		return $this->m_intIsManual;
	}

	public function sqlIsManual() {
		return ( true == isset( $this->m_intIsManual ) ) ? ( string ) $this->m_intIsManual : '0';
	}

	public function setIsApproved( $intIsApproved ) {
		$this->set( 'm_intIsApproved', CStrings::strToIntDef( $intIsApproved, NULL, false ) );
	}

	public function getIsApproved() {
		return $this->m_intIsApproved;
	}

	public function sqlIsApproved() {
		return ( true == isset( $this->m_intIsApproved ) ) ? ( string ) $this->m_intIsApproved : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setWorldpayReturnId( $intWorldpayReturnId ) {
		$this->set( 'm_intWorldpayReturnId', CStrings::strToIntDef( $intWorldpayReturnId, NULL, false ) );
	}

	public function getWorldpayReturnId() {
		return $this->m_intWorldpayReturnId;
	}

	public function sqlWorldpayReturnId() {
		return ( true == isset( $this->m_intWorldpayReturnId ) ) ? ( string ) $this->m_intWorldpayReturnId : 'NULL';
	}

	public function setAftFileBatchId( $intAftFileBatchId ) {
		$this->set( 'm_intAftFileBatchId', CStrings::strToIntDef( $intAftFileBatchId, NULL, false ) );
	}

	public function getAftFileBatchId() {
		return $this->m_intAftFileBatchId;
	}

	public function sqlAftFileBatchId() {
		return ( true == isset( $this->m_intAftFileBatchId ) ) ? ( string ) $this->m_intAftFileBatchId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, processing_bank_account_id, reconciliation_id, reconciliation_entry_type_id, eft_instruction_id, nacha_entry_detail_record_id, nacha_return_addenda_detail_record_id, charge_back_id, eft_batch_id, x937_file_id, x937_return_file_id, nacha_return_file_id, settlement_distribution_id, western_union_output_file_id, company_payment_id, ar_payment_id, bai2_file_id, trace_number, movement_datetime, total, memo, notes, transaction_details, is_manual, is_approved, updated_by, updated_on, created_by, created_on, worldpay_return_id, aft_file_batch_id )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlProcessingBankAccountId() . ', ' .
 						$this->sqlReconciliationId() . ', ' .
 						$this->sqlReconciliationEntryTypeId() . ', ' .
 						$this->sqlEftInstructionId() . ', ' .
 						$this->sqlNachaEntryDetailRecordId() . ', ' .
 						$this->sqlNachaReturnAddendaDetailRecordId() . ', ' .
 						$this->sqlChargeBackId() . ', ' .
 						$this->sqlEftBatchId() . ', ' .
 						$this->sqlX937FileId() . ', ' .
 						$this->sqlX937ReturnFileId() . ', ' .
 						$this->sqlNachaReturnFileId() . ', ' .
 						$this->sqlSettlementDistributionId() . ', ' .
 						$this->sqlWesternUnionOutputFileId() . ', ' .
 						$this->sqlCompanyPaymentId() . ', ' .
 						$this->sqlArPaymentId() . ', ' .
 						$this->sqlBai2FileId() . ', ' .
 						$this->sqlTraceNumber() . ', ' .
 						$this->sqlMovementDatetime() . ', ' .
 						$this->sqlTotal() . ', ' .
 						$this->sqlMemo() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlTransactionDetails() . ', ' .
 						$this->sqlIsManual() . ', ' .
 						$this->sqlIsApproved() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlWorldpayReturnId() . ', ' .
 						$this->sqlAftFileBatchId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; } elseif( true == array_key_exists( 'ProcessingBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reconciliation_id = ' . $this->sqlReconciliationId() . ','; } elseif( true == array_key_exists( 'ReconciliationId', $this->getChangedColumns() ) ) { $strSql .= ' reconciliation_id = ' . $this->sqlReconciliationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reconciliation_entry_type_id = ' . $this->sqlReconciliationEntryTypeId() . ','; } elseif( true == array_key_exists( 'ReconciliationEntryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' reconciliation_entry_type_id = ' . $this->sqlReconciliationEntryTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eft_instruction_id = ' . $this->sqlEftInstructionId() . ','; } elseif( true == array_key_exists( 'EftInstructionId', $this->getChangedColumns() ) ) { $strSql .= ' eft_instruction_id = ' . $this->sqlEftInstructionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_entry_detail_record_id = ' . $this->sqlNachaEntryDetailRecordId() . ','; } elseif( true == array_key_exists( 'NachaEntryDetailRecordId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_entry_detail_record_id = ' . $this->sqlNachaEntryDetailRecordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_return_addenda_detail_record_id = ' . $this->sqlNachaReturnAddendaDetailRecordId() . ','; } elseif( true == array_key_exists( 'NachaReturnAddendaDetailRecordId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_return_addenda_detail_record_id = ' . $this->sqlNachaReturnAddendaDetailRecordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_back_id = ' . $this->sqlChargeBackId() . ','; } elseif( true == array_key_exists( 'ChargeBackId', $this->getChangedColumns() ) ) { $strSql .= ' charge_back_id = ' . $this->sqlChargeBackId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eft_batch_id = ' . $this->sqlEftBatchId() . ','; } elseif( true == array_key_exists( 'EftBatchId', $this->getChangedColumns() ) ) { $strSql .= ' eft_batch_id = ' . $this->sqlEftBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_file_id = ' . $this->sqlX937FileId() . ','; } elseif( true == array_key_exists( 'X937FileId', $this->getChangedColumns() ) ) { $strSql .= ' x937_file_id = ' . $this->sqlX937FileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_return_file_id = ' . $this->sqlX937ReturnFileId() . ','; } elseif( true == array_key_exists( 'X937ReturnFileId', $this->getChangedColumns() ) ) { $strSql .= ' x937_return_file_id = ' . $this->sqlX937ReturnFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_return_file_id = ' . $this->sqlNachaReturnFileId() . ','; } elseif( true == array_key_exists( 'NachaReturnFileId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_return_file_id = ' . $this->sqlNachaReturnFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId() . ','; } elseif( true == array_key_exists( 'SettlementDistributionId', $this->getChangedColumns() ) ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' western_union_output_file_id = ' . $this->sqlWesternUnionOutputFileId() . ','; } elseif( true == array_key_exists( 'WesternUnionOutputFileId', $this->getChangedColumns() ) ) { $strSql .= ' western_union_output_file_id = ' . $this->sqlWesternUnionOutputFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId() . ','; } elseif( true == array_key_exists( 'CompanyPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bai2_file_id = ' . $this->sqlBai2FileId() . ','; } elseif( true == array_key_exists( 'Bai2FileId', $this->getChangedColumns() ) ) { $strSql .= ' bai2_file_id = ' . $this->sqlBai2FileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trace_number = ' . $this->sqlTraceNumber() . ','; } elseif( true == array_key_exists( 'TraceNumber', $this->getChangedColumns() ) ) { $strSql .= ' trace_number = ' . $this->sqlTraceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' movement_datetime = ' . $this->sqlMovementDatetime() . ','; } elseif( true == array_key_exists( 'MovementDatetime', $this->getChangedColumns() ) ) { $strSql .= ' movement_datetime = ' . $this->sqlMovementDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total = ' . $this->sqlTotal() . ','; } elseif( true == array_key_exists( 'Total', $this->getChangedColumns() ) ) { $strSql .= ' total = ' . $this->sqlTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_details = ' . $this->sqlTransactionDetails() . ','; } elseif( true == array_key_exists( 'TransactionDetails', $this->getChangedColumns() ) ) { $strSql .= ' transaction_details = ' . $this->sqlTransactionDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manual = ' . $this->sqlIsManual() . ','; } elseif( true == array_key_exists( 'IsManual', $this->getChangedColumns() ) ) { $strSql .= ' is_manual = ' . $this->sqlIsManual() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved() . ','; } elseif( true == array_key_exists( 'IsApproved', $this->getChangedColumns() ) ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' worldpay_return_id = ' . $this->sqlWorldpayReturnId() . ','; } elseif( true == array_key_exists( 'WorldpayReturnId', $this->getChangedColumns() ) ) { $strSql .= ' worldpay_return_id = ' . $this->sqlIsApproved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' aft_file_batch_id = ' . $this->sqlAftFileBatchId() . ','; } elseif( true == array_key_exists( 'AftFileBatchId', $this->getChangedColumns() ) ) { $strSql .= ' aft_file_batch_id = ' . $this->sqlIsApproved() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'processing_bank_account_id' => $this->getProcessingBankAccountId(),
			'reconciliation_id' => $this->getReconciliationId(),
			'reconciliation_entry_type_id' => $this->getReconciliationEntryTypeId(),
			'eft_instruction_id' => $this->getEftInstructionId(),
			'nacha_entry_detail_record_id' => $this->getNachaEntryDetailRecordId(),
			'nacha_return_addenda_detail_record_id' => $this->getNachaReturnAddendaDetailRecordId(),
			'charge_back_id' => $this->getChargeBackId(),
			'eft_batch_id' => $this->getEftBatchId(),
			'x937_file_id' => $this->getX937FileId(),
			'x937_return_file_id' => $this->getX937ReturnFileId(),
			'nacha_return_file_id' => $this->getNachaReturnFileId(),
			'settlement_distribution_id' => $this->getSettlementDistributionId(),
			'western_union_output_file_id' => $this->getWesternUnionOutputFileId(),
			'company_payment_id' => $this->getCompanyPaymentId(),
			'ar_payment_id' => $this->getArPaymentId(),
			'bai2_file_id' => $this->getBai2FileId(),
			'trace_number' => $this->getTraceNumber(),
			'movement_datetime' => $this->getMovementDatetime(),
			'total' => $this->getTotal(),
			'memo' => $this->getMemo(),
			'notes' => $this->getNotes(),
			'transaction_details' => $this->getTransactionDetails(),
			'is_manual' => $this->getIsManual(),
			'is_approved' => $this->getIsApproved(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'worldpay_return_id' => $this->getWorldpayReturnId(),
			'aft_file_batch_id' => $this->getAftFileBatchId()
		);
	}

}
?>