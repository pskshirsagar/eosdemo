<?php

class CBaseWorldpayTransactionDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.worldpay_transaction_details';

	protected $m_intId;
	protected $m_strReportingGroup;
	protected $m_strMerchantName;
	protected $m_strActivityDate;
	protected $m_strSettlementDate;
	protected $m_strVantivPaymentId;
	protected $m_strParentVantivPaymentId;
	protected $m_intMerchantOrderNumber;
	protected $m_strTxnType;
	protected $m_strPurchaseCurrency;
	protected $m_fltPurchaseAmount;
	protected $m_strSettlementCurrency;
	protected $m_fltSettlementAmount;
	protected $m_strPaymentType;
	protected $m_intPaymentTypeId;
	protected $m_strAccountSuffix;
	protected $m_strBin;
	protected $m_strResponseReasonMessage;
	protected $m_strBatchId;
	protected $m_strSessionId;
	protected $m_strArn;
	protected $m_strInterchangeRate;
	protected $m_strCustomerId;
	protected $m_intMerchantTransactionId;
	protected $m_strAffiliate;
	protected $m_strCampaign;
	protected $m_strMerchantGroupingId;
	protected $m_strTokenNumber;
	protected $m_strTransactionProcessingTimestampGmt;
	protected $m_fltApproxInterchangeFeeAmount;
	protected $m_fltInterchangeFlatRate;
	protected $m_fltInterchangePercentRate;
	protected $m_strFundingMethod;
	protected $m_strIssuingBank;
	protected $m_strBillingDescriptor;
	protected $m_strMerchantId;
	protected $m_strPresenter;
	protected $m_strApiReportingGroup;
	protected $m_strCustomerReference;
	protected $m_fltSecondaryAmt;
	protected $m_fltSecondarySettlementAmt;
	protected $m_fltRequestedAuthAmount;
	protected $m_strOriginalAuthCode;
	protected $m_strCustomerName;
	protected $m_strAddressLine1;
	protected $m_strAddressLine2;
	protected $m_strCity;
	protected $m_strState;
	protected $m_strPostalCode;
	protected $m_strFraudChecksumResponseCode;
	protected $m_strFraudChecksumResponseMessage;
	protected $m_strAvsResponseCode;
	protected $m_strAvsResponseMessage;
	protected $m_strTokenResponseCode;
	protected $m_strTokenResponseMessage;
	protected $m_strPinlessDebitNetwork;
	protected $m_strProcessedOn;
	protected $m_intPaymentFileId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['reporting_group'] ) && $boolDirectSet ) $this->set( 'm_strReportingGroup', trim( $arrValues['reporting_group'] ) ); elseif( isset( $arrValues['reporting_group'] ) ) $this->setReportingGroup( $arrValues['reporting_group'] );
		if( isset( $arrValues['merchant_name'] ) && $boolDirectSet ) $this->set( 'm_strMerchantName', trim( $arrValues['merchant_name'] ) ); elseif( isset( $arrValues['merchant_name'] ) ) $this->setMerchantName( $arrValues['merchant_name'] );
		if( isset( $arrValues['activity_date'] ) && $boolDirectSet ) $this->set( 'm_strActivityDate', trim( $arrValues['activity_date'] ) ); elseif( isset( $arrValues['activity_date'] ) ) $this->setActivityDate( $arrValues['activity_date'] );
		if( isset( $arrValues['settlement_date'] ) && $boolDirectSet ) $this->set( 'm_strSettlementDate', trim( $arrValues['settlement_date'] ) ); elseif( isset( $arrValues['settlement_date'] ) ) $this->setSettlementDate( $arrValues['settlement_date'] );
		if( isset( $arrValues['vantiv_payment_id'] ) && $boolDirectSet ) $this->set( 'm_strVantivPaymentId', trim( $arrValues['vantiv_payment_id'] ) ); elseif( isset( $arrValues['vantiv_payment_id'] ) ) $this->setVantivPaymentId( $arrValues['vantiv_payment_id'] );
		if( isset( $arrValues['parent_vantiv_payment_id'] ) && $boolDirectSet ) $this->set( 'm_strParentVantivPaymentId', trim( $arrValues['parent_vantiv_payment_id'] ) ); elseif( isset( $arrValues['parent_vantiv_payment_id'] ) ) $this->setParentVantivPaymentId( $arrValues['parent_vantiv_payment_id'] );
		if( isset( $arrValues['merchant_order_number'] ) && $boolDirectSet ) $this->set( 'm_intMerchantOrderNumber', trim( $arrValues['merchant_order_number'] ) ); elseif( isset( $arrValues['merchant_order_number'] ) ) $this->setMerchantOrderNumber( $arrValues['merchant_order_number'] );
		if( isset( $arrValues['txn_type'] ) && $boolDirectSet ) $this->set( 'm_strTxnType', trim( $arrValues['txn_type'] ) ); elseif( isset( $arrValues['txn_type'] ) ) $this->setTxnType( $arrValues['txn_type'] );
		if( isset( $arrValues['purchase_currency'] ) && $boolDirectSet ) $this->set( 'm_strPurchaseCurrency', trim( $arrValues['purchase_currency'] ) ); elseif( isset( $arrValues['purchase_currency'] ) ) $this->setPurchaseCurrency( $arrValues['purchase_currency'] );
		if( isset( $arrValues['purchase_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPurchaseAmount', trim( $arrValues['purchase_amount'] ) ); elseif( isset( $arrValues['purchase_amount'] ) ) $this->setPurchaseAmount( $arrValues['purchase_amount'] );
		if( isset( $arrValues['settlement_currency'] ) && $boolDirectSet ) $this->set( 'm_strSettlementCurrency', trim( $arrValues['settlement_currency'] ) ); elseif( isset( $arrValues['settlement_currency'] ) ) $this->setSettlementCurrency( $arrValues['settlement_currency'] );
		if( isset( $arrValues['settlement_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSettlementAmount', trim( $arrValues['settlement_amount'] ) ); elseif( isset( $arrValues['settlement_amount'] ) ) $this->setSettlementAmount( $arrValues['settlement_amount'] );
		if( isset( $arrValues['payment_type'] ) && $boolDirectSet ) $this->set( 'm_strPaymentType', trim( $arrValues['payment_type'] ) ); elseif( isset( $arrValues['payment_type'] ) ) $this->setPaymentType( $arrValues['payment_type'] );
		if( isset( $arrValues['payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentTypeId', trim( $arrValues['payment_type_id'] ) ); elseif( isset( $arrValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrValues['payment_type_id'] );
		if( isset( $arrValues['account_suffix'] ) && $boolDirectSet ) $this->set( 'm_strAccountSuffix', trim( $arrValues['account_suffix'] ) ); elseif( isset( $arrValues['account_suffix'] ) ) $this->setAccountSuffix( $arrValues['account_suffix'] );
		if( isset( $arrValues['bin'] ) && $boolDirectSet ) $this->set( 'm_strBin', trim( $arrValues['bin'] ) ); elseif( isset( $arrValues['bin'] ) ) $this->setBin( $arrValues['bin'] );
		if( isset( $arrValues['response_reason_message'] ) && $boolDirectSet ) $this->set( 'm_strResponseReasonMessage', trim( $arrValues['response_reason_message'] ) ); elseif( isset( $arrValues['response_reason_message'] ) ) $this->setResponseReasonMessage( $arrValues['response_reason_message'] );
		if( isset( $arrValues['batch_id'] ) && $boolDirectSet ) $this->set( 'm_strBatchId', trim( $arrValues['batch_id'] ) ); elseif( isset( $arrValues['batch_id'] ) ) $this->setBatchId( $arrValues['batch_id'] );
		if( isset( $arrValues['session_id'] ) && $boolDirectSet ) $this->set( 'm_strSessionId', trim( $arrValues['session_id'] ) ); elseif( isset( $arrValues['session_id'] ) ) $this->setSessionId( $arrValues['session_id'] );
		if( isset( $arrValues['arn'] ) && $boolDirectSet ) $this->set( 'm_strArn', trim( $arrValues['arn'] ) ); elseif( isset( $arrValues['arn'] ) ) $this->setArn( $arrValues['arn'] );
		if( isset( $arrValues['interchange_rate'] ) && $boolDirectSet ) $this->set( 'm_strInterchangeRate', trim( $arrValues['interchange_rate'] ) ); elseif( isset( $arrValues['interchange_rate'] ) ) $this->setInterchangeRate( $arrValues['interchange_rate'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_strCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['merchant_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantTransactionId', trim( $arrValues['merchant_transaction_id'] ) ); elseif( isset( $arrValues['merchant_transaction_id'] ) ) $this->setMerchantTransactionId( $arrValues['merchant_transaction_id'] );
		if( isset( $arrValues['affiliate'] ) && $boolDirectSet ) $this->set( 'm_strAffiliate', trim( $arrValues['affiliate'] ) ); elseif( isset( $arrValues['affiliate'] ) ) $this->setAffiliate( $arrValues['affiliate'] );
		if( isset( $arrValues['campaign'] ) && $boolDirectSet ) $this->set( 'm_strCampaign', trim( $arrValues['campaign'] ) ); elseif( isset( $arrValues['campaign'] ) ) $this->setCampaign( $arrValues['campaign'] );
		if( isset( $arrValues['merchant_grouping_id'] ) && $boolDirectSet ) $this->set( 'm_strMerchantGroupingId', trim( $arrValues['merchant_grouping_id'] ) ); elseif( isset( $arrValues['merchant_grouping_id'] ) ) $this->setMerchantGroupingId( $arrValues['merchant_grouping_id'] );
		if( isset( $arrValues['token_number'] ) && $boolDirectSet ) $this->set( 'm_strTokenNumber', trim( $arrValues['token_number'] ) ); elseif( isset( $arrValues['token_number'] ) ) $this->setTokenNumber( $arrValues['token_number'] );
		if( isset( $arrValues['transaction_processing_timestamp_gmt'] ) && $boolDirectSet ) $this->set( 'm_strTransactionProcessingTimestampGmt', trim( $arrValues['transaction_processing_timestamp_gmt'] ) ); elseif( isset( $arrValues['transaction_processing_timestamp_gmt'] ) ) $this->setTransactionProcessingTimestampGmt( $arrValues['transaction_processing_timestamp_gmt'] );
		if( isset( $arrValues['approx_interchange_fee_amount'] ) && $boolDirectSet ) $this->set( 'm_fltApproxInterchangeFeeAmount', trim( $arrValues['approx_interchange_fee_amount'] ) ); elseif( isset( $arrValues['approx_interchange_fee_amount'] ) ) $this->setApproxInterchangeFeeAmount( $arrValues['approx_interchange_fee_amount'] );
		if( isset( $arrValues['interchange_flat_rate'] ) && $boolDirectSet ) $this->set( 'm_fltInterchangeFlatRate', trim( $arrValues['interchange_flat_rate'] ) ); elseif( isset( $arrValues['interchange_flat_rate'] ) ) $this->setInterchangeFlatRate( $arrValues['interchange_flat_rate'] );
		if( isset( $arrValues['interchange_percent_rate'] ) && $boolDirectSet ) $this->set( 'm_fltInterchangePercentRate', trim( $arrValues['interchange_percent_rate'] ) ); elseif( isset( $arrValues['interchange_percent_rate'] ) ) $this->setInterchangePercentRate( $arrValues['interchange_percent_rate'] );
		if( isset( $arrValues['funding_method'] ) && $boolDirectSet ) $this->set( 'm_strFundingMethod', trim( $arrValues['funding_method'] ) ); elseif( isset( $arrValues['funding_method'] ) ) $this->setFundingMethod( $arrValues['funding_method'] );
		if( isset( $arrValues['issuing_bank'] ) && $boolDirectSet ) $this->set( 'm_strIssuingBank', trim( $arrValues['issuing_bank'] ) ); elseif( isset( $arrValues['issuing_bank'] ) ) $this->setIssuingBank( $arrValues['issuing_bank'] );
		if( isset( $arrValues['billing_descriptor'] ) && $boolDirectSet ) $this->set( 'm_strBillingDescriptor', trim( $arrValues['billing_descriptor'] ) ); elseif( isset( $arrValues['billing_descriptor'] ) ) $this->setBillingDescriptor( $arrValues['billing_descriptor'] );
		if( isset( $arrValues['merchant_id'] ) && $boolDirectSet ) $this->set( 'm_strMerchantId', trim( $arrValues['merchant_id'] ) ); elseif( isset( $arrValues['merchant_id'] ) ) $this->setMerchantId( $arrValues['merchant_id'] );
		if( isset( $arrValues['presenter'] ) && $boolDirectSet ) $this->set( 'm_strPresenter', trim( $arrValues['presenter'] ) ); elseif( isset( $arrValues['presenter'] ) ) $this->setPresenter( $arrValues['presenter'] );
		if( isset( $arrValues['api_reporting_group'] ) && $boolDirectSet ) $this->set( 'm_strApiReportingGroup', trim( $arrValues['api_reporting_group'] ) ); elseif( isset( $arrValues['api_reporting_group'] ) ) $this->setApiReportingGroup( $arrValues['api_reporting_group'] );
		if( isset( $arrValues['customer_reference'] ) && $boolDirectSet ) $this->set( 'm_strCustomerReference', trim( $arrValues['customer_reference'] ) ); elseif( isset( $arrValues['customer_reference'] ) ) $this->setCustomerReference( $arrValues['customer_reference'] );
		if( isset( $arrValues['secondary_amt'] ) && $boolDirectSet ) $this->set( 'm_fltSecondaryAmt', trim( $arrValues['secondary_amt'] ) ); elseif( isset( $arrValues['secondary_amt'] ) ) $this->setSecondaryAmt( $arrValues['secondary_amt'] );
		if( isset( $arrValues['secondary_settlement_amt'] ) && $boolDirectSet ) $this->set( 'm_fltSecondarySettlementAmt', trim( $arrValues['secondary_settlement_amt'] ) ); elseif( isset( $arrValues['secondary_settlement_amt'] ) ) $this->setSecondarySettlementAmt( $arrValues['secondary_settlement_amt'] );
		if( isset( $arrValues['requested_auth_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRequestedAuthAmount', trim( $arrValues['requested_auth_amount'] ) ); elseif( isset( $arrValues['requested_auth_amount'] ) ) $this->setRequestedAuthAmount( $arrValues['requested_auth_amount'] );
		if( isset( $arrValues['original_auth_code'] ) && $boolDirectSet ) $this->set( 'm_strOriginalAuthCode', trim( $arrValues['original_auth_code'] ) ); elseif( isset( $arrValues['original_auth_code'] ) ) $this->setOriginalAuthCode( $arrValues['original_auth_code'] );
		if( isset( $arrValues['customer_name'] ) && $boolDirectSet ) $this->set( 'm_strCustomerName', trim( $arrValues['customer_name'] ) ); elseif( isset( $arrValues['customer_name'] ) ) $this->setCustomerName( $arrValues['customer_name'] );
		if( isset( $arrValues['address_line_1'] ) && $boolDirectSet ) $this->set( 'm_strAddressLine1', trim( $arrValues['address_line_1'] ) ); elseif( isset( $arrValues['address_line_1'] ) ) $this->setAddressLine1( $arrValues['address_line_1'] );
		if( isset( $arrValues['address_line_2'] ) && $boolDirectSet ) $this->set( 'm_strAddressLine2', trim( $arrValues['address_line_2'] ) ); elseif( isset( $arrValues['address_line_2'] ) ) $this->setAddressLine2( $arrValues['address_line_2'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( $arrValues['city'] ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( $arrValues['city'] );
		if( isset( $arrValues['state'] ) && $boolDirectSet ) $this->set( 'm_strState', trim( $arrValues['state'] ) ); elseif( isset( $arrValues['state'] ) ) $this->setState( $arrValues['state'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( $arrValues['postal_code'] ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( $arrValues['postal_code'] );
		if( isset( $arrValues['fraud_checksum_response_code'] ) && $boolDirectSet ) $this->set( 'm_strFraudChecksumResponseCode', trim( $arrValues['fraud_checksum_response_code'] ) ); elseif( isset( $arrValues['fraud_checksum_response_code'] ) ) $this->setFraudChecksumResponseCode( $arrValues['fraud_checksum_response_code'] );
		if( isset( $arrValues['fraud_checksum_response_message'] ) && $boolDirectSet ) $this->set( 'm_strFraudChecksumResponseMessage', trim( $arrValues['fraud_checksum_response_message'] ) ); elseif( isset( $arrValues['fraud_checksum_response_message'] ) ) $this->setFraudChecksumResponseMessage( $arrValues['fraud_checksum_response_message'] );
		if( isset( $arrValues['avs_response_code'] ) && $boolDirectSet ) $this->set( 'm_strAvsResponseCode', trim( $arrValues['avs_response_code'] ) ); elseif( isset( $arrValues['avs_response_code'] ) ) $this->setAvsResponseCode( $arrValues['avs_response_code'] );
		if( isset( $arrValues['avs_response_message'] ) && $boolDirectSet ) $this->set( 'm_strAvsResponseMessage', trim( $arrValues['avs_response_message'] ) ); elseif( isset( $arrValues['avs_response_message'] ) ) $this->setAvsResponseMessage( $arrValues['avs_response_message'] );
		if( isset( $arrValues['token_response_code'] ) && $boolDirectSet ) $this->set( 'm_strTokenResponseCode', trim( $arrValues['token_response_code'] ) ); elseif( isset( $arrValues['token_response_code'] ) ) $this->setTokenResponseCode( $arrValues['token_response_code'] );
		if( isset( $arrValues['token_response_message'] ) && $boolDirectSet ) $this->set( 'm_strTokenResponseMessage', trim( $arrValues['token_response_message'] ) ); elseif( isset( $arrValues['token_response_message'] ) ) $this->setTokenResponseMessage( $arrValues['token_response_message'] );
		if( isset( $arrValues['pinless_debit_network'] ) && $boolDirectSet ) $this->set( 'm_strPinlessDebitNetwork', trim( $arrValues['pinless_debit_network'] ) ); elseif( isset( $arrValues['pinless_debit_network'] ) ) $this->setPinlessDebitNetwork( $arrValues['pinless_debit_network'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['payment_file_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentFileId', trim( $arrValues['payment_file_id'] ) ); elseif( isset( $arrValues['payment_file_id'] ) ) $this->setPaymentFileId( $arrValues['payment_file_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setReportingGroup( $strReportingGroup ) {
		$this->set( 'm_strReportingGroup', CStrings::strTrimDef( $strReportingGroup, 255, NULL, true ) );
	}

	public function getReportingGroup() {
		return $this->m_strReportingGroup;
	}

	public function sqlReportingGroup() {
		return ( true == isset( $this->m_strReportingGroup ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReportingGroup ) : '\'' . addslashes( $this->m_strReportingGroup ) . '\'' ) : 'NULL';
	}

	public function setMerchantName( $strMerchantName ) {
		$this->set( 'm_strMerchantName', CStrings::strTrimDef( $strMerchantName, 500, NULL, true ) );
	}

	public function getMerchantName() {
		return $this->m_strMerchantName;
	}

	public function sqlMerchantName() {
		return ( true == isset( $this->m_strMerchantName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMerchantName ) : '\'' . addslashes( $this->m_strMerchantName ) . '\'' ) : 'NULL';
	}

	public function setActivityDate( $strActivityDate ) {
		$this->set( 'm_strActivityDate', CStrings::strTrimDef( $strActivityDate, -1, NULL, true ) );
	}

	public function getActivityDate() {
		return $this->m_strActivityDate;
	}

	public function sqlActivityDate() {
		return ( true == isset( $this->m_strActivityDate ) ) ? '\'' . $this->m_strActivityDate . '\'' : 'NULL';
	}

	public function setSettlementDate( $strSettlementDate ) {
		$this->set( 'm_strSettlementDate', CStrings::strTrimDef( $strSettlementDate, -1, NULL, true ) );
	}

	public function getSettlementDate() {
		return $this->m_strSettlementDate;
	}

	public function sqlSettlementDate() {
		return ( true == isset( $this->m_strSettlementDate ) ) ? '\'' . $this->m_strSettlementDate . '\'' : 'NULL';
	}

	public function setVantivPaymentId( $strVantivPaymentId ) {
		$this->set( 'm_strVantivPaymentId', CStrings::strTrimDef( $strVantivPaymentId, 255, NULL, true ) );
	}

	public function getVantivPaymentId() {
		return $this->m_strVantivPaymentId;
	}

	public function sqlVantivPaymentId() {
		return ( true == isset( $this->m_strVantivPaymentId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strVantivPaymentId ) : '\'' . addslashes( $this->m_strVantivPaymentId ) . '\'' ) : 'NULL';
	}

	public function setParentVantivPaymentId( $strParentVantivPaymentId ) {
		$this->set( 'm_strParentVantivPaymentId', CStrings::strTrimDef( $strParentVantivPaymentId, 255, NULL, true ) );
	}

	public function getParentVantivPaymentId() {
		return $this->m_strParentVantivPaymentId;
	}

	public function sqlParentVantivPaymentId() {
		return ( true == isset( $this->m_strParentVantivPaymentId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strParentVantivPaymentId ) : '\'' . addslashes( $this->m_strParentVantivPaymentId ) . '\'' ) : 'NULL';
	}

	public function setMerchantOrderNumber( $intMerchantOrderNumber ) {
		$this->set( 'm_intMerchantOrderNumber', CStrings::strToIntDef( $intMerchantOrderNumber, NULL, false ) );
	}

	public function getMerchantOrderNumber() {
		return $this->m_intMerchantOrderNumber;
	}

	public function sqlMerchantOrderNumber() {
		return ( true == isset( $this->m_intMerchantOrderNumber ) ) ? ( string ) $this->m_intMerchantOrderNumber : 'NULL';
	}

	public function setTxnType( $strTxnType ) {
		$this->set( 'm_strTxnType', CStrings::strTrimDef( $strTxnType, 50, NULL, true ) );
	}

	public function getTxnType() {
		return $this->m_strTxnType;
	}

	public function sqlTxnType() {
		return ( true == isset( $this->m_strTxnType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTxnType ) : '\'' . addslashes( $this->m_strTxnType ) . '\'' ) : 'NULL';
	}

	public function setPurchaseCurrency( $strPurchaseCurrency ) {
		$this->set( 'm_strPurchaseCurrency', CStrings::strTrimDef( $strPurchaseCurrency, 5, NULL, true ) );
	}

	public function getPurchaseCurrency() {
		return $this->m_strPurchaseCurrency;
	}

	public function sqlPurchaseCurrency() {
		return ( true == isset( $this->m_strPurchaseCurrency ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPurchaseCurrency ) : '\'' . addslashes( $this->m_strPurchaseCurrency ) . '\'' ) : 'NULL';
	}

	public function setPurchaseAmount( $fltPurchaseAmount ) {
		$this->set( 'm_fltPurchaseAmount', CStrings::strToFloatDef( $fltPurchaseAmount, NULL, false, 0 ) );
	}

	public function getPurchaseAmount() {
		return $this->m_fltPurchaseAmount;
	}

	public function sqlPurchaseAmount() {
		return ( true == isset( $this->m_fltPurchaseAmount ) ) ? ( string ) $this->m_fltPurchaseAmount : 'NULL';
	}

	public function setSettlementCurrency( $strSettlementCurrency ) {
		$this->set( 'm_strSettlementCurrency', CStrings::strTrimDef( $strSettlementCurrency, 5, NULL, true ) );
	}

	public function getSettlementCurrency() {
		return $this->m_strSettlementCurrency;
	}

	public function sqlSettlementCurrency() {
		return ( true == isset( $this->m_strSettlementCurrency ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSettlementCurrency ) : '\'' . addslashes( $this->m_strSettlementCurrency ) . '\'' ) : 'NULL';
	}

	public function setSettlementAmount( $fltSettlementAmount ) {
		$this->set( 'm_fltSettlementAmount', CStrings::strToFloatDef( $fltSettlementAmount, NULL, false, 0 ) );
	}

	public function getSettlementAmount() {
		return $this->m_fltSettlementAmount;
	}

	public function sqlSettlementAmount() {
		return ( true == isset( $this->m_fltSettlementAmount ) ) ? ( string ) $this->m_fltSettlementAmount : 'NULL';
	}

	public function setPaymentType( $strPaymentType ) {
		$this->set( 'm_strPaymentType', CStrings::strTrimDef( $strPaymentType, 25, NULL, true ) );
	}

	public function getPaymentType() {
		return $this->m_strPaymentType;
	}

	public function sqlPaymentType() {
		return ( true == isset( $this->m_strPaymentType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPaymentType ) : '\'' . addslashes( $this->m_strPaymentType ) . '\'' ) : 'NULL';
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->set( 'm_intPaymentTypeId', CStrings::strToIntDef( $intPaymentTypeId, NULL, false ) );
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function sqlPaymentTypeId() {
		return ( true == isset( $this->m_intPaymentTypeId ) ) ? ( string ) $this->m_intPaymentTypeId : 'NULL';
	}

	public function setAccountSuffix( $strAccountSuffix ) {
		$this->set( 'm_strAccountSuffix', CStrings::strTrimDef( $strAccountSuffix, 4, NULL, true ) );
	}

	public function getAccountSuffix() {
		return $this->m_strAccountSuffix;
	}

	public function sqlAccountSuffix() {
		return ( true == isset( $this->m_strAccountSuffix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAccountSuffix ) : '\'' . addslashes( $this->m_strAccountSuffix ) . '\'' ) : 'NULL';
	}

	public function setBin( $strBin ) {
		$this->set( 'm_strBin', CStrings::strTrimDef( $strBin, 10, NULL, true ) );
	}

	public function getBin() {
		return $this->m_strBin;
	}

	public function sqlBin() {
		return ( true == isset( $this->m_strBin ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBin ) : '\'' . addslashes( $this->m_strBin ) . '\'' ) : 'NULL';
	}

	public function setResponseReasonMessage( $strResponseReasonMessage ) {
		$this->set( 'm_strResponseReasonMessage', CStrings::strTrimDef( $strResponseReasonMessage, 255, NULL, true ) );
	}

	public function getResponseReasonMessage() {
		return $this->m_strResponseReasonMessage;
	}

	public function sqlResponseReasonMessage() {
		return ( true == isset( $this->m_strResponseReasonMessage ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResponseReasonMessage ) : '\'' . addslashes( $this->m_strResponseReasonMessage ) . '\'' ) : 'NULL';
	}

	public function setBatchId( $strBatchId ) {
		$this->set( 'm_strBatchId', CStrings::strTrimDef( $strBatchId, 255, NULL, true ) );
	}

	public function getBatchId() {
		return $this->m_strBatchId;
	}

	public function sqlBatchId() {
		return ( true == isset( $this->m_strBatchId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBatchId ) : '\'' . addslashes( $this->m_strBatchId ) . '\'' ) : 'NULL';
	}

	public function setSessionId( $strSessionId ) {
		$this->set( 'm_strSessionId', CStrings::strTrimDef( $strSessionId, 255, NULL, true ) );
	}

	public function getSessionId() {
		return $this->m_strSessionId;
	}

	public function sqlSessionId() {
		return ( true == isset( $this->m_strSessionId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSessionId ) : '\'' . addslashes( $this->m_strSessionId ) . '\'' ) : 'NULL';
	}

	public function setArn( $strArn ) {
		$this->set( 'm_strArn', CStrings::strTrimDef( $strArn, 255, NULL, true ) );
	}

	public function getArn() {
		return $this->m_strArn;
	}

	public function sqlArn() {
		return ( true == isset( $this->m_strArn ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strArn ) : '\'' . addslashes( $this->m_strArn ) . '\'' ) : 'NULL';
	}

	public function setInterchangeRate( $strInterchangeRate ) {
		$this->set( 'm_strInterchangeRate', CStrings::strTrimDef( $strInterchangeRate, 255, NULL, true ) );
	}

	public function getInterchangeRate() {
		return $this->m_strInterchangeRate;
	}

	public function sqlInterchangeRate() {
		return ( true == isset( $this->m_strInterchangeRate ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInterchangeRate ) : '\'' . addslashes( $this->m_strInterchangeRate ) . '\'' ) : 'NULL';
	}

	public function setCustomerId( $strCustomerId ) {
		$this->set( 'm_strCustomerId', CStrings::strTrimDef( $strCustomerId, 50, NULL, true ) );
	}

	public function getCustomerId() {
		return $this->m_strCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_strCustomerId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCustomerId ) : '\'' . addslashes( $this->m_strCustomerId ) . '\'' ) : 'NULL';
	}

	public function setMerchantTransactionId( $intMerchantTransactionId ) {
		$this->set( 'm_intMerchantTransactionId', CStrings::strToIntDef( $intMerchantTransactionId, NULL, false ) );
	}

	public function getMerchantTransactionId() {
		return $this->m_intMerchantTransactionId;
	}

	public function sqlMerchantTransactionId() {
		return ( true == isset( $this->m_intMerchantTransactionId ) ) ? ( string ) $this->m_intMerchantTransactionId : 'NULL';
	}

	public function setAffiliate( $strAffiliate ) {
		$this->set( 'm_strAffiliate', CStrings::strTrimDef( $strAffiliate, 50, NULL, true ) );
	}

	public function getAffiliate() {
		return $this->m_strAffiliate;
	}

	public function sqlAffiliate() {
		return ( true == isset( $this->m_strAffiliate ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAffiliate ) : '\'' . addslashes( $this->m_strAffiliate ) . '\'' ) : 'NULL';
	}

	public function setCampaign( $strCampaign ) {
		$this->set( 'm_strCampaign', CStrings::strTrimDef( $strCampaign, 50, NULL, true ) );
	}

	public function getCampaign() {
		return $this->m_strCampaign;
	}

	public function sqlCampaign() {
		return ( true == isset( $this->m_strCampaign ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCampaign ) : '\'' . addslashes( $this->m_strCampaign ) . '\'' ) : 'NULL';
	}

	public function setMerchantGroupingId( $strMerchantGroupingId ) {
		$this->set( 'm_strMerchantGroupingId', CStrings::strTrimDef( $strMerchantGroupingId, 50, NULL, true ) );
	}

	public function getMerchantGroupingId() {
		return $this->m_strMerchantGroupingId;
	}

	public function sqlMerchantGroupingId() {
		return ( true == isset( $this->m_strMerchantGroupingId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMerchantGroupingId ) : '\'' . addslashes( $this->m_strMerchantGroupingId ) . '\'' ) : 'NULL';
	}

	public function setTokenNumber( $strTokenNumber ) {
		$this->set( 'm_strTokenNumber', CStrings::strTrimDef( $strTokenNumber, 50, NULL, true ) );
	}

	public function getTokenNumber() {
		return $this->m_strTokenNumber;
	}

	public function sqlTokenNumber() {
		return ( true == isset( $this->m_strTokenNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTokenNumber ) : '\'' . addslashes( $this->m_strTokenNumber ) . '\'' ) : 'NULL';
	}

	public function setTransactionProcessingTimestampGmt( $strTransactionProcessingTimestampGmt ) {
		$this->set( 'm_strTransactionProcessingTimestampGmt', CStrings::strTrimDef( $strTransactionProcessingTimestampGmt, -1, NULL, true ) );
	}

	public function getTransactionProcessingTimestampGmt() {
		return $this->m_strTransactionProcessingTimestampGmt;
	}

	public function sqlTransactionProcessingTimestampGmt() {
		return ( true == isset( $this->m_strTransactionProcessingTimestampGmt ) ) ? '\'' . $this->m_strTransactionProcessingTimestampGmt . '\'' : 'NULL';
	}

	public function setApproxInterchangeFeeAmount( $fltApproxInterchangeFeeAmount ) {
		$this->set( 'm_fltApproxInterchangeFeeAmount', CStrings::strToFloatDef( $fltApproxInterchangeFeeAmount, NULL, false, 0 ) );
	}

	public function getApproxInterchangeFeeAmount() {
		return $this->m_fltApproxInterchangeFeeAmount;
	}

	public function sqlApproxInterchangeFeeAmount() {
		return ( true == isset( $this->m_fltApproxInterchangeFeeAmount ) ) ? ( string ) $this->m_fltApproxInterchangeFeeAmount : 'NULL';
	}

	public function setInterchangeFlatRate( $fltInterchangeFlatRate ) {
		$this->set( 'm_fltInterchangeFlatRate', CStrings::strToFloatDef( $fltInterchangeFlatRate, NULL, false, 0 ) );
	}

	public function getInterchangeFlatRate() {
		return $this->m_fltInterchangeFlatRate;
	}

	public function sqlInterchangeFlatRate() {
		return ( true == isset( $this->m_fltInterchangeFlatRate ) ) ? ( string ) $this->m_fltInterchangeFlatRate : 'NULL';
	}

	public function setInterchangePercentRate( $fltInterchangePercentRate ) {
		$this->set( 'm_fltInterchangePercentRate', CStrings::strToFloatDef( $fltInterchangePercentRate, NULL, false, 0 ) );
	}

	public function getInterchangePercentRate() {
		return $this->m_fltInterchangePercentRate;
	}

	public function sqlInterchangePercentRate() {
		return ( true == isset( $this->m_fltInterchangePercentRate ) ) ? ( string ) $this->m_fltInterchangePercentRate : 'NULL';
	}

	public function setFundingMethod( $strFundingMethod ) {
		$this->set( 'm_strFundingMethod', CStrings::strTrimDef( $strFundingMethod, 25, NULL, true ) );
	}

	public function getFundingMethod() {
		return $this->m_strFundingMethod;
	}

	public function sqlFundingMethod() {
		return ( true == isset( $this->m_strFundingMethod ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFundingMethod ) : '\'' . addslashes( $this->m_strFundingMethod ) . '\'' ) : 'NULL';
	}

	public function setIssuingBank( $strIssuingBank ) {
		$this->set( 'm_strIssuingBank', CStrings::strTrimDef( $strIssuingBank, 255, NULL, true ) );
	}

	public function getIssuingBank() {
		return $this->m_strIssuingBank;
	}

	public function sqlIssuingBank() {
		return ( true == isset( $this->m_strIssuingBank ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIssuingBank ) : '\'' . addslashes( $this->m_strIssuingBank ) . '\'' ) : 'NULL';
	}

	public function setBillingDescriptor( $strBillingDescriptor ) {
		$this->set( 'm_strBillingDescriptor', CStrings::strTrimDef( $strBillingDescriptor, 255, NULL, true ) );
	}

	public function getBillingDescriptor() {
		return $this->m_strBillingDescriptor;
	}

	public function sqlBillingDescriptor() {
		return ( true == isset( $this->m_strBillingDescriptor ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBillingDescriptor ) : '\'' . addslashes( $this->m_strBillingDescriptor ) . '\'' ) : 'NULL';
	}

	public function setMerchantId( $strMerchantId ) {
		$this->set( 'm_strMerchantId', CStrings::strTrimDef( $strMerchantId, 50, NULL, true ) );
	}

	public function getMerchantId() {
		return $this->m_strMerchantId;
	}

	public function sqlMerchantId() {
		return ( true == isset( $this->m_strMerchantId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMerchantId ) : '\'' . addslashes( $this->m_strMerchantId ) . '\'' ) : 'NULL';
	}

	public function setPresenter( $strPresenter ) {
		$this->set( 'm_strPresenter', CStrings::strTrimDef( $strPresenter, 20, NULL, true ) );
	}

	public function getPresenter() {
		return $this->m_strPresenter;
	}

	public function sqlPresenter() {
		return ( true == isset( $this->m_strPresenter ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPresenter ) : '\'' . addslashes( $this->m_strPresenter ) . '\'' ) : 'NULL';
	}

	public function setApiReportingGroup( $strApiReportingGroup ) {
		$this->set( 'm_strApiReportingGroup', CStrings::strTrimDef( $strApiReportingGroup, 50, NULL, true ) );
	}

	public function getApiReportingGroup() {
		return $this->m_strApiReportingGroup;
	}

	public function sqlApiReportingGroup() {
		return ( true == isset( $this->m_strApiReportingGroup ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApiReportingGroup ) : '\'' . addslashes( $this->m_strApiReportingGroup ) . '\'' ) : 'NULL';
	}

	public function setCustomerReference( $strCustomerReference ) {
		$this->set( 'm_strCustomerReference', CStrings::strTrimDef( $strCustomerReference, 255, NULL, true ) );
	}

	public function getCustomerReference() {
		return $this->m_strCustomerReference;
	}

	public function sqlCustomerReference() {
		return ( true == isset( $this->m_strCustomerReference ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCustomerReference ) : '\'' . addslashes( $this->m_strCustomerReference ) . '\'' ) : 'NULL';
	}

	public function setSecondaryAmt( $fltSecondaryAmt ) {
		$this->set( 'm_fltSecondaryAmt', CStrings::strToFloatDef( $fltSecondaryAmt, NULL, false, 0 ) );
	}

	public function getSecondaryAmt() {
		return $this->m_fltSecondaryAmt;
	}

	public function sqlSecondaryAmt() {
		return ( true == isset( $this->m_fltSecondaryAmt ) ) ? ( string ) $this->m_fltSecondaryAmt : 'NULL';
	}

	public function setSecondarySettlementAmt( $fltSecondarySettlementAmt ) {
		$this->set( 'm_fltSecondarySettlementAmt', CStrings::strToFloatDef( $fltSecondarySettlementAmt, NULL, false, 0 ) );
	}

	public function getSecondarySettlementAmt() {
		return $this->m_fltSecondarySettlementAmt;
	}

	public function sqlSecondarySettlementAmt() {
		return ( true == isset( $this->m_fltSecondarySettlementAmt ) ) ? ( string ) $this->m_fltSecondarySettlementAmt : 'NULL';
	}

	public function setRequestedAuthAmount( $fltRequestedAuthAmount ) {
		$this->set( 'm_fltRequestedAuthAmount', CStrings::strToFloatDef( $fltRequestedAuthAmount, NULL, false, 0 ) );
	}

	public function getRequestedAuthAmount() {
		return $this->m_fltRequestedAuthAmount;
	}

	public function sqlRequestedAuthAmount() {
		return ( true == isset( $this->m_fltRequestedAuthAmount ) ) ? ( string ) $this->m_fltRequestedAuthAmount : 'NULL';
	}

	public function setOriginalAuthCode( $strOriginalAuthCode ) {
		$this->set( 'm_strOriginalAuthCode', CStrings::strTrimDef( $strOriginalAuthCode, 50, NULL, true ) );
	}

	public function getOriginalAuthCode() {
		return $this->m_strOriginalAuthCode;
	}

	public function sqlOriginalAuthCode() {
		return ( true == isset( $this->m_strOriginalAuthCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOriginalAuthCode ) : '\'' . addslashes( $this->m_strOriginalAuthCode ) . '\'' ) : 'NULL';
	}

	public function setCustomerName( $strCustomerName ) {
		$this->set( 'm_strCustomerName', CStrings::strTrimDef( $strCustomerName, 255, NULL, true ) );
	}

	public function getCustomerName() {
		return $this->m_strCustomerName;
	}

	public function sqlCustomerName() {
		return ( true == isset( $this->m_strCustomerName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCustomerName ) : '\'' . addslashes( $this->m_strCustomerName ) . '\'' ) : 'NULL';
	}

	public function setAddressLine1( $strAddressLine1 ) {
		$this->set( 'm_strAddressLine1', CStrings::strTrimDef( $strAddressLine1, 255, NULL, true ) );
	}

	public function getAddressLine1() {
		return $this->m_strAddressLine1;
	}

	public function sqlAddressLine1() {
		return ( true == isset( $this->m_strAddressLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAddressLine1 ) : '\'' . addslashes( $this->m_strAddressLine1 ) . '\'' ) : 'NULL';
	}

	public function setAddressLine2( $strAddressLine2 ) {
		$this->set( 'm_strAddressLine2', CStrings::strTrimDef( $strAddressLine2, 255, NULL, true ) );
	}

	public function getAddressLine2() {
		return $this->m_strAddressLine2;
	}

	public function sqlAddressLine2() {
		return ( true == isset( $this->m_strAddressLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAddressLine2 ) : '\'' . addslashes( $this->m_strAddressLine2 ) . '\'' ) : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 255, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCity ) : '\'' . addslashes( $this->m_strCity ) . '\'' ) : 'NULL';
	}

	public function setState( $strState ) {
		$this->set( 'm_strState', CStrings::strTrimDef( $strState, 50, NULL, true ) );
	}

	public function getState() {
		return $this->m_strState;
	}

	public function sqlState() {
		return ( true == isset( $this->m_strState ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strState ) : '\'' . addslashes( $this->m_strState ) . '\'' ) : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 15, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostalCode ) : '\'' . addslashes( $this->m_strPostalCode ) . '\'' ) : 'NULL';
	}

	public function setFraudChecksumResponseCode( $strFraudChecksumResponseCode ) {
		$this->set( 'm_strFraudChecksumResponseCode', CStrings::strTrimDef( $strFraudChecksumResponseCode, 50, NULL, true ) );
	}

	public function getFraudChecksumResponseCode() {
		return $this->m_strFraudChecksumResponseCode;
	}

	public function sqlFraudChecksumResponseCode() {
		return ( true == isset( $this->m_strFraudChecksumResponseCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFraudChecksumResponseCode ) : '\'' . addslashes( $this->m_strFraudChecksumResponseCode ) . '\'' ) : 'NULL';
	}

	public function setFraudChecksumResponseMessage( $strFraudChecksumResponseMessage ) {
		$this->set( 'm_strFraudChecksumResponseMessage', CStrings::strTrimDef( $strFraudChecksumResponseMessage, 255, NULL, true ) );
	}

	public function getFraudChecksumResponseMessage() {
		return $this->m_strFraudChecksumResponseMessage;
	}

	public function sqlFraudChecksumResponseMessage() {
		return ( true == isset( $this->m_strFraudChecksumResponseMessage ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFraudChecksumResponseMessage ) : '\'' . addslashes( $this->m_strFraudChecksumResponseMessage ) . '\'' ) : 'NULL';
	}

	public function setAvsResponseCode( $strAvsResponseCode ) {
		$this->set( 'm_strAvsResponseCode', CStrings::strTrimDef( $strAvsResponseCode, 10, NULL, true ) );
	}

	public function getAvsResponseCode() {
		return $this->m_strAvsResponseCode;
	}

	public function sqlAvsResponseCode() {
		return ( true == isset( $this->m_strAvsResponseCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAvsResponseCode ) : '\'' . addslashes( $this->m_strAvsResponseCode ) . '\'' ) : 'NULL';
	}

	public function setAvsResponseMessage( $strAvsResponseMessage ) {
		$this->set( 'm_strAvsResponseMessage', CStrings::strTrimDef( $strAvsResponseMessage, 255, NULL, true ) );
	}

	public function getAvsResponseMessage() {
		return $this->m_strAvsResponseMessage;
	}

	public function sqlAvsResponseMessage() {
		return ( true == isset( $this->m_strAvsResponseMessage ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAvsResponseMessage ) : '\'' . addslashes( $this->m_strAvsResponseMessage ) . '\'' ) : 'NULL';
	}

	public function setTokenResponseCode( $strTokenResponseCode ) {
		$this->set( 'm_strTokenResponseCode', CStrings::strTrimDef( $strTokenResponseCode, 10, NULL, true ) );
	}

	public function getTokenResponseCode() {
		return $this->m_strTokenResponseCode;
	}

	public function sqlTokenResponseCode() {
		return ( true == isset( $this->m_strTokenResponseCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTokenResponseCode ) : '\'' . addslashes( $this->m_strTokenResponseCode ) . '\'' ) : 'NULL';
	}

	public function setTokenResponseMessage( $strTokenResponseMessage ) {
		$this->set( 'm_strTokenResponseMessage', CStrings::strTrimDef( $strTokenResponseMessage, 255, NULL, true ) );
	}

	public function getTokenResponseMessage() {
		return $this->m_strTokenResponseMessage;
	}

	public function sqlTokenResponseMessage() {
		return ( true == isset( $this->m_strTokenResponseMessage ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTokenResponseMessage ) : '\'' . addslashes( $this->m_strTokenResponseMessage ) . '\'' ) : 'NULL';
	}

	public function setPinlessDebitNetwork( $strPinlessDebitNetwork ) {
		$this->set( 'm_strPinlessDebitNetwork', CStrings::strTrimDef( $strPinlessDebitNetwork, 255, NULL, true ) );
	}

	public function getPinlessDebitNetwork() {
		return $this->m_strPinlessDebitNetwork;
	}

	public function sqlPinlessDebitNetwork() {
		return ( true == isset( $this->m_strPinlessDebitNetwork ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPinlessDebitNetwork ) : '\'' . addslashes( $this->m_strPinlessDebitNetwork ) . '\'' ) : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setPaymentFileId( $intPaymentFileId ) {
		$this->set( 'm_intPaymentFileId', CStrings::strToIntDef( $intPaymentFileId, NULL, false ) );
	}

	public function getPaymentFileId() {
		return $this->m_intPaymentFileId;
	}

	public function sqlPaymentFileId() {
		return ( true == isset( $this->m_intPaymentFileId ) ) ? ( string ) $this->m_intPaymentFileId : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'reporting_group' => $this->getReportingGroup(),
			'merchant_name' => $this->getMerchantName(),
			'activity_date' => $this->getActivityDate(),
			'settlement_date' => $this->getSettlementDate(),
			'vantiv_payment_id' => $this->getVantivPaymentId(),
			'parent_vantiv_payment_id' => $this->getParentVantivPaymentId(),
			'merchant_order_number' => $this->getMerchantOrderNumber(),
			'txn_type' => $this->getTxnType(),
			'purchase_currency' => $this->getPurchaseCurrency(),
			'purchase_amount' => $this->getPurchaseAmount(),
			'settlement_currency' => $this->getSettlementCurrency(),
			'settlement_amount' => $this->getSettlementAmount(),
			'payment_type' => $this->getPaymentType(),
			'payment_type_id' => $this->getPaymentTypeId(),
			'account_suffix' => $this->getAccountSuffix(),
			'bin' => $this->getBin(),
			'response_reason_message' => $this->getResponseReasonMessage(),
			'batch_id' => $this->getBatchId(),
			'session_id' => $this->getSessionId(),
			'arn' => $this->getArn(),
			'interchange_rate' => $this->getInterchangeRate(),
			'customer_id' => $this->getCustomerId(),
			'merchant_transaction_id' => $this->getMerchantTransactionId(),
			'affiliate' => $this->getAffiliate(),
			'campaign' => $this->getCampaign(),
			'merchant_grouping_id' => $this->getMerchantGroupingId(),
			'token_number' => $this->getTokenNumber(),
			'transaction_processing_timestamp_gmt' => $this->getTransactionProcessingTimestampGmt(),
			'approx_interchange_fee_amount' => $this->getApproxInterchangeFeeAmount(),
			'interchange_flat_rate' => $this->getInterchangeFlatRate(),
			'interchange_percent_rate' => $this->getInterchangePercentRate(),
			'funding_method' => $this->getFundingMethod(),
			'issuing_bank' => $this->getIssuingBank(),
			'billing_descriptor' => $this->getBillingDescriptor(),
			'merchant_id' => $this->getMerchantId(),
			'presenter' => $this->getPresenter(),
			'api_reporting_group' => $this->getApiReportingGroup(),
			'customer_reference' => $this->getCustomerReference(),
			'secondary_amt' => $this->getSecondaryAmt(),
			'secondary_settlement_amt' => $this->getSecondarySettlementAmt(),
			'requested_auth_amount' => $this->getRequestedAuthAmount(),
			'original_auth_code' => $this->getOriginalAuthCode(),
			'customer_name' => $this->getCustomerName(),
			'address_line_1' => $this->getAddressLine1(),
			'address_line_2' => $this->getAddressLine2(),
			'city' => $this->getCity(),
			'state' => $this->getState(),
			'postal_code' => $this->getPostalCode(),
			'fraud_checksum_response_code' => $this->getFraudChecksumResponseCode(),
			'fraud_checksum_response_message' => $this->getFraudChecksumResponseMessage(),
			'avs_response_code' => $this->getAvsResponseCode(),
			'avs_response_message' => $this->getAvsResponseMessage(),
			'token_response_code' => $this->getTokenResponseCode(),
			'token_response_message' => $this->getTokenResponseMessage(),
			'pinless_debit_network' => $this->getPinlessDebitNetwork(),
			'processed_on' => $this->getProcessedOn(),
			'payment_file_id' => $this->getPaymentFileId()
		);
	}

}
?>