<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CReconciliations
 * Do not add any new functions to this class.
 */

class CBaseReconciliations extends CEosPluralBase {

	/**
	 * @return CReconciliation[]
	 */
	public static function fetchReconciliations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReconciliation::class, $objDatabase );
	}

	/**
	 * @return CReconciliation
	 */
	public static function fetchReconciliation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReconciliation::class, $objDatabase );
	}

	public static function fetchReconciliationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reconciliations', $objDatabase );
	}

	public static function fetchReconciliationById( $intId, $objDatabase ) {
		return self::fetchReconciliation( sprintf( 'SELECT * FROM reconciliations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReconciliationsByProcessingBankAccountId( $intProcessingBankAccountId, $objDatabase ) {
		return self::fetchReconciliations( sprintf( 'SELECT * FROM reconciliations WHERE processing_bank_account_id = %d', ( int ) $intProcessingBankAccountId ), $objDatabase );
	}

}
?>