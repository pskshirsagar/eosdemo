<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CApplicationBankAccounts
 * Do not add any new functions to this class.
 */

class CBaseApplicationBankAccounts extends CEosPluralBase {

	/**
	 * @return CApplicationBankAccount[]
	 */
	public static function fetchApplicationBankAccounts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApplicationBankAccount::class, $objDatabase );
	}

	/**
	 * @return CApplicationBankAccount
	 */
	public static function fetchApplicationBankAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplicationBankAccount::class, $objDatabase );
	}

	public static function fetchApplicationBankAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_bank_accounts', $objDatabase );
	}

	public static function fetchApplicationBankAccountById( $intId, $objDatabase ) {
		return self::fetchApplicationBankAccount( sprintf( 'SELECT * FROM application_bank_accounts WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchApplicationBankAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchApplicationBankAccounts( sprintf( 'SELECT * FROM application_bank_accounts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationBankAccountsByMerchantAccountApplicationId( $intMerchantAccountApplicationId, $objDatabase ) {
		return self::fetchApplicationBankAccounts( sprintf( 'SELECT * FROM application_bank_accounts WHERE merchant_account_application_id = %d', ( int ) $intMerchantAccountApplicationId ), $objDatabase );
	}

}
?>