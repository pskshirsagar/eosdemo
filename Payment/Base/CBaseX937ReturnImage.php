<?php

class CBaseX937ReturnImage extends CEosSingularBase {

	const TABLE_NAME = 'public.x937_return_images';

	protected $m_intId;
	protected $m_intX937ReturnFileId;
	protected $m_intX937ReturnItemId;
	protected $m_intPaymentImageTypeId;
	protected $m_strImagePath;
	protected $m_strImageName;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['x937_return_file_id'] ) && $boolDirectSet ) $this->set( 'm_intX937ReturnFileId', trim( $arrValues['x937_return_file_id'] ) ); elseif( isset( $arrValues['x937_return_file_id'] ) ) $this->setX937ReturnFileId( $arrValues['x937_return_file_id'] );
		if( isset( $arrValues['x937_return_item_id'] ) && $boolDirectSet ) $this->set( 'm_intX937ReturnItemId', trim( $arrValues['x937_return_item_id'] ) ); elseif( isset( $arrValues['x937_return_item_id'] ) ) $this->setX937ReturnItemId( $arrValues['x937_return_item_id'] );
		if( isset( $arrValues['payment_image_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentImageTypeId', trim( $arrValues['payment_image_type_id'] ) ); elseif( isset( $arrValues['payment_image_type_id'] ) ) $this->setPaymentImageTypeId( $arrValues['payment_image_type_id'] );
		if( isset( $arrValues['image_path'] ) && $boolDirectSet ) $this->set( 'm_strImagePath', trim( stripcslashes( $arrValues['image_path'] ) ) ); elseif( isset( $arrValues['image_path'] ) ) $this->setImagePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_path'] ) : $arrValues['image_path'] );
		if( isset( $arrValues['image_name'] ) && $boolDirectSet ) $this->set( 'm_strImageName', trim( stripcslashes( $arrValues['image_name'] ) ) ); elseif( isset( $arrValues['image_name'] ) ) $this->setImageName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_name'] ) : $arrValues['image_name'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setX937ReturnFileId( $intX937ReturnFileId ) {
		$this->set( 'm_intX937ReturnFileId', CStrings::strToIntDef( $intX937ReturnFileId, NULL, false ) );
	}

	public function getX937ReturnFileId() {
		return $this->m_intX937ReturnFileId;
	}

	public function sqlX937ReturnFileId() {
		return ( true == isset( $this->m_intX937ReturnFileId ) ) ? ( string ) $this->m_intX937ReturnFileId : 'NULL';
	}

	public function setX937ReturnItemId( $intX937ReturnItemId ) {
		$this->set( 'm_intX937ReturnItemId', CStrings::strToIntDef( $intX937ReturnItemId, NULL, false ) );
	}

	public function getX937ReturnItemId() {
		return $this->m_intX937ReturnItemId;
	}

	public function sqlX937ReturnItemId() {
		return ( true == isset( $this->m_intX937ReturnItemId ) ) ? ( string ) $this->m_intX937ReturnItemId : 'NULL';
	}

	public function setPaymentImageTypeId( $intPaymentImageTypeId ) {
		$this->set( 'm_intPaymentImageTypeId', CStrings::strToIntDef( $intPaymentImageTypeId, NULL, false ) );
	}

	public function getPaymentImageTypeId() {
		return $this->m_intPaymentImageTypeId;
	}

	public function sqlPaymentImageTypeId() {
		return ( true == isset( $this->m_intPaymentImageTypeId ) ) ? ( string ) $this->m_intPaymentImageTypeId : 'NULL';
	}

	public function setImagePath( $strImagePath ) {
		$this->set( 'm_strImagePath', CStrings::strTrimDef( $strImagePath, 4096, NULL, true ) );
	}

	public function getImagePath() {
		return $this->m_strImagePath;
	}

	public function sqlImagePath() {
		return ( true == isset( $this->m_strImagePath ) ) ? '\'' . addslashes( $this->m_strImagePath ) . '\'' : 'NULL';
	}

	public function setImageName( $strImageName ) {
		$this->set( 'm_strImageName', CStrings::strTrimDef( $strImageName, 50, NULL, true ) );
	}

	public function getImageName() {
		return $this->m_strImageName;
	}

	public function sqlImageName() {
		return ( true == isset( $this->m_strImageName ) ) ? '\'' . addslashes( $this->m_strImageName ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, x937_return_file_id, x937_return_item_id, payment_image_type_id, image_path, image_name, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlX937ReturnFileId() . ', ' .
 						$this->sqlX937ReturnItemId() . ', ' .
 						$this->sqlPaymentImageTypeId() . ', ' .
 						$this->sqlImagePath() . ', ' .
 						$this->sqlImageName() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_return_file_id = ' . $this->sqlX937ReturnFileId() . ','; } elseif( true == array_key_exists( 'X937ReturnFileId', $this->getChangedColumns() ) ) { $strSql .= ' x937_return_file_id = ' . $this->sqlX937ReturnFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_return_item_id = ' . $this->sqlX937ReturnItemId() . ','; } elseif( true == array_key_exists( 'X937ReturnItemId', $this->getChangedColumns() ) ) { $strSql .= ' x937_return_item_id = ' . $this->sqlX937ReturnItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_image_type_id = ' . $this->sqlPaymentImageTypeId() . ','; } elseif( true == array_key_exists( 'PaymentImageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_image_type_id = ' . $this->sqlPaymentImageTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_path = ' . $this->sqlImagePath() . ','; } elseif( true == array_key_exists( 'ImagePath', $this->getChangedColumns() ) ) { $strSql .= ' image_path = ' . $this->sqlImagePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_name = ' . $this->sqlImageName() . ','; } elseif( true == array_key_exists( 'ImageName', $this->getChangedColumns() ) ) { $strSql .= ' image_name = ' . $this->sqlImageName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'x937_return_file_id' => $this->getX937ReturnFileId(),
			'x937_return_item_id' => $this->getX937ReturnItemId(),
			'payment_image_type_id' => $this->getPaymentImageTypeId(),
			'image_path' => $this->getImagePath(),
			'image_name' => $this->getImageName(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>