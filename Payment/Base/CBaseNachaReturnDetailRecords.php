<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaReturnDetailRecords
 * Do not add any new functions to this class.
 */

class CBaseNachaReturnDetailRecords extends CEosPluralBase {

	/**
	 * @return CNachaReturnDetailRecord[]
	 */
	public static function fetchNachaReturnDetailRecords( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CNachaReturnDetailRecord::class, $objDatabase );
	}

	/**
	 * @return CNachaReturnDetailRecord
	 */
	public static function fetchNachaReturnDetailRecord( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CNachaReturnDetailRecord::class, $objDatabase );
	}

	public static function fetchNachaReturnDetailRecordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'nacha_return_detail_records', $objDatabase );
	}

	public static function fetchNachaReturnDetailRecordById( $intId, $objDatabase ) {
		return self::fetchNachaReturnDetailRecord( sprintf( 'SELECT * FROM nacha_return_detail_records WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchNachaReturnDetailRecordsByNachaReturnFileId( $intNachaReturnFileId, $objDatabase ) {
		return self::fetchNachaReturnDetailRecords( sprintf( 'SELECT * FROM nacha_return_detail_records WHERE nacha_return_file_id = %d', ( int ) $intNachaReturnFileId ), $objDatabase );
	}

	public static function fetchNachaReturnDetailRecordsByNachaReturnFileBatchId( $intNachaReturnFileBatchId, $objDatabase ) {
		return self::fetchNachaReturnDetailRecords( sprintf( 'SELECT * FROM nacha_return_detail_records WHERE nacha_return_file_batch_id = %d', ( int ) $intNachaReturnFileBatchId ), $objDatabase );
	}

	public static function fetchNachaReturnDetailRecordsByCheckAccountTypeId( $intCheckAccountTypeId, $objDatabase ) {
		return self::fetchNachaReturnDetailRecords( sprintf( 'SELECT * FROM nacha_return_detail_records WHERE check_account_type_id = %d', ( int ) $intCheckAccountTypeId ), $objDatabase );
	}

}
?>