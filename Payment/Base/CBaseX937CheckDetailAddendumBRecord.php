<?php

class CBaseX937CheckDetailAddendumBRecord extends CEosSingularBase {

	const TABLE_NAME = 'public.x937_check_detail_addendum_b_records';

	protected $m_intId;
	protected $m_intX937CheckDetailRecordId;
	protected $m_strRecordType;
	protected $m_strVariableSizeRecordIndicator;
	protected $m_strMicrofilmArchiveSequenceNumber;
	protected $m_strLengthOfImageArchiveLocator;
	protected $m_strImageArchiveLocator;
	protected $m_strDescription;
	protected $m_strUserField;
	protected $m_strReserved;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['x937_check_detail_record_id'] ) && $boolDirectSet ) $this->set( 'm_intX937CheckDetailRecordId', trim( $arrValues['x937_check_detail_record_id'] ) ); elseif( isset( $arrValues['x937_check_detail_record_id'] ) ) $this->setX937CheckDetailRecordId( $arrValues['x937_check_detail_record_id'] );
		if( isset( $arrValues['record_type'] ) && $boolDirectSet ) $this->set( 'm_strRecordType', trim( stripcslashes( $arrValues['record_type'] ) ) ); elseif( isset( $arrValues['record_type'] ) ) $this->setRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_type'] ) : $arrValues['record_type'] );
		if( isset( $arrValues['variable_size_record_indicator'] ) && $boolDirectSet ) $this->set( 'm_strVariableSizeRecordIndicator', trim( stripcslashes( $arrValues['variable_size_record_indicator'] ) ) ); elseif( isset( $arrValues['variable_size_record_indicator'] ) ) $this->setVariableSizeRecordIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['variable_size_record_indicator'] ) : $arrValues['variable_size_record_indicator'] );
		if( isset( $arrValues['microfilm_archive_sequence_number'] ) && $boolDirectSet ) $this->set( 'm_strMicrofilmArchiveSequenceNumber', trim( stripcslashes( $arrValues['microfilm_archive_sequence_number'] ) ) ); elseif( isset( $arrValues['microfilm_archive_sequence_number'] ) ) $this->setMicrofilmArchiveSequenceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['microfilm_archive_sequence_number'] ) : $arrValues['microfilm_archive_sequence_number'] );
		if( isset( $arrValues['length_of_image_archive_locator'] ) && $boolDirectSet ) $this->set( 'm_strLengthOfImageArchiveLocator', trim( stripcslashes( $arrValues['length_of_image_archive_locator'] ) ) ); elseif( isset( $arrValues['length_of_image_archive_locator'] ) ) $this->setLengthOfImageArchiveLocator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['length_of_image_archive_locator'] ) : $arrValues['length_of_image_archive_locator'] );
		if( isset( $arrValues['image_archive_locator'] ) && $boolDirectSet ) $this->set( 'm_strImageArchiveLocator', trim( stripcslashes( $arrValues['image_archive_locator'] ) ) ); elseif( isset( $arrValues['image_archive_locator'] ) ) $this->setImageArchiveLocator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_archive_locator'] ) : $arrValues['image_archive_locator'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['user_field'] ) && $boolDirectSet ) $this->set( 'm_strUserField', trim( stripcslashes( $arrValues['user_field'] ) ) ); elseif( isset( $arrValues['user_field'] ) ) $this->setUserField( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['user_field'] ) : $arrValues['user_field'] );
		if( isset( $arrValues['reserved'] ) && $boolDirectSet ) $this->set( 'm_strReserved', trim( stripcslashes( $arrValues['reserved'] ) ) ); elseif( isset( $arrValues['reserved'] ) ) $this->setReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reserved'] ) : $arrValues['reserved'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setX937CheckDetailRecordId( $intX937CheckDetailRecordId ) {
		$this->set( 'm_intX937CheckDetailRecordId', CStrings::strToIntDef( $intX937CheckDetailRecordId, NULL, false ) );
	}

	public function getX937CheckDetailRecordId() {
		return $this->m_intX937CheckDetailRecordId;
	}

	public function sqlX937CheckDetailRecordId() {
		return ( true == isset( $this->m_intX937CheckDetailRecordId ) ) ? ( string ) $this->m_intX937CheckDetailRecordId : 'NULL';
	}

	public function setRecordType( $strRecordType ) {
		$this->set( 'm_strRecordType', CStrings::strTrimDef( $strRecordType, 2, NULL, true ) );
	}

	public function getRecordType() {
		return $this->m_strRecordType;
	}

	public function sqlRecordType() {
		return ( true == isset( $this->m_strRecordType ) ) ? '\'' . addslashes( $this->m_strRecordType ) . '\'' : 'NULL';
	}

	public function setVariableSizeRecordIndicator( $strVariableSizeRecordIndicator ) {
		$this->set( 'm_strVariableSizeRecordIndicator', CStrings::strTrimDef( $strVariableSizeRecordIndicator, 1, NULL, true ) );
	}

	public function getVariableSizeRecordIndicator() {
		return $this->m_strVariableSizeRecordIndicator;
	}

	public function sqlVariableSizeRecordIndicator() {
		return ( true == isset( $this->m_strVariableSizeRecordIndicator ) ) ? '\'' . addslashes( $this->m_strVariableSizeRecordIndicator ) . '\'' : 'NULL';
	}

	public function setMicrofilmArchiveSequenceNumber( $strMicrofilmArchiveSequenceNumber ) {
		$this->set( 'm_strMicrofilmArchiveSequenceNumber', CStrings::strTrimDef( $strMicrofilmArchiveSequenceNumber, 15, NULL, true ) );
	}

	public function getMicrofilmArchiveSequenceNumber() {
		return $this->m_strMicrofilmArchiveSequenceNumber;
	}

	public function sqlMicrofilmArchiveSequenceNumber() {
		return ( true == isset( $this->m_strMicrofilmArchiveSequenceNumber ) ) ? '\'' . addslashes( $this->m_strMicrofilmArchiveSequenceNumber ) . '\'' : 'NULL';
	}

	public function setLengthOfImageArchiveLocator( $strLengthOfImageArchiveLocator ) {
		$this->set( 'm_strLengthOfImageArchiveLocator', CStrings::strTrimDef( $strLengthOfImageArchiveLocator, 4, NULL, true ) );
	}

	public function getLengthOfImageArchiveLocator() {
		return $this->m_strLengthOfImageArchiveLocator;
	}

	public function sqlLengthOfImageArchiveLocator() {
		return ( true == isset( $this->m_strLengthOfImageArchiveLocator ) ) ? '\'' . addslashes( $this->m_strLengthOfImageArchiveLocator ) . '\'' : 'NULL';
	}

	public function setImageArchiveLocator( $strImageArchiveLocator ) {
		$this->set( 'm_strImageArchiveLocator', CStrings::strTrimDef( $strImageArchiveLocator, -1, NULL, true ) );
	}

	public function getImageArchiveLocator() {
		return $this->m_strImageArchiveLocator;
	}

	public function sqlImageArchiveLocator() {
		return ( true == isset( $this->m_strImageArchiveLocator ) ) ? '\'' . addslashes( $this->m_strImageArchiveLocator ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 15, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setUserField( $strUserField ) {
		$this->set( 'm_strUserField', CStrings::strTrimDef( $strUserField, 4, NULL, true ) );
	}

	public function getUserField() {
		return $this->m_strUserField;
	}

	public function sqlUserField() {
		return ( true == isset( $this->m_strUserField ) ) ? '\'' . addslashes( $this->m_strUserField ) . '\'' : 'NULL';
	}

	public function setReserved( $strReserved ) {
		$this->set( 'm_strReserved', CStrings::strTrimDef( $strReserved, 5, NULL, true ) );
	}

	public function getReserved() {
		return $this->m_strReserved;
	}

	public function sqlReserved() {
		return ( true == isset( $this->m_strReserved ) ) ? '\'' . addslashes( $this->m_strReserved ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, x937_check_detail_record_id, record_type, variable_size_record_indicator, microfilm_archive_sequence_number, length_of_image_archive_locator, image_archive_locator, description, user_field, reserved, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlX937CheckDetailRecordId() . ', ' .
 						$this->sqlRecordType() . ', ' .
 						$this->sqlVariableSizeRecordIndicator() . ', ' .
 						$this->sqlMicrofilmArchiveSequenceNumber() . ', ' .
 						$this->sqlLengthOfImageArchiveLocator() . ', ' .
 						$this->sqlImageArchiveLocator() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlUserField() . ', ' .
 						$this->sqlReserved() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_check_detail_record_id = ' . $this->sqlX937CheckDetailRecordId() . ','; } elseif( true == array_key_exists( 'X937CheckDetailRecordId', $this->getChangedColumns() ) ) { $strSql .= ' x937_check_detail_record_id = ' . $this->sqlX937CheckDetailRecordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; } elseif( true == array_key_exists( 'RecordType', $this->getChangedColumns() ) ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' variable_size_record_indicator = ' . $this->sqlVariableSizeRecordIndicator() . ','; } elseif( true == array_key_exists( 'VariableSizeRecordIndicator', $this->getChangedColumns() ) ) { $strSql .= ' variable_size_record_indicator = ' . $this->sqlVariableSizeRecordIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' microfilm_archive_sequence_number = ' . $this->sqlMicrofilmArchiveSequenceNumber() . ','; } elseif( true == array_key_exists( 'MicrofilmArchiveSequenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' microfilm_archive_sequence_number = ' . $this->sqlMicrofilmArchiveSequenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' length_of_image_archive_locator = ' . $this->sqlLengthOfImageArchiveLocator() . ','; } elseif( true == array_key_exists( 'LengthOfImageArchiveLocator', $this->getChangedColumns() ) ) { $strSql .= ' length_of_image_archive_locator = ' . $this->sqlLengthOfImageArchiveLocator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_archive_locator = ' . $this->sqlImageArchiveLocator() . ','; } elseif( true == array_key_exists( 'ImageArchiveLocator', $this->getChangedColumns() ) ) { $strSql .= ' image_archive_locator = ' . $this->sqlImageArchiveLocator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_field = ' . $this->sqlUserField() . ','; } elseif( true == array_key_exists( 'UserField', $this->getChangedColumns() ) ) { $strSql .= ' user_field = ' . $this->sqlUserField() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reserved = ' . $this->sqlReserved() . ','; } elseif( true == array_key_exists( 'Reserved', $this->getChangedColumns() ) ) { $strSql .= ' reserved = ' . $this->sqlReserved() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'x937_check_detail_record_id' => $this->getX937CheckDetailRecordId(),
			'record_type' => $this->getRecordType(),
			'variable_size_record_indicator' => $this->getVariableSizeRecordIndicator(),
			'microfilm_archive_sequence_number' => $this->getMicrofilmArchiveSequenceNumber(),
			'length_of_image_archive_locator' => $this->getLengthOfImageArchiveLocator(),
			'image_archive_locator' => $this->getImageArchiveLocator(),
			'description' => $this->getDescription(),
			'user_field' => $this->getUserField(),
			'reserved' => $this->getReserved(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>