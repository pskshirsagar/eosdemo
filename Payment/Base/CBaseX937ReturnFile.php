<?php

class CBaseX937ReturnFile extends CEosSingularBase {

	const TABLE_NAME = 'public.x937_return_files';

	protected $m_intId;
	protected $m_intProcessingBankId;
	protected $m_intProcessingBankAccountId;
	protected $m_intX937FileTypeId;
	protected $m_intX937FileStatusTypeId;
	protected $m_intMerchantGatewayId;
	protected $m_intFileNumber;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strX937ReturnFileDatetime;
	protected $m_strReturnedToCheck21AccountOn;
	protected $m_strProcessedOn;
	protected $m_intIsReconPrepped;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsReconPrepped = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['processing_bank_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankId', trim( $arrValues['processing_bank_id'] ) ); elseif( isset( $arrValues['processing_bank_id'] ) ) $this->setProcessingBankId( $arrValues['processing_bank_id'] );
		if( isset( $arrValues['processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankAccountId', trim( $arrValues['processing_bank_account_id'] ) ); elseif( isset( $arrValues['processing_bank_account_id'] ) ) $this->setProcessingBankAccountId( $arrValues['processing_bank_account_id'] );
		if( isset( $arrValues['x937_file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intX937FileTypeId', trim( $arrValues['x937_file_type_id'] ) ); elseif( isset( $arrValues['x937_file_type_id'] ) ) $this->setX937FileTypeId( $arrValues['x937_file_type_id'] );
		if( isset( $arrValues['x937_file_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intX937FileStatusTypeId', trim( $arrValues['x937_file_status_type_id'] ) ); elseif( isset( $arrValues['x937_file_status_type_id'] ) ) $this->setX937FileStatusTypeId( $arrValues['x937_file_status_type_id'] );
		if( isset( $arrValues['merchant_gateway_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantGatewayId', trim( $arrValues['merchant_gateway_id'] ) ); elseif( isset( $arrValues['merchant_gateway_id'] ) ) $this->setMerchantGatewayId( $arrValues['merchant_gateway_id'] );
		if( isset( $arrValues['file_number'] ) && $boolDirectSet ) $this->set( 'm_intFileNumber', trim( $arrValues['file_number'] ) ); elseif( isset( $arrValues['file_number'] ) ) $this->setFileNumber( $arrValues['file_number'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['x937_return_file_datetime'] ) && $boolDirectSet ) $this->set( 'm_strX937ReturnFileDatetime', trim( $arrValues['x937_return_file_datetime'] ) ); elseif( isset( $arrValues['x937_return_file_datetime'] ) ) $this->setX937ReturnFileDatetime( $arrValues['x937_return_file_datetime'] );
		if( isset( $arrValues['returned_to_check21_account_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnedToCheck21AccountOn', trim( $arrValues['returned_to_check21_account_on'] ) ); elseif( isset( $arrValues['returned_to_check21_account_on'] ) ) $this->setReturnedToCheck21AccountOn( $arrValues['returned_to_check21_account_on'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['is_recon_prepped'] ) && $boolDirectSet ) $this->set( 'm_intIsReconPrepped', trim( $arrValues['is_recon_prepped'] ) ); elseif( isset( $arrValues['is_recon_prepped'] ) ) $this->setIsReconPrepped( $arrValues['is_recon_prepped'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setProcessingBankId( $intProcessingBankId ) {
		$this->set( 'm_intProcessingBankId', CStrings::strToIntDef( $intProcessingBankId, NULL, false ) );
	}

	public function getProcessingBankId() {
		return $this->m_intProcessingBankId;
	}

	public function sqlProcessingBankId() {
		return ( true == isset( $this->m_intProcessingBankId ) ) ? ( string ) $this->m_intProcessingBankId : 'NULL';
	}

	public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
		$this->set( 'm_intProcessingBankAccountId', CStrings::strToIntDef( $intProcessingBankAccountId, NULL, false ) );
	}

	public function getProcessingBankAccountId() {
		return $this->m_intProcessingBankAccountId;
	}

	public function sqlProcessingBankAccountId() {
		return ( true == isset( $this->m_intProcessingBankAccountId ) ) ? ( string ) $this->m_intProcessingBankAccountId : 'NULL';
	}

	public function setX937FileTypeId( $intX937FileTypeId ) {
		$this->set( 'm_intX937FileTypeId', CStrings::strToIntDef( $intX937FileTypeId, NULL, false ) );
	}

	public function getX937FileTypeId() {
		return $this->m_intX937FileTypeId;
	}

	public function sqlX937FileTypeId() {
		return ( true == isset( $this->m_intX937FileTypeId ) ) ? ( string ) $this->m_intX937FileTypeId : 'NULL';
	}

	public function setX937FileStatusTypeId( $intX937FileStatusTypeId ) {
		$this->set( 'm_intX937FileStatusTypeId', CStrings::strToIntDef( $intX937FileStatusTypeId, NULL, false ) );
	}

	public function getX937FileStatusTypeId() {
		return $this->m_intX937FileStatusTypeId;
	}

	public function sqlX937FileStatusTypeId() {
		return ( true == isset( $this->m_intX937FileStatusTypeId ) ) ? ( string ) $this->m_intX937FileStatusTypeId : 'NULL';
	}

	public function setMerchantGatewayId( $intMerchantGatewayId ) {
		$this->set( 'm_intMerchantGatewayId', CStrings::strToIntDef( $intMerchantGatewayId, NULL, false ) );
	}

	public function getMerchantGatewayId() {
		return $this->m_intMerchantGatewayId;
	}

	public function sqlMerchantGatewayId() {
		return ( true == isset( $this->m_intMerchantGatewayId ) ) ? ( string ) $this->m_intMerchantGatewayId : 'NULL';
	}

	public function setFileNumber( $intFileNumber ) {
		$this->set( 'm_intFileNumber', CStrings::strToIntDef( $intFileNumber, NULL, false ) );
	}

	public function getFileNumber() {
		return $this->m_intFileNumber;
	}

	public function sqlFileNumber() {
		return ( true == isset( $this->m_intFileNumber ) ) ? ( string ) $this->m_intFileNumber : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setX937ReturnFileDatetime( $strX937ReturnFileDatetime ) {
		$this->set( 'm_strX937ReturnFileDatetime', CStrings::strTrimDef( $strX937ReturnFileDatetime, -1, NULL, true ) );
	}

	public function getX937ReturnFileDatetime() {
		return $this->m_strX937ReturnFileDatetime;
	}

	public function sqlX937ReturnFileDatetime() {
		return ( true == isset( $this->m_strX937ReturnFileDatetime ) ) ? '\'' . $this->m_strX937ReturnFileDatetime . '\'' : 'NOW()';
	}

	public function setReturnedToCheck21AccountOn( $strReturnedToCheck21AccountOn ) {
		$this->set( 'm_strReturnedToCheck21AccountOn', CStrings::strTrimDef( $strReturnedToCheck21AccountOn, -1, NULL, true ) );
	}

	public function getReturnedToCheck21AccountOn() {
		return $this->m_strReturnedToCheck21AccountOn;
	}

	public function sqlReturnedToCheck21AccountOn() {
		return ( true == isset( $this->m_strReturnedToCheck21AccountOn ) ) ? '\'' . $this->m_strReturnedToCheck21AccountOn . '\'' : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setIsReconPrepped( $intIsReconPrepped ) {
		$this->set( 'm_intIsReconPrepped', CStrings::strToIntDef( $intIsReconPrepped, NULL, false ) );
	}

	public function getIsReconPrepped() {
		return $this->m_intIsReconPrepped;
	}

	public function sqlIsReconPrepped() {
		return ( true == isset( $this->m_intIsReconPrepped ) ) ? ( string ) $this->m_intIsReconPrepped : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, processing_bank_id, processing_bank_account_id, x937_file_type_id, x937_file_status_type_id, merchant_gateway_id, file_number, file_name, file_path, x937_return_file_datetime, returned_to_check21_account_on, processed_on, is_recon_prepped, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlProcessingBankId() . ', ' .
 						$this->sqlProcessingBankAccountId() . ', ' .
 						$this->sqlX937FileTypeId() . ', ' .
 						$this->sqlX937FileStatusTypeId() . ', ' .
 						$this->sqlMerchantGatewayId() . ', ' .
 						$this->sqlFileNumber() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlX937ReturnFileDatetime() . ', ' .
 						$this->sqlReturnedToCheck21AccountOn() . ', ' .
 						$this->sqlProcessedOn() . ', ' .
 						$this->sqlIsReconPrepped() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; } elseif( true == array_key_exists( 'ProcessingBankId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; } elseif( true == array_key_exists( 'ProcessingBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_file_type_id = ' . $this->sqlX937FileTypeId() . ','; } elseif( true == array_key_exists( 'X937FileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' x937_file_type_id = ' . $this->sqlX937FileTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_file_status_type_id = ' . $this->sqlX937FileStatusTypeId() . ','; } elseif( true == array_key_exists( 'X937FileStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' x937_file_status_type_id = ' . $this->sqlX937FileStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; } elseif( true == array_key_exists( 'MerchantGatewayId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_number = ' . $this->sqlFileNumber() . ','; } elseif( true == array_key_exists( 'FileNumber', $this->getChangedColumns() ) ) { $strSql .= ' file_number = ' . $this->sqlFileNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_return_file_datetime = ' . $this->sqlX937ReturnFileDatetime() . ','; } elseif( true == array_key_exists( 'X937ReturnFileDatetime', $this->getChangedColumns() ) ) { $strSql .= ' x937_return_file_datetime = ' . $this->sqlX937ReturnFileDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_to_check21_account_on = ' . $this->sqlReturnedToCheck21AccountOn() . ','; } elseif( true == array_key_exists( 'ReturnedToCheck21AccountOn', $this->getChangedColumns() ) ) { $strSql .= ' returned_to_check21_account_on = ' . $this->sqlReturnedToCheck21AccountOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; } elseif( true == array_key_exists( 'IsReconPrepped', $this->getChangedColumns() ) ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'processing_bank_id' => $this->getProcessingBankId(),
			'processing_bank_account_id' => $this->getProcessingBankAccountId(),
			'x937_file_type_id' => $this->getX937FileTypeId(),
			'x937_file_status_type_id' => $this->getX937FileStatusTypeId(),
			'merchant_gateway_id' => $this->getMerchantGatewayId(),
			'file_number' => $this->getFileNumber(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'x937_return_file_datetime' => $this->getX937ReturnFileDatetime(),
			'returned_to_check21_account_on' => $this->getReturnedToCheck21AccountOn(),
			'processed_on' => $this->getProcessedOn(),
			'is_recon_prepped' => $this->getIsReconPrepped(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>