<?php

class CBaseNachaFiles extends CEosPluralBase {

	/**
	 * @return CNachaFile[]
	 */
	public static function fetchNachaFiles( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CNachaFile::class, $objDatabase );
	}

	/**
	 * @return CNachaFile
	 */
	public static function fetchNachaFile( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CNachaFile::class, $objDatabase );
	}

	public static function fetchNachaFileCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'nacha_files', $objDatabase );
	}

	public static function fetchNachaFileById( $intId, $objDatabase ) {
		return self::fetchNachaFile( sprintf( 'SELECT * FROM nacha_files WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchNachaFilesByNachaFileTypeId( $intNachaFileTypeId, $objDatabase ) {
		return self::fetchNachaFiles( sprintf( 'SELECT * FROM nacha_files WHERE nacha_file_type_id = %d', ( int ) $intNachaFileTypeId ), $objDatabase );
	}

	public static function fetchNachaFilesByNachaFileStatusTypeId( $intNachaFileStatusTypeId, $objDatabase ) {
		return self::fetchNachaFiles( sprintf( 'SELECT * FROM nacha_files WHERE nacha_file_status_type_id = %d', ( int ) $intNachaFileStatusTypeId ), $objDatabase );
	}

	public static function fetchNachaFilesByProcessingBankId( $intProcessingBankId, $objDatabase ) {
		return self::fetchNachaFiles( sprintf( 'SELECT * FROM nacha_files WHERE processing_bank_id = %d', ( int ) $intProcessingBankId ), $objDatabase );
	}

	public static function fetchNachaFilesByMerchantGatewayId( $intMerchantGatewayId, $objDatabase ) {
		return self::fetchNachaFiles( sprintf( 'SELECT * FROM nacha_files WHERE merchant_gateway_id = %d', ( int ) $intMerchantGatewayId ), $objDatabase );
	}

}
?>