<?php

class CBaseX937ViewDataRecord extends CEosSingularBase {

	const TABLE_NAME = 'public.x937_view_data_records';

	protected $m_intId;
	protected $m_intX937CheckDetailRecordId;
	protected $m_strRecordType;
	protected $m_strEceInstitutionRoutingNumber;
	protected $m_strBundleBusinessDate;
	protected $m_strCycleNumber;
	protected $m_strEceInstitutionItemSequenceNumber;
	protected $m_strSecurityOriginatorName;
	protected $m_strSecurityAuthenticatorName;
	protected $m_strSecurityKeyName;
	protected $m_strClippingOrigin;
	protected $m_strClippingCoordinateH1;
	protected $m_strClippingCoordinateH2;
	protected $m_strClippingCoordinateV1;
	protected $m_strClippingCoordinateV2;
	protected $m_strLengthOfImageReferenceKey;
	protected $m_strImageReferenceKey;
	protected $m_strLengthOfDigitalSignature;
	protected $m_strDigitalSignature;
	protected $m_strLengthOfImageData;
	protected $m_strImageData;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['x937_check_detail_record_id'] ) && $boolDirectSet ) $this->set( 'm_intX937CheckDetailRecordId', trim( $arrValues['x937_check_detail_record_id'] ) ); elseif( isset( $arrValues['x937_check_detail_record_id'] ) ) $this->setX937CheckDetailRecordId( $arrValues['x937_check_detail_record_id'] );
		if( isset( $arrValues['record_type'] ) && $boolDirectSet ) $this->set( 'm_strRecordType', trim( stripcslashes( $arrValues['record_type'] ) ) ); elseif( isset( $arrValues['record_type'] ) ) $this->setRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_type'] ) : $arrValues['record_type'] );
		if( isset( $arrValues['ece_institution_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strEceInstitutionRoutingNumber', trim( stripcslashes( $arrValues['ece_institution_routing_number'] ) ) ); elseif( isset( $arrValues['ece_institution_routing_number'] ) ) $this->setEceInstitutionRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ece_institution_routing_number'] ) : $arrValues['ece_institution_routing_number'] );
		if( isset( $arrValues['bundle_business_date'] ) && $boolDirectSet ) $this->set( 'm_strBundleBusinessDate', trim( stripcslashes( $arrValues['bundle_business_date'] ) ) ); elseif( isset( $arrValues['bundle_business_date'] ) ) $this->setBundleBusinessDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bundle_business_date'] ) : $arrValues['bundle_business_date'] );
		if( isset( $arrValues['cycle_number'] ) && $boolDirectSet ) $this->set( 'm_strCycleNumber', trim( stripcslashes( $arrValues['cycle_number'] ) ) ); elseif( isset( $arrValues['cycle_number'] ) ) $this->setCycleNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cycle_number'] ) : $arrValues['cycle_number'] );
		if( isset( $arrValues['ece_institution_item_sequence_number'] ) && $boolDirectSet ) $this->set( 'm_strEceInstitutionItemSequenceNumber', trim( stripcslashes( $arrValues['ece_institution_item_sequence_number'] ) ) ); elseif( isset( $arrValues['ece_institution_item_sequence_number'] ) ) $this->setEceInstitutionItemSequenceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ece_institution_item_sequence_number'] ) : $arrValues['ece_institution_item_sequence_number'] );
		if( isset( $arrValues['security_originator_name'] ) && $boolDirectSet ) $this->set( 'm_strSecurityOriginatorName', trim( stripcslashes( $arrValues['security_originator_name'] ) ) ); elseif( isset( $arrValues['security_originator_name'] ) ) $this->setSecurityOriginatorName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['security_originator_name'] ) : $arrValues['security_originator_name'] );
		if( isset( $arrValues['security_authenticator_name'] ) && $boolDirectSet ) $this->set( 'm_strSecurityAuthenticatorName', trim( stripcslashes( $arrValues['security_authenticator_name'] ) ) ); elseif( isset( $arrValues['security_authenticator_name'] ) ) $this->setSecurityAuthenticatorName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['security_authenticator_name'] ) : $arrValues['security_authenticator_name'] );
		if( isset( $arrValues['security_key_name'] ) && $boolDirectSet ) $this->set( 'm_strSecurityKeyName', trim( stripcslashes( $arrValues['security_key_name'] ) ) ); elseif( isset( $arrValues['security_key_name'] ) ) $this->setSecurityKeyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['security_key_name'] ) : $arrValues['security_key_name'] );
		if( isset( $arrValues['clipping_origin'] ) && $boolDirectSet ) $this->set( 'm_strClippingOrigin', trim( stripcslashes( $arrValues['clipping_origin'] ) ) ); elseif( isset( $arrValues['clipping_origin'] ) ) $this->setClippingOrigin( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['clipping_origin'] ) : $arrValues['clipping_origin'] );
		if( isset( $arrValues['clipping_coordinate_h1'] ) && $boolDirectSet ) $this->set( 'm_strClippingCoordinateH1', trim( stripcslashes( $arrValues['clipping_coordinate_h1'] ) ) ); elseif( isset( $arrValues['clipping_coordinate_h1'] ) ) $this->setClippingCoordinateH1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['clipping_coordinate_h1'] ) : $arrValues['clipping_coordinate_h1'] );
		if( isset( $arrValues['clipping_coordinate_h2'] ) && $boolDirectSet ) $this->set( 'm_strClippingCoordinateH2', trim( stripcslashes( $arrValues['clipping_coordinate_h2'] ) ) ); elseif( isset( $arrValues['clipping_coordinate_h2'] ) ) $this->setClippingCoordinateH2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['clipping_coordinate_h2'] ) : $arrValues['clipping_coordinate_h2'] );
		if( isset( $arrValues['clipping_coordinate_v1'] ) && $boolDirectSet ) $this->set( 'm_strClippingCoordinateV1', trim( stripcslashes( $arrValues['clipping_coordinate_v1'] ) ) ); elseif( isset( $arrValues['clipping_coordinate_v1'] ) ) $this->setClippingCoordinateV1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['clipping_coordinate_v1'] ) : $arrValues['clipping_coordinate_v1'] );
		if( isset( $arrValues['clipping_coordinate_v2'] ) && $boolDirectSet ) $this->set( 'm_strClippingCoordinateV2', trim( stripcslashes( $arrValues['clipping_coordinate_v2'] ) ) ); elseif( isset( $arrValues['clipping_coordinate_v2'] ) ) $this->setClippingCoordinateV2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['clipping_coordinate_v2'] ) : $arrValues['clipping_coordinate_v2'] );
		if( isset( $arrValues['length_of_image_reference_key'] ) && $boolDirectSet ) $this->set( 'm_strLengthOfImageReferenceKey', trim( stripcslashes( $arrValues['length_of_image_reference_key'] ) ) ); elseif( isset( $arrValues['length_of_image_reference_key'] ) ) $this->setLengthOfImageReferenceKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['length_of_image_reference_key'] ) : $arrValues['length_of_image_reference_key'] );
		if( isset( $arrValues['image_reference_key'] ) && $boolDirectSet ) $this->set( 'm_strImageReferenceKey', trim( stripcslashes( $arrValues['image_reference_key'] ) ) ); elseif( isset( $arrValues['image_reference_key'] ) ) $this->setImageReferenceKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_reference_key'] ) : $arrValues['image_reference_key'] );
		if( isset( $arrValues['length_of_digital_signature'] ) && $boolDirectSet ) $this->set( 'm_strLengthOfDigitalSignature', trim( stripcslashes( $arrValues['length_of_digital_signature'] ) ) ); elseif( isset( $arrValues['length_of_digital_signature'] ) ) $this->setLengthOfDigitalSignature( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['length_of_digital_signature'] ) : $arrValues['length_of_digital_signature'] );
		if( isset( $arrValues['digital_signature'] ) && $boolDirectSet ) $this->set( 'm_strDigitalSignature', trim( stripcslashes( $arrValues['digital_signature'] ) ) ); elseif( isset( $arrValues['digital_signature'] ) ) $this->setDigitalSignature( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['digital_signature'] ) : $arrValues['digital_signature'] );
		if( isset( $arrValues['length_of_image_data'] ) && $boolDirectSet ) $this->set( 'm_strLengthOfImageData', trim( stripcslashes( $arrValues['length_of_image_data'] ) ) ); elseif( isset( $arrValues['length_of_image_data'] ) ) $this->setLengthOfImageData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['length_of_image_data'] ) : $arrValues['length_of_image_data'] );
		if( isset( $arrValues['image_data'] ) && $boolDirectSet ) $this->set( 'm_strImageData', trim( stripcslashes( $arrValues['image_data'] ) ) ); elseif( isset( $arrValues['image_data'] ) ) $this->setImageData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_data'] ) : $arrValues['image_data'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setX937CheckDetailRecordId( $intX937CheckDetailRecordId ) {
		$this->set( 'm_intX937CheckDetailRecordId', CStrings::strToIntDef( $intX937CheckDetailRecordId, NULL, false ) );
	}

	public function getX937CheckDetailRecordId() {
		return $this->m_intX937CheckDetailRecordId;
	}

	public function sqlX937CheckDetailRecordId() {
		return ( true == isset( $this->m_intX937CheckDetailRecordId ) ) ? ( string ) $this->m_intX937CheckDetailRecordId : 'NULL';
	}

	public function setRecordType( $strRecordType ) {
		$this->set( 'm_strRecordType', CStrings::strTrimDef( $strRecordType, 2, NULL, true ) );
	}

	public function getRecordType() {
		return $this->m_strRecordType;
	}

	public function sqlRecordType() {
		return ( true == isset( $this->m_strRecordType ) ) ? '\'' . addslashes( $this->m_strRecordType ) . '\'' : 'NULL';
	}

	public function setEceInstitutionRoutingNumber( $strEceInstitutionRoutingNumber ) {
		$this->set( 'm_strEceInstitutionRoutingNumber', CStrings::strTrimDef( $strEceInstitutionRoutingNumber, 9, NULL, true ) );
	}

	public function getEceInstitutionRoutingNumber() {
		return $this->m_strEceInstitutionRoutingNumber;
	}

	public function sqlEceInstitutionRoutingNumber() {
		return ( true == isset( $this->m_strEceInstitutionRoutingNumber ) ) ? '\'' . addslashes( $this->m_strEceInstitutionRoutingNumber ) . '\'' : 'NULL';
	}

	public function setBundleBusinessDate( $strBundleBusinessDate ) {
		$this->set( 'm_strBundleBusinessDate', CStrings::strTrimDef( $strBundleBusinessDate, 8, NULL, true ) );
	}

	public function getBundleBusinessDate() {
		return $this->m_strBundleBusinessDate;
	}

	public function sqlBundleBusinessDate() {
		return ( true == isset( $this->m_strBundleBusinessDate ) ) ? '\'' . addslashes( $this->m_strBundleBusinessDate ) . '\'' : 'NULL';
	}

	public function setCycleNumber( $strCycleNumber ) {
		$this->set( 'm_strCycleNumber', CStrings::strTrimDef( $strCycleNumber, 2, NULL, true ) );
	}

	public function getCycleNumber() {
		return $this->m_strCycleNumber;
	}

	public function sqlCycleNumber() {
		return ( true == isset( $this->m_strCycleNumber ) ) ? '\'' . addslashes( $this->m_strCycleNumber ) . '\'' : 'NULL';
	}

	public function setEceInstitutionItemSequenceNumber( $strEceInstitutionItemSequenceNumber ) {
		$this->set( 'm_strEceInstitutionItemSequenceNumber', CStrings::strTrimDef( $strEceInstitutionItemSequenceNumber, 15, NULL, true ) );
	}

	public function getEceInstitutionItemSequenceNumber() {
		return $this->m_strEceInstitutionItemSequenceNumber;
	}

	public function sqlEceInstitutionItemSequenceNumber() {
		return ( true == isset( $this->m_strEceInstitutionItemSequenceNumber ) ) ? '\'' . addslashes( $this->m_strEceInstitutionItemSequenceNumber ) . '\'' : 'NULL';
	}

	public function setSecurityOriginatorName( $strSecurityOriginatorName ) {
		$this->set( 'm_strSecurityOriginatorName', CStrings::strTrimDef( $strSecurityOriginatorName, 16, NULL, true ) );
	}

	public function getSecurityOriginatorName() {
		return $this->m_strSecurityOriginatorName;
	}

	public function sqlSecurityOriginatorName() {
		return ( true == isset( $this->m_strSecurityOriginatorName ) ) ? '\'' . addslashes( $this->m_strSecurityOriginatorName ) . '\'' : 'NULL';
	}

	public function setSecurityAuthenticatorName( $strSecurityAuthenticatorName ) {
		$this->set( 'm_strSecurityAuthenticatorName', CStrings::strTrimDef( $strSecurityAuthenticatorName, 16, NULL, true ) );
	}

	public function getSecurityAuthenticatorName() {
		return $this->m_strSecurityAuthenticatorName;
	}

	public function sqlSecurityAuthenticatorName() {
		return ( true == isset( $this->m_strSecurityAuthenticatorName ) ) ? '\'' . addslashes( $this->m_strSecurityAuthenticatorName ) . '\'' : 'NULL';
	}

	public function setSecurityKeyName( $strSecurityKeyName ) {
		$this->set( 'm_strSecurityKeyName', CStrings::strTrimDef( $strSecurityKeyName, 16, NULL, true ) );
	}

	public function getSecurityKeyName() {
		return $this->m_strSecurityKeyName;
	}

	public function sqlSecurityKeyName() {
		return ( true == isset( $this->m_strSecurityKeyName ) ) ? '\'' . addslashes( $this->m_strSecurityKeyName ) . '\'' : 'NULL';
	}

	public function setClippingOrigin( $strClippingOrigin ) {
		$this->set( 'm_strClippingOrigin', CStrings::strTrimDef( $strClippingOrigin, 1, NULL, true ) );
	}

	public function getClippingOrigin() {
		return $this->m_strClippingOrigin;
	}

	public function sqlClippingOrigin() {
		return ( true == isset( $this->m_strClippingOrigin ) ) ? '\'' . addslashes( $this->m_strClippingOrigin ) . '\'' : 'NULL';
	}

	public function setClippingCoordinateH1( $strClippingCoordinateH1 ) {
		$this->set( 'm_strClippingCoordinateH1', CStrings::strTrimDef( $strClippingCoordinateH1, 4, NULL, true ) );
	}

	public function getClippingCoordinateH1() {
		return $this->m_strClippingCoordinateH1;
	}

	public function sqlClippingCoordinateH1() {
		return ( true == isset( $this->m_strClippingCoordinateH1 ) ) ? '\'' . addslashes( $this->m_strClippingCoordinateH1 ) . '\'' : 'NULL';
	}

	public function setClippingCoordinateH2( $strClippingCoordinateH2 ) {
		$this->set( 'm_strClippingCoordinateH2', CStrings::strTrimDef( $strClippingCoordinateH2, 4, NULL, true ) );
	}

	public function getClippingCoordinateH2() {
		return $this->m_strClippingCoordinateH2;
	}

	public function sqlClippingCoordinateH2() {
		return ( true == isset( $this->m_strClippingCoordinateH2 ) ) ? '\'' . addslashes( $this->m_strClippingCoordinateH2 ) . '\'' : 'NULL';
	}

	public function setClippingCoordinateV1( $strClippingCoordinateV1 ) {
		$this->set( 'm_strClippingCoordinateV1', CStrings::strTrimDef( $strClippingCoordinateV1, 4, NULL, true ) );
	}

	public function getClippingCoordinateV1() {
		return $this->m_strClippingCoordinateV1;
	}

	public function sqlClippingCoordinateV1() {
		return ( true == isset( $this->m_strClippingCoordinateV1 ) ) ? '\'' . addslashes( $this->m_strClippingCoordinateV1 ) . '\'' : 'NULL';
	}

	public function setClippingCoordinateV2( $strClippingCoordinateV2 ) {
		$this->set( 'm_strClippingCoordinateV2', CStrings::strTrimDef( $strClippingCoordinateV2, 4, NULL, true ) );
	}

	public function getClippingCoordinateV2() {
		return $this->m_strClippingCoordinateV2;
	}

	public function sqlClippingCoordinateV2() {
		return ( true == isset( $this->m_strClippingCoordinateV2 ) ) ? '\'' . addslashes( $this->m_strClippingCoordinateV2 ) . '\'' : 'NULL';
	}

	public function setLengthOfImageReferenceKey( $strLengthOfImageReferenceKey ) {
		$this->set( 'm_strLengthOfImageReferenceKey', CStrings::strTrimDef( $strLengthOfImageReferenceKey, 4, NULL, true ) );
	}

	public function getLengthOfImageReferenceKey() {
		return $this->m_strLengthOfImageReferenceKey;
	}

	public function sqlLengthOfImageReferenceKey() {
		return ( true == isset( $this->m_strLengthOfImageReferenceKey ) ) ? '\'' . addslashes( $this->m_strLengthOfImageReferenceKey ) . '\'' : 'NULL';
	}

	public function setImageReferenceKey( $strImageReferenceKey ) {
		$this->set( 'm_strImageReferenceKey', CStrings::strTrimDef( $strImageReferenceKey, -1, NULL, true ) );
	}

	public function getImageReferenceKey() {
		return $this->m_strImageReferenceKey;
	}

	public function sqlImageReferenceKey() {
		return ( true == isset( $this->m_strImageReferenceKey ) ) ? '\'' . addslashes( $this->m_strImageReferenceKey ) . '\'' : 'NULL';
	}

	public function setLengthOfDigitalSignature( $strLengthOfDigitalSignature ) {
		$this->set( 'm_strLengthOfDigitalSignature', CStrings::strTrimDef( $strLengthOfDigitalSignature, 5, NULL, true ) );
	}

	public function getLengthOfDigitalSignature() {
		return $this->m_strLengthOfDigitalSignature;
	}

	public function sqlLengthOfDigitalSignature() {
		return ( true == isset( $this->m_strLengthOfDigitalSignature ) ) ? '\'' . addslashes( $this->m_strLengthOfDigitalSignature ) . '\'' : 'NULL';
	}

	public function setDigitalSignature( $strDigitalSignature ) {
		$this->set( 'm_strDigitalSignature', CStrings::strTrimDef( $strDigitalSignature, -1, NULL, true ) );
	}

	public function getDigitalSignature() {
		return $this->m_strDigitalSignature;
	}

	public function sqlDigitalSignature() {
		return ( true == isset( $this->m_strDigitalSignature ) ) ? '\'' . addslashes( $this->m_strDigitalSignature ) . '\'' : 'NULL';
	}

	public function setLengthOfImageData( $strLengthOfImageData ) {
		$this->set( 'm_strLengthOfImageData', CStrings::strTrimDef( $strLengthOfImageData, 7, NULL, true ) );
	}

	public function getLengthOfImageData() {
		return $this->m_strLengthOfImageData;
	}

	public function sqlLengthOfImageData() {
		return ( true == isset( $this->m_strLengthOfImageData ) ) ? '\'' . addslashes( $this->m_strLengthOfImageData ) . '\'' : 'NULL';
	}

	public function setImageData( $strImageData ) {
		$this->set( 'm_strImageData', CStrings::strTrimDef( $strImageData, -1, NULL, true ) );
	}

	public function getImageData() {
		return $this->m_strImageData;
	}

	public function sqlImageData() {
		return ( true == isset( $this->m_strImageData ) ) ? '\'' . addslashes( $this->m_strImageData ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, x937_check_detail_record_id, record_type, ece_institution_routing_number, bundle_business_date, cycle_number, ece_institution_item_sequence_number, security_originator_name, security_authenticator_name, security_key_name, clipping_origin, clipping_coordinate_h1, clipping_coordinate_h2, clipping_coordinate_v1, clipping_coordinate_v2, length_of_image_reference_key, image_reference_key, length_of_digital_signature, digital_signature, length_of_image_data, image_data, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlX937CheckDetailRecordId() . ', ' .
 						$this->sqlRecordType() . ', ' .
 						$this->sqlEceInstitutionRoutingNumber() . ', ' .
 						$this->sqlBundleBusinessDate() . ', ' .
 						$this->sqlCycleNumber() . ', ' .
 						$this->sqlEceInstitutionItemSequenceNumber() . ', ' .
 						$this->sqlSecurityOriginatorName() . ', ' .
 						$this->sqlSecurityAuthenticatorName() . ', ' .
 						$this->sqlSecurityKeyName() . ', ' .
 						$this->sqlClippingOrigin() . ', ' .
 						$this->sqlClippingCoordinateH1() . ', ' .
 						$this->sqlClippingCoordinateH2() . ', ' .
 						$this->sqlClippingCoordinateV1() . ', ' .
 						$this->sqlClippingCoordinateV2() . ', ' .
 						$this->sqlLengthOfImageReferenceKey() . ', ' .
 						$this->sqlImageReferenceKey() . ', ' .
 						$this->sqlLengthOfDigitalSignature() . ', ' .
 						$this->sqlDigitalSignature() . ', ' .
 						$this->sqlLengthOfImageData() . ', ' .
 						$this->sqlImageData() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_check_detail_record_id = ' . $this->sqlX937CheckDetailRecordId() . ','; } elseif( true == array_key_exists( 'X937CheckDetailRecordId', $this->getChangedColumns() ) ) { $strSql .= ' x937_check_detail_record_id = ' . $this->sqlX937CheckDetailRecordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; } elseif( true == array_key_exists( 'RecordType', $this->getChangedColumns() ) ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ece_institution_routing_number = ' . $this->sqlEceInstitutionRoutingNumber() . ','; } elseif( true == array_key_exists( 'EceInstitutionRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' ece_institution_routing_number = ' . $this->sqlEceInstitutionRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bundle_business_date = ' . $this->sqlBundleBusinessDate() . ','; } elseif( true == array_key_exists( 'BundleBusinessDate', $this->getChangedColumns() ) ) { $strSql .= ' bundle_business_date = ' . $this->sqlBundleBusinessDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cycle_number = ' . $this->sqlCycleNumber() . ','; } elseif( true == array_key_exists( 'CycleNumber', $this->getChangedColumns() ) ) { $strSql .= ' cycle_number = ' . $this->sqlCycleNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ece_institution_item_sequence_number = ' . $this->sqlEceInstitutionItemSequenceNumber() . ','; } elseif( true == array_key_exists( 'EceInstitutionItemSequenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' ece_institution_item_sequence_number = ' . $this->sqlEceInstitutionItemSequenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' security_originator_name = ' . $this->sqlSecurityOriginatorName() . ','; } elseif( true == array_key_exists( 'SecurityOriginatorName', $this->getChangedColumns() ) ) { $strSql .= ' security_originator_name = ' . $this->sqlSecurityOriginatorName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' security_authenticator_name = ' . $this->sqlSecurityAuthenticatorName() . ','; } elseif( true == array_key_exists( 'SecurityAuthenticatorName', $this->getChangedColumns() ) ) { $strSql .= ' security_authenticator_name = ' . $this->sqlSecurityAuthenticatorName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' security_key_name = ' . $this->sqlSecurityKeyName() . ','; } elseif( true == array_key_exists( 'SecurityKeyName', $this->getChangedColumns() ) ) { $strSql .= ' security_key_name = ' . $this->sqlSecurityKeyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clipping_origin = ' . $this->sqlClippingOrigin() . ','; } elseif( true == array_key_exists( 'ClippingOrigin', $this->getChangedColumns() ) ) { $strSql .= ' clipping_origin = ' . $this->sqlClippingOrigin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clipping_coordinate_h1 = ' . $this->sqlClippingCoordinateH1() . ','; } elseif( true == array_key_exists( 'ClippingCoordinateH1', $this->getChangedColumns() ) ) { $strSql .= ' clipping_coordinate_h1 = ' . $this->sqlClippingCoordinateH1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clipping_coordinate_h2 = ' . $this->sqlClippingCoordinateH2() . ','; } elseif( true == array_key_exists( 'ClippingCoordinateH2', $this->getChangedColumns() ) ) { $strSql .= ' clipping_coordinate_h2 = ' . $this->sqlClippingCoordinateH2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clipping_coordinate_v1 = ' . $this->sqlClippingCoordinateV1() . ','; } elseif( true == array_key_exists( 'ClippingCoordinateV1', $this->getChangedColumns() ) ) { $strSql .= ' clipping_coordinate_v1 = ' . $this->sqlClippingCoordinateV1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clipping_coordinate_v2 = ' . $this->sqlClippingCoordinateV2() . ','; } elseif( true == array_key_exists( 'ClippingCoordinateV2', $this->getChangedColumns() ) ) { $strSql .= ' clipping_coordinate_v2 = ' . $this->sqlClippingCoordinateV2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' length_of_image_reference_key = ' . $this->sqlLengthOfImageReferenceKey() . ','; } elseif( true == array_key_exists( 'LengthOfImageReferenceKey', $this->getChangedColumns() ) ) { $strSql .= ' length_of_image_reference_key = ' . $this->sqlLengthOfImageReferenceKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_reference_key = ' . $this->sqlImageReferenceKey() . ','; } elseif( true == array_key_exists( 'ImageReferenceKey', $this->getChangedColumns() ) ) { $strSql .= ' image_reference_key = ' . $this->sqlImageReferenceKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' length_of_digital_signature = ' . $this->sqlLengthOfDigitalSignature() . ','; } elseif( true == array_key_exists( 'LengthOfDigitalSignature', $this->getChangedColumns() ) ) { $strSql .= ' length_of_digital_signature = ' . $this->sqlLengthOfDigitalSignature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' digital_signature = ' . $this->sqlDigitalSignature() . ','; } elseif( true == array_key_exists( 'DigitalSignature', $this->getChangedColumns() ) ) { $strSql .= ' digital_signature = ' . $this->sqlDigitalSignature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' length_of_image_data = ' . $this->sqlLengthOfImageData() . ','; } elseif( true == array_key_exists( 'LengthOfImageData', $this->getChangedColumns() ) ) { $strSql .= ' length_of_image_data = ' . $this->sqlLengthOfImageData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_data = ' . $this->sqlImageData() . ','; } elseif( true == array_key_exists( 'ImageData', $this->getChangedColumns() ) ) { $strSql .= ' image_data = ' . $this->sqlImageData() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'x937_check_detail_record_id' => $this->getX937CheckDetailRecordId(),
			'record_type' => $this->getRecordType(),
			'ece_institution_routing_number' => $this->getEceInstitutionRoutingNumber(),
			'bundle_business_date' => $this->getBundleBusinessDate(),
			'cycle_number' => $this->getCycleNumber(),
			'ece_institution_item_sequence_number' => $this->getEceInstitutionItemSequenceNumber(),
			'security_originator_name' => $this->getSecurityOriginatorName(),
			'security_authenticator_name' => $this->getSecurityAuthenticatorName(),
			'security_key_name' => $this->getSecurityKeyName(),
			'clipping_origin' => $this->getClippingOrigin(),
			'clipping_coordinate_h1' => $this->getClippingCoordinateH1(),
			'clipping_coordinate_h2' => $this->getClippingCoordinateH2(),
			'clipping_coordinate_v1' => $this->getClippingCoordinateV1(),
			'clipping_coordinate_v2' => $this->getClippingCoordinateV2(),
			'length_of_image_reference_key' => $this->getLengthOfImageReferenceKey(),
			'image_reference_key' => $this->getImageReferenceKey(),
			'length_of_digital_signature' => $this->getLengthOfDigitalSignature(),
			'digital_signature' => $this->getDigitalSignature(),
			'length_of_image_data' => $this->getLengthOfImageData(),
			'image_data' => $this->getImageData(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>