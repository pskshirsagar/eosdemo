<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMerchantAccountMethods
 * Do not add any new functions to this class.
 */

class CBaseMerchantAccountMethods extends CEosPluralBase {

	/**
	 * @return CMerchantAccountMethod[]
	 */
	public static function fetchMerchantAccountMethods( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMerchantAccountMethod::class, $objDatabase );
	}

	/**
	 * @return CMerchantAccountMethod
	 */
	public static function fetchMerchantAccountMethod( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMerchantAccountMethod::class, $objDatabase );
	}

	public static function fetchMerchantAccountMethodCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'merchant_account_methods', $objDatabase );
	}

	public static function fetchMerchantAccountMethodById( $intId, $objDatabase ) {
		return self::fetchMerchantAccountMethod( sprintf( 'SELECT * FROM merchant_account_methods WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMerchantAccountMethodsByCid( $intCid, $objDatabase ) {
		return self::fetchMerchantAccountMethods( sprintf( 'SELECT * FROM merchant_account_methods WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountMethodsByCompanyMerchantAccountId( $intCompanyMerchantAccountId, $objDatabase ) {
		return self::fetchMerchantAccountMethods( sprintf( 'SELECT * FROM merchant_account_methods WHERE company_merchant_account_id = %d', ( int ) $intCompanyMerchantAccountId ), $objDatabase );
	}

	public static function fetchMerchantAccountMethodsByProcessingBankAccountId( $intProcessingBankAccountId, $objDatabase ) {
		return self::fetchMerchantAccountMethods( sprintf( 'SELECT * FROM merchant_account_methods WHERE processing_bank_account_id = %d', ( int ) $intProcessingBankAccountId ), $objDatabase );
	}

	public static function fetchMerchantAccountMethodsByMerchantGatewayId( $intMerchantGatewayId, $objDatabase ) {
		return self::fetchMerchantAccountMethods( sprintf( 'SELECT * FROM merchant_account_methods WHERE merchant_gateway_id = %d', ( int ) $intMerchantGatewayId ), $objDatabase );
	}

	public static function fetchMerchantAccountMethodsByPaymentTypeId( $intPaymentTypeId, $objDatabase ) {
		return self::fetchMerchantAccountMethods( sprintf( 'SELECT * FROM merchant_account_methods WHERE payment_type_id = %d', ( int ) $intPaymentTypeId ), $objDatabase );
	}

	public static function fetchMerchantAccountMethodsByPaymentMediumId( $intPaymentMediumId, $objDatabase ) {
		return self::fetchMerchantAccountMethods( sprintf( 'SELECT * FROM merchant_account_methods WHERE payment_medium_id = %d', ( int ) $intPaymentMediumId ), $objDatabase );
	}

}
?>