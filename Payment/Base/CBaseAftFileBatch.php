<?php

class CBaseAftFileBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.aft_file_batches';

	protected $m_intId;
	protected $m_intAftFileId;
	protected $m_strBatchPaymentTypeCode;
	protected $m_strTransactionTypeCode;
	protected $m_strPayableDate;
	protected $m_strOriginatorShortName;
	protected $m_strOriginatorLongName;
	protected $m_strReturnsInstitutionId;
	protected $m_strReturnsAccountNumberEncrypted;
	protected $m_strBatchRecordCount;
	protected $m_strBatchAmount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsReconPrepped;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['aft_file_id'] ) && $boolDirectSet ) $this->set( 'm_intAftFileId', trim( $arrValues['aft_file_id'] ) ); elseif( isset( $arrValues['aft_file_id'] ) ) $this->setAftFileId( $arrValues['aft_file_id'] );
		if( isset( $arrValues['batch_payment_type_code'] ) && $boolDirectSet ) $this->set( 'm_strBatchPaymentTypeCode', trim( $arrValues['batch_payment_type_code'] ) ); elseif( isset( $arrValues['batch_payment_type_code'] ) ) $this->setBatchPaymentTypeCode( $arrValues['batch_payment_type_code'] );
		if( isset( $arrValues['transaction_type_code'] ) && $boolDirectSet ) $this->set( 'm_strTransactionTypeCode', trim( $arrValues['transaction_type_code'] ) ); elseif( isset( $arrValues['transaction_type_code'] ) ) $this->setTransactionTypeCode( $arrValues['transaction_type_code'] );
		if( isset( $arrValues['payable_date'] ) && $boolDirectSet ) $this->set( 'm_strPayableDate', trim( $arrValues['payable_date'] ) ); elseif( isset( $arrValues['payable_date'] ) ) $this->setPayableDate( $arrValues['payable_date'] );
		if( isset( $arrValues['originator_short_name'] ) && $boolDirectSet ) $this->set( 'm_strOriginatorShortName', trim( $arrValues['originator_short_name'] ) ); elseif( isset( $arrValues['originator_short_name'] ) ) $this->setOriginatorShortName( $arrValues['originator_short_name'] );
		if( isset( $arrValues['originator_long_name'] ) && $boolDirectSet ) $this->set( 'm_strOriginatorLongName', trim( $arrValues['originator_long_name'] ) ); elseif( isset( $arrValues['originator_long_name'] ) ) $this->setOriginatorLongName( $arrValues['originator_long_name'] );
		if( isset( $arrValues['returns_institution_id'] ) && $boolDirectSet ) $this->set( 'm_strReturnsInstitutionId', trim( $arrValues['returns_institution_id'] ) ); elseif( isset( $arrValues['returns_institution_id'] ) ) $this->setReturnsInstitutionId( $arrValues['returns_institution_id'] );
		if( isset( $arrValues['returns_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strReturnsAccountNumberEncrypted', trim( $arrValues['returns_account_number_encrypted'] ) ); elseif( isset( $arrValues['returns_account_number_encrypted'] ) ) $this->setReturnsAccountNumberEncrypted( $arrValues['returns_account_number_encrypted'] );
		if( isset( $arrValues['batch_record_count'] ) && $boolDirectSet ) $this->set( 'm_strBatchRecordCount', trim( $arrValues['batch_record_count'] ) ); elseif( isset( $arrValues['batch_record_count'] ) ) $this->setBatchRecordCount( $arrValues['batch_record_count'] );
		if( isset( $arrValues['batch_amount'] ) && $boolDirectSet ) $this->set( 'm_strBatchAmount', trim( $arrValues['batch_amount'] ) ); elseif( isset( $arrValues['batch_amount'] ) ) $this->setBatchAmount( $arrValues['batch_amount'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_recon_prepped'] ) && $boolDirectSet ) $this->set( 'm_boolIsReconPrepped', trim( stripcslashes( $arrValues['is_recon_prepped'] ) ) ); elseif( isset( $arrValues['is_recon_prepped'] ) ) $this->setIsReconPrepped( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_recon_prepped'] ) : $arrValues['is_recon_prepped'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAftFileId( $intAftFileId ) {
		$this->set( 'm_intAftFileId', CStrings::strToIntDef( $intAftFileId, NULL, false ) );
	}

	public function getAftFileId() {
		return $this->m_intAftFileId;
	}

	public function sqlAftFileId() {
		return ( true == isset( $this->m_intAftFileId ) ) ? ( string ) $this->m_intAftFileId : 'NULL';
	}

	public function setBatchPaymentTypeCode( $strBatchPaymentTypeCode ) {
		$this->set( 'm_strBatchPaymentTypeCode', CStrings::strTrimDef( $strBatchPaymentTypeCode, 1, NULL, true ) );
	}

	public function getBatchPaymentTypeCode() {
		return $this->m_strBatchPaymentTypeCode;
	}

	public function sqlBatchPaymentTypeCode() {
		return ( true == isset( $this->m_strBatchPaymentTypeCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBatchPaymentTypeCode ) : '\'' . addslashes( $this->m_strBatchPaymentTypeCode ) . '\'' ) : 'NULL';
	}

	public function setTransactionTypeCode( $strTransactionTypeCode ) {
		$this->set( 'm_strTransactionTypeCode', CStrings::strTrimDef( $strTransactionTypeCode, 3, NULL, true ) );
	}

	public function getTransactionTypeCode() {
		return $this->m_strTransactionTypeCode;
	}

	public function sqlTransactionTypeCode() {
		return ( true == isset( $this->m_strTransactionTypeCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTransactionTypeCode ) : '\'' . addslashes( $this->m_strTransactionTypeCode ) . '\'' ) : 'NULL';
	}

	public function setPayableDate( $strPayableDate ) {
		$this->set( 'm_strPayableDate', CStrings::strTrimDef( $strPayableDate, 6, NULL, true ) );
	}

	public function getPayableDate() {
		return $this->m_strPayableDate;
	}

	public function sqlPayableDate() {
		return ( true == isset( $this->m_strPayableDate ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPayableDate ) : '\'' . addslashes( $this->m_strPayableDate ) . '\'' ) : 'NULL';
	}

	public function setOriginatorShortName( $strOriginatorShortName ) {
		$this->set( 'm_strOriginatorShortName', CStrings::strTrimDef( $strOriginatorShortName, 15, NULL, true ) );
	}

	public function getOriginatorShortName() {
		return $this->m_strOriginatorShortName;
	}

	public function sqlOriginatorShortName() {
		return ( true == isset( $this->m_strOriginatorShortName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOriginatorShortName ) : '\'' . addslashes( $this->m_strOriginatorShortName ) . '\'' ) : 'NULL';
	}

	public function setOriginatorLongName( $strOriginatorLongName ) {
		$this->set( 'm_strOriginatorLongName', CStrings::strTrimDef( $strOriginatorLongName, 30, NULL, true ) );
	}

	public function getOriginatorLongName() {
		return $this->m_strOriginatorLongName;
	}

	public function sqlOriginatorLongName() {
		return ( true == isset( $this->m_strOriginatorLongName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOriginatorLongName ) : '\'' . addslashes( $this->m_strOriginatorLongName ) . '\'' ) : 'NULL';
	}

	public function setReturnsInstitutionId( $strReturnsInstitutionId ) {
		$this->set( 'm_strReturnsInstitutionId', CStrings::strTrimDef( $strReturnsInstitutionId, 9, NULL, true ) );
	}

	public function getReturnsInstitutionId() {
		return $this->m_strReturnsInstitutionId;
	}

	public function sqlReturnsInstitutionId() {
		return ( true == isset( $this->m_strReturnsInstitutionId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReturnsInstitutionId ) : '\'' . addslashes( $this->m_strReturnsInstitutionId ) . '\'' ) : 'NULL';
	}

	public function setReturnsAccountNumberEncrypted( $strReturnsAccountNumberEncrypted ) {
		$this->set( 'm_strReturnsAccountNumberEncrypted', CStrings::strTrimDef( $strReturnsAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getReturnsAccountNumberEncrypted() {
		return $this->m_strReturnsAccountNumberEncrypted;
	}

	public function sqlReturnsAccountNumberEncrypted() {
		return ( true == isset( $this->m_strReturnsAccountNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReturnsAccountNumberEncrypted ) : '\'' . addslashes( $this->m_strReturnsAccountNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setBatchRecordCount( $strBatchRecordCount ) {
		$this->set( 'm_strBatchRecordCount', CStrings::strTrimDef( $strBatchRecordCount, 10, NULL, true ) );
	}

	public function getBatchRecordCount() {
		return $this->m_strBatchRecordCount;
	}

	public function sqlBatchRecordCount() {
		return ( true == isset( $this->m_strBatchRecordCount ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBatchRecordCount ) : '\'' . addslashes( $this->m_strBatchRecordCount ) . '\'' ) : 'NULL';
	}

	public function setBatchAmount( $strBatchAmount ) {
		$this->set( 'm_strBatchAmount', CStrings::strTrimDef( $strBatchAmount, 14, NULL, true ) );
	}

	public function getBatchAmount() {
		return $this->m_strBatchAmount;
	}

	public function sqlBatchAmount() {
		return ( true == isset( $this->m_strBatchAmount ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBatchAmount ) : '\'' . addslashes( $this->m_strBatchAmount ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsReconPrepped( $boolIsReconPrepped ) {
		$this->set( 'm_boolIsReconPrepped', CStrings::strToBool( $boolIsReconPrepped ) );
	}

	public function getIsReconPrepped() {
		return $this->m_boolIsReconPrepped;
	}

	public function sqlIsReconPrepped() {
		return ( true == isset( $this->m_boolIsReconPrepped ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReconPrepped ? 'true' : 'false' ) . '\'' : 'FALSE';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, aft_file_id, batch_payment_type_code, transaction_type_code, payable_date, originator_short_name, originator_long_name, returns_institution_id, returns_account_number_encrypted, batch_record_count, batch_amount, updated_by, updated_on, created_by, created_on, is_recon_prepped )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlAftFileId() . ', ' .
						$this->sqlBatchPaymentTypeCode() . ', ' .
						$this->sqlTransactionTypeCode() . ', ' .
						$this->sqlPayableDate() . ', ' .
						$this->sqlOriginatorShortName() . ', ' .
						$this->sqlOriginatorLongName() . ', ' .
						$this->sqlReturnsInstitutionId() . ', ' .
						$this->sqlReturnsAccountNumberEncrypted() . ', ' .
						$this->sqlBatchRecordCount() . ', ' .
						$this->sqlBatchAmount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsReconPrepped() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' aft_file_id = ' . $this->sqlAftFileId(). ',' ; } elseif( true == array_key_exists( 'AftFileId', $this->getChangedColumns() ) ) { $strSql .= ' aft_file_id = ' . $this->sqlAftFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_payment_type_code = ' . $this->sqlBatchPaymentTypeCode(). ',' ; } elseif( true == array_key_exists( 'BatchPaymentTypeCode', $this->getChangedColumns() ) ) { $strSql .= ' batch_payment_type_code = ' . $this->sqlBatchPaymentTypeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_type_code = ' . $this->sqlTransactionTypeCode(). ',' ; } elseif( true == array_key_exists( 'TransactionTypeCode', $this->getChangedColumns() ) ) { $strSql .= ' transaction_type_code = ' . $this->sqlTransactionTypeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payable_date = ' . $this->sqlPayableDate(). ',' ; } elseif( true == array_key_exists( 'PayableDate', $this->getChangedColumns() ) ) { $strSql .= ' payable_date = ' . $this->sqlPayableDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' originator_short_name = ' . $this->sqlOriginatorShortName(). ',' ; } elseif( true == array_key_exists( 'OriginatorShortName', $this->getChangedColumns() ) ) { $strSql .= ' originator_short_name = ' . $this->sqlOriginatorShortName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' originator_long_name = ' . $this->sqlOriginatorLongName(). ',' ; } elseif( true == array_key_exists( 'OriginatorLongName', $this->getChangedColumns() ) ) { $strSql .= ' originator_long_name = ' . $this->sqlOriginatorLongName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returns_institution_id = ' . $this->sqlReturnsInstitutionId(). ',' ; } elseif( true == array_key_exists( 'ReturnsInstitutionId', $this->getChangedColumns() ) ) { $strSql .= ' returns_institution_id = ' . $this->sqlReturnsInstitutionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returns_account_number_encrypted = ' . $this->sqlReturnsAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'ReturnsAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' returns_account_number_encrypted = ' . $this->sqlReturnsAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_record_count = ' . $this->sqlBatchRecordCount(). ',' ; } elseif( true == array_key_exists( 'BatchRecordCount', $this->getChangedColumns() ) ) { $strSql .= ' batch_record_count = ' . $this->sqlBatchRecordCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_amount = ' . $this->sqlBatchAmount(). ',' ; } elseif( true == array_key_exists( 'BatchAmount', $this->getChangedColumns() ) ) { $strSql .= ' batch_amount = ' . $this->sqlBatchAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped(). ',' ; } elseif( true == array_key_exists( 'IsReconPrepped', $this->getChangedColumns() ) ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'aft_file_id' => $this->getAftFileId(),
			'batch_payment_type_code' => $this->getBatchPaymentTypeCode(),
			'transaction_type_code' => $this->getTransactionTypeCode(),
			'payable_date' => $this->getPayableDate(),
			'originator_short_name' => $this->getOriginatorShortName(),
			'originator_long_name' => $this->getOriginatorLongName(),
			'returns_institution_id' => $this->getReturnsInstitutionId(),
			'returns_account_number_encrypted' => $this->getReturnsAccountNumberEncrypted(),
			'batch_record_count' => $this->getBatchRecordCount(),
			'batch_amount' => $this->getBatchAmount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_recon_prepped' => $this->getIsReconPrepped()
		);
	}

}
?>