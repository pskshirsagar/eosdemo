<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CClearingBatches
 * Do not add any new functions to this class.
 */

class CBaseClearingBatches extends CEosPluralBase {

	/**
	 * @return CClearingBatch[]
	 */
	public static function fetchClearingBatches( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CClearingBatch::class, $objDatabase );
	}

	/**
	 * @return CClearingBatch
	 */
	public static function fetchClearingBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CClearingBatch::class, $objDatabase );
	}

	public static function fetchClearingBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'clearing_batches', $objDatabase );
	}

	public static function fetchClearingBatchById( $intId, $objDatabase ) {
		return self::fetchClearingBatch( sprintf( 'SELECT * FROM clearing_batches WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchClearingBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchClearingBatches( sprintf( 'SELECT * FROM clearing_batches WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchClearingBatchesByProcessingBankId( $intProcessingBankId, $objDatabase ) {
		return self::fetchClearingBatches( sprintf( 'SELECT * FROM clearing_batches WHERE processing_bank_id = %d', ( int ) $intProcessingBankId ), $objDatabase );
	}

	public static function fetchClearingBatchesByCompanyMerchantAccountId( $intCompanyMerchantAccountId, $objDatabase ) {
		return self::fetchClearingBatches( sprintf( 'SELECT * FROM clearing_batches WHERE company_merchant_account_id = %d', ( int ) $intCompanyMerchantAccountId ), $objDatabase );
	}

	public static function fetchClearingBatchesByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchClearingBatches( sprintf( 'SELECT * FROM clearing_batches WHERE account_id = %d', ( int ) $intAccountId ), $objDatabase );
	}

	public static function fetchClearingBatchesByClearingBatchTypeId( $intClearingBatchTypeId, $objDatabase ) {
		return self::fetchClearingBatches( sprintf( 'SELECT * FROM clearing_batches WHERE clearing_batch_type_id = %d', ( int ) $intClearingBatchTypeId ), $objDatabase );
	}

	public static function fetchClearingBatchesByInvoiceId( $intInvoiceId, $objDatabase ) {
		return self::fetchClearingBatches( sprintf( 'SELECT * FROM clearing_batches WHERE invoice_id = %d', ( int ) $intInvoiceId ), $objDatabase );
	}

	public static function fetchClearingBatchesByReturnTypeId( $intReturnTypeId, $objDatabase ) {
		return self::fetchClearingBatches( sprintf( 'SELECT * FROM clearing_batches WHERE return_type_id = %d', ( int ) $intReturnTypeId ), $objDatabase );
	}

}
?>