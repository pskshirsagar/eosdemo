<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentNotes
 * Do not add any new functions to this class.
 */

class CBaseArPaymentNotes extends CEosPluralBase {

	/**
	 * @return CArPaymentNote[]
	 */
	public static function fetchArPaymentNotes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CArPaymentNote::class, $objDatabase );
	}

	/**
	 * @return CArPaymentNote
	 */
	public static function fetchArPaymentNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CArPaymentNote::class, $objDatabase );
	}

	public static function fetchArPaymentNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ar_payment_notes', $objDatabase );
	}

	public static function fetchArPaymentNoteById( $intId, $objDatabase ) {
		return self::fetchArPaymentNote( sprintf( 'SELECT * FROM ar_payment_notes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchArPaymentNotesByCid( $intCid, $objDatabase ) {
		return self::fetchArPaymentNotes( sprintf( 'SELECT * FROM ar_payment_notes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentNotesByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchArPaymentNotes( sprintf( 'SELECT * FROM ar_payment_notes WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

	public static function fetchArPaymentNotesByArPaymentNoteTypeId( $intArPaymentNoteTypeId, $objDatabase ) {
		return self::fetchArPaymentNotes( sprintf( 'SELECT * FROM ar_payment_notes WHERE ar_payment_note_type_id = %d', ( int ) $intArPaymentNoteTypeId ), $objDatabase );
	}

}
?>