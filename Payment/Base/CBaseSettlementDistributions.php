<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CSettlementDistributions
 * Do not add any new functions to this class.
 */

class CBaseSettlementDistributions extends CEosPluralBase {

	/**
	 * @return CSettlementDistribution[]
	 */
	public static function fetchSettlementDistributions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSettlementDistribution::class, $objDatabase );
	}

	/**
	 * @return CSettlementDistribution
	 */
	public static function fetchSettlementDistribution( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSettlementDistribution::class, $objDatabase );
	}

	public static function fetchSettlementDistributionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'settlement_distributions', $objDatabase );
	}

	public static function fetchSettlementDistributionById( $intId, $objDatabase ) {
		return self::fetchSettlementDistribution( sprintf( 'SELECT * FROM settlement_distributions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSettlementDistributionsByCid( $intCid, $objDatabase ) {
		return self::fetchSettlementDistributions( sprintf( 'SELECT * FROM settlement_distributions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSettlementDistributionsByProcessingBankId( $intProcessingBankId, $objDatabase ) {
		return self::fetchSettlementDistributions( sprintf( 'SELECT * FROM settlement_distributions WHERE processing_bank_id = %d', ( int ) $intProcessingBankId ), $objDatabase );
	}

	public static function fetchSettlementDistributionsByPaymentTypeId( $intPaymentTypeId, $objDatabase ) {
		return self::fetchSettlementDistributions( sprintf( 'SELECT * FROM settlement_distributions WHERE payment_type_id = %d', ( int ) $intPaymentTypeId ), $objDatabase );
	}

	public static function fetchSettlementDistributionsByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchSettlementDistributions( sprintf( 'SELECT * FROM settlement_distributions WHERE account_id = %d', ( int ) $intAccountId ), $objDatabase );
	}

	public static function fetchSettlementDistributionsByCompanyMerchantAccountId( $intCompanyMerchantAccountId, $objDatabase ) {
		return self::fetchSettlementDistributions( sprintf( 'SELECT * FROM settlement_distributions WHERE company_merchant_account_id = %d', ( int ) $intCompanyMerchantAccountId ), $objDatabase );
	}

	public static function fetchSettlementDistributionsByDistributionStatusTypeId( $intDistributionStatusTypeId, $objDatabase ) {
		return self::fetchSettlementDistributions( sprintf( 'SELECT * FROM settlement_distributions WHERE distribution_status_type_id = %d', ( int ) $intDistributionStatusTypeId ), $objDatabase );
	}

	public static function fetchSettlementDistributionsByReturnTypeId( $intReturnTypeId, $objDatabase ) {
		return self::fetchSettlementDistributions( sprintf( 'SELECT * FROM settlement_distributions WHERE return_type_id = %d', ( int ) $intReturnTypeId ), $objDatabase );
	}

	public static function fetchSettlementDistributionsByCheckAccountTypeId( $intCheckAccountTypeId, $objDatabase ) {
		return self::fetchSettlementDistributions( sprintf( 'SELECT * FROM settlement_distributions WHERE check_account_type_id = %d', ( int ) $intCheckAccountTypeId ), $objDatabase );
	}

}
?>