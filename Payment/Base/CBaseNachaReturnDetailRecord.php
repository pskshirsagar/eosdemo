<?php

class CBaseNachaReturnDetailRecord extends CEosSingularBase {

	const TABLE_NAME = 'public.nacha_return_detail_records';

	protected $m_intId;
	protected $m_intNachaReturnFileId;
	protected $m_intNachaReturnFileBatchId;
	protected $m_intCheckAccountTypeId;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckAccountNumberEncrypted;
	protected $m_strRecordTypeCode;
	protected $m_strRecordTransactionCode;
	protected $m_strReceivingDfiIdentification;
	protected $m_strCheckDigit;
	protected $m_strDfiAccountNumber;
	protected $m_strAmount;
	protected $m_strIdentificationNumber;
	protected $m_strReceivingCompanyName;
	protected $m_strDiscretionaryData;
	protected $m_strAddendaRecordIndicator;
	protected $m_strTraceNumber;
	protected $m_strReturnedOn;
	protected $m_strSentToReturnsAccountOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['nacha_return_file_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaReturnFileId', trim( $arrValues['nacha_return_file_id'] ) ); elseif( isset( $arrValues['nacha_return_file_id'] ) ) $this->setNachaReturnFileId( $arrValues['nacha_return_file_id'] );
		if( isset( $arrValues['nacha_return_file_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaReturnFileBatchId', trim( $arrValues['nacha_return_file_batch_id'] ) ); elseif( isset( $arrValues['nacha_return_file_batch_id'] ) ) $this->setNachaReturnFileBatchId( $arrValues['nacha_return_file_batch_id'] );
		if( isset( $arrValues['check_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckAccountTypeId', trim( $arrValues['check_account_type_id'] ) ); elseif( isset( $arrValues['check_account_type_id'] ) ) $this->setCheckAccountTypeId( $arrValues['check_account_type_id'] );
		if( isset( $arrValues['check_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckRoutingNumber', trim( stripcslashes( $arrValues['check_routing_number'] ) ) ); elseif( isset( $arrValues['check_routing_number'] ) ) $this->setCheckRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_routing_number'] ) : $arrValues['check_routing_number'] );
		if( isset( $arrValues['check_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberEncrypted', trim( stripcslashes( $arrValues['check_account_number_encrypted'] ) ) ); elseif( isset( $arrValues['check_account_number_encrypted'] ) ) $this->setCheckAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_account_number_encrypted'] ) : $arrValues['check_account_number_encrypted'] );
		if( isset( $arrValues['record_type_code'] ) && $boolDirectSet ) $this->set( 'm_strRecordTypeCode', trim( stripcslashes( $arrValues['record_type_code'] ) ) ); elseif( isset( $arrValues['record_type_code'] ) ) $this->setRecordTypeCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_type_code'] ) : $arrValues['record_type_code'] );
		if( isset( $arrValues['record_transaction_code'] ) && $boolDirectSet ) $this->set( 'm_strRecordTransactionCode', trim( stripcslashes( $arrValues['record_transaction_code'] ) ) ); elseif( isset( $arrValues['record_transaction_code'] ) ) $this->setRecordTransactionCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_transaction_code'] ) : $arrValues['record_transaction_code'] );
		if( isset( $arrValues['receiving_dfi_identification'] ) && $boolDirectSet ) $this->set( 'm_strReceivingDfiIdentification', trim( stripcslashes( $arrValues['receiving_dfi_identification'] ) ) ); elseif( isset( $arrValues['receiving_dfi_identification'] ) ) $this->setReceivingDfiIdentification( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['receiving_dfi_identification'] ) : $arrValues['receiving_dfi_identification'] );
		if( isset( $arrValues['check_digit'] ) && $boolDirectSet ) $this->set( 'm_strCheckDigit', trim( stripcslashes( $arrValues['check_digit'] ) ) ); elseif( isset( $arrValues['check_digit'] ) ) $this->setCheckDigit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_digit'] ) : $arrValues['check_digit'] );
		if( isset( $arrValues['dfi_account_number'] ) && $boolDirectSet ) $this->set( 'm_strDfiAccountNumber', trim( stripcslashes( $arrValues['dfi_account_number'] ) ) ); elseif( isset( $arrValues['dfi_account_number'] ) ) $this->setDfiAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dfi_account_number'] ) : $arrValues['dfi_account_number'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_strAmount', trim( stripcslashes( $arrValues['amount'] ) ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['amount'] ) : $arrValues['amount'] );
		if( isset( $arrValues['identification_number'] ) && $boolDirectSet ) $this->set( 'm_strIdentificationNumber', trim( stripcslashes( $arrValues['identification_number'] ) ) ); elseif( isset( $arrValues['identification_number'] ) ) $this->setIdentificationNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['identification_number'] ) : $arrValues['identification_number'] );
		if( isset( $arrValues['receiving_company_name'] ) && $boolDirectSet ) $this->set( 'm_strReceivingCompanyName', trim( stripcslashes( $arrValues['receiving_company_name'] ) ) ); elseif( isset( $arrValues['receiving_company_name'] ) ) $this->setReceivingCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['receiving_company_name'] ) : $arrValues['receiving_company_name'] );
		if( isset( $arrValues['discretionary_data'] ) && $boolDirectSet ) $this->set( 'm_strDiscretionaryData', trim( stripcslashes( $arrValues['discretionary_data'] ) ) ); elseif( isset( $arrValues['discretionary_data'] ) ) $this->setDiscretionaryData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['discretionary_data'] ) : $arrValues['discretionary_data'] );
		if( isset( $arrValues['addenda_record_indicator'] ) && $boolDirectSet ) $this->set( 'm_strAddendaRecordIndicator', trim( stripcslashes( $arrValues['addenda_record_indicator'] ) ) ); elseif( isset( $arrValues['addenda_record_indicator'] ) ) $this->setAddendaRecordIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['addenda_record_indicator'] ) : $arrValues['addenda_record_indicator'] );
		if( isset( $arrValues['trace_number'] ) && $boolDirectSet ) $this->set( 'm_strTraceNumber', trim( stripcslashes( $arrValues['trace_number'] ) ) ); elseif( isset( $arrValues['trace_number'] ) ) $this->setTraceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['trace_number'] ) : $arrValues['trace_number'] );
		if( isset( $arrValues['returned_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnedOn', trim( $arrValues['returned_on'] ) ); elseif( isset( $arrValues['returned_on'] ) ) $this->setReturnedOn( $arrValues['returned_on'] );
		if( isset( $arrValues['sent_to_returns_account_on'] ) && $boolDirectSet ) $this->set( 'm_strSentToReturnsAccountOn', trim( $arrValues['sent_to_returns_account_on'] ) ); elseif( isset( $arrValues['sent_to_returns_account_on'] ) ) $this->setSentToReturnsAccountOn( $arrValues['sent_to_returns_account_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setNachaReturnFileId( $intNachaReturnFileId ) {
		$this->set( 'm_intNachaReturnFileId', CStrings::strToIntDef( $intNachaReturnFileId, NULL, false ) );
	}

	public function getNachaReturnFileId() {
		return $this->m_intNachaReturnFileId;
	}

	public function sqlNachaReturnFileId() {
		return ( true == isset( $this->m_intNachaReturnFileId ) ) ? ( string ) $this->m_intNachaReturnFileId : 'NULL';
	}

	public function setNachaReturnFileBatchId( $intNachaReturnFileBatchId ) {
		$this->set( 'm_intNachaReturnFileBatchId', CStrings::strToIntDef( $intNachaReturnFileBatchId, NULL, false ) );
	}

	public function getNachaReturnFileBatchId() {
		return $this->m_intNachaReturnFileBatchId;
	}

	public function sqlNachaReturnFileBatchId() {
		return ( true == isset( $this->m_intNachaReturnFileBatchId ) ) ? ( string ) $this->m_intNachaReturnFileBatchId : 'NULL';
	}

	public function setCheckAccountTypeId( $intCheckAccountTypeId ) {
		$this->set( 'm_intCheckAccountTypeId', CStrings::strToIntDef( $intCheckAccountTypeId, NULL, false ) );
	}

	public function getCheckAccountTypeId() {
		return $this->m_intCheckAccountTypeId;
	}

	public function sqlCheckAccountTypeId() {
		return ( true == isset( $this->m_intCheckAccountTypeId ) ) ? ( string ) $this->m_intCheckAccountTypeId : 'NULL';
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->set( 'm_strCheckRoutingNumber', CStrings::strTrimDef( $strCheckRoutingNumber, 240, NULL, true ) );
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function sqlCheckRoutingNumber() {
		return ( true == isset( $this->m_strCheckRoutingNumber ) ) ? '\'' . addslashes( $this->m_strCheckRoutingNumber ) . '\'' : 'NULL';
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->set( 'm_strCheckAccountNumberEncrypted', CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function sqlCheckAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCheckAccountNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setRecordTypeCode( $strRecordTypeCode ) {
		$this->set( 'm_strRecordTypeCode', CStrings::strTrimDef( $strRecordTypeCode, 1, NULL, true ) );
	}

	public function getRecordTypeCode() {
		return $this->m_strRecordTypeCode;
	}

	public function sqlRecordTypeCode() {
		return ( true == isset( $this->m_strRecordTypeCode ) ) ? '\'' . addslashes( $this->m_strRecordTypeCode ) . '\'' : 'NULL';
	}

	public function setRecordTransactionCode( $strRecordTransactionCode ) {
		$this->set( 'm_strRecordTransactionCode', CStrings::strTrimDef( $strRecordTransactionCode, 2, NULL, true ) );
	}

	public function getRecordTransactionCode() {
		return $this->m_strRecordTransactionCode;
	}

	public function sqlRecordTransactionCode() {
		return ( true == isset( $this->m_strRecordTransactionCode ) ) ? '\'' . addslashes( $this->m_strRecordTransactionCode ) . '\'' : 'NULL';
	}

	public function setReceivingDfiIdentification( $strReceivingDfiIdentification ) {
		$this->set( 'm_strReceivingDfiIdentification', CStrings::strTrimDef( $strReceivingDfiIdentification, 8, NULL, true ) );
	}

	public function getReceivingDfiIdentification() {
		return $this->m_strReceivingDfiIdentification;
	}

	public function sqlReceivingDfiIdentification() {
		return ( true == isset( $this->m_strReceivingDfiIdentification ) ) ? '\'' . addslashes( $this->m_strReceivingDfiIdentification ) . '\'' : 'NULL';
	}

	public function setCheckDigit( $strCheckDigit ) {
		$this->set( 'm_strCheckDigit', CStrings::strTrimDef( $strCheckDigit, 1, NULL, true ) );
	}

	public function getCheckDigit() {
		return $this->m_strCheckDigit;
	}

	public function sqlCheckDigit() {
		return ( true == isset( $this->m_strCheckDigit ) ) ? '\'' . addslashes( $this->m_strCheckDigit ) . '\'' : 'NULL';
	}

	public function setDfiAccountNumber( $strDfiAccountNumber ) {
		$this->set( 'm_strDfiAccountNumber', CStrings::strTrimDef( $strDfiAccountNumber, 17, NULL, true ) );
	}

	public function getDfiAccountNumber() {
		return $this->m_strDfiAccountNumber;
	}

	public function sqlDfiAccountNumber() {
		return ( true == isset( $this->m_strDfiAccountNumber ) ) ? '\'' . addslashes( $this->m_strDfiAccountNumber ) . '\'' : 'NULL';
	}

	public function setAmount( $strAmount ) {
		$this->set( 'm_strAmount', CStrings::strTrimDef( $strAmount, 10, NULL, true ) );
	}

	public function getAmount() {
		return $this->m_strAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_strAmount ) ) ? '\'' . addslashes( $this->m_strAmount ) . '\'' : 'NULL';
	}

	public function setIdentificationNumber( $strIdentificationNumber ) {
		$this->set( 'm_strIdentificationNumber', CStrings::strTrimDef( $strIdentificationNumber, 15, NULL, true ) );
	}

	public function getIdentificationNumber() {
		return $this->m_strIdentificationNumber;
	}

	public function sqlIdentificationNumber() {
		return ( true == isset( $this->m_strIdentificationNumber ) ) ? '\'' . addslashes( $this->m_strIdentificationNumber ) . '\'' : 'NULL';
	}

	public function setReceivingCompanyName( $strReceivingCompanyName ) {
		$this->set( 'm_strReceivingCompanyName', CStrings::strTrimDef( $strReceivingCompanyName, 22, NULL, true ) );
	}

	public function getReceivingCompanyName() {
		return $this->m_strReceivingCompanyName;
	}

	public function sqlReceivingCompanyName() {
		return ( true == isset( $this->m_strReceivingCompanyName ) ) ? '\'' . addslashes( $this->m_strReceivingCompanyName ) . '\'' : 'NULL';
	}

	public function setDiscretionaryData( $strDiscretionaryData ) {
		$this->set( 'm_strDiscretionaryData', CStrings::strTrimDef( $strDiscretionaryData, 2, NULL, true ) );
	}

	public function getDiscretionaryData() {
		return $this->m_strDiscretionaryData;
	}

	public function sqlDiscretionaryData() {
		return ( true == isset( $this->m_strDiscretionaryData ) ) ? '\'' . addslashes( $this->m_strDiscretionaryData ) . '\'' : 'NULL';
	}

	public function setAddendaRecordIndicator( $strAddendaRecordIndicator ) {
		$this->set( 'm_strAddendaRecordIndicator', CStrings::strTrimDef( $strAddendaRecordIndicator, 1, NULL, true ) );
	}

	public function getAddendaRecordIndicator() {
		return $this->m_strAddendaRecordIndicator;
	}

	public function sqlAddendaRecordIndicator() {
		return ( true == isset( $this->m_strAddendaRecordIndicator ) ) ? '\'' . addslashes( $this->m_strAddendaRecordIndicator ) . '\'' : 'NULL';
	}

	public function setTraceNumber( $strTraceNumber ) {
		$this->set( 'm_strTraceNumber', CStrings::strTrimDef( $strTraceNumber, 15, NULL, true ) );
	}

	public function getTraceNumber() {
		return $this->m_strTraceNumber;
	}

	public function sqlTraceNumber() {
		return ( true == isset( $this->m_strTraceNumber ) ) ? '\'' . addslashes( $this->m_strTraceNumber ) . '\'' : 'NULL';
	}

	public function setReturnedOn( $strReturnedOn ) {
		$this->set( 'm_strReturnedOn', CStrings::strTrimDef( $strReturnedOn, -1, NULL, true ) );
	}

	public function getReturnedOn() {
		return $this->m_strReturnedOn;
	}

	public function sqlReturnedOn() {
		return ( true == isset( $this->m_strReturnedOn ) ) ? '\'' . $this->m_strReturnedOn . '\'' : 'NULL';
	}

	public function setSentToReturnsAccountOn( $strSentToReturnsAccountOn ) {
		$this->set( 'm_strSentToReturnsAccountOn', CStrings::strTrimDef( $strSentToReturnsAccountOn, -1, NULL, true ) );
	}

	public function getSentToReturnsAccountOn() {
		return $this->m_strSentToReturnsAccountOn;
	}

	public function sqlSentToReturnsAccountOn() {
		return ( true == isset( $this->m_strSentToReturnsAccountOn ) ) ? '\'' . $this->m_strSentToReturnsAccountOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, nacha_return_file_id, nacha_return_file_batch_id, check_account_type_id, check_routing_number, check_account_number_encrypted, record_type_code, record_transaction_code, receiving_dfi_identification, check_digit, dfi_account_number, amount, identification_number, receiving_company_name, discretionary_data, addenda_record_indicator, trace_number, returned_on, sent_to_returns_account_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlNachaReturnFileId() . ', ' .
 						$this->sqlNachaReturnFileBatchId() . ', ' .
 						$this->sqlCheckAccountTypeId() . ', ' .
 						$this->sqlCheckRoutingNumber() . ', ' .
 						$this->sqlCheckAccountNumberEncrypted() . ', ' .
 						$this->sqlRecordTypeCode() . ', ' .
 						$this->sqlRecordTransactionCode() . ', ' .
 						$this->sqlReceivingDfiIdentification() . ', ' .
 						$this->sqlCheckDigit() . ', ' .
 						$this->sqlDfiAccountNumber() . ', ' .
 						$this->sqlAmount() . ', ' .
 						$this->sqlIdentificationNumber() . ', ' .
 						$this->sqlReceivingCompanyName() . ', ' .
 						$this->sqlDiscretionaryData() . ', ' .
 						$this->sqlAddendaRecordIndicator() . ', ' .
 						$this->sqlTraceNumber() . ', ' .
 						$this->sqlReturnedOn() . ', ' .
 						$this->sqlSentToReturnsAccountOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_return_file_id = ' . $this->sqlNachaReturnFileId() . ','; } elseif( true == array_key_exists( 'NachaReturnFileId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_return_file_id = ' . $this->sqlNachaReturnFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_return_file_batch_id = ' . $this->sqlNachaReturnFileBatchId() . ','; } elseif( true == array_key_exists( 'NachaReturnFileBatchId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_return_file_batch_id = ' . $this->sqlNachaReturnFileBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId() . ','; } elseif( true == array_key_exists( 'CheckAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber() . ','; } elseif( true == array_key_exists( 'CheckRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted() . ','; } elseif( true == array_key_exists( 'CheckAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_type_code = ' . $this->sqlRecordTypeCode() . ','; } elseif( true == array_key_exists( 'RecordTypeCode', $this->getChangedColumns() ) ) { $strSql .= ' record_type_code = ' . $this->sqlRecordTypeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_transaction_code = ' . $this->sqlRecordTransactionCode() . ','; } elseif( true == array_key_exists( 'RecordTransactionCode', $this->getChangedColumns() ) ) { $strSql .= ' record_transaction_code = ' . $this->sqlRecordTransactionCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' receiving_dfi_identification = ' . $this->sqlReceivingDfiIdentification() . ','; } elseif( true == array_key_exists( 'ReceivingDfiIdentification', $this->getChangedColumns() ) ) { $strSql .= ' receiving_dfi_identification = ' . $this->sqlReceivingDfiIdentification() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_digit = ' . $this->sqlCheckDigit() . ','; } elseif( true == array_key_exists( 'CheckDigit', $this->getChangedColumns() ) ) { $strSql .= ' check_digit = ' . $this->sqlCheckDigit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dfi_account_number = ' . $this->sqlDfiAccountNumber() . ','; } elseif( true == array_key_exists( 'DfiAccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' dfi_account_number = ' . $this->sqlDfiAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' identification_number = ' . $this->sqlIdentificationNumber() . ','; } elseif( true == array_key_exists( 'IdentificationNumber', $this->getChangedColumns() ) ) { $strSql .= ' identification_number = ' . $this->sqlIdentificationNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' receiving_company_name = ' . $this->sqlReceivingCompanyName() . ','; } elseif( true == array_key_exists( 'ReceivingCompanyName', $this->getChangedColumns() ) ) { $strSql .= ' receiving_company_name = ' . $this->sqlReceivingCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' discretionary_data = ' . $this->sqlDiscretionaryData() . ','; } elseif( true == array_key_exists( 'DiscretionaryData', $this->getChangedColumns() ) ) { $strSql .= ' discretionary_data = ' . $this->sqlDiscretionaryData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' addenda_record_indicator = ' . $this->sqlAddendaRecordIndicator() . ','; } elseif( true == array_key_exists( 'AddendaRecordIndicator', $this->getChangedColumns() ) ) { $strSql .= ' addenda_record_indicator = ' . $this->sqlAddendaRecordIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trace_number = ' . $this->sqlTraceNumber() . ','; } elseif( true == array_key_exists( 'TraceNumber', $this->getChangedColumns() ) ) { $strSql .= ' trace_number = ' . $this->sqlTraceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; } elseif( true == array_key_exists( 'ReturnedOn', $this->getChangedColumns() ) ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_to_returns_account_on = ' . $this->sqlSentToReturnsAccountOn() . ','; } elseif( true == array_key_exists( 'SentToReturnsAccountOn', $this->getChangedColumns() ) ) { $strSql .= ' sent_to_returns_account_on = ' . $this->sqlSentToReturnsAccountOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'nacha_return_file_id' => $this->getNachaReturnFileId(),
			'nacha_return_file_batch_id' => $this->getNachaReturnFileBatchId(),
			'check_account_type_id' => $this->getCheckAccountTypeId(),
			'check_routing_number' => $this->getCheckRoutingNumber(),
			'check_account_number_encrypted' => $this->getCheckAccountNumberEncrypted(),
			'record_type_code' => $this->getRecordTypeCode(),
			'record_transaction_code' => $this->getRecordTransactionCode(),
			'receiving_dfi_identification' => $this->getReceivingDfiIdentification(),
			'check_digit' => $this->getCheckDigit(),
			'dfi_account_number' => $this->getDfiAccountNumber(),
			'amount' => $this->getAmount(),
			'identification_number' => $this->getIdentificationNumber(),
			'receiving_company_name' => $this->getReceivingCompanyName(),
			'discretionary_data' => $this->getDiscretionaryData(),
			'addenda_record_indicator' => $this->getAddendaRecordIndicator(),
			'trace_number' => $this->getTraceNumber(),
			'returned_on' => $this->getReturnedOn(),
			'sent_to_returns_account_on' => $this->getSentToReturnsAccountOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>