<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMoneyGramBillPayments
 * Do not add any new functions to this class.
 */

class CBaseMoneyGramBillPayments extends CEosPluralBase {

	/**
	 * @return CMoneyGramBillPayment[]
	 */
	public static function fetchMoneyGramBillPayments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMoneyGramBillPayment::class, $objDatabase );
	}

	/**
	 * @return CMoneyGramBillPayment
	 */
	public static function fetchMoneyGramBillPayment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMoneyGramBillPayment::class, $objDatabase );
	}

	public static function fetchMoneyGramBillPaymentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'money_gram_bill_payments', $objDatabase );
	}

	public static function fetchMoneyGramBillPaymentById( $intId, $objDatabase ) {
		return self::fetchMoneyGramBillPayment( sprintf( 'SELECT * FROM money_gram_bill_payments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMoneyGramBillPaymentsByCid( $intCid, $objDatabase ) {
		return self::fetchMoneyGramBillPayments( sprintf( 'SELECT * FROM money_gram_bill_payments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyGramBillPaymentsByMoneyGramAccountId( $intMoneyGramAccountId, $objDatabase ) {
		return self::fetchMoneyGramBillPayments( sprintf( 'SELECT * FROM money_gram_bill_payments WHERE money_gram_account_id = %d', ( int ) $intMoneyGramAccountId ), $objDatabase );
	}

	public static function fetchMoneyGramBillPaymentsByAgentId( $intAgentId, $objDatabase ) {
		return self::fetchMoneyGramBillPayments( sprintf( 'SELECT * FROM money_gram_bill_payments WHERE agent_id = %d', ( int ) $intAgentId ), $objDatabase );
	}

	public static function fetchMoneyGramBillPaymentsByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchMoneyGramBillPayments( sprintf( 'SELECT * FROM money_gram_bill_payments WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

}
?>