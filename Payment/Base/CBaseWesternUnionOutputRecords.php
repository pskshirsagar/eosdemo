<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CWesternUnionOutputRecords
 * Do not add any new functions to this class.
 */

class CBaseWesternUnionOutputRecords extends CEosPluralBase {

	/**
	 * @return CWesternUnionOutputRecord[]
	 */
	public static function fetchWesternUnionOutputRecords( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CWesternUnionOutputRecord::class, $objDatabase );
	}

	/**
	 * @return CWesternUnionOutputRecord
	 */
	public static function fetchWesternUnionOutputRecord( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CWesternUnionOutputRecord::class, $objDatabase );
	}

	public static function fetchWesternUnionOutputRecordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'western_union_output_records', $objDatabase );
	}

	public static function fetchWesternUnionOutputRecordById( $intId, $objDatabase ) {
		return self::fetchWesternUnionOutputRecord( sprintf( 'SELECT * FROM western_union_output_records WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchWesternUnionOutputRecordsByFileId( $intFileId, $objDatabase ) {
		return self::fetchWesternUnionOutputRecords( sprintf( 'SELECT * FROM western_union_output_records WHERE file_id = %d', ( int ) $intFileId ), $objDatabase );
	}

	public static function fetchWesternUnionOutputRecordsByCid( $intCid, $objDatabase ) {
		return self::fetchWesternUnionOutputRecords( sprintf( 'SELECT * FROM western_union_output_records WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchWesternUnionOutputRecordsByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchWesternUnionOutputRecords( sprintf( 'SELECT * FROM western_union_output_records WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

	public static function fetchWesternUnionOutputRecordsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchWesternUnionOutputRecords( sprintf( 'SELECT * FROM western_union_output_records WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchWesternUnionOutputRecordsByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchWesternUnionOutputRecords( sprintf( 'SELECT * FROM western_union_output_records WHERE customer_id = %d', ( int ) $intCustomerId ), $objDatabase );
	}

	public static function fetchWesternUnionOutputRecordsByLeaseId( $intLeaseId, $objDatabase ) {
		return self::fetchWesternUnionOutputRecords( sprintf( 'SELECT * FROM western_union_output_records WHERE lease_id = %d', ( int ) $intLeaseId ), $objDatabase );
	}

}
?>