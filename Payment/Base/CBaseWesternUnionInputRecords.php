<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CWesternUnionInputRecords
 * Do not add any new functions to this class.
 */

class CBaseWesternUnionInputRecords extends CEosPluralBase {

	/**
	 * @return CWesternUnionInputRecord[]
	 */
	public static function fetchWesternUnionInputRecords( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CWesternUnionInputRecord::class, $objDatabase );
	}

	/**
	 * @return CWesternUnionInputRecord
	 */
	public static function fetchWesternUnionInputRecord( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CWesternUnionInputRecord::class, $objDatabase );
	}

	public static function fetchWesternUnionInputRecordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'western_union_input_records', $objDatabase );
	}

	public static function fetchWesternUnionInputRecordById( $intId, $objDatabase ) {
		return self::fetchWesternUnionInputRecord( sprintf( 'SELECT * FROM western_union_input_records WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchWesternUnionInputRecordsByFileId( $intFileId, $objDatabase ) {
		return self::fetchWesternUnionInputRecords( sprintf( 'SELECT * FROM western_union_input_records WHERE file_id = %d', ( int ) $intFileId ), $objDatabase );
	}

	public static function fetchWesternUnionInputRecordsByCid( $intCid, $objDatabase ) {
		return self::fetchWesternUnionInputRecords( sprintf( 'SELECT * FROM western_union_input_records WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchWesternUnionInputRecordsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchWesternUnionInputRecords( sprintf( 'SELECT * FROM western_union_input_records WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchWesternUnionInputRecordsByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchWesternUnionInputRecords( sprintf( 'SELECT * FROM western_union_input_records WHERE customer_id = %d', ( int ) $intCustomerId ), $objDatabase );
	}

	public static function fetchWesternUnionInputRecordsByLeaseId( $intLeaseId, $objDatabase ) {
		return self::fetchWesternUnionInputRecords( sprintf( 'SELECT * FROM western_union_input_records WHERE lease_id = %d', ( int ) $intLeaseId ), $objDatabase );
	}

}
?>