<?php

class CBaseEftBatchTransaction extends CEosSingularBase {

	const TABLE_NAME = 'public.eft_batch_transactions';

	protected $m_intId;
	protected $m_intEftBatchId;
	protected $m_intItemId;
	protected $m_strTransactionDatetime;
	protected $m_fltTransactionAmount;
	protected $m_strTransactionMemo;
	protected $m_strResponseCode;
	protected $m_strResponseText;
	protected $m_strRawData;
	protected $m_intIsReconPrepped;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsReconPrepped = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['eft_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intEftBatchId', trim( $arrValues['eft_batch_id'] ) ); elseif( isset( $arrValues['eft_batch_id'] ) ) $this->setEftBatchId( $arrValues['eft_batch_id'] );
		if( isset( $arrValues['item_id'] ) && $boolDirectSet ) $this->set( 'm_intItemId', trim( $arrValues['item_id'] ) ); elseif( isset( $arrValues['item_id'] ) ) $this->setItemId( $arrValues['item_id'] );
		if( isset( $arrValues['transaction_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDatetime', trim( $arrValues['transaction_datetime'] ) ); elseif( isset( $arrValues['transaction_datetime'] ) ) $this->setTransactionDatetime( $arrValues['transaction_datetime'] );
		if( isset( $arrValues['transaction_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmount', trim( $arrValues['transaction_amount'] ) ); elseif( isset( $arrValues['transaction_amount'] ) ) $this->setTransactionAmount( $arrValues['transaction_amount'] );
		if( isset( $arrValues['transaction_memo'] ) && $boolDirectSet ) $this->set( 'm_strTransactionMemo', trim( stripcslashes( $arrValues['transaction_memo'] ) ) ); elseif( isset( $arrValues['transaction_memo'] ) ) $this->setTransactionMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['transaction_memo'] ) : $arrValues['transaction_memo'] );
		if( isset( $arrValues['response_code'] ) && $boolDirectSet ) $this->set( 'm_strResponseCode', trim( stripcslashes( $arrValues['response_code'] ) ) ); elseif( isset( $arrValues['response_code'] ) ) $this->setResponseCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['response_code'] ) : $arrValues['response_code'] );
		if( isset( $arrValues['response_text'] ) && $boolDirectSet ) $this->set( 'm_strResponseText', trim( stripcslashes( $arrValues['response_text'] ) ) ); elseif( isset( $arrValues['response_text'] ) ) $this->setResponseText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['response_text'] ) : $arrValues['response_text'] );
		if( isset( $arrValues['raw_data'] ) && $boolDirectSet ) $this->set( 'm_strRawData', trim( stripcslashes( $arrValues['raw_data'] ) ) ); elseif( isset( $arrValues['raw_data'] ) ) $this->setRawData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['raw_data'] ) : $arrValues['raw_data'] );
		if( isset( $arrValues['is_recon_prepped'] ) && $boolDirectSet ) $this->set( 'm_intIsReconPrepped', trim( $arrValues['is_recon_prepped'] ) ); elseif( isset( $arrValues['is_recon_prepped'] ) ) $this->setIsReconPrepped( $arrValues['is_recon_prepped'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEftBatchId( $intEftBatchId ) {
		$this->set( 'm_intEftBatchId', CStrings::strToIntDef( $intEftBatchId, NULL, false ) );
	}

	public function getEftBatchId() {
		return $this->m_intEftBatchId;
	}

	public function sqlEftBatchId() {
		return ( true == isset( $this->m_intEftBatchId ) ) ? ( string ) $this->m_intEftBatchId : 'NULL';
	}

	public function setItemId( $intItemId ) {
		$this->set( 'm_intItemId', CStrings::strToIntDef( $intItemId, NULL, false ) );
	}

	public function getItemId() {
		return $this->m_intItemId;
	}

	public function sqlItemId() {
		return ( true == isset( $this->m_intItemId ) ) ? ( string ) $this->m_intItemId : 'NULL';
	}

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->set( 'm_strTransactionDatetime', CStrings::strTrimDef( $strTransactionDatetime, -1, NULL, true ) );
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function sqlTransactionDatetime() {
		return ( true == isset( $this->m_strTransactionDatetime ) ) ? '\'' . $this->m_strTransactionDatetime . '\'' : 'NOW()';
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->set( 'm_fltTransactionAmount', CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 4 ) );
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function sqlTransactionAmount() {
		return ( true == isset( $this->m_fltTransactionAmount ) ) ? ( string ) $this->m_fltTransactionAmount : 'NULL';
	}

	public function setTransactionMemo( $strTransactionMemo ) {
		$this->set( 'm_strTransactionMemo', CStrings::strTrimDef( $strTransactionMemo, 2000, NULL, true ) );
	}

	public function getTransactionMemo() {
		return $this->m_strTransactionMemo;
	}

	public function sqlTransactionMemo() {
		return ( true == isset( $this->m_strTransactionMemo ) ) ? '\'' . addslashes( $this->m_strTransactionMemo ) . '\'' : 'NULL';
	}

	public function setResponseCode( $strResponseCode ) {
		$this->set( 'm_strResponseCode', CStrings::strTrimDef( $strResponseCode, 5, NULL, true ) );
	}

	public function getResponseCode() {
		return $this->m_strResponseCode;
	}

	public function sqlResponseCode() {
		return ( true == isset( $this->m_strResponseCode ) ) ? '\'' . addslashes( $this->m_strResponseCode ) . '\'' : 'NULL';
	}

	public function setResponseText( $strResponseText ) {
		$this->set( 'm_strResponseText', CStrings::strTrimDef( $strResponseText, 240, NULL, true ) );
	}

	public function getResponseText() {
		return $this->m_strResponseText;
	}

	public function sqlResponseText() {
		return ( true == isset( $this->m_strResponseText ) ) ? '\'' . addslashes( $this->m_strResponseText ) . '\'' : 'NULL';
	}

	public function setRawData( $strRawData ) {
		$this->set( 'm_strRawData', CStrings::strTrimDef( $strRawData, -1, NULL, true ) );
	}

	public function getRawData() {
		return $this->m_strRawData;
	}

	public function sqlRawData() {
		return ( true == isset( $this->m_strRawData ) ) ? '\'' . addslashes( $this->m_strRawData ) . '\'' : 'NULL';
	}

	public function setIsReconPrepped( $intIsReconPrepped ) {
		$this->set( 'm_intIsReconPrepped', CStrings::strToIntDef( $intIsReconPrepped, NULL, false ) );
	}

	public function getIsReconPrepped() {
		return $this->m_intIsReconPrepped;
	}

	public function sqlIsReconPrepped() {
		return ( true == isset( $this->m_intIsReconPrepped ) ) ? ( string ) $this->m_intIsReconPrepped : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, eft_batch_id, item_id, transaction_datetime, transaction_amount, transaction_memo, response_code, response_text, raw_data, is_recon_prepped, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEftBatchId() . ', ' .
 						$this->sqlItemId() . ', ' .
 						$this->sqlTransactionDatetime() . ', ' .
 						$this->sqlTransactionAmount() . ', ' .
 						$this->sqlTransactionMemo() . ', ' .
 						$this->sqlResponseCode() . ', ' .
 						$this->sqlResponseText() . ', ' .
 						$this->sqlRawData() . ', ' .
 						$this->sqlIsReconPrepped() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eft_batch_id = ' . $this->sqlEftBatchId() . ','; } elseif( true == array_key_exists( 'EftBatchId', $this->getChangedColumns() ) ) { $strSql .= ' eft_batch_id = ' . $this->sqlEftBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_id = ' . $this->sqlItemId() . ','; } elseif( true == array_key_exists( 'ItemId', $this->getChangedColumns() ) ) { $strSql .= ' item_id = ' . $this->sqlItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; } elseif( true == array_key_exists( 'TransactionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; } elseif( true == array_key_exists( 'TransactionAmount', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_memo = ' . $this->sqlTransactionMemo() . ','; } elseif( true == array_key_exists( 'TransactionMemo', $this->getChangedColumns() ) ) { $strSql .= ' transaction_memo = ' . $this->sqlTransactionMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_code = ' . $this->sqlResponseCode() . ','; } elseif( true == array_key_exists( 'ResponseCode', $this->getChangedColumns() ) ) { $strSql .= ' response_code = ' . $this->sqlResponseCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_text = ' . $this->sqlResponseText() . ','; } elseif( true == array_key_exists( 'ResponseText', $this->getChangedColumns() ) ) { $strSql .= ' response_text = ' . $this->sqlResponseText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' raw_data = ' . $this->sqlRawData() . ','; } elseif( true == array_key_exists( 'RawData', $this->getChangedColumns() ) ) { $strSql .= ' raw_data = ' . $this->sqlRawData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; } elseif( true == array_key_exists( 'IsReconPrepped', $this->getChangedColumns() ) ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'eft_batch_id' => $this->getEftBatchId(),
			'item_id' => $this->getItemId(),
			'transaction_datetime' => $this->getTransactionDatetime(),
			'transaction_amount' => $this->getTransactionAmount(),
			'transaction_memo' => $this->getTransactionMemo(),
			'response_code' => $this->getResponseCode(),
			'response_text' => $this->getResponseText(),
			'raw_data' => $this->getRawData(),
			'is_recon_prepped' => $this->getIsReconPrepped(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>