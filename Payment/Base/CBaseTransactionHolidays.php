<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CTransactionHolidays
 * Do not add any new functions to this class.
 */

class CBaseTransactionHolidays extends CEosPluralBase {

	/**
	 * @return CTransactionHoliday[]
	 */
	public static function fetchTransactionHolidays( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTransactionHoliday::class, $objDatabase );
	}

	/**
	 * @return CTransactionHoliday
	 */
	public static function fetchTransactionHoliday( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTransactionHoliday::class, $objDatabase );
	}

	public static function fetchTransactionHolidayCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transaction_holidays', $objDatabase );
	}

	public static function fetchTransactionHolidayById( $intId, $objDatabase ) {
		return self::fetchTransactionHoliday( sprintf( 'SELECT * FROM transaction_holidays WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>