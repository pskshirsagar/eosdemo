<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CFundMovementTypes
 * Do not add any new functions to this class.
 */

class CBaseFundMovementTypes extends CEosPluralBase {

	/**
	 * @return CFundMovementType[]
	 */
	public static function fetchFundMovementTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CFundMovementType::class, $objDatabase );
	}

	/**
	 * @return CFundMovementType
	 */
	public static function fetchFundMovementType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFundMovementType::class, $objDatabase );
	}

	public static function fetchFundMovementTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'fund_movement_types', $objDatabase );
	}

	public static function fetchFundMovementTypeById( $intId, $objDatabase ) {
		return self::fetchFundMovementType( sprintf( 'SELECT * FROM fund_movement_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>