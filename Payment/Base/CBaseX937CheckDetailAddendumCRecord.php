<?php

class CBaseX937CheckDetailAddendumCRecord extends CEosSingularBase {

	const TABLE_NAME = 'public.x937_check_detail_addendum_c_records';

	protected $m_intId;
	protected $m_intX937CheckDetailRecordId;
	protected $m_strRecordType;
	protected $m_strCheckDetailAddendumCRecordNumber;
	protected $m_strEndorsingBankRoutingNumber;
	protected $m_strEndorsingBankEndorsementDate;
	protected $m_strEndorsingBankItemSequenceNumber;
	protected $m_strTruncationIndicator;
	protected $m_strEndorsingBankConversionIndicator;
	protected $m_strEndorsingBankCorrectionIndicator;
	protected $m_strReturnReason;
	protected $m_strUserField;
	protected $m_strReserved;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['x937_check_detail_record_id'] ) && $boolDirectSet ) $this->set( 'm_intX937CheckDetailRecordId', trim( $arrValues['x937_check_detail_record_id'] ) ); elseif( isset( $arrValues['x937_check_detail_record_id'] ) ) $this->setX937CheckDetailRecordId( $arrValues['x937_check_detail_record_id'] );
		if( isset( $arrValues['record_type'] ) && $boolDirectSet ) $this->set( 'm_strRecordType', trim( stripcslashes( $arrValues['record_type'] ) ) ); elseif( isset( $arrValues['record_type'] ) ) $this->setRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_type'] ) : $arrValues['record_type'] );
		if( isset( $arrValues['check_detail_addendum_c_record_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckDetailAddendumCRecordNumber', trim( stripcslashes( $arrValues['check_detail_addendum_c_record_number'] ) ) ); elseif( isset( $arrValues['check_detail_addendum_c_record_number'] ) ) $this->setCheckDetailAddendumCRecordNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_detail_addendum_c_record_number'] ) : $arrValues['check_detail_addendum_c_record_number'] );
		if( isset( $arrValues['endorsing_bank_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strEndorsingBankRoutingNumber', trim( stripcslashes( $arrValues['endorsing_bank_routing_number'] ) ) ); elseif( isset( $arrValues['endorsing_bank_routing_number'] ) ) $this->setEndorsingBankRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['endorsing_bank_routing_number'] ) : $arrValues['endorsing_bank_routing_number'] );
		if( isset( $arrValues['endorsing_bank_endorsement_date'] ) && $boolDirectSet ) $this->set( 'm_strEndorsingBankEndorsementDate', trim( stripcslashes( $arrValues['endorsing_bank_endorsement_date'] ) ) ); elseif( isset( $arrValues['endorsing_bank_endorsement_date'] ) ) $this->setEndorsingBankEndorsementDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['endorsing_bank_endorsement_date'] ) : $arrValues['endorsing_bank_endorsement_date'] );
		if( isset( $arrValues['endorsing_bank_item_sequence_number'] ) && $boolDirectSet ) $this->set( 'm_strEndorsingBankItemSequenceNumber', trim( stripcslashes( $arrValues['endorsing_bank_item_sequence_number'] ) ) ); elseif( isset( $arrValues['endorsing_bank_item_sequence_number'] ) ) $this->setEndorsingBankItemSequenceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['endorsing_bank_item_sequence_number'] ) : $arrValues['endorsing_bank_item_sequence_number'] );
		if( isset( $arrValues['truncation_indicator'] ) && $boolDirectSet ) $this->set( 'm_strTruncationIndicator', trim( stripcslashes( $arrValues['truncation_indicator'] ) ) ); elseif( isset( $arrValues['truncation_indicator'] ) ) $this->setTruncationIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['truncation_indicator'] ) : $arrValues['truncation_indicator'] );
		if( isset( $arrValues['endorsing_bank_conversion_indicator'] ) && $boolDirectSet ) $this->set( 'm_strEndorsingBankConversionIndicator', trim( stripcslashes( $arrValues['endorsing_bank_conversion_indicator'] ) ) ); elseif( isset( $arrValues['endorsing_bank_conversion_indicator'] ) ) $this->setEndorsingBankConversionIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['endorsing_bank_conversion_indicator'] ) : $arrValues['endorsing_bank_conversion_indicator'] );
		if( isset( $arrValues['endorsing_bank_correction_indicator'] ) && $boolDirectSet ) $this->set( 'm_strEndorsingBankCorrectionIndicator', trim( stripcslashes( $arrValues['endorsing_bank_correction_indicator'] ) ) ); elseif( isset( $arrValues['endorsing_bank_correction_indicator'] ) ) $this->setEndorsingBankCorrectionIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['endorsing_bank_correction_indicator'] ) : $arrValues['endorsing_bank_correction_indicator'] );
		if( isset( $arrValues['return_reason'] ) && $boolDirectSet ) $this->set( 'm_strReturnReason', trim( stripcslashes( $arrValues['return_reason'] ) ) ); elseif( isset( $arrValues['return_reason'] ) ) $this->setReturnReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['return_reason'] ) : $arrValues['return_reason'] );
		if( isset( $arrValues['user_field'] ) && $boolDirectSet ) $this->set( 'm_strUserField', trim( stripcslashes( $arrValues['user_field'] ) ) ); elseif( isset( $arrValues['user_field'] ) ) $this->setUserField( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['user_field'] ) : $arrValues['user_field'] );
		if( isset( $arrValues['reserved'] ) && $boolDirectSet ) $this->set( 'm_strReserved', trim( stripcslashes( $arrValues['reserved'] ) ) ); elseif( isset( $arrValues['reserved'] ) ) $this->setReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reserved'] ) : $arrValues['reserved'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setX937CheckDetailRecordId( $intX937CheckDetailRecordId ) {
		$this->set( 'm_intX937CheckDetailRecordId', CStrings::strToIntDef( $intX937CheckDetailRecordId, NULL, false ) );
	}

	public function getX937CheckDetailRecordId() {
		return $this->m_intX937CheckDetailRecordId;
	}

	public function sqlX937CheckDetailRecordId() {
		return ( true == isset( $this->m_intX937CheckDetailRecordId ) ) ? ( string ) $this->m_intX937CheckDetailRecordId : 'NULL';
	}

	public function setRecordType( $strRecordType ) {
		$this->set( 'm_strRecordType', CStrings::strTrimDef( $strRecordType, 2, NULL, true ) );
	}

	public function getRecordType() {
		return $this->m_strRecordType;
	}

	public function sqlRecordType() {
		return ( true == isset( $this->m_strRecordType ) ) ? '\'' . addslashes( $this->m_strRecordType ) . '\'' : 'NULL';
	}

	public function setCheckDetailAddendumCRecordNumber( $strCheckDetailAddendumCRecordNumber ) {
		$this->set( 'm_strCheckDetailAddendumCRecordNumber', CStrings::strTrimDef( $strCheckDetailAddendumCRecordNumber, 2, NULL, true ) );
	}

	public function getCheckDetailAddendumCRecordNumber() {
		return $this->m_strCheckDetailAddendumCRecordNumber;
	}

	public function sqlCheckDetailAddendumCRecordNumber() {
		return ( true == isset( $this->m_strCheckDetailAddendumCRecordNumber ) ) ? '\'' . addslashes( $this->m_strCheckDetailAddendumCRecordNumber ) . '\'' : 'NULL';
	}

	public function setEndorsingBankRoutingNumber( $strEndorsingBankRoutingNumber ) {
		$this->set( 'm_strEndorsingBankRoutingNumber', CStrings::strTrimDef( $strEndorsingBankRoutingNumber, 9, NULL, true ) );
	}

	public function getEndorsingBankRoutingNumber() {
		return $this->m_strEndorsingBankRoutingNumber;
	}

	public function sqlEndorsingBankRoutingNumber() {
		return ( true == isset( $this->m_strEndorsingBankRoutingNumber ) ) ? '\'' . addslashes( $this->m_strEndorsingBankRoutingNumber ) . '\'' : 'NULL';
	}

	public function setEndorsingBankEndorsementDate( $strEndorsingBankEndorsementDate ) {
		$this->set( 'm_strEndorsingBankEndorsementDate', CStrings::strTrimDef( $strEndorsingBankEndorsementDate, 8, NULL, true ) );
	}

	public function getEndorsingBankEndorsementDate() {
		return $this->m_strEndorsingBankEndorsementDate;
	}

	public function sqlEndorsingBankEndorsementDate() {
		return ( true == isset( $this->m_strEndorsingBankEndorsementDate ) ) ? '\'' . addslashes( $this->m_strEndorsingBankEndorsementDate ) . '\'' : 'NULL';
	}

	public function setEndorsingBankItemSequenceNumber( $strEndorsingBankItemSequenceNumber ) {
		$this->set( 'm_strEndorsingBankItemSequenceNumber', CStrings::strTrimDef( $strEndorsingBankItemSequenceNumber, 15, NULL, true ) );
	}

	public function getEndorsingBankItemSequenceNumber() {
		return $this->m_strEndorsingBankItemSequenceNumber;
	}

	public function sqlEndorsingBankItemSequenceNumber() {
		return ( true == isset( $this->m_strEndorsingBankItemSequenceNumber ) ) ? '\'' . addslashes( $this->m_strEndorsingBankItemSequenceNumber ) . '\'' : 'NULL';
	}

	public function setTruncationIndicator( $strTruncationIndicator ) {
		$this->set( 'm_strTruncationIndicator', CStrings::strTrimDef( $strTruncationIndicator, 1, NULL, true ) );
	}

	public function getTruncationIndicator() {
		return $this->m_strTruncationIndicator;
	}

	public function sqlTruncationIndicator() {
		return ( true == isset( $this->m_strTruncationIndicator ) ) ? '\'' . addslashes( $this->m_strTruncationIndicator ) . '\'' : 'NULL';
	}

	public function setEndorsingBankConversionIndicator( $strEndorsingBankConversionIndicator ) {
		$this->set( 'm_strEndorsingBankConversionIndicator', CStrings::strTrimDef( $strEndorsingBankConversionIndicator, 1, NULL, true ) );
	}

	public function getEndorsingBankConversionIndicator() {
		return $this->m_strEndorsingBankConversionIndicator;
	}

	public function sqlEndorsingBankConversionIndicator() {
		return ( true == isset( $this->m_strEndorsingBankConversionIndicator ) ) ? '\'' . addslashes( $this->m_strEndorsingBankConversionIndicator ) . '\'' : 'NULL';
	}

	public function setEndorsingBankCorrectionIndicator( $strEndorsingBankCorrectionIndicator ) {
		$this->set( 'm_strEndorsingBankCorrectionIndicator', CStrings::strTrimDef( $strEndorsingBankCorrectionIndicator, 1, NULL, true ) );
	}

	public function getEndorsingBankCorrectionIndicator() {
		return $this->m_strEndorsingBankCorrectionIndicator;
	}

	public function sqlEndorsingBankCorrectionIndicator() {
		return ( true == isset( $this->m_strEndorsingBankCorrectionIndicator ) ) ? '\'' . addslashes( $this->m_strEndorsingBankCorrectionIndicator ) . '\'' : 'NULL';
	}

	public function setReturnReason( $strReturnReason ) {
		$this->set( 'm_strReturnReason', CStrings::strTrimDef( $strReturnReason, 1, NULL, true ) );
	}

	public function getReturnReason() {
		return $this->m_strReturnReason;
	}

	public function sqlReturnReason() {
		return ( true == isset( $this->m_strReturnReason ) ) ? '\'' . addslashes( $this->m_strReturnReason ) . '\'' : 'NULL';
	}

	public function setUserField( $strUserField ) {
		$this->set( 'm_strUserField', CStrings::strTrimDef( $strUserField, 15, NULL, true ) );
	}

	public function getUserField() {
		return $this->m_strUserField;
	}

	public function sqlUserField() {
		return ( true == isset( $this->m_strUserField ) ) ? '\'' . addslashes( $this->m_strUserField ) . '\'' : 'NULL';
	}

	public function setReserved( $strReserved ) {
		$this->set( 'm_strReserved', CStrings::strTrimDef( $strReserved, 25, NULL, true ) );
	}

	public function getReserved() {
		return $this->m_strReserved;
	}

	public function sqlReserved() {
		return ( true == isset( $this->m_strReserved ) ) ? '\'' . addslashes( $this->m_strReserved ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, x937_check_detail_record_id, record_type, check_detail_addendum_c_record_number, endorsing_bank_routing_number, endorsing_bank_endorsement_date, endorsing_bank_item_sequence_number, truncation_indicator, endorsing_bank_conversion_indicator, endorsing_bank_correction_indicator, return_reason, user_field, reserved, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlX937CheckDetailRecordId() . ', ' .
 						$this->sqlRecordType() . ', ' .
 						$this->sqlCheckDetailAddendumCRecordNumber() . ', ' .
 						$this->sqlEndorsingBankRoutingNumber() . ', ' .
 						$this->sqlEndorsingBankEndorsementDate() . ', ' .
 						$this->sqlEndorsingBankItemSequenceNumber() . ', ' .
 						$this->sqlTruncationIndicator() . ', ' .
 						$this->sqlEndorsingBankConversionIndicator() . ', ' .
 						$this->sqlEndorsingBankCorrectionIndicator() . ', ' .
 						$this->sqlReturnReason() . ', ' .
 						$this->sqlUserField() . ', ' .
 						$this->sqlReserved() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_check_detail_record_id = ' . $this->sqlX937CheckDetailRecordId() . ','; } elseif( true == array_key_exists( 'X937CheckDetailRecordId', $this->getChangedColumns() ) ) { $strSql .= ' x937_check_detail_record_id = ' . $this->sqlX937CheckDetailRecordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; } elseif( true == array_key_exists( 'RecordType', $this->getChangedColumns() ) ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_detail_addendum_c_record_number = ' . $this->sqlCheckDetailAddendumCRecordNumber() . ','; } elseif( true == array_key_exists( 'CheckDetailAddendumCRecordNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_detail_addendum_c_record_number = ' . $this->sqlCheckDetailAddendumCRecordNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' endorsing_bank_routing_number = ' . $this->sqlEndorsingBankRoutingNumber() . ','; } elseif( true == array_key_exists( 'EndorsingBankRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' endorsing_bank_routing_number = ' . $this->sqlEndorsingBankRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' endorsing_bank_endorsement_date = ' . $this->sqlEndorsingBankEndorsementDate() . ','; } elseif( true == array_key_exists( 'EndorsingBankEndorsementDate', $this->getChangedColumns() ) ) { $strSql .= ' endorsing_bank_endorsement_date = ' . $this->sqlEndorsingBankEndorsementDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' endorsing_bank_item_sequence_number = ' . $this->sqlEndorsingBankItemSequenceNumber() . ','; } elseif( true == array_key_exists( 'EndorsingBankItemSequenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' endorsing_bank_item_sequence_number = ' . $this->sqlEndorsingBankItemSequenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' truncation_indicator = ' . $this->sqlTruncationIndicator() . ','; } elseif( true == array_key_exists( 'TruncationIndicator', $this->getChangedColumns() ) ) { $strSql .= ' truncation_indicator = ' . $this->sqlTruncationIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' endorsing_bank_conversion_indicator = ' . $this->sqlEndorsingBankConversionIndicator() . ','; } elseif( true == array_key_exists( 'EndorsingBankConversionIndicator', $this->getChangedColumns() ) ) { $strSql .= ' endorsing_bank_conversion_indicator = ' . $this->sqlEndorsingBankConversionIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' endorsing_bank_correction_indicator = ' . $this->sqlEndorsingBankCorrectionIndicator() . ','; } elseif( true == array_key_exists( 'EndorsingBankCorrectionIndicator', $this->getChangedColumns() ) ) { $strSql .= ' endorsing_bank_correction_indicator = ' . $this->sqlEndorsingBankCorrectionIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_reason = ' . $this->sqlReturnReason() . ','; } elseif( true == array_key_exists( 'ReturnReason', $this->getChangedColumns() ) ) { $strSql .= ' return_reason = ' . $this->sqlReturnReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_field = ' . $this->sqlUserField() . ','; } elseif( true == array_key_exists( 'UserField', $this->getChangedColumns() ) ) { $strSql .= ' user_field = ' . $this->sqlUserField() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reserved = ' . $this->sqlReserved() . ','; } elseif( true == array_key_exists( 'Reserved', $this->getChangedColumns() ) ) { $strSql .= ' reserved = ' . $this->sqlReserved() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'x937_check_detail_record_id' => $this->getX937CheckDetailRecordId(),
			'record_type' => $this->getRecordType(),
			'check_detail_addendum_c_record_number' => $this->getCheckDetailAddendumCRecordNumber(),
			'endorsing_bank_routing_number' => $this->getEndorsingBankRoutingNumber(),
			'endorsing_bank_endorsement_date' => $this->getEndorsingBankEndorsementDate(),
			'endorsing_bank_item_sequence_number' => $this->getEndorsingBankItemSequenceNumber(),
			'truncation_indicator' => $this->getTruncationIndicator(),
			'endorsing_bank_conversion_indicator' => $this->getEndorsingBankConversionIndicator(),
			'endorsing_bank_correction_indicator' => $this->getEndorsingBankCorrectionIndicator(),
			'return_reason' => $this->getReturnReason(),
			'user_field' => $this->getUserField(),
			'reserved' => $this->getReserved(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>