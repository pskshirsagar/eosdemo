<?php

class CBaseAftFile extends CEosSingularBase {

	const TABLE_NAME = 'public.aft_files';

	protected $m_intId;
	protected $m_intPaymentFileId;
	protected $m_intAftFileTypeId;
	protected $m_intProcessingBankId;
	protected $m_intMerchantGatewayId;
	protected $m_strOriginatorId;
	protected $m_strFileCreationNumber;
	protected $m_strFileCreationDate;
	protected $m_strDestinationDataCentre;
	protected $m_intTotalValueDebit;
	protected $m_intTotalValueCredit;
	protected $m_intTotalCountDebit;
	protected $m_intTotalCountCredit;
	protected $m_strTransmittedOn;
	protected $m_strConfirmedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strRejectedOn;
	protected $m_intConfirmationFileId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['payment_file_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentFileId', trim( $arrValues['payment_file_id'] ) ); elseif( isset( $arrValues['payment_file_id'] ) ) $this->setPaymentFileId( $arrValues['payment_file_id'] );
		if( isset( $arrValues['aft_file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAftFileTypeId', trim( $arrValues['aft_file_type_id'] ) ); elseif( isset( $arrValues['aft_file_type_id'] ) ) $this->setAftFileTypeId( $arrValues['aft_file_type_id'] );
		if( isset( $arrValues['processing_bank_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankId', trim( $arrValues['processing_bank_id'] ) ); elseif( isset( $arrValues['processing_bank_id'] ) ) $this->setProcessingBankId( $arrValues['processing_bank_id'] );
		if( isset( $arrValues['merchant_gateway_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantGatewayId', trim( $arrValues['merchant_gateway_id'] ) ); elseif( isset( $arrValues['merchant_gateway_id'] ) ) $this->setMerchantGatewayId( $arrValues['merchant_gateway_id'] );
		if( isset( $arrValues['originator_id'] ) && $boolDirectSet ) $this->set( 'm_strOriginatorId', trim( $arrValues['originator_id'] ) ); elseif( isset( $arrValues['originator_id'] ) ) $this->setOriginatorId( $arrValues['originator_id'] );
		if( isset( $arrValues['file_creation_number'] ) && $boolDirectSet ) $this->set( 'm_strFileCreationNumber', trim( $arrValues['file_creation_number'] ) ); elseif( isset( $arrValues['file_creation_number'] ) ) $this->setFileCreationNumber( $arrValues['file_creation_number'] );
		if( isset( $arrValues['file_creation_date'] ) && $boolDirectSet ) $this->set( 'm_strFileCreationDate', trim( $arrValues['file_creation_date'] ) ); elseif( isset( $arrValues['file_creation_date'] ) ) $this->setFileCreationDate( $arrValues['file_creation_date'] );
		if( isset( $arrValues['destination_data_centre'] ) && $boolDirectSet ) $this->set( 'm_strDestinationDataCentre', trim( $arrValues['destination_data_centre'] ) ); elseif( isset( $arrValues['destination_data_centre'] ) ) $this->setDestinationDataCentre( $arrValues['destination_data_centre'] );
		if( isset( $arrValues['total_value_debit'] ) && $boolDirectSet ) $this->set( 'm_intTotalValueDebit', trim( $arrValues['total_value_debit'] ) ); elseif( isset( $arrValues['total_value_debit'] ) ) $this->setTotalValueDebit( $arrValues['total_value_debit'] );
		if( isset( $arrValues['total_value_credit'] ) && $boolDirectSet ) $this->set( 'm_intTotalValueCredit', trim( $arrValues['total_value_credit'] ) ); elseif( isset( $arrValues['total_value_credit'] ) ) $this->setTotalValueCredit( $arrValues['total_value_credit'] );
		if( isset( $arrValues['total_count_debit'] ) && $boolDirectSet ) $this->set( 'm_intTotalCountDebit', trim( $arrValues['total_count_debit'] ) ); elseif( isset( $arrValues['total_count_debit'] ) ) $this->setTotalCountDebit( $arrValues['total_count_debit'] );
		if( isset( $arrValues['total_count_credit'] ) && $boolDirectSet ) $this->set( 'm_intTotalCountCredit', trim( $arrValues['total_count_credit'] ) ); elseif( isset( $arrValues['total_count_credit'] ) ) $this->setTotalCountCredit( $arrValues['total_count_credit'] );
		if( isset( $arrValues['transmitted_on'] ) && $boolDirectSet ) $this->set( 'm_strTransmittedOn', trim( $arrValues['transmitted_on'] ) ); elseif( isset( $arrValues['transmitted_on'] ) ) $this->setTransmittedOn( $arrValues['transmitted_on'] );
		if( isset( $arrValues['confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strConfirmedOn', trim( $arrValues['confirmed_on'] ) ); elseif( isset( $arrValues['confirmed_on'] ) ) $this->setConfirmedOn( $arrValues['confirmed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['rejected_on'] ) && $boolDirectSet ) $this->set( 'm_strRejectedOn', trim( $arrValues['rejected_on'] ) ); elseif( isset( $arrValues['rejected_on'] ) ) $this->setRejectedOn( $arrValues['rejected_on'] );
		if( isset( $arrValues['confirmation_file_id'] ) && $boolDirectSet ) $this->set( 'm_intConfirmationFileId', trim( $arrValues['confirmation_file_id'] ) ); elseif( isset( $arrValues['confirmation_file_id'] ) ) $this->setConfirmationFileId( $arrValues['confirmation_file_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPaymentFileId( $intPaymentFileId ) {
		$this->set( 'm_intPaymentFileId', CStrings::strToIntDef( $intPaymentFileId, NULL, false ) );
	}

	public function getPaymentFileId() {
		return $this->m_intPaymentFileId;
	}

	public function sqlPaymentFileId() {
		return ( true == isset( $this->m_intPaymentFileId ) ) ? ( string ) $this->m_intPaymentFileId : 'NULL';
	}

	public function setAftFileTypeId( $intAftFileTypeId ) {
		$this->set( 'm_intAftFileTypeId', CStrings::strToIntDef( $intAftFileTypeId, NULL, false ) );
	}

	public function getAftFileTypeId() {
		return $this->m_intAftFileTypeId;
	}

	public function sqlAftFileTypeId() {
		return ( true == isset( $this->m_intAftFileTypeId ) ) ? ( string ) $this->m_intAftFileTypeId : 'NULL';
	}

	public function setProcessingBankId( $intProcessingBankId ) {
		$this->set( 'm_intProcessingBankId', CStrings::strToIntDef( $intProcessingBankId, NULL, false ) );
	}

	public function getProcessingBankId() {
		return $this->m_intProcessingBankId;
	}

	public function sqlProcessingBankId() {
		return ( true == isset( $this->m_intProcessingBankId ) ) ? ( string ) $this->m_intProcessingBankId : 'NULL';
	}

	public function setMerchantGatewayId( $intMerchantGatewayId ) {
		$this->set( 'm_intMerchantGatewayId', CStrings::strToIntDef( $intMerchantGatewayId, NULL, false ) );
	}

	public function getMerchantGatewayId() {
		return $this->m_intMerchantGatewayId;
	}

	public function sqlMerchantGatewayId() {
		return ( true == isset( $this->m_intMerchantGatewayId ) ) ? ( string ) $this->m_intMerchantGatewayId : 'NULL';
	}

	public function setOriginatorId( $strOriginatorId ) {
		$this->set( 'm_strOriginatorId', CStrings::strTrimDef( $strOriginatorId, 10, NULL, true ) );
	}

	public function getOriginatorId() {
		return $this->m_strOriginatorId;
	}

	public function sqlOriginatorId() {
		return ( true == isset( $this->m_strOriginatorId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOriginatorId ) : '\'' . addslashes( $this->m_strOriginatorId ) . '\'' ) : 'NULL';
	}

	public function setFileCreationNumber( $strFileCreationNumber ) {
		$this->set( 'm_strFileCreationNumber', CStrings::strTrimDef( $strFileCreationNumber, 4, NULL, true ) );
	}

	public function getFileCreationNumber() {
		return $this->m_strFileCreationNumber;
	}

	public function sqlFileCreationNumber() {
		return ( true == isset( $this->m_strFileCreationNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileCreationNumber ) : '\'' . addslashes( $this->m_strFileCreationNumber ) . '\'' ) : 'NULL';
	}

	public function setFileCreationDate( $strFileCreationDate ) {
		$this->set( 'm_strFileCreationDate', CStrings::strTrimDef( $strFileCreationDate, 6, NULL, true ) );
	}

	public function getFileCreationDate() {
		return $this->m_strFileCreationDate;
	}

	public function sqlFileCreationDate() {
		return ( true == isset( $this->m_strFileCreationDate ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileCreationDate ) : '\'' . addslashes( $this->m_strFileCreationDate ) . '\'' ) : 'NULL';
	}

	public function setDestinationDataCentre( $strDestinationDataCentre ) {
		$this->set( 'm_strDestinationDataCentre', CStrings::strTrimDef( $strDestinationDataCentre, 5, NULL, true ) );
	}

	public function getDestinationDataCentre() {
		return $this->m_strDestinationDataCentre;
	}

	public function sqlDestinationDataCentre() {
		return ( true == isset( $this->m_strDestinationDataCentre ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDestinationDataCentre ) : '\'' . addslashes( $this->m_strDestinationDataCentre ) . '\'' ) : 'NULL';
	}

	public function setTotalValueDebit( $intTotalValueDebit ) {
		$this->set( 'm_intTotalValueDebit', CStrings::strToIntDef( $intTotalValueDebit, NULL, false ) );
	}

	public function getTotalValueDebit() {
		return $this->m_intTotalValueDebit;
	}

	public function sqlTotalValueDebit() {
		return ( true == isset( $this->m_intTotalValueDebit ) ) ? ( string ) $this->m_intTotalValueDebit : 'NULL';
	}

	public function setTotalValueCredit( $intTotalValueCredit ) {
		$this->set( 'm_intTotalValueCredit', CStrings::strToIntDef( $intTotalValueCredit, NULL, false ) );
	}

	public function getTotalValueCredit() {
		return $this->m_intTotalValueCredit;
	}

	public function sqlTotalValueCredit() {
		return ( true == isset( $this->m_intTotalValueCredit ) ) ? ( string ) $this->m_intTotalValueCredit : 'NULL';
	}

	public function setTotalCountDebit( $intTotalCountDebit ) {
		$this->set( 'm_intTotalCountDebit', CStrings::strToIntDef( $intTotalCountDebit, NULL, false ) );
	}

	public function getTotalCountDebit() {
		return $this->m_intTotalCountDebit;
	}

	public function sqlTotalCountDebit() {
		return ( true == isset( $this->m_intTotalCountDebit ) ) ? ( string ) $this->m_intTotalCountDebit : 'NULL';
	}

	public function setTotalCountCredit( $intTotalCountCredit ) {
		$this->set( 'm_intTotalCountCredit', CStrings::strToIntDef( $intTotalCountCredit, NULL, false ) );
	}

	public function getTotalCountCredit() {
		return $this->m_intTotalCountCredit;
	}

	public function sqlTotalCountCredit() {
		return ( true == isset( $this->m_intTotalCountCredit ) ) ? ( string ) $this->m_intTotalCountCredit : 'NULL';
	}

	public function setTransmittedOn( $strTransmittedOn ) {
		$this->set( 'm_strTransmittedOn', CStrings::strTrimDef( $strTransmittedOn, -1, NULL, true ) );
	}

	public function getTransmittedOn() {
		return $this->m_strTransmittedOn;
	}

	public function sqlTransmittedOn() {
		return ( true == isset( $this->m_strTransmittedOn ) ) ? '\'' . $this->m_strTransmittedOn . '\'' : 'NULL';
	}

	public function setConfirmedOn( $strConfirmedOn ) {
		$this->set( 'm_strConfirmedOn', CStrings::strTrimDef( $strConfirmedOn, -1, NULL, true ) );
	}

	public function getConfirmedOn() {
		return $this->m_strConfirmedOn;
	}

	public function sqlConfirmedOn() {
		return ( true == isset( $this->m_strConfirmedOn ) ) ? '\'' . $this->m_strConfirmedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setRejectedOn( $strRejectedOn ) {
		$this->set( 'm_strRejectedOn', CStrings::strTrimDef( $strRejectedOn, -1, NULL, true ) );
	}

	public function getRejectedOn() {
		return $this->m_strRejectedOn;
	}

	public function sqlRejectedOn() {
		return ( true == isset( $this->m_strRejectedOn ) ) ? '\'' . $this->m_strRejectedOn . '\'' : 'NULL';
	}

	public function setConfirmationFileId( $intConfirmationFileId ) {
		$this->set( 'm_intConfirmationFileId', CStrings::strToIntDef( $intConfirmationFileId, NULL, false ) );
	}

	public function getConfirmationFileId() {
		return $this->m_intConfirmationFileId;
	}

	public function sqlConfirmationFileId() {
		return ( true == isset( $this->m_intConfirmationFileId ) ) ? ( string ) $this->m_intConfirmationFileId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, payment_file_id, aft_file_type_id, processing_bank_id, merchant_gateway_id, originator_id, file_creation_number, file_creation_date, destination_data_centre, total_value_debit, total_value_credit, total_count_debit, total_count_credit, transmitted_on, confirmed_on, updated_by, updated_on, created_by, created_on, rejected_on, confirmation_file_id )
					VALUES ( ' .
			$strId . ', ' .
			$this->sqlPaymentFileId() . ', ' .
			$this->sqlAftFileTypeId() . ', ' .
			$this->sqlProcessingBankId() . ', ' .
			$this->sqlMerchantGatewayId() . ', ' .
			$this->sqlOriginatorId() . ', ' .
			$this->sqlFileCreationNumber() . ', ' .
			$this->sqlFileCreationDate() . ', ' .
			$this->sqlDestinationDataCentre() . ', ' .
			$this->sqlTotalValueDebit() . ', ' .
			$this->sqlTotalValueCredit() . ', ' .
			$this->sqlTotalCountDebit() . ', ' .
			$this->sqlTotalCountCredit() . ', ' .
			$this->sqlTransmittedOn() . ', ' .
			$this->sqlConfirmedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlUpdatedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlCreatedOn() . ', ' .
			$this->sqlRejectedOn() . ', ' .
			$this->sqlConfirmationFileId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_file_id = ' . $this->sqlPaymentFileId(). ',' ; } elseif( true == array_key_exists( 'PaymentFileId', $this->getChangedColumns() ) ) { $strSql .= ' payment_file_id = ' . $this->sqlPaymentFileId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' aft_file_type_id = ' . $this->sqlAftFileTypeId(). ',' ; } elseif( true == array_key_exists( 'AftFileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' aft_file_type_id = ' . $this->sqlAftFileTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId(). ',' ; } elseif( true == array_key_exists( 'ProcessingBankId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId(). ',' ; } elseif( true == array_key_exists( 'MerchantGatewayId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' originator_id = ' . $this->sqlOriginatorId(). ',' ; } elseif( true == array_key_exists( 'OriginatorId', $this->getChangedColumns() ) ) { $strSql .= ' originator_id = ' . $this->sqlOriginatorId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_creation_number = ' . $this->sqlFileCreationNumber(). ',' ; } elseif( true == array_key_exists( 'FileCreationNumber', $this->getChangedColumns() ) ) { $strSql .= ' file_creation_number = ' . $this->sqlFileCreationNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_creation_date = ' . $this->sqlFileCreationDate(). ',' ; } elseif( true == array_key_exists( 'FileCreationDate', $this->getChangedColumns() ) ) { $strSql .= ' file_creation_date = ' . $this->sqlFileCreationDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_data_centre = ' . $this->sqlDestinationDataCentre(). ',' ; } elseif( true == array_key_exists( 'DestinationDataCentre', $this->getChangedColumns() ) ) { $strSql .= ' destination_data_centre = ' . $this->sqlDestinationDataCentre() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_value_debit = ' . $this->sqlTotalValueDebit(). ',' ; } elseif( true == array_key_exists( 'TotalValueDebit', $this->getChangedColumns() ) ) { $strSql .= ' total_value_debit = ' . $this->sqlTotalValueDebit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_value_credit = ' . $this->sqlTotalValueCredit(). ',' ; } elseif( true == array_key_exists( 'TotalValueCredit', $this->getChangedColumns() ) ) { $strSql .= ' total_value_credit = ' . $this->sqlTotalValueCredit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_count_debit = ' . $this->sqlTotalCountDebit(). ',' ; } elseif( true == array_key_exists( 'TotalCountDebit', $this->getChangedColumns() ) ) { $strSql .= ' total_count_debit = ' . $this->sqlTotalCountDebit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_count_credit = ' . $this->sqlTotalCountCredit(). ',' ; } elseif( true == array_key_exists( 'TotalCountCredit', $this->getChangedColumns() ) ) { $strSql .= ' total_count_credit = ' . $this->sqlTotalCountCredit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmitted_on = ' . $this->sqlTransmittedOn(). ',' ; } elseif( true == array_key_exists( 'TransmittedOn', $this->getChangedColumns() ) ) { $strSql .= ' transmitted_on = ' . $this->sqlTransmittedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn(). ',' ; } elseif( true == array_key_exists( 'ConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn(). ',' ; } elseif( true == array_key_exists( 'RejectedOn', $this->getChangedColumns() ) ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmation_file_id = ' . $this->sqlConfirmationFileId(). ',' ; } elseif( true == array_key_exists( 'ConfirmationFileId', $this->getChangedColumns() ) ) { $strSql .= ' confirmation_file_id = ' . $this->sqlConfirmationFileId() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'payment_file_id' => $this->getPaymentFileId(),
			'aft_file_type_id' => $this->getAftFileTypeId(),
			'processing_bank_id' => $this->getProcessingBankId(),
			'merchant_gateway_id' => $this->getMerchantGatewayId(),
			'originator_id' => $this->getOriginatorId(),
			'file_creation_number' => $this->getFileCreationNumber(),
			'file_creation_date' => $this->getFileCreationDate(),
			'destination_data_centre' => $this->getDestinationDataCentre(),
			'total_value_debit' => $this->getTotalValueDebit(),
			'total_value_credit' => $this->getTotalValueCredit(),
			'total_count_debit' => $this->getTotalCountDebit(),
			'total_count_credit' => $this->getTotalCountCredit(),
			'transmitted_on' => $this->getTransmittedOn(),
			'confirmed_on' => $this->getConfirmedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'rejected_on' => $this->getRejectedOn(),
			'confirmation_file_id' => $this->getConfirmationFileId()
		);
	}

}
?>