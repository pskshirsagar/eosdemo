<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\COfacNames
 * Do not add any new functions to this class.
 */

class CBaseOfacNames extends CEosPluralBase {

	/**
	 * @return COfacName[]
	 */
	public static function fetchOfacNames( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, COfacName::class, $objDatabase );
	}

	/**
	 * @return COfacName
	 */
	public static function fetchOfacName( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COfacName::class, $objDatabase );
	}

	public static function fetchOfacNameCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ofac_names', $objDatabase );
	}

	public static function fetchOfacNameById( $intId, $objDatabase ) {
		return self::fetchOfacName( sprintf( 'SELECT * FROM ofac_names WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchOfacNamesByOfacId( $intOfacId, $objDatabase ) {
		return self::fetchOfacNames( sprintf( 'SELECT * FROM ofac_names WHERE ofac_id = %d', ( int ) $intOfacId ), $objDatabase );
	}

}
?>