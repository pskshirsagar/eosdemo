<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937CheckDetailAddendumCRecords
 * Do not add any new functions to this class.
 */

class CBaseX937CheckDetailAddendumCRecords extends CEosPluralBase {

	/**
	 * @return CX937CheckDetailAddendumCRecord[]
	 */
	public static function fetchX937CheckDetailAddendumCRecords( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CX937CheckDetailAddendumCRecord::class, $objDatabase );
	}

	/**
	 * @return CX937CheckDetailAddendumCRecord
	 */
	public static function fetchX937CheckDetailAddendumCRecord( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CX937CheckDetailAddendumCRecord::class, $objDatabase );
	}

	public static function fetchX937CheckDetailAddendumCRecordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'x937_check_detail_addendum_c_records', $objDatabase );
	}

	public static function fetchX937CheckDetailAddendumCRecordById( $intId, $objDatabase ) {
		return self::fetchX937CheckDetailAddendumCRecord( sprintf( 'SELECT * FROM x937_check_detail_addendum_c_records WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchX937CheckDetailAddendumCRecordsByX937CheckDetailRecordId( $intX937CheckDetailRecordId, $objDatabase ) {
		return self::fetchX937CheckDetailAddendumCRecords( sprintf( 'SELECT * FROM x937_check_detail_addendum_c_records WHERE x937_check_detail_record_id = %d', ( int ) $intX937CheckDetailRecordId ), $objDatabase );
	}

}
?>