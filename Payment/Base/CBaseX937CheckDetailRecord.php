<?php

class CBaseX937CheckDetailRecord extends CEosSingularBase {

	const TABLE_NAME = 'public.x937_check_detail_records';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intProcessingBankAccountId;
	protected $m_intArPaymentId;
	protected $m_intSettlementDistributionId;
	protected $m_intCompanyPaymentId;
	protected $m_intX937FileId;
	protected $m_intX937CashLetterId;
	protected $m_intX937BundleId;
	protected $m_intReturnTypeId;
	protected $m_strRecordType;
	protected $m_strAuxiliaryOnUs;
	protected $m_strExternalProcessingCode;
	protected $m_strPayorBankRoutingNumber;
	protected $m_strPayorBankRoutingNumberCheckDigit;
	protected $m_strOnUs;
	protected $m_strItemAmount;
	protected $m_strEceInstitutionItemSequenceNumber;
	protected $m_strDocumentationTypeIndicator;
	protected $m_strReturnAcceptanceIndicator;
	protected $m_strMicrValidIndicator;
	protected $m_strBofdIndicator;
	protected $m_strAddendumCount;
	protected $m_strCorrectionIndicator;
	protected $m_strArchiveTypeIndicator;
	protected $m_strTransferredToIntermediaryOn;
	protected $m_strReturnedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankAccountId', trim( $arrValues['processing_bank_account_id'] ) ); elseif( isset( $arrValues['processing_bank_account_id'] ) ) $this->setProcessingBankAccountId( $arrValues['processing_bank_account_id'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['settlement_distribution_id'] ) && $boolDirectSet ) $this->set( 'm_intSettlementDistributionId', trim( $arrValues['settlement_distribution_id'] ) ); elseif( isset( $arrValues['settlement_distribution_id'] ) ) $this->setSettlementDistributionId( $arrValues['settlement_distribution_id'] );
		if( isset( $arrValues['company_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyPaymentId', trim( $arrValues['company_payment_id'] ) ); elseif( isset( $arrValues['company_payment_id'] ) ) $this->setCompanyPaymentId( $arrValues['company_payment_id'] );
		if( isset( $arrValues['x937_file_id'] ) && $boolDirectSet ) $this->set( 'm_intX937FileId', trim( $arrValues['x937_file_id'] ) ); elseif( isset( $arrValues['x937_file_id'] ) ) $this->setX937FileId( $arrValues['x937_file_id'] );
		if( isset( $arrValues['x937_cash_letter_id'] ) && $boolDirectSet ) $this->set( 'm_intX937CashLetterId', trim( $arrValues['x937_cash_letter_id'] ) ); elseif( isset( $arrValues['x937_cash_letter_id'] ) ) $this->setX937CashLetterId( $arrValues['x937_cash_letter_id'] );
		if( isset( $arrValues['x937_bundle_id'] ) && $boolDirectSet ) $this->set( 'm_intX937BundleId', trim( $arrValues['x937_bundle_id'] ) ); elseif( isset( $arrValues['x937_bundle_id'] ) ) $this->setX937BundleId( $arrValues['x937_bundle_id'] );
		if( isset( $arrValues['return_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReturnTypeId', trim( $arrValues['return_type_id'] ) ); elseif( isset( $arrValues['return_type_id'] ) ) $this->setReturnTypeId( $arrValues['return_type_id'] );
		if( isset( $arrValues['record_type'] ) && $boolDirectSet ) $this->set( 'm_strRecordType', trim( stripcslashes( $arrValues['record_type'] ) ) ); elseif( isset( $arrValues['record_type'] ) ) $this->setRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_type'] ) : $arrValues['record_type'] );
		if( isset( $arrValues['auxiliary_on_us'] ) && $boolDirectSet ) $this->set( 'm_strAuxiliaryOnUs', trim( stripcslashes( $arrValues['auxiliary_on_us'] ) ) ); elseif( isset( $arrValues['auxiliary_on_us'] ) ) $this->setAuxiliaryOnUs( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auxiliary_on_us'] ) : $arrValues['auxiliary_on_us'] );
		if( isset( $arrValues['external_processing_code'] ) && $boolDirectSet ) $this->set( 'm_strExternalProcessingCode', trim( stripcslashes( $arrValues['external_processing_code'] ) ) ); elseif( isset( $arrValues['external_processing_code'] ) ) $this->setExternalProcessingCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['external_processing_code'] ) : $arrValues['external_processing_code'] );
		if( isset( $arrValues['payor_bank_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strPayorBankRoutingNumber', trim( stripcslashes( $arrValues['payor_bank_routing_number'] ) ) ); elseif( isset( $arrValues['payor_bank_routing_number'] ) ) $this->setPayorBankRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payor_bank_routing_number'] ) : $arrValues['payor_bank_routing_number'] );
		if( isset( $arrValues['payor_bank_routing_number_check_digit'] ) && $boolDirectSet ) $this->set( 'm_strPayorBankRoutingNumberCheckDigit', trim( stripcslashes( $arrValues['payor_bank_routing_number_check_digit'] ) ) ); elseif( isset( $arrValues['payor_bank_routing_number_check_digit'] ) ) $this->setPayorBankRoutingNumberCheckDigit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payor_bank_routing_number_check_digit'] ) : $arrValues['payor_bank_routing_number_check_digit'] );
		if( isset( $arrValues['on_us'] ) && $boolDirectSet ) $this->set( 'm_strOnUs', trim( stripcslashes( $arrValues['on_us'] ) ) ); elseif( isset( $arrValues['on_us'] ) ) $this->setOnUs( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['on_us'] ) : $arrValues['on_us'] );
		if( isset( $arrValues['item_amount'] ) && $boolDirectSet ) $this->set( 'm_strItemAmount', trim( stripcslashes( $arrValues['item_amount'] ) ) ); elseif( isset( $arrValues['item_amount'] ) ) $this->setItemAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['item_amount'] ) : $arrValues['item_amount'] );
		if( isset( $arrValues['ece_institution_item_sequence_number'] ) && $boolDirectSet ) $this->set( 'm_strEceInstitutionItemSequenceNumber', trim( stripcslashes( $arrValues['ece_institution_item_sequence_number'] ) ) ); elseif( isset( $arrValues['ece_institution_item_sequence_number'] ) ) $this->setEceInstitutionItemSequenceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ece_institution_item_sequence_number'] ) : $arrValues['ece_institution_item_sequence_number'] );
		if( isset( $arrValues['documentation_type_indicator'] ) && $boolDirectSet ) $this->set( 'm_strDocumentationTypeIndicator', trim( stripcslashes( $arrValues['documentation_type_indicator'] ) ) ); elseif( isset( $arrValues['documentation_type_indicator'] ) ) $this->setDocumentationTypeIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['documentation_type_indicator'] ) : $arrValues['documentation_type_indicator'] );
		if( isset( $arrValues['return_acceptance_indicator'] ) && $boolDirectSet ) $this->set( 'm_strReturnAcceptanceIndicator', trim( stripcslashes( $arrValues['return_acceptance_indicator'] ) ) ); elseif( isset( $arrValues['return_acceptance_indicator'] ) ) $this->setReturnAcceptanceIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['return_acceptance_indicator'] ) : $arrValues['return_acceptance_indicator'] );
		if( isset( $arrValues['micr_valid_indicator'] ) && $boolDirectSet ) $this->set( 'm_strMicrValidIndicator', trim( stripcslashes( $arrValues['micr_valid_indicator'] ) ) ); elseif( isset( $arrValues['micr_valid_indicator'] ) ) $this->setMicrValidIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['micr_valid_indicator'] ) : $arrValues['micr_valid_indicator'] );
		if( isset( $arrValues['bofd_indicator'] ) && $boolDirectSet ) $this->set( 'm_strBofdIndicator', trim( stripcslashes( $arrValues['bofd_indicator'] ) ) ); elseif( isset( $arrValues['bofd_indicator'] ) ) $this->setBofdIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bofd_indicator'] ) : $arrValues['bofd_indicator'] );
		if( isset( $arrValues['addendum_count'] ) && $boolDirectSet ) $this->set( 'm_strAddendumCount', trim( stripcslashes( $arrValues['addendum_count'] ) ) ); elseif( isset( $arrValues['addendum_count'] ) ) $this->setAddendumCount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['addendum_count'] ) : $arrValues['addendum_count'] );
		if( isset( $arrValues['correction_indicator'] ) && $boolDirectSet ) $this->set( 'm_strCorrectionIndicator', trim( stripcslashes( $arrValues['correction_indicator'] ) ) ); elseif( isset( $arrValues['correction_indicator'] ) ) $this->setCorrectionIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['correction_indicator'] ) : $arrValues['correction_indicator'] );
		if( isset( $arrValues['archive_type_indicator'] ) && $boolDirectSet ) $this->set( 'm_strArchiveTypeIndicator', trim( stripcslashes( $arrValues['archive_type_indicator'] ) ) ); elseif( isset( $arrValues['archive_type_indicator'] ) ) $this->setArchiveTypeIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['archive_type_indicator'] ) : $arrValues['archive_type_indicator'] );
		if( isset( $arrValues['transferred_to_intermediary_on'] ) && $boolDirectSet ) $this->set( 'm_strTransferredToIntermediaryOn', trim( $arrValues['transferred_to_intermediary_on'] ) ); elseif( isset( $arrValues['transferred_to_intermediary_on'] ) ) $this->setTransferredToIntermediaryOn( $arrValues['transferred_to_intermediary_on'] );
		if( isset( $arrValues['returned_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnedOn', trim( $arrValues['returned_on'] ) ); elseif( isset( $arrValues['returned_on'] ) ) $this->setReturnedOn( $arrValues['returned_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
		$this->set( 'm_intProcessingBankAccountId', CStrings::strToIntDef( $intProcessingBankAccountId, NULL, false ) );
	}

	public function getProcessingBankAccountId() {
		return $this->m_intProcessingBankAccountId;
	}

	public function sqlProcessingBankAccountId() {
		return ( true == isset( $this->m_intProcessingBankAccountId ) ) ? ( string ) $this->m_intProcessingBankAccountId : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setSettlementDistributionId( $intSettlementDistributionId ) {
		$this->set( 'm_intSettlementDistributionId', CStrings::strToIntDef( $intSettlementDistributionId, NULL, false ) );
	}

	public function getSettlementDistributionId() {
		return $this->m_intSettlementDistributionId;
	}

	public function sqlSettlementDistributionId() {
		return ( true == isset( $this->m_intSettlementDistributionId ) ) ? ( string ) $this->m_intSettlementDistributionId : 'NULL';
	}

	public function setCompanyPaymentId( $intCompanyPaymentId ) {
		$this->set( 'm_intCompanyPaymentId', CStrings::strToIntDef( $intCompanyPaymentId, NULL, false ) );
	}

	public function getCompanyPaymentId() {
		return $this->m_intCompanyPaymentId;
	}

	public function sqlCompanyPaymentId() {
		return ( true == isset( $this->m_intCompanyPaymentId ) ) ? ( string ) $this->m_intCompanyPaymentId : 'NULL';
	}

	public function setX937FileId( $intX937FileId ) {
		$this->set( 'm_intX937FileId', CStrings::strToIntDef( $intX937FileId, NULL, false ) );
	}

	public function getX937FileId() {
		return $this->m_intX937FileId;
	}

	public function sqlX937FileId() {
		return ( true == isset( $this->m_intX937FileId ) ) ? ( string ) $this->m_intX937FileId : 'NULL';
	}

	public function setX937CashLetterId( $intX937CashLetterId ) {
		$this->set( 'm_intX937CashLetterId', CStrings::strToIntDef( $intX937CashLetterId, NULL, false ) );
	}

	public function getX937CashLetterId() {
		return $this->m_intX937CashLetterId;
	}

	public function sqlX937CashLetterId() {
		return ( true == isset( $this->m_intX937CashLetterId ) ) ? ( string ) $this->m_intX937CashLetterId : 'NULL';
	}

	public function setX937BundleId( $intX937BundleId ) {
		$this->set( 'm_intX937BundleId', CStrings::strToIntDef( $intX937BundleId, NULL, false ) );
	}

	public function getX937BundleId() {
		return $this->m_intX937BundleId;
	}

	public function sqlX937BundleId() {
		return ( true == isset( $this->m_intX937BundleId ) ) ? ( string ) $this->m_intX937BundleId : 'NULL';
	}

	public function setReturnTypeId( $intReturnTypeId ) {
		$this->set( 'm_intReturnTypeId', CStrings::strToIntDef( $intReturnTypeId, NULL, false ) );
	}

	public function getReturnTypeId() {
		return $this->m_intReturnTypeId;
	}

	public function sqlReturnTypeId() {
		return ( true == isset( $this->m_intReturnTypeId ) ) ? ( string ) $this->m_intReturnTypeId : 'NULL';
	}

	public function setRecordType( $strRecordType ) {
		$this->set( 'm_strRecordType', CStrings::strTrimDef( $strRecordType, 2, NULL, true ) );
	}

	public function getRecordType() {
		return $this->m_strRecordType;
	}

	public function sqlRecordType() {
		return ( true == isset( $this->m_strRecordType ) ) ? '\'' . addslashes( $this->m_strRecordType ) . '\'' : 'NULL';
	}

	public function setAuxiliaryOnUs( $strAuxiliaryOnUs ) {
		$this->set( 'm_strAuxiliaryOnUs', CStrings::strTrimDef( $strAuxiliaryOnUs, 15, NULL, true ) );
	}

	public function getAuxiliaryOnUs() {
		return $this->m_strAuxiliaryOnUs;
	}

	public function sqlAuxiliaryOnUs() {
		return ( true == isset( $this->m_strAuxiliaryOnUs ) ) ? '\'' . addslashes( $this->m_strAuxiliaryOnUs ) . '\'' : 'NULL';
	}

	public function setExternalProcessingCode( $strExternalProcessingCode ) {
		$this->set( 'm_strExternalProcessingCode', CStrings::strTrimDef( $strExternalProcessingCode, 1, NULL, true ) );
	}

	public function getExternalProcessingCode() {
		return $this->m_strExternalProcessingCode;
	}

	public function sqlExternalProcessingCode() {
		return ( true == isset( $this->m_strExternalProcessingCode ) ) ? '\'' . addslashes( $this->m_strExternalProcessingCode ) . '\'' : 'NULL';
	}

	public function setPayorBankRoutingNumber( $strPayorBankRoutingNumber ) {
		$this->set( 'm_strPayorBankRoutingNumber', CStrings::strTrimDef( $strPayorBankRoutingNumber, 8, NULL, true ) );
	}

	public function getPayorBankRoutingNumber() {
		return $this->m_strPayorBankRoutingNumber;
	}

	public function sqlPayorBankRoutingNumber() {
		return ( true == isset( $this->m_strPayorBankRoutingNumber ) ) ? '\'' . addslashes( $this->m_strPayorBankRoutingNumber ) . '\'' : 'NULL';
	}

	public function setPayorBankRoutingNumberCheckDigit( $strPayorBankRoutingNumberCheckDigit ) {
		$this->set( 'm_strPayorBankRoutingNumberCheckDigit', CStrings::strTrimDef( $strPayorBankRoutingNumberCheckDigit, 1, NULL, true ) );
	}

	public function getPayorBankRoutingNumberCheckDigit() {
		return $this->m_strPayorBankRoutingNumberCheckDigit;
	}

	public function sqlPayorBankRoutingNumberCheckDigit() {
		return ( true == isset( $this->m_strPayorBankRoutingNumberCheckDigit ) ) ? '\'' . addslashes( $this->m_strPayorBankRoutingNumberCheckDigit ) . '\'' : 'NULL';
	}

	public function setOnUs( $strOnUs ) {
		$this->set( 'm_strOnUs', CStrings::strTrimDef( $strOnUs, 20, NULL, true ) );
	}

	public function getOnUs() {
		return $this->m_strOnUs;
	}

	public function sqlOnUs() {
		return ( true == isset( $this->m_strOnUs ) ) ? '\'' . addslashes( $this->m_strOnUs ) . '\'' : 'NULL';
	}

	public function setItemAmount( $strItemAmount ) {
		$this->set( 'm_strItemAmount', CStrings::strTrimDef( $strItemAmount, 10, NULL, true ) );
	}

	public function getItemAmount() {
		return $this->m_strItemAmount;
	}

	public function sqlItemAmount() {
		return ( true == isset( $this->m_strItemAmount ) ) ? '\'' . addslashes( $this->m_strItemAmount ) . '\'' : 'NULL';
	}

	public function setEceInstitutionItemSequenceNumber( $strEceInstitutionItemSequenceNumber ) {
		$this->set( 'm_strEceInstitutionItemSequenceNumber', CStrings::strTrimDef( $strEceInstitutionItemSequenceNumber, 15, NULL, true ) );
	}

	public function getEceInstitutionItemSequenceNumber() {
		return $this->m_strEceInstitutionItemSequenceNumber;
	}

	public function sqlEceInstitutionItemSequenceNumber() {
		return ( true == isset( $this->m_strEceInstitutionItemSequenceNumber ) ) ? '\'' . addslashes( $this->m_strEceInstitutionItemSequenceNumber ) . '\'' : 'NULL';
	}

	public function setDocumentationTypeIndicator( $strDocumentationTypeIndicator ) {
		$this->set( 'm_strDocumentationTypeIndicator', CStrings::strTrimDef( $strDocumentationTypeIndicator, 1, NULL, true ) );
	}

	public function getDocumentationTypeIndicator() {
		return $this->m_strDocumentationTypeIndicator;
	}

	public function sqlDocumentationTypeIndicator() {
		return ( true == isset( $this->m_strDocumentationTypeIndicator ) ) ? '\'' . addslashes( $this->m_strDocumentationTypeIndicator ) . '\'' : 'NULL';
	}

	public function setReturnAcceptanceIndicator( $strReturnAcceptanceIndicator ) {
		$this->set( 'm_strReturnAcceptanceIndicator', CStrings::strTrimDef( $strReturnAcceptanceIndicator, 1, NULL, true ) );
	}

	public function getReturnAcceptanceIndicator() {
		return $this->m_strReturnAcceptanceIndicator;
	}

	public function sqlReturnAcceptanceIndicator() {
		return ( true == isset( $this->m_strReturnAcceptanceIndicator ) ) ? '\'' . addslashes( $this->m_strReturnAcceptanceIndicator ) . '\'' : 'NULL';
	}

	public function setMicrValidIndicator( $strMicrValidIndicator ) {
		$this->set( 'm_strMicrValidIndicator', CStrings::strTrimDef( $strMicrValidIndicator, 1, NULL, true ) );
	}

	public function getMicrValidIndicator() {
		return $this->m_strMicrValidIndicator;
	}

	public function sqlMicrValidIndicator() {
		return ( true == isset( $this->m_strMicrValidIndicator ) ) ? '\'' . addslashes( $this->m_strMicrValidIndicator ) . '\'' : 'NULL';
	}

	public function setBofdIndicator( $strBofdIndicator ) {
		$this->set( 'm_strBofdIndicator', CStrings::strTrimDef( $strBofdIndicator, 1, NULL, true ) );
	}

	public function getBofdIndicator() {
		return $this->m_strBofdIndicator;
	}

	public function sqlBofdIndicator() {
		return ( true == isset( $this->m_strBofdIndicator ) ) ? '\'' . addslashes( $this->m_strBofdIndicator ) . '\'' : 'NULL';
	}

	public function setAddendumCount( $strAddendumCount ) {
		$this->set( 'm_strAddendumCount', CStrings::strTrimDef( $strAddendumCount, 2, NULL, true ) );
	}

	public function getAddendumCount() {
		return $this->m_strAddendumCount;
	}

	public function sqlAddendumCount() {
		return ( true == isset( $this->m_strAddendumCount ) ) ? '\'' . addslashes( $this->m_strAddendumCount ) . '\'' : 'NULL';
	}

	public function setCorrectionIndicator( $strCorrectionIndicator ) {
		$this->set( 'm_strCorrectionIndicator', CStrings::strTrimDef( $strCorrectionIndicator, 1, NULL, true ) );
	}

	public function getCorrectionIndicator() {
		return $this->m_strCorrectionIndicator;
	}

	public function sqlCorrectionIndicator() {
		return ( true == isset( $this->m_strCorrectionIndicator ) ) ? '\'' . addslashes( $this->m_strCorrectionIndicator ) . '\'' : 'NULL';
	}

	public function setArchiveTypeIndicator( $strArchiveTypeIndicator ) {
		$this->set( 'm_strArchiveTypeIndicator', CStrings::strTrimDef( $strArchiveTypeIndicator, 1, NULL, true ) );
	}

	public function getArchiveTypeIndicator() {
		return $this->m_strArchiveTypeIndicator;
	}

	public function sqlArchiveTypeIndicator() {
		return ( true == isset( $this->m_strArchiveTypeIndicator ) ) ? '\'' . addslashes( $this->m_strArchiveTypeIndicator ) . '\'' : 'NULL';
	}

	public function setTransferredToIntermediaryOn( $strTransferredToIntermediaryOn ) {
		$this->set( 'm_strTransferredToIntermediaryOn', CStrings::strTrimDef( $strTransferredToIntermediaryOn, -1, NULL, true ) );
	}

	public function getTransferredToIntermediaryOn() {
		return $this->m_strTransferredToIntermediaryOn;
	}

	public function sqlTransferredToIntermediaryOn() {
		return ( true == isset( $this->m_strTransferredToIntermediaryOn ) ) ? '\'' . $this->m_strTransferredToIntermediaryOn . '\'' : 'NULL';
	}

	public function setReturnedOn( $strReturnedOn ) {
		$this->set( 'm_strReturnedOn', CStrings::strTrimDef( $strReturnedOn, -1, NULL, true ) );
	}

	public function getReturnedOn() {
		return $this->m_strReturnedOn;
	}

	public function sqlReturnedOn() {
		return ( true == isset( $this->m_strReturnedOn ) ) ? '\'' . $this->m_strReturnedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, processing_bank_account_id, ar_payment_id, settlement_distribution_id, company_payment_id, x937_file_id, x937_cash_letter_id, x937_bundle_id, return_type_id, record_type, auxiliary_on_us, external_processing_code, payor_bank_routing_number, payor_bank_routing_number_check_digit, on_us, item_amount, ece_institution_item_sequence_number, documentation_type_indicator, return_acceptance_indicator, micr_valid_indicator, bofd_indicator, addendum_count, correction_indicator, archive_type_indicator, transferred_to_intermediary_on, returned_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlProcessingBankAccountId() . ', ' .
 						$this->sqlArPaymentId() . ', ' .
 						$this->sqlSettlementDistributionId() . ', ' .
 						$this->sqlCompanyPaymentId() . ', ' .
 						$this->sqlX937FileId() . ', ' .
 						$this->sqlX937CashLetterId() . ', ' .
 						$this->sqlX937BundleId() . ', ' .
 						$this->sqlReturnTypeId() . ', ' .
 						$this->sqlRecordType() . ', ' .
 						$this->sqlAuxiliaryOnUs() . ', ' .
 						$this->sqlExternalProcessingCode() . ', ' .
 						$this->sqlPayorBankRoutingNumber() . ', ' .
 						$this->sqlPayorBankRoutingNumberCheckDigit() . ', ' .
 						$this->sqlOnUs() . ', ' .
 						$this->sqlItemAmount() . ', ' .
 						$this->sqlEceInstitutionItemSequenceNumber() . ', ' .
 						$this->sqlDocumentationTypeIndicator() . ', ' .
 						$this->sqlReturnAcceptanceIndicator() . ', ' .
 						$this->sqlMicrValidIndicator() . ', ' .
 						$this->sqlBofdIndicator() . ', ' .
 						$this->sqlAddendumCount() . ', ' .
 						$this->sqlCorrectionIndicator() . ', ' .
 						$this->sqlArchiveTypeIndicator() . ', ' .
 						$this->sqlTransferredToIntermediaryOn() . ', ' .
 						$this->sqlReturnedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; } elseif( true == array_key_exists( 'ProcessingBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId() . ','; } elseif( true == array_key_exists( 'SettlementDistributionId', $this->getChangedColumns() ) ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId() . ','; } elseif( true == array_key_exists( 'CompanyPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_file_id = ' . $this->sqlX937FileId() . ','; } elseif( true == array_key_exists( 'X937FileId', $this->getChangedColumns() ) ) { $strSql .= ' x937_file_id = ' . $this->sqlX937FileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_cash_letter_id = ' . $this->sqlX937CashLetterId() . ','; } elseif( true == array_key_exists( 'X937CashLetterId', $this->getChangedColumns() ) ) { $strSql .= ' x937_cash_letter_id = ' . $this->sqlX937CashLetterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_bundle_id = ' . $this->sqlX937BundleId() . ','; } elseif( true == array_key_exists( 'X937BundleId', $this->getChangedColumns() ) ) { $strSql .= ' x937_bundle_id = ' . $this->sqlX937BundleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; } elseif( true == array_key_exists( 'ReturnTypeId', $this->getChangedColumns() ) ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; } elseif( true == array_key_exists( 'RecordType', $this->getChangedColumns() ) ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auxiliary_on_us = ' . $this->sqlAuxiliaryOnUs() . ','; } elseif( true == array_key_exists( 'AuxiliaryOnUs', $this->getChangedColumns() ) ) { $strSql .= ' auxiliary_on_us = ' . $this->sqlAuxiliaryOnUs() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_processing_code = ' . $this->sqlExternalProcessingCode() . ','; } elseif( true == array_key_exists( 'ExternalProcessingCode', $this->getChangedColumns() ) ) { $strSql .= ' external_processing_code = ' . $this->sqlExternalProcessingCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payor_bank_routing_number = ' . $this->sqlPayorBankRoutingNumber() . ','; } elseif( true == array_key_exists( 'PayorBankRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' payor_bank_routing_number = ' . $this->sqlPayorBankRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payor_bank_routing_number_check_digit = ' . $this->sqlPayorBankRoutingNumberCheckDigit() . ','; } elseif( true == array_key_exists( 'PayorBankRoutingNumberCheckDigit', $this->getChangedColumns() ) ) { $strSql .= ' payor_bank_routing_number_check_digit = ' . $this->sqlPayorBankRoutingNumberCheckDigit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' on_us = ' . $this->sqlOnUs() . ','; } elseif( true == array_key_exists( 'OnUs', $this->getChangedColumns() ) ) { $strSql .= ' on_us = ' . $this->sqlOnUs() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_amount = ' . $this->sqlItemAmount() . ','; } elseif( true == array_key_exists( 'ItemAmount', $this->getChangedColumns() ) ) { $strSql .= ' item_amount = ' . $this->sqlItemAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ece_institution_item_sequence_number = ' . $this->sqlEceInstitutionItemSequenceNumber() . ','; } elseif( true == array_key_exists( 'EceInstitutionItemSequenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' ece_institution_item_sequence_number = ' . $this->sqlEceInstitutionItemSequenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' documentation_type_indicator = ' . $this->sqlDocumentationTypeIndicator() . ','; } elseif( true == array_key_exists( 'DocumentationTypeIndicator', $this->getChangedColumns() ) ) { $strSql .= ' documentation_type_indicator = ' . $this->sqlDocumentationTypeIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_acceptance_indicator = ' . $this->sqlReturnAcceptanceIndicator() . ','; } elseif( true == array_key_exists( 'ReturnAcceptanceIndicator', $this->getChangedColumns() ) ) { $strSql .= ' return_acceptance_indicator = ' . $this->sqlReturnAcceptanceIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' micr_valid_indicator = ' . $this->sqlMicrValidIndicator() . ','; } elseif( true == array_key_exists( 'MicrValidIndicator', $this->getChangedColumns() ) ) { $strSql .= ' micr_valid_indicator = ' . $this->sqlMicrValidIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bofd_indicator = ' . $this->sqlBofdIndicator() . ','; } elseif( true == array_key_exists( 'BofdIndicator', $this->getChangedColumns() ) ) { $strSql .= ' bofd_indicator = ' . $this->sqlBofdIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' addendum_count = ' . $this->sqlAddendumCount() . ','; } elseif( true == array_key_exists( 'AddendumCount', $this->getChangedColumns() ) ) { $strSql .= ' addendum_count = ' . $this->sqlAddendumCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' correction_indicator = ' . $this->sqlCorrectionIndicator() . ','; } elseif( true == array_key_exists( 'CorrectionIndicator', $this->getChangedColumns() ) ) { $strSql .= ' correction_indicator = ' . $this->sqlCorrectionIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' archive_type_indicator = ' . $this->sqlArchiveTypeIndicator() . ','; } elseif( true == array_key_exists( 'ArchiveTypeIndicator', $this->getChangedColumns() ) ) { $strSql .= ' archive_type_indicator = ' . $this->sqlArchiveTypeIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transferred_to_intermediary_on = ' . $this->sqlTransferredToIntermediaryOn() . ','; } elseif( true == array_key_exists( 'TransferredToIntermediaryOn', $this->getChangedColumns() ) ) { $strSql .= ' transferred_to_intermediary_on = ' . $this->sqlTransferredToIntermediaryOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; } elseif( true == array_key_exists( 'ReturnedOn', $this->getChangedColumns() ) ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'processing_bank_account_id' => $this->getProcessingBankAccountId(),
			'ar_payment_id' => $this->getArPaymentId(),
			'settlement_distribution_id' => $this->getSettlementDistributionId(),
			'company_payment_id' => $this->getCompanyPaymentId(),
			'x937_file_id' => $this->getX937FileId(),
			'x937_cash_letter_id' => $this->getX937CashLetterId(),
			'x937_bundle_id' => $this->getX937BundleId(),
			'return_type_id' => $this->getReturnTypeId(),
			'record_type' => $this->getRecordType(),
			'auxiliary_on_us' => $this->getAuxiliaryOnUs(),
			'external_processing_code' => $this->getExternalProcessingCode(),
			'payor_bank_routing_number' => $this->getPayorBankRoutingNumber(),
			'payor_bank_routing_number_check_digit' => $this->getPayorBankRoutingNumberCheckDigit(),
			'on_us' => $this->getOnUs(),
			'item_amount' => $this->getItemAmount(),
			'ece_institution_item_sequence_number' => $this->getEceInstitutionItemSequenceNumber(),
			'documentation_type_indicator' => $this->getDocumentationTypeIndicator(),
			'return_acceptance_indicator' => $this->getReturnAcceptanceIndicator(),
			'micr_valid_indicator' => $this->getMicrValidIndicator(),
			'bofd_indicator' => $this->getBofdIndicator(),
			'addendum_count' => $this->getAddendumCount(),
			'correction_indicator' => $this->getCorrectionIndicator(),
			'archive_type_indicator' => $this->getArchiveTypeIndicator(),
			'transferred_to_intermediary_on' => $this->getTransferredToIntermediaryOn(),
			'returned_on' => $this->getReturnedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>