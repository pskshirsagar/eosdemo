<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937CheckDetailAddendumARecords
 * Do not add any new functions to this class.
 */

class CBaseX937CheckDetailAddendumARecords extends CEosPluralBase {

	/**
	 * @return CX937CheckDetailAddendumARecord[]
	 */
	public static function fetchX937CheckDetailAddendumARecords( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CX937CheckDetailAddendumARecord::class, $objDatabase );
	}

	/**
	 * @return CX937CheckDetailAddendumARecord
	 */
	public static function fetchX937CheckDetailAddendumARecord( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CX937CheckDetailAddendumARecord::class, $objDatabase );
	}

	public static function fetchX937CheckDetailAddendumARecordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'x937_check_detail_addendum_a_records', $objDatabase );
	}

	public static function fetchX937CheckDetailAddendumARecordById( $intId, $objDatabase ) {
		return self::fetchX937CheckDetailAddendumARecord( sprintf( 'SELECT * FROM x937_check_detail_addendum_a_records WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchX937CheckDetailAddendumARecordsByX937CheckDetailRecordId( $intX937CheckDetailRecordId, $objDatabase ) {
		return self::fetchX937CheckDetailAddendumARecords( sprintf( 'SELECT * FROM x937_check_detail_addendum_a_records WHERE x937_check_detail_record_id = %d', ( int ) $intX937CheckDetailRecordId ), $objDatabase );
	}

}
?>