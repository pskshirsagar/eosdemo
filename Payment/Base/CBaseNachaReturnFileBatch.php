<?php

class CBaseNachaReturnFileBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.nacha_return_file_batches';

	protected $m_intId;
	protected $m_intNachaReturnFileId;
	protected $m_intCid;
	protected $m_strRecordTypeCode;
	protected $m_strServiceClassCode;
	protected $m_strCompanyName;
	protected $m_strCompanyDiscretionaryData;
	protected $m_strCompanyIdentification;
	protected $m_strStandardEntryClassCode;
	protected $m_strCompanyEntryDescription;
	protected $m_strCompanyDescriptiveDate;
	protected $m_strEffectiveEntryDate;
	protected $m_strSettlementDate;
	protected $m_strOriginatorStatusCode;
	protected $m_strOriginatingDfiIdentification;
	protected $m_strBatchNumber;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['nacha_return_file_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaReturnFileId', trim( $arrValues['nacha_return_file_id'] ) ); elseif( isset( $arrValues['nacha_return_file_id'] ) ) $this->setNachaReturnFileId( $arrValues['nacha_return_file_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['record_type_code'] ) && $boolDirectSet ) $this->set( 'm_strRecordTypeCode', trim( stripcslashes( $arrValues['record_type_code'] ) ) ); elseif( isset( $arrValues['record_type_code'] ) ) $this->setRecordTypeCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_type_code'] ) : $arrValues['record_type_code'] );
		if( isset( $arrValues['service_class_code'] ) && $boolDirectSet ) $this->set( 'm_strServiceClassCode', trim( stripcslashes( $arrValues['service_class_code'] ) ) ); elseif( isset( $arrValues['service_class_code'] ) ) $this->setServiceClassCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['service_class_code'] ) : $arrValues['service_class_code'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( stripcslashes( $arrValues['company_name'] ) ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_name'] ) : $arrValues['company_name'] );
		if( isset( $arrValues['company_discretionary_data'] ) && $boolDirectSet ) $this->set( 'm_strCompanyDiscretionaryData', trim( stripcslashes( $arrValues['company_discretionary_data'] ) ) ); elseif( isset( $arrValues['company_discretionary_data'] ) ) $this->setCompanyDiscretionaryData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_discretionary_data'] ) : $arrValues['company_discretionary_data'] );
		if( isset( $arrValues['company_identification'] ) && $boolDirectSet ) $this->set( 'm_strCompanyIdentification', trim( stripcslashes( $arrValues['company_identification'] ) ) ); elseif( isset( $arrValues['company_identification'] ) ) $this->setCompanyIdentification( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_identification'] ) : $arrValues['company_identification'] );
		if( isset( $arrValues['standard_entry_class_code'] ) && $boolDirectSet ) $this->set( 'm_strStandardEntryClassCode', trim( stripcslashes( $arrValues['standard_entry_class_code'] ) ) ); elseif( isset( $arrValues['standard_entry_class_code'] ) ) $this->setStandardEntryClassCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['standard_entry_class_code'] ) : $arrValues['standard_entry_class_code'] );
		if( isset( $arrValues['company_entry_description'] ) && $boolDirectSet ) $this->set( 'm_strCompanyEntryDescription', trim( stripcslashes( $arrValues['company_entry_description'] ) ) ); elseif( isset( $arrValues['company_entry_description'] ) ) $this->setCompanyEntryDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_entry_description'] ) : $arrValues['company_entry_description'] );
		if( isset( $arrValues['company_descriptive_date'] ) && $boolDirectSet ) $this->set( 'm_strCompanyDescriptiveDate', trim( stripcslashes( $arrValues['company_descriptive_date'] ) ) ); elseif( isset( $arrValues['company_descriptive_date'] ) ) $this->setCompanyDescriptiveDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_descriptive_date'] ) : $arrValues['company_descriptive_date'] );
		if( isset( $arrValues['effective_entry_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveEntryDate', trim( stripcslashes( $arrValues['effective_entry_date'] ) ) ); elseif( isset( $arrValues['effective_entry_date'] ) ) $this->setEffectiveEntryDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['effective_entry_date'] ) : $arrValues['effective_entry_date'] );
		if( isset( $arrValues['settlement_date'] ) && $boolDirectSet ) $this->set( 'm_strSettlementDate', trim( stripcslashes( $arrValues['settlement_date'] ) ) ); elseif( isset( $arrValues['settlement_date'] ) ) $this->setSettlementDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['settlement_date'] ) : $arrValues['settlement_date'] );
		if( isset( $arrValues['originator_status_code'] ) && $boolDirectSet ) $this->set( 'm_strOriginatorStatusCode', trim( stripcslashes( $arrValues['originator_status_code'] ) ) ); elseif( isset( $arrValues['originator_status_code'] ) ) $this->setOriginatorStatusCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['originator_status_code'] ) : $arrValues['originator_status_code'] );
		if( isset( $arrValues['originating_dfi_identification'] ) && $boolDirectSet ) $this->set( 'm_strOriginatingDfiIdentification', trim( stripcslashes( $arrValues['originating_dfi_identification'] ) ) ); elseif( isset( $arrValues['originating_dfi_identification'] ) ) $this->setOriginatingDfiIdentification( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['originating_dfi_identification'] ) : $arrValues['originating_dfi_identification'] );
		if( isset( $arrValues['batch_number'] ) && $boolDirectSet ) $this->set( 'm_strBatchNumber', trim( stripcslashes( $arrValues['batch_number'] ) ) ); elseif( isset( $arrValues['batch_number'] ) ) $this->setBatchNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['batch_number'] ) : $arrValues['batch_number'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setNachaReturnFileId( $intNachaReturnFileId ) {
		$this->set( 'm_intNachaReturnFileId', CStrings::strToIntDef( $intNachaReturnFileId, NULL, false ) );
	}

	public function getNachaReturnFileId() {
		return $this->m_intNachaReturnFileId;
	}

	public function sqlNachaReturnFileId() {
		return ( true == isset( $this->m_intNachaReturnFileId ) ) ? ( string ) $this->m_intNachaReturnFileId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setRecordTypeCode( $strRecordTypeCode ) {
		$this->set( 'm_strRecordTypeCode', CStrings::strTrimDef( $strRecordTypeCode, 1, NULL, true ) );
	}

	public function getRecordTypeCode() {
		return $this->m_strRecordTypeCode;
	}

	public function sqlRecordTypeCode() {
		return ( true == isset( $this->m_strRecordTypeCode ) ) ? '\'' . addslashes( $this->m_strRecordTypeCode ) . '\'' : 'NULL';
	}

	public function setServiceClassCode( $strServiceClassCode ) {
		$this->set( 'm_strServiceClassCode', CStrings::strTrimDef( $strServiceClassCode, 3, NULL, true ) );
	}

	public function getServiceClassCode() {
		return $this->m_strServiceClassCode;
	}

	public function sqlServiceClassCode() {
		return ( true == isset( $this->m_strServiceClassCode ) ) ? '\'' . addslashes( $this->m_strServiceClassCode ) . '\'' : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 16, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? '\'' . addslashes( $this->m_strCompanyName ) . '\'' : 'NULL';
	}

	public function setCompanyDiscretionaryData( $strCompanyDiscretionaryData ) {
		$this->set( 'm_strCompanyDiscretionaryData', CStrings::strTrimDef( $strCompanyDiscretionaryData, 20, NULL, true ) );
	}

	public function getCompanyDiscretionaryData() {
		return $this->m_strCompanyDiscretionaryData;
	}

	public function sqlCompanyDiscretionaryData() {
		return ( true == isset( $this->m_strCompanyDiscretionaryData ) ) ? '\'' . addslashes( $this->m_strCompanyDiscretionaryData ) . '\'' : 'NULL';
	}

	public function setCompanyIdentification( $strCompanyIdentification ) {
		$this->set( 'm_strCompanyIdentification', CStrings::strTrimDef( $strCompanyIdentification, 10, NULL, true ) );
	}

	public function getCompanyIdentification() {
		return $this->m_strCompanyIdentification;
	}

	public function sqlCompanyIdentification() {
		return ( true == isset( $this->m_strCompanyIdentification ) ) ? '\'' . addslashes( $this->m_strCompanyIdentification ) . '\'' : 'NULL';
	}

	public function setStandardEntryClassCode( $strStandardEntryClassCode ) {
		$this->set( 'm_strStandardEntryClassCode', CStrings::strTrimDef( $strStandardEntryClassCode, 3, NULL, true ) );
	}

	public function getStandardEntryClassCode() {
		return $this->m_strStandardEntryClassCode;
	}

	public function sqlStandardEntryClassCode() {
		return ( true == isset( $this->m_strStandardEntryClassCode ) ) ? '\'' . addslashes( $this->m_strStandardEntryClassCode ) . '\'' : 'NULL';
	}

	public function setCompanyEntryDescription( $strCompanyEntryDescription ) {
		$this->set( 'm_strCompanyEntryDescription', CStrings::strTrimDef( $strCompanyEntryDescription, 10, NULL, true ) );
	}

	public function getCompanyEntryDescription() {
		return $this->m_strCompanyEntryDescription;
	}

	public function sqlCompanyEntryDescription() {
		return ( true == isset( $this->m_strCompanyEntryDescription ) ) ? '\'' . addslashes( $this->m_strCompanyEntryDescription ) . '\'' : 'NULL';
	}

	public function setCompanyDescriptiveDate( $strCompanyDescriptiveDate ) {
		$this->set( 'm_strCompanyDescriptiveDate', CStrings::strTrimDef( $strCompanyDescriptiveDate, 6, NULL, true ) );
	}

	public function getCompanyDescriptiveDate() {
		return $this->m_strCompanyDescriptiveDate;
	}

	public function sqlCompanyDescriptiveDate() {
		return ( true == isset( $this->m_strCompanyDescriptiveDate ) ) ? '\'' . addslashes( $this->m_strCompanyDescriptiveDate ) . '\'' : 'NULL';
	}

	public function setEffectiveEntryDate( $strEffectiveEntryDate ) {
		$this->set( 'm_strEffectiveEntryDate', CStrings::strTrimDef( $strEffectiveEntryDate, 6, NULL, true ) );
	}

	public function getEffectiveEntryDate() {
		return $this->m_strEffectiveEntryDate;
	}

	public function sqlEffectiveEntryDate() {
		return ( true == isset( $this->m_strEffectiveEntryDate ) ) ? '\'' . addslashes( $this->m_strEffectiveEntryDate ) . '\'' : 'NULL';
	}

	public function setSettlementDate( $strSettlementDate ) {
		$this->set( 'm_strSettlementDate', CStrings::strTrimDef( $strSettlementDate, 3, NULL, true ) );
	}

	public function getSettlementDate() {
		return $this->m_strSettlementDate;
	}

	public function sqlSettlementDate() {
		return ( true == isset( $this->m_strSettlementDate ) ) ? '\'' . addslashes( $this->m_strSettlementDate ) . '\'' : 'NULL';
	}

	public function setOriginatorStatusCode( $strOriginatorStatusCode ) {
		$this->set( 'm_strOriginatorStatusCode', CStrings::strTrimDef( $strOriginatorStatusCode, 1, NULL, true ) );
	}

	public function getOriginatorStatusCode() {
		return $this->m_strOriginatorStatusCode;
	}

	public function sqlOriginatorStatusCode() {
		return ( true == isset( $this->m_strOriginatorStatusCode ) ) ? '\'' . addslashes( $this->m_strOriginatorStatusCode ) . '\'' : 'NULL';
	}

	public function setOriginatingDfiIdentification( $strOriginatingDfiIdentification ) {
		$this->set( 'm_strOriginatingDfiIdentification', CStrings::strTrimDef( $strOriginatingDfiIdentification, 8, NULL, true ) );
	}

	public function getOriginatingDfiIdentification() {
		return $this->m_strOriginatingDfiIdentification;
	}

	public function sqlOriginatingDfiIdentification() {
		return ( true == isset( $this->m_strOriginatingDfiIdentification ) ) ? '\'' . addslashes( $this->m_strOriginatingDfiIdentification ) . '\'' : 'NULL';
	}

	public function setBatchNumber( $strBatchNumber ) {
		$this->set( 'm_strBatchNumber', CStrings::strTrimDef( $strBatchNumber, 7, NULL, true ) );
	}

	public function getBatchNumber() {
		return $this->m_strBatchNumber;
	}

	public function sqlBatchNumber() {
		return ( true == isset( $this->m_strBatchNumber ) ) ? '\'' . addslashes( $this->m_strBatchNumber ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, nacha_return_file_id, cid, record_type_code, service_class_code, company_name, company_discretionary_data, company_identification, standard_entry_class_code, company_entry_description, company_descriptive_date, effective_entry_date, settlement_date, originator_status_code, originating_dfi_identification, batch_number, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlNachaReturnFileId() . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlRecordTypeCode() . ', ' .
 						$this->sqlServiceClassCode() . ', ' .
 						$this->sqlCompanyName() . ', ' .
 						$this->sqlCompanyDiscretionaryData() . ', ' .
 						$this->sqlCompanyIdentification() . ', ' .
 						$this->sqlStandardEntryClassCode() . ', ' .
 						$this->sqlCompanyEntryDescription() . ', ' .
 						$this->sqlCompanyDescriptiveDate() . ', ' .
 						$this->sqlEffectiveEntryDate() . ', ' .
 						$this->sqlSettlementDate() . ', ' .
 						$this->sqlOriginatorStatusCode() . ', ' .
 						$this->sqlOriginatingDfiIdentification() . ', ' .
 						$this->sqlBatchNumber() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_return_file_id = ' . $this->sqlNachaReturnFileId() . ','; } elseif( true == array_key_exists( 'NachaReturnFileId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_return_file_id = ' . $this->sqlNachaReturnFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_type_code = ' . $this->sqlRecordTypeCode() . ','; } elseif( true == array_key_exists( 'RecordTypeCode', $this->getChangedColumns() ) ) { $strSql .= ' record_type_code = ' . $this->sqlRecordTypeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_class_code = ' . $this->sqlServiceClassCode() . ','; } elseif( true == array_key_exists( 'ServiceClassCode', $this->getChangedColumns() ) ) { $strSql .= ' service_class_code = ' . $this->sqlServiceClassCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_discretionary_data = ' . $this->sqlCompanyDiscretionaryData() . ','; } elseif( true == array_key_exists( 'CompanyDiscretionaryData', $this->getChangedColumns() ) ) { $strSql .= ' company_discretionary_data = ' . $this->sqlCompanyDiscretionaryData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_identification = ' . $this->sqlCompanyIdentification() . ','; } elseif( true == array_key_exists( 'CompanyIdentification', $this->getChangedColumns() ) ) { $strSql .= ' company_identification = ' . $this->sqlCompanyIdentification() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' standard_entry_class_code = ' . $this->sqlStandardEntryClassCode() . ','; } elseif( true == array_key_exists( 'StandardEntryClassCode', $this->getChangedColumns() ) ) { $strSql .= ' standard_entry_class_code = ' . $this->sqlStandardEntryClassCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_entry_description = ' . $this->sqlCompanyEntryDescription() . ','; } elseif( true == array_key_exists( 'CompanyEntryDescription', $this->getChangedColumns() ) ) { $strSql .= ' company_entry_description = ' . $this->sqlCompanyEntryDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_descriptive_date = ' . $this->sqlCompanyDescriptiveDate() . ','; } elseif( true == array_key_exists( 'CompanyDescriptiveDate', $this->getChangedColumns() ) ) { $strSql .= ' company_descriptive_date = ' . $this->sqlCompanyDescriptiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_entry_date = ' . $this->sqlEffectiveEntryDate() . ','; } elseif( true == array_key_exists( 'EffectiveEntryDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_entry_date = ' . $this->sqlEffectiveEntryDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' settlement_date = ' . $this->sqlSettlementDate() . ','; } elseif( true == array_key_exists( 'SettlementDate', $this->getChangedColumns() ) ) { $strSql .= ' settlement_date = ' . $this->sqlSettlementDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' originator_status_code = ' . $this->sqlOriginatorStatusCode() . ','; } elseif( true == array_key_exists( 'OriginatorStatusCode', $this->getChangedColumns() ) ) { $strSql .= ' originator_status_code = ' . $this->sqlOriginatorStatusCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' originating_dfi_identification = ' . $this->sqlOriginatingDfiIdentification() . ','; } elseif( true == array_key_exists( 'OriginatingDfiIdentification', $this->getChangedColumns() ) ) { $strSql .= ' originating_dfi_identification = ' . $this->sqlOriginatingDfiIdentification() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_number = ' . $this->sqlBatchNumber() . ','; } elseif( true == array_key_exists( 'BatchNumber', $this->getChangedColumns() ) ) { $strSql .= ' batch_number = ' . $this->sqlBatchNumber() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'nacha_return_file_id' => $this->getNachaReturnFileId(),
			'cid' => $this->getCid(),
			'record_type_code' => $this->getRecordTypeCode(),
			'service_class_code' => $this->getServiceClassCode(),
			'company_name' => $this->getCompanyName(),
			'company_discretionary_data' => $this->getCompanyDiscretionaryData(),
			'company_identification' => $this->getCompanyIdentification(),
			'standard_entry_class_code' => $this->getStandardEntryClassCode(),
			'company_entry_description' => $this->getCompanyEntryDescription(),
			'company_descriptive_date' => $this->getCompanyDescriptiveDate(),
			'effective_entry_date' => $this->getEffectiveEntryDate(),
			'settlement_date' => $this->getSettlementDate(),
			'originator_status_code' => $this->getOriginatorStatusCode(),
			'originating_dfi_identification' => $this->getOriginatingDfiIdentification(),
			'batch_number' => $this->getBatchNumber(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>