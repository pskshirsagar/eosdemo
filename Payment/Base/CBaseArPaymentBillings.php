<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentBillings
 * Do not add any new functions to this class.
 */

class CBaseArPaymentBillings extends CEosPluralBase {

	/**
	 * @return CArPaymentBilling[]
	 */
	public static function fetchArPaymentBillings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CArPaymentBilling::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CArPaymentBilling
	 */
	public static function fetchArPaymentBilling( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CArPaymentBilling::class, $objDatabase );
	}

	public static function fetchArPaymentBillingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ar_payment_billings', $objDatabase );
	}

	public static function fetchArPaymentBillingById( $intId, $objDatabase ) {
		return self::fetchArPaymentBilling( sprintf( 'SELECT * FROM ar_payment_billings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchArPaymentBillingsByCid( $intCid, $objDatabase ) {
		return self::fetchArPaymentBillings( sprintf( 'SELECT * FROM ar_payment_billings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentBillingsByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchArPaymentBillings( sprintf( 'SELECT * FROM ar_payment_billings WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

	public static function fetchArPaymentBillingsByCheckAccountTypeId( $intCheckAccountTypeId, $objDatabase ) {
		return self::fetchArPaymentBillings( sprintf( 'SELECT * FROM ar_payment_billings WHERE check_account_type_id = %d', ( int ) $intCheckAccountTypeId ), $objDatabase );
	}

}
?>