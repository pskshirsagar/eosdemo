<?php

class CBaseMerchantAccountApplication extends CEosSingularBase {

	const TABLE_NAME = 'public.merchant_account_applications';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMerchantAccountApplicationTypesId;
	protected $m_strCompanyName;
	protected $m_strCompanyDbaName;
	protected $m_strCompanyStreetLine1;
	protected $m_strCompanyCity;
	protected $m_strCompanyStateCode;
	protected $m_strCompanyPostalCode;
	protected $m_strCompanyCountryCode;
	protected $m_strCompanyTaxNumberEncrypted;
	protected $m_strCompanyTaxNumberMasked;
	protected $m_strCompanyPhoneNumber;
	protected $m_strCompanyFaxNumber;
	protected $m_intOwnershipTypeId;
	protected $m_intNumberOfOwners;
	protected $m_intNumberOfLocations;
	protected $m_strTypeOfBusiness;
	protected $m_strAgeOfBusiness;
	protected $m_strDateBusinessAcquired;
	protected $m_strBilltoStreetLine1;
	protected $m_strBilltoCity;
	protected $m_strBilltoStateCode;
	protected $m_strBilltoPostalCode;
	protected $m_strBilltoCountryCode;
	protected $m_strContactName;
	protected $m_strContactDayTimePhone;
	protected $m_strContactEveningPhone;
	protected $m_strContactEmail;
	protected $m_strWebsiteUrl;
	protected $m_strVpName;
	protected $m_strVpTitle;
	protected $m_strVpOwnership;
	protected $m_strVpTaxNumberEncrypted;
	protected $m_strVpTaxNumberMasked;
	protected $m_strVpDob;
	protected $m_strVpStreetLine1;
	protected $m_strVpCity;
	protected $m_strVpStateCode;
	protected $m_strVpPostalCode;
	protected $m_strVpResidence;
	protected $m_strVpRentOrOwn;
	protected $m_strVpHomePhone;
	protected $m_strVpPreviousAddress;
	protected $m_intHavePoorCredit;
	protected $m_intApplyingForAch;
	protected $m_intApplyingForCreditCard;
	protected $m_intApplyingForDiscover;
	protected $m_intApplyingForAmericanExpress;
	protected $m_intNumOfAccountsRequested;
	protected $m_intAchMaxTransactionsPerDay;
	protected $m_fltAchMaxAmountPerTransaction;
	protected $m_fltAchAvgAmountPerTransaction;
	protected $m_fltAchMaxAmountPerDay;
	protected $m_fltAchMaxAmountPerMonth;
	protected $m_fltCcAvgAmountPerTransaction;
	protected $m_fltCcMaxAmountPerTransaction;
	protected $m_fltCcMaxAmountPerMonth;
	protected $m_intHavePreviousMerchantAccount;
	protected $m_strPreviousMerchantAccountInfo;
	protected $m_strInstructionsForEcho;
	protected $m_intIsFeeManaged;
	protected $m_intPhotosRequired;
	protected $m_strDiscoverPin;
	protected $m_strAmexPin;
	protected $m_strUsername;
	protected $m_strPassword;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intMerchantAccountApplicationTypesId = '1';
		$this->m_strCompanyCountryCode = 'US';
		$this->m_intNumberOfOwners = '1';
		$this->m_strBilltoCountryCode = 'US';
		$this->m_intHavePoorCredit = '0';
		$this->m_intApplyingForAch = '0';
		$this->m_intApplyingForCreditCard = '0';
		$this->m_intApplyingForDiscover = '0';
		$this->m_intApplyingForAmericanExpress = '0';
		$this->m_intHavePreviousMerchantAccount = '0';
		$this->m_intIsFeeManaged = '0';
		$this->m_intPhotosRequired = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['merchant_account_application_types_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantAccountApplicationTypesId', trim( $arrValues['merchant_account_application_types_id'] ) ); elseif( isset( $arrValues['merchant_account_application_types_id'] ) ) $this->setMerchantAccountApplicationTypesId( $arrValues['merchant_account_application_types_id'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( stripcslashes( $arrValues['company_name'] ) ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_name'] ) : $arrValues['company_name'] );
		if( isset( $arrValues['company_dba_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyDbaName', trim( stripcslashes( $arrValues['company_dba_name'] ) ) ); elseif( isset( $arrValues['company_dba_name'] ) ) $this->setCompanyDbaName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_dba_name'] ) : $arrValues['company_dba_name'] );
		if( isset( $arrValues['company_street_line1'] ) && $boolDirectSet ) $this->set( 'm_strCompanyStreetLine1', trim( stripcslashes( $arrValues['company_street_line1'] ) ) ); elseif( isset( $arrValues['company_street_line1'] ) ) $this->setCompanyStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_street_line1'] ) : $arrValues['company_street_line1'] );
		if( isset( $arrValues['company_city'] ) && $boolDirectSet ) $this->set( 'm_strCompanyCity', trim( stripcslashes( $arrValues['company_city'] ) ) ); elseif( isset( $arrValues['company_city'] ) ) $this->setCompanyCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_city'] ) : $arrValues['company_city'] );
		if( isset( $arrValues['company_state_code'] ) && $boolDirectSet ) $this->set( 'm_strCompanyStateCode', trim( stripcslashes( $arrValues['company_state_code'] ) ) ); elseif( isset( $arrValues['company_state_code'] ) ) $this->setCompanyStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_state_code'] ) : $arrValues['company_state_code'] );
		if( isset( $arrValues['company_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strCompanyPostalCode', trim( stripcslashes( $arrValues['company_postal_code'] ) ) ); elseif( isset( $arrValues['company_postal_code'] ) ) $this->setCompanyPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_postal_code'] ) : $arrValues['company_postal_code'] );
		if( isset( $arrValues['company_country_code'] ) && $boolDirectSet ) $this->set( 'm_strCompanyCountryCode', trim( stripcslashes( $arrValues['company_country_code'] ) ) ); elseif( isset( $arrValues['company_country_code'] ) ) $this->setCompanyCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_country_code'] ) : $arrValues['company_country_code'] );
		if( isset( $arrValues['company_tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCompanyTaxNumberEncrypted', trim( stripcslashes( $arrValues['company_tax_number_encrypted'] ) ) ); elseif( isset( $arrValues['company_tax_number_encrypted'] ) ) $this->setCompanyTaxNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_tax_number_encrypted'] ) : $arrValues['company_tax_number_encrypted'] );
		if( isset( $arrValues['company_tax_number_masked'] ) && $boolDirectSet ) $this->set( 'm_strCompanyTaxNumberMasked', trim( stripcslashes( $arrValues['company_tax_number_masked'] ) ) ); elseif( isset( $arrValues['company_tax_number_masked'] ) ) $this->setCompanyTaxNumberMasked( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_tax_number_masked'] ) : $arrValues['company_tax_number_masked'] );
		if( isset( $arrValues['company_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strCompanyPhoneNumber', trim( stripcslashes( $arrValues['company_phone_number'] ) ) ); elseif( isset( $arrValues['company_phone_number'] ) ) $this->setCompanyPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_phone_number'] ) : $arrValues['company_phone_number'] );
		if( isset( $arrValues['company_fax_number'] ) && $boolDirectSet ) $this->set( 'm_strCompanyFaxNumber', trim( stripcslashes( $arrValues['company_fax_number'] ) ) ); elseif( isset( $arrValues['company_fax_number'] ) ) $this->setCompanyFaxNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_fax_number'] ) : $arrValues['company_fax_number'] );
		if( isset( $arrValues['ownership_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOwnershipTypeId', trim( $arrValues['ownership_type_id'] ) ); elseif( isset( $arrValues['ownership_type_id'] ) ) $this->setOwnershipTypeId( $arrValues['ownership_type_id'] );
		if( isset( $arrValues['number_of_owners'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfOwners', trim( $arrValues['number_of_owners'] ) ); elseif( isset( $arrValues['number_of_owners'] ) ) $this->setNumberOfOwners( $arrValues['number_of_owners'] );
		if( isset( $arrValues['number_of_locations'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfLocations', trim( $arrValues['number_of_locations'] ) ); elseif( isset( $arrValues['number_of_locations'] ) ) $this->setNumberOfLocations( $arrValues['number_of_locations'] );
		if( isset( $arrValues['type_of_business'] ) && $boolDirectSet ) $this->set( 'm_strTypeOfBusiness', trim( stripcslashes( $arrValues['type_of_business'] ) ) ); elseif( isset( $arrValues['type_of_business'] ) ) $this->setTypeOfBusiness( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['type_of_business'] ) : $arrValues['type_of_business'] );
		if( isset( $arrValues['age_of_business'] ) && $boolDirectSet ) $this->set( 'm_strAgeOfBusiness', trim( stripcslashes( $arrValues['age_of_business'] ) ) ); elseif( isset( $arrValues['age_of_business'] ) ) $this->setAgeOfBusiness( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['age_of_business'] ) : $arrValues['age_of_business'] );
		if( isset( $arrValues['date_business_acquired'] ) && $boolDirectSet ) $this->set( 'm_strDateBusinessAcquired', trim( $arrValues['date_business_acquired'] ) ); elseif( isset( $arrValues['date_business_acquired'] ) ) $this->setDateBusinessAcquired( $arrValues['date_business_acquired'] );
		if( isset( $arrValues['billto_street_line1'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStreetLine1', trim( stripcslashes( $arrValues['billto_street_line1'] ) ) ); elseif( isset( $arrValues['billto_street_line1'] ) ) $this->setBilltoStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line1'] ) : $arrValues['billto_street_line1'] );
		if( isset( $arrValues['billto_city'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCity', trim( stripcslashes( $arrValues['billto_city'] ) ) ); elseif( isset( $arrValues['billto_city'] ) ) $this->setBilltoCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_city'] ) : $arrValues['billto_city'] );
		if( isset( $arrValues['billto_state_code'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStateCode', trim( stripcslashes( $arrValues['billto_state_code'] ) ) ); elseif( isset( $arrValues['billto_state_code'] ) ) $this->setBilltoStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_state_code'] ) : $arrValues['billto_state_code'] );
		if( isset( $arrValues['billto_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strBilltoPostalCode', trim( stripcslashes( $arrValues['billto_postal_code'] ) ) ); elseif( isset( $arrValues['billto_postal_code'] ) ) $this->setBilltoPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_postal_code'] ) : $arrValues['billto_postal_code'] );
		if( isset( $arrValues['billto_country_code'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCountryCode', trim( stripcslashes( $arrValues['billto_country_code'] ) ) ); elseif( isset( $arrValues['billto_country_code'] ) ) $this->setBilltoCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_country_code'] ) : $arrValues['billto_country_code'] );
		if( isset( $arrValues['contact_name'] ) && $boolDirectSet ) $this->set( 'm_strContactName', trim( stripcslashes( $arrValues['contact_name'] ) ) ); elseif( isset( $arrValues['contact_name'] ) ) $this->setContactName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_name'] ) : $arrValues['contact_name'] );
		if( isset( $arrValues['contact_day_time_phone'] ) && $boolDirectSet ) $this->set( 'm_strContactDayTimePhone', trim( stripcslashes( $arrValues['contact_day_time_phone'] ) ) ); elseif( isset( $arrValues['contact_day_time_phone'] ) ) $this->setContactDayTimePhone( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_day_time_phone'] ) : $arrValues['contact_day_time_phone'] );
		if( isset( $arrValues['contact_evening_phone'] ) && $boolDirectSet ) $this->set( 'm_strContactEveningPhone', trim( stripcslashes( $arrValues['contact_evening_phone'] ) ) ); elseif( isset( $arrValues['contact_evening_phone'] ) ) $this->setContactEveningPhone( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_evening_phone'] ) : $arrValues['contact_evening_phone'] );
		if( isset( $arrValues['contact_email'] ) && $boolDirectSet ) $this->set( 'm_strContactEmail', trim( stripcslashes( $arrValues['contact_email'] ) ) ); elseif( isset( $arrValues['contact_email'] ) ) $this->setContactEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_email'] ) : $arrValues['contact_email'] );
		if( isset( $arrValues['website_url'] ) && $boolDirectSet ) $this->set( 'm_strWebsiteUrl', trim( stripcslashes( $arrValues['website_url'] ) ) ); elseif( isset( $arrValues['website_url'] ) ) $this->setWebsiteUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['website_url'] ) : $arrValues['website_url'] );
		if( isset( $arrValues['vp_name'] ) && $boolDirectSet ) $this->set( 'm_strVpName', trim( stripcslashes( $arrValues['vp_name'] ) ) ); elseif( isset( $arrValues['vp_name'] ) ) $this->setVpName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vp_name'] ) : $arrValues['vp_name'] );
		if( isset( $arrValues['vp_title'] ) && $boolDirectSet ) $this->set( 'm_strVpTitle', trim( stripcslashes( $arrValues['vp_title'] ) ) ); elseif( isset( $arrValues['vp_title'] ) ) $this->setVpTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vp_title'] ) : $arrValues['vp_title'] );
		if( isset( $arrValues['vp_ownership'] ) && $boolDirectSet ) $this->set( 'm_strVpOwnership', trim( stripcslashes( $arrValues['vp_ownership'] ) ) ); elseif( isset( $arrValues['vp_ownership'] ) ) $this->setVpOwnership( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vp_ownership'] ) : $arrValues['vp_ownership'] );
		if( isset( $arrValues['vp_tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strVpTaxNumberEncrypted', trim( stripcslashes( $arrValues['vp_tax_number_encrypted'] ) ) ); elseif( isset( $arrValues['vp_tax_number_encrypted'] ) ) $this->setVpTaxNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vp_tax_number_encrypted'] ) : $arrValues['vp_tax_number_encrypted'] );
		if( isset( $arrValues['vp_tax_number_masked'] ) && $boolDirectSet ) $this->set( 'm_strVpTaxNumberMasked', trim( stripcslashes( $arrValues['vp_tax_number_masked'] ) ) ); elseif( isset( $arrValues['vp_tax_number_masked'] ) ) $this->setVpTaxNumberMasked( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vp_tax_number_masked'] ) : $arrValues['vp_tax_number_masked'] );
		if( isset( $arrValues['vp_dob'] ) && $boolDirectSet ) $this->set( 'm_strVpDob', trim( $arrValues['vp_dob'] ) ); elseif( isset( $arrValues['vp_dob'] ) ) $this->setVpDob( $arrValues['vp_dob'] );
		if( isset( $arrValues['vp_street_line1'] ) && $boolDirectSet ) $this->set( 'm_strVpStreetLine1', trim( stripcslashes( $arrValues['vp_street_line1'] ) ) ); elseif( isset( $arrValues['vp_street_line1'] ) ) $this->setVpStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vp_street_line1'] ) : $arrValues['vp_street_line1'] );
		if( isset( $arrValues['vp_city'] ) && $boolDirectSet ) $this->set( 'm_strVpCity', trim( stripcslashes( $arrValues['vp_city'] ) ) ); elseif( isset( $arrValues['vp_city'] ) ) $this->setVpCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vp_city'] ) : $arrValues['vp_city'] );
		if( isset( $arrValues['vp_state_code'] ) && $boolDirectSet ) $this->set( 'm_strVpStateCode', trim( stripcslashes( $arrValues['vp_state_code'] ) ) ); elseif( isset( $arrValues['vp_state_code'] ) ) $this->setVpStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vp_state_code'] ) : $arrValues['vp_state_code'] );
		if( isset( $arrValues['vp_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strVpPostalCode', trim( stripcslashes( $arrValues['vp_postal_code'] ) ) ); elseif( isset( $arrValues['vp_postal_code'] ) ) $this->setVpPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vp_postal_code'] ) : $arrValues['vp_postal_code'] );
		if( isset( $arrValues['vp_residence'] ) && $boolDirectSet ) $this->set( 'm_strVpResidence', trim( stripcslashes( $arrValues['vp_residence'] ) ) ); elseif( isset( $arrValues['vp_residence'] ) ) $this->setVpResidence( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vp_residence'] ) : $arrValues['vp_residence'] );
		if( isset( $arrValues['vp_rent_or_own'] ) && $boolDirectSet ) $this->set( 'm_strVpRentOrOwn', trim( stripcslashes( $arrValues['vp_rent_or_own'] ) ) ); elseif( isset( $arrValues['vp_rent_or_own'] ) ) $this->setVpRentOrOwn( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vp_rent_or_own'] ) : $arrValues['vp_rent_or_own'] );
		if( isset( $arrValues['vp_home_phone'] ) && $boolDirectSet ) $this->set( 'm_strVpHomePhone', trim( stripcslashes( $arrValues['vp_home_phone'] ) ) ); elseif( isset( $arrValues['vp_home_phone'] ) ) $this->setVpHomePhone( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vp_home_phone'] ) : $arrValues['vp_home_phone'] );
		if( isset( $arrValues['vp_previous_address'] ) && $boolDirectSet ) $this->set( 'm_strVpPreviousAddress', trim( stripcslashes( $arrValues['vp_previous_address'] ) ) ); elseif( isset( $arrValues['vp_previous_address'] ) ) $this->setVpPreviousAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vp_previous_address'] ) : $arrValues['vp_previous_address'] );
		if( isset( $arrValues['have_poor_credit'] ) && $boolDirectSet ) $this->set( 'm_intHavePoorCredit', trim( $arrValues['have_poor_credit'] ) ); elseif( isset( $arrValues['have_poor_credit'] ) ) $this->setHavePoorCredit( $arrValues['have_poor_credit'] );
		if( isset( $arrValues['applying_for_ach'] ) && $boolDirectSet ) $this->set( 'm_intApplyingForAch', trim( $arrValues['applying_for_ach'] ) ); elseif( isset( $arrValues['applying_for_ach'] ) ) $this->setApplyingForAch( $arrValues['applying_for_ach'] );
		if( isset( $arrValues['applying_for_credit_card'] ) && $boolDirectSet ) $this->set( 'm_intApplyingForCreditCard', trim( $arrValues['applying_for_credit_card'] ) ); elseif( isset( $arrValues['applying_for_credit_card'] ) ) $this->setApplyingForCreditCard( $arrValues['applying_for_credit_card'] );
		if( isset( $arrValues['applying_for_discover'] ) && $boolDirectSet ) $this->set( 'm_intApplyingForDiscover', trim( $arrValues['applying_for_discover'] ) ); elseif( isset( $arrValues['applying_for_discover'] ) ) $this->setApplyingForDiscover( $arrValues['applying_for_discover'] );
		if( isset( $arrValues['applying_for_american_express'] ) && $boolDirectSet ) $this->set( 'm_intApplyingForAmericanExpress', trim( $arrValues['applying_for_american_express'] ) ); elseif( isset( $arrValues['applying_for_american_express'] ) ) $this->setApplyingForAmericanExpress( $arrValues['applying_for_american_express'] );
		if( isset( $arrValues['num_of_accounts_requested'] ) && $boolDirectSet ) $this->set( 'm_intNumOfAccountsRequested', trim( $arrValues['num_of_accounts_requested'] ) ); elseif( isset( $arrValues['num_of_accounts_requested'] ) ) $this->setNumOfAccountsRequested( $arrValues['num_of_accounts_requested'] );
		if( isset( $arrValues['ach_max_transactions_per_day'] ) && $boolDirectSet ) $this->set( 'm_intAchMaxTransactionsPerDay', trim( $arrValues['ach_max_transactions_per_day'] ) ); elseif( isset( $arrValues['ach_max_transactions_per_day'] ) ) $this->setAchMaxTransactionsPerDay( $arrValues['ach_max_transactions_per_day'] );
		if( isset( $arrValues['ach_max_amount_per_transaction'] ) && $boolDirectSet ) $this->set( 'm_fltAchMaxAmountPerTransaction', trim( $arrValues['ach_max_amount_per_transaction'] ) ); elseif( isset( $arrValues['ach_max_amount_per_transaction'] ) ) $this->setAchMaxAmountPerTransaction( $arrValues['ach_max_amount_per_transaction'] );
		if( isset( $arrValues['ach_avg_amount_per_transaction'] ) && $boolDirectSet ) $this->set( 'm_fltAchAvgAmountPerTransaction', trim( $arrValues['ach_avg_amount_per_transaction'] ) ); elseif( isset( $arrValues['ach_avg_amount_per_transaction'] ) ) $this->setAchAvgAmountPerTransaction( $arrValues['ach_avg_amount_per_transaction'] );
		if( isset( $arrValues['ach_max_amount_per_day'] ) && $boolDirectSet ) $this->set( 'm_fltAchMaxAmountPerDay', trim( $arrValues['ach_max_amount_per_day'] ) ); elseif( isset( $arrValues['ach_max_amount_per_day'] ) ) $this->setAchMaxAmountPerDay( $arrValues['ach_max_amount_per_day'] );
		if( isset( $arrValues['ach_max_amount_per_month'] ) && $boolDirectSet ) $this->set( 'm_fltAchMaxAmountPerMonth', trim( $arrValues['ach_max_amount_per_month'] ) ); elseif( isset( $arrValues['ach_max_amount_per_month'] ) ) $this->setAchMaxAmountPerMonth( $arrValues['ach_max_amount_per_month'] );
		if( isset( $arrValues['cc_avg_amount_per_transaction'] ) && $boolDirectSet ) $this->set( 'm_fltCcAvgAmountPerTransaction', trim( $arrValues['cc_avg_amount_per_transaction'] ) ); elseif( isset( $arrValues['cc_avg_amount_per_transaction'] ) ) $this->setCcAvgAmountPerTransaction( $arrValues['cc_avg_amount_per_transaction'] );
		if( isset( $arrValues['cc_max_amount_per_transaction'] ) && $boolDirectSet ) $this->set( 'm_fltCcMaxAmountPerTransaction', trim( $arrValues['cc_max_amount_per_transaction'] ) ); elseif( isset( $arrValues['cc_max_amount_per_transaction'] ) ) $this->setCcMaxAmountPerTransaction( $arrValues['cc_max_amount_per_transaction'] );
		if( isset( $arrValues['cc_max_amount_per_month'] ) && $boolDirectSet ) $this->set( 'm_fltCcMaxAmountPerMonth', trim( $arrValues['cc_max_amount_per_month'] ) ); elseif( isset( $arrValues['cc_max_amount_per_month'] ) ) $this->setCcMaxAmountPerMonth( $arrValues['cc_max_amount_per_month'] );
		if( isset( $arrValues['have_previous_merchant_account'] ) && $boolDirectSet ) $this->set( 'm_intHavePreviousMerchantAccount', trim( $arrValues['have_previous_merchant_account'] ) ); elseif( isset( $arrValues['have_previous_merchant_account'] ) ) $this->setHavePreviousMerchantAccount( $arrValues['have_previous_merchant_account'] );
		if( isset( $arrValues['previous_merchant_account_info'] ) && $boolDirectSet ) $this->set( 'm_strPreviousMerchantAccountInfo', trim( stripcslashes( $arrValues['previous_merchant_account_info'] ) ) ); elseif( isset( $arrValues['previous_merchant_account_info'] ) ) $this->setPreviousMerchantAccountInfo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['previous_merchant_account_info'] ) : $arrValues['previous_merchant_account_info'] );
		if( isset( $arrValues['instructions_for_echo'] ) && $boolDirectSet ) $this->set( 'm_strInstructionsForEcho', trim( stripcslashes( $arrValues['instructions_for_echo'] ) ) ); elseif( isset( $arrValues['instructions_for_echo'] ) ) $this->setInstructionsForEcho( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['instructions_for_echo'] ) : $arrValues['instructions_for_echo'] );
		if( isset( $arrValues['is_fee_managed'] ) && $boolDirectSet ) $this->set( 'm_intIsFeeManaged', trim( $arrValues['is_fee_managed'] ) ); elseif( isset( $arrValues['is_fee_managed'] ) ) $this->setIsFeeManaged( $arrValues['is_fee_managed'] );
		if( isset( $arrValues['photos_required'] ) && $boolDirectSet ) $this->set( 'm_intPhotosRequired', trim( $arrValues['photos_required'] ) ); elseif( isset( $arrValues['photos_required'] ) ) $this->setPhotosRequired( $arrValues['photos_required'] );
		if( isset( $arrValues['discover_pin'] ) && $boolDirectSet ) $this->set( 'm_strDiscoverPin', trim( stripcslashes( $arrValues['discover_pin'] ) ) ); elseif( isset( $arrValues['discover_pin'] ) ) $this->setDiscoverPin( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['discover_pin'] ) : $arrValues['discover_pin'] );
		if( isset( $arrValues['amex_pin'] ) && $boolDirectSet ) $this->set( 'm_strAmexPin', trim( stripcslashes( $arrValues['amex_pin'] ) ) ); elseif( isset( $arrValues['amex_pin'] ) ) $this->setAmexPin( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['amex_pin'] ) : $arrValues['amex_pin'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( stripcslashes( $arrValues['username'] ) ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username'] ) : $arrValues['username'] );
		if( isset( $arrValues['password'] ) && $boolDirectSet ) $this->set( 'm_strPassword', trim( stripcslashes( $arrValues['password'] ) ) ); elseif( isset( $arrValues['password'] ) ) $this->setPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password'] ) : $arrValues['password'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMerchantAccountApplicationTypesId( $intMerchantAccountApplicationTypesId ) {
		$this->set( 'm_intMerchantAccountApplicationTypesId', CStrings::strToIntDef( $intMerchantAccountApplicationTypesId, NULL, false ) );
	}

	public function getMerchantAccountApplicationTypesId() {
		return $this->m_intMerchantAccountApplicationTypesId;
	}

	public function sqlMerchantAccountApplicationTypesId() {
		return ( true == isset( $this->m_intMerchantAccountApplicationTypesId ) ) ? ( string ) $this->m_intMerchantAccountApplicationTypesId : '1';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? '\'' . addslashes( $this->m_strCompanyName ) . '\'' : 'NULL';
	}

	public function setCompanyDbaName( $strCompanyDbaName ) {
		$this->set( 'm_strCompanyDbaName', CStrings::strTrimDef( $strCompanyDbaName, 100, NULL, true ) );
	}

	public function getCompanyDbaName() {
		return $this->m_strCompanyDbaName;
	}

	public function sqlCompanyDbaName() {
		return ( true == isset( $this->m_strCompanyDbaName ) ) ? '\'' . addslashes( $this->m_strCompanyDbaName ) . '\'' : 'NULL';
	}

	public function setCompanyStreetLine1( $strCompanyStreetLine1 ) {
		$this->set( 'm_strCompanyStreetLine1', CStrings::strTrimDef( $strCompanyStreetLine1, 100, NULL, true ) );
	}

	public function getCompanyStreetLine1() {
		return $this->m_strCompanyStreetLine1;
	}

	public function sqlCompanyStreetLine1() {
		return ( true == isset( $this->m_strCompanyStreetLine1 ) ) ? '\'' . addslashes( $this->m_strCompanyStreetLine1 ) . '\'' : 'NULL';
	}

	public function setCompanyCity( $strCompanyCity ) {
		$this->set( 'm_strCompanyCity', CStrings::strTrimDef( $strCompanyCity, 50, NULL, true ) );
	}

	public function getCompanyCity() {
		return $this->m_strCompanyCity;
	}

	public function sqlCompanyCity() {
		return ( true == isset( $this->m_strCompanyCity ) ) ? '\'' . addslashes( $this->m_strCompanyCity ) . '\'' : 'NULL';
	}

	public function setCompanyStateCode( $strCompanyStateCode ) {
		$this->set( 'm_strCompanyStateCode', CStrings::strTrimDef( $strCompanyStateCode, 2, NULL, true ) );
	}

	public function getCompanyStateCode() {
		return $this->m_strCompanyStateCode;
	}

	public function sqlCompanyStateCode() {
		return ( true == isset( $this->m_strCompanyStateCode ) ) ? '\'' . addslashes( $this->m_strCompanyStateCode ) . '\'' : 'NULL';
	}

	public function setCompanyPostalCode( $strCompanyPostalCode ) {
		$this->set( 'm_strCompanyPostalCode', CStrings::strTrimDef( $strCompanyPostalCode, 20, NULL, true ) );
	}

	public function getCompanyPostalCode() {
		return $this->m_strCompanyPostalCode;
	}

	public function sqlCompanyPostalCode() {
		return ( true == isset( $this->m_strCompanyPostalCode ) ) ? '\'' . addslashes( $this->m_strCompanyPostalCode ) . '\'' : 'NULL';
	}

	public function setCompanyCountryCode( $strCompanyCountryCode ) {
		$this->set( 'm_strCompanyCountryCode', CStrings::strTrimDef( $strCompanyCountryCode, 2, NULL, true ) );
	}

	public function getCompanyCountryCode() {
		return $this->m_strCompanyCountryCode;
	}

	public function sqlCompanyCountryCode() {
		return ( true == isset( $this->m_strCompanyCountryCode ) ) ? '\'' . addslashes( $this->m_strCompanyCountryCode ) . '\'' : '\'US\'';
	}

	public function setCompanyTaxNumberEncrypted( $strCompanyTaxNumberEncrypted ) {
		$this->set( 'm_strCompanyTaxNumberEncrypted', CStrings::strTrimDef( $strCompanyTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getCompanyTaxNumberEncrypted() {
		return $this->m_strCompanyTaxNumberEncrypted;
	}

	public function sqlCompanyTaxNumberEncrypted() {
		return ( true == isset( $this->m_strCompanyTaxNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCompanyTaxNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setCompanyTaxNumberMasked( $strCompanyTaxNumberMasked ) {
		$this->set( 'm_strCompanyTaxNumberMasked', CStrings::strTrimDef( $strCompanyTaxNumberMasked, 240, NULL, true ) );
	}

	public function getCompanyTaxNumberMasked() {
		return $this->m_strCompanyTaxNumberMasked;
	}

	public function sqlCompanyTaxNumberMasked() {
		return ( true == isset( $this->m_strCompanyTaxNumberMasked ) ) ? '\'' . addslashes( $this->m_strCompanyTaxNumberMasked ) . '\'' : 'NULL';
	}

	public function setCompanyPhoneNumber( $strCompanyPhoneNumber ) {
		$this->set( 'm_strCompanyPhoneNumber', CStrings::strTrimDef( $strCompanyPhoneNumber, 30, NULL, true ) );
	}

	public function getCompanyPhoneNumber() {
		return $this->m_strCompanyPhoneNumber;
	}

	public function sqlCompanyPhoneNumber() {
		return ( true == isset( $this->m_strCompanyPhoneNumber ) ) ? '\'' . addslashes( $this->m_strCompanyPhoneNumber ) . '\'' : 'NULL';
	}

	public function setCompanyFaxNumber( $strCompanyFaxNumber ) {
		$this->set( 'm_strCompanyFaxNumber', CStrings::strTrimDef( $strCompanyFaxNumber, 30, NULL, true ) );
	}

	public function getCompanyFaxNumber() {
		return $this->m_strCompanyFaxNumber;
	}

	public function sqlCompanyFaxNumber() {
		return ( true == isset( $this->m_strCompanyFaxNumber ) ) ? '\'' . addslashes( $this->m_strCompanyFaxNumber ) . '\'' : 'NULL';
	}

	public function setOwnershipTypeId( $intOwnershipTypeId ) {
		$this->set( 'm_intOwnershipTypeId', CStrings::strToIntDef( $intOwnershipTypeId, NULL, false ) );
	}

	public function getOwnershipTypeId() {
		return $this->m_intOwnershipTypeId;
	}

	public function sqlOwnershipTypeId() {
		return ( true == isset( $this->m_intOwnershipTypeId ) ) ? ( string ) $this->m_intOwnershipTypeId : 'NULL';
	}

	public function setNumberOfOwners( $intNumberOfOwners ) {
		$this->set( 'm_intNumberOfOwners', CStrings::strToIntDef( $intNumberOfOwners, NULL, false ) );
	}

	public function getNumberOfOwners() {
		return $this->m_intNumberOfOwners;
	}

	public function sqlNumberOfOwners() {
		return ( true == isset( $this->m_intNumberOfOwners ) ) ? ( string ) $this->m_intNumberOfOwners : '1';
	}

	public function setNumberOfLocations( $intNumberOfLocations ) {
		$this->set( 'm_intNumberOfLocations', CStrings::strToIntDef( $intNumberOfLocations, NULL, false ) );
	}

	public function getNumberOfLocations() {
		return $this->m_intNumberOfLocations;
	}

	public function sqlNumberOfLocations() {
		return ( true == isset( $this->m_intNumberOfLocations ) ) ? ( string ) $this->m_intNumberOfLocations : 'NULL';
	}

	public function setTypeOfBusiness( $strTypeOfBusiness ) {
		$this->set( 'm_strTypeOfBusiness', CStrings::strTrimDef( $strTypeOfBusiness, 240, NULL, true ) );
	}

	public function getTypeOfBusiness() {
		return $this->m_strTypeOfBusiness;
	}

	public function sqlTypeOfBusiness() {
		return ( true == isset( $this->m_strTypeOfBusiness ) ) ? '\'' . addslashes( $this->m_strTypeOfBusiness ) . '\'' : 'NULL';
	}

	public function setAgeOfBusiness( $strAgeOfBusiness ) {
		$this->set( 'm_strAgeOfBusiness', CStrings::strTrimDef( $strAgeOfBusiness, 240, NULL, true ) );
	}

	public function getAgeOfBusiness() {
		return $this->m_strAgeOfBusiness;
	}

	public function sqlAgeOfBusiness() {
		return ( true == isset( $this->m_strAgeOfBusiness ) ) ? '\'' . addslashes( $this->m_strAgeOfBusiness ) . '\'' : 'NULL';
	}

	public function setDateBusinessAcquired( $strDateBusinessAcquired ) {
		$this->set( 'm_strDateBusinessAcquired', CStrings::strTrimDef( $strDateBusinessAcquired, -1, NULL, true ) );
	}

	public function getDateBusinessAcquired() {
		return $this->m_strDateBusinessAcquired;
	}

	public function sqlDateBusinessAcquired() {
		return ( true == isset( $this->m_strDateBusinessAcquired ) ) ? '\'' . $this->m_strDateBusinessAcquired . '\'' : 'NULL';
	}

	public function setBilltoStreetLine1( $strBilltoStreetLine1 ) {
		$this->set( 'm_strBilltoStreetLine1', CStrings::strTrimDef( $strBilltoStreetLine1, 100, NULL, true ) );
	}

	public function getBilltoStreetLine1() {
		return $this->m_strBilltoStreetLine1;
	}

	public function sqlBilltoStreetLine1() {
		return ( true == isset( $this->m_strBilltoStreetLine1 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine1 ) . '\'' : 'NULL';
	}

	public function setBilltoCity( $strBilltoCity ) {
		$this->set( 'm_strBilltoCity', CStrings::strTrimDef( $strBilltoCity, 50, NULL, true ) );
	}

	public function getBilltoCity() {
		return $this->m_strBilltoCity;
	}

	public function sqlBilltoCity() {
		return ( true == isset( $this->m_strBilltoCity ) ) ? '\'' . addslashes( $this->m_strBilltoCity ) . '\'' : 'NULL';
	}

	public function setBilltoStateCode( $strBilltoStateCode ) {
		$this->set( 'm_strBilltoStateCode', CStrings::strTrimDef( $strBilltoStateCode, 2, NULL, true ) );
	}

	public function getBilltoStateCode() {
		return $this->m_strBilltoStateCode;
	}

	public function sqlBilltoStateCode() {
		return ( true == isset( $this->m_strBilltoStateCode ) ) ? '\'' . addslashes( $this->m_strBilltoStateCode ) . '\'' : 'NULL';
	}

	public function setBilltoPostalCode( $strBilltoPostalCode ) {
		$this->set( 'm_strBilltoPostalCode', CStrings::strTrimDef( $strBilltoPostalCode, 20, NULL, true ) );
	}

	public function getBilltoPostalCode() {
		return $this->m_strBilltoPostalCode;
	}

	public function sqlBilltoPostalCode() {
		return ( true == isset( $this->m_strBilltoPostalCode ) ) ? '\'' . addslashes( $this->m_strBilltoPostalCode ) . '\'' : 'NULL';
	}

	public function setBilltoCountryCode( $strBilltoCountryCode ) {
		$this->set( 'm_strBilltoCountryCode', CStrings::strTrimDef( $strBilltoCountryCode, 2, NULL, true ) );
	}

	public function getBilltoCountryCode() {
		return $this->m_strBilltoCountryCode;
	}

	public function sqlBilltoCountryCode() {
		return ( true == isset( $this->m_strBilltoCountryCode ) ) ? '\'' . addslashes( $this->m_strBilltoCountryCode ) . '\'' : '\'US\'';
	}

	public function setContactName( $strContactName ) {
		$this->set( 'm_strContactName', CStrings::strTrimDef( $strContactName, 100, NULL, true ) );
	}

	public function getContactName() {
		return $this->m_strContactName;
	}

	public function sqlContactName() {
		return ( true == isset( $this->m_strContactName ) ) ? '\'' . addslashes( $this->m_strContactName ) . '\'' : 'NULL';
	}

	public function setContactDayTimePhone( $strContactDayTimePhone ) {
		$this->set( 'm_strContactDayTimePhone', CStrings::strTrimDef( $strContactDayTimePhone, 30, NULL, true ) );
	}

	public function getContactDayTimePhone() {
		return $this->m_strContactDayTimePhone;
	}

	public function sqlContactDayTimePhone() {
		return ( true == isset( $this->m_strContactDayTimePhone ) ) ? '\'' . addslashes( $this->m_strContactDayTimePhone ) . '\'' : 'NULL';
	}

	public function setContactEveningPhone( $strContactEveningPhone ) {
		$this->set( 'm_strContactEveningPhone', CStrings::strTrimDef( $strContactEveningPhone, 30, NULL, true ) );
	}

	public function getContactEveningPhone() {
		return $this->m_strContactEveningPhone;
	}

	public function sqlContactEveningPhone() {
		return ( true == isset( $this->m_strContactEveningPhone ) ) ? '\'' . addslashes( $this->m_strContactEveningPhone ) . '\'' : 'NULL';
	}

	public function setContactEmail( $strContactEmail ) {
		$this->set( 'm_strContactEmail', CStrings::strTrimDef( $strContactEmail, 240, NULL, true ) );
	}

	public function getContactEmail() {
		return $this->m_strContactEmail;
	}

	public function sqlContactEmail() {
		return ( true == isset( $this->m_strContactEmail ) ) ? '\'' . addslashes( $this->m_strContactEmail ) . '\'' : 'NULL';
	}

	public function setWebsiteUrl( $strWebsiteUrl ) {
		$this->set( 'm_strWebsiteUrl', CStrings::strTrimDef( $strWebsiteUrl, 4096, NULL, true ) );
	}

	public function getWebsiteUrl() {
		return $this->m_strWebsiteUrl;
	}

	public function sqlWebsiteUrl() {
		return ( true == isset( $this->m_strWebsiteUrl ) ) ? '\'' . addslashes( $this->m_strWebsiteUrl ) . '\'' : 'NULL';
	}

	public function setVpName( $strVpName ) {
		$this->set( 'm_strVpName', CStrings::strTrimDef( $strVpName, 100, NULL, true ) );
	}

	public function getVpName() {
		return $this->m_strVpName;
	}

	public function sqlVpName() {
		return ( true == isset( $this->m_strVpName ) ) ? '\'' . addslashes( $this->m_strVpName ) . '\'' : 'NULL';
	}

	public function setVpTitle( $strVpTitle ) {
		$this->set( 'm_strVpTitle', CStrings::strTrimDef( $strVpTitle, 50, NULL, true ) );
	}

	public function getVpTitle() {
		return $this->m_strVpTitle;
	}

	public function sqlVpTitle() {
		return ( true == isset( $this->m_strVpTitle ) ) ? '\'' . addslashes( $this->m_strVpTitle ) . '\'' : 'NULL';
	}

	public function setVpOwnership( $strVpOwnership ) {
		$this->set( 'm_strVpOwnership', CStrings::strTrimDef( $strVpOwnership, 240, NULL, true ) );
	}

	public function getVpOwnership() {
		return $this->m_strVpOwnership;
	}

	public function sqlVpOwnership() {
		return ( true == isset( $this->m_strVpOwnership ) ) ? '\'' . addslashes( $this->m_strVpOwnership ) . '\'' : 'NULL';
	}

	public function setVpTaxNumberEncrypted( $strVpTaxNumberEncrypted ) {
		$this->set( 'm_strVpTaxNumberEncrypted', CStrings::strTrimDef( $strVpTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getVpTaxNumberEncrypted() {
		return $this->m_strVpTaxNumberEncrypted;
	}

	public function sqlVpTaxNumberEncrypted() {
		return ( true == isset( $this->m_strVpTaxNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strVpTaxNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setVpTaxNumberMasked( $strVpTaxNumberMasked ) {
		$this->set( 'm_strVpTaxNumberMasked', CStrings::strTrimDef( $strVpTaxNumberMasked, 240, NULL, true ) );
	}

	public function getVpTaxNumberMasked() {
		return $this->m_strVpTaxNumberMasked;
	}

	public function sqlVpTaxNumberMasked() {
		return ( true == isset( $this->m_strVpTaxNumberMasked ) ) ? '\'' . addslashes( $this->m_strVpTaxNumberMasked ) . '\'' : 'NULL';
	}

	public function setVpDob( $strVpDob ) {
		$this->set( 'm_strVpDob', CStrings::strTrimDef( $strVpDob, -1, NULL, true ) );
	}

	public function getVpDob() {
		return $this->m_strVpDob;
	}

	public function sqlVpDob() {
		return ( true == isset( $this->m_strVpDob ) ) ? '\'' . $this->m_strVpDob . '\'' : 'NULL';
	}

	public function setVpStreetLine1( $strVpStreetLine1 ) {
		$this->set( 'm_strVpStreetLine1', CStrings::strTrimDef( $strVpStreetLine1, 100, NULL, true ) );
	}

	public function getVpStreetLine1() {
		return $this->m_strVpStreetLine1;
	}

	public function sqlVpStreetLine1() {
		return ( true == isset( $this->m_strVpStreetLine1 ) ) ? '\'' . addslashes( $this->m_strVpStreetLine1 ) . '\'' : 'NULL';
	}

	public function setVpCity( $strVpCity ) {
		$this->set( 'm_strVpCity', CStrings::strTrimDef( $strVpCity, 50, NULL, true ) );
	}

	public function getVpCity() {
		return $this->m_strVpCity;
	}

	public function sqlVpCity() {
		return ( true == isset( $this->m_strVpCity ) ) ? '\'' . addslashes( $this->m_strVpCity ) . '\'' : 'NULL';
	}

	public function setVpStateCode( $strVpStateCode ) {
		$this->set( 'm_strVpStateCode', CStrings::strTrimDef( $strVpStateCode, 2, NULL, true ) );
	}

	public function getVpStateCode() {
		return $this->m_strVpStateCode;
	}

	public function sqlVpStateCode() {
		return ( true == isset( $this->m_strVpStateCode ) ) ? '\'' . addslashes( $this->m_strVpStateCode ) . '\'' : 'NULL';
	}

	public function setVpPostalCode( $strVpPostalCode ) {
		$this->set( 'm_strVpPostalCode', CStrings::strTrimDef( $strVpPostalCode, 20, NULL, true ) );
	}

	public function getVpPostalCode() {
		return $this->m_strVpPostalCode;
	}

	public function sqlVpPostalCode() {
		return ( true == isset( $this->m_strVpPostalCode ) ) ? '\'' . addslashes( $this->m_strVpPostalCode ) . '\'' : 'NULL';
	}

	public function setVpResidence( $strVpResidence ) {
		$this->set( 'm_strVpResidence', CStrings::strTrimDef( $strVpResidence, 240, NULL, true ) );
	}

	public function getVpResidence() {
		return $this->m_strVpResidence;
	}

	public function sqlVpResidence() {
		return ( true == isset( $this->m_strVpResidence ) ) ? '\'' . addslashes( $this->m_strVpResidence ) . '\'' : 'NULL';
	}

	public function setVpRentOrOwn( $strVpRentOrOwn ) {
		$this->set( 'm_strVpRentOrOwn', CStrings::strTrimDef( $strVpRentOrOwn, 240, NULL, true ) );
	}

	public function getVpRentOrOwn() {
		return $this->m_strVpRentOrOwn;
	}

	public function sqlVpRentOrOwn() {
		return ( true == isset( $this->m_strVpRentOrOwn ) ) ? '\'' . addslashes( $this->m_strVpRentOrOwn ) . '\'' : 'NULL';
	}

	public function setVpHomePhone( $strVpHomePhone ) {
		$this->set( 'm_strVpHomePhone', CStrings::strTrimDef( $strVpHomePhone, 30, NULL, true ) );
	}

	public function getVpHomePhone() {
		return $this->m_strVpHomePhone;
	}

	public function sqlVpHomePhone() {
		return ( true == isset( $this->m_strVpHomePhone ) ) ? '\'' . addslashes( $this->m_strVpHomePhone ) . '\'' : 'NULL';
	}

	public function setVpPreviousAddress( $strVpPreviousAddress ) {
		$this->set( 'm_strVpPreviousAddress', CStrings::strTrimDef( $strVpPreviousAddress, -1, NULL, true ) );
	}

	public function getVpPreviousAddress() {
		return $this->m_strVpPreviousAddress;
	}

	public function sqlVpPreviousAddress() {
		return ( true == isset( $this->m_strVpPreviousAddress ) ) ? '\'' . addslashes( $this->m_strVpPreviousAddress ) . '\'' : 'NULL';
	}

	public function setHavePoorCredit( $intHavePoorCredit ) {
		$this->set( 'm_intHavePoorCredit', CStrings::strToIntDef( $intHavePoorCredit, NULL, false ) );
	}

	public function getHavePoorCredit() {
		return $this->m_intHavePoorCredit;
	}

	public function sqlHavePoorCredit() {
		return ( true == isset( $this->m_intHavePoorCredit ) ) ? ( string ) $this->m_intHavePoorCredit : '0';
	}

	public function setApplyingForAch( $intApplyingForAch ) {
		$this->set( 'm_intApplyingForAch', CStrings::strToIntDef( $intApplyingForAch, NULL, false ) );
	}

	public function getApplyingForAch() {
		return $this->m_intApplyingForAch;
	}

	public function sqlApplyingForAch() {
		return ( true == isset( $this->m_intApplyingForAch ) ) ? ( string ) $this->m_intApplyingForAch : '0';
	}

	public function setApplyingForCreditCard( $intApplyingForCreditCard ) {
		$this->set( 'm_intApplyingForCreditCard', CStrings::strToIntDef( $intApplyingForCreditCard, NULL, false ) );
	}

	public function getApplyingForCreditCard() {
		return $this->m_intApplyingForCreditCard;
	}

	public function sqlApplyingForCreditCard() {
		return ( true == isset( $this->m_intApplyingForCreditCard ) ) ? ( string ) $this->m_intApplyingForCreditCard : '0';
	}

	public function setApplyingForDiscover( $intApplyingForDiscover ) {
		$this->set( 'm_intApplyingForDiscover', CStrings::strToIntDef( $intApplyingForDiscover, NULL, false ) );
	}

	public function getApplyingForDiscover() {
		return $this->m_intApplyingForDiscover;
	}

	public function sqlApplyingForDiscover() {
		return ( true == isset( $this->m_intApplyingForDiscover ) ) ? ( string ) $this->m_intApplyingForDiscover : '0';
	}

	public function setApplyingForAmericanExpress( $intApplyingForAmericanExpress ) {
		$this->set( 'm_intApplyingForAmericanExpress', CStrings::strToIntDef( $intApplyingForAmericanExpress, NULL, false ) );
	}

	public function getApplyingForAmericanExpress() {
		return $this->m_intApplyingForAmericanExpress;
	}

	public function sqlApplyingForAmericanExpress() {
		return ( true == isset( $this->m_intApplyingForAmericanExpress ) ) ? ( string ) $this->m_intApplyingForAmericanExpress : '0';
	}

	public function setNumOfAccountsRequested( $intNumOfAccountsRequested ) {
		$this->set( 'm_intNumOfAccountsRequested', CStrings::strToIntDef( $intNumOfAccountsRequested, NULL, false ) );
	}

	public function getNumOfAccountsRequested() {
		return $this->m_intNumOfAccountsRequested;
	}

	public function sqlNumOfAccountsRequested() {
		return ( true == isset( $this->m_intNumOfAccountsRequested ) ) ? ( string ) $this->m_intNumOfAccountsRequested : 'NULL';
	}

	public function setAchMaxTransactionsPerDay( $intAchMaxTransactionsPerDay ) {
		$this->set( 'm_intAchMaxTransactionsPerDay', CStrings::strToIntDef( $intAchMaxTransactionsPerDay, NULL, false ) );
	}

	public function getAchMaxTransactionsPerDay() {
		return $this->m_intAchMaxTransactionsPerDay;
	}

	public function sqlAchMaxTransactionsPerDay() {
		return ( true == isset( $this->m_intAchMaxTransactionsPerDay ) ) ? ( string ) $this->m_intAchMaxTransactionsPerDay : 'NULL';
	}

	public function setAchMaxAmountPerTransaction( $fltAchMaxAmountPerTransaction ) {
		$this->set( 'm_fltAchMaxAmountPerTransaction', CStrings::strToFloatDef( $fltAchMaxAmountPerTransaction, NULL, false, 4 ) );
	}

	public function getAchMaxAmountPerTransaction() {
		return $this->m_fltAchMaxAmountPerTransaction;
	}

	public function sqlAchMaxAmountPerTransaction() {
		return ( true == isset( $this->m_fltAchMaxAmountPerTransaction ) ) ? ( string ) $this->m_fltAchMaxAmountPerTransaction : 'NULL';
	}

	public function setAchAvgAmountPerTransaction( $fltAchAvgAmountPerTransaction ) {
		$this->set( 'm_fltAchAvgAmountPerTransaction', CStrings::strToFloatDef( $fltAchAvgAmountPerTransaction, NULL, false, 4 ) );
	}

	public function getAchAvgAmountPerTransaction() {
		return $this->m_fltAchAvgAmountPerTransaction;
	}

	public function sqlAchAvgAmountPerTransaction() {
		return ( true == isset( $this->m_fltAchAvgAmountPerTransaction ) ) ? ( string ) $this->m_fltAchAvgAmountPerTransaction : 'NULL';
	}

	public function setAchMaxAmountPerDay( $fltAchMaxAmountPerDay ) {
		$this->set( 'm_fltAchMaxAmountPerDay', CStrings::strToFloatDef( $fltAchMaxAmountPerDay, NULL, false, 4 ) );
	}

	public function getAchMaxAmountPerDay() {
		return $this->m_fltAchMaxAmountPerDay;
	}

	public function sqlAchMaxAmountPerDay() {
		return ( true == isset( $this->m_fltAchMaxAmountPerDay ) ) ? ( string ) $this->m_fltAchMaxAmountPerDay : 'NULL';
	}

	public function setAchMaxAmountPerMonth( $fltAchMaxAmountPerMonth ) {
		$this->set( 'm_fltAchMaxAmountPerMonth', CStrings::strToFloatDef( $fltAchMaxAmountPerMonth, NULL, false, 4 ) );
	}

	public function getAchMaxAmountPerMonth() {
		return $this->m_fltAchMaxAmountPerMonth;
	}

	public function sqlAchMaxAmountPerMonth() {
		return ( true == isset( $this->m_fltAchMaxAmountPerMonth ) ) ? ( string ) $this->m_fltAchMaxAmountPerMonth : 'NULL';
	}

	public function setCcAvgAmountPerTransaction( $fltCcAvgAmountPerTransaction ) {
		$this->set( 'm_fltCcAvgAmountPerTransaction', CStrings::strToFloatDef( $fltCcAvgAmountPerTransaction, NULL, false, 4 ) );
	}

	public function getCcAvgAmountPerTransaction() {
		return $this->m_fltCcAvgAmountPerTransaction;
	}

	public function sqlCcAvgAmountPerTransaction() {
		return ( true == isset( $this->m_fltCcAvgAmountPerTransaction ) ) ? ( string ) $this->m_fltCcAvgAmountPerTransaction : 'NULL';
	}

	public function setCcMaxAmountPerTransaction( $fltCcMaxAmountPerTransaction ) {
		$this->set( 'm_fltCcMaxAmountPerTransaction', CStrings::strToFloatDef( $fltCcMaxAmountPerTransaction, NULL, false, 4 ) );
	}

	public function getCcMaxAmountPerTransaction() {
		return $this->m_fltCcMaxAmountPerTransaction;
	}

	public function sqlCcMaxAmountPerTransaction() {
		return ( true == isset( $this->m_fltCcMaxAmountPerTransaction ) ) ? ( string ) $this->m_fltCcMaxAmountPerTransaction : 'NULL';
	}

	public function setCcMaxAmountPerMonth( $fltCcMaxAmountPerMonth ) {
		$this->set( 'm_fltCcMaxAmountPerMonth', CStrings::strToFloatDef( $fltCcMaxAmountPerMonth, NULL, false, 4 ) );
	}

	public function getCcMaxAmountPerMonth() {
		return $this->m_fltCcMaxAmountPerMonth;
	}

	public function sqlCcMaxAmountPerMonth() {
		return ( true == isset( $this->m_fltCcMaxAmountPerMonth ) ) ? ( string ) $this->m_fltCcMaxAmountPerMonth : 'NULL';
	}

	public function setHavePreviousMerchantAccount( $intHavePreviousMerchantAccount ) {
		$this->set( 'm_intHavePreviousMerchantAccount', CStrings::strToIntDef( $intHavePreviousMerchantAccount, NULL, false ) );
	}

	public function getHavePreviousMerchantAccount() {
		return $this->m_intHavePreviousMerchantAccount;
	}

	public function sqlHavePreviousMerchantAccount() {
		return ( true == isset( $this->m_intHavePreviousMerchantAccount ) ) ? ( string ) $this->m_intHavePreviousMerchantAccount : '0';
	}

	public function setPreviousMerchantAccountInfo( $strPreviousMerchantAccountInfo ) {
		$this->set( 'm_strPreviousMerchantAccountInfo', CStrings::strTrimDef( $strPreviousMerchantAccountInfo, -1, NULL, true ) );
	}

	public function getPreviousMerchantAccountInfo() {
		return $this->m_strPreviousMerchantAccountInfo;
	}

	public function sqlPreviousMerchantAccountInfo() {
		return ( true == isset( $this->m_strPreviousMerchantAccountInfo ) ) ? '\'' . addslashes( $this->m_strPreviousMerchantAccountInfo ) . '\'' : 'NULL';
	}

	public function setInstructionsForEcho( $strInstructionsForEcho ) {
		$this->set( 'm_strInstructionsForEcho', CStrings::strTrimDef( $strInstructionsForEcho, -1, NULL, true ) );
	}

	public function getInstructionsForEcho() {
		return $this->m_strInstructionsForEcho;
	}

	public function sqlInstructionsForEcho() {
		return ( true == isset( $this->m_strInstructionsForEcho ) ) ? '\'' . addslashes( $this->m_strInstructionsForEcho ) . '\'' : 'NULL';
	}

	public function setIsFeeManaged( $intIsFeeManaged ) {
		$this->set( 'm_intIsFeeManaged', CStrings::strToIntDef( $intIsFeeManaged, NULL, false ) );
	}

	public function getIsFeeManaged() {
		return $this->m_intIsFeeManaged;
	}

	public function sqlIsFeeManaged() {
		return ( true == isset( $this->m_intIsFeeManaged ) ) ? ( string ) $this->m_intIsFeeManaged : '0';
	}

	public function setPhotosRequired( $intPhotosRequired ) {
		$this->set( 'm_intPhotosRequired', CStrings::strToIntDef( $intPhotosRequired, NULL, false ) );
	}

	public function getPhotosRequired() {
		return $this->m_intPhotosRequired;
	}

	public function sqlPhotosRequired() {
		return ( true == isset( $this->m_intPhotosRequired ) ) ? ( string ) $this->m_intPhotosRequired : '0';
	}

	public function setDiscoverPin( $strDiscoverPin ) {
		$this->set( 'm_strDiscoverPin', CStrings::strTrimDef( $strDiscoverPin, 50, NULL, true ) );
	}

	public function getDiscoverPin() {
		return $this->m_strDiscoverPin;
	}

	public function sqlDiscoverPin() {
		return ( true == isset( $this->m_strDiscoverPin ) ) ? '\'' . addslashes( $this->m_strDiscoverPin ) . '\'' : 'NULL';
	}

	public function setAmexPin( $strAmexPin ) {
		$this->set( 'm_strAmexPin', CStrings::strTrimDef( $strAmexPin, 50, NULL, true ) );
	}

	public function getAmexPin() {
		return $this->m_strAmexPin;
	}

	public function sqlAmexPin() {
		return ( true == isset( $this->m_strAmexPin ) ) ? '\'' . addslashes( $this->m_strAmexPin ) . '\'' : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 240, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? '\'' . addslashes( $this->m_strUsername ) . '\'' : 'NULL';
	}

	public function setPassword( $strPassword ) {
		$this->set( 'm_strPassword', CStrings::strTrimDef( $strPassword, 240, NULL, true ) );
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function sqlPassword() {
		return ( true == isset( $this->m_strPassword ) ) ? '\'' . addslashes( $this->m_strPassword ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, merchant_account_application_types_id, company_name, company_dba_name, company_street_line1, company_city, company_state_code, company_postal_code, company_country_code, company_tax_number_encrypted, company_tax_number_masked, company_phone_number, company_fax_number, ownership_type_id, number_of_owners, number_of_locations, type_of_business, age_of_business, date_business_acquired, billto_street_line1, billto_city, billto_state_code, billto_postal_code, billto_country_code, contact_name, contact_day_time_phone, contact_evening_phone, contact_email, website_url, vp_name, vp_title, vp_ownership, vp_tax_number_encrypted, vp_tax_number_masked, vp_dob, vp_street_line1, vp_city, vp_state_code, vp_postal_code, vp_residence, vp_rent_or_own, vp_home_phone, vp_previous_address, have_poor_credit, applying_for_ach, applying_for_credit_card, applying_for_discover, applying_for_american_express, num_of_accounts_requested, ach_max_transactions_per_day, ach_max_amount_per_transaction, ach_avg_amount_per_transaction, ach_max_amount_per_day, ach_max_amount_per_month, cc_avg_amount_per_transaction, cc_max_amount_per_transaction, cc_max_amount_per_month, have_previous_merchant_account, previous_merchant_account_info, instructions_for_echo, is_fee_managed, photos_required, discover_pin, amex_pin, username, password, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlMerchantAccountApplicationTypesId() . ', ' .
 						$this->sqlCompanyName() . ', ' .
 						$this->sqlCompanyDbaName() . ', ' .
 						$this->sqlCompanyStreetLine1() . ', ' .
 						$this->sqlCompanyCity() . ', ' .
 						$this->sqlCompanyStateCode() . ', ' .
 						$this->sqlCompanyPostalCode() . ', ' .
 						$this->sqlCompanyCountryCode() . ', ' .
 						$this->sqlCompanyTaxNumberEncrypted() . ', ' .
 						$this->sqlCompanyTaxNumberMasked() . ', ' .
 						$this->sqlCompanyPhoneNumber() . ', ' .
 						$this->sqlCompanyFaxNumber() . ', ' .
 						$this->sqlOwnershipTypeId() . ', ' .
 						$this->sqlNumberOfOwners() . ', ' .
 						$this->sqlNumberOfLocations() . ', ' .
 						$this->sqlTypeOfBusiness() . ', ' .
 						$this->sqlAgeOfBusiness() . ', ' .
 						$this->sqlDateBusinessAcquired() . ', ' .
 						$this->sqlBilltoStreetLine1() . ', ' .
 						$this->sqlBilltoCity() . ', ' .
 						$this->sqlBilltoStateCode() . ', ' .
 						$this->sqlBilltoPostalCode() . ', ' .
 						$this->sqlBilltoCountryCode() . ', ' .
 						$this->sqlContactName() . ', ' .
 						$this->sqlContactDayTimePhone() . ', ' .
 						$this->sqlContactEveningPhone() . ', ' .
 						$this->sqlContactEmail() . ', ' .
 						$this->sqlWebsiteUrl() . ', ' .
 						$this->sqlVpName() . ', ' .
 						$this->sqlVpTitle() . ', ' .
 						$this->sqlVpOwnership() . ', ' .
 						$this->sqlVpTaxNumberEncrypted() . ', ' .
 						$this->sqlVpTaxNumberMasked() . ', ' .
 						$this->sqlVpDob() . ', ' .
 						$this->sqlVpStreetLine1() . ', ' .
 						$this->sqlVpCity() . ', ' .
 						$this->sqlVpStateCode() . ', ' .
 						$this->sqlVpPostalCode() . ', ' .
 						$this->sqlVpResidence() . ', ' .
 						$this->sqlVpRentOrOwn() . ', ' .
 						$this->sqlVpHomePhone() . ', ' .
 						$this->sqlVpPreviousAddress() . ', ' .
 						$this->sqlHavePoorCredit() . ', ' .
 						$this->sqlApplyingForAch() . ', ' .
 						$this->sqlApplyingForCreditCard() . ', ' .
 						$this->sqlApplyingForDiscover() . ', ' .
 						$this->sqlApplyingForAmericanExpress() . ', ' .
 						$this->sqlNumOfAccountsRequested() . ', ' .
 						$this->sqlAchMaxTransactionsPerDay() . ', ' .
 						$this->sqlAchMaxAmountPerTransaction() . ', ' .
 						$this->sqlAchAvgAmountPerTransaction() . ', ' .
 						$this->sqlAchMaxAmountPerDay() . ', ' .
 						$this->sqlAchMaxAmountPerMonth() . ', ' .
 						$this->sqlCcAvgAmountPerTransaction() . ', ' .
 						$this->sqlCcMaxAmountPerTransaction() . ', ' .
 						$this->sqlCcMaxAmountPerMonth() . ', ' .
 						$this->sqlHavePreviousMerchantAccount() . ', ' .
 						$this->sqlPreviousMerchantAccountInfo() . ', ' .
 						$this->sqlInstructionsForEcho() . ', ' .
 						$this->sqlIsFeeManaged() . ', ' .
 						$this->sqlPhotosRequired() . ', ' .
 						$this->sqlDiscoverPin() . ', ' .
 						$this->sqlAmexPin() . ', ' .
 						$this->sqlUsername() . ', ' .
 						$this->sqlPassword() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_account_application_types_id = ' . $this->sqlMerchantAccountApplicationTypesId() . ','; } elseif( true == array_key_exists( 'MerchantAccountApplicationTypesId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_account_application_types_id = ' . $this->sqlMerchantAccountApplicationTypesId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_dba_name = ' . $this->sqlCompanyDbaName() . ','; } elseif( true == array_key_exists( 'CompanyDbaName', $this->getChangedColumns() ) ) { $strSql .= ' company_dba_name = ' . $this->sqlCompanyDbaName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_street_line1 = ' . $this->sqlCompanyStreetLine1() . ','; } elseif( true == array_key_exists( 'CompanyStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' company_street_line1 = ' . $this->sqlCompanyStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_city = ' . $this->sqlCompanyCity() . ','; } elseif( true == array_key_exists( 'CompanyCity', $this->getChangedColumns() ) ) { $strSql .= ' company_city = ' . $this->sqlCompanyCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_state_code = ' . $this->sqlCompanyStateCode() . ','; } elseif( true == array_key_exists( 'CompanyStateCode', $this->getChangedColumns() ) ) { $strSql .= ' company_state_code = ' . $this->sqlCompanyStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_postal_code = ' . $this->sqlCompanyPostalCode() . ','; } elseif( true == array_key_exists( 'CompanyPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' company_postal_code = ' . $this->sqlCompanyPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_country_code = ' . $this->sqlCompanyCountryCode() . ','; } elseif( true == array_key_exists( 'CompanyCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' company_country_code = ' . $this->sqlCompanyCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_tax_number_encrypted = ' . $this->sqlCompanyTaxNumberEncrypted() . ','; } elseif( true == array_key_exists( 'CompanyTaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' company_tax_number_encrypted = ' . $this->sqlCompanyTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_tax_number_masked = ' . $this->sqlCompanyTaxNumberMasked() . ','; } elseif( true == array_key_exists( 'CompanyTaxNumberMasked', $this->getChangedColumns() ) ) { $strSql .= ' company_tax_number_masked = ' . $this->sqlCompanyTaxNumberMasked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_phone_number = ' . $this->sqlCompanyPhoneNumber() . ','; } elseif( true == array_key_exists( 'CompanyPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' company_phone_number = ' . $this->sqlCompanyPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_fax_number = ' . $this->sqlCompanyFaxNumber() . ','; } elseif( true == array_key_exists( 'CompanyFaxNumber', $this->getChangedColumns() ) ) { $strSql .= ' company_fax_number = ' . $this->sqlCompanyFaxNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ownership_type_id = ' . $this->sqlOwnershipTypeId() . ','; } elseif( true == array_key_exists( 'OwnershipTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ownership_type_id = ' . $this->sqlOwnershipTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_owners = ' . $this->sqlNumberOfOwners() . ','; } elseif( true == array_key_exists( 'NumberOfOwners', $this->getChangedColumns() ) ) { $strSql .= ' number_of_owners = ' . $this->sqlNumberOfOwners() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_locations = ' . $this->sqlNumberOfLocations() . ','; } elseif( true == array_key_exists( 'NumberOfLocations', $this->getChangedColumns() ) ) { $strSql .= ' number_of_locations = ' . $this->sqlNumberOfLocations() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' type_of_business = ' . $this->sqlTypeOfBusiness() . ','; } elseif( true == array_key_exists( 'TypeOfBusiness', $this->getChangedColumns() ) ) { $strSql .= ' type_of_business = ' . $this->sqlTypeOfBusiness() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' age_of_business = ' . $this->sqlAgeOfBusiness() . ','; } elseif( true == array_key_exists( 'AgeOfBusiness', $this->getChangedColumns() ) ) { $strSql .= ' age_of_business = ' . $this->sqlAgeOfBusiness() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_business_acquired = ' . $this->sqlDateBusinessAcquired() . ','; } elseif( true == array_key_exists( 'DateBusinessAcquired', $this->getChangedColumns() ) ) { $strSql .= ' date_business_acquired = ' . $this->sqlDateBusinessAcquired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line1 = ' . $this->sqlBilltoStreetLine1() . ','; } elseif( true == array_key_exists( 'BilltoStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line1 = ' . $this->sqlBilltoStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_city = ' . $this->sqlBilltoCity() . ','; } elseif( true == array_key_exists( 'BilltoCity', $this->getChangedColumns() ) ) { $strSql .= ' billto_city = ' . $this->sqlBilltoCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_state_code = ' . $this->sqlBilltoStateCode() . ','; } elseif( true == array_key_exists( 'BilltoStateCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_state_code = ' . $this->sqlBilltoStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_postal_code = ' . $this->sqlBilltoPostalCode() . ','; } elseif( true == array_key_exists( 'BilltoPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_postal_code = ' . $this->sqlBilltoPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_country_code = ' . $this->sqlBilltoCountryCode() . ','; } elseif( true == array_key_exists( 'BilltoCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_country_code = ' . $this->sqlBilltoCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_name = ' . $this->sqlContactName() . ','; } elseif( true == array_key_exists( 'ContactName', $this->getChangedColumns() ) ) { $strSql .= ' contact_name = ' . $this->sqlContactName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_day_time_phone = ' . $this->sqlContactDayTimePhone() . ','; } elseif( true == array_key_exists( 'ContactDayTimePhone', $this->getChangedColumns() ) ) { $strSql .= ' contact_day_time_phone = ' . $this->sqlContactDayTimePhone() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_evening_phone = ' . $this->sqlContactEveningPhone() . ','; } elseif( true == array_key_exists( 'ContactEveningPhone', $this->getChangedColumns() ) ) { $strSql .= ' contact_evening_phone = ' . $this->sqlContactEveningPhone() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_email = ' . $this->sqlContactEmail() . ','; } elseif( true == array_key_exists( 'ContactEmail', $this->getChangedColumns() ) ) { $strSql .= ' contact_email = ' . $this->sqlContactEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_url = ' . $this->sqlWebsiteUrl() . ','; } elseif( true == array_key_exists( 'WebsiteUrl', $this->getChangedColumns() ) ) { $strSql .= ' website_url = ' . $this->sqlWebsiteUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vp_name = ' . $this->sqlVpName() . ','; } elseif( true == array_key_exists( 'VpName', $this->getChangedColumns() ) ) { $strSql .= ' vp_name = ' . $this->sqlVpName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vp_title = ' . $this->sqlVpTitle() . ','; } elseif( true == array_key_exists( 'VpTitle', $this->getChangedColumns() ) ) { $strSql .= ' vp_title = ' . $this->sqlVpTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vp_ownership = ' . $this->sqlVpOwnership() . ','; } elseif( true == array_key_exists( 'VpOwnership', $this->getChangedColumns() ) ) { $strSql .= ' vp_ownership = ' . $this->sqlVpOwnership() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vp_tax_number_encrypted = ' . $this->sqlVpTaxNumberEncrypted() . ','; } elseif( true == array_key_exists( 'VpTaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' vp_tax_number_encrypted = ' . $this->sqlVpTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vp_tax_number_masked = ' . $this->sqlVpTaxNumberMasked() . ','; } elseif( true == array_key_exists( 'VpTaxNumberMasked', $this->getChangedColumns() ) ) { $strSql .= ' vp_tax_number_masked = ' . $this->sqlVpTaxNumberMasked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vp_dob = ' . $this->sqlVpDob() . ','; } elseif( true == array_key_exists( 'VpDob', $this->getChangedColumns() ) ) { $strSql .= ' vp_dob = ' . $this->sqlVpDob() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vp_street_line1 = ' . $this->sqlVpStreetLine1() . ','; } elseif( true == array_key_exists( 'VpStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' vp_street_line1 = ' . $this->sqlVpStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vp_city = ' . $this->sqlVpCity() . ','; } elseif( true == array_key_exists( 'VpCity', $this->getChangedColumns() ) ) { $strSql .= ' vp_city = ' . $this->sqlVpCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vp_state_code = ' . $this->sqlVpStateCode() . ','; } elseif( true == array_key_exists( 'VpStateCode', $this->getChangedColumns() ) ) { $strSql .= ' vp_state_code = ' . $this->sqlVpStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vp_postal_code = ' . $this->sqlVpPostalCode() . ','; } elseif( true == array_key_exists( 'VpPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' vp_postal_code = ' . $this->sqlVpPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vp_residence = ' . $this->sqlVpResidence() . ','; } elseif( true == array_key_exists( 'VpResidence', $this->getChangedColumns() ) ) { $strSql .= ' vp_residence = ' . $this->sqlVpResidence() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vp_rent_or_own = ' . $this->sqlVpRentOrOwn() . ','; } elseif( true == array_key_exists( 'VpRentOrOwn', $this->getChangedColumns() ) ) { $strSql .= ' vp_rent_or_own = ' . $this->sqlVpRentOrOwn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vp_home_phone = ' . $this->sqlVpHomePhone() . ','; } elseif( true == array_key_exists( 'VpHomePhone', $this->getChangedColumns() ) ) { $strSql .= ' vp_home_phone = ' . $this->sqlVpHomePhone() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vp_previous_address = ' . $this->sqlVpPreviousAddress() . ','; } elseif( true == array_key_exists( 'VpPreviousAddress', $this->getChangedColumns() ) ) { $strSql .= ' vp_previous_address = ' . $this->sqlVpPreviousAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' have_poor_credit = ' . $this->sqlHavePoorCredit() . ','; } elseif( true == array_key_exists( 'HavePoorCredit', $this->getChangedColumns() ) ) { $strSql .= ' have_poor_credit = ' . $this->sqlHavePoorCredit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applying_for_ach = ' . $this->sqlApplyingForAch() . ','; } elseif( true == array_key_exists( 'ApplyingForAch', $this->getChangedColumns() ) ) { $strSql .= ' applying_for_ach = ' . $this->sqlApplyingForAch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applying_for_credit_card = ' . $this->sqlApplyingForCreditCard() . ','; } elseif( true == array_key_exists( 'ApplyingForCreditCard', $this->getChangedColumns() ) ) { $strSql .= ' applying_for_credit_card = ' . $this->sqlApplyingForCreditCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applying_for_discover = ' . $this->sqlApplyingForDiscover() . ','; } elseif( true == array_key_exists( 'ApplyingForDiscover', $this->getChangedColumns() ) ) { $strSql .= ' applying_for_discover = ' . $this->sqlApplyingForDiscover() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applying_for_american_express = ' . $this->sqlApplyingForAmericanExpress() . ','; } elseif( true == array_key_exists( 'ApplyingForAmericanExpress', $this->getChangedColumns() ) ) { $strSql .= ' applying_for_american_express = ' . $this->sqlApplyingForAmericanExpress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' num_of_accounts_requested = ' . $this->sqlNumOfAccountsRequested() . ','; } elseif( true == array_key_exists( 'NumOfAccountsRequested', $this->getChangedColumns() ) ) { $strSql .= ' num_of_accounts_requested = ' . $this->sqlNumOfAccountsRequested() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ach_max_transactions_per_day = ' . $this->sqlAchMaxTransactionsPerDay() . ','; } elseif( true == array_key_exists( 'AchMaxTransactionsPerDay', $this->getChangedColumns() ) ) { $strSql .= ' ach_max_transactions_per_day = ' . $this->sqlAchMaxTransactionsPerDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ach_max_amount_per_transaction = ' . $this->sqlAchMaxAmountPerTransaction() . ','; } elseif( true == array_key_exists( 'AchMaxAmountPerTransaction', $this->getChangedColumns() ) ) { $strSql .= ' ach_max_amount_per_transaction = ' . $this->sqlAchMaxAmountPerTransaction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ach_avg_amount_per_transaction = ' . $this->sqlAchAvgAmountPerTransaction() . ','; } elseif( true == array_key_exists( 'AchAvgAmountPerTransaction', $this->getChangedColumns() ) ) { $strSql .= ' ach_avg_amount_per_transaction = ' . $this->sqlAchAvgAmountPerTransaction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ach_max_amount_per_day = ' . $this->sqlAchMaxAmountPerDay() . ','; } elseif( true == array_key_exists( 'AchMaxAmountPerDay', $this->getChangedColumns() ) ) { $strSql .= ' ach_max_amount_per_day = ' . $this->sqlAchMaxAmountPerDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ach_max_amount_per_month = ' . $this->sqlAchMaxAmountPerMonth() . ','; } elseif( true == array_key_exists( 'AchMaxAmountPerMonth', $this->getChangedColumns() ) ) { $strSql .= ' ach_max_amount_per_month = ' . $this->sqlAchMaxAmountPerMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_avg_amount_per_transaction = ' . $this->sqlCcAvgAmountPerTransaction() . ','; } elseif( true == array_key_exists( 'CcAvgAmountPerTransaction', $this->getChangedColumns() ) ) { $strSql .= ' cc_avg_amount_per_transaction = ' . $this->sqlCcAvgAmountPerTransaction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_max_amount_per_transaction = ' . $this->sqlCcMaxAmountPerTransaction() . ','; } elseif( true == array_key_exists( 'CcMaxAmountPerTransaction', $this->getChangedColumns() ) ) { $strSql .= ' cc_max_amount_per_transaction = ' . $this->sqlCcMaxAmountPerTransaction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_max_amount_per_month = ' . $this->sqlCcMaxAmountPerMonth() . ','; } elseif( true == array_key_exists( 'CcMaxAmountPerMonth', $this->getChangedColumns() ) ) { $strSql .= ' cc_max_amount_per_month = ' . $this->sqlCcMaxAmountPerMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' have_previous_merchant_account = ' . $this->sqlHavePreviousMerchantAccount() . ','; } elseif( true == array_key_exists( 'HavePreviousMerchantAccount', $this->getChangedColumns() ) ) { $strSql .= ' have_previous_merchant_account = ' . $this->sqlHavePreviousMerchantAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_merchant_account_info = ' . $this->sqlPreviousMerchantAccountInfo() . ','; } elseif( true == array_key_exists( 'PreviousMerchantAccountInfo', $this->getChangedColumns() ) ) { $strSql .= ' previous_merchant_account_info = ' . $this->sqlPreviousMerchantAccountInfo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' instructions_for_echo = ' . $this->sqlInstructionsForEcho() . ','; } elseif( true == array_key_exists( 'InstructionsForEcho', $this->getChangedColumns() ) ) { $strSql .= ' instructions_for_echo = ' . $this->sqlInstructionsForEcho() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_fee_managed = ' . $this->sqlIsFeeManaged() . ','; } elseif( true == array_key_exists( 'IsFeeManaged', $this->getChangedColumns() ) ) { $strSql .= ' is_fee_managed = ' . $this->sqlIsFeeManaged() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' photos_required = ' . $this->sqlPhotosRequired() . ','; } elseif( true == array_key_exists( 'PhotosRequired', $this->getChangedColumns() ) ) { $strSql .= ' photos_required = ' . $this->sqlPhotosRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' discover_pin = ' . $this->sqlDiscoverPin() . ','; } elseif( true == array_key_exists( 'DiscoverPin', $this->getChangedColumns() ) ) { $strSql .= ' discover_pin = ' . $this->sqlDiscoverPin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amex_pin = ' . $this->sqlAmexPin() . ','; } elseif( true == array_key_exists( 'AmexPin', $this->getChangedColumns() ) ) { $strSql .= ' amex_pin = ' . $this->sqlAmexPin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password = ' . $this->sqlPassword() . ','; } elseif( true == array_key_exists( 'Password', $this->getChangedColumns() ) ) { $strSql .= ' password = ' . $this->sqlPassword() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'merchant_account_application_types_id' => $this->getMerchantAccountApplicationTypesId(),
			'company_name' => $this->getCompanyName(),
			'company_dba_name' => $this->getCompanyDbaName(),
			'company_street_line1' => $this->getCompanyStreetLine1(),
			'company_city' => $this->getCompanyCity(),
			'company_state_code' => $this->getCompanyStateCode(),
			'company_postal_code' => $this->getCompanyPostalCode(),
			'company_country_code' => $this->getCompanyCountryCode(),
			'company_tax_number_encrypted' => $this->getCompanyTaxNumberEncrypted(),
			'company_tax_number_masked' => $this->getCompanyTaxNumberMasked(),
			'company_phone_number' => $this->getCompanyPhoneNumber(),
			'company_fax_number' => $this->getCompanyFaxNumber(),
			'ownership_type_id' => $this->getOwnershipTypeId(),
			'number_of_owners' => $this->getNumberOfOwners(),
			'number_of_locations' => $this->getNumberOfLocations(),
			'type_of_business' => $this->getTypeOfBusiness(),
			'age_of_business' => $this->getAgeOfBusiness(),
			'date_business_acquired' => $this->getDateBusinessAcquired(),
			'billto_street_line1' => $this->getBilltoStreetLine1(),
			'billto_city' => $this->getBilltoCity(),
			'billto_state_code' => $this->getBilltoStateCode(),
			'billto_postal_code' => $this->getBilltoPostalCode(),
			'billto_country_code' => $this->getBilltoCountryCode(),
			'contact_name' => $this->getContactName(),
			'contact_day_time_phone' => $this->getContactDayTimePhone(),
			'contact_evening_phone' => $this->getContactEveningPhone(),
			'contact_email' => $this->getContactEmail(),
			'website_url' => $this->getWebsiteUrl(),
			'vp_name' => $this->getVpName(),
			'vp_title' => $this->getVpTitle(),
			'vp_ownership' => $this->getVpOwnership(),
			'vp_tax_number_encrypted' => $this->getVpTaxNumberEncrypted(),
			'vp_tax_number_masked' => $this->getVpTaxNumberMasked(),
			'vp_dob' => $this->getVpDob(),
			'vp_street_line1' => $this->getVpStreetLine1(),
			'vp_city' => $this->getVpCity(),
			'vp_state_code' => $this->getVpStateCode(),
			'vp_postal_code' => $this->getVpPostalCode(),
			'vp_residence' => $this->getVpResidence(),
			'vp_rent_or_own' => $this->getVpRentOrOwn(),
			'vp_home_phone' => $this->getVpHomePhone(),
			'vp_previous_address' => $this->getVpPreviousAddress(),
			'have_poor_credit' => $this->getHavePoorCredit(),
			'applying_for_ach' => $this->getApplyingForAch(),
			'applying_for_credit_card' => $this->getApplyingForCreditCard(),
			'applying_for_discover' => $this->getApplyingForDiscover(),
			'applying_for_american_express' => $this->getApplyingForAmericanExpress(),
			'num_of_accounts_requested' => $this->getNumOfAccountsRequested(),
			'ach_max_transactions_per_day' => $this->getAchMaxTransactionsPerDay(),
			'ach_max_amount_per_transaction' => $this->getAchMaxAmountPerTransaction(),
			'ach_avg_amount_per_transaction' => $this->getAchAvgAmountPerTransaction(),
			'ach_max_amount_per_day' => $this->getAchMaxAmountPerDay(),
			'ach_max_amount_per_month' => $this->getAchMaxAmountPerMonth(),
			'cc_avg_amount_per_transaction' => $this->getCcAvgAmountPerTransaction(),
			'cc_max_amount_per_transaction' => $this->getCcMaxAmountPerTransaction(),
			'cc_max_amount_per_month' => $this->getCcMaxAmountPerMonth(),
			'have_previous_merchant_account' => $this->getHavePreviousMerchantAccount(),
			'previous_merchant_account_info' => $this->getPreviousMerchantAccountInfo(),
			'instructions_for_echo' => $this->getInstructionsForEcho(),
			'is_fee_managed' => $this->getIsFeeManaged(),
			'photos_required' => $this->getPhotosRequired(),
			'discover_pin' => $this->getDiscoverPin(),
			'amex_pin' => $this->getAmexPin(),
			'username' => $this->getUsername(),
			'password' => $this->getPassword(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>