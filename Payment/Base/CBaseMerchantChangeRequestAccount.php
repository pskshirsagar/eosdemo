<?php

class CBaseMerchantChangeRequestAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.merchant_change_request_accounts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMerchantChangeRequestId;
	protected $m_intCompanyMerchantAccountId;
	protected $m_intMerchantChangeRequestAccountTypeId;
	protected $m_intMerchantProcessingTypeId;
	protected $m_intMerchantChangeRequestAccountId;
	protected $m_intAccountId;
	protected $m_intBillingAccountId;
	protected $m_intReturnAccountId;
	protected $m_strPropertyName;
	protected $m_strAccountName;
	protected $m_strBilltoCompanyName;
	protected $m_strBilltoNameFirst;
	protected $m_strBilltoNameFirstEncrypted;
	protected $m_strBilltoNameMiddle;
	protected $m_strBilltoNameMiddleEncrypted;
	protected $m_strBilltoNameLast;
	protected $m_strBilltoNameLastEncrypted;
	protected $m_strBilltoStreetLine1;
	protected $m_strBilltoStreetLine2;
	protected $m_strBilltoStreetLine3;
	protected $m_strBilltoCity;
	protected $m_strBilltoStateCode;
	protected $m_strBilltoProvince;
	protected $m_strBilltoPostalCode;
	protected $m_strBilltoCountryCode;
	protected $m_strBilltoPhoneNumber;
	protected $m_strBilltoPhoneNumberEncrypted;
	protected $m_strBilltoEmailAddress;
	protected $m_strBilltoEmailAddressEncrypted;
	protected $m_strBilltoTaxNumberEncrypted;
	protected $m_intCheckAccountTypeId;
	protected $m_strCheckNameOnAccount;
	protected $m_strCheckBankName;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckAccountNumberEncrypted;
	protected $m_strCheckTaxNumberEncrypted;
	protected $m_strCheckTaxLegalName;
	protected $m_intIsAlternateBillingAccount;
	protected $m_intBillingAccountTypeId;
	protected $m_strBillingNameOnAccount;
	protected $m_strBillingBankName;
	protected $m_strBillingRoutingNumber;
	protected $m_strBillingAccountNumberEncrypted;
	protected $m_intIsAlternateReturnAccount;
	protected $m_intReturnAccountTypeId;
	protected $m_strReturnNameOnAccount;
	protected $m_strReturnBankName;
	protected $m_strReturnRoutingNumber;
	protected $m_strReturnAccountNumberEncrypted;
	protected $m_intInvoicesEmailed;
	protected $m_intInvoicesMailed;
	protected $m_strEffectiveDate;
	protected $m_strProcessedOn;
	protected $m_intIsNewAccountRequest;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intMerchantProcessingTypeId = '1';
		$this->m_intIsAlternateBillingAccount = '0';
		$this->m_intIsAlternateReturnAccount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['merchant_change_request_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantChangeRequestId', trim( $arrValues['merchant_change_request_id'] ) ); elseif( isset( $arrValues['merchant_change_request_id'] ) ) $this->setMerchantChangeRequestId( $arrValues['merchant_change_request_id'] );
		if( isset( $arrValues['company_merchant_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMerchantAccountId', trim( $arrValues['company_merchant_account_id'] ) ); elseif( isset( $arrValues['company_merchant_account_id'] ) ) $this->setCompanyMerchantAccountId( $arrValues['company_merchant_account_id'] );
		if( isset( $arrValues['merchant_change_request_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantChangeRequestAccountTypeId', trim( $arrValues['merchant_change_request_account_type_id'] ) ); elseif( isset( $arrValues['merchant_change_request_account_type_id'] ) ) $this->setMerchantChangeRequestAccountTypeId( $arrValues['merchant_change_request_account_type_id'] );
		if( isset( $arrValues['merchant_processing_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantProcessingTypeId', trim( $arrValues['merchant_processing_type_id'] ) ); elseif( isset( $arrValues['merchant_processing_type_id'] ) ) $this->setMerchantProcessingTypeId( $arrValues['merchant_processing_type_id'] );
		if( isset( $arrValues['merchant_change_request_account_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantChangeRequestAccountId', trim( $arrValues['merchant_change_request_account_id'] ) ); elseif( isset( $arrValues['merchant_change_request_account_id'] ) ) $this->setMerchantChangeRequestAccountId( $arrValues['merchant_change_request_account_id'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['billing_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingAccountId', trim( $arrValues['billing_account_id'] ) ); elseif( isset( $arrValues['billing_account_id'] ) ) $this->setBillingAccountId( $arrValues['billing_account_id'] );
		if( isset( $arrValues['return_account_id'] ) && $boolDirectSet ) $this->set( 'm_intReturnAccountId', trim( $arrValues['return_account_id'] ) ); elseif( isset( $arrValues['return_account_id'] ) ) $this->setReturnAccountId( $arrValues['return_account_id'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( stripcslashes( $arrValues['property_name'] ) ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_name'] ) : $arrValues['property_name'] );
		if( isset( $arrValues['account_name'] ) && $boolDirectSet ) $this->set( 'm_strAccountName', trim( stripcslashes( $arrValues['account_name'] ) ) ); elseif( isset( $arrValues['account_name'] ) ) $this->setAccountName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_name'] ) : $arrValues['account_name'] );
		if( isset( $arrValues['billto_company_name'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCompanyName', trim( stripcslashes( $arrValues['billto_company_name'] ) ) ); elseif( isset( $arrValues['billto_company_name'] ) ) $this->setBilltoCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_company_name'] ) : $arrValues['billto_company_name'] );
		if( isset( $arrValues['billto_name_first'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameFirst', trim( stripcslashes( $arrValues['billto_name_first'] ) ) ); elseif( isset( $arrValues['billto_name_first'] ) ) $this->setBilltoNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_first'] ) : $arrValues['billto_name_first'] );
		if( isset( $arrValues['billto_name_first_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameFirstEncrypted', trim( stripcslashes( $arrValues['billto_name_first_encrypted'] ) ) ); elseif( isset( $arrValues['billto_name_first_encrypted'] ) ) $this->setBilltoNameFirstEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_first_encrypted'] ) : $arrValues['billto_name_first_encrypted'] );
		if( isset( $arrValues['billto_name_middle'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameMiddle', trim( stripcslashes( $arrValues['billto_name_middle'] ) ) ); elseif( isset( $arrValues['billto_name_middle'] ) ) $this->setBilltoNameMiddle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_middle'] ) : $arrValues['billto_name_middle'] );
		if( isset( $arrValues['billto_name_middle_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameMiddleEncrypted', trim( stripcslashes( $arrValues['billto_name_middle_encrypted'] ) ) ); elseif( isset( $arrValues['billto_name_middle_encrypted'] ) ) $this->setBilltoNameMiddleEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_middle_encrypted'] ) : $arrValues['billto_name_middle_encrypted'] );
		if( isset( $arrValues['billto_name_last'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameLast', trim( stripcslashes( $arrValues['billto_name_last'] ) ) ); elseif( isset( $arrValues['billto_name_last'] ) ) $this->setBilltoNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_last'] ) : $arrValues['billto_name_last'] );
		if( isset( $arrValues['billto_name_last_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameLastEncrypted', trim( stripcslashes( $arrValues['billto_name_last_encrypted'] ) ) ); elseif( isset( $arrValues['billto_name_last_encrypted'] ) ) $this->setBilltoNameLastEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_last_encrypted'] ) : $arrValues['billto_name_last_encrypted'] );
		if( isset( $arrValues['billto_street_line1'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStreetLine1', trim( stripcslashes( $arrValues['billto_street_line1'] ) ) ); elseif( isset( $arrValues['billto_street_line1'] ) ) $this->setBilltoStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line1'] ) : $arrValues['billto_street_line1'] );
		if( isset( $arrValues['billto_street_line2'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStreetLine2', trim( stripcslashes( $arrValues['billto_street_line2'] ) ) ); elseif( isset( $arrValues['billto_street_line2'] ) ) $this->setBilltoStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line2'] ) : $arrValues['billto_street_line2'] );
		if( isset( $arrValues['billto_street_line3'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStreetLine3', trim( stripcslashes( $arrValues['billto_street_line3'] ) ) ); elseif( isset( $arrValues['billto_street_line3'] ) ) $this->setBilltoStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line3'] ) : $arrValues['billto_street_line3'] );
		if( isset( $arrValues['billto_city'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCity', trim( stripcslashes( $arrValues['billto_city'] ) ) ); elseif( isset( $arrValues['billto_city'] ) ) $this->setBilltoCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_city'] ) : $arrValues['billto_city'] );
		if( isset( $arrValues['billto_state_code'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStateCode', trim( stripcslashes( $arrValues['billto_state_code'] ) ) ); elseif( isset( $arrValues['billto_state_code'] ) ) $this->setBilltoStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_state_code'] ) : $arrValues['billto_state_code'] );
		if( isset( $arrValues['billto_province'] ) && $boolDirectSet ) $this->set( 'm_strBilltoProvince', trim( stripcslashes( $arrValues['billto_province'] ) ) ); elseif( isset( $arrValues['billto_province'] ) ) $this->setBilltoProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_province'] ) : $arrValues['billto_province'] );
		if( isset( $arrValues['billto_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strBilltoPostalCode', trim( stripcslashes( $arrValues['billto_postal_code'] ) ) ); elseif( isset( $arrValues['billto_postal_code'] ) ) $this->setBilltoPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_postal_code'] ) : $arrValues['billto_postal_code'] );
		if( isset( $arrValues['billto_country_code'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCountryCode', trim( stripcslashes( $arrValues['billto_country_code'] ) ) ); elseif( isset( $arrValues['billto_country_code'] ) ) $this->setBilltoCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_country_code'] ) : $arrValues['billto_country_code'] );
		if( isset( $arrValues['billto_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strBilltoPhoneNumber', trim( stripcslashes( $arrValues['billto_phone_number'] ) ) ); elseif( isset( $arrValues['billto_phone_number'] ) ) $this->setBilltoPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_phone_number'] ) : $arrValues['billto_phone_number'] );
		if( isset( $arrValues['billto_phone_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBilltoPhoneNumberEncrypted', trim( stripcslashes( $arrValues['billto_phone_number_encrypted'] ) ) ); elseif( isset( $arrValues['billto_phone_number_encrypted'] ) ) $this->setBilltoPhoneNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_phone_number_encrypted'] ) : $arrValues['billto_phone_number_encrypted'] );
		if( isset( $arrValues['billto_email_address'] ) && $boolDirectSet ) $this->set( 'm_strBilltoEmailAddress', trim( stripcslashes( $arrValues['billto_email_address'] ) ) ); elseif( isset( $arrValues['billto_email_address'] ) ) $this->setBilltoEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_email_address'] ) : $arrValues['billto_email_address'] );
		if( isset( $arrValues['billto_email_address_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBilltoEmailAddressEncrypted', trim( stripcslashes( $arrValues['billto_email_address_encrypted'] ) ) ); elseif( isset( $arrValues['billto_email_address_encrypted'] ) ) $this->setBilltoEmailAddressEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_email_address_encrypted'] ) : $arrValues['billto_email_address_encrypted'] );
		if( isset( $arrValues['billto_tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBilltoTaxNumberEncrypted', trim( stripcslashes( $arrValues['billto_tax_number_encrypted'] ) ) ); elseif( isset( $arrValues['billto_tax_number_encrypted'] ) ) $this->setBilltoTaxNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_tax_number_encrypted'] ) : $arrValues['billto_tax_number_encrypted'] );
		if( isset( $arrValues['check_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckAccountTypeId', trim( $arrValues['check_account_type_id'] ) ); elseif( isset( $arrValues['check_account_type_id'] ) ) $this->setCheckAccountTypeId( $arrValues['check_account_type_id'] );
		if( isset( $arrValues['check_name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strCheckNameOnAccount', trim( stripcslashes( $arrValues['check_name_on_account'] ) ) ); elseif( isset( $arrValues['check_name_on_account'] ) ) $this->setCheckNameOnAccount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_name_on_account'] ) : $arrValues['check_name_on_account'] );
		if( isset( $arrValues['check_bank_name'] ) && $boolDirectSet ) $this->set( 'm_strCheckBankName', trim( stripcslashes( $arrValues['check_bank_name'] ) ) ); elseif( isset( $arrValues['check_bank_name'] ) ) $this->setCheckBankName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_bank_name'] ) : $arrValues['check_bank_name'] );
		if( isset( $arrValues['check_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckRoutingNumber', trim( stripcslashes( $arrValues['check_routing_number'] ) ) ); elseif( isset( $arrValues['check_routing_number'] ) ) $this->setCheckRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_routing_number'] ) : $arrValues['check_routing_number'] );
		if( isset( $arrValues['check_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberEncrypted', trim( stripcslashes( $arrValues['check_account_number_encrypted'] ) ) ); elseif( isset( $arrValues['check_account_number_encrypted'] ) ) $this->setCheckAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_account_number_encrypted'] ) : $arrValues['check_account_number_encrypted'] );
		if( isset( $arrValues['check_tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCheckTaxNumberEncrypted', trim( stripcslashes( $arrValues['check_tax_number_encrypted'] ) ) ); elseif( isset( $arrValues['check_tax_number_encrypted'] ) ) $this->setCheckTaxNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_tax_number_encrypted'] ) : $arrValues['check_tax_number_encrypted'] );
		if( isset( $arrValues['check_tax_legal_name'] ) && $boolDirectSet ) $this->set( 'm_strCheckTaxLegalName', trim( stripcslashes( $arrValues['check_tax_legal_name'] ) ) ); elseif( isset( $arrValues['check_tax_legal_name'] ) ) $this->setCheckTaxLegalName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_tax_legal_name'] ) : $arrValues['check_tax_legal_name'] );
		if( isset( $arrValues['is_alternate_billing_account'] ) && $boolDirectSet ) $this->set( 'm_intIsAlternateBillingAccount', trim( $arrValues['is_alternate_billing_account'] ) ); elseif( isset( $arrValues['is_alternate_billing_account'] ) ) $this->setIsAlternateBillingAccount( $arrValues['is_alternate_billing_account'] );
		if( isset( $arrValues['billing_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingAccountTypeId', trim( $arrValues['billing_account_type_id'] ) ); elseif( isset( $arrValues['billing_account_type_id'] ) ) $this->setBillingAccountTypeId( $arrValues['billing_account_type_id'] );
		if( isset( $arrValues['billing_name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strBillingNameOnAccount', trim( stripcslashes( $arrValues['billing_name_on_account'] ) ) ); elseif( isset( $arrValues['billing_name_on_account'] ) ) $this->setBillingNameOnAccount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billing_name_on_account'] ) : $arrValues['billing_name_on_account'] );
		if( isset( $arrValues['billing_bank_name'] ) && $boolDirectSet ) $this->set( 'm_strBillingBankName', trim( stripcslashes( $arrValues['billing_bank_name'] ) ) ); elseif( isset( $arrValues['billing_bank_name'] ) ) $this->setBillingBankName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billing_bank_name'] ) : $arrValues['billing_bank_name'] );
		if( isset( $arrValues['billing_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strBillingRoutingNumber', trim( stripcslashes( $arrValues['billing_routing_number'] ) ) ); elseif( isset( $arrValues['billing_routing_number'] ) ) $this->setBillingRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billing_routing_number'] ) : $arrValues['billing_routing_number'] );
		if( isset( $arrValues['billing_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBillingAccountNumberEncrypted', trim( stripcslashes( $arrValues['billing_account_number_encrypted'] ) ) ); elseif( isset( $arrValues['billing_account_number_encrypted'] ) ) $this->setBillingAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billing_account_number_encrypted'] ) : $arrValues['billing_account_number_encrypted'] );
		if( isset( $arrValues['is_alternate_return_account'] ) && $boolDirectSet ) $this->set( 'm_intIsAlternateReturnAccount', trim( $arrValues['is_alternate_return_account'] ) ); elseif( isset( $arrValues['is_alternate_return_account'] ) ) $this->setIsAlternateReturnAccount( $arrValues['is_alternate_return_account'] );
		if( isset( $arrValues['return_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReturnAccountTypeId', trim( $arrValues['return_account_type_id'] ) ); elseif( isset( $arrValues['return_account_type_id'] ) ) $this->setReturnAccountTypeId( $arrValues['return_account_type_id'] );
		if( isset( $arrValues['return_name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strReturnNameOnAccount', trim( stripcslashes( $arrValues['return_name_on_account'] ) ) ); elseif( isset( $arrValues['return_name_on_account'] ) ) $this->setReturnNameOnAccount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['return_name_on_account'] ) : $arrValues['return_name_on_account'] );
		if( isset( $arrValues['return_bank_name'] ) && $boolDirectSet ) $this->set( 'm_strReturnBankName', trim( stripcslashes( $arrValues['return_bank_name'] ) ) ); elseif( isset( $arrValues['return_bank_name'] ) ) $this->setReturnBankName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['return_bank_name'] ) : $arrValues['return_bank_name'] );
		if( isset( $arrValues['return_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strReturnRoutingNumber', trim( stripcslashes( $arrValues['return_routing_number'] ) ) ); elseif( isset( $arrValues['return_routing_number'] ) ) $this->setReturnRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['return_routing_number'] ) : $arrValues['return_routing_number'] );
		if( isset( $arrValues['return_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strReturnAccountNumberEncrypted', trim( stripcslashes( $arrValues['return_account_number_encrypted'] ) ) ); elseif( isset( $arrValues['return_account_number_encrypted'] ) ) $this->setReturnAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['return_account_number_encrypted'] ) : $arrValues['return_account_number_encrypted'] );
		if( isset( $arrValues['invoices_emailed'] ) && $boolDirectSet ) $this->set( 'm_intInvoicesEmailed', trim( $arrValues['invoices_emailed'] ) ); elseif( isset( $arrValues['invoices_emailed'] ) ) $this->setInvoicesEmailed( $arrValues['invoices_emailed'] );
		if( isset( $arrValues['invoices_mailed'] ) && $boolDirectSet ) $this->set( 'm_intInvoicesMailed', trim( $arrValues['invoices_mailed'] ) ); elseif( isset( $arrValues['invoices_mailed'] ) ) $this->setInvoicesMailed( $arrValues['invoices_mailed'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['is_new_account_request'] ) && $boolDirectSet ) $this->set( 'm_intIsNewAccountRequest', trim( $arrValues['is_new_account_request'] ) ); elseif( isset( $arrValues['is_new_account_request'] ) ) $this->setIsNewAccountRequest( $arrValues['is_new_account_request'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMerchantChangeRequestId( $intMerchantChangeRequestId ) {
		$this->set( 'm_intMerchantChangeRequestId', CStrings::strToIntDef( $intMerchantChangeRequestId, NULL, false ) );
	}

	public function getMerchantChangeRequestId() {
		return $this->m_intMerchantChangeRequestId;
	}

	public function sqlMerchantChangeRequestId() {
		return ( true == isset( $this->m_intMerchantChangeRequestId ) ) ? ( string ) $this->m_intMerchantChangeRequestId : 'NULL';
	}

	public function setCompanyMerchantAccountId( $intCompanyMerchantAccountId ) {
		$this->set( 'm_intCompanyMerchantAccountId', CStrings::strToIntDef( $intCompanyMerchantAccountId, NULL, false ) );
	}

	public function getCompanyMerchantAccountId() {
		return $this->m_intCompanyMerchantAccountId;
	}

	public function sqlCompanyMerchantAccountId() {
		return ( true == isset( $this->m_intCompanyMerchantAccountId ) ) ? ( string ) $this->m_intCompanyMerchantAccountId : 'NULL';
	}

	public function setMerchantChangeRequestAccountTypeId( $intMerchantChangeRequestAccountTypeId ) {
		$this->set( 'm_intMerchantChangeRequestAccountTypeId', CStrings::strToIntDef( $intMerchantChangeRequestAccountTypeId, NULL, false ) );
	}

	public function getMerchantChangeRequestAccountTypeId() {
		return $this->m_intMerchantChangeRequestAccountTypeId;
	}

	public function sqlMerchantChangeRequestAccountTypeId() {
		return ( true == isset( $this->m_intMerchantChangeRequestAccountTypeId ) ) ? ( string ) $this->m_intMerchantChangeRequestAccountTypeId : 'NULL';
	}

	public function setMerchantProcessingTypeId( $intMerchantProcessingTypeId ) {
		$this->set( 'm_intMerchantProcessingTypeId', CStrings::strToIntDef( $intMerchantProcessingTypeId, NULL, false ) );
	}

	public function getMerchantProcessingTypeId() {
		return $this->m_intMerchantProcessingTypeId;
	}

	public function sqlMerchantProcessingTypeId() {
		return ( true == isset( $this->m_intMerchantProcessingTypeId ) ) ? ( string ) $this->m_intMerchantProcessingTypeId : '1';
	}

	public function setMerchantChangeRequestAccountId( $intMerchantChangeRequestAccountId ) {
		$this->set( 'm_intMerchantChangeRequestAccountId', CStrings::strToIntDef( $intMerchantChangeRequestAccountId, NULL, false ) );
	}

	public function getMerchantChangeRequestAccountId() {
		return $this->m_intMerchantChangeRequestAccountId;
	}

	public function sqlMerchantChangeRequestAccountId() {
		return ( true == isset( $this->m_intMerchantChangeRequestAccountId ) ) ? ( string ) $this->m_intMerchantChangeRequestAccountId : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setBillingAccountId( $intBillingAccountId ) {
		$this->set( 'm_intBillingAccountId', CStrings::strToIntDef( $intBillingAccountId, NULL, false ) );
	}

	public function getBillingAccountId() {
		return $this->m_intBillingAccountId;
	}

	public function sqlBillingAccountId() {
		return ( true == isset( $this->m_intBillingAccountId ) ) ? ( string ) $this->m_intBillingAccountId : 'NULL';
	}

	public function setReturnAccountId( $intReturnAccountId ) {
		$this->set( 'm_intReturnAccountId', CStrings::strToIntDef( $intReturnAccountId, NULL, false ) );
	}

	public function getReturnAccountId() {
		return $this->m_intReturnAccountId;
	}

	public function sqlReturnAccountId() {
		return ( true == isset( $this->m_intReturnAccountId ) ) ? ( string ) $this->m_intReturnAccountId : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 50, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? '\'' . addslashes( $this->m_strPropertyName ) . '\'' : 'NULL';
	}

	public function setAccountName( $strAccountName ) {
		$this->set( 'm_strAccountName', CStrings::strTrimDef( $strAccountName, 50, NULL, true ) );
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function sqlAccountName() {
		return ( true == isset( $this->m_strAccountName ) ) ? '\'' . addslashes( $this->m_strAccountName ) . '\'' : 'NULL';
	}

	public function setBilltoCompanyName( $strBilltoCompanyName ) {
		$this->set( 'm_strBilltoCompanyName', CStrings::strTrimDef( $strBilltoCompanyName, 100, NULL, true ) );
	}

	public function getBilltoCompanyName() {
		return $this->m_strBilltoCompanyName;
	}

	public function sqlBilltoCompanyName() {
		return ( true == isset( $this->m_strBilltoCompanyName ) ) ? '\'' . addslashes( $this->m_strBilltoCompanyName ) . '\'' : 'NULL';
	}

	public function setBilltoNameFirst( $strBilltoNameFirst ) {
		$this->set( 'm_strBilltoNameFirst', CStrings::strTrimDef( $strBilltoNameFirst, 50, NULL, true ) );
	}

	public function getBilltoNameFirst() {
		return $this->m_strBilltoNameFirst;
	}

	public function sqlBilltoNameFirst() {
		return ( true == isset( $this->m_strBilltoNameFirst ) ) ? '\'' . addslashes( $this->m_strBilltoNameFirst ) . '\'' : 'NULL';
	}

	public function setBilltoNameFirstEncrypted( $strBilltoNameFirstEncrypted ) {
		$this->set( 'm_strBilltoNameFirstEncrypted', CStrings::strTrimDef( $strBilltoNameFirstEncrypted, 240, NULL, true ) );
	}

	public function getBilltoNameFirstEncrypted() {
		return $this->m_strBilltoNameFirstEncrypted;
	}

	public function sqlBilltoNameFirstEncrypted() {
		return ( true == isset( $this->m_strBilltoNameFirstEncrypted ) ) ? '\'' . addslashes( $this->m_strBilltoNameFirstEncrypted ) . '\'' : 'NULL';
	}

	public function setBilltoNameMiddle( $strBilltoNameMiddle ) {
		$this->set( 'm_strBilltoNameMiddle', CStrings::strTrimDef( $strBilltoNameMiddle, 50, NULL, true ) );
	}

	public function getBilltoNameMiddle() {
		return $this->m_strBilltoNameMiddle;
	}

	public function sqlBilltoNameMiddle() {
		return ( true == isset( $this->m_strBilltoNameMiddle ) ) ? '\'' . addslashes( $this->m_strBilltoNameMiddle ) . '\'' : 'NULL';
	}

	public function setBilltoNameMiddleEncrypted( $strBilltoNameMiddleEncrypted ) {
		$this->set( 'm_strBilltoNameMiddleEncrypted', CStrings::strTrimDef( $strBilltoNameMiddleEncrypted, 240, NULL, true ) );
	}

	public function getBilltoNameMiddleEncrypted() {
		return $this->m_strBilltoNameMiddleEncrypted;
	}

	public function sqlBilltoNameMiddleEncrypted() {
		return ( true == isset( $this->m_strBilltoNameMiddleEncrypted ) ) ? '\'' . addslashes( $this->m_strBilltoNameMiddleEncrypted ) . '\'' : 'NULL';
	}

	public function setBilltoNameLast( $strBilltoNameLast ) {
		$this->set( 'm_strBilltoNameLast', CStrings::strTrimDef( $strBilltoNameLast, 50, NULL, true ) );
	}

	public function getBilltoNameLast() {
		return $this->m_strBilltoNameLast;
	}

	public function sqlBilltoNameLast() {
		return ( true == isset( $this->m_strBilltoNameLast ) ) ? '\'' . addslashes( $this->m_strBilltoNameLast ) . '\'' : 'NULL';
	}

	public function setBilltoNameLastEncrypted( $strBilltoNameLastEncrypted ) {
		$this->set( 'm_strBilltoNameLastEncrypted', CStrings::strTrimDef( $strBilltoNameLastEncrypted, 240, NULL, true ) );
	}

	public function getBilltoNameLastEncrypted() {
		return $this->m_strBilltoNameLastEncrypted;
	}

	public function sqlBilltoNameLastEncrypted() {
		return ( true == isset( $this->m_strBilltoNameLastEncrypted ) ) ? '\'' . addslashes( $this->m_strBilltoNameLastEncrypted ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine1( $strBilltoStreetLine1 ) {
		$this->set( 'm_strBilltoStreetLine1', CStrings::strTrimDef( $strBilltoStreetLine1, 100, NULL, true ) );
	}

	public function getBilltoStreetLine1() {
		return $this->m_strBilltoStreetLine1;
	}

	public function sqlBilltoStreetLine1() {
		return ( true == isset( $this->m_strBilltoStreetLine1 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine1 ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine2( $strBilltoStreetLine2 ) {
		$this->set( 'm_strBilltoStreetLine2', CStrings::strTrimDef( $strBilltoStreetLine2, 100, NULL, true ) );
	}

	public function getBilltoStreetLine2() {
		return $this->m_strBilltoStreetLine2;
	}

	public function sqlBilltoStreetLine2() {
		return ( true == isset( $this->m_strBilltoStreetLine2 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine2 ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine3( $strBilltoStreetLine3 ) {
		$this->set( 'm_strBilltoStreetLine3', CStrings::strTrimDef( $strBilltoStreetLine3, 100, NULL, true ) );
	}

	public function getBilltoStreetLine3() {
		return $this->m_strBilltoStreetLine3;
	}

	public function sqlBilltoStreetLine3() {
		return ( true == isset( $this->m_strBilltoStreetLine3 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine3 ) . '\'' : 'NULL';
	}

	public function setBilltoCity( $strBilltoCity ) {
		$this->set( 'm_strBilltoCity', CStrings::strTrimDef( $strBilltoCity, 50, NULL, true ) );
	}

	public function getBilltoCity() {
		return $this->m_strBilltoCity;
	}

	public function sqlBilltoCity() {
		return ( true == isset( $this->m_strBilltoCity ) ) ? '\'' . addslashes( $this->m_strBilltoCity ) . '\'' : 'NULL';
	}

	public function setBilltoStateCode( $strBilltoStateCode ) {
		$this->set( 'm_strBilltoStateCode', CStrings::strTrimDef( $strBilltoStateCode, 2, NULL, true ) );
	}

	public function getBilltoStateCode() {
		return $this->m_strBilltoStateCode;
	}

	public function sqlBilltoStateCode() {
		return ( true == isset( $this->m_strBilltoStateCode ) ) ? '\'' . addslashes( $this->m_strBilltoStateCode ) . '\'' : 'NULL';
	}

	public function setBilltoProvince( $strBilltoProvince ) {
		$this->set( 'm_strBilltoProvince', CStrings::strTrimDef( $strBilltoProvince, 50, NULL, true ) );
	}

	public function getBilltoProvince() {
		return $this->m_strBilltoProvince;
	}

	public function sqlBilltoProvince() {
		return ( true == isset( $this->m_strBilltoProvince ) ) ? '\'' . addslashes( $this->m_strBilltoProvince ) . '\'' : 'NULL';
	}

	public function setBilltoPostalCode( $strBilltoPostalCode ) {
		$this->set( 'm_strBilltoPostalCode', CStrings::strTrimDef( $strBilltoPostalCode, 20, NULL, true ) );
	}

	public function getBilltoPostalCode() {
		return $this->m_strBilltoPostalCode;
	}

	public function sqlBilltoPostalCode() {
		return ( true == isset( $this->m_strBilltoPostalCode ) ) ? '\'' . addslashes( $this->m_strBilltoPostalCode ) . '\'' : 'NULL';
	}

	public function setBilltoCountryCode( $strBilltoCountryCode ) {
		$this->set( 'm_strBilltoCountryCode', CStrings::strTrimDef( $strBilltoCountryCode, 2, NULL, true ) );
	}

	public function getBilltoCountryCode() {
		return $this->m_strBilltoCountryCode;
	}

	public function sqlBilltoCountryCode() {
		return ( true == isset( $this->m_strBilltoCountryCode ) ) ? '\'' . addslashes( $this->m_strBilltoCountryCode ) . '\'' : 'NULL';
	}

	public function setBilltoPhoneNumber( $strBilltoPhoneNumber ) {
		$this->set( 'm_strBilltoPhoneNumber', CStrings::strTrimDef( $strBilltoPhoneNumber, 30, NULL, true ) );
	}

	public function getBilltoPhoneNumber() {
		return $this->m_strBilltoPhoneNumber;
	}

	public function sqlBilltoPhoneNumber() {
		return ( true == isset( $this->m_strBilltoPhoneNumber ) ) ? '\'' . addslashes( $this->m_strBilltoPhoneNumber ) . '\'' : 'NULL';
	}

	public function setBilltoPhoneNumberEncrypted( $strBilltoPhoneNumberEncrypted ) {
		$this->set( 'm_strBilltoPhoneNumberEncrypted', CStrings::strTrimDef( $strBilltoPhoneNumberEncrypted, 240, NULL, true ) );
	}

	public function getBilltoPhoneNumberEncrypted() {
		return $this->m_strBilltoPhoneNumberEncrypted;
	}

	public function sqlBilltoPhoneNumberEncrypted() {
		return ( true == isset( $this->m_strBilltoPhoneNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strBilltoPhoneNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setBilltoEmailAddress( $strBilltoEmailAddress ) {
		$this->set( 'm_strBilltoEmailAddress', CStrings::strTrimDef( $strBilltoEmailAddress, 240, NULL, true ) );
	}

	public function getBilltoEmailAddress() {
		return $this->m_strBilltoEmailAddress;
	}

	public function sqlBilltoEmailAddress() {
		return ( true == isset( $this->m_strBilltoEmailAddress ) ) ? '\'' . addslashes( $this->m_strBilltoEmailAddress ) . '\'' : 'NULL';
	}

	public function setBilltoEmailAddressEncrypted( $strBilltoEmailAddressEncrypted ) {
		$this->set( 'm_strBilltoEmailAddressEncrypted', CStrings::strTrimDef( $strBilltoEmailAddressEncrypted, 550, NULL, true ) );
	}

	public function getBilltoEmailAddressEncrypted() {
		return $this->m_strBilltoEmailAddressEncrypted;
	}

	public function sqlBilltoEmailAddressEncrypted() {
		return ( true == isset( $this->m_strBilltoEmailAddressEncrypted ) ) ? '\'' . addslashes( $this->m_strBilltoEmailAddressEncrypted ) . '\'' : 'NULL';
	}

	public function setBilltoTaxNumberEncrypted( $strBilltoTaxNumberEncrypted ) {
		$this->set( 'm_strBilltoTaxNumberEncrypted', CStrings::strTrimDef( $strBilltoTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getBilltoTaxNumberEncrypted() {
		return $this->m_strBilltoTaxNumberEncrypted;
	}

	public function sqlBilltoTaxNumberEncrypted() {
		return ( true == isset( $this->m_strBilltoTaxNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strBilltoTaxNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setCheckAccountTypeId( $intCheckAccountTypeId ) {
		$this->set( 'm_intCheckAccountTypeId', CStrings::strToIntDef( $intCheckAccountTypeId, NULL, false ) );
	}

	public function getCheckAccountTypeId() {
		return $this->m_intCheckAccountTypeId;
	}

	public function sqlCheckAccountTypeId() {
		return ( true == isset( $this->m_intCheckAccountTypeId ) ) ? ( string ) $this->m_intCheckAccountTypeId : 'NULL';
	}

	public function setCheckNameOnAccount( $strCheckNameOnAccount ) {
		$this->set( 'm_strCheckNameOnAccount', CStrings::strTrimDef( $strCheckNameOnAccount, 50, NULL, true ) );
	}

	public function getCheckNameOnAccount() {
		return $this->m_strCheckNameOnAccount;
	}

	public function sqlCheckNameOnAccount() {
		return ( true == isset( $this->m_strCheckNameOnAccount ) ) ? '\'' . addslashes( $this->m_strCheckNameOnAccount ) . '\'' : 'NULL';
	}

	public function setCheckBankName( $strCheckBankName ) {
		$this->set( 'm_strCheckBankName', CStrings::strTrimDef( $strCheckBankName, 100, NULL, true ) );
	}

	public function getCheckBankName() {
		return $this->m_strCheckBankName;
	}

	public function sqlCheckBankName() {
		return ( true == isset( $this->m_strCheckBankName ) ) ? '\'' . addslashes( $this->m_strCheckBankName ) . '\'' : 'NULL';
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->set( 'm_strCheckRoutingNumber', CStrings::strTrimDef( $strCheckRoutingNumber, 240, NULL, true ) );
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function sqlCheckRoutingNumber() {
		return ( true == isset( $this->m_strCheckRoutingNumber ) ) ? '\'' . addslashes( $this->m_strCheckRoutingNumber ) . '\'' : 'NULL';
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->set( 'm_strCheckAccountNumberEncrypted', CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function sqlCheckAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCheckAccountNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setCheckTaxNumberEncrypted( $strCheckTaxNumberEncrypted ) {
		$this->set( 'm_strCheckTaxNumberEncrypted', CStrings::strTrimDef( $strCheckTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getCheckTaxNumberEncrypted() {
		return $this->m_strCheckTaxNumberEncrypted;
	}

	public function sqlCheckTaxNumberEncrypted() {
		return ( true == isset( $this->m_strCheckTaxNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCheckTaxNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setCheckTaxLegalName( $strCheckTaxLegalName ) {
		$this->set( 'm_strCheckTaxLegalName', CStrings::strTrimDef( $strCheckTaxLegalName, 100, NULL, true ) );
	}

	public function getCheckTaxLegalName() {
		return $this->m_strCheckTaxLegalName;
	}

	public function sqlCheckTaxLegalName() {
		return ( true == isset( $this->m_strCheckTaxLegalName ) ) ? '\'' . addslashes( $this->m_strCheckTaxLegalName ) . '\'' : 'NULL';
	}

	public function setIsAlternateBillingAccount( $intIsAlternateBillingAccount ) {
		$this->set( 'm_intIsAlternateBillingAccount', CStrings::strToIntDef( $intIsAlternateBillingAccount, NULL, false ) );
	}

	public function getIsAlternateBillingAccount() {
		return $this->m_intIsAlternateBillingAccount;
	}

	public function sqlIsAlternateBillingAccount() {
		return ( true == isset( $this->m_intIsAlternateBillingAccount ) ) ? ( string ) $this->m_intIsAlternateBillingAccount : '0';
	}

	public function setBillingAccountTypeId( $intBillingAccountTypeId ) {
		$this->set( 'm_intBillingAccountTypeId', CStrings::strToIntDef( $intBillingAccountTypeId, NULL, false ) );
	}

	public function getBillingAccountTypeId() {
		return $this->m_intBillingAccountTypeId;
	}

	public function sqlBillingAccountTypeId() {
		return ( true == isset( $this->m_intBillingAccountTypeId ) ) ? ( string ) $this->m_intBillingAccountTypeId : 'NULL';
	}

	public function setBillingNameOnAccount( $strBillingNameOnAccount ) {
		$this->set( 'm_strBillingNameOnAccount', CStrings::strTrimDef( $strBillingNameOnAccount, 50, NULL, true ) );
	}

	public function getBillingNameOnAccount() {
		return $this->m_strBillingNameOnAccount;
	}

	public function sqlBillingNameOnAccount() {
		return ( true == isset( $this->m_strBillingNameOnAccount ) ) ? '\'' . addslashes( $this->m_strBillingNameOnAccount ) . '\'' : 'NULL';
	}

	public function setBillingBankName( $strBillingBankName ) {
		$this->set( 'm_strBillingBankName', CStrings::strTrimDef( $strBillingBankName, 100, NULL, true ) );
	}

	public function getBillingBankName() {
		return $this->m_strBillingBankName;
	}

	public function sqlBillingBankName() {
		return ( true == isset( $this->m_strBillingBankName ) ) ? '\'' . addslashes( $this->m_strBillingBankName ) . '\'' : 'NULL';
	}

	public function setBillingRoutingNumber( $strBillingRoutingNumber ) {
		$this->set( 'm_strBillingRoutingNumber', CStrings::strTrimDef( $strBillingRoutingNumber, 240, NULL, true ) );
	}

	public function getBillingRoutingNumber() {
		return $this->m_strBillingRoutingNumber;
	}

	public function sqlBillingRoutingNumber() {
		return ( true == isset( $this->m_strBillingRoutingNumber ) ) ? '\'' . addslashes( $this->m_strBillingRoutingNumber ) . '\'' : 'NULL';
	}

	public function setBillingAccountNumberEncrypted( $strBillingAccountNumberEncrypted ) {
		$this->set( 'm_strBillingAccountNumberEncrypted', CStrings::strTrimDef( $strBillingAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getBillingAccountNumberEncrypted() {
		return $this->m_strBillingAccountNumberEncrypted;
	}

	public function sqlBillingAccountNumberEncrypted() {
		return ( true == isset( $this->m_strBillingAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strBillingAccountNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setIsAlternateReturnAccount( $intIsAlternateReturnAccount ) {
		$this->set( 'm_intIsAlternateReturnAccount', CStrings::strToIntDef( $intIsAlternateReturnAccount, NULL, false ) );
	}

	public function getIsAlternateReturnAccount() {
		return $this->m_intIsAlternateReturnAccount;
	}

	public function sqlIsAlternateReturnAccount() {
		return ( true == isset( $this->m_intIsAlternateReturnAccount ) ) ? ( string ) $this->m_intIsAlternateReturnAccount : '0';
	}

	public function setReturnAccountTypeId( $intReturnAccountTypeId ) {
		$this->set( 'm_intReturnAccountTypeId', CStrings::strToIntDef( $intReturnAccountTypeId, NULL, false ) );
	}

	public function getReturnAccountTypeId() {
		return $this->m_intReturnAccountTypeId;
	}

	public function sqlReturnAccountTypeId() {
		return ( true == isset( $this->m_intReturnAccountTypeId ) ) ? ( string ) $this->m_intReturnAccountTypeId : 'NULL';
	}

	public function setReturnNameOnAccount( $strReturnNameOnAccount ) {
		$this->set( 'm_strReturnNameOnAccount', CStrings::strTrimDef( $strReturnNameOnAccount, 50, NULL, true ) );
	}

	public function getReturnNameOnAccount() {
		return $this->m_strReturnNameOnAccount;
	}

	public function sqlReturnNameOnAccount() {
		return ( true == isset( $this->m_strReturnNameOnAccount ) ) ? '\'' . addslashes( $this->m_strReturnNameOnAccount ) . '\'' : 'NULL';
	}

	public function setReturnBankName( $strReturnBankName ) {
		$this->set( 'm_strReturnBankName', CStrings::strTrimDef( $strReturnBankName, 100, NULL, true ) );
	}

	public function getReturnBankName() {
		return $this->m_strReturnBankName;
	}

	public function sqlReturnBankName() {
		return ( true == isset( $this->m_strReturnBankName ) ) ? '\'' . addslashes( $this->m_strReturnBankName ) . '\'' : 'NULL';
	}

	public function setReturnRoutingNumber( $strReturnRoutingNumber ) {
		$this->set( 'm_strReturnRoutingNumber', CStrings::strTrimDef( $strReturnRoutingNumber, 240, NULL, true ) );
	}

	public function getReturnRoutingNumber() {
		return $this->m_strReturnRoutingNumber;
	}

	public function sqlReturnRoutingNumber() {
		return ( true == isset( $this->m_strReturnRoutingNumber ) ) ? '\'' . addslashes( $this->m_strReturnRoutingNumber ) . '\'' : 'NULL';
	}

	public function setReturnAccountNumberEncrypted( $strReturnAccountNumberEncrypted ) {
		$this->set( 'm_strReturnAccountNumberEncrypted', CStrings::strTrimDef( $strReturnAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getReturnAccountNumberEncrypted() {
		return $this->m_strReturnAccountNumberEncrypted;
	}

	public function sqlReturnAccountNumberEncrypted() {
		return ( true == isset( $this->m_strReturnAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strReturnAccountNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setInvoicesEmailed( $intInvoicesEmailed ) {
		$this->set( 'm_intInvoicesEmailed', CStrings::strToIntDef( $intInvoicesEmailed, NULL, false ) );
	}

	public function getInvoicesEmailed() {
		return $this->m_intInvoicesEmailed;
	}

	public function sqlInvoicesEmailed() {
		return ( true == isset( $this->m_intInvoicesEmailed ) ) ? ( string ) $this->m_intInvoicesEmailed : 'NULL';
	}

	public function setInvoicesMailed( $intInvoicesMailed ) {
		$this->set( 'm_intInvoicesMailed', CStrings::strToIntDef( $intInvoicesMailed, NULL, false ) );
	}

	public function getInvoicesMailed() {
		return $this->m_intInvoicesMailed;
	}

	public function sqlInvoicesMailed() {
		return ( true == isset( $this->m_intInvoicesMailed ) ) ? ( string ) $this->m_intInvoicesMailed : 'NULL';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setIsNewAccountRequest( $intIsNewAccountRequest ) {
		$this->set( 'm_intIsNewAccountRequest', CStrings::strToIntDef( $intIsNewAccountRequest, NULL, false ) );
	}

	public function getIsNewAccountRequest() {
		return $this->m_intIsNewAccountRequest;
	}

	public function sqlIsNewAccountRequest() {
		return ( true == isset( $this->m_intIsNewAccountRequest ) ) ? ( string ) $this->m_intIsNewAccountRequest : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, merchant_change_request_id, company_merchant_account_id, merchant_change_request_account_type_id, merchant_processing_type_id, merchant_change_request_account_id, account_id, billing_account_id, return_account_id, property_name, account_name, billto_company_name, billto_name_first, billto_name_first_encrypted, billto_name_middle, billto_name_middle_encrypted, billto_name_last, billto_name_last_encrypted, billto_street_line1, billto_street_line2, billto_street_line3, billto_city, billto_state_code, billto_province, billto_postal_code, billto_country_code, billto_phone_number, billto_phone_number_encrypted, billto_email_address, billto_email_address_encrypted, billto_tax_number_encrypted, check_account_type_id, check_name_on_account, check_bank_name, check_routing_number, check_account_number_encrypted, check_tax_number_encrypted, check_tax_legal_name, is_alternate_billing_account, billing_account_type_id, billing_name_on_account, billing_bank_name, billing_routing_number, billing_account_number_encrypted, is_alternate_return_account, return_account_type_id, return_name_on_account, return_bank_name, return_routing_number, return_account_number_encrypted, invoices_emailed, invoices_mailed, effective_date, processed_on, is_new_account_request, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlMerchantChangeRequestId() . ', ' .
 						$this->sqlCompanyMerchantAccountId() . ', ' .
 						$this->sqlMerchantChangeRequestAccountTypeId() . ', ' .
 						$this->sqlMerchantProcessingTypeId() . ', ' .
 						$this->sqlMerchantChangeRequestAccountId() . ', ' .
 						$this->sqlAccountId() . ', ' .
 						$this->sqlBillingAccountId() . ', ' .
 						$this->sqlReturnAccountId() . ', ' .
 						$this->sqlPropertyName() . ', ' .
 						$this->sqlAccountName() . ', ' .
 						$this->sqlBilltoCompanyName() . ', ' .
 						$this->sqlBilltoNameFirst() . ', ' .
 						$this->sqlBilltoNameFirstEncrypted() . ', ' .
 						$this->sqlBilltoNameMiddle() . ', ' .
 						$this->sqlBilltoNameMiddleEncrypted() . ', ' .
 						$this->sqlBilltoNameLast() . ', ' .
 						$this->sqlBilltoNameLastEncrypted() . ', ' .
 						$this->sqlBilltoStreetLine1() . ', ' .
 						$this->sqlBilltoStreetLine2() . ', ' .
 						$this->sqlBilltoStreetLine3() . ', ' .
 						$this->sqlBilltoCity() . ', ' .
 						$this->sqlBilltoStateCode() . ', ' .
 						$this->sqlBilltoProvince() . ', ' .
 						$this->sqlBilltoPostalCode() . ', ' .
 						$this->sqlBilltoCountryCode() . ', ' .
 						$this->sqlBilltoPhoneNumber() . ', ' .
 						$this->sqlBilltoPhoneNumberEncrypted() . ', ' .
 						$this->sqlBilltoEmailAddress() . ', ' .
 						$this->sqlBilltoEmailAddressEncrypted() . ', ' .
 						$this->sqlBilltoTaxNumberEncrypted() . ', ' .
 						$this->sqlCheckAccountTypeId() . ', ' .
 						$this->sqlCheckNameOnAccount() . ', ' .
 						$this->sqlCheckBankName() . ', ' .
 						$this->sqlCheckRoutingNumber() . ', ' .
 						$this->sqlCheckAccountNumberEncrypted() . ', ' .
 						$this->sqlCheckTaxNumberEncrypted() . ', ' .
 						$this->sqlCheckTaxLegalName() . ', ' .
 						$this->sqlIsAlternateBillingAccount() . ', ' .
 						$this->sqlBillingAccountTypeId() . ', ' .
 						$this->sqlBillingNameOnAccount() . ', ' .
 						$this->sqlBillingBankName() . ', ' .
 						$this->sqlBillingRoutingNumber() . ', ' .
 						$this->sqlBillingAccountNumberEncrypted() . ', ' .
 						$this->sqlIsAlternateReturnAccount() . ', ' .
 						$this->sqlReturnAccountTypeId() . ', ' .
 						$this->sqlReturnNameOnAccount() . ', ' .
 						$this->sqlReturnBankName() . ', ' .
 						$this->sqlReturnRoutingNumber() . ', ' .
 						$this->sqlReturnAccountNumberEncrypted() . ', ' .
 						$this->sqlInvoicesEmailed() . ', ' .
 						$this->sqlInvoicesMailed() . ', ' .
 						$this->sqlEffectiveDate() . ', ' .
 						$this->sqlProcessedOn() . ', ' .
 						$this->sqlIsNewAccountRequest() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_change_request_id = ' . $this->sqlMerchantChangeRequestId() . ','; } elseif( true == array_key_exists( 'MerchantChangeRequestId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_change_request_id = ' . $this->sqlMerchantChangeRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_merchant_account_id = ' . $this->sqlCompanyMerchantAccountId() . ','; } elseif( true == array_key_exists( 'CompanyMerchantAccountId', $this->getChangedColumns() ) ) { $strSql .= ' company_merchant_account_id = ' . $this->sqlCompanyMerchantAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_change_request_account_type_id = ' . $this->sqlMerchantChangeRequestAccountTypeId() . ','; } elseif( true == array_key_exists( 'MerchantChangeRequestAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_change_request_account_type_id = ' . $this->sqlMerchantChangeRequestAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_processing_type_id = ' . $this->sqlMerchantProcessingTypeId() . ','; } elseif( true == array_key_exists( 'MerchantProcessingTypeId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_processing_type_id = ' . $this->sqlMerchantProcessingTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_change_request_account_id = ' . $this->sqlMerchantChangeRequestAccountId() . ','; } elseif( true == array_key_exists( 'MerchantChangeRequestAccountId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_change_request_account_id = ' . $this->sqlMerchantChangeRequestAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_account_id = ' . $this->sqlBillingAccountId() . ','; } elseif( true == array_key_exists( 'BillingAccountId', $this->getChangedColumns() ) ) { $strSql .= ' billing_account_id = ' . $this->sqlBillingAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_account_id = ' . $this->sqlReturnAccountId() . ','; } elseif( true == array_key_exists( 'ReturnAccountId', $this->getChangedColumns() ) ) { $strSql .= ' return_account_id = ' . $this->sqlReturnAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_name = ' . $this->sqlAccountName() . ','; } elseif( true == array_key_exists( 'AccountName', $this->getChangedColumns() ) ) { $strSql .= ' account_name = ' . $this->sqlAccountName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_company_name = ' . $this->sqlBilltoCompanyName() . ','; } elseif( true == array_key_exists( 'BilltoCompanyName', $this->getChangedColumns() ) ) { $strSql .= ' billto_company_name = ' . $this->sqlBilltoCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_name_first = ' . $this->sqlBilltoNameFirst() . ','; } elseif( true == array_key_exists( 'BilltoNameFirst', $this->getChangedColumns() ) ) { $strSql .= ' billto_name_first = ' . $this->sqlBilltoNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_name_first_encrypted = ' . $this->sqlBilltoNameFirstEncrypted() . ','; } elseif( true == array_key_exists( 'BilltoNameFirstEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' billto_name_first_encrypted = ' . $this->sqlBilltoNameFirstEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_name_middle = ' . $this->sqlBilltoNameMiddle() . ','; } elseif( true == array_key_exists( 'BilltoNameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' billto_name_middle = ' . $this->sqlBilltoNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_name_middle_encrypted = ' . $this->sqlBilltoNameMiddleEncrypted() . ','; } elseif( true == array_key_exists( 'BilltoNameMiddleEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' billto_name_middle_encrypted = ' . $this->sqlBilltoNameMiddleEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_name_last = ' . $this->sqlBilltoNameLast() . ','; } elseif( true == array_key_exists( 'BilltoNameLast', $this->getChangedColumns() ) ) { $strSql .= ' billto_name_last = ' . $this->sqlBilltoNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_name_last_encrypted = ' . $this->sqlBilltoNameLastEncrypted() . ','; } elseif( true == array_key_exists( 'BilltoNameLastEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' billto_name_last_encrypted = ' . $this->sqlBilltoNameLastEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line1 = ' . $this->sqlBilltoStreetLine1() . ','; } elseif( true == array_key_exists( 'BilltoStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line1 = ' . $this->sqlBilltoStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line2 = ' . $this->sqlBilltoStreetLine2() . ','; } elseif( true == array_key_exists( 'BilltoStreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line2 = ' . $this->sqlBilltoStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line3 = ' . $this->sqlBilltoStreetLine3() . ','; } elseif( true == array_key_exists( 'BilltoStreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line3 = ' . $this->sqlBilltoStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_city = ' . $this->sqlBilltoCity() . ','; } elseif( true == array_key_exists( 'BilltoCity', $this->getChangedColumns() ) ) { $strSql .= ' billto_city = ' . $this->sqlBilltoCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_state_code = ' . $this->sqlBilltoStateCode() . ','; } elseif( true == array_key_exists( 'BilltoStateCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_state_code = ' . $this->sqlBilltoStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_province = ' . $this->sqlBilltoProvince() . ','; } elseif( true == array_key_exists( 'BilltoProvince', $this->getChangedColumns() ) ) { $strSql .= ' billto_province = ' . $this->sqlBilltoProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_postal_code = ' . $this->sqlBilltoPostalCode() . ','; } elseif( true == array_key_exists( 'BilltoPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_postal_code = ' . $this->sqlBilltoPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_country_code = ' . $this->sqlBilltoCountryCode() . ','; } elseif( true == array_key_exists( 'BilltoCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_country_code = ' . $this->sqlBilltoCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_phone_number = ' . $this->sqlBilltoPhoneNumber() . ','; } elseif( true == array_key_exists( 'BilltoPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' billto_phone_number = ' . $this->sqlBilltoPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_phone_number_encrypted = ' . $this->sqlBilltoPhoneNumberEncrypted() . ','; } elseif( true == array_key_exists( 'BilltoPhoneNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' billto_phone_number_encrypted = ' . $this->sqlBilltoPhoneNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_email_address = ' . $this->sqlBilltoEmailAddress() . ','; } elseif( true == array_key_exists( 'BilltoEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' billto_email_address = ' . $this->sqlBilltoEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_email_address_encrypted = ' . $this->sqlBilltoEmailAddressEncrypted() . ','; } elseif( true == array_key_exists( 'BilltoEmailAddressEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' billto_email_address_encrypted = ' . $this->sqlBilltoEmailAddressEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_tax_number_encrypted = ' . $this->sqlBilltoTaxNumberEncrypted() . ','; } elseif( true == array_key_exists( 'BilltoTaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' billto_tax_number_encrypted = ' . $this->sqlBilltoTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId() . ','; } elseif( true == array_key_exists( 'CheckAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount() . ','; } elseif( true == array_key_exists( 'CheckNameOnAccount', $this->getChangedColumns() ) ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName() . ','; } elseif( true == array_key_exists( 'CheckBankName', $this->getChangedColumns() ) ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber() . ','; } elseif( true == array_key_exists( 'CheckRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted() . ','; } elseif( true == array_key_exists( 'CheckAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_tax_number_encrypted = ' . $this->sqlCheckTaxNumberEncrypted() . ','; } elseif( true == array_key_exists( 'CheckTaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' check_tax_number_encrypted = ' . $this->sqlCheckTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_tax_legal_name = ' . $this->sqlCheckTaxLegalName() . ','; } elseif( true == array_key_exists( 'CheckTaxLegalName', $this->getChangedColumns() ) ) { $strSql .= ' check_tax_legal_name = ' . $this->sqlCheckTaxLegalName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_alternate_billing_account = ' . $this->sqlIsAlternateBillingAccount() . ','; } elseif( true == array_key_exists( 'IsAlternateBillingAccount', $this->getChangedColumns() ) ) { $strSql .= ' is_alternate_billing_account = ' . $this->sqlIsAlternateBillingAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_account_type_id = ' . $this->sqlBillingAccountTypeId() . ','; } elseif( true == array_key_exists( 'BillingAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' billing_account_type_id = ' . $this->sqlBillingAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_name_on_account = ' . $this->sqlBillingNameOnAccount() . ','; } elseif( true == array_key_exists( 'BillingNameOnAccount', $this->getChangedColumns() ) ) { $strSql .= ' billing_name_on_account = ' . $this->sqlBillingNameOnAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_bank_name = ' . $this->sqlBillingBankName() . ','; } elseif( true == array_key_exists( 'BillingBankName', $this->getChangedColumns() ) ) { $strSql .= ' billing_bank_name = ' . $this->sqlBillingBankName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_routing_number = ' . $this->sqlBillingRoutingNumber() . ','; } elseif( true == array_key_exists( 'BillingRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' billing_routing_number = ' . $this->sqlBillingRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_account_number_encrypted = ' . $this->sqlBillingAccountNumberEncrypted() . ','; } elseif( true == array_key_exists( 'BillingAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' billing_account_number_encrypted = ' . $this->sqlBillingAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_alternate_return_account = ' . $this->sqlIsAlternateReturnAccount() . ','; } elseif( true == array_key_exists( 'IsAlternateReturnAccount', $this->getChangedColumns() ) ) { $strSql .= ' is_alternate_return_account = ' . $this->sqlIsAlternateReturnAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_account_type_id = ' . $this->sqlReturnAccountTypeId() . ','; } elseif( true == array_key_exists( 'ReturnAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' return_account_type_id = ' . $this->sqlReturnAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_name_on_account = ' . $this->sqlReturnNameOnAccount() . ','; } elseif( true == array_key_exists( 'ReturnNameOnAccount', $this->getChangedColumns() ) ) { $strSql .= ' return_name_on_account = ' . $this->sqlReturnNameOnAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_bank_name = ' . $this->sqlReturnBankName() . ','; } elseif( true == array_key_exists( 'ReturnBankName', $this->getChangedColumns() ) ) { $strSql .= ' return_bank_name = ' . $this->sqlReturnBankName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_routing_number = ' . $this->sqlReturnRoutingNumber() . ','; } elseif( true == array_key_exists( 'ReturnRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' return_routing_number = ' . $this->sqlReturnRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_account_number_encrypted = ' . $this->sqlReturnAccountNumberEncrypted() . ','; } elseif( true == array_key_exists( 'ReturnAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' return_account_number_encrypted = ' . $this->sqlReturnAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoices_emailed = ' . $this->sqlInvoicesEmailed() . ','; } elseif( true == array_key_exists( 'InvoicesEmailed', $this->getChangedColumns() ) ) { $strSql .= ' invoices_emailed = ' . $this->sqlInvoicesEmailed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoices_mailed = ' . $this->sqlInvoicesMailed() . ','; } elseif( true == array_key_exists( 'InvoicesMailed', $this->getChangedColumns() ) ) { $strSql .= ' invoices_mailed = ' . $this->sqlInvoicesMailed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_new_account_request = ' . $this->sqlIsNewAccountRequest() . ','; } elseif( true == array_key_exists( 'IsNewAccountRequest', $this->getChangedColumns() ) ) { $strSql .= ' is_new_account_request = ' . $this->sqlIsNewAccountRequest() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'merchant_change_request_id' => $this->getMerchantChangeRequestId(),
			'company_merchant_account_id' => $this->getCompanyMerchantAccountId(),
			'merchant_change_request_account_type_id' => $this->getMerchantChangeRequestAccountTypeId(),
			'merchant_processing_type_id' => $this->getMerchantProcessingTypeId(),
			'merchant_change_request_account_id' => $this->getMerchantChangeRequestAccountId(),
			'account_id' => $this->getAccountId(),
			'billing_account_id' => $this->getBillingAccountId(),
			'return_account_id' => $this->getReturnAccountId(),
			'property_name' => $this->getPropertyName(),
			'account_name' => $this->getAccountName(),
			'billto_company_name' => $this->getBilltoCompanyName(),
			'billto_name_first' => $this->getBilltoNameFirst(),
			'billto_name_first_encrypted' => $this->getBilltoNameFirstEncrypted(),
			'billto_name_middle' => $this->getBilltoNameMiddle(),
			'billto_name_middle_encrypted' => $this->getBilltoNameMiddleEncrypted(),
			'billto_name_last' => $this->getBilltoNameLast(),
			'billto_name_last_encrypted' => $this->getBilltoNameLastEncrypted(),
			'billto_street_line1' => $this->getBilltoStreetLine1(),
			'billto_street_line2' => $this->getBilltoStreetLine2(),
			'billto_street_line3' => $this->getBilltoStreetLine3(),
			'billto_city' => $this->getBilltoCity(),
			'billto_state_code' => $this->getBilltoStateCode(),
			'billto_province' => $this->getBilltoProvince(),
			'billto_postal_code' => $this->getBilltoPostalCode(),
			'billto_country_code' => $this->getBilltoCountryCode(),
			'billto_phone_number' => $this->getBilltoPhoneNumber(),
			'billto_phone_number_encrypted' => $this->getBilltoPhoneNumberEncrypted(),
			'billto_email_address' => $this->getBilltoEmailAddress(),
			'billto_email_address_encrypted' => $this->getBilltoEmailAddressEncrypted(),
			'billto_tax_number_encrypted' => $this->getBilltoTaxNumberEncrypted(),
			'check_account_type_id' => $this->getCheckAccountTypeId(),
			'check_name_on_account' => $this->getCheckNameOnAccount(),
			'check_bank_name' => $this->getCheckBankName(),
			'check_routing_number' => $this->getCheckRoutingNumber(),
			'check_account_number_encrypted' => $this->getCheckAccountNumberEncrypted(),
			'check_tax_number_encrypted' => $this->getCheckTaxNumberEncrypted(),
			'check_tax_legal_name' => $this->getCheckTaxLegalName(),
			'is_alternate_billing_account' => $this->getIsAlternateBillingAccount(),
			'billing_account_type_id' => $this->getBillingAccountTypeId(),
			'billing_name_on_account' => $this->getBillingNameOnAccount(),
			'billing_bank_name' => $this->getBillingBankName(),
			'billing_routing_number' => $this->getBillingRoutingNumber(),
			'billing_account_number_encrypted' => $this->getBillingAccountNumberEncrypted(),
			'is_alternate_return_account' => $this->getIsAlternateReturnAccount(),
			'return_account_type_id' => $this->getReturnAccountTypeId(),
			'return_name_on_account' => $this->getReturnNameOnAccount(),
			'return_bank_name' => $this->getReturnBankName(),
			'return_routing_number' => $this->getReturnRoutingNumber(),
			'return_account_number_encrypted' => $this->getReturnAccountNumberEncrypted(),
			'invoices_emailed' => $this->getInvoicesEmailed(),
			'invoices_mailed' => $this->getInvoicesMailed(),
			'effective_date' => $this->getEffectiveDate(),
			'processed_on' => $this->getProcessedOn(),
			'is_new_account_request' => $this->getIsNewAccountRequest(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>