<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMerchantChangeRequests
 * Do not add any new functions to this class.
 */

class CBaseMerchantChangeRequests extends CEosPluralBase {

	/**
	 * @return CMerchantChangeRequest[]
	 */
	public static function fetchMerchantChangeRequests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMerchantChangeRequest::class, $objDatabase );
	}

	/**
	 * @return CMerchantChangeRequest
	 */
	public static function fetchMerchantChangeRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMerchantChangeRequest::class, $objDatabase );
	}

	public static function fetchMerchantChangeRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'merchant_change_requests', $objDatabase );
	}

	public static function fetchMerchantChangeRequestById( $intId, $objDatabase ) {
		return self::fetchMerchantChangeRequest( sprintf( 'SELECT * FROM merchant_change_requests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMerchantChangeRequestsByCid( $intCid, $objDatabase ) {
		return self::fetchMerchantChangeRequests( sprintf( 'SELECT * FROM merchant_change_requests WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantChangeRequestsBySignatureDocumentId( $intSignatureDocumentId, $objDatabase ) {
		return self::fetchMerchantChangeRequests( sprintf( 'SELECT * FROM merchant_change_requests WHERE signature_document_id = %d', ( int ) $intSignatureDocumentId ), $objDatabase );
	}

}
?>