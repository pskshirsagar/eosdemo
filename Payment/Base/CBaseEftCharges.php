<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CEftCharges
 * Do not add any new functions to this class.
 */

class CBaseEftCharges extends CEosPluralBase {

	/**
	 * @return CEftCharge[]
	 */
	public static function fetchEftCharges( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEftCharge::class, $objDatabase );
	}

	/**
	 * @return CEftCharge
	 */
	public static function fetchEftCharge( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEftCharge::class, $objDatabase );
	}

	public static function fetchEftChargeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'eft_charges', $objDatabase );
	}

	public static function fetchEftChargeById( $intId, $objDatabase ) {
		return self::fetchEftCharge( sprintf( 'SELECT * FROM eft_charges WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEftChargesByCid( $intCid, $objDatabase ) {
		return self::fetchEftCharges( sprintf( 'SELECT * FROM eft_charges WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEftChargesByEftChargeTypeId( $intEftChargeTypeId, $objDatabase ) {
		return self::fetchEftCharges( sprintf( 'SELECT * FROM eft_charges WHERE eft_charge_type_id = %d', ( int ) $intEftChargeTypeId ), $objDatabase );
	}

	public static function fetchEftChargesByCompanyMerchantAccountId( $intCompanyMerchantAccountId, $objDatabase ) {
		return self::fetchEftCharges( sprintf( 'SELECT * FROM eft_charges WHERE company_merchant_account_id = %d', ( int ) $intCompanyMerchantAccountId ), $objDatabase );
	}

	public static function fetchEftChargesByEftInstructionId( $intEftInstructionId, $objDatabase ) {
		return self::fetchEftCharges( sprintf( 'SELECT * FROM eft_charges WHERE eft_instruction_id = %d', ( int ) $intEftInstructionId ), $objDatabase );
	}

	public static function fetchEftChargesByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchEftCharges( sprintf( 'SELECT * FROM eft_charges WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

	public static function fetchEftChargesBySettlementDistributionId( $intSettlementDistributionId, $objDatabase ) {
		return self::fetchEftCharges( sprintf( 'SELECT * FROM eft_charges WHERE settlement_distribution_id = %d', ( int ) $intSettlementDistributionId ), $objDatabase );
	}

	public static function fetchEftChargesByCompanyTransactionId( $intCompanyTransactionId, $objDatabase ) {
		return self::fetchEftCharges( sprintf( 'SELECT * FROM eft_charges WHERE company_transaction_id = %d', ( int ) $intCompanyTransactionId ), $objDatabase );
	}

	public static function fetchEftChargesByTransactionId( $intTransactionId, $objDatabase ) {
		return self::fetchEftCharges( sprintf( 'SELECT * FROM eft_charges WHERE transaction_id = %d', ( int ) $intTransactionId ), $objDatabase );
	}

	public static function fetchEftChargesByClearingTransactionId( $intClearingTransactionId, $objDatabase ) {
		return self::fetchEftCharges( sprintf( 'SELECT * FROM eft_charges WHERE clearing_transaction_id = %d', ( int ) $intClearingTransactionId ), $objDatabase );
	}

	public static function fetchEftChargesByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchEftCharges( sprintf( 'SELECT * FROM eft_charges WHERE account_id = %d', ( int ) $intAccountId ), $objDatabase );
	}

	public static function fetchEftChargesByChargeCodeId( $intChargeCodeId, $objDatabase ) {
		return self::fetchEftCharges( sprintf( 'SELECT * FROM eft_charges WHERE charge_code_id = %d', ( int ) $intChargeCodeId ), $objDatabase );
	}

	public static function fetchEftChargesByEftChargeId( $intEftChargeId, $objDatabase ) {
		return self::fetchEftCharges( sprintf( 'SELECT * FROM eft_charges WHERE eft_charge_id = %d', ( int ) $intEftChargeId ), $objDatabase );
	}

	public static function fetchEftChargesByGiactRequestLogId( $intGiactRequestLogId, $objDatabase ) {
		return self::fetchEftCharges( sprintf( 'SELECT * FROM eft_charges WHERE giact_request_log_id = %d', ( int ) $intGiactRequestLogId ), $objDatabase );
	}

}
?>