<?php

class CBaseOfacAlternateName extends CEosSingularBase {

	const TABLE_NAME = 'public.ofac_alternate_names';

	protected $m_intId;
	protected $m_intOfacId;
	protected $m_strOfacAlternateType;
	protected $m_strAlternateName;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ofac_id'] ) && $boolDirectSet ) $this->set( 'm_intOfacId', trim( $arrValues['ofac_id'] ) ); elseif( isset( $arrValues['ofac_id'] ) ) $this->setOfacId( $arrValues['ofac_id'] );
		if( isset( $arrValues['ofac_alternate_type'] ) && $boolDirectSet ) $this->set( 'm_strOfacAlternateType', trim( stripcslashes( $arrValues['ofac_alternate_type'] ) ) ); elseif( isset( $arrValues['ofac_alternate_type'] ) ) $this->setOfacAlternateType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ofac_alternate_type'] ) : $arrValues['ofac_alternate_type'] );
		if( isset( $arrValues['alternate_name'] ) && $boolDirectSet ) $this->set( 'm_strAlternateName', trim( stripcslashes( $arrValues['alternate_name'] ) ) ); elseif( isset( $arrValues['alternate_name'] ) ) $this->setAlternateName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['alternate_name'] ) : $arrValues['alternate_name'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOfacId( $intOfacId ) {
		$this->set( 'm_intOfacId', CStrings::strToIntDef( $intOfacId, NULL, false ) );
	}

	public function getOfacId() {
		return $this->m_intOfacId;
	}

	public function sqlOfacId() {
		return ( true == isset( $this->m_intOfacId ) ) ? ( string ) $this->m_intOfacId : 'NULL';
	}

	public function setOfacAlternateType( $strOfacAlternateType ) {
		$this->set( 'm_strOfacAlternateType', CStrings::strTrimDef( $strOfacAlternateType, 8, NULL, true ) );
	}

	public function getOfacAlternateType() {
		return $this->m_strOfacAlternateType;
	}

	public function sqlOfacAlternateType() {
		return ( true == isset( $this->m_strOfacAlternateType ) ) ? '\'' . addslashes( $this->m_strOfacAlternateType ) . '\'' : 'NULL';
	}

	public function setAlternateName( $strAlternateName ) {
		$this->set( 'm_strAlternateName', CStrings::strTrimDef( $strAlternateName, 350, NULL, true ) );
	}

	public function getAlternateName() {
		return $this->m_strAlternateName;
	}

	public function sqlAlternateName() {
		return ( true == isset( $this->m_strAlternateName ) ) ? '\'' . addslashes( $this->m_strAlternateName ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ofac_id, ofac_alternate_type, alternate_name, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlOfacId() . ', ' .
 						$this->sqlOfacAlternateType() . ', ' .
 						$this->sqlAlternateName() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ofac_id = ' . $this->sqlOfacId() . ','; } elseif( true == array_key_exists( 'OfacId', $this->getChangedColumns() ) ) { $strSql .= ' ofac_id = ' . $this->sqlOfacId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ofac_alternate_type = ' . $this->sqlOfacAlternateType() . ','; } elseif( true == array_key_exists( 'OfacAlternateType', $this->getChangedColumns() ) ) { $strSql .= ' ofac_alternate_type = ' . $this->sqlOfacAlternateType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alternate_name = ' . $this->sqlAlternateName() . ','; } elseif( true == array_key_exists( 'AlternateName', $this->getChangedColumns() ) ) { $strSql .= ' alternate_name = ' . $this->sqlAlternateName() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ofac_id' => $this->getOfacId(),
			'ofac_alternate_type' => $this->getOfacAlternateType(),
			'alternate_name' => $this->getAlternateName(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>