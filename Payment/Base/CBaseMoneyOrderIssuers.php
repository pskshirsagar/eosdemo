<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMoneyOrderIssuers
 * Do not add any new functions to this class.
 */

class CBaseMoneyOrderIssuers extends CEosPluralBase {

	/**
	 * @return CMoneyOrderIssuer[]
	 */
	public static function fetchMoneyOrderIssuers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMoneyOrderIssuer::class, $objDatabase );
	}

	/**
	 * @return CMoneyOrderIssuer
	 */
	public static function fetchMoneyOrderIssuer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMoneyOrderIssuer::class, $objDatabase );
	}

	public static function fetchMoneyOrderIssuerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'money_order_issuers', $objDatabase );
	}

	public static function fetchMoneyOrderIssuerById( $intId, $objDatabase ) {
		return self::fetchMoneyOrderIssuer( sprintf( 'SELECT * FROM money_order_issuers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>