<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentNoteTypes
 * Do not add any new functions to this class.
 */

class CBaseArPaymentNoteTypes extends CEosPluralBase {

	/**
	 * @return CArPaymentNoteType[]
	 */
	public static function fetchArPaymentNoteTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CArPaymentNoteType::class, $objDatabase );
	}

	/**
	 * @return CArPaymentNoteType
	 */
	public static function fetchArPaymentNoteType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CArPaymentNoteType::class, $objDatabase );
	}

	public static function fetchArPaymentNoteTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ar_payment_note_types', $objDatabase );
	}

	public static function fetchArPaymentNoteTypeById( $intId, $objDatabase ) {
		return self::fetchArPaymentNoteType( sprintf( 'SELECT * FROM ar_payment_note_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>