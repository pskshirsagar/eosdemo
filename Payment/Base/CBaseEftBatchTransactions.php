<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CEftBatchTransactions
 * Do not add any new functions to this class.
 */

class CBaseEftBatchTransactions extends CEosPluralBase {

	/**
	 * @return CEftBatchTransaction[]
	 */
	public static function fetchEftBatchTransactions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEftBatchTransaction::class, $objDatabase );
	}

	/**
	 * @return CEftBatchTransaction
	 */
	public static function fetchEftBatchTransaction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEftBatchTransaction::class, $objDatabase );
	}

	public static function fetchEftBatchTransactionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'eft_batch_transactions', $objDatabase );
	}

	public static function fetchEftBatchTransactionById( $intId, $objDatabase ) {
		return self::fetchEftBatchTransaction( sprintf( 'SELECT * FROM eft_batch_transactions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEftBatchTransactionsByEftBatchId( $intEftBatchId, $objDatabase ) {
		return self::fetchEftBatchTransactions( sprintf( 'SELECT * FROM eft_batch_transactions WHERE eft_batch_id = %d', ( int ) $intEftBatchId ), $objDatabase );
	}

	public static function fetchEftBatchTransactionsByItemId( $intItemId, $objDatabase ) {
		return self::fetchEftBatchTransactions( sprintf( 'SELECT * FROM eft_batch_transactions WHERE item_id = %d', ( int ) $intItemId ), $objDatabase );
	}

}
?>