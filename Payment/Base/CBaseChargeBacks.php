<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CChargeBacks
 * Do not add any new functions to this class.
 */

class CBaseChargeBacks extends CEosPluralBase {

	/**
	 * @return CChargeBack[]
	 */
	public static function fetchChargeBacks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CChargeBack::class, $objDatabase );
	}

	/**
	 * @return CChargeBack
	 */
	public static function fetchChargeBack( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChargeBack::class, $objDatabase );
	}

	public static function fetchChargeBackCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'charge_backs', $objDatabase );
	}

	public static function fetchChargeBackById( $intId, $objDatabase ) {
		return self::fetchChargeBack( sprintf( 'SELECT * FROM charge_backs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchChargeBacksByCid( $intCid, $objDatabase ) {
		return self::fetchChargeBacks( sprintf( 'SELECT * FROM charge_backs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChargeBacksByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchChargeBacks( sprintf( 'SELECT * FROM charge_backs WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

	public static function fetchChargeBacksByEftId( $intEftId, $objDatabase ) {
		return self::fetchChargeBacks( sprintf( 'SELECT * FROM charge_backs WHERE eft_id = %d', ( int ) $intEftId ), $objDatabase );
	}

}
?>