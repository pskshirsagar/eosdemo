<?php

class CBaseProcessingBankAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.processing_bank_accounts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intProcessingBankId;
	protected $m_intFundMovementTypeId;
	protected $m_strCurrencyCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strRoutingNumber;
	protected $m_strAccountNumberEncrypted;
	protected $m_strLastReconciledOn;
	protected $m_intRequiresReconciliation;
	protected $m_strX937ImmOrigRoutingNumber;
	protected $m_intIsPublished;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strAccountNumberBindex;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';
		$this->m_intRequiresReconciliation = '1';
		$this->m_intIsPublished = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['processing_bank_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankId', trim( $arrValues['processing_bank_id'] ) ); elseif( isset( $arrValues['processing_bank_id'] ) ) $this->setProcessingBankId( $arrValues['processing_bank_id'] );
		if( isset( $arrValues['fund_movement_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFundMovementTypeId', trim( $arrValues['fund_movement_type_id'] ) ); elseif( isset( $arrValues['fund_movement_type_id'] ) ) $this->setFundMovementTypeId( $arrValues['fund_movement_type_id'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( $arrValues['currency_code'] ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( $arrValues['currency_code'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['routing_number'] ) && $boolDirectSet ) $this->set( 'm_strRoutingNumber', trim( $arrValues['routing_number'] ) ); elseif( isset( $arrValues['routing_number'] ) ) $this->setRoutingNumber( $arrValues['routing_number'] );
		if( isset( $arrValues['account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumberEncrypted', trim( $arrValues['account_number_encrypted'] ) ); elseif( isset( $arrValues['account_number_encrypted'] ) ) $this->setAccountNumberEncrypted( $arrValues['account_number_encrypted'] );
		if( isset( $arrValues['last_reconciled_on'] ) && $boolDirectSet ) $this->set( 'm_strLastReconciledOn', trim( $arrValues['last_reconciled_on'] ) ); elseif( isset( $arrValues['last_reconciled_on'] ) ) $this->setLastReconciledOn( $arrValues['last_reconciled_on'] );
		if( isset( $arrValues['requires_reconciliation'] ) && $boolDirectSet ) $this->set( 'm_intRequiresReconciliation', trim( $arrValues['requires_reconciliation'] ) ); elseif( isset( $arrValues['requires_reconciliation'] ) ) $this->setRequiresReconciliation( $arrValues['requires_reconciliation'] );
		if( isset( $arrValues['x937_imm_orig_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strX937ImmOrigRoutingNumber', trim( $arrValues['x937_imm_orig_routing_number'] ) ); elseif( isset( $arrValues['x937_imm_orig_routing_number'] ) ) $this->setX937ImmOrigRoutingNumber( $arrValues['x937_imm_orig_routing_number'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['account_number_bindex'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumberBindex', trim( $arrValues['account_number_bindex'] ) ); elseif( isset( $arrValues['account_number_bindex'] ) ) $this->setAccountNumberBindex( $arrValues['account_number_bindex'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setProcessingBankId( $intProcessingBankId ) {
		$this->set( 'm_intProcessingBankId', CStrings::strToIntDef( $intProcessingBankId, NULL, false ) );
	}

	public function getProcessingBankId() {
		return $this->m_intProcessingBankId;
	}

	public function sqlProcessingBankId() {
		return ( true == isset( $this->m_intProcessingBankId ) ) ? ( string ) $this->m_intProcessingBankId : 'NULL';
	}

	public function setFundMovementTypeId( $intFundMovementTypeId ) {
		$this->set( 'm_intFundMovementTypeId', CStrings::strToIntDef( $intFundMovementTypeId, NULL, false ) );
	}

	public function getFundMovementTypeId() {
		return $this->m_intFundMovementTypeId;
	}

	public function sqlFundMovementTypeId() {
		return ( true == isset( $this->m_intFundMovementTypeId ) ) ? ( string ) $this->m_intFundMovementTypeId : 'NULL';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCurrencyCode ) : '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' ) : '\'USD\'';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setRoutingNumber( $strRoutingNumber ) {
		$this->set( 'm_strRoutingNumber', CStrings::strTrimDef( $strRoutingNumber, 240, NULL, true ) );
	}

	public function getRoutingNumber() {
		return $this->m_strRoutingNumber;
	}

	public function sqlRoutingNumber() {
		return ( true == isset( $this->m_strRoutingNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRoutingNumber ) : '\'' . addslashes( $this->m_strRoutingNumber ) . '\'' ) : 'NULL';
	}

	public function setAccountNumberEncrypted( $strAccountNumberEncrypted ) {
		$this->set( 'm_strAccountNumberEncrypted', CStrings::strTrimDef( $strAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getAccountNumberEncrypted() {
		return $this->m_strAccountNumberEncrypted;
	}

	public function sqlAccountNumberEncrypted() {
		return ( true == isset( $this->m_strAccountNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAccountNumberEncrypted ) : '\'' . addslashes( $this->m_strAccountNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setLastReconciledOn( $strLastReconciledOn ) {
		$this->set( 'm_strLastReconciledOn', CStrings::strTrimDef( $strLastReconciledOn, -1, NULL, true ) );
	}

	public function getLastReconciledOn() {
		return $this->m_strLastReconciledOn;
	}

	public function sqlLastReconciledOn() {
		return ( true == isset( $this->m_strLastReconciledOn ) ) ? '\'' . $this->m_strLastReconciledOn . '\'' : 'NULL';
	}

	public function setRequiresReconciliation( $intRequiresReconciliation ) {
		$this->set( 'm_intRequiresReconciliation', CStrings::strToIntDef( $intRequiresReconciliation, NULL, false ) );
	}

	public function getRequiresReconciliation() {
		return $this->m_intRequiresReconciliation;
	}

	public function sqlRequiresReconciliation() {
		return ( true == isset( $this->m_intRequiresReconciliation ) ) ? ( string ) $this->m_intRequiresReconciliation : '1';
	}

	public function setX937ImmOrigRoutingNumber( $strX937ImmOrigRoutingNumber ) {
		$this->set( 'm_strX937ImmOrigRoutingNumber', CStrings::strTrimDef( $strX937ImmOrigRoutingNumber, 240, NULL, true ) );
	}

	public function getX937ImmOrigRoutingNumber() {
		return $this->m_strX937ImmOrigRoutingNumber;
	}

	public function sqlX937ImmOrigRoutingNumber() {
		return ( true == isset( $this->m_strX937ImmOrigRoutingNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strX937ImmOrigRoutingNumber ) : '\'' . addslashes( $this->m_strX937ImmOrigRoutingNumber ) . '\'' ) : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAccountNumberBindex( $strAccountNumberBindex ) {
		$this->set( 'm_strAccountNumberBindex', CStrings::strTrimDef( $strAccountNumberBindex, -1, NULL, true ) );
	}

	public function getAccountNumberBindex() {
		return $this->m_strAccountNumberBindex;
	}

	public function sqlAccountNumberBindex() {
		return ( true == isset( $this->m_strAccountNumberBindex ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAccountNumberBindex ) : '\'' . addslashes( $this->m_strAccountNumberBindex ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, processing_bank_id, fund_movement_type_id, currency_code, name, description, routing_number, account_number_encrypted, last_reconciled_on, requires_reconciliation, x937_imm_orig_routing_number, is_published, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, account_number_bindex )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlProcessingBankId() . ', ' .
						$this->sqlFundMovementTypeId() . ', ' .
						$this->sqlCurrencyCode() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlRoutingNumber() . ', ' .
						$this->sqlAccountNumberEncrypted() . ', ' .
						$this->sqlLastReconciledOn() . ', ' .
						$this->sqlRequiresReconciliation() . ', ' .
						$this->sqlX937ImmOrigRoutingNumber() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlAccountNumberBindex() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId(). ',' ; } elseif( true == array_key_exists( 'ProcessingBankId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fund_movement_type_id = ' . $this->sqlFundMovementTypeId(). ',' ; } elseif( true == array_key_exists( 'FundMovementTypeId', $this->getChangedColumns() ) ) { $strSql .= ' fund_movement_type_id = ' . $this->sqlFundMovementTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode(). ',' ; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' routing_number = ' . $this->sqlRoutingNumber(). ',' ; } elseif( true == array_key_exists( 'RoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' routing_number = ' . $this->sqlRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number_encrypted = ' . $this->sqlAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'AccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' account_number_encrypted = ' . $this->sqlAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_reconciled_on = ' . $this->sqlLastReconciledOn(). ',' ; } elseif( true == array_key_exists( 'LastReconciledOn', $this->getChangedColumns() ) ) { $strSql .= ' last_reconciled_on = ' . $this->sqlLastReconciledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_reconciliation = ' . $this->sqlRequiresReconciliation(). ',' ; } elseif( true == array_key_exists( 'RequiresReconciliation', $this->getChangedColumns() ) ) { $strSql .= ' requires_reconciliation = ' . $this->sqlRequiresReconciliation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_imm_orig_routing_number = ' . $this->sqlX937ImmOrigRoutingNumber(). ',' ; } elseif( true == array_key_exists( 'X937ImmOrigRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' x937_imm_orig_routing_number = ' . $this->sqlX937ImmOrigRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number_bindex = ' . $this->sqlAccountNumberBindex(). ',' ; } elseif( true == array_key_exists( 'AccountNumberBindex', $this->getChangedColumns() ) ) { $strSql .= ' account_number_bindex = ' . $this->sqlAccountNumberBindex() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'processing_bank_id' => $this->getProcessingBankId(),
			'fund_movement_type_id' => $this->getFundMovementTypeId(),
			'currency_code' => $this->getCurrencyCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'routing_number' => $this->getRoutingNumber(),
			'account_number_encrypted' => $this->getAccountNumberEncrypted(),
			'last_reconciled_on' => $this->getLastReconciledOn(),
			'requires_reconciliation' => $this->getRequiresReconciliation(),
			'x937_imm_orig_routing_number' => $this->getX937ImmOrigRoutingNumber(),
			'is_published' => $this->getIsPublished(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'account_number_bindex' => $this->getAccountNumberBindex()
		);
	}

}
?>