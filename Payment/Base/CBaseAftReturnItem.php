<?php

class CBaseAftReturnItem extends CEosSingularBase {

	const TABLE_NAME = 'public.aft_return_items';

	protected $m_intId;
	protected $m_intAftReturnFileId;
	protected $m_strReturnTypeId;
	protected $m_intSettlementDistributionId;
	protected $m_intEftInstructionId;
	protected $m_intPaymentBlacklistEntryId;
	protected $m_strLogicalRecordTypeCode;
	protected $m_strTransactionType;
	protected $m_strAmount;
	protected $m_strPayableDate;
	protected $m_strDestinationInstitutionId;
	protected $m_strDestinationAccountNumberEncrypted;
	protected $m_strShortName;
	protected $m_strLongName;
	protected $m_strNameOnAccount;
	protected $m_strIdentificationNumber;
	protected $m_strReturnInstitutionId;
	protected $m_strReturnAccountNumberEncrypted;
	protected $m_strReturnReasonCode;
	protected $m_strCorrectedInstitutionId;
	protected $m_strCorrectedAccountNumberEncrypted;
	protected $m_boolIsNoticeOfChange;
	protected $m_boolIsReconPrepped;
	protected $m_strProcessedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsNoticeOfChange = false;
		$this->m_boolIsReconPrepped = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['aft_return_file_id'] ) && $boolDirectSet ) $this->set( 'm_intAftReturnFileId', trim( $arrValues['aft_return_file_id'] ) ); elseif( isset( $arrValues['aft_return_file_id'] ) ) $this->setAftReturnFileId( $arrValues['aft_return_file_id'] );
		if( isset( $arrValues['return_type_id'] ) && $boolDirectSet ) $this->set( 'm_strReturnTypeId', trim( $arrValues['return_type_id'] ) ); elseif( isset( $arrValues['return_type_id'] ) ) $this->setReturnTypeId( $arrValues['return_type_id'] );
		if( isset( $arrValues['settlement_distribution_id'] ) && $boolDirectSet ) $this->set( 'm_intSettlementDistributionId', trim( $arrValues['settlement_distribution_id'] ) ); elseif( isset( $arrValues['settlement_distribution_id'] ) ) $this->setSettlementDistributionId( $arrValues['settlement_distribution_id'] );
		if( isset( $arrValues['eft_instruction_id'] ) && $boolDirectSet ) $this->set( 'm_intEftInstructionId', trim( $arrValues['eft_instruction_id'] ) ); elseif( isset( $arrValues['eft_instruction_id'] ) ) $this->setEftInstructionId( $arrValues['eft_instruction_id'] );
		if( isset( $arrValues['payment_blacklist_entry_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentBlacklistEntryId', trim( $arrValues['payment_blacklist_entry_id'] ) ); elseif( isset( $arrValues['payment_blacklist_entry_id'] ) ) $this->setPaymentBlacklistEntryId( $arrValues['payment_blacklist_entry_id'] );
		if( isset( $arrValues['logical_record_type_code'] ) && $boolDirectSet ) $this->set( 'm_strLogicalRecordTypeCode', trim( $arrValues['logical_record_type_code'] ) ); elseif( isset( $arrValues['logical_record_type_code'] ) ) $this->setLogicalRecordTypeCode( $arrValues['logical_record_type_code'] );
		if( isset( $arrValues['transaction_type'] ) && $boolDirectSet ) $this->set( 'm_strTransactionType', trim( $arrValues['transaction_type'] ) ); elseif( isset( $arrValues['transaction_type'] ) ) $this->setTransactionType( $arrValues['transaction_type'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_strAmount', trim( $arrValues['amount'] ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['payable_date'] ) && $boolDirectSet ) $this->set( 'm_strPayableDate', trim( $arrValues['payable_date'] ) ); elseif( isset( $arrValues['payable_date'] ) ) $this->setPayableDate( $arrValues['payable_date'] );
		if( isset( $arrValues['destination_institution_id'] ) && $boolDirectSet ) $this->set( 'm_strDestinationInstitutionId', trim( $arrValues['destination_institution_id'] ) ); elseif( isset( $arrValues['destination_institution_id'] ) ) $this->setDestinationInstitutionId( $arrValues['destination_institution_id'] );
		if( isset( $arrValues['destination_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strDestinationAccountNumberEncrypted', trim( $arrValues['destination_account_number_encrypted'] ) ); elseif( isset( $arrValues['destination_account_number_encrypted'] ) ) $this->setDestinationAccountNumberEncrypted( $arrValues['destination_account_number_encrypted'] );
		if( isset( $arrValues['short_name'] ) && $boolDirectSet ) $this->set( 'm_strShortName', trim( $arrValues['short_name'] ) ); elseif( isset( $arrValues['short_name'] ) ) $this->setShortName( $arrValues['short_name'] );
		if( isset( $arrValues['long_name'] ) && $boolDirectSet ) $this->set( 'm_strLongName', trim( $arrValues['long_name'] ) ); elseif( isset( $arrValues['long_name'] ) ) $this->setLongName( $arrValues['long_name'] );
		if( isset( $arrValues['name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strNameOnAccount', trim( $arrValues['name_on_account'] ) ); elseif( isset( $arrValues['name_on_account'] ) ) $this->setNameOnAccount( $arrValues['name_on_account'] );
		if( isset( $arrValues['identification_number'] ) && $boolDirectSet ) $this->set( 'm_strIdentificationNumber', trim( $arrValues['identification_number'] ) ); elseif( isset( $arrValues['identification_number'] ) ) $this->setIdentificationNumber( $arrValues['identification_number'] );
		if( isset( $arrValues['return_institution_id'] ) && $boolDirectSet ) $this->set( 'm_strReturnInstitutionId', trim( $arrValues['return_institution_id'] ) ); elseif( isset( $arrValues['return_institution_id'] ) ) $this->setReturnInstitutionId( $arrValues['return_institution_id'] );
		if( isset( $arrValues['return_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strReturnAccountNumberEncrypted', trim( $arrValues['return_account_number_encrypted'] ) ); elseif( isset( $arrValues['return_account_number_encrypted'] ) ) $this->setReturnAccountNumberEncrypted( $arrValues['return_account_number_encrypted'] );
		if( isset( $arrValues['return_reason_code'] ) && $boolDirectSet ) $this->set( 'm_strReturnReasonCode', trim( $arrValues['return_reason_code'] ) ); elseif( isset( $arrValues['return_reason_code'] ) ) $this->setReturnReasonCode( $arrValues['return_reason_code'] );
		if( isset( $arrValues['corrected_institution_id'] ) && $boolDirectSet ) $this->set( 'm_strCorrectedInstitutionId', trim( $arrValues['corrected_institution_id'] ) ); elseif( isset( $arrValues['corrected_institution_id'] ) ) $this->setCorrectedInstitutionId( $arrValues['corrected_institution_id'] );
		if( isset( $arrValues['corrected_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCorrectedAccountNumberEncrypted', trim( $arrValues['corrected_account_number_encrypted'] ) ); elseif( isset( $arrValues['corrected_account_number_encrypted'] ) ) $this->setCorrectedAccountNumberEncrypted( $arrValues['corrected_account_number_encrypted'] );
		if( isset( $arrValues['is_notice_of_change'] ) && $boolDirectSet ) $this->set( 'm_boolIsNoticeOfChange', trim( stripcslashes( $arrValues['is_notice_of_change'] ) ) ); elseif( isset( $arrValues['is_notice_of_change'] ) ) $this->setIsNoticeOfChange( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_notice_of_change'] ) : $arrValues['is_notice_of_change'] );
		if( isset( $arrValues['is_recon_prepped'] ) && $boolDirectSet ) $this->set( 'm_boolIsReconPrepped', trim( stripcslashes( $arrValues['is_recon_prepped'] ) ) ); elseif( isset( $arrValues['is_recon_prepped'] ) ) $this->setIsReconPrepped( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_recon_prepped'] ) : $arrValues['is_recon_prepped'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAftReturnFileId( $intAftReturnFileId ) {
		$this->set( 'm_intAftReturnFileId', CStrings::strToIntDef( $intAftReturnFileId, NULL, false ) );
	}

	public function getAftReturnFileId() {
		return $this->m_intAftReturnFileId;
	}

	public function sqlAftReturnFileId() {
		return ( true == isset( $this->m_intAftReturnFileId ) ) ? ( string ) $this->m_intAftReturnFileId : 'NULL';
	}

	public function setReturnTypeId( $strReturnTypeId ) {
		$this->set( 'm_strReturnTypeId', CStrings::strTrimDef( $strReturnTypeId, NULL, NULL, true ) );
	}

	public function getReturnTypeId() {
		return $this->m_strReturnTypeId;
	}

	public function sqlReturnTypeId() {
		return ( true == isset( $this->m_strReturnTypeId ) ) ? '\'' . addslashes( $this->m_strReturnTypeId ) . '\'' : 'NULL';
	}

	public function setSettlementDistributionId( $intSettlementDistributionId ) {
		$this->set( 'm_intSettlementDistributionId', CStrings::strToIntDef( $intSettlementDistributionId, NULL, false ) );
	}

	public function getSettlementDistributionId() {
		return $this->m_intSettlementDistributionId;
	}

	public function sqlSettlementDistributionId() {
		return ( true == isset( $this->m_intSettlementDistributionId ) ) ? ( string ) $this->m_intSettlementDistributionId : 'NULL';
	}

	public function setEftInstructionId( $intEftInstructionId ) {
		$this->set( 'm_intEftInstructionId', CStrings::strToIntDef( $intEftInstructionId, NULL, false ) );
	}

	public function getEftInstructionId() {
		return $this->m_intEftInstructionId;
	}

	public function sqlEftInstructionId() {
		return ( true == isset( $this->m_intEftInstructionId ) ) ? ( string ) $this->m_intEftInstructionId : 'NULL';
	}

	public function setPaymentBlacklistEntryId( $intPaymentBlacklistEntryId ) {
		$this->set( 'm_intPaymentBlacklistEntryId', CStrings::strToIntDef( $intPaymentBlacklistEntryId, NULL, false ) );
	}

	public function getPaymentBlacklistEntryId() {
		return $this->m_intPaymentBlacklistEntryId;
	}

	public function sqlPaymentBlacklistEntryId() {
		return ( true == isset( $this->m_intPaymentBlacklistEntryId ) ) ? ( string ) $this->m_intPaymentBlacklistEntryId : 'NULL';
	}

	public function setLogicalRecordTypeCode( $strLogicalRecordTypeCode ) {
		$this->set( 'm_strLogicalRecordTypeCode', CStrings::strTrimDef( $strLogicalRecordTypeCode, 5, NULL, true ) );
	}

	public function getLogicalRecordTypeCode() {
		return $this->m_strLogicalRecordTypeCode;
	}

	public function sqlLogicalRecordTypeCode() {
		return ( true == isset( $this->m_strLogicalRecordTypeCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLogicalRecordTypeCode ) : '\'' . addslashes( $this->m_strLogicalRecordTypeCode ) . '\'' ) : 'NULL';
	}

	public function setTransactionType( $strTransactionType ) {
		$this->set( 'm_strTransactionType', CStrings::strTrimDef( $strTransactionType, 6, NULL, true ) );
	}

	public function getTransactionType() {
		return $this->m_strTransactionType;
	}

	public function sqlTransactionType() {
		return ( true == isset( $this->m_strTransactionType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTransactionType ) : '\'' . addslashes( $this->m_strTransactionType ) . '\'' ) : 'NULL';
	}

	public function setAmount( $strAmount ) {
		$this->set( 'm_strAmount', CStrings::strTrimDef( $strAmount, 13, NULL, true ) );
	}

	public function getAmount() {
		return $this->m_strAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_strAmount ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAmount ) : '\'' . addslashes( $this->m_strAmount ) . '\'' ) : 'NULL';
	}

	public function setPayableDate( $strPayableDate ) {
		$this->set( 'm_strPayableDate', CStrings::strTrimDef( $strPayableDate, 12, NULL, true ) );
	}

	public function getPayableDate() {
		return $this->m_strPayableDate;
	}

	public function sqlPayableDate() {
		return ( true == isset( $this->m_strPayableDate ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPayableDate ) : '\'' . addslashes( $this->m_strPayableDate ) . '\'' ) : 'NULL';
	}

	public function setDestinationInstitutionId( $strDestinationInstitutionId ) {
		$this->set( 'm_strDestinationInstitutionId', CStrings::strTrimDef( $strDestinationInstitutionId, 9, NULL, true ) );
	}

	public function getDestinationInstitutionId() {
		return $this->m_strDestinationInstitutionId;
	}

	public function sqlDestinationInstitutionId() {
		return ( true == isset( $this->m_strDestinationInstitutionId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDestinationInstitutionId ) : '\'' . addslashes( $this->m_strDestinationInstitutionId ) . '\'' ) : 'NULL';
	}

	public function setDestinationAccountNumberEncrypted( $strDestinationAccountNumberEncrypted ) {
		$this->set( 'm_strDestinationAccountNumberEncrypted', CStrings::strTrimDef( $strDestinationAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getDestinationAccountNumberEncrypted() {
		return $this->m_strDestinationAccountNumberEncrypted;
	}

	public function sqlDestinationAccountNumberEncrypted() {
		return ( true == isset( $this->m_strDestinationAccountNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDestinationAccountNumberEncrypted ) : '\'' . addslashes( $this->m_strDestinationAccountNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setShortName( $strShortName ) {
		$this->set( 'm_strShortName', CStrings::strTrimDef( $strShortName, 15, NULL, true ) );
	}

	public function getShortName() {
		return $this->m_strShortName;
	}

	public function sqlShortName() {
		return ( true == isset( $this->m_strShortName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strShortName ) : '\'' . addslashes( $this->m_strShortName ) . '\'' ) : 'NULL';
	}

	public function setLongName( $strLongName ) {
		$this->set( 'm_strLongName', CStrings::strTrimDef( $strLongName, 30, NULL, true ) );
	}

	public function getLongName() {
		return $this->m_strLongName;
	}

	public function sqlLongName() {
		return ( true == isset( $this->m_strLongName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLongName ) : '\'' . addslashes( $this->m_strLongName ) . '\'' ) : 'NULL';
	}

	public function setNameOnAccount( $strNameOnAccount ) {
		$this->set( 'm_strNameOnAccount', CStrings::strTrimDef( $strNameOnAccount, 100, NULL, true ) );
	}

	public function getNameOnAccount() {
		return $this->m_strNameOnAccount;
	}

	public function sqlNameOnAccount() {
		return ( true == isset( $this->m_strNameOnAccount ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameOnAccount ) : '\'' . addslashes( $this->m_strNameOnAccount ) . '\'' ) : 'NULL';
	}

	public function setIdentificationNumber( $strIdentificationNumber ) {
		$this->set( 'm_strIdentificationNumber', CStrings::strTrimDef( $strIdentificationNumber, 30, NULL, true ) );
	}

	public function getIdentificationNumber() {
		return $this->m_strIdentificationNumber;
	}

	public function sqlIdentificationNumber() {
		return ( true == isset( $this->m_strIdentificationNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIdentificationNumber ) : '\'' . addslashes( $this->m_strIdentificationNumber ) . '\'' ) : 'NULL';
	}

	public function setReturnInstitutionId( $strReturnInstitutionId ) {
		$this->set( 'm_strReturnInstitutionId', CStrings::strTrimDef( $strReturnInstitutionId, 9, NULL, true ) );
	}

	public function getReturnInstitutionId() {
		return $this->m_strReturnInstitutionId;
	}

	public function sqlReturnInstitutionId() {
		return ( true == isset( $this->m_strReturnInstitutionId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReturnInstitutionId ) : '\'' . addslashes( $this->m_strReturnInstitutionId ) . '\'' ) : 'NULL';
	}

	public function setReturnAccountNumberEncrypted( $strReturnAccountNumberEncrypted ) {
		$this->set( 'm_strReturnAccountNumberEncrypted', CStrings::strTrimDef( $strReturnAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getReturnAccountNumberEncrypted() {
		return $this->m_strReturnAccountNumberEncrypted;
	}

	public function sqlReturnAccountNumberEncrypted() {
		return ( true == isset( $this->m_strReturnAccountNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReturnAccountNumberEncrypted ) : '\'' . addslashes( $this->m_strReturnAccountNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setReturnReasonCode( $strReturnReasonCode ) {
		$this->set( 'm_strReturnReasonCode', CStrings::strTrimDef( $strReturnReasonCode, 10, NULL, true ) );
	}

	public function getReturnReasonCode() {
		return $this->m_strReturnReasonCode;
	}

	public function sqlReturnReasonCode() {
		return ( true == isset( $this->m_strReturnReasonCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReturnReasonCode ) : '\'' . addslashes( $this->m_strReturnReasonCode ) . '\'' ) : 'NULL';
	}

	public function setCorrectedInstitutionId( $strCorrectedInstitutionId ) {
		$this->set( 'm_strCorrectedInstitutionId', CStrings::strTrimDef( $strCorrectedInstitutionId, 9, NULL, true ) );
	}

	public function getCorrectedInstitutionId() {
		return $this->m_strCorrectedInstitutionId;
	}

	public function sqlCorrectedInstitutionId() {
		return ( true == isset( $this->m_strCorrectedInstitutionId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCorrectedInstitutionId ) : '\'' . addslashes( $this->m_strCorrectedInstitutionId ) . '\'' ) : 'NULL';
	}

	public function setCorrectedAccountNumberEncrypted( $strCorrectedAccountNumberEncrypted ) {
		$this->set( 'm_strCorrectedAccountNumberEncrypted', CStrings::strTrimDef( $strCorrectedAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getCorrectedAccountNumberEncrypted() {
		return $this->m_strCorrectedAccountNumberEncrypted;
	}

	public function sqlCorrectedAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCorrectedAccountNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCorrectedAccountNumberEncrypted ) : '\'' . addslashes( $this->m_strCorrectedAccountNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setIsNoticeOfChange( $boolIsNoticeOfChange ) {
		$this->set( 'm_boolIsNoticeOfChange', CStrings::strToBool( $boolIsNoticeOfChange ) );
	}

	public function getIsNoticeOfChange() {
		return $this->m_boolIsNoticeOfChange;
	}

	public function sqlIsNoticeOfChange() {
		return ( true == isset( $this->m_boolIsNoticeOfChange ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNoticeOfChange ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsReconPrepped( $boolIsReconPrepped ) {
		$this->set( 'm_boolIsReconPrepped', CStrings::strToBool( $boolIsReconPrepped ) );
	}

	public function getIsReconPrepped() {
		return $this->m_boolIsReconPrepped;
	}

	public function sqlIsReconPrepped() {
		return ( true == isset( $this->m_boolIsReconPrepped ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReconPrepped ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, aft_return_file_id, return_type_id, settlement_distribution_id, eft_instruction_id, payment_blacklist_entry_id, logical_record_type_code, transaction_type, amount, payable_date, destination_institution_id, destination_account_number_encrypted, short_name, long_name, name_on_account, identification_number, return_institution_id, return_account_number_encrypted, return_reason_code, corrected_institution_id, corrected_account_number_encrypted, is_notice_of_change, is_recon_prepped, processed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlAftReturnFileId() . ', ' .
						$this->sqlReturnTypeId() . ', ' .
						$this->sqlSettlementDistributionId() . ', ' .
						$this->sqlEftInstructionId() . ', ' .
						$this->sqlPaymentBlacklistEntryId() . ', ' .
						$this->sqlLogicalRecordTypeCode() . ', ' .
						$this->sqlTransactionType() . ', ' .
						$this->sqlAmount() . ', ' .
						$this->sqlPayableDate() . ', ' .
						$this->sqlDestinationInstitutionId() . ', ' .
						$this->sqlDestinationAccountNumberEncrypted() . ', ' .
						$this->sqlShortName() . ', ' .
						$this->sqlLongName() . ', ' .
						$this->sqlNameOnAccount() . ', ' .
						$this->sqlIdentificationNumber() . ', ' .
						$this->sqlReturnInstitutionId() . ', ' .
						$this->sqlReturnAccountNumberEncrypted() . ', ' .
						$this->sqlReturnReasonCode() . ', ' .
						$this->sqlCorrectedInstitutionId() . ', ' .
						$this->sqlCorrectedAccountNumberEncrypted() . ', ' .
						$this->sqlIsNoticeOfChange() . ', ' .
						$this->sqlIsReconPrepped() . ', ' .
						$this->sqlProcessedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' aft_return_file_id = ' . $this->sqlAftReturnFileId(). ',' ; } elseif( true == array_key_exists( 'AftReturnFileId', $this->getChangedColumns() ) ) { $strSql .= ' aft_return_file_id = ' . $this->sqlAftReturnFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId(). ',' ; } elseif( true == array_key_exists( 'ReturnTypeId', $this->getChangedColumns() ) ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId(). ',' ; } elseif( true == array_key_exists( 'SettlementDistributionId', $this->getChangedColumns() ) ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eft_instruction_id = ' . $this->sqlEftInstructionId(). ',' ; } elseif( true == array_key_exists( 'EftInstructionId', $this->getChangedColumns() ) ) { $strSql .= ' eft_instruction_id = ' . $this->sqlEftInstructionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_blacklist_entry_id = ' . $this->sqlPaymentBlacklistEntryId(). ',' ; } elseif( true == array_key_exists( 'PaymentBlacklistEntryId', $this->getChangedColumns() ) ) { $strSql .= ' payment_blacklist_entry_id = ' . $this->sqlPaymentBlacklistEntryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' logical_record_type_code = ' . $this->sqlLogicalRecordTypeCode(). ',' ; } elseif( true == array_key_exists( 'LogicalRecordTypeCode', $this->getChangedColumns() ) ) { $strSql .= ' logical_record_type_code = ' . $this->sqlLogicalRecordTypeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_type = ' . $this->sqlTransactionType(). ',' ; } elseif( true == array_key_exists( 'TransactionType', $this->getChangedColumns() ) ) { $strSql .= ' transaction_type = ' . $this->sqlTransactionType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount(). ',' ; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payable_date = ' . $this->sqlPayableDate(). ',' ; } elseif( true == array_key_exists( 'PayableDate', $this->getChangedColumns() ) ) { $strSql .= ' payable_date = ' . $this->sqlPayableDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_institution_id = ' . $this->sqlDestinationInstitutionId(). ',' ; } elseif( true == array_key_exists( 'DestinationInstitutionId', $this->getChangedColumns() ) ) { $strSql .= ' destination_institution_id = ' . $this->sqlDestinationInstitutionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_account_number_encrypted = ' . $this->sqlDestinationAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'DestinationAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' destination_account_number_encrypted = ' . $this->sqlDestinationAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' short_name = ' . $this->sqlShortName(). ',' ; } elseif( true == array_key_exists( 'ShortName', $this->getChangedColumns() ) ) { $strSql .= ' short_name = ' . $this->sqlShortName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' long_name = ' . $this->sqlLongName(). ',' ; } elseif( true == array_key_exists( 'LongName', $this->getChangedColumns() ) ) { $strSql .= ' long_name = ' . $this->sqlLongName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_on_account = ' . $this->sqlNameOnAccount(). ',' ; } elseif( true == array_key_exists( 'NameOnAccount', $this->getChangedColumns() ) ) { $strSql .= ' name_on_account = ' . $this->sqlNameOnAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' identification_number = ' . $this->sqlIdentificationNumber(). ',' ; } elseif( true == array_key_exists( 'IdentificationNumber', $this->getChangedColumns() ) ) { $strSql .= ' identification_number = ' . $this->sqlIdentificationNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_institution_id = ' . $this->sqlReturnInstitutionId(). ',' ; } elseif( true == array_key_exists( 'ReturnInstitutionId', $this->getChangedColumns() ) ) { $strSql .= ' return_institution_id = ' . $this->sqlReturnInstitutionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_account_number_encrypted = ' . $this->sqlReturnAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'ReturnAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' return_account_number_encrypted = ' . $this->sqlReturnAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_reason_code = ' . $this->sqlReturnReasonCode(). ',' ; } elseif( true == array_key_exists( 'ReturnReasonCode', $this->getChangedColumns() ) ) { $strSql .= ' return_reason_code = ' . $this->sqlReturnReasonCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' corrected_institution_id = ' . $this->sqlCorrectedInstitutionId(). ',' ; } elseif( true == array_key_exists( 'CorrectedInstitutionId', $this->getChangedColumns() ) ) { $strSql .= ' corrected_institution_id = ' . $this->sqlCorrectedInstitutionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' corrected_account_number_encrypted = ' . $this->sqlCorrectedAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'CorrectedAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' corrected_account_number_encrypted = ' . $this->sqlCorrectedAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_notice_of_change = ' . $this->sqlIsNoticeOfChange(). ',' ; } elseif( true == array_key_exists( 'IsNoticeOfChange', $this->getChangedColumns() ) ) { $strSql .= ' is_notice_of_change = ' . $this->sqlIsNoticeOfChange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped(). ',' ; } elseif( true == array_key_exists( 'IsReconPrepped', $this->getChangedColumns() ) ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn(). ',' ; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'aft_return_file_id' => $this->getAftReturnFileId(),
			'return_type_id' => $this->getReturnTypeId(),
			'settlement_distribution_id' => $this->getSettlementDistributionId(),
			'eft_instruction_id' => $this->getEftInstructionId(),
			'payment_blacklist_entry_id' => $this->getPaymentBlacklistEntryId(),
			'logical_record_type_code' => $this->getLogicalRecordTypeCode(),
			'transaction_type' => $this->getTransactionType(),
			'amount' => $this->getAmount(),
			'payable_date' => $this->getPayableDate(),
			'destination_institution_id' => $this->getDestinationInstitutionId(),
			'destination_account_number_encrypted' => $this->getDestinationAccountNumberEncrypted(),
			'short_name' => $this->getShortName(),
			'long_name' => $this->getLongName(),
			'name_on_account' => $this->getNameOnAccount(),
			'identification_number' => $this->getIdentificationNumber(),
			'return_institution_id' => $this->getReturnInstitutionId(),
			'return_account_number_encrypted' => $this->getReturnAccountNumberEncrypted(),
			'return_reason_code' => $this->getReturnReasonCode(),
			'corrected_institution_id' => $this->getCorrectedInstitutionId(),
			'corrected_account_number_encrypted' => $this->getCorrectedAccountNumberEncrypted(),
			'is_notice_of_change' => $this->getIsNoticeOfChange(),
			'is_recon_prepped' => $this->getIsReconPrepped(),
			'processed_on' => $this->getProcessedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>