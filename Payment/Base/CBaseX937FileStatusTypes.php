<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937FileStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseX937FileStatusTypes extends CEosPluralBase {

	/**
	 * @return CX937FileStatusType[]
	 */
	public static function fetchX937FileStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CX937FileStatusType::class, $objDatabase );
	}

	/**
	 * @return CX937FileStatusType
	 */
	public static function fetchX937FileStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CX937FileStatusType::class, $objDatabase );
	}

	public static function fetchX937FileStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'x937_file_status_types', $objDatabase );
	}

	public static function fetchX937FileStatusTypeById( $intId, $objDatabase ) {
		return self::fetchX937FileStatusType( sprintf( 'SELECT * FROM x937_file_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>