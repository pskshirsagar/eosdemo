<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentTransactions
 * Do not add any new functions to this class.
 */

class CBaseArPaymentTransactions extends CEosPluralBase {

	/**
	 * @return CArPaymentTransaction[]
	 */
	public static function fetchArPaymentTransactions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CArPaymentTransaction::class, $objDatabase );
	}

	/**
	 * @return CArPaymentTransaction
	 */
	public static function fetchArPaymentTransaction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CArPaymentTransaction::class, $objDatabase );
	}

	public static function fetchArPaymentTransactionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ar_payment_transactions', $objDatabase );
	}

	public static function fetchArPaymentTransactionById( $intId, $objDatabase ) {
		return self::fetchArPaymentTransaction( sprintf( 'SELECT * FROM ar_payment_transactions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchArPaymentTransactionsByCid( $intCid, $objDatabase ) {
		return self::fetchArPaymentTransactions( sprintf( 'SELECT * FROM ar_payment_transactions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransactionsByCompanyMerchantAccountId( $intCompanyMerchantAccountId, $objDatabase ) {
		return self::fetchArPaymentTransactions( sprintf( 'SELECT * FROM ar_payment_transactions WHERE company_merchant_account_id = %d', ( int ) $intCompanyMerchantAccountId ), $objDatabase );
	}

	public static function fetchArPaymentTransactionsByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchArPaymentTransactions( sprintf( 'SELECT * FROM ar_payment_transactions WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

	public static function fetchArPaymentTransactionsByPaymentTransactionTypeId( $intPaymentTransactionTypeId, $objDatabase ) {
		return self::fetchArPaymentTransactions( sprintf( 'SELECT * FROM ar_payment_transactions WHERE payment_transaction_type_id = %d', ( int ) $intPaymentTransactionTypeId ), $objDatabase );
	}

}
?>