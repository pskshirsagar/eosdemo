<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaDetailRecordContents
 * Do not add any new functions to this class.
 */

class CBaseNachaDetailRecordContents extends CEosPluralBase {

	/**
	 * @return CNachaDetailRecordContent[]
	 */
	public static function fetchNachaDetailRecordContents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CNachaDetailRecordContent::class, $objDatabase );
	}

	/**
	 * @return CNachaDetailRecordContent
	 */
	public static function fetchNachaDetailRecordContent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CNachaDetailRecordContent::class, $objDatabase );
	}

	public static function fetchNachaDetailRecordContentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'nacha_detail_record_contents', $objDatabase );
	}

	public static function fetchNachaDetailRecordContentById( $intId, $objDatabase ) {
		return self::fetchNachaDetailRecordContent( sprintf( 'SELECT * FROM nacha_detail_record_contents WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchNachaDetailRecordContentsByNachaFileId( $intNachaFileId, $objDatabase ) {
		return self::fetchNachaDetailRecordContents( sprintf( 'SELECT * FROM nacha_detail_record_contents WHERE nacha_file_id = %d', ( int ) $intNachaFileId ), $objDatabase );
	}

	public static function fetchNachaDetailRecordContentsByNachaEntryDetailRecordId( $intNachaEntryDetailRecordId, $objDatabase ) {
		return self::fetchNachaDetailRecordContents( sprintf( 'SELECT * FROM nacha_detail_record_contents WHERE nacha_entry_detail_record_id = %d', ( int ) $intNachaEntryDetailRecordId ), $objDatabase );
	}

	public static function fetchNachaDetailRecordContentsByCid( $intCid, $objDatabase ) {
		return self::fetchNachaDetailRecordContents( sprintf( 'SELECT * FROM nacha_detail_record_contents WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNachaDetailRecordContentsByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchNachaDetailRecordContents( sprintf( 'SELECT * FROM nacha_detail_record_contents WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

	public static function fetchNachaDetailRecordContentsBySettlementDistributionId( $intSettlementDistributionId, $objDatabase ) {
		return self::fetchNachaDetailRecordContents( sprintf( 'SELECT * FROM nacha_detail_record_contents WHERE settlement_distribution_id = %d', ( int ) $intSettlementDistributionId ), $objDatabase );
	}

	public static function fetchNachaDetailRecordContentsByCompanyPaymentId( $intCompanyPaymentId, $objDatabase ) {
		return self::fetchNachaDetailRecordContents( sprintf( 'SELECT * FROM nacha_detail_record_contents WHERE company_payment_id = %d', ( int ) $intCompanyPaymentId ), $objDatabase );
	}

}
?>