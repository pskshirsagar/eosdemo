<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937CashLetters
 * Do not add any new functions to this class.
 */

class CBaseX937CashLetters extends CEosPluralBase {

	/**
	 * @return CX937CashLetter[]
	 */
	public static function fetchX937CashLetters( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CX937CashLetter::class, $objDatabase );
	}

	/**
	 * @return CX937CashLetter
	 */
	public static function fetchX937CashLetter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CX937CashLetter::class, $objDatabase );
	}

	public static function fetchX937CashLetterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'x937_cash_letters', $objDatabase );
	}

	public static function fetchX937CashLetterById( $intId, $objDatabase ) {
		return self::fetchX937CashLetter( sprintf( 'SELECT * FROM x937_cash_letters WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchX937CashLettersByCid( $intCid, $objDatabase ) {
		return self::fetchX937CashLetters( sprintf( 'SELECT * FROM x937_cash_letters WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchX937CashLettersByX937FileId( $intX937FileId, $objDatabase ) {
		return self::fetchX937CashLetters( sprintf( 'SELECT * FROM x937_cash_letters WHERE x937_file_id = %d', ( int ) $intX937FileId ), $objDatabase );
	}

	public static function fetchX937CashLettersByHeaderId( $strHeaderId, $objDatabase ) {
		return self::fetchX937CashLetters( sprintf( 'SELECT * FROM x937_cash_letters WHERE header_id = \'%s\'', $strHeaderId ), $objDatabase );
	}

}
?>