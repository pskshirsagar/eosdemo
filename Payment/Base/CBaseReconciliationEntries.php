<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CReconciliationEntries
 * Do not add any new functions to this class.
 */

class CBaseReconciliationEntries extends CEosPluralBase {

	/**
	 * @return CReconciliationEntry[]
	 */
	public static function fetchReconciliationEntries( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReconciliationEntry::class, $objDatabase );
	}

	/**
	 * @return CReconciliationEntry
	 */
	public static function fetchReconciliationEntry( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReconciliationEntry::class, $objDatabase );
	}

	public static function fetchReconciliationEntryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reconciliation_entries', $objDatabase );
	}

	public static function fetchReconciliationEntryById( $intId, $objDatabase ) {
		return self::fetchReconciliationEntry( sprintf( 'SELECT * FROM reconciliation_entries WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesByProcessingBankAccountId( $intProcessingBankAccountId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE processing_bank_account_id = %d', ( int ) $intProcessingBankAccountId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesByReconciliationId( $intReconciliationId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE reconciliation_id = %d', ( int ) $intReconciliationId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesByReconciliationEntryTypeId( $intReconciliationEntryTypeId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE reconciliation_entry_type_id = %d', ( int ) $intReconciliationEntryTypeId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesByEftInstructionId( $intEftInstructionId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE eft_instruction_id = %d', ( int ) $intEftInstructionId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesByNachaEntryDetailRecordId( $intNachaEntryDetailRecordId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE nacha_entry_detail_record_id = %d', ( int ) $intNachaEntryDetailRecordId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesByNachaReturnAddendaDetailRecordId( $intNachaReturnAddendaDetailRecordId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE nacha_return_addenda_detail_record_id = %d', ( int ) $intNachaReturnAddendaDetailRecordId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesByChargeBackId( $intChargeBackId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE charge_back_id = %d', ( int ) $intChargeBackId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesByEftBatchId( $intEftBatchId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE eft_batch_id = %d', ( int ) $intEftBatchId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesByX937FileId( $intX937FileId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE x937_file_id = %d', ( int ) $intX937FileId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesByX937ReturnFileId( $intX937ReturnFileId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE x937_return_file_id = %d', ( int ) $intX937ReturnFileId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesByNachaReturnFileId( $intNachaReturnFileId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE nacha_return_file_id = %d', ( int ) $intNachaReturnFileId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesBySettlementDistributionId( $intSettlementDistributionId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE settlement_distribution_id = %d', ( int ) $intSettlementDistributionId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesByWesternUnionOutputFileId( $intWesternUnionOutputFileId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE western_union_output_file_id = %d', ( int ) $intWesternUnionOutputFileId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesByCompanyPaymentId( $intCompanyPaymentId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE company_payment_id = %d', ( int ) $intCompanyPaymentId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

	public static function fetchReconciliationEntriesByBai2FileId( $intBai2FileId, $objDatabase ) {
		return self::fetchReconciliationEntries( sprintf( 'SELECT * FROM reconciliation_entries WHERE bai2_file_id = %d', ( int ) $intBai2FileId ), $objDatabase );
	}

}
?>