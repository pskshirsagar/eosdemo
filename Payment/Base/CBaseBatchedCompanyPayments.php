<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CBatchedCompanyPayments
 * Do not add any new functions to this class.
 */

class CBaseBatchedCompanyPayments extends CEosPluralBase {

	/**
	 * @return CBatchedCompanyPayment[]
	 */
	public static function fetchBatchedCompanyPayments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBatchedCompanyPayment::class, $objDatabase );
	}

	/**
	 * @return CBatchedCompanyPayment
	 */
	public static function fetchBatchedCompanyPayment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBatchedCompanyPayment::class, $objDatabase );
	}

	public static function fetchBatchedCompanyPaymentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'batched_company_payments', $objDatabase );
	}

	public static function fetchBatchedCompanyPaymentById( $intId, $objDatabase ) {
		return self::fetchBatchedCompanyPayment( sprintf( 'SELECT * FROM batched_company_payments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchBatchedCompanyPaymentsByCid( $intCid, $objDatabase ) {
		return self::fetchBatchedCompanyPayments( sprintf( 'SELECT * FROM batched_company_payments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBatchedCompanyPaymentsByEftBatchId( $intEftBatchId, $objDatabase ) {
		return self::fetchBatchedCompanyPayments( sprintf( 'SELECT * FROM batched_company_payments WHERE eft_batch_id = %d', ( int ) $intEftBatchId ), $objDatabase );
	}

	public static function fetchBatchedCompanyPaymentsByCompanyPaymentId( $intCompanyPaymentId, $objDatabase ) {
		return self::fetchBatchedCompanyPayments( sprintf( 'SELECT * FROM batched_company_payments WHERE company_payment_id = %d', ( int ) $intCompanyPaymentId ), $objDatabase );
	}

}
?>