<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937ReturnImages
 * Do not add any new functions to this class.
 */

class CBaseX937ReturnImages extends CEosPluralBase {

	/**
	 * @return CX937ReturnImage[]
	 */
	public static function fetchX937ReturnImages( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CX937ReturnImage::class, $objDatabase );
	}

	/**
	 * @return CX937ReturnImage
	 */
	public static function fetchX937ReturnImage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CX937ReturnImage::class, $objDatabase );
	}

	public static function fetchX937ReturnImageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'x937_return_images', $objDatabase );
	}

	public static function fetchX937ReturnImageById( $intId, $objDatabase ) {
		return self::fetchX937ReturnImage( sprintf( 'SELECT * FROM x937_return_images WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchX937ReturnImagesByX937ReturnFileId( $intX937ReturnFileId, $objDatabase ) {
		return self::fetchX937ReturnImages( sprintf( 'SELECT * FROM x937_return_images WHERE x937_return_file_id = %d', ( int ) $intX937ReturnFileId ), $objDatabase );
	}

	public static function fetchX937ReturnImagesByX937ReturnItemId( $intX937ReturnItemId, $objDatabase ) {
		return self::fetchX937ReturnImages( sprintf( 'SELECT * FROM x937_return_images WHERE x937_return_item_id = %d', ( int ) $intX937ReturnItemId ), $objDatabase );
	}

	public static function fetchX937ReturnImagesByPaymentImageTypeId( $intPaymentImageTypeId, $objDatabase ) {
		return self::fetchX937ReturnImages( sprintf( 'SELECT * FROM x937_return_images WHERE payment_image_type_id = %d', ( int ) $intPaymentImageTypeId ), $objDatabase );
	}

}
?>