<?php

class CBaseCreditCardBinNumber extends CEosSingularBase {

	const TABLE_NAME = 'public.credit_card_bin_numbers';

	protected $m_intId;
	protected $m_intSequenceNumber;
	protected $m_intBinLength;
	protected $m_intPanLength;
	protected $m_strBinNumber;
	protected $m_strTableId;
	protected $m_intIsDebitCard;
	protected $m_intIsCheckCard;
	protected $m_intIsCreditCard;
	protected $m_intIsGiftCard;
	protected $m_intIsCommercialCard;
	protected $m_intIsFleetCard;
	protected $m_intIsPrepaidCard;
	protected $m_intIsHsaFsaAccount;
	protected $m_intIsPinlessBillPay;
	protected $m_intIsEbt;
	protected $m_intIsWicBin;
	protected $m_intIsInternationalBin;
	protected $m_intIsDurbinBinRegulation;
	protected $m_intIsPinlessPos;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsDebitCard = '0';
		$this->m_intIsCheckCard = '0';
		$this->m_intIsCreditCard = '0';
		$this->m_intIsGiftCard = '0';
		$this->m_intIsCommercialCard = '0';
		$this->m_intIsFleetCard = '0';
		$this->m_intIsPrepaidCard = '0';
		$this->m_intIsHsaFsaAccount = '0';
		$this->m_intIsPinlessBillPay = '0';
		$this->m_intIsEbt = '0';
		$this->m_intIsWicBin = '0';
		$this->m_intIsInternationalBin = '0';
		$this->m_intIsDurbinBinRegulation = '0';
		$this->m_intIsPinlessPos = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['sequence_number'] ) && $boolDirectSet ) $this->set( 'm_intSequenceNumber', trim( $arrValues['sequence_number'] ) ); elseif( isset( $arrValues['sequence_number'] ) ) $this->setSequenceNumber( $arrValues['sequence_number'] );
		if( isset( $arrValues['bin_length'] ) && $boolDirectSet ) $this->set( 'm_intBinLength', trim( $arrValues['bin_length'] ) ); elseif( isset( $arrValues['bin_length'] ) ) $this->setBinLength( $arrValues['bin_length'] );
		if( isset( $arrValues['pan_length'] ) && $boolDirectSet ) $this->set( 'm_intPanLength', trim( $arrValues['pan_length'] ) ); elseif( isset( $arrValues['pan_length'] ) ) $this->setPanLength( $arrValues['pan_length'] );
		if( isset( $arrValues['bin_number'] ) && $boolDirectSet ) $this->set( 'm_strBinNumber', trim( stripcslashes( $arrValues['bin_number'] ) ) ); elseif( isset( $arrValues['bin_number'] ) ) $this->setBinNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bin_number'] ) : $arrValues['bin_number'] );
		if( isset( $arrValues['table_id'] ) && $boolDirectSet ) $this->set( 'm_strTableId', trim( stripcslashes( $arrValues['table_id'] ) ) ); elseif( isset( $arrValues['table_id'] ) ) $this->setTableId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['table_id'] ) : $arrValues['table_id'] );
		if( isset( $arrValues['is_debit_card'] ) && $boolDirectSet ) $this->set( 'm_intIsDebitCard', trim( $arrValues['is_debit_card'] ) ); elseif( isset( $arrValues['is_debit_card'] ) ) $this->setIsDebitCard( $arrValues['is_debit_card'] );
		if( isset( $arrValues['is_check_card'] ) && $boolDirectSet ) $this->set( 'm_intIsCheckCard', trim( $arrValues['is_check_card'] ) ); elseif( isset( $arrValues['is_check_card'] ) ) $this->setIsCheckCard( $arrValues['is_check_card'] );
		if( isset( $arrValues['is_credit_card'] ) && $boolDirectSet ) $this->set( 'm_intIsCreditCard', trim( $arrValues['is_credit_card'] ) ); elseif( isset( $arrValues['is_credit_card'] ) ) $this->setIsCreditCard( $arrValues['is_credit_card'] );
		if( isset( $arrValues['is_gift_card'] ) && $boolDirectSet ) $this->set( 'm_intIsGiftCard', trim( $arrValues['is_gift_card'] ) ); elseif( isset( $arrValues['is_gift_card'] ) ) $this->setIsGiftCard( $arrValues['is_gift_card'] );
		if( isset( $arrValues['is_commercial_card'] ) && $boolDirectSet ) $this->set( 'm_intIsCommercialCard', trim( $arrValues['is_commercial_card'] ) ); elseif( isset( $arrValues['is_commercial_card'] ) ) $this->setIsCommercialCard( $arrValues['is_commercial_card'] );
		if( isset( $arrValues['is_fleet_card'] ) && $boolDirectSet ) $this->set( 'm_intIsFleetCard', trim( $arrValues['is_fleet_card'] ) ); elseif( isset( $arrValues['is_fleet_card'] ) ) $this->setIsFleetCard( $arrValues['is_fleet_card'] );
		if( isset( $arrValues['is_prepaid_card'] ) && $boolDirectSet ) $this->set( 'm_intIsPrepaidCard', trim( $arrValues['is_prepaid_card'] ) ); elseif( isset( $arrValues['is_prepaid_card'] ) ) $this->setIsPrepaidCard( $arrValues['is_prepaid_card'] );
		if( isset( $arrValues['is_hsa_fsa_account'] ) && $boolDirectSet ) $this->set( 'm_intIsHsaFsaAccount', trim( $arrValues['is_hsa_fsa_account'] ) ); elseif( isset( $arrValues['is_hsa_fsa_account'] ) ) $this->setIsHsaFsaAccount( $arrValues['is_hsa_fsa_account'] );
		if( isset( $arrValues['is_pinless_bill_pay'] ) && $boolDirectSet ) $this->set( 'm_intIsPinlessBillPay', trim( $arrValues['is_pinless_bill_pay'] ) ); elseif( isset( $arrValues['is_pinless_bill_pay'] ) ) $this->setIsPinlessBillPay( $arrValues['is_pinless_bill_pay'] );
		if( isset( $arrValues['is_ebt'] ) && $boolDirectSet ) $this->set( 'm_intIsEbt', trim( $arrValues['is_ebt'] ) ); elseif( isset( $arrValues['is_ebt'] ) ) $this->setIsEbt( $arrValues['is_ebt'] );
		if( isset( $arrValues['is_wic_bin'] ) && $boolDirectSet ) $this->set( 'm_intIsWicBin', trim( $arrValues['is_wic_bin'] ) ); elseif( isset( $arrValues['is_wic_bin'] ) ) $this->setIsWicBin( $arrValues['is_wic_bin'] );
		if( isset( $arrValues['is_international_bin'] ) && $boolDirectSet ) $this->set( 'm_intIsInternationalBin', trim( $arrValues['is_international_bin'] ) ); elseif( isset( $arrValues['is_international_bin'] ) ) $this->setIsInternationalBin( $arrValues['is_international_bin'] );
		if( isset( $arrValues['is_durbin_bin_regulation'] ) && $boolDirectSet ) $this->set( 'm_intIsDurbinBinRegulation', trim( $arrValues['is_durbin_bin_regulation'] ) ); elseif( isset( $arrValues['is_durbin_bin_regulation'] ) ) $this->setIsDurbinBinRegulation( $arrValues['is_durbin_bin_regulation'] );
		if( isset( $arrValues['is_pinless_pos'] ) && $boolDirectSet ) $this->set( 'm_intIsPinlessPos', trim( $arrValues['is_pinless_pos'] ) ); elseif( isset( $arrValues['is_pinless_pos'] ) ) $this->setIsPinlessPos( $arrValues['is_pinless_pos'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSequenceNumber( $intSequenceNumber ) {
		$this->set( 'm_intSequenceNumber', CStrings::strToIntDef( $intSequenceNumber, NULL, false ) );
	}

	public function getSequenceNumber() {
		return $this->m_intSequenceNumber;
	}

	public function sqlSequenceNumber() {
		return ( true == isset( $this->m_intSequenceNumber ) ) ? ( string ) $this->m_intSequenceNumber : 'NULL';
	}

	public function setBinLength( $intBinLength ) {
		$this->set( 'm_intBinLength', CStrings::strToIntDef( $intBinLength, NULL, false ) );
	}

	public function getBinLength() {
		return $this->m_intBinLength;
	}

	public function sqlBinLength() {
		return ( true == isset( $this->m_intBinLength ) ) ? ( string ) $this->m_intBinLength : 'NULL';
	}

	public function setPanLength( $intPanLength ) {
		$this->set( 'm_intPanLength', CStrings::strToIntDef( $intPanLength, NULL, false ) );
	}

	public function getPanLength() {
		return $this->m_intPanLength;
	}

	public function sqlPanLength() {
		return ( true == isset( $this->m_intPanLength ) ) ? ( string ) $this->m_intPanLength : 'NULL';
	}

	public function setBinNumber( $strBinNumber ) {
		$this->set( 'm_strBinNumber', CStrings::strTrimDef( $strBinNumber, 32, NULL, true ) );
	}

	public function getBinNumber() {
		return $this->m_strBinNumber;
	}

	public function sqlBinNumber() {
		return ( true == isset( $this->m_strBinNumber ) ) ? '\'' . addslashes( $this->m_strBinNumber ) . '\'' : 'NULL';
	}

	public function setTableId( $strTableId ) {
		$this->set( 'm_strTableId', CStrings::strTrimDef( $strTableId, 4, NULL, true ) );
	}

	public function getTableId() {
		return $this->m_strTableId;
	}

	public function sqlTableId() {
		return ( true == isset( $this->m_strTableId ) ) ? '\'' . addslashes( $this->m_strTableId ) . '\'' : 'NULL';
	}

	public function setIsDebitCard( $intIsDebitCard ) {
		$this->set( 'm_intIsDebitCard', CStrings::strToIntDef( $intIsDebitCard, NULL, false ) );
	}

	public function getIsDebitCard() {
		return $this->m_intIsDebitCard;
	}

	public function sqlIsDebitCard() {
		return ( true == isset( $this->m_intIsDebitCard ) ) ? ( string ) $this->m_intIsDebitCard : '0';
	}

	public function setIsCheckCard( $intIsCheckCard ) {
		$this->set( 'm_intIsCheckCard', CStrings::strToIntDef( $intIsCheckCard, NULL, false ) );
	}

	public function getIsCheckCard() {
		return $this->m_intIsCheckCard;
	}

	public function sqlIsCheckCard() {
		return ( true == isset( $this->m_intIsCheckCard ) ) ? ( string ) $this->m_intIsCheckCard : '0';
	}

	public function setIsCreditCard( $intIsCreditCard ) {
		$this->set( 'm_intIsCreditCard', CStrings::strToIntDef( $intIsCreditCard, NULL, false ) );
	}

	public function getIsCreditCard() {
		return $this->m_intIsCreditCard;
	}

	public function sqlIsCreditCard() {
		return ( true == isset( $this->m_intIsCreditCard ) ) ? ( string ) $this->m_intIsCreditCard : '0';
	}

	public function setIsGiftCard( $intIsGiftCard ) {
		$this->set( 'm_intIsGiftCard', CStrings::strToIntDef( $intIsGiftCard, NULL, false ) );
	}

	public function getIsGiftCard() {
		return $this->m_intIsGiftCard;
	}

	public function sqlIsGiftCard() {
		return ( true == isset( $this->m_intIsGiftCard ) ) ? ( string ) $this->m_intIsGiftCard : '0';
	}

	public function setIsCommercialCard( $intIsCommercialCard ) {
		$this->set( 'm_intIsCommercialCard', CStrings::strToIntDef( $intIsCommercialCard, NULL, false ) );
	}

	public function getIsCommercialCard() {
		return $this->m_intIsCommercialCard;
	}

	public function sqlIsCommercialCard() {
		return ( true == isset( $this->m_intIsCommercialCard ) ) ? ( string ) $this->m_intIsCommercialCard : '0';
	}

	public function setIsFleetCard( $intIsFleetCard ) {
		$this->set( 'm_intIsFleetCard', CStrings::strToIntDef( $intIsFleetCard, NULL, false ) );
	}

	public function getIsFleetCard() {
		return $this->m_intIsFleetCard;
	}

	public function sqlIsFleetCard() {
		return ( true == isset( $this->m_intIsFleetCard ) ) ? ( string ) $this->m_intIsFleetCard : '0';
	}

	public function setIsPrepaidCard( $intIsPrepaidCard ) {
		$this->set( 'm_intIsPrepaidCard', CStrings::strToIntDef( $intIsPrepaidCard, NULL, false ) );
	}

	public function getIsPrepaidCard() {
		return $this->m_intIsPrepaidCard;
	}

	public function sqlIsPrepaidCard() {
		return ( true == isset( $this->m_intIsPrepaidCard ) ) ? ( string ) $this->m_intIsPrepaidCard : '0';
	}

	public function setIsHsaFsaAccount( $intIsHsaFsaAccount ) {
		$this->set( 'm_intIsHsaFsaAccount', CStrings::strToIntDef( $intIsHsaFsaAccount, NULL, false ) );
	}

	public function getIsHsaFsaAccount() {
		return $this->m_intIsHsaFsaAccount;
	}

	public function sqlIsHsaFsaAccount() {
		return ( true == isset( $this->m_intIsHsaFsaAccount ) ) ? ( string ) $this->m_intIsHsaFsaAccount : '0';
	}

	public function setIsPinlessBillPay( $intIsPinlessBillPay ) {
		$this->set( 'm_intIsPinlessBillPay', CStrings::strToIntDef( $intIsPinlessBillPay, NULL, false ) );
	}

	public function getIsPinlessBillPay() {
		return $this->m_intIsPinlessBillPay;
	}

	public function sqlIsPinlessBillPay() {
		return ( true == isset( $this->m_intIsPinlessBillPay ) ) ? ( string ) $this->m_intIsPinlessBillPay : '0';
	}

	public function setIsEbt( $intIsEbt ) {
		$this->set( 'm_intIsEbt', CStrings::strToIntDef( $intIsEbt, NULL, false ) );
	}

	public function getIsEbt() {
		return $this->m_intIsEbt;
	}

	public function sqlIsEbt() {
		return ( true == isset( $this->m_intIsEbt ) ) ? ( string ) $this->m_intIsEbt : '0';
	}

	public function setIsWicBin( $intIsWicBin ) {
		$this->set( 'm_intIsWicBin', CStrings::strToIntDef( $intIsWicBin, NULL, false ) );
	}

	public function getIsWicBin() {
		return $this->m_intIsWicBin;
	}

	public function sqlIsWicBin() {
		return ( true == isset( $this->m_intIsWicBin ) ) ? ( string ) $this->m_intIsWicBin : '0';
	}

	public function setIsInternationalBin( $intIsInternationalBin ) {
		$this->set( 'm_intIsInternationalBin', CStrings::strToIntDef( $intIsInternationalBin, NULL, false ) );
	}

	public function getIsInternationalBin() {
		return $this->m_intIsInternationalBin;
	}

	public function sqlIsInternationalBin() {
		return ( true == isset( $this->m_intIsInternationalBin ) ) ? ( string ) $this->m_intIsInternationalBin : '0';
	}

	public function setIsDurbinBinRegulation( $intIsDurbinBinRegulation ) {
		$this->set( 'm_intIsDurbinBinRegulation', CStrings::strToIntDef( $intIsDurbinBinRegulation, NULL, false ) );
	}

	public function getIsDurbinBinRegulation() {
		return $this->m_intIsDurbinBinRegulation;
	}

	public function sqlIsDurbinBinRegulation() {
		return ( true == isset( $this->m_intIsDurbinBinRegulation ) ) ? ( string ) $this->m_intIsDurbinBinRegulation : '0';
	}

	public function setIsPinlessPos( $intIsPinlessPos ) {
		$this->set( 'm_intIsPinlessPos', CStrings::strToIntDef( $intIsPinlessPos, NULL, false ) );
	}

	public function getIsPinlessPos() {
		return $this->m_intIsPinlessPos;
	}

	public function sqlIsPinlessPos() {
		return ( true == isset( $this->m_intIsPinlessPos ) ) ? ( string ) $this->m_intIsPinlessPos : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, sequence_number, bin_length, pan_length, bin_number, table_id, is_debit_card, is_check_card, is_credit_card, is_gift_card, is_commercial_card, is_fleet_card, is_prepaid_card, is_hsa_fsa_account, is_pinless_bill_pay, is_ebt, is_wic_bin, is_international_bin, is_durbin_bin_regulation, is_pinless_pos, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSequenceNumber() . ', ' .
 						$this->sqlBinLength() . ', ' .
 						$this->sqlPanLength() . ', ' .
 						$this->sqlBinNumber() . ', ' .
 						$this->sqlTableId() . ', ' .
 						$this->sqlIsDebitCard() . ', ' .
 						$this->sqlIsCheckCard() . ', ' .
 						$this->sqlIsCreditCard() . ', ' .
 						$this->sqlIsGiftCard() . ', ' .
 						$this->sqlIsCommercialCard() . ', ' .
 						$this->sqlIsFleetCard() . ', ' .
 						$this->sqlIsPrepaidCard() . ', ' .
 						$this->sqlIsHsaFsaAccount() . ', ' .
 						$this->sqlIsPinlessBillPay() . ', ' .
 						$this->sqlIsEbt() . ', ' .
 						$this->sqlIsWicBin() . ', ' .
 						$this->sqlIsInternationalBin() . ', ' .
 						$this->sqlIsDurbinBinRegulation() . ', ' .
 						$this->sqlIsPinlessPos() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sequence_number = ' . $this->sqlSequenceNumber() . ','; } elseif( true == array_key_exists( 'SequenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' sequence_number = ' . $this->sqlSequenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bin_length = ' . $this->sqlBinLength() . ','; } elseif( true == array_key_exists( 'BinLength', $this->getChangedColumns() ) ) { $strSql .= ' bin_length = ' . $this->sqlBinLength() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pan_length = ' . $this->sqlPanLength() . ','; } elseif( true == array_key_exists( 'PanLength', $this->getChangedColumns() ) ) { $strSql .= ' pan_length = ' . $this->sqlPanLength() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bin_number = ' . $this->sqlBinNumber() . ','; } elseif( true == array_key_exists( 'BinNumber', $this->getChangedColumns() ) ) { $strSql .= ' bin_number = ' . $this->sqlBinNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' table_id = ' . $this->sqlTableId() . ','; } elseif( true == array_key_exists( 'TableId', $this->getChangedColumns() ) ) { $strSql .= ' table_id = ' . $this->sqlTableId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_debit_card = ' . $this->sqlIsDebitCard() . ','; } elseif( true == array_key_exists( 'IsDebitCard', $this->getChangedColumns() ) ) { $strSql .= ' is_debit_card = ' . $this->sqlIsDebitCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_check_card = ' . $this->sqlIsCheckCard() . ','; } elseif( true == array_key_exists( 'IsCheckCard', $this->getChangedColumns() ) ) { $strSql .= ' is_check_card = ' . $this->sqlIsCheckCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_credit_card = ' . $this->sqlIsCreditCard() . ','; } elseif( true == array_key_exists( 'IsCreditCard', $this->getChangedColumns() ) ) { $strSql .= ' is_credit_card = ' . $this->sqlIsCreditCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_gift_card = ' . $this->sqlIsGiftCard() . ','; } elseif( true == array_key_exists( 'IsGiftCard', $this->getChangedColumns() ) ) { $strSql .= ' is_gift_card = ' . $this->sqlIsGiftCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_commercial_card = ' . $this->sqlIsCommercialCard() . ','; } elseif( true == array_key_exists( 'IsCommercialCard', $this->getChangedColumns() ) ) { $strSql .= ' is_commercial_card = ' . $this->sqlIsCommercialCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_fleet_card = ' . $this->sqlIsFleetCard() . ','; } elseif( true == array_key_exists( 'IsFleetCard', $this->getChangedColumns() ) ) { $strSql .= ' is_fleet_card = ' . $this->sqlIsFleetCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_prepaid_card = ' . $this->sqlIsPrepaidCard() . ','; } elseif( true == array_key_exists( 'IsPrepaidCard', $this->getChangedColumns() ) ) { $strSql .= ' is_prepaid_card = ' . $this->sqlIsPrepaidCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hsa_fsa_account = ' . $this->sqlIsHsaFsaAccount() . ','; } elseif( true == array_key_exists( 'IsHsaFsaAccount', $this->getChangedColumns() ) ) { $strSql .= ' is_hsa_fsa_account = ' . $this->sqlIsHsaFsaAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_pinless_bill_pay = ' . $this->sqlIsPinlessBillPay() . ','; } elseif( true == array_key_exists( 'IsPinlessBillPay', $this->getChangedColumns() ) ) { $strSql .= ' is_pinless_bill_pay = ' . $this->sqlIsPinlessBillPay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ebt = ' . $this->sqlIsEbt() . ','; } elseif( true == array_key_exists( 'IsEbt', $this->getChangedColumns() ) ) { $strSql .= ' is_ebt = ' . $this->sqlIsEbt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_wic_bin = ' . $this->sqlIsWicBin() . ','; } elseif( true == array_key_exists( 'IsWicBin', $this->getChangedColumns() ) ) { $strSql .= ' is_wic_bin = ' . $this->sqlIsWicBin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_international_bin = ' . $this->sqlIsInternationalBin() . ','; } elseif( true == array_key_exists( 'IsInternationalBin', $this->getChangedColumns() ) ) { $strSql .= ' is_international_bin = ' . $this->sqlIsInternationalBin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_durbin_bin_regulation = ' . $this->sqlIsDurbinBinRegulation() . ','; } elseif( true == array_key_exists( 'IsDurbinBinRegulation', $this->getChangedColumns() ) ) { $strSql .= ' is_durbin_bin_regulation = ' . $this->sqlIsDurbinBinRegulation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_pinless_pos = ' . $this->sqlIsPinlessPos() . ','; } elseif( true == array_key_exists( 'IsPinlessPos', $this->getChangedColumns() ) ) { $strSql .= ' is_pinless_pos = ' . $this->sqlIsPinlessPos() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'sequence_number' => $this->getSequenceNumber(),
			'bin_length' => $this->getBinLength(),
			'pan_length' => $this->getPanLength(),
			'bin_number' => $this->getBinNumber(),
			'table_id' => $this->getTableId(),
			'is_debit_card' => $this->getIsDebitCard(),
			'is_check_card' => $this->getIsCheckCard(),
			'is_credit_card' => $this->getIsCreditCard(),
			'is_gift_card' => $this->getIsGiftCard(),
			'is_commercial_card' => $this->getIsCommercialCard(),
			'is_fleet_card' => $this->getIsFleetCard(),
			'is_prepaid_card' => $this->getIsPrepaidCard(),
			'is_hsa_fsa_account' => $this->getIsHsaFsaAccount(),
			'is_pinless_bill_pay' => $this->getIsPinlessBillPay(),
			'is_ebt' => $this->getIsEbt(),
			'is_wic_bin' => $this->getIsWicBin(),
			'is_international_bin' => $this->getIsInternationalBin(),
			'is_durbin_bin_regulation' => $this->getIsDurbinBinRegulation(),
			'is_pinless_pos' => $this->getIsPinlessPos(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>