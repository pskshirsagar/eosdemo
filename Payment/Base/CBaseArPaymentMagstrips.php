<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentMagstrips
 * Do not add any new functions to this class.
 */

class CBaseArPaymentMagstrips extends CEosPluralBase {

	/**
	 * @return CArPaymentMagstrip[]
	 */
	public static function fetchArPaymentMagstrips( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CArPaymentMagstrip::class, $objDatabase );
	}

	/**
	 * @return CArPaymentMagstrip
	 */
	public static function fetchArPaymentMagstrip( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CArPaymentMagstrip::class, $objDatabase );
	}

	public static function fetchArPaymentMagstripCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ar_payment_magstrips', $objDatabase );
	}

	public static function fetchArPaymentMagstripById( $intId, $objDatabase ) {
		return self::fetchArPaymentMagstrip( sprintf( 'SELECT * FROM ar_payment_magstrips WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchArPaymentMagstripsByCid( $intCid, $objDatabase ) {
		return self::fetchArPaymentMagstrips( sprintf( 'SELECT * FROM ar_payment_magstrips WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentMagstripsByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchArPaymentMagstrips( sprintf( 'SELECT * FROM ar_payment_magstrips WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

}
?>