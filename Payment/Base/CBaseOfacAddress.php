<?php

class CBaseOfacAddress extends CEosSingularBase {

	const TABLE_NAME = 'public.ofac_addresses';

	protected $m_intId;
	protected $m_intOfacId;
	protected $m_strAddress;
	protected $m_strCityOrProvince;
	protected $m_strCountry;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ofac_id'] ) && $boolDirectSet ) $this->set( 'm_intOfacId', trim( $arrValues['ofac_id'] ) ); elseif( isset( $arrValues['ofac_id'] ) ) $this->setOfacId( $arrValues['ofac_id'] );
		if( isset( $arrValues['address'] ) && $boolDirectSet ) $this->set( 'm_strAddress', trim( stripcslashes( $arrValues['address'] ) ) ); elseif( isset( $arrValues['address'] ) ) $this->setAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['address'] ) : $arrValues['address'] );
		if( isset( $arrValues['city_or_province'] ) && $boolDirectSet ) $this->set( 'm_strCityOrProvince', trim( stripcslashes( $arrValues['city_or_province'] ) ) ); elseif( isset( $arrValues['city_or_province'] ) ) $this->setCityOrProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city_or_province'] ) : $arrValues['city_or_province'] );
		if( isset( $arrValues['country'] ) && $boolDirectSet ) $this->set( 'm_strCountry', trim( stripcslashes( $arrValues['country'] ) ) ); elseif( isset( $arrValues['country'] ) ) $this->setCountry( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country'] ) : $arrValues['country'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOfacId( $intOfacId ) {
		$this->set( 'm_intOfacId', CStrings::strToIntDef( $intOfacId, NULL, false ) );
	}

	public function getOfacId() {
		return $this->m_intOfacId;
	}

	public function sqlOfacId() {
		return ( true == isset( $this->m_intOfacId ) ) ? ( string ) $this->m_intOfacId : 'NULL';
	}

	public function setAddress( $strAddress ) {
		$this->set( 'm_strAddress', CStrings::strTrimDef( $strAddress, -1, NULL, true ) );
	}

	public function getAddress() {
		return $this->m_strAddress;
	}

	public function sqlAddress() {
		return ( true == isset( $this->m_strAddress ) ) ? '\'' . addslashes( $this->m_strAddress ) . '\'' : 'NULL';
	}

	public function setCityOrProvince( $strCityOrProvince ) {
		$this->set( 'm_strCityOrProvince', CStrings::strTrimDef( $strCityOrProvince, 116, NULL, true ) );
	}

	public function getCityOrProvince() {
		return $this->m_strCityOrProvince;
	}

	public function sqlCityOrProvince() {
		return ( true == isset( $this->m_strCityOrProvince ) ) ? '\'' . addslashes( $this->m_strCityOrProvince ) . '\'' : 'NULL';
	}

	public function setCountry( $strCountry ) {
		$this->set( 'm_strCountry', CStrings::strTrimDef( $strCountry, 250, NULL, true ) );
	}

	public function getCountry() {
		return $this->m_strCountry;
	}

	public function sqlCountry() {
		return ( true == isset( $this->m_strCountry ) ) ? '\'' . addslashes( $this->m_strCountry ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ofac_id, address, city_or_province, country, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlOfacId() . ', ' .
 						$this->sqlAddress() . ', ' .
 						$this->sqlCityOrProvince() . ', ' .
 						$this->sqlCountry() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ofac_id = ' . $this->sqlOfacId() . ','; } elseif( true == array_key_exists( 'OfacId', $this->getChangedColumns() ) ) { $strSql .= ' ofac_id = ' . $this->sqlOfacId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address = ' . $this->sqlAddress() . ','; } elseif( true == array_key_exists( 'Address', $this->getChangedColumns() ) ) { $strSql .= ' address = ' . $this->sqlAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city_or_province = ' . $this->sqlCityOrProvince() . ','; } elseif( true == array_key_exists( 'CityOrProvince', $this->getChangedColumns() ) ) { $strSql .= ' city_or_province = ' . $this->sqlCityOrProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country = ' . $this->sqlCountry() . ','; } elseif( true == array_key_exists( 'Country', $this->getChangedColumns() ) ) { $strSql .= ' country = ' . $this->sqlCountry() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ofac_id' => $this->getOfacId(),
			'address' => $this->getAddress(),
			'city_or_province' => $this->getCityOrProvince(),
			'country' => $this->getCountry(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>