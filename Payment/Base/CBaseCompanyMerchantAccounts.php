<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CCompanyMerchantAccounts
 * Do not add any new functions to this class.
 */

class CBaseCompanyMerchantAccounts extends CEosPluralBase {

	/**
	 * @return CCompanyMerchantAccount[]
	 */
	public static function fetchCompanyMerchantAccounts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCompanyMerchantAccount::class, $objDatabase );
	}

	/**
	 * @return CCompanyMerchantAccount
	 */
	public static function fetchCompanyMerchantAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyMerchantAccount::class, $objDatabase );
	}

	public static function fetchCompanyMerchantAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_merchant_accounts', $objDatabase );
	}

	public static function fetchCompanyMerchantAccountById( $intId, $objDatabase ) {
		return self::fetchCompanyMerchantAccount( sprintf( 'SELECT * FROM company_merchant_accounts WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountsByProcessingBankId( $intProcessingBankId, $objDatabase ) {
		return self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE processing_bank_id = %d', ( int ) $intProcessingBankId ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountsByFundMovementTypeId( $intFundMovementTypeId, $objDatabase ) {
		return self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE fund_movement_type_id = %d', ( int ) $intFundMovementTypeId ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountsByCommercialMerchantAccountId( $intCommercialMerchantAccountId, $objDatabase ) {
		return self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE commercial_merchant_account_id = %d', ( int ) $intCommercialMerchantAccountId ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountsByDistributionAccountId( $intDistributionAccountId, $objDatabase ) {
		return self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE distribution_account_id = %d', ( int ) $intDistributionAccountId ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountsByIntermediaryAccountId( $intIntermediaryAccountId, $objDatabase ) {
		return self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE intermediary_account_id = %d', ( int ) $intIntermediaryAccountId ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountsByBillingAccountId( $intBillingAccountId, $objDatabase ) {
		return self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE billing_account_id = %d', ( int ) $intBillingAccountId ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountsByReturnsAccountId( $intReturnsAccountId, $objDatabase ) {
		return self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE returns_account_id = %d', ( int ) $intReturnsAccountId ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountsByClearingAccountId( $intClearingAccountId, $objDatabase ) {
		return self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE clearing_account_id = %d', ( int ) $intClearingAccountId ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountsByDonationAccountId( $intDonationAccountId, $objDatabase ) {
		return self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE donation_account_id = %d', ( int ) $intDonationAccountId ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountsByRebateAccountId( $intRebateAccountId, $objDatabase ) {
		return self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE rebate_account_id = %d', ( int ) $intRebateAccountId ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountsByMerchantProcessingTypeId( $intMerchantProcessingTypeId, $objDatabase ) {
		return self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE merchant_processing_type_id = %d', ( int ) $intMerchantProcessingTypeId ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountsByDistributionGroupTypeId( $intDistributionGroupTypeId, $objDatabase ) {
		return self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE distribution_group_type_id = %d', ( int ) $intDistributionGroupTypeId ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountsByAllocationTypeId( $intAllocationTypeId, $objDatabase ) {
		return self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE allocation_type_id = %d', ( int ) $intAllocationTypeId ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountsByExternalLegalEntityId( $strExternalLegalEntityId, $objDatabase ) {
		return self::fetchCompanyMerchantAccounts( sprintf( 'SELECT * FROM company_merchant_accounts WHERE external_legal_entity_id = \'%s\'', $strExternalLegalEntityId ), $objDatabase );
	}

}
?>