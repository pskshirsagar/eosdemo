<?php

class CBaseArPaymentStatistic extends CEosSingularBase {

	const TABLE_NAME = 'public.ar_payment_statistics';

	protected $m_intId;
	protected $m_strMonth;
	protected $m_fltDonationTotal;
	protected $m_fltConvenienceFeeTotal;
	protected $m_fltCompanyFeeTotal;
	protected $m_fltCostTotal;
	protected $m_fltProfitTotal;
	protected $m_fltAchTotal;
	protected $m_fltViTotal;
	protected $m_fltMcTotal;
	protected $m_fltAmexTotal;
	protected $m_fltDiTotal;
	protected $m_fltCh21Total;
	protected $m_fltWuTotal;
	protected $m_fltPnmTotal;
	protected $m_fltMgTotal;
	protected $m_fltAchRevenue;
	protected $m_fltCcRevenue;
	protected $m_fltDebitCardRevenue;
	protected $m_fltCardRevenue;
	protected $m_fltCh21Revenue;
	protected $m_fltWuRevenue;
	protected $m_intAchCount;
	protected $m_intViCount;
	protected $m_intMcCount;
	protected $m_intAmexCount;
	protected $m_intDiCount;
	protected $m_intCreditCardCount;
	protected $m_intDebitCardCount;
	protected $m_intCardCount;
	protected $m_intCh21Count;
	protected $m_intWuCount;
	protected $m_intPnmCount;
	protected $m_intMgCount;
	protected $m_intCcReturnCount;
	protected $m_intAchReturnCount;
	protected $m_intAchUnauthReturnCount;
	protected $m_intCheck21ReturnCount;
	protected $m_intCheck21UnauthReturnCount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->set( 'm_strMonth', trim( $arrValues['month'] ) ); elseif( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['donation_total'] ) && $boolDirectSet ) $this->set( 'm_fltDonationTotal', trim( $arrValues['donation_total'] ) ); elseif( isset( $arrValues['donation_total'] ) ) $this->setDonationTotal( $arrValues['donation_total'] );
		if( isset( $arrValues['convenience_fee_total'] ) && $boolDirectSet ) $this->set( 'm_fltConvenienceFeeTotal', trim( $arrValues['convenience_fee_total'] ) ); elseif( isset( $arrValues['convenience_fee_total'] ) ) $this->setConvenienceFeeTotal( $arrValues['convenience_fee_total'] );
		if( isset( $arrValues['company_fee_total'] ) && $boolDirectSet ) $this->set( 'm_fltCompanyFeeTotal', trim( $arrValues['company_fee_total'] ) ); elseif( isset( $arrValues['company_fee_total'] ) ) $this->setCompanyFeeTotal( $arrValues['company_fee_total'] );
		if( isset( $arrValues['cost_total'] ) && $boolDirectSet ) $this->set( 'm_fltCostTotal', trim( $arrValues['cost_total'] ) ); elseif( isset( $arrValues['cost_total'] ) ) $this->setCostTotal( $arrValues['cost_total'] );
		if( isset( $arrValues['profit_total'] ) && $boolDirectSet ) $this->set( 'm_fltProfitTotal', trim( $arrValues['profit_total'] ) ); elseif( isset( $arrValues['profit_total'] ) ) $this->setProfitTotal( $arrValues['profit_total'] );
		if( isset( $arrValues['ach_total'] ) && $boolDirectSet ) $this->set( 'm_fltAchTotal', trim( $arrValues['ach_total'] ) ); elseif( isset( $arrValues['ach_total'] ) ) $this->setAchTotal( $arrValues['ach_total'] );
		if( isset( $arrValues['vi_total'] ) && $boolDirectSet ) $this->set( 'm_fltViTotal', trim( $arrValues['vi_total'] ) ); elseif( isset( $arrValues['vi_total'] ) ) $this->setViTotal( $arrValues['vi_total'] );
		if( isset( $arrValues['mc_total'] ) && $boolDirectSet ) $this->set( 'm_fltMcTotal', trim( $arrValues['mc_total'] ) ); elseif( isset( $arrValues['mc_total'] ) ) $this->setMcTotal( $arrValues['mc_total'] );
		if( isset( $arrValues['amex_total'] ) && $boolDirectSet ) $this->set( 'm_fltAmexTotal', trim( $arrValues['amex_total'] ) ); elseif( isset( $arrValues['amex_total'] ) ) $this->setAmexTotal( $arrValues['amex_total'] );
		if( isset( $arrValues['di_total'] ) && $boolDirectSet ) $this->set( 'm_fltDiTotal', trim( $arrValues['di_total'] ) ); elseif( isset( $arrValues['di_total'] ) ) $this->setDiTotal( $arrValues['di_total'] );
		if( isset( $arrValues['ch21_total'] ) && $boolDirectSet ) $this->set( 'm_fltCh21Total', trim( $arrValues['ch21_total'] ) ); elseif( isset( $arrValues['ch21_total'] ) ) $this->setCh21Total( $arrValues['ch21_total'] );
		if( isset( $arrValues['wu_total'] ) && $boolDirectSet ) $this->set( 'm_fltWuTotal', trim( $arrValues['wu_total'] ) ); elseif( isset( $arrValues['wu_total'] ) ) $this->setWuTotal( $arrValues['wu_total'] );
		if( isset( $arrValues['pnm_total'] ) && $boolDirectSet ) $this->set( 'm_fltPnmTotal', trim( $arrValues['pnm_total'] ) ); elseif( isset( $arrValues['pnm_total'] ) ) $this->setPnmTotal( $arrValues['pnm_total'] );
		if( isset( $arrValues['mg_total'] ) && $boolDirectSet ) $this->set( 'm_fltMgTotal', trim( $arrValues['mg_total'] ) ); elseif( isset( $arrValues['mg_total'] ) ) $this->setMgTotal( $arrValues['mg_total'] );
		if( isset( $arrValues['ach_revenue'] ) && $boolDirectSet ) $this->set( 'm_fltAchRevenue', trim( $arrValues['ach_revenue'] ) ); elseif( isset( $arrValues['ach_revenue'] ) ) $this->setAchRevenue( $arrValues['ach_revenue'] );
		if( isset( $arrValues['cc_revenue'] ) && $boolDirectSet ) $this->set( 'm_fltCcRevenue', trim( $arrValues['cc_revenue'] ) ); elseif( isset( $arrValues['cc_revenue'] ) ) $this->setCcRevenue( $arrValues['cc_revenue'] );
		if( isset( $arrValues['debit_card_revenue'] ) && $boolDirectSet ) $this->set( 'm_fltDebitCardRevenue', trim( $arrValues['debit_card_revenue'] ) ); elseif( isset( $arrValues['debit_card_revenue'] ) ) $this->setDebitCardRevenue( $arrValues['debit_card_revenue'] );
		if( isset( $arrValues['card_revenue'] ) && $boolDirectSet ) $this->set( 'm_fltCardRevenue', trim( $arrValues['card_revenue'] ) ); elseif( isset( $arrValues['card_revenue'] ) ) $this->setCardRevenue( $arrValues['card_revenue'] );
		if( isset( $arrValues['ch21_revenue'] ) && $boolDirectSet ) $this->set( 'm_fltCh21Revenue', trim( $arrValues['ch21_revenue'] ) ); elseif( isset( $arrValues['ch21_revenue'] ) ) $this->setCh21Revenue( $arrValues['ch21_revenue'] );
		if( isset( $arrValues['wu_revenue'] ) && $boolDirectSet ) $this->set( 'm_fltWuRevenue', trim( $arrValues['wu_revenue'] ) ); elseif( isset( $arrValues['wu_revenue'] ) ) $this->setWuRevenue( $arrValues['wu_revenue'] );
		if( isset( $arrValues['ach_count'] ) && $boolDirectSet ) $this->set( 'm_intAchCount', trim( $arrValues['ach_count'] ) ); elseif( isset( $arrValues['ach_count'] ) ) $this->setAchCount( $arrValues['ach_count'] );
		if( isset( $arrValues['vi_count'] ) && $boolDirectSet ) $this->set( 'm_intViCount', trim( $arrValues['vi_count'] ) ); elseif( isset( $arrValues['vi_count'] ) ) $this->setViCount( $arrValues['vi_count'] );
		if( isset( $arrValues['mc_count'] ) && $boolDirectSet ) $this->set( 'm_intMcCount', trim( $arrValues['mc_count'] ) ); elseif( isset( $arrValues['mc_count'] ) ) $this->setMcCount( $arrValues['mc_count'] );
		if( isset( $arrValues['amex_count'] ) && $boolDirectSet ) $this->set( 'm_intAmexCount', trim( $arrValues['amex_count'] ) ); elseif( isset( $arrValues['amex_count'] ) ) $this->setAmexCount( $arrValues['amex_count'] );
		if( isset( $arrValues['credit_card_count'] ) && $boolDirectSet ) $this->set( 'm_intCreditCardCount', trim( $arrValues['credit_card_count'] ) ); elseif( isset( $arrValues['credit_card_count'] ) ) $this->setCreditCardCount( $arrValues['credit_card_count'] );
		if( isset( $arrValues['debit_card_count'] ) && $boolDirectSet ) $this->set( 'm_intDebitCardCount', trim( $arrValues['debit_card_count'] ) ); elseif( isset( $arrValues['debit_card_count'] ) ) $this->setDebitCardCount( $arrValues['debit_card_count'] );
		if( isset( $arrValues['card_count'] ) && $boolDirectSet ) $this->set( 'm_intCardCount', trim( $arrValues['card_count'] ) ); elseif( isset( $arrValues['card_count'] ) ) $this->setCardCount( $arrValues['card_count'] );
		if( isset( $arrValues['di_count'] ) && $boolDirectSet ) $this->set( 'm_intDiCount', trim( $arrValues['di_count'] ) ); elseif( isset( $arrValues['di_count'] ) ) $this->setDiCount( $arrValues['di_count'] );
		if( isset( $arrValues['ch21_count'] ) && $boolDirectSet ) $this->set( 'm_intCh21Count', trim( $arrValues['ch21_count'] ) ); elseif( isset( $arrValues['ch21_count'] ) ) $this->setCh21Count( $arrValues['ch21_count'] );
		if( isset( $arrValues['wu_count'] ) && $boolDirectSet ) $this->set( 'm_intWuCount', trim( $arrValues['wu_count'] ) ); elseif( isset( $arrValues['wu_count'] ) ) $this->setWuCount( $arrValues['wu_count'] );
		if( isset( $arrValues['pnm_count'] ) && $boolDirectSet ) $this->set( 'm_intPnmCount', trim( $arrValues['pnm_count'] ) ); elseif( isset( $arrValues['pnm_count'] ) ) $this->setPnmCount( $arrValues['pnm_count'] );
		if( isset( $arrValues['mg_count'] ) && $boolDirectSet ) $this->set( 'm_intMgCount', trim( $arrValues['mg_count'] ) ); elseif( isset( $arrValues['mg_count'] ) ) $this->setMgCount( $arrValues['mg_count'] );
		if( isset( $arrValues['cc_return_count'] ) && $boolDirectSet ) $this->set( 'm_intCcReturnCount', trim( $arrValues['cc_return_count'] ) ); elseif( isset( $arrValues['cc_return_count'] ) ) $this->setCcReturnCount( $arrValues['cc_return_count'] );
		if( isset( $arrValues['ach_return_count'] ) && $boolDirectSet ) $this->set( 'm_intAchReturnCount', trim( $arrValues['ach_return_count'] ) ); elseif( isset( $arrValues['ach_return_count'] ) ) $this->setAchReturnCount( $arrValues['ach_return_count'] );
		if( isset( $arrValues['ach_unauth_return_count'] ) && $boolDirectSet ) $this->set( 'm_intAchUnauthReturnCount', trim( $arrValues['ach_unauth_return_count'] ) ); elseif( isset( $arrValues['ach_unauth_return_count'] ) ) $this->setAchUnauthReturnCount( $arrValues['ach_unauth_return_count'] );
		if( isset( $arrValues['check21_return_count'] ) && $boolDirectSet ) $this->set( 'm_intCheck21ReturnCount', trim( $arrValues['check21_return_count'] ) ); elseif( isset( $arrValues['check21_return_count'] ) ) $this->setCheck21ReturnCount( $arrValues['check21_return_count'] );
		if( isset( $arrValues['check21_unauth_return_count'] ) && $boolDirectSet ) $this->set( 'm_intCheck21UnauthReturnCount', trim( $arrValues['check21_unauth_return_count'] ) ); elseif( isset( $arrValues['check21_unauth_return_count'] ) ) $this->setCheck21UnauthReturnCount( $arrValues['check21_unauth_return_count'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMonth( $strMonth ) {
		$this->set( 'm_strMonth', CStrings::strTrimDef( $strMonth, -1, NULL, true ) );
	}

	public function getMonth() {
		return $this->m_strMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_strMonth ) ) ? '\'' . $this->m_strMonth . '\'' : 'NOW()';
	}

	public function setDonationTotal( $fltDonationTotal ) {
		$this->set( 'm_fltDonationTotal', CStrings::strToFloatDef( $fltDonationTotal, NULL, false, 2 ) );
	}

	public function getDonationTotal() {
		return $this->m_fltDonationTotal;
	}

	public function sqlDonationTotal() {
		return ( true == isset( $this->m_fltDonationTotal ) ) ? ( string ) $this->m_fltDonationTotal : 'NULL';
	}

	public function setConvenienceFeeTotal( $fltConvenienceFeeTotal ) {
		$this->set( 'm_fltConvenienceFeeTotal', CStrings::strToFloatDef( $fltConvenienceFeeTotal, NULL, false, 2 ) );
	}

	public function getConvenienceFeeTotal() {
		return $this->m_fltConvenienceFeeTotal;
	}

	public function sqlConvenienceFeeTotal() {
		return ( true == isset( $this->m_fltConvenienceFeeTotal ) ) ? ( string ) $this->m_fltConvenienceFeeTotal : 'NULL';
	}

	public function setCompanyFeeTotal( $fltCompanyFeeTotal ) {
		$this->set( 'm_fltCompanyFeeTotal', CStrings::strToFloatDef( $fltCompanyFeeTotal, NULL, false, 2 ) );
	}

	public function getCompanyFeeTotal() {
		return $this->m_fltCompanyFeeTotal;
	}

	public function sqlCompanyFeeTotal() {
		return ( true == isset( $this->m_fltCompanyFeeTotal ) ) ? ( string ) $this->m_fltCompanyFeeTotal : 'NULL';
	}

	public function setCostTotal( $fltCostTotal ) {
		$this->set( 'm_fltCostTotal', CStrings::strToFloatDef( $fltCostTotal, NULL, false, 2 ) );
	}

	public function getCostTotal() {
		return $this->m_fltCostTotal;
	}

	public function sqlCostTotal() {
		return ( true == isset( $this->m_fltCostTotal ) ) ? ( string ) $this->m_fltCostTotal : 'NULL';
	}

	public function setProfitTotal( $fltProfitTotal ) {
		$this->set( 'm_fltProfitTotal', CStrings::strToFloatDef( $fltProfitTotal, NULL, false, 2 ) );
	}

	public function getProfitTotal() {
		return $this->m_fltProfitTotal;
	}

	public function sqlProfitTotal() {
		return ( true == isset( $this->m_fltProfitTotal ) ) ? ( string ) $this->m_fltProfitTotal : 'NULL';
	}

	public function setAchTotal( $fltAchTotal ) {
		$this->set( 'm_fltAchTotal', CStrings::strToFloatDef( $fltAchTotal, NULL, false, 2 ) );
	}

	public function getAchTotal() {
		return $this->m_fltAchTotal;
	}

	public function sqlAchTotal() {
		return ( true == isset( $this->m_fltAchTotal ) ) ? ( string ) $this->m_fltAchTotal : 'NULL';
	}

	public function setViTotal( $fltViTotal ) {
		$this->set( 'm_fltViTotal', CStrings::strToFloatDef( $fltViTotal, NULL, false, 2 ) );
	}

	public function getViTotal() {
		return $this->m_fltViTotal;
	}

	public function sqlViTotal() {
		return ( true == isset( $this->m_fltViTotal ) ) ? ( string ) $this->m_fltViTotal : 'NULL';
	}

	public function setMcTotal( $fltMcTotal ) {
		$this->set( 'm_fltMcTotal', CStrings::strToFloatDef( $fltMcTotal, NULL, false, 2 ) );
	}

	public function getMcTotal() {
		return $this->m_fltMcTotal;
	}

	public function sqlMcTotal() {
		return ( true == isset( $this->m_fltMcTotal ) ) ? ( string ) $this->m_fltMcTotal : 'NULL';
	}

	public function setAmexTotal( $fltAmexTotal ) {
		$this->set( 'm_fltAmexTotal', CStrings::strToFloatDef( $fltAmexTotal, NULL, false, 2 ) );
	}

	public function getAmexTotal() {
		return $this->m_fltAmexTotal;
	}

	public function sqlAmexTotal() {
		return ( true == isset( $this->m_fltAmexTotal ) ) ? ( string ) $this->m_fltAmexTotal : 'NULL';
	}

	public function setDiTotal( $fltDiTotal ) {
		$this->set( 'm_fltDiTotal', CStrings::strToFloatDef( $fltDiTotal, NULL, false, 2 ) );
	}

	public function getDiTotal() {
		return $this->m_fltDiTotal;
	}

	public function sqlDiTotal() {
		return ( true == isset( $this->m_fltDiTotal ) ) ? ( string ) $this->m_fltDiTotal : 'NULL';
	}

	public function setCh21Total( $fltCh21Total ) {
		$this->set( 'm_fltCh21Total', CStrings::strToFloatDef( $fltCh21Total, NULL, false, 2 ) );
	}

	public function getCh21Total() {
		return $this->m_fltCh21Total;
	}

	public function sqlCh21Total() {
		return ( true == isset( $this->m_fltCh21Total ) ) ? ( string ) $this->m_fltCh21Total : 'NULL';
	}

	public function setWuTotal( $fltWuTotal ) {
		$this->set( 'm_fltWuTotal', CStrings::strToFloatDef( $fltWuTotal, NULL, false, 2 ) );
	}

	public function getWuTotal() {
		return $this->m_fltWuTotal;
	}

	public function sqlWuTotal() {
		return ( true == isset( $this->m_fltWuTotal ) ) ? ( string ) $this->m_fltWuTotal : 'NULL';
	}

	public function setPnmTotal( $fltPnmTotal ) {
		$this->set( 'm_fltPnmTotal', CStrings::strToFloatDef( $fltPnmTotal, NULL, false, 2 ) );
	}

	public function getPnmTotal() {
		return $this->m_fltPnmTotal;
	}

	public function sqlPnmTotal() {
		return ( true == isset( $this->m_fltPnmTotal ) ) ? ( string ) $this->m_fltPnmTotal : 'NULL';
	}

	public function setMgTotal( $fltMgTotal ) {
		$this->set( 'm_fltMgTotal', CStrings::strToFloatDef( $fltMgTotal, NULL, false, 2 ) );
	}

	public function getMgTotal() {
		return $this->m_fltMgTotal;
	}

	public function sqlMgTotal() {
		return ( true == isset( $this->m_fltMgTotal ) ) ? ( string ) $this->m_fltMgTotal : 'NULL';
	}

	public function setAchRevenue( $fltAchRevenue ) {
		$this->set( 'm_fltAchRevenue', CStrings::strToFloatDef( $fltAchRevenue, NULL, false, 2 ) );
	}

	public function getAchRevenue() {
		return $this->m_fltAchRevenue;
	}

	public function sqlAchRevenue() {
		return ( true == isset( $this->m_fltAchRevenue ) ) ? ( string ) $this->m_fltAchRevenue : 'NULL';
	}

	public function setCcRevenue( $fltCcRevenue ) {
		$this->set( 'm_fltCcRevenue', CStrings::strToFloatDef( $fltCcRevenue, NULL, false, 2 ) );
	}

	public function getCcRevenue() {
		return $this->m_fltCcRevenue;
	}

	public function sqlCcRevenue() {
		return ( true == isset( $this->m_fltCcRevenue ) ) ? ( string ) $this->m_fltCcRevenue : 'NULL';
	}

	public function setDebitCardRevenue( $fltDebitCardRevenue ) {
		$this->set( 'm_fltDebitCardRevenue', CStrings::strToFloatDef( $fltDebitCardRevenue, NULL, false, 2 ) );
	}

	public function getDebitCardRevenue() {
		return $this->m_fltDebitCardRevenue;
	}

	public function sqlDebitCardRevenue() {
		return ( true == isset( $this->m_fltDebitCardRevenue ) ) ? ( string ) $this->m_fltDebitCardRevenue : 'NULL';
	}

	public function setCardRevenue( $fltCardRevenue ) {
		$this->set( 'm_fltCardRevenue', CStrings::strToFloatDef( $fltCardRevenue, NULL, false, 2 ) );
	}

	public function getCardRevenue() {
		return $this->m_fltCardRevenue;
	}

	public function sqlCardRevenue() {
		return ( true == isset( $this->m_fltCardRevenue ) ) ? ( string ) $this->m_fltCardRevenue : 'NULL';
	}

	public function setCh21Revenue( $fltCh21Revenue ) {
		$this->set( 'm_fltCh21Revenue', CStrings::strToFloatDef( $fltCh21Revenue, NULL, false, 2 ) );
	}

	public function getCh21Revenue() {
		return $this->m_fltCh21Revenue;
	}

	public function sqlCh21Revenue() {
		return ( true == isset( $this->m_fltCh21Revenue ) ) ? ( string ) $this->m_fltCh21Revenue : 'NULL';
	}

	public function setWuRevenue( $fltWuRevenue ) {
		$this->set( 'm_fltWuRevenue', CStrings::strToFloatDef( $fltWuRevenue, NULL, false, 2 ) );
	}

	public function getWuRevenue() {
		return $this->m_fltWuRevenue;
	}

	public function sqlWuRevenue() {
		return ( true == isset( $this->m_fltWuRevenue ) ) ? ( string ) $this->m_fltWuRevenue : 'NULL';
	}

	public function setAchCount( $intAchCount ) {
		$this->set( 'm_intAchCount', CStrings::strToIntDef( $intAchCount, NULL, false ) );
	}

	public function getAchCount() {
		return $this->m_intAchCount;
	}

	public function sqlAchCount() {
		return ( true == isset( $this->m_intAchCount ) ) ? ( string ) $this->m_intAchCount : 'NULL';
	}

	public function setViCount( $intViCount ) {
		$this->set( 'm_intViCount', CStrings::strToIntDef( $intViCount, NULL, false ) );
	}

	public function getViCount() {
		return $this->m_intViCount;
	}

	public function sqlViCount() {
		return ( true == isset( $this->m_intViCount ) ) ? ( string ) $this->m_intViCount : 'NULL';
	}

	public function setMcCount( $intMcCount ) {
		$this->set( 'm_intMcCount', CStrings::strToIntDef( $intMcCount, NULL, false ) );
	}

	public function getMcCount() {
		return $this->m_intMcCount;
	}

	public function sqlMcCount() {
		return ( true == isset( $this->m_intMcCount ) ) ? ( string ) $this->m_intMcCount : 'NULL';
	}

	public function setAmexCount( $intAmexCount ) {
		$this->set( 'm_intAmexCount', CStrings::strToIntDef( $intAmexCount, NULL, false ) );
	}

	public function getAmexCount() {
		return $this->m_intAmexCount;
	}

	public function sqlAmexCount() {
		return ( true == isset( $this->m_intAmexCount ) ) ? ( string ) $this->m_intAmexCount : 'NULL';
	}

	public function setDiCount( $intDiCount ) {
		$this->set( 'm_intDiCount', CStrings::strToIntDef( $intDiCount, NULL, false ) );
	}

	public function getDiCount() {
		return $this->m_intDiCount;
	}

	public function sqlDiCount() {
		return ( true == isset( $this->m_intDiCount ) ) ? ( string ) $this->m_intDiCount : 'NULL';
	}

	public function setCreditCardCount( $intCreditCardCount ) {
		$this->set( 'm_intCreditCardCount', CStrings::strToIntDef( $intCreditCardCount, NULL, false ) );
	}

	public function getCreditCardCount() {
		return $this->m_intCreditCardCount;
	}

	public function sqlCreditCardCount() {
		return ( true == isset( $this->m_intCreditCardCount ) ) ? ( string ) $this->m_intCreditCardCount : 'NULL';
	}

	public function setDebitCardCount( $intDebitCardCount ) {
		$this->set( 'm_intDebitCardCount', CStrings::strToIntDef( $intDebitCardCount, NULL, false ) );
	}

	public function getDebitCardCount() {
		return $this->m_intDebitCardCount;
	}

	public function sqlDebitCardCount() {
		return ( true == isset( $this->m_intDebitCardCount ) ) ? ( string ) $this->m_intDebitCardCount : 'NULL';
	}

	public function setCardCount( $intCardCount ) {
		$this->set( 'm_intCardCount', CStrings::strToIntDef( $intCardCount, NULL, false ) );
	}

	public function getCardCount() {
		return $this->m_intCardCount;
	}

	public function sqlCardCount() {
		return ( true == isset( $this->m_intCardCount ) ) ? ( string ) $this->m_intCardCount : 'NULL';
	}

	public function setCh21Count( $intCh21Count ) {
		$this->set( 'm_intCh21Count', CStrings::strToIntDef( $intCh21Count, NULL, false ) );
	}

	public function getCh21Count() {
		return $this->m_intCh21Count;
	}

	public function sqlCh21Count() {
		return ( true == isset( $this->m_intCh21Count ) ) ? ( string ) $this->m_intCh21Count : 'NULL';
	}

	public function setWuCount( $intWuCount ) {
		$this->set( 'm_intWuCount', CStrings::strToIntDef( $intWuCount, NULL, false ) );
	}

	public function getWuCount() {
		return $this->m_intWuCount;
	}

	public function sqlWuCount() {
		return ( true == isset( $this->m_intWuCount ) ) ? ( string ) $this->m_intWuCount : 'NULL';
	}

	public function setPnmCount( $intPnmCount ) {
		$this->set( 'm_intPnmCount', CStrings::strToIntDef( $intPnmCount, NULL, false ) );
	}

	public function getPnmCount() {
		return $this->m_intPnmCount;
	}

	public function sqlPnmCount() {
		return ( true == isset( $this->m_intPnmCount ) ) ? ( string ) $this->m_intPnmCount : 'NULL';
	}

	public function setMgCount( $intMgCount ) {
		$this->set( 'm_intMgCount', CStrings::strToIntDef( $intMgCount, NULL, false ) );
	}

	public function getMgCount() {
		return $this->m_intMgCount;
	}

	public function sqlMgCount() {
		return ( true == isset( $this->m_intMgCount ) ) ? ( string ) $this->m_intMgCount : 'NULL';
	}

	public function setCcReturnCount( $intCcReturnCount ) {
		$this->set( 'm_intCcReturnCount', CStrings::strToIntDef( $intCcReturnCount, NULL, false ) );
	}

	public function getCcReturnCount() {
		return $this->m_intCcReturnCount;
	}

	public function sqlCcReturnCount() {
		return ( true == isset( $this->m_intCcReturnCount ) ) ? ( string ) $this->m_intCcReturnCount : 'NULL';
	}

	public function setAchReturnCount( $intAchReturnCount ) {
		$this->set( 'm_intAchReturnCount', CStrings::strToIntDef( $intAchReturnCount, NULL, false ) );
	}

	public function getAchReturnCount() {
		return $this->m_intAchReturnCount;
	}

	public function sqlAchReturnCount() {
		return ( true == isset( $this->m_intAchReturnCount ) ) ? ( string ) $this->m_intAchReturnCount : 'NULL';
	}

	public function setAchUnauthReturnCount( $intAchUnauthReturnCount ) {
		$this->set( 'm_intAchUnauthReturnCount', CStrings::strToIntDef( $intAchUnauthReturnCount, NULL, false ) );
	}

	public function getAchUnauthReturnCount() {
		return $this->m_intAchUnauthReturnCount;
	}

	public function sqlAchUnauthReturnCount() {
		return ( true == isset( $this->m_intAchUnauthReturnCount ) ) ? ( string ) $this->m_intAchUnauthReturnCount : 'NULL';
	}

	public function setCheck21ReturnCount( $intCheck21ReturnCount ) {
		$this->set( 'm_intCheck21ReturnCount', CStrings::strToIntDef( $intCheck21ReturnCount, NULL, false ) );
	}

	public function getCheck21ReturnCount() {
		return $this->m_intCheck21ReturnCount;
	}

	public function sqlCheck21ReturnCount() {
		return ( true == isset( $this->m_intCheck21ReturnCount ) ) ? ( string ) $this->m_intCheck21ReturnCount : 'NULL';
	}

	public function setCheck21UnauthReturnCount( $intCheck21UnauthReturnCount ) {
		$this->set( 'm_intCheck21UnauthReturnCount', CStrings::strToIntDef( $intCheck21UnauthReturnCount, NULL, false ) );
	}

	public function getCheck21UnauthReturnCount() {
		return $this->m_intCheck21UnauthReturnCount;
	}

	public function sqlCheck21UnauthReturnCount() {
		return ( true == isset( $this->m_intCheck21UnauthReturnCount ) ) ? ( string ) $this->m_intCheck21UnauthReturnCount : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, month, donation_total, convenience_fee_total, company_fee_total, cost_total, profit_total, ach_total, vi_total, mc_total,
							amex_total, di_total, ch21_total, wu_total, pnm_total, mg_total, ach_revenue,
							cc_revenue,
							debit_card_revenue,
							card_revenue,
							ch21_revenue, wu_revenue, ach_count, vi_count, mc_count, amex_count, di_count,
							credit_card_count,
							debit_card_count,
							card_count,
							ch21_count, wu_count, pnm_count, mg_count, cc_return_count, ach_return_count, ach_unauth_return_count, check21_return_count,
							check21_unauth_return_count, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlMonth() . ', ' .
 						$this->sqlDonationTotal() . ', ' .
 						$this->sqlConvenienceFeeTotal() . ', ' .
 						$this->sqlCompanyFeeTotal() . ', ' .
 						$this->sqlCostTotal() . ', ' .
 						$this->sqlProfitTotal() . ', ' .
 						$this->sqlAchTotal() . ', ' .
 						$this->sqlViTotal() . ', ' .
 						$this->sqlMcTotal() . ', ' .
 						$this->sqlAmexTotal() . ', ' .
 						$this->sqlDiTotal() . ', ' .
 						$this->sqlCh21Total() . ', ' .
 						$this->sqlWuTotal() . ', ' .
 						$this->sqlPnmTotal() . ', ' .
 						$this->sqlMgTotal() . ', ' .
 						$this->sqlAchRevenue() . ', ' .
 						$this->sqlCcRevenue() . ', ' .
						$this->sqlDebitCardRevenue() . ', ' .
						$this->sqlCardRevenue() . ', ' .
 						$this->sqlCh21Revenue() . ', ' .
 						$this->sqlWuRevenue() . ', ' .
 						$this->sqlAchCount() . ', ' .
 						$this->sqlViCount() . ', ' .
 						$this->sqlMcCount() . ', ' .
 						$this->sqlAmexCount() . ', ' .
 						$this->sqlDiCount() . ', ' .
						$this->sqlCreditCardCount() . ', ' .
						$this->sqlDebitCardCount() . ', ' .
						$this->sqlCardCount() . ', ' .
						$this->sqlCh21Count() . ', ' .
 						$this->sqlWuCount() . ', ' .
 						$this->sqlPnmCount() . ', ' .
 						$this->sqlMgCount() . ', ' .
 						$this->sqlCcReturnCount() . ', ' .
 						$this->sqlAchReturnCount() . ', ' .
 						$this->sqlAchUnauthReturnCount() . ', ' .
 						$this->sqlCheck21ReturnCount() . ', ' .
 						$this->sqlCheck21UnauthReturnCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; } elseif( true == array_key_exists( 'Month', $this->getChangedColumns() ) ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' donation_total = ' . $this->sqlDonationTotal() . ','; } elseif( true == array_key_exists( 'DonationTotal', $this->getChangedColumns() ) ) { $strSql .= ' donation_total = ' . $this->sqlDonationTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' convenience_fee_total = ' . $this->sqlConvenienceFeeTotal() . ','; } elseif( true == array_key_exists( 'ConvenienceFeeTotal', $this->getChangedColumns() ) ) { $strSql .= ' convenience_fee_total = ' . $this->sqlConvenienceFeeTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_fee_total = ' . $this->sqlCompanyFeeTotal() . ','; } elseif( true == array_key_exists( 'CompanyFeeTotal', $this->getChangedColumns() ) ) { $strSql .= ' company_fee_total = ' . $this->sqlCompanyFeeTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cost_total = ' . $this->sqlCostTotal() . ','; } elseif( true == array_key_exists( 'CostTotal', $this->getChangedColumns() ) ) { $strSql .= ' cost_total = ' . $this->sqlCostTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' profit_total = ' . $this->sqlProfitTotal() . ','; } elseif( true == array_key_exists( 'ProfitTotal', $this->getChangedColumns() ) ) { $strSql .= ' profit_total = ' . $this->sqlProfitTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ach_total = ' . $this->sqlAchTotal() . ','; } elseif( true == array_key_exists( 'AchTotal', $this->getChangedColumns() ) ) { $strSql .= ' ach_total = ' . $this->sqlAchTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vi_total = ' . $this->sqlViTotal() . ','; } elseif( true == array_key_exists( 'ViTotal', $this->getChangedColumns() ) ) { $strSql .= ' vi_total = ' . $this->sqlViTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mc_total = ' . $this->sqlMcTotal() . ','; } elseif( true == array_key_exists( 'McTotal', $this->getChangedColumns() ) ) { $strSql .= ' mc_total = ' . $this->sqlMcTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amex_total = ' . $this->sqlAmexTotal() . ','; } elseif( true == array_key_exists( 'AmexTotal', $this->getChangedColumns() ) ) { $strSql .= ' amex_total = ' . $this->sqlAmexTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' di_total = ' . $this->sqlDiTotal() . ','; } elseif( true == array_key_exists( 'DiTotal', $this->getChangedColumns() ) ) { $strSql .= ' di_total = ' . $this->sqlDiTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ch21_total = ' . $this->sqlCh21Total() . ','; } elseif( true == array_key_exists( 'Ch21Total', $this->getChangedColumns() ) ) { $strSql .= ' ch21_total = ' . $this->sqlCh21Total() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wu_total = ' . $this->sqlWuTotal() . ','; } elseif( true == array_key_exists( 'WuTotal', $this->getChangedColumns() ) ) { $strSql .= ' wu_total = ' . $this->sqlWuTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pnm_total = ' . $this->sqlPnmTotal() . ','; } elseif( true == array_key_exists( 'PnmTotal', $this->getChangedColumns() ) ) { $strSql .= ' pnm_total = ' . $this->sqlPnmTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mg_total = ' . $this->sqlMgTotal() . ','; } elseif( true == array_key_exists( 'MgTotal', $this->getChangedColumns() ) ) { $strSql .= ' mg_total = ' . $this->sqlMgTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ach_revenue = ' . $this->sqlAchRevenue() . ','; } elseif( true == array_key_exists( 'AchRevenue', $this->getChangedColumns() ) ) { $strSql .= ' ach_revenue = ' . $this->sqlAchRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_revenue = ' . $this->sqlCcRevenue() . ','; } elseif( true == array_key_exists( 'CcRevenue', $this->getChangedColumns() ) ) { $strSql .= ' cc_revenue = ' . $this->sqlCcRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' debit_card_revenue = ' . $this->sqlDebitCardRevenue() . ','; } elseif( true == array_key_exists( 'DebitCardRevenue', $this->getChangedColumns() ) ) { $strSql .= ' debit_card_revenue = ' . $this->sqlDebitCardRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' card_revenue = ' . $this->sqlCardRevenue() . ','; } elseif( true == array_key_exists( 'CardRevenue', $this->getChangedColumns() ) ) { $strSql .= ' card_revenue = ' . $this->sqlCardRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ch21_revenue = ' . $this->sqlCh21Revenue() . ','; } elseif( true == array_key_exists( 'Ch21Revenue', $this->getChangedColumns() ) ) { $strSql .= ' ch21_revenue = ' . $this->sqlCh21Revenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wu_revenue = ' . $this->sqlWuRevenue() . ','; } elseif( true == array_key_exists( 'WuRevenue', $this->getChangedColumns() ) ) { $strSql .= ' wu_revenue = ' . $this->sqlWuRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ach_count = ' . $this->sqlAchCount() . ','; } elseif( true == array_key_exists( 'AchCount', $this->getChangedColumns() ) ) { $strSql .= ' ach_count = ' . $this->sqlAchCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vi_count = ' . $this->sqlViCount() . ','; } elseif( true == array_key_exists( 'ViCount', $this->getChangedColumns() ) ) { $strSql .= ' vi_count = ' . $this->sqlViCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mc_count = ' . $this->sqlMcCount() . ','; } elseif( true == array_key_exists( 'McCount', $this->getChangedColumns() ) ) { $strSql .= ' mc_count = ' . $this->sqlMcCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amex_count = ' . $this->sqlAmexCount() . ','; } elseif( true == array_key_exists( 'AmexCount', $this->getChangedColumns() ) ) { $strSql .= ' amex_count = ' . $this->sqlAmexCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' di_count = ' . $this->sqlDiCount() . ','; } elseif( true == array_key_exists( 'DiCount', $this->getChangedColumns() ) ) { $strSql .= ' di_count = ' . $this->sqlDiCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_card_count = ' . $this->sqlCreditCardCount() . ','; } elseif( true == array_key_exists( 'CreditCardCount', $this->getChangedColumns() ) ) { $strSql .= ' credit_card_count = ' . $this->sqlCreditCardCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' debit_card_count = ' . $this->sqlDebitCardCount() . ','; } elseif( true == array_key_exists( 'DebitCardCount', $this->getChangedColumns() ) ) { $strSql .= ' debit_card_count = ' . $this->sqlDebitCardCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' card_count = ' . $this->sqlCardCount() . ','; } elseif( true == array_key_exists( 'CardCount', $this->getChangedColumns() ) ) { $strSql .= ' card_count = ' . $this->sqlCardCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ch21_count = ' . $this->sqlCh21Count() . ','; } elseif( true == array_key_exists( 'Ch21Count', $this->getChangedColumns() ) ) { $strSql .= ' ch21_count = ' . $this->sqlCh21Count() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wu_count = ' . $this->sqlWuCount() . ','; } elseif( true == array_key_exists( 'WuCount', $this->getChangedColumns() ) ) { $strSql .= ' wu_count = ' . $this->sqlWuCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pnm_count = ' . $this->sqlPnmCount() . ','; } elseif( true == array_key_exists( 'PnmCount', $this->getChangedColumns() ) ) { $strSql .= ' pnm_count = ' . $this->sqlPnmCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mg_count = ' . $this->sqlMgCount() . ','; } elseif( true == array_key_exists( 'MgCount', $this->getChangedColumns() ) ) { $strSql .= ' mg_count = ' . $this->sqlMgCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_return_count = ' . $this->sqlCcReturnCount() . ','; } elseif( true == array_key_exists( 'CcReturnCount', $this->getChangedColumns() ) ) { $strSql .= ' cc_return_count = ' . $this->sqlCcReturnCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ach_return_count = ' . $this->sqlAchReturnCount() . ','; } elseif( true == array_key_exists( 'AchReturnCount', $this->getChangedColumns() ) ) { $strSql .= ' ach_return_count = ' . $this->sqlAchReturnCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ach_unauth_return_count = ' . $this->sqlAchUnauthReturnCount() . ','; } elseif( true == array_key_exists( 'AchUnauthReturnCount', $this->getChangedColumns() ) ) { $strSql .= ' ach_unauth_return_count = ' . $this->sqlAchUnauthReturnCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check21_return_count = ' . $this->sqlCheck21ReturnCount() . ','; } elseif( true == array_key_exists( 'Check21ReturnCount', $this->getChangedColumns() ) ) { $strSql .= ' check21_return_count = ' . $this->sqlCheck21ReturnCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check21_unauth_return_count = ' . $this->sqlCheck21UnauthReturnCount() . ','; } elseif( true == array_key_exists( 'Check21UnauthReturnCount', $this->getChangedColumns() ) ) { $strSql .= ' check21_unauth_return_count = ' . $this->sqlCheck21UnauthReturnCount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'month' => $this->getMonth(),
			'donation_total' => $this->getDonationTotal(),
			'convenience_fee_total' => $this->getConvenienceFeeTotal(),
			'company_fee_total' => $this->getCompanyFeeTotal(),
			'cost_total' => $this->getCostTotal(),
			'profit_total' => $this->getProfitTotal(),
			'ach_total' => $this->getAchTotal(),
			'vi_total' => $this->getViTotal(),
			'mc_total' => $this->getMcTotal(),
			'amex_total' => $this->getAmexTotal(),
			'di_total' => $this->getDiTotal(),
			'ch21_total' => $this->getCh21Total(),
			'wu_total' => $this->getWuTotal(),
			'pnm_total' => $this->getPnmTotal(),
			'mg_total' => $this->getMgTotal(),
			'ach_revenue' => $this->getAchRevenue(),
			'cc_revenue' => $this->getCcRevenue(),
			'ch21_revenue' => $this->getCh21Revenue(),
			'wu_revenue' => $this->getWuRevenue(),
			'ach_count' => $this->getAchCount(),
			'vi_count' => $this->getViCount(),
			'mc_count' => $this->getMcCount(),
			'amex_count' => $this->getAmexCount(),
			'di_count' => $this->getDiCount(),
			'ch21_count' => $this->getCh21Count(),
			'wu_count' => $this->getWuCount(),
			'pnm_count' => $this->getPnmCount(),
			'mg_count' => $this->getMgCount(),
			'cc_return_count' => $this->getCcReturnCount(),
			'ach_return_count' => $this->getAchReturnCount(),
			'ach_unauth_return_count' => $this->getAchUnauthReturnCount(),
			'check21_return_count' => $this->getCheck21ReturnCount(),
			'check21_unauth_return_count' => $this->getCheck21UnauthReturnCount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>