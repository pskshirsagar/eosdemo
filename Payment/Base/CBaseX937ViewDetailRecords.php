<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937ViewDetailRecords
 * Do not add any new functions to this class.
 */

class CBaseX937ViewDetailRecords extends CEosPluralBase {

	/**
	 * @return CX937ViewDetailRecord[]
	 */
	public static function fetchX937ViewDetailRecords( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CX937ViewDetailRecord::class, $objDatabase );
	}

	/**
	 * @return CX937ViewDetailRecord
	 */
	public static function fetchX937ViewDetailRecord( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CX937ViewDetailRecord::class, $objDatabase );
	}

	public static function fetchX937ViewDetailRecordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'x937_view_detail_records', $objDatabase );
	}

	public static function fetchX937ViewDetailRecordById( $intId, $objDatabase ) {
		return self::fetchX937ViewDetailRecord( sprintf( 'SELECT * FROM x937_view_detail_records WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchX937ViewDetailRecordsByX937CheckDetailRecordId( $intX937CheckDetailRecordId, $objDatabase ) {
		return self::fetchX937ViewDetailRecords( sprintf( 'SELECT * FROM x937_view_detail_records WHERE x937_check_detail_record_id = %d', ( int ) $intX937CheckDetailRecordId ), $objDatabase );
	}

}
?>