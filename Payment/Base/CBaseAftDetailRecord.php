<?php

class CBaseAftDetailRecord extends CEosSingularBase {

	const TABLE_NAME = 'public.aft_detail_records';

	protected $m_intId;
	protected $m_intAftFileId;
	protected $m_intAftFileBatchId;
	protected $m_intCid;
	protected $m_intSettlementDistributionId;
	protected $m_intEftInstructionId;
	protected $m_strLogicalRecordTypeCode;
	protected $m_strAmount;
	protected $m_strInstitutionId;
	protected $m_strAccountNumberEncrypted;
	protected $m_strNameOnAccount;
	protected $m_strIdentificationNumber;
	protected $m_strReturnedOn;
	protected $m_boolIsReconPrepped;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsReconPrepped = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['aft_file_id'] ) && $boolDirectSet ) $this->set( 'm_intAftFileId', trim( $arrValues['aft_file_id'] ) ); elseif( isset( $arrValues['aft_file_id'] ) ) $this->setAftFileId( $arrValues['aft_file_id'] );
		if( isset( $arrValues['aft_file_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intAftFileBatchId', trim( $arrValues['aft_file_batch_id'] ) ); elseif( isset( $arrValues['aft_file_batch_id'] ) ) $this->setAftFileBatchId( $arrValues['aft_file_batch_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['settlement_distribution_id'] ) && $boolDirectSet ) $this->set( 'm_intSettlementDistributionId', trim( $arrValues['settlement_distribution_id'] ) ); elseif( isset( $arrValues['settlement_distribution_id'] ) ) $this->setSettlementDistributionId( $arrValues['settlement_distribution_id'] );
		if( isset( $arrValues['eft_instruction_id'] ) && $boolDirectSet ) $this->set( 'm_intEftInstructionId', trim( $arrValues['eft_instruction_id'] ) ); elseif( isset( $arrValues['eft_instruction_id'] ) ) $this->setEftInstructionId( $arrValues['eft_instruction_id'] );
		if( isset( $arrValues['logical_record_type_code'] ) && $boolDirectSet ) $this->set( 'm_strLogicalRecordTypeCode', trim( $arrValues['logical_record_type_code'] ) ); elseif( isset( $arrValues['logical_record_type_code'] ) ) $this->setLogicalRecordTypeCode( $arrValues['logical_record_type_code'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_strAmount', trim( $arrValues['amount'] ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['institution_id'] ) && $boolDirectSet ) $this->set( 'm_strInstitutionId', trim( $arrValues['institution_id'] ) ); elseif( isset( $arrValues['institution_id'] ) ) $this->setInstitutionId( $arrValues['institution_id'] );
		if( isset( $arrValues['account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumberEncrypted', trim( $arrValues['account_number_encrypted'] ) ); elseif( isset( $arrValues['account_number_encrypted'] ) ) $this->setAccountNumberEncrypted( $arrValues['account_number_encrypted'] );
		if( isset( $arrValues['name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strNameOnAccount', trim( $arrValues['name_on_account'] ) ); elseif( isset( $arrValues['name_on_account'] ) ) $this->setNameOnAccount( $arrValues['name_on_account'] );
		if( isset( $arrValues['identification_number'] ) && $boolDirectSet ) $this->set( 'm_strIdentificationNumber', trim( $arrValues['identification_number'] ) ); elseif( isset( $arrValues['identification_number'] ) ) $this->setIdentificationNumber( $arrValues['identification_number'] );
		if( isset( $arrValues['returned_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnedOn', trim( $arrValues['returned_on'] ) ); elseif( isset( $arrValues['returned_on'] ) ) $this->setReturnedOn( $arrValues['returned_on'] );
		if( isset( $arrValues['is_recon_prepped'] ) && $boolDirectSet ) $this->set( 'm_boolIsReconPrepped', trim( stripcslashes( $arrValues['is_recon_prepped'] ) ) ); elseif( isset( $arrValues['is_recon_prepped'] ) ) $this->setIsReconPrepped( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_recon_prepped'] ) : $arrValues['is_recon_prepped'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAftFileId( $intAftFileId ) {
		$this->set( 'm_intAftFileId', CStrings::strToIntDef( $intAftFileId, NULL, false ) );
	}

	public function getAftFileId() {
		return $this->m_intAftFileId;
	}

	public function sqlAftFileId() {
		return ( true == isset( $this->m_intAftFileId ) ) ? ( string ) $this->m_intAftFileId : 'NULL';
	}

	public function setAftFileBatchId( $intAftFileBatchId ) {
		$this->set( 'm_intAftFileBatchId', CStrings::strToIntDef( $intAftFileBatchId, NULL, false ) );
	}

	public function getAftFileBatchId() {
		return $this->m_intAftFileBatchId;
	}

	public function sqlAftFileBatchId() {
		return ( true == isset( $this->m_intAftFileBatchId ) ) ? ( string ) $this->m_intAftFileBatchId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setSettlementDistributionId( $intSettlementDistributionId ) {
		$this->set( 'm_intSettlementDistributionId', CStrings::strToIntDef( $intSettlementDistributionId, NULL, false ) );
	}

	public function getSettlementDistributionId() {
		return $this->m_intSettlementDistributionId;
	}

	public function sqlSettlementDistributionId() {
		return ( true == isset( $this->m_intSettlementDistributionId ) ) ? ( string ) $this->m_intSettlementDistributionId : 'NULL';
	}

	public function setEftInstructionId( $intEftInstructionId ) {
		$this->set( 'm_intEftInstructionId', CStrings::strToIntDef( $intEftInstructionId, NULL, false ) );
	}

	public function getEftInstructionId() {
		return $this->m_intEftInstructionId;
	}

	public function sqlEftInstructionId() {
		return ( true == isset( $this->m_intEftInstructionId ) ) ? ( string ) $this->m_intEftInstructionId : 'NULL';
	}

	public function setLogicalRecordTypeCode( $strLogicalRecordTypeCode ) {
		$this->set( 'm_strLogicalRecordTypeCode', CStrings::strTrimDef( $strLogicalRecordTypeCode, 1, NULL, true ) );
	}

	public function getLogicalRecordTypeCode() {
		return $this->m_strLogicalRecordTypeCode;
	}

	public function sqlLogicalRecordTypeCode() {
		return ( true == isset( $this->m_strLogicalRecordTypeCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLogicalRecordTypeCode ) : '\'' . addslashes( $this->m_strLogicalRecordTypeCode ) . '\'' ) : 'NULL';
	}

	public function setAmount( $strAmount ) {
		$this->set( 'm_strAmount', CStrings::strTrimDef( $strAmount, 10, NULL, true ) );
	}

	public function getAmount() {
		return $this->m_strAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_strAmount ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAmount ) : '\'' . addslashes( $this->m_strAmount ) . '\'' ) : 'NULL';
	}

	public function setInstitutionId( $strInstitutionId ) {
		$this->set( 'm_strInstitutionId', CStrings::strTrimDef( $strInstitutionId, 9, NULL, true ) );
	}

	public function getInstitutionId() {
		return $this->m_strInstitutionId;
	}

	public function sqlInstitutionId() {
		return ( true == isset( $this->m_strInstitutionId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInstitutionId ) : '\'' . addslashes( $this->m_strInstitutionId ) . '\'' ) : 'NULL';
	}

	public function setAccountNumberEncrypted( $strAccountNumberEncrypted ) {
		$this->set( 'm_strAccountNumberEncrypted', CStrings::strTrimDef( $strAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getAccountNumberEncrypted() {
		return $this->m_strAccountNumberEncrypted;
	}

	public function sqlAccountNumberEncrypted() {
		return ( true == isset( $this->m_strAccountNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAccountNumberEncrypted ) : '\'' . addslashes( $this->m_strAccountNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setNameOnAccount( $strNameOnAccount ) {
		$this->set( 'm_strNameOnAccount', CStrings::strTrimDef( $strNameOnAccount, 29, NULL, true ) );
	}

	public function getNameOnAccount() {
		return $this->m_strNameOnAccount;
	}

	public function sqlNameOnAccount() {
		return ( true == isset( $this->m_strNameOnAccount ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameOnAccount ) : '\'' . addslashes( $this->m_strNameOnAccount ) . '\'' ) : 'NULL';
	}

	public function setIdentificationNumber( $strIdentificationNumber ) {
		$this->set( 'm_strIdentificationNumber', CStrings::strTrimDef( $strIdentificationNumber, 19, NULL, true ) );
	}

	public function getIdentificationNumber() {
		return $this->m_strIdentificationNumber;
	}

	public function sqlIdentificationNumber() {
		return ( true == isset( $this->m_strIdentificationNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIdentificationNumber ) : '\'' . addslashes( $this->m_strIdentificationNumber ) . '\'' ) : 'NULL';
	}

	public function setReturnedOn( $strReturnedOn ) {
		$this->set( 'm_strReturnedOn', CStrings::strTrimDef( $strReturnedOn, -1, NULL, true ) );
	}

	public function getReturnedOn() {
		return $this->m_strReturnedOn;
	}

	public function sqlReturnedOn() {
		return ( true == isset( $this->m_strReturnedOn ) ) ? '\'' . $this->m_strReturnedOn . '\'' : 'NULL';
	}

	public function setIsReconPrepped( $boolIsReconPrepped ) {
		$this->set( 'm_boolIsReconPrepped', CStrings::strToBool( $boolIsReconPrepped ) );
	}

	public function getIsReconPrepped() {
		return $this->m_boolIsReconPrepped;
	}

	public function sqlIsReconPrepped() {
		return ( true == isset( $this->m_boolIsReconPrepped ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReconPrepped ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, aft_file_id, aft_file_batch_id, cid, settlement_distribution_id, eft_instruction_id, logical_record_type_code, amount, institution_id, account_number_encrypted, name_on_account, identification_number, returned_on, is_recon_prepped, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlAftFileId() . ', ' .
						$this->sqlAftFileBatchId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlSettlementDistributionId() . ', ' .
						$this->sqlEftInstructionId() . ', ' .
						$this->sqlLogicalRecordTypeCode() . ', ' .
						$this->sqlAmount() . ', ' .
						$this->sqlInstitutionId() . ', ' .
						$this->sqlAccountNumberEncrypted() . ', ' .
						$this->sqlNameOnAccount() . ', ' .
						$this->sqlIdentificationNumber() . ', ' .
						$this->sqlReturnedOn() . ', ' .
						$this->sqlIsReconPrepped() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' aft_file_id = ' . $this->sqlAftFileId(). ',' ; } elseif( true == array_key_exists( 'AftFileId', $this->getChangedColumns() ) ) { $strSql .= ' aft_file_id = ' . $this->sqlAftFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' aft_file_batch_id = ' . $this->sqlAftFileBatchId(). ',' ; } elseif( true == array_key_exists( 'AftFileBatchId', $this->getChangedColumns() ) ) { $strSql .= ' aft_file_batch_id = ' . $this->sqlAftFileBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId(). ',' ; } elseif( true == array_key_exists( 'SettlementDistributionId', $this->getChangedColumns() ) ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eft_instruction_id = ' . $this->sqlEftInstructionId(). ',' ; } elseif( true == array_key_exists( 'EftInstructionId', $this->getChangedColumns() ) ) { $strSql .= ' eft_instruction_id = ' . $this->sqlEftInstructionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' logical_record_type_code = ' . $this->sqlLogicalRecordTypeCode(). ',' ; } elseif( true == array_key_exists( 'LogicalRecordTypeCode', $this->getChangedColumns() ) ) { $strSql .= ' logical_record_type_code = ' . $this->sqlLogicalRecordTypeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount(). ',' ; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_id = ' . $this->sqlInstitutionId(). ',' ; } elseif( true == array_key_exists( 'InstitutionId', $this->getChangedColumns() ) ) { $strSql .= ' institution_id = ' . $this->sqlInstitutionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number_encrypted = ' . $this->sqlAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'AccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' account_number_encrypted = ' . $this->sqlAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_on_account = ' . $this->sqlNameOnAccount(). ',' ; } elseif( true == array_key_exists( 'NameOnAccount', $this->getChangedColumns() ) ) { $strSql .= ' name_on_account = ' . $this->sqlNameOnAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' identification_number = ' . $this->sqlIdentificationNumber(). ',' ; } elseif( true == array_key_exists( 'IdentificationNumber', $this->getChangedColumns() ) ) { $strSql .= ' identification_number = ' . $this->sqlIdentificationNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn(). ',' ; } elseif( true == array_key_exists( 'ReturnedOn', $this->getChangedColumns() ) ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped(). ',' ; } elseif( true == array_key_exists( 'IsReconPrepped', $this->getChangedColumns() ) ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'aft_file_id' => $this->getAftFileId(),
			'aft_file_batch_id' => $this->getAftFileBatchId(),
			'cid' => $this->getCid(),
			'settlement_distribution_id' => $this->getSettlementDistributionId(),
			'eft_instruction_id' => $this->getEftInstructionId(),
			'logical_record_type_code' => $this->getLogicalRecordTypeCode(),
			'amount' => $this->getAmount(),
			'institution_id' => $this->getInstitutionId(),
			'account_number_encrypted' => $this->getAccountNumberEncrypted(),
			'name_on_account' => $this->getNameOnAccount(),
			'identification_number' => $this->getIdentificationNumber(),
			'returned_on' => $this->getReturnedOn(),
			'is_recon_prepped' => $this->getIsReconPrepped(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>