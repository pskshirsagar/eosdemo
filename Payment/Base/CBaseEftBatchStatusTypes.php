<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CEftBatchStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseEftBatchStatusTypes extends CEosPluralBase {

	/**
	 * @return CEftBatchStatusType[]
	 */
	public static function fetchEftBatchStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEftBatchStatusType::class, $objDatabase );
	}

	/**
	 * @return CEftBatchStatusType
	 */
	public static function fetchEftBatchStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEftBatchStatusType::class, $objDatabase );
	}

	public static function fetchEftBatchStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'eft_batch_status_types', $objDatabase );
	}

	public static function fetchEftBatchStatusTypeById( $intId, $objDatabase ) {
		return self::fetchEftBatchStatusType( sprintf( 'SELECT * FROM eft_batch_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>