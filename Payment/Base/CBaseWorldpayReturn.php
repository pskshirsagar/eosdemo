<?php

class CBaseWorldpayReturn extends CEosSingularBase {

	const TABLE_NAME = 'public.worldpay_returns';

	protected $m_intId;
	protected $m_intOrderNumber;
	protected $m_strWorldpayCaseId;
	protected $m_intAmount;
	protected $m_strReasonCode;
	protected $m_strReasonDescription;
	protected $m_strPaymentDate;
	protected $m_strReturnDate;
	protected $m_strProcessedOn;
	protected $m_intArPaymentId;
	protected $m_intEftInstructionId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intCid;
	protected $m_intReturnTypeId;
	protected $m_boolRequiresReturnFee;
	protected $m_strCorrectedAccountNumberEncrypted;
	protected $m_strCorrectedRoutingNumber;
	protected $m_strCorrectedTransactionCode;
	protected $m_strCorrectedNameOnAccount;
	protected $m_boolIsNoticeOfChange;
	protected $m_boolIsReconPrepped;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['order_number'] ) && $boolDirectSet ) $this->set( 'm_intOrderNumber', trim( $arrValues['order_number'] ) ); elseif( isset( $arrValues['order_number'] ) ) $this->setOrderNumber( $arrValues['order_number'] );
		if( isset( $arrValues['worldpay_case_id'] ) && $boolDirectSet ) $this->set( 'm_strWorldpayCaseId', trim( $arrValues['worldpay_case_id'] ) ); elseif( isset( $arrValues['worldpay_case_id'] ) ) $this->setWorldpayCaseId( $arrValues['worldpay_case_id'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_intAmount', trim( $arrValues['amount'] ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['reason_code'] ) && $boolDirectSet ) $this->set( 'm_strReasonCode', trim( $arrValues['reason_code'] ) ); elseif( isset( $arrValues['reason_code'] ) ) $this->setReasonCode( $arrValues['reason_code'] );
		if( isset( $arrValues['reason_description'] ) && $boolDirectSet ) $this->set( 'm_strReasonDescription', trim( $arrValues['reason_description'] ) ); elseif( isset( $arrValues['reason_description'] ) ) $this->setReasonDescription( $arrValues['reason_description'] );
		if( isset( $arrValues['payment_date'] ) && $boolDirectSet ) $this->set( 'm_strPaymentDate', trim( $arrValues['payment_date'] ) ); elseif( isset( $arrValues['payment_date'] ) ) $this->setPaymentDate( $arrValues['payment_date'] );
		if( isset( $arrValues['return_date'] ) && $boolDirectSet ) $this->set( 'm_strReturnDate', trim( $arrValues['return_date'] ) ); elseif( isset( $arrValues['return_date'] ) ) $this->setReturnDate( $arrValues['return_date'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['eft_instruction_id'] ) && $boolDirectSet ) $this->set( 'm_intEftInstructionId', trim( $arrValues['eft_instruction_id'] ) ); elseif( isset( $arrValues['eft_instruction_id'] ) ) $this->setEftInstructionId( $arrValues['eft_instruction_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['return_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReturnTypeId', trim( $arrValues['return_type_id'] ) ); elseif( isset( $arrValues['return_type_id'] ) ) $this->setReturnTypeId( $arrValues['return_type_id'] );
		if( isset( $arrValues['requires_return_fee'] ) && $boolDirectSet ) $this->set( 'm_boolRequiresReturnFee', trim( stripcslashes( $arrValues['requires_return_fee'] ) ) ); elseif( isset( $arrValues['requires_return_fee'] ) ) $this->setRequiresReturnFee( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['requires_return_fee'] ) : $arrValues['requires_return_fee'] );
		if( isset( $arrValues['corrected_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCorrectedAccountNumberEncrypted', trim( $arrValues['corrected_account_number_encrypted'] ) ); elseif( isset( $arrValues['corrected_account_number_encrypted'] ) ) $this->setCorrectedAccountNumberEncrypted( $arrValues['corrected_account_number_encrypted'] );
		if( isset( $arrValues['corrected_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strCorrectedRoutingNumber', trim( $arrValues['corrected_routing_number'] ) ); elseif( isset( $arrValues['corrected_routing_number'] ) ) $this->setCorrectedRoutingNumber( $arrValues['corrected_routing_number'] );
		if( isset( $arrValues['corrected_transaction_code'] ) && $boolDirectSet ) $this->set( 'm_strCorrectedTransactionCode', trim( $arrValues['corrected_transaction_code'] ) ); elseif( isset( $arrValues['corrected_transaction_code'] ) ) $this->setCorrectedTransactionCode( $arrValues['corrected_transaction_code'] );
		if( isset( $arrValues['corrected_name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strCorrectedNameOnAccount', trim( $arrValues['corrected_name_on_account'] ) ); elseif( isset( $arrValues['corrected_name_on_account'] ) ) $this->setCorrectedNameOnAccount( $arrValues['corrected_name_on_account'] );
		if( isset( $arrValues['is_notice_of_change'] ) && $boolDirectSet ) $this->set( 'm_boolIsNoticeOfChange', trim( stripcslashes( $arrValues['is_notice_of_change'] ) ) ); elseif( isset( $arrValues['is_notice_of_change'] ) ) $this->setIsNoticeOfChange( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_notice_of_change'] ) : $arrValues['is_notice_of_change'] );
		if( isset( $arrValues['is_recon_prepped'] ) && $boolDirectSet ) $this->set( 'm_boolIsReconPrepped', trim( stripcslashes( $arrValues['is_recon_prepped'] ) ) ); elseif( isset( $arrValues['is_recon_prepped'] ) ) $this->setIsReconPrepped( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_recon_prepped'] ) : $arrValues['is_recon_prepped'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOrderNumber( $intOrderNumber ) {
		$this->set( 'm_intOrderNumber', CStrings::strToIntDef( $intOrderNumber, NULL, false ) );
	}

	public function getOrderNumber() {
		return $this->m_intOrderNumber;
	}

	public function sqlOrderNumber() {
		return ( true == isset( $this->m_intOrderNumber ) ) ? ( string ) $this->m_intOrderNumber : 'NULL';
	}

	public function setWorldpayCaseId( $strWorldpayCaseId ) {
		$this->set( 'm_strWorldpayCaseId', CStrings::strTrimDef( $strWorldpayCaseId, 64, NULL, true ) );
	}

	public function getWorldpayCaseId() {
		return $this->m_strWorldpayCaseId;
	}

	public function sqlWorldpayCaseId() {
		return ( true == isset( $this->m_strWorldpayCaseId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWorldpayCaseId ) : '\'' . addslashes( $this->m_strWorldpayCaseId ) . '\'' ) : 'NULL';
	}

	public function setAmount( $intAmount ) {
		$this->set( 'm_intAmount', CStrings::strToIntDef( $intAmount, NULL, false ) );
	}

	public function getAmount() {
		return $this->m_intAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_intAmount ) ) ? ( string ) $this->m_intAmount : 'NULL';
	}

	public function setReasonCode( $strReasonCode ) {
		$this->set( 'm_strReasonCode', CStrings::strTrimDef( $strReasonCode, 7, NULL, true ) );
	}

	public function getReasonCode() {
		return $this->m_strReasonCode;
	}

	public function sqlReasonCode() {
		return ( true == isset( $this->m_strReasonCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReasonCode ) : '\'' . addslashes( $this->m_strReasonCode ) . '\'' ) : 'NULL';
	}

	public function setReasonDescription( $strReasonDescription ) {
		$this->set( 'm_strReasonDescription', CStrings::strTrimDef( $strReasonDescription, -1, NULL, true ) );
	}

	public function getReasonDescription() {
		return $this->m_strReasonDescription;
	}

	public function sqlReasonDescription() {
		return ( true == isset( $this->m_strReasonDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReasonDescription ) : '\'' . addslashes( $this->m_strReasonDescription ) . '\'' ) : 'NULL';
	}

	public function setPaymentDate( $strPaymentDate ) {
		$this->set( 'm_strPaymentDate', CStrings::strTrimDef( $strPaymentDate, -1, NULL, true ) );
	}

	public function getPaymentDate() {
		return $this->m_strPaymentDate;
	}

	public function sqlPaymentDate() {
		return ( true == isset( $this->m_strPaymentDate ) ) ? '\'' . $this->m_strPaymentDate . '\'' : 'NOW()';
	}

	public function setReturnDate( $strReturnDate ) {
		$this->set( 'm_strReturnDate', CStrings::strTrimDef( $strReturnDate, -1, NULL, true ) );
	}

	public function getReturnDate() {
		return $this->m_strReturnDate;
	}

	public function sqlReturnDate() {
		return ( true == isset( $this->m_strReturnDate ) ) ? '\'' . $this->m_strReturnDate . '\'' : 'NOW()';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setEftInstructionId( $intEftInstructionId ) {
		$this->set( 'm_intEftInstructionId', CStrings::strToIntDef( $intEftInstructionId, NULL, false ) );
	}

	public function getEftInstructionId() {
		return $this->m_intEftInstructionId;
	}

	public function sqlEftInstructionId() {
		return ( true == isset( $this->m_intEftInstructionId ) ) ? ( string ) $this->m_intEftInstructionId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setReturnTypeId( $intReturnTypeId ) {
		$this->set( 'm_intReturnTypeId', CStrings::strToIntDef( $intReturnTypeId, NULL, false ) );
	}

	public function getReturnTypeId() {
		return $this->m_intReturnTypeId;
	}

	public function sqlReturnTypeId() {
		return ( true == isset( $this->m_intReturnTypeId ) ) ? ( string ) $this->m_intReturnTypeId : 'NULL';
	}

	public function setRequiresReturnFee( $boolRequiresReturnFee ) {
		$this->set( 'm_boolRequiresReturnFee', CStrings::strToBool( $boolRequiresReturnFee ) );
	}

	public function getRequiresReturnFee() {
		return $this->m_boolRequiresReturnFee;
	}

	public function sqlRequiresReturnFee() {
		return ( true == isset( $this->m_boolRequiresReturnFee ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequiresReturnFee ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCorrectedAccountNumberEncrypted( $strCorrectedAccountNumberEncrypted ) {
		$this->set( 'm_strCorrectedAccountNumberEncrypted', CStrings::strTrimDef( $strCorrectedAccountNumberEncrypted, 255, NULL, true ) );
	}

	public function getCorrectedAccountNumberEncrypted() {
		return $this->m_strCorrectedAccountNumberEncrypted;
	}

	public function sqlCorrectedAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCorrectedAccountNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCorrectedAccountNumberEncrypted ) : '\'' . addslashes( $this->m_strCorrectedAccountNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setCorrectedRoutingNumber( $strCorrectedRoutingNumber ) {
		$this->set( 'm_strCorrectedRoutingNumber', CStrings::strTrimDef( $strCorrectedRoutingNumber, 10, NULL, true ) );
	}

	public function getCorrectedRoutingNumber() {
		return $this->m_strCorrectedRoutingNumber;
	}

	public function sqlCorrectedRoutingNumber() {
		return ( true == isset( $this->m_strCorrectedRoutingNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCorrectedRoutingNumber ) : '\'' . addslashes( $this->m_strCorrectedRoutingNumber ) . '\'' ) : 'NULL';
	}

	public function setCorrectedTransactionCode( $strCorrectedTransactionCode ) {
		$this->set( 'm_strCorrectedTransactionCode', CStrings::strTrimDef( $strCorrectedTransactionCode, 15, NULL, true ) );
	}

	public function getCorrectedTransactionCode() {
		return $this->m_strCorrectedTransactionCode;
	}

	public function sqlCorrectedTransactionCode() {
		return ( true == isset( $this->m_strCorrectedTransactionCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCorrectedTransactionCode ) : '\'' . addslashes( $this->m_strCorrectedTransactionCode ) . '\'' ) : 'NULL';
	}

	public function setCorrectedNameOnAccount( $strCorrectedNameOnAccount ) {
		$this->set( 'm_strCorrectedNameOnAccount', CStrings::strTrimDef( $strCorrectedNameOnAccount, 200, NULL, true ) );
	}

	public function getCorrectedNameOnAccount() {
		return $this->m_strCorrectedNameOnAccount;
	}

	public function sqlCorrectedNameOnAccount() {
		return ( true == isset( $this->m_strCorrectedNameOnAccount ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCorrectedNameOnAccount ) : '\'' . addslashes( $this->m_strCorrectedNameOnAccount ) . '\'' ) : 'NULL';
	}

	public function setIsNoticeOfChange( $boolIsNoticeOfChange ) {
		$this->set( 'm_boolIsNoticeOfChange', CStrings::strToBool( $boolIsNoticeOfChange ) );
	}

	public function getIsNoticeOfChange() {
		return $this->m_boolIsNoticeOfChange;
	}

	public function sqlIsNoticeOfChange() {
		return ( true == isset( $this->m_boolIsNoticeOfChange ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNoticeOfChange ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsReconPrepped( $boolIsReconPrepped ) {
		$this->set( 'm_boolIsReconPrepped', CStrings::strToBool( $boolIsReconPrepped ) );
	}

	public function getIsReconPrepped() {
		return $this->m_boolIsReconPrepped;
	}

	public function sqlIsReconPrepped() {
		return ( true == isset( $this->m_boolIsReconPrepped ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReconPrepped ? 'true' : 'false' ) . '\'' : 'false';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, order_number, worldpay_case_id, amount, reason_code, reason_description, payment_date, return_date, processed_on, ar_payment_id, eft_instruction_id, updated_by, updated_on, created_by, created_on, cid, return_type_id, requires_return_fee, corrected_account_number_encrypted, corrected_routing_number, corrected_transaction_code, corrected_name_on_account, is_notice_of_change, is_recon_prepped )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlOrderNumber() . ', ' .
						$this->sqlWorldpayCaseId() . ', ' .
						$this->sqlAmount() . ', ' .
						$this->sqlReasonCode() . ', ' .
						$this->sqlReasonDescription() . ', ' .
						$this->sqlPaymentDate() . ', ' .
						$this->sqlReturnDate() . ', ' .
						$this->sqlProcessedOn() . ', ' .
						$this->sqlArPaymentId() . ', ' .
						$this->sqlEftInstructionId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlReturnTypeId() . ', ' .
						$this->sqlRequiresReturnFee() . ', ' .
						$this->sqlCorrectedAccountNumberEncrypted() . ', ' .
						$this->sqlCorrectedRoutingNumber() . ', ' .
						$this->sqlCorrectedTransactionCode() . ', ' .
						$this->sqlCorrectedNameOnAccount() . ', ' .
						$this->sqlIsNoticeOfChange() . ', ' .
						$this->sqlIsReconPrepped() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_number = ' . $this->sqlOrderNumber(). ',' ; } elseif( true == array_key_exists( 'OrderNumber', $this->getChangedColumns() ) ) { $strSql .= ' order_number = ' . $this->sqlOrderNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' worldpay_case_id = ' . $this->sqlWorldpayCaseId(). ',' ; } elseif( true == array_key_exists( 'WorldpayCaseId', $this->getChangedColumns() ) ) { $strSql .= ' worldpay_case_id = ' . $this->sqlWorldpayCaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount(). ',' ; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason_code = ' . $this->sqlReasonCode(). ',' ; } elseif( true == array_key_exists( 'ReasonCode', $this->getChangedColumns() ) ) { $strSql .= ' reason_code = ' . $this->sqlReasonCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason_description = ' . $this->sqlReasonDescription(). ',' ; } elseif( true == array_key_exists( 'ReasonDescription', $this->getChangedColumns() ) ) { $strSql .= ' reason_description = ' . $this->sqlReasonDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_date = ' . $this->sqlPaymentDate(). ',' ; } elseif( true == array_key_exists( 'PaymentDate', $this->getChangedColumns() ) ) { $strSql .= ' payment_date = ' . $this->sqlPaymentDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_date = ' . $this->sqlReturnDate(). ',' ; } elseif( true == array_key_exists( 'ReturnDate', $this->getChangedColumns() ) ) { $strSql .= ' return_date = ' . $this->sqlReturnDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn(). ',' ; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId(). ',' ; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eft_instruction_id = ' . $this->sqlEftInstructionId(). ',' ; } elseif( true == array_key_exists( 'EftInstructionId', $this->getChangedColumns() ) ) { $strSql .= ' eft_instruction_id = ' . $this->sqlEftInstructionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId(). ',' ; } elseif( true == array_key_exists( 'ReturnTypeId', $this->getChangedColumns() ) ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_return_fee = ' . $this->sqlRequiresReturnFee(). ',' ; } elseif( true == array_key_exists( 'RequiresReturnFee', $this->getChangedColumns() ) ) { $strSql .= ' requires_return_fee = ' . $this->sqlRequiresReturnFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' corrected_account_number_encrypted = ' . $this->sqlCorrectedAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'CorrectedAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' corrected_account_number_encrypted = ' . $this->sqlCorrectedAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' corrected_routing_number = ' . $this->sqlCorrectedRoutingNumber(). ',' ; } elseif( true == array_key_exists( 'CorrectedRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' corrected_routing_number = ' . $this->sqlCorrectedRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' corrected_transaction_code = ' . $this->sqlCorrectedTransactionCode(). ',' ; } elseif( true == array_key_exists( 'CorrectedTransactionCode', $this->getChangedColumns() ) ) { $strSql .= ' corrected_transaction_code = ' . $this->sqlCorrectedTransactionCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' corrected_name_on_account = ' . $this->sqlCorrectedNameOnAccount(). ',' ; } elseif( true == array_key_exists( 'CorrectedNameOnAccount', $this->getChangedColumns() ) ) { $strSql .= ' corrected_name_on_account = ' . $this->sqlCorrectedNameOnAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_notice_of_change = ' . $this->sqlIsNoticeOfChange(). ',' ; } elseif( true == array_key_exists( 'IsNoticeOfChange', $this->getChangedColumns() ) ) { $strSql .= ' is_notice_of_change = ' . $this->sqlIsNoticeOfChange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped(). ',' ; } elseif( true == array_key_exists( 'IsReconPrepped', $this->getChangedColumns() ) ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'order_number' => $this->getOrderNumber(),
			'worldpay_case_id' => $this->getWorldpayCaseId(),
			'amount' => $this->getAmount(),
			'reason_code' => $this->getReasonCode(),
			'reason_description' => $this->getReasonDescription(),
			'payment_date' => $this->getPaymentDate(),
			'return_date' => $this->getReturnDate(),
			'processed_on' => $this->getProcessedOn(),
			'ar_payment_id' => $this->getArPaymentId(),
			'eft_instruction_id' => $this->getEftInstructionId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'cid' => $this->getCid(),
			'return_type_id' => $this->getReturnTypeId(),
			'requires_return_fee' => $this->getRequiresReturnFee(),
			'corrected_account_number_encrypted' => $this->getCorrectedAccountNumberEncrypted(),
			'corrected_routing_number' => $this->getCorrectedRoutingNumber(),
			'corrected_transaction_code' => $this->getCorrectedTransactionCode(),
			'corrected_name_on_account' => $this->getCorrectedNameOnAccount(),
			'is_notice_of_change' => $this->getIsNoticeOfChange(),
			'is_recon_prepped' => $this->getIsReconPrepped()
		);
	}

}
?>