<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CBatchedArPayments
 * Do not add any new functions to this class.
 */

class CBaseBatchedArPayments extends CEosPluralBase {

	/**
	 * @return CBatchedArPayment[]
	 */
	public static function fetchBatchedArPayments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBatchedArPayment::class, $objDatabase );
	}

	/**
	 * @return CBatchedArPayment
	 */
	public static function fetchBatchedArPayment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBatchedArPayment::class, $objDatabase );
	}

	public static function fetchBatchedArPaymentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'batched_ar_payments', $objDatabase );
	}

	public static function fetchBatchedArPaymentById( $intId, $objDatabase ) {
		return self::fetchBatchedArPayment( sprintf( 'SELECT * FROM batched_ar_payments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchBatchedArPaymentsByCid( $intCid, $objDatabase ) {
		return self::fetchBatchedArPayments( sprintf( 'SELECT * FROM batched_ar_payments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBatchedArPaymentsByEftBatchId( $intEftBatchId, $objDatabase ) {
		return self::fetchBatchedArPayments( sprintf( 'SELECT * FROM batched_ar_payments WHERE eft_batch_id = %d', ( int ) $intEftBatchId ), $objDatabase );
	}

	public static function fetchBatchedArPaymentsByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchBatchedArPayments( sprintf( 'SELECT * FROM batched_ar_payments WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

}
?>