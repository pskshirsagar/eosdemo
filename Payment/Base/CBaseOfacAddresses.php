<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\COfacAddresses
 * Do not add any new functions to this class.
 */

class CBaseOfacAddresses extends CEosPluralBase {

	/**
	 * @return COfacAddress[]
	 */
	public static function fetchOfacAddresses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, COfacAddress::class, $objDatabase );
	}

	/**
	 * @return COfacAddress
	 */
	public static function fetchOfacAddress( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COfacAddress::class, $objDatabase );
	}

	public static function fetchOfacAddressCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ofac_addresses', $objDatabase );
	}

	public static function fetchOfacAddressById( $intId, $objDatabase ) {
		return self::fetchOfacAddress( sprintf( 'SELECT * FROM ofac_addresses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchOfacAddressesByOfacId( $intOfacId, $objDatabase ) {
		return self::fetchOfacAddresses( sprintf( 'SELECT * FROM ofac_addresses WHERE ofac_id = %d', ( int ) $intOfacId ), $objDatabase );
	}

}
?>