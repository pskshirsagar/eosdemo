<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CProcessingBankAccounts
 * Do not add any new functions to this class.
 */

class CBaseProcessingBankAccounts extends CEosPluralBase {

	/**
	 * @return CProcessingBankAccount[]
	 */
	public static function fetchProcessingBankAccounts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CProcessingBankAccount::class, $objDatabase );
	}

	/**
	 * @return CProcessingBankAccount
	 */
	public static function fetchProcessingBankAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CProcessingBankAccount::class, $objDatabase );
	}

	public static function fetchProcessingBankAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'processing_bank_accounts', $objDatabase );
	}

	public static function fetchProcessingBankAccountById( $intId, $objDatabase ) {
		return self::fetchProcessingBankAccount( sprintf( 'SELECT * FROM processing_bank_accounts WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchProcessingBankAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchProcessingBankAccounts( sprintf( 'SELECT * FROM processing_bank_accounts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchProcessingBankAccountsByProcessingBankId( $intProcessingBankId, $objDatabase ) {
		return self::fetchProcessingBankAccounts( sprintf( 'SELECT * FROM processing_bank_accounts WHERE processing_bank_id = %d', ( int ) $intProcessingBankId ), $objDatabase );
	}

	public static function fetchProcessingBankAccountsByFundMovementTypeId( $intFundMovementTypeId, $objDatabase ) {
		return self::fetchProcessingBankAccounts( sprintf( 'SELECT * FROM processing_bank_accounts WHERE fund_movement_type_id = %d', ( int ) $intFundMovementTypeId ), $objDatabase );
	}

}
?>