<?php

class CBaseMerchantChangeRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.merchant_change_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intSignatureDocumentId;
	protected $m_strRequestorName;
	protected $m_strRequestorEmailAddress;
	protected $m_strRequestorPhoneNumber;
	protected $m_strRequestDescription;
	protected $m_strRequestDatetime;
	protected $m_strFormReceivedOn;
	protected $m_strSignatureFirstName;
	protected $m_strSignatureLastName;
	protected $m_strSignatureTitle;
	protected $m_strSignatureVerifiedOn;
	protected $m_strConfirmedOn;
	protected $m_strProcessedOn;
	protected $m_intIsCloseAccountRequest;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsCloseAccountRequest = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['signature_document_id'] ) && $boolDirectSet ) $this->set( 'm_intSignatureDocumentId', trim( $arrValues['signature_document_id'] ) ); elseif( isset( $arrValues['signature_document_id'] ) ) $this->setSignatureDocumentId( $arrValues['signature_document_id'] );
		if( isset( $arrValues['requestor_name'] ) && $boolDirectSet ) $this->set( 'm_strRequestorName', trim( stripcslashes( $arrValues['requestor_name'] ) ) ); elseif( isset( $arrValues['requestor_name'] ) ) $this->setRequestorName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['requestor_name'] ) : $arrValues['requestor_name'] );
		if( isset( $arrValues['requestor_email_address'] ) && $boolDirectSet ) $this->set( 'm_strRequestorEmailAddress', trim( stripcslashes( $arrValues['requestor_email_address'] ) ) ); elseif( isset( $arrValues['requestor_email_address'] ) ) $this->setRequestorEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['requestor_email_address'] ) : $arrValues['requestor_email_address'] );
		if( isset( $arrValues['requestor_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strRequestorPhoneNumber', trim( stripcslashes( $arrValues['requestor_phone_number'] ) ) ); elseif( isset( $arrValues['requestor_phone_number'] ) ) $this->setRequestorPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['requestor_phone_number'] ) : $arrValues['requestor_phone_number'] );
		if( isset( $arrValues['request_description'] ) && $boolDirectSet ) $this->set( 'm_strRequestDescription', trim( stripcslashes( $arrValues['request_description'] ) ) ); elseif( isset( $arrValues['request_description'] ) ) $this->setRequestDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['request_description'] ) : $arrValues['request_description'] );
		if( isset( $arrValues['request_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRequestDatetime', trim( $arrValues['request_datetime'] ) ); elseif( isset( $arrValues['request_datetime'] ) ) $this->setRequestDatetime( $arrValues['request_datetime'] );
		if( isset( $arrValues['form_received_on'] ) && $boolDirectSet ) $this->set( 'm_strFormReceivedOn', trim( $arrValues['form_received_on'] ) ); elseif( isset( $arrValues['form_received_on'] ) ) $this->setFormReceivedOn( $arrValues['form_received_on'] );
		if( isset( $arrValues['signature_first_name'] ) && $boolDirectSet ) $this->set( 'm_strSignatureFirstName', trim( stripcslashes( $arrValues['signature_first_name'] ) ) ); elseif( isset( $arrValues['signature_first_name'] ) ) $this->setSignatureFirstName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['signature_first_name'] ) : $arrValues['signature_first_name'] );
		if( isset( $arrValues['signature_last_name'] ) && $boolDirectSet ) $this->set( 'm_strSignatureLastName', trim( stripcslashes( $arrValues['signature_last_name'] ) ) ); elseif( isset( $arrValues['signature_last_name'] ) ) $this->setSignatureLastName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['signature_last_name'] ) : $arrValues['signature_last_name'] );
		if( isset( $arrValues['signature_title'] ) && $boolDirectSet ) $this->set( 'm_strSignatureTitle', trim( stripcslashes( $arrValues['signature_title'] ) ) ); elseif( isset( $arrValues['signature_title'] ) ) $this->setSignatureTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['signature_title'] ) : $arrValues['signature_title'] );
		if( isset( $arrValues['signature_verified_on'] ) && $boolDirectSet ) $this->set( 'm_strSignatureVerifiedOn', trim( $arrValues['signature_verified_on'] ) ); elseif( isset( $arrValues['signature_verified_on'] ) ) $this->setSignatureVerifiedOn( $arrValues['signature_verified_on'] );
		if( isset( $arrValues['confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strConfirmedOn', trim( $arrValues['confirmed_on'] ) ); elseif( isset( $arrValues['confirmed_on'] ) ) $this->setConfirmedOn( $arrValues['confirmed_on'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['is_close_account_request'] ) && $boolDirectSet ) $this->set( 'm_intIsCloseAccountRequest', trim( $arrValues['is_close_account_request'] ) ); elseif( isset( $arrValues['is_close_account_request'] ) ) $this->setIsCloseAccountRequest( $arrValues['is_close_account_request'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setSignatureDocumentId( $intSignatureDocumentId ) {
		$this->set( 'm_intSignatureDocumentId', CStrings::strToIntDef( $intSignatureDocumentId, NULL, false ) );
	}

	public function getSignatureDocumentId() {
		return $this->m_intSignatureDocumentId;
	}

	public function sqlSignatureDocumentId() {
		return ( true == isset( $this->m_intSignatureDocumentId ) ) ? ( string ) $this->m_intSignatureDocumentId : 'NULL';
	}

	public function setRequestorName( $strRequestorName ) {
		$this->set( 'm_strRequestorName', CStrings::strTrimDef( $strRequestorName, 50, NULL, true ) );
	}

	public function getRequestorName() {
		return $this->m_strRequestorName;
	}

	public function sqlRequestorName() {
		return ( true == isset( $this->m_strRequestorName ) ) ? '\'' . addslashes( $this->m_strRequestorName ) . '\'' : 'NULL';
	}

	public function setRequestorEmailAddress( $strRequestorEmailAddress ) {
		$this->set( 'm_strRequestorEmailAddress', CStrings::strTrimDef( $strRequestorEmailAddress, 240, NULL, true ) );
	}

	public function getRequestorEmailAddress() {
		return $this->m_strRequestorEmailAddress;
	}

	public function sqlRequestorEmailAddress() {
		return ( true == isset( $this->m_strRequestorEmailAddress ) ) ? '\'' . addslashes( $this->m_strRequestorEmailAddress ) . '\'' : 'NULL';
	}

	public function setRequestorPhoneNumber( $strRequestorPhoneNumber ) {
		$this->set( 'm_strRequestorPhoneNumber', CStrings::strTrimDef( $strRequestorPhoneNumber, 30, NULL, true ) );
	}

	public function getRequestorPhoneNumber() {
		return $this->m_strRequestorPhoneNumber;
	}

	public function sqlRequestorPhoneNumber() {
		return ( true == isset( $this->m_strRequestorPhoneNumber ) ) ? '\'' . addslashes( $this->m_strRequestorPhoneNumber ) . '\'' : 'NULL';
	}

	public function setRequestDescription( $strRequestDescription ) {
		$this->set( 'm_strRequestDescription', CStrings::strTrimDef( $strRequestDescription, 2000, NULL, true ) );
	}

	public function getRequestDescription() {
		return $this->m_strRequestDescription;
	}

	public function sqlRequestDescription() {
		return ( true == isset( $this->m_strRequestDescription ) ) ? '\'' . addslashes( $this->m_strRequestDescription ) . '\'' : 'NULL';
	}

	public function setRequestDatetime( $strRequestDatetime ) {
		$this->set( 'm_strRequestDatetime', CStrings::strTrimDef( $strRequestDatetime, -1, NULL, true ) );
	}

	public function getRequestDatetime() {
		return $this->m_strRequestDatetime;
	}

	public function sqlRequestDatetime() {
		return ( true == isset( $this->m_strRequestDatetime ) ) ? '\'' . $this->m_strRequestDatetime . '\'' : 'NOW()';
	}

	public function setFormReceivedOn( $strFormReceivedOn ) {
		$this->set( 'm_strFormReceivedOn', CStrings::strTrimDef( $strFormReceivedOn, -1, NULL, true ) );
	}

	public function getFormReceivedOn() {
		return $this->m_strFormReceivedOn;
	}

	public function sqlFormReceivedOn() {
		return ( true == isset( $this->m_strFormReceivedOn ) ) ? '\'' . $this->m_strFormReceivedOn . '\'' : 'NULL';
	}

	public function setSignatureFirstName( $strSignatureFirstName ) {
		$this->set( 'm_strSignatureFirstName', CStrings::strTrimDef( $strSignatureFirstName, 50, NULL, true ) );
	}

	public function getSignatureFirstName() {
		return $this->m_strSignatureFirstName;
	}

	public function sqlSignatureFirstName() {
		return ( true == isset( $this->m_strSignatureFirstName ) ) ? '\'' . addslashes( $this->m_strSignatureFirstName ) . '\'' : 'NULL';
	}

	public function setSignatureLastName( $strSignatureLastName ) {
		$this->set( 'm_strSignatureLastName', CStrings::strTrimDef( $strSignatureLastName, 50, NULL, true ) );
	}

	public function getSignatureLastName() {
		return $this->m_strSignatureLastName;
	}

	public function sqlSignatureLastName() {
		return ( true == isset( $this->m_strSignatureLastName ) ) ? '\'' . addslashes( $this->m_strSignatureLastName ) . '\'' : 'NULL';
	}

	public function setSignatureTitle( $strSignatureTitle ) {
		$this->set( 'm_strSignatureTitle', CStrings::strTrimDef( $strSignatureTitle, 50, NULL, true ) );
	}

	public function getSignatureTitle() {
		return $this->m_strSignatureTitle;
	}

	public function sqlSignatureTitle() {
		return ( true == isset( $this->m_strSignatureTitle ) ) ? '\'' . addslashes( $this->m_strSignatureTitle ) . '\'' : 'NULL';
	}

	public function setSignatureVerifiedOn( $strSignatureVerifiedOn ) {
		$this->set( 'm_strSignatureVerifiedOn', CStrings::strTrimDef( $strSignatureVerifiedOn, -1, NULL, true ) );
	}

	public function getSignatureVerifiedOn() {
		return $this->m_strSignatureVerifiedOn;
	}

	public function sqlSignatureVerifiedOn() {
		return ( true == isset( $this->m_strSignatureVerifiedOn ) ) ? '\'' . $this->m_strSignatureVerifiedOn . '\'' : 'NULL';
	}

	public function setConfirmedOn( $strConfirmedOn ) {
		$this->set( 'm_strConfirmedOn', CStrings::strTrimDef( $strConfirmedOn, -1, NULL, true ) );
	}

	public function getConfirmedOn() {
		return $this->m_strConfirmedOn;
	}

	public function sqlConfirmedOn() {
		return ( true == isset( $this->m_strConfirmedOn ) ) ? '\'' . $this->m_strConfirmedOn . '\'' : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setIsCloseAccountRequest( $intIsCloseAccountRequest ) {
		$this->set( 'm_intIsCloseAccountRequest', CStrings::strToIntDef( $intIsCloseAccountRequest, NULL, false ) );
	}

	public function getIsCloseAccountRequest() {
		return $this->m_intIsCloseAccountRequest;
	}

	public function sqlIsCloseAccountRequest() {
		return ( true == isset( $this->m_intIsCloseAccountRequest ) ) ? ( string ) $this->m_intIsCloseAccountRequest : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, signature_document_id, requestor_name, requestor_email_address, requestor_phone_number, request_description, request_datetime, form_received_on, signature_first_name, signature_last_name, signature_title, signature_verified_on, confirmed_on, processed_on, is_close_account_request, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlSignatureDocumentId() . ', ' .
 						$this->sqlRequestorName() . ', ' .
 						$this->sqlRequestorEmailAddress() . ', ' .
 						$this->sqlRequestorPhoneNumber() . ', ' .
 						$this->sqlRequestDescription() . ', ' .
 						$this->sqlRequestDatetime() . ', ' .
 						$this->sqlFormReceivedOn() . ', ' .
 						$this->sqlSignatureFirstName() . ', ' .
 						$this->sqlSignatureLastName() . ', ' .
 						$this->sqlSignatureTitle() . ', ' .
 						$this->sqlSignatureVerifiedOn() . ', ' .
 						$this->sqlConfirmedOn() . ', ' .
 						$this->sqlProcessedOn() . ', ' .
 						$this->sqlIsCloseAccountRequest() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature_document_id = ' . $this->sqlSignatureDocumentId() . ','; } elseif( true == array_key_exists( 'SignatureDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' signature_document_id = ' . $this->sqlSignatureDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requestor_name = ' . $this->sqlRequestorName() . ','; } elseif( true == array_key_exists( 'RequestorName', $this->getChangedColumns() ) ) { $strSql .= ' requestor_name = ' . $this->sqlRequestorName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requestor_email_address = ' . $this->sqlRequestorEmailAddress() . ','; } elseif( true == array_key_exists( 'RequestorEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' requestor_email_address = ' . $this->sqlRequestorEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requestor_phone_number = ' . $this->sqlRequestorPhoneNumber() . ','; } elseif( true == array_key_exists( 'RequestorPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' requestor_phone_number = ' . $this->sqlRequestorPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_description = ' . $this->sqlRequestDescription() . ','; } elseif( true == array_key_exists( 'RequestDescription', $this->getChangedColumns() ) ) { $strSql .= ' request_description = ' . $this->sqlRequestDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime() . ','; } elseif( true == array_key_exists( 'RequestDatetime', $this->getChangedColumns() ) ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' form_received_on = ' . $this->sqlFormReceivedOn() . ','; } elseif( true == array_key_exists( 'FormReceivedOn', $this->getChangedColumns() ) ) { $strSql .= ' form_received_on = ' . $this->sqlFormReceivedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature_first_name = ' . $this->sqlSignatureFirstName() . ','; } elseif( true == array_key_exists( 'SignatureFirstName', $this->getChangedColumns() ) ) { $strSql .= ' signature_first_name = ' . $this->sqlSignatureFirstName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature_last_name = ' . $this->sqlSignatureLastName() . ','; } elseif( true == array_key_exists( 'SignatureLastName', $this->getChangedColumns() ) ) { $strSql .= ' signature_last_name = ' . $this->sqlSignatureLastName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature_title = ' . $this->sqlSignatureTitle() . ','; } elseif( true == array_key_exists( 'SignatureTitle', $this->getChangedColumns() ) ) { $strSql .= ' signature_title = ' . $this->sqlSignatureTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature_verified_on = ' . $this->sqlSignatureVerifiedOn() . ','; } elseif( true == array_key_exists( 'SignatureVerifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' signature_verified_on = ' . $this->sqlSignatureVerifiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; } elseif( true == array_key_exists( 'ConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_close_account_request = ' . $this->sqlIsCloseAccountRequest() . ','; } elseif( true == array_key_exists( 'IsCloseAccountRequest', $this->getChangedColumns() ) ) { $strSql .= ' is_close_account_request = ' . $this->sqlIsCloseAccountRequest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'signature_document_id' => $this->getSignatureDocumentId(),
			'requestor_name' => $this->getRequestorName(),
			'requestor_email_address' => $this->getRequestorEmailAddress(),
			'requestor_phone_number' => $this->getRequestorPhoneNumber(),
			'request_description' => $this->getRequestDescription(),
			'request_datetime' => $this->getRequestDatetime(),
			'form_received_on' => $this->getFormReceivedOn(),
			'signature_first_name' => $this->getSignatureFirstName(),
			'signature_last_name' => $this->getSignatureLastName(),
			'signature_title' => $this->getSignatureTitle(),
			'signature_verified_on' => $this->getSignatureVerifiedOn(),
			'confirmed_on' => $this->getConfirmedOn(),
			'processed_on' => $this->getProcessedOn(),
			'is_close_account_request' => $this->getIsCloseAccountRequest(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>