<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMerchantChangeRequestAccounts
 * Do not add any new functions to this class.
 */

class CBaseMerchantChangeRequestAccounts extends CEosPluralBase {

	/**
	 * @return CMerchantChangeRequestAccount[]
	 */
	public static function fetchMerchantChangeRequestAccounts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMerchantChangeRequestAccount::class, $objDatabase );
	}

	/**
	 * @return CMerchantChangeRequestAccount
	 */
	public static function fetchMerchantChangeRequestAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMerchantChangeRequestAccount::class, $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'merchant_change_request_accounts', $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountById( $intId, $objDatabase ) {
		return self::fetchMerchantChangeRequestAccount( sprintf( 'SELECT * FROM merchant_change_request_accounts WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchMerchantChangeRequestAccounts( sprintf( 'SELECT * FROM merchant_change_request_accounts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountsByMerchantChangeRequestId( $intMerchantChangeRequestId, $objDatabase ) {
		return self::fetchMerchantChangeRequestAccounts( sprintf( 'SELECT * FROM merchant_change_request_accounts WHERE merchant_change_request_id = %d', ( int ) $intMerchantChangeRequestId ), $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountsByCompanyMerchantAccountId( $intCompanyMerchantAccountId, $objDatabase ) {
		return self::fetchMerchantChangeRequestAccounts( sprintf( 'SELECT * FROM merchant_change_request_accounts WHERE company_merchant_account_id = %d', ( int ) $intCompanyMerchantAccountId ), $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountsByMerchantChangeRequestAccountTypeId( $intMerchantChangeRequestAccountTypeId, $objDatabase ) {
		return self::fetchMerchantChangeRequestAccounts( sprintf( 'SELECT * FROM merchant_change_request_accounts WHERE merchant_change_request_account_type_id = %d', ( int ) $intMerchantChangeRequestAccountTypeId ), $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountsByMerchantProcessingTypeId( $intMerchantProcessingTypeId, $objDatabase ) {
		return self::fetchMerchantChangeRequestAccounts( sprintf( 'SELECT * FROM merchant_change_request_accounts WHERE merchant_processing_type_id = %d', ( int ) $intMerchantProcessingTypeId ), $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountsByMerchantChangeRequestAccountId( $intMerchantChangeRequestAccountId, $objDatabase ) {
		return self::fetchMerchantChangeRequestAccounts( sprintf( 'SELECT * FROM merchant_change_request_accounts WHERE merchant_change_request_account_id = %d', ( int ) $intMerchantChangeRequestAccountId ), $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountsByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchMerchantChangeRequestAccounts( sprintf( 'SELECT * FROM merchant_change_request_accounts WHERE account_id = %d', ( int ) $intAccountId ), $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountsByBillingAccountId( $intBillingAccountId, $objDatabase ) {
		return self::fetchMerchantChangeRequestAccounts( sprintf( 'SELECT * FROM merchant_change_request_accounts WHERE billing_account_id = %d', ( int ) $intBillingAccountId ), $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountsByReturnAccountId( $intReturnAccountId, $objDatabase ) {
		return self::fetchMerchantChangeRequestAccounts( sprintf( 'SELECT * FROM merchant_change_request_accounts WHERE return_account_id = %d', ( int ) $intReturnAccountId ), $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountsByCheckAccountTypeId( $intCheckAccountTypeId, $objDatabase ) {
		return self::fetchMerchantChangeRequestAccounts( sprintf( 'SELECT * FROM merchant_change_request_accounts WHERE check_account_type_id = %d', ( int ) $intCheckAccountTypeId ), $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountsByBillingAccountTypeId( $intBillingAccountTypeId, $objDatabase ) {
		return self::fetchMerchantChangeRequestAccounts( sprintf( 'SELECT * FROM merchant_change_request_accounts WHERE billing_account_type_id = %d', ( int ) $intBillingAccountTypeId ), $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountsByReturnAccountTypeId( $intReturnAccountTypeId, $objDatabase ) {
		return self::fetchMerchantChangeRequestAccounts( sprintf( 'SELECT * FROM merchant_change_request_accounts WHERE return_account_type_id = %d', ( int ) $intReturnAccountTypeId ), $objDatabase );
	}

}
?>