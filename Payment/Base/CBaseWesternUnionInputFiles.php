<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CWesternUnionInputFiles
 * Do not add any new functions to this class.
 */

class CBaseWesternUnionInputFiles extends CEosPluralBase {

	/**
	 * @return CWesternUnionInputFile[]
	 */
	public static function fetchWesternUnionInputFiles( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CWesternUnionInputFile::class, $objDatabase );
	}

	/**
	 * @return CWesternUnionInputFile
	 */
	public static function fetchWesternUnionInputFile( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CWesternUnionInputFile::class, $objDatabase );
	}

	public static function fetchWesternUnionInputFileCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'western_union_input_files', $objDatabase );
	}

	public static function fetchWesternUnionInputFileById( $intId, $objDatabase ) {
		return self::fetchWesternUnionInputFile( sprintf( 'SELECT * FROM western_union_input_files WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchWesternUnionInputFilesByFileTypeId( $intFileTypeId, $objDatabase ) {
		return self::fetchWesternUnionInputFiles( sprintf( 'SELECT * FROM western_union_input_files WHERE file_type_id = %d', ( int ) $intFileTypeId ), $objDatabase );
	}

	public static function fetchWesternUnionInputFilesByHeaderClientId( $strHeaderClientId, $objDatabase ) {
		return self::fetchWesternUnionInputFiles( sprintf( 'SELECT * FROM western_union_input_files WHERE header_client_id = \'%s\'', $strHeaderClientId ), $objDatabase );
	}

}
?>