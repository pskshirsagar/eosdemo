<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMerchantAccountApplications
 * Do not add any new functions to this class.
 */

class CBaseMerchantAccountApplications extends CEosPluralBase {

	/**
	 * @return CMerchantAccountApplication[]
	 */
	public static function fetchMerchantAccountApplications( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMerchantAccountApplication::class, $objDatabase );
	}

	/**
	 * @return CMerchantAccountApplication
	 */
	public static function fetchMerchantAccountApplication( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMerchantAccountApplication::class, $objDatabase );
	}

	public static function fetchMerchantAccountApplicationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'merchant_account_applications', $objDatabase );
	}

	public static function fetchMerchantAccountApplicationById( $intId, $objDatabase ) {
		return self::fetchMerchantAccountApplication( sprintf( 'SELECT * FROM merchant_account_applications WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMerchantAccountApplicationsByCid( $intCid, $objDatabase ) {
		return self::fetchMerchantAccountApplications( sprintf( 'SELECT * FROM merchant_account_applications WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountApplicationsByMerchantAccountApplicationTypesId( $intMerchantAccountApplicationTypesId, $objDatabase ) {
		return self::fetchMerchantAccountApplications( sprintf( 'SELECT * FROM merchant_account_applications WHERE merchant_account_application_types_id = %d', ( int ) $intMerchantAccountApplicationTypesId ), $objDatabase );
	}

	public static function fetchMerchantAccountApplicationsByOwnershipTypeId( $intOwnershipTypeId, $objDatabase ) {
		return self::fetchMerchantAccountApplications( sprintf( 'SELECT * FROM merchant_account_applications WHERE ownership_type_id = %d', ( int ) $intOwnershipTypeId ), $objDatabase );
	}

}
?>