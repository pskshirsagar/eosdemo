<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CBeneficialOwners
 * Do not add any new functions to this class.
 */

class CBaseBeneficialOwners extends CEosPluralBase {

	/**
	 * @return CBeneficialOwner[]
	 */
	public static function fetchBeneficialOwners( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBeneficialOwner::class, $objDatabase );
	}

	/**
	 * @return CBeneficialOwner
	 */
	public static function fetchBeneficialOwner( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBeneficialOwner::class, $objDatabase );
	}

	public static function fetchBeneficialOwnerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'beneficial_owners', $objDatabase );
	}

	public static function fetchBeneficialOwnerById( $intId, $objDatabase ) {
		return self::fetchBeneficialOwner( sprintf( 'SELECT * FROM beneficial_owners WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchBeneficialOwnersByCid( $intCid, $objDatabase ) {
		return self::fetchBeneficialOwners( sprintf( 'SELECT * FROM beneficial_owners WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBeneficialOwnersByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchBeneficialOwners( sprintf( 'SELECT * FROM beneficial_owners WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchBeneficialOwnersByUnderwritingTypeId( $intUnderwritingTypeId, $objDatabase ) {
		return self::fetchBeneficialOwners( sprintf( 'SELECT * FROM beneficial_owners WHERE underwriting_type_id = %d', ( int ) $intUnderwritingTypeId ), $objDatabase );
	}

}
?>