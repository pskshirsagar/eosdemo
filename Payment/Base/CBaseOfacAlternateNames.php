<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\COfacAlternateNames
 * Do not add any new functions to this class.
 */

class CBaseOfacAlternateNames extends CEosPluralBase {

	/**
	 * @return COfacAlternateName[]
	 */
	public static function fetchOfacAlternateNames( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, COfacAlternateName::class, $objDatabase );
	}

	/**
	 * @return COfacAlternateName
	 */
	public static function fetchOfacAlternateName( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COfacAlternateName::class, $objDatabase );
	}

	public static function fetchOfacAlternateNameCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ofac_alternate_names', $objDatabase );
	}

	public static function fetchOfacAlternateNameById( $intId, $objDatabase ) {
		return self::fetchOfacAlternateName( sprintf( 'SELECT * FROM ofac_alternate_names WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchOfacAlternateNamesByOfacId( $intOfacId, $objDatabase ) {
		return self::fetchOfacAlternateNames( sprintf( 'SELECT * FROM ofac_alternate_names WHERE ofac_id = %d', ( int ) $intOfacId ), $objDatabase );
	}

}
?>