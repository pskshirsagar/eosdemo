<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaReturnAddendaDetailRecords
 * Do not add any new functions to this class.
 */

class CBaseNachaReturnAddendaDetailRecords extends CEosPluralBase {

	/**
	 * @return CNachaReturnAddendaDetailRecord[]
	 */
	public static function fetchNachaReturnAddendaDetailRecords( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CNachaReturnAddendaDetailRecord::class, $objDatabase );
	}

	/**
	 * @return CNachaReturnAddendaDetailRecord
	 */
	public static function fetchNachaReturnAddendaDetailRecord( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CNachaReturnAddendaDetailRecord::class, $objDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'nacha_return_addenda_detail_records', $objDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordById( $intId, $objDatabase ) {
		return self::fetchNachaReturnAddendaDetailRecord( sprintf( 'SELECT * FROM nacha_return_addenda_detail_records WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsByNachaReturnDetailRecordId( $intNachaReturnDetailRecordId, $objDatabase ) {
		return self::fetchNachaReturnAddendaDetailRecords( sprintf( 'SELECT * FROM nacha_return_addenda_detail_records WHERE nacha_return_detail_record_id = %d', ( int ) $intNachaReturnDetailRecordId ), $objDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsByEntryDetailRecordTypeId( $intEntryDetailRecordTypeId, $objDatabase ) {
		return self::fetchNachaReturnAddendaDetailRecords( sprintf( 'SELECT * FROM nacha_return_addenda_detail_records WHERE entry_detail_record_type_id = %d', ( int ) $intEntryDetailRecordTypeId ), $objDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsByNachaFileId( $intNachaFileId, $objDatabase ) {
		return self::fetchNachaReturnAddendaDetailRecords( sprintf( 'SELECT * FROM nacha_return_addenda_detail_records WHERE nacha_file_id = %d', ( int ) $intNachaFileId ), $objDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsByNachaFileBatchId( $intNachaFileBatchId, $objDatabase ) {
		return self::fetchNachaReturnAddendaDetailRecords( sprintf( 'SELECT * FROM nacha_return_addenda_detail_records WHERE nacha_file_batch_id = %d', ( int ) $intNachaFileBatchId ), $objDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsByClearingBatchId( $intClearingBatchId, $objDatabase ) {
		return self::fetchNachaReturnAddendaDetailRecords( sprintf( 'SELECT * FROM nacha_return_addenda_detail_records WHERE clearing_batch_id = %d', ( int ) $intClearingBatchId ), $objDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsByNachaEntryDetailRecordId( $intNachaEntryDetailRecordId, $objDatabase ) {
		return self::fetchNachaReturnAddendaDetailRecords( sprintf( 'SELECT * FROM nacha_return_addenda_detail_records WHERE nacha_entry_detail_record_id = %d', ( int ) $intNachaEntryDetailRecordId ), $objDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsByReturnTypeId( $intReturnTypeId, $objDatabase ) {
		return self::fetchNachaReturnAddendaDetailRecords( sprintf( 'SELECT * FROM nacha_return_addenda_detail_records WHERE return_type_id = %d', ( int ) $intReturnTypeId ), $objDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsByCid( $intCid, $objDatabase ) {
		return self::fetchNachaReturnAddendaDetailRecords( sprintf( 'SELECT * FROM nacha_return_addenda_detail_records WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchNachaReturnAddendaDetailRecords( sprintf( 'SELECT * FROM nacha_return_addenda_detail_records WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsBySettlementDistributionId( $intSettlementDistributionId, $objDatabase ) {
		return self::fetchNachaReturnAddendaDetailRecords( sprintf( 'SELECT * FROM nacha_return_addenda_detail_records WHERE settlement_distribution_id = %d', ( int ) $intSettlementDistributionId ), $objDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsByCompanyPaymentId( $intCompanyPaymentId, $objDatabase ) {
		return self::fetchNachaReturnAddendaDetailRecords( sprintf( 'SELECT * FROM nacha_return_addenda_detail_records WHERE company_payment_id = %d', ( int ) $intCompanyPaymentId ), $objDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsByEftInstructionId( $intEftInstructionId, $objDatabase ) {
		return self::fetchNachaReturnAddendaDetailRecords( sprintf( 'SELECT * FROM nacha_return_addenda_detail_records WHERE eft_instruction_id = %d', ( int ) $intEftInstructionId ), $objDatabase );
	}

	public static function fetchNachaReturnAddendaDetailRecordsByPaymentBlacklistEntryId( $intPaymentBlacklistEntryId, $objDatabase ) {
		return self::fetchNachaReturnAddendaDetailRecords( sprintf( 'SELECT * FROM nacha_return_addenda_detail_records WHERE payment_blacklist_entry_id = %d', ( int ) $intPaymentBlacklistEntryId ), $objDatabase );
	}

}
?>