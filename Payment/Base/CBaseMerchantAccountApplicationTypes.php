<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMerchantAccountApplicationTypes
 * Do not add any new functions to this class.
 */

class CBaseMerchantAccountApplicationTypes extends CEosPluralBase {

	/**
	 * @return CMerchantAccountApplicationType[]
	 */
	public static function fetchMerchantAccountApplicationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMerchantAccountApplicationType::class, $objDatabase );
	}

	/**
	 * @return CMerchantAccountApplicationType
	 */
	public static function fetchMerchantAccountApplicationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMerchantAccountApplicationType::class, $objDatabase );
	}

	public static function fetchMerchantAccountApplicationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'merchant_account_application_types', $objDatabase );
	}

	public static function fetchMerchantAccountApplicationTypeById( $intId, $objDatabase ) {
		return self::fetchMerchantAccountApplicationType( sprintf( 'SELECT * FROM merchant_account_application_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>