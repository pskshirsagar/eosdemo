<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CCompanyMerchantAccountMethodPaymentStatistics
 * Do not add any new functions to this class.
 */

class CBaseCompanyMerchantAccountMethodPaymentStatistics extends CEosPluralBase {

	/**
	 * @return CCompanyMerchantAccountMethodPaymentStatistic[]
	 */
	public static function fetchCompanyMerchantAccountMethodPaymentStatistics( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCompanyMerchantAccountMethodPaymentStatistic::class, $objDatabase );
	}

	/**
	 * @return CCompanyMerchantAccountMethodPaymentStatistic
	 */
	public static function fetchCompanyMerchantAccountMethodPaymentStatistic( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyMerchantAccountMethodPaymentStatistic::class, $objDatabase );
	}

	public static function fetchCompanyMerchantAccountMethodPaymentStatisticCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_merchant_account_method_payment_statistics', $objDatabase );
	}

	public static function fetchCompanyMerchantAccountMethodPaymentStatisticById( $intId, $objDatabase ) {
		return self::fetchCompanyMerchantAccountMethodPaymentStatistic( sprintf( 'SELECT * FROM company_merchant_account_method_payment_statistics WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCompanyMerchantAccountMethodPaymentStatisticsByMerchantAccountMethodId( $intMerchantAccountMethodId, $objDatabase ) {
		return self::fetchCompanyMerchantAccountMethodPaymentStatistics( sprintf( 'SELECT * FROM company_merchant_account_method_payment_statistics WHERE merchant_account_method_id = %d', $intMerchantAccountMethodId ), $objDatabase );
	}

}
?>