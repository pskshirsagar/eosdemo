<?php

class CBaseApplicationBankAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.application_bank_accounts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMerchantAccountApplicationId;
	protected $m_strPropertyName;
	protected $m_strBankName;
	protected $m_strBankAccountNumberEncrypted;
	protected $m_strBankAccountNumberMasked;
	protected $m_strBankRoutingNumberEncrypted;
	protected $m_strBankRoutingNumberMasked;
	protected $m_strDiscoverPin;
	protected $m_strAmexPin;
	protected $m_strMerchantNotifiedOn;
	protected $m_strCompletedOn;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['merchant_account_application_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantAccountApplicationId', trim( $arrValues['merchant_account_application_id'] ) ); elseif( isset( $arrValues['merchant_account_application_id'] ) ) $this->setMerchantAccountApplicationId( $arrValues['merchant_account_application_id'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( stripcslashes( $arrValues['property_name'] ) ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_name'] ) : $arrValues['property_name'] );
		if( isset( $arrValues['bank_name'] ) && $boolDirectSet ) $this->set( 'm_strBankName', trim( stripcslashes( $arrValues['bank_name'] ) ) ); elseif( isset( $arrValues['bank_name'] ) ) $this->setBankName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_name'] ) : $arrValues['bank_name'] );
		if( isset( $arrValues['bank_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBankAccountNumberEncrypted', trim( stripcslashes( $arrValues['bank_account_number_encrypted'] ) ) ); elseif( isset( $arrValues['bank_account_number_encrypted'] ) ) $this->setBankAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_account_number_encrypted'] ) : $arrValues['bank_account_number_encrypted'] );
		if( isset( $arrValues['bank_account_number_masked'] ) && $boolDirectSet ) $this->set( 'm_strBankAccountNumberMasked', trim( stripcslashes( $arrValues['bank_account_number_masked'] ) ) ); elseif( isset( $arrValues['bank_account_number_masked'] ) ) $this->setBankAccountNumberMasked( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_account_number_masked'] ) : $arrValues['bank_account_number_masked'] );
		if( isset( $arrValues['bank_routing_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBankRoutingNumberEncrypted', trim( stripcslashes( $arrValues['bank_routing_number_encrypted'] ) ) ); elseif( isset( $arrValues['bank_routing_number_encrypted'] ) ) $this->setBankRoutingNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_routing_number_encrypted'] ) : $arrValues['bank_routing_number_encrypted'] );
		if( isset( $arrValues['bank_routing_number_masked'] ) && $boolDirectSet ) $this->set( 'm_strBankRoutingNumberMasked', trim( stripcslashes( $arrValues['bank_routing_number_masked'] ) ) ); elseif( isset( $arrValues['bank_routing_number_masked'] ) ) $this->setBankRoutingNumberMasked( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_routing_number_masked'] ) : $arrValues['bank_routing_number_masked'] );
		if( isset( $arrValues['discover_pin'] ) && $boolDirectSet ) $this->set( 'm_strDiscoverPin', trim( stripcslashes( $arrValues['discover_pin'] ) ) ); elseif( isset( $arrValues['discover_pin'] ) ) $this->setDiscoverPin( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['discover_pin'] ) : $arrValues['discover_pin'] );
		if( isset( $arrValues['amex_pin'] ) && $boolDirectSet ) $this->set( 'm_strAmexPin', trim( stripcslashes( $arrValues['amex_pin'] ) ) ); elseif( isset( $arrValues['amex_pin'] ) ) $this->setAmexPin( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['amex_pin'] ) : $arrValues['amex_pin'] );
		if( isset( $arrValues['merchant_notified_on'] ) && $boolDirectSet ) $this->set( 'm_strMerchantNotifiedOn', trim( $arrValues['merchant_notified_on'] ) ); elseif( isset( $arrValues['merchant_notified_on'] ) ) $this->setMerchantNotifiedOn( $arrValues['merchant_notified_on'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMerchantAccountApplicationId( $intMerchantAccountApplicationId ) {
		$this->set( 'm_intMerchantAccountApplicationId', CStrings::strToIntDef( $intMerchantAccountApplicationId, NULL, false ) );
	}

	public function getMerchantAccountApplicationId() {
		return $this->m_intMerchantAccountApplicationId;
	}

	public function sqlMerchantAccountApplicationId() {
		return ( true == isset( $this->m_intMerchantAccountApplicationId ) ) ? ( string ) $this->m_intMerchantAccountApplicationId : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 50, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? '\'' . addslashes( $this->m_strPropertyName ) . '\'' : 'NULL';
	}

	public function setBankName( $strBankName ) {
		$this->set( 'm_strBankName', CStrings::strTrimDef( $strBankName, 50, NULL, true ) );
	}

	public function getBankName() {
		return $this->m_strBankName;
	}

	public function sqlBankName() {
		return ( true == isset( $this->m_strBankName ) ) ? '\'' . addslashes( $this->m_strBankName ) . '\'' : 'NULL';
	}

	public function setBankAccountNumberEncrypted( $strBankAccountNumberEncrypted ) {
		$this->set( 'm_strBankAccountNumberEncrypted', CStrings::strTrimDef( $strBankAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getBankAccountNumberEncrypted() {
		return $this->m_strBankAccountNumberEncrypted;
	}

	public function sqlBankAccountNumberEncrypted() {
		return ( true == isset( $this->m_strBankAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strBankAccountNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setBankAccountNumberMasked( $strBankAccountNumberMasked ) {
		$this->set( 'm_strBankAccountNumberMasked', CStrings::strTrimDef( $strBankAccountNumberMasked, 240, NULL, true ) );
	}

	public function getBankAccountNumberMasked() {
		return $this->m_strBankAccountNumberMasked;
	}

	public function sqlBankAccountNumberMasked() {
		return ( true == isset( $this->m_strBankAccountNumberMasked ) ) ? '\'' . addslashes( $this->m_strBankAccountNumberMasked ) . '\'' : 'NULL';
	}

	public function setBankRoutingNumberEncrypted( $strBankRoutingNumberEncrypted ) {
		$this->set( 'm_strBankRoutingNumberEncrypted', CStrings::strTrimDef( $strBankRoutingNumberEncrypted, 240, NULL, true ) );
	}

	public function getBankRoutingNumberEncrypted() {
		return $this->m_strBankRoutingNumberEncrypted;
	}

	public function sqlBankRoutingNumberEncrypted() {
		return ( true == isset( $this->m_strBankRoutingNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strBankRoutingNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setBankRoutingNumberMasked( $strBankRoutingNumberMasked ) {
		$this->set( 'm_strBankRoutingNumberMasked', CStrings::strTrimDef( $strBankRoutingNumberMasked, 240, NULL, true ) );
	}

	public function getBankRoutingNumberMasked() {
		return $this->m_strBankRoutingNumberMasked;
	}

	public function sqlBankRoutingNumberMasked() {
		return ( true == isset( $this->m_strBankRoutingNumberMasked ) ) ? '\'' . addslashes( $this->m_strBankRoutingNumberMasked ) . '\'' : 'NULL';
	}

	public function setDiscoverPin( $strDiscoverPin ) {
		$this->set( 'm_strDiscoverPin', CStrings::strTrimDef( $strDiscoverPin, 50, NULL, true ) );
	}

	public function getDiscoverPin() {
		return $this->m_strDiscoverPin;
	}

	public function sqlDiscoverPin() {
		return ( true == isset( $this->m_strDiscoverPin ) ) ? '\'' . addslashes( $this->m_strDiscoverPin ) . '\'' : 'NULL';
	}

	public function setAmexPin( $strAmexPin ) {
		$this->set( 'm_strAmexPin', CStrings::strTrimDef( $strAmexPin, 50, NULL, true ) );
	}

	public function getAmexPin() {
		return $this->m_strAmexPin;
	}

	public function sqlAmexPin() {
		return ( true == isset( $this->m_strAmexPin ) ) ? '\'' . addslashes( $this->m_strAmexPin ) . '\'' : 'NULL';
	}

	public function setMerchantNotifiedOn( $strMerchantNotifiedOn ) {
		$this->set( 'm_strMerchantNotifiedOn', CStrings::strTrimDef( $strMerchantNotifiedOn, -1, NULL, true ) );
	}

	public function getMerchantNotifiedOn() {
		return $this->m_strMerchantNotifiedOn;
	}

	public function sqlMerchantNotifiedOn() {
		return ( true == isset( $this->m_strMerchantNotifiedOn ) ) ? '\'' . $this->m_strMerchantNotifiedOn . '\'' : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, merchant_account_application_id, property_name, bank_name, bank_account_number_encrypted, bank_account_number_masked, bank_routing_number_encrypted, bank_routing_number_masked, discover_pin, amex_pin, merchant_notified_on, completed_on, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlMerchantAccountApplicationId() . ', ' .
 						$this->sqlPropertyName() . ', ' .
 						$this->sqlBankName() . ', ' .
 						$this->sqlBankAccountNumberEncrypted() . ', ' .
 						$this->sqlBankAccountNumberMasked() . ', ' .
 						$this->sqlBankRoutingNumberEncrypted() . ', ' .
 						$this->sqlBankRoutingNumberMasked() . ', ' .
 						$this->sqlDiscoverPin() . ', ' .
 						$this->sqlAmexPin() . ', ' .
 						$this->sqlMerchantNotifiedOn() . ', ' .
 						$this->sqlCompletedOn() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_account_application_id = ' . $this->sqlMerchantAccountApplicationId() . ','; } elseif( true == array_key_exists( 'MerchantAccountApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_account_application_id = ' . $this->sqlMerchantAccountApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_name = ' . $this->sqlBankName() . ','; } elseif( true == array_key_exists( 'BankName', $this->getChangedColumns() ) ) { $strSql .= ' bank_name = ' . $this->sqlBankName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_number_encrypted = ' . $this->sqlBankAccountNumberEncrypted() . ','; } elseif( true == array_key_exists( 'BankAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_number_encrypted = ' . $this->sqlBankAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_number_masked = ' . $this->sqlBankAccountNumberMasked() . ','; } elseif( true == array_key_exists( 'BankAccountNumberMasked', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_number_masked = ' . $this->sqlBankAccountNumberMasked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_routing_number_encrypted = ' . $this->sqlBankRoutingNumberEncrypted() . ','; } elseif( true == array_key_exists( 'BankRoutingNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' bank_routing_number_encrypted = ' . $this->sqlBankRoutingNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_routing_number_masked = ' . $this->sqlBankRoutingNumberMasked() . ','; } elseif( true == array_key_exists( 'BankRoutingNumberMasked', $this->getChangedColumns() ) ) { $strSql .= ' bank_routing_number_masked = ' . $this->sqlBankRoutingNumberMasked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' discover_pin = ' . $this->sqlDiscoverPin() . ','; } elseif( true == array_key_exists( 'DiscoverPin', $this->getChangedColumns() ) ) { $strSql .= ' discover_pin = ' . $this->sqlDiscoverPin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amex_pin = ' . $this->sqlAmexPin() . ','; } elseif( true == array_key_exists( 'AmexPin', $this->getChangedColumns() ) ) { $strSql .= ' amex_pin = ' . $this->sqlAmexPin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_notified_on = ' . $this->sqlMerchantNotifiedOn() . ','; } elseif( true == array_key_exists( 'MerchantNotifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' merchant_notified_on = ' . $this->sqlMerchantNotifiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'merchant_account_application_id' => $this->getMerchantAccountApplicationId(),
			'property_name' => $this->getPropertyName(),
			'bank_name' => $this->getBankName(),
			'bank_account_number_encrypted' => $this->getBankAccountNumberEncrypted(),
			'bank_account_number_masked' => $this->getBankAccountNumberMasked(),
			'bank_routing_number_encrypted' => $this->getBankRoutingNumberEncrypted(),
			'bank_routing_number_masked' => $this->getBankRoutingNumberMasked(),
			'discover_pin' => $this->getDiscoverPin(),
			'amex_pin' => $this->getAmexPin(),
			'merchant_notified_on' => $this->getMerchantNotifiedOn(),
			'completed_on' => $this->getCompletedOn(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>