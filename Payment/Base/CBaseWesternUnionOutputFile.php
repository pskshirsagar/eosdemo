<?php

class CBaseWesternUnionOutputFile extends CEosSingularBase {

	const TABLE_NAME = 'public.western_union_output_files';

	protected $m_intId;
	protected $m_intProcessingBankId;
	protected $m_intProcessingBankAccountId;
	protected $m_intFileTypeId;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strHeaderRecordType;
	protected $m_strHeaderOutputType;
	protected $m_strHeaderDatetime;
	protected $m_strHeaderClientId;
	protected $m_strHeaderFiller;
	protected $m_strTrailerRecordType;
	protected $m_strTrailerNumberOfRecords;
	protected $m_strTrailerTotalDollarAmount;
	protected $m_strTrailerTotalCustomerFees;
	protected $m_strReceivedOn;
	protected $m_intIsReconPrepped;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsReconPrepped = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['processing_bank_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankId', trim( $arrValues['processing_bank_id'] ) ); elseif( isset( $arrValues['processing_bank_id'] ) ) $this->setProcessingBankId( $arrValues['processing_bank_id'] );
		if( isset( $arrValues['processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankAccountId', trim( $arrValues['processing_bank_account_id'] ) ); elseif( isset( $arrValues['processing_bank_account_id'] ) ) $this->setProcessingBankAccountId( $arrValues['processing_bank_account_id'] );
		if( isset( $arrValues['file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFileTypeId', trim( $arrValues['file_type_id'] ) ); elseif( isset( $arrValues['file_type_id'] ) ) $this->setFileTypeId( $arrValues['file_type_id'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['header_record_type'] ) && $boolDirectSet ) $this->set( 'm_strHeaderRecordType', trim( stripcslashes( $arrValues['header_record_type'] ) ) ); elseif( isset( $arrValues['header_record_type'] ) ) $this->setHeaderRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_record_type'] ) : $arrValues['header_record_type'] );
		if( isset( $arrValues['header_output_type'] ) && $boolDirectSet ) $this->set( 'm_strHeaderOutputType', trim( stripcslashes( $arrValues['header_output_type'] ) ) ); elseif( isset( $arrValues['header_output_type'] ) ) $this->setHeaderOutputType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_output_type'] ) : $arrValues['header_output_type'] );
		if( isset( $arrValues['header_datetime'] ) && $boolDirectSet ) $this->set( 'm_strHeaderDatetime', trim( stripcslashes( $arrValues['header_datetime'] ) ) ); elseif( isset( $arrValues['header_datetime'] ) ) $this->setHeaderDatetime( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_datetime'] ) : $arrValues['header_datetime'] );
		if( isset( $arrValues['header_client_id'] ) && $boolDirectSet ) $this->set( 'm_strHeaderClientId', trim( stripcslashes( $arrValues['header_client_id'] ) ) ); elseif( isset( $arrValues['header_client_id'] ) ) $this->setHeaderClientId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_client_id'] ) : $arrValues['header_client_id'] );
		if( isset( $arrValues['header_filler'] ) && $boolDirectSet ) $this->set( 'm_strHeaderFiller', trim( stripcslashes( $arrValues['header_filler'] ) ) ); elseif( isset( $arrValues['header_filler'] ) ) $this->setHeaderFiller( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_filler'] ) : $arrValues['header_filler'] );
		if( isset( $arrValues['trailer_record_type'] ) && $boolDirectSet ) $this->set( 'm_strTrailerRecordType', trim( stripcslashes( $arrValues['trailer_record_type'] ) ) ); elseif( isset( $arrValues['trailer_record_type'] ) ) $this->setTrailerRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['trailer_record_type'] ) : $arrValues['trailer_record_type'] );
		if( isset( $arrValues['trailer_number_of_records'] ) && $boolDirectSet ) $this->set( 'm_strTrailerNumberOfRecords', trim( stripcslashes( $arrValues['trailer_number_of_records'] ) ) ); elseif( isset( $arrValues['trailer_number_of_records'] ) ) $this->setTrailerNumberOfRecords( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['trailer_number_of_records'] ) : $arrValues['trailer_number_of_records'] );
		if( isset( $arrValues['trailer_total_dollar_amount'] ) && $boolDirectSet ) $this->set( 'm_strTrailerTotalDollarAmount', trim( stripcslashes( $arrValues['trailer_total_dollar_amount'] ) ) ); elseif( isset( $arrValues['trailer_total_dollar_amount'] ) ) $this->setTrailerTotalDollarAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['trailer_total_dollar_amount'] ) : $arrValues['trailer_total_dollar_amount'] );
		if( isset( $arrValues['trailer_total_customer_fees'] ) && $boolDirectSet ) $this->set( 'm_strTrailerTotalCustomerFees', trim( stripcslashes( $arrValues['trailer_total_customer_fees'] ) ) ); elseif( isset( $arrValues['trailer_total_customer_fees'] ) ) $this->setTrailerTotalCustomerFees( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['trailer_total_customer_fees'] ) : $arrValues['trailer_total_customer_fees'] );
		if( isset( $arrValues['received_on'] ) && $boolDirectSet ) $this->set( 'm_strReceivedOn', trim( $arrValues['received_on'] ) ); elseif( isset( $arrValues['received_on'] ) ) $this->setReceivedOn( $arrValues['received_on'] );
		if( isset( $arrValues['is_recon_prepped'] ) && $boolDirectSet ) $this->set( 'm_intIsReconPrepped', trim( $arrValues['is_recon_prepped'] ) ); elseif( isset( $arrValues['is_recon_prepped'] ) ) $this->setIsReconPrepped( $arrValues['is_recon_prepped'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setProcessingBankId( $intProcessingBankId ) {
		$this->set( 'm_intProcessingBankId', CStrings::strToIntDef( $intProcessingBankId, NULL, false ) );
	}

	public function getProcessingBankId() {
		return $this->m_intProcessingBankId;
	}

	public function sqlProcessingBankId() {
		return ( true == isset( $this->m_intProcessingBankId ) ) ? ( string ) $this->m_intProcessingBankId : 'NULL';
	}

	public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
		$this->set( 'm_intProcessingBankAccountId', CStrings::strToIntDef( $intProcessingBankAccountId, NULL, false ) );
	}

	public function getProcessingBankAccountId() {
		return $this->m_intProcessingBankAccountId;
	}

	public function sqlProcessingBankAccountId() {
		return ( true == isset( $this->m_intProcessingBankAccountId ) ) ? ( string ) $this->m_intProcessingBankAccountId : 'NULL';
	}

	public function setFileTypeId( $intFileTypeId ) {
		$this->set( 'm_intFileTypeId', CStrings::strToIntDef( $intFileTypeId, NULL, false ) );
	}

	public function getFileTypeId() {
		return $this->m_intFileTypeId;
	}

	public function sqlFileTypeId() {
		return ( true == isset( $this->m_intFileTypeId ) ) ? ( string ) $this->m_intFileTypeId : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setHeaderRecordType( $strHeaderRecordType ) {
		$this->set( 'm_strHeaderRecordType', CStrings::strTrimDef( $strHeaderRecordType, 1, NULL, true ) );
	}

	public function getHeaderRecordType() {
		return $this->m_strHeaderRecordType;
	}

	public function sqlHeaderRecordType() {
		return ( true == isset( $this->m_strHeaderRecordType ) ) ? '\'' . addslashes( $this->m_strHeaderRecordType ) . '\'' : 'NULL';
	}

	public function setHeaderOutputType( $strHeaderOutputType ) {
		$this->set( 'm_strHeaderOutputType', CStrings::strTrimDef( $strHeaderOutputType, 3, NULL, true ) );
	}

	public function getHeaderOutputType() {
		return $this->m_strHeaderOutputType;
	}

	public function sqlHeaderOutputType() {
		return ( true == isset( $this->m_strHeaderOutputType ) ) ? '\'' . addslashes( $this->m_strHeaderOutputType ) . '\'' : 'NULL';
	}

	public function setHeaderDatetime( $strHeaderDatetime ) {
		$this->set( 'm_strHeaderDatetime', CStrings::strTrimDef( $strHeaderDatetime, 21, NULL, true ) );
	}

	public function getHeaderDatetime() {
		return $this->m_strHeaderDatetime;
	}

	public function sqlHeaderDatetime() {
		return ( true == isset( $this->m_strHeaderDatetime ) ) ? '\'' . addslashes( $this->m_strHeaderDatetime ) . '\'' : 'NULL';
	}

	public function setHeaderClientId( $strHeaderClientId ) {
		$this->set( 'm_strHeaderClientId', CStrings::strTrimDef( $strHeaderClientId, 9, NULL, true ) );
	}

	public function getHeaderClientId() {
		return $this->m_strHeaderClientId;
	}

	public function sqlHeaderClientId() {
		return ( true == isset( $this->m_strHeaderClientId ) ) ? '\'' . addslashes( $this->m_strHeaderClientId ) . '\'' : 'NULL';
	}

	public function setHeaderFiller( $strHeaderFiller ) {
		$this->set( 'm_strHeaderFiller', CStrings::strTrimDef( $strHeaderFiller, 42, NULL, true ) );
	}

	public function getHeaderFiller() {
		return $this->m_strHeaderFiller;
	}

	public function sqlHeaderFiller() {
		return ( true == isset( $this->m_strHeaderFiller ) ) ? '\'' . addslashes( $this->m_strHeaderFiller ) . '\'' : 'NULL';
	}

	public function setTrailerRecordType( $strTrailerRecordType ) {
		$this->set( 'm_strTrailerRecordType', CStrings::strTrimDef( $strTrailerRecordType, 1, NULL, true ) );
	}

	public function getTrailerRecordType() {
		return $this->m_strTrailerRecordType;
	}

	public function sqlTrailerRecordType() {
		return ( true == isset( $this->m_strTrailerRecordType ) ) ? '\'' . addslashes( $this->m_strTrailerRecordType ) . '\'' : 'NULL';
	}

	public function setTrailerNumberOfRecords( $strTrailerNumberOfRecords ) {
		$this->set( 'm_strTrailerNumberOfRecords', CStrings::strTrimDef( $strTrailerNumberOfRecords, 6, NULL, true ) );
	}

	public function getTrailerNumberOfRecords() {
		return $this->m_strTrailerNumberOfRecords;
	}

	public function sqlTrailerNumberOfRecords() {
		return ( true == isset( $this->m_strTrailerNumberOfRecords ) ) ? '\'' . addslashes( $this->m_strTrailerNumberOfRecords ) . '\'' : 'NULL';
	}

	public function setTrailerTotalDollarAmount( $strTrailerTotalDollarAmount ) {
		$this->set( 'm_strTrailerTotalDollarAmount', CStrings::strTrimDef( $strTrailerTotalDollarAmount, 14, NULL, true ) );
	}

	public function getTrailerTotalDollarAmount() {
		return $this->m_strTrailerTotalDollarAmount;
	}

	public function sqlTrailerTotalDollarAmount() {
		return ( true == isset( $this->m_strTrailerTotalDollarAmount ) ) ? '\'' . addslashes( $this->m_strTrailerTotalDollarAmount ) . '\'' : 'NULL';
	}

	public function setTrailerTotalCustomerFees( $strTrailerTotalCustomerFees ) {
		$this->set( 'm_strTrailerTotalCustomerFees', CStrings::strTrimDef( $strTrailerTotalCustomerFees, 14, NULL, true ) );
	}

	public function getTrailerTotalCustomerFees() {
		return $this->m_strTrailerTotalCustomerFees;
	}

	public function sqlTrailerTotalCustomerFees() {
		return ( true == isset( $this->m_strTrailerTotalCustomerFees ) ) ? '\'' . addslashes( $this->m_strTrailerTotalCustomerFees ) . '\'' : 'NULL';
	}

	public function setReceivedOn( $strReceivedOn ) {
		$this->set( 'm_strReceivedOn', CStrings::strTrimDef( $strReceivedOn, -1, NULL, true ) );
	}

	public function getReceivedOn() {
		return $this->m_strReceivedOn;
	}

	public function sqlReceivedOn() {
		return ( true == isset( $this->m_strReceivedOn ) ) ? '\'' . $this->m_strReceivedOn . '\'' : 'NULL';
	}

	public function setIsReconPrepped( $intIsReconPrepped ) {
		$this->set( 'm_intIsReconPrepped', CStrings::strToIntDef( $intIsReconPrepped, NULL, false ) );
	}

	public function getIsReconPrepped() {
		return $this->m_intIsReconPrepped;
	}

	public function sqlIsReconPrepped() {
		return ( true == isset( $this->m_intIsReconPrepped ) ) ? ( string ) $this->m_intIsReconPrepped : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, processing_bank_id, processing_bank_account_id, file_type_id, file_name, file_path, header_record_type, header_output_type, header_datetime, header_client_id, header_filler, trailer_record_type, trailer_number_of_records, trailer_total_dollar_amount, trailer_total_customer_fees, received_on, is_recon_prepped, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlProcessingBankId() . ', ' .
 						$this->sqlProcessingBankAccountId() . ', ' .
 						$this->sqlFileTypeId() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlHeaderRecordType() . ', ' .
 						$this->sqlHeaderOutputType() . ', ' .
 						$this->sqlHeaderDatetime() . ', ' .
 						$this->sqlHeaderClientId() . ', ' .
 						$this->sqlHeaderFiller() . ', ' .
 						$this->sqlTrailerRecordType() . ', ' .
 						$this->sqlTrailerNumberOfRecords() . ', ' .
 						$this->sqlTrailerTotalDollarAmount() . ', ' .
 						$this->sqlTrailerTotalCustomerFees() . ', ' .
 						$this->sqlReceivedOn() . ', ' .
 						$this->sqlIsReconPrepped() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; } elseif( true == array_key_exists( 'ProcessingBankId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; } elseif( true == array_key_exists( 'ProcessingBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId() . ','; } elseif( true == array_key_exists( 'FileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_record_type = ' . $this->sqlHeaderRecordType() . ','; } elseif( true == array_key_exists( 'HeaderRecordType', $this->getChangedColumns() ) ) { $strSql .= ' header_record_type = ' . $this->sqlHeaderRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_output_type = ' . $this->sqlHeaderOutputType() . ','; } elseif( true == array_key_exists( 'HeaderOutputType', $this->getChangedColumns() ) ) { $strSql .= ' header_output_type = ' . $this->sqlHeaderOutputType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_datetime = ' . $this->sqlHeaderDatetime() . ','; } elseif( true == array_key_exists( 'HeaderDatetime', $this->getChangedColumns() ) ) { $strSql .= ' header_datetime = ' . $this->sqlHeaderDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_client_id = ' . $this->sqlHeaderClientId() . ','; } elseif( true == array_key_exists( 'HeaderClientId', $this->getChangedColumns() ) ) { $strSql .= ' header_client_id = ' . $this->sqlHeaderClientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_filler = ' . $this->sqlHeaderFiller() . ','; } elseif( true == array_key_exists( 'HeaderFiller', $this->getChangedColumns() ) ) { $strSql .= ' header_filler = ' . $this->sqlHeaderFiller() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trailer_record_type = ' . $this->sqlTrailerRecordType() . ','; } elseif( true == array_key_exists( 'TrailerRecordType', $this->getChangedColumns() ) ) { $strSql .= ' trailer_record_type = ' . $this->sqlTrailerRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trailer_number_of_records = ' . $this->sqlTrailerNumberOfRecords() . ','; } elseif( true == array_key_exists( 'TrailerNumberOfRecords', $this->getChangedColumns() ) ) { $strSql .= ' trailer_number_of_records = ' . $this->sqlTrailerNumberOfRecords() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trailer_total_dollar_amount = ' . $this->sqlTrailerTotalDollarAmount() . ','; } elseif( true == array_key_exists( 'TrailerTotalDollarAmount', $this->getChangedColumns() ) ) { $strSql .= ' trailer_total_dollar_amount = ' . $this->sqlTrailerTotalDollarAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trailer_total_customer_fees = ' . $this->sqlTrailerTotalCustomerFees() . ','; } elseif( true == array_key_exists( 'TrailerTotalCustomerFees', $this->getChangedColumns() ) ) { $strSql .= ' trailer_total_customer_fees = ' . $this->sqlTrailerTotalCustomerFees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' received_on = ' . $this->sqlReceivedOn() . ','; } elseif( true == array_key_exists( 'ReceivedOn', $this->getChangedColumns() ) ) { $strSql .= ' received_on = ' . $this->sqlReceivedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; } elseif( true == array_key_exists( 'IsReconPrepped', $this->getChangedColumns() ) ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'processing_bank_id' => $this->getProcessingBankId(),
			'processing_bank_account_id' => $this->getProcessingBankAccountId(),
			'file_type_id' => $this->getFileTypeId(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'header_record_type' => $this->getHeaderRecordType(),
			'header_output_type' => $this->getHeaderOutputType(),
			'header_datetime' => $this->getHeaderDatetime(),
			'header_client_id' => $this->getHeaderClientId(),
			'header_filler' => $this->getHeaderFiller(),
			'trailer_record_type' => $this->getTrailerRecordType(),
			'trailer_number_of_records' => $this->getTrailerNumberOfRecords(),
			'trailer_total_dollar_amount' => $this->getTrailerTotalDollarAmount(),
			'trailer_total_customer_fees' => $this->getTrailerTotalCustomerFees(),
			'received_on' => $this->getReceivedOn(),
			'is_recon_prepped' => $this->getIsReconPrepped(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>