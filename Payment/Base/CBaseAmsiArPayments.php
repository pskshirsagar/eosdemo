<?php

class CBaseAmsiArPayments extends CEosPluralBase {

	/**
	 * @return CAmsiArPayment[]
	 */
	public static function fetchAmsiArPayments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CAmsiArPayment', $objDatabase );
	}

	/**
	 * @return CAmsiArPayment
	 */
	public static function fetchAmsiArPayment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAmsiArPayment', $objDatabase );
	}

	public static function fetchAmsiArPaymentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'amsi_ar_payments', $objDatabase );
	}

	public static function fetchAmsiArPaymentById( $intId, $objDatabase ) {
		return self::fetchAmsiArPayment( sprintf( 'SELECT * FROM amsi_ar_payments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchAmsiArPaymentsByCid( $intCid, $objDatabase ) {
		return self::fetchAmsiArPayments( sprintf( 'SELECT * FROM amsi_ar_payments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAmsiArPaymentsByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchAmsiArPayments( sprintf( 'SELECT * FROM amsi_ar_payments WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

}
?>