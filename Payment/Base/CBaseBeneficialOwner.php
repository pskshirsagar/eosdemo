<?php

class CBaseBeneficialOwner extends CEosSingularBase {

    const TABLE_NAME = 'public.beneficial_owners';

    protected $m_intId;
    protected $m_intCid;
    protected $m_intPropertyId;
    protected $m_intUnderwritingTypeId;
    protected $m_intOwnerType;
    protected $m_boolIsOwnerControlIndividual;
    protected $m_strOwnerTitle;
    protected $m_strOwnerName;
    protected $m_strOwnerSsnEncrypted;
    protected $m_strOwnerDob;
    protected $m_intOwnerOwnershipPercent;
    protected $m_strOwnerAddress;
    protected $m_strOwnerCity;
    protected $m_strOwnerStateCode;
    protected $m_strOwnerCountryCode;
    protected $m_strOwnerPostalCode;
    protected $m_strOwnerPhoneNumber;
    protected $m_strOwnerEmail;
    protected $m_intDeletedBy;
    protected $m_strDeletedOn;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;
    protected $m_strDlNumberEncrypted;
    protected $m_strDlStateCode;
    protected $m_intAccountId;
	protected $m_boolIsOwnerPep;

    public function __construct() {
        parent::__construct();

        $this->m_boolIsOwnerControlIndividual = false;
        $this->m_strOwnerCountryCode = 'US';

        return;
    }

    public function setDefaults() {
        return;
    }

    /**
     * @SuppressWarnings( BooleanArgumentFlag )
     */
    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
        if( isset( $arrValues['underwriting_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnderwritingTypeId', trim( $arrValues['underwriting_type_id'] ) ); elseif( isset( $arrValues['underwriting_type_id'] ) ) $this->setUnderwritingTypeId( $arrValues['underwriting_type_id'] );
        if( isset( $arrValues['owner_type'] ) && $boolDirectSet ) $this->set( 'm_intOwnerType', trim( $arrValues['owner_type'] ) ); elseif( isset( $arrValues['owner_type'] ) ) $this->setOwnerType( $arrValues['owner_type'] );
        if( isset( $arrValues['is_owner_control_individual'] ) && $boolDirectSet ) $this->set( 'm_boolIsOwnerControlIndividual', trim( stripcslashes( $arrValues['is_owner_control_individual'] ) ) ); elseif( isset( $arrValues['is_owner_control_individual'] ) ) $this->setIsOwnerControlIndividual( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_owner_control_individual'] ) : $arrValues['is_owner_control_individual'] );
        if( isset( $arrValues['owner_title'] ) && $boolDirectSet ) $this->set( 'm_strOwnerTitle', trim( $arrValues['owner_title'] ) ); elseif( isset( $arrValues['owner_title'] ) ) $this->setOwnerTitle( $arrValues['owner_title'] );
        if( isset( $arrValues['owner_name'] ) && $boolDirectSet ) $this->set( 'm_strOwnerName', trim( $arrValues['owner_name'] ) ); elseif( isset( $arrValues['owner_name'] ) ) $this->setOwnerName( $arrValues['owner_name'] );
        if( isset( $arrValues['owner_ssn_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strOwnerSsnEncrypted', trim( $arrValues['owner_ssn_encrypted'] ) ); elseif( isset( $arrValues['owner_ssn_encrypted'] ) ) $this->setOwnerSsnEncrypted( $arrValues['owner_ssn_encrypted'] );
        if( isset( $arrValues['owner_dob'] ) && $boolDirectSet ) $this->set( 'm_strOwnerDob', trim( $arrValues['owner_dob'] ) ); elseif( isset( $arrValues['owner_dob'] ) ) $this->setOwnerDob( $arrValues['owner_dob'] );
        if( isset( $arrValues['owner_ownership_percent'] ) && $boolDirectSet ) $this->set( 'm_intOwnerOwnershipPercent', trim( $arrValues['owner_ownership_percent'] ) ); elseif( isset( $arrValues['owner_ownership_percent'] ) ) $this->setOwnerOwnershipPercent( $arrValues['owner_ownership_percent'] );
        if( isset( $arrValues['owner_address'] ) && $boolDirectSet ) $this->set( 'm_strOwnerAddress', trim( $arrValues['owner_address'] ) ); elseif( isset( $arrValues['owner_address'] ) ) $this->setOwnerAddress( $arrValues['owner_address'] );
        if( isset( $arrValues['owner_city'] ) && $boolDirectSet ) $this->set( 'm_strOwnerCity', trim( $arrValues['owner_city'] ) ); elseif( isset( $arrValues['owner_city'] ) ) $this->setOwnerCity( $arrValues['owner_city'] );
        if( isset( $arrValues['owner_state_code'] ) && $boolDirectSet ) $this->set( 'm_strOwnerStateCode', trim( $arrValues['owner_state_code'] ) ); elseif( isset( $arrValues['owner_state_code'] ) ) $this->setOwnerStateCode( $arrValues['owner_state_code'] );
        if( isset( $arrValues['owner_country_code'] ) && $boolDirectSet ) $this->set( 'm_strOwnerCountryCode', trim( $arrValues['owner_country_code'] ) ); elseif( isset( $arrValues['owner_country_code'] ) ) $this->setOwnerCountryCode( $arrValues['owner_country_code'] );
        if( isset( $arrValues['owner_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strOwnerPostalCode', trim( $arrValues['owner_postal_code'] ) ); elseif( isset( $arrValues['owner_postal_code'] ) ) $this->setOwnerPostalCode( $arrValues['owner_postal_code'] );
        if( isset( $arrValues['owner_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strOwnerPhoneNumber', trim( $arrValues['owner_phone_number'] ) ); elseif( isset( $arrValues['owner_phone_number'] ) ) $this->setOwnerPhoneNumber( $arrValues['owner_phone_number'] );
        if( isset( $arrValues['owner_email'] ) && $boolDirectSet ) $this->set( 'm_strOwnerEmail', trim( $arrValues['owner_email'] ) ); elseif( isset( $arrValues['owner_email'] ) ) $this->setOwnerEmail( $arrValues['owner_email'] );
        if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
        if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
        if( isset( $arrValues['dl_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strDlNumberEncrypted', trim( $arrValues['dl_number_encrypted'] ) ); elseif( isset( $arrValues['dl_number_encrypted'] ) ) $this->setDlNumberEncrypted( $arrValues['dl_number_encrypted'] );
        if( isset( $arrValues['dl_state_code'] ) && $boolDirectSet ) $this->set( 'm_strDlStateCode', trim( $arrValues['dl_state_code'] ) ); elseif( isset( $arrValues['dl_state_code'] ) ) $this->setDlStateCode( $arrValues['dl_state_code'] );
        if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
	    if( isset( $arrValues['is_owner_pep'] ) && $boolDirectSet ) $this->set( 'm_boolIsOwnerPep', trim( stripcslashes( $arrValues['is_owner_pep'] ) ) ); elseif( isset( $arrValues['is_owner_pep'] ) ) $this->setIsOwnerPep( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_owner_pep'] ) : $arrValues['is_owner_pep'] );
        $this->m_boolInitialized = true;
    }

    public function setId( $intId ) {
        $this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
    }

    public function setPropertyId( $intPropertyId ) {
        $this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
    }

    public function getPropertyId() {
        return $this->m_intPropertyId;
    }

    public function sqlPropertyId() {
        return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
    }

    public function setUnderwritingTypeId( $intUnderwritingTypeId ) {
        $this->set( 'm_intUnderwritingTypeId', CStrings::strToIntDef( $intUnderwritingTypeId, NULL, false ) );
    }

    public function getUnderwritingTypeId() {
        return $this->m_intUnderwritingTypeId;
    }

    public function sqlUnderwritingTypeId() {
        return ( true == isset( $this->m_intUnderwritingTypeId ) ) ? ( string ) $this->m_intUnderwritingTypeId : 'NULL';
    }

    public function setOwnerType( $intOwnerType ) {
        $this->set( 'm_intOwnerType', CStrings::strToIntDef( $intOwnerType, NULL, false ) );
    }

    public function getOwnerType() {
        return $this->m_intOwnerType;
    }

    public function sqlOwnerType() {
        return ( true == isset( $this->m_intOwnerType ) ) ? ( string ) $this->m_intOwnerType : 'NULL';
    }

    public function setIsOwnerControlIndividual( $boolIsOwnerControlIndividual ) {
        $this->set( 'm_boolIsOwnerControlIndividual', CStrings::strToBool( $boolIsOwnerControlIndividual ) );
    }

    public function getIsOwnerControlIndividual() {
        return $this->m_boolIsOwnerControlIndividual;
    }

    public function sqlIsOwnerControlIndividual() {
        return ( true == isset( $this->m_boolIsOwnerControlIndividual ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOwnerControlIndividual ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setOwnerTitle( $strOwnerTitle ) {
        $this->set( 'm_strOwnerTitle', CStrings::strTrimDef( $strOwnerTitle, 255, NULL, true ) );
    }

    public function getOwnerTitle() {
        return $this->m_strOwnerTitle;
    }

    public function sqlOwnerTitle() {
        return ( true == isset( $this->m_strOwnerTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOwnerTitle ) : '\'' . addslashes( $this->m_strOwnerTitle ) . '\'' ) : 'NULL';
    }

    public function setOwnerName( $strOwnerName ) {
        $this->set( 'm_strOwnerName', CStrings::strTrimDef( $strOwnerName, 255, NULL, true ) );
    }

    public function getOwnerName() {
        return $this->m_strOwnerName;
    }

    public function sqlOwnerName() {
        return ( true == isset( $this->m_strOwnerName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOwnerName ) : '\'' . addslashes( $this->m_strOwnerName ) . '\'' ) : 'NULL';
    }

    public function setOwnerSsnEncrypted( $strOwnerSsnEncrypted ) {
        $this->set( 'm_strOwnerSsnEncrypted', CStrings::strTrimDef( $strOwnerSsnEncrypted, 255, NULL, true ) );
    }

    public function getOwnerSsnEncrypted() {
        return $this->m_strOwnerSsnEncrypted;
    }

    public function sqlOwnerSsnEncrypted() {
        return ( true == isset( $this->m_strOwnerSsnEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOwnerSsnEncrypted ) : '\'' . addslashes( $this->m_strOwnerSsnEncrypted ) . '\'' ) : 'NULL';
    }

    public function setOwnerDob( $strOwnerDob ) {
        $this->set( 'm_strOwnerDob', CStrings::strTrimDef( $strOwnerDob, -1, NULL, true ) );
    }

    public function getOwnerDob() {
        return $this->m_strOwnerDob;
    }

    public function sqlOwnerDob() {
        return ( true == isset( $this->m_strOwnerDob ) ) ? '\'' . $this->m_strOwnerDob . '\'' : 'NOW()';
    }

    public function setOwnerOwnershipPercent( $intOwnerOwnershipPercent ) {
        $this->set( 'm_intOwnerOwnershipPercent', CStrings::strToIntDef( $intOwnerOwnershipPercent, NULL, false ) );
    }

    public function getOwnerOwnershipPercent() {
        return $this->m_intOwnerOwnershipPercent;
    }

    public function sqlOwnerOwnershipPercent() {
        return ( true == isset( $this->m_intOwnerOwnershipPercent ) ) ? ( string ) $this->m_intOwnerOwnershipPercent : 'NULL';
    }

    public function setOwnerAddress( $strOwnerAddress ) {
        $this->set( 'm_strOwnerAddress', CStrings::strTrimDef( $strOwnerAddress, 255, NULL, true ) );
    }

    public function getOwnerAddress() {
        return $this->m_strOwnerAddress;
    }

    public function sqlOwnerAddress() {
        return ( true == isset( $this->m_strOwnerAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOwnerAddress ) : '\'' . addslashes( $this->m_strOwnerAddress ) . '\'' ) : 'NULL';
    }

    public function setOwnerCity( $strOwnerCity ) {
        $this->set( 'm_strOwnerCity', CStrings::strTrimDef( $strOwnerCity, 255, NULL, true ) );
    }

    public function getOwnerCity() {
        return $this->m_strOwnerCity;
    }

    public function sqlOwnerCity() {
        return ( true == isset( $this->m_strOwnerCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOwnerCity ) : '\'' . addslashes( $this->m_strOwnerCity ) . '\'' ) : 'NULL';
    }

    public function setOwnerStateCode( $strOwnerStateCode ) {
        $this->set( 'm_strOwnerStateCode', CStrings::strTrimDef( $strOwnerStateCode, 2, NULL, true ) );
    }

    public function getOwnerStateCode() {
        return $this->m_strOwnerStateCode;
    }

    public function sqlOwnerStateCode() {
        return ( true == isset( $this->m_strOwnerStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOwnerStateCode ) : '\'' . addslashes( $this->m_strOwnerStateCode ) . '\'' ) : 'NULL';
    }

    public function setOwnerCountryCode( $strOwnerCountryCode ) {
        $this->set( 'm_strOwnerCountryCode', CStrings::strTrimDef( $strOwnerCountryCode, 2, NULL, true ) );
    }

    public function getOwnerCountryCode() {
        return $this->m_strOwnerCountryCode;
    }

    public function sqlOwnerCountryCode() {
        return ( true == isset( $this->m_strOwnerCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOwnerCountryCode ) : '\'' . addslashes( $this->m_strOwnerCountryCode ) . '\'' ) : '\'US\'';
    }

    public function setOwnerPostalCode( $strOwnerPostalCode ) {
        $this->set( 'm_strOwnerPostalCode', CStrings::strTrimDef( $strOwnerPostalCode, 20, NULL, true ) );
    }

    public function getOwnerPostalCode() {
        return $this->m_strOwnerPostalCode;
    }

    public function sqlOwnerPostalCode() {
        return ( true == isset( $this->m_strOwnerPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOwnerPostalCode ) : '\'' . addslashes( $this->m_strOwnerPostalCode ) . '\'' ) : 'NULL';
    }

    public function setOwnerPhoneNumber( $strOwnerPhoneNumber ) {
        $this->set( 'm_strOwnerPhoneNumber', CStrings::strTrimDef( $strOwnerPhoneNumber, 30, NULL, true ) );
    }

    public function getOwnerPhoneNumber() {
        return $this->m_strOwnerPhoneNumber;
    }

    public function sqlOwnerPhoneNumber() {
        return ( true == isset( $this->m_strOwnerPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOwnerPhoneNumber ) : '\'' . addslashes( $this->m_strOwnerPhoneNumber ) . '\'' ) : 'NULL';
    }

    public function setOwnerEmail( $strOwnerEmail ) {
        $this->set( 'm_strOwnerEmail', CStrings::strTrimDef( $strOwnerEmail, 255, NULL, true ) );
    }

    public function getOwnerEmail() {
        return $this->m_strOwnerEmail;
    }

    public function sqlOwnerEmail() {
        return ( true == isset( $this->m_strOwnerEmail ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOwnerEmail ) : '\'' . addslashes( $this->m_strOwnerEmail ) . '\'' ) : 'NULL';
    }

    public function setDeletedBy( $intDeletedBy ) {
        $this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
    }

    public function getDeletedBy() {
        return $this->m_intDeletedBy;
    }

    public function sqlDeletedBy() {
        return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
    }

    public function setDeletedOn( $strDeletedOn ) {
        $this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
    }

    public function getDeletedOn() {
        return $this->m_strDeletedOn;
    }

    public function sqlDeletedOn() {
        return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function setDlNumberEncrypted( $strDlNumberEncrypted ) {
        $this->set( 'm_strDlNumberEncrypted', CStrings::strTrimDef( $strDlNumberEncrypted, 240, NULL, true ) );
    }

    public function getDlNumberEncrypted() {
        return $this->m_strDlNumberEncrypted;
    }

    public function sqlDlNumberEncrypted() {
        return ( true == isset( $this->m_strDlNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDlNumberEncrypted ) : '\'' . addslashes( $this->m_strDlNumberEncrypted ) . '\'' ) : 'NULL';
    }

    public function setDlStateCode( $strDlStateCode ) {
        $this->set( 'm_strDlStateCode', CStrings::strTrimDef( $strDlStateCode, 2, NULL, true ) );
    }

    public function getDlStateCode() {
        return $this->m_strDlStateCode;
    }

    public function sqlDlStateCode() {
        return ( true == isset( $this->m_strDlStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDlStateCode ) : '\'' . addslashes( $this->m_strDlStateCode ) . '\'' ) : 'NULL';
    }

    public function setAccountId( $intAccountId ) {
        $this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
    }

    public function getAccountId() {
        return $this->m_intAccountId;
    }

    public function sqlAccountId() {
        return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
    }

	public function setIsOwnerPep( $boolIsOwnerPep ) {
		$this->set( 'm_boolIsOwnerPep', CStrings::strToBool( $boolIsOwnerPep ) );
	}

	public function getIsOwnerPep() {
		return $this->m_boolIsOwnerPep;
	}

	public function sqlIsOwnerPep() {
		return ( true == isset( $this->m_boolIsOwnerPep ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOwnerPep ? 'true' : 'false' ) . '\'' : 'NULL';
	}

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $this->setDatabase( $objDatabase );

        $strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

        $strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, underwriting_type_id, owner_type, is_owner_control_individual, owner_title, owner_name, owner_ssn_encrypted, owner_dob, owner_ownership_percent, owner_address, owner_city, owner_state_code, owner_country_code, owner_postal_code, owner_phone_number, owner_email, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, dl_number_encrypted, dl_state_code, account_id, is_owner_pep )
					VALUES ( ' .
            $strId . ', ' .
            $this->sqlCid() . ', ' .
            $this->sqlPropertyId() . ', ' .
            $this->sqlUnderwritingTypeId() . ', ' .
            $this->sqlOwnerType() . ', ' .
            $this->sqlIsOwnerControlIndividual() . ', ' .
            $this->sqlOwnerTitle() . ', ' .
            $this->sqlOwnerName() . ', ' .
            $this->sqlOwnerSsnEncrypted() . ', ' .
            $this->sqlOwnerDob() . ', ' .
            $this->sqlOwnerOwnershipPercent() . ', ' .
            $this->sqlOwnerAddress() . ', ' .
            $this->sqlOwnerCity() . ', ' .
            $this->sqlOwnerStateCode() . ', ' .
            $this->sqlOwnerCountryCode() . ', ' .
            $this->sqlOwnerPostalCode() . ', ' .
            $this->sqlOwnerPhoneNumber() . ', ' .
            $this->sqlOwnerEmail() . ', ' .
            $this->sqlDeletedBy() . ', ' .
            $this->sqlDeletedOn() . ', ' .
            ( int ) $intCurrentUserId . ', ' .
            $this->sqlUpdatedOn() . ', ' .
            ( int ) $intCurrentUserId . ', ' .
            $this->sqlCreatedOn() . ', ' .
            $this->sqlDlNumberEncrypted() . ', ' .
            $this->sqlDlStateCode() . ', ' .
            $this->sqlAccountId() . ', ' .
            $this->sqlIsOwnerPep() . ' ) ' . ' RETURNING id;';

	    if( true == $boolReturnSqlOnly ) {
            return $strSql;
        } else {
            return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        if( false == $this->getAllowDifferentialUpdate() ) {
            $boolUpdate = true;
        } else {
            $boolUpdate = false;
        }

        $strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' underwriting_type_id = ' . $this->sqlUnderwritingTypeId(). ',' ; } elseif( true == array_key_exists( 'UnderwritingTypeId', $this->getChangedColumns() ) ) { $strSql .= ' underwriting_type_id = ' . $this->sqlUnderwritingTypeId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_type = ' . $this->sqlOwnerType(). ',' ; } elseif( true == array_key_exists( 'OwnerType', $this->getChangedColumns() ) ) { $strSql .= ' owner_type = ' . $this->sqlOwnerType() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_owner_control_individual = ' . $this->sqlIsOwnerControlIndividual(). ',' ; } elseif( true == array_key_exists( 'IsOwnerControlIndividual', $this->getChangedColumns() ) ) { $strSql .= ' is_owner_control_individual = ' . $this->sqlIsOwnerControlIndividual() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_title = ' . $this->sqlOwnerTitle(). ',' ; } elseif( true == array_key_exists( 'OwnerTitle', $this->getChangedColumns() ) ) { $strSql .= ' owner_title = ' . $this->sqlOwnerTitle() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_name = ' . $this->sqlOwnerName(). ',' ; } elseif( true == array_key_exists( 'OwnerName', $this->getChangedColumns() ) ) { $strSql .= ' owner_name = ' . $this->sqlOwnerName() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_ssn_encrypted = ' . $this->sqlOwnerSsnEncrypted(). ',' ; } elseif( true == array_key_exists( 'OwnerSsnEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' owner_ssn_encrypted = ' . $this->sqlOwnerSsnEncrypted() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_dob = ' . $this->sqlOwnerDob(). ',' ; } elseif( true == array_key_exists( 'OwnerDob', $this->getChangedColumns() ) ) { $strSql .= ' owner_dob = ' . $this->sqlOwnerDob() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_ownership_percent = ' . $this->sqlOwnerOwnershipPercent(). ',' ; } elseif( true == array_key_exists( 'OwnerOwnershipPercent', $this->getChangedColumns() ) ) { $strSql .= ' owner_ownership_percent = ' . $this->sqlOwnerOwnershipPercent() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_address = ' . $this->sqlOwnerAddress(). ',' ; } elseif( true == array_key_exists( 'OwnerAddress', $this->getChangedColumns() ) ) { $strSql .= ' owner_address = ' . $this->sqlOwnerAddress() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_city = ' . $this->sqlOwnerCity(). ',' ; } elseif( true == array_key_exists( 'OwnerCity', $this->getChangedColumns() ) ) { $strSql .= ' owner_city = ' . $this->sqlOwnerCity() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_state_code = ' . $this->sqlOwnerStateCode(). ',' ; } elseif( true == array_key_exists( 'OwnerStateCode', $this->getChangedColumns() ) ) { $strSql .= ' owner_state_code = ' . $this->sqlOwnerStateCode() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_country_code = ' . $this->sqlOwnerCountryCode(). ',' ; } elseif( true == array_key_exists( 'OwnerCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' owner_country_code = ' . $this->sqlOwnerCountryCode() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_postal_code = ' . $this->sqlOwnerPostalCode(). ',' ; } elseif( true == array_key_exists( 'OwnerPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' owner_postal_code = ' . $this->sqlOwnerPostalCode() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_phone_number = ' . $this->sqlOwnerPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'OwnerPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' owner_phone_number = ' . $this->sqlOwnerPhoneNumber() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_email = ' . $this->sqlOwnerEmail(). ',' ; } elseif( true == array_key_exists( 'OwnerEmail', $this->getChangedColumns() ) ) { $strSql .= ' owner_email = ' . $this->sqlOwnerEmail() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dl_number_encrypted = ' . $this->sqlDlNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'DlNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' dl_number_encrypted = ' . $this->sqlDlNumberEncrypted() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dl_state_code = ' . $this->sqlDlStateCode(). ',' ; } elseif( true == array_key_exists( 'DlStateCode', $this->getChangedColumns() ) ) { $strSql .= ' dl_state_code = ' . $this->sqlDlStateCode() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
	    if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_owner_pep = ' . $this->sqlIsOwnerPep(). ',' ; } elseif( true == array_key_exists( 'IsOwnerPep', $this->getChangedColumns() ) ) { $strSql .= ' is_owner_pep = ' . $this->sqlIsOwnerPep() . ','; $boolUpdate = true; }
        $strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
        $strSql .= ' updated_on = \'NOW()\' ';

        $strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

        if( true == $boolReturnSqlOnly ) {
            return ( true == $boolUpdate ) ? $strSql : false;
        } else {
            if( true == $boolUpdate ) {
                if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
                    if( true == $this->getAllowDifferentialUpdate() ) {
                        $this->resetChangedColumns();
                        return true;
                    }
                } else {
                    return false;
                }
            }
            return true;
        }
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

        if( true == $boolReturnSqlOnly ) {
            return $strSql;
        } else {
            return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function toArray() {
        return array(
            'id' => $this->getId(),
            'cid' => $this->getCid(),
            'property_id' => $this->getPropertyId(),
            'underwriting_type_id' => $this->getUnderwritingTypeId(),
            'owner_type' => $this->getOwnerType(),
            'is_owner_control_individual' => $this->getIsOwnerControlIndividual(),
            'owner_title' => $this->getOwnerTitle(),
            'owner_name' => $this->getOwnerName(),
            'owner_ssn_encrypted' => $this->getOwnerSsnEncrypted(),
            'owner_dob' => $this->getOwnerDob(),
            'owner_ownership_percent' => $this->getOwnerOwnershipPercent(),
            'owner_address' => $this->getOwnerAddress(),
            'owner_city' => $this->getOwnerCity(),
            'owner_state_code' => $this->getOwnerStateCode(),
            'owner_country_code' => $this->getOwnerCountryCode(),
            'owner_postal_code' => $this->getOwnerPostalCode(),
            'owner_phone_number' => $this->getOwnerPhoneNumber(),
            'owner_email' => $this->getOwnerEmail(),
            'deleted_by' => $this->getDeletedBy(),
            'deleted_on' => $this->getDeletedOn(),
            'updated_by' => $this->getUpdatedBy(),
            'updated_on' => $this->getUpdatedOn(),
            'created_by' => $this->getCreatedBy(),
            'created_on' => $this->getCreatedOn(),
            'dl_number_encrypted' => $this->getDlNumberEncrypted(),
            'dl_state_code' => $this->getDlStateCode(),
            'account_id' => $this->getAccountId(),
            'is_owner_pep' => $this->getIsOwnerPep()
        );
    }

}
?>