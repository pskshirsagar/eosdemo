<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentDistributions
 * Do not add any new functions to this class.
 */

class CBaseArPaymentDistributions extends CEosPluralBase {

	/**
	 * @return CArPaymentDistribution[]
	 */
	public static function fetchArPaymentDistributions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CArPaymentDistribution::class, $objDatabase );
	}

	/**
	 * @return CArPaymentDistribution
	 */
	public static function fetchArPaymentDistribution( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CArPaymentDistribution::class, $objDatabase );
	}

	public static function fetchArPaymentDistributionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ar_payment_distributions', $objDatabase );
	}

	public static function fetchArPaymentDistributionById( $intId, $objDatabase ) {
		return self::fetchArPaymentDistribution( sprintf( 'SELECT * FROM ar_payment_distributions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchArPaymentDistributionsByCid( $intCid, $objDatabase ) {
		return self::fetchArPaymentDistributions( sprintf( 'SELECT * FROM ar_payment_distributions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentDistributionsByProcessingBankId( $intProcessingBankId, $objDatabase ) {
		return self::fetchArPaymentDistributions( sprintf( 'SELECT * FROM ar_payment_distributions WHERE processing_bank_id = %d', ( int ) $intProcessingBankId ), $objDatabase );
	}

	public static function fetchArPaymentDistributionsByProcessingBankAccountId( $intProcessingBankAccountId, $objDatabase ) {
		return self::fetchArPaymentDistributions( sprintf( 'SELECT * FROM ar_payment_distributions WHERE processing_bank_account_id = %d', ( int ) $intProcessingBankAccountId ), $objDatabase );
	}

	public static function fetchArPaymentDistributionsBySettlementDistributionId( $intSettlementDistributionId, $objDatabase ) {
		return self::fetchArPaymentDistributions( sprintf( 'SELECT * FROM ar_payment_distributions WHERE settlement_distribution_id = %d', ( int ) $intSettlementDistributionId ), $objDatabase );
	}

	public static function fetchArPaymentDistributionsByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchArPaymentDistributions( sprintf( 'SELECT * FROM ar_payment_distributions WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

	public static function fetchArPaymentDistributionsByPaymentDistributionTypeId( $intPaymentDistributionTypeId, $objDatabase ) {
		return self::fetchArPaymentDistributions( sprintf( 'SELECT * FROM ar_payment_distributions WHERE payment_distribution_type_id = %d', ( int ) $intPaymentDistributionTypeId ), $objDatabase );
	}

}
?>