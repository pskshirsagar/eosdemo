<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentStatistics
 * Do not add any new functions to this class.
 */

class CBaseArPaymentStatistics extends CEosPluralBase {

	/**
	 * @return CArPaymentStatistic[]
	 */
	public static function fetchArPaymentStatistics( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CArPaymentStatistic::class, $objDatabase );
	}

	/**
	 * @return CArPaymentStatistic
	 */
	public static function fetchArPaymentStatistic( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CArPaymentStatistic::class, $objDatabase );
	}

	public static function fetchArPaymentStatisticCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ar_payment_statistics', $objDatabase );
	}

	public static function fetchArPaymentStatisticById( $intId, $objDatabase ) {
		return self::fetchArPaymentStatistic( sprintf( 'SELECT * FROM ar_payment_statistics WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>