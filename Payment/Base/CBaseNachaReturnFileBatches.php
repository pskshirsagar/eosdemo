<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaReturnFileBatches
 * Do not add any new functions to this class.
 */

class CBaseNachaReturnFileBatches extends CEosPluralBase {

	/**
	 * @return CNachaReturnFileBatch[]
	 */
	public static function fetchNachaReturnFileBatches( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CNachaReturnFileBatch::class, $objDatabase );
	}

	/**
	 * @return CNachaReturnFileBatch
	 */
	public static function fetchNachaReturnFileBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CNachaReturnFileBatch::class, $objDatabase );
	}

	public static function fetchNachaReturnFileBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'nacha_return_file_batches', $objDatabase );
	}

	public static function fetchNachaReturnFileBatchById( $intId, $objDatabase ) {
		return self::fetchNachaReturnFileBatch( sprintf( 'SELECT * FROM nacha_return_file_batches WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchNachaReturnFileBatchesByNachaReturnFileId( $intNachaReturnFileId, $objDatabase ) {
		return self::fetchNachaReturnFileBatches( sprintf( 'SELECT * FROM nacha_return_file_batches WHERE nacha_return_file_id = %d', ( int ) $intNachaReturnFileId ), $objDatabase );
	}

	public static function fetchNachaReturnFileBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchNachaReturnFileBatches( sprintf( 'SELECT * FROM nacha_return_file_batches WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>