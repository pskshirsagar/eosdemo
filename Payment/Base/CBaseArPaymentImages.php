<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentImages
 * Do not add any new functions to this class.
 */

class CBaseArPaymentImages extends CEosPluralBase {

	/**
	 * @return CArPaymentImage[]
	 */
	public static function fetchArPaymentImages( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CArPaymentImage::class, $objDatabase );
	}

	/**
	 * @return CArPaymentImage
	 */
	public static function fetchArPaymentImage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CArPaymentImage::class, $objDatabase );
	}

	public static function fetchArPaymentImageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ar_payment_images', $objDatabase );
	}

	public static function fetchArPaymentImageById( $intId, $objDatabase ) {
		return self::fetchArPaymentImage( sprintf( 'SELECT * FROM ar_payment_images WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchArPaymentImagesByCid( $intCid, $objDatabase ) {
		return self::fetchArPaymentImages( sprintf( 'SELECT * FROM ar_payment_images WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentImagesByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchArPaymentImages( sprintf( 'SELECT * FROM ar_payment_images WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

	public static function fetchArPaymentImagesByFileExtensionId( $intFileExtensionId, $objDatabase ) {
		return self::fetchArPaymentImages( sprintf( 'SELECT * FROM ar_payment_images WHERE file_extension_id = %d', ( int ) $intFileExtensionId ), $objDatabase );
	}

	public static function fetchArPaymentImagesByPaymentImageTypeId( $intPaymentImageTypeId, $objDatabase ) {
		return self::fetchArPaymentImages( sprintf( 'SELECT * FROM ar_payment_images WHERE payment_image_type_id = %d', ( int ) $intPaymentImageTypeId ), $objDatabase );
	}

}
?>