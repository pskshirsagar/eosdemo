<?php

class CBaseWesternUnionInputRecord extends CEosSingularBase {

	const TABLE_NAME = 'public.western_union_input_records';

	protected $m_intId;
	protected $m_intFileId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_strRecordType;
	protected $m_strTransactionType;
	protected $m_strMoneyTransferType;
	protected $m_strNameFormatCode;
	protected $m_strCustomerFirstName;
	protected $m_strCustomerLastName;
	protected $m_strCustomerPaternalName;
	protected $m_strCustomerMaternalName;
	protected $m_strCustomerStreetAddress;
	protected $m_strCustomerCity;
	protected $m_strCustomerStateprovince;
	protected $m_strCustomerPostalCode;
	protected $m_strCustomerTelephone;
	protected $m_strCustomerCountry;
	protected $m_strCustomerEmailAddress;
	protected $m_strClientOrderNumber;
	protected $m_strClientReference;
	protected $m_strPurchaseAmount;
	protected $m_strCodeCity;
	protected $m_strCodeState;
	protected $m_strOptInFlag;
	protected $m_strClientName;
	protected $m_strClientDate;
	protected $m_strClientTime;
	protected $m_strExpirationDate;
	protected $m_strExpirationTime;
	protected $m_strCancelledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['file_id'] ) && $boolDirectSet ) $this->set( 'm_intFileId', trim( $arrValues['file_id'] ) ); elseif( isset( $arrValues['file_id'] ) ) $this->setFileId( $arrValues['file_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['record_type'] ) && $boolDirectSet ) $this->set( 'm_strRecordType', trim( stripcslashes( $arrValues['record_type'] ) ) ); elseif( isset( $arrValues['record_type'] ) ) $this->setRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_type'] ) : $arrValues['record_type'] );
		if( isset( $arrValues['transaction_type'] ) && $boolDirectSet ) $this->set( 'm_strTransactionType', trim( stripcslashes( $arrValues['transaction_type'] ) ) ); elseif( isset( $arrValues['transaction_type'] ) ) $this->setTransactionType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['transaction_type'] ) : $arrValues['transaction_type'] );
		if( isset( $arrValues['money_transfer_type'] ) && $boolDirectSet ) $this->set( 'm_strMoneyTransferType', trim( stripcslashes( $arrValues['money_transfer_type'] ) ) ); elseif( isset( $arrValues['money_transfer_type'] ) ) $this->setMoneyTransferType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['money_transfer_type'] ) : $arrValues['money_transfer_type'] );
		if( isset( $arrValues['name_format_code'] ) && $boolDirectSet ) $this->set( 'm_strNameFormatCode', trim( stripcslashes( $arrValues['name_format_code'] ) ) ); elseif( isset( $arrValues['name_format_code'] ) ) $this->setNameFormatCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_format_code'] ) : $arrValues['name_format_code'] );
		if( isset( $arrValues['customer_first_name'] ) && $boolDirectSet ) $this->set( 'm_strCustomerFirstName', trim( stripcslashes( $arrValues['customer_first_name'] ) ) ); elseif( isset( $arrValues['customer_first_name'] ) ) $this->setCustomerFirstName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_first_name'] ) : $arrValues['customer_first_name'] );
		if( isset( $arrValues['customer_last_name'] ) && $boolDirectSet ) $this->set( 'm_strCustomerLastName', trim( stripcslashes( $arrValues['customer_last_name'] ) ) ); elseif( isset( $arrValues['customer_last_name'] ) ) $this->setCustomerLastName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_last_name'] ) : $arrValues['customer_last_name'] );
		if( isset( $arrValues['customer_paternal_name'] ) && $boolDirectSet ) $this->set( 'm_strCustomerPaternalName', trim( stripcslashes( $arrValues['customer_paternal_name'] ) ) ); elseif( isset( $arrValues['customer_paternal_name'] ) ) $this->setCustomerPaternalName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_paternal_name'] ) : $arrValues['customer_paternal_name'] );
		if( isset( $arrValues['customer_maternal_name'] ) && $boolDirectSet ) $this->set( 'm_strCustomerMaternalName', trim( stripcslashes( $arrValues['customer_maternal_name'] ) ) ); elseif( isset( $arrValues['customer_maternal_name'] ) ) $this->setCustomerMaternalName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_maternal_name'] ) : $arrValues['customer_maternal_name'] );
		if( isset( $arrValues['customer_street_address'] ) && $boolDirectSet ) $this->set( 'm_strCustomerStreetAddress', trim( stripcslashes( $arrValues['customer_street_address'] ) ) ); elseif( isset( $arrValues['customer_street_address'] ) ) $this->setCustomerStreetAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_street_address'] ) : $arrValues['customer_street_address'] );
		if( isset( $arrValues['customer_city'] ) && $boolDirectSet ) $this->set( 'm_strCustomerCity', trim( stripcslashes( $arrValues['customer_city'] ) ) ); elseif( isset( $arrValues['customer_city'] ) ) $this->setCustomerCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_city'] ) : $arrValues['customer_city'] );
		if( isset( $arrValues['customer_stateprovince'] ) && $boolDirectSet ) $this->set( 'm_strCustomerStateprovince', trim( stripcslashes( $arrValues['customer_stateprovince'] ) ) ); elseif( isset( $arrValues['customer_stateprovince'] ) ) $this->setCustomerStateprovince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_stateprovince'] ) : $arrValues['customer_stateprovince'] );
		if( isset( $arrValues['customer_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strCustomerPostalCode', trim( stripcslashes( $arrValues['customer_postal_code'] ) ) ); elseif( isset( $arrValues['customer_postal_code'] ) ) $this->setCustomerPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_postal_code'] ) : $arrValues['customer_postal_code'] );
		if( isset( $arrValues['customer_telephone'] ) && $boolDirectSet ) $this->set( 'm_strCustomerTelephone', trim( stripcslashes( $arrValues['customer_telephone'] ) ) ); elseif( isset( $arrValues['customer_telephone'] ) ) $this->setCustomerTelephone( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_telephone'] ) : $arrValues['customer_telephone'] );
		if( isset( $arrValues['customer_country'] ) && $boolDirectSet ) $this->set( 'm_strCustomerCountry', trim( stripcslashes( $arrValues['customer_country'] ) ) ); elseif( isset( $arrValues['customer_country'] ) ) $this->setCustomerCountry( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_country'] ) : $arrValues['customer_country'] );
		if( isset( $arrValues['customer_email_address'] ) && $boolDirectSet ) $this->set( 'm_strCustomerEmailAddress', trim( stripcslashes( $arrValues['customer_email_address'] ) ) ); elseif( isset( $arrValues['customer_email_address'] ) ) $this->setCustomerEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_email_address'] ) : $arrValues['customer_email_address'] );
		if( isset( $arrValues['client_order_number'] ) && $boolDirectSet ) $this->set( 'm_strClientOrderNumber', trim( stripcslashes( $arrValues['client_order_number'] ) ) ); elseif( isset( $arrValues['client_order_number'] ) ) $this->setClientOrderNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_order_number'] ) : $arrValues['client_order_number'] );
		if( isset( $arrValues['client_reference'] ) && $boolDirectSet ) $this->set( 'm_strClientReference', trim( stripcslashes( $arrValues['client_reference'] ) ) ); elseif( isset( $arrValues['client_reference'] ) ) $this->setClientReference( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_reference'] ) : $arrValues['client_reference'] );
		if( isset( $arrValues['purchase_amount'] ) && $boolDirectSet ) $this->set( 'm_strPurchaseAmount', trim( stripcslashes( $arrValues['purchase_amount'] ) ) ); elseif( isset( $arrValues['purchase_amount'] ) ) $this->setPurchaseAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['purchase_amount'] ) : $arrValues['purchase_amount'] );
		if( isset( $arrValues['code_city'] ) && $boolDirectSet ) $this->set( 'm_strCodeCity', trim( stripcslashes( $arrValues['code_city'] ) ) ); elseif( isset( $arrValues['code_city'] ) ) $this->setCodeCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['code_city'] ) : $arrValues['code_city'] );
		if( isset( $arrValues['code_state'] ) && $boolDirectSet ) $this->set( 'm_strCodeState', trim( stripcslashes( $arrValues['code_state'] ) ) ); elseif( isset( $arrValues['code_state'] ) ) $this->setCodeState( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['code_state'] ) : $arrValues['code_state'] );
		if( isset( $arrValues['opt_in_flag'] ) && $boolDirectSet ) $this->set( 'm_strOptInFlag', trim( stripcslashes( $arrValues['opt_in_flag'] ) ) ); elseif( isset( $arrValues['opt_in_flag'] ) ) $this->setOptInFlag( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['opt_in_flag'] ) : $arrValues['opt_in_flag'] );
		if( isset( $arrValues['client_name'] ) && $boolDirectSet ) $this->set( 'm_strClientName', trim( stripcslashes( $arrValues['client_name'] ) ) ); elseif( isset( $arrValues['client_name'] ) ) $this->setClientName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_name'] ) : $arrValues['client_name'] );
		if( isset( $arrValues['client_date'] ) && $boolDirectSet ) $this->set( 'm_strClientDate', trim( stripcslashes( $arrValues['client_date'] ) ) ); elseif( isset( $arrValues['client_date'] ) ) $this->setClientDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_date'] ) : $arrValues['client_date'] );
		if( isset( $arrValues['client_time'] ) && $boolDirectSet ) $this->set( 'm_strClientTime', trim( stripcslashes( $arrValues['client_time'] ) ) ); elseif( isset( $arrValues['client_time'] ) ) $this->setClientTime( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_time'] ) : $arrValues['client_time'] );
		if( isset( $arrValues['expiration_date'] ) && $boolDirectSet ) $this->set( 'm_strExpirationDate', trim( stripcslashes( $arrValues['expiration_date'] ) ) ); elseif( isset( $arrValues['expiration_date'] ) ) $this->setExpirationDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['expiration_date'] ) : $arrValues['expiration_date'] );
		if( isset( $arrValues['expiration_time'] ) && $boolDirectSet ) $this->set( 'm_strExpirationTime', trim( stripcslashes( $arrValues['expiration_time'] ) ) ); elseif( isset( $arrValues['expiration_time'] ) ) $this->setExpirationTime( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['expiration_time'] ) : $arrValues['expiration_time'] );
		if( isset( $arrValues['cancelled_on'] ) && $boolDirectSet ) $this->set( 'm_strCancelledOn', trim( $arrValues['cancelled_on'] ) ); elseif( isset( $arrValues['cancelled_on'] ) ) $this->setCancelledOn( $arrValues['cancelled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setFileId( $intFileId ) {
		$this->set( 'm_intFileId', CStrings::strToIntDef( $intFileId, NULL, false ) );
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function sqlFileId() {
		return ( true == isset( $this->m_intFileId ) ) ? ( string ) $this->m_intFileId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setRecordType( $strRecordType ) {
		$this->set( 'm_strRecordType', CStrings::strTrimDef( $strRecordType, 1, NULL, true ) );
	}

	public function getRecordType() {
		return $this->m_strRecordType;
	}

	public function sqlRecordType() {
		return ( true == isset( $this->m_strRecordType ) ) ? '\'' . addslashes( $this->m_strRecordType ) . '\'' : 'NULL';
	}

	public function setTransactionType( $strTransactionType ) {
		$this->set( 'm_strTransactionType', CStrings::strTrimDef( $strTransactionType, 1, NULL, true ) );
	}

	public function getTransactionType() {
		return $this->m_strTransactionType;
	}

	public function sqlTransactionType() {
		return ( true == isset( $this->m_strTransactionType ) ) ? '\'' . addslashes( $this->m_strTransactionType ) . '\'' : 'NULL';
	}

	public function setMoneyTransferType( $strMoneyTransferType ) {
		$this->set( 'm_strMoneyTransferType', CStrings::strTrimDef( $strMoneyTransferType, 6, NULL, true ) );
	}

	public function getMoneyTransferType() {
		return $this->m_strMoneyTransferType;
	}

	public function sqlMoneyTransferType() {
		return ( true == isset( $this->m_strMoneyTransferType ) ) ? '\'' . addslashes( $this->m_strMoneyTransferType ) . '\'' : 'NULL';
	}

	public function setNameFormatCode( $strNameFormatCode ) {
		$this->set( 'm_strNameFormatCode', CStrings::strTrimDef( $strNameFormatCode, 1, NULL, true ) );
	}

	public function getNameFormatCode() {
		return $this->m_strNameFormatCode;
	}

	public function sqlNameFormatCode() {
		return ( true == isset( $this->m_strNameFormatCode ) ) ? '\'' . addslashes( $this->m_strNameFormatCode ) . '\'' : 'NULL';
	}

	public function setCustomerFirstName( $strCustomerFirstName ) {
		$this->set( 'm_strCustomerFirstName', CStrings::strTrimDef( $strCustomerFirstName, 20, NULL, true ) );
	}

	public function getCustomerFirstName() {
		return $this->m_strCustomerFirstName;
	}

	public function sqlCustomerFirstName() {
		return ( true == isset( $this->m_strCustomerFirstName ) ) ? '\'' . addslashes( $this->m_strCustomerFirstName ) . '\'' : 'NULL';
	}

	public function setCustomerLastName( $strCustomerLastName ) {
		$this->set( 'm_strCustomerLastName', CStrings::strTrimDef( $strCustomerLastName, 20, NULL, true ) );
	}

	public function getCustomerLastName() {
		return $this->m_strCustomerLastName;
	}

	public function sqlCustomerLastName() {
		return ( true == isset( $this->m_strCustomerLastName ) ) ? '\'' . addslashes( $this->m_strCustomerLastName ) . '\'' : 'NULL';
	}

	public function setCustomerPaternalName( $strCustomerPaternalName ) {
		$this->set( 'm_strCustomerPaternalName', CStrings::strTrimDef( $strCustomerPaternalName, 20, NULL, true ) );
	}

	public function getCustomerPaternalName() {
		return $this->m_strCustomerPaternalName;
	}

	public function sqlCustomerPaternalName() {
		return ( true == isset( $this->m_strCustomerPaternalName ) ) ? '\'' . addslashes( $this->m_strCustomerPaternalName ) . '\'' : 'NULL';
	}

	public function setCustomerMaternalName( $strCustomerMaternalName ) {
		$this->set( 'm_strCustomerMaternalName', CStrings::strTrimDef( $strCustomerMaternalName, 20, NULL, true ) );
	}

	public function getCustomerMaternalName() {
		return $this->m_strCustomerMaternalName;
	}

	public function sqlCustomerMaternalName() {
		return ( true == isset( $this->m_strCustomerMaternalName ) ) ? '\'' . addslashes( $this->m_strCustomerMaternalName ) . '\'' : 'NULL';
	}

	public function setCustomerStreetAddress( $strCustomerStreetAddress ) {
		$this->set( 'm_strCustomerStreetAddress', CStrings::strTrimDef( $strCustomerStreetAddress, 40, NULL, true ) );
	}

	public function getCustomerStreetAddress() {
		return $this->m_strCustomerStreetAddress;
	}

	public function sqlCustomerStreetAddress() {
		return ( true == isset( $this->m_strCustomerStreetAddress ) ) ? '\'' . addslashes( $this->m_strCustomerStreetAddress ) . '\'' : 'NULL';
	}

	public function setCustomerCity( $strCustomerCity ) {
		$this->set( 'm_strCustomerCity', CStrings::strTrimDef( $strCustomerCity, 24, NULL, true ) );
	}

	public function getCustomerCity() {
		return $this->m_strCustomerCity;
	}

	public function sqlCustomerCity() {
		return ( true == isset( $this->m_strCustomerCity ) ) ? '\'' . addslashes( $this->m_strCustomerCity ) . '\'' : 'NULL';
	}

	public function setCustomerStateprovince( $strCustomerStateprovince ) {
		$this->set( 'm_strCustomerStateprovince', CStrings::strTrimDef( $strCustomerStateprovince, 10, NULL, true ) );
	}

	public function getCustomerStateprovince() {
		return $this->m_strCustomerStateprovince;
	}

	public function sqlCustomerStateprovince() {
		return ( true == isset( $this->m_strCustomerStateprovince ) ) ? '\'' . addslashes( $this->m_strCustomerStateprovince ) . '\'' : 'NULL';
	}

	public function setCustomerPostalCode( $strCustomerPostalCode ) {
		$this->set( 'm_strCustomerPostalCode', CStrings::strTrimDef( $strCustomerPostalCode, 10, NULL, true ) );
	}

	public function getCustomerPostalCode() {
		return $this->m_strCustomerPostalCode;
	}

	public function sqlCustomerPostalCode() {
		return ( true == isset( $this->m_strCustomerPostalCode ) ) ? '\'' . addslashes( $this->m_strCustomerPostalCode ) . '\'' : 'NULL';
	}

	public function setCustomerTelephone( $strCustomerTelephone ) {
		$this->set( 'm_strCustomerTelephone', CStrings::strTrimDef( $strCustomerTelephone, 17, NULL, true ) );
	}

	public function getCustomerTelephone() {
		return $this->m_strCustomerTelephone;
	}

	public function sqlCustomerTelephone() {
		return ( true == isset( $this->m_strCustomerTelephone ) ) ? '\'' . addslashes( $this->m_strCustomerTelephone ) . '\'' : 'NULL';
	}

	public function setCustomerCountry( $strCustomerCountry ) {
		$this->set( 'm_strCustomerCountry', CStrings::strTrimDef( $strCustomerCountry, 2, NULL, true ) );
	}

	public function getCustomerCountry() {
		return $this->m_strCustomerCountry;
	}

	public function sqlCustomerCountry() {
		return ( true == isset( $this->m_strCustomerCountry ) ) ? '\'' . addslashes( $this->m_strCustomerCountry ) . '\'' : 'NULL';
	}

	public function setCustomerEmailAddress( $strCustomerEmailAddress ) {
		$this->set( 'm_strCustomerEmailAddress', CStrings::strTrimDef( $strCustomerEmailAddress, 80, NULL, true ) );
	}

	public function getCustomerEmailAddress() {
		return $this->m_strCustomerEmailAddress;
	}

	public function sqlCustomerEmailAddress() {
		return ( true == isset( $this->m_strCustomerEmailAddress ) ) ? '\'' . addslashes( $this->m_strCustomerEmailAddress ) . '\'' : 'NULL';
	}

	public function setClientOrderNumber( $strClientOrderNumber ) {
		$this->set( 'm_strClientOrderNumber', CStrings::strTrimDef( $strClientOrderNumber, 24, NULL, true ) );
	}

	public function getClientOrderNumber() {
		return $this->m_strClientOrderNumber;
	}

	public function sqlClientOrderNumber() {
		return ( true == isset( $this->m_strClientOrderNumber ) ) ? '\'' . addslashes( $this->m_strClientOrderNumber ) . '\'' : 'NULL';
	}

	public function setClientReference( $strClientReference ) {
		$this->set( 'm_strClientReference', CStrings::strTrimDef( $strClientReference, 24, NULL, true ) );
	}

	public function getClientReference() {
		return $this->m_strClientReference;
	}

	public function sqlClientReference() {
		return ( true == isset( $this->m_strClientReference ) ) ? '\'' . addslashes( $this->m_strClientReference ) . '\'' : 'NULL';
	}

	public function setPurchaseAmount( $strPurchaseAmount ) {
		$this->set( 'm_strPurchaseAmount', CStrings::strTrimDef( $strPurchaseAmount, 10, NULL, true ) );
	}

	public function getPurchaseAmount() {
		return $this->m_strPurchaseAmount;
	}

	public function sqlPurchaseAmount() {
		return ( true == isset( $this->m_strPurchaseAmount ) ) ? '\'' . addslashes( $this->m_strPurchaseAmount ) . '\'' : 'NULL';
	}

	public function setCodeCity( $strCodeCity ) {
		$this->set( 'm_strCodeCity', CStrings::strTrimDef( $strCodeCity, 24, NULL, true ) );
	}

	public function getCodeCity() {
		return $this->m_strCodeCity;
	}

	public function sqlCodeCity() {
		return ( true == isset( $this->m_strCodeCity ) ) ? '\'' . addslashes( $this->m_strCodeCity ) . '\'' : 'NULL';
	}

	public function setCodeState( $strCodeState ) {
		$this->set( 'm_strCodeState', CStrings::strTrimDef( $strCodeState, 2, NULL, true ) );
	}

	public function getCodeState() {
		return $this->m_strCodeState;
	}

	public function sqlCodeState() {
		return ( true == isset( $this->m_strCodeState ) ) ? '\'' . addslashes( $this->m_strCodeState ) . '\'' : 'NULL';
	}

	public function setOptInFlag( $strOptInFlag ) {
		$this->set( 'm_strOptInFlag', CStrings::strTrimDef( $strOptInFlag, 1, NULL, true ) );
	}

	public function getOptInFlag() {
		return $this->m_strOptInFlag;
	}

	public function sqlOptInFlag() {
		return ( true == isset( $this->m_strOptInFlag ) ) ? '\'' . addslashes( $this->m_strOptInFlag ) . '\'' : 'NULL';
	}

	public function setClientName( $strClientName ) {
		$this->set( 'm_strClientName', CStrings::strTrimDef( $strClientName, 30, NULL, true ) );
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function sqlClientName() {
		return ( true == isset( $this->m_strClientName ) ) ? '\'' . addslashes( $this->m_strClientName ) . '\'' : 'NULL';
	}

	public function setClientDate( $strClientDate ) {
		$this->set( 'm_strClientDate', CStrings::strTrimDef( $strClientDate, 8, NULL, true ) );
	}

	public function getClientDate() {
		return $this->m_strClientDate;
	}

	public function sqlClientDate() {
		return ( true == isset( $this->m_strClientDate ) ) ? '\'' . addslashes( $this->m_strClientDate ) . '\'' : 'NULL';
	}

	public function setClientTime( $strClientTime ) {
		$this->set( 'm_strClientTime', CStrings::strTrimDef( $strClientTime, 6, NULL, true ) );
	}

	public function getClientTime() {
		return $this->m_strClientTime;
	}

	public function sqlClientTime() {
		return ( true == isset( $this->m_strClientTime ) ) ? '\'' . addslashes( $this->m_strClientTime ) . '\'' : 'NULL';
	}

	public function setExpirationDate( $strExpirationDate ) {
		$this->set( 'm_strExpirationDate', CStrings::strTrimDef( $strExpirationDate, 8, NULL, true ) );
	}

	public function getExpirationDate() {
		return $this->m_strExpirationDate;
	}

	public function sqlExpirationDate() {
		return ( true == isset( $this->m_strExpirationDate ) ) ? '\'' . addslashes( $this->m_strExpirationDate ) . '\'' : 'NULL';
	}

	public function setExpirationTime( $strExpirationTime ) {
		$this->set( 'm_strExpirationTime', CStrings::strTrimDef( $strExpirationTime, 6, NULL, true ) );
	}

	public function getExpirationTime() {
		return $this->m_strExpirationTime;
	}

	public function sqlExpirationTime() {
		return ( true == isset( $this->m_strExpirationTime ) ) ? '\'' . addslashes( $this->m_strExpirationTime ) . '\'' : 'NULL';
	}

	public function setCancelledOn( $strCancelledOn ) {
		$this->set( 'm_strCancelledOn', CStrings::strTrimDef( $strCancelledOn, -1, NULL, true ) );
	}

	public function getCancelledOn() {
		return $this->m_strCancelledOn;
	}

	public function sqlCancelledOn() {
		return ( true == isset( $this->m_strCancelledOn ) ) ? '\'' . $this->m_strCancelledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, file_id, cid, property_id, customer_id, lease_id, record_type, transaction_type, money_transfer_type, name_format_code, customer_first_name, customer_last_name, customer_paternal_name, customer_maternal_name, customer_street_address, customer_city, customer_stateprovince, customer_postal_code, customer_telephone, customer_country, customer_email_address, client_order_number, client_reference, purchase_amount, code_city, code_state, opt_in_flag, client_name, client_date, client_time, expiration_date, expiration_time, cancelled_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlFileId() . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlRecordType() . ', ' .
 						$this->sqlTransactionType() . ', ' .
 						$this->sqlMoneyTransferType() . ', ' .
 						$this->sqlNameFormatCode() . ', ' .
 						$this->sqlCustomerFirstName() . ', ' .
 						$this->sqlCustomerLastName() . ', ' .
 						$this->sqlCustomerPaternalName() . ', ' .
 						$this->sqlCustomerMaternalName() . ', ' .
 						$this->sqlCustomerStreetAddress() . ', ' .
 						$this->sqlCustomerCity() . ', ' .
 						$this->sqlCustomerStateprovince() . ', ' .
 						$this->sqlCustomerPostalCode() . ', ' .
 						$this->sqlCustomerTelephone() . ', ' .
 						$this->sqlCustomerCountry() . ', ' .
 						$this->sqlCustomerEmailAddress() . ', ' .
 						$this->sqlClientOrderNumber() . ', ' .
 						$this->sqlClientReference() . ', ' .
 						$this->sqlPurchaseAmount() . ', ' .
 						$this->sqlCodeCity() . ', ' .
 						$this->sqlCodeState() . ', ' .
 						$this->sqlOptInFlag() . ', ' .
 						$this->sqlClientName() . ', ' .
 						$this->sqlClientDate() . ', ' .
 						$this->sqlClientTime() . ', ' .
 						$this->sqlExpirationDate() . ', ' .
 						$this->sqlExpirationTime() . ', ' .
 						$this->sqlCancelledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; } elseif( true == array_key_exists( 'FileId', $this->getChangedColumns() ) ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; } elseif( true == array_key_exists( 'RecordType', $this->getChangedColumns() ) ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_type = ' . $this->sqlTransactionType() . ','; } elseif( true == array_key_exists( 'TransactionType', $this->getChangedColumns() ) ) { $strSql .= ' transaction_type = ' . $this->sqlTransactionType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' money_transfer_type = ' . $this->sqlMoneyTransferType() . ','; } elseif( true == array_key_exists( 'MoneyTransferType', $this->getChangedColumns() ) ) { $strSql .= ' money_transfer_type = ' . $this->sqlMoneyTransferType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_format_code = ' . $this->sqlNameFormatCode() . ','; } elseif( true == array_key_exists( 'NameFormatCode', $this->getChangedColumns() ) ) { $strSql .= ' name_format_code = ' . $this->sqlNameFormatCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_first_name = ' . $this->sqlCustomerFirstName() . ','; } elseif( true == array_key_exists( 'CustomerFirstName', $this->getChangedColumns() ) ) { $strSql .= ' customer_first_name = ' . $this->sqlCustomerFirstName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_last_name = ' . $this->sqlCustomerLastName() . ','; } elseif( true == array_key_exists( 'CustomerLastName', $this->getChangedColumns() ) ) { $strSql .= ' customer_last_name = ' . $this->sqlCustomerLastName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_paternal_name = ' . $this->sqlCustomerPaternalName() . ','; } elseif( true == array_key_exists( 'CustomerPaternalName', $this->getChangedColumns() ) ) { $strSql .= ' customer_paternal_name = ' . $this->sqlCustomerPaternalName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_maternal_name = ' . $this->sqlCustomerMaternalName() . ','; } elseif( true == array_key_exists( 'CustomerMaternalName', $this->getChangedColumns() ) ) { $strSql .= ' customer_maternal_name = ' . $this->sqlCustomerMaternalName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_street_address = ' . $this->sqlCustomerStreetAddress() . ','; } elseif( true == array_key_exists( 'CustomerStreetAddress', $this->getChangedColumns() ) ) { $strSql .= ' customer_street_address = ' . $this->sqlCustomerStreetAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_city = ' . $this->sqlCustomerCity() . ','; } elseif( true == array_key_exists( 'CustomerCity', $this->getChangedColumns() ) ) { $strSql .= ' customer_city = ' . $this->sqlCustomerCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_stateprovince = ' . $this->sqlCustomerStateprovince() . ','; } elseif( true == array_key_exists( 'CustomerStateprovince', $this->getChangedColumns() ) ) { $strSql .= ' customer_stateprovince = ' . $this->sqlCustomerStateprovince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_postal_code = ' . $this->sqlCustomerPostalCode() . ','; } elseif( true == array_key_exists( 'CustomerPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' customer_postal_code = ' . $this->sqlCustomerPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_telephone = ' . $this->sqlCustomerTelephone() . ','; } elseif( true == array_key_exists( 'CustomerTelephone', $this->getChangedColumns() ) ) { $strSql .= ' customer_telephone = ' . $this->sqlCustomerTelephone() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_country = ' . $this->sqlCustomerCountry() . ','; } elseif( true == array_key_exists( 'CustomerCountry', $this->getChangedColumns() ) ) { $strSql .= ' customer_country = ' . $this->sqlCustomerCountry() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_email_address = ' . $this->sqlCustomerEmailAddress() . ','; } elseif( true == array_key_exists( 'CustomerEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' customer_email_address = ' . $this->sqlCustomerEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_order_number = ' . $this->sqlClientOrderNumber() . ','; } elseif( true == array_key_exists( 'ClientOrderNumber', $this->getChangedColumns() ) ) { $strSql .= ' client_order_number = ' . $this->sqlClientOrderNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_reference = ' . $this->sqlClientReference() . ','; } elseif( true == array_key_exists( 'ClientReference', $this->getChangedColumns() ) ) { $strSql .= ' client_reference = ' . $this->sqlClientReference() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchase_amount = ' . $this->sqlPurchaseAmount() . ','; } elseif( true == array_key_exists( 'PurchaseAmount', $this->getChangedColumns() ) ) { $strSql .= ' purchase_amount = ' . $this->sqlPurchaseAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' code_city = ' . $this->sqlCodeCity() . ','; } elseif( true == array_key_exists( 'CodeCity', $this->getChangedColumns() ) ) { $strSql .= ' code_city = ' . $this->sqlCodeCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' code_state = ' . $this->sqlCodeState() . ','; } elseif( true == array_key_exists( 'CodeState', $this->getChangedColumns() ) ) { $strSql .= ' code_state = ' . $this->sqlCodeState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' opt_in_flag = ' . $this->sqlOptInFlag() . ','; } elseif( true == array_key_exists( 'OptInFlag', $this->getChangedColumns() ) ) { $strSql .= ' opt_in_flag = ' . $this->sqlOptInFlag() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_name = ' . $this->sqlClientName() . ','; } elseif( true == array_key_exists( 'ClientName', $this->getChangedColumns() ) ) { $strSql .= ' client_name = ' . $this->sqlClientName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_date = ' . $this->sqlClientDate() . ','; } elseif( true == array_key_exists( 'ClientDate', $this->getChangedColumns() ) ) { $strSql .= ' client_date = ' . $this->sqlClientDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_time = ' . $this->sqlClientTime() . ','; } elseif( true == array_key_exists( 'ClientTime', $this->getChangedColumns() ) ) { $strSql .= ' client_time = ' . $this->sqlClientTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expiration_date = ' . $this->sqlExpirationDate() . ','; } elseif( true == array_key_exists( 'ExpirationDate', $this->getChangedColumns() ) ) { $strSql .= ' expiration_date = ' . $this->sqlExpirationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expiration_time = ' . $this->sqlExpirationTime() . ','; } elseif( true == array_key_exists( 'ExpirationTime', $this->getChangedColumns() ) ) { $strSql .= ' expiration_time = ' . $this->sqlExpirationTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn() . ','; } elseif( true == array_key_exists( 'CancelledOn', $this->getChangedColumns() ) ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'file_id' => $this->getFileId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'customer_id' => $this->getCustomerId(),
			'lease_id' => $this->getLeaseId(),
			'record_type' => $this->getRecordType(),
			'transaction_type' => $this->getTransactionType(),
			'money_transfer_type' => $this->getMoneyTransferType(),
			'name_format_code' => $this->getNameFormatCode(),
			'customer_first_name' => $this->getCustomerFirstName(),
			'customer_last_name' => $this->getCustomerLastName(),
			'customer_paternal_name' => $this->getCustomerPaternalName(),
			'customer_maternal_name' => $this->getCustomerMaternalName(),
			'customer_street_address' => $this->getCustomerStreetAddress(),
			'customer_city' => $this->getCustomerCity(),
			'customer_stateprovince' => $this->getCustomerStateprovince(),
			'customer_postal_code' => $this->getCustomerPostalCode(),
			'customer_telephone' => $this->getCustomerTelephone(),
			'customer_country' => $this->getCustomerCountry(),
			'customer_email_address' => $this->getCustomerEmailAddress(),
			'client_order_number' => $this->getClientOrderNumber(),
			'client_reference' => $this->getClientReference(),
			'purchase_amount' => $this->getPurchaseAmount(),
			'code_city' => $this->getCodeCity(),
			'code_state' => $this->getCodeState(),
			'opt_in_flag' => $this->getOptInFlag(),
			'client_name' => $this->getClientName(),
			'client_date' => $this->getClientDate(),
			'client_time' => $this->getClientTime(),
			'expiration_date' => $this->getExpirationDate(),
			'expiration_time' => $this->getExpirationTime(),
			'cancelled_on' => $this->getCancelledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>