<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CEftChargeTypes
 * Do not add any new functions to this class.
 */

class CBaseEftChargeTypes extends CEosPluralBase {

	/**
	 * @return CEftChargeType[]
	 */
	public static function fetchEftChargeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEftChargeType::class, $objDatabase );
	}

	/**
	 * @return CEftChargeType
	 */
	public static function fetchEftChargeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEftChargeType::class, $objDatabase );
	}

	public static function fetchEftChargeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'eft_charge_types', $objDatabase );
	}

	public static function fetchEftChargeTypeById( $intId, $objDatabase ) {
		return self::fetchEftChargeType( sprintf( 'SELECT * FROM eft_charge_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>