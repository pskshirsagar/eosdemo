<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CWesternUnionFileTypes
 * Do not add any new functions to this class.
 */

class CBaseWesternUnionFileTypes extends CEosPluralBase {

	/**
	 * @return CWesternUnionFileType[]
	 */
	public static function fetchWesternUnionFileTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CWesternUnionFileType::class, $objDatabase );
	}

	/**
	 * @return CWesternUnionFileType
	 */
	public static function fetchWesternUnionFileType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CWesternUnionFileType::class, $objDatabase );
	}

	public static function fetchWesternUnionFileTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'western_union_file_types', $objDatabase );
	}

	public static function fetchWesternUnionFileTypeById( $intId, $objDatabase ) {
		return self::fetchWesternUnionFileType( sprintf( 'SELECT * FROM western_union_file_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>