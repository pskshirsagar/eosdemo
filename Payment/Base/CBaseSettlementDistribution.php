<?php

class CBaseSettlementDistribution extends CEosSingularBase {

	const TABLE_NAME = 'public.settlement_distributions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intProcessingBankId;
	protected $m_intPaymentTypeId;
	protected $m_intAccountId;
	protected $m_intCompanyMerchantAccountId;
	protected $m_intDistributionStatusTypeId;
	protected $m_intReturnTypeId;
	protected $m_strRemotePrimaryKey;
	protected $m_strDistributionDatetime;
	protected $m_strSettlementDatetime;
	protected $m_fltDistributionAmount;
	protected $m_strDistributionMemo;
	protected $m_strBilltoCompanyName;
	protected $m_strBilltoNameFirst;
	protected $m_strBilltoNameMiddle;
	protected $m_strBilltoNameLast;
	protected $m_strBilltoNameFull;
	protected $m_strBilltoStreetLine1;
	protected $m_strBilltoStreetLine2;
	protected $m_strBilltoStreetLine3;
	protected $m_strBilltoCity;
	protected $m_strBilltoStateCode;
	protected $m_strBilltoProvince;
	protected $m_strBilltoPostalCode;
	protected $m_strBilltoCountryCode;
	protected $m_strBilltoEmailAddress;
	protected $m_intCheckAccountTypeId;
	protected $m_strCheckNameOnAccount;
	protected $m_strCheckBankName;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckAccountNumberEncrypted;
	protected $m_intIsPostedToArDeposits;
	protected $m_intLockSequence;
	protected $m_strEmailedOn;
	protected $m_strConfirmedOn;
	protected $m_strBatchedOn;
	protected $m_strReturnedOn;
	protected $m_strExportedOn;
	protected $m_strFeePostedOn;
	protected $m_strReturnFeePostedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPostedToArDeposits = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['processing_bank_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankId', trim( $arrValues['processing_bank_id'] ) ); elseif( isset( $arrValues['processing_bank_id'] ) ) $this->setProcessingBankId( $arrValues['processing_bank_id'] );
		if( isset( $arrValues['payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentTypeId', trim( $arrValues['payment_type_id'] ) ); elseif( isset( $arrValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrValues['payment_type_id'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['company_merchant_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMerchantAccountId', trim( $arrValues['company_merchant_account_id'] ) ); elseif( isset( $arrValues['company_merchant_account_id'] ) ) $this->setCompanyMerchantAccountId( $arrValues['company_merchant_account_id'] );
		if( isset( $arrValues['distribution_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDistributionStatusTypeId', trim( $arrValues['distribution_status_type_id'] ) ); elseif( isset( $arrValues['distribution_status_type_id'] ) ) $this->setDistributionStatusTypeId( $arrValues['distribution_status_type_id'] );
		if( isset( $arrValues['return_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReturnTypeId', trim( $arrValues['return_type_id'] ) ); elseif( isset( $arrValues['return_type_id'] ) ) $this->setReturnTypeId( $arrValues['return_type_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['distribution_datetime'] ) && $boolDirectSet ) $this->set( 'm_strDistributionDatetime', trim( $arrValues['distribution_datetime'] ) ); elseif( isset( $arrValues['distribution_datetime'] ) ) $this->setDistributionDatetime( $arrValues['distribution_datetime'] );
		if( isset( $arrValues['settlement_datetime'] ) && $boolDirectSet ) $this->set( 'm_strSettlementDatetime', trim( $arrValues['settlement_datetime'] ) ); elseif( isset( $arrValues['settlement_datetime'] ) ) $this->setSettlementDatetime( $arrValues['settlement_datetime'] );
		if( isset( $arrValues['distribution_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDistributionAmount', trim( $arrValues['distribution_amount'] ) ); elseif( isset( $arrValues['distribution_amount'] ) ) $this->setDistributionAmount( $arrValues['distribution_amount'] );
		if( isset( $arrValues['distribution_memo'] ) && $boolDirectSet ) $this->set( 'm_strDistributionMemo', trim( $arrValues['distribution_memo'] ) ); elseif( isset( $arrValues['distribution_memo'] ) ) $this->setDistributionMemo( $arrValues['distribution_memo'] );
		if( isset( $arrValues['billto_company_name'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCompanyName', trim( $arrValues['billto_company_name'] ) ); elseif( isset( $arrValues['billto_company_name'] ) ) $this->setBilltoCompanyName( $arrValues['billto_company_name'] );
		if( isset( $arrValues['billto_name_first'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameFirst', trim( $arrValues['billto_name_first'] ) ); elseif( isset( $arrValues['billto_name_first'] ) ) $this->setBilltoNameFirst( $arrValues['billto_name_first'] );
		if( isset( $arrValues['billto_name_middle'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameMiddle', trim( $arrValues['billto_name_middle'] ) ); elseif( isset( $arrValues['billto_name_middle'] ) ) $this->setBilltoNameMiddle( $arrValues['billto_name_middle'] );
		if( isset( $arrValues['billto_name_last'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameLast', trim( $arrValues['billto_name_last'] ) ); elseif( isset( $arrValues['billto_name_last'] ) ) $this->setBilltoNameLast( $arrValues['billto_name_last'] );
		if( isset( $arrValues['billto_name_full'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameFull', trim( $arrValues['billto_name_full'] ) ); elseif( isset( $arrValues['billto_name_full'] ) ) $this->setBilltoNameFull( $arrValues['billto_name_full'] );
		if( isset( $arrValues['billto_street_line1'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStreetLine1', trim( $arrValues['billto_street_line1'] ) ); elseif( isset( $arrValues['billto_street_line1'] ) ) $this->setBilltoStreetLine1( $arrValues['billto_street_line1'] );
		if( isset( $arrValues['billto_street_line2'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStreetLine2', trim( $arrValues['billto_street_line2'] ) ); elseif( isset( $arrValues['billto_street_line2'] ) ) $this->setBilltoStreetLine2( $arrValues['billto_street_line2'] );
		if( isset( $arrValues['billto_street_line3'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStreetLine3', trim( $arrValues['billto_street_line3'] ) ); elseif( isset( $arrValues['billto_street_line3'] ) ) $this->setBilltoStreetLine3( $arrValues['billto_street_line3'] );
		if( isset( $arrValues['billto_city'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCity', trim( $arrValues['billto_city'] ) ); elseif( isset( $arrValues['billto_city'] ) ) $this->setBilltoCity( $arrValues['billto_city'] );
		if( isset( $arrValues['billto_state_code'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStateCode', trim( $arrValues['billto_state_code'] ) ); elseif( isset( $arrValues['billto_state_code'] ) ) $this->setBilltoStateCode( $arrValues['billto_state_code'] );
		if( isset( $arrValues['billto_province'] ) && $boolDirectSet ) $this->set( 'm_strBilltoProvince', trim( $arrValues['billto_province'] ) ); elseif( isset( $arrValues['billto_province'] ) ) $this->setBilltoProvince( $arrValues['billto_province'] );
		if( isset( $arrValues['billto_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strBilltoPostalCode', trim( $arrValues['billto_postal_code'] ) ); elseif( isset( $arrValues['billto_postal_code'] ) ) $this->setBilltoPostalCode( $arrValues['billto_postal_code'] );
		if( isset( $arrValues['billto_country_code'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCountryCode', trim( $arrValues['billto_country_code'] ) ); elseif( isset( $arrValues['billto_country_code'] ) ) $this->setBilltoCountryCode( $arrValues['billto_country_code'] );
		if( isset( $arrValues['billto_email_address'] ) && $boolDirectSet ) $this->set( 'm_strBilltoEmailAddress', trim( $arrValues['billto_email_address'] ) ); elseif( isset( $arrValues['billto_email_address'] ) ) $this->setBilltoEmailAddress( $arrValues['billto_email_address'] );
		if( isset( $arrValues['check_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckAccountTypeId', trim( $arrValues['check_account_type_id'] ) ); elseif( isset( $arrValues['check_account_type_id'] ) ) $this->setCheckAccountTypeId( $arrValues['check_account_type_id'] );
		if( isset( $arrValues['check_name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strCheckNameOnAccount', trim( $arrValues['check_name_on_account'] ) ); elseif( isset( $arrValues['check_name_on_account'] ) ) $this->setCheckNameOnAccount( $arrValues['check_name_on_account'] );
		if( isset( $arrValues['check_bank_name'] ) && $boolDirectSet ) $this->set( 'm_strCheckBankName', trim( $arrValues['check_bank_name'] ) ); elseif( isset( $arrValues['check_bank_name'] ) ) $this->setCheckBankName( $arrValues['check_bank_name'] );
		if( isset( $arrValues['check_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckRoutingNumber', trim( $arrValues['check_routing_number'] ) ); elseif( isset( $arrValues['check_routing_number'] ) ) $this->setCheckRoutingNumber( $arrValues['check_routing_number'] );
		if( isset( $arrValues['check_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberEncrypted', trim( $arrValues['check_account_number_encrypted'] ) ); elseif( isset( $arrValues['check_account_number_encrypted'] ) ) $this->setCheckAccountNumberEncrypted( $arrValues['check_account_number_encrypted'] );
		if( isset( $arrValues['is_posted_to_ar_deposits'] ) && $boolDirectSet ) $this->set( 'm_intIsPostedToArDeposits', trim( $arrValues['is_posted_to_ar_deposits'] ) ); elseif( isset( $arrValues['is_posted_to_ar_deposits'] ) ) $this->setIsPostedToArDeposits( $arrValues['is_posted_to_ar_deposits'] );
		if( isset( $arrValues['lock_sequence'] ) && $boolDirectSet ) $this->set( 'm_intLockSequence', trim( $arrValues['lock_sequence'] ) ); elseif( isset( $arrValues['lock_sequence'] ) ) $this->setLockSequence( $arrValues['lock_sequence'] );
		if( isset( $arrValues['emailed_on'] ) && $boolDirectSet ) $this->set( 'm_strEmailedOn', trim( $arrValues['emailed_on'] ) ); elseif( isset( $arrValues['emailed_on'] ) ) $this->setEmailedOn( $arrValues['emailed_on'] );
		if( isset( $arrValues['confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strConfirmedOn', trim( $arrValues['confirmed_on'] ) ); elseif( isset( $arrValues['confirmed_on'] ) ) $this->setConfirmedOn( $arrValues['confirmed_on'] );
		if( isset( $arrValues['batched_on'] ) && $boolDirectSet ) $this->set( 'm_strBatchedOn', trim( $arrValues['batched_on'] ) ); elseif( isset( $arrValues['batched_on'] ) ) $this->setBatchedOn( $arrValues['batched_on'] );
		if( isset( $arrValues['returned_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnedOn', trim( $arrValues['returned_on'] ) ); elseif( isset( $arrValues['returned_on'] ) ) $this->setReturnedOn( $arrValues['returned_on'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['fee_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strFeePostedOn', trim( $arrValues['fee_posted_on'] ) ); elseif( isset( $arrValues['fee_posted_on'] ) ) $this->setFeePostedOn( $arrValues['fee_posted_on'] );
		if( isset( $arrValues['return_fee_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnFeePostedOn', trim( $arrValues['return_fee_posted_on'] ) ); elseif( isset( $arrValues['return_fee_posted_on'] ) ) $this->setReturnFeePostedOn( $arrValues['return_fee_posted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setProcessingBankId( $intProcessingBankId ) {
		$this->set( 'm_intProcessingBankId', CStrings::strToIntDef( $intProcessingBankId, NULL, false ) );
	}

	public function getProcessingBankId() {
		return $this->m_intProcessingBankId;
	}

	public function sqlProcessingBankId() {
		return ( true == isset( $this->m_intProcessingBankId ) ) ? ( string ) $this->m_intProcessingBankId : 'NULL';
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->set( 'm_intPaymentTypeId', CStrings::strToIntDef( $intPaymentTypeId, NULL, false ) );
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function sqlPaymentTypeId() {
		return ( true == isset( $this->m_intPaymentTypeId ) ) ? ( string ) $this->m_intPaymentTypeId : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setCompanyMerchantAccountId( $intCompanyMerchantAccountId ) {
		$this->set( 'm_intCompanyMerchantAccountId', CStrings::strToIntDef( $intCompanyMerchantAccountId, NULL, false ) );
	}

	public function getCompanyMerchantAccountId() {
		return $this->m_intCompanyMerchantAccountId;
	}

	public function sqlCompanyMerchantAccountId() {
		return ( true == isset( $this->m_intCompanyMerchantAccountId ) ) ? ( string ) $this->m_intCompanyMerchantAccountId : 'NULL';
	}

	public function setDistributionStatusTypeId( $intDistributionStatusTypeId ) {
		$this->set( 'm_intDistributionStatusTypeId', CStrings::strToIntDef( $intDistributionStatusTypeId, NULL, false ) );
	}

	public function getDistributionStatusTypeId() {
		return $this->m_intDistributionStatusTypeId;
	}

	public function sqlDistributionStatusTypeId() {
		return ( true == isset( $this->m_intDistributionStatusTypeId ) ) ? ( string ) $this->m_intDistributionStatusTypeId : 'NULL';
	}

	public function setReturnTypeId( $intReturnTypeId ) {
		$this->set( 'm_intReturnTypeId', CStrings::strToIntDef( $intReturnTypeId, NULL, false ) );
	}

	public function getReturnTypeId() {
		return $this->m_intReturnTypeId;
	}

	public function sqlReturnTypeId() {
		return ( true == isset( $this->m_intReturnTypeId ) ) ? ( string ) $this->m_intReturnTypeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setDistributionDatetime( $strDistributionDatetime ) {
		$this->set( 'm_strDistributionDatetime', CStrings::strTrimDef( $strDistributionDatetime, -1, NULL, true ) );
	}

	public function getDistributionDatetime() {
		return $this->m_strDistributionDatetime;
	}

	public function sqlDistributionDatetime() {
		return ( true == isset( $this->m_strDistributionDatetime ) ) ? '\'' . $this->m_strDistributionDatetime . '\'' : 'NOW()';
	}

	public function setSettlementDatetime( $strSettlementDatetime ) {
		$this->set( 'm_strSettlementDatetime', CStrings::strTrimDef( $strSettlementDatetime, -1, NULL, true ) );
	}

	public function getSettlementDatetime() {
		return $this->m_strSettlementDatetime;
	}

	public function sqlSettlementDatetime() {
		return ( true == isset( $this->m_strSettlementDatetime ) ) ? '\'' . $this->m_strSettlementDatetime . '\'' : 'NULL';
	}

	public function setDistributionAmount( $fltDistributionAmount ) {
		$this->set( 'm_fltDistributionAmount', CStrings::strToFloatDef( $fltDistributionAmount, NULL, false, 4 ) );
	}

	public function getDistributionAmount() {
		return $this->m_fltDistributionAmount;
	}

	public function sqlDistributionAmount() {
		return ( true == isset( $this->m_fltDistributionAmount ) ) ? ( string ) $this->m_fltDistributionAmount : 'NULL';
	}

	public function setDistributionMemo( $strDistributionMemo ) {
		$this->set( 'm_strDistributionMemo', CStrings::strTrimDef( $strDistributionMemo, 2000, NULL, true ) );
	}

	public function getDistributionMemo() {
		return $this->m_strDistributionMemo;
	}

	public function sqlDistributionMemo() {
		return ( true == isset( $this->m_strDistributionMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDistributionMemo ) : '\'' . addslashes( $this->m_strDistributionMemo ) . '\'' ) : 'NULL';
	}

	public function setBilltoCompanyName( $strBilltoCompanyName ) {
		$this->set( 'm_strBilltoCompanyName', CStrings::strTrimDef( $strBilltoCompanyName, 50, NULL, true ) );
	}

	public function getBilltoCompanyName() {
		return $this->m_strBilltoCompanyName;
	}

	public function sqlBilltoCompanyName() {
		return ( true == isset( $this->m_strBilltoCompanyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoCompanyName ) : '\'' . addslashes( $this->m_strBilltoCompanyName ) . '\'' ) : 'NULL';
	}

	public function setBilltoNameFirst( $strBilltoNameFirst ) {
		$this->set( 'm_strBilltoNameFirst', CStrings::strTrimDef( $strBilltoNameFirst, 50, NULL, true ) );
	}

	public function getBilltoNameFirst() {
		return $this->m_strBilltoNameFirst;
	}

	public function sqlBilltoNameFirst() {
		return ( true == isset( $this->m_strBilltoNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoNameFirst ) : '\'' . addslashes( $this->m_strBilltoNameFirst ) . '\'' ) : 'NULL';
	}

	public function setBilltoNameMiddle( $strBilltoNameMiddle ) {
		$this->set( 'm_strBilltoNameMiddle', CStrings::strTrimDef( $strBilltoNameMiddle, 50, NULL, true ) );
	}

	public function getBilltoNameMiddle() {
		return $this->m_strBilltoNameMiddle;
	}

	public function sqlBilltoNameMiddle() {
		return ( true == isset( $this->m_strBilltoNameMiddle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoNameMiddle ) : '\'' . addslashes( $this->m_strBilltoNameMiddle ) . '\'' ) : 'NULL';
	}

	public function setBilltoNameLast( $strBilltoNameLast ) {
		$this->set( 'm_strBilltoNameLast', CStrings::strTrimDef( $strBilltoNameLast, 50, NULL, true ) );
	}

	public function getBilltoNameLast() {
		return $this->m_strBilltoNameLast;
	}

	public function sqlBilltoNameLast() {
		return ( true == isset( $this->m_strBilltoNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoNameLast ) : '\'' . addslashes( $this->m_strBilltoNameLast ) . '\'' ) : 'NULL';
	}

	public function setBilltoNameFull( $strBilltoNameFull ) {
		$this->set( 'm_strBilltoNameFull', CStrings::strTrimDef( $strBilltoNameFull, 100, NULL, true ) );
	}

	public function getBilltoNameFull() {
		return $this->m_strBilltoNameFull;
	}

	public function sqlBilltoNameFull() {
		return ( true == isset( $this->m_strBilltoNameFull ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoNameFull ) : '\'' . addslashes( $this->m_strBilltoNameFull ) . '\'' ) : 'NULL';
	}

	public function setBilltoStreetLine1( $strBilltoStreetLine1 ) {
		$this->set( 'm_strBilltoStreetLine1', CStrings::strTrimDef( $strBilltoStreetLine1, 100, NULL, true ) );
	}

	public function getBilltoStreetLine1() {
		return $this->m_strBilltoStreetLine1;
	}

	public function sqlBilltoStreetLine1() {
		return ( true == isset( $this->m_strBilltoStreetLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoStreetLine1 ) : '\'' . addslashes( $this->m_strBilltoStreetLine1 ) . '\'' ) : 'NULL';
	}

	public function setBilltoStreetLine2( $strBilltoStreetLine2 ) {
		$this->set( 'm_strBilltoStreetLine2', CStrings::strTrimDef( $strBilltoStreetLine2, 100, NULL, true ) );
	}

	public function getBilltoStreetLine2() {
		return $this->m_strBilltoStreetLine2;
	}

	public function sqlBilltoStreetLine2() {
		return ( true == isset( $this->m_strBilltoStreetLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoStreetLine2 ) : '\'' . addslashes( $this->m_strBilltoStreetLine2 ) . '\'' ) : 'NULL';
	}

	public function setBilltoStreetLine3( $strBilltoStreetLine3 ) {
		$this->set( 'm_strBilltoStreetLine3', CStrings::strTrimDef( $strBilltoStreetLine3, 100, NULL, true ) );
	}

	public function getBilltoStreetLine3() {
		return $this->m_strBilltoStreetLine3;
	}

	public function sqlBilltoStreetLine3() {
		return ( true == isset( $this->m_strBilltoStreetLine3 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoStreetLine3 ) : '\'' . addslashes( $this->m_strBilltoStreetLine3 ) . '\'' ) : 'NULL';
	}

	public function setBilltoCity( $strBilltoCity ) {
		$this->set( 'm_strBilltoCity', CStrings::strTrimDef( $strBilltoCity, 50, NULL, true ) );
	}

	public function getBilltoCity() {
		return $this->m_strBilltoCity;
	}

	public function sqlBilltoCity() {
		return ( true == isset( $this->m_strBilltoCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoCity ) : '\'' . addslashes( $this->m_strBilltoCity ) . '\'' ) : 'NULL';
	}

	public function setBilltoStateCode( $strBilltoStateCode ) {
		$this->set( 'm_strBilltoStateCode', CStrings::strTrimDef( $strBilltoStateCode, 2, NULL, true ) );
	}

	public function getBilltoStateCode() {
		return $this->m_strBilltoStateCode;
	}

	public function sqlBilltoStateCode() {
		return ( true == isset( $this->m_strBilltoStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoStateCode ) : '\'' . addslashes( $this->m_strBilltoStateCode ) . '\'' ) : 'NULL';
	}

	public function setBilltoProvince( $strBilltoProvince ) {
		$this->set( 'm_strBilltoProvince', CStrings::strTrimDef( $strBilltoProvince, 50, NULL, true ) );
	}

	public function getBilltoProvince() {
		return $this->m_strBilltoProvince;
	}

	public function sqlBilltoProvince() {
		return ( true == isset( $this->m_strBilltoProvince ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoProvince ) : '\'' . addslashes( $this->m_strBilltoProvince ) . '\'' ) : 'NULL';
	}

	public function setBilltoPostalCode( $strBilltoPostalCode ) {
		$this->set( 'm_strBilltoPostalCode', CStrings::strTrimDef( $strBilltoPostalCode, 20, NULL, true ) );
	}

	public function getBilltoPostalCode() {
		return $this->m_strBilltoPostalCode;
	}

	public function sqlBilltoPostalCode() {
		return ( true == isset( $this->m_strBilltoPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoPostalCode ) : '\'' . addslashes( $this->m_strBilltoPostalCode ) . '\'' ) : 'NULL';
	}

	public function setBilltoCountryCode( $strBilltoCountryCode ) {
		$this->set( 'm_strBilltoCountryCode', CStrings::strTrimDef( $strBilltoCountryCode, 2, NULL, true ) );
	}

	public function getBilltoCountryCode() {
		return $this->m_strBilltoCountryCode;
	}

	public function sqlBilltoCountryCode() {
		return ( true == isset( $this->m_strBilltoCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoCountryCode ) : '\'' . addslashes( $this->m_strBilltoCountryCode ) . '\'' ) : 'NULL';
	}

	public function setBilltoEmailAddress( $strBilltoEmailAddress ) {
		$this->set( 'm_strBilltoEmailAddress', CStrings::strTrimDef( $strBilltoEmailAddress, 240, NULL, true ) );
	}

	public function getBilltoEmailAddress() {
		return $this->m_strBilltoEmailAddress;
	}

	public function sqlBilltoEmailAddress() {
		return ( true == isset( $this->m_strBilltoEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoEmailAddress ) : '\'' . addslashes( $this->m_strBilltoEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setCheckAccountTypeId( $intCheckAccountTypeId ) {
		$this->set( 'm_intCheckAccountTypeId', CStrings::strToIntDef( $intCheckAccountTypeId, NULL, false ) );
	}

	public function getCheckAccountTypeId() {
		return $this->m_intCheckAccountTypeId;
	}

	public function sqlCheckAccountTypeId() {
		return ( true == isset( $this->m_intCheckAccountTypeId ) ) ? ( string ) $this->m_intCheckAccountTypeId : 'NULL';
	}

	public function setCheckNameOnAccount( $strCheckNameOnAccount ) {
		$this->set( 'm_strCheckNameOnAccount', CStrings::strTrimDef( $strCheckNameOnAccount, 50, NULL, true ) );
	}

	public function getCheckNameOnAccount() {
		return $this->m_strCheckNameOnAccount;
	}

	public function sqlCheckNameOnAccount() {
		return ( true == isset( $this->m_strCheckNameOnAccount ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckNameOnAccount ) : '\'' . addslashes( $this->m_strCheckNameOnAccount ) . '\'' ) : 'NULL';
	}

	public function setCheckBankName( $strCheckBankName ) {
		$this->set( 'm_strCheckBankName', CStrings::strTrimDef( $strCheckBankName, 100, NULL, true ) );
	}

	public function getCheckBankName() {
		return $this->m_strCheckBankName;
	}

	public function sqlCheckBankName() {
		return ( true == isset( $this->m_strCheckBankName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckBankName ) : '\'' . addslashes( $this->m_strCheckBankName ) . '\'' ) : 'NULL';
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->set( 'm_strCheckRoutingNumber', CStrings::strTrimDef( $strCheckRoutingNumber, 240, NULL, true ) );
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function sqlCheckRoutingNumber() {
		return ( true == isset( $this->m_strCheckRoutingNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckRoutingNumber ) : '\'' . addslashes( $this->m_strCheckRoutingNumber ) . '\'' ) : 'NULL';
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->set( 'm_strCheckAccountNumberEncrypted', CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function sqlCheckAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckAccountNumberEncrypted ) : '\'' . addslashes( $this->m_strCheckAccountNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setIsPostedToArDeposits( $intIsPostedToArDeposits ) {
		$this->set( 'm_intIsPostedToArDeposits', CStrings::strToIntDef( $intIsPostedToArDeposits, NULL, false ) );
	}

	public function getIsPostedToArDeposits() {
		return $this->m_intIsPostedToArDeposits;
	}

	public function sqlIsPostedToArDeposits() {
		return ( true == isset( $this->m_intIsPostedToArDeposits ) ) ? ( string ) $this->m_intIsPostedToArDeposits : '0';
	}

	public function setLockSequence( $intLockSequence ) {
		$this->set( 'm_intLockSequence', CStrings::strToIntDef( $intLockSequence, NULL, false ) );
	}

	public function getLockSequence() {
		return $this->m_intLockSequence;
	}

	public function sqlLockSequence() {
		return ( true == isset( $this->m_intLockSequence ) ) ? ( string ) $this->m_intLockSequence : 'NULL';
	}

	public function setEmailedOn( $strEmailedOn ) {
		$this->set( 'm_strEmailedOn', CStrings::strTrimDef( $strEmailedOn, -1, NULL, true ) );
	}

	public function getEmailedOn() {
		return $this->m_strEmailedOn;
	}

	public function sqlEmailedOn() {
		return ( true == isset( $this->m_strEmailedOn ) ) ? '\'' . $this->m_strEmailedOn . '\'' : 'NULL';
	}

	public function setConfirmedOn( $strConfirmedOn ) {
		$this->set( 'm_strConfirmedOn', CStrings::strTrimDef( $strConfirmedOn, -1, NULL, true ) );
	}

	public function getConfirmedOn() {
		return $this->m_strConfirmedOn;
	}

	public function sqlConfirmedOn() {
		return ( true == isset( $this->m_strConfirmedOn ) ) ? '\'' . $this->m_strConfirmedOn . '\'' : 'NULL';
	}

	public function setBatchedOn( $strBatchedOn ) {
		$this->set( 'm_strBatchedOn', CStrings::strTrimDef( $strBatchedOn, -1, NULL, true ) );
	}

	public function getBatchedOn() {
		return $this->m_strBatchedOn;
	}

	public function sqlBatchedOn() {
		return ( true == isset( $this->m_strBatchedOn ) ) ? '\'' . $this->m_strBatchedOn . '\'' : 'NULL';
	}

	public function setReturnedOn( $strReturnedOn ) {
		$this->set( 'm_strReturnedOn', CStrings::strTrimDef( $strReturnedOn, -1, NULL, true ) );
	}

	public function getReturnedOn() {
		return $this->m_strReturnedOn;
	}

	public function sqlReturnedOn() {
		return ( true == isset( $this->m_strReturnedOn ) ) ? '\'' . $this->m_strReturnedOn . '\'' : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setFeePostedOn( $strFeePostedOn ) {
		$this->set( 'm_strFeePostedOn', CStrings::strTrimDef( $strFeePostedOn, -1, NULL, true ) );
	}

	public function getFeePostedOn() {
		return $this->m_strFeePostedOn;
	}

	public function sqlFeePostedOn() {
		return ( true == isset( $this->m_strFeePostedOn ) ) ? '\'' . $this->m_strFeePostedOn . '\'' : 'NULL';
	}

	public function setReturnFeePostedOn( $strReturnFeePostedOn ) {
		$this->set( 'm_strReturnFeePostedOn', CStrings::strTrimDef( $strReturnFeePostedOn, -1, NULL, true ) );
	}

	public function getReturnFeePostedOn() {
		return $this->m_strReturnFeePostedOn;
	}

	public function sqlReturnFeePostedOn() {
		return ( true == isset( $this->m_strReturnFeePostedOn ) ) ? '\'' . $this->m_strReturnFeePostedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, processing_bank_id, payment_type_id, account_id, company_merchant_account_id, distribution_status_type_id, return_type_id, remote_primary_key, distribution_datetime, settlement_datetime, distribution_amount, distribution_memo, billto_company_name, billto_name_first, billto_name_middle, billto_name_last, billto_name_full, billto_street_line1, billto_street_line2, billto_street_line3, billto_city, billto_state_code, billto_province, billto_postal_code, billto_country_code, billto_email_address, check_account_type_id, check_name_on_account, check_bank_name, check_routing_number, check_account_number_encrypted, is_posted_to_ar_deposits, lock_sequence, emailed_on, confirmed_on, batched_on, returned_on, exported_on, fee_posted_on, return_fee_posted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlProcessingBankId() . ', ' .
		          $this->sqlPaymentTypeId() . ', ' .
		          $this->sqlAccountId() . ', ' .
		          $this->sqlCompanyMerchantAccountId() . ', ' .
		          $this->sqlDistributionStatusTypeId() . ', ' .
		          $this->sqlReturnTypeId() . ', ' .
		          $this->sqlRemotePrimaryKey() . ', ' .
		          $this->sqlDistributionDatetime() . ', ' .
		          $this->sqlSettlementDatetime() . ', ' .
		          $this->sqlDistributionAmount() . ', ' .
		          $this->sqlDistributionMemo() . ', ' .
		          $this->sqlBilltoCompanyName() . ', ' .
		          $this->sqlBilltoNameFirst() . ', ' .
		          $this->sqlBilltoNameMiddle() . ', ' .
		          $this->sqlBilltoNameLast() . ', ' .
		          $this->sqlBilltoNameFull() . ', ' .
		          $this->sqlBilltoStreetLine1() . ', ' .
		          $this->sqlBilltoStreetLine2() . ', ' .
		          $this->sqlBilltoStreetLine3() . ', ' .
		          $this->sqlBilltoCity() . ', ' .
		          $this->sqlBilltoStateCode() . ', ' .
		          $this->sqlBilltoProvince() . ', ' .
		          $this->sqlBilltoPostalCode() . ', ' .
		          $this->sqlBilltoCountryCode() . ', ' .
		          $this->sqlBilltoEmailAddress() . ', ' .
		          $this->sqlCheckAccountTypeId() . ', ' .
		          $this->sqlCheckNameOnAccount() . ', ' .
		          $this->sqlCheckBankName() . ', ' .
		          $this->sqlCheckRoutingNumber() . ', ' .
		          $this->sqlCheckAccountNumberEncrypted() . ', ' .
		          $this->sqlIsPostedToArDeposits() . ', ' .
		          $this->sqlLockSequence() . ', ' .
		          $this->sqlEmailedOn() . ', ' .
		          $this->sqlConfirmedOn() . ', ' .
		          $this->sqlBatchedOn() . ', ' .
		          $this->sqlReturnedOn() . ', ' .
		          $this->sqlExportedOn() . ', ' .
		          $this->sqlFeePostedOn() . ', ' .
		          $this->sqlReturnFeePostedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId(). ',' ; } elseif( true == array_key_exists( 'ProcessingBankId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_merchant_account_id = ' . $this->sqlCompanyMerchantAccountId(). ',' ; } elseif( true == array_key_exists( 'CompanyMerchantAccountId', $this->getChangedColumns() ) ) { $strSql .= ' company_merchant_account_id = ' . $this->sqlCompanyMerchantAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' distribution_status_type_id = ' . $this->sqlDistributionStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'DistributionStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' distribution_status_type_id = ' . $this->sqlDistributionStatusTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId(). ',' ; } elseif( true == array_key_exists( 'ReturnTypeId', $this->getChangedColumns() ) ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' distribution_datetime = ' . $this->sqlDistributionDatetime(). ',' ; } elseif( true == array_key_exists( 'DistributionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' distribution_datetime = ' . $this->sqlDistributionDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' settlement_datetime = ' . $this->sqlSettlementDatetime(). ',' ; } elseif( true == array_key_exists( 'SettlementDatetime', $this->getChangedColumns() ) ) { $strSql .= ' settlement_datetime = ' . $this->sqlSettlementDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' distribution_amount = ' . $this->sqlDistributionAmount(). ',' ; } elseif( true == array_key_exists( 'DistributionAmount', $this->getChangedColumns() ) ) { $strSql .= ' distribution_amount = ' . $this->sqlDistributionAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' distribution_memo = ' . $this->sqlDistributionMemo(). ',' ; } elseif( true == array_key_exists( 'DistributionMemo', $this->getChangedColumns() ) ) { $strSql .= ' distribution_memo = ' . $this->sqlDistributionMemo() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_company_name = ' . $this->sqlBilltoCompanyName(). ',' ; } elseif( true == array_key_exists( 'BilltoCompanyName', $this->getChangedColumns() ) ) { $strSql .= ' billto_company_name = ' . $this->sqlBilltoCompanyName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_name_first = ' . $this->sqlBilltoNameFirst(). ',' ; } elseif( true == array_key_exists( 'BilltoNameFirst', $this->getChangedColumns() ) ) { $strSql .= ' billto_name_first = ' . $this->sqlBilltoNameFirst() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_name_middle = ' . $this->sqlBilltoNameMiddle(). ',' ; } elseif( true == array_key_exists( 'BilltoNameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' billto_name_middle = ' . $this->sqlBilltoNameMiddle() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_name_last = ' . $this->sqlBilltoNameLast(). ',' ; } elseif( true == array_key_exists( 'BilltoNameLast', $this->getChangedColumns() ) ) { $strSql .= ' billto_name_last = ' . $this->sqlBilltoNameLast() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_name_full = ' . $this->sqlBilltoNameFull(). ',' ; } elseif( true == array_key_exists( 'BilltoNameFull', $this->getChangedColumns() ) ) { $strSql .= ' billto_name_full = ' . $this->sqlBilltoNameFull() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line1 = ' . $this->sqlBilltoStreetLine1(). ',' ; } elseif( true == array_key_exists( 'BilltoStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line1 = ' . $this->sqlBilltoStreetLine1() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line2 = ' . $this->sqlBilltoStreetLine2(). ',' ; } elseif( true == array_key_exists( 'BilltoStreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line2 = ' . $this->sqlBilltoStreetLine2() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line3 = ' . $this->sqlBilltoStreetLine3(). ',' ; } elseif( true == array_key_exists( 'BilltoStreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line3 = ' . $this->sqlBilltoStreetLine3() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_city = ' . $this->sqlBilltoCity(). ',' ; } elseif( true == array_key_exists( 'BilltoCity', $this->getChangedColumns() ) ) { $strSql .= ' billto_city = ' . $this->sqlBilltoCity() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_state_code = ' . $this->sqlBilltoStateCode(). ',' ; } elseif( true == array_key_exists( 'BilltoStateCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_state_code = ' . $this->sqlBilltoStateCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_province = ' . $this->sqlBilltoProvince(). ',' ; } elseif( true == array_key_exists( 'BilltoProvince', $this->getChangedColumns() ) ) { $strSql .= ' billto_province = ' . $this->sqlBilltoProvince() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_postal_code = ' . $this->sqlBilltoPostalCode(). ',' ; } elseif( true == array_key_exists( 'BilltoPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_postal_code = ' . $this->sqlBilltoPostalCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_country_code = ' . $this->sqlBilltoCountryCode(). ',' ; } elseif( true == array_key_exists( 'BilltoCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_country_code = ' . $this->sqlBilltoCountryCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_email_address = ' . $this->sqlBilltoEmailAddress(). ',' ; } elseif( true == array_key_exists( 'BilltoEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' billto_email_address = ' . $this->sqlBilltoEmailAddress() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId(). ',' ; } elseif( true == array_key_exists( 'CheckAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount(). ',' ; } elseif( true == array_key_exists( 'CheckNameOnAccount', $this->getChangedColumns() ) ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName(). ',' ; } elseif( true == array_key_exists( 'CheckBankName', $this->getChangedColumns() ) ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber(). ',' ; } elseif( true == array_key_exists( 'CheckRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'CheckAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_posted_to_ar_deposits = ' . $this->sqlIsPostedToArDeposits(). ',' ; } elseif( true == array_key_exists( 'IsPostedToArDeposits', $this->getChangedColumns() ) ) { $strSql .= ' is_posted_to_ar_deposits = ' . $this->sqlIsPostedToArDeposits() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lock_sequence = ' . $this->sqlLockSequence(). ',' ; } elseif( true == array_key_exists( 'LockSequence', $this->getChangedColumns() ) ) { $strSql .= ' lock_sequence = ' . $this->sqlLockSequence() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn(). ',' ; } elseif( true == array_key_exists( 'EmailedOn', $this->getChangedColumns() ) ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn(). ',' ; } elseif( true == array_key_exists( 'ConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batched_on = ' . $this->sqlBatchedOn(). ',' ; } elseif( true == array_key_exists( 'BatchedOn', $this->getChangedColumns() ) ) { $strSql .= ' batched_on = ' . $this->sqlBatchedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn(). ',' ; } elseif( true == array_key_exists( 'ReturnedOn', $this->getChangedColumns() ) ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fee_posted_on = ' . $this->sqlFeePostedOn(). ',' ; } elseif( true == array_key_exists( 'FeePostedOn', $this->getChangedColumns() ) ) { $strSql .= ' fee_posted_on = ' . $this->sqlFeePostedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_fee_posted_on = ' . $this->sqlReturnFeePostedOn(). ',' ; } elseif( true == array_key_exists( 'ReturnFeePostedOn', $this->getChangedColumns() ) ) { $strSql .= ' return_fee_posted_on = ' . $this->sqlReturnFeePostedOn() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'processing_bank_id' => $this->getProcessingBankId(),
			'payment_type_id' => $this->getPaymentTypeId(),
			'account_id' => $this->getAccountId(),
			'company_merchant_account_id' => $this->getCompanyMerchantAccountId(),
			'distribution_status_type_id' => $this->getDistributionStatusTypeId(),
			'return_type_id' => $this->getReturnTypeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'distribution_datetime' => $this->getDistributionDatetime(),
			'settlement_datetime' => $this->getSettlementDatetime(),
			'distribution_amount' => $this->getDistributionAmount(),
			'distribution_memo' => $this->getDistributionMemo(),
			'billto_company_name' => $this->getBilltoCompanyName(),
			'billto_name_first' => $this->getBilltoNameFirst(),
			'billto_name_middle' => $this->getBilltoNameMiddle(),
			'billto_name_last' => $this->getBilltoNameLast(),
			'billto_name_full' => $this->getBilltoNameFull(),
			'billto_street_line1' => $this->getBilltoStreetLine1(),
			'billto_street_line2' => $this->getBilltoStreetLine2(),
			'billto_street_line3' => $this->getBilltoStreetLine3(),
			'billto_city' => $this->getBilltoCity(),
			'billto_state_code' => $this->getBilltoStateCode(),
			'billto_province' => $this->getBilltoProvince(),
			'billto_postal_code' => $this->getBilltoPostalCode(),
			'billto_country_code' => $this->getBilltoCountryCode(),
			'billto_email_address' => $this->getBilltoEmailAddress(),
			'check_account_type_id' => $this->getCheckAccountTypeId(),
			'check_name_on_account' => $this->getCheckNameOnAccount(),
			'check_bank_name' => $this->getCheckBankName(),
			'check_routing_number' => $this->getCheckRoutingNumber(),
			'check_account_number_encrypted' => $this->getCheckAccountNumberEncrypted(),
			'is_posted_to_ar_deposits' => $this->getIsPostedToArDeposits(),
			'lock_sequence' => $this->getLockSequence(),
			'emailed_on' => $this->getEmailedOn(),
			'confirmed_on' => $this->getConfirmedOn(),
			'batched_on' => $this->getBatchedOn(),
			'returned_on' => $this->getReturnedOn(),
			'exported_on' => $this->getExportedOn(),
			'fee_posted_on' => $this->getFeePostedOn(),
			'return_fee_posted_on' => $this->getReturnFeePostedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>