<?php

class CBaseNachaEntryDetailRecord extends CEosSingularBase {

	const TABLE_NAME = 'public.nacha_entry_detail_records';

	protected $m_intId;
	protected $m_intNachaFileId;
	protected $m_intNachaFileBatchId;
	protected $m_intClearingBatchId;
	protected $m_intEntryDetailRecordTypeId;
	protected $m_intProcessingBankAccountId;
	protected $m_intReturnTypeId;
	protected $m_intCid;
	protected $m_intIntermediaryAccountId;
	protected $m_intArPaymentId;
	protected $m_intRepairArPaymentId;
	protected $m_intSettlementDistributionId;
	protected $m_intCompanyPaymentId;
	protected $m_intEftInstructionId;
	protected $m_intX937CheckDetailRecordId;
	protected $m_intAccountId;
	protected $m_intOffsettingNachaEntryDetailRecordId;
	protected $m_intNachaReturnAddendaDetailRecordId;
	protected $m_intCheckAccountTypeId;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckAccountNumberEncrypted;
	protected $m_strOffsettingRoutingNumber;
	protected $m_strOffsettingAccountNumberEncrypted;
	protected $m_strRecordTypeCode;
	protected $m_strRecordTransactionCode;
	protected $m_strReceivingDfiIdentification;
	protected $m_strCheckDigit;
	protected $m_strDfiAccountNumber;
	protected $m_strAmount;
	protected $m_fltFormattedAmount;
	protected $m_strIdentificationNumber;
	protected $m_strReceivingCompanyName;
	protected $m_strDiscretionaryData;
	protected $m_strAddendaRecordIndicator;
	protected $m_strTraceNumber;
	protected $m_strReturnedOn;
	protected $m_strTransferredToIntermediaryOn;
	protected $m_intIsReconPrepped;
	protected $m_intIsIntermediary;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsReconPrepped = '0';
		$this->m_intIsIntermediary = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['nacha_file_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaFileId', trim( $arrValues['nacha_file_id'] ) ); elseif( isset( $arrValues['nacha_file_id'] ) ) $this->setNachaFileId( $arrValues['nacha_file_id'] );
		if( isset( $arrValues['nacha_file_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaFileBatchId', trim( $arrValues['nacha_file_batch_id'] ) ); elseif( isset( $arrValues['nacha_file_batch_id'] ) ) $this->setNachaFileBatchId( $arrValues['nacha_file_batch_id'] );
		if( isset( $arrValues['clearing_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intClearingBatchId', trim( $arrValues['clearing_batch_id'] ) ); elseif( isset( $arrValues['clearing_batch_id'] ) ) $this->setClearingBatchId( $arrValues['clearing_batch_id'] );
		if( isset( $arrValues['entry_detail_record_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEntryDetailRecordTypeId', trim( $arrValues['entry_detail_record_type_id'] ) ); elseif( isset( $arrValues['entry_detail_record_type_id'] ) ) $this->setEntryDetailRecordTypeId( $arrValues['entry_detail_record_type_id'] );
		if( isset( $arrValues['processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankAccountId', trim( $arrValues['processing_bank_account_id'] ) ); elseif( isset( $arrValues['processing_bank_account_id'] ) ) $this->setProcessingBankAccountId( $arrValues['processing_bank_account_id'] );
		if( isset( $arrValues['return_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReturnTypeId', trim( $arrValues['return_type_id'] ) ); elseif( isset( $arrValues['return_type_id'] ) ) $this->setReturnTypeId( $arrValues['return_type_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['intermediary_account_id'] ) && $boolDirectSet ) $this->set( 'm_intIntermediaryAccountId', trim( $arrValues['intermediary_account_id'] ) ); elseif( isset( $arrValues['intermediary_account_id'] ) ) $this->setIntermediaryAccountId( $arrValues['intermediary_account_id'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['repair_ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intRepairArPaymentId', trim( $arrValues['repair_ar_payment_id'] ) ); elseif( isset( $arrValues['repair_ar_payment_id'] ) ) $this->setRepairArPaymentId( $arrValues['repair_ar_payment_id'] );
		if( isset( $arrValues['settlement_distribution_id'] ) && $boolDirectSet ) $this->set( 'm_intSettlementDistributionId', trim( $arrValues['settlement_distribution_id'] ) ); elseif( isset( $arrValues['settlement_distribution_id'] ) ) $this->setSettlementDistributionId( $arrValues['settlement_distribution_id'] );
		if( isset( $arrValues['company_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyPaymentId', trim( $arrValues['company_payment_id'] ) ); elseif( isset( $arrValues['company_payment_id'] ) ) $this->setCompanyPaymentId( $arrValues['company_payment_id'] );
		if( isset( $arrValues['eft_instruction_id'] ) && $boolDirectSet ) $this->set( 'm_intEftInstructionId', trim( $arrValues['eft_instruction_id'] ) ); elseif( isset( $arrValues['eft_instruction_id'] ) ) $this->setEftInstructionId( $arrValues['eft_instruction_id'] );
		if( isset( $arrValues['x937_check_detail_record_id'] ) && $boolDirectSet ) $this->set( 'm_intX937CheckDetailRecordId', trim( $arrValues['x937_check_detail_record_id'] ) ); elseif( isset( $arrValues['x937_check_detail_record_id'] ) ) $this->setX937CheckDetailRecordId( $arrValues['x937_check_detail_record_id'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['offsetting_nacha_entry_detail_record_id'] ) && $boolDirectSet ) $this->set( 'm_intOffsettingNachaEntryDetailRecordId', trim( $arrValues['offsetting_nacha_entry_detail_record_id'] ) ); elseif( isset( $arrValues['offsetting_nacha_entry_detail_record_id'] ) ) $this->setOffsettingNachaEntryDetailRecordId( $arrValues['offsetting_nacha_entry_detail_record_id'] );
		if( isset( $arrValues['nacha_return_addenda_detail_record_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaReturnAddendaDetailRecordId', trim( $arrValues['nacha_return_addenda_detail_record_id'] ) ); elseif( isset( $arrValues['nacha_return_addenda_detail_record_id'] ) ) $this->setNachaReturnAddendaDetailRecordId( $arrValues['nacha_return_addenda_detail_record_id'] );
		if( isset( $arrValues['check_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckAccountTypeId', trim( $arrValues['check_account_type_id'] ) ); elseif( isset( $arrValues['check_account_type_id'] ) ) $this->setCheckAccountTypeId( $arrValues['check_account_type_id'] );
		if( isset( $arrValues['check_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckRoutingNumber', trim( stripcslashes( $arrValues['check_routing_number'] ) ) ); elseif( isset( $arrValues['check_routing_number'] ) ) $this->setCheckRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_routing_number'] ) : $arrValues['check_routing_number'] );
		if( isset( $arrValues['check_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberEncrypted', trim( stripcslashes( $arrValues['check_account_number_encrypted'] ) ) ); elseif( isset( $arrValues['check_account_number_encrypted'] ) ) $this->setCheckAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_account_number_encrypted'] ) : $arrValues['check_account_number_encrypted'] );
		if( isset( $arrValues['offsetting_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strOffsettingRoutingNumber', trim( stripcslashes( $arrValues['offsetting_routing_number'] ) ) ); elseif( isset( $arrValues['offsetting_routing_number'] ) ) $this->setOffsettingRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['offsetting_routing_number'] ) : $arrValues['offsetting_routing_number'] );
		if( isset( $arrValues['offsetting_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strOffsettingAccountNumberEncrypted', trim( stripcslashes( $arrValues['offsetting_account_number_encrypted'] ) ) ); elseif( isset( $arrValues['offsetting_account_number_encrypted'] ) ) $this->setOffsettingAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['offsetting_account_number_encrypted'] ) : $arrValues['offsetting_account_number_encrypted'] );
		if( isset( $arrValues['record_type_code'] ) && $boolDirectSet ) $this->set( 'm_strRecordTypeCode', trim( stripcslashes( $arrValues['record_type_code'] ) ) ); elseif( isset( $arrValues['record_type_code'] ) ) $this->setRecordTypeCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_type_code'] ) : $arrValues['record_type_code'] );
		if( isset( $arrValues['record_transaction_code'] ) && $boolDirectSet ) $this->set( 'm_strRecordTransactionCode', trim( stripcslashes( $arrValues['record_transaction_code'] ) ) ); elseif( isset( $arrValues['record_transaction_code'] ) ) $this->setRecordTransactionCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_transaction_code'] ) : $arrValues['record_transaction_code'] );
		if( isset( $arrValues['receiving_dfi_identification'] ) && $boolDirectSet ) $this->set( 'm_strReceivingDfiIdentification', trim( stripcslashes( $arrValues['receiving_dfi_identification'] ) ) ); elseif( isset( $arrValues['receiving_dfi_identification'] ) ) $this->setReceivingDfiIdentification( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['receiving_dfi_identification'] ) : $arrValues['receiving_dfi_identification'] );
		if( isset( $arrValues['check_digit'] ) && $boolDirectSet ) $this->set( 'm_strCheckDigit', trim( stripcslashes( $arrValues['check_digit'] ) ) ); elseif( isset( $arrValues['check_digit'] ) ) $this->setCheckDigit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_digit'] ) : $arrValues['check_digit'] );
		if( isset( $arrValues['dfi_account_number'] ) && $boolDirectSet ) $this->set( 'm_strDfiAccountNumber', trim( stripcslashes( $arrValues['dfi_account_number'] ) ) ); elseif( isset( $arrValues['dfi_account_number'] ) ) $this->setDfiAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dfi_account_number'] ) : $arrValues['dfi_account_number'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_strAmount', trim( stripcslashes( $arrValues['amount'] ) ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['amount'] ) : $arrValues['amount'] );
		if( isset( $arrValues['formatted_amount'] ) && $boolDirectSet ) $this->set( 'm_fltFormattedAmount', trim( $arrValues['formatted_amount'] ) ); elseif( isset( $arrValues['formatted_amount'] ) ) $this->setFormattedAmount( $arrValues['formatted_amount'] );
		if( isset( $arrValues['identification_number'] ) && $boolDirectSet ) $this->set( 'm_strIdentificationNumber', trim( stripcslashes( $arrValues['identification_number'] ) ) ); elseif( isset( $arrValues['identification_number'] ) ) $this->setIdentificationNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['identification_number'] ) : $arrValues['identification_number'] );
		if( isset( $arrValues['receiving_company_name'] ) && $boolDirectSet ) $this->set( 'm_strReceivingCompanyName', trim( stripcslashes( $arrValues['receiving_company_name'] ) ) ); elseif( isset( $arrValues['receiving_company_name'] ) ) $this->setReceivingCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['receiving_company_name'] ) : $arrValues['receiving_company_name'] );
		if( isset( $arrValues['discretionary_data'] ) && $boolDirectSet ) $this->set( 'm_strDiscretionaryData', trim( stripcslashes( $arrValues['discretionary_data'] ) ) ); elseif( isset( $arrValues['discretionary_data'] ) ) $this->setDiscretionaryData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['discretionary_data'] ) : $arrValues['discretionary_data'] );
		if( isset( $arrValues['addenda_record_indicator'] ) && $boolDirectSet ) $this->set( 'm_strAddendaRecordIndicator', trim( stripcslashes( $arrValues['addenda_record_indicator'] ) ) ); elseif( isset( $arrValues['addenda_record_indicator'] ) ) $this->setAddendaRecordIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['addenda_record_indicator'] ) : $arrValues['addenda_record_indicator'] );
		if( isset( $arrValues['trace_number'] ) && $boolDirectSet ) $this->set( 'm_strTraceNumber', trim( stripcslashes( $arrValues['trace_number'] ) ) ); elseif( isset( $arrValues['trace_number'] ) ) $this->setTraceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['trace_number'] ) : $arrValues['trace_number'] );
		if( isset( $arrValues['returned_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnedOn', trim( $arrValues['returned_on'] ) ); elseif( isset( $arrValues['returned_on'] ) ) $this->setReturnedOn( $arrValues['returned_on'] );
		if( isset( $arrValues['transferred_to_intermediary_on'] ) && $boolDirectSet ) $this->set( 'm_strTransferredToIntermediaryOn', trim( $arrValues['transferred_to_intermediary_on'] ) ); elseif( isset( $arrValues['transferred_to_intermediary_on'] ) ) $this->setTransferredToIntermediaryOn( $arrValues['transferred_to_intermediary_on'] );
		if( isset( $arrValues['is_recon_prepped'] ) && $boolDirectSet ) $this->set( 'm_intIsReconPrepped', trim( $arrValues['is_recon_prepped'] ) ); elseif( isset( $arrValues['is_recon_prepped'] ) ) $this->setIsReconPrepped( $arrValues['is_recon_prepped'] );
		if( isset( $arrValues['is_intermediary'] ) && $boolDirectSet ) $this->set( 'm_intIsIntermediary', trim( $arrValues['is_intermediary'] ) ); elseif( isset( $arrValues['is_intermediary'] ) ) $this->setIsIntermediary( $arrValues['is_intermediary'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setNachaFileId( $intNachaFileId ) {
		$this->set( 'm_intNachaFileId', CStrings::strToIntDef( $intNachaFileId, NULL, false ) );
	}

	public function getNachaFileId() {
		return $this->m_intNachaFileId;
	}

	public function sqlNachaFileId() {
		return ( true == isset( $this->m_intNachaFileId ) ) ? ( string ) $this->m_intNachaFileId : 'NULL';
	}

	public function setNachaFileBatchId( $intNachaFileBatchId ) {
		$this->set( 'm_intNachaFileBatchId', CStrings::strToIntDef( $intNachaFileBatchId, NULL, false ) );
	}

	public function getNachaFileBatchId() {
		return $this->m_intNachaFileBatchId;
	}

	public function sqlNachaFileBatchId() {
		return ( true == isset( $this->m_intNachaFileBatchId ) ) ? ( string ) $this->m_intNachaFileBatchId : 'NULL';
	}

	public function setClearingBatchId( $intClearingBatchId ) {
		$this->set( 'm_intClearingBatchId', CStrings::strToIntDef( $intClearingBatchId, NULL, false ) );
	}

	public function getClearingBatchId() {
		return $this->m_intClearingBatchId;
	}

	public function sqlClearingBatchId() {
		return ( true == isset( $this->m_intClearingBatchId ) ) ? ( string ) $this->m_intClearingBatchId : 'NULL';
	}

	public function setEntryDetailRecordTypeId( $intEntryDetailRecordTypeId ) {
		$this->set( 'm_intEntryDetailRecordTypeId', CStrings::strToIntDef( $intEntryDetailRecordTypeId, NULL, false ) );
	}

	public function getEntryDetailRecordTypeId() {
		return $this->m_intEntryDetailRecordTypeId;
	}

	public function sqlEntryDetailRecordTypeId() {
		return ( true == isset( $this->m_intEntryDetailRecordTypeId ) ) ? ( string ) $this->m_intEntryDetailRecordTypeId : 'NULL';
	}

	public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
		$this->set( 'm_intProcessingBankAccountId', CStrings::strToIntDef( $intProcessingBankAccountId, NULL, false ) );
	}

	public function getProcessingBankAccountId() {
		return $this->m_intProcessingBankAccountId;
	}

	public function sqlProcessingBankAccountId() {
		return ( true == isset( $this->m_intProcessingBankAccountId ) ) ? ( string ) $this->m_intProcessingBankAccountId : 'NULL';
	}

	public function setReturnTypeId( $intReturnTypeId ) {
		$this->set( 'm_intReturnTypeId', CStrings::strToIntDef( $intReturnTypeId, NULL, false ) );
	}

	public function getReturnTypeId() {
		return $this->m_intReturnTypeId;
	}

	public function sqlReturnTypeId() {
		return ( true == isset( $this->m_intReturnTypeId ) ) ? ( string ) $this->m_intReturnTypeId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setIntermediaryAccountId( $intIntermediaryAccountId ) {
		$this->set( 'm_intIntermediaryAccountId', CStrings::strToIntDef( $intIntermediaryAccountId, NULL, false ) );
	}

	public function getIntermediaryAccountId() {
		return $this->m_intIntermediaryAccountId;
	}

	public function sqlIntermediaryAccountId() {
		return ( true == isset( $this->m_intIntermediaryAccountId ) ) ? ( string ) $this->m_intIntermediaryAccountId : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setRepairArPaymentId( $intRepairArPaymentId ) {
		$this->set( 'm_intRepairArPaymentId', CStrings::strToIntDef( $intRepairArPaymentId, NULL, false ) );
	}

	public function getRepairArPaymentId() {
		return $this->m_intRepairArPaymentId;
	}

	public function sqlRepairArPaymentId() {
		return ( true == isset( $this->m_intRepairArPaymentId ) ) ? ( string ) $this->m_intRepairArPaymentId : 'NULL';
	}

	public function setSettlementDistributionId( $intSettlementDistributionId ) {
		$this->set( 'm_intSettlementDistributionId', CStrings::strToIntDef( $intSettlementDistributionId, NULL, false ) );
	}

	public function getSettlementDistributionId() {
		return $this->m_intSettlementDistributionId;
	}

	public function sqlSettlementDistributionId() {
		return ( true == isset( $this->m_intSettlementDistributionId ) ) ? ( string ) $this->m_intSettlementDistributionId : 'NULL';
	}

	public function setCompanyPaymentId( $intCompanyPaymentId ) {
		$this->set( 'm_intCompanyPaymentId', CStrings::strToIntDef( $intCompanyPaymentId, NULL, false ) );
	}

	public function getCompanyPaymentId() {
		return $this->m_intCompanyPaymentId;
	}

	public function sqlCompanyPaymentId() {
		return ( true == isset( $this->m_intCompanyPaymentId ) ) ? ( string ) $this->m_intCompanyPaymentId : 'NULL';
	}

	public function setEftInstructionId( $intEftInstructionId ) {
		$this->set( 'm_intEftInstructionId', CStrings::strToIntDef( $intEftInstructionId, NULL, false ) );
	}

	public function getEftInstructionId() {
		return $this->m_intEftInstructionId;
	}

	public function sqlEftInstructionId() {
		return ( true == isset( $this->m_intEftInstructionId ) ) ? ( string ) $this->m_intEftInstructionId : 'NULL';
	}

	public function setX937CheckDetailRecordId( $intX937CheckDetailRecordId ) {
		$this->set( 'm_intX937CheckDetailRecordId', CStrings::strToIntDef( $intX937CheckDetailRecordId, NULL, false ) );
	}

	public function getX937CheckDetailRecordId() {
		return $this->m_intX937CheckDetailRecordId;
	}

	public function sqlX937CheckDetailRecordId() {
		return ( true == isset( $this->m_intX937CheckDetailRecordId ) ) ? ( string ) $this->m_intX937CheckDetailRecordId : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setOffsettingNachaEntryDetailRecordId( $intOffsettingNachaEntryDetailRecordId ) {
		$this->set( 'm_intOffsettingNachaEntryDetailRecordId', CStrings::strToIntDef( $intOffsettingNachaEntryDetailRecordId, NULL, false ) );
	}

	public function getOffsettingNachaEntryDetailRecordId() {
		return $this->m_intOffsettingNachaEntryDetailRecordId;
	}

	public function sqlOffsettingNachaEntryDetailRecordId() {
		return ( true == isset( $this->m_intOffsettingNachaEntryDetailRecordId ) ) ? ( string ) $this->m_intOffsettingNachaEntryDetailRecordId : 'NULL';
	}

	public function setNachaReturnAddendaDetailRecordId( $intNachaReturnAddendaDetailRecordId ) {
		$this->set( 'm_intNachaReturnAddendaDetailRecordId', CStrings::strToIntDef( $intNachaReturnAddendaDetailRecordId, NULL, false ) );
	}

	public function getNachaReturnAddendaDetailRecordId() {
		return $this->m_intNachaReturnAddendaDetailRecordId;
	}

	public function sqlNachaReturnAddendaDetailRecordId() {
		return ( true == isset( $this->m_intNachaReturnAddendaDetailRecordId ) ) ? ( string ) $this->m_intNachaReturnAddendaDetailRecordId : 'NULL';
	}

	public function setCheckAccountTypeId( $intCheckAccountTypeId ) {
		$this->set( 'm_intCheckAccountTypeId', CStrings::strToIntDef( $intCheckAccountTypeId, NULL, false ) );
	}

	public function getCheckAccountTypeId() {
		return $this->m_intCheckAccountTypeId;
	}

	public function sqlCheckAccountTypeId() {
		return ( true == isset( $this->m_intCheckAccountTypeId ) ) ? ( string ) $this->m_intCheckAccountTypeId : 'NULL';
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->set( 'm_strCheckRoutingNumber', CStrings::strTrimDef( $strCheckRoutingNumber, 240, NULL, true ) );
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function sqlCheckRoutingNumber() {
		return ( true == isset( $this->m_strCheckRoutingNumber ) ) ? '\'' . addslashes( $this->m_strCheckRoutingNumber ) . '\'' : 'NULL';
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->set( 'm_strCheckAccountNumberEncrypted', CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function sqlCheckAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCheckAccountNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setOffsettingRoutingNumber( $strOffsettingRoutingNumber ) {
		$this->set( 'm_strOffsettingRoutingNumber', CStrings::strTrimDef( $strOffsettingRoutingNumber, 240, NULL, true ) );
	}

	public function getOffsettingRoutingNumber() {
		return $this->m_strOffsettingRoutingNumber;
	}

	public function sqlOffsettingRoutingNumber() {
		return ( true == isset( $this->m_strOffsettingRoutingNumber ) ) ? '\'' . addslashes( $this->m_strOffsettingRoutingNumber ) . '\'' : 'NULL';
	}

	public function setOffsettingAccountNumberEncrypted( $strOffsettingAccountNumberEncrypted ) {
		$this->set( 'm_strOffsettingAccountNumberEncrypted', CStrings::strTrimDef( $strOffsettingAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getOffsettingAccountNumberEncrypted() {
		return $this->m_strOffsettingAccountNumberEncrypted;
	}

	public function sqlOffsettingAccountNumberEncrypted() {
		return ( true == isset( $this->m_strOffsettingAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strOffsettingAccountNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setRecordTypeCode( $strRecordTypeCode ) {
		$this->set( 'm_strRecordTypeCode', CStrings::strTrimDef( $strRecordTypeCode, 1, NULL, true ) );
	}

	public function getRecordTypeCode() {
		return $this->m_strRecordTypeCode;
	}

	public function sqlRecordTypeCode() {
		return ( true == isset( $this->m_strRecordTypeCode ) ) ? '\'' . addslashes( $this->m_strRecordTypeCode ) . '\'' : 'NULL';
	}

	public function setRecordTransactionCode( $strRecordTransactionCode ) {
		$this->set( 'm_strRecordTransactionCode', CStrings::strTrimDef( $strRecordTransactionCode, 2, NULL, true ) );
	}

	public function getRecordTransactionCode() {
		return $this->m_strRecordTransactionCode;
	}

	public function sqlRecordTransactionCode() {
		return ( true == isset( $this->m_strRecordTransactionCode ) ) ? '\'' . addslashes( $this->m_strRecordTransactionCode ) . '\'' : 'NULL';
	}

	public function setReceivingDfiIdentification( $strReceivingDfiIdentification ) {
		$this->set( 'm_strReceivingDfiIdentification', CStrings::strTrimDef( $strReceivingDfiIdentification, 8, NULL, true ) );
	}

	public function getReceivingDfiIdentification() {
		return $this->m_strReceivingDfiIdentification;
	}

	public function sqlReceivingDfiIdentification() {
		return ( true == isset( $this->m_strReceivingDfiIdentification ) ) ? '\'' . addslashes( $this->m_strReceivingDfiIdentification ) . '\'' : 'NULL';
	}

	public function setCheckDigit( $strCheckDigit ) {
		$this->set( 'm_strCheckDigit', CStrings::strTrimDef( $strCheckDigit, 1, NULL, true ) );
	}

	public function getCheckDigit() {
		return $this->m_strCheckDigit;
	}

	public function sqlCheckDigit() {
		return ( true == isset( $this->m_strCheckDigit ) ) ? '\'' . addslashes( $this->m_strCheckDigit ) . '\'' : 'NULL';
	}

	public function setDfiAccountNumber( $strDfiAccountNumber ) {
		$this->set( 'm_strDfiAccountNumber', CStrings::strTrimDef( $strDfiAccountNumber, 17, NULL, true ) );
	}

	public function getDfiAccountNumber() {
		return $this->m_strDfiAccountNumber;
	}

	public function sqlDfiAccountNumber() {
		return ( true == isset( $this->m_strDfiAccountNumber ) ) ? '\'' . addslashes( $this->m_strDfiAccountNumber ) . '\'' : 'NULL';
	}

	public function setAmount( $strAmount ) {
		$this->set( 'm_strAmount', CStrings::strTrimDef( $strAmount, 10, NULL, true ) );
	}

	public function getAmount() {
		return $this->m_strAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_strAmount ) ) ? '\'' . addslashes( $this->m_strAmount ) . '\'' : 'NULL';
	}

	public function setFormattedAmount( $fltFormattedAmount ) {
		$this->set( 'm_fltFormattedAmount', CStrings::strToFloatDef( $fltFormattedAmount, NULL, false, 2 ) );
	}

	public function getFormattedAmount() {
		return $this->m_fltFormattedAmount;
	}

	public function sqlFormattedAmount() {
		return ( true == isset( $this->m_fltFormattedAmount ) ) ? ( string ) $this->m_fltFormattedAmount : 'NULL';
	}

	public function setIdentificationNumber( $strIdentificationNumber ) {
		$this->set( 'm_strIdentificationNumber', CStrings::strTrimDef( $strIdentificationNumber, 15, NULL, true ) );
	}

	public function getIdentificationNumber() {
		return $this->m_strIdentificationNumber;
	}

	public function sqlIdentificationNumber() {
		return ( true == isset( $this->m_strIdentificationNumber ) ) ? '\'' . addslashes( $this->m_strIdentificationNumber ) . '\'' : 'NULL';
	}

	public function setReceivingCompanyName( $strReceivingCompanyName ) {
		$this->set( 'm_strReceivingCompanyName', CStrings::strTrimDef( $strReceivingCompanyName, 22, NULL, true ) );
	}

	public function getReceivingCompanyName() {
		return $this->m_strReceivingCompanyName;
	}

	public function sqlReceivingCompanyName() {
		return ( true == isset( $this->m_strReceivingCompanyName ) ) ? '\'' . addslashes( $this->m_strReceivingCompanyName ) . '\'' : 'NULL';
	}

	public function setDiscretionaryData( $strDiscretionaryData ) {
		$this->set( 'm_strDiscretionaryData', CStrings::strTrimDef( $strDiscretionaryData, 2, NULL, true ) );
	}

	public function getDiscretionaryData() {
		return $this->m_strDiscretionaryData;
	}

	public function sqlDiscretionaryData() {
		return ( true == isset( $this->m_strDiscretionaryData ) ) ? '\'' . addslashes( $this->m_strDiscretionaryData ) . '\'' : 'NULL';
	}

	public function setAddendaRecordIndicator( $strAddendaRecordIndicator ) {
		$this->set( 'm_strAddendaRecordIndicator', CStrings::strTrimDef( $strAddendaRecordIndicator, 1, NULL, true ) );
	}

	public function getAddendaRecordIndicator() {
		return $this->m_strAddendaRecordIndicator;
	}

	public function sqlAddendaRecordIndicator() {
		return ( true == isset( $this->m_strAddendaRecordIndicator ) ) ? '\'' . addslashes( $this->m_strAddendaRecordIndicator ) . '\'' : 'NULL';
	}

	public function setTraceNumber( $strTraceNumber ) {
		$this->set( 'm_strTraceNumber', CStrings::strTrimDef( $strTraceNumber, 15, NULL, true ) );
	}

	public function getTraceNumber() {
		return $this->m_strTraceNumber;
	}

	public function sqlTraceNumber() {
		return ( true == isset( $this->m_strTraceNumber ) ) ? '\'' . addslashes( $this->m_strTraceNumber ) . '\'' : 'NULL';
	}

	public function setReturnedOn( $strReturnedOn ) {
		$this->set( 'm_strReturnedOn', CStrings::strTrimDef( $strReturnedOn, -1, NULL, true ) );
	}

	public function getReturnedOn() {
		return $this->m_strReturnedOn;
	}

	public function sqlReturnedOn() {
		return ( true == isset( $this->m_strReturnedOn ) ) ? '\'' . $this->m_strReturnedOn . '\'' : 'NULL';
	}

	public function setTransferredToIntermediaryOn( $strTransferredToIntermediaryOn ) {
		$this->set( 'm_strTransferredToIntermediaryOn', CStrings::strTrimDef( $strTransferredToIntermediaryOn, -1, NULL, true ) );
	}

	public function getTransferredToIntermediaryOn() {
		return $this->m_strTransferredToIntermediaryOn;
	}

	public function sqlTransferredToIntermediaryOn() {
		return ( true == isset( $this->m_strTransferredToIntermediaryOn ) ) ? '\'' . $this->m_strTransferredToIntermediaryOn . '\'' : 'NULL';
	}

	public function setIsReconPrepped( $intIsReconPrepped ) {
		$this->set( 'm_intIsReconPrepped', CStrings::strToIntDef( $intIsReconPrepped, NULL, false ) );
	}

	public function getIsReconPrepped() {
		return $this->m_intIsReconPrepped;
	}

	public function sqlIsReconPrepped() {
		return ( true == isset( $this->m_intIsReconPrepped ) ) ? ( string ) $this->m_intIsReconPrepped : '0';
	}

	public function setIsIntermediary( $intIsIntermediary ) {
		$this->set( 'm_intIsIntermediary', CStrings::strToIntDef( $intIsIntermediary, NULL, false ) );
	}

	public function getIsIntermediary() {
		return $this->m_intIsIntermediary;
	}

	public function sqlIsIntermediary() {
		return ( true == isset( $this->m_intIsIntermediary ) ) ? ( string ) $this->m_intIsIntermediary : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, nacha_file_id, nacha_file_batch_id, clearing_batch_id, entry_detail_record_type_id, processing_bank_account_id, return_type_id, cid, intermediary_account_id, ar_payment_id, repair_ar_payment_id, settlement_distribution_id, company_payment_id, eft_instruction_id, x937_check_detail_record_id, account_id, offsetting_nacha_entry_detail_record_id, nacha_return_addenda_detail_record_id, check_account_type_id, check_routing_number, check_account_number_encrypted, offsetting_routing_number, offsetting_account_number_encrypted, record_type_code, record_transaction_code, receiving_dfi_identification, check_digit, dfi_account_number, amount, formatted_amount, identification_number, receiving_company_name, discretionary_data, addenda_record_indicator, trace_number, returned_on, transferred_to_intermediary_on, is_recon_prepped, is_intermediary, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlNachaFileId() . ', ' .
 						$this->sqlNachaFileBatchId() . ', ' .
 						$this->sqlClearingBatchId() . ', ' .
 						$this->sqlEntryDetailRecordTypeId() . ', ' .
 						$this->sqlProcessingBankAccountId() . ', ' .
 						$this->sqlReturnTypeId() . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlIntermediaryAccountId() . ', ' .
 						$this->sqlArPaymentId() . ', ' .
 						$this->sqlRepairArPaymentId() . ', ' .
 						$this->sqlSettlementDistributionId() . ', ' .
 						$this->sqlCompanyPaymentId() . ', ' .
 						$this->sqlEftInstructionId() . ', ' .
 						$this->sqlX937CheckDetailRecordId() . ', ' .
 						$this->sqlAccountId() . ', ' .
 						$this->sqlOffsettingNachaEntryDetailRecordId() . ', ' .
 						$this->sqlNachaReturnAddendaDetailRecordId() . ', ' .
 						$this->sqlCheckAccountTypeId() . ', ' .
 						$this->sqlCheckRoutingNumber() . ', ' .
 						$this->sqlCheckAccountNumberEncrypted() . ', ' .
 						$this->sqlOffsettingRoutingNumber() . ', ' .
 						$this->sqlOffsettingAccountNumberEncrypted() . ', ' .
 						$this->sqlRecordTypeCode() . ', ' .
 						$this->sqlRecordTransactionCode() . ', ' .
 						$this->sqlReceivingDfiIdentification() . ', ' .
 						$this->sqlCheckDigit() . ', ' .
 						$this->sqlDfiAccountNumber() . ', ' .
 						$this->sqlAmount() . ', ' .
 						$this->sqlFormattedAmount() . ', ' .
 						$this->sqlIdentificationNumber() . ', ' .
 						$this->sqlReceivingCompanyName() . ', ' .
 						$this->sqlDiscretionaryData() . ', ' .
 						$this->sqlAddendaRecordIndicator() . ', ' .
 						$this->sqlTraceNumber() . ', ' .
 						$this->sqlReturnedOn() . ', ' .
 						$this->sqlTransferredToIntermediaryOn() . ', ' .
 						$this->sqlIsReconPrepped() . ', ' .
 						$this->sqlIsIntermediary() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_file_id = ' . $this->sqlNachaFileId() . ','; } elseif( true == array_key_exists( 'NachaFileId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_file_id = ' . $this->sqlNachaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_file_batch_id = ' . $this->sqlNachaFileBatchId() . ','; } elseif( true == array_key_exists( 'NachaFileBatchId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_file_batch_id = ' . $this->sqlNachaFileBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clearing_batch_id = ' . $this->sqlClearingBatchId() . ','; } elseif( true == array_key_exists( 'ClearingBatchId', $this->getChangedColumns() ) ) { $strSql .= ' clearing_batch_id = ' . $this->sqlClearingBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entry_detail_record_type_id = ' . $this->sqlEntryDetailRecordTypeId() . ','; } elseif( true == array_key_exists( 'EntryDetailRecordTypeId', $this->getChangedColumns() ) ) { $strSql .= ' entry_detail_record_type_id = ' . $this->sqlEntryDetailRecordTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; } elseif( true == array_key_exists( 'ProcessingBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; } elseif( true == array_key_exists( 'ReturnTypeId', $this->getChangedColumns() ) ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' intermediary_account_id = ' . $this->sqlIntermediaryAccountId() . ','; } elseif( true == array_key_exists( 'IntermediaryAccountId', $this->getChangedColumns() ) ) { $strSql .= ' intermediary_account_id = ' . $this->sqlIntermediaryAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repair_ar_payment_id = ' . $this->sqlRepairArPaymentId() . ','; } elseif( true == array_key_exists( 'RepairArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' repair_ar_payment_id = ' . $this->sqlRepairArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId() . ','; } elseif( true == array_key_exists( 'SettlementDistributionId', $this->getChangedColumns() ) ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId() . ','; } elseif( true == array_key_exists( 'CompanyPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eft_instruction_id = ' . $this->sqlEftInstructionId() . ','; } elseif( true == array_key_exists( 'EftInstructionId', $this->getChangedColumns() ) ) { $strSql .= ' eft_instruction_id = ' . $this->sqlEftInstructionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_check_detail_record_id = ' . $this->sqlX937CheckDetailRecordId() . ','; } elseif( true == array_key_exists( 'X937CheckDetailRecordId', $this->getChangedColumns() ) ) { $strSql .= ' x937_check_detail_record_id = ' . $this->sqlX937CheckDetailRecordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offsetting_nacha_entry_detail_record_id = ' . $this->sqlOffsettingNachaEntryDetailRecordId() . ','; } elseif( true == array_key_exists( 'OffsettingNachaEntryDetailRecordId', $this->getChangedColumns() ) ) { $strSql .= ' offsetting_nacha_entry_detail_record_id = ' . $this->sqlOffsettingNachaEntryDetailRecordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_return_addenda_detail_record_id = ' . $this->sqlNachaReturnAddendaDetailRecordId() . ','; } elseif( true == array_key_exists( 'NachaReturnAddendaDetailRecordId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_return_addenda_detail_record_id = ' . $this->sqlNachaReturnAddendaDetailRecordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId() . ','; } elseif( true == array_key_exists( 'CheckAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber() . ','; } elseif( true == array_key_exists( 'CheckRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted() . ','; } elseif( true == array_key_exists( 'CheckAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offsetting_routing_number = ' . $this->sqlOffsettingRoutingNumber() . ','; } elseif( true == array_key_exists( 'OffsettingRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' offsetting_routing_number = ' . $this->sqlOffsettingRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offsetting_account_number_encrypted = ' . $this->sqlOffsettingAccountNumberEncrypted() . ','; } elseif( true == array_key_exists( 'OffsettingAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' offsetting_account_number_encrypted = ' . $this->sqlOffsettingAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_type_code = ' . $this->sqlRecordTypeCode() . ','; } elseif( true == array_key_exists( 'RecordTypeCode', $this->getChangedColumns() ) ) { $strSql .= ' record_type_code = ' . $this->sqlRecordTypeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_transaction_code = ' . $this->sqlRecordTransactionCode() . ','; } elseif( true == array_key_exists( 'RecordTransactionCode', $this->getChangedColumns() ) ) { $strSql .= ' record_transaction_code = ' . $this->sqlRecordTransactionCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' receiving_dfi_identification = ' . $this->sqlReceivingDfiIdentification() . ','; } elseif( true == array_key_exists( 'ReceivingDfiIdentification', $this->getChangedColumns() ) ) { $strSql .= ' receiving_dfi_identification = ' . $this->sqlReceivingDfiIdentification() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_digit = ' . $this->sqlCheckDigit() . ','; } elseif( true == array_key_exists( 'CheckDigit', $this->getChangedColumns() ) ) { $strSql .= ' check_digit = ' . $this->sqlCheckDigit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dfi_account_number = ' . $this->sqlDfiAccountNumber() . ','; } elseif( true == array_key_exists( 'DfiAccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' dfi_account_number = ' . $this->sqlDfiAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' formatted_amount = ' . $this->sqlFormattedAmount() . ','; } elseif( true == array_key_exists( 'FormattedAmount', $this->getChangedColumns() ) ) { $strSql .= ' formatted_amount = ' . $this->sqlFormattedAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' identification_number = ' . $this->sqlIdentificationNumber() . ','; } elseif( true == array_key_exists( 'IdentificationNumber', $this->getChangedColumns() ) ) { $strSql .= ' identification_number = ' . $this->sqlIdentificationNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' receiving_company_name = ' . $this->sqlReceivingCompanyName() . ','; } elseif( true == array_key_exists( 'ReceivingCompanyName', $this->getChangedColumns() ) ) { $strSql .= ' receiving_company_name = ' . $this->sqlReceivingCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' discretionary_data = ' . $this->sqlDiscretionaryData() . ','; } elseif( true == array_key_exists( 'DiscretionaryData', $this->getChangedColumns() ) ) { $strSql .= ' discretionary_data = ' . $this->sqlDiscretionaryData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' addenda_record_indicator = ' . $this->sqlAddendaRecordIndicator() . ','; } elseif( true == array_key_exists( 'AddendaRecordIndicator', $this->getChangedColumns() ) ) { $strSql .= ' addenda_record_indicator = ' . $this->sqlAddendaRecordIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trace_number = ' . $this->sqlTraceNumber() . ','; } elseif( true == array_key_exists( 'TraceNumber', $this->getChangedColumns() ) ) { $strSql .= ' trace_number = ' . $this->sqlTraceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; } elseif( true == array_key_exists( 'ReturnedOn', $this->getChangedColumns() ) ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transferred_to_intermediary_on = ' . $this->sqlTransferredToIntermediaryOn() . ','; } elseif( true == array_key_exists( 'TransferredToIntermediaryOn', $this->getChangedColumns() ) ) { $strSql .= ' transferred_to_intermediary_on = ' . $this->sqlTransferredToIntermediaryOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; } elseif( true == array_key_exists( 'IsReconPrepped', $this->getChangedColumns() ) ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_intermediary = ' . $this->sqlIsIntermediary() . ','; } elseif( true == array_key_exists( 'IsIntermediary', $this->getChangedColumns() ) ) { $strSql .= ' is_intermediary = ' . $this->sqlIsIntermediary() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'nacha_file_id' => $this->getNachaFileId(),
			'nacha_file_batch_id' => $this->getNachaFileBatchId(),
			'clearing_batch_id' => $this->getClearingBatchId(),
			'entry_detail_record_type_id' => $this->getEntryDetailRecordTypeId(),
			'processing_bank_account_id' => $this->getProcessingBankAccountId(),
			'return_type_id' => $this->getReturnTypeId(),
			'cid' => $this->getCid(),
			'intermediary_account_id' => $this->getIntermediaryAccountId(),
			'ar_payment_id' => $this->getArPaymentId(),
			'repair_ar_payment_id' => $this->getRepairArPaymentId(),
			'settlement_distribution_id' => $this->getSettlementDistributionId(),
			'company_payment_id' => $this->getCompanyPaymentId(),
			'eft_instruction_id' => $this->getEftInstructionId(),
			'x937_check_detail_record_id' => $this->getX937CheckDetailRecordId(),
			'account_id' => $this->getAccountId(),
			'offsetting_nacha_entry_detail_record_id' => $this->getOffsettingNachaEntryDetailRecordId(),
			'nacha_return_addenda_detail_record_id' => $this->getNachaReturnAddendaDetailRecordId(),
			'check_account_type_id' => $this->getCheckAccountTypeId(),
			'check_routing_number' => $this->getCheckRoutingNumber(),
			'check_account_number_encrypted' => $this->getCheckAccountNumberEncrypted(),
			'offsetting_routing_number' => $this->getOffsettingRoutingNumber(),
			'offsetting_account_number_encrypted' => $this->getOffsettingAccountNumberEncrypted(),
			'record_type_code' => $this->getRecordTypeCode(),
			'record_transaction_code' => $this->getRecordTransactionCode(),
			'receiving_dfi_identification' => $this->getReceivingDfiIdentification(),
			'check_digit' => $this->getCheckDigit(),
			'dfi_account_number' => $this->getDfiAccountNumber(),
			'amount' => $this->getAmount(),
			'formatted_amount' => $this->getFormattedAmount(),
			'identification_number' => $this->getIdentificationNumber(),
			'receiving_company_name' => $this->getReceivingCompanyName(),
			'discretionary_data' => $this->getDiscretionaryData(),
			'addenda_record_indicator' => $this->getAddendaRecordIndicator(),
			'trace_number' => $this->getTraceNumber(),
			'returned_on' => $this->getReturnedOn(),
			'transferred_to_intermediary_on' => $this->getTransferredToIntermediaryOn(),
			'is_recon_prepped' => $this->getIsReconPrepped(),
			'is_intermediary' => $this->getIsIntermediary(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>