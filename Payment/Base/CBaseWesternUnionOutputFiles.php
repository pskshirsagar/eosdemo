<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CWesternUnionOutputFiles
 * Do not add any new functions to this class.
 */

class CBaseWesternUnionOutputFiles extends CEosPluralBase {

	/**
	 * @return CWesternUnionOutputFile[]
	 */
	public static function fetchWesternUnionOutputFiles( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CWesternUnionOutputFile::class, $objDatabase );
	}

	/**
	 * @return CWesternUnionOutputFile
	 */
	public static function fetchWesternUnionOutputFile( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CWesternUnionOutputFile::class, $objDatabase );
	}

	public static function fetchWesternUnionOutputFileCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'western_union_output_files', $objDatabase );
	}

	public static function fetchWesternUnionOutputFileById( $intId, $objDatabase ) {
		return self::fetchWesternUnionOutputFile( sprintf( 'SELECT * FROM western_union_output_files WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchWesternUnionOutputFilesByProcessingBankId( $intProcessingBankId, $objDatabase ) {
		return self::fetchWesternUnionOutputFiles( sprintf( 'SELECT * FROM western_union_output_files WHERE processing_bank_id = %d', ( int ) $intProcessingBankId ), $objDatabase );
	}

	public static function fetchWesternUnionOutputFilesByProcessingBankAccountId( $intProcessingBankAccountId, $objDatabase ) {
		return self::fetchWesternUnionOutputFiles( sprintf( 'SELECT * FROM western_union_output_files WHERE processing_bank_account_id = %d', ( int ) $intProcessingBankAccountId ), $objDatabase );
	}

	public static function fetchWesternUnionOutputFilesByFileTypeId( $intFileTypeId, $objDatabase ) {
		return self::fetchWesternUnionOutputFiles( sprintf( 'SELECT * FROM western_union_output_files WHERE file_type_id = %d', ( int ) $intFileTypeId ), $objDatabase );
	}

	public static function fetchWesternUnionOutputFilesByHeaderClientId( $strHeaderClientId, $objDatabase ) {
		return self::fetchWesternUnionOutputFiles( sprintf( 'SELECT * FROM western_union_output_files WHERE header_client_id = \'%s\'', $strHeaderClientId ), $objDatabase );
	}

}
?>