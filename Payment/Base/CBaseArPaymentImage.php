<?php

class CBaseArPaymentImage extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.ar_payment_images';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intArPaymentId;
	protected $m_intFileExtensionId;
	protected $m_intPaymentImageTypeId;
	protected $m_strImagePath;
	protected $m_strImageName;
	protected $m_strImageContent;
	protected $m_strImageHash;
	protected $m_intOrderNum;
	protected $m_intLockNumber;
	protected $m_strLockedOn;
	protected $m_strEndorsedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['file_extension_id'] ) && $boolDirectSet ) $this->set( 'm_intFileExtensionId', trim( $arrValues['file_extension_id'] ) ); elseif( isset( $arrValues['file_extension_id'] ) ) $this->setFileExtensionId( $arrValues['file_extension_id'] );
		if( isset( $arrValues['payment_image_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentImageTypeId', trim( $arrValues['payment_image_type_id'] ) ); elseif( isset( $arrValues['payment_image_type_id'] ) ) $this->setPaymentImageTypeId( $arrValues['payment_image_type_id'] );
		if( isset( $arrValues['image_path'] ) && $boolDirectSet ) $this->set( 'm_strImagePath', trim( stripcslashes( $arrValues['image_path'] ) ) ); elseif( isset( $arrValues['image_path'] ) ) $this->setImagePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_path'] ) : $arrValues['image_path'] );
		if( isset( $arrValues['image_name'] ) && $boolDirectSet ) $this->set( 'm_strImageName', trim( stripcslashes( $arrValues['image_name'] ) ) ); elseif( isset( $arrValues['image_name'] ) ) $this->setImageName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_name'] ) : $arrValues['image_name'] );
		if( isset( $arrValues['image_content'] ) && $boolDirectSet ) $this->set( 'm_strImageContent', trim( stripcslashes( $arrValues['image_content'] ) ) ); elseif( isset( $arrValues['image_content'] ) ) $this->setImageContent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_content'] ) : $arrValues['image_content'] );
		if( isset( $arrValues['image_hash'] ) && $boolDirectSet ) $this->set( 'm_strImageHash', trim( stripcslashes( $arrValues['image_hash'] ) ) ); elseif( isset( $arrValues['image_hash'] ) ) $this->setImageHash( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_hash'] ) : $arrValues['image_hash'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['lock_number'] ) && $boolDirectSet ) $this->set( 'm_intLockNumber', trim( $arrValues['lock_number'] ) ); elseif( isset( $arrValues['lock_number'] ) ) $this->setLockNumber( $arrValues['lock_number'] );
		if( isset( $arrValues['locked_on'] ) && $boolDirectSet ) $this->set( 'm_strLockedOn', trim( $arrValues['locked_on'] ) ); elseif( isset( $arrValues['locked_on'] ) ) $this->setLockedOn( $arrValues['locked_on'] );
		if( isset( $arrValues['endorsed_on'] ) && $boolDirectSet ) $this->set( 'm_strEndorsedOn', trim( $arrValues['endorsed_on'] ) ); elseif( isset( $arrValues['endorsed_on'] ) ) $this->setEndorsedOn( $arrValues['endorsed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setFileExtensionId( $intFileExtensionId ) {
		$this->set( 'm_intFileExtensionId', CStrings::strToIntDef( $intFileExtensionId, NULL, false ) );
	}

	public function getFileExtensionId() {
		return $this->m_intFileExtensionId;
	}

	public function sqlFileExtensionId() {
		return ( true == isset( $this->m_intFileExtensionId ) ) ? ( string ) $this->m_intFileExtensionId : 'NULL';
	}

	public function setPaymentImageTypeId( $intPaymentImageTypeId ) {
		$this->set( 'm_intPaymentImageTypeId', CStrings::strToIntDef( $intPaymentImageTypeId, NULL, false ) );
	}

	public function getPaymentImageTypeId() {
		return $this->m_intPaymentImageTypeId;
	}

	public function sqlPaymentImageTypeId() {
		return ( true == isset( $this->m_intPaymentImageTypeId ) ) ? ( string ) $this->m_intPaymentImageTypeId : 'NULL';
	}

	public function setImagePath( $strImagePath ) {
		$this->set( 'm_strImagePath', CStrings::strTrimDef( $strImagePath, 4096, NULL, true ) );
	}

	public function getImagePath() {
		return $this->m_strImagePath;
	}

	public function sqlImagePath() {
		return ( true == isset( $this->m_strImagePath ) ) ? '\'' . addslashes( $this->m_strImagePath ) . '\'' : 'NULL';
	}

	public function setImageName( $strImageName ) {
		$this->set( 'm_strImageName', CStrings::strTrimDef( $strImageName, 50, NULL, true ) );
	}

	public function getImageName() {
		return $this->m_strImageName;
	}

	public function sqlImageName() {
		return ( true == isset( $this->m_strImageName ) ) ? '\'' . addslashes( $this->m_strImageName ) . '\'' : 'NULL';
	}

	public function setImageContent( $strImageContent ) {
		$this->set( 'm_strImageContent', CStrings::strTrimDef( $strImageContent, -1, NULL, true ) );
	}

	public function getImageContent() {
		return $this->m_strImageContent;
	}

	public function sqlImageContent() {
		return ( true == isset( $this->m_strImageContent ) ) ? '\'' . addslashes( $this->m_strImageContent ) . '\'' : 'NULL';
	}

	public function setImageHash( $strImageHash ) {
		$this->set( 'm_strImageHash', CStrings::strTrimDef( $strImageHash, 240, NULL, true ) );
	}

	public function getImageHash() {
		return $this->m_strImageHash;
	}

	public function sqlImageHash() {
		return ( true == isset( $this->m_strImageHash ) ) ? '\'' . addslashes( $this->m_strImageHash ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setLockNumber( $intLockNumber ) {
		$this->set( 'm_intLockNumber', CStrings::strToIntDef( $intLockNumber, NULL, false ) );
	}

	public function getLockNumber() {
		return $this->m_intLockNumber;
	}

	public function sqlLockNumber() {
		return ( true == isset( $this->m_intLockNumber ) ) ? ( string ) $this->m_intLockNumber : 'NULL';
	}

	public function setLockedOn( $strLockedOn ) {
		$this->set( 'm_strLockedOn', CStrings::strTrimDef( $strLockedOn, -1, NULL, true ) );
	}

	public function getLockedOn() {
		return $this->m_strLockedOn;
	}

	public function sqlLockedOn() {
		return ( true == isset( $this->m_strLockedOn ) ) ? '\'' . $this->m_strLockedOn . '\'' : 'NULL';
	}

	public function setEndorsedOn( $strEndorsedOn ) {
		$this->set( 'm_strEndorsedOn', CStrings::strTrimDef( $strEndorsedOn, -1, NULL, true ) );
	}

	public function getEndorsedOn() {
		return $this->m_strEndorsedOn;
	}

	public function sqlEndorsedOn() {
		return ( true == isset( $this->m_strEndorsedOn ) ) ? '\'' . $this->m_strEndorsedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ar_payment_id, file_extension_id, payment_image_type_id, image_path, image_name, image_content, image_hash, order_num, lock_number, locked_on, endorsed_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlArPaymentId() . ', ' .
						$this->sqlFileExtensionId() . ', ' .
						$this->sqlPaymentImageTypeId() . ', ' .
						$this->sqlImagePath() . ', ' .
						$this->sqlImageName() . ', ' .
						$this->sqlImageContent() . ', ' .
						$this->sqlImageHash() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlLockNumber() . ', ' .
						$this->sqlLockedOn() . ', ' .
						$this->sqlEndorsedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId(). ',' ; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_extension_id = ' . $this->sqlFileExtensionId(). ',' ; } elseif( true == array_key_exists( 'FileExtensionId', $this->getChangedColumns() ) ) { $strSql .= ' file_extension_id = ' . $this->sqlFileExtensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_image_type_id = ' . $this->sqlPaymentImageTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentImageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_image_type_id = ' . $this->sqlPaymentImageTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_path = ' . $this->sqlImagePath(). ',' ; } elseif( true == array_key_exists( 'ImagePath', $this->getChangedColumns() ) ) { $strSql .= ' image_path = ' . $this->sqlImagePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_name = ' . $this->sqlImageName(). ',' ; } elseif( true == array_key_exists( 'ImageName', $this->getChangedColumns() ) ) { $strSql .= ' image_name = ' . $this->sqlImageName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_content = ' . $this->sqlImageContent(). ',' ; } elseif( true == array_key_exists( 'ImageContent', $this->getChangedColumns() ) ) { $strSql .= ' image_content = ' . $this->sqlImageContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_hash = ' . $this->sqlImageHash(). ',' ; } elseif( true == array_key_exists( 'ImageHash', $this->getChangedColumns() ) ) { $strSql .= ' image_hash = ' . $this->sqlImageHash() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lock_number = ' . $this->sqlLockNumber(). ',' ; } elseif( true == array_key_exists( 'LockNumber', $this->getChangedColumns() ) ) { $strSql .= ' lock_number = ' . $this->sqlLockNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn(). ',' ; } elseif( true == array_key_exists( 'LockedOn', $this->getChangedColumns() ) ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' endorsed_on = ' . $this->sqlEndorsedOn(). ',' ; } elseif( true == array_key_exists( 'EndorsedOn', $this->getChangedColumns() ) ) { $strSql .= ' endorsed_on = ' . $this->sqlEndorsedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ar_payment_id' => $this->getArPaymentId(),
			'file_extension_id' => $this->getFileExtensionId(),
			'payment_image_type_id' => $this->getPaymentImageTypeId(),
			'image_path' => $this->getImagePath(),
			'image_name' => $this->getImageName(),
			'image_content' => $this->getImageContent(),
			'image_hash' => $this->getImageHash(),
			'order_num' => $this->getOrderNum(),
			'lock_number' => $this->getLockNumber(),
			'locked_on' => $this->getLockedOn(),
			'endorsed_on' => $this->getEndorsedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>