<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937FileTypes
 * Do not add any new functions to this class.
 */

class CBaseX937FileTypes extends CEosPluralBase {

	/**
	 * @return CX937FileType[]
	 */
	public static function fetchX937FileTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CX937FileType::class, $objDatabase );
	}

	/**
	 * @return CX937FileType
	 */
	public static function fetchX937FileType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CX937FileType::class, $objDatabase );
	}

	public static function fetchX937FileTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'x937_file_types', $objDatabase );
	}

	public static function fetchX937FileTypeById( $intId, $objDatabase ) {
		return self::fetchX937FileType( sprintf( 'SELECT * FROM x937_file_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>