<?php

class CBaseMoneyGramBillPayment extends CEosSingularBase {

	const TABLE_NAME = 'public.money_gram_bill_payments';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMoneyGramAccountId;
	protected $m_intAgentId;
	protected $m_strMgReferenceNumber;
	protected $m_intArPaymentId;
	protected $m_fltCustomerFee;
	protected $m_fltPaymentAmount;
	protected $m_strFirstName;
	protected $m_strMiddleName;
	protected $m_strLastName;
	protected $m_strAddress1;
	protected $m_strCity;
	protected $m_strState;
	protected $m_strPostalCode;
	protected $m_strPhoneNumber;
	protected $m_strAgentZip;
	protected $m_strProcessedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['money_gram_account_id'] ) && $boolDirectSet ) $this->set( 'm_intMoneyGramAccountId', trim( $arrValues['money_gram_account_id'] ) ); elseif( isset( $arrValues['money_gram_account_id'] ) ) $this->setMoneyGramAccountId( $arrValues['money_gram_account_id'] );
		if( isset( $arrValues['agent_id'] ) && $boolDirectSet ) $this->set( 'm_intAgentId', trim( $arrValues['agent_id'] ) ); elseif( isset( $arrValues['agent_id'] ) ) $this->setAgentId( $arrValues['agent_id'] );
		if( isset( $arrValues['mg_reference_number'] ) && $boolDirectSet ) $this->set( 'm_strMgReferenceNumber', trim( stripcslashes( $arrValues['mg_reference_number'] ) ) ); elseif( isset( $arrValues['mg_reference_number'] ) ) $this->setMgReferenceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mg_reference_number'] ) : $arrValues['mg_reference_number'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['customer_fee'] ) && $boolDirectSet ) $this->set( 'm_fltCustomerFee', trim( $arrValues['customer_fee'] ) ); elseif( isset( $arrValues['customer_fee'] ) ) $this->setCustomerFee( $arrValues['customer_fee'] );
		if( isset( $arrValues['payment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentAmount', trim( $arrValues['payment_amount'] ) ); elseif( isset( $arrValues['payment_amount'] ) ) $this->setPaymentAmount( $arrValues['payment_amount'] );
		if( isset( $arrValues['first_name'] ) && $boolDirectSet ) $this->set( 'm_strFirstName', trim( stripcslashes( $arrValues['first_name'] ) ) ); elseif( isset( $arrValues['first_name'] ) ) $this->setFirstName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['first_name'] ) : $arrValues['first_name'] );
		if( isset( $arrValues['middle_name'] ) && $boolDirectSet ) $this->set( 'm_strMiddleName', trim( stripcslashes( $arrValues['middle_name'] ) ) ); elseif( isset( $arrValues['middle_name'] ) ) $this->setMiddleName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['middle_name'] ) : $arrValues['middle_name'] );
		if( isset( $arrValues['last_name'] ) && $boolDirectSet ) $this->set( 'm_strLastName', trim( stripcslashes( $arrValues['last_name'] ) ) ); elseif( isset( $arrValues['last_name'] ) ) $this->setLastName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['last_name'] ) : $arrValues['last_name'] );
		if( isset( $arrValues['address1'] ) && $boolDirectSet ) $this->set( 'm_strAddress1', trim( stripcslashes( $arrValues['address1'] ) ) ); elseif( isset( $arrValues['address1'] ) ) $this->setAddress1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['address1'] ) : $arrValues['address1'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state'] ) && $boolDirectSet ) $this->set( 'm_strState', trim( stripcslashes( $arrValues['state'] ) ) ); elseif( isset( $arrValues['state'] ) ) $this->setState( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state'] ) : $arrValues['state'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['agent_zip'] ) && $boolDirectSet ) $this->set( 'm_strAgentZip', trim( stripcslashes( $arrValues['agent_zip'] ) ) ); elseif( isset( $arrValues['agent_zip'] ) ) $this->setAgentZip( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['agent_zip'] ) : $arrValues['agent_zip'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMoneyGramAccountId( $intMoneyGramAccountId ) {
		$this->set( 'm_intMoneyGramAccountId', CStrings::strToIntDef( $intMoneyGramAccountId, NULL, false ) );
	}

	public function getMoneyGramAccountId() {
		return $this->m_intMoneyGramAccountId;
	}

	public function sqlMoneyGramAccountId() {
		return ( true == isset( $this->m_intMoneyGramAccountId ) ) ? ( string ) $this->m_intMoneyGramAccountId : 'NULL';
	}

	public function setAgentId( $intAgentId ) {
		$this->set( 'm_intAgentId', CStrings::strToIntDef( $intAgentId, NULL, false ) );
	}

	public function getAgentId() {
		return $this->m_intAgentId;
	}

	public function sqlAgentId() {
		return ( true == isset( $this->m_intAgentId ) ) ? ( string ) $this->m_intAgentId : 'NULL';
	}

	public function setMgReferenceNumber( $strMgReferenceNumber ) {
		$this->set( 'm_strMgReferenceNumber', CStrings::strTrimDef( $strMgReferenceNumber, 8, NULL, true ) );
	}

	public function getMgReferenceNumber() {
		return $this->m_strMgReferenceNumber;
	}

	public function sqlMgReferenceNumber() {
		return ( true == isset( $this->m_strMgReferenceNumber ) ) ? '\'' . addslashes( $this->m_strMgReferenceNumber ) . '\'' : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setCustomerFee( $fltCustomerFee ) {
		$this->set( 'm_fltCustomerFee', CStrings::strToFloatDef( $fltCustomerFee, NULL, false, 3 ) );
	}

	public function getCustomerFee() {
		return $this->m_fltCustomerFee;
	}

	public function sqlCustomerFee() {
		return ( true == isset( $this->m_fltCustomerFee ) ) ? ( string ) $this->m_fltCustomerFee : 'NULL';
	}

	public function setPaymentAmount( $fltPaymentAmount ) {
		$this->set( 'm_fltPaymentAmount', CStrings::strToFloatDef( $fltPaymentAmount, NULL, false, 3 ) );
	}

	public function getPaymentAmount() {
		return $this->m_fltPaymentAmount;
	}

	public function sqlPaymentAmount() {
		return ( true == isset( $this->m_fltPaymentAmount ) ) ? ( string ) $this->m_fltPaymentAmount : 'NULL';
	}

	public function setFirstName( $strFirstName ) {
		$this->set( 'm_strFirstName', CStrings::strTrimDef( $strFirstName, 40, NULL, true ) );
	}

	public function getFirstName() {
		return $this->m_strFirstName;
	}

	public function sqlFirstName() {
		return ( true == isset( $this->m_strFirstName ) ) ? '\'' . addslashes( $this->m_strFirstName ) . '\'' : 'NULL';
	}

	public function setMiddleName( $strMiddleName ) {
		$this->set( 'm_strMiddleName', CStrings::strTrimDef( $strMiddleName, 40, NULL, true ) );
	}

	public function getMiddleName() {
		return $this->m_strMiddleName;
	}

	public function sqlMiddleName() {
		return ( true == isset( $this->m_strMiddleName ) ) ? '\'' . addslashes( $this->m_strMiddleName ) . '\'' : 'NULL';
	}

	public function setLastName( $strLastName ) {
		$this->set( 'm_strLastName', CStrings::strTrimDef( $strLastName, 40, NULL, true ) );
	}

	public function getLastName() {
		return $this->m_strLastName;
	}

	public function sqlLastName() {
		return ( true == isset( $this->m_strLastName ) ) ? '\'' . addslashes( $this->m_strLastName ) . '\'' : 'NULL';
	}

	public function setAddress1( $strAddress1 ) {
		$this->set( 'm_strAddress1', CStrings::strTrimDef( $strAddress1, 60, NULL, true ) );
	}

	public function getAddress1() {
		return $this->m_strAddress1;
	}

	public function sqlAddress1() {
		return ( true == isset( $this->m_strAddress1 ) ) ? '\'' . addslashes( $this->m_strAddress1 ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 40, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setState( $strState ) {
		$this->set( 'm_strState', CStrings::strTrimDef( $strState, 40, NULL, true ) );
	}

	public function getState() {
		return $this->m_strState;
	}

	public function sqlState() {
		return ( true == isset( $this->m_strState ) ) ? '\'' . addslashes( $this->m_strState ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 14, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 17, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setAgentZip( $strAgentZip ) {
		$this->set( 'm_strAgentZip', CStrings::strTrimDef( $strAgentZip, 14, NULL, true ) );
	}

	public function getAgentZip() {
		return $this->m_strAgentZip;
	}

	public function sqlAgentZip() {
		return ( true == isset( $this->m_strAgentZip ) ) ? '\'' . addslashes( $this->m_strAgentZip ) . '\'' : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, money_gram_account_id, agent_id, mg_reference_number, ar_payment_id, customer_fee, payment_amount, first_name, middle_name, last_name, address1, city, state, postal_code, phone_number, agent_zip, processed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlMoneyGramAccountId() . ', ' .
 						$this->sqlAgentId() . ', ' .
 						$this->sqlMgReferenceNumber() . ', ' .
 						$this->sqlArPaymentId() . ', ' .
 						$this->sqlCustomerFee() . ', ' .
 						$this->sqlPaymentAmount() . ', ' .
 						$this->sqlFirstName() . ', ' .
 						$this->sqlMiddleName() . ', ' .
 						$this->sqlLastName() . ', ' .
 						$this->sqlAddress1() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlState() . ', ' .
 						$this->sqlPostalCode() . ', ' .
 						$this->sqlPhoneNumber() . ', ' .
 						$this->sqlAgentZip() . ', ' .
 						$this->sqlProcessedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' money_gram_account_id = ' . $this->sqlMoneyGramAccountId() . ','; } elseif( true == array_key_exists( 'MoneyGramAccountId', $this->getChangedColumns() ) ) { $strSql .= ' money_gram_account_id = ' . $this->sqlMoneyGramAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agent_id = ' . $this->sqlAgentId() . ','; } elseif( true == array_key_exists( 'AgentId', $this->getChangedColumns() ) ) { $strSql .= ' agent_id = ' . $this->sqlAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mg_reference_number = ' . $this->sqlMgReferenceNumber() . ','; } elseif( true == array_key_exists( 'MgReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' mg_reference_number = ' . $this->sqlMgReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_fee = ' . $this->sqlCustomerFee() . ','; } elseif( true == array_key_exists( 'CustomerFee', $this->getChangedColumns() ) ) { $strSql .= ' customer_fee = ' . $this->sqlCustomerFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_amount = ' . $this->sqlPaymentAmount() . ','; } elseif( true == array_key_exists( 'PaymentAmount', $this->getChangedColumns() ) ) { $strSql .= ' payment_amount = ' . $this->sqlPaymentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_name = ' . $this->sqlFirstName() . ','; } elseif( true == array_key_exists( 'FirstName', $this->getChangedColumns() ) ) { $strSql .= ' first_name = ' . $this->sqlFirstName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' middle_name = ' . $this->sqlMiddleName() . ','; } elseif( true == array_key_exists( 'MiddleName', $this->getChangedColumns() ) ) { $strSql .= ' middle_name = ' . $this->sqlMiddleName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_name = ' . $this->sqlLastName() . ','; } elseif( true == array_key_exists( 'LastName', $this->getChangedColumns() ) ) { $strSql .= ' last_name = ' . $this->sqlLastName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address1 = ' . $this->sqlAddress1() . ','; } elseif( true == array_key_exists( 'Address1', $this->getChangedColumns() ) ) { $strSql .= ' address1 = ' . $this->sqlAddress1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state = ' . $this->sqlState() . ','; } elseif( true == array_key_exists( 'State', $this->getChangedColumns() ) ) { $strSql .= ' state = ' . $this->sqlState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agent_zip = ' . $this->sqlAgentZip() . ','; } elseif( true == array_key_exists( 'AgentZip', $this->getChangedColumns() ) ) { $strSql .= ' agent_zip = ' . $this->sqlAgentZip() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'money_gram_account_id' => $this->getMoneyGramAccountId(),
			'agent_id' => $this->getAgentId(),
			'mg_reference_number' => $this->getMgReferenceNumber(),
			'ar_payment_id' => $this->getArPaymentId(),
			'customer_fee' => $this->getCustomerFee(),
			'payment_amount' => $this->getPaymentAmount(),
			'first_name' => $this->getFirstName(),
			'middle_name' => $this->getMiddleName(),
			'last_name' => $this->getLastName(),
			'address1' => $this->getAddress1(),
			'city' => $this->getCity(),
			'state' => $this->getState(),
			'postal_code' => $this->getPostalCode(),
			'phone_number' => $this->getPhoneNumber(),
			'agent_zip' => $this->getAgentZip(),
			'processed_on' => $this->getProcessedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>