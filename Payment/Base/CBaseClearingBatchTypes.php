<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CClearingBatchTypes
 * Do not add any new functions to this class.
 */

class CBaseClearingBatchTypes extends CEosPluralBase {

	/**
	 * @return CClearingBatchType[]
	 */
	public static function fetchClearingBatchTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CClearingBatchType::class, $objDatabase );
	}

	/**
	 * @return CClearingBatchType
	 */
	public static function fetchClearingBatchType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CClearingBatchType::class, $objDatabase );
	}

	public static function fetchClearingBatchTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'clearing_batch_types', $objDatabase );
	}

	public static function fetchClearingBatchTypeById( $intId, $objDatabase ) {
		return self::fetchClearingBatchType( sprintf( 'SELECT * FROM clearing_batch_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>