<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMerchantChangeRequestAccountTypes
 * Do not add any new functions to this class.
 */

class CBaseMerchantChangeRequestAccountTypes extends CEosPluralBase {

	/**
	 * @return CMerchantChangeRequestAccountType[]
	 */
	public static function fetchMerchantChangeRequestAccountTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMerchantChangeRequestAccountType::class, $objDatabase );
	}

	/**
	 * @return CMerchantChangeRequestAccountType
	 */
	public static function fetchMerchantChangeRequestAccountType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMerchantChangeRequestAccountType::class, $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'merchant_change_request_account_types', $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountTypeById( $intId, $objDatabase ) {
		return self::fetchMerchantChangeRequestAccountType( sprintf( 'SELECT * FROM merchant_change_request_account_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>