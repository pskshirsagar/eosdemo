<?php

class CBaseWesternUnionInputFile extends CEosSingularBase {

	const TABLE_NAME = 'public.western_union_input_files';

	protected $m_intId;
	protected $m_intFileTypeId;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strHeaderRecordType;
	protected $m_strHeaderInputType;
	protected $m_strHeaderServiceType;
	protected $m_strHeaderDate;
	protected $m_strHeaderTime;
	protected $m_strHeaderCycleNumber;
	protected $m_strHeaderClientId;
	protected $m_strHeaderClientName;
	protected $m_strHeaderFiller;
	protected $m_strTrailerRecordType;
	protected $m_strTrailerAddCount;
	protected $m_strTrailerStopCount;
	protected $m_strTrailerNumberOfRecords;
	protected $m_strTrailerTotalPurchaseAmount;
	protected $m_strTrailerFiller;
	protected $m_strSentOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFileTypeId', trim( $arrValues['file_type_id'] ) ); elseif( isset( $arrValues['file_type_id'] ) ) $this->setFileTypeId( $arrValues['file_type_id'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['header_record_type'] ) && $boolDirectSet ) $this->set( 'm_strHeaderRecordType', trim( stripcslashes( $arrValues['header_record_type'] ) ) ); elseif( isset( $arrValues['header_record_type'] ) ) $this->setHeaderRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_record_type'] ) : $arrValues['header_record_type'] );
		if( isset( $arrValues['header_input_type'] ) && $boolDirectSet ) $this->set( 'm_strHeaderInputType', trim( stripcslashes( $arrValues['header_input_type'] ) ) ); elseif( isset( $arrValues['header_input_type'] ) ) $this->setHeaderInputType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_input_type'] ) : $arrValues['header_input_type'] );
		if( isset( $arrValues['header_service_type'] ) && $boolDirectSet ) $this->set( 'm_strHeaderServiceType', trim( stripcslashes( $arrValues['header_service_type'] ) ) ); elseif( isset( $arrValues['header_service_type'] ) ) $this->setHeaderServiceType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_service_type'] ) : $arrValues['header_service_type'] );
		if( isset( $arrValues['header_date'] ) && $boolDirectSet ) $this->set( 'm_strHeaderDate', trim( stripcslashes( $arrValues['header_date'] ) ) ); elseif( isset( $arrValues['header_date'] ) ) $this->setHeaderDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_date'] ) : $arrValues['header_date'] );
		if( isset( $arrValues['header_time'] ) && $boolDirectSet ) $this->set( 'm_strHeaderTime', trim( stripcslashes( $arrValues['header_time'] ) ) ); elseif( isset( $arrValues['header_time'] ) ) $this->setHeaderTime( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_time'] ) : $arrValues['header_time'] );
		if( isset( $arrValues['header_cycle_number'] ) && $boolDirectSet ) $this->set( 'm_strHeaderCycleNumber', trim( stripcslashes( $arrValues['header_cycle_number'] ) ) ); elseif( isset( $arrValues['header_cycle_number'] ) ) $this->setHeaderCycleNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_cycle_number'] ) : $arrValues['header_cycle_number'] );
		if( isset( $arrValues['header_client_id'] ) && $boolDirectSet ) $this->set( 'm_strHeaderClientId', trim( stripcslashes( $arrValues['header_client_id'] ) ) ); elseif( isset( $arrValues['header_client_id'] ) ) $this->setHeaderClientId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_client_id'] ) : $arrValues['header_client_id'] );
		if( isset( $arrValues['header_client_name'] ) && $boolDirectSet ) $this->set( 'm_strHeaderClientName', trim( stripcslashes( $arrValues['header_client_name'] ) ) ); elseif( isset( $arrValues['header_client_name'] ) ) $this->setHeaderClientName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_client_name'] ) : $arrValues['header_client_name'] );
		if( isset( $arrValues['header_filler'] ) && $boolDirectSet ) $this->set( 'm_strHeaderFiller', trim( stripcslashes( $arrValues['header_filler'] ) ) ); elseif( isset( $arrValues['header_filler'] ) ) $this->setHeaderFiller( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_filler'] ) : $arrValues['header_filler'] );
		if( isset( $arrValues['trailer_record_type'] ) && $boolDirectSet ) $this->set( 'm_strTrailerRecordType', trim( stripcslashes( $arrValues['trailer_record_type'] ) ) ); elseif( isset( $arrValues['trailer_record_type'] ) ) $this->setTrailerRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['trailer_record_type'] ) : $arrValues['trailer_record_type'] );
		if( isset( $arrValues['trailer_add_count'] ) && $boolDirectSet ) $this->set( 'm_strTrailerAddCount', trim( stripcslashes( $arrValues['trailer_add_count'] ) ) ); elseif( isset( $arrValues['trailer_add_count'] ) ) $this->setTrailerAddCount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['trailer_add_count'] ) : $arrValues['trailer_add_count'] );
		if( isset( $arrValues['trailer_stop_count'] ) && $boolDirectSet ) $this->set( 'm_strTrailerStopCount', trim( stripcslashes( $arrValues['trailer_stop_count'] ) ) ); elseif( isset( $arrValues['trailer_stop_count'] ) ) $this->setTrailerStopCount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['trailer_stop_count'] ) : $arrValues['trailer_stop_count'] );
		if( isset( $arrValues['trailer_number_of_records'] ) && $boolDirectSet ) $this->set( 'm_strTrailerNumberOfRecords', trim( stripcslashes( $arrValues['trailer_number_of_records'] ) ) ); elseif( isset( $arrValues['trailer_number_of_records'] ) ) $this->setTrailerNumberOfRecords( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['trailer_number_of_records'] ) : $arrValues['trailer_number_of_records'] );
		if( isset( $arrValues['trailer_total_purchase_amount'] ) && $boolDirectSet ) $this->set( 'm_strTrailerTotalPurchaseAmount', trim( stripcslashes( $arrValues['trailer_total_purchase_amount'] ) ) ); elseif( isset( $arrValues['trailer_total_purchase_amount'] ) ) $this->setTrailerTotalPurchaseAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['trailer_total_purchase_amount'] ) : $arrValues['trailer_total_purchase_amount'] );
		if( isset( $arrValues['trailer_filler'] ) && $boolDirectSet ) $this->set( 'm_strTrailerFiller', trim( stripcslashes( $arrValues['trailer_filler'] ) ) ); elseif( isset( $arrValues['trailer_filler'] ) ) $this->setTrailerFiller( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['trailer_filler'] ) : $arrValues['trailer_filler'] );
		if( isset( $arrValues['sent_on'] ) && $boolDirectSet ) $this->set( 'm_strSentOn', trim( $arrValues['sent_on'] ) ); elseif( isset( $arrValues['sent_on'] ) ) $this->setSentOn( $arrValues['sent_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setFileTypeId( $intFileTypeId ) {
		$this->set( 'm_intFileTypeId', CStrings::strToIntDef( $intFileTypeId, NULL, false ) );
	}

	public function getFileTypeId() {
		return $this->m_intFileTypeId;
	}

	public function sqlFileTypeId() {
		return ( true == isset( $this->m_intFileTypeId ) ) ? ( string ) $this->m_intFileTypeId : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setHeaderRecordType( $strHeaderRecordType ) {
		$this->set( 'm_strHeaderRecordType', CStrings::strTrimDef( $strHeaderRecordType, 1, NULL, true ) );
	}

	public function getHeaderRecordType() {
		return $this->m_strHeaderRecordType;
	}

	public function sqlHeaderRecordType() {
		return ( true == isset( $this->m_strHeaderRecordType ) ) ? '\'' . addslashes( $this->m_strHeaderRecordType ) . '\'' : 'NULL';
	}

	public function setHeaderInputType( $strHeaderInputType ) {
		$this->set( 'm_strHeaderInputType', CStrings::strTrimDef( $strHeaderInputType, 3, NULL, true ) );
	}

	public function getHeaderInputType() {
		return $this->m_strHeaderInputType;
	}

	public function sqlHeaderInputType() {
		return ( true == isset( $this->m_strHeaderInputType ) ) ? '\'' . addslashes( $this->m_strHeaderInputType ) . '\'' : 'NULL';
	}

	public function setHeaderServiceType( $strHeaderServiceType ) {
		$this->set( 'm_strHeaderServiceType', CStrings::strTrimDef( $strHeaderServiceType, 3, NULL, true ) );
	}

	public function getHeaderServiceType() {
		return $this->m_strHeaderServiceType;
	}

	public function sqlHeaderServiceType() {
		return ( true == isset( $this->m_strHeaderServiceType ) ) ? '\'' . addslashes( $this->m_strHeaderServiceType ) . '\'' : 'NULL';
	}

	public function setHeaderDate( $strHeaderDate ) {
		$this->set( 'm_strHeaderDate', CStrings::strTrimDef( $strHeaderDate, 8, NULL, true ) );
	}

	public function getHeaderDate() {
		return $this->m_strHeaderDate;
	}

	public function sqlHeaderDate() {
		return ( true == isset( $this->m_strHeaderDate ) ) ? '\'' . addslashes( $this->m_strHeaderDate ) . '\'' : 'NULL';
	}

	public function setHeaderTime( $strHeaderTime ) {
		$this->set( 'm_strHeaderTime', CStrings::strTrimDef( $strHeaderTime, 6, NULL, true ) );
	}

	public function getHeaderTime() {
		return $this->m_strHeaderTime;
	}

	public function sqlHeaderTime() {
		return ( true == isset( $this->m_strHeaderTime ) ) ? '\'' . addslashes( $this->m_strHeaderTime ) . '\'' : 'NULL';
	}

	public function setHeaderCycleNumber( $strHeaderCycleNumber ) {
		$this->set( 'm_strHeaderCycleNumber', CStrings::strTrimDef( $strHeaderCycleNumber, 6, NULL, true ) );
	}

	public function getHeaderCycleNumber() {
		return $this->m_strHeaderCycleNumber;
	}

	public function sqlHeaderCycleNumber() {
		return ( true == isset( $this->m_strHeaderCycleNumber ) ) ? '\'' . addslashes( $this->m_strHeaderCycleNumber ) . '\'' : 'NULL';
	}

	public function setHeaderClientId( $strHeaderClientId ) {
		$this->set( 'm_strHeaderClientId', CStrings::strTrimDef( $strHeaderClientId, 9, NULL, true ) );
	}

	public function getHeaderClientId() {
		return $this->m_strHeaderClientId;
	}

	public function sqlHeaderClientId() {
		return ( true == isset( $this->m_strHeaderClientId ) ) ? '\'' . addslashes( $this->m_strHeaderClientId ) . '\'' : 'NULL';
	}

	public function setHeaderClientName( $strHeaderClientName ) {
		$this->set( 'm_strHeaderClientName', CStrings::strTrimDef( $strHeaderClientName, 60, NULL, true ) );
	}

	public function getHeaderClientName() {
		return $this->m_strHeaderClientName;
	}

	public function sqlHeaderClientName() {
		return ( true == isset( $this->m_strHeaderClientName ) ) ? '\'' . addslashes( $this->m_strHeaderClientName ) . '\'' : 'NULL';
	}

	public function setHeaderFiller( $strHeaderFiller ) {
		$this->set( 'm_strHeaderFiller', CStrings::strTrimDef( $strHeaderFiller, 42, NULL, true ) );
	}

	public function getHeaderFiller() {
		return $this->m_strHeaderFiller;
	}

	public function sqlHeaderFiller() {
		return ( true == isset( $this->m_strHeaderFiller ) ) ? '\'' . addslashes( $this->m_strHeaderFiller ) . '\'' : 'NULL';
	}

	public function setTrailerRecordType( $strTrailerRecordType ) {
		$this->set( 'm_strTrailerRecordType', CStrings::strTrimDef( $strTrailerRecordType, 1, NULL, true ) );
	}

	public function getTrailerRecordType() {
		return $this->m_strTrailerRecordType;
	}

	public function sqlTrailerRecordType() {
		return ( true == isset( $this->m_strTrailerRecordType ) ) ? '\'' . addslashes( $this->m_strTrailerRecordType ) . '\'' : 'NULL';
	}

	public function setTrailerAddCount( $strTrailerAddCount ) {
		$this->set( 'm_strTrailerAddCount', CStrings::strTrimDef( $strTrailerAddCount, 6, NULL, true ) );
	}

	public function getTrailerAddCount() {
		return $this->m_strTrailerAddCount;
	}

	public function sqlTrailerAddCount() {
		return ( true == isset( $this->m_strTrailerAddCount ) ) ? '\'' . addslashes( $this->m_strTrailerAddCount ) . '\'' : 'NULL';
	}

	public function setTrailerStopCount( $strTrailerStopCount ) {
		$this->set( 'm_strTrailerStopCount', CStrings::strTrimDef( $strTrailerStopCount, 6, NULL, true ) );
	}

	public function getTrailerStopCount() {
		return $this->m_strTrailerStopCount;
	}

	public function sqlTrailerStopCount() {
		return ( true == isset( $this->m_strTrailerStopCount ) ) ? '\'' . addslashes( $this->m_strTrailerStopCount ) . '\'' : 'NULL';
	}

	public function setTrailerNumberOfRecords( $strTrailerNumberOfRecords ) {
		$this->set( 'm_strTrailerNumberOfRecords', CStrings::strTrimDef( $strTrailerNumberOfRecords, 6, NULL, true ) );
	}

	public function getTrailerNumberOfRecords() {
		return $this->m_strTrailerNumberOfRecords;
	}

	public function sqlTrailerNumberOfRecords() {
		return ( true == isset( $this->m_strTrailerNumberOfRecords ) ) ? '\'' . addslashes( $this->m_strTrailerNumberOfRecords ) . '\'' : 'NULL';
	}

	public function setTrailerTotalPurchaseAmount( $strTrailerTotalPurchaseAmount ) {
		$this->set( 'm_strTrailerTotalPurchaseAmount', CStrings::strTrimDef( $strTrailerTotalPurchaseAmount, 14, NULL, true ) );
	}

	public function getTrailerTotalPurchaseAmount() {
		return $this->m_strTrailerTotalPurchaseAmount;
	}

	public function sqlTrailerTotalPurchaseAmount() {
		return ( true == isset( $this->m_strTrailerTotalPurchaseAmount ) ) ? '\'' . addslashes( $this->m_strTrailerTotalPurchaseAmount ) . '\'' : 'NULL';
	}

	public function setTrailerFiller( $strTrailerFiller ) {
		$this->set( 'm_strTrailerFiller', CStrings::strTrimDef( $strTrailerFiller, 50, NULL, true ) );
	}

	public function getTrailerFiller() {
		return $this->m_strTrailerFiller;
	}

	public function sqlTrailerFiller() {
		return ( true == isset( $this->m_strTrailerFiller ) ) ? '\'' . addslashes( $this->m_strTrailerFiller ) . '\'' : 'NULL';
	}

	public function setSentOn( $strSentOn ) {
		$this->set( 'm_strSentOn', CStrings::strTrimDef( $strSentOn, -1, NULL, true ) );
	}

	public function getSentOn() {
		return $this->m_strSentOn;
	}

	public function sqlSentOn() {
		return ( true == isset( $this->m_strSentOn ) ) ? '\'' . $this->m_strSentOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, file_type_id, file_name, file_path, header_record_type, header_input_type, header_service_type, header_date, header_time, header_cycle_number, header_client_id, header_client_name, header_filler, trailer_record_type, trailer_add_count, trailer_stop_count, trailer_number_of_records, trailer_total_purchase_amount, trailer_filler, sent_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlFileTypeId() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlHeaderRecordType() . ', ' .
 						$this->sqlHeaderInputType() . ', ' .
 						$this->sqlHeaderServiceType() . ', ' .
 						$this->sqlHeaderDate() . ', ' .
 						$this->sqlHeaderTime() . ', ' .
 						$this->sqlHeaderCycleNumber() . ', ' .
 						$this->sqlHeaderClientId() . ', ' .
 						$this->sqlHeaderClientName() . ', ' .
 						$this->sqlHeaderFiller() . ', ' .
 						$this->sqlTrailerRecordType() . ', ' .
 						$this->sqlTrailerAddCount() . ', ' .
 						$this->sqlTrailerStopCount() . ', ' .
 						$this->sqlTrailerNumberOfRecords() . ', ' .
 						$this->sqlTrailerTotalPurchaseAmount() . ', ' .
 						$this->sqlTrailerFiller() . ', ' .
 						$this->sqlSentOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId() . ','; } elseif( true == array_key_exists( 'FileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_record_type = ' . $this->sqlHeaderRecordType() . ','; } elseif( true == array_key_exists( 'HeaderRecordType', $this->getChangedColumns() ) ) { $strSql .= ' header_record_type = ' . $this->sqlHeaderRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_input_type = ' . $this->sqlHeaderInputType() . ','; } elseif( true == array_key_exists( 'HeaderInputType', $this->getChangedColumns() ) ) { $strSql .= ' header_input_type = ' . $this->sqlHeaderInputType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_service_type = ' . $this->sqlHeaderServiceType() . ','; } elseif( true == array_key_exists( 'HeaderServiceType', $this->getChangedColumns() ) ) { $strSql .= ' header_service_type = ' . $this->sqlHeaderServiceType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_date = ' . $this->sqlHeaderDate() . ','; } elseif( true == array_key_exists( 'HeaderDate', $this->getChangedColumns() ) ) { $strSql .= ' header_date = ' . $this->sqlHeaderDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_time = ' . $this->sqlHeaderTime() . ','; } elseif( true == array_key_exists( 'HeaderTime', $this->getChangedColumns() ) ) { $strSql .= ' header_time = ' . $this->sqlHeaderTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_cycle_number = ' . $this->sqlHeaderCycleNumber() . ','; } elseif( true == array_key_exists( 'HeaderCycleNumber', $this->getChangedColumns() ) ) { $strSql .= ' header_cycle_number = ' . $this->sqlHeaderCycleNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_client_id = ' . $this->sqlHeaderClientId() . ','; } elseif( true == array_key_exists( 'HeaderClientId', $this->getChangedColumns() ) ) { $strSql .= ' header_client_id = ' . $this->sqlHeaderClientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_client_name = ' . $this->sqlHeaderClientName() . ','; } elseif( true == array_key_exists( 'HeaderClientName', $this->getChangedColumns() ) ) { $strSql .= ' header_client_name = ' . $this->sqlHeaderClientName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_filler = ' . $this->sqlHeaderFiller() . ','; } elseif( true == array_key_exists( 'HeaderFiller', $this->getChangedColumns() ) ) { $strSql .= ' header_filler = ' . $this->sqlHeaderFiller() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trailer_record_type = ' . $this->sqlTrailerRecordType() . ','; } elseif( true == array_key_exists( 'TrailerRecordType', $this->getChangedColumns() ) ) { $strSql .= ' trailer_record_type = ' . $this->sqlTrailerRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trailer_add_count = ' . $this->sqlTrailerAddCount() . ','; } elseif( true == array_key_exists( 'TrailerAddCount', $this->getChangedColumns() ) ) { $strSql .= ' trailer_add_count = ' . $this->sqlTrailerAddCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trailer_stop_count = ' . $this->sqlTrailerStopCount() . ','; } elseif( true == array_key_exists( 'TrailerStopCount', $this->getChangedColumns() ) ) { $strSql .= ' trailer_stop_count = ' . $this->sqlTrailerStopCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trailer_number_of_records = ' . $this->sqlTrailerNumberOfRecords() . ','; } elseif( true == array_key_exists( 'TrailerNumberOfRecords', $this->getChangedColumns() ) ) { $strSql .= ' trailer_number_of_records = ' . $this->sqlTrailerNumberOfRecords() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trailer_total_purchase_amount = ' . $this->sqlTrailerTotalPurchaseAmount() . ','; } elseif( true == array_key_exists( 'TrailerTotalPurchaseAmount', $this->getChangedColumns() ) ) { $strSql .= ' trailer_total_purchase_amount = ' . $this->sqlTrailerTotalPurchaseAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trailer_filler = ' . $this->sqlTrailerFiller() . ','; } elseif( true == array_key_exists( 'TrailerFiller', $this->getChangedColumns() ) ) { $strSql .= ' trailer_filler = ' . $this->sqlTrailerFiller() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; } elseif( true == array_key_exists( 'SentOn', $this->getChangedColumns() ) ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'file_type_id' => $this->getFileTypeId(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'header_record_type' => $this->getHeaderRecordType(),
			'header_input_type' => $this->getHeaderInputType(),
			'header_service_type' => $this->getHeaderServiceType(),
			'header_date' => $this->getHeaderDate(),
			'header_time' => $this->getHeaderTime(),
			'header_cycle_number' => $this->getHeaderCycleNumber(),
			'header_client_id' => $this->getHeaderClientId(),
			'header_client_name' => $this->getHeaderClientName(),
			'header_filler' => $this->getHeaderFiller(),
			'trailer_record_type' => $this->getTrailerRecordType(),
			'trailer_add_count' => $this->getTrailerAddCount(),
			'trailer_stop_count' => $this->getTrailerStopCount(),
			'trailer_number_of_records' => $this->getTrailerNumberOfRecords(),
			'trailer_total_purchase_amount' => $this->getTrailerTotalPurchaseAmount(),
			'trailer_filler' => $this->getTrailerFiller(),
			'sent_on' => $this->getSentOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>