<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CBai2Files
 * Do not add any new functions to this class.
 */

class CBaseBai2Files extends CEosPluralBase {

	/**
	 * @return CBai2File[]
	 */
	public static function fetchBai2Files( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBai2File::class, $objDatabase );
	}

	/**
	 * @return CBai2File
	 */
	public static function fetchBai2File( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBai2File::class, $objDatabase );
	}

	public static function fetchBai2FileCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'bai2_files', $objDatabase );
	}

	public static function fetchBai2FileById( $intId, $objDatabase ) {
		return self::fetchBai2File( sprintf( 'SELECT * FROM bai2_files WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchBai2FilesByProcessingBankId( $intProcessingBankId, $objDatabase ) {
		return self::fetchBai2Files( sprintf( 'SELECT * FROM bai2_files WHERE processing_bank_id = %d', ( int ) $intProcessingBankId ), $objDatabase );
	}

}
?>