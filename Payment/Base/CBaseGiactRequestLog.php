<?php

class CBaseGiactRequestLog extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.giact_request_logs';

	protected $m_intId;
	protected $m_intProductId;
	protected $m_intPsProductOptionId;
	protected $m_strGiactReferenceId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strAccountResponseCode;
	protected $m_strCustomerResponseCode;
	protected $m_strVerificationResult;
	protected $m_strRawRequest;
	protected $m_strRawResponse;
	protected $m_intLockSequence;
	protected $m_boolIsFeeRequired;
	protected $m_strFeePostedOn;
	protected $m_strRequestTimestamp;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsFeeRequired = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['product_id'] ) && $boolDirectSet ) $this->set( 'm_intProductId', trim( $arrValues['product_id'] ) ); elseif( isset( $arrValues['product_id'] ) ) $this->setProductId( $arrValues['product_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductOptionId', trim( $arrValues['ps_product_option_id'] ) ); elseif( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['giact_reference_id'] ) && $boolDirectSet ) $this->set( 'm_strGiactReferenceId', trim( stripcslashes( $arrValues['giact_reference_id'] ) ) ); elseif( isset( $arrValues['giact_reference_id'] ) ) $this->setGiactReferenceId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['giact_reference_id'] ) : $arrValues['giact_reference_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['account_response_code'] ) && $boolDirectSet ) $this->set( 'm_strAccountResponseCode', trim( stripcslashes( $arrValues['account_response_code'] ) ) ); elseif( isset( $arrValues['account_response_code'] ) ) $this->setAccountResponseCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_response_code'] ) : $arrValues['account_response_code'] );
		if( isset( $arrValues['customer_response_code'] ) && $boolDirectSet ) $this->set( 'm_strCustomerResponseCode', trim( stripcslashes( $arrValues['customer_response_code'] ) ) ); elseif( isset( $arrValues['customer_response_code'] ) ) $this->setCustomerResponseCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_response_code'] ) : $arrValues['customer_response_code'] );
		if( isset( $arrValues['verification_result'] ) && $boolDirectSet ) $this->set( 'm_strVerificationResult', trim( stripcslashes( $arrValues['verification_result'] ) ) ); elseif( isset( $arrValues['verification_result'] ) ) $this->setVerificationResult( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['verification_result'] ) : $arrValues['verification_result'] );
		if( isset( $arrValues['raw_request'] ) && $boolDirectSet ) $this->set( 'm_strRawRequest', trim( stripcslashes( $arrValues['raw_request'] ) ) ); elseif( isset( $arrValues['raw_request'] ) ) $this->setRawRequest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['raw_request'] ) : $arrValues['raw_request'] );
		if( isset( $arrValues['raw_response'] ) && $boolDirectSet ) $this->set( 'm_strRawResponse', trim( stripcslashes( $arrValues['raw_response'] ) ) ); elseif( isset( $arrValues['raw_response'] ) ) $this->setRawResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['raw_response'] ) : $arrValues['raw_response'] );
		if( isset( $arrValues['lock_sequence'] ) && $boolDirectSet ) $this->set( 'm_intLockSequence', trim( $arrValues['lock_sequence'] ) ); elseif( isset( $arrValues['lock_sequence'] ) ) $this->setLockSequence( $arrValues['lock_sequence'] );
		if( isset( $arrValues['is_fee_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsFeeRequired', trim( stripcslashes( $arrValues['is_fee_required'] ) ) ); elseif( isset( $arrValues['is_fee_required'] ) ) $this->setIsFeeRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_fee_required'] ) : $arrValues['is_fee_required'] );
		if( isset( $arrValues['fee_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strFeePostedOn', trim( $arrValues['fee_posted_on'] ) ); elseif( isset( $arrValues['fee_posted_on'] ) ) $this->setFeePostedOn( $arrValues['fee_posted_on'] );
		if( isset( $arrValues['request_timestamp'] ) && $boolDirectSet ) $this->set( 'm_strRequestTimestamp', trim( $arrValues['request_timestamp'] ) ); elseif( isset( $arrValues['request_timestamp'] ) ) $this->setRequestTimestamp( $arrValues['request_timestamp'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setProductId( $intProductId ) {
		$this->set( 'm_intProductId', CStrings::strToIntDef( $intProductId, NULL, false ) );
	}

	public function getProductId() {
		return $this->m_intProductId;
	}

	public function sqlProductId() {
		return ( true == isset( $this->m_intProductId ) ) ? ( string ) $this->m_intProductId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->set( 'm_intPsProductOptionId', CStrings::strToIntDef( $intPsProductOptionId, NULL, false ) );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setGiactReferenceId( $strGiactReferenceId ) {
		$this->set( 'm_strGiactReferenceId', CStrings::strTrimDef( $strGiactReferenceId, 25, NULL, true ) );
	}

	public function getGiactReferenceId() {
		return $this->m_strGiactReferenceId;
	}

	public function sqlGiactReferenceId() {
		return ( true == isset( $this->m_strGiactReferenceId ) ) ? '\'' . addslashes( $this->m_strGiactReferenceId ) . '\'' : 'NULL';
	}

	public function setAccountResponseCode( $strAccountResponseCode ) {
		$this->set( 'm_strAccountResponseCode', CStrings::strTrimDef( $strAccountResponseCode, 10, NULL, true ) );
	}

	public function getAccountResponseCode() {
		return $this->m_strAccountResponseCode;
	}

	public function sqlAccountResponseCode() {
		return ( true == isset( $this->m_strAccountResponseCode ) ) ? '\'' . addslashes( $this->m_strAccountResponseCode ) . '\'' : 'NULL';
	}

	public function setCustomerResponseCode( $strCustomerResponseCode ) {
		$this->set( 'm_strCustomerResponseCode', CStrings::strTrimDef( $strCustomerResponseCode, 10, NULL, true ) );
	}

	public function getCustomerResponseCode() {
		return $this->m_strCustomerResponseCode;
	}

	public function sqlCustomerResponseCode() {
		return ( true == isset( $this->m_strCustomerResponseCode ) ) ? '\'' . addslashes( $this->m_strCustomerResponseCode ) . '\'' : 'NULL';
	}

	public function setVerificationResult( $strVerificationResult ) {
		$this->set( 'm_strVerificationResult', CStrings::strTrimDef( $strVerificationResult, 25, NULL, true ) );
	}

	public function getVerificationResult() {
		return $this->m_strVerificationResult;
	}

	public function sqlVerificationResult() {
		return ( true == isset( $this->m_strVerificationResult ) ) ? '\'' . addslashes( $this->m_strVerificationResult ) . '\'' : 'NULL';
	}

	public function setRawRequest( $strRawRequest ) {
		$this->set( 'm_strRawRequest', CStrings::strTrimDef( $strRawRequest, -1, NULL, true ) );
	}

	public function getRawRequest() {
		return $this->m_strRawRequest;
	}

	public function sqlRawRequest() {
		return ( true == isset( $this->m_strRawRequest ) ) ? '\'' . addslashes( $this->m_strRawRequest ) . '\'' : 'NULL';
	}

	public function setRawResponse( $strRawResponse ) {
		$this->set( 'm_strRawResponse', CStrings::strTrimDef( $strRawResponse, -1, NULL, true ) );
	}

	public function getRawResponse() {
		return $this->m_strRawResponse;
	}

	public function sqlRawResponse() {
		return ( true == isset( $this->m_strRawResponse ) ) ? '\'' . addslashes( $this->m_strRawResponse ) . '\'' : 'NULL';
	}

	public function setLockSequence( $intLockSequence ) {
		$this->set( 'm_intLockSequence', CStrings::strToIntDef( $intLockSequence, NULL, false ) );
	}

	public function getLockSequence() {
		return $this->m_intLockSequence;
	}

	public function sqlLockSequence() {
		return ( true == isset( $this->m_intLockSequence ) ) ? ( string ) $this->m_intLockSequence : 'NULL';
	}

	public function setIsFeeRequired( $boolIsFeeRequired ) {
		$this->set( 'm_boolIsFeeRequired', CStrings::strToBool( $boolIsFeeRequired ) );
	}

	public function getIsFeeRequired() {
		return $this->m_boolIsFeeRequired;
	}

	public function sqlIsFeeRequired() {
		return ( true == isset( $this->m_boolIsFeeRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFeeRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setFeePostedOn( $strFeePostedOn ) {
		$this->set( 'm_strFeePostedOn', CStrings::strTrimDef( $strFeePostedOn, -1, NULL, true ) );
	}

	public function getFeePostedOn() {
		return $this->m_strFeePostedOn;
	}

	public function sqlFeePostedOn() {
		return ( true == isset( $this->m_strFeePostedOn ) ) ? '\'' . $this->m_strFeePostedOn . '\'' : 'NULL';
	}

	public function setRequestTimestamp( $strRequestTimestamp ) {
		$this->set( 'm_strRequestTimestamp', CStrings::strTrimDef( $strRequestTimestamp, -1, NULL, true ) );
	}

	public function getRequestTimestamp() {
		return $this->m_strRequestTimestamp;
	}

	public function sqlRequestTimestamp() {
		return ( true == isset( $this->m_strRequestTimestamp ) ) ? '\'' . $this->m_strRequestTimestamp . '\'' : 'NOW()';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'product_id' => $this->getProductId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'giact_reference_id' => $this->getGiactReferenceId(),
			'details' => $this->getDetails(),
			'account_response_code' => $this->getAccountResponseCode(),
			'customer_response_code' => $this->getCustomerResponseCode(),
			'verification_result' => $this->getVerificationResult(),
			'raw_request' => $this->getRawRequest(),
			'raw_response' => $this->getRawResponse(),
			'lock_sequence' => $this->getLockSequence(),
			'is_fee_required' => $this->getIsFeeRequired(),
			'fee_posted_on' => $this->getFeePostedOn(),
			'request_timestamp' => $this->getRequestTimestamp()
		);
	}

}
?>