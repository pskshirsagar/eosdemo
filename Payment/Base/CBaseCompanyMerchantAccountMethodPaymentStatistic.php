<?php

class CBaseCompanyMerchantAccountMethodPaymentStatistic extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.company_merchant_account_method_payment_statistics';

	protected $m_intId;
	protected $m_intMerchantAccountMethodId;
	protected $m_intNumUnitsTotal;
	protected $m_intNumUnitsRpayEnabled;
	protected $m_intNumUnitsWithPayments;
	protected $m_intNumLeasesTotal;
	protected $m_intNumLeasesRpayEnabled;
	protected $m_intNumLeasesWithPayments;
	protected $m_strMonthYear;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['merchant_account_method_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantAccountMethodId', trim( $arrValues['merchant_account_method_id'] ) ); elseif( isset( $arrValues['merchant_account_method_id'] ) ) $this->setMerchantAccountMethodId( $arrValues['merchant_account_method_id'] );
		if( isset( $arrValues['num_units_total'] ) && $boolDirectSet ) $this->set( 'm_intNumUnitsTotal', trim( $arrValues['num_units_total'] ) ); elseif( isset( $arrValues['num_units_total'] ) ) $this->setNumUnitsTotal( $arrValues['num_units_total'] );
		if( isset( $arrValues['num_units_rpay_enabled'] ) && $boolDirectSet ) $this->set( 'm_intNumUnitsRpayEnabled', trim( $arrValues['num_units_rpay_enabled'] ) ); elseif( isset( $arrValues['num_units_rpay_enabled'] ) ) $this->setNumUnitsRpayEnabled( $arrValues['num_units_rpay_enabled'] );
		if( isset( $arrValues['num_units_with_payments'] ) && $boolDirectSet ) $this->set( 'm_intNumUnitsWithPayments', trim( $arrValues['num_units_with_payments'] ) ); elseif( isset( $arrValues['num_units_with_payments'] ) ) $this->setNumUnitsWithPayments( $arrValues['num_units_with_payments'] );
		if( isset( $arrValues['num_leases_total'] ) && $boolDirectSet ) $this->set( 'm_intNumLeasesTotal', trim( $arrValues['num_leases_total'] ) ); elseif( isset( $arrValues['num_leases_total'] ) ) $this->setNumLeasesTotal( $arrValues['num_leases_total'] );
		if( isset( $arrValues['num_leases_rpay_enabled'] ) && $boolDirectSet ) $this->set( 'm_intNumLeasesRpayEnabled', trim( $arrValues['num_leases_rpay_enabled'] ) ); elseif( isset( $arrValues['num_leases_rpay_enabled'] ) ) $this->setNumLeasesRpayEnabled( $arrValues['num_leases_rpay_enabled'] );
		if( isset( $arrValues['num_leases_with_payments'] ) && $boolDirectSet ) $this->set( 'm_intNumLeasesWithPayments', trim( $arrValues['num_leases_with_payments'] ) ); elseif( isset( $arrValues['num_leases_with_payments'] ) ) $this->setNumLeasesWithPayments( $arrValues['num_leases_with_payments'] );
		if( isset( $arrValues['month_year'] ) && $boolDirectSet ) $this->set( 'm_strMonthYear', trim( $arrValues['month_year'] ) ); elseif( isset( $arrValues['month_year'] ) ) $this->setMonthYear( $arrValues['month_year'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMerchantAccountMethodId( $intMerchantAccountMethodId ) {
		$this->set( 'm_intMerchantAccountMethodId', CStrings::strToIntDef( $intMerchantAccountMethodId, NULL, false ) );
	}

	public function getMerchantAccountMethodId() {
		return $this->m_intMerchantAccountMethodId;
	}

	public function sqlMerchantAccountMethodId() {
		return ( true == isset( $this->m_intMerchantAccountMethodId ) ) ? ( string ) $this->m_intMerchantAccountMethodId : 'NULL';
	}

	public function setNumUnitsTotal( $intNumUnitsTotal ) {
		$this->set( 'm_intNumUnitsTotal', CStrings::strToIntDef( $intNumUnitsTotal, NULL, false ) );
	}

	public function getNumUnitsTotal() {
		return $this->m_intNumUnitsTotal;
	}

	public function sqlNumUnitsTotal() {
		return ( true == isset( $this->m_intNumUnitsTotal ) ) ? ( string ) $this->m_intNumUnitsTotal : 'NULL';
	}

	public function setNumUnitsRpayEnabled( $intNumUnitsRpayEnabled ) {
		$this->set( 'm_intNumUnitsRpayEnabled', CStrings::strToIntDef( $intNumUnitsRpayEnabled, NULL, false ) );
	}

	public function getNumUnitsRpayEnabled() {
		return $this->m_intNumUnitsRpayEnabled;
	}

	public function sqlNumUnitsRpayEnabled() {
		return ( true == isset( $this->m_intNumUnitsRpayEnabled ) ) ? ( string ) $this->m_intNumUnitsRpayEnabled : 'NULL';
	}

	public function setNumUnitsWithPayments( $intNumUnitsWithPayments ) {
		$this->set( 'm_intNumUnitsWithPayments', CStrings::strToIntDef( $intNumUnitsWithPayments, NULL, false ) );
	}

	public function getNumUnitsWithPayments() {
		return $this->m_intNumUnitsWithPayments;
	}

	public function sqlNumUnitsWithPayments() {
		return ( true == isset( $this->m_intNumUnitsWithPayments ) ) ? ( string ) $this->m_intNumUnitsWithPayments : 'NULL';
	}

	public function setNumLeasesTotal( $intNumLeasesTotal ) {
		$this->set( 'm_intNumLeasesTotal', CStrings::strToIntDef( $intNumLeasesTotal, NULL, false ) );
	}

	public function getNumLeasesTotal() {
		return $this->m_intNumLeasesTotal;
	}

	public function sqlNumLeasesTotal() {
		return ( true == isset( $this->m_intNumLeasesTotal ) ) ? ( string ) $this->m_intNumLeasesTotal : 'NULL';
	}

	public function setNumLeasesRpayEnabled( $intNumLeasesRpayEnabled ) {
		$this->set( 'm_intNumLeasesRpayEnabled', CStrings::strToIntDef( $intNumLeasesRpayEnabled, NULL, false ) );
	}

	public function getNumLeasesRpayEnabled() {
		return $this->m_intNumLeasesRpayEnabled;
	}

	public function sqlNumLeasesRpayEnabled() {
		return ( true == isset( $this->m_intNumLeasesRpayEnabled ) ) ? ( string ) $this->m_intNumLeasesRpayEnabled : 'NULL';
	}

	public function setNumLeasesWithPayments( $intNumLeasesWithPayments ) {
		$this->set( 'm_intNumLeasesWithPayments', CStrings::strToIntDef( $intNumLeasesWithPayments, NULL, false ) );
	}

	public function getNumLeasesWithPayments() {
		return $this->m_intNumLeasesWithPayments;
	}

	public function sqlNumLeasesWithPayments() {
		return ( true == isset( $this->m_intNumLeasesWithPayments ) ) ? ( string ) $this->m_intNumLeasesWithPayments : 'NULL';
	}

	public function setMonthYear( $strMonthYear ) {
		$this->set( 'm_strMonthYear', CStrings::strTrimDef( $strMonthYear, -1, NULL, true ) );
	}

	public function getMonthYear() {
		return $this->m_strMonthYear;
	}

	public function sqlMonthYear() {
		return ( true == isset( $this->m_strMonthYear ) ) ? '\'' . $this->m_strMonthYear . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, merchant_account_method_id, num_units_total, num_units_rpay_enabled, num_units_with_payments, num_leases_total, num_leases_rpay_enabled, num_leases_with_payments, month_year, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlMerchantAccountMethodId() . ', ' .
						$this->sqlNumUnitsTotal() . ', ' .
						$this->sqlNumUnitsRpayEnabled() . ', ' .
						$this->sqlNumUnitsWithPayments() . ', ' .
						$this->sqlNumLeasesTotal() . ', ' .
						$this->sqlNumLeasesRpayEnabled() . ', ' .
						$this->sqlNumLeasesWithPayments() . ', ' .
						$this->sqlMonthYear() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_account_method_id = ' . $this->sqlMerchantAccountMethodId(). ',' ; } elseif( true == array_key_exists( 'MerchantAccountMethodId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_account_method_id = ' . $this->sqlMerchantAccountMethodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' num_units_total = ' . $this->sqlNumUnitsTotal(). ',' ; } elseif( true == array_key_exists( 'NumUnitsTotal', $this->getChangedColumns() ) ) { $strSql .= ' num_units_total = ' . $this->sqlNumUnitsTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' num_units_rpay_enabled = ' . $this->sqlNumUnitsRpayEnabled(). ',' ; } elseif( true == array_key_exists( 'NumUnitsRpayEnabled', $this->getChangedColumns() ) ) { $strSql .= ' num_units_rpay_enabled = ' . $this->sqlNumUnitsRpayEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' num_units_with_payments = ' . $this->sqlNumUnitsWithPayments(). ',' ; } elseif( true == array_key_exists( 'NumUnitsWithPayments', $this->getChangedColumns() ) ) { $strSql .= ' num_units_with_payments = ' . $this->sqlNumUnitsWithPayments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' num_leases_total = ' . $this->sqlNumLeasesTotal(). ',' ; } elseif( true == array_key_exists( 'NumLeasesTotal', $this->getChangedColumns() ) ) { $strSql .= ' num_leases_total = ' . $this->sqlNumLeasesTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' num_leases_rpay_enabled = ' . $this->sqlNumLeasesRpayEnabled(). ',' ; } elseif( true == array_key_exists( 'NumLeasesRpayEnabled', $this->getChangedColumns() ) ) { $strSql .= ' num_leases_rpay_enabled = ' . $this->sqlNumLeasesRpayEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' num_leases_with_payments = ' . $this->sqlNumLeasesWithPayments(). ',' ; } elseif( true == array_key_exists( 'NumLeasesWithPayments', $this->getChangedColumns() ) ) { $strSql .= ' num_leases_with_payments = ' . $this->sqlNumLeasesWithPayments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month_year = ' . $this->sqlMonthYear(). ',' ; } elseif( true == array_key_exists( 'MonthYear', $this->getChangedColumns() ) ) { $strSql .= ' month_year = ' . $this->sqlMonthYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'merchant_account_method_id' => $this->getMerchantAccountMethodId(),
			'num_units_total' => $this->getNumUnitsTotal(),
			'num_units_rpay_enabled' => $this->getNumUnitsRpayEnabled(),
			'num_units_with_payments' => $this->getNumUnitsWithPayments(),
			'num_leases_total' => $this->getNumLeasesTotal(),
			'num_leases_rpay_enabled' => $this->getNumLeasesRpayEnabled(),
			'num_leases_with_payments' => $this->getNumLeasesWithPayments(),
			'month_year' => $this->getMonthYear(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>