<?php

class CBaseBinFlag extends CEosSingularBase {

	const TABLE_NAME = 'public.bin_flags';

	protected $m_intId;
	protected $m_boolIsCommercialCard;
	protected $m_boolIsInternationalCard;
	protected $m_boolIsCreditCard;
	protected $m_boolIsPrepaidCard;
	protected $m_boolIsGiftCard;
	protected $m_boolIsCheckCard;
	protected $m_boolIsDebitCard;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['is_commercial_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsCommercialCard', trim( stripcslashes( $arrValues['is_commercial_card'] ) ) ); elseif( isset( $arrValues['is_commercial_card'] ) ) $this->setIsCommercialCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_commercial_card'] ) : $arrValues['is_commercial_card'] );
		if( isset( $arrValues['is_international_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsInternationalCard', trim( stripcslashes( $arrValues['is_international_card'] ) ) ); elseif( isset( $arrValues['is_international_card'] ) ) $this->setIsInternationalCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_international_card'] ) : $arrValues['is_international_card'] );
		if( isset( $arrValues['is_credit_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsCreditCard', trim( stripcslashes( $arrValues['is_credit_card'] ) ) ); elseif( isset( $arrValues['is_credit_card'] ) ) $this->setIsCreditCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_credit_card'] ) : $arrValues['is_credit_card'] );
		if( isset( $arrValues['is_prepaid_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsPrepaidCard', trim( stripcslashes( $arrValues['is_prepaid_card'] ) ) ); elseif( isset( $arrValues['is_prepaid_card'] ) ) $this->setIsPrepaidCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_prepaid_card'] ) : $arrValues['is_prepaid_card'] );
		if( isset( $arrValues['is_gift_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsGiftCard', trim( stripcslashes( $arrValues['is_gift_card'] ) ) ); elseif( isset( $arrValues['is_gift_card'] ) ) $this->setIsGiftCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_gift_card'] ) : $arrValues['is_gift_card'] );
		if( isset( $arrValues['is_check_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsCheckCard', trim( stripcslashes( $arrValues['is_check_card'] ) ) ); elseif( isset( $arrValues['is_check_card'] ) ) $this->setIsCheckCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_check_card'] ) : $arrValues['is_check_card'] );
		if( isset( $arrValues['is_debit_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsDebitCard', trim( stripcslashes( $arrValues['is_debit_card'] ) ) ); elseif( isset( $arrValues['is_debit_card'] ) ) $this->setIsDebitCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_debit_card'] ) : $arrValues['is_debit_card'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setIsCommercialCard( $boolIsCommercialCard ) {
		$this->set( 'm_boolIsCommercialCard', CStrings::strToBool( $boolIsCommercialCard ) );
	}

	public function getIsCommercialCard() {
		return $this->m_boolIsCommercialCard;
	}

	public function sqlIsCommercialCard() {
		return ( true == isset( $this->m_boolIsCommercialCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCommercialCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsInternationalCard( $boolIsInternationalCard ) {
		$this->set( 'm_boolIsInternationalCard', CStrings::strToBool( $boolIsInternationalCard ) );
	}

	public function getIsInternationalCard() {
		return $this->m_boolIsInternationalCard;
	}

	public function sqlIsInternationalCard() {
		return ( true == isset( $this->m_boolIsInternationalCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInternationalCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCreditCard( $boolIsCreditCard ) {
		$this->set( 'm_boolIsCreditCard', CStrings::strToBool( $boolIsCreditCard ) );
	}

	public function getIsCreditCard() {
		return $this->m_boolIsCreditCard;
	}

	public function sqlIsCreditCard() {
		return ( true == isset( $this->m_boolIsCreditCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCreditCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPrepaidCard( $boolIsPrepaidCard ) {
		$this->set( 'm_boolIsPrepaidCard', CStrings::strToBool( $boolIsPrepaidCard ) );
	}

	public function getIsPrepaidCard() {
		return $this->m_boolIsPrepaidCard;
	}

	public function sqlIsPrepaidCard() {
		return ( true == isset( $this->m_boolIsPrepaidCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPrepaidCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsGiftCard( $boolIsGiftCard ) {
		$this->set( 'm_boolIsGiftCard', CStrings::strToBool( $boolIsGiftCard ) );
	}

	public function getIsGiftCard() {
		return $this->m_boolIsGiftCard;
	}

	public function sqlIsGiftCard() {
		return ( true == isset( $this->m_boolIsGiftCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsGiftCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCheckCard( $boolIsCheckCard ) {
		$this->set( 'm_boolIsCheckCard', CStrings::strToBool( $boolIsCheckCard ) );
	}

	public function getIsCheckCard() {
		return $this->m_boolIsCheckCard;
	}

	public function sqlIsCheckCard() {
		return ( true == isset( $this->m_boolIsCheckCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCheckCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDebitCard( $boolIsDebitCard ) {
		$this->set( 'm_boolIsDebitCard', CStrings::strToBool( $boolIsDebitCard ) );
	}

	public function getIsDebitCard() {
		return $this->m_boolIsDebitCard;
	}

	public function sqlIsDebitCard() {
		return ( true == isset( $this->m_boolIsDebitCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDebitCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'is_commercial_card' => $this->getIsCommercialCard(),
			'is_international_card' => $this->getIsInternationalCard(),
			'is_credit_card' => $this->getIsCreditCard(),
			'is_prepaid_card' => $this->getIsPrepaidCard(),
			'is_gift_card' => $this->getIsGiftCard(),
			'is_check_card' => $this->getIsCheckCard(),
			'is_debit_card' => $this->getIsDebitCard()
		);
	}

}
?>