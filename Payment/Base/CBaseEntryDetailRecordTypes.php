<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CEntryDetailRecordTypes
 * Do not add any new functions to this class.
 */

class CBaseEntryDetailRecordTypes extends CEosPluralBase {

	/**
	 * @return CEntryDetailRecordType[]
	 */
	public static function fetchEntryDetailRecordTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEntryDetailRecordType::class, $objDatabase );
	}

	/**
	 * @return CEntryDetailRecordType
	 */
	public static function fetchEntryDetailRecordType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEntryDetailRecordType::class, $objDatabase );
	}

	public static function fetchEntryDetailRecordTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'entry_detail_record_types', $objDatabase );
	}

	public static function fetchEntryDetailRecordTypeById( $intId, $objDatabase ) {
		return self::fetchEntryDetailRecordType( sprintf( 'SELECT * FROM entry_detail_record_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>