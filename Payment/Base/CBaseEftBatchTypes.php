<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CEftBatchTypes
 * Do not add any new functions to this class.
 */

class CBaseEftBatchTypes extends CEosPluralBase {

	/**
	 * @return CEftBatchType[]
	 */
	public static function fetchEftBatchTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEftBatchType::class, $objDatabase );
	}

	/**
	 * @return CEftBatchType
	 */
	public static function fetchEftBatchType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEftBatchType::class, $objDatabase );
	}

	public static function fetchEftBatchTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'eft_batch_types', $objDatabase );
	}

	public static function fetchEftBatchTypeById( $intId, $objDatabase ) {
		return self::fetchEftBatchType( sprintf( 'SELECT * FROM eft_batch_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>