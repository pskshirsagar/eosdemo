<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CCreditCardBinNumbers
 * Do not add any new functions to this class.
 */

class CBaseCreditCardBinNumbers extends CEosPluralBase {

	/**
	 * @return CCreditCardBinNumber[]
	 */
	public static function fetchCreditCardBinNumbers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCreditCardBinNumber::class, $objDatabase );
	}

	/**
	 * @return CCreditCardBinNumber
	 */
	public static function fetchCreditCardBinNumber( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCreditCardBinNumber::class, $objDatabase );
	}

	public static function fetchCreditCardBinNumberCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'credit_card_bin_numbers', $objDatabase );
	}

	public static function fetchCreditCardBinNumberById( $intId, $objDatabase ) {
		return self::fetchCreditCardBinNumber( sprintf( 'SELECT * FROM credit_card_bin_numbers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCreditCardBinNumbersByTableId( $strTableId, $objDatabase ) {
		return self::fetchCreditCardBinNumbers( sprintf( 'SELECT * FROM credit_card_bin_numbers WHERE table_id = \'%s\'', $strTableId ), $objDatabase );
	}

}
?>