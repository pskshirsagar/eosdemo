<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937ReturnItems
 * Do not add any new functions to this class.
 */

class CBaseX937ReturnItems extends CEosPluralBase {

	/**
	 * @return CX937ReturnItem[]
	 */
	public static function fetchX937ReturnItems( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CX937ReturnItem::class, $objDatabase );
	}

	/**
	 * @return CX937ReturnItem
	 */
	public static function fetchX937ReturnItem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CX937ReturnItem::class, $objDatabase );
	}

	public static function fetchX937ReturnItemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'x937_return_items', $objDatabase );
	}

	public static function fetchX937ReturnItemById( $intId, $objDatabase ) {
		return self::fetchX937ReturnItem( sprintf( 'SELECT * FROM x937_return_items WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchX937ReturnItemsByCid( $intCid, $objDatabase ) {
		return self::fetchX937ReturnItems( sprintf( 'SELECT * FROM x937_return_items WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchX937ReturnItemsByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchX937ReturnItems( sprintf( 'SELECT * FROM x937_return_items WHERE ar_payment_id = %d', $intArPaymentId ), $objDatabase );
	}

	public static function fetchX937ReturnItemsByCompanyPaymentId( $intCompanyPaymentId, $objDatabase ) {
		return self::fetchX937ReturnItems( sprintf( 'SELECT * FROM x937_return_items WHERE company_payment_id = %d', $intCompanyPaymentId ), $objDatabase );
	}

	public static function fetchX937ReturnItemsByX937ReturnFileId( $intX937ReturnFileId, $objDatabase ) {
		return self::fetchX937ReturnItems( sprintf( 'SELECT * FROM x937_return_items WHERE x937_return_file_id = %d', $intX937ReturnFileId ), $objDatabase );
	}

	public static function fetchX937ReturnItemsByReturnTypeId( $intReturnTypeId, $objDatabase ) {
		return self::fetchX937ReturnItems( sprintf( 'SELECT * FROM x937_return_items WHERE return_type_id = %d', $intReturnTypeId ), $objDatabase );
	}

}
?>