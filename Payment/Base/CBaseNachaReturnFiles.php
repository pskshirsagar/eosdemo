<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaReturnFiles
 * Do not add any new functions to this class.
 */

class CBaseNachaReturnFiles extends CEosPluralBase {

	/**
	 * @return CNachaReturnFile[]
	 */
	public static function fetchNachaReturnFiles( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CNachaReturnFile::class, $objDatabase );
	}

	/**
	 * @return CNachaReturnFile
	 */
	public static function fetchNachaReturnFile( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CNachaReturnFile::class, $objDatabase );
	}

	public static function fetchNachaReturnFileCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'nacha_return_files', $objDatabase );
	}

	public static function fetchNachaReturnFileById( $intId, $objDatabase ) {
		return self::fetchNachaReturnFile( sprintf( 'SELECT * FROM nacha_return_files WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchNachaReturnFilesByProcessingBankId( $intProcessingBankId, $objDatabase ) {
		return self::fetchNachaReturnFiles( sprintf( 'SELECT * FROM nacha_return_files WHERE processing_bank_id = %d', ( int ) $intProcessingBankId ), $objDatabase );
	}

	public static function fetchNachaReturnFilesByProcessingBankAccountId( $intProcessingBankAccountId, $objDatabase ) {
		return self::fetchNachaReturnFiles( sprintf( 'SELECT * FROM nacha_return_files WHERE processing_bank_account_id = %d', ( int ) $intProcessingBankAccountId ), $objDatabase );
	}

	public static function fetchNachaReturnFilesByNachaFileTypeId( $intNachaFileTypeId, $objDatabase ) {
		return self::fetchNachaReturnFiles( sprintf( 'SELECT * FROM nacha_return_files WHERE nacha_file_type_id = %d', ( int ) $intNachaFileTypeId ), $objDatabase );
	}

	public static function fetchNachaReturnFilesByNachaFileStatusTypeId( $intNachaFileStatusTypeId, $objDatabase ) {
		return self::fetchNachaReturnFiles( sprintf( 'SELECT * FROM nacha_return_files WHERE nacha_file_status_type_id = %d', ( int ) $intNachaFileStatusTypeId ), $objDatabase );
	}

	public static function fetchNachaReturnFilesByMerchantGatewayId( $intMerchantGatewayId, $objDatabase ) {
		return self::fetchNachaReturnFiles( sprintf( 'SELECT * FROM nacha_return_files WHERE merchant_gateway_id = %d', ( int ) $intMerchantGatewayId ), $objDatabase );
	}

}
?>