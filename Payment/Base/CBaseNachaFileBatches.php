<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaFileBatches
 * Do not add any new functions to this class.
 */

class CBaseNachaFileBatches extends CEosPluralBase {

	/**
	 * @return CNachaFileBatch[]
	 */
	public static function fetchNachaFileBatches( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CNachaFileBatch::class, $objDatabase );
	}

	/**
	 * @return CNachaFileBatch
	 */
	public static function fetchNachaFileBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CNachaFileBatch::class, $objDatabase );
	}

	public static function fetchNachaFileBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'nacha_file_batches', $objDatabase );
	}

	public static function fetchNachaFileBatchById( $intId, $objDatabase ) {
		return self::fetchNachaFileBatch( sprintf( 'SELECT * FROM nacha_file_batches WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchNachaFileBatchesByNachaFileId( $intNachaFileId, $objDatabase ) {
		return self::fetchNachaFileBatches( sprintf( 'SELECT * FROM nacha_file_batches WHERE nacha_file_id = %d', ( int ) $intNachaFileId ), $objDatabase );
	}

	public static function fetchNachaFileBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchNachaFileBatches( sprintf( 'SELECT * FROM nacha_file_batches WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>