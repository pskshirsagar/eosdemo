<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaFileTypes
 * Do not add any new functions to this class.
 */

class CBaseNachaFileTypes extends CEosPluralBase {

	/**
	 * @return CNachaFileType[]
	 */
	public static function fetchNachaFileTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CNachaFileType::class, $objDatabase );
	}

	/**
	 * @return CNachaFileType
	 */
	public static function fetchNachaFileType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CNachaFileType::class, $objDatabase );
	}

	public static function fetchNachaFileTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'nacha_file_types', $objDatabase );
	}

	public static function fetchNachaFileTypeById( $intId, $objDatabase ) {
		return self::fetchNachaFileType( sprintf( 'SELECT * FROM nacha_file_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>