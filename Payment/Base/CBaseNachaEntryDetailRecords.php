<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaEntryDetailRecords
 * Do not add any new functions to this class.
 */

class CBaseNachaEntryDetailRecords extends CEosPluralBase {

	/**
	 * @return CNachaEntryDetailRecord[]
	 */
	public static function fetchNachaEntryDetailRecords( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CNachaEntryDetailRecord::class, $objDatabase );
	}

	/**
	 * @return CNachaEntryDetailRecord
	 */
	public static function fetchNachaEntryDetailRecord( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CNachaEntryDetailRecord::class, $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'nacha_entry_detail_records', $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordById( $intId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecord( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByNachaFileId( $intNachaFileId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE nacha_file_id = %d', ( int ) $intNachaFileId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByNachaFileBatchId( $intNachaFileBatchId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE nacha_file_batch_id = %d', ( int ) $intNachaFileBatchId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByClearingBatchId( $intClearingBatchId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE clearing_batch_id = %d', ( int ) $intClearingBatchId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByEntryDetailRecordTypeId( $intEntryDetailRecordTypeId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE entry_detail_record_type_id = %d', ( int ) $intEntryDetailRecordTypeId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByProcessingBankAccountId( $intProcessingBankAccountId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE processing_bank_account_id = %d', ( int ) $intProcessingBankAccountId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByReturnTypeId( $intReturnTypeId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE return_type_id = %d', ( int ) $intReturnTypeId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByCid( $intCid, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByIntermediaryAccountId( $intIntermediaryAccountId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE intermediary_account_id = %d', ( int ) $intIntermediaryAccountId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByRepairArPaymentId( $intRepairArPaymentId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE repair_ar_payment_id = %d', ( int ) $intRepairArPaymentId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsBySettlementDistributionId( $intSettlementDistributionId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE settlement_distribution_id = %d', ( int ) $intSettlementDistributionId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByCompanyPaymentId( $intCompanyPaymentId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE company_payment_id = %d', ( int ) $intCompanyPaymentId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByEftInstructionId( $intEftInstructionId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE eft_instruction_id = %d', ( int ) $intEftInstructionId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByX937CheckDetailRecordId( $intX937CheckDetailRecordId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE x937_check_detail_record_id = %d', ( int ) $intX937CheckDetailRecordId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE account_id = %d', ( int ) $intAccountId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByOffsettingNachaEntryDetailRecordId( $intOffsettingNachaEntryDetailRecordId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE offsetting_nacha_entry_detail_record_id = %d', ( int ) $intOffsettingNachaEntryDetailRecordId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByNachaReturnAddendaDetailRecordId( $intNachaReturnAddendaDetailRecordId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE nacha_return_addenda_detail_record_id = %d', ( int ) $intNachaReturnAddendaDetailRecordId ), $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByCheckAccountTypeId( $intCheckAccountTypeId, $objDatabase ) {
		return self::fetchNachaEntryDetailRecords( sprintf( 'SELECT * FROM nacha_entry_detail_records WHERE check_account_type_id = %d', ( int ) $intCheckAccountTypeId ), $objDatabase );
	}

}
?>