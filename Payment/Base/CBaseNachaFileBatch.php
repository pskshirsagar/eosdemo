<?php

class CBaseNachaFileBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.nacha_file_batches';

	protected $m_intId;
	protected $m_intNachaFileId;
	protected $m_intCid;
	protected $m_strHeaderRecordTypeCode;
	protected $m_strHeaderServiceClassCode;
	protected $m_strHeaderCompanyName;
	protected $m_strHeaderCompanyDiscretionaryData;
	protected $m_strHeaderCompanyIdentification;
	protected $m_strHeaderStandardEntryClassCode;
	protected $m_strHeaderCompanyEntryDescription;
	protected $m_strHeaderCompanyDescriptiveDate;
	protected $m_strHeaderEffectiveEntryDate;
	protected $m_strHeaderSettlementDate;
	protected $m_strHeaderOriginatorStatusCode;
	protected $m_strHeaderOriginatingDfiIdentification;
	protected $m_strHeaderBatchNumber;
	protected $m_strControlRecordTypeCode;
	protected $m_strControlServiceClassCode;
	protected $m_strControlEntryAddendaCount;
	protected $m_strControlEntryHash;
	protected $m_strControlTotalDebitAmount;
	protected $m_strControlTotalCreditAmount;
	protected $m_strControlCompanyIdentification;
	protected $m_strControlMessageAuthenticationCode;
	protected $m_strControlReserved;
	protected $m_strControlOriginatingDfiIdentification;
	protected $m_strControlBatchNumber;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['nacha_file_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaFileId', trim( $arrValues['nacha_file_id'] ) ); elseif( isset( $arrValues['nacha_file_id'] ) ) $this->setNachaFileId( $arrValues['nacha_file_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['header_record_type_code'] ) && $boolDirectSet ) $this->set( 'm_strHeaderRecordTypeCode', trim( stripcslashes( $arrValues['header_record_type_code'] ) ) ); elseif( isset( $arrValues['header_record_type_code'] ) ) $this->setHeaderRecordTypeCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_record_type_code'] ) : $arrValues['header_record_type_code'] );
		if( isset( $arrValues['header_service_class_code'] ) && $boolDirectSet ) $this->set( 'm_strHeaderServiceClassCode', trim( stripcslashes( $arrValues['header_service_class_code'] ) ) ); elseif( isset( $arrValues['header_service_class_code'] ) ) $this->setHeaderServiceClassCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_service_class_code'] ) : $arrValues['header_service_class_code'] );
		if( isset( $arrValues['header_company_name'] ) && $boolDirectSet ) $this->set( 'm_strHeaderCompanyName', trim( stripcslashes( $arrValues['header_company_name'] ) ) ); elseif( isset( $arrValues['header_company_name'] ) ) $this->setHeaderCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_company_name'] ) : $arrValues['header_company_name'] );
		if( isset( $arrValues['header_company_discretionary_data'] ) && $boolDirectSet ) $this->set( 'm_strHeaderCompanyDiscretionaryData', trim( stripcslashes( $arrValues['header_company_discretionary_data'] ) ) ); elseif( isset( $arrValues['header_company_discretionary_data'] ) ) $this->setHeaderCompanyDiscretionaryData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_company_discretionary_data'] ) : $arrValues['header_company_discretionary_data'] );
		if( isset( $arrValues['header_company_identification'] ) && $boolDirectSet ) $this->set( 'm_strHeaderCompanyIdentification', trim( stripcslashes( $arrValues['header_company_identification'] ) ) ); elseif( isset( $arrValues['header_company_identification'] ) ) $this->setHeaderCompanyIdentification( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_company_identification'] ) : $arrValues['header_company_identification'] );
		if( isset( $arrValues['header_standard_entry_class_code'] ) && $boolDirectSet ) $this->set( 'm_strHeaderStandardEntryClassCode', trim( stripcslashes( $arrValues['header_standard_entry_class_code'] ) ) ); elseif( isset( $arrValues['header_standard_entry_class_code'] ) ) $this->setHeaderStandardEntryClassCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_standard_entry_class_code'] ) : $arrValues['header_standard_entry_class_code'] );
		if( isset( $arrValues['header_company_entry_description'] ) && $boolDirectSet ) $this->set( 'm_strHeaderCompanyEntryDescription', trim( stripcslashes( $arrValues['header_company_entry_description'] ) ) ); elseif( isset( $arrValues['header_company_entry_description'] ) ) $this->setHeaderCompanyEntryDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_company_entry_description'] ) : $arrValues['header_company_entry_description'] );
		if( isset( $arrValues['header_company_descriptive_date'] ) && $boolDirectSet ) $this->set( 'm_strHeaderCompanyDescriptiveDate', trim( stripcslashes( $arrValues['header_company_descriptive_date'] ) ) ); elseif( isset( $arrValues['header_company_descriptive_date'] ) ) $this->setHeaderCompanyDescriptiveDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_company_descriptive_date'] ) : $arrValues['header_company_descriptive_date'] );
		if( isset( $arrValues['header_effective_entry_date'] ) && $boolDirectSet ) $this->set( 'm_strHeaderEffectiveEntryDate', trim( stripcslashes( $arrValues['header_effective_entry_date'] ) ) ); elseif( isset( $arrValues['header_effective_entry_date'] ) ) $this->setHeaderEffectiveEntryDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_effective_entry_date'] ) : $arrValues['header_effective_entry_date'] );
		if( isset( $arrValues['header_settlement_date'] ) && $boolDirectSet ) $this->set( 'm_strHeaderSettlementDate', trim( stripcslashes( $arrValues['header_settlement_date'] ) ) ); elseif( isset( $arrValues['header_settlement_date'] ) ) $this->setHeaderSettlementDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_settlement_date'] ) : $arrValues['header_settlement_date'] );
		if( isset( $arrValues['header_originator_status_code'] ) && $boolDirectSet ) $this->set( 'm_strHeaderOriginatorStatusCode', trim( stripcslashes( $arrValues['header_originator_status_code'] ) ) ); elseif( isset( $arrValues['header_originator_status_code'] ) ) $this->setHeaderOriginatorStatusCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_originator_status_code'] ) : $arrValues['header_originator_status_code'] );
		if( isset( $arrValues['header_originating_dfi_identification'] ) && $boolDirectSet ) $this->set( 'm_strHeaderOriginatingDfiIdentification', trim( stripcslashes( $arrValues['header_originating_dfi_identification'] ) ) ); elseif( isset( $arrValues['header_originating_dfi_identification'] ) ) $this->setHeaderOriginatingDfiIdentification( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_originating_dfi_identification'] ) : $arrValues['header_originating_dfi_identification'] );
		if( isset( $arrValues['header_batch_number'] ) && $boolDirectSet ) $this->set( 'm_strHeaderBatchNumber', trim( stripcslashes( $arrValues['header_batch_number'] ) ) ); elseif( isset( $arrValues['header_batch_number'] ) ) $this->setHeaderBatchNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_batch_number'] ) : $arrValues['header_batch_number'] );
		if( isset( $arrValues['control_record_type_code'] ) && $boolDirectSet ) $this->set( 'm_strControlRecordTypeCode', trim( stripcslashes( $arrValues['control_record_type_code'] ) ) ); elseif( isset( $arrValues['control_record_type_code'] ) ) $this->setControlRecordTypeCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_record_type_code'] ) : $arrValues['control_record_type_code'] );
		if( isset( $arrValues['control_service_class_code'] ) && $boolDirectSet ) $this->set( 'm_strControlServiceClassCode', trim( stripcslashes( $arrValues['control_service_class_code'] ) ) ); elseif( isset( $arrValues['control_service_class_code'] ) ) $this->setControlServiceClassCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_service_class_code'] ) : $arrValues['control_service_class_code'] );
		if( isset( $arrValues['control_entry_addenda_count'] ) && $boolDirectSet ) $this->set( 'm_strControlEntryAddendaCount', trim( stripcslashes( $arrValues['control_entry_addenda_count'] ) ) ); elseif( isset( $arrValues['control_entry_addenda_count'] ) ) $this->setControlEntryAddendaCount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_entry_addenda_count'] ) : $arrValues['control_entry_addenda_count'] );
		if( isset( $arrValues['control_entry_hash'] ) && $boolDirectSet ) $this->set( 'm_strControlEntryHash', trim( stripcslashes( $arrValues['control_entry_hash'] ) ) ); elseif( isset( $arrValues['control_entry_hash'] ) ) $this->setControlEntryHash( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_entry_hash'] ) : $arrValues['control_entry_hash'] );
		if( isset( $arrValues['control_total_debit_amount'] ) && $boolDirectSet ) $this->set( 'm_strControlTotalDebitAmount', trim( stripcslashes( $arrValues['control_total_debit_amount'] ) ) ); elseif( isset( $arrValues['control_total_debit_amount'] ) ) $this->setControlTotalDebitAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_total_debit_amount'] ) : $arrValues['control_total_debit_amount'] );
		if( isset( $arrValues['control_total_credit_amount'] ) && $boolDirectSet ) $this->set( 'm_strControlTotalCreditAmount', trim( stripcslashes( $arrValues['control_total_credit_amount'] ) ) ); elseif( isset( $arrValues['control_total_credit_amount'] ) ) $this->setControlTotalCreditAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_total_credit_amount'] ) : $arrValues['control_total_credit_amount'] );
		if( isset( $arrValues['control_company_identification'] ) && $boolDirectSet ) $this->set( 'm_strControlCompanyIdentification', trim( stripcslashes( $arrValues['control_company_identification'] ) ) ); elseif( isset( $arrValues['control_company_identification'] ) ) $this->setControlCompanyIdentification( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_company_identification'] ) : $arrValues['control_company_identification'] );
		if( isset( $arrValues['control_message_authentication_code'] ) && $boolDirectSet ) $this->set( 'm_strControlMessageAuthenticationCode', trim( stripcslashes( $arrValues['control_message_authentication_code'] ) ) ); elseif( isset( $arrValues['control_message_authentication_code'] ) ) $this->setControlMessageAuthenticationCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_message_authentication_code'] ) : $arrValues['control_message_authentication_code'] );
		if( isset( $arrValues['control_reserved'] ) && $boolDirectSet ) $this->set( 'm_strControlReserved', trim( stripcslashes( $arrValues['control_reserved'] ) ) ); elseif( isset( $arrValues['control_reserved'] ) ) $this->setControlReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_reserved'] ) : $arrValues['control_reserved'] );
		if( isset( $arrValues['control_originating_dfi_identification'] ) && $boolDirectSet ) $this->set( 'm_strControlOriginatingDfiIdentification', trim( stripcslashes( $arrValues['control_originating_dfi_identification'] ) ) ); elseif( isset( $arrValues['control_originating_dfi_identification'] ) ) $this->setControlOriginatingDfiIdentification( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_originating_dfi_identification'] ) : $arrValues['control_originating_dfi_identification'] );
		if( isset( $arrValues['control_batch_number'] ) && $boolDirectSet ) $this->set( 'm_strControlBatchNumber', trim( stripcslashes( $arrValues['control_batch_number'] ) ) ); elseif( isset( $arrValues['control_batch_number'] ) ) $this->setControlBatchNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_batch_number'] ) : $arrValues['control_batch_number'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setNachaFileId( $intNachaFileId ) {
		$this->set( 'm_intNachaFileId', CStrings::strToIntDef( $intNachaFileId, NULL, false ) );
	}

	public function getNachaFileId() {
		return $this->m_intNachaFileId;
	}

	public function sqlNachaFileId() {
		return ( true == isset( $this->m_intNachaFileId ) ) ? ( string ) $this->m_intNachaFileId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setHeaderRecordTypeCode( $strHeaderRecordTypeCode ) {
		$this->set( 'm_strHeaderRecordTypeCode', CStrings::strTrimDef( $strHeaderRecordTypeCode, 1, NULL, true ) );
	}

	public function getHeaderRecordTypeCode() {
		return $this->m_strHeaderRecordTypeCode;
	}

	public function sqlHeaderRecordTypeCode() {
		return ( true == isset( $this->m_strHeaderRecordTypeCode ) ) ? '\'' . addslashes( $this->m_strHeaderRecordTypeCode ) . '\'' : 'NULL';
	}

	public function setHeaderServiceClassCode( $strHeaderServiceClassCode ) {
		$this->set( 'm_strHeaderServiceClassCode', CStrings::strTrimDef( $strHeaderServiceClassCode, 3, NULL, true ) );
	}

	public function getHeaderServiceClassCode() {
		return $this->m_strHeaderServiceClassCode;
	}

	public function sqlHeaderServiceClassCode() {
		return ( true == isset( $this->m_strHeaderServiceClassCode ) ) ? '\'' . addslashes( $this->m_strHeaderServiceClassCode ) . '\'' : 'NULL';
	}

	public function setHeaderCompanyName( $strHeaderCompanyName ) {
		$this->set( 'm_strHeaderCompanyName', CStrings::strTrimDef( $strHeaderCompanyName, 16, NULL, true ) );
	}

	public function getHeaderCompanyName() {
		return $this->m_strHeaderCompanyName;
	}

	public function sqlHeaderCompanyName() {
		return ( true == isset( $this->m_strHeaderCompanyName ) ) ? '\'' . addslashes( $this->m_strHeaderCompanyName ) . '\'' : 'NULL';
	}

	public function setHeaderCompanyDiscretionaryData( $strHeaderCompanyDiscretionaryData ) {
		$this->set( 'm_strHeaderCompanyDiscretionaryData', CStrings::strTrimDef( $strHeaderCompanyDiscretionaryData, 20, NULL, true ) );
	}

	public function getHeaderCompanyDiscretionaryData() {
		return $this->m_strHeaderCompanyDiscretionaryData;
	}

	public function sqlHeaderCompanyDiscretionaryData() {
		return ( true == isset( $this->m_strHeaderCompanyDiscretionaryData ) ) ? '\'' . addslashes( $this->m_strHeaderCompanyDiscretionaryData ) . '\'' : 'NULL';
	}

	public function setHeaderCompanyIdentification( $strHeaderCompanyIdentification ) {
		$this->set( 'm_strHeaderCompanyIdentification', CStrings::strTrimDef( $strHeaderCompanyIdentification, 10, NULL, true ) );
	}

	public function getHeaderCompanyIdentification() {
		return $this->m_strHeaderCompanyIdentification;
	}

	public function sqlHeaderCompanyIdentification() {
		return ( true == isset( $this->m_strHeaderCompanyIdentification ) ) ? '\'' . addslashes( $this->m_strHeaderCompanyIdentification ) . '\'' : 'NULL';
	}

	public function setHeaderStandardEntryClassCode( $strHeaderStandardEntryClassCode ) {
		$this->set( 'm_strHeaderStandardEntryClassCode', CStrings::strTrimDef( $strHeaderStandardEntryClassCode, 3, NULL, true ) );
	}

	public function getHeaderStandardEntryClassCode() {
		return $this->m_strHeaderStandardEntryClassCode;
	}

	public function sqlHeaderStandardEntryClassCode() {
		return ( true == isset( $this->m_strHeaderStandardEntryClassCode ) ) ? '\'' . addslashes( $this->m_strHeaderStandardEntryClassCode ) . '\'' : 'NULL';
	}

	public function setHeaderCompanyEntryDescription( $strHeaderCompanyEntryDescription ) {
		$this->set( 'm_strHeaderCompanyEntryDescription', CStrings::strTrimDef( $strHeaderCompanyEntryDescription, 10, NULL, true ) );
	}

	public function getHeaderCompanyEntryDescription() {
		return $this->m_strHeaderCompanyEntryDescription;
	}

	public function sqlHeaderCompanyEntryDescription() {
		return ( true == isset( $this->m_strHeaderCompanyEntryDescription ) ) ? '\'' . addslashes( $this->m_strHeaderCompanyEntryDescription ) . '\'' : 'NULL';
	}

	public function setHeaderCompanyDescriptiveDate( $strHeaderCompanyDescriptiveDate ) {
		$this->set( 'm_strHeaderCompanyDescriptiveDate', CStrings::strTrimDef( $strHeaderCompanyDescriptiveDate, 6, NULL, true ) );
	}

	public function getHeaderCompanyDescriptiveDate() {
		return $this->m_strHeaderCompanyDescriptiveDate;
	}

	public function sqlHeaderCompanyDescriptiveDate() {
		return ( true == isset( $this->m_strHeaderCompanyDescriptiveDate ) ) ? '\'' . addslashes( $this->m_strHeaderCompanyDescriptiveDate ) . '\'' : 'NULL';
	}

	public function setHeaderEffectiveEntryDate( $strHeaderEffectiveEntryDate ) {
		$this->set( 'm_strHeaderEffectiveEntryDate', CStrings::strTrimDef( $strHeaderEffectiveEntryDate, 6, NULL, true ) );
	}

	public function getHeaderEffectiveEntryDate() {
		return $this->m_strHeaderEffectiveEntryDate;
	}

	public function sqlHeaderEffectiveEntryDate() {
		return ( true == isset( $this->m_strHeaderEffectiveEntryDate ) ) ? '\'' . addslashes( $this->m_strHeaderEffectiveEntryDate ) . '\'' : 'NULL';
	}

	public function setHeaderSettlementDate( $strHeaderSettlementDate ) {
		$this->set( 'm_strHeaderSettlementDate', CStrings::strTrimDef( $strHeaderSettlementDate, 3, NULL, true ) );
	}

	public function getHeaderSettlementDate() {
		return $this->m_strHeaderSettlementDate;
	}

	public function sqlHeaderSettlementDate() {
		return ( true == isset( $this->m_strHeaderSettlementDate ) ) ? '\'' . addslashes( $this->m_strHeaderSettlementDate ) . '\'' : 'NULL';
	}

	public function setHeaderOriginatorStatusCode( $strHeaderOriginatorStatusCode ) {
		$this->set( 'm_strHeaderOriginatorStatusCode', CStrings::strTrimDef( $strHeaderOriginatorStatusCode, 1, NULL, true ) );
	}

	public function getHeaderOriginatorStatusCode() {
		return $this->m_strHeaderOriginatorStatusCode;
	}

	public function sqlHeaderOriginatorStatusCode() {
		return ( true == isset( $this->m_strHeaderOriginatorStatusCode ) ) ? '\'' . addslashes( $this->m_strHeaderOriginatorStatusCode ) . '\'' : 'NULL';
	}

	public function setHeaderOriginatingDfiIdentification( $strHeaderOriginatingDfiIdentification ) {
		$this->set( 'm_strHeaderOriginatingDfiIdentification', CStrings::strTrimDef( $strHeaderOriginatingDfiIdentification, 8, NULL, true ) );
	}

	public function getHeaderOriginatingDfiIdentification() {
		return $this->m_strHeaderOriginatingDfiIdentification;
	}

	public function sqlHeaderOriginatingDfiIdentification() {
		return ( true == isset( $this->m_strHeaderOriginatingDfiIdentification ) ) ? '\'' . addslashes( $this->m_strHeaderOriginatingDfiIdentification ) . '\'' : 'NULL';
	}

	public function setHeaderBatchNumber( $strHeaderBatchNumber ) {
		$this->set( 'm_strHeaderBatchNumber', CStrings::strTrimDef( $strHeaderBatchNumber, 7, NULL, true ) );
	}

	public function getHeaderBatchNumber() {
		return $this->m_strHeaderBatchNumber;
	}

	public function sqlHeaderBatchNumber() {
		return ( true == isset( $this->m_strHeaderBatchNumber ) ) ? '\'' . addslashes( $this->m_strHeaderBatchNumber ) . '\'' : 'NULL';
	}

	public function setControlRecordTypeCode( $strControlRecordTypeCode ) {
		$this->set( 'm_strControlRecordTypeCode', CStrings::strTrimDef( $strControlRecordTypeCode, 1, NULL, true ) );
	}

	public function getControlRecordTypeCode() {
		return $this->m_strControlRecordTypeCode;
	}

	public function sqlControlRecordTypeCode() {
		return ( true == isset( $this->m_strControlRecordTypeCode ) ) ? '\'' . addslashes( $this->m_strControlRecordTypeCode ) . '\'' : 'NULL';
	}

	public function setControlServiceClassCode( $strControlServiceClassCode ) {
		$this->set( 'm_strControlServiceClassCode', CStrings::strTrimDef( $strControlServiceClassCode, 3, NULL, true ) );
	}

	public function getControlServiceClassCode() {
		return $this->m_strControlServiceClassCode;
	}

	public function sqlControlServiceClassCode() {
		return ( true == isset( $this->m_strControlServiceClassCode ) ) ? '\'' . addslashes( $this->m_strControlServiceClassCode ) . '\'' : 'NULL';
	}

	public function setControlEntryAddendaCount( $strControlEntryAddendaCount ) {
		$this->set( 'm_strControlEntryAddendaCount', CStrings::strTrimDef( $strControlEntryAddendaCount, 6, NULL, true ) );
	}

	public function getControlEntryAddendaCount() {
		return $this->m_strControlEntryAddendaCount;
	}

	public function sqlControlEntryAddendaCount() {
		return ( true == isset( $this->m_strControlEntryAddendaCount ) ) ? '\'' . addslashes( $this->m_strControlEntryAddendaCount ) . '\'' : 'NULL';
	}

	public function setControlEntryHash( $strControlEntryHash ) {
		$this->set( 'm_strControlEntryHash', CStrings::strTrimDef( $strControlEntryHash, 10, NULL, true ) );
	}

	public function getControlEntryHash() {
		return $this->m_strControlEntryHash;
	}

	public function sqlControlEntryHash() {
		return ( true == isset( $this->m_strControlEntryHash ) ) ? '\'' . addslashes( $this->m_strControlEntryHash ) . '\'' : 'NULL';
	}

	public function setControlTotalDebitAmount( $strControlTotalDebitAmount ) {
		$this->set( 'm_strControlTotalDebitAmount', CStrings::strTrimDef( $strControlTotalDebitAmount, 12, NULL, true ) );
	}

	public function getControlTotalDebitAmount() {
		return $this->m_strControlTotalDebitAmount;
	}

	public function sqlControlTotalDebitAmount() {
		return ( true == isset( $this->m_strControlTotalDebitAmount ) ) ? '\'' . addslashes( $this->m_strControlTotalDebitAmount ) . '\'' : 'NULL';
	}

	public function setControlTotalCreditAmount( $strControlTotalCreditAmount ) {
		$this->set( 'm_strControlTotalCreditAmount', CStrings::strTrimDef( $strControlTotalCreditAmount, 12, NULL, true ) );
	}

	public function getControlTotalCreditAmount() {
		return $this->m_strControlTotalCreditAmount;
	}

	public function sqlControlTotalCreditAmount() {
		return ( true == isset( $this->m_strControlTotalCreditAmount ) ) ? '\'' . addslashes( $this->m_strControlTotalCreditAmount ) . '\'' : 'NULL';
	}

	public function setControlCompanyIdentification( $strControlCompanyIdentification ) {
		$this->set( 'm_strControlCompanyIdentification', CStrings::strTrimDef( $strControlCompanyIdentification, 10, NULL, true ) );
	}

	public function getControlCompanyIdentification() {
		return $this->m_strControlCompanyIdentification;
	}

	public function sqlControlCompanyIdentification() {
		return ( true == isset( $this->m_strControlCompanyIdentification ) ) ? '\'' . addslashes( $this->m_strControlCompanyIdentification ) . '\'' : 'NULL';
	}

	public function setControlMessageAuthenticationCode( $strControlMessageAuthenticationCode ) {
		$this->set( 'm_strControlMessageAuthenticationCode', CStrings::strTrimDef( $strControlMessageAuthenticationCode, 19, NULL, true ) );
	}

	public function getControlMessageAuthenticationCode() {
		return $this->m_strControlMessageAuthenticationCode;
	}

	public function sqlControlMessageAuthenticationCode() {
		return ( true == isset( $this->m_strControlMessageAuthenticationCode ) ) ? '\'' . addslashes( $this->m_strControlMessageAuthenticationCode ) . '\'' : 'NULL';
	}

	public function setControlReserved( $strControlReserved ) {
		$this->set( 'm_strControlReserved', CStrings::strTrimDef( $strControlReserved, 6, NULL, true ) );
	}

	public function getControlReserved() {
		return $this->m_strControlReserved;
	}

	public function sqlControlReserved() {
		return ( true == isset( $this->m_strControlReserved ) ) ? '\'' . addslashes( $this->m_strControlReserved ) . '\'' : 'NULL';
	}

	public function setControlOriginatingDfiIdentification( $strControlOriginatingDfiIdentification ) {
		$this->set( 'm_strControlOriginatingDfiIdentification', CStrings::strTrimDef( $strControlOriginatingDfiIdentification, 8, NULL, true ) );
	}

	public function getControlOriginatingDfiIdentification() {
		return $this->m_strControlOriginatingDfiIdentification;
	}

	public function sqlControlOriginatingDfiIdentification() {
		return ( true == isset( $this->m_strControlOriginatingDfiIdentification ) ) ? '\'' . addslashes( $this->m_strControlOriginatingDfiIdentification ) . '\'' : 'NULL';
	}

	public function setControlBatchNumber( $strControlBatchNumber ) {
		$this->set( 'm_strControlBatchNumber', CStrings::strTrimDef( $strControlBatchNumber, 7, NULL, true ) );
	}

	public function getControlBatchNumber() {
		return $this->m_strControlBatchNumber;
	}

	public function sqlControlBatchNumber() {
		return ( true == isset( $this->m_strControlBatchNumber ) ) ? '\'' . addslashes( $this->m_strControlBatchNumber ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, nacha_file_id, cid, header_record_type_code, header_service_class_code, header_company_name, header_company_discretionary_data, header_company_identification, header_standard_entry_class_code, header_company_entry_description, header_company_descriptive_date, header_effective_entry_date, header_settlement_date, header_originator_status_code, header_originating_dfi_identification, header_batch_number, control_record_type_code, control_service_class_code, control_entry_addenda_count, control_entry_hash, control_total_debit_amount, control_total_credit_amount, control_company_identification, control_message_authentication_code, control_reserved, control_originating_dfi_identification, control_batch_number, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlNachaFileId() . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlHeaderRecordTypeCode() . ', ' .
 						$this->sqlHeaderServiceClassCode() . ', ' .
 						$this->sqlHeaderCompanyName() . ', ' .
 						$this->sqlHeaderCompanyDiscretionaryData() . ', ' .
 						$this->sqlHeaderCompanyIdentification() . ', ' .
 						$this->sqlHeaderStandardEntryClassCode() . ', ' .
 						$this->sqlHeaderCompanyEntryDescription() . ', ' .
 						$this->sqlHeaderCompanyDescriptiveDate() . ', ' .
 						$this->sqlHeaderEffectiveEntryDate() . ', ' .
 						$this->sqlHeaderSettlementDate() . ', ' .
 						$this->sqlHeaderOriginatorStatusCode() . ', ' .
 						$this->sqlHeaderOriginatingDfiIdentification() . ', ' .
 						$this->sqlHeaderBatchNumber() . ', ' .
 						$this->sqlControlRecordTypeCode() . ', ' .
 						$this->sqlControlServiceClassCode() . ', ' .
 						$this->sqlControlEntryAddendaCount() . ', ' .
 						$this->sqlControlEntryHash() . ', ' .
 						$this->sqlControlTotalDebitAmount() . ', ' .
 						$this->sqlControlTotalCreditAmount() . ', ' .
 						$this->sqlControlCompanyIdentification() . ', ' .
 						$this->sqlControlMessageAuthenticationCode() . ', ' .
 						$this->sqlControlReserved() . ', ' .
 						$this->sqlControlOriginatingDfiIdentification() . ', ' .
 						$this->sqlControlBatchNumber() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_file_id = ' . $this->sqlNachaFileId() . ','; } elseif( true == array_key_exists( 'NachaFileId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_file_id = ' . $this->sqlNachaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_record_type_code = ' . $this->sqlHeaderRecordTypeCode() . ','; } elseif( true == array_key_exists( 'HeaderRecordTypeCode', $this->getChangedColumns() ) ) { $strSql .= ' header_record_type_code = ' . $this->sqlHeaderRecordTypeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_service_class_code = ' . $this->sqlHeaderServiceClassCode() . ','; } elseif( true == array_key_exists( 'HeaderServiceClassCode', $this->getChangedColumns() ) ) { $strSql .= ' header_service_class_code = ' . $this->sqlHeaderServiceClassCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_company_name = ' . $this->sqlHeaderCompanyName() . ','; } elseif( true == array_key_exists( 'HeaderCompanyName', $this->getChangedColumns() ) ) { $strSql .= ' header_company_name = ' . $this->sqlHeaderCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_company_discretionary_data = ' . $this->sqlHeaderCompanyDiscretionaryData() . ','; } elseif( true == array_key_exists( 'HeaderCompanyDiscretionaryData', $this->getChangedColumns() ) ) { $strSql .= ' header_company_discretionary_data = ' . $this->sqlHeaderCompanyDiscretionaryData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_company_identification = ' . $this->sqlHeaderCompanyIdentification() . ','; } elseif( true == array_key_exists( 'HeaderCompanyIdentification', $this->getChangedColumns() ) ) { $strSql .= ' header_company_identification = ' . $this->sqlHeaderCompanyIdentification() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_standard_entry_class_code = ' . $this->sqlHeaderStandardEntryClassCode() . ','; } elseif( true == array_key_exists( 'HeaderStandardEntryClassCode', $this->getChangedColumns() ) ) { $strSql .= ' header_standard_entry_class_code = ' . $this->sqlHeaderStandardEntryClassCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_company_entry_description = ' . $this->sqlHeaderCompanyEntryDescription() . ','; } elseif( true == array_key_exists( 'HeaderCompanyEntryDescription', $this->getChangedColumns() ) ) { $strSql .= ' header_company_entry_description = ' . $this->sqlHeaderCompanyEntryDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_company_descriptive_date = ' . $this->sqlHeaderCompanyDescriptiveDate() . ','; } elseif( true == array_key_exists( 'HeaderCompanyDescriptiveDate', $this->getChangedColumns() ) ) { $strSql .= ' header_company_descriptive_date = ' . $this->sqlHeaderCompanyDescriptiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_effective_entry_date = ' . $this->sqlHeaderEffectiveEntryDate() . ','; } elseif( true == array_key_exists( 'HeaderEffectiveEntryDate', $this->getChangedColumns() ) ) { $strSql .= ' header_effective_entry_date = ' . $this->sqlHeaderEffectiveEntryDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_settlement_date = ' . $this->sqlHeaderSettlementDate() . ','; } elseif( true == array_key_exists( 'HeaderSettlementDate', $this->getChangedColumns() ) ) { $strSql .= ' header_settlement_date = ' . $this->sqlHeaderSettlementDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_originator_status_code = ' . $this->sqlHeaderOriginatorStatusCode() . ','; } elseif( true == array_key_exists( 'HeaderOriginatorStatusCode', $this->getChangedColumns() ) ) { $strSql .= ' header_originator_status_code = ' . $this->sqlHeaderOriginatorStatusCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_originating_dfi_identification = ' . $this->sqlHeaderOriginatingDfiIdentification() . ','; } elseif( true == array_key_exists( 'HeaderOriginatingDfiIdentification', $this->getChangedColumns() ) ) { $strSql .= ' header_originating_dfi_identification = ' . $this->sqlHeaderOriginatingDfiIdentification() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_batch_number = ' . $this->sqlHeaderBatchNumber() . ','; } elseif( true == array_key_exists( 'HeaderBatchNumber', $this->getChangedColumns() ) ) { $strSql .= ' header_batch_number = ' . $this->sqlHeaderBatchNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_record_type_code = ' . $this->sqlControlRecordTypeCode() . ','; } elseif( true == array_key_exists( 'ControlRecordTypeCode', $this->getChangedColumns() ) ) { $strSql .= ' control_record_type_code = ' . $this->sqlControlRecordTypeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_service_class_code = ' . $this->sqlControlServiceClassCode() . ','; } elseif( true == array_key_exists( 'ControlServiceClassCode', $this->getChangedColumns() ) ) { $strSql .= ' control_service_class_code = ' . $this->sqlControlServiceClassCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_entry_addenda_count = ' . $this->sqlControlEntryAddendaCount() . ','; } elseif( true == array_key_exists( 'ControlEntryAddendaCount', $this->getChangedColumns() ) ) { $strSql .= ' control_entry_addenda_count = ' . $this->sqlControlEntryAddendaCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_entry_hash = ' . $this->sqlControlEntryHash() . ','; } elseif( true == array_key_exists( 'ControlEntryHash', $this->getChangedColumns() ) ) { $strSql .= ' control_entry_hash = ' . $this->sqlControlEntryHash() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_total_debit_amount = ' . $this->sqlControlTotalDebitAmount() . ','; } elseif( true == array_key_exists( 'ControlTotalDebitAmount', $this->getChangedColumns() ) ) { $strSql .= ' control_total_debit_amount = ' . $this->sqlControlTotalDebitAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_total_credit_amount = ' . $this->sqlControlTotalCreditAmount() . ','; } elseif( true == array_key_exists( 'ControlTotalCreditAmount', $this->getChangedColumns() ) ) { $strSql .= ' control_total_credit_amount = ' . $this->sqlControlTotalCreditAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_company_identification = ' . $this->sqlControlCompanyIdentification() . ','; } elseif( true == array_key_exists( 'ControlCompanyIdentification', $this->getChangedColumns() ) ) { $strSql .= ' control_company_identification = ' . $this->sqlControlCompanyIdentification() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_message_authentication_code = ' . $this->sqlControlMessageAuthenticationCode() . ','; } elseif( true == array_key_exists( 'ControlMessageAuthenticationCode', $this->getChangedColumns() ) ) { $strSql .= ' control_message_authentication_code = ' . $this->sqlControlMessageAuthenticationCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_reserved = ' . $this->sqlControlReserved() . ','; } elseif( true == array_key_exists( 'ControlReserved', $this->getChangedColumns() ) ) { $strSql .= ' control_reserved = ' . $this->sqlControlReserved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_originating_dfi_identification = ' . $this->sqlControlOriginatingDfiIdentification() . ','; } elseif( true == array_key_exists( 'ControlOriginatingDfiIdentification', $this->getChangedColumns() ) ) { $strSql .= ' control_originating_dfi_identification = ' . $this->sqlControlOriginatingDfiIdentification() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_batch_number = ' . $this->sqlControlBatchNumber() . ','; } elseif( true == array_key_exists( 'ControlBatchNumber', $this->getChangedColumns() ) ) { $strSql .= ' control_batch_number = ' . $this->sqlControlBatchNumber() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'nacha_file_id' => $this->getNachaFileId(),
			'cid' => $this->getCid(),
			'header_record_type_code' => $this->getHeaderRecordTypeCode(),
			'header_service_class_code' => $this->getHeaderServiceClassCode(),
			'header_company_name' => $this->getHeaderCompanyName(),
			'header_company_discretionary_data' => $this->getHeaderCompanyDiscretionaryData(),
			'header_company_identification' => $this->getHeaderCompanyIdentification(),
			'header_standard_entry_class_code' => $this->getHeaderStandardEntryClassCode(),
			'header_company_entry_description' => $this->getHeaderCompanyEntryDescription(),
			'header_company_descriptive_date' => $this->getHeaderCompanyDescriptiveDate(),
			'header_effective_entry_date' => $this->getHeaderEffectiveEntryDate(),
			'header_settlement_date' => $this->getHeaderSettlementDate(),
			'header_originator_status_code' => $this->getHeaderOriginatorStatusCode(),
			'header_originating_dfi_identification' => $this->getHeaderOriginatingDfiIdentification(),
			'header_batch_number' => $this->getHeaderBatchNumber(),
			'control_record_type_code' => $this->getControlRecordTypeCode(),
			'control_service_class_code' => $this->getControlServiceClassCode(),
			'control_entry_addenda_count' => $this->getControlEntryAddendaCount(),
			'control_entry_hash' => $this->getControlEntryHash(),
			'control_total_debit_amount' => $this->getControlTotalDebitAmount(),
			'control_total_credit_amount' => $this->getControlTotalCreditAmount(),
			'control_company_identification' => $this->getControlCompanyIdentification(),
			'control_message_authentication_code' => $this->getControlMessageAuthenticationCode(),
			'control_reserved' => $this->getControlReserved(),
			'control_originating_dfi_identification' => $this->getControlOriginatingDfiIdentification(),
			'control_batch_number' => $this->getControlBatchNumber(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>