<?php

class CBaseProcessingBank extends CEosSingularBase {

	const TABLE_NAME = 'public.processing_banks';

	protected $m_intId;
	protected $m_intAchMerchantGatewayId;
	protected $m_intCcMerchantGatewayId;
	protected $m_intX937MerchantGatewayId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strNotificationEmail;
	protected $m_strAchImmDestRoutingNumber;
	protected $m_strAchImmOrigRoutingNumber;
	protected $m_strAchImmDestName;
	protected $m_strAchImmOrigName;
	protected $m_strX937ImmDestRoutingNumber;
	protected $m_strX937ImmOrigRoutingNumber;
	protected $m_strX937ImmDestName;
	protected $m_strX937ImmOrigName;
	protected $m_strX937ImmOrigContactName;
	protected $m_strX937ImmOrigContactPhone;
	protected $m_strEndrsBankName;
	protected $m_strEndrsAcctDescription;
	protected $m_strEndrsRtrnsRoutingNum;
	protected $m_strEndrsRtrnsAcctNumEncrypted;
	protected $m_intIsBalancedAchFiles;
	protected $m_intIsPublished;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_intIsBalancedAchFiles = '1';
		$this->m_intIsPublished = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ach_merchant_gateway_id'] ) && $boolDirectSet ) $this->set( 'm_intAchMerchantGatewayId', trim( $arrValues['ach_merchant_gateway_id'] ) ); elseif( isset( $arrValues['ach_merchant_gateway_id'] ) ) $this->setAchMerchantGatewayId( $arrValues['ach_merchant_gateway_id'] );
		if( isset( $arrValues['cc_merchant_gateway_id'] ) && $boolDirectSet ) $this->set( 'm_intCcMerchantGatewayId', trim( $arrValues['cc_merchant_gateway_id'] ) ); elseif( isset( $arrValues['cc_merchant_gateway_id'] ) ) $this->setCcMerchantGatewayId( $arrValues['cc_merchant_gateway_id'] );
		if( isset( $arrValues['x937_merchant_gateway_id'] ) && $boolDirectSet ) $this->set( 'm_intX937MerchantGatewayId', trim( $arrValues['x937_merchant_gateway_id'] ) ); elseif( isset( $arrValues['x937_merchant_gateway_id'] ) ) $this->setX937MerchantGatewayId( $arrValues['x937_merchant_gateway_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['notification_email'] ) && $boolDirectSet ) $this->set( 'm_strNotificationEmail', trim( stripcslashes( $arrValues['notification_email'] ) ) ); elseif( isset( $arrValues['notification_email'] ) ) $this->setNotificationEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notification_email'] ) : $arrValues['notification_email'] );
		if( isset( $arrValues['ach_imm_dest_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strAchImmDestRoutingNumber', trim( stripcslashes( $arrValues['ach_imm_dest_routing_number'] ) ) ); elseif( isset( $arrValues['ach_imm_dest_routing_number'] ) ) $this->setAchImmDestRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ach_imm_dest_routing_number'] ) : $arrValues['ach_imm_dest_routing_number'] );
		if( isset( $arrValues['ach_imm_orig_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strAchImmOrigRoutingNumber', trim( stripcslashes( $arrValues['ach_imm_orig_routing_number'] ) ) ); elseif( isset( $arrValues['ach_imm_orig_routing_number'] ) ) $this->setAchImmOrigRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ach_imm_orig_routing_number'] ) : $arrValues['ach_imm_orig_routing_number'] );
		if( isset( $arrValues['ach_imm_dest_name'] ) && $boolDirectSet ) $this->set( 'm_strAchImmDestName', trim( stripcslashes( $arrValues['ach_imm_dest_name'] ) ) ); elseif( isset( $arrValues['ach_imm_dest_name'] ) ) $this->setAchImmDestName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ach_imm_dest_name'] ) : $arrValues['ach_imm_dest_name'] );
		if( isset( $arrValues['ach_imm_orig_name'] ) && $boolDirectSet ) $this->set( 'm_strAchImmOrigName', trim( stripcslashes( $arrValues['ach_imm_orig_name'] ) ) ); elseif( isset( $arrValues['ach_imm_orig_name'] ) ) $this->setAchImmOrigName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ach_imm_orig_name'] ) : $arrValues['ach_imm_orig_name'] );
		if( isset( $arrValues['x937_imm_dest_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strX937ImmDestRoutingNumber', trim( stripcslashes( $arrValues['x937_imm_dest_routing_number'] ) ) ); elseif( isset( $arrValues['x937_imm_dest_routing_number'] ) ) $this->setX937ImmDestRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['x937_imm_dest_routing_number'] ) : $arrValues['x937_imm_dest_routing_number'] );
		if( isset( $arrValues['x937_imm_orig_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strX937ImmOrigRoutingNumber', trim( stripcslashes( $arrValues['x937_imm_orig_routing_number'] ) ) ); elseif( isset( $arrValues['x937_imm_orig_routing_number'] ) ) $this->setX937ImmOrigRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['x937_imm_orig_routing_number'] ) : $arrValues['x937_imm_orig_routing_number'] );
		if( isset( $arrValues['x937_imm_dest_name'] ) && $boolDirectSet ) $this->set( 'm_strX937ImmDestName', trim( stripcslashes( $arrValues['x937_imm_dest_name'] ) ) ); elseif( isset( $arrValues['x937_imm_dest_name'] ) ) $this->setX937ImmDestName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['x937_imm_dest_name'] ) : $arrValues['x937_imm_dest_name'] );
		if( isset( $arrValues['x937_imm_orig_name'] ) && $boolDirectSet ) $this->set( 'm_strX937ImmOrigName', trim( stripcslashes( $arrValues['x937_imm_orig_name'] ) ) ); elseif( isset( $arrValues['x937_imm_orig_name'] ) ) $this->setX937ImmOrigName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['x937_imm_orig_name'] ) : $arrValues['x937_imm_orig_name'] );
		if( isset( $arrValues['x937_imm_orig_contact_name'] ) && $boolDirectSet ) $this->set( 'm_strX937ImmOrigContactName', trim( stripcslashes( $arrValues['x937_imm_orig_contact_name'] ) ) ); elseif( isset( $arrValues['x937_imm_orig_contact_name'] ) ) $this->setX937ImmOrigContactName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['x937_imm_orig_contact_name'] ) : $arrValues['x937_imm_orig_contact_name'] );
		if( isset( $arrValues['x937_imm_orig_contact_phone'] ) && $boolDirectSet ) $this->set( 'm_strX937ImmOrigContactPhone', trim( stripcslashes( $arrValues['x937_imm_orig_contact_phone'] ) ) ); elseif( isset( $arrValues['x937_imm_orig_contact_phone'] ) ) $this->setX937ImmOrigContactPhone( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['x937_imm_orig_contact_phone'] ) : $arrValues['x937_imm_orig_contact_phone'] );
		if( isset( $arrValues['endrs_bank_name'] ) && $boolDirectSet ) $this->set( 'm_strEndrsBankName', trim( stripcslashes( $arrValues['endrs_bank_name'] ) ) ); elseif( isset( $arrValues['endrs_bank_name'] ) ) $this->setEndrsBankName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['endrs_bank_name'] ) : $arrValues['endrs_bank_name'] );
		if( isset( $arrValues['endrs_acct_description'] ) && $boolDirectSet ) $this->set( 'm_strEndrsAcctDescription', trim( stripcslashes( $arrValues['endrs_acct_description'] ) ) ); elseif( isset( $arrValues['endrs_acct_description'] ) ) $this->setEndrsAcctDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['endrs_acct_description'] ) : $arrValues['endrs_acct_description'] );
		if( isset( $arrValues['endrs_rtrns_routing_num'] ) && $boolDirectSet ) $this->set( 'm_strEndrsRtrnsRoutingNum', trim( stripcslashes( $arrValues['endrs_rtrns_routing_num'] ) ) ); elseif( isset( $arrValues['endrs_rtrns_routing_num'] ) ) $this->setEndrsRtrnsRoutingNum( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['endrs_rtrns_routing_num'] ) : $arrValues['endrs_rtrns_routing_num'] );
		if( isset( $arrValues['endrs_rtrns_acct_num_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strEndrsRtrnsAcctNumEncrypted', trim( stripcslashes( $arrValues['endrs_rtrns_acct_num_encrypted'] ) ) ); elseif( isset( $arrValues['endrs_rtrns_acct_num_encrypted'] ) ) $this->setEndrsRtrnsAcctNumEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['endrs_rtrns_acct_num_encrypted'] ) : $arrValues['endrs_rtrns_acct_num_encrypted'] );
		if( isset( $arrValues['is_balanced_ach_files'] ) && $boolDirectSet ) $this->set( 'm_intIsBalancedAchFiles', trim( $arrValues['is_balanced_ach_files'] ) ); elseif( isset( $arrValues['is_balanced_ach_files'] ) ) $this->setIsBalancedAchFiles( $arrValues['is_balanced_ach_files'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAchMerchantGatewayId( $intAchMerchantGatewayId ) {
		$this->set( 'm_intAchMerchantGatewayId', CStrings::strToIntDef( $intAchMerchantGatewayId, NULL, false ) );
	}

	public function getAchMerchantGatewayId() {
		return $this->m_intAchMerchantGatewayId;
	}

	public function sqlAchMerchantGatewayId() {
		return ( true == isset( $this->m_intAchMerchantGatewayId ) ) ? ( string ) $this->m_intAchMerchantGatewayId : 'NULL';
	}

	public function setCcMerchantGatewayId( $intCcMerchantGatewayId ) {
		$this->set( 'm_intCcMerchantGatewayId', CStrings::strToIntDef( $intCcMerchantGatewayId, NULL, false ) );
	}

	public function getCcMerchantGatewayId() {
		return $this->m_intCcMerchantGatewayId;
	}

	public function sqlCcMerchantGatewayId() {
		return ( true == isset( $this->m_intCcMerchantGatewayId ) ) ? ( string ) $this->m_intCcMerchantGatewayId : 'NULL';
	}

	public function setX937MerchantGatewayId( $intX937MerchantGatewayId ) {
		$this->set( 'm_intX937MerchantGatewayId', CStrings::strToIntDef( $intX937MerchantGatewayId, NULL, false ) );
	}

	public function getX937MerchantGatewayId() {
		return $this->m_intX937MerchantGatewayId;
	}

	public function sqlX937MerchantGatewayId() {
		return ( true == isset( $this->m_intX937MerchantGatewayId ) ) ? ( string ) $this->m_intX937MerchantGatewayId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setNotificationEmail( $strNotificationEmail ) {
		$this->set( 'm_strNotificationEmail', CStrings::strTrimDef( $strNotificationEmail, 240, NULL, true ) );
	}

	public function getNotificationEmail() {
		return $this->m_strNotificationEmail;
	}

	public function sqlNotificationEmail() {
		return ( true == isset( $this->m_strNotificationEmail ) ) ? '\'' . addslashes( $this->m_strNotificationEmail ) . '\'' : 'NULL';
	}

	public function setAchImmDestRoutingNumber( $strAchImmDestRoutingNumber ) {
		$this->set( 'm_strAchImmDestRoutingNumber', CStrings::strTrimDef( $strAchImmDestRoutingNumber, 240, NULL, true ) );
	}

	public function getAchImmDestRoutingNumber() {
		return $this->m_strAchImmDestRoutingNumber;
	}

	public function sqlAchImmDestRoutingNumber() {
		return ( true == isset( $this->m_strAchImmDestRoutingNumber ) ) ? '\'' . addslashes( $this->m_strAchImmDestRoutingNumber ) . '\'' : 'NULL';
	}

	public function setAchImmOrigRoutingNumber( $strAchImmOrigRoutingNumber ) {
		$this->set( 'm_strAchImmOrigRoutingNumber', CStrings::strTrimDef( $strAchImmOrigRoutingNumber, 240, NULL, true ) );
	}

	public function getAchImmOrigRoutingNumber() {
		return $this->m_strAchImmOrigRoutingNumber;
	}

	public function sqlAchImmOrigRoutingNumber() {
		return ( true == isset( $this->m_strAchImmOrigRoutingNumber ) ) ? '\'' . addslashes( $this->m_strAchImmOrigRoutingNumber ) . '\'' : 'NULL';
	}

	public function setAchImmDestName( $strAchImmDestName ) {
		$this->set( 'm_strAchImmDestName', CStrings::strTrimDef( $strAchImmDestName, 50, NULL, true ) );
	}

	public function getAchImmDestName() {
		return $this->m_strAchImmDestName;
	}

	public function sqlAchImmDestName() {
		return ( true == isset( $this->m_strAchImmDestName ) ) ? '\'' . addslashes( $this->m_strAchImmDestName ) . '\'' : 'NULL';
	}

	public function setAchImmOrigName( $strAchImmOrigName ) {
		$this->set( 'm_strAchImmOrigName', CStrings::strTrimDef( $strAchImmOrigName, 50, NULL, true ) );
	}

	public function getAchImmOrigName() {
		return $this->m_strAchImmOrigName;
	}

	public function sqlAchImmOrigName() {
		return ( true == isset( $this->m_strAchImmOrigName ) ) ? '\'' . addslashes( $this->m_strAchImmOrigName ) . '\'' : 'NULL';
	}

	public function setX937ImmDestRoutingNumber( $strX937ImmDestRoutingNumber ) {
		$this->set( 'm_strX937ImmDestRoutingNumber', CStrings::strTrimDef( $strX937ImmDestRoutingNumber, 240, NULL, true ) );
	}

	public function getX937ImmDestRoutingNumber() {
		return $this->m_strX937ImmDestRoutingNumber;
	}

	public function sqlX937ImmDestRoutingNumber() {
		return ( true == isset( $this->m_strX937ImmDestRoutingNumber ) ) ? '\'' . addslashes( $this->m_strX937ImmDestRoutingNumber ) . '\'' : 'NULL';
	}

	public function setX937ImmOrigRoutingNumber( $strX937ImmOrigRoutingNumber ) {
		$this->set( 'm_strX937ImmOrigRoutingNumber', CStrings::strTrimDef( $strX937ImmOrigRoutingNumber, 240, NULL, true ) );
	}

	public function getX937ImmOrigRoutingNumber() {
		return $this->m_strX937ImmOrigRoutingNumber;
	}

	public function sqlX937ImmOrigRoutingNumber() {
		return ( true == isset( $this->m_strX937ImmOrigRoutingNumber ) ) ? '\'' . addslashes( $this->m_strX937ImmOrigRoutingNumber ) . '\'' : 'NULL';
	}

	public function setX937ImmDestName( $strX937ImmDestName ) {
		$this->set( 'm_strX937ImmDestName', CStrings::strTrimDef( $strX937ImmDestName, 50, NULL, true ) );
	}

	public function getX937ImmDestName() {
		return $this->m_strX937ImmDestName;
	}

	public function sqlX937ImmDestName() {
		return ( true == isset( $this->m_strX937ImmDestName ) ) ? '\'' . addslashes( $this->m_strX937ImmDestName ) . '\'' : 'NULL';
	}

	public function setX937ImmOrigName( $strX937ImmOrigName ) {
		$this->set( 'm_strX937ImmOrigName', CStrings::strTrimDef( $strX937ImmOrigName, 50, NULL, true ) );
	}

	public function getX937ImmOrigName() {
		return $this->m_strX937ImmOrigName;
	}

	public function sqlX937ImmOrigName() {
		return ( true == isset( $this->m_strX937ImmOrigName ) ) ? '\'' . addslashes( $this->m_strX937ImmOrigName ) . '\'' : 'NULL';
	}

	public function setX937ImmOrigContactName( $strX937ImmOrigContactName ) {
		$this->set( 'm_strX937ImmOrigContactName', CStrings::strTrimDef( $strX937ImmOrigContactName, 50, NULL, true ) );
	}

	public function getX937ImmOrigContactName() {
		return $this->m_strX937ImmOrigContactName;
	}

	public function sqlX937ImmOrigContactName() {
		return ( true == isset( $this->m_strX937ImmOrigContactName ) ) ? '\'' . addslashes( $this->m_strX937ImmOrigContactName ) . '\'' : 'NULL';
	}

	public function setX937ImmOrigContactPhone( $strX937ImmOrigContactPhone ) {
		$this->set( 'm_strX937ImmOrigContactPhone', CStrings::strTrimDef( $strX937ImmOrigContactPhone, 30, NULL, true ) );
	}

	public function getX937ImmOrigContactPhone() {
		return $this->m_strX937ImmOrigContactPhone;
	}

	public function sqlX937ImmOrigContactPhone() {
		return ( true == isset( $this->m_strX937ImmOrigContactPhone ) ) ? '\'' . addslashes( $this->m_strX937ImmOrigContactPhone ) . '\'' : 'NULL';
	}

	public function setEndrsBankName( $strEndrsBankName ) {
		$this->set( 'm_strEndrsBankName', CStrings::strTrimDef( $strEndrsBankName, 50, NULL, true ) );
	}

	public function getEndrsBankName() {
		return $this->m_strEndrsBankName;
	}

	public function sqlEndrsBankName() {
		return ( true == isset( $this->m_strEndrsBankName ) ) ? '\'' . addslashes( $this->m_strEndrsBankName ) . '\'' : 'NULL';
	}

	public function setEndrsAcctDescription( $strEndrsAcctDescription ) {
		$this->set( 'm_strEndrsAcctDescription', CStrings::strTrimDef( $strEndrsAcctDescription, 240, NULL, true ) );
	}

	public function getEndrsAcctDescription() {
		return $this->m_strEndrsAcctDescription;
	}

	public function sqlEndrsAcctDescription() {
		return ( true == isset( $this->m_strEndrsAcctDescription ) ) ? '\'' . addslashes( $this->m_strEndrsAcctDescription ) . '\'' : 'NULL';
	}

	public function setEndrsRtrnsRoutingNum( $strEndrsRtrnsRoutingNum ) {
		$this->set( 'm_strEndrsRtrnsRoutingNum', CStrings::strTrimDef( $strEndrsRtrnsRoutingNum, 240, NULL, true ) );
	}

	public function getEndrsRtrnsRoutingNum() {
		return $this->m_strEndrsRtrnsRoutingNum;
	}

	public function sqlEndrsRtrnsRoutingNum() {
		return ( true == isset( $this->m_strEndrsRtrnsRoutingNum ) ) ? '\'' . addslashes( $this->m_strEndrsRtrnsRoutingNum ) . '\'' : 'NULL';
	}

	public function setEndrsRtrnsAcctNumEncrypted( $strEndrsRtrnsAcctNumEncrypted ) {
		$this->set( 'm_strEndrsRtrnsAcctNumEncrypted', CStrings::strTrimDef( $strEndrsRtrnsAcctNumEncrypted, 240, NULL, true ) );
	}

	public function getEndrsRtrnsAcctNumEncrypted() {
		return $this->m_strEndrsRtrnsAcctNumEncrypted;
	}

	public function sqlEndrsRtrnsAcctNumEncrypted() {
		return ( true == isset( $this->m_strEndrsRtrnsAcctNumEncrypted ) ) ? '\'' . addslashes( $this->m_strEndrsRtrnsAcctNumEncrypted ) . '\'' : 'NULL';
	}

	public function setIsBalancedAchFiles( $intIsBalancedAchFiles ) {
		$this->set( 'm_intIsBalancedAchFiles', CStrings::strToIntDef( $intIsBalancedAchFiles, NULL, false ) );
	}

	public function getIsBalancedAchFiles() {
		return $this->m_intIsBalancedAchFiles;
	}

	public function sqlIsBalancedAchFiles() {
		return ( true == isset( $this->m_intIsBalancedAchFiles ) ) ? ( string ) $this->m_intIsBalancedAchFiles : '1';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ach_merchant_gateway_id' => $this->getAchMerchantGatewayId(),
			'cc_merchant_gateway_id' => $this->getCcMerchantGatewayId(),
			'x937_merchant_gateway_id' => $this->getX937MerchantGatewayId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'notification_email' => $this->getNotificationEmail(),
			'ach_imm_dest_routing_number' => $this->getAchImmDestRoutingNumber(),
			'ach_imm_orig_routing_number' => $this->getAchImmOrigRoutingNumber(),
			'ach_imm_dest_name' => $this->getAchImmDestName(),
			'ach_imm_orig_name' => $this->getAchImmOrigName(),
			'x937_imm_dest_routing_number' => $this->getX937ImmDestRoutingNumber(),
			'x937_imm_orig_routing_number' => $this->getX937ImmOrigRoutingNumber(),
			'x937_imm_dest_name' => $this->getX937ImmDestName(),
			'x937_imm_orig_name' => $this->getX937ImmOrigName(),
			'x937_imm_orig_contact_name' => $this->getX937ImmOrigContactName(),
			'x937_imm_orig_contact_phone' => $this->getX937ImmOrigContactPhone(),
			'endrs_bank_name' => $this->getEndrsBankName(),
			'endrs_acct_description' => $this->getEndrsAcctDescription(),
			'endrs_rtrns_routing_num' => $this->getEndrsRtrnsRoutingNum(),
			'endrs_rtrns_acct_num_encrypted' => $this->getEndrsRtrnsAcctNumEncrypted(),
			'is_balanced_ach_files' => $this->getIsBalancedAchFiles(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>