<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937ViewDataRecords
 * Do not add any new functions to this class.
 */

class CBaseX937ViewDataRecords extends CEosPluralBase {

	/**
	 * @return CX937ViewDataRecord[]
	 */
	public static function fetchX937ViewDataRecords( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CX937ViewDataRecord::class, $objDatabase );
	}

	/**
	 * @return CX937ViewDataRecord
	 */
	public static function fetchX937ViewDataRecord( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CX937ViewDataRecord::class, $objDatabase );
	}

	public static function fetchX937ViewDataRecordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'x937_view_data_records', $objDatabase );
	}

	public static function fetchX937ViewDataRecordById( $intId, $objDatabase ) {
		return self::fetchX937ViewDataRecord( sprintf( 'SELECT * FROM x937_view_data_records WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchX937ViewDataRecordsByX937CheckDetailRecordId( $intX937CheckDetailRecordId, $objDatabase ) {
		return self::fetchX937ViewDataRecords( sprintf( 'SELECT * FROM x937_view_data_records WHERE x937_check_detail_record_id = %d', ( int ) $intX937CheckDetailRecordId ), $objDatabase );
	}

}
?>