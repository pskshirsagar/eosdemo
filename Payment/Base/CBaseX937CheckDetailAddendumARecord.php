<?php

class CBaseX937CheckDetailAddendumARecord extends CEosSingularBase {

	const TABLE_NAME = 'public.x937_check_detail_addendum_a_records';

	protected $m_intId;
	protected $m_intX937CheckDetailRecordId;
	protected $m_strRecordType;
	protected $m_strCheckDetailAddendumARecordNumber;
	protected $m_strBankOfFirstDepositRoutingNumber;
	protected $m_strBofdBusinessEndorsementDate;
	protected $m_strBofdItemSequenceNumber;
	protected $m_strDepositAccountNumberAtBofd;
	protected $m_strBofdDepositBranch;
	protected $m_strPayeeName;
	protected $m_strTruncationIndicator;
	protected $m_strBofdConversionIndicator;
	protected $m_strBofdCorrectionIndicator;
	protected $m_strUserField;
	protected $m_strReserved;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['x937_check_detail_record_id'] ) && $boolDirectSet ) $this->set( 'm_intX937CheckDetailRecordId', trim( $arrValues['x937_check_detail_record_id'] ) ); elseif( isset( $arrValues['x937_check_detail_record_id'] ) ) $this->setX937CheckDetailRecordId( $arrValues['x937_check_detail_record_id'] );
		if( isset( $arrValues['record_type'] ) && $boolDirectSet ) $this->set( 'm_strRecordType', trim( stripcslashes( $arrValues['record_type'] ) ) ); elseif( isset( $arrValues['record_type'] ) ) $this->setRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_type'] ) : $arrValues['record_type'] );
		if( isset( $arrValues['check_detail_addendum_a_record_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckDetailAddendumARecordNumber', trim( stripcslashes( $arrValues['check_detail_addendum_a_record_number'] ) ) ); elseif( isset( $arrValues['check_detail_addendum_a_record_number'] ) ) $this->setCheckDetailAddendumARecordNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_detail_addendum_a_record_number'] ) : $arrValues['check_detail_addendum_a_record_number'] );
		if( isset( $arrValues['bank_of_first_deposit_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strBankOfFirstDepositRoutingNumber', trim( stripcslashes( $arrValues['bank_of_first_deposit_routing_number'] ) ) ); elseif( isset( $arrValues['bank_of_first_deposit_routing_number'] ) ) $this->setBankOfFirstDepositRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_of_first_deposit_routing_number'] ) : $arrValues['bank_of_first_deposit_routing_number'] );
		if( isset( $arrValues['bofd_business_endorsement_date'] ) && $boolDirectSet ) $this->set( 'm_strBofdBusinessEndorsementDate', trim( stripcslashes( $arrValues['bofd_business_endorsement_date'] ) ) ); elseif( isset( $arrValues['bofd_business_endorsement_date'] ) ) $this->setBofdBusinessEndorsementDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bofd_business_endorsement_date'] ) : $arrValues['bofd_business_endorsement_date'] );
		if( isset( $arrValues['bofd_item_sequence_number'] ) && $boolDirectSet ) $this->set( 'm_strBofdItemSequenceNumber', trim( stripcslashes( $arrValues['bofd_item_sequence_number'] ) ) ); elseif( isset( $arrValues['bofd_item_sequence_number'] ) ) $this->setBofdItemSequenceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bofd_item_sequence_number'] ) : $arrValues['bofd_item_sequence_number'] );
		if( isset( $arrValues['deposit_account_number_at_bofd'] ) && $boolDirectSet ) $this->set( 'm_strDepositAccountNumberAtBofd', trim( stripcslashes( $arrValues['deposit_account_number_at_bofd'] ) ) ); elseif( isset( $arrValues['deposit_account_number_at_bofd'] ) ) $this->setDepositAccountNumberAtBofd( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['deposit_account_number_at_bofd'] ) : $arrValues['deposit_account_number_at_bofd'] );
		if( isset( $arrValues['bofd_deposit_branch'] ) && $boolDirectSet ) $this->set( 'm_strBofdDepositBranch', trim( stripcslashes( $arrValues['bofd_deposit_branch'] ) ) ); elseif( isset( $arrValues['bofd_deposit_branch'] ) ) $this->setBofdDepositBranch( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bofd_deposit_branch'] ) : $arrValues['bofd_deposit_branch'] );
		if( isset( $arrValues['payee_name'] ) && $boolDirectSet ) $this->set( 'm_strPayeeName', trim( stripcslashes( $arrValues['payee_name'] ) ) ); elseif( isset( $arrValues['payee_name'] ) ) $this->setPayeeName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payee_name'] ) : $arrValues['payee_name'] );
		if( isset( $arrValues['truncation_indicator'] ) && $boolDirectSet ) $this->set( 'm_strTruncationIndicator', trim( stripcslashes( $arrValues['truncation_indicator'] ) ) ); elseif( isset( $arrValues['truncation_indicator'] ) ) $this->setTruncationIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['truncation_indicator'] ) : $arrValues['truncation_indicator'] );
		if( isset( $arrValues['bofd_conversion_indicator'] ) && $boolDirectSet ) $this->set( 'm_strBofdConversionIndicator', trim( stripcslashes( $arrValues['bofd_conversion_indicator'] ) ) ); elseif( isset( $arrValues['bofd_conversion_indicator'] ) ) $this->setBofdConversionIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bofd_conversion_indicator'] ) : $arrValues['bofd_conversion_indicator'] );
		if( isset( $arrValues['bofd_correction_indicator'] ) && $boolDirectSet ) $this->set( 'm_strBofdCorrectionIndicator', trim( stripcslashes( $arrValues['bofd_correction_indicator'] ) ) ); elseif( isset( $arrValues['bofd_correction_indicator'] ) ) $this->setBofdCorrectionIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bofd_correction_indicator'] ) : $arrValues['bofd_correction_indicator'] );
		if( isset( $arrValues['user_field'] ) && $boolDirectSet ) $this->set( 'm_strUserField', trim( stripcslashes( $arrValues['user_field'] ) ) ); elseif( isset( $arrValues['user_field'] ) ) $this->setUserField( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['user_field'] ) : $arrValues['user_field'] );
		if( isset( $arrValues['reserved'] ) && $boolDirectSet ) $this->set( 'm_strReserved', trim( stripcslashes( $arrValues['reserved'] ) ) ); elseif( isset( $arrValues['reserved'] ) ) $this->setReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reserved'] ) : $arrValues['reserved'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setX937CheckDetailRecordId( $intX937CheckDetailRecordId ) {
		$this->set( 'm_intX937CheckDetailRecordId', CStrings::strToIntDef( $intX937CheckDetailRecordId, NULL, false ) );
	}

	public function getX937CheckDetailRecordId() {
		return $this->m_intX937CheckDetailRecordId;
	}

	public function sqlX937CheckDetailRecordId() {
		return ( true == isset( $this->m_intX937CheckDetailRecordId ) ) ? ( string ) $this->m_intX937CheckDetailRecordId : 'NULL';
	}

	public function setRecordType( $strRecordType ) {
		$this->set( 'm_strRecordType', CStrings::strTrimDef( $strRecordType, 2, NULL, true ) );
	}

	public function getRecordType() {
		return $this->m_strRecordType;
	}

	public function sqlRecordType() {
		return ( true == isset( $this->m_strRecordType ) ) ? '\'' . addslashes( $this->m_strRecordType ) . '\'' : 'NULL';
	}

	public function setCheckDetailAddendumARecordNumber( $strCheckDetailAddendumARecordNumber ) {
		$this->set( 'm_strCheckDetailAddendumARecordNumber', CStrings::strTrimDef( $strCheckDetailAddendumARecordNumber, 1, NULL, true ) );
	}

	public function getCheckDetailAddendumARecordNumber() {
		return $this->m_strCheckDetailAddendumARecordNumber;
	}

	public function sqlCheckDetailAddendumARecordNumber() {
		return ( true == isset( $this->m_strCheckDetailAddendumARecordNumber ) ) ? '\'' . addslashes( $this->m_strCheckDetailAddendumARecordNumber ) . '\'' : 'NULL';
	}

	public function setBankOfFirstDepositRoutingNumber( $strBankOfFirstDepositRoutingNumber ) {
		$this->set( 'm_strBankOfFirstDepositRoutingNumber', CStrings::strTrimDef( $strBankOfFirstDepositRoutingNumber, 9, NULL, true ) );
	}

	public function getBankOfFirstDepositRoutingNumber() {
		return $this->m_strBankOfFirstDepositRoutingNumber;
	}

	public function sqlBankOfFirstDepositRoutingNumber() {
		return ( true == isset( $this->m_strBankOfFirstDepositRoutingNumber ) ) ? '\'' . addslashes( $this->m_strBankOfFirstDepositRoutingNumber ) . '\'' : 'NULL';
	}

	public function setBofdBusinessEndorsementDate( $strBofdBusinessEndorsementDate ) {
		$this->set( 'm_strBofdBusinessEndorsementDate', CStrings::strTrimDef( $strBofdBusinessEndorsementDate, 8, NULL, true ) );
	}

	public function getBofdBusinessEndorsementDate() {
		return $this->m_strBofdBusinessEndorsementDate;
	}

	public function sqlBofdBusinessEndorsementDate() {
		return ( true == isset( $this->m_strBofdBusinessEndorsementDate ) ) ? '\'' . addslashes( $this->m_strBofdBusinessEndorsementDate ) . '\'' : 'NULL';
	}

	public function setBofdItemSequenceNumber( $strBofdItemSequenceNumber ) {
		$this->set( 'm_strBofdItemSequenceNumber', CStrings::strTrimDef( $strBofdItemSequenceNumber, 15, NULL, true ) );
	}

	public function getBofdItemSequenceNumber() {
		return $this->m_strBofdItemSequenceNumber;
	}

	public function sqlBofdItemSequenceNumber() {
		return ( true == isset( $this->m_strBofdItemSequenceNumber ) ) ? '\'' . addslashes( $this->m_strBofdItemSequenceNumber ) . '\'' : 'NULL';
	}

	public function setDepositAccountNumberAtBofd( $strDepositAccountNumberAtBofd ) {
		$this->set( 'm_strDepositAccountNumberAtBofd', CStrings::strTrimDef( $strDepositAccountNumberAtBofd, 18, NULL, true ) );
	}

	public function getDepositAccountNumberAtBofd() {
		return $this->m_strDepositAccountNumberAtBofd;
	}

	public function sqlDepositAccountNumberAtBofd() {
		return ( true == isset( $this->m_strDepositAccountNumberAtBofd ) ) ? '\'' . addslashes( $this->m_strDepositAccountNumberAtBofd ) . '\'' : 'NULL';
	}

	public function setBofdDepositBranch( $strBofdDepositBranch ) {
		$this->set( 'm_strBofdDepositBranch', CStrings::strTrimDef( $strBofdDepositBranch, 5, NULL, true ) );
	}

	public function getBofdDepositBranch() {
		return $this->m_strBofdDepositBranch;
	}

	public function sqlBofdDepositBranch() {
		return ( true == isset( $this->m_strBofdDepositBranch ) ) ? '\'' . addslashes( $this->m_strBofdDepositBranch ) . '\'' : 'NULL';
	}

	public function setPayeeName( $strPayeeName ) {
		$this->set( 'm_strPayeeName', CStrings::strTrimDef( $strPayeeName, 15, NULL, true ) );
	}

	public function getPayeeName() {
		return $this->m_strPayeeName;
	}

	public function sqlPayeeName() {
		return ( true == isset( $this->m_strPayeeName ) ) ? '\'' . addslashes( $this->m_strPayeeName ) . '\'' : 'NULL';
	}

	public function setTruncationIndicator( $strTruncationIndicator ) {
		$this->set( 'm_strTruncationIndicator', CStrings::strTrimDef( $strTruncationIndicator, 1, NULL, true ) );
	}

	public function getTruncationIndicator() {
		return $this->m_strTruncationIndicator;
	}

	public function sqlTruncationIndicator() {
		return ( true == isset( $this->m_strTruncationIndicator ) ) ? '\'' . addslashes( $this->m_strTruncationIndicator ) . '\'' : 'NULL';
	}

	public function setBofdConversionIndicator( $strBofdConversionIndicator ) {
		$this->set( 'm_strBofdConversionIndicator', CStrings::strTrimDef( $strBofdConversionIndicator, 1, NULL, true ) );
	}

	public function getBofdConversionIndicator() {
		return $this->m_strBofdConversionIndicator;
	}

	public function sqlBofdConversionIndicator() {
		return ( true == isset( $this->m_strBofdConversionIndicator ) ) ? '\'' . addslashes( $this->m_strBofdConversionIndicator ) . '\'' : 'NULL';
	}

	public function setBofdCorrectionIndicator( $strBofdCorrectionIndicator ) {
		$this->set( 'm_strBofdCorrectionIndicator', CStrings::strTrimDef( $strBofdCorrectionIndicator, 1, NULL, true ) );
	}

	public function getBofdCorrectionIndicator() {
		return $this->m_strBofdCorrectionIndicator;
	}

	public function sqlBofdCorrectionIndicator() {
		return ( true == isset( $this->m_strBofdCorrectionIndicator ) ) ? '\'' . addslashes( $this->m_strBofdCorrectionIndicator ) . '\'' : 'NULL';
	}

	public function setUserField( $strUserField ) {
		$this->set( 'm_strUserField', CStrings::strTrimDef( $strUserField, 1, NULL, true ) );
	}

	public function getUserField() {
		return $this->m_strUserField;
	}

	public function sqlUserField() {
		return ( true == isset( $this->m_strUserField ) ) ? '\'' . addslashes( $this->m_strUserField ) . '\'' : 'NULL';
	}

	public function setReserved( $strReserved ) {
		$this->set( 'm_strReserved', CStrings::strTrimDef( $strReserved, 3, NULL, true ) );
	}

	public function getReserved() {
		return $this->m_strReserved;
	}

	public function sqlReserved() {
		return ( true == isset( $this->m_strReserved ) ) ? '\'' . addslashes( $this->m_strReserved ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, x937_check_detail_record_id, record_type, check_detail_addendum_a_record_number, bank_of_first_deposit_routing_number, bofd_business_endorsement_date, bofd_item_sequence_number, deposit_account_number_at_bofd, bofd_deposit_branch, payee_name, truncation_indicator, bofd_conversion_indicator, bofd_correction_indicator, user_field, reserved, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlX937CheckDetailRecordId() . ', ' .
 						$this->sqlRecordType() . ', ' .
 						$this->sqlCheckDetailAddendumARecordNumber() . ', ' .
 						$this->sqlBankOfFirstDepositRoutingNumber() . ', ' .
 						$this->sqlBofdBusinessEndorsementDate() . ', ' .
 						$this->sqlBofdItemSequenceNumber() . ', ' .
 						$this->sqlDepositAccountNumberAtBofd() . ', ' .
 						$this->sqlBofdDepositBranch() . ', ' .
 						$this->sqlPayeeName() . ', ' .
 						$this->sqlTruncationIndicator() . ', ' .
 						$this->sqlBofdConversionIndicator() . ', ' .
 						$this->sqlBofdCorrectionIndicator() . ', ' .
 						$this->sqlUserField() . ', ' .
 						$this->sqlReserved() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_check_detail_record_id = ' . $this->sqlX937CheckDetailRecordId() . ','; } elseif( true == array_key_exists( 'X937CheckDetailRecordId', $this->getChangedColumns() ) ) { $strSql .= ' x937_check_detail_record_id = ' . $this->sqlX937CheckDetailRecordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; } elseif( true == array_key_exists( 'RecordType', $this->getChangedColumns() ) ) { $strSql .= ' record_type = ' . $this->sqlRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_detail_addendum_a_record_number = ' . $this->sqlCheckDetailAddendumARecordNumber() . ','; } elseif( true == array_key_exists( 'CheckDetailAddendumARecordNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_detail_addendum_a_record_number = ' . $this->sqlCheckDetailAddendumARecordNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_of_first_deposit_routing_number = ' . $this->sqlBankOfFirstDepositRoutingNumber() . ','; } elseif( true == array_key_exists( 'BankOfFirstDepositRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' bank_of_first_deposit_routing_number = ' . $this->sqlBankOfFirstDepositRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bofd_business_endorsement_date = ' . $this->sqlBofdBusinessEndorsementDate() . ','; } elseif( true == array_key_exists( 'BofdBusinessEndorsementDate', $this->getChangedColumns() ) ) { $strSql .= ' bofd_business_endorsement_date = ' . $this->sqlBofdBusinessEndorsementDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bofd_item_sequence_number = ' . $this->sqlBofdItemSequenceNumber() . ','; } elseif( true == array_key_exists( 'BofdItemSequenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' bofd_item_sequence_number = ' . $this->sqlBofdItemSequenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_account_number_at_bofd = ' . $this->sqlDepositAccountNumberAtBofd() . ','; } elseif( true == array_key_exists( 'DepositAccountNumberAtBofd', $this->getChangedColumns() ) ) { $strSql .= ' deposit_account_number_at_bofd = ' . $this->sqlDepositAccountNumberAtBofd() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bofd_deposit_branch = ' . $this->sqlBofdDepositBranch() . ','; } elseif( true == array_key_exists( 'BofdDepositBranch', $this->getChangedColumns() ) ) { $strSql .= ' bofd_deposit_branch = ' . $this->sqlBofdDepositBranch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payee_name = ' . $this->sqlPayeeName() . ','; } elseif( true == array_key_exists( 'PayeeName', $this->getChangedColumns() ) ) { $strSql .= ' payee_name = ' . $this->sqlPayeeName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' truncation_indicator = ' . $this->sqlTruncationIndicator() . ','; } elseif( true == array_key_exists( 'TruncationIndicator', $this->getChangedColumns() ) ) { $strSql .= ' truncation_indicator = ' . $this->sqlTruncationIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bofd_conversion_indicator = ' . $this->sqlBofdConversionIndicator() . ','; } elseif( true == array_key_exists( 'BofdConversionIndicator', $this->getChangedColumns() ) ) { $strSql .= ' bofd_conversion_indicator = ' . $this->sqlBofdConversionIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bofd_correction_indicator = ' . $this->sqlBofdCorrectionIndicator() . ','; } elseif( true == array_key_exists( 'BofdCorrectionIndicator', $this->getChangedColumns() ) ) { $strSql .= ' bofd_correction_indicator = ' . $this->sqlBofdCorrectionIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_field = ' . $this->sqlUserField() . ','; } elseif( true == array_key_exists( 'UserField', $this->getChangedColumns() ) ) { $strSql .= ' user_field = ' . $this->sqlUserField() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reserved = ' . $this->sqlReserved() . ','; } elseif( true == array_key_exists( 'Reserved', $this->getChangedColumns() ) ) { $strSql .= ' reserved = ' . $this->sqlReserved() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'x937_check_detail_record_id' => $this->getX937CheckDetailRecordId(),
			'record_type' => $this->getRecordType(),
			'check_detail_addendum_a_record_number' => $this->getCheckDetailAddendumARecordNumber(),
			'bank_of_first_deposit_routing_number' => $this->getBankOfFirstDepositRoutingNumber(),
			'bofd_business_endorsement_date' => $this->getBofdBusinessEndorsementDate(),
			'bofd_item_sequence_number' => $this->getBofdItemSequenceNumber(),
			'deposit_account_number_at_bofd' => $this->getDepositAccountNumberAtBofd(),
			'bofd_deposit_branch' => $this->getBofdDepositBranch(),
			'payee_name' => $this->getPayeeName(),
			'truncation_indicator' => $this->getTruncationIndicator(),
			'bofd_conversion_indicator' => $this->getBofdConversionIndicator(),
			'bofd_correction_indicator' => $this->getBofdCorrectionIndicator(),
			'user_field' => $this->getUserField(),
			'reserved' => $this->getReserved(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>