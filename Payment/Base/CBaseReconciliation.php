<?php

class CBaseReconciliation extends CEosSingularBase {

	const TABLE_NAME = 'public.reconciliations';

	protected $m_intId;
	protected $m_intProcessingBankAccountId;
	protected $m_strMemo;
	protected $m_strFromDate;
	protected $m_strToDate;
	protected $m_fltBeginningBalance;
	protected $m_fltEndingBalance;
	protected $m_fltAdjustmentAmount;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intIsPending;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltAdjustmentAmount = '0';
		$this->m_intIsPending = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankAccountId', trim( $arrValues['processing_bank_account_id'] ) ); elseif( isset( $arrValues['processing_bank_account_id'] ) ) $this->setProcessingBankAccountId( $arrValues['processing_bank_account_id'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( stripcslashes( $arrValues['memo'] ) ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['memo'] ) : $arrValues['memo'] );
		if( isset( $arrValues['from_date'] ) && $boolDirectSet ) $this->set( 'm_strFromDate', trim( $arrValues['from_date'] ) ); elseif( isset( $arrValues['from_date'] ) ) $this->setFromDate( $arrValues['from_date'] );
		if( isset( $arrValues['to_date'] ) && $boolDirectSet ) $this->set( 'm_strToDate', trim( $arrValues['to_date'] ) ); elseif( isset( $arrValues['to_date'] ) ) $this->setToDate( $arrValues['to_date'] );
		if( isset( $arrValues['beginning_balance'] ) && $boolDirectSet ) $this->set( 'm_fltBeginningBalance', trim( $arrValues['beginning_balance'] ) ); elseif( isset( $arrValues['beginning_balance'] ) ) $this->setBeginningBalance( $arrValues['beginning_balance'] );
		if( isset( $arrValues['ending_balance'] ) && $boolDirectSet ) $this->set( 'm_fltEndingBalance', trim( $arrValues['ending_balance'] ) ); elseif( isset( $arrValues['ending_balance'] ) ) $this->setEndingBalance( $arrValues['ending_balance'] );
		if( isset( $arrValues['adjustment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdjustmentAmount', trim( $arrValues['adjustment_amount'] ) ); elseif( isset( $arrValues['adjustment_amount'] ) ) $this->setAdjustmentAmount( $arrValues['adjustment_amount'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['is_pending'] ) && $boolDirectSet ) $this->set( 'm_intIsPending', trim( $arrValues['is_pending'] ) ); elseif( isset( $arrValues['is_pending'] ) ) $this->setIsPending( $arrValues['is_pending'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
		$this->set( 'm_intProcessingBankAccountId', CStrings::strToIntDef( $intProcessingBankAccountId, NULL, false ) );
	}

	public function getProcessingBankAccountId() {
		return $this->m_intProcessingBankAccountId;
	}

	public function sqlProcessingBankAccountId() {
		return ( true == isset( $this->m_intProcessingBankAccountId ) ) ? ( string ) $this->m_intProcessingBankAccountId : 'NULL';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, -1, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? '\'' . addslashes( $this->m_strMemo ) . '\'' : 'NULL';
	}

	public function setFromDate( $strFromDate ) {
		$this->set( 'm_strFromDate', CStrings::strTrimDef( $strFromDate, -1, NULL, true ) );
	}

	public function getFromDate() {
		return $this->m_strFromDate;
	}

	public function sqlFromDate() {
		return ( true == isset( $this->m_strFromDate ) ) ? '\'' . $this->m_strFromDate . '\'' : 'NOW()';
	}

	public function setToDate( $strToDate ) {
		$this->set( 'm_strToDate', CStrings::strTrimDef( $strToDate, -1, NULL, true ) );
	}

	public function getToDate() {
		return $this->m_strToDate;
	}

	public function sqlToDate() {
		return ( true == isset( $this->m_strToDate ) ) ? '\'' . $this->m_strToDate . '\'' : 'NOW()';
	}

	public function setBeginningBalance( $fltBeginningBalance ) {
		$this->set( 'm_fltBeginningBalance', CStrings::strToFloatDef( $fltBeginningBalance, NULL, false, 2 ) );
	}

	public function getBeginningBalance() {
		return $this->m_fltBeginningBalance;
	}

	public function sqlBeginningBalance() {
		return ( true == isset( $this->m_fltBeginningBalance ) ) ? ( string ) $this->m_fltBeginningBalance : 'NULL';
	}

	public function setEndingBalance( $fltEndingBalance ) {
		$this->set( 'm_fltEndingBalance', CStrings::strToFloatDef( $fltEndingBalance, NULL, false, 2 ) );
	}

	public function getEndingBalance() {
		return $this->m_fltEndingBalance;
	}

	public function sqlEndingBalance() {
		return ( true == isset( $this->m_fltEndingBalance ) ) ? ( string ) $this->m_fltEndingBalance : 'NULL';
	}

	public function setAdjustmentAmount( $fltAdjustmentAmount ) {
		$this->set( 'm_fltAdjustmentAmount', CStrings::strToFloatDef( $fltAdjustmentAmount, NULL, false, 2 ) );
	}

	public function getAdjustmentAmount() {
		return $this->m_fltAdjustmentAmount;
	}

	public function sqlAdjustmentAmount() {
		return ( true == isset( $this->m_fltAdjustmentAmount ) ) ? ( string ) $this->m_fltAdjustmentAmount : '0';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setIsPending( $intIsPending ) {
		$this->set( 'm_intIsPending', CStrings::strToIntDef( $intIsPending, NULL, false ) );
	}

	public function getIsPending() {
		return $this->m_intIsPending;
	}

	public function sqlIsPending() {
		return ( true == isset( $this->m_intIsPending ) ) ? ( string ) $this->m_intIsPending : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, processing_bank_account_id, memo, from_date, to_date, beginning_balance, ending_balance, adjustment_amount, approved_by, approved_on, is_pending, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlProcessingBankAccountId() . ', ' .
 						$this->sqlMemo() . ', ' .
 						$this->sqlFromDate() . ', ' .
 						$this->sqlToDate() . ', ' .
 						$this->sqlBeginningBalance() . ', ' .
 						$this->sqlEndingBalance() . ', ' .
 						$this->sqlAdjustmentAmount() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlIsPending() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; } elseif( true == array_key_exists( 'ProcessingBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' from_date = ' . $this->sqlFromDate() . ','; } elseif( true == array_key_exists( 'FromDate', $this->getChangedColumns() ) ) { $strSql .= ' from_date = ' . $this->sqlFromDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' to_date = ' . $this->sqlToDate() . ','; } elseif( true == array_key_exists( 'ToDate', $this->getChangedColumns() ) ) { $strSql .= ' to_date = ' . $this->sqlToDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' beginning_balance = ' . $this->sqlBeginningBalance() . ','; } elseif( true == array_key_exists( 'BeginningBalance', $this->getChangedColumns() ) ) { $strSql .= ' beginning_balance = ' . $this->sqlBeginningBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ending_balance = ' . $this->sqlEndingBalance() . ','; } elseif( true == array_key_exists( 'EndingBalance', $this->getChangedColumns() ) ) { $strSql .= ' ending_balance = ' . $this->sqlEndingBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adjustment_amount = ' . $this->sqlAdjustmentAmount() . ','; } elseif( true == array_key_exists( 'AdjustmentAmount', $this->getChangedColumns() ) ) { $strSql .= ' adjustment_amount = ' . $this->sqlAdjustmentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_pending = ' . $this->sqlIsPending() . ','; } elseif( true == array_key_exists( 'IsPending', $this->getChangedColumns() ) ) { $strSql .= ' is_pending = ' . $this->sqlIsPending() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'processing_bank_account_id' => $this->getProcessingBankAccountId(),
			'memo' => $this->getMemo(),
			'from_date' => $this->getFromDate(),
			'to_date' => $this->getToDate(),
			'beginning_balance' => $this->getBeginningBalance(),
			'ending_balance' => $this->getEndingBalance(),
			'adjustment_amount' => $this->getAdjustmentAmount(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'is_pending' => $this->getIsPending(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>