<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CUnderwritingTypes
 * Do not add any new functions to this class.
 */

class CBaseUnderwritingTypes extends CEosPluralBase {

	/**
	 * @return CUnderwritingType[]
	 */
	public static function fetchUnderwritingTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CUnderwritingType::class, $objDatabase );
	}

	/**
	 * @return CUnderwritingType
	 */
	public static function fetchUnderwritingType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CUnderwritingType::class, $objDatabase );
	}

	public static function fetchUnderwritingTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'underwriting_types', $objDatabase );
	}

	public static function fetchUnderwritingTypeById( $intId, $objDatabase ) {
		return self::fetchUnderwritingType( sprintf( 'SELECT * FROM underwriting_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>