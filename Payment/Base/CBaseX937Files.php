<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937Files
 * Do not add any new functions to this class.
 */

class CBaseX937Files extends CEosPluralBase {

	/**
	 * @return CX937File[]
	 */
	public static function fetchX937Files( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CX937File::class, $objDatabase );
	}

	/**
	 * @return CX937File
	 */
	public static function fetchX937File( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CX937File::class, $objDatabase );
	}

	public static function fetchX937FileCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'x937_files', $objDatabase );
	}

	public static function fetchX937FileById( $intId, $objDatabase ) {
		return self::fetchX937File( sprintf( 'SELECT * FROM x937_files WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchX937FilesByX937FileTypeId( $intX937FileTypeId, $objDatabase ) {
		return self::fetchX937Files( sprintf( 'SELECT * FROM x937_files WHERE x937_file_type_id = %d', ( int ) $intX937FileTypeId ), $objDatabase );
	}

	public static function fetchX937FilesByX937FileStatusTypeId( $intX937FileStatusTypeId, $objDatabase ) {
		return self::fetchX937Files( sprintf( 'SELECT * FROM x937_files WHERE x937_file_status_type_id = %d', ( int ) $intX937FileStatusTypeId ), $objDatabase );
	}

	public static function fetchX937FilesByProcessingBankId( $intProcessingBankId, $objDatabase ) {
		return self::fetchX937Files( sprintf( 'SELECT * FROM x937_files WHERE processing_bank_id = %d', ( int ) $intProcessingBankId ), $objDatabase );
	}

	public static function fetchX937FilesByMerchantGatewayId( $intMerchantGatewayId, $objDatabase ) {
		return self::fetchX937Files( sprintf( 'SELECT * FROM x937_files WHERE merchant_gateway_id = %d', ( int ) $intMerchantGatewayId ), $objDatabase );
	}

}
?>