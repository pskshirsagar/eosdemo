<?php

class CBaseNachaReturnAddendaDetailRecord extends CEosSingularBase {

	const TABLE_NAME = 'public.nacha_return_addenda_detail_records';

	protected $m_intId;
	protected $m_intNachaReturnDetailRecordId;
	protected $m_intEntryDetailRecordTypeId;
	protected $m_intNachaFileId;
	protected $m_intNachaFileBatchId;
	protected $m_intClearingBatchId;
	protected $m_intNachaEntryDetailRecordId;
	protected $m_intReturnTypeId;
	protected $m_intCid;
	protected $m_intArPaymentId;
	protected $m_intSettlementDistributionId;
	protected $m_intCompanyPaymentId;
	protected $m_intEftInstructionId;
	protected $m_intPaymentBlacklistEntryId;
	protected $m_strRecordTypeCode;
	protected $m_strTypeCode;
	protected $m_strReturnReasonCode;
	protected $m_strOriginalEntryTraceNumber;
	protected $m_strDateOfDeath;
	protected $m_strReceivingDfiIdentification;
	protected $m_strInformation;
	protected $m_strCorrectedData;
	protected $m_strCorrectedDataEncrypted;
	protected $m_strReserved;
	protected $m_strTraceNumber;
	protected $m_intReturnFeeAttemptCount;
	protected $m_intRequiresReturnFee;
	protected $m_intIsNoticeOfChange;
	protected $m_intIsReconPrepped;
	protected $m_strReturnProcessedOn;
	protected $m_strReturnsAccountTransferredOn;
	protected $m_strLastReturnFeeAttemptOn;
	protected $m_strLastReturnFeeFailedOn;
	protected $m_strChangedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intReturnFeeAttemptCount = '0';
		$this->m_intRequiresReturnFee = '0';
		$this->m_intIsNoticeOfChange = '0';
		$this->m_intIsReconPrepped = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['nacha_return_detail_record_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaReturnDetailRecordId', trim( $arrValues['nacha_return_detail_record_id'] ) ); elseif( isset( $arrValues['nacha_return_detail_record_id'] ) ) $this->setNachaReturnDetailRecordId( $arrValues['nacha_return_detail_record_id'] );
		if( isset( $arrValues['entry_detail_record_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEntryDetailRecordTypeId', trim( $arrValues['entry_detail_record_type_id'] ) ); elseif( isset( $arrValues['entry_detail_record_type_id'] ) ) $this->setEntryDetailRecordTypeId( $arrValues['entry_detail_record_type_id'] );
		if( isset( $arrValues['nacha_file_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaFileId', trim( $arrValues['nacha_file_id'] ) ); elseif( isset( $arrValues['nacha_file_id'] ) ) $this->setNachaFileId( $arrValues['nacha_file_id'] );
		if( isset( $arrValues['nacha_file_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaFileBatchId', trim( $arrValues['nacha_file_batch_id'] ) ); elseif( isset( $arrValues['nacha_file_batch_id'] ) ) $this->setNachaFileBatchId( $arrValues['nacha_file_batch_id'] );
		if( isset( $arrValues['clearing_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intClearingBatchId', trim( $arrValues['clearing_batch_id'] ) ); elseif( isset( $arrValues['clearing_batch_id'] ) ) $this->setClearingBatchId( $arrValues['clearing_batch_id'] );
		if( isset( $arrValues['nacha_entry_detail_record_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaEntryDetailRecordId', trim( $arrValues['nacha_entry_detail_record_id'] ) ); elseif( isset( $arrValues['nacha_entry_detail_record_id'] ) ) $this->setNachaEntryDetailRecordId( $arrValues['nacha_entry_detail_record_id'] );
		if( isset( $arrValues['return_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReturnTypeId', trim( $arrValues['return_type_id'] ) ); elseif( isset( $arrValues['return_type_id'] ) ) $this->setReturnTypeId( $arrValues['return_type_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['settlement_distribution_id'] ) && $boolDirectSet ) $this->set( 'm_intSettlementDistributionId', trim( $arrValues['settlement_distribution_id'] ) ); elseif( isset( $arrValues['settlement_distribution_id'] ) ) $this->setSettlementDistributionId( $arrValues['settlement_distribution_id'] );
		if( isset( $arrValues['company_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyPaymentId', trim( $arrValues['company_payment_id'] ) ); elseif( isset( $arrValues['company_payment_id'] ) ) $this->setCompanyPaymentId( $arrValues['company_payment_id'] );
		if( isset( $arrValues['eft_instruction_id'] ) && $boolDirectSet ) $this->set( 'm_intEftInstructionId', trim( $arrValues['eft_instruction_id'] ) ); elseif( isset( $arrValues['eft_instruction_id'] ) ) $this->setEftInstructionId( $arrValues['eft_instruction_id'] );
		if( isset( $arrValues['payment_blacklist_entry_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentBlacklistEntryId', trim( $arrValues['payment_blacklist_entry_id'] ) ); elseif( isset( $arrValues['payment_blacklist_entry_id'] ) ) $this->setPaymentBlacklistEntryId( $arrValues['payment_blacklist_entry_id'] );
		if( isset( $arrValues['record_type_code'] ) && $boolDirectSet ) $this->set( 'm_strRecordTypeCode', trim( stripcslashes( $arrValues['record_type_code'] ) ) ); elseif( isset( $arrValues['record_type_code'] ) ) $this->setRecordTypeCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_type_code'] ) : $arrValues['record_type_code'] );
		if( isset( $arrValues['type_code'] ) && $boolDirectSet ) $this->set( 'm_strTypeCode', trim( stripcslashes( $arrValues['type_code'] ) ) ); elseif( isset( $arrValues['type_code'] ) ) $this->setTypeCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['type_code'] ) : $arrValues['type_code'] );
		if( isset( $arrValues['return_reason_code'] ) && $boolDirectSet ) $this->set( 'm_strReturnReasonCode', trim( stripcslashes( $arrValues['return_reason_code'] ) ) ); elseif( isset( $arrValues['return_reason_code'] ) ) $this->setReturnReasonCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['return_reason_code'] ) : $arrValues['return_reason_code'] );
		if( isset( $arrValues['original_entry_trace_number'] ) && $boolDirectSet ) $this->set( 'm_strOriginalEntryTraceNumber', trim( stripcslashes( $arrValues['original_entry_trace_number'] ) ) ); elseif( isset( $arrValues['original_entry_trace_number'] ) ) $this->setOriginalEntryTraceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['original_entry_trace_number'] ) : $arrValues['original_entry_trace_number'] );
		if( isset( $arrValues['date_of_death'] ) && $boolDirectSet ) $this->set( 'm_strDateOfDeath', trim( stripcslashes( $arrValues['date_of_death'] ) ) ); elseif( isset( $arrValues['date_of_death'] ) ) $this->setDateOfDeath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['date_of_death'] ) : $arrValues['date_of_death'] );
		if( isset( $arrValues['receiving_dfi_identification'] ) && $boolDirectSet ) $this->set( 'm_strReceivingDfiIdentification', trim( stripcslashes( $arrValues['receiving_dfi_identification'] ) ) ); elseif( isset( $arrValues['receiving_dfi_identification'] ) ) $this->setReceivingDfiIdentification( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['receiving_dfi_identification'] ) : $arrValues['receiving_dfi_identification'] );
		if( isset( $arrValues['information'] ) && $boolDirectSet ) $this->set( 'm_strInformation', trim( stripcslashes( $arrValues['information'] ) ) ); elseif( isset( $arrValues['information'] ) ) $this->setInformation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['information'] ) : $arrValues['information'] );
		if( isset( $arrValues['corrected_data'] ) && $boolDirectSet ) $this->set( 'm_strCorrectedData', trim( stripcslashes( $arrValues['corrected_data'] ) ) ); elseif( isset( $arrValues['corrected_data'] ) ) $this->setCorrectedData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['corrected_data'] ) : $arrValues['corrected_data'] );
		if( isset( $arrValues['corrected_data_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCorrectedDataEncrypted', trim( stripcslashes( $arrValues['corrected_data_encrypted'] ) ) ); elseif( isset( $arrValues['corrected_data_encrypted'] ) ) $this->setCorrectedDataEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['corrected_data_encrypted'] ) : $arrValues['corrected_data_encrypted'] );
		if( isset( $arrValues['reserved'] ) && $boolDirectSet ) $this->set( 'm_strReserved', trim( stripcslashes( $arrValues['reserved'] ) ) ); elseif( isset( $arrValues['reserved'] ) ) $this->setReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reserved'] ) : $arrValues['reserved'] );
		if( isset( $arrValues['trace_number'] ) && $boolDirectSet ) $this->set( 'm_strTraceNumber', trim( stripcslashes( $arrValues['trace_number'] ) ) ); elseif( isset( $arrValues['trace_number'] ) ) $this->setTraceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['trace_number'] ) : $arrValues['trace_number'] );
		if( isset( $arrValues['return_fee_attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intReturnFeeAttemptCount', trim( $arrValues['return_fee_attempt_count'] ) ); elseif( isset( $arrValues['return_fee_attempt_count'] ) ) $this->setReturnFeeAttemptCount( $arrValues['return_fee_attempt_count'] );
		if( isset( $arrValues['requires_return_fee'] ) && $boolDirectSet ) $this->set( 'm_intRequiresReturnFee', trim( $arrValues['requires_return_fee'] ) ); elseif( isset( $arrValues['requires_return_fee'] ) ) $this->setRequiresReturnFee( $arrValues['requires_return_fee'] );
		if( isset( $arrValues['is_notice_of_change'] ) && $boolDirectSet ) $this->set( 'm_intIsNoticeOfChange', trim( $arrValues['is_notice_of_change'] ) ); elseif( isset( $arrValues['is_notice_of_change'] ) ) $this->setIsNoticeOfChange( $arrValues['is_notice_of_change'] );
		if( isset( $arrValues['is_recon_prepped'] ) && $boolDirectSet ) $this->set( 'm_intIsReconPrepped', trim( $arrValues['is_recon_prepped'] ) ); elseif( isset( $arrValues['is_recon_prepped'] ) ) $this->setIsReconPrepped( $arrValues['is_recon_prepped'] );
		if( isset( $arrValues['return_processed_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnProcessedOn', trim( $arrValues['return_processed_on'] ) ); elseif( isset( $arrValues['return_processed_on'] ) ) $this->setReturnProcessedOn( $arrValues['return_processed_on'] );
		if( isset( $arrValues['returns_account_transferred_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnsAccountTransferredOn', trim( $arrValues['returns_account_transferred_on'] ) ); elseif( isset( $arrValues['returns_account_transferred_on'] ) ) $this->setReturnsAccountTransferredOn( $arrValues['returns_account_transferred_on'] );
		if( isset( $arrValues['last_return_fee_attempt_on'] ) && $boolDirectSet ) $this->set( 'm_strLastReturnFeeAttemptOn', trim( $arrValues['last_return_fee_attempt_on'] ) ); elseif( isset( $arrValues['last_return_fee_attempt_on'] ) ) $this->setLastReturnFeeAttemptOn( $arrValues['last_return_fee_attempt_on'] );
		if( isset( $arrValues['last_return_fee_failed_on'] ) && $boolDirectSet ) $this->set( 'm_strLastReturnFeeFailedOn', trim( $arrValues['last_return_fee_failed_on'] ) ); elseif( isset( $arrValues['last_return_fee_failed_on'] ) ) $this->setLastReturnFeeFailedOn( $arrValues['last_return_fee_failed_on'] );
		if( isset( $arrValues['changed_on'] ) && $boolDirectSet ) $this->set( 'm_strChangedOn', trim( $arrValues['changed_on'] ) ); elseif( isset( $arrValues['changed_on'] ) ) $this->setChangedOn( $arrValues['changed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setNachaReturnDetailRecordId( $intNachaReturnDetailRecordId ) {
		$this->set( 'm_intNachaReturnDetailRecordId', CStrings::strToIntDef( $intNachaReturnDetailRecordId, NULL, false ) );
	}

	public function getNachaReturnDetailRecordId() {
		return $this->m_intNachaReturnDetailRecordId;
	}

	public function sqlNachaReturnDetailRecordId() {
		return ( true == isset( $this->m_intNachaReturnDetailRecordId ) ) ? ( string ) $this->m_intNachaReturnDetailRecordId : 'NULL';
	}

	public function setEntryDetailRecordTypeId( $intEntryDetailRecordTypeId ) {
		$this->set( 'm_intEntryDetailRecordTypeId', CStrings::strToIntDef( $intEntryDetailRecordTypeId, NULL, false ) );
	}

	public function getEntryDetailRecordTypeId() {
		return $this->m_intEntryDetailRecordTypeId;
	}

	public function sqlEntryDetailRecordTypeId() {
		return ( true == isset( $this->m_intEntryDetailRecordTypeId ) ) ? ( string ) $this->m_intEntryDetailRecordTypeId : 'NULL';
	}

	public function setNachaFileId( $intNachaFileId ) {
		$this->set( 'm_intNachaFileId', CStrings::strToIntDef( $intNachaFileId, NULL, false ) );
	}

	public function getNachaFileId() {
		return $this->m_intNachaFileId;
	}

	public function sqlNachaFileId() {
		return ( true == isset( $this->m_intNachaFileId ) ) ? ( string ) $this->m_intNachaFileId : 'NULL';
	}

	public function setNachaFileBatchId( $intNachaFileBatchId ) {
		$this->set( 'm_intNachaFileBatchId', CStrings::strToIntDef( $intNachaFileBatchId, NULL, false ) );
	}

	public function getNachaFileBatchId() {
		return $this->m_intNachaFileBatchId;
	}

	public function sqlNachaFileBatchId() {
		return ( true == isset( $this->m_intNachaFileBatchId ) ) ? ( string ) $this->m_intNachaFileBatchId : 'NULL';
	}

	public function setClearingBatchId( $intClearingBatchId ) {
		$this->set( 'm_intClearingBatchId', CStrings::strToIntDef( $intClearingBatchId, NULL, false ) );
	}

	public function getClearingBatchId() {
		return $this->m_intClearingBatchId;
	}

	public function sqlClearingBatchId() {
		return ( true == isset( $this->m_intClearingBatchId ) ) ? ( string ) $this->m_intClearingBatchId : 'NULL';
	}

	public function setNachaEntryDetailRecordId( $intNachaEntryDetailRecordId ) {
		$this->set( 'm_intNachaEntryDetailRecordId', CStrings::strToIntDef( $intNachaEntryDetailRecordId, NULL, false ) );
	}

	public function getNachaEntryDetailRecordId() {
		return $this->m_intNachaEntryDetailRecordId;
	}

	public function sqlNachaEntryDetailRecordId() {
		return ( true == isset( $this->m_intNachaEntryDetailRecordId ) ) ? ( string ) $this->m_intNachaEntryDetailRecordId : 'NULL';
	}

	public function setReturnTypeId( $intReturnTypeId ) {
		$this->set( 'm_intReturnTypeId', CStrings::strToIntDef( $intReturnTypeId, NULL, false ) );
	}

	public function getReturnTypeId() {
		return $this->m_intReturnTypeId;
	}

	public function sqlReturnTypeId() {
		return ( true == isset( $this->m_intReturnTypeId ) ) ? ( string ) $this->m_intReturnTypeId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setSettlementDistributionId( $intSettlementDistributionId ) {
		$this->set( 'm_intSettlementDistributionId', CStrings::strToIntDef( $intSettlementDistributionId, NULL, false ) );
	}

	public function getSettlementDistributionId() {
		return $this->m_intSettlementDistributionId;
	}

	public function sqlSettlementDistributionId() {
		return ( true == isset( $this->m_intSettlementDistributionId ) ) ? ( string ) $this->m_intSettlementDistributionId : 'NULL';
	}

	public function setCompanyPaymentId( $intCompanyPaymentId ) {
		$this->set( 'm_intCompanyPaymentId', CStrings::strToIntDef( $intCompanyPaymentId, NULL, false ) );
	}

	public function getCompanyPaymentId() {
		return $this->m_intCompanyPaymentId;
	}

	public function sqlCompanyPaymentId() {
		return ( true == isset( $this->m_intCompanyPaymentId ) ) ? ( string ) $this->m_intCompanyPaymentId : 'NULL';
	}

	public function setEftInstructionId( $intEftInstructionId ) {
		$this->set( 'm_intEftInstructionId', CStrings::strToIntDef( $intEftInstructionId, NULL, false ) );
	}

	public function getEftInstructionId() {
		return $this->m_intEftInstructionId;
	}

	public function sqlEftInstructionId() {
		return ( true == isset( $this->m_intEftInstructionId ) ) ? ( string ) $this->m_intEftInstructionId : 'NULL';
	}

	public function setPaymentBlacklistEntryId( $intPaymentBlacklistEntryId ) {
		$this->set( 'm_intPaymentBlacklistEntryId', CStrings::strToIntDef( $intPaymentBlacklistEntryId, NULL, false ) );
	}

	public function getPaymentBlacklistEntryId() {
		return $this->m_intPaymentBlacklistEntryId;
	}

	public function sqlPaymentBlacklistEntryId() {
		return ( true == isset( $this->m_intPaymentBlacklistEntryId ) ) ? ( string ) $this->m_intPaymentBlacklistEntryId : 'NULL';
	}

	public function setRecordTypeCode( $strRecordTypeCode ) {
		$this->set( 'm_strRecordTypeCode', CStrings::strTrimDef( $strRecordTypeCode, 1, NULL, true ) );
	}

	public function getRecordTypeCode() {
		return $this->m_strRecordTypeCode;
	}

	public function sqlRecordTypeCode() {
		return ( true == isset( $this->m_strRecordTypeCode ) ) ? '\'' . addslashes( $this->m_strRecordTypeCode ) . '\'' : 'NULL';
	}

	public function setTypeCode( $strTypeCode ) {
		$this->set( 'm_strTypeCode', CStrings::strTrimDef( $strTypeCode, 2, NULL, true ) );
	}

	public function getTypeCode() {
		return $this->m_strTypeCode;
	}

	public function sqlTypeCode() {
		return ( true == isset( $this->m_strTypeCode ) ) ? '\'' . addslashes( $this->m_strTypeCode ) . '\'' : 'NULL';
	}

	public function setReturnReasonCode( $strReturnReasonCode ) {
		$this->set( 'm_strReturnReasonCode', CStrings::strTrimDef( $strReturnReasonCode, 3, NULL, true ) );
	}

	public function getReturnReasonCode() {
		return $this->m_strReturnReasonCode;
	}

	public function sqlReturnReasonCode() {
		return ( true == isset( $this->m_strReturnReasonCode ) ) ? '\'' . addslashes( $this->m_strReturnReasonCode ) . '\'' : 'NULL';
	}

	public function setOriginalEntryTraceNumber( $strOriginalEntryTraceNumber ) {
		$this->set( 'm_strOriginalEntryTraceNumber', CStrings::strTrimDef( $strOriginalEntryTraceNumber, 15, NULL, true ) );
	}

	public function getOriginalEntryTraceNumber() {
		return $this->m_strOriginalEntryTraceNumber;
	}

	public function sqlOriginalEntryTraceNumber() {
		return ( true == isset( $this->m_strOriginalEntryTraceNumber ) ) ? '\'' . addslashes( $this->m_strOriginalEntryTraceNumber ) . '\'' : 'NULL';
	}

	public function setDateOfDeath( $strDateOfDeath ) {
		$this->set( 'm_strDateOfDeath', CStrings::strTrimDef( $strDateOfDeath, 6, NULL, true ) );
	}

	public function getDateOfDeath() {
		return $this->m_strDateOfDeath;
	}

	public function sqlDateOfDeath() {
		return ( true == isset( $this->m_strDateOfDeath ) ) ? '\'' . addslashes( $this->m_strDateOfDeath ) . '\'' : 'NULL';
	}

	public function setReceivingDfiIdentification( $strReceivingDfiIdentification ) {
		$this->set( 'm_strReceivingDfiIdentification', CStrings::strTrimDef( $strReceivingDfiIdentification, 8, NULL, true ) );
	}

	public function getReceivingDfiIdentification() {
		return $this->m_strReceivingDfiIdentification;
	}

	public function sqlReceivingDfiIdentification() {
		return ( true == isset( $this->m_strReceivingDfiIdentification ) ) ? '\'' . addslashes( $this->m_strReceivingDfiIdentification ) . '\'' : 'NULL';
	}

	public function setInformation( $strInformation ) {
		$this->set( 'm_strInformation', CStrings::strTrimDef( $strInformation, 44, NULL, true ) );
	}

	public function getInformation() {
		return $this->m_strInformation;
	}

	public function sqlInformation() {
		return ( true == isset( $this->m_strInformation ) ) ? '\'' . addslashes( $this->m_strInformation ) . '\'' : 'NULL';
	}

	public function setCorrectedData( $strCorrectedData ) {
		$this->set( 'm_strCorrectedData', CStrings::strTrimDef( $strCorrectedData, 29, NULL, true ) );
	}

	public function getCorrectedData() {
		return $this->m_strCorrectedData;
	}

	public function sqlCorrectedData() {
		return ( true == isset( $this->m_strCorrectedData ) ) ? '\'' . addslashes( $this->m_strCorrectedData ) . '\'' : 'NULL';
	}

	public function setCorrectedDataEncrypted( $strCorrectedDataEncrypted ) {
		$this->set( 'm_strCorrectedDataEncrypted', CStrings::strTrimDef( $strCorrectedDataEncrypted, 255, NULL, true ) );
	}

	public function getCorrectedDataEncrypted() {
		return $this->m_strCorrectedDataEncrypted;
	}

	public function sqlCorrectedDataEncrypted() {
		return ( true == isset( $this->m_strCorrectedDataEncrypted ) ) ? '\'' . addslashes( $this->m_strCorrectedDataEncrypted ) . '\'' : 'NULL';
	}

	public function setReserved( $strReserved ) {
		$this->set( 'm_strReserved', CStrings::strTrimDef( $strReserved, 15, NULL, true ) );
	}

	public function getReserved() {
		return $this->m_strReserved;
	}

	public function sqlReserved() {
		return ( true == isset( $this->m_strReserved ) ) ? '\'' . addslashes( $this->m_strReserved ) . '\'' : 'NULL';
	}

	public function setTraceNumber( $strTraceNumber ) {
		$this->set( 'm_strTraceNumber', CStrings::strTrimDef( $strTraceNumber, 15, NULL, true ) );
	}

	public function getTraceNumber() {
		return $this->m_strTraceNumber;
	}

	public function sqlTraceNumber() {
		return ( true == isset( $this->m_strTraceNumber ) ) ? '\'' . addslashes( $this->m_strTraceNumber ) . '\'' : 'NULL';
	}

	public function setReturnFeeAttemptCount( $intReturnFeeAttemptCount ) {
		$this->set( 'm_intReturnFeeAttemptCount', CStrings::strToIntDef( $intReturnFeeAttemptCount, NULL, false ) );
	}

	public function getReturnFeeAttemptCount() {
		return $this->m_intReturnFeeAttemptCount;
	}

	public function sqlReturnFeeAttemptCount() {
		return ( true == isset( $this->m_intReturnFeeAttemptCount ) ) ? ( string ) $this->m_intReturnFeeAttemptCount : '0';
	}

	public function setRequiresReturnFee( $intRequiresReturnFee ) {
		$this->set( 'm_intRequiresReturnFee', CStrings::strToIntDef( $intRequiresReturnFee, NULL, false ) );
	}

	public function getRequiresReturnFee() {
		return $this->m_intRequiresReturnFee;
	}

	public function sqlRequiresReturnFee() {
		return ( true == isset( $this->m_intRequiresReturnFee ) ) ? ( string ) $this->m_intRequiresReturnFee : '0';
	}

	public function setIsNoticeOfChange( $intIsNoticeOfChange ) {
		$this->set( 'm_intIsNoticeOfChange', CStrings::strToIntDef( $intIsNoticeOfChange, NULL, false ) );
	}

	public function getIsNoticeOfChange() {
		return $this->m_intIsNoticeOfChange;
	}

	public function sqlIsNoticeOfChange() {
		return ( true == isset( $this->m_intIsNoticeOfChange ) ) ? ( string ) $this->m_intIsNoticeOfChange : '0';
	}

	public function setIsReconPrepped( $intIsReconPrepped ) {
		$this->set( 'm_intIsReconPrepped', CStrings::strToIntDef( $intIsReconPrepped, NULL, false ) );
	}

	public function getIsReconPrepped() {
		return $this->m_intIsReconPrepped;
	}

	public function sqlIsReconPrepped() {
		return ( true == isset( $this->m_intIsReconPrepped ) ) ? ( string ) $this->m_intIsReconPrepped : '0';
	}

	public function setReturnProcessedOn( $strReturnProcessedOn ) {
		$this->set( 'm_strReturnProcessedOn', CStrings::strTrimDef( $strReturnProcessedOn, -1, NULL, true ) );
	}

	public function getReturnProcessedOn() {
		return $this->m_strReturnProcessedOn;
	}

	public function sqlReturnProcessedOn() {
		return ( true == isset( $this->m_strReturnProcessedOn ) ) ? '\'' . $this->m_strReturnProcessedOn . '\'' : 'NULL';
	}

	public function setReturnsAccountTransferredOn( $strReturnsAccountTransferredOn ) {
		$this->set( 'm_strReturnsAccountTransferredOn', CStrings::strTrimDef( $strReturnsAccountTransferredOn, -1, NULL, true ) );
	}

	public function getReturnsAccountTransferredOn() {
		return $this->m_strReturnsAccountTransferredOn;
	}

	public function sqlReturnsAccountTransferredOn() {
		return ( true == isset( $this->m_strReturnsAccountTransferredOn ) ) ? '\'' . $this->m_strReturnsAccountTransferredOn . '\'' : 'NULL';
	}

	public function setLastReturnFeeAttemptOn( $strLastReturnFeeAttemptOn ) {
		$this->set( 'm_strLastReturnFeeAttemptOn', CStrings::strTrimDef( $strLastReturnFeeAttemptOn, -1, NULL, true ) );
	}

	public function getLastReturnFeeAttemptOn() {
		return $this->m_strLastReturnFeeAttemptOn;
	}

	public function sqlLastReturnFeeAttemptOn() {
		return ( true == isset( $this->m_strLastReturnFeeAttemptOn ) ) ? '\'' . $this->m_strLastReturnFeeAttemptOn . '\'' : 'NULL';
	}

	public function setLastReturnFeeFailedOn( $strLastReturnFeeFailedOn ) {
		$this->set( 'm_strLastReturnFeeFailedOn', CStrings::strTrimDef( $strLastReturnFeeFailedOn, -1, NULL, true ) );
	}

	public function getLastReturnFeeFailedOn() {
		return $this->m_strLastReturnFeeFailedOn;
	}

	public function sqlLastReturnFeeFailedOn() {
		return ( true == isset( $this->m_strLastReturnFeeFailedOn ) ) ? '\'' . $this->m_strLastReturnFeeFailedOn . '\'' : 'NULL';
	}

	public function setChangedOn( $strChangedOn ) {
		$this->set( 'm_strChangedOn', CStrings::strTrimDef( $strChangedOn, -1, NULL, true ) );
	}

	public function getChangedOn() {
		return $this->m_strChangedOn;
	}

	public function sqlChangedOn() {
		return ( true == isset( $this->m_strChangedOn ) ) ? '\'' . $this->m_strChangedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, nacha_return_detail_record_id, entry_detail_record_type_id, nacha_file_id, nacha_file_batch_id, clearing_batch_id, nacha_entry_detail_record_id, return_type_id, cid, ar_payment_id, settlement_distribution_id, company_payment_id, eft_instruction_id, payment_blacklist_entry_id, record_type_code, type_code, return_reason_code, original_entry_trace_number, date_of_death, receiving_dfi_identification, information, corrected_data, corrected_data_encrypted, reserved, trace_number, return_fee_attempt_count, requires_return_fee, is_notice_of_change, is_recon_prepped, return_processed_on, returns_account_transferred_on, last_return_fee_attempt_on, last_return_fee_failed_on, changed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlNachaReturnDetailRecordId() . ', ' .
 						$this->sqlEntryDetailRecordTypeId() . ', ' .
 						$this->sqlNachaFileId() . ', ' .
 						$this->sqlNachaFileBatchId() . ', ' .
 						$this->sqlClearingBatchId() . ', ' .
 						$this->sqlNachaEntryDetailRecordId() . ', ' .
 						$this->sqlReturnTypeId() . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlArPaymentId() . ', ' .
 						$this->sqlSettlementDistributionId() . ', ' .
 						$this->sqlCompanyPaymentId() . ', ' .
 						$this->sqlEftInstructionId() . ', ' .
 						$this->sqlPaymentBlacklistEntryId() . ', ' .
 						$this->sqlRecordTypeCode() . ', ' .
 						$this->sqlTypeCode() . ', ' .
 						$this->sqlReturnReasonCode() . ', ' .
 						$this->sqlOriginalEntryTraceNumber() . ', ' .
 						$this->sqlDateOfDeath() . ', ' .
 						$this->sqlReceivingDfiIdentification() . ', ' .
 						$this->sqlInformation() . ', ' .
 						$this->sqlCorrectedData() . ', ' .
 						$this->sqlCorrectedDataEncrypted() . ', ' .
 						$this->sqlReserved() . ', ' .
 						$this->sqlTraceNumber() . ', ' .
 						$this->sqlReturnFeeAttemptCount() . ', ' .
 						$this->sqlRequiresReturnFee() . ', ' .
 						$this->sqlIsNoticeOfChange() . ', ' .
 						$this->sqlIsReconPrepped() . ', ' .
 						$this->sqlReturnProcessedOn() . ', ' .
 						$this->sqlReturnsAccountTransferredOn() . ', ' .
 						$this->sqlLastReturnFeeAttemptOn() . ', ' .
 						$this->sqlLastReturnFeeFailedOn() . ', ' .
 						$this->sqlChangedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_return_detail_record_id = ' . $this->sqlNachaReturnDetailRecordId() . ','; } elseif( true == array_key_exists( 'NachaReturnDetailRecordId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_return_detail_record_id = ' . $this->sqlNachaReturnDetailRecordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entry_detail_record_type_id = ' . $this->sqlEntryDetailRecordTypeId() . ','; } elseif( true == array_key_exists( 'EntryDetailRecordTypeId', $this->getChangedColumns() ) ) { $strSql .= ' entry_detail_record_type_id = ' . $this->sqlEntryDetailRecordTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_file_id = ' . $this->sqlNachaFileId() . ','; } elseif( true == array_key_exists( 'NachaFileId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_file_id = ' . $this->sqlNachaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_file_batch_id = ' . $this->sqlNachaFileBatchId() . ','; } elseif( true == array_key_exists( 'NachaFileBatchId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_file_batch_id = ' . $this->sqlNachaFileBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clearing_batch_id = ' . $this->sqlClearingBatchId() . ','; } elseif( true == array_key_exists( 'ClearingBatchId', $this->getChangedColumns() ) ) { $strSql .= ' clearing_batch_id = ' . $this->sqlClearingBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_entry_detail_record_id = ' . $this->sqlNachaEntryDetailRecordId() . ','; } elseif( true == array_key_exists( 'NachaEntryDetailRecordId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_entry_detail_record_id = ' . $this->sqlNachaEntryDetailRecordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; } elseif( true == array_key_exists( 'ReturnTypeId', $this->getChangedColumns() ) ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId() . ','; } elseif( true == array_key_exists( 'SettlementDistributionId', $this->getChangedColumns() ) ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId() . ','; } elseif( true == array_key_exists( 'CompanyPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eft_instruction_id = ' . $this->sqlEftInstructionId() . ','; } elseif( true == array_key_exists( 'EftInstructionId', $this->getChangedColumns() ) ) { $strSql .= ' eft_instruction_id = ' . $this->sqlEftInstructionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_blacklist_entry_id = ' . $this->sqlPaymentBlacklistEntryId() . ','; } elseif( true == array_key_exists( 'PaymentBlacklistEntryId', $this->getChangedColumns() ) ) { $strSql .= ' payment_blacklist_entry_id = ' . $this->sqlPaymentBlacklistEntryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_type_code = ' . $this->sqlRecordTypeCode() . ','; } elseif( true == array_key_exists( 'RecordTypeCode', $this->getChangedColumns() ) ) { $strSql .= ' record_type_code = ' . $this->sqlRecordTypeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' type_code = ' . $this->sqlTypeCode() . ','; } elseif( true == array_key_exists( 'TypeCode', $this->getChangedColumns() ) ) { $strSql .= ' type_code = ' . $this->sqlTypeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_reason_code = ' . $this->sqlReturnReasonCode() . ','; } elseif( true == array_key_exists( 'ReturnReasonCode', $this->getChangedColumns() ) ) { $strSql .= ' return_reason_code = ' . $this->sqlReturnReasonCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_entry_trace_number = ' . $this->sqlOriginalEntryTraceNumber() . ','; } elseif( true == array_key_exists( 'OriginalEntryTraceNumber', $this->getChangedColumns() ) ) { $strSql .= ' original_entry_trace_number = ' . $this->sqlOriginalEntryTraceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_of_death = ' . $this->sqlDateOfDeath() . ','; } elseif( true == array_key_exists( 'DateOfDeath', $this->getChangedColumns() ) ) { $strSql .= ' date_of_death = ' . $this->sqlDateOfDeath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' receiving_dfi_identification = ' . $this->sqlReceivingDfiIdentification() . ','; } elseif( true == array_key_exists( 'ReceivingDfiIdentification', $this->getChangedColumns() ) ) { $strSql .= ' receiving_dfi_identification = ' . $this->sqlReceivingDfiIdentification() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' information = ' . $this->sqlInformation() . ','; } elseif( true == array_key_exists( 'Information', $this->getChangedColumns() ) ) { $strSql .= ' information = ' . $this->sqlInformation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' corrected_data = ' . $this->sqlCorrectedData() . ','; } elseif( true == array_key_exists( 'CorrectedData', $this->getChangedColumns() ) ) { $strSql .= ' corrected_data = ' . $this->sqlCorrectedData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' corrected_data_encrypted = ' . $this->sqlCorrectedDataEncrypted() . ','; } elseif( true == array_key_exists( 'CorrectedDataEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' corrected_data_encrypted = ' . $this->sqlCorrectedDataEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reserved = ' . $this->sqlReserved() . ','; } elseif( true == array_key_exists( 'Reserved', $this->getChangedColumns() ) ) { $strSql .= ' reserved = ' . $this->sqlReserved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trace_number = ' . $this->sqlTraceNumber() . ','; } elseif( true == array_key_exists( 'TraceNumber', $this->getChangedColumns() ) ) { $strSql .= ' trace_number = ' . $this->sqlTraceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_fee_attempt_count = ' . $this->sqlReturnFeeAttemptCount() . ','; } elseif( true == array_key_exists( 'ReturnFeeAttemptCount', $this->getChangedColumns() ) ) { $strSql .= ' return_fee_attempt_count = ' . $this->sqlReturnFeeAttemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_return_fee = ' . $this->sqlRequiresReturnFee() . ','; } elseif( true == array_key_exists( 'RequiresReturnFee', $this->getChangedColumns() ) ) { $strSql .= ' requires_return_fee = ' . $this->sqlRequiresReturnFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_notice_of_change = ' . $this->sqlIsNoticeOfChange() . ','; } elseif( true == array_key_exists( 'IsNoticeOfChange', $this->getChangedColumns() ) ) { $strSql .= ' is_notice_of_change = ' . $this->sqlIsNoticeOfChange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; } elseif( true == array_key_exists( 'IsReconPrepped', $this->getChangedColumns() ) ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_processed_on = ' . $this->sqlReturnProcessedOn() . ','; } elseif( true == array_key_exists( 'ReturnProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' return_processed_on = ' . $this->sqlReturnProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returns_account_transferred_on = ' . $this->sqlReturnsAccountTransferredOn() . ','; } elseif( true == array_key_exists( 'ReturnsAccountTransferredOn', $this->getChangedColumns() ) ) { $strSql .= ' returns_account_transferred_on = ' . $this->sqlReturnsAccountTransferredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_return_fee_attempt_on = ' . $this->sqlLastReturnFeeAttemptOn() . ','; } elseif( true == array_key_exists( 'LastReturnFeeAttemptOn', $this->getChangedColumns() ) ) { $strSql .= ' last_return_fee_attempt_on = ' . $this->sqlLastReturnFeeAttemptOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_return_fee_failed_on = ' . $this->sqlLastReturnFeeFailedOn() . ','; } elseif( true == array_key_exists( 'LastReturnFeeFailedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_return_fee_failed_on = ' . $this->sqlLastReturnFeeFailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' changed_on = ' . $this->sqlChangedOn() . ','; } elseif( true == array_key_exists( 'ChangedOn', $this->getChangedColumns() ) ) { $strSql .= ' changed_on = ' . $this->sqlChangedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'nacha_return_detail_record_id' => $this->getNachaReturnDetailRecordId(),
			'entry_detail_record_type_id' => $this->getEntryDetailRecordTypeId(),
			'nacha_file_id' => $this->getNachaFileId(),
			'nacha_file_batch_id' => $this->getNachaFileBatchId(),
			'clearing_batch_id' => $this->getClearingBatchId(),
			'nacha_entry_detail_record_id' => $this->getNachaEntryDetailRecordId(),
			'return_type_id' => $this->getReturnTypeId(),
			'cid' => $this->getCid(),
			'ar_payment_id' => $this->getArPaymentId(),
			'settlement_distribution_id' => $this->getSettlementDistributionId(),
			'company_payment_id' => $this->getCompanyPaymentId(),
			'eft_instruction_id' => $this->getEftInstructionId(),
			'payment_blacklist_entry_id' => $this->getPaymentBlacklistEntryId(),
			'record_type_code' => $this->getRecordTypeCode(),
			'type_code' => $this->getTypeCode(),
			'return_reason_code' => $this->getReturnReasonCode(),
			'original_entry_trace_number' => $this->getOriginalEntryTraceNumber(),
			'date_of_death' => $this->getDateOfDeath(),
			'receiving_dfi_identification' => $this->getReceivingDfiIdentification(),
			'information' => $this->getInformation(),
			'corrected_data' => $this->getCorrectedData(),
			'corrected_data_encrypted' => $this->getCorrectedDataEncrypted(),
			'reserved' => $this->getReserved(),
			'trace_number' => $this->getTraceNumber(),
			'return_fee_attempt_count' => $this->getReturnFeeAttemptCount(),
			'requires_return_fee' => $this->getRequiresReturnFee(),
			'is_notice_of_change' => $this->getIsNoticeOfChange(),
			'is_recon_prepped' => $this->getIsReconPrepped(),
			'return_processed_on' => $this->getReturnProcessedOn(),
			'returns_account_transferred_on' => $this->getReturnsAccountTransferredOn(),
			'last_return_fee_attempt_on' => $this->getLastReturnFeeAttemptOn(),
			'last_return_fee_failed_on' => $this->getLastReturnFeeFailedOn(),
			'changed_on' => $this->getChangedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>