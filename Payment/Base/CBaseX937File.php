<?php

class CBaseX937File extends CEosSingularBase {

	const TABLE_NAME = 'public.x937_files';

	protected $m_intId;
	protected $m_intX937FileTypeId;
	protected $m_intX937FileStatusTypeId;
	protected $m_intProcessingBankId;
	protected $m_intMerchantGatewayId;
	protected $m_strHeaderRecordType;
	protected $m_strHeaderStandardLevel;
	protected $m_strHeaderTestFileIndicator;
	protected $m_strHeaderImmediateDestinationRoutingNumber;
	protected $m_strHeaderImmediateOriginRoutingNumber;
	protected $m_strHeaderFileCreationDate;
	protected $m_strHeaderFileCreationTime;
	protected $m_strHeaderResendIndicator;
	protected $m_strHeaderImmediateDestinationName;
	protected $m_strHeaderImmediateOrginName;
	protected $m_strHeaderFileIdModifier;
	protected $m_strHeaderCountryCode;
	protected $m_strHeaderUserField;
	protected $m_strHeaderReserved;
	protected $m_strControlRecordType;
	protected $m_strControlCashLetterCount;
	protected $m_strControlTotalRecordCount;
	protected $m_strControlTotalItemCount;
	protected $m_strControlFileTotalAmount;
	protected $m_strControlImmediateOriginContactName;
	protected $m_strControlImmediateOriginContactPhoneNumber;
	protected $m_strControlReserved;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_intIsReconPrepped;
	protected $m_strFileDatetime;
	protected $m_strTransmittedOn;
	protected $m_strConfirmedOn;
	protected $m_strTransferredToIntermediaryOn;
	protected $m_strTransferredForReturnsOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsReconPrepped = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['x937_file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intX937FileTypeId', trim( $arrValues['x937_file_type_id'] ) ); elseif( isset( $arrValues['x937_file_type_id'] ) ) $this->setX937FileTypeId( $arrValues['x937_file_type_id'] );
		if( isset( $arrValues['x937_file_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intX937FileStatusTypeId', trim( $arrValues['x937_file_status_type_id'] ) ); elseif( isset( $arrValues['x937_file_status_type_id'] ) ) $this->setX937FileStatusTypeId( $arrValues['x937_file_status_type_id'] );
		if( isset( $arrValues['processing_bank_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankId', trim( $arrValues['processing_bank_id'] ) ); elseif( isset( $arrValues['processing_bank_id'] ) ) $this->setProcessingBankId( $arrValues['processing_bank_id'] );
		if( isset( $arrValues['merchant_gateway_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantGatewayId', trim( $arrValues['merchant_gateway_id'] ) ); elseif( isset( $arrValues['merchant_gateway_id'] ) ) $this->setMerchantGatewayId( $arrValues['merchant_gateway_id'] );
		if( isset( $arrValues['header_record_type'] ) && $boolDirectSet ) $this->set( 'm_strHeaderRecordType', trim( stripcslashes( $arrValues['header_record_type'] ) ) ); elseif( isset( $arrValues['header_record_type'] ) ) $this->setHeaderRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_record_type'] ) : $arrValues['header_record_type'] );
		if( isset( $arrValues['header_standard_level'] ) && $boolDirectSet ) $this->set( 'm_strHeaderStandardLevel', trim( stripcslashes( $arrValues['header_standard_level'] ) ) ); elseif( isset( $arrValues['header_standard_level'] ) ) $this->setHeaderStandardLevel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_standard_level'] ) : $arrValues['header_standard_level'] );
		if( isset( $arrValues['header_test_file_indicator'] ) && $boolDirectSet ) $this->set( 'm_strHeaderTestFileIndicator', trim( stripcslashes( $arrValues['header_test_file_indicator'] ) ) ); elseif( isset( $arrValues['header_test_file_indicator'] ) ) $this->setHeaderTestFileIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_test_file_indicator'] ) : $arrValues['header_test_file_indicator'] );
		if( isset( $arrValues['header_immediate_destination_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strHeaderImmediateDestinationRoutingNumber', trim( stripcslashes( $arrValues['header_immediate_destination_routing_number'] ) ) ); elseif( isset( $arrValues['header_immediate_destination_routing_number'] ) ) $this->setHeaderImmediateDestinationRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_immediate_destination_routing_number'] ) : $arrValues['header_immediate_destination_routing_number'] );
		if( isset( $arrValues['header_immediate_origin_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strHeaderImmediateOriginRoutingNumber', trim( stripcslashes( $arrValues['header_immediate_origin_routing_number'] ) ) ); elseif( isset( $arrValues['header_immediate_origin_routing_number'] ) ) $this->setHeaderImmediateOriginRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_immediate_origin_routing_number'] ) : $arrValues['header_immediate_origin_routing_number'] );
		if( isset( $arrValues['header_file_creation_date'] ) && $boolDirectSet ) $this->set( 'm_strHeaderFileCreationDate', trim( stripcslashes( $arrValues['header_file_creation_date'] ) ) ); elseif( isset( $arrValues['header_file_creation_date'] ) ) $this->setHeaderFileCreationDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_file_creation_date'] ) : $arrValues['header_file_creation_date'] );
		if( isset( $arrValues['header_file_creation_time'] ) && $boolDirectSet ) $this->set( 'm_strHeaderFileCreationTime', trim( stripcslashes( $arrValues['header_file_creation_time'] ) ) ); elseif( isset( $arrValues['header_file_creation_time'] ) ) $this->setHeaderFileCreationTime( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_file_creation_time'] ) : $arrValues['header_file_creation_time'] );
		if( isset( $arrValues['header_resend_indicator'] ) && $boolDirectSet ) $this->set( 'm_strHeaderResendIndicator', trim( stripcslashes( $arrValues['header_resend_indicator'] ) ) ); elseif( isset( $arrValues['header_resend_indicator'] ) ) $this->setHeaderResendIndicator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_resend_indicator'] ) : $arrValues['header_resend_indicator'] );
		if( isset( $arrValues['header_immediate_destination_name'] ) && $boolDirectSet ) $this->set( 'm_strHeaderImmediateDestinationName', trim( stripcslashes( $arrValues['header_immediate_destination_name'] ) ) ); elseif( isset( $arrValues['header_immediate_destination_name'] ) ) $this->setHeaderImmediateDestinationName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_immediate_destination_name'] ) : $arrValues['header_immediate_destination_name'] );
		if( isset( $arrValues['header_immediate_orgin_name'] ) && $boolDirectSet ) $this->set( 'm_strHeaderImmediateOrginName', trim( stripcslashes( $arrValues['header_immediate_orgin_name'] ) ) ); elseif( isset( $arrValues['header_immediate_orgin_name'] ) ) $this->setHeaderImmediateOrginName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_immediate_orgin_name'] ) : $arrValues['header_immediate_orgin_name'] );
		if( isset( $arrValues['header_file_id_modifier'] ) && $boolDirectSet ) $this->set( 'm_strHeaderFileIdModifier', trim( stripcslashes( $arrValues['header_file_id_modifier'] ) ) ); elseif( isset( $arrValues['header_file_id_modifier'] ) ) $this->setHeaderFileIdModifier( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_file_id_modifier'] ) : $arrValues['header_file_id_modifier'] );
		if( isset( $arrValues['header_country_code'] ) && $boolDirectSet ) $this->set( 'm_strHeaderCountryCode', trim( stripcslashes( $arrValues['header_country_code'] ) ) ); elseif( isset( $arrValues['header_country_code'] ) ) $this->setHeaderCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_country_code'] ) : $arrValues['header_country_code'] );
		if( isset( $arrValues['header_user_field'] ) && $boolDirectSet ) $this->set( 'm_strHeaderUserField', trim( stripcslashes( $arrValues['header_user_field'] ) ) ); elseif( isset( $arrValues['header_user_field'] ) ) $this->setHeaderUserField( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_user_field'] ) : $arrValues['header_user_field'] );
		if( isset( $arrValues['header_reserved'] ) && $boolDirectSet ) $this->set( 'm_strHeaderReserved', trim( stripcslashes( $arrValues['header_reserved'] ) ) ); elseif( isset( $arrValues['header_reserved'] ) ) $this->setHeaderReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_reserved'] ) : $arrValues['header_reserved'] );
		if( isset( $arrValues['control_record_type'] ) && $boolDirectSet ) $this->set( 'm_strControlRecordType', trim( stripcslashes( $arrValues['control_record_type'] ) ) ); elseif( isset( $arrValues['control_record_type'] ) ) $this->setControlRecordType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_record_type'] ) : $arrValues['control_record_type'] );
		if( isset( $arrValues['control_cash_letter_count'] ) && $boolDirectSet ) $this->set( 'm_strControlCashLetterCount', trim( stripcslashes( $arrValues['control_cash_letter_count'] ) ) ); elseif( isset( $arrValues['control_cash_letter_count'] ) ) $this->setControlCashLetterCount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_cash_letter_count'] ) : $arrValues['control_cash_letter_count'] );
		if( isset( $arrValues['control_total_record_count'] ) && $boolDirectSet ) $this->set( 'm_strControlTotalRecordCount', trim( stripcslashes( $arrValues['control_total_record_count'] ) ) ); elseif( isset( $arrValues['control_total_record_count'] ) ) $this->setControlTotalRecordCount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_total_record_count'] ) : $arrValues['control_total_record_count'] );
		if( isset( $arrValues['control_total_item_count'] ) && $boolDirectSet ) $this->set( 'm_strControlTotalItemCount', trim( stripcslashes( $arrValues['control_total_item_count'] ) ) ); elseif( isset( $arrValues['control_total_item_count'] ) ) $this->setControlTotalItemCount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_total_item_count'] ) : $arrValues['control_total_item_count'] );
		if( isset( $arrValues['control_file_total_amount'] ) && $boolDirectSet ) $this->set( 'm_strControlFileTotalAmount', trim( stripcslashes( $arrValues['control_file_total_amount'] ) ) ); elseif( isset( $arrValues['control_file_total_amount'] ) ) $this->setControlFileTotalAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_file_total_amount'] ) : $arrValues['control_file_total_amount'] );
		if( isset( $arrValues['control_immediate_origin_contact_name'] ) && $boolDirectSet ) $this->set( 'm_strControlImmediateOriginContactName', trim( stripcslashes( $arrValues['control_immediate_origin_contact_name'] ) ) ); elseif( isset( $arrValues['control_immediate_origin_contact_name'] ) ) $this->setControlImmediateOriginContactName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_immediate_origin_contact_name'] ) : $arrValues['control_immediate_origin_contact_name'] );
		if( isset( $arrValues['control_immediate_origin_contact_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strControlImmediateOriginContactPhoneNumber', trim( stripcslashes( $arrValues['control_immediate_origin_contact_phone_number'] ) ) ); elseif( isset( $arrValues['control_immediate_origin_contact_phone_number'] ) ) $this->setControlImmediateOriginContactPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_immediate_origin_contact_phone_number'] ) : $arrValues['control_immediate_origin_contact_phone_number'] );
		if( isset( $arrValues['control_reserved'] ) && $boolDirectSet ) $this->set( 'm_strControlReserved', trim( stripcslashes( $arrValues['control_reserved'] ) ) ); elseif( isset( $arrValues['control_reserved'] ) ) $this->setControlReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_reserved'] ) : $arrValues['control_reserved'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['is_recon_prepped'] ) && $boolDirectSet ) $this->set( 'm_intIsReconPrepped', trim( $arrValues['is_recon_prepped'] ) ); elseif( isset( $arrValues['is_recon_prepped'] ) ) $this->setIsReconPrepped( $arrValues['is_recon_prepped'] );
		if( isset( $arrValues['file_datetime'] ) && $boolDirectSet ) $this->set( 'm_strFileDatetime', trim( $arrValues['file_datetime'] ) ); elseif( isset( $arrValues['file_datetime'] ) ) $this->setFileDatetime( $arrValues['file_datetime'] );
		if( isset( $arrValues['transmitted_on'] ) && $boolDirectSet ) $this->set( 'm_strTransmittedOn', trim( $arrValues['transmitted_on'] ) ); elseif( isset( $arrValues['transmitted_on'] ) ) $this->setTransmittedOn( $arrValues['transmitted_on'] );
		if( isset( $arrValues['confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strConfirmedOn', trim( $arrValues['confirmed_on'] ) ); elseif( isset( $arrValues['confirmed_on'] ) ) $this->setConfirmedOn( $arrValues['confirmed_on'] );
		if( isset( $arrValues['transferred_to_intermediary_on'] ) && $boolDirectSet ) $this->set( 'm_strTransferredToIntermediaryOn', trim( $arrValues['transferred_to_intermediary_on'] ) ); elseif( isset( $arrValues['transferred_to_intermediary_on'] ) ) $this->setTransferredToIntermediaryOn( $arrValues['transferred_to_intermediary_on'] );
		if( isset( $arrValues['transferred_for_returns_on'] ) && $boolDirectSet ) $this->set( 'm_strTransferredForReturnsOn', trim( $arrValues['transferred_for_returns_on'] ) ); elseif( isset( $arrValues['transferred_for_returns_on'] ) ) $this->setTransferredForReturnsOn( $arrValues['transferred_for_returns_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setX937FileTypeId( $intX937FileTypeId ) {
		$this->set( 'm_intX937FileTypeId', CStrings::strToIntDef( $intX937FileTypeId, NULL, false ) );
	}

	public function getX937FileTypeId() {
		return $this->m_intX937FileTypeId;
	}

	public function sqlX937FileTypeId() {
		return ( true == isset( $this->m_intX937FileTypeId ) ) ? ( string ) $this->m_intX937FileTypeId : 'NULL';
	}

	public function setX937FileStatusTypeId( $intX937FileStatusTypeId ) {
		$this->set( 'm_intX937FileStatusTypeId', CStrings::strToIntDef( $intX937FileStatusTypeId, NULL, false ) );
	}

	public function getX937FileStatusTypeId() {
		return $this->m_intX937FileStatusTypeId;
	}

	public function sqlX937FileStatusTypeId() {
		return ( true == isset( $this->m_intX937FileStatusTypeId ) ) ? ( string ) $this->m_intX937FileStatusTypeId : 'NULL';
	}

	public function setProcessingBankId( $intProcessingBankId ) {
		$this->set( 'm_intProcessingBankId', CStrings::strToIntDef( $intProcessingBankId, NULL, false ) );
	}

	public function getProcessingBankId() {
		return $this->m_intProcessingBankId;
	}

	public function sqlProcessingBankId() {
		return ( true == isset( $this->m_intProcessingBankId ) ) ? ( string ) $this->m_intProcessingBankId : 'NULL';
	}

	public function setMerchantGatewayId( $intMerchantGatewayId ) {
		$this->set( 'm_intMerchantGatewayId', CStrings::strToIntDef( $intMerchantGatewayId, NULL, false ) );
	}

	public function getMerchantGatewayId() {
		return $this->m_intMerchantGatewayId;
	}

	public function sqlMerchantGatewayId() {
		return ( true == isset( $this->m_intMerchantGatewayId ) ) ? ( string ) $this->m_intMerchantGatewayId : 'NULL';
	}

	public function setHeaderRecordType( $strHeaderRecordType ) {
		$this->set( 'm_strHeaderRecordType', CStrings::strTrimDef( $strHeaderRecordType, 2, NULL, true ) );
	}

	public function getHeaderRecordType() {
		return $this->m_strHeaderRecordType;
	}

	public function sqlHeaderRecordType() {
		return ( true == isset( $this->m_strHeaderRecordType ) ) ? '\'' . addslashes( $this->m_strHeaderRecordType ) . '\'' : 'NULL';
	}

	public function setHeaderStandardLevel( $strHeaderStandardLevel ) {
		$this->set( 'm_strHeaderStandardLevel', CStrings::strTrimDef( $strHeaderStandardLevel, 2, NULL, true ) );
	}

	public function getHeaderStandardLevel() {
		return $this->m_strHeaderStandardLevel;
	}

	public function sqlHeaderStandardLevel() {
		return ( true == isset( $this->m_strHeaderStandardLevel ) ) ? '\'' . addslashes( $this->m_strHeaderStandardLevel ) . '\'' : 'NULL';
	}

	public function setHeaderTestFileIndicator( $strHeaderTestFileIndicator ) {
		$this->set( 'm_strHeaderTestFileIndicator', CStrings::strTrimDef( $strHeaderTestFileIndicator, 1, NULL, true ) );
	}

	public function getHeaderTestFileIndicator() {
		return $this->m_strHeaderTestFileIndicator;
	}

	public function sqlHeaderTestFileIndicator() {
		return ( true == isset( $this->m_strHeaderTestFileIndicator ) ) ? '\'' . addslashes( $this->m_strHeaderTestFileIndicator ) . '\'' : 'NULL';
	}

	public function setHeaderImmediateDestinationRoutingNumber( $strHeaderImmediateDestinationRoutingNumber ) {
		$this->set( 'm_strHeaderImmediateDestinationRoutingNumber', CStrings::strTrimDef( $strHeaderImmediateDestinationRoutingNumber, 9, NULL, true ) );
	}

	public function getHeaderImmediateDestinationRoutingNumber() {
		return $this->m_strHeaderImmediateDestinationRoutingNumber;
	}

	public function sqlHeaderImmediateDestinationRoutingNumber() {
		return ( true == isset( $this->m_strHeaderImmediateDestinationRoutingNumber ) ) ? '\'' . addslashes( $this->m_strHeaderImmediateDestinationRoutingNumber ) . '\'' : 'NULL';
	}

	public function setHeaderImmediateOriginRoutingNumber( $strHeaderImmediateOriginRoutingNumber ) {
		$this->set( 'm_strHeaderImmediateOriginRoutingNumber', CStrings::strTrimDef( $strHeaderImmediateOriginRoutingNumber, 9, NULL, true ) );
	}

	public function getHeaderImmediateOriginRoutingNumber() {
		return $this->m_strHeaderImmediateOriginRoutingNumber;
	}

	public function sqlHeaderImmediateOriginRoutingNumber() {
		return ( true == isset( $this->m_strHeaderImmediateOriginRoutingNumber ) ) ? '\'' . addslashes( $this->m_strHeaderImmediateOriginRoutingNumber ) . '\'' : 'NULL';
	}

	public function setHeaderFileCreationDate( $strHeaderFileCreationDate ) {
		$this->set( 'm_strHeaderFileCreationDate', CStrings::strTrimDef( $strHeaderFileCreationDate, 8, NULL, true ) );
	}

	public function getHeaderFileCreationDate() {
		return $this->m_strHeaderFileCreationDate;
	}

	public function sqlHeaderFileCreationDate() {
		return ( true == isset( $this->m_strHeaderFileCreationDate ) ) ? '\'' . addslashes( $this->m_strHeaderFileCreationDate ) . '\'' : 'NULL';
	}

	public function setHeaderFileCreationTime( $strHeaderFileCreationTime ) {
		$this->set( 'm_strHeaderFileCreationTime', CStrings::strTrimDef( $strHeaderFileCreationTime, 4, NULL, true ) );
	}

	public function getHeaderFileCreationTime() {
		return $this->m_strHeaderFileCreationTime;
	}

	public function sqlHeaderFileCreationTime() {
		return ( true == isset( $this->m_strHeaderFileCreationTime ) ) ? '\'' . addslashes( $this->m_strHeaderFileCreationTime ) . '\'' : 'NULL';
	}

	public function setHeaderResendIndicator( $strHeaderResendIndicator ) {
		$this->set( 'm_strHeaderResendIndicator', CStrings::strTrimDef( $strHeaderResendIndicator, 1, NULL, true ) );
	}

	public function getHeaderResendIndicator() {
		return $this->m_strHeaderResendIndicator;
	}

	public function sqlHeaderResendIndicator() {
		return ( true == isset( $this->m_strHeaderResendIndicator ) ) ? '\'' . addslashes( $this->m_strHeaderResendIndicator ) . '\'' : 'NULL';
	}

	public function setHeaderImmediateDestinationName( $strHeaderImmediateDestinationName ) {
		$this->set( 'm_strHeaderImmediateDestinationName', CStrings::strTrimDef( $strHeaderImmediateDestinationName, 18, NULL, true ) );
	}

	public function getHeaderImmediateDestinationName() {
		return $this->m_strHeaderImmediateDestinationName;
	}

	public function sqlHeaderImmediateDestinationName() {
		return ( true == isset( $this->m_strHeaderImmediateDestinationName ) ) ? '\'' . addslashes( $this->m_strHeaderImmediateDestinationName ) . '\'' : 'NULL';
	}

	public function setHeaderImmediateOrginName( $strHeaderImmediateOrginName ) {
		$this->set( 'm_strHeaderImmediateOrginName', CStrings::strTrimDef( $strHeaderImmediateOrginName, 18, NULL, true ) );
	}

	public function getHeaderImmediateOrginName() {
		return $this->m_strHeaderImmediateOrginName;
	}

	public function sqlHeaderImmediateOrginName() {
		return ( true == isset( $this->m_strHeaderImmediateOrginName ) ) ? '\'' . addslashes( $this->m_strHeaderImmediateOrginName ) . '\'' : 'NULL';
	}

	public function setHeaderFileIdModifier( $strHeaderFileIdModifier ) {
		$this->set( 'm_strHeaderFileIdModifier', CStrings::strTrimDef( $strHeaderFileIdModifier, 1, NULL, true ) );
	}

	public function getHeaderFileIdModifier() {
		return $this->m_strHeaderFileIdModifier;
	}

	public function sqlHeaderFileIdModifier() {
		return ( true == isset( $this->m_strHeaderFileIdModifier ) ) ? '\'' . addslashes( $this->m_strHeaderFileIdModifier ) . '\'' : 'NULL';
	}

	public function setHeaderCountryCode( $strHeaderCountryCode ) {
		$this->set( 'm_strHeaderCountryCode', CStrings::strTrimDef( $strHeaderCountryCode, 2, NULL, true ) );
	}

	public function getHeaderCountryCode() {
		return $this->m_strHeaderCountryCode;
	}

	public function sqlHeaderCountryCode() {
		return ( true == isset( $this->m_strHeaderCountryCode ) ) ? '\'' . addslashes( $this->m_strHeaderCountryCode ) . '\'' : 'NULL';
	}

	public function setHeaderUserField( $strHeaderUserField ) {
		$this->set( 'm_strHeaderUserField', CStrings::strTrimDef( $strHeaderUserField, 4, NULL, true ) );
	}

	public function getHeaderUserField() {
		return $this->m_strHeaderUserField;
	}

	public function sqlHeaderUserField() {
		return ( true == isset( $this->m_strHeaderUserField ) ) ? '\'' . addslashes( $this->m_strHeaderUserField ) . '\'' : 'NULL';
	}

	public function setHeaderReserved( $strHeaderReserved ) {
		$this->set( 'm_strHeaderReserved', CStrings::strTrimDef( $strHeaderReserved, 1, NULL, true ) );
	}

	public function getHeaderReserved() {
		return $this->m_strHeaderReserved;
	}

	public function sqlHeaderReserved() {
		return ( true == isset( $this->m_strHeaderReserved ) ) ? '\'' . addslashes( $this->m_strHeaderReserved ) . '\'' : 'NULL';
	}

	public function setControlRecordType( $strControlRecordType ) {
		$this->set( 'm_strControlRecordType', CStrings::strTrimDef( $strControlRecordType, 2, NULL, true ) );
	}

	public function getControlRecordType() {
		return $this->m_strControlRecordType;
	}

	public function sqlControlRecordType() {
		return ( true == isset( $this->m_strControlRecordType ) ) ? '\'' . addslashes( $this->m_strControlRecordType ) . '\'' : 'NULL';
	}

	public function setControlCashLetterCount( $strControlCashLetterCount ) {
		$this->set( 'm_strControlCashLetterCount', CStrings::strTrimDef( $strControlCashLetterCount, 6, NULL, true ) );
	}

	public function getControlCashLetterCount() {
		return $this->m_strControlCashLetterCount;
	}

	public function sqlControlCashLetterCount() {
		return ( true == isset( $this->m_strControlCashLetterCount ) ) ? '\'' . addslashes( $this->m_strControlCashLetterCount ) . '\'' : 'NULL';
	}

	public function setControlTotalRecordCount( $strControlTotalRecordCount ) {
		$this->set( 'm_strControlTotalRecordCount', CStrings::strTrimDef( $strControlTotalRecordCount, 8, NULL, true ) );
	}

	public function getControlTotalRecordCount() {
		return $this->m_strControlTotalRecordCount;
	}

	public function sqlControlTotalRecordCount() {
		return ( true == isset( $this->m_strControlTotalRecordCount ) ) ? '\'' . addslashes( $this->m_strControlTotalRecordCount ) . '\'' : 'NULL';
	}

	public function setControlTotalItemCount( $strControlTotalItemCount ) {
		$this->set( 'm_strControlTotalItemCount', CStrings::strTrimDef( $strControlTotalItemCount, 8, NULL, true ) );
	}

	public function getControlTotalItemCount() {
		return $this->m_strControlTotalItemCount;
	}

	public function sqlControlTotalItemCount() {
		return ( true == isset( $this->m_strControlTotalItemCount ) ) ? '\'' . addslashes( $this->m_strControlTotalItemCount ) . '\'' : 'NULL';
	}

	public function setControlFileTotalAmount( $strControlFileTotalAmount ) {
		$this->set( 'm_strControlFileTotalAmount', CStrings::strTrimDef( $strControlFileTotalAmount, 16, NULL, true ) );
	}

	public function getControlFileTotalAmount() {
		return $this->m_strControlFileTotalAmount;
	}

	public function sqlControlFileTotalAmount() {
		return ( true == isset( $this->m_strControlFileTotalAmount ) ) ? '\'' . addslashes( $this->m_strControlFileTotalAmount ) . '\'' : 'NULL';
	}

	public function setControlImmediateOriginContactName( $strControlImmediateOriginContactName ) {
		$this->set( 'm_strControlImmediateOriginContactName', CStrings::strTrimDef( $strControlImmediateOriginContactName, 14, NULL, true ) );
	}

	public function getControlImmediateOriginContactName() {
		return $this->m_strControlImmediateOriginContactName;
	}

	public function sqlControlImmediateOriginContactName() {
		return ( true == isset( $this->m_strControlImmediateOriginContactName ) ) ? '\'' . addslashes( $this->m_strControlImmediateOriginContactName ) . '\'' : 'NULL';
	}

	public function setControlImmediateOriginContactPhoneNumber( $strControlImmediateOriginContactPhoneNumber ) {
		$this->set( 'm_strControlImmediateOriginContactPhoneNumber', CStrings::strTrimDef( $strControlImmediateOriginContactPhoneNumber, 10, NULL, true ) );
	}

	public function getControlImmediateOriginContactPhoneNumber() {
		return $this->m_strControlImmediateOriginContactPhoneNumber;
	}

	public function sqlControlImmediateOriginContactPhoneNumber() {
		return ( true == isset( $this->m_strControlImmediateOriginContactPhoneNumber ) ) ? '\'' . addslashes( $this->m_strControlImmediateOriginContactPhoneNumber ) . '\'' : 'NULL';
	}

	public function setControlReserved( $strControlReserved ) {
		$this->set( 'm_strControlReserved', CStrings::strTrimDef( $strControlReserved, 16, NULL, true ) );
	}

	public function getControlReserved() {
		return $this->m_strControlReserved;
	}

	public function sqlControlReserved() {
		return ( true == isset( $this->m_strControlReserved ) ) ? '\'' . addslashes( $this->m_strControlReserved ) . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setIsReconPrepped( $intIsReconPrepped ) {
		$this->set( 'm_intIsReconPrepped', CStrings::strToIntDef( $intIsReconPrepped, NULL, false ) );
	}

	public function getIsReconPrepped() {
		return $this->m_intIsReconPrepped;
	}

	public function sqlIsReconPrepped() {
		return ( true == isset( $this->m_intIsReconPrepped ) ) ? ( string ) $this->m_intIsReconPrepped : '0';
	}

	public function setFileDatetime( $strFileDatetime ) {
		$this->set( 'm_strFileDatetime', CStrings::strTrimDef( $strFileDatetime, -1, NULL, true ) );
	}

	public function getFileDatetime() {
		return $this->m_strFileDatetime;
	}

	public function sqlFileDatetime() {
		return ( true == isset( $this->m_strFileDatetime ) ) ? '\'' . $this->m_strFileDatetime . '\'' : 'NULL';
	}

	public function setTransmittedOn( $strTransmittedOn ) {
		$this->set( 'm_strTransmittedOn', CStrings::strTrimDef( $strTransmittedOn, -1, NULL, true ) );
	}

	public function getTransmittedOn() {
		return $this->m_strTransmittedOn;
	}

	public function sqlTransmittedOn() {
		return ( true == isset( $this->m_strTransmittedOn ) ) ? '\'' . $this->m_strTransmittedOn . '\'' : 'NULL';
	}

	public function setConfirmedOn( $strConfirmedOn ) {
		$this->set( 'm_strConfirmedOn', CStrings::strTrimDef( $strConfirmedOn, -1, NULL, true ) );
	}

	public function getConfirmedOn() {
		return $this->m_strConfirmedOn;
	}

	public function sqlConfirmedOn() {
		return ( true == isset( $this->m_strConfirmedOn ) ) ? '\'' . $this->m_strConfirmedOn . '\'' : 'NULL';
	}

	public function setTransferredToIntermediaryOn( $strTransferredToIntermediaryOn ) {
		$this->set( 'm_strTransferredToIntermediaryOn', CStrings::strTrimDef( $strTransferredToIntermediaryOn, -1, NULL, true ) );
	}

	public function getTransferredToIntermediaryOn() {
		return $this->m_strTransferredToIntermediaryOn;
	}

	public function sqlTransferredToIntermediaryOn() {
		return ( true == isset( $this->m_strTransferredToIntermediaryOn ) ) ? '\'' . $this->m_strTransferredToIntermediaryOn . '\'' : 'NULL';
	}

	public function setTransferredForReturnsOn( $strTransferredForReturnsOn ) {
		$this->set( 'm_strTransferredForReturnsOn', CStrings::strTrimDef( $strTransferredForReturnsOn, -1, NULL, true ) );
	}

	public function getTransferredForReturnsOn() {
		return $this->m_strTransferredForReturnsOn;
	}

	public function sqlTransferredForReturnsOn() {
		return ( true == isset( $this->m_strTransferredForReturnsOn ) ) ? '\'' . $this->m_strTransferredForReturnsOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, x937_file_type_id, x937_file_status_type_id, processing_bank_id, merchant_gateway_id, header_record_type, header_standard_level, header_test_file_indicator, header_immediate_destination_routing_number, header_immediate_origin_routing_number, header_file_creation_date, header_file_creation_time, header_resend_indicator, header_immediate_destination_name, header_immediate_orgin_name, header_file_id_modifier, header_country_code, header_user_field, header_reserved, control_record_type, control_cash_letter_count, control_total_record_count, control_total_item_count, control_file_total_amount, control_immediate_origin_contact_name, control_immediate_origin_contact_phone_number, control_reserved, file_name, file_path, is_recon_prepped, file_datetime, transmitted_on, confirmed_on, transferred_to_intermediary_on, transferred_for_returns_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlX937FileTypeId() . ', ' .
 						$this->sqlX937FileStatusTypeId() . ', ' .
 						$this->sqlProcessingBankId() . ', ' .
 						$this->sqlMerchantGatewayId() . ', ' .
 						$this->sqlHeaderRecordType() . ', ' .
 						$this->sqlHeaderStandardLevel() . ', ' .
 						$this->sqlHeaderTestFileIndicator() . ', ' .
 						$this->sqlHeaderImmediateDestinationRoutingNumber() . ', ' .
 						$this->sqlHeaderImmediateOriginRoutingNumber() . ', ' .
 						$this->sqlHeaderFileCreationDate() . ', ' .
 						$this->sqlHeaderFileCreationTime() . ', ' .
 						$this->sqlHeaderResendIndicator() . ', ' .
 						$this->sqlHeaderImmediateDestinationName() . ', ' .
 						$this->sqlHeaderImmediateOrginName() . ', ' .
 						$this->sqlHeaderFileIdModifier() . ', ' .
 						$this->sqlHeaderCountryCode() . ', ' .
 						$this->sqlHeaderUserField() . ', ' .
 						$this->sqlHeaderReserved() . ', ' .
 						$this->sqlControlRecordType() . ', ' .
 						$this->sqlControlCashLetterCount() . ', ' .
 						$this->sqlControlTotalRecordCount() . ', ' .
 						$this->sqlControlTotalItemCount() . ', ' .
 						$this->sqlControlFileTotalAmount() . ', ' .
 						$this->sqlControlImmediateOriginContactName() . ', ' .
 						$this->sqlControlImmediateOriginContactPhoneNumber() . ', ' .
 						$this->sqlControlReserved() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlIsReconPrepped() . ', ' .
 						$this->sqlFileDatetime() . ', ' .
 						$this->sqlTransmittedOn() . ', ' .
 						$this->sqlConfirmedOn() . ', ' .
 						$this->sqlTransferredToIntermediaryOn() . ', ' .
 						$this->sqlTransferredForReturnsOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_file_type_id = ' . $this->sqlX937FileTypeId() . ','; } elseif( true == array_key_exists( 'X937FileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' x937_file_type_id = ' . $this->sqlX937FileTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x937_file_status_type_id = ' . $this->sqlX937FileStatusTypeId() . ','; } elseif( true == array_key_exists( 'X937FileStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' x937_file_status_type_id = ' . $this->sqlX937FileStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; } elseif( true == array_key_exists( 'ProcessingBankId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; } elseif( true == array_key_exists( 'MerchantGatewayId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_record_type = ' . $this->sqlHeaderRecordType() . ','; } elseif( true == array_key_exists( 'HeaderRecordType', $this->getChangedColumns() ) ) { $strSql .= ' header_record_type = ' . $this->sqlHeaderRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_standard_level = ' . $this->sqlHeaderStandardLevel() . ','; } elseif( true == array_key_exists( 'HeaderStandardLevel', $this->getChangedColumns() ) ) { $strSql .= ' header_standard_level = ' . $this->sqlHeaderStandardLevel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_test_file_indicator = ' . $this->sqlHeaderTestFileIndicator() . ','; } elseif( true == array_key_exists( 'HeaderTestFileIndicator', $this->getChangedColumns() ) ) { $strSql .= ' header_test_file_indicator = ' . $this->sqlHeaderTestFileIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_immediate_destination_routing_number = ' . $this->sqlHeaderImmediateDestinationRoutingNumber() . ','; } elseif( true == array_key_exists( 'HeaderImmediateDestinationRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' header_immediate_destination_routing_number = ' . $this->sqlHeaderImmediateDestinationRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_immediate_origin_routing_number = ' . $this->sqlHeaderImmediateOriginRoutingNumber() . ','; } elseif( true == array_key_exists( 'HeaderImmediateOriginRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' header_immediate_origin_routing_number = ' . $this->sqlHeaderImmediateOriginRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_file_creation_date = ' . $this->sqlHeaderFileCreationDate() . ','; } elseif( true == array_key_exists( 'HeaderFileCreationDate', $this->getChangedColumns() ) ) { $strSql .= ' header_file_creation_date = ' . $this->sqlHeaderFileCreationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_file_creation_time = ' . $this->sqlHeaderFileCreationTime() . ','; } elseif( true == array_key_exists( 'HeaderFileCreationTime', $this->getChangedColumns() ) ) { $strSql .= ' header_file_creation_time = ' . $this->sqlHeaderFileCreationTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_resend_indicator = ' . $this->sqlHeaderResendIndicator() . ','; } elseif( true == array_key_exists( 'HeaderResendIndicator', $this->getChangedColumns() ) ) { $strSql .= ' header_resend_indicator = ' . $this->sqlHeaderResendIndicator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_immediate_destination_name = ' . $this->sqlHeaderImmediateDestinationName() . ','; } elseif( true == array_key_exists( 'HeaderImmediateDestinationName', $this->getChangedColumns() ) ) { $strSql .= ' header_immediate_destination_name = ' . $this->sqlHeaderImmediateDestinationName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_immediate_orgin_name = ' . $this->sqlHeaderImmediateOrginName() . ','; } elseif( true == array_key_exists( 'HeaderImmediateOrginName', $this->getChangedColumns() ) ) { $strSql .= ' header_immediate_orgin_name = ' . $this->sqlHeaderImmediateOrginName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_file_id_modifier = ' . $this->sqlHeaderFileIdModifier() . ','; } elseif( true == array_key_exists( 'HeaderFileIdModifier', $this->getChangedColumns() ) ) { $strSql .= ' header_file_id_modifier = ' . $this->sqlHeaderFileIdModifier() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_country_code = ' . $this->sqlHeaderCountryCode() . ','; } elseif( true == array_key_exists( 'HeaderCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' header_country_code = ' . $this->sqlHeaderCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_user_field = ' . $this->sqlHeaderUserField() . ','; } elseif( true == array_key_exists( 'HeaderUserField', $this->getChangedColumns() ) ) { $strSql .= ' header_user_field = ' . $this->sqlHeaderUserField() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_reserved = ' . $this->sqlHeaderReserved() . ','; } elseif( true == array_key_exists( 'HeaderReserved', $this->getChangedColumns() ) ) { $strSql .= ' header_reserved = ' . $this->sqlHeaderReserved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_record_type = ' . $this->sqlControlRecordType() . ','; } elseif( true == array_key_exists( 'ControlRecordType', $this->getChangedColumns() ) ) { $strSql .= ' control_record_type = ' . $this->sqlControlRecordType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_cash_letter_count = ' . $this->sqlControlCashLetterCount() . ','; } elseif( true == array_key_exists( 'ControlCashLetterCount', $this->getChangedColumns() ) ) { $strSql .= ' control_cash_letter_count = ' . $this->sqlControlCashLetterCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_total_record_count = ' . $this->sqlControlTotalRecordCount() . ','; } elseif( true == array_key_exists( 'ControlTotalRecordCount', $this->getChangedColumns() ) ) { $strSql .= ' control_total_record_count = ' . $this->sqlControlTotalRecordCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_total_item_count = ' . $this->sqlControlTotalItemCount() . ','; } elseif( true == array_key_exists( 'ControlTotalItemCount', $this->getChangedColumns() ) ) { $strSql .= ' control_total_item_count = ' . $this->sqlControlTotalItemCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_file_total_amount = ' . $this->sqlControlFileTotalAmount() . ','; } elseif( true == array_key_exists( 'ControlFileTotalAmount', $this->getChangedColumns() ) ) { $strSql .= ' control_file_total_amount = ' . $this->sqlControlFileTotalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_immediate_origin_contact_name = ' . $this->sqlControlImmediateOriginContactName() . ','; } elseif( true == array_key_exists( 'ControlImmediateOriginContactName', $this->getChangedColumns() ) ) { $strSql .= ' control_immediate_origin_contact_name = ' . $this->sqlControlImmediateOriginContactName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_immediate_origin_contact_phone_number = ' . $this->sqlControlImmediateOriginContactPhoneNumber() . ','; } elseif( true == array_key_exists( 'ControlImmediateOriginContactPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' control_immediate_origin_contact_phone_number = ' . $this->sqlControlImmediateOriginContactPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_reserved = ' . $this->sqlControlReserved() . ','; } elseif( true == array_key_exists( 'ControlReserved', $this->getChangedColumns() ) ) { $strSql .= ' control_reserved = ' . $this->sqlControlReserved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; } elseif( true == array_key_exists( 'IsReconPrepped', $this->getChangedColumns() ) ) { $strSql .= ' is_recon_prepped = ' . $this->sqlIsReconPrepped() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_datetime = ' . $this->sqlFileDatetime() . ','; } elseif( true == array_key_exists( 'FileDatetime', $this->getChangedColumns() ) ) { $strSql .= ' file_datetime = ' . $this->sqlFileDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmitted_on = ' . $this->sqlTransmittedOn() . ','; } elseif( true == array_key_exists( 'TransmittedOn', $this->getChangedColumns() ) ) { $strSql .= ' transmitted_on = ' . $this->sqlTransmittedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; } elseif( true == array_key_exists( 'ConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transferred_to_intermediary_on = ' . $this->sqlTransferredToIntermediaryOn() . ','; } elseif( true == array_key_exists( 'TransferredToIntermediaryOn', $this->getChangedColumns() ) ) { $strSql .= ' transferred_to_intermediary_on = ' . $this->sqlTransferredToIntermediaryOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transferred_for_returns_on = ' . $this->sqlTransferredForReturnsOn() . ','; } elseif( true == array_key_exists( 'TransferredForReturnsOn', $this->getChangedColumns() ) ) { $strSql .= ' transferred_for_returns_on = ' . $this->sqlTransferredForReturnsOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'x937_file_type_id' => $this->getX937FileTypeId(),
			'x937_file_status_type_id' => $this->getX937FileStatusTypeId(),
			'processing_bank_id' => $this->getProcessingBankId(),
			'merchant_gateway_id' => $this->getMerchantGatewayId(),
			'header_record_type' => $this->getHeaderRecordType(),
			'header_standard_level' => $this->getHeaderStandardLevel(),
			'header_test_file_indicator' => $this->getHeaderTestFileIndicator(),
			'header_immediate_destination_routing_number' => $this->getHeaderImmediateDestinationRoutingNumber(),
			'header_immediate_origin_routing_number' => $this->getHeaderImmediateOriginRoutingNumber(),
			'header_file_creation_date' => $this->getHeaderFileCreationDate(),
			'header_file_creation_time' => $this->getHeaderFileCreationTime(),
			'header_resend_indicator' => $this->getHeaderResendIndicator(),
			'header_immediate_destination_name' => $this->getHeaderImmediateDestinationName(),
			'header_immediate_orgin_name' => $this->getHeaderImmediateOrginName(),
			'header_file_id_modifier' => $this->getHeaderFileIdModifier(),
			'header_country_code' => $this->getHeaderCountryCode(),
			'header_user_field' => $this->getHeaderUserField(),
			'header_reserved' => $this->getHeaderReserved(),
			'control_record_type' => $this->getControlRecordType(),
			'control_cash_letter_count' => $this->getControlCashLetterCount(),
			'control_total_record_count' => $this->getControlTotalRecordCount(),
			'control_total_item_count' => $this->getControlTotalItemCount(),
			'control_file_total_amount' => $this->getControlFileTotalAmount(),
			'control_immediate_origin_contact_name' => $this->getControlImmediateOriginContactName(),
			'control_immediate_origin_contact_phone_number' => $this->getControlImmediateOriginContactPhoneNumber(),
			'control_reserved' => $this->getControlReserved(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'is_recon_prepped' => $this->getIsReconPrepped(),
			'file_datetime' => $this->getFileDatetime(),
			'transmitted_on' => $this->getTransmittedOn(),
			'confirmed_on' => $this->getConfirmedOn(),
			'transferred_to_intermediary_on' => $this->getTransferredToIntermediaryOn(),
			'transferred_for_returns_on' => $this->getTransferredForReturnsOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>