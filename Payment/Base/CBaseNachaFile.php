<?php

class CBaseNachaFile extends CEosSingularBase {

	const TABLE_NAME = 'public.nacha_files';

	protected $m_intId;
	protected $m_intNachaFileTypeId;
	protected $m_intNachaFileStatusTypeId;
	protected $m_intProcessingBankId;
	protected $m_intMerchantGatewayId;
	protected $m_strHeaderRecordTypeCode;
	protected $m_strHeaderPriorityCode;
	protected $m_strHeaderImmediateDestination;
	protected $m_strHeaderImmediateOrigin;
	protected $m_strHeaderFileCreationDate;
	protected $m_strHeaderFileCreationTime;
	protected $m_strHeaderFileIdModifier;
	protected $m_strHeaderRecordSize;
	protected $m_strHeaderBlockingFactor;
	protected $m_strHeaderFormatCode;
	protected $m_strHeaderImmediateDestinationName;
	protected $m_strHeaderImmediateOriginName;
	protected $m_strHeaderReferenceCode;
	protected $m_strControlRecordTypeCode;
	protected $m_strControlBatchCount;
	protected $m_strControlBlockCount;
	protected $m_strControlEntryAddendaCount;
	protected $m_strControlEntryHash;
	protected $m_strControlTotalDebitEntryAmount;
	protected $m_strControlTotalCreditEntryAmount;
	protected $m_strControlReserved;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strNachaFileDatetime;
	protected $m_strTransmittedOn;
	protected $m_strConfirmedOn;
	protected $m_strReturnsProcessedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['nacha_file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaFileTypeId', trim( $arrValues['nacha_file_type_id'] ) ); elseif( isset( $arrValues['nacha_file_type_id'] ) ) $this->setNachaFileTypeId( $arrValues['nacha_file_type_id'] );
		if( isset( $arrValues['nacha_file_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaFileStatusTypeId', trim( $arrValues['nacha_file_status_type_id'] ) ); elseif( isset( $arrValues['nacha_file_status_type_id'] ) ) $this->setNachaFileStatusTypeId( $arrValues['nacha_file_status_type_id'] );
		if( isset( $arrValues['processing_bank_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankId', trim( $arrValues['processing_bank_id'] ) ); elseif( isset( $arrValues['processing_bank_id'] ) ) $this->setProcessingBankId( $arrValues['processing_bank_id'] );
		if( isset( $arrValues['merchant_gateway_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantGatewayId', trim( $arrValues['merchant_gateway_id'] ) ); elseif( isset( $arrValues['merchant_gateway_id'] ) ) $this->setMerchantGatewayId( $arrValues['merchant_gateway_id'] );
		if( isset( $arrValues['header_record_type_code'] ) && $boolDirectSet ) $this->set( 'm_strHeaderRecordTypeCode', trim( stripcslashes( $arrValues['header_record_type_code'] ) ) ); elseif( isset( $arrValues['header_record_type_code'] ) ) $this->setHeaderRecordTypeCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_record_type_code'] ) : $arrValues['header_record_type_code'] );
		if( isset( $arrValues['header_priority_code'] ) && $boolDirectSet ) $this->set( 'm_strHeaderPriorityCode', trim( stripcslashes( $arrValues['header_priority_code'] ) ) ); elseif( isset( $arrValues['header_priority_code'] ) ) $this->setHeaderPriorityCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_priority_code'] ) : $arrValues['header_priority_code'] );
		if( isset( $arrValues['header_immediate_destination'] ) && $boolDirectSet ) $this->set( 'm_strHeaderImmediateDestination', trim( stripcslashes( $arrValues['header_immediate_destination'] ) ) ); elseif( isset( $arrValues['header_immediate_destination'] ) ) $this->setHeaderImmediateDestination( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_immediate_destination'] ) : $arrValues['header_immediate_destination'] );
		if( isset( $arrValues['header_immediate_origin'] ) && $boolDirectSet ) $this->set( 'm_strHeaderImmediateOrigin', trim( stripcslashes( $arrValues['header_immediate_origin'] ) ) ); elseif( isset( $arrValues['header_immediate_origin'] ) ) $this->setHeaderImmediateOrigin( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_immediate_origin'] ) : $arrValues['header_immediate_origin'] );
		if( isset( $arrValues['header_file_creation_date'] ) && $boolDirectSet ) $this->set( 'm_strHeaderFileCreationDate', trim( stripcslashes( $arrValues['header_file_creation_date'] ) ) ); elseif( isset( $arrValues['header_file_creation_date'] ) ) $this->setHeaderFileCreationDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_file_creation_date'] ) : $arrValues['header_file_creation_date'] );
		if( isset( $arrValues['header_file_creation_time'] ) && $boolDirectSet ) $this->set( 'm_strHeaderFileCreationTime', trim( stripcslashes( $arrValues['header_file_creation_time'] ) ) ); elseif( isset( $arrValues['header_file_creation_time'] ) ) $this->setHeaderFileCreationTime( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_file_creation_time'] ) : $arrValues['header_file_creation_time'] );
		if( isset( $arrValues['header_file_id_modifier'] ) && $boolDirectSet ) $this->set( 'm_strHeaderFileIdModifier', trim( stripcslashes( $arrValues['header_file_id_modifier'] ) ) ); elseif( isset( $arrValues['header_file_id_modifier'] ) ) $this->setHeaderFileIdModifier( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_file_id_modifier'] ) : $arrValues['header_file_id_modifier'] );
		if( isset( $arrValues['header_record_size'] ) && $boolDirectSet ) $this->set( 'm_strHeaderRecordSize', trim( stripcslashes( $arrValues['header_record_size'] ) ) ); elseif( isset( $arrValues['header_record_size'] ) ) $this->setHeaderRecordSize( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_record_size'] ) : $arrValues['header_record_size'] );
		if( isset( $arrValues['header_blocking_factor'] ) && $boolDirectSet ) $this->set( 'm_strHeaderBlockingFactor', trim( stripcslashes( $arrValues['header_blocking_factor'] ) ) ); elseif( isset( $arrValues['header_blocking_factor'] ) ) $this->setHeaderBlockingFactor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_blocking_factor'] ) : $arrValues['header_blocking_factor'] );
		if( isset( $arrValues['header_format_code'] ) && $boolDirectSet ) $this->set( 'm_strHeaderFormatCode', trim( stripcslashes( $arrValues['header_format_code'] ) ) ); elseif( isset( $arrValues['header_format_code'] ) ) $this->setHeaderFormatCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_format_code'] ) : $arrValues['header_format_code'] );
		if( isset( $arrValues['header_immediate_destination_name'] ) && $boolDirectSet ) $this->set( 'm_strHeaderImmediateDestinationName', trim( stripcslashes( $arrValues['header_immediate_destination_name'] ) ) ); elseif( isset( $arrValues['header_immediate_destination_name'] ) ) $this->setHeaderImmediateDestinationName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_immediate_destination_name'] ) : $arrValues['header_immediate_destination_name'] );
		if( isset( $arrValues['header_immediate_origin_name'] ) && $boolDirectSet ) $this->set( 'm_strHeaderImmediateOriginName', trim( stripcslashes( $arrValues['header_immediate_origin_name'] ) ) ); elseif( isset( $arrValues['header_immediate_origin_name'] ) ) $this->setHeaderImmediateOriginName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_immediate_origin_name'] ) : $arrValues['header_immediate_origin_name'] );
		if( isset( $arrValues['header_reference_code'] ) && $boolDirectSet ) $this->set( 'm_strHeaderReferenceCode', trim( stripcslashes( $arrValues['header_reference_code'] ) ) ); elseif( isset( $arrValues['header_reference_code'] ) ) $this->setHeaderReferenceCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_reference_code'] ) : $arrValues['header_reference_code'] );
		if( isset( $arrValues['control_record_type_code'] ) && $boolDirectSet ) $this->set( 'm_strControlRecordTypeCode', trim( stripcslashes( $arrValues['control_record_type_code'] ) ) ); elseif( isset( $arrValues['control_record_type_code'] ) ) $this->setControlRecordTypeCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_record_type_code'] ) : $arrValues['control_record_type_code'] );
		if( isset( $arrValues['control_batch_count'] ) && $boolDirectSet ) $this->set( 'm_strControlBatchCount', trim( stripcslashes( $arrValues['control_batch_count'] ) ) ); elseif( isset( $arrValues['control_batch_count'] ) ) $this->setControlBatchCount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_batch_count'] ) : $arrValues['control_batch_count'] );
		if( isset( $arrValues['control_block_count'] ) && $boolDirectSet ) $this->set( 'm_strControlBlockCount', trim( stripcslashes( $arrValues['control_block_count'] ) ) ); elseif( isset( $arrValues['control_block_count'] ) ) $this->setControlBlockCount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_block_count'] ) : $arrValues['control_block_count'] );
		if( isset( $arrValues['control_entry_addenda_count'] ) && $boolDirectSet ) $this->set( 'm_strControlEntryAddendaCount', trim( stripcslashes( $arrValues['control_entry_addenda_count'] ) ) ); elseif( isset( $arrValues['control_entry_addenda_count'] ) ) $this->setControlEntryAddendaCount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_entry_addenda_count'] ) : $arrValues['control_entry_addenda_count'] );
		if( isset( $arrValues['control_entry_hash'] ) && $boolDirectSet ) $this->set( 'm_strControlEntryHash', trim( stripcslashes( $arrValues['control_entry_hash'] ) ) ); elseif( isset( $arrValues['control_entry_hash'] ) ) $this->setControlEntryHash( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_entry_hash'] ) : $arrValues['control_entry_hash'] );
		if( isset( $arrValues['control_total_debit_entry_amount'] ) && $boolDirectSet ) $this->set( 'm_strControlTotalDebitEntryAmount', trim( stripcslashes( $arrValues['control_total_debit_entry_amount'] ) ) ); elseif( isset( $arrValues['control_total_debit_entry_amount'] ) ) $this->setControlTotalDebitEntryAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_total_debit_entry_amount'] ) : $arrValues['control_total_debit_entry_amount'] );
		if( isset( $arrValues['control_total_credit_entry_amount'] ) && $boolDirectSet ) $this->set( 'm_strControlTotalCreditEntryAmount', trim( stripcslashes( $arrValues['control_total_credit_entry_amount'] ) ) ); elseif( isset( $arrValues['control_total_credit_entry_amount'] ) ) $this->setControlTotalCreditEntryAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_total_credit_entry_amount'] ) : $arrValues['control_total_credit_entry_amount'] );
		if( isset( $arrValues['control_reserved'] ) && $boolDirectSet ) $this->set( 'm_strControlReserved', trim( stripcslashes( $arrValues['control_reserved'] ) ) ); elseif( isset( $arrValues['control_reserved'] ) ) $this->setControlReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['control_reserved'] ) : $arrValues['control_reserved'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['nacha_file_datetime'] ) && $boolDirectSet ) $this->set( 'm_strNachaFileDatetime', trim( $arrValues['nacha_file_datetime'] ) ); elseif( isset( $arrValues['nacha_file_datetime'] ) ) $this->setNachaFileDatetime( $arrValues['nacha_file_datetime'] );
		if( isset( $arrValues['transmitted_on'] ) && $boolDirectSet ) $this->set( 'm_strTransmittedOn', trim( $arrValues['transmitted_on'] ) ); elseif( isset( $arrValues['transmitted_on'] ) ) $this->setTransmittedOn( $arrValues['transmitted_on'] );
		if( isset( $arrValues['confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strConfirmedOn', trim( $arrValues['confirmed_on'] ) ); elseif( isset( $arrValues['confirmed_on'] ) ) $this->setConfirmedOn( $arrValues['confirmed_on'] );
		if( isset( $arrValues['returns_processed_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnsProcessedOn', trim( $arrValues['returns_processed_on'] ) ); elseif( isset( $arrValues['returns_processed_on'] ) ) $this->setReturnsProcessedOn( $arrValues['returns_processed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setNachaFileTypeId( $intNachaFileTypeId ) {
		$this->set( 'm_intNachaFileTypeId', CStrings::strToIntDef( $intNachaFileTypeId, NULL, false ) );
	}

	public function getNachaFileTypeId() {
		return $this->m_intNachaFileTypeId;
	}

	public function sqlNachaFileTypeId() {
		return ( true == isset( $this->m_intNachaFileTypeId ) ) ? ( string ) $this->m_intNachaFileTypeId : 'NULL';
	}

	public function setNachaFileStatusTypeId( $intNachaFileStatusTypeId ) {
		$this->set( 'm_intNachaFileStatusTypeId', CStrings::strToIntDef( $intNachaFileStatusTypeId, NULL, false ) );
	}

	public function getNachaFileStatusTypeId() {
		return $this->m_intNachaFileStatusTypeId;
	}

	public function sqlNachaFileStatusTypeId() {
		return ( true == isset( $this->m_intNachaFileStatusTypeId ) ) ? ( string ) $this->m_intNachaFileStatusTypeId : 'NULL';
	}

	public function setProcessingBankId( $intProcessingBankId ) {
		$this->set( 'm_intProcessingBankId', CStrings::strToIntDef( $intProcessingBankId, NULL, false ) );
	}

	public function getProcessingBankId() {
		return $this->m_intProcessingBankId;
	}

	public function sqlProcessingBankId() {
		return ( true == isset( $this->m_intProcessingBankId ) ) ? ( string ) $this->m_intProcessingBankId : 'NULL';
	}

	public function setMerchantGatewayId( $intMerchantGatewayId ) {
		$this->set( 'm_intMerchantGatewayId', CStrings::strToIntDef( $intMerchantGatewayId, NULL, false ) );
	}

	public function getMerchantGatewayId() {
		return $this->m_intMerchantGatewayId;
	}

	public function sqlMerchantGatewayId() {
		return ( true == isset( $this->m_intMerchantGatewayId ) ) ? ( string ) $this->m_intMerchantGatewayId : 'NULL';
	}

	public function setHeaderRecordTypeCode( $strHeaderRecordTypeCode ) {
		$this->set( 'm_strHeaderRecordTypeCode', CStrings::strTrimDef( $strHeaderRecordTypeCode, 1, NULL, true ) );
	}

	public function getHeaderRecordTypeCode() {
		return $this->m_strHeaderRecordTypeCode;
	}

	public function sqlHeaderRecordTypeCode() {
		return ( true == isset( $this->m_strHeaderRecordTypeCode ) ) ? '\'' . addslashes( $this->m_strHeaderRecordTypeCode ) . '\'' : 'NULL';
	}

	public function setHeaderPriorityCode( $strHeaderPriorityCode ) {
		$this->set( 'm_strHeaderPriorityCode', CStrings::strTrimDef( $strHeaderPriorityCode, 2, NULL, true ) );
	}

	public function getHeaderPriorityCode() {
		return $this->m_strHeaderPriorityCode;
	}

	public function sqlHeaderPriorityCode() {
		return ( true == isset( $this->m_strHeaderPriorityCode ) ) ? '\'' . addslashes( $this->m_strHeaderPriorityCode ) . '\'' : 'NULL';
	}

	public function setHeaderImmediateDestination( $strHeaderImmediateDestination ) {
		$this->set( 'm_strHeaderImmediateDestination', CStrings::strTrimDef( $strHeaderImmediateDestination, 10, NULL, true ) );
	}

	public function getHeaderImmediateDestination() {
		return $this->m_strHeaderImmediateDestination;
	}

	public function sqlHeaderImmediateDestination() {
		return ( true == isset( $this->m_strHeaderImmediateDestination ) ) ? '\'' . addslashes( $this->m_strHeaderImmediateDestination ) . '\'' : 'NULL';
	}

	public function setHeaderImmediateOrigin( $strHeaderImmediateOrigin ) {
		$this->set( 'm_strHeaderImmediateOrigin', CStrings::strTrimDef( $strHeaderImmediateOrigin, 10, NULL, true ) );
	}

	public function getHeaderImmediateOrigin() {
		return $this->m_strHeaderImmediateOrigin;
	}

	public function sqlHeaderImmediateOrigin() {
		return ( true == isset( $this->m_strHeaderImmediateOrigin ) ) ? '\'' . addslashes( $this->m_strHeaderImmediateOrigin ) . '\'' : 'NULL';
	}

	public function setHeaderFileCreationDate( $strHeaderFileCreationDate ) {
		$this->set( 'm_strHeaderFileCreationDate', CStrings::strTrimDef( $strHeaderFileCreationDate, 6, NULL, true ) );
	}

	public function getHeaderFileCreationDate() {
		return $this->m_strHeaderFileCreationDate;
	}

	public function sqlHeaderFileCreationDate() {
		return ( true == isset( $this->m_strHeaderFileCreationDate ) ) ? '\'' . addslashes( $this->m_strHeaderFileCreationDate ) . '\'' : 'NULL';
	}

	public function setHeaderFileCreationTime( $strHeaderFileCreationTime ) {
		$this->set( 'm_strHeaderFileCreationTime', CStrings::strTrimDef( $strHeaderFileCreationTime, 4, NULL, true ) );
	}

	public function getHeaderFileCreationTime() {
		return $this->m_strHeaderFileCreationTime;
	}

	public function sqlHeaderFileCreationTime() {
		return ( true == isset( $this->m_strHeaderFileCreationTime ) ) ? '\'' . addslashes( $this->m_strHeaderFileCreationTime ) . '\'' : 'NULL';
	}

	public function setHeaderFileIdModifier( $strHeaderFileIdModifier ) {
		$this->set( 'm_strHeaderFileIdModifier', CStrings::strTrimDef( $strHeaderFileIdModifier, 1, NULL, true ) );
	}

	public function getHeaderFileIdModifier() {
		return $this->m_strHeaderFileIdModifier;
	}

	public function sqlHeaderFileIdModifier() {
		return ( true == isset( $this->m_strHeaderFileIdModifier ) ) ? '\'' . addslashes( $this->m_strHeaderFileIdModifier ) . '\'' : 'NULL';
	}

	public function setHeaderRecordSize( $strHeaderRecordSize ) {
		$this->set( 'm_strHeaderRecordSize', CStrings::strTrimDef( $strHeaderRecordSize, 3, NULL, true ) );
	}

	public function getHeaderRecordSize() {
		return $this->m_strHeaderRecordSize;
	}

	public function sqlHeaderRecordSize() {
		return ( true == isset( $this->m_strHeaderRecordSize ) ) ? '\'' . addslashes( $this->m_strHeaderRecordSize ) . '\'' : 'NULL';
	}

	public function setHeaderBlockingFactor( $strHeaderBlockingFactor ) {
		$this->set( 'm_strHeaderBlockingFactor', CStrings::strTrimDef( $strHeaderBlockingFactor, 2, NULL, true ) );
	}

	public function getHeaderBlockingFactor() {
		return $this->m_strHeaderBlockingFactor;
	}

	public function sqlHeaderBlockingFactor() {
		return ( true == isset( $this->m_strHeaderBlockingFactor ) ) ? '\'' . addslashes( $this->m_strHeaderBlockingFactor ) . '\'' : 'NULL';
	}

	public function setHeaderFormatCode( $strHeaderFormatCode ) {
		$this->set( 'm_strHeaderFormatCode', CStrings::strTrimDef( $strHeaderFormatCode, 1, NULL, true ) );
	}

	public function getHeaderFormatCode() {
		return $this->m_strHeaderFormatCode;
	}

	public function sqlHeaderFormatCode() {
		return ( true == isset( $this->m_strHeaderFormatCode ) ) ? '\'' . addslashes( $this->m_strHeaderFormatCode ) . '\'' : 'NULL';
	}

	public function setHeaderImmediateDestinationName( $strHeaderImmediateDestinationName ) {
		$this->set( 'm_strHeaderImmediateDestinationName', CStrings::strTrimDef( $strHeaderImmediateDestinationName, 23, NULL, true ) );
	}

	public function getHeaderImmediateDestinationName() {
		return $this->m_strHeaderImmediateDestinationName;
	}

	public function sqlHeaderImmediateDestinationName() {
		return ( true == isset( $this->m_strHeaderImmediateDestinationName ) ) ? '\'' . addslashes( $this->m_strHeaderImmediateDestinationName ) . '\'' : 'NULL';
	}

	public function setHeaderImmediateOriginName( $strHeaderImmediateOriginName ) {
		$this->set( 'm_strHeaderImmediateOriginName', CStrings::strTrimDef( $strHeaderImmediateOriginName, 23, NULL, true ) );
	}

	public function getHeaderImmediateOriginName() {
		return $this->m_strHeaderImmediateOriginName;
	}

	public function sqlHeaderImmediateOriginName() {
		return ( true == isset( $this->m_strHeaderImmediateOriginName ) ) ? '\'' . addslashes( $this->m_strHeaderImmediateOriginName ) . '\'' : 'NULL';
	}

	public function setHeaderReferenceCode( $strHeaderReferenceCode ) {
		$this->set( 'm_strHeaderReferenceCode', CStrings::strTrimDef( $strHeaderReferenceCode, 8, NULL, true ) );
	}

	public function getHeaderReferenceCode() {
		return $this->m_strHeaderReferenceCode;
	}

	public function sqlHeaderReferenceCode() {
		return ( true == isset( $this->m_strHeaderReferenceCode ) ) ? '\'' . addslashes( $this->m_strHeaderReferenceCode ) . '\'' : 'NULL';
	}

	public function setControlRecordTypeCode( $strControlRecordTypeCode ) {
		$this->set( 'm_strControlRecordTypeCode', CStrings::strTrimDef( $strControlRecordTypeCode, 1, NULL, true ) );
	}

	public function getControlRecordTypeCode() {
		return $this->m_strControlRecordTypeCode;
	}

	public function sqlControlRecordTypeCode() {
		return ( true == isset( $this->m_strControlRecordTypeCode ) ) ? '\'' . addslashes( $this->m_strControlRecordTypeCode ) . '\'' : 'NULL';
	}

	public function setControlBatchCount( $strControlBatchCount ) {
		$this->set( 'm_strControlBatchCount', CStrings::strTrimDef( $strControlBatchCount, 6, NULL, true ) );
	}

	public function getControlBatchCount() {
		return $this->m_strControlBatchCount;
	}

	public function sqlControlBatchCount() {
		return ( true == isset( $this->m_strControlBatchCount ) ) ? '\'' . addslashes( $this->m_strControlBatchCount ) . '\'' : 'NULL';
	}

	public function setControlBlockCount( $strControlBlockCount ) {
		$this->set( 'm_strControlBlockCount', CStrings::strTrimDef( $strControlBlockCount, 6, NULL, true ) );
	}

	public function getControlBlockCount() {
		return $this->m_strControlBlockCount;
	}

	public function sqlControlBlockCount() {
		return ( true == isset( $this->m_strControlBlockCount ) ) ? '\'' . addslashes( $this->m_strControlBlockCount ) . '\'' : 'NULL';
	}

	public function setControlEntryAddendaCount( $strControlEntryAddendaCount ) {
		$this->set( 'm_strControlEntryAddendaCount', CStrings::strTrimDef( $strControlEntryAddendaCount, 8, NULL, true ) );
	}

	public function getControlEntryAddendaCount() {
		return $this->m_strControlEntryAddendaCount;
	}

	public function sqlControlEntryAddendaCount() {
		return ( true == isset( $this->m_strControlEntryAddendaCount ) ) ? '\'' . addslashes( $this->m_strControlEntryAddendaCount ) . '\'' : 'NULL';
	}

	public function setControlEntryHash( $strControlEntryHash ) {
		$this->set( 'm_strControlEntryHash', CStrings::strTrimDef( $strControlEntryHash, 10, NULL, true ) );
	}

	public function getControlEntryHash() {
		return $this->m_strControlEntryHash;
	}

	public function sqlControlEntryHash() {
		return ( true == isset( $this->m_strControlEntryHash ) ) ? '\'' . addslashes( $this->m_strControlEntryHash ) . '\'' : 'NULL';
	}

	public function setControlTotalDebitEntryAmount( $strControlTotalDebitEntryAmount ) {
		$this->set( 'm_strControlTotalDebitEntryAmount', CStrings::strTrimDef( $strControlTotalDebitEntryAmount, 12, NULL, true ) );
	}

	public function getControlTotalDebitEntryAmount() {
		return $this->m_strControlTotalDebitEntryAmount;
	}

	public function sqlControlTotalDebitEntryAmount() {
		return ( true == isset( $this->m_strControlTotalDebitEntryAmount ) ) ? '\'' . addslashes( $this->m_strControlTotalDebitEntryAmount ) . '\'' : 'NULL';
	}

	public function setControlTotalCreditEntryAmount( $strControlTotalCreditEntryAmount ) {
		$this->set( 'm_strControlTotalCreditEntryAmount', CStrings::strTrimDef( $strControlTotalCreditEntryAmount, 12, NULL, true ) );
	}

	public function getControlTotalCreditEntryAmount() {
		return $this->m_strControlTotalCreditEntryAmount;
	}

	public function sqlControlTotalCreditEntryAmount() {
		return ( true == isset( $this->m_strControlTotalCreditEntryAmount ) ) ? '\'' . addslashes( $this->m_strControlTotalCreditEntryAmount ) . '\'' : 'NULL';
	}

	public function setControlReserved( $strControlReserved ) {
		$this->set( 'm_strControlReserved', CStrings::strTrimDef( $strControlReserved, 39, NULL, true ) );
	}

	public function getControlReserved() {
		return $this->m_strControlReserved;
	}

	public function sqlControlReserved() {
		return ( true == isset( $this->m_strControlReserved ) ) ? '\'' . addslashes( $this->m_strControlReserved ) . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setNachaFileDatetime( $strNachaFileDatetime ) {
		$this->set( 'm_strNachaFileDatetime', CStrings::strTrimDef( $strNachaFileDatetime, -1, NULL, true ) );
	}

	public function getNachaFileDatetime() {
		return $this->m_strNachaFileDatetime;
	}

	public function sqlNachaFileDatetime() {
		return ( true == isset( $this->m_strNachaFileDatetime ) ) ? '\'' . $this->m_strNachaFileDatetime . '\'' : 'NULL';
	}

	public function setTransmittedOn( $strTransmittedOn ) {
		$this->set( 'm_strTransmittedOn', CStrings::strTrimDef( $strTransmittedOn, -1, NULL, true ) );
	}

	public function getTransmittedOn() {
		return $this->m_strTransmittedOn;
	}

	public function sqlTransmittedOn() {
		return ( true == isset( $this->m_strTransmittedOn ) ) ? '\'' . $this->m_strTransmittedOn . '\'' : 'NULL';
	}

	public function setConfirmedOn( $strConfirmedOn ) {
		$this->set( 'm_strConfirmedOn', CStrings::strTrimDef( $strConfirmedOn, -1, NULL, true ) );
	}

	public function getConfirmedOn() {
		return $this->m_strConfirmedOn;
	}

	public function sqlConfirmedOn() {
		return ( true == isset( $this->m_strConfirmedOn ) ) ? '\'' . $this->m_strConfirmedOn . '\'' : 'NULL';
	}

	public function setReturnsProcessedOn( $strReturnsProcessedOn ) {
		$this->set( 'm_strReturnsProcessedOn', CStrings::strTrimDef( $strReturnsProcessedOn, -1, NULL, true ) );
	}

	public function getReturnsProcessedOn() {
		return $this->m_strReturnsProcessedOn;
	}

	public function sqlReturnsProcessedOn() {
		return ( true == isset( $this->m_strReturnsProcessedOn ) ) ? '\'' . $this->m_strReturnsProcessedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, nacha_file_type_id, nacha_file_status_type_id, processing_bank_id, merchant_gateway_id, header_record_type_code, header_priority_code, header_immediate_destination, header_immediate_origin, header_file_creation_date, header_file_creation_time, header_file_id_modifier, header_record_size, header_blocking_factor, header_format_code, header_immediate_destination_name, header_immediate_origin_name, header_reference_code, control_record_type_code, control_batch_count, control_block_count, control_entry_addenda_count, control_entry_hash, control_total_debit_entry_amount, control_total_credit_entry_amount, control_reserved, file_name, file_path, nacha_file_datetime, transmitted_on, confirmed_on, returns_processed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlNachaFileTypeId() . ', ' .
 						$this->sqlNachaFileStatusTypeId() . ', ' .
 						$this->sqlProcessingBankId() . ', ' .
 						$this->sqlMerchantGatewayId() . ', ' .
 						$this->sqlHeaderRecordTypeCode() . ', ' .
 						$this->sqlHeaderPriorityCode() . ', ' .
 						$this->sqlHeaderImmediateDestination() . ', ' .
 						$this->sqlHeaderImmediateOrigin() . ', ' .
 						$this->sqlHeaderFileCreationDate() . ', ' .
 						$this->sqlHeaderFileCreationTime() . ', ' .
 						$this->sqlHeaderFileIdModifier() . ', ' .
 						$this->sqlHeaderRecordSize() . ', ' .
 						$this->sqlHeaderBlockingFactor() . ', ' .
 						$this->sqlHeaderFormatCode() . ', ' .
 						$this->sqlHeaderImmediateDestinationName() . ', ' .
 						$this->sqlHeaderImmediateOriginName() . ', ' .
 						$this->sqlHeaderReferenceCode() . ', ' .
 						$this->sqlControlRecordTypeCode() . ', ' .
 						$this->sqlControlBatchCount() . ', ' .
 						$this->sqlControlBlockCount() . ', ' .
 						$this->sqlControlEntryAddendaCount() . ', ' .
 						$this->sqlControlEntryHash() . ', ' .
 						$this->sqlControlTotalDebitEntryAmount() . ', ' .
 						$this->sqlControlTotalCreditEntryAmount() . ', ' .
 						$this->sqlControlReserved() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlNachaFileDatetime() . ', ' .
 						$this->sqlTransmittedOn() . ', ' .
 						$this->sqlConfirmedOn() . ', ' .
 						$this->sqlReturnsProcessedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_file_type_id = ' . $this->sqlNachaFileTypeId() . ','; } elseif( true == array_key_exists( 'NachaFileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_file_type_id = ' . $this->sqlNachaFileTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_file_status_type_id = ' . $this->sqlNachaFileStatusTypeId() . ','; } elseif( true == array_key_exists( 'NachaFileStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_file_status_type_id = ' . $this->sqlNachaFileStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; } elseif( true == array_key_exists( 'ProcessingBankId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_id = ' . $this->sqlProcessingBankId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; } elseif( true == array_key_exists( 'MerchantGatewayId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_record_type_code = ' . $this->sqlHeaderRecordTypeCode() . ','; } elseif( true == array_key_exists( 'HeaderRecordTypeCode', $this->getChangedColumns() ) ) { $strSql .= ' header_record_type_code = ' . $this->sqlHeaderRecordTypeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_priority_code = ' . $this->sqlHeaderPriorityCode() . ','; } elseif( true == array_key_exists( 'HeaderPriorityCode', $this->getChangedColumns() ) ) { $strSql .= ' header_priority_code = ' . $this->sqlHeaderPriorityCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_immediate_destination = ' . $this->sqlHeaderImmediateDestination() . ','; } elseif( true == array_key_exists( 'HeaderImmediateDestination', $this->getChangedColumns() ) ) { $strSql .= ' header_immediate_destination = ' . $this->sqlHeaderImmediateDestination() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_immediate_origin = ' . $this->sqlHeaderImmediateOrigin() . ','; } elseif( true == array_key_exists( 'HeaderImmediateOrigin', $this->getChangedColumns() ) ) { $strSql .= ' header_immediate_origin = ' . $this->sqlHeaderImmediateOrigin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_file_creation_date = ' . $this->sqlHeaderFileCreationDate() . ','; } elseif( true == array_key_exists( 'HeaderFileCreationDate', $this->getChangedColumns() ) ) { $strSql .= ' header_file_creation_date = ' . $this->sqlHeaderFileCreationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_file_creation_time = ' . $this->sqlHeaderFileCreationTime() . ','; } elseif( true == array_key_exists( 'HeaderFileCreationTime', $this->getChangedColumns() ) ) { $strSql .= ' header_file_creation_time = ' . $this->sqlHeaderFileCreationTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_file_id_modifier = ' . $this->sqlHeaderFileIdModifier() . ','; } elseif( true == array_key_exists( 'HeaderFileIdModifier', $this->getChangedColumns() ) ) { $strSql .= ' header_file_id_modifier = ' . $this->sqlHeaderFileIdModifier() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_record_size = ' . $this->sqlHeaderRecordSize() . ','; } elseif( true == array_key_exists( 'HeaderRecordSize', $this->getChangedColumns() ) ) { $strSql .= ' header_record_size = ' . $this->sqlHeaderRecordSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_blocking_factor = ' . $this->sqlHeaderBlockingFactor() . ','; } elseif( true == array_key_exists( 'HeaderBlockingFactor', $this->getChangedColumns() ) ) { $strSql .= ' header_blocking_factor = ' . $this->sqlHeaderBlockingFactor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_format_code = ' . $this->sqlHeaderFormatCode() . ','; } elseif( true == array_key_exists( 'HeaderFormatCode', $this->getChangedColumns() ) ) { $strSql .= ' header_format_code = ' . $this->sqlHeaderFormatCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_immediate_destination_name = ' . $this->sqlHeaderImmediateDestinationName() . ','; } elseif( true == array_key_exists( 'HeaderImmediateDestinationName', $this->getChangedColumns() ) ) { $strSql .= ' header_immediate_destination_name = ' . $this->sqlHeaderImmediateDestinationName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_immediate_origin_name = ' . $this->sqlHeaderImmediateOriginName() . ','; } elseif( true == array_key_exists( 'HeaderImmediateOriginName', $this->getChangedColumns() ) ) { $strSql .= ' header_immediate_origin_name = ' . $this->sqlHeaderImmediateOriginName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_reference_code = ' . $this->sqlHeaderReferenceCode() . ','; } elseif( true == array_key_exists( 'HeaderReferenceCode', $this->getChangedColumns() ) ) { $strSql .= ' header_reference_code = ' . $this->sqlHeaderReferenceCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_record_type_code = ' . $this->sqlControlRecordTypeCode() . ','; } elseif( true == array_key_exists( 'ControlRecordTypeCode', $this->getChangedColumns() ) ) { $strSql .= ' control_record_type_code = ' . $this->sqlControlRecordTypeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_batch_count = ' . $this->sqlControlBatchCount() . ','; } elseif( true == array_key_exists( 'ControlBatchCount', $this->getChangedColumns() ) ) { $strSql .= ' control_batch_count = ' . $this->sqlControlBatchCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_block_count = ' . $this->sqlControlBlockCount() . ','; } elseif( true == array_key_exists( 'ControlBlockCount', $this->getChangedColumns() ) ) { $strSql .= ' control_block_count = ' . $this->sqlControlBlockCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_entry_addenda_count = ' . $this->sqlControlEntryAddendaCount() . ','; } elseif( true == array_key_exists( 'ControlEntryAddendaCount', $this->getChangedColumns() ) ) { $strSql .= ' control_entry_addenda_count = ' . $this->sqlControlEntryAddendaCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_entry_hash = ' . $this->sqlControlEntryHash() . ','; } elseif( true == array_key_exists( 'ControlEntryHash', $this->getChangedColumns() ) ) { $strSql .= ' control_entry_hash = ' . $this->sqlControlEntryHash() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_total_debit_entry_amount = ' . $this->sqlControlTotalDebitEntryAmount() . ','; } elseif( true == array_key_exists( 'ControlTotalDebitEntryAmount', $this->getChangedColumns() ) ) { $strSql .= ' control_total_debit_entry_amount = ' . $this->sqlControlTotalDebitEntryAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_total_credit_entry_amount = ' . $this->sqlControlTotalCreditEntryAmount() . ','; } elseif( true == array_key_exists( 'ControlTotalCreditEntryAmount', $this->getChangedColumns() ) ) { $strSql .= ' control_total_credit_entry_amount = ' . $this->sqlControlTotalCreditEntryAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_reserved = ' . $this->sqlControlReserved() . ','; } elseif( true == array_key_exists( 'ControlReserved', $this->getChangedColumns() ) ) { $strSql .= ' control_reserved = ' . $this->sqlControlReserved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_file_datetime = ' . $this->sqlNachaFileDatetime() . ','; } elseif( true == array_key_exists( 'NachaFileDatetime', $this->getChangedColumns() ) ) { $strSql .= ' nacha_file_datetime = ' . $this->sqlNachaFileDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmitted_on = ' . $this->sqlTransmittedOn() . ','; } elseif( true == array_key_exists( 'TransmittedOn', $this->getChangedColumns() ) ) { $strSql .= ' transmitted_on = ' . $this->sqlTransmittedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; } elseif( true == array_key_exists( 'ConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returns_processed_on = ' . $this->sqlReturnsProcessedOn() . ','; } elseif( true == array_key_exists( 'ReturnsProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' returns_processed_on = ' . $this->sqlReturnsProcessedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'nacha_file_type_id' => $this->getNachaFileTypeId(),
			'nacha_file_status_type_id' => $this->getNachaFileStatusTypeId(),
			'processing_bank_id' => $this->getProcessingBankId(),
			'merchant_gateway_id' => $this->getMerchantGatewayId(),
			'header_record_type_code' => $this->getHeaderRecordTypeCode(),
			'header_priority_code' => $this->getHeaderPriorityCode(),
			'header_immediate_destination' => $this->getHeaderImmediateDestination(),
			'header_immediate_origin' => $this->getHeaderImmediateOrigin(),
			'header_file_creation_date' => $this->getHeaderFileCreationDate(),
			'header_file_creation_time' => $this->getHeaderFileCreationTime(),
			'header_file_id_modifier' => $this->getHeaderFileIdModifier(),
			'header_record_size' => $this->getHeaderRecordSize(),
			'header_blocking_factor' => $this->getHeaderBlockingFactor(),
			'header_format_code' => $this->getHeaderFormatCode(),
			'header_immediate_destination_name' => $this->getHeaderImmediateDestinationName(),
			'header_immediate_origin_name' => $this->getHeaderImmediateOriginName(),
			'header_reference_code' => $this->getHeaderReferenceCode(),
			'control_record_type_code' => $this->getControlRecordTypeCode(),
			'control_batch_count' => $this->getControlBatchCount(),
			'control_block_count' => $this->getControlBlockCount(),
			'control_entry_addenda_count' => $this->getControlEntryAddendaCount(),
			'control_entry_hash' => $this->getControlEntryHash(),
			'control_total_debit_entry_amount' => $this->getControlTotalDebitEntryAmount(),
			'control_total_credit_entry_amount' => $this->getControlTotalCreditEntryAmount(),
			'control_reserved' => $this->getControlReserved(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'nacha_file_datetime' => $this->getNachaFileDatetime(),
			'transmitted_on' => $this->getTransmittedOn(),
			'confirmed_on' => $this->getConfirmedOn(),
			'returns_processed_on' => $this->getReturnsProcessedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>