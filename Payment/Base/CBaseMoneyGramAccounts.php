<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMoneyGramAccounts
 * Do not add any new functions to this class.
 */

class CBaseMoneyGramAccounts extends CEosPluralBase {

	/**
	 * @return CMoneyGramAccount[]
	 */
	public static function fetchMoneyGramAccounts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMoneyGramAccount::class, $objDatabase );
	}

	/**
	 * @return CMoneyGramAccount
	 */
	public static function fetchMoneyGramAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMoneyGramAccount::class, $objDatabase );
	}

	public static function fetchMoneyGramAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'money_gram_accounts', $objDatabase );
	}

	public static function fetchMoneyGramAccountById( $intId, $objDatabase ) {
		return self::fetchMoneyGramAccount( sprintf( 'SELECT * FROM money_gram_accounts WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMoneyGramAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchMoneyGramAccounts( sprintf( 'SELECT * FROM money_gram_accounts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyGramAccountsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMoneyGramAccounts( sprintf( 'SELECT * FROM money_gram_accounts WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchMoneyGramAccountsByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchMoneyGramAccounts( sprintf( 'SELECT * FROM money_gram_accounts WHERE customer_id = %d', ( int ) $intCustomerId ), $objDatabase );
	}

	public static function fetchMoneyGramAccountsByLeaseId( $intLeaseId, $objDatabase ) {
		return self::fetchMoneyGramAccounts( sprintf( 'SELECT * FROM money_gram_accounts WHERE lease_id = %d', ( int ) $intLeaseId ), $objDatabase );
	}

}
?>