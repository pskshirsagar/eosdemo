<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CDistributionTransactions
 * Do not add any new functions to this class.
 */

class CBaseDistributionTransactions extends CEosPluralBase {

	/**
	 * @return CDistributionTransaction[]
	 */
	public static function fetchDistributionTransactions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CDistributionTransaction::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CDistributionTransaction
	 */
	public static function fetchDistributionTransaction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDistributionTransaction::class, $objDatabase );
	}

	public static function fetchDistributionTransactionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'distribution_transactions', $objDatabase );
	}

	public static function fetchDistributionTransactionById( $intId, $objDatabase ) {
		return self::fetchDistributionTransaction( sprintf( 'SELECT * FROM distribution_transactions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDistributionTransactionsByCid( $intCid, $objDatabase ) {
		return self::fetchDistributionTransactions( sprintf( 'SELECT * FROM distribution_transactions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDistributionTransactionsByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchDistributionTransactions( sprintf( 'SELECT * FROM distribution_transactions WHERE account_id = %d', ( int ) $intAccountId ), $objDatabase );
	}

	public static function fetchDistributionTransactionsByCompanyMerchantAccountId( $intCompanyMerchantAccountId, $objDatabase ) {
		return self::fetchDistributionTransactions( sprintf( 'SELECT * FROM distribution_transactions WHERE company_merchant_account_id = %d', ( int ) $intCompanyMerchantAccountId ), $objDatabase );
	}

	public static function fetchDistributionTransactionsBySettlementDistributionId( $intSettlementDistributionId, $objDatabase ) {
		return self::fetchDistributionTransactions( sprintf( 'SELECT * FROM distribution_transactions WHERE settlement_distribution_id = %d', ( int ) $intSettlementDistributionId ), $objDatabase );
	}

	public static function fetchDistributionTransactionsByPaymentTransactionTypeId( $intPaymentTransactionTypeId, $objDatabase ) {
		return self::fetchDistributionTransactions( sprintf( 'SELECT * FROM distribution_transactions WHERE payment_transaction_type_id = %d', ( int ) $intPaymentTransactionTypeId ), $objDatabase );
	}

}
?>