<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CEftBatchStatusTypes
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CEftBatchStatusTypes extends CBaseEftBatchStatusTypes {

	public static function fetchEftBatchStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEftBatchStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEftBatchStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEftBatchStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>