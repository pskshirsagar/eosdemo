<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CProcessingBankAccounts
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CProcessingBankAccounts extends CBaseProcessingBankAccounts {

    public static function fetchProcessingBankAccountsByIds( $arrintProcessingBankAccountIds, $objPaymentDatabase ) {
        if( false == valArr( $arrintProcessingBankAccountIds ) ) return NULL;
        $strSql = 'SELECT * FROM processing_bank_accounts WHERE id IN ( ' . implode( ',', $arrintProcessingBankAccountIds ) . ' )';
        return self::fetchProcessingBankAccounts( $strSql, $objPaymentDatabase );
    }

	public static function fetchBankIntermediaryProcessingBankAccountByAccount( $objAccount, $objPaymentDatabase ) {

    	$strSql = 'SELECT
    					*
    				FROM
    					processing_bank_accounts
    			   WHERE
    			   	   cid IS NULL
	    			   AND fund_movement_type_id = ' . ( int ) CFundMovementType::BANK_INTERMEDIARY_MOVEMENT . '
	    			   AND routing_number = \'' . addslashes( trim( $objAccount->getCheckRoutingNumber() ) ) . '\'
	    			   AND account_number_bindex = \'' . \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( trim( $objAccount->getCheckAccountNumber() ), CONFIG_SODIUM_KEY_BLIND_INDEX ) . '\'
	    			   AND deleted_on IS NULL
	    			ORDER BY id DESC
	    			LIMIT 1';

       return self::fetchProcessingBankAccount( $strSql, $objPaymentDatabase );
	}

	public static function fetchBankIntermediaryProcessingBankAccounts( $objPaymentDatabase ) {

    	$strSql = 'SELECT
    					*
    				FROM
    					processing_bank_accounts
    			   WHERE
    			   	   cid IS NULL
	    			   AND fund_movement_type_id = ' . ( int ) CFundMovementType::BANK_INTERMEDIARY_MOVEMENT . '
	    			   AND deleted_on IS NULL
	    			ORDER BY id DESC';

       return self::fetchProcessingBankAccounts( $strSql, $objPaymentDatabase );
	}

	public static function fetchPaginatedProcessingBankAccounts( $intPageNo, $intPageSize, $objPaymentDatabase, $intShowDisabledData = 0 ) {

    	$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    	$intLimit = ( int ) $intPageSize;

    	$strSql = 'SELECT
			 			pba.id,
						pba.cid,
		    			c.company_name as company_name,
		    			pba.name,
						pba.description,
						pba.account_number_encrypted,
			    		pba.requires_reconciliation,
			   			( SELECT to_date FROM reconciliations WHERE processing_bank_account_id = pba.id ORDER BY created_on DESC LIMIT 1 ) AS last_reconciled_on,
    					( SELECT id FROM reconciliations WHERE processing_bank_account_id = pba.id AND is_pending = 1 LIMIT 1 ) AS pending_reconciliation_id
			   		FROM
			    		processing_bank_accounts pba
			    		LEFT OUTER JOIN clients c ON ( pba.cid = c.id )
				    WHERE '
			    		. ( ( 0 == $intShowDisabledData ) ? ' pba.requires_reconciliation = 1 AND pba.deleted_on IS NULL ' : ' 1 = 1 ' ) . '
			    		AND ( c.id IS NULL OR c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
			    		AND pba.is_published = 1
				ORDER BY
					fund_movement_type_id DESC,
					company_name ASC,
					lower( pba.name )
				OFFSET ' . ( int ) $intOffset . '
				LIMIT ' . $intLimit;

	    return self::fetchProcessingBankAccounts( $strSql, $objPaymentDatabase );
    }

    public static function fetchPaginatedProcessingBankAccountsCount( $objPaymentDatabase, $intShowDisabledData = 0 ) {

    	  $strSql = 'SELECT
			 			count( pba.id ) as count
			   		FROM
			    		processing_bank_accounts pba
			    		LEFT OUTER JOIN clients c ON ( pba.cid = c.id )
				    WHERE '
			    		. ( ( 0 == $intShowDisabledData ) ? ' pba.requires_reconciliation = 1 AND pba.deleted_on IS NULL ' : ' 1 = 1 ' ) . '
			    		AND ( c.id IS NULL OR c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
			    		AND pba.is_published = 1';

		$arrstrData = fetchData( $strSql, $objPaymentDatabase );

    	if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

    	return 0;
    }

    public static function fetchSimpleProcessingBankAccountsWithoutCids( $objPaymentDatabase ) {

        $strSql = 'SELECT
                        id,
                        name,
                        description
                    FROM
                        processing_bank_accounts
                    WHERE is_published = 1 AND cid IS NULL';

        $arrstrData = ( array ) fetchData( $strSql, $objPaymentDatabase );
        return $arrstrData;
    }

    public static function fetchProcessingBankAccountIdForMerchantGatewayId( $intMerchantGatewayId, $intProcessingBankId, $boolIsAnticipatory = false ) {
    	switch( $intMerchantGatewayId ) {
    		case CMerchantGateway::FIRST_REGIONAL_ACH:
    			return CProcessingBankAccount::FRB_CENTRAL_ACH;

    		case CMerchantGateway::ECHO_ACH:
    		case CMerchantGateway::ZIONS_ACH:
    			return CProcessingBankAccount::ZIONS_CENTRAL_ACH;

    		case CMerchantGateway::FIRST_REGIONAL_CHECK21:
    			return CProcessingBankAccount::FRB_CENTRAL_CHECK21;

    		case CMerchantGateway::ZIONS_CHECK21:
    			if( $boolIsAnticipatory ) {
    				return CProcessingBankAccount::ZIONS_CENTRAL_CHECK21;
    			} else {
    				return CProcessingBankAccount::ZIONS_CENTRAL_CHECK21;
    			}

    		case CMerchantGateway::ECHO_CREDIT_CARD:
    		case CMerchantGateway::VITAL:
    		case CMerchantGateway::EPX_CREDIT_CARD:
    		case CMerchantGateway::VANTIV_CREDIT_CARD:
    			switch( $intProcessingBankId ) {
    				case CProcessingBank::ZIONS_BANK:
    					return CProcessingBankAccount::ZIONS_CREDIT_CARD;

    				case CProcessingBank::FIFTH_THIRD:
    					return CProcessingBankAccount::ZIONS_CREDIT_CARD;

					case CProcessingBank::BANK_OF_MONTREAL:
						return CProcessingBankAccount::BANK_OF_MONTREAL_ACH_CREDIT_CARD;

    				default:
    					trigger_error( 'Unhandled merchant gateway when fetching processing bank account id: ' . ( int ) $intMerchantGatewayId );
    					return CProcessingBankAccount::ZIONS_CREDIT_CARD;
    			}

    		case CMerchantGateway::PAY_NEAR_ME:
    			return CProcessingBankAccount::ZIONS_CENTRAL_ACH;

    		case CMerchantGateway::FIFTH_THIRD_ACH:
    			return CProcessingBankAccount::FIFTH_THIRD_ACH;

    		case CMerchantGateway::FIFTH_THIRD_CHECK21:
    			return CProcessingBankAccount::FIFTH_THIRD_CHECK21;

    		case CMerchantGateway::FIRST_REGIONAL_WU_EMO:
    		case CMerchantGateway::MONEY_GRAM:
    			switch( $intProcessingBankId ) {
    				case CProcessingBank::ZIONS_BANK:
    					return CProcessingBankAccount::ZIONS_CENTRAL_MONEYGRAM;

    				case CProcessingBank::FIFTH_THIRD:
    					return CProcessingBankAccount::ZIONS_CENTRAL_MONEYGRAM;

    				default:
    					trigger_error( 'Unhandled merchant gateway when fetching processing bank account id: ' . ( int ) $intMerchantGatewayId );
    					return CProcessingBankAccount::ZIONS_CENTRAL_MONEYGRAM;
    			}

			case CMerchantGateway::WORLDPAY_INTL_CREDIT_CARD:
				switch( $intProcessingBankId ) {
					case CProcessingBank::INTERNATIONAL:
					default:
						return CProcessingBankAccount::INTERNATIONAL;
				}

			case CMerchantGateway::SEPA_DIRECT_DEBIT:
				switch( $intProcessingBankId ) {
					case CProcessingBank::INTERNATIONAL:
					default:
						return CProcessingBankAccount::INTERNATIONAL;
				}

			case CMerchantGateway::WORLDPAY_DIRECT_DEBIT:
				switch( $intProcessingBankId ) {
					case CProcessingBank::BANK_OF_MONTREAL:
					default:
						return CProcessingBankAccount::BANK_OF_MONTREAL_ACH_CREDIT_CARD;
				}

    		case CMerchantGateway::LITLE_CREDIT_CARD:
    			switch( $intProcessingBankId ) {
    				case CProcessingBank::ZIONS_BANK:
    					return CProcessingBankAccount::ZIONS_CREDIT_CARD_LITLE;

    				case CProcessingBank::FIFTH_THIRD:
    					return CProcessingBankAccount::ZIONS_CREDIT_CARD_LITLE;

					case CProcessingBank::BANK_OF_MONTREAL:
						return CProcessingBankAccount::BANK_OF_MONTREAL_ACH_CREDIT_CARD;

    				default:
    					trigger_error( 'Unhandled merchant gateway when fetching processing bank account id: ' . ( int ) $intMerchantGatewayId );
    					return CProcessingBankAccount::ZIONS_CREDIT_CARD_LITLE;
    			}

    		default:
    			trigger_error( 'Unhandled merchant gateway when fetching processing bank account id: ' . ( int ) $intMerchantGatewayId );
    			return CProcessingBankAccount::ZIONS_CENTRAL_ACH;
    	}
    }

}
?>