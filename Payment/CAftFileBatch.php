<?php

class CAftFileBatch extends CBaseAftFileBatch {

	const CREDIT_BATCH_TYPE = 'C';
	const DEBIT_BATCH_TYPE = 'D';

	protected $m_arrobjAftDetailRecords;

	public function setReturnsAccountNumber( $strReturnsAccountNumber ) {
		if( !valStr( $strReturnsAccountNumber ) ) {
			return;
		}

		$this->setReturnsAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strReturnsAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
	}

	public function getReturnsAccountNumber() {
		if( !valStr( $this->getReturnsAccountNumberEncrypted() ) ) {
			return NULL;
		}

		return \Psi\Libraries\Cryptography\CCrypto::createService()->decyrpt( $this->getReturnsAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER );
	}

	public function fetchAftFile( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CAftFiles::createService()->fetchAftFileById( $this->getAftFileId(), $objPaymentDatabase );
	}

	public function createReconciliationEntry( $objPaymentDatabase ) {

		$objAftFile = $this->fetchAftFile( $objPaymentDatabase );
		$intProcessingBankAccountId = NULL;
		$intReconciliationEntryTypeId = NULL;

		switch( $objAftFile->getAftFileTypeId() ) {
			case CAftFileType::SETTLEMENT_DISTRIBUTIONS;
				$intProcessingBankAccountId = CProcessingBankAccount::BANK_OF_MONTREAL_ACH_CREDIT_CARD;
				$intReconciliationEntryTypeId = CReconciliationEntryType::SETTLEMENT_DISTRIBUTIONS;
				break;

			default:
				return false;
		}

		$objReconciliationEntry = new CReconciliationEntry();
		$objReconciliationEntry->setDefaults();
		$objReconciliationEntry->setProcessingBankAccountId( $intProcessingBankAccountId );
		$objReconciliationEntry->setReconciliationEntryTypeId( $intReconciliationEntryTypeId );
		$objReconciliationEntry->setAftFileBatchId( $this->getId() );

		$fltBatchAmount = $this->getBatchAmount() / 100.0;
		if( self::CREDIT_BATCH_TYPE == $this->getBatchPaymentTypeCode() ) {
			$fltBatchAmount = - 1 * $fltBatchAmount;
		}

		$objReconciliationEntry->setTotal( number_format( $fltBatchAmount, 2, '.', '' ) );
		$strPayableYear = substr( $this->getPayableDate(), 1, 2 );
		$strPayableDay = trim( substr( $this->getPayableDate(), 3 ) );
		$objPayableDate = \DateTime::createFromFormat( 'y-z', $strPayableYear . '-' . $strPayableDay );
		$objReconciliationEntry->setMovementDatetime( $objPayableDate->format( 'm/d/Y' ) );

		return $objReconciliationEntry;
	}

	public function getAftDetailRecords() {
		return $this->m_arrobjAftDetailRecords;
	}

	public function loadAftDetailRecords( $objPaymentDatabase ) {
		$this->m_arrobjAftDetailRecords = \Psi\Eos\Payment\CAftDetailRecords::createService()->fetchAftDetailRecordsByAftFileBatchId( $this->getId(), $objPaymentDatabase );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAftFileId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchPaymentTypeCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionTypeCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPayableDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginatorShortName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginatorLongName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnsInstitutionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnsAccountNumberEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchRecordCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>