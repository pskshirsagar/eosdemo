<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentImages
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CArPaymentImages extends CBaseArPaymentImages {

	public static function fetchArPaymentImagesByArPaymentId( $intArPaymentId, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM ar_payment_images WHERE ar_payment_id = ' . ( int ) $intArPaymentId . ' ORDER BY payment_image_type_id';

		return self::fetchArPaymentImages( $strSql, $objPaymentDatabase );
	}

	public static function fetchArPaymentImagesByArPaymentIds( $arrintArPaymentIds, $objPaymentDatabase ) {
		if( false == valArr( $arrintArPaymentIds ) ) return NULL;

		$strSql = 'SELECT
						api.*,
						mam.processing_bank_account_id as processing_bank_account_id,
						pba.processing_bank_id as processing_bank_id
					FROM
						ar_payment_images api,
						ar_payments ap,
						merchant_account_methods mam,
						processing_bank_accounts pba
					WHERE
						api.ar_payment_id = ap.id
						AND ap.company_merchant_account_id = mam.company_merchant_account_id
						AND ap.payment_type_id = mam.payment_type_id
						AND ap.payment_medium_id = mam.payment_medium_id
						AND mam.processing_bank_account_id = pba.id
						AND api.ar_payment_id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' );';

		return self::fetchArPaymentImages( $strSql, $objPaymentDatabase );
	}

	public static function fetchOrderedArPaymentImagesByArPaymentIds( $arrintArPaymentIds, $objPaymentDatabase ) {
		if( false == valArr( $arrintArPaymentIds ) ) return NULL;

		$strSql = 'SELECT
						api.*,
						mam.processing_bank_account_id as processing_bank_account_id,
						pba.processing_bank_id as processing_bank_id
					FROM
						ar_payment_images api,
						ar_payments ap,
						merchant_account_methods mam,
						processing_bank_accounts pba
					WHERE
						api.ar_payment_id = ap.id
						AND ap.company_merchant_account_id = mam.company_merchant_account_id
						AND ap.payment_type_id = mam.payment_type_id
						AND ap.payment_medium_id = mam.payment_medium_id
						AND mam.processing_bank_account_id = pba.id
						AND api.ar_payment_id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' ) ORDER BY ap.id, api.payment_image_type_id';
		return self::fetchArPaymentImages( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnBatchedEndorsedArPaymentImagesByCompanyMerchantAccountId( $intCompanyMerchantAccountId, $objPaymentDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (cpi.ar_payment_id)
						cpi.*
					FROM
						ar_payment_images cpi,
						ar_payments cp,
						clients c
					WHERE
						cpi.ar_payment_id = cp.id
						AND cp.cid = c.id
						AND cpi.cid = c.id
						AND cp.batched_on IS NULL
						AND cpi.endorsed_on IS NOT NULL
						AND cpi.image_name ILIKE \'endorsed_%\'
						AND c.company_status_type_id = ' . ( int ) CCompanyStatusType::CLIENT . '
						AND cp.company_merchant_account_id = ' . ( int ) $intCompanyMerchantAccountId . '
						AND cp.payment_status_type_id IN ( ' . CPaymentStatusType::CAPTURED . ',' . CPaymentStatusType::AUTHORIZED . ',' . CPaymentStatusType::BATCHING . ' )
						AND cp.payment_type_id = ' . ( int ) CPaymentType::CHECK_21 . '
						AND cpi.payment_image_type_id = ' . ( int ) CPaymentImageType::REVERSE;

		return self::fetchArPaymentImages( $strSql, $objPaymentDatabase );
	}

	public static function fetchFrontArPaymentImagesByArPaymentIds( $arrintArPaymentIds, $objPaymentDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ar_payment_images
					WHERE
						ar_payment_id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' )
						AND payment_image_type_id = ' . ( int ) CPaymentImageType::FRONT . '
					ORDER BY
						id';

		return self::fetchArPaymentImages( $strSql, $objPaymentDatabase );
	}

	public static function fetchFrontArPaymentImagePathsByArPaymentIds( $arrintArPaymentIds, $objPaymentDatabase ) {

		$strSql = 'SELECT
						image_path, image_name, payment_image_type_id
					FROM
						ar_payment_images
					WHERE
						ar_payment_id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' )
						AND payment_image_type_id IN ( ' . ( int ) CPaymentImageType::FRONT . ',' . CPaymentImageType::REVERSE . ' )
					ORDER BY
						id, payment_image_type_id';

		return fetchData( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnEndorsedLockedArPaymentImagesByCompanyMerchantAccountId( $intCompanyMerchantAccountId, $objPaymentDatabase ) {

		$strSql = 'SELECT
						api.*
					FROM
						ar_payment_images api
						JOIN ar_payments ap ON (api.ar_payment_id = ap.id AND api.cid = ap.cid)
						JOIN clients c ON (ap.cid = c.id)
					WHERE
						api.endorsed_on IS NULL
						AND api.locked_on IS NOT NULL
						AND api.locked_on > NOW() - INTERVAL \'1 hour\'
						AND c.company_status_type_id = ' . ( int ) CCompanyStatusType::CLIENT . '
						AND ap.company_merchant_account_id = ' . ( int ) $intCompanyMerchantAccountId . '
						AND ap.payment_status_type_id IN ( ' . CPaymentStatusType::CAPTURED . ',' . CPaymentStatusType::AUTHORIZED . ',' . CPaymentStatusType::BATCHING . ' )
						AND ap.payment_type_id = ' . ( int ) CPaymentType::CHECK_21 . '
						AND api.payment_image_type_id = ' . ( int ) CPaymentImageType::REVERSE;

		return self::fetchArPaymentImages( $strSql, $objPaymentDatabase );
	}

	public static function fetchArPaymentImagesByCidByDate( $intCid, $strCreatedOnDate,  $objPaymentDatabase ) {

		if( false == is_numeric( $intCid ) || false == valStr( $strCreatedOnDate ) ) return NULL;

		$strSql = 'SELECT
						api.*
					FROM
						ar_payment_images api
					WHERE
						api.cid = ' . ( int ) $intCid . '
						AND to_char( api.created_on, \'YYYY-MM-DD\' ) like \'' . $strCreatedOnDate . '%\'';

		return self::fetchArPaymentImages( $strSql, $objPaymentDatabase );
	}

	public static function fetchCardSignatureArPaymentImageByArPaymentId( $arrintArPaymentId, $objPaymentDatabase ) {

		if( false == valId( $arrintArPaymentId ) ) return NULL;

		$strSql = 'SELECT
						api.image_content
					FROM
						ar_payment_images api
					WHERE
						api.ar_payment_id  = ' . $arrintArPaymentId . '
						AND api.payment_image_type_id = ' . ( int ) CPaymentImageType::SIGNATURE . '';

		return self::fetchArPaymentImage( $strSql, $objPaymentDatabase );
	}

	public static function fetchArPaymentImagesByCidByArPaymentIdByDate( $intCid, $intArPaymentId, $strCreatedOnDate, $objPaymentDatabase ) {

		if( false == is_numeric( $intCid ) || false == valStr( $strCreatedOnDate ) || false == is_numeric( $intArPaymentId ) ) return NULL;

		$strSql = 'SELECT
						api.image_path,
						api.image_name
					FROM
						ar_payment_images api
					WHERE
						api.cid = ' . ( int ) $intCid . '
						AND api.ar_payment_id = ' . $intArPaymentId . '
						AND to_char( api.created_on, \'YYYY-MM-DD\' ) like \'' . $strCreatedOnDate . '%\'
					ORDER BY payment_image_type_id';
		return self::fetchArPaymentImages( $strSql, $objPaymentDatabase );
	}

	public static function setNoCacheHeaders() {
		header( 'Expires: Tue, 03 Jul 2001 06:00:00 GMT' );
		header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
		header( 'Cache-Control: no-store, no-cache, must-revalidate, max-age=0' );
		header( 'Cache-Control: post-check=0, pre-check=0', false );
		header( 'Pragma: no-cache' );
	}

	public static function setCacheHeaders( $intDuration ) {
		header( 'Pragma: public' );
		header( 'Cache-Control: maxage=' . ( int ) $intDuration );
		header( 'Expires: ' . gmdate( 'D, d M Y H:i:s \G\M\T', strtotime( '+' . $intDuration . ' seconds' ) ) );
		header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s \G\M\T', time() ) );
	}

}

?>