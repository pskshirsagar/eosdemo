<?php

use Psi\Libraries\ExternalFpdf\CFpdf;

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMerchantAccountApplications
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CMerchantAccountApplications extends CBaseMerchantAccountApplications {

    public static function fetchMerchantAccountApplicationByCid( $intCid, $objPaymentDatabase ) {
        return self::fetchMerchantAccountApplications( sprintf( 'SELECT * FROM %s WHERE cid = %d and deleted_on is null', ( string ) 'merchant_account_applications', ( int ) $intCid ), $objPaymentDatabase );
    }

	public static function fetchMerchantApplicationById( $intMerchantAccountApplicationId, $objPaymentDatabase ) {
		return self::fetchMerchantAccountApplicationById( $intMerchantAccountApplicationId, $objPaymentDatabase );
	}

	public static function fetchAllUnsettledCompletedMerchantAccountApplications( $objPaymentDatabase ) {
		$strSql = "SELECT 	maa.*
					FROM 	merchant_account_applications maa,
					        application_key_values akv
					WHERE 	maa.id = akv.merchant_account_application_id
					    AND	maa.deleted_on IS NULL
					    AND akv.key = 'APPLICATION_COMPLETED_ON'
					    AND akv.value IS NOT NULL
					    AND maa.id NOT IN (
					    	SELECT 	m.id
							FROM 	merchant_account_applications m,
					        		application_key_values a
							WHERE 	m.id = a.merchant_account_application_id
					        	AND	m.deleted_on IS NULL
					   	 		AND a.key = 'SUBMIT_SETTLEMENT_INFORMATION_RW'
					    		AND a.value IS NOT NULL
					    )";

		return self::fetchMerchantAccountApplications( $strSql, $objPaymentDatabase );
	}

	/**
	 * Other Functions
	 */

	public static function loadTimeTrackingData( $arrintMerchantAccountApplicationIds, $objPaymentDatabase ) {

       	$arrobjTempApplicationTimeTractDetails  = \Psi\Eos\Payment\CApplicationKeyValues::createService()->fetchApplicationKeyValueByMerchantAccountApplicationIds( $arrintMerchantAccountApplicationIds, $objPaymentDatabase );

       	if( false == valArr( $arrobjTempApplicationTimeTractDetails ) ) return NULL;

        // We are going to calculate 2 different times here
    	// 1. The time it took from start to finish
    	// 2. The time it took for Echo to complete the underwriting
       	foreach( $arrobjTempApplicationTimeTractDetails as $arrobjApplicationTimeTract ) {
       		$intPacketSentToEchoOn = ( false == is_null( $arrobjApplicationTimeTract['packet_sent_to_echo_on'] ) ) ? strtotime( $arrobjApplicationTimeTract['packet_sent_to_echo_on'] ) : time();
       		$intPinReceivedOn	   = ( false == is_null( $arrobjApplicationTimeTract['pin_received_on'] ) ) ? strtotime( $arrobjApplicationTimeTract['pin_received_on'] ) : time();
       		$intInfoRequestSentOn  = ( false == is_null( $arrobjApplicationTimeTract['info_request_sent_on'] ) ) ? strtotime( $arrobjApplicationTimeTract['info_request_sent_on'] ) : time();
       		$intProcessCompleted   = ( false == is_null( $arrobjApplicationTimeTract['application_completed_on'] ) ) ? strtotime( $arrobjApplicationTimeTract['application_completed_on'] ) : time();

       		$arrobjApplicationTimeTractDetails[$arrobjApplicationTimeTract['merchant_account_application_id']]['DaysToCompleteEntireProcess'] = ( ( $intProcessCompleted - $intInfoRequestSentOn ) / ( 3600 * 24 ) );
       		$arrobjApplicationTimeTractDetails[$arrobjApplicationTimeTract['merchant_account_application_id']]['DaysForBankToComplete'] = ( ( $intPinReceivedOn - $intPacketSentToEchoOn ) / ( 3600 * 24 ) );
       	}

		return $arrobjApplicationTimeTractDetails;
    }

    public static function fetchMerchantAccountApplicationsCountByCid( $intCid, $objDatabase ) {

    	if( true == empty( $intCid ) || false == is_numeric( $intCid ) ) return NULL;

    	$strSql = 'SELECT count(id) FROM merchant_account_applications mca WHERE mca.cid = ' . ( int ) $intCid;

    	$arrintResponse = fetchData( $strSql, $objDatabase );

    	if( true == isset ( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

    }

    public static function fetchPaginatedMerchantAccountApplicationsByCid( $intCid, $objPagination, $objDatabase ) {

		$intLimit  = $objPagination->getPageSize();
		$intOffset = ( true == is_numeric( $objPagination->getPageNo() ) && true == is_numeric( $objPagination->getPageSize() ) ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;

		$strSql = 'SELECT mca.*
					FROM  merchant_account_applications mca
					WHERE mca.cid = ' . ( int ) $intCid . '
						  AND mca.deleted_on IS NULL
					ORDER BY mca.created_on DESC
					OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchMerchantAccountApplications( $strSql, $objDatabase );

    }

    public static function fetchCompletedMerchantAccountApplicationByCid( $intCid, $objPaymentDatabase ) {
    	$strSql = 'SELECT 	maa.*
					FROM 	merchant_account_applications maa
					JOIN application_key_values akv ON maa.id = akv.merchant_account_application_id
					WHERE
					    maa.deleted_on IS NULL
					    AND akv.key = \'APPLICATION_COMPLETED_ON\'
					    AND akv.value IS NOT NULL
					    AND maa.cid = ' . ( int ) $intCid . '
					ORDER BY updated_on DESC
					LIMIT 1';

    	return self::fetchMerchantAccountApplication( $strSql, $objPaymentDatabase );
    }

	public static function fetchMerchantAccountApplicationByUsername( $strUsername, $objPaymentDatabase ) {
		$strSql = "SELECT 	maa.*
					FROM 	merchant_account_applications maa
					WHERE
					    maa.deleted_on IS NULL
					    AND maa.username = '" . ( string ) $strUsername . "'
					ORDER BY updated_on DESC
					LIMIT 1";

		return self::fetchMerchantAccountApplication( $strSql, $objPaymentDatabase );
	}

	public function fetchAllMerchantAccountApplicationByUsername( $strUsername, $objPaymentDatabase ) {
		$strSql = "SELECT 	maa.*
					FROM 	merchant_account_applications maa
					WHERE
					    maa.username = '" . ( string ) $strUsername . "'
					ORDER BY updated_on DESC";

		return self::fetchMerchantAccountApplications( $strSql, $objPaymentDatabase );
	}

	public function fetchLatestMerchantAccountApplicationByCid( $intCid, $objPaymentDatabase ) {
		$strSql = 'SELECT 	maa.*
					FROM 	merchant_account_applications maa
					WHERE
					    maa.cid = ' . ( int ) $intCid . '
					ORDER BY created_on DESC
					LIMIT 1';

		return self::fetchMerchantAccountApplications( $strSql, $objPaymentDatabase );
	}

    public static function generateMerchantAccountApplicationCoverPage( $objPdf ) {

    	$objPdf->AddPage();

    	// Code to generate cover page

    	$objPdf->SetFont( 'Arial', 'B', 10 );
    	$objPdf->setTextColor( 0, 0, 0 );
    	$objPdf->SetFillColor( 255, 255, 255 );

    	$objPdf->SetXY( 50, 50 );
    	$objPdf->SetFont( 'Arial', '', 12 );
    	$objPdf->SetFillColor( 255, 255, 255 );
    	$objPdf->setTextColor( 0, 0, 0 );
    	$objPdf->Cell( 200, 20, 'Dear Client,', '', 1, 'L', 1 );

    	$objPdf->SetXY( 50, $objPdf->GetY() + 20 );
    	$objPdf->MultiCell( 500, 20, 'Please see below, we have noted some guidelines for filling out the application and submitting the underwriting package.' );

    	$objPdf->SetXY( 50, $objPdf->GetY() + 20 );
    	$objPdf->MultiCell( 520, 20, 'In order to expedite the process, please fill out the attached application, print it, get the officer\'s signature on it, and then submit it to our office along with the required supporting documents noted on the application. We normally underwrite the corporate entity then create separate merchant accounts for each property listed on the Resident Pay contract.' );

    	$objPdf->SetXY( 50, $objPdf->GetY() + 20 );
    	$objPdf->Cell( 200, 20, 'Please note: ', '', 1, 'L', 1 );

    	$objPdf->SetXY( 50, $objPdf->GetY() + 20 );
    	$objPdf->MultiCell( 520, 20, '1) The officer entered must also sign the application and MUST be one of the officers as noted on the application.This person will be the "Approved Signer" for any future changes to the merchant account(s). This Approved Signer can also submit additional approved signers as needed.' );

    	$objPdf->SetXY( 50, $objPdf->GetY() + 20 );
    	$objPdf->MultiCell( 520, 20, '2)All supporting documents must show the same entity name as the entity name we are underwriting. ' );

    	$objPdf->SetXY( 50, $objPdf->GetY() + 20 );
    	$objPdf->MultiCell( 520, 20, 'When you have everything together, you can submit the information by email at ' . CSystemEmail::MERCHANTSERVICES_EMAIL_ADDRESS . ', fax it to Merchant Services at 1-801-437-3329 or mail it to Merchant Services / ' . CONFIG_COMPANY_NAME . ' Int. at 4205 Chapel Ridge Rd, Lehi, UT 84043. ' );

    	$objPdf->SetXY( 50, $objPdf->GetY() + 20 );
    	$objPdf->MultiCell( 520, 20, 'For any questions or concerns, please contact us at ' . CSystemEmail::MERCHANTSERVICES_EMAIL_ADDRESS . '. ' );

    	$objPdf->SetXY( 50, $objPdf->GetY() + 20 );
    	$objPdf->Cell( 520, 20, 'WHY UNDERWRITING', 'LTR', 1, 'C', 1 );
    	$objPdf->SetXY( 50, $objPdf->GetY() );
    	$objPdf->Cell( 520, 15, ' ', 'LR', 1, 'C', 1 );
    	$objPdf->SetXY( 50, $objPdf->GetY() );
    	$objPdf->MultiCell( 520, 20, 'The Credit Card Companies, Processing Gateways and their banks, will not do payment processing for a client unless they are underwritten.' . CONFIG_COMPANY_NAME . ' has made every effort to minimize the time, effort and required documents required for underwriting. ', 'LRB', 1, 'L', 1 );

    	return $objPdf;
    }

    public static function generateMerchantAccountApplicationForm( $objClient,$objMerchantAccountApplication, $arrobjApplicationKeyValues, $boolIsForCoverPage = false ) {

    	$strUploadPath = CONFIG_CACHE_DIRECTORY . 'merchant_account_applications/';

    	if( false == is_dir( $strUploadPath ) && false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
    		trigger_error( 'Couldn\'t create directory.', E_USER_ERROR );
    		return false;
    	}

    	$strFileName 			= 'merchant_account_application_' . $objClient->getId() . '.pdf';
    	$strAttachmentFilePath	= $strUploadPath . $strFileName;

    	$objPdf = new CFpdf( 'P', 'pt', 'letter' );

    	$objPdf->SetAuthor( CONFIG_COMPANY_NAME );
    	$objPdf->SetCreator( CONFIG_COMPANY_NAME );
    	$objPdf->SetTitle( 'Merchant Account Application' );

    	if( true == $boolIsForCoverPage ) {
    		// Code to generate cover page
    		$objPdf = CMerchantAccountApplications::generateMerchantAccountApplicationCoverPage( $objPdf );
    	}

    	// Code to generate additional authorized signatories page
    	$objPdf = CMerchantAccountApplications::generateAdditionalAuthorizedSignatoriesEmail( $objClient, $objMerchantAccountApplication, $objPdf, $arrobjApplicationKeyValues );

    	// Method use to generate PDF in file format on specified path.
    	$objPdf->Output( 'F', $strAttachmentFilePath );

    	return $strAttachmentFilePath;
    }

    public static function generateAdditionalAuthorizedSignatoriesEmail( $objClient, $objMerchantAccountApplication, $objPdf, $arrobjApplicationKeyValues ) {

    	if( true == valObj( $objMerchantAccountApplication, 'CMerchantAccountApplication' ) && false == is_null( $objMerchantAccountApplication->getCompanyName() ) ) {
    		$strCompanyName = $objMerchantAccountApplication->getCompanyName();
    	} else {
    		$strCompanyName = $objClient->getCompanyName();
    	}

    	$strPresidentName = '';

    	if( true == valArr( $arrobjApplicationKeyValues ) ) {
    		$arrobjApplicationKeyValues = rekeyObjects( 'Key', $arrobjApplicationKeyValues );

    		if( true == isset( $arrobjApplicationKeyValues['APPROVED_SIGNER_NAME'] ) ) {
    			$strPresidentName = $arrobjApplicationKeyValues['APPROVED_SIGNER_NAME']->getValue();
    		}
    	}

    	// Code to generate additional authorized signatories page
    	$objPdf->AddPage();

    	// Entrata logo
    	$objPdf->Image( PATH_WWW_ROOT . 'Admin/images/logos/' . CONFIG_ENTRATA_LOGO_PREFIX . 'ps_logo_large.jpg', 40, 45, 130 );

    	$objPdf->SetFont( 'Arial', 'B', 8 );
    	$objPdf->setTextColor( 0, 0, 0 );
    	$objPdf->SetFillColor( 225, 225, 225 );

    	$objPdf->SetXY( 300, 43 );
    	$objPdf->Cell( 270, 18, ' QUESTIONS? CALL 801-375-5522', 'LTR', 1, 'L', 1 );

    	$objPdf->SetXY( 300, 59 );
    	$objPdf->Cell( 270, 18, ' MERCHANT : ' . $strCompanyName, 'LRB', 1, 'L', 1 );

    	// reset fond and text settings
    	$objPdf->SetXY( 40, 130 );
    	$objPdf->SetFont( 'Arial', 'B', 15 );
    	$objPdf->SetFillColor( 255, 255, 255 );
    	$objPdf->Cell( 500, 30, 'ADDITIONAL AUTHORIZED SIGNATORIES', '', 1, 'L', 1 );

    	// Set dynamic PDF contents
    	$strDocumentData  = ' If you would like to authorize additional signers on "' . $strCompanyName . '" merchant accounts provided by ' . CONFIG_COMPANY_NAME . ', enter those names here. Any persons identified below will be granted authority to request new merchant accounts as well as edit deposit bank accounts on existing merchant accounts. ';
    	$strDocumentData .= $strCompanyName . ' indemnifies and holds harmless ' . CONFIG_COMPANY_NAME . ' for any losses or damages arising as a result of mis-entered, mis-typed, or otherwise inaccurate or fraudulent deposit account information entered by these persons. ';
    	$strDocumentData .= ' It is the sole responsibility of ' . $strCompanyName . ' to notify ' . CONFIG_COMPANY_NAME . ' in the event that ' . $strCompanyName . ' desires to discontinue authorization for anyone named as an authorized signatory on the account.';

    	// reset fond and text settings
    	$objPdf->SetXY( 38, 165 );
    	$objPdf->SetFont( 'Arial', 'B', 10 );
    	$objPdf->MultiCell( 520, 15, $strDocumentData );

    	$objPdf->SetY( $objPdf->GetY() + 30 );

    	// Additional Authorized Signatory #1
    	$objPdf->SetFont( 'Arial', 'B', 10 );
    	$objPdf->SetXY( 40, $objPdf->GetY() );
    	$objPdf->Cell( 180, 25, 'Additional Authorized Signatory #1:', '', 1, 'L', 1 );

    	$objPdf->SetXY( 230, $objPdf->GetY() );
    	$objPdf->Cell( 140, 20, 'PRINT NAME ', 'T', 1, 'C', 1 );

    	$objPdf->SetXY( 390, $objPdf->GetY() - 20 );
    	$objPdf->Cell( 140, 20, 'SIGN', 'T', 1, 'C', 1 );

    	// Additional Authorized Signatory #2
    	$objPdf->SetFont( 'Arial', 'B', 10 );
    	$objPdf->SetXY( 40, $objPdf->GetY() + 5 );
    	$objPdf->Cell( 180, 25, 'Additional Authorized Signatory #2:', '', 1, 'L', 1 );

    	$objPdf->SetXY( 230, $objPdf->GetY() );
    	$objPdf->Cell( 140, 20, 'PRINT NAME ', 'T', 1, 'C', 1 );

    	$objPdf->SetXY( 390, $objPdf->GetY() - 20 );
    	$objPdf->Cell( 140, 20, 'SIGN', 'T', 1, 'C', 1 );

    	// Additional Authorized Signatory #3
    	$objPdf->SetFont( 'Arial', 'B', 10 );
    	$objPdf->SetXY( 40, $objPdf->GetY() + 5 );
    	$objPdf->Cell( 180, 25, 'Additional Authorized Signatory #3:', '', 1, 'L', 1 );

    	$objPdf->SetXY( 230, $objPdf->GetY() );
    	$objPdf->Cell( 140, 20, 'PRINT NAME ', 'T', 1, 'C', 1 );

    	$objPdf->SetXY( 390, $objPdf->GetY() - 20 );
    	$objPdf->Cell( 140, 20, 'SIGN', 'T', 1, 'C', 1 );

    	// Additional Authorized Signatory #4
    	$objPdf->SetFont( 'Arial', 'B', 10 );
    	$objPdf->SetXY( 40, $objPdf->GetY() + 5 );
    	$objPdf->Cell( 180, 25, 'Additional Authorized Signatory #4:', '', 1, 'L', 1 );

    	$objPdf->SetXY( 230, $objPdf->GetY() );
    	$objPdf->Cell( 140, 20, 'PRINT NAME ', 'T', 1, 'C', 1 );

    	$objPdf->SetXY( 390, $objPdf->GetY() - 20 );
    	$objPdf->Cell( 140, 20, 'SIGN', 'T', 1, 'C', 1 );

    	// Additional Authorized Signatory #5
    	$objPdf->SetFont( 'Arial', 'B', 10 );
    	$objPdf->SetXY( 40, $objPdf->GetY() + 5 );
    	$objPdf->Cell( 180, 25, 'Additional Authorized Signatory #5:', '', 1, 'L', 1 );

    	$objPdf->SetXY( 230, $objPdf->GetY() );
    	$objPdf->Cell( 140, 20, 'PRINT NAME ', 'T', 1, 'C', 1 );

    	$objPdf->SetXY( 390, $objPdf->GetY() - 20 );
    	$objPdf->Cell( 140, 20, 'SIGN', 'T', 1, 'C', 1 );

    	// footer part
    	$objPdf->SetFont( 'Arial', '', 10 );
    	$objPdf->SetXY( 40, -120 );
    	$objPdf->Cell( 180, 25, $strPresidentName, '', 1, 'C', 1 );

    	$objPdf->SetFont( 'Arial', 'B', 10 );
    	$objPdf->SetXY( 410, -120 );
    	$objPdf->Cell( 140, 25, '', '', 1, 'L', 1 );

    	$objPdf->SetXY( 40, $objPdf->GetY() + 5 );
    	$objPdf->Cell( 180, 20, 'Original Authorized Signer', 'T', 1, 'L', 1 );

    	$objPdf->SetXY( 235, $objPdf->GetY() - 20 );
    	$objPdf->Cell( 160, 20, 'SIGNATURE', 'T', 1, 'C', 1 );

    	$objPdf->SetXY( 410, $objPdf->GetY() - 20 );
    	$objPdf->Cell( 140, 20, 'DATE', 'T', 1, 'C', 1 );

    	return $objPdf;
    }

}

?>
