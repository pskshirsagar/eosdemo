<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937ViewDataRecords
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CX937ViewDataRecords extends CBaseX937ViewDataRecords {

	public static function fetchX937ViewDataRecordsByX937CheckDetailRecordId( $intX937CheckDetailRecordId, $objDatabase ) {
		return self::fetchX937ViewDataRecords( sprintf( 'SELECT * FROM x937_view_data_records WHERE x937_check_detail_record_id = %d', ( int ) $intX937CheckDetailRecordId ), $objDatabase );
	}

}
?>