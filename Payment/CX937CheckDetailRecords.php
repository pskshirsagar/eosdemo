<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937CheckDetailRecords
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */
class CX937CheckDetailRecords extends CBaseX937CheckDetailRecords {

	public static function fetchX937CheckDetailRecordsByX937FileIdOrderedById( $intX937FileId, $objPaymentDatabase ) {
        return self::fetchX937CheckDetailRecords( sprintf( 'SELECT * FROM %s WHERE x937_file_id = %d ORDER BY id', 'x937_check_detail_records', ( int ) $intX937FileId ), $objPaymentDatabase );
    }

	public static function fetchFirstX937CheckDetailRecordByX937FileId( $intX937FileId, $objPaymentDatabase ) {
        return self::fetchX937CheckDetailRecord( sprintf( 'SELECT * FROM %s WHERE x937_file_id = %d ORDER BY id LIMIT 1', 'x937_check_detail_records', ( int ) $intX937FileId ), $objPaymentDatabase );
    }

    public static function fetchX937CheckDetailRecordsByX937FileId( $intX937FileId, $objDatabase ) {
    	return self::fetchX937CheckDetailRecords( sprintf( 'SELECT * FROM x937_check_detail_records WHERE x937_file_id = %d', ( int ) $intX937FileId ), $objDatabase );
    }

    public static function fetchX937CheckDetailRecordsByArPaymentId( $intArPaymentId, $objDatabase ) {
    	return self::fetchX937CheckDetailRecords( sprintf( 'SELECT * FROM x937_check_detail_records WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
    }

}
?>