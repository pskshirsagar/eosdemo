<?php

class CClearingBatchType extends CBaseClearingBatchType {

	const CONVENIENCE_FEE_CLEARING 	= 1;
	const CLIENT_DONATION_CLEARING 	= 2;
	const PS_DONATION_CLEARING		= 3;
}
?>