<?php

class CX937ReturnFile extends CBaseX937ReturnFile {

	protected $m_fltTotal;
	protected $m_strParsedFilePath;
	protected $m_arrobjX937ReturnItems;

	public function __construct() {
        parent::__construct();

        $this->m_fltTotal = 0;
		$this->m_arrobjX937ReturnItems		= array();

        return;
    }

	public function getFilePath() {
		return PATH_MOUNTS . $this->m_strFilePath;
	}

    public function getTotal() {
    	return $this->m_fltTotal;
    }

	public function getX937ReturnItems() {
		return $this->m_arrobjX937ReturnItems;
	}

	public function getParsedFilePath() {
    	if( true == valStr( $this->m_strParsedFilePath ) ) {
    		return $this->m_strParsedFilePath;
    	}

    	$this->m_strParsedFilePath = str_replace( 'Pending', 'Parsed', $this->m_strFilePath ) . date( 'Y' ) . '/' . date( 'm' ) . '/';
    	return $this->m_strParsedFilePath;
    }

	public function getFullPath() {
		$strFullPath = $this->getFilePath() . $this->getId() . '_' . $this->getFileName();
		if( false == file_exists( $strFullPath ) ) {
			$strFullPath = $this->getFilePath() . $this->getFileName();
		}

		return $strFullPath;
	}

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['total'] ) ) $this->setTotal( $arrmixValues['total'] );

        return;
    }

    public function setTotal( $fltTotal ) {
		$this->m_fltTotal = $fltTotal;
    }

	public function addX937ReturnItem( $objX937ReturnItem ) {
		$this->m_arrobjX937ReturnItems[$objX937ReturnItem->getId()] = $objX937ReturnItem;
	}

    /**
     * Create Functions
     */

	public function createX937ReturnItem( $intProcessingBankId ) {

		if( CProcessingBank::FIFTH_THIRD == $intProcessingBankId ) {
			$objX937ReturnItem = new CFifthThirdX937ReturnItem();
		} else {
			$objX937ReturnItem = new CX937ReturnItem();
		}
		$objX937ReturnItem->setDefaults();
		$objX937ReturnItem->setX937ReturnFileId( $this->getId() );
		$objX937ReturnItem->setReturnTypeId( CReturnType::GENERIC_CHECK21_RETURN_TYPE );
		return $objX937ReturnItem;
	}

	public function createReconciliationEntry( $objPaymentDatabase ) {

		$objReconciliationEntry = new CReconciliationEntry();
		$objReconciliationEntry->setProcessingBankAccountId( $this->getProcessingBankAccountId() );
		$objReconciliationEntry->setX937ReturnFileId( $this->getId() );
		$objReconciliationEntry->setTotal( -1 * abs( $this->getTotal() ) );

		// In this table we need to add x937_file_typ_id so that we can have company payments return in future.

		$objReconciliationEntry->setReconciliationEntryTypeId( CReconciliationEntryType::AR_PAYMENTS_CHECK_21 );

		// Make sure that the return date is not in the common.transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.

		$intCreatedOn = strtotime( $this->getCreatedOn() );

		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y', $intCreatedOn ), date( 'm/d/Y', ( $intCreatedOn + ( 21 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = array();

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intCreatedOn = strtotime( '+1 day', $intCreatedOn );

		while( 'Sun' == date( 'D', $intCreatedOn ) || 'Sat' == date( 'D', $intCreatedOn ) || true == in_array( $intCreatedOn, $arrintProhibitedTransactionHolidays ) ) {
			$intCreatedOn = strtotime( '+1 day', $intCreatedOn );
		}

		$objReconciliationEntry->setMovementDatetime( date( 'm/d/Y', $intCreatedOn ) );

		return $objReconciliationEntry;
	}

    public function valFileNumber() {
        $boolIsValid = true;

        if( true == is_null( $this->getFileNumber() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_number', 'File number is required.' ) );
        }

        return $boolIsValid;
    }

    public function valFileName() {
        $boolIsValid = true;

        if( true == is_null( $this->getFileName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'File name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valFilePath() {
        $boolIsValid = true;

        if( true == is_null( $this->getFilePath() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_path', 'File path is required.' ) );
        }

        return $boolIsValid;
    }

    public function valX937ReturnFileDatetime() {
        $boolIsValid = true;

        if( true == is_null( $this->getX937ReturnFileDatetime() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'x937_return_file_datetime', 'File return date time is required.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valFileNumber();
            	$boolIsValid &= $this->valFileName();
            	$boolIsValid &= $this->valFilePath();
            	$boolIsValid &= $this->valX937ReturnFileDatetime();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
        }

        return $boolIsValid;
    }

	public function moveReturnsFileToParsedFolder( $strPendingFilePath ) {

		$strPendingFileName = $this->m_strFileName;
		$strParsedFilePath = PATH_MOUNTS . $this->getParsedFilePath();
		$strParsedFileName = $this->m_intId . '_' . $this->m_strFileName;
		$strPendingFilePath = PATH_MOUNTS . $strPendingFilePath;

		if( false == is_dir( $strParsedFilePath ) ) {
			CFileIo::recursiveMakeDir( $strParsedFilePath );
		}

		if( false == rename( $strPendingFilePath . $strPendingFileName, $strParsedFilePath . $strParsedFileName ) ) {
			return false;
		}

		return true;
	}

}

?>