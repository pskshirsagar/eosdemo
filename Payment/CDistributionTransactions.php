<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CDistributionTransactions
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CDistributionTransactions extends CBaseDistributionTransactions {

	public static function fetchDistributionTransactionsBySettlementDistributionId( $intSettlementDistributionId, $objDatabase ) {
		return self::fetchDistributionTransactions( sprintf( 'SELECT * FROM distribution_transactions WHERE settlement_distribution_id = %d', ( int ) $intSettlementDistributionId ), $objDatabase );
	}

}

?>