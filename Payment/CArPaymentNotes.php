<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentNotes
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CArPaymentNotes extends CBaseArPaymentNotes {

	public static function fetchArPaymentNotesByArPaymentId( $intArPaymentId, $objDatabase ) {
		$strSql = 'SELECT * FROM ar_payment_notes WHERE ar_payment_id = ' . ( int ) $intArPaymentId;
		return self::fetchArPaymentNotes( $strSql, $objDatabase );
	}

}
?>