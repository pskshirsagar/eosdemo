<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CCreditCardBinNumbers
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CCreditCardBinNumbers extends CBaseCreditCardBinNumbers {

	public static function fetchCachedCreditCardBinNumbersByBinNumberAndPanLength( $strBinNumber, $intPanLength, $objDatabase ) {

		$arrstrAcceptedTableIds = array(
			'American Express' 	=> "'AMEX'",
			'Diners Club'		=> "'DINR'",
			'Discover'			=> "'DISC'",
			'JCB'				=> "'JCBC'",
			'Maestro'			=> "'MAES'",
			'MasterCard'		=> "'MCRD'",
			'Visa'				=> "'VISN'"
		);

		$strSql = '
			SELECT * FROM cached_credit_card_bin_numbers cccbn
			WHERE cccbn.bin_number LIKE \'' . $strBinNumber . '%\'
			AND cccbn.bin_length = 6
			AND cccbn.pan_length = ' . ( int ) $intPanLength . '
			AND cccbn.table_id IN (' . implode( ',', $arrstrAcceptedTableIds ) . ')';

		return self::fetchCreditCardBinNumbers( $strSql, $objDatabase );
	}

	public static function fetchCachedCreditCardBinNumbersByBinNumber( $strBinNumber, $objDatabase ) {

		$arrstrAcceptedTableIds = array(
			'American Express' 	=> "'AMEX'",
			'Diners Club'		=> "'DINR'",
			'Discover'			=> "'DISC'",
			'JCB'				=> "'JCBC'",
			'Maestro'			=> "'MAES'",
			'MasterCard'		=> "'MCRD'",
			'Visa'				=> "'VISN'"
		);

		$strSql = '
			SELECT * FROM cached_credit_card_bin_numbers cccbn
			WHERE cccbn.bin_number LIKE \'' . $strBinNumber . '%\'
			AND cccbn.bin_length = 6
			AND cccbn.table_id IN (' . implode( ',', $arrstrAcceptedTableIds ) . ')';

		return self::fetchCreditCardBinNumbers( $strSql, $objDatabase );
	}

}
?>