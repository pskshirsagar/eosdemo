<?php

class CWorldpayTransactionDetail extends CBaseWorldpayTransactionDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportingGroup() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMerchantName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActivityDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSettlementDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVantivPaymentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentVantivPaymentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMerchantOrderNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTxnType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPurchaseCurrency() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPurchaseAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSettlementCurrency() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSettlementAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountSuffix() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBin() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResponseReasonMessage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSessionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInterchangeRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMerchantTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAffiliate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCampaign() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMerchantGroupingId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTokenNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionProcessingTimestampGmt() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApproxInterchangeFeeAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInterchangeFlatRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInterchangePercentRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFundingMethod() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIssuingBank() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillingDescriptor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMerchantId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPresenter() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApiReportingGroup() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerReference() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSecondaryAmt() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSecondarySettlementAmt() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestedAuthAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalAuthCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddressLine1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddressLine2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valState() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFraudChecksumResponseCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFraudChecksumResponseMessage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvsResponseCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvsResponseMessage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTokenResponseCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTokenResponseMessage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPinlessDebitNetwork() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentFileId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function updateProcessedOn( CDatabase $objPaymentDatabase ) {
		if( !valId( $this->getId() ) ) {
			return false;
		}

		$strSql = sprintf( 'UPDATE ' . self::TABLE_NAME . ' SET processed_on = \'%s\' WHERE id = %d', $this->getProcessedOn(), $this->getId() );

		return fetchData( $strSql, $objPaymentDatabase );
	}

}
?>