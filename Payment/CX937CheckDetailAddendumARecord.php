<?php

class CX937CheckDetailAddendumARecord extends CBaseX937CheckDetailAddendumARecord {

	protected $m_objPaymentDatabase;

    /**
     * Set Functions
     */

    // I still don't totally know what the purpose of the addenda A record but Mark from Federal Reserve told me that
    // this record, if set to Huntington Bank's routing number will cause electronic return information to be sent to Huntington instead of 1st Regional.
    // In conjunction with this, the Truncation Indicator has to be set to "N"

	public function setDefaults() {

    	$this->setId( $this->fetchNextId( $this->m_objPaymentDatabase ) );

    	// A code used to identify this type of record. [01-02]
    	$this->setRecordType( '26' );

    	// This field shall contain the number representing the chronological order (oldest to newest) in which each Check Deetail Addendum A Record (Type 26),
    	// associated with the immediately preceding Check Detail Record (Type 25) was created.  The order shall represent the endorsement order
    	// with '1' being the oldest endorsement.  Check Detail Addendum A Records shall be in sequential order starting with '1'. [03-03].  I don't think
    	// we should every have more than one endorsement.
    	$this->setCheckDetailAddendumARecordNumber( '1' );

    	// A Routing Number specified by the ECE Institution, indicating the location to which returns, final return notifications and preliminary return notifications
    	// and preliminary return notifications shall be sent.  This is usually the Bank of First Deposit.  This field shall be applied to this item and takes
    	// precedent over any value present in the Return Location Routing Number (Field 10) in the Bundle Header Record (Type 20).
    	// Here we should set the routing number to First Regional Bank. [04-12] ( 122242597 is routing number of First Regional Bank ).  Returns used to go to Huntington.
    	// Mark said this should be huntington.
    	// (WE DON'T USE HUNTINGTON ANY MORE)  THIS WILL GET SET DYNAMICALLY, depending on the bank we are processing through
    	// $this->setBankOfFirstDepositRoutingNumber( '122242597' );  -- Set in CX937CheckDetailRecord

    	// The year month and day in the endorsement that designates the business date at the Bank of First Deposit. [13-20]
    	$this->setBofdBusinessEndorsementDate( date( 'Ymd' ) );

		// A number used to identify the item at the Bank of First Deposit [21-35]
		// Mark from the Federal Reserve instructed us to set this to the same value as check detail record sequence number.
		// This is how the a record and c records are linked back to the same check detail record.
    	// $this->setBofdItemSequenceNumber();

		// A number used to identify the depository account at the Bank of First Deposit.  This field shall be used when this record is identifying the BOFD. [36-53]
    	// Mark Said this field is not really relevant.
		$this->setDepositAccountNumberAtBofd( '                  ' );

    	// A code used to identify the branch at the Bank of First Deposit. [54-58]
    	$this->setBofdDepositBranch( '    ' );

    	// The name of the payee from the check. [59-73]
    	// This information is not necessary but may be helpful in identifying a transaction.  -- Mark - Federal Reserve
    	$this->setPayeeName( '               ' );

    	// An indicator used to identify if this BOFD is the truncator of the original check. [74]
    	$this->setTruncationIndicator( 'N' ); // put this to "No" said mark

		// A code used to indicate the conversion between the physical document, an image, or a subsequent IRD.  The indicator is specific to the
		// action of this BOFD endorser. [75] (2 = Original Check Converted to Image)
		// I had this as 2 but Mark told me to leave it blank.
    	$this->setBofdConversionIndicator( ' ' );

		// An indicator to identify whether and how the MICR line was repaired by the BOFD, for fields other than Payor Bank Routing Number and Amount. [76] (0 = No Repair)
    	$this->setBofdCorrectionIndicator( ' ' );

		// A field used at the discretion of the users of the standard. [77]
    	$this->setUserField( ' ' );

    	// A field reserved for future use by the Accredited Standards Committee X9 [78-80]
    	$this->setReserved( '   ' );
	}

    public function setPaymentDatabase( $objPaymentDatabase ) {
    	$this->m_objPaymentDatabase = $objPaymentDatabase;
    }

	public function setRecordType( $strRecordType ) {
	    $this->m_strRecordType = CStrings::strNachaDef( $strRecordType, 2, true, true, '0' );
	}

	public function setCheckDetailAddendumARecordNumber( $strCheckDetailAddendumARecordNumber ) {
	    $this->m_strCheckDetailAddendumARecordNumber = CStrings::strNachaDef( $strCheckDetailAddendumARecordNumber, 1, true, true, '0' );
	}

	public function setBankOfFirstDepositRoutingNumber( $strBankOfFirstDepositRoutingNumber ) {
	    $this->m_strBankOfFirstDepositRoutingNumber = CStrings::strNachaDef( $strBankOfFirstDepositRoutingNumber, 9, true, true, '0' );
	}

	public function setBofdBusinessEndorsementDate( $strBofdBusinessEndorsementDate ) {
	    $this->m_strBofdBusinessEndorsementDate = CStrings::strNachaDef( $strBofdBusinessEndorsementDate, 8, true, true, '0' );
	}

	public function setBofdItemSequenceNumber( $strBofdItemSequenceNumber ) {
	    $this->m_strBofdItemSequenceNumber = CStrings::strNachaDef( $strBofdItemSequenceNumber, 15, true, true, '0' );
	}

	public function setDepositAccountNumberAtBofd( $strDepositAccountNumberAtBofd ) {
	    $this->m_strDepositAccountNumberAtBofd = CStrings::strNachaDef( $strDepositAccountNumberAtBofd, 18, false, false );
	}

	public function setBofdDepositBranch( $strBofdDepositBranch ) {
	    $this->m_strBofdDepositBranch = CStrings::strNachaDef( $strBofdDepositBranch, 5, false, false );
	}

	public function setPayeeName( $strPayeeName ) {
	    $this->m_strPayeeName = CStrings::strNachaDef( $strPayeeName, 15, false, false );
	}

	public function setTruncationIndicator( $strTruncationIndicator ) {
	    $this->m_strTruncationIndicator = CStrings::strNachaDef( $strTruncationIndicator, 1, false, false );
	}

	public function setBofdConversionIndicator( $strBofdConversionIndicator ) {
	    $this->m_strBofdConversionIndicator = CStrings::strNachaDef( $strBofdConversionIndicator, 1, false, false );
	}

	public function setBofdCorrectionIndicator( $strBofdCorrectionIndicator ) {
	    $this->m_strBofdCorrectionIndicator = CStrings::strNachaDef( $strBofdCorrectionIndicator, 1, true, true, '0' );
	}

	public function setUserField( $strUserField ) {
	    $this->m_strUserField = CStrings::strNachaDef( $strUserField, 1, false, false );
	}

	public function setReserved( $strReserved ) {
	    $this->m_strReserved = CStrings::strNachaDef( $strReserved, 3, false, false );
	}

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Other Functions
     */

	public function buildRecord( $boolConvertToEbcdic = false ) {

		$strRecord = '';

		$strRecord .= $this->getRecordType();
		$strRecord .= $this->getCheckDetailAddendumARecordNumber();
		$strRecord .= $this->getBankOfFirstDepositRoutingNumber();
		$strRecord .= $this->getBofdBusinessEndorsementDate();
		$strRecord .= $this->getBofdItemSequenceNumber();
		$strRecord .= $this->getDepositAccountNumberAtBofd();
		$strRecord .= $this->getBofdDepositBranch();
		$strRecord .= $this->getPayeeName();
		$strRecord .= $this->getTruncationIndicator();
		$strRecord .= $this->getBofdConversionIndicator();
		$strRecord .= $this->getBofdCorrectionIndicator();
		$strRecord .= $this->getUserField();
		$strRecord .= $this->getReserved();

		if( true == $boolConvertToEbcdic ) {
			$strRecord = \Psi\Libraries\UtilStrings\CStrings::createService()->asciiToEbcdic( $strRecord );
		}

		return $strRecord;
	}
}
?>