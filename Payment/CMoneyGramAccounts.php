<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMoneyGramAccounts
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CMoneyGramAccounts extends CBaseMoneyGramAccounts {

	public static function fetchMoneyGramAccountByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $intCid, $objPaymentDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						money_gram_accounts
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND lease_id = ' . ( int ) $intLeaseId . '
						AND cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return self::fetchMoneyGramAccount( $strSql, $objPaymentDatabase );
	}

	public static function fetchMoneyGramAccountNumberByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $intCid, $objPaymentDatabase ) {
		$strSql = 'SELECT
						id
					FROM
						money_gram_accounts
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND lease_id = ' . ( int ) $intLeaseId . '
						AND cid = ' . ( int ) $intCid . '
					LIMIT 1';

		$arrstrResponse = fetchData( $strSql, $objPaymentDatabase );
		if( true == valArr( $arrstrResponse ) && true == valId( $arrstrResponse[0]['id'] ) ) return $arrstrResponse[0]['id'];

		return NULL;
	}

	public static function fetchMoneyGramAccountByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objPaymentDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT
						id,
						customer_id,
						lease_id
					FROM
						money_gram_accounts
					WHERE
						customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
					GROUP BY id, customer_id, lease_id';

		return self::fetchMoneyGramAccounts( $strSql, $objPaymentDatabase );
	}

	public static function fetchOrCreateMoneyGramAccountByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $intCid, $objClientDatabase, $objPaymentDatabase, $boolIsAllowCreateMoneygramAccount = true, $intCreatedBy = SYSTEM_USER_ID ) {
		$strMoneyGramAccountNumber = CMoneyGramAccounts::fetchMoneyGramAccountNumberByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $intCid, $objPaymentDatabase );

		if( true == valId( $strMoneyGramAccountNumber ) ) {
			return CMoneyGramAccount::getValidFormatOfMoneyGramAccountNumber( $strMoneyGramAccountNumber );
		}

		if( false == $boolIsAllowCreateMoneygramAccount ) {
			return NULL;
		}

		switch( NULL ) {
			default:
				$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchNonDeletedLeaseByIdByCid( $intLeaseId, $intCid, $objClientDatabase );

				if( false == valObj( $objLease, 'CLease' ) ) {
					break;
				}

				$objPaymentDatabase->begin();

				$objMoneyGramAccount = new CMoneyGramAccount();
				$objMoneyGramAccount->setCustomerId( ( int ) $intCustomerId );
				$objMoneyGramAccount->setLeaseId( ( int ) $intLeaseId );
				$objMoneyGramAccount->setPropertyId( ( int ) $objLease->getPropertyId() );
				$objMoneyGramAccount->setCid( ( int ) $intCid );

				if( false == $objMoneyGramAccount->validate( 'generate_account' ) || false == $objMoneyGramAccount->insert( $intCreatedBy, $objPaymentDatabase ) ) {
					$objPaymentDatabase->rollback();
					break;
				}

				$objPaymentDatabase->commit();

				// While returning money gram account number it is be always minimum of 8 digit length
				return CMoneyGramAccount::getValidFormatOfMoneyGramAccountNumber( $objMoneyGramAccount->getId() );
		}

		return false;
	}

}
?>