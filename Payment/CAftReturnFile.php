<?php

class CAftReturnFile extends CBaseAftReturnFile {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessingBankId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentFileId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMerchantGatewayId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnsProcessedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConfirmedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsReconPrepped() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>