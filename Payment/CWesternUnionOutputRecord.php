<?php

class CWesternUnionOutputRecord extends CBaseWesternUnionOutputRecord {

	protected $m_objClient;
	protected $m_intCompanyMerchantAccountId;

    /**
     * Get Functions
     */

	public function getClient() {
		return $this->m_objClient;
	}

	public function getCompanyMerchantAccountId() {
		return $this->m_intCompanyMerchantAccountId;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchWesternUnionInputRecord( $objPaymentDatabase ) {

    	// The "output->transaction_id_number" should match with exactly one "input->client_order_number"
    	return \Psi\Eos\Payment\CWesternUnionInputRecords::createService()->fetchWesternUnionInputRecordByClientOrderNumber( $this->m_strTransactionIdNumber, $objPaymentDatabase );
    }

    /**
     * Set Functions
     */

	public function setForeignKeys( $objWesternUnionInputRecord ) {
		$this->m_intCid = $objWesternUnionInputRecord->getCid();
		// $this->m_intCompanyMerchantAccountId = $objWesternUnionInputRecord->getCompanyMerchantAccountId();
		$this->m_intPropertyId = $objWesternUnionInputRecord->getPropertyId();
		$this->m_intLeaseId = $objWesternUnionInputRecord->getLeaseId();
		$this->m_intCustomerId = $objWesternUnionInputRecord->getCustomerId();
	}

	public function setClient( $objClient ) {
		$this->m_objClient = $objClient;
	}

	public function setCompanyMerchantAccountId( $intCompanyMerchantAccountId ) {
		$this->m_intCompanyMerchantAccountId = $intCompanyMerchantAccountId;
	}

	/**
	 * Val Functions
	 */

    public function valId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ));
        // }

        return $boolIsValid;
    }

    public function valFileId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getFileId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getCid())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ));
        // }

        return $boolIsValid;
    }

    public function valArPaymentId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getArPaymentId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_payment_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getPropertyId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getCustomerId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getLeaseId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valRecordType() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getRecordType())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'record_type', '' ));
        // }

        return $boolIsValid;
    }

    public function valTransactionIdNumber() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getTransactionIdNumber())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_id_number', '' ));
        // }

        return $boolIsValid;
    }

    public function valFirstName() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getFirstName())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_name', '' ));
        // }

        return $boolIsValid;
    }

    public function valLastName() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getLastName())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'last_name', '' ));
        // }

        return $boolIsValid;
    }

    public function valAddress() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getAddress())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address', '' ));
        // }

        return $boolIsValid;
    }

    public function valCity() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getCity())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', '' ));
        // }

        return $boolIsValid;
    }

    public function valStateProvince() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getStateProvince())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_province', '' ));
        // }

        return $boolIsValid;
    }

    public function valPostalCode() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getPostalCode())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', '' ));
        // }

        return $boolIsValid;
    }

    public function valTelephone() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getTelephone())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'telephone', '' ));
        // }

        return $boolIsValid;
    }

    public function valCountry() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getCountry())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country', '' ));
        // }

        return $boolIsValid;
    }

    public function valMtcn() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getMtcn())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mtcn', '' ));
        // }

        return $boolIsValid;
    }

    public function valAgentZip() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getAgentZip())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'agent_zip', '' ));
        // }

        return $boolIsValid;
    }

    public function valPaymentAmount() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getPaymentAmount())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', '' ));
        // }

        return $boolIsValid;
    }

    public function valTransactionDatetime() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getTransactionDatetime())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_datetime', '' ));
        // }

        return $boolIsValid;
    }

    public function valCommentText() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getCommentText())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'comment_text', '' ));
        // }

        return $boolIsValid;
    }

    public function valClientIdentifier() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getClientIdentifier())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_identifier', '' ));
        // }

        return $boolIsValid;
    }

    public function valClientReference() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getClientReference())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_reference', '' ));
        // }

        return $boolIsValid;
    }

    public function valCustomerFee() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getCustomerFee())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_fee', '' ));
        // }

        return $boolIsValid;
    }

    public function valProcessedOn() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getProcessedOn())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processed_on', '' ));
        // }

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getUpdatedBy())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ));
        // }

        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getUpdatedOn())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ));
        // }

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getCreatedBy())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ));
        // }

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getCreatedOn())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ));
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>