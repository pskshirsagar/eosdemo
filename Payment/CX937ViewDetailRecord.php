<?php

class CX937ViewDetailRecord extends CBaseX937ViewDetailRecord {

	protected $m_objX937Bundle;
	protected $m_objX937CheckDetailRecord;
	protected $m_objPaymentDatabase;

    /**
     * Set Functions
     */

	public function setDefaults() {

    	$this->setId( $this->fetchNextId( $this->m_objPaymentDatabase ) );

    	// A code used to identify this type of record. [01-02]
    	$this->setRecordType( '50' );

    	if( false == valObj( $this->m_objX937Bundle, 'CX937Bundle' ) ) {
			trigger_error( 'Bundle was not set.', E_USER_ERROR );
			exit;
		}

    	// A code that indicates the presence and disposition of an image view conveyed in the related Image View Data Record (Type 52) [03-03]
    	// 1 == Image view present, check or substitute check.
    	$this->setImageIndicator( '1' );

    	// A number that identifies the financial institution that created the image view in the related Image View Data Record (Type 52) Image Data (Field 27).
    	// If this field is not known, the Bundle ECE Institution Routing Number (Field 4) of the Bundle Header Record (Type 20) may be used.  The endorsement
    	// information conveyed in the applicable Addendum Records (Check Detail Addendum A Record (Type 26), Check Detail Addenda C Record (Type 28), Return Addenda
    	// A Record (Type 32) and Return Addenda D Record (Type 35) ) provides an electronic endorsement for the image creator. [04-12]
    	$this->setImageCreatorRoutingNumber( $this->m_objX937Bundle->getHeaderEceInstitutionRoutingNumber() );

    	// Date assigned by the image creator for the image view conveyed in teh related Image View Data Record (Type 52) Image Data (Field 27).  If this field is
    	// not known, the Bundle Business Date (Field 5) of the Bundle Header Record (Type 20) may be used. [13-20]
    	$this->setImageCreatorDate( $this->m_objX937Bundle->getHeaderBusinessDate() );

		// A code that identifies the type of image format used in the related Image View Data Record (Type 52) Image Data (Field 27).  Values '00' through '20'
		// may be used only when an agreement exists indicating that sender and receiver both support the specified image format type as specified below. [21-22]
		// 00 = TIFF 6 (only compression Group 4 Black and White allowed) See annex N for information on recommended tags and values.  Extension "tif".
		$this->setImageViewFormatIndicator( '00' );

		// A code that identifies the algorith or method used to compess the Image Data in teh related Image View Data Record (Type 52) Image Data (Field 27). [23-24]
		// 00 = Group 4 facsimile compression (ITU-T Rec. T.563/CCITT Rec. T.6)
    	$this->setImageViewCompressionAlgorithmIdentifier( '00' );

		// The total number of bytes in the related Image View Data Record (Type 52) Image Data (Field 19).  This field has the
		// same value as the Length of Image Data (Field 18) in the corresponding Image View Data Record. (Type52).  If Image
		// View Data Size is Greater than 9,999,999 then Image Indicator Defined Value shall equal '0. [25-31]
    	// $this->setImageViewDataSize();

    	// A code that indicates the image view conveyed in the related Image View Data Record (Type 52) Image Data Field (Field 27).  An image view may be a full
    	// view of the item (i.e., the entire full face of the document) or a partial view (snippet) as determined by the value of the View Descriptor field (Field 9). [32]
    	// 0 = Front image view, 1 = Back image view
    	// $this->setViewSideIndicator(); (this is set in the create function in check detail record object)

    	// A code that indicates the nature of the image view conveyed by the Image Data (Field 27) in the Image View Data Record (Type 52).  Refer to TR 2 for a more complete
    	// description of Area of Interest (See Clause 2).  If the view is unknown, Defined Value of '00' shall be used.
    	// 00 = Full view (meaning it isn't a partial image). [33-34]
    	$this->setViewDescriptor( '0' );

		// A code that indicates the presence or absense of a digital signature for the image view contained in teh related Image View Data Record (Type 52)
		// Image Data (Field 27).  If present, the Digital Signature is conveyed in the related image View Data Record (Type 52) Digital Signature (Field 23).
		// This standard does not dictate that a Digital Signature must be present. 0 = Digital Signature not present, 1 = Digital Signature is present. [35]
    	$this->setDigitalSignatureIndicator( '0' );

    	// A code used to identify the hash function algorithm to generate and validate the Digital Signature in the related Image View Data Record (Type 52)
    	// Digital Signature (Field 23).  See Annex M for details on current usage and valid combinations. [36-37] should be null if no digital signature is present.
    	$this->setDigitalSignatureMethod( '  ' );

    	// The length in bits of the cryptographic algorithm key used to create the Digital Signature in the related Image View Data Record (Type 52) Digital Signature (Field 17). [38-42]
    	$this->setSecurityKeySize( '     ' );

    	// A number that represents the offset in bytes from teh first bye (counted as byte 1) of the image data in the related image view data record (Type 52) Image Data (Field 27) to the
    	// first byte of the image data protected by the digital signature. [43-49] (should be empty unless we are using a digital signature which we are not)
    	$this->setStartOfProtectedData( '       ' );

    	// The number of contiguous bytes of image data in the related Image View Data Record (Type 52) Image Data (Field 27) protected by the digital signature, starting with the byte indicated
    	// by the value of the Start of Protected Data in this image View Detail Record.  The Length of Protected Data value shall not exceed the Length of Image Data (Field 20) value in the
    	// corresponding Image View Data Record (Type 52) [50-56] (should be empty)
    	 $this->setLengthOfProtectedData( '       ' );

		// A code that indicates whether the sender has the ability to recreate the image view conveyed in the related Image View Data Record (Type 52) Image Data (Field 27). [57]
		// Says should only be set under clearing arrangements.  I think I'll go ahead and set it anyway (0 = Sender can recreate image)
    	$this->setImageRecreateIndicator( ' ' );

		// A field used at the discretion of users of the standard. [58-65]
    	$this->setUserField( '        ' );

    	// A field reserved for future use by the Accredited Standards Committee x9. [66-80]
    	$this->setReserved( '               ' );
	}

    public function setPaymentDatabase( $objPaymentDatabase ) {
    	$this->m_objPaymentDatabase = $objPaymentDatabase;
    }

	public function setX937Bundle( $objX937Bundle ) {
		$this->m_objX937Bundle = $objX937Bundle;
	}

	public function setX937CheckDetailRecord( $objX937CheckDetailRecord ) {
		$this->m_objX937CheckDetailRecord = $objX937CheckDetailRecord;
	}

	public function setRecordType( $strRecordType ) {
        $this->m_strRecordType = CStrings::strNachaDef( $strRecordType, 2, true, true, '0' );
    }

	public function setImageIndicator( $strImageIndicator ) {
	    $this->m_strImageIndicator = CStrings::strNachaDef( $strImageIndicator, 1, true, true, '0' );
	}

	public function setImageCreatorRoutingNumber( $strImageCreatorRoutingNumber ) {
	    $this->m_strImageCreatorRoutingNumber = CStrings::strNachaDef( $strImageCreatorRoutingNumber, 9, true, true, '0' );
	}

	public function setImageCreatorDate( $strImageCreatorDate ) {
	    $this->m_strImageCreatorDate = CStrings::strNachaDef( $strImageCreatorDate, 8, true, true, '0' );
	}

	public function setImageViewFormatIndicator( $strImageViewFormatIndicator ) {
	    $this->m_strImageViewFormatIndicator = CStrings::strNachaDef( $strImageViewFormatIndicator, 2, false, false );
	}

	public function setImageViewCompressionAlgorithmIdentifier( $strImageViewCompressionAlgorithmIdentifier ) {
	    $this->m_strImageViewCompressionAlgorithmIdentifier = CStrings::strNachaDef( $strImageViewCompressionAlgorithmIdentifier, 2, false, false );
	}

	public function setImageViewDataSize( $strImageViewDataSize ) {
		$this->m_strImageViewDataSize = CStrings::strNachaDef( $strImageViewDataSize, 7, true, true, '0' );
	}

	public function setViewSideIndicator( $strViewSideIndicator ) {
	    $this->m_strViewSideIndicator = CStrings::strNachaDef( $strViewSideIndicator, 1, true, true, '0' );
	}

	public function setViewDescriptor( $strViewDescriptor ) {
	    $this->m_strViewDescriptor = CStrings::strNachaDef( $strViewDescriptor, 2, true, true, '0' );
	}

	public function setDigitalSignatureIndicator( $strDigitalSignatureIndicator ) {
	    $this->m_strDigitalSignatureIndicator = CStrings::strNachaDef( $strDigitalSignatureIndicator, 1, false, false );
	}

	public function setDigitalSignatureMethod( $strDigitalSignatureMethod ) {
	    $this->m_strDigitalSignatureMethod = CStrings::strNachaDef( $strDigitalSignatureMethod, 2, true, true, ' ' );
	}

	public function setSecurityKeySize( $strSecurityKeySize ) {
	    $this->m_strSecurityKeySize = CStrings::strNachaDef( $strSecurityKeySize, 5, true, true, ' ' );
	}

	public function setStartOfProtectedData( $strStartOfProtectedData ) {
	    $this->m_strStartOfProtectedData = CStrings::strNachaDef( $strStartOfProtectedData, 7, true, true, ' ' );
	}

	public function setLengthOfProtectedData( $strLengthOfProtectedData ) {
	    $this->m_strLengthOfProtectedData = CStrings::strNachaDef( $strLengthOfProtectedData, 7, true, true, ' ' );
	}

	public function setImageRecreateIndicator( $strImageRecreateIndicator ) {
	    $this->m_strImageRecreateIndicator = CStrings::strNachaDef( $strImageRecreateIndicator, 1, true, true, ' ' );
	}

	public function setUserField( $strUserField ) {
	    $this->m_strUserField = CStrings::strNachaDef( $strUserField, 8, false, false );
	}

	public function setReserved( $strReserved ) {
	    $this->m_strReserved = CStrings::strNachaDef( $strReserved, 15, false, false );
	}

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	/**
	 * Other Functions
	 */

	public function buildRecord( $boolConvertToEbcdic = false ) {

		$strRecord = '';

		$strRecord .= $this->getRecordType();
		$strRecord .= $this->getImageIndicator();
		$strRecord .= $this->getImageCreatorRoutingNumber();
		$strRecord .= $this->getImageCreatorDate();
		$strRecord .= $this->getImageViewFormatIndicator();
		$strRecord .= $this->getImageViewCompressionAlgorithmIdentifier();
		$strRecord .= $this->getImageViewDataSize();
		$strRecord .= $this->getViewSideIndicator();
		$strRecord .= $this->getViewDescriptor();
		$strRecord .= $this->getDigitalSignatureIndicator();
		$strRecord .= $this->getDigitalSignatureMethod();
		$strRecord .= $this->getSecurityKeySize();
		$strRecord .= $this->getStartOfProtectedData();
		$strRecord .= $this->getLengthOfProtectedData();
		$strRecord .= $this->getImageRecreateIndicator();
		$strRecord .= $this->getUserField();
		$strRecord .= $this->getReserved();

		if( true == $boolConvertToEbcdic ) {
			$strRecord = \Psi\Libraries\UtilStrings\CStrings::createService()->asciiToEbcdic( $strRecord );
		}

		return $strRecord;
	}
}
?>