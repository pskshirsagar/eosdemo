<?php

class CMerchantProcessingType extends CBaseMerchantProcessingType {

	const STANDARD 			= 1;
	const VARIABLE_FEES 	= 2;
	const CLEARING 			= 3;
	const VARIABLE_VISA 	= 4;
	const ACCOUNTS_PAYABLE 	= 5;
	const COMMERCIAL		= 6;
	const INTERNATIONAL		= 7;

	public static $c_arrintRHTPAllowedMerchantProcessingTypes = [
		self::STANDARD,
		self::COMMERCIAL,
		self::INTERNATIONAL
	];
}
?>