<?php

use Psi\Libraries\UtilObjectModifiers\CObjectModifiers;

class CX937File extends CBaseX937File {

	protected $m_intProcessingBankAccountId;

	protected $m_arrobjX937CashLetters;
	protected $m_objPaymentDatabase;
	protected $m_intUserId;
	protected $m_fltTotalAmount;
	protected $m_objProcessingBank;

	protected $m_strFileContent;

	const PATH_X937_FILES = 'x937_files/';

    public function __construct() {
        parent::__construct();

        $this->m_arrobjX937CashLetters = array();

        return;
    }

	public function getFilePath() {
		return PATH_MOUNTS_X937_FILES_OUTGOING . $this->m_strFilePath;
	}

	public function getFullPath() {
		return $this->getFilePath() . $this->getFileName();
	}

	public function getFileContent() {
    	return $this->m_strFileContent;
	}

	public function createX937CashLetter() {

		$objX937CashLetter = new CX937CashLetter();
		$objX937CashLetter->setPaymentDatabase( $this->m_objPaymentDatabase );
		$objX937CashLetter->setHeaderDestinationRoutingNumber( $this->getHeaderImmediateDestinationRoutingNumber() );
		$objX937CashLetter->setHeaderEceInstitutionRoutingNumber( $this->getHeaderImmediateOriginRoutingNumber() );
    	$objX937CashLetter->setHeaderOriginatorContactName( $this->getControlImmediateOriginContactName() );
    	$objX937CashLetter->setHeaderOriginatorContactPhoneNumber( $this->getControlImmediateOriginContactPhoneNumber() );
        $objX937CashLetter->setControlEceInstitutionName( $this->getHeaderImmediateOrginName() );
        $objX937CashLetter->setDefaults();
		$objX937CashLetter->setX937FileId( $this->m_intId );
		$objX937CashLetter->setX937File( $this );

		$this->m_arrobjX937CashLetters[] = $objX937CashLetter;

		return $objX937CashLetter;
	}

	public function createReconciliationEntry( $objPaymentDatabase ) {

		$objReconciliationEntry = new CReconciliationEntry();
		$objReconciliationEntry->setX937FileId( $this->getId() );
		$objReconciliationEntry->setTotal( $this->getTotalAmount() );

		$intProcessingBankAccountId = NULL;

		switch( $this->getProcessingBankId() ) {
			case CProcessingBank::FIRST_REGIONAL:
				$intProcessingBankAccountId = CProcessingBankAccount::FRB_CENTRAL_CHECK21;
				break;

			case CProcessingBank::ZIONS_BANK:
			case CProcessingBank::FIFTH_THIRD:

				// Assume that all XCDR entries have the same PBA (which they should).  Load the PBA from the first one
				$objFirstX937CheckDetailRecord = \Psi\Eos\Payment\CX937CheckDetailRecords::createService()->fetchFirstX937CheckDetailRecordByX937FileId( $this->getId(), $objPaymentDatabase );

				if( $objFirstX937CheckDetailRecord != NULL ) {
					$intProcessingBankAccountId = $objFirstX937CheckDetailRecord->getProcessingBankAccountId();
				}
				break;

			default:
				$intProcessingBankAccountId = NULL; // this should trigger error.
		}

		$objReconciliationEntry->setProcessingBankAccountId( $intProcessingBankAccountId );

		$intReconciliationEntryTypeId = NULL;

		switch( $this->getX937FileTypeId() ) {
			case CX937FileType::AR_PAYMENTS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::AR_PAYMENTS_CHECK_21;
				break;

			case CX937FileType::COMPANY_PAYMENTS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::COMPANY_PAYMENTS_CHECK_21;
				break;

			default:
				$intReconciliationEntryTypeId = NULL; // this should trigger error.
		}

		$objReconciliationEntry->setReconciliationEntryTypeId( $intReconciliationEntryTypeId );

		// Make sure that the date is not in the common.transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.

		$intCreatedOn = strtotime( $this->getCreatedOn() );

		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y', $intCreatedOn ), date( 'm/d/Y', ( $intCreatedOn + ( 21 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = array();

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intCreatedOn = strtotime( '+1 day', $intCreatedOn );

		while( 'Sun' == date( 'D', $intCreatedOn ) || 'Sat' == date( 'D', $intCreatedOn ) || true == in_array( $intCreatedOn, $arrintProhibitedTransactionHolidays ) ) {
			$intCreatedOn = strtotime( '+1 day', $intCreatedOn );
		}

		$objReconciliationEntry->setMovementDatetime( date( 'm/d/Y', $intCreatedOn ) );

		return $objReconciliationEntry;
	}

	/**
	 * Get Functions
	 */

    public function getTotalAmount() {
        return $this->m_fltTotalAmount;
    }

    public function getProcessingBankAccountId() {
    	return $this->m_intProcessingBankAccountId;
    }

	/**
	 * Set Functions
	 */

    public function setDefaults() {

		$this->setId( $this->fetchNextId( $this->m_objPaymentDatabase ) );

        $this->setX937FileTypeId( CX937FileType::AR_PAYMENTS );

		if( false == valObj( $this->m_objProcessingBank, 'CProcessingBank' ) ) {
		    trigger_error( 'Processing bank not properly loaded.', E_USER_ERROR );
		}

        $this->setMerchantGatewayId( $this->m_objProcessingBank->getX937MerchantGatewayId() );

		// Header Record Type is always 01 [01-02]
		$this->setHeaderRecordType( '01' );

		// Identifies the version of the standard used to generate the file -- (We use ANS X9.100-180-2006) = 20 [03-04]
		$this->setHeaderStandardLevel( '03' );

		// Identifies if it is a test file or not ('P' = Production, 'T' Test Financial Exchange File) [5]
		$this->setHeaderTestFileIndicator( 'P' );

		// Federal Reserve Bank (LA) (122000166) (Institution that receives the file) [06-14] (Charles gave me this number and should be correct).
		$this->setHeaderImmediateDestinationRoutingNumber( $this->m_objProcessingBank->getX937ImmDestRoutingNumber() );

		// A number that identifies the institution that originates the file. [15-23]
		$this->setHeaderImmediateOriginRoutingNumber( $this->m_objProcessingBank->getX937ImmOrigRoutingNumber() );

		// File Creation Date(YYYYMMDD) [24-31]
		$this->setHeaderFileCreationDate( date( 'Ymd' ) );

		// File Creation Time (hhmm) [32-35]
		$this->setHeaderFileCreationTime( date( 'Hi' ) );

		// A code that indicates if a file has already been transmitted [36]
		$this->setHeaderResendIndicator( 'N' );

		// The short name, as defined in this standard, identifies the institution that sends the file. [37-54] (Federal Reserve Bank)
		$this->setHeaderImmediateDestinationName( $this->m_objProcessingBank->getX937ImmDestName() );

		// Institution that sends the file [55-72]
		$this->setHeaderImmediateOrginName( $this->m_objProcessingBank->getX937ImmOrigName() );

		// A code used to uniquely identify each file created on the same date, at the same time and between the same exchanging institutions [73]
		$this->setHeaderFileIdModifier( ' ' );

		// Only needs to be set if bank says so [74-75]
		$this->setHeaderCountryCode( '  ' );

		// This is a descretionary field that we don't need to sue [76-79]
		$this->setHeaderUserField( '    ' );

		// A field reserved for future use by the Accredit Standards Committee x9 (should be blank) [80]
		$this->setHeaderReserved( ' ' );

		// A field that identifies the type of record (Always 99) [01-02]
		$this->setControlRecordType( '99' );

		// The total number of cash letters within the file [03-08]
		// $this->setControlCashLetterCount();

		// The total number of records of all types sent in the file, including the File Header Record (Type 01) and the File Control Record [9-16]
		// $this->setControlTotalRecordCount();

		// The total number of items sent within the file, all Check Detail Records (Type 25), all Return Records (Type 31) and all Credit/Reconcilation records (Type 61) [17-24]
		// $this->setControlTotalItemCount();

		// The total US dollar value of the complete file, all Check Detail Records (Type 25) and all Return Records (Type 31) [25-40]
		// $this->setControlFileTotalAmount();

		// A name indicating contact at the institution that creates the ECE file. [41-54]
		$this->setControlImmediateOriginContactName( $this->m_objProcessingBank->getX937ImmOrigContactName() );

		// The phone number of the contact name at the institution that creates the file [55-64]
		$this->setControlImmediateOriginContactPhoneNumber( $this->m_objProcessingBank->getX937ImmOrigContactPhone() );

		// A field reserved for future use by the Accredited Statndards Committee X9 [65-80]
		$this->setControlReserved( '                ' );

		$this->loadFileNameAndFilePath();
		$this->setFileDatetime( date( 'm/d/Y H:i:s' ) );
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrValues['total_amount'] ) ) $this->setTotalAmount( $arrValues['total_amount'] );
        if( true == isset( $arrValues['processing_bank_account_id'] ) ) $this->setProcessingBankAccountId( $arrValues['processing_bank_account_id'] );

        return;
    }

	public function setHeaderRecordType( $strHeaderRecordType ) {
	    $this->m_strHeaderRecordType = CStrings::strNachaDef( $strHeaderRecordType, 2, true, true, '0' );
	}

	public function setHeaderStandardLevel( $strHeaderStandardLevel ) {
	    $this->m_strHeaderStandardLevel = CStrings::strNachaDef( $strHeaderStandardLevel, 2, true, true, '0' );
	}

	public function setHeaderTestFileIndicator( $strHeaderFileTypeIndicator ) {
	    $this->m_strHeaderTestFileIndicator = CStrings::strNachaDef( $strHeaderFileTypeIndicator, 1, false, false );
	}

	public function setHeaderImmediateDestinationRoutingNumber( $strHeaderImmediateDestinationRoutingNumber ) {
	    $this->m_strHeaderImmediateDestinationRoutingNumber = CStrings::strNachaDef( $strHeaderImmediateDestinationRoutingNumber, 9, true, true, '0' );
	}

	public function setHeaderImmediateOriginRoutingNumber( $strHeaderImmediateOriginRoutingNumber ) {
	    $this->m_strHeaderImmediateOriginRoutingNumber = CStrings::strNachaDef( $strHeaderImmediateOriginRoutingNumber, 9, true, true, '0' );
	}

	public function setHeaderFileCreationDate( $strHeaderFileCreationDate ) {
	    $this->m_strHeaderFileCreationDate = CStrings::strNachaDef( $strHeaderFileCreationDate, 8, true, true, '0' );
	}

	public function setHeaderFileCreationTime( $strHeaderFileCreationTime ) {
	    $this->m_strHeaderFileCreationTime = CStrings::strNachaDef( $strHeaderFileCreationTime, 4, true, true, '0' );
	}

	public function setHeaderResendIndicator( $strHeaderResendIndicator ) {
	    $this->m_strHeaderResendIndicator = CStrings::strNachaDef( $strHeaderResendIndicator, 1, false, false );
	}

	public function setHeaderImmediateDestinationName( $strHeaderImmediateDestinationName ) {
	    $this->m_strHeaderImmediateDestinationName = CStrings::strNachaDef( $strHeaderImmediateDestinationName, 18, false, false );
	}

	public function setHeaderImmediateOrginName( $strHeaderImmediateOrginName ) {
	    $this->m_strHeaderImmediateOrginName = CStrings::strNachaDef( $strHeaderImmediateOrginName, 18, false, false );
	}

	public function setHeaderFileIdModifier( $strHeaderFileIdModifier ) {
	    $this->m_strHeaderFileIdModifier = CStrings::strNachaDef( $strHeaderFileIdModifier, 1, false, false );
	}

	public function setHeaderCountryCode( $strHeaderCountryCode ) {
	    $this->m_strHeaderCountryCode = CStrings::strNachaDef( $strHeaderCountryCode, 2, false, false );
	}

	public function setHeaderUserField( $strHeaderUserField ) {
	    $this->m_strHeaderUserField = CStrings::strNachaDef( $strHeaderUserField, 4, false, false );
	}

	public function setHeaderReserved( $strHeaderReserved ) {
	    $this->m_strHeaderReserved = CStrings::strNachaDef( $strHeaderReserved, 1, false, false );
	}

	public function setControlRecordType( $strControlRecordType ) {
	    $this->m_strControlRecordType = CStrings::strNachaDef( $strControlRecordType, 2, true, true, '0' );
	}

	public function setControlCashLetterCount( $strControlCashLetterCount ) {
	    $this->m_strControlCashLetterCount = CStrings::strNachaDef( $strControlCashLetterCount, 6, true, true, '0' );
	}

	public function setControlTotalRecordCount( $strControlTotalRecordCount ) {
	    $this->m_strControlTotalRecordCount = CStrings::strNachaDef( $strControlTotalRecordCount, 8, true, true, '0' );
	}

	public function setControlTotalItemCount( $strControlTotalItemCount ) {
	    $this->m_strControlTotalItemCount = CStrings::strNachaDef( $strControlTotalItemCount, 8, true, true, '0' );
	}

	public function setFormattedControlFileTotalAmount( $strControlFileTotalAmount ) {

    	// Validate to make sure amount is in $$.cc format
    	if( false == stristr( $strControlFileTotalAmount, '.' ) ) {
    		trigger_error( 'Total Control File Total Amount is not in $$.cc format.', E_USER_ERROR );
    		exit;
    	}

	    $this->m_strControlFileTotalAmount = CStrings::strNachaDef( $strControlFileTotalAmount, 16, true, true, '0' );
	}

	public function setControlImmediateOriginContactName( $strControlImmediateOriginContactName ) {
	    $this->m_strControlImmediateOriginContactName = CStrings::strNachaDef( $strControlImmediateOriginContactName, 14, false, false );
	}

	public function setControlImmediateOriginContactPhoneNumber( $strControlImmediateOriginContactPhoneNumber ) {
	    $this->m_strControlImmediateOriginContactPhoneNumber = CStrings::strNachaDef( $strControlImmediateOriginContactPhoneNumber, 10, true, true, '0' );
	}

	public function setControlReserved( $strControlReserved ) {
	    $this->m_strControlReserved = CStrings::strNachaDef( $strControlReserved, 16, false, false );
	}

    public function setPaymentDatabase( $objPaymentDatabase ) {
    	$this->m_objPaymentDatabase = $objPaymentDatabase;
    }

    public function setUserId( $intUserId ) {
    	$this->m_intUserId = ( int ) $intUserId;
    }

    public function setTotalAmount( $fltTotalAmount ) {
        $this->m_fltTotalAmount = $fltTotalAmount;
    }

	public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
    	$this->m_intProcessingBankAccountId = $intProcessingBankAccountId;
    }

    public function setProcessingBank( $objProcessingBank ) {
        $this->m_objProcessingBank = $objProcessingBank;
        $this->m_intProcessingBankId = $objProcessingBank->getId();
    }

    public function setFileContent( $strFileContent ) {
    	$this->m_strFileContent = $strFileContent;
	}

	/**
	 * Validate Functions
	 */

	public function valRevert() {
		$boolIsValid = true;

		if( false == is_null( $this->getTransferredToIntermediaryOn() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'You cannot revert an x937 file that has already been distributed to intermediary accounts.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            case 'validate_revert':
            	$boolIsValid &= $this->valRevert();
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	/**
	 * Fetch Functions
	 */

    public function fetchNextX937FileIdModifier( $objPaymentDatabase ) {

		$strSql = "	SELECT
						count(id)
					FROM
						x937_files
					WHERE
						header_file_creation_date = '" . $this->m_strHeaderFileCreationDate . "'
						AND processing_bank_id = " . ( int ) $this->m_intProcessingBankId;

		$arrstrData = fetchData( $strSql, $objPaymentDatabase );

		$intNextX937FileId = ( int ) $arrstrData[0]['count'];
		$intNextX937FileId += 1;

		$this->setHeaderFileIdModifier( $intNextX937FileId );
		return $intNextX937FileId;
    }

    public function fetchArPayments( $objPaymentDatabase ) {
		return CArPayments::fetchCustomArPaymentsByX937FileId( $this->getId(), $objPaymentDatabase );
	}

	public function fetchX937CashLetters( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CX937CashLetters::createService()->fetchX937CashLettersByX937FileId( $this->getId(), $objPaymentDatabase );
	}

	public function fetchX937Bundles( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CX937Bundles::createService()->fetchX937BundlesByX937FileId( $this->getId(), $objPaymentDatabase );
	}

	public function fetchX937CheckDetailRecords( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CX937CheckDetailRecords::createService()->fetchX937CheckDetailRecordsByX937FileId( $this->getId(), $objPaymentDatabase );
	}

	public function fetchEftBatch( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CEftBatches::createService()->fetchEftBatchByX937FileId( $this->getId(), $objPaymentDatabase );
	}

	/**
	 * Other Functions
	 */

	// I believe that the majority of the time spent batching x937 files is spent inserting the check detail records and addenda records.

	public function buildAndSaveFile( $boolInsertNewFile = true ) {

		// echo "Building File...\n";

		$boolIsSuccessful 		= true;
		$boolConvertToEbcdic 	= true;

    	CFileIo::recursiveMakeDir( $this->getFilePath() );

		$strFileContent 				= '';
		$strFileContent 				.= \Psi\Libraries\UtilStrings\CStrings::createService()->convert4ByteInt2String( strlen( $this->buildHeaderRecord( $boolConvertToEbcdic ) ), $boolConvertToEbcdic ) . $this->buildHeaderRecord( $boolConvertToEbcdic );
		$intControlTotalRecordCount 	= 1;
		$intControlTotalItemCount 		= 0;
		$fltTotalAmount 				= 0;

		if( false == valArr( $this->m_arrobjX937CashLetters ) ) {
			trigger_error( 'Cash letters were not found.', E_USER_ERROR );
			return false;
		}

		foreach( $this->m_arrobjX937CashLetters as $objX937CashLetter ) {

			$strFileContent 					.= \Psi\Libraries\UtilStrings\CStrings::createService()->convert4ByteInt2String( strlen( $objX937CashLetter->buildHeaderRecord( $boolConvertToEbcdic ) ), $boolConvertToEbcdic ) . $objX937CashLetter->buildHeaderRecord( $boolConvertToEbcdic );
			$intControlTotalRecordCount++;
			$intCashLetterTotalItemCount		= 0;
			$fltCashLetterTotalAmount			= 0;

			$arrobjX937Bundles = $objX937CashLetter->getX937Bundles();
			if( false == valArr( $arrobjX937Bundles ) ) {
				trigger_error( 'Bundle records were not found.', E_USER_ERROR );
				return false;
			}

			if( true == $boolInsertNewFile && false == $objX937CashLetter->insert( $this->m_intUserId, $this->m_objPaymentDatabase ) ) {
				$this->m_objPaymentDatabase->rollback();
				trigger_error( 'X937 file cash letter failed to insert.', E_USER_ERROR );
				exit;
			}

			foreach( $arrobjX937Bundles as $objX937Bundle ) {

				$strFileContent 					.= \Psi\Libraries\UtilStrings\CStrings::createService()->convert4ByteInt2String( strlen( $objX937Bundle->buildHeaderRecord( $boolConvertToEbcdic ) ), $boolConvertToEbcdic ) . $objX937Bundle->buildHeaderRecord( $boolConvertToEbcdic );
				$intControlTotalRecordCount++;
				$fltBundleMicrValidTotalAmount = 0;

				$arrobjX937CheckDetailRecords = $objX937Bundle->getX937CheckDetailRecords();
				if( false == valArr( $arrobjX937CheckDetailRecords ) ) {
					trigger_error( 'Check detail records were not found.', E_USER_ERROR );
					return false;
				}

				if( true == $boolInsertNewFile && false == $objX937Bundle->insert( $this->m_intUserId, $this->m_objPaymentDatabase ) ) {
					$this->m_objPaymentDatabase->rollback();
					trigger_error( 'X937 file bundle failed to insert.', E_USER_ERROR );
					exit;
				}

				foreach( $arrobjX937CheckDetailRecords as $objX937CheckDetailRecord ) {

					$strFileContent .= \Psi\Libraries\UtilStrings\CStrings::createService()->convert4ByteInt2String( strlen( $objX937CheckDetailRecord->buildRecord( $boolConvertToEbcdic ) ), $boolConvertToEbcdic ) . $objX937CheckDetailRecord->buildRecord( $boolConvertToEbcdic );
					$intControlTotalRecordCount++;
					$intControlTotalItemCount++;
					$intCashLetterTotalItemCount++;

					$fltTotalAmount 			+= $objX937CheckDetailRecord->getRebuiltItemAmount();
					$fltCashLetterTotalAmount 	+= $objX937CheckDetailRecord->getRebuiltItemAmount();

					if( '1' == $objX937CheckDetailRecord->getMicrValidIndicator() ) {
						$fltBundleMicrValidTotalAmount += $objX937CheckDetailRecord->getRebuiltItemAmount();
					}

					$objX937CheckDetailAddendumARecord 	= $objX937CheckDetailRecord->getX937CheckDetailAddendumARecord();
					$objX937CheckDetailAddendumCRecord 	= $objX937CheckDetailRecord->getX937CheckDetailAddendumCRecord();
					$objFrontX937ViewDetailRecord 		= $objX937CheckDetailRecord->getFrontX937ViewDetailRecord();
					$objFrontX937ViewDataRecord 		= $objX937CheckDetailRecord->getFrontX937ViewDataRecord();
					$objReverseX937ViewDetailRecord 	= $objX937CheckDetailRecord->getReverseX937ViewDetailRecord();
					$objReverseX937ViewDataRecord 		= $objX937CheckDetailRecord->getReverseX937ViewDataRecord();

					if( false == valObj( $objX937CheckDetailAddendumARecord, 'CX937CheckDetailAddendumARecord' ) ) {
						trigger_error( 'Check detail addendum a record was not found.', E_USER_ERROR );
						return false;
					}

					if( false == valObj( $objX937CheckDetailAddendumCRecord, 'CX937CheckDetailAddendumCRecord' ) ) {
						trigger_error( 'Check detail addendum c record was not found.', E_USER_ERROR );
						return false;
					}

					if( false == valObj( $objFrontX937ViewDetailRecord, 'CX937ViewDetailRecord' ) ) {
						trigger_error( 'Front x937 view detail record was not found.', E_USER_ERROR );
						return false;
					}

					if( false == valObj( $objFrontX937ViewDataRecord, 'CX937ViewDataRecord' ) ) {
						trigger_error( 'Front x937 view data record was not found.', E_USER_ERROR );
						return false;
					}

					if( false == valObj( $objReverseX937ViewDetailRecord, 'CX937ViewDetailRecord' ) ) {
						trigger_error( 'Reverse X937 view detail record was not found.', E_USER_ERROR );
						return false;
					}

					if( false == valObj( $objReverseX937ViewDataRecord, 'CX937ViewDataRecord' ) ) {
						trigger_error( 'Reverse X937 view data record was not found.', E_USER_ERROR );
						return false;
					}

					$strFileContent .= \Psi\Libraries\UtilStrings\CStrings::createService()->convert4ByteInt2String( strlen( $objX937CheckDetailAddendumARecord->buildRecord( $boolConvertToEbcdic ) ), $boolConvertToEbcdic ) . $objX937CheckDetailAddendumARecord->buildRecord( $boolConvertToEbcdic );
					$strFileContent .= \Psi\Libraries\UtilStrings\CStrings::createService()->convert4ByteInt2String( strlen( $objX937CheckDetailAddendumCRecord->buildRecord( $boolConvertToEbcdic ) ), $boolConvertToEbcdic ) . $objX937CheckDetailAddendumCRecord->buildRecord( $boolConvertToEbcdic );
					$strFileContent .= \Psi\Libraries\UtilStrings\CStrings::createService()->convert4ByteInt2String( strlen( $objFrontX937ViewDetailRecord->buildRecord( $boolConvertToEbcdic ) ), $boolConvertToEbcdic ) . $objFrontX937ViewDetailRecord->buildRecord( $boolConvertToEbcdic );
					$strFileContent .= \Psi\Libraries\UtilStrings\CStrings::createService()->convert4ByteInt2String( strlen( $objFrontX937ViewDataRecord->buildRecord( $boolConvertToEbcdic ) ), $boolConvertToEbcdic ) . $objFrontX937ViewDataRecord->buildRecord( $boolConvertToEbcdic );
					$strFileContent .= \Psi\Libraries\UtilStrings\CStrings::createService()->convert4ByteInt2String( strlen( $objReverseX937ViewDetailRecord->buildRecord( $boolConvertToEbcdic ) ), $boolConvertToEbcdic ) . $objReverseX937ViewDetailRecord->buildRecord( $boolConvertToEbcdic );
					$strFileContent .= \Psi\Libraries\UtilStrings\CStrings::createService()->convert4ByteInt2String( strlen( $objReverseX937ViewDataRecord->buildRecord( $boolConvertToEbcdic ) ), $boolConvertToEbcdic ) . $objReverseX937ViewDataRecord->buildRecord( $boolConvertToEbcdic );

					$intControlTotalRecordCount += 6;

					if( true == $boolInsertNewFile && false == $objX937CheckDetailRecord->insert( $this->m_intUserId, $this->m_objPaymentDatabase ) ) {
						$this->m_objPaymentDatabase->rollback();
						trigger_error( 'X937 check detail record failed to insert.', E_USER_ERROR );
						exit;
					}

					if( true == $boolInsertNewFile && false == $objX937CheckDetailAddendumARecord->insert( $this->m_intUserId, $this->m_objPaymentDatabase ) ) {
						$this->m_objPaymentDatabase->rollback();
						trigger_error( 'X937 check detail addendum A record failed to insert.', E_USER_ERROR );
						exit;
					}

					if( true == $boolInsertNewFile && false == $objX937CheckDetailAddendumCRecord->insert( $this->m_intUserId, $this->m_objPaymentDatabase ) ) {
						$this->m_objPaymentDatabase->rollback();
						trigger_error( 'X937 check detail addendum C record failed to insert.', E_USER_ERROR );
						exit;
					}

					if( true == $boolInsertNewFile && false == $objFrontX937ViewDetailRecord->insert( $this->m_intUserId, $this->m_objPaymentDatabase ) ) {
						$this->m_objPaymentDatabase->rollback();
						trigger_error( 'X937 front view detail record failed to insert.', E_USER_ERROR );
						exit;
					}

					if( true == $boolInsertNewFile && false == $objFrontX937ViewDataRecord->insert( $this->m_intUserId, $this->m_objPaymentDatabase ) ) {
						$this->m_objPaymentDatabase->rollback();
						trigger_error( 'X937 front view data record failed to insert.', E_USER_ERROR );
						exit;
					}

					if( true == $boolInsertNewFile && false == $objReverseX937ViewDetailRecord->insert( $this->m_intUserId, $this->m_objPaymentDatabase ) ) {
						$this->m_objPaymentDatabase->rollback();
						trigger_error( 'X937 reverse view detail record failed to insert.', E_USER_ERROR );
						exit;
					}

					if( true == $boolInsertNewFile && false == $objReverseX937ViewDataRecord->insert( $this->m_intUserId, $this->m_objPaymentDatabase ) ) {
						$this->m_objPaymentDatabase->rollback();
						trigger_error( 'X937 reverse view data record failed to insert.', E_USER_ERROR );
						exit;
					}
				}

				if( true == $boolInsertNewFile ) {
					// The total number of items sent within a bundle. [03-06]
					$objX937Bundle->setControlItemsCount( $intCashLetterTotalItemCount );

					// The amount sum value of the items within the bundle. [07-18]
					$objX937Bundle->setFormattedControlTotalAmount( number_format( $fltCashLetterTotalAmount, 2 ) );

					// The amount sum value of all Check Detail Records (Type 25) that contain the Defined Value '1' in the MICR Valid Indicator (Field 10) [19-30]
					$objX937Bundle->setFormattedControlMicrValidTotalAmount( number_format( $fltBundleMicrValidTotalAmount, 2 ) );

					// The total number of image view record pairs within a bundle regardless of whether image data is actually present. Each image view is represented
	    			// by an Image View Detail Record (Type 50) and an Image View Data Record (Type 52) pair. [31-35]
					$objX937Bundle->setControlImagesCount( $intCashLetterTotalItemCount * 2 );

					if( false == $objX937Bundle->update( $this->m_intUserId, $this->m_objPaymentDatabase ) ) {
						$this->m_objPaymentDatabase->rollback();
						trigger_error( 'X937 file bundle failed to update.', E_USER_ERROR );
						exit;
					}
				} else {
					if( $intCashLetterTotalItemCount != $objX937Bundle->getControlItemsCount() ) {
						trigger_error( 'X937 bundle calculated/stored ControlItemsCount are different.', E_USER_ERROR );
						exit;
					}

					if( 0 != bccomp( floatval( $fltCashLetterTotalAmount * 100 ), floatval( $objX937Bundle->getControlTotalAmount() ) ) ) {
						trigger_error( 'X937 bundle calculated/stored FormattedControlTotalAmount are different.', E_USER_ERROR );
						exit;
					}

					if( 0 != bccomp( floatval( $fltBundleMicrValidTotalAmount * 100 ), floatval( $objX937Bundle->getControlMicrValidTotalAmount() ) ) ) {
						trigger_error( 'X937 bundle calculated/stored FormattedControlTotalAmount are different.', E_USER_ERROR );
						exit;
					}

					if( ( $intCashLetterTotalItemCount * 2 ) != $objX937Bundle->getControlImagesCount() ) {
						trigger_error( 'X937 bundle calculated/stored ControlImagesCount are different.', E_USER_ERROR );
						exit;
					}
				}

				$strFileContent .= \Psi\Libraries\UtilStrings\CStrings::createService()->convert4ByteInt2String( strlen( $objX937Bundle->buildControlRecord( $boolConvertToEbcdic ) ), $boolConvertToEbcdic ) . $objX937Bundle->buildControlRecord( $boolConvertToEbcdic );
				$intControlTotalRecordCount++;
			}

			if( true == $boolInsertNewFile ) {
				// The number used to indicate the total number of bundles within the cash letter. [03-08]
				$objX937CashLetter->setControlBundleCount( 1 );

				// The total number of items sent within a cash letter [09-16]
				$objX937CashLetter->setControlItemsCount( $intCashLetterTotalItemCount );

				// The amount sum value of the items within the cash letter. [17-30]
				$objX937CashLetter->setFormattedControlTotalAmount( number_format( $fltCashLetterTotalAmount, 2 ) );

				// The total number of images views within a cash letter.  Each image is represented by an Image View Detail Record (Type 50) and an Image View Data Record (Type 52) pair.
				$objX937CashLetter->setControlImagesCount( $intCashLetterTotalItemCount * 2 );

				if( false == $objX937CashLetter->update( $this->m_intUserId, $this->m_objPaymentDatabase ) ) {
					$this->m_objPaymentDatabase->rollback();
					trigger_error( 'X937 file cash letter failed to update.', E_USER_ERROR );
					exit;
				}
			} else {
				if( 1 != $objX937CashLetter->getControlBundleCount() ) {
					trigger_error( 'X937 cash letter calculated/stored ControlBundleCount are different.', E_USER_ERROR );
					exit;
				}

				if( $intCashLetterTotalItemCount != $objX937CashLetter->getControlItemsCount() ) {
					trigger_error( 'X937 cash letter calculated/stored ControlItemsCount are different.', E_USER_ERROR );
					exit;
				}

				if( 0 != bccomp( floatval( $fltCashLetterTotalAmount * 100 ), floatval( $objX937CashLetter->getControlTotalAmount() ) ) ) {
					trigger_error( 'X937 cash letter calculated/stored FormattedControlTotalAmount are different.', E_USER_ERROR );
					exit;
				}

				if( ( $intCashLetterTotalItemCount * 2 ) != $objX937CashLetter->getControlImagesCount() ) {
					trigger_error( 'X937 cash letter calculated/stored ControlImagesCount are different.', E_USER_ERROR );
					exit;
				}
			}

			$strFileContent .= \Psi\Libraries\UtilStrings\CStrings::createService()->convert4ByteInt2String( strlen( $objX937CashLetter->buildControlRecord( $boolConvertToEbcdic ) ), $boolConvertToEbcdic ) . $objX937CashLetter->buildControlRecord( $boolConvertToEbcdic );
			$intControlTotalRecordCount++;
		}

		// Increment one more to include the file control record.
		$intControlTotalRecordCount++;

		if( true == $boolInsertNewFile ) {
			// The total number of cash letters within the file [03-08]
			$this->setControlCashLetterCount( \Psi\Libraries\UtilFunctions\count( $this->m_arrobjX937CashLetters ) );

			// The total number of records of all types sent in the file, including the File Header Record (Type 01) and the File Control Record [9-16]
			$this->setControlTotalRecordCount( $intControlTotalRecordCount );

			// The total number of items sent within the file, all Check Detail Records (Type 25), all Return Records (Type 31) and all Credit/Reconcilation records (Type 61) [17-24]
			$this->setControlTotalItemCount( $intControlTotalItemCount );

			// The total US dollar value of the complete file, all Check Detail Records (Type 25) and all Return Records (Type 31)
			$this->setFormattedControlFileTotalAmount( number_format( $fltTotalAmount, 2 ) );
		} else {
			if( \Psi\Libraries\UtilFunctions\count( $this->m_arrobjX937CashLetters ) != $this->getControlCashLetterCount() ) {
				trigger_error( 'X937 file calculated/stored ControlCashLetterCount are different.', E_USER_ERROR );
				exit;
			}

			if( $intControlTotalRecordCount != $this->getControlTotalRecordCount() ) {
				trigger_error( 'X937 file calculated/stored ControlTotalRecordCount are different.', E_USER_ERROR );
				exit;
			}

			if( $intControlTotalItemCount != $this->getControlTotalItemCount() ) {
				trigger_error( 'X937 file calculated/stored ControlTotalItemCount are different.', E_USER_ERROR );
				exit;
			}

			if( 0 != bccomp( floatval( $fltTotalAmount * 100 ), floatval( $this->getControlFileTotalAmount() ) ) ) {
				trigger_error( 'X937 file calculated/stored FormattedControlFileTotalAmount are different.', E_USER_ERROR );
				exit;
			}
		}

		$strFileContent .= \Psi\Libraries\UtilStrings\CStrings::createService()->convert4ByteInt2String( strlen( $this->buildControlRecord( $boolConvertToEbcdic ) ), $boolConvertToEbcdic ) . $this->buildControlRecord( $boolConvertToEbcdic );
		$this->setFileContent( $strFileContent );

		// echo "Writing file: " . $this->getFilePath() . $this->getFileName() . "...\n\n";

    	// Truncate file to zero length
    	$resHandle = CFileIo::fileOpen( $this->getFilePath() . $this->getFileName(), 'w' );

		if( false === fwrite( $resHandle, $strFileContent ) ) {
    		trigger_error( 'File could not be saved to file system.', E_USER_ERROR );
    		exit;
    	}

		try {
			$objPutx937File = \Psi\Core\Payment\PaymentFiles\CPutX937File::createService();
			$objPutx937File->setX937File( $this )
				->setObjectStorageKey( self::PATH_X937_FILES . $this->m_strFilePath . $this->getFileName() );

			$objPutx937File->paymentSystemPutObject();
		} catch( \Throwable $objThrowable ) {
			trigger_error( 'X937 file could not be saved to the object store. X937 File ID: ' . $this->getId(), E_USER_NOTICE );
		}

    	if( true == $boolInsertNewFile ) {
			if( false == $this->update( SYSTEM_USER_ID, $this->m_objPaymentDatabase ) ) {
				$this->m_objPaymentDatabase->rollback();
				trigger_error( 'Customer payment nacha file failed to update.', E_USER_ERROR );
				exit;
			}
    	}

    	return $boolIsSuccessful;
    }

    public function loadFileNameAndFilePath() {

		$arrobjMerchantGateways = \Psi\Eos\Payment\CMerchantGateways::createService()->fetchAllMerchantGateways( $this->m_objPaymentDatabase );
		$arrobjX937FileTypes 	= \Psi\Eos\Payment\CX937FileTypes::createService()->fetchAllX937FileTypes( $this->m_objPaymentDatabase );

		$intNextFileNumber = $this->fetchNextX937FileIdModifier( $this->m_objPaymentDatabase );
		$strFileName = NULL;

		// Filename needs to be formatted differently for each processing bank

		switch( $this->getProcessingBankId() ) {
			case CProcessingBank::ZIONS_BANK:
				// Example: C015121D20100104B0001_1797.c21
				// $strFileName = 'C015121D' . date( 'Ymd' ) . 'B' . str_pad( $intNextFileNumber, 4, "0", STR_PAD_LEFT ) . '_' . $this->getId() . '.c21';
				$strFileName = 'C' . substr( $this->getHeaderImmediateOriginRoutingNumber(), strlen( $this->getHeaderImmediateOriginRoutingNumber() ) - 6, 6 ) . 'D' . date( 'Ymd' ) . 'B' . str_pad( $intNextFileNumber, 4, '0', STR_PAD_LEFT ) . '_' . $this->getId() . '.c21';
				break;

			case CProcessingBank::FIFTH_THIRD:
				// Example Dev/Test: PSOL_TESTX937-53_20111031_0003244.txt 	( Format: PSOL_TESTX937-53_{YYYYMMDD}_{NNNNNNN}.txt, where NNNNNNN is file id left zero padded to 7 digits)
				// Example Prod: 	 PSOL_X937-53_20111031_0029887.txt 		( Format: PSOL_X937-53_{YYYYMMDD}_{NNNNNNN}.txt, where NNNNNNN is file id left zero padded to 7 digits)
				$strFileName = 'PSOL_';

				if( CONFIG_ENVIRONMENT != 'production' ) {
					$strFileName .= 'TEST';
				}

				$strFileName .= sprintf( 'X937-53_%s_%07s.txt', date( 'Ymd' ), $this->getId() );
				break;

			case CProcessingBank::FIRST_REGIONAL:
				// Example: 20100104_PSI_CustomerPayments_Daily_x937_File_No_1_1797.dat
				$strFileName = date( 'Ymd' ) . '_PSI_' . preg_replace( '/[^a-zA-Z0-9]/', '', trim( $arrobjX937FileTypes[$this->m_intX937FileTypeId]->getName() ) ) . '_Daily_x937_File_No_' . $intNextFileNumber . '_' . $this->getId() . '.dat';
				break;

			default:
				// default case
				break;
		}

    	if( true == is_null( $strFileName ) ) {
			trigger_error( 'File name could not be set.', E_USER_ERROR );
			exit;
		}

    	$this->setFileName( $strFileName );
		$this->setFilePath( preg_replace( '/[^a-zA-Z0-9]/', '', trim( $arrobjMerchantGateways[$this->m_intMerchantGatewayId]->getName() ) ) . '/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . preg_replace( '/[^a-zA-Z0-9]/', '', trim( $arrobjX937FileTypes[$this->m_intX937FileTypeId]->getName() ) ) . '/' );
    }

	public function buildHeaderRecord( $boolConvertToEbcdic = false ) {

		$strRecord = '';

		$strRecord .= $this->getHeaderRecordType();
		$strRecord .= $this->getHeaderStandardLevel();
		$strRecord .= $this->getHeaderTestFileIndicator();
		$strRecord .= $this->getHeaderImmediateDestinationRoutingNumber();
		$strRecord .= $this->getHeaderImmediateOriginRoutingNumber();
		$strRecord .= $this->getHeaderFileCreationDate();
		$strRecord .= $this->getHeaderFileCreationTime();
		$strRecord .= $this->getHeaderResendIndicator();
		$strRecord .= $this->getHeaderImmediateDestinationName();
		$strRecord .= $this->getHeaderImmediateOrginName();
		$strRecord .= $this->getHeaderFileIdModifier();
		$strRecord .= $this->getHeaderCountryCode();
		$strRecord .= $this->getHeaderUserField();
		$strRecord .= $this->getHeaderReserved();

		if( true == $boolConvertToEbcdic ) {
			$strRecord = \Psi\Libraries\UtilStrings\CStrings::createService()->asciiToEbcdic( $strRecord );
		}

		return $strRecord;
	}

	public function buildControlRecord( $boolConvertToEbcdic = false ) {

		$strRecord = '';

		$strRecord .= $this->getControlRecordType();
		$strRecord .= $this->getControlCashLetterCount();
		$strRecord .= $this->getControlTotalRecordCount();
		$strRecord .= $this->getControlTotalItemCount();
		$strRecord .= $this->getControlFileTotalAmount();
		$strRecord .= $this->getControlImmediateOriginContactName();
		$strRecord .= $this->getControlImmediateOriginContactPhoneNumber();
		$strRecord .= $this->getControlReserved();

		if( true == $boolConvertToEbcdic ) {
			$strRecord = \Psi\Libraries\UtilStrings\CStrings::createService()->asciiToEbcdic( $strRecord );
		}

		return $strRecord;
	}

    public function restoreDatabaseData( $objPaymentDatabase ) {

    	// ******************************************************************************************************
    	//  This function is to reload all the x937 file data back into the objects for the purpose
    	//  of file regneration or data validation
		// ******************************************************************************************************

		// DO THIS IN CALLER
		// $objX937File = \Psi\Eos\Payment\CX937Files::createService()->fetchX937FileById( $intParamX937FileId, $objDatabase );
		// $objX937File->restoreDatabaseData( $objDatabase );
		// $objX937File->buildAndSaveFile( false );

		// echo "Nesting objects...\n";

        $this->m_arrobjX937CashLetters = \Psi\Eos\Payment\CX937CashLetters::createService()->fetchX937CashLettersByX937FileId( $this->getId(), $objPaymentDatabase );
        $arrobjX937Bundles = \Psi\Eos\Payment\CX937Bundles::createService()->fetchX937BundlesByX937FileId( $this->getId(), $objPaymentDatabase );
        $arrobjX937CheckDetailRecords = \Psi\Eos\Payment\CX937CheckDetailRecords::createService()->fetchX937CheckDetailRecordsByX937FileIdOrderedById( $this->getId(), $objPaymentDatabase );
        $arrobjX937CheckDetailAddendumARecords = \Psi\Eos\Payment\CX937CheckDetailAddendumARecords::createService()->fetchX937CheckDetailAddendumARecordsByX937FileId( $this->getId(), $objPaymentDatabase );
        $arrobjX937CheckDetailAddendumBRecords = \Psi\Eos\Payment\CX937CheckDetailAddendumBRecords::createService()->fetchX937CheckDetailAddendumBRecordsByX937FileId( $this->getId(), $objPaymentDatabase );
        $arrobjX937CheckDetailAddendumCRecords = \Psi\Eos\Payment\CX937CheckDetailAddendumCRecords::createService()->fetchX937CheckDetailAddendumCRecordsByX937FileId( $this->getId(), $objPaymentDatabase );

        CObjectModifiers::createService()->nestObjects( $arrobjX937Bundles, $this->m_arrobjX937CashLetters );
        CObjectModifiers::createService()->nestObjects( $arrobjX937CheckDetailRecords, $arrobjX937Bundles );
        CObjectModifiers::createService()->nestObjects( $arrobjX937CheckDetailAddendumARecords, $arrobjX937CheckDetailRecords, true );
        CObjectModifiers::createService()->nestObjects( $arrobjX937CheckDetailAddendumBRecords, $arrobjX937CheckDetailRecords, true );
        CObjectModifiers::createService()->nestObjects( $arrobjX937CheckDetailAddendumCRecords, $arrobjX937CheckDetailRecords, true );

        // ******************************************************************************************************
        // We need the front and reverse image for each payment.  Here we create an array of ArPayment objects,
        //  fetch the corresponding front and reverse ArPaymentImage objects into each ArPayment, and then read
        //  the image content into each ArPaymentImage from the file system.
        // ******************************************************************************************************

        $arrintArPaymentIds = array();
        $arrobjArPayments = array();

        if( true == valArr( $arrobjX937CheckDetailRecords ) ) {
			foreach( $arrobjX937CheckDetailRecords as $objX937CheckDetailRecord ) {
		    	$arrintArPaymentIds[$objX937CheckDetailRecord->getArPaymentId()] = $objX937CheckDetailRecord->getArPaymentId();
		    	$arrobjArPayments[$objX937CheckDetailRecord->getArPaymentId()] = new CArPayment();
		    	$arrobjArPayments[$objX937CheckDetailRecord->getArPaymentId()]->setId( $objX937CheckDetailRecord->getArPaymentId() );
		    }
        }

    	$arrobjArPaymentImages = CArPaymentImages::fetchArPaymentImagesByArPaymentIds( $arrintArPaymentIds, $objPaymentDatabase );

    	if( true == valArr( $arrobjArPaymentImages ) ) {
		    foreach( $arrobjArPaymentImages as $objArPaymentImage ) {
		 		$objArPaymentImage->readImageContentFromFileSystem();
		 		// echo $objArPaymentImage->getImagePath() . $objArPaymentImage->getImageName() . "\n";

				if( CPaymentImageType::FRONT == $objArPaymentImage->getPaymentImageTypeId() ) {
					$arrobjArPayments[$objArPaymentImage->getArPaymentId()]->setArPaymentImageFront( $objArPaymentImage );
				} elseif( CPaymentImageType::REVERSE == $objArPaymentImage->getPaymentImageTypeId() ) {
					$arrobjArPayments[$objArPaymentImage->getArPaymentId()]->setArPaymentImageReverse( $objArPaymentImage );
				}
		    }
    	}
    	// echo "Image count: " . sizeof ( $arrobjArPaymentImages ) . "\n";

        // ******************************************************************************************************
        // We need to nest the 2 X937ViewDetailRecord and the 2 X937ViewDataRecord objects into each CheckDetailRecord
        //  Each X937ViewDataRecord record needs to be with image data from the corresponding ArPaymentImage object
        // ******************************************************************************************************

    	if( true == valArr( $arrobjX937CheckDetailRecords ) ) {
			foreach( $arrobjX937CheckDetailRecords as $objX937CheckDetailRecord ) {

		        $arrobjX937ViewDetailRecords = \Psi\Eos\Payment\CX937ViewDetailRecords::createService()->fetchX937ViewDetailRecordsByX937CheckDetailRecordId( $objX937CheckDetailRecord->getId(), $objPaymentDatabase );
		        $arrobjX937ViewDataRecords = \Psi\Eos\Payment\CX937ViewDataRecords::createService()->fetchX937ViewDataRecordsByX937CheckDetailRecordId( $objX937CheckDetailRecord->getId(), $objPaymentDatabase );

		        if( true == valArr( $arrobjX937ViewDetailRecords ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrobjX937ViewDetailRecords ) ) {

	        		$objX937ViewDetailRecord = reset( $arrobjX937ViewDetailRecords );

	        		// "0" is the front ViewSideIndicator, "1" is the reverse ViewSideIndicator
		        	if( '0' == $objX937ViewDetailRecord->getViewSideIndicator() ) {
	        			$objX937CheckDetailRecord->setFrontX937ViewDetailRecord( $objX937ViewDetailRecord );
		    			$objX937CheckDetailRecord->setReverseX937ViewDetailRecord( next( $arrobjX937ViewDetailRecords ) );
	        		} elseif( '1' == $objX937ViewDetailRecord->getViewSideIndicator() ) {
	        			$objX937CheckDetailRecord->setReverseX937ViewDetailRecord( $objX937ViewDetailRecord );
	        			$objX937CheckDetailRecord->setFrontX937ViewDetailRecord( next( $arrobjX937ViewDetailRecords ) );
	        		} else {
	   					trigger_error( 'Unsupported ViewSideIndicator value ' . $objX937ViewDetailRecord->getViewSideIndicator(), E_USER_ERROR );
	   					return false;
	   				}
		        }

		    	if( true == valArr( $arrobjX937ViewDataRecords ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrobjX937ViewDataRecords ) ) {

		    		$objFirstX937ViewDataRecord = reset( $arrobjX937ViewDataRecords );
		    		$objSecondX937ViewDataRecord = next( $arrobjX937ViewDataRecords );
		    		$strFrontImageContent   = $arrobjArPayments[$objX937CheckDetailRecord->getArPaymentId()]->getArPaymentImageFront()->getImageContent();
		    		$strReverseImageContent = $arrobjArPayments[$objX937CheckDetailRecord->getArPaymentId()]->getArPaymentImageReverse()->getImageContent();

		    		// Front is inserted first, Reverse is second
		    		if( $objFirstX937ViewDataRecord->getId() < $objSecondX937ViewDataRecord->getId() ) {
		    			if( $objFirstX937ViewDataRecord->getLengthOfImageData() != strlen( $strFrontImageContent ) ) {
		    				trigger_error( 'Front image data length stored/actual unmatched (#' . $objX937CheckDetailRecord->getArPaymentId() . '): ' . $objFirstX937ViewDataRecord->getLengthOfImageData() . ' and ' . strlen( $strFrontImageContent ), E_USER_WARNING );
		    			}

		    			if( $objSecondX937ViewDataRecord->getLengthOfImageData() != strlen( $strReverseImageContent ) ) {
		    				trigger_error( 'Reverse image data length stored/actual unmatched (#' . $objX937CheckDetailRecord->getArPaymentId() . '): ' . $objSecondX937ViewDataRecord->getLengthOfImageData() . ' and ' . strlen( $strReverseImageContent ), E_USER_WARNING );
		    			}

		    			$objFirstX937ViewDataRecord->setImageData( $strFrontImageContent );
		    			$objSecondX937ViewDataRecord->setImageData( $strReverseImageContent );
		    			$objX937CheckDetailRecord->setFrontX937ViewDataRecord( $objFirstX937ViewDataRecord );
		    			$objX937CheckDetailRecord->setReverseX937ViewDataRecord( $objSecondX937ViewDataRecord );
		    		} else {
		    			if( $objFirstX937ViewDataRecord->getLengthOfImageData() != strlen( $strReverseImageContent ) ) {
		    				trigger_error( 'Reverse image data length stored/actual unmatched (#' . $objX937CheckDetailRecord->getArPaymentId() . '): ' . $objFirstX937ViewDataRecord->getLengthOfImageData() . ' and ' . strlen( $strReverseImageContent ), E_USER_WARNING );
		    			}

		    			if( $objSecondX937ViewDataRecord->getLengthOfImageData() != strlen( $strFrontImageContent ) ) {
		    				trigger_error( 'Front image data length stored/actual unmatched (#' . $objX937CheckDetailRecord->getArPaymentId() . '): ' . $objSecondX937ViewDataRecord->getLengthOfImageData() . ' and ' . strlen( $strFrontImageContent ), E_USER_WARNING );
		    			}

		    			$objFirstX937ViewDataRecord->setImageData( $strReverseImageContent );
		    			$objSecondX937ViewDataRecord->setImageData( $strFrontImageContent );
		    			$objX937CheckDetailRecord->setFrontX937ViewDataRecord( $objSecondX937ViewDataRecord );
		    			$objX937CheckDetailRecord->setReverseX937ViewDataRecord( $objFirstX937ViewDataRecord );
		    		}
		        }
		    }
	    }

	    // echo "Done nesting objects\n\n";

	    // Add a prefix to the filename
	    $this->setFileName( 'regenerated_' . $this->getFileName() );

	    return true;
    }

}
?>