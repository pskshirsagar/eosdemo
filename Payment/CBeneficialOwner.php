<?php

class CBeneficialOwner extends CBaseBeneficialOwner {

	const UNDERWRITING_TYPE_CLIENT = 1;
	const UNDERWRITING_TYPE_PROPERTY = 2;

	const ALLOWED_FIELD_DISTANCE = 5;
	const MATCHES_NEEDED_FOR_UPDATE = 2;

	public function __construct() {
		parent::__construct();

		$this->m_intUnderwritingTypeId = 1;
		$this->m_intOwnerType = 1;
		$this->m_intOwnerOwnershipPercent = 0;

		return;
	}

	public function applyRequestForm( $arrstrRequestForm, $arrstrFormFields = NULL ) {

		if( true == valArr( $arrstrFormFields ) ) {
			$arrstrRequestForm = mergeIntersectArray( $arrstrFormFields, $arrstrRequestForm );
		}

		$this->setValues( $arrstrRequestForm, false );
	}

	public function setEncryptOwnerSsn( $strOwnerSsn ) {
		if( false == valStr( $this->getOwnerSsnEncrypted() ) || false == valStr( $strOwnerSsn ) ) {
			return NULL;
		}

		$this->m_strOwnerSsnEncrypted = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( CStrings::strTrimDef( $strOwnerSsn, 255, NULL, true ), CONFIG_SODIUM_KEY_APPLICATION_KEY_VALUE );

		$this->setOwnerSsnEncrypted( $this->m_strOwnerSsnEncrypted );

		return $this->m_strOwnerSsnEncrypted;
	}

	public function getOwnerSsn( $boolIsMasked = false ) {
		if( false == valStr( $this->getOwnerSsnEncrypted() ) ) {
			return NULL;
		}

		$strOwnerSsn = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getOwnerSsnEncrypted(), CONFIG_SODIUM_KEY_APPLICATION_KEY_VALUE, [ 'legacy_secret_key' => CONFIG_KEY_APPLICATION_KEY_VALUE ] );

		if( true == $boolIsMasked ) {
			$strOwnerSsn = '###-##-' . substr( $strOwnerSsn, -4 );
		}

		return $strOwnerSsn;
	}

	/**
	 * Validate Functions
	 */
	public function valCid( $intCid ) {
			$boolIsValid = true;
			if( true == is_null( $intCid ) ) {
				$boolIsValid = false;
			}
		return $boolIsValid;
	}

	public function valUnderwritingTypeId( $intUnderwritingTypeId ) {
			$boolIsValid = true;
			if( true == is_null( $intUnderwritingTypeId ) ) {
				$boolIsValid = false;
			}
		return $boolIsValid;
	}

	public function valOwnerName( $strOwnerName ) {
			$boolIsValid = true;
			if( true == empty( $strOwnerName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Owner Name', 'Beneficial Owner name is required.' ) );
				$boolIsValid = false;
			}
		return $boolIsValid;
	}

	public function valOwnerSsnEncrypted( $strOwner_ssn_encrypted ) {
			$boolIsValid = true;
			if( true == empty( $strOwner_ssn_encrypted ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'SSN', 'Beneficial Owner SS# (TIN/EIN#) is required' ) );
				$boolIsValid = false;
			}
		return $boolIsValid;
	}

	public function valOwnerDob( $strOwnerDob ) {
			$boolIsValid = true;
			if( true == empty( $strOwnerDob ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Owner DOB', 'Beneficial Owner birth/formation date is required.' ) );
				$boolIsValid = false;
			}
		return $boolIsValid;
	}

	public function valOwnerOwnershipPercent( $intOwnerOwnershipPercent ) {
			$boolIsValid = true;
			if( false == is_numeric( $intOwnerOwnershipPercent ) || $intOwnerOwnershipPercent < 0 || $intOwnerOwnershipPercent > 100 ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Owner Pwnership', 'Beneficial Owner/Member Equity Ownership is required.' ) );
				$boolIsValid = false;
			}
		return $boolIsValid;
	}

	public function valOwnerAddress( $strOwnerAddress ) {
			$boolIsValid = true;
			if( true == empty( $strOwnerAddress ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Owner Address', 'Beneficial Owner home/legal address is required.' ) );
				$boolIsValid = false;
			}
		return $boolIsValid;
	}

	public function valOwnerCity( $strOwnerCity ) {
			$boolIsValid = true;
			if( true == empty( $strOwnerCity ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Owner City', 'Beneficial Owner city is required.' ) );
				$boolIsValid = false;
			}
		return $boolIsValid;
	}

	public function valOwnerStateCode( $strOwnerStateCode ) {
			$boolIsValid = true;
			if( true == empty( $strOwnerStateCode ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Owner State Code', 'Beneficial Owner state code is required.' ) );
				$boolIsValid = false;
			}
		return $boolIsValid;
	}

	public function valOwnerPostalCode( $strOwnerPostalCode ) {
			$boolIsValid = true;
			if( true == empty( $strOwnerPostalCode ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Owner Postal Code', 'Beneficial Owner zip code is required.' ) );
				$boolIsValid = false;
			}
		return $boolIsValid;
	}

	public function valOwnerPhoneNumber( $strOwnerPhoneNumber ) {
			$boolIsValid = true;
			if( true == empty( $strOwnerPhoneNumber ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Owner Phone Number', 'Beneficial Owner phone number is required.' ) );
				$boolIsValid = false;
			}
		return $boolIsValid;
	}

	public function valOwnerEmail( $strOwnerEmail ) {
			$boolIsValid = true;
			if( true == empty( $strOwnerEmail ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Owner Email', 'Beneficial email address is required.' ) );
				$boolIsValid = false;
			}
		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixBeneficialOwnerKeyValues ) {

			$boolIsValid = true;
			switch( $strAction ) {
				case 'VALIDATE_INSERT':
				case 'VALIDATE_UPDATE':
						$boolIsValid &= $this->valCid( $arrmixBeneficialOwnerKeyValues['cid'] );
						$boolIsValid &= $this->valUnderwritingTypeId( $arrmixBeneficialOwnerKeyValues['underwriting_type_id'] );
						$boolIsValid &= $this->valOwnerName( $arrmixBeneficialOwnerKeyValues['owner_name'] );
						$boolIsValid &= $this->valOwnerSsnEncrypted( $arrmixBeneficialOwnerKeyValues['owner_ssn_encrypted'] );
						$boolIsValid &= $this->valOwnerDob( $arrmixBeneficialOwnerKeyValues['owner_dob'] );
						$boolIsValid &= $this->valOwnerOwnershipPercent( $arrmixBeneficialOwnerKeyValues['owner_ownership_percent'] );
						$boolIsValid &= $this->valOwnerAddress( $arrmixBeneficialOwnerKeyValues['owner_address'] );
						$boolIsValid &= $this->valOwnerCity( $arrmixBeneficialOwnerKeyValues['owner_city'] );
						$boolIsValid &= $this->valOwnerStateCode( $arrmixBeneficialOwnerKeyValues['owner_state_code'] );
						$boolIsValid &= $this->valOwnerPostalCode( $arrmixBeneficialOwnerKeyValues['owner_postal_code'] );
						$boolIsValid &= $this->valOwnerPhoneNumber( $arrmixBeneficialOwnerKeyValues['owner_phone_number'] );
						$boolIsValid &= $this->valOwnerEmail( $arrmixBeneficialOwnerKeyValues['owner_email'] );
					break;

				default:
						// default case
						$boolIsValid = true;
					break;
			}

		return $boolIsValid;
	}

}

?>