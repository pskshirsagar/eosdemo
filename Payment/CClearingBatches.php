<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CClearingBatches
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CClearingBatches extends CBaseClearingBatches {

    public static function fetchRetryClearingBatchesByClearingBatchTypeIdByProcessingBankId( $arrintClearingBatchTypeIds, $intProcessingBankId, $objPaymentDatabase ) {

    	$strSql = 'SELECT
    					cb.*
    				FROM
    					clearing_batches cb
    				WHERE
				    	cb.returned_on IS NOT NULL
				    	AND cb.clearing_amount > 0
				    	AND cb.clearing_batch_type_id IN ( ' . implode( ',', $arrintClearingBatchTypeIds ) . ' )
				    	AND cb.processing_bank_id = ' . ( int ) $intProcessingBankId;

    	return self::fetchClearingBatches( $strSql, $objPaymentDatabase );
    }

	public static function fetchClearingBatchesProcessingBankAccountDetails( $arrintClearingBatchIds, $objAdminDatabase, $objPaymentDatabase ) {
		// ********************************************************************************************
	 	// Fetch ar_payments for these clearing batch ids via transactions in admin
		// ********************************************************************************************

		$strSql = '	SELECT
					    t.clearing_batch_id,
					    t.ar_payment_id,
					    t.transaction_amount
					FROM
					    transactions t
					    JOIN clients c ON t.cid = c.id AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
					WHERE
					    t.charge_code_id IN ( ' . CChargeCode::CHARITY_DONATION . ' )
					    AND t.clearing_batch_id IN ( ' . implode( ',', $arrintClearingBatchIds ) . ' )
					ORDER BY
					    t.clearing_batch_id,
					    t.ar_payment_id';

		$arrmixBatchData = fetchData( $strSql, $objAdminDatabase );
		if( false == valArr( $arrmixBatchData ) ) return NULL;

		$arrintArPaymentIds = array_filter( array_keys( rekeyArray( 'ar_payment_id', $arrmixBatchData ) ) );

		// ***************************************************************************************
		// Get the processing_bank_account_id for these payments
		// ***************************************************************************************

		$strSql = '	SELECT
						ap.id ar_payment_id,
					    ap.processing_bank_account_id
					FROM
						ar_payments ap
					WHERE
						ap.id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' )
					ORDER BY
						ap.cid,
					    ap.processing_bank_account_id;';

		$arrmixPaymentData = fetchData( $strSql, $objPaymentDatabase );
		if( false == valArr( $arrmixPaymentData ) ) return NULL;

		$arrmixRekeyedPaymentData = rekeyArray( 'ar_payment_id', $arrmixPaymentData );

		// ********************************************************************************************
		// We'll return a mixed array containing values:
		//  array ( clearing_batch_id, ar_payment_id, transaction_amount, processing_bank_account_id )
		// ********************************************************************************************

		foreach( $arrmixBatchData as &$mixBatchData ) {
			$mixBatchData['processing_bank_account_id'] = $arrmixRekeyedPaymentData[$mixBatchData['ar_payment_id']]['processing_bank_account_id'];
		}

		return $arrmixBatchData;
	}

}
?>