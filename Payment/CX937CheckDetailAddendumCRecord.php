<?php

class CX937CheckDetailAddendumCRecord extends CBaseX937CheckDetailAddendumCRecord {

	protected $m_objPaymentDatabase;

    /**
     * Set Functions
     */

    // This record is the equivalent to an endorsement on the back of a check.

	public function setDefaults() {

    	$this->setId( $this->fetchNextId( $this->m_objPaymentDatabase ) );

    	// A code used to identify this type of record. [01-02]
    	$this->setRecordType( '28' );

    	// This field shall contain the number representing the chronological order (oldest to newest) in which each Check Detail Addendum C
    	// Record (Type 28), associated with the immediately preceding Check Detail Record (Type 25) was created.  The order shall represent
    	// the endorsement order with '01' being the oldest endorsement.  Check Detail Addendum C Records shall be in sequential order starting
    	// with '01'. (I assume we will only have one endorsement). [03-04]
    	$this->setCheckDetailAddendumCRecordNumber( '1' );

    	// A number used to identify the bank that endorsed the check. [05-13]
    	// Should be set dynamically, depending on processing bank.
    	// $this->setEndorsingBankRoutingNumber( '122242597' ); -- set in CX937CheckDetailRecord

    	// The year, month and day in the endorsement that designates the business data at the endorsing bank. [14-21]
    	$this->setEndorsingBankEndorsementDate( date( 'Ymd' ) );

		// A number used to identify the item at the endorsing bank. [22-36]
		 // Set in Check Detail Record.  According to Mark at the Federal Reserve this should be the sequence id from the check detail record.
    	// $this->setEndorsingBankItemSequenceNumber();

    	// An indicator used to identify if this endorsing institution is the truncator of the original check. [37-37]
    	// Mark instructed me to set this value to yes.
    	$this->setTruncationIndicator( 'Y' );

		// A code used to indicate the conversion between the physical document, an image, or a subsequent IRD.  The indicator is specific to the action of this ensorser. [38-38]
    	$this->setEndorsingBankConversionIndicator( ' ' );

    	// An indicator to identify whether and how the MICR line was repaired by the endorsing bank, for fields other than the Payor Bank Routing Number and Amount. [39-39]
    	$this->setEndorsingBankCorrectionIndicator( ' ' );

    	// A code used to indicate the reason for non-payment on a represented item.  Thei is the reason for return under applicable law or exchange agreements.  This code
    	// can be obtained from the Return Reason included in the Return Addendum B Record (Type 33) or from teh Return Addendum D Record (Type 35). [40]
    	$this->setReturnReason( ' ' );

    	// A field used at the descretion of users of the standard. [41-55]
    	$this->setUserField( '               ' );

    	// A field reserved for future use by the Accredited Standards Committee X9. [56-80]
    	$this->setReserved( '               ' );
	}

    public function setPaymentDatabase( $objPaymentDatabase ) {
    	$this->m_objPaymentDatabase = $objPaymentDatabase;
    }

	public function setRecordType( $strRecordType ) {
	    $this->m_strRecordType = CStrings::strNachaDef( $strRecordType, 2, true, true, '0' );
	}

	public function setCheckDetailAddendumCRecordNumber( $strCheckDetailAddendumCRecordNumber ) {
	    $this->m_strCheckDetailAddendumCRecordNumber = CStrings::strNachaDef( $strCheckDetailAddendumCRecordNumber, 2, true, true, '0' );
	}

	public function setEndorsingBankRoutingNumber( $strEndorsingBankRoutingNumber ) {
	    $this->m_strEndorsingBankRoutingNumber = CStrings::strNachaDef( $strEndorsingBankRoutingNumber, 9, true, true, '0' );
	}

	public function setEndorsingBankEndorsementDate( $strEndorsingBankEndorsementDate ) {
	    $this->m_strEndorsingBankEndorsementDate = CStrings::strNachaDef( $strEndorsingBankEndorsementDate, 8, true, true, '0' );
	}

	public function setEndorsingBankItemSequenceNumber( $strEndorsingBankItemSequenceNumber ) {
	    $this->m_strEndorsingBankItemSequenceNumber = CStrings::strNachaDef( $strEndorsingBankItemSequenceNumber, 15, true, true, '0' );
	}

	public function setTruncationIndicator( $strTruncationIndicator ) {
	    $this->m_strTruncationIndicator = CStrings::strNachaDef( $strTruncationIndicator, 1, false, false );
	}

	public function setEndorsingBankConversionIndicator( $strEndorsingBankConversionIndicator ) {
	    $this->m_strEndorsingBankConversionIndicator = CStrings::strNachaDef( $strEndorsingBankConversionIndicator, 1, false, false );
	}

	public function setEndorsingBankCorrectionIndicator( $strEndorsingBankCorrectionIndicator ) {
	    $this->m_strEndorsingBankCorrectionIndicator = CStrings::strNachaDef( $strEndorsingBankCorrectionIndicator, 1, true, true, '0' );
	}

	public function setReturnReason( $strReturnReason ) {
	    $this->m_strReturnReason = CStrings::strNachaDef( $strReturnReason, 1, false, false );
	}

	public function setUserField( $strUserField ) {
	    $this->m_strUserField = CStrings::strNachaDef( $strUserField, 15, false, false );
	}

	public function setReserved( $strReserved ) {
	    $this->m_strReserved = CStrings::strNachaDef( $strReserved, 25, false, false );
	}

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	/**
	 * Other Functions
	 */

	public function buildRecord( $boolConvertToEbcdic = false ) {

		$strRecord = '';

		$strRecord .= $this->getRecordType();
		$strRecord .= $this->getCheckDetailAddendumCRecordNumber();
		$strRecord .= $this->getEndorsingBankRoutingNumber();
		$strRecord .= $this->getEndorsingBankEndorsementDate();
		$strRecord .= $this->getEndorsingBankItemSequenceNumber();
		$strRecord .= $this->getTruncationIndicator();
		$strRecord .= $this->getEndorsingBankConversionIndicator();
		$strRecord .= $this->getEndorsingBankCorrectionIndicator();
		$strRecord .= $this->getReturnReason();
		$strRecord .= $this->getUserField();
		$strRecord .= $this->getReserved();

		if( true == $boolConvertToEbcdic ) {
			$strRecord = \Psi\Libraries\UtilStrings\CStrings::createService()->asciiToEbcdic( $strRecord );
		}

		return $strRecord;
	}
}
?>