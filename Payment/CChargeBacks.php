<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CChargeBacks
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CChargeBacks extends CBaseChargeBacks {

	public static function fetchUnReconPreppedChargeBacks( $objPaymentDatabase ) {

		$strSql = 'SELECT
						cb.*,
						ap.total_amount AS total,
						ap.processing_bank_account_id
					FROM
						charge_backs cb,
						ar_payments ap,
                        clients c,
						company_merchant_accounts cma,
                        merchant_account_methods mam
					WHERE
						cb.ar_payment_id = ap.id
                    AND ap.cid = c.id
                    AND c.company_status_type_id IN ( ' . CCompanyStatusType::CLIENT . ', ' . CCompanyStatusType::TERMINATED . ' )
                    AND ap.company_merchant_account_id = cma.id
				    AND cma.id = mam.company_merchant_account_id
				    AND ap.payment_type_id = mam.payment_type_id
				    AND ap.payment_medium_id = mam.payment_medium_id
                    AND mam.is_controlled_distribution = 1
                    AND cb.is_recon_prepped <> 1
                    AND ap.total_amount * 100 <> cb.amount';

		return self::fetchChargeBacks( $strSql, $objPaymentDatabase );
	}

	public static function fetchChargeBackByArPaymentIdByCidByIsRefund( $intCid, $intArPaymentId, $intIsRefund, $objPaymentDatabase ) {

		$strSql = 'SELECT *
					FROM charge_backs
					WHERE is_refund = ' . ( int ) $intIsRefund . ' AND
						  ar_payment_id = ' . ( int ) $intArPaymentId . ' AND
					      cid = ' . ( int ) $intCid;

		return self::fetchChargeBack( $strSql, $objPaymentDatabase );
	}

}
?>