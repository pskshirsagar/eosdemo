<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentNoteTypes
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CArPaymentNoteTypes extends CBaseArPaymentNoteTypes {

	public static function fetchArPaymentNoteTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CArPaymentNoteType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchArPaymentNoteType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CArPaymentNoteType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllArPaymentNoteTypes( $objDatabase ) {
		return self::fetchArPaymentNoteTypes( sprintf( 'SELECT * FROM %s WHERE is_published = true ORDER BY order_num', 'ar_payment_note_types' ), $objDatabase );
	}

}
?>