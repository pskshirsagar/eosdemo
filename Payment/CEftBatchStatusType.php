<?php

class CEftBatchStatusType extends CBaseEftBatchStatusType {

	const FILE_CREATION_STARTED 			= 1;
	const FILE_CREATION_COMPLETED 			= 2;
	const FILE_TRANSMISSION_STARTED 		= 3;
	const FILE_TRANSMISSION_COMPLETED 		= 4;
	const FILE_VALIDATION_VERIFIED 			= 5;
	const FAILED 							= 6;
}
?>