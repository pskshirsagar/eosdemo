<?php

class CFundMovementType extends CBaseFundMovementType {

	// These types give us instructions on how to configure the settlement for a merchant account.

	// Client intermediary movement means all funds deposit into a single bank account designated for the client (owned by the client) at the intermediary bank (Zions). (cc, ach, ch21).
	// This type of movement is no longer available.
	const CLIENT_INTERMEDIARY_MOVEMENT	= 1;

	// Bank intermediary means we push funds into bank account owned by the Bank.  At zions we have one bank account for ach, one for cc, and one for ch21.
	// This type, as of Feb 2012 is the only type we support (besides type "anticipatory" 4 found below)
	const BANK_INTERMEDIARY_MOVEMENT		= 2;

	// This is a weird type that we had for a while while we were migration from first regional bank to zions bank.
	// cc was deposited into customer specific accounts at first regional, while ach and ch21 was transferred to bank owned accounts at Zions.  This type of settlement is no longer available.
	const BANK_AND_CLIENT_INTERMEDIARY	= 3;

	// Some clients wanted us to settle funds faster, so we asked Zions if they would allow it.  They said yes, for extra pennies per transaction.
	// They asked us to push these ch21 transactions through a separate intermediary bank account so they could know to bill us more for those transactions.
	// This type is in use in Feb 2012
	const BANK_INTERMEDIARY_ANTICIPATORY	= 4;
}
?>