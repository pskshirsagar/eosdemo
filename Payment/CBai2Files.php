<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CBai2Files
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CBai2Files extends CBaseBai2Files {

	public static function fetchBai2FilesByFileName( $strFileName, $objDatabase ) {
		return self::fetchBai2Files( sprintf( 'SELECT * FROM bai2_files WHERE file_name = \'%s\'', $strFileName ), $objDatabase );
	}

}
?>