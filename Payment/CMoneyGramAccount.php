<?php

class CMoneyGramAccount extends CBaseMoneyGramAccount {

	protected $m_strAccountNumber;
	protected $m_objProperty;
	protected $m_objCustomer;

	const MONEYGRAM_AGENT_ID			= 9817;
	const MONEY_GRAM_MAX_PAYMENT_AMOUNT	= 10000;

	public function getAccountNumber() {
		return $this->m_intId;
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->m_strAccountNumber = $strAccountNumber;
	}

	public function getValidFormatOfMoneyGramAccountNumber( $strMoneyGramAccountNumber ) {
		$strMoneyGramAccountNumber = ( int ) $strMoneyGramAccountNumber;

		// making receiveAccountNumber to 8 digit minimun by adding or removing preceding zero's as ajent is not accepting account number less than 8 digits.
		if( 8 > strlen( $strMoneyGramAccountNumber ) ) {
			$strMoneyGramAccountNumber = sprintf( '%08d', $strMoneyGramAccountNumber );
		}
		return $strMoneyGramAccountNumber;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intCustomerId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', 'Customer Id is required' ) );
		}

		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intLeaseId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', 'Lease Id is required' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'generate_account':
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valLeaseId();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function getOrFetchCustomer( $objDatabase ) {

		if( true == valObj( $this->m_objCustomer, 'CCustomer' ) ) {
			return $this->m_objCustomer;
		} else {
			$this->m_objCustomer = $this->fetchCustomer( $objDatabase );
			return $this->m_objCustomer;
		}
	}

	public function getOrFetchProperty( $objDatabase ) {

		if( true == valObj( $this->m_objProperty, 'CProperty' ) ) {
			return $this->m_objProperty;
		} else {
			return $this->fetchProperty( $objDatabase );
		}
	}

	public function getOrFetchLease( $objDatabase ) {

		if( true == valObj( $this->m_objLease, 'CLease' ) ) {
			return $this->m_objLease;
		} else {
			$this->m_objLease = $this->fetchLease( $objDatabase );
			return $this->m_objLease;
		}
	}

	public function fetchProperty( $objDatabase ) {

		if( false == is_int( $this->m_intCid ) || false == is_int( $this->m_intPropertyId ) )
			return NULL;

		$this->m_objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->m_intPropertyId, $this->m_intCid, $objDatabase );

		return $this->m_objProperty;
	}

	public function fetchCustomer( $objDatabase ) {
		if( false == is_int( $this->m_intCid ) || false == is_int( $this->m_intCustomerId ) )
			return NULL;

		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->m_intCustomerId, $this->m_intCid, $objDatabase );
	}

	public function fetchLease( $objDatabase ) {
		if( false == is_int( $this->m_intCid ) || false == is_int( $this->m_intLeaseId ) )
			return NULL;

		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCidBySchema( $this->m_intLeaseId, $this->m_intCid, $objDatabase );
	}

}

?>