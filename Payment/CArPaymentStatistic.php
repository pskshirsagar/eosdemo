<?php

class CArPaymentStatistic extends CBaseArPaymentStatistic {

	protected $m_intCardCount;
	protected $m_intTotalCount;
	protected $m_intTotalTransactions;
	protected $m_fltAchPercentage;
	protected $m_fltCcPercentage;
	protected $m_fltDebitCardPercentage;
	protected $m_fltCh21Percentage;
	protected $m_fltWuPercentage;
	protected $m_fltRevenueFeeTotal;
	protected $m_fltPaymentAmountTotal;
	protected $m_fltVolumePercentIncrease;
	protected $m_fltConvenienceFeePercentIncrease;
	protected $m_fltAchRevenuePerTransaction;
	protected $m_fltCcRevenuePerTransaction;
	protected $m_fltDebitCardRevenuePerTransaction;
	protected $m_fltCardRevenuePerTransaction;
	protected $m_fltCh21RevenuePerTransaction;

    public function __construct() {
        parent::__construct();

        $this->m_intCardCount  = NULL;
        $this->m_intTotalCount = NULL;
        $this->m_intTotalTransactions				= NULL;
        $this->m_fltAchPercentage 					= NULL;
        $this->m_fltCcPercentage 					= NULL;
		$this->m_fltDebitCardPercentage 			= NULL;
		$this->m_fltCh21Percentage					= NULL;
        $this->m_fltWuPercentage					= NULL;
        $this->m_fltRevenueFeeTotal 				= NULL;
        $this->m_fltPaymentAmountTotal 				= NULL;
        $this->m_fltVolumePercentIncrease 			= NULL;
        $this->m_fltConvenienceFeePercentIncrease 	= NULL;
        $this->m_fltAchRevenuePerTransaction		= NULL;
        $this->m_fltCcRevenuePerTransaction			= NULL;
		$this->m_fltDebitCardRevenuePerTransaction	= NULL;
		$this->m_fltCardRevenuePerTransaction	= NULL;
		$this->m_fltCh21RevenuePerTransaction		= NULL;

        return;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        return $boolIsValid;
    }

    /**
     * SET FUNCTION
     */

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

    	$this->setCardCount( $this->getViCount() + $this->getMcCount() + $this->getDiCount() + $this->getAmexCount() );
    	$this->setTotalCount( $this->getAchCount() + $this->getCardCount() + $this->getCh21Count() + $this->getWuCount() );
    	$this->setTotalTransactions( $this->getAchCount() + $this->getCardCount() + $this->getCh21Count() );
    	$this->setAchPercentage( $this->getAchCount() / $this->getTotalCount() * 100 );
    	$this->setCcPercentage( $this->getCreditCardCount() / $this->getTotalCount() * 100 );
		$this->setDebitCardPercentage( $this->getDebitCardCount() / $this->getTotalCount() * 100 );
		$this->setCh21Percentage( $this->getCh21Count() / $this->getTotalCount() * 100 );
    	$this->setWuPercentage( $this->getWuCount() / $this->getTotalCount() * 100 );
		$this->setRevenueFeeTotal( $this->getConvenienceFeeTotal() + $this->getCompanyFeeTotal() );
		$this->setPaymentAmountTotal( $this->getAchTotal() + $this->getViTotal() + $this->getMcTotal() + $this->getAmexTotal() + $this->getDiTotal() + $this->getCh21Total() + $this->getWuTotal() );
		$this->setAchRevenuePerTransaction( $this->getAchCount() == 0 ? 0 : $this->getAchRevenue() / $this->getAchCount() );
		$this->setCcRevenuePerTransaction( $this->getCreditCardCount() == 0 ? 0 : $this->getCcRevenue() / $this->getCreditCardCount() );
		$this->setDebitCardRevenuePerTransaction( $this->getDebitCardCount() == 0 ? 0 : $this->getDebitCardRevenue() / $this->getDebitCardCount() );
		$this->setCardRevenuePerTransaction( $this->getCardCount() == 0 ? 0 : $this->getCardRevenue() / $this->getCardCount() );
		$this->setCh21RevenuePerTransaction( $this->getCh21Count() == 0 ? 0 : $this->getCh21Revenue() / $this->getCh21Count() );
    	return;
    }

//    public function setCardCount( $intCardCount ) {
//    	$this->m_intCardCount = $intCardCount;
//    }

    public function setTotalCount( $intTotalCount ) {
    	$this->m_intTotalCount = $intTotalCount;
    }

    public function setTotalTransactions( $intTotalTransactions ) {
    	$this->m_intTotalTransactions = $intTotalTransactions;
    }

	public function setAchPercentage( $fltAchPercentage ) {
    	$this->m_fltAchPercentage = $fltAchPercentage;
    }

	public function setCcPercentage( $fltCcPercentage ) {
    	$this->m_fltCcPercentage = $fltCcPercentage;
    }

	public function setDebitCardPercentage( $fltDebitCardPercentage ) {
		$this->m_fltDebitCardPercentage = $fltDebitCardPercentage;
	}

	public function setCh21Percentage( $fltCh21Percentage ) {
    	$this->m_fltCh21Percentage = $fltCh21Percentage;
    }

	public function setWuPercentage( $fltWuPercentage ) {
    	$this->m_fltWuPercentage = $fltWuPercentage;
    }

	public function setRevenueFeeTotal( $fltRevenueFeeTotal ) {
    	$this->m_fltRevenueFeeTotal = $fltRevenueFeeTotal;
    }

	public function setPaymentAmountTotal( $fltPaymentAmountTotal ) {
    	$this->m_fltPaymentAmountTotal = $fltPaymentAmountTotal;
    }

	public function setVolumePercentIncrease( $fltVolumePercentIncrease ) {
    	$this->m_fltVolumePercentIncrease = $fltVolumePercentIncrease;
    }

	public function setConvenienceFeePercentIncrease( $fltConvenienceFeePercentIncrease ) {
    	$this->m_fltConvenienceFeePercentIncrease = $fltConvenienceFeePercentIncrease;
    }

	public function setAchRevenuePerTransaction( $fltAchRevenuePerTransaction ) {
    	$this->m_fltAchRevenuePerTransaction = $fltAchRevenuePerTransaction;
    }

	public function setCcRevenuePerTransaction( $fltCcRevenuePerTransaction ) {
    	$this->m_fltCcRevenuePerTransaction = $fltCcRevenuePerTransaction;
    }

	public function setDebitCardRevenuePerTransaction( $fltDebitCardRevenuePerTransaction ) {
		$this->m_fltDebitCardRevenuePerTransaction = $fltDebitCardRevenuePerTransaction;
	}

	public function setCardRevenuePerTransaction( $fltCardRevenuePerTransaction ) {
		$this->m_fltCardRevenuePerTransaction = $fltCardRevenuePerTransaction;
	}

	public function setCh21RevenuePerTransaction( $fltCh21RevenuePerTransaction ) {
    	$this->m_fltCh21RevenuePerTransaction = $fltCh21RevenuePerTransaction;
    }

    /**
     * GET FUNCTION
     */

//    public function getCardCount() {
//    	return $this->m_intCardCount;
//    }

    public function getTotalCount() {
    	return $this->m_intTotalCount;
    }

    public function getTotalTransactions() {
    	return $this->m_intTotalTransactions;
    }

	public function getAchPercentage() {
    	return $this->m_fltAchPercentage;
    }

	public function getCcPercentage() {
    	return $this->m_fltCcPercentage;
    }

	public function getDebitCardPercentage() {
		return $this->m_fltDebitCardPercentage;
	}

	public function getCh21Percentage() {
    	return $this->m_fltCh21Percentage;
    }

	public function getWuPercentage() {
    	return $this->m_fltWuPercentage;
    }

	public function getRevenueFeeTotal() {
    	return $this->m_fltRevenueFeeTotal;
    }

	public function getPaymentAmountTotal() {
    	return $this->m_fltPaymentAmountTotal;
    }

	public function getVolumePercentIncrease() {
    	return $this->m_fltVolumePercentIncrease;
    }

	public function getConvenienceFeePercentIncrease() {
    	return $this->m_fltConvenienceFeePercentIncrease;
    }

    public function getAchRevenuePerTransaction() {
    	return $this->m_fltAchRevenuePerTransaction;
    }

    public function getCcRevenuePerTransaction() {
    	return $this->m_fltCcRevenuePerTransaction;
    }

	public function getDebitCardRevenuePerTransaction() {
		return $this->m_fltDebitCardRevenuePerTransaction;
	}

	public function getCardRevenuePerTransaction() {
		return $this->m_fltCardRevenuePerTransaction;
	}

	public function getCh21RevenuePerTransaction() {
    	return $this->m_fltCh21RevenuePerTransaction;
    }

    public function getWuRevenuePerTransaction() {
    	return $this->m_fltWuRevenuePerTransaction;
    }

    public function getRevenuePerTransaction() {
    	return $this->getRevenueFeeTotal() / $this->getTotalTransactions();
    }

}
?>