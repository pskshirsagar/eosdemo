<?php

class CEftChargeType extends CBaseEftChargeType {

	const CONVENIENCE_FEES = 1;
	const MANAGEMENT_FEES  = 2;
	const DONATION_FEES    = 3;
	const RETURN_CHARGE    = 4;
	const REVERSAL_CHARGE  = 5;
	const DISTRIBUTION_FEE = 6;
	const AUTH_FAILURE_FEE = 7;
	const CREDIT_CARD_CHARGE_BACK = 8;
	const DISTRIBUTION_RETURN_FEE = 9;
	const REBATE_FEES = 10;
	const GIACT_VERIFICATION_FEE = 12;
	const UNAUTHORIZED_RETURN_FEE = 13;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>