<?php

class CX937CheckDetailRecord extends CBaseX937CheckDetailRecord {

	protected $m_objX937CheckDetailAddendumARecord;
	protected $m_objX937CheckDetailAddendumBRecord;
	protected $m_objX937CheckDetailAddendumCRecord;

	protected $m_objFrontX937ViewDetailRecord;
	protected $m_objFrontX937ViewDataRecord;

	protected $m_objReverseX937ViewDetailRecord;
	protected $m_objReverseX937ViewDataRecord;

	protected $m_objX937Bundle;
	protected $m_objPaymentDatabase;

	protected $m_intIntermediaryAccountId;

	/**
	 * Create Functions
	 */

	public function createX937CheckDetailAddendumARecord() {

		$this->m_objX937CheckDetailAddendumARecord = new CX937CheckDetailAddendumARecord();
		$this->m_objX937CheckDetailAddendumARecord->setPaymentDatabase( $this->m_objPaymentDatabase );
		$this->m_objX937CheckDetailAddendumARecord->setDefaults();
		$this->m_objX937CheckDetailAddendumARecord->setX937CheckDetailRecordId( $this->getId() );
    	$this->m_objX937CheckDetailAddendumARecord->setBofdItemSequenceNumber( $this->m_intId );
        $this->m_objX937CheckDetailAddendumARecord->setBankOfFirstDepositRoutingNumber( $this->m_objX937Bundle->getHeaderDestinationRoutingNumber() );

		return $this->m_objX937CheckDetailAddendumARecord;
	}

	public function createX937CheckDetailAddendumCRecord() {

		$this->m_objX937CheckDetailAddendumCRecord = new CX937CheckDetailAddendumCRecord();
		$this->m_objX937CheckDetailAddendumCRecord->setPaymentDatabase( $this->m_objPaymentDatabase );
		$this->m_objX937CheckDetailAddendumCRecord->setDefaults();
		$this->m_objX937CheckDetailAddendumCRecord->setEndorsingBankItemSequenceNumber( $this->getId() );
		$this->m_objX937CheckDetailAddendumCRecord->setX937CheckDetailRecordId( $this->getId() );
		$this->m_objX937CheckDetailAddendumCRecord->setEndorsingBankRoutingNumber( $this->m_objX937Bundle->getHeaderDestinationRoutingNumber() );

		return $this->m_objX937CheckDetailAddendumCRecord;
	}

	public function createFrontX937ViewDetailRecord() {

		$this->m_objFrontX937ViewDetailRecord = new CX937ViewDetailRecord();
		$this->m_objFrontX937ViewDetailRecord->setPaymentDatabase( $this->m_objPaymentDatabase );
		$this->m_objFrontX937ViewDetailRecord->setX937Bundle( $this->m_objX937Bundle );
		$this->m_objFrontX937ViewDetailRecord->setX937CheckDetailRecord( $this );
		$this->m_objFrontX937ViewDetailRecord->setX937CheckDetailRecordId( $this->getId() );
		$this->m_objFrontX937ViewDetailRecord->setViewSideIndicator( '0' ); // Front
		$this->m_objFrontX937ViewDetailRecord->setDefaults();

		return $this->m_objFrontX937ViewDetailRecord;
	}

	public function createFrontX937ViewDataRecord() {

		$this->m_objFrontX937ViewDataRecord = new CX937ViewDataRecord();
		$this->m_objFrontX937ViewDataRecord->setPaymentDatabase( $this->m_objPaymentDatabase );
		$this->m_objFrontX937ViewDataRecord->setX937Bundle( $this->m_objX937Bundle );
		$this->m_objFrontX937ViewDataRecord->setX937CheckDetailRecord( $this );
		$this->m_objFrontX937ViewDataRecord->setX937CheckDetailRecordId( $this->getId() );
		$this->m_objFrontX937ViewDataRecord->setDefaults();

		return $this->m_objFrontX937ViewDataRecord;
	}

	public function createReverseX937ViewDetailRecord() {

		$this->m_objReverseX937ViewDetailRecord = new CX937ViewDetailRecord();
		$this->m_objReverseX937ViewDetailRecord->setPaymentDatabase( $this->m_objPaymentDatabase );
		$this->m_objReverseX937ViewDetailRecord->setX937Bundle( $this->m_objX937Bundle );
		$this->m_objReverseX937ViewDetailRecord->setX937CheckDetailRecord( $this );
		$this->m_objReverseX937ViewDetailRecord->setX937CheckDetailRecordId( $this->getId() );
		$this->m_objReverseX937ViewDetailRecord->setViewSideIndicator( '1' ); // Back
		$this->m_objReverseX937ViewDetailRecord->setDefaults();

		return $this->m_objReverseX937ViewDetailRecord;
	}

	public function createReverseX937ViewDataRecord() {

		$this->m_objReverseX937ViewDataRecord = new CX937ViewDataRecord();
		$this->m_objReverseX937ViewDataRecord->setPaymentDatabase( $this->m_objPaymentDatabase );
		$this->m_objReverseX937ViewDataRecord->setX937Bundle( $this->m_objX937Bundle );
		$this->m_objReverseX937ViewDataRecord->setX937CheckDetailRecord( $this );
		$this->m_objReverseX937ViewDataRecord->setX937CheckDetailRecordId( $this->getId() );
		$this->m_objReverseX937ViewDataRecord->setDefaults();

		return $this->m_objReverseX937ViewDataRecord;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrValues['intermediary_account_id'] ) ) $this->setIntermediaryAccountId( $arrValues['intermediary_account_id'] );

        return;
    }

	// The MICR line at the bottom of a check encodes three or four separate items. If the check is longer than 6.5 inches,
	// the left most field is the Auxiliary On-Us. It usually has the check number in it. If the check is 6.5 inches or less in length,
	// this field is not present.

	// The next area (to the right) is the Transit field. This identifies the bank or institution.
	// Next, (to the right), is the On-Us field, which is usually the business or person's bank account number.
	// Finally, the right most area, which appears blank when the check is printed, is the Amount field.
	// The check amount is filled in by the bank or another processing authority.

	// Here is some useful explanation of what each of these items is and means.
	// http://www.electronicpayments.org/pdfs/AuxOnUsField.pdf

	public function setDefaults() {

    	$this->setId( $this->fetchNextId( $this->m_objPaymentDatabase ) );

    	// A code used to identify this type of record. [01-02]
    	$this->setRecordType( '25' );

    	// A number used on commercial checks at the discretion of the payor bank.  The handling of dashes and spaces shall be determined between the exchange partners
    	// If the Auxiliary On-Us field exceeds 15 data characters, this field shall contain the 15 right most digits on the check. [03-17] (new description).
    	// Old Description 2003 standard = A code used on commercial checks at the discretion of the payor bank.
    	// The MICR line at the bottom of a check encodes three or four separate items. If the check is longer than 6.5 inches, the left most field is the Auxiliary On-Us.
    	// It usually has the check number in it. If the check is 6.5 inches or less in length, this field is not present.
    	// Here is a bunch of information about the auxillary on us information -- http://www.electronicpayments.org/pdfs/AuxOnUsField.pdf
    	// $this->setAuxiliaryOnUs( '               ' ); (set in controller)

    	// A code used for special purposes as authorized by the Accredited Standards Committee X9, also known as position 44.  See X9.100-160-2 for code usage. [18-18] ????????????
    	// The External processing code is position 44 or 45 of the MICR line.  This field is located immediately ot he left of the Routing Transit field.
    	// This field is also known as field 6.  Rules for formatting this field are:  "If the field is not present on the item, the field must be formatted with spaces."
    	// This should be set in the controller also.
    	// $this->setExternalProcessingCode( ' ' );

    	// A number used to identify the institution by or through which the item is payable. [19-26]
    	// $this->setPayorBankRoutingNumber( '' ); (variable) (set in controller)

    	// A number used to identify the institution by or through which the item is payable. [27-27]
    	// $this->setPayorBankRoutingNumberCheckDigit( '' ); (variable) (set in controller)

    	// On-Us field from the MICR line of the check.  On-Us data usually consists of the payor's account number, a serial number or transaction code or both.  See Annex A for formatting of this field.
    	// The handling of dashes and spaces shall be determined between the exchange partners. [28-47]
    	// The on us information includes the account number and check number.
    	// $this->setOnUs( '                    ' ); (set in controller)

		// The dollar value of the check.  Default is U.S. dollars unless otherwise agreed. [48-57]
    	// $this->setItemAmount( '' ); (set in controller)

    	// A number assigned by the institution that creates the check detail record [58-72] (this is the key field that has to be in 26, and 28 records also)
    	$this->setEceInstitutionItemSequenceNumber( $this->m_intId );

    	// A code used to indicate the type of documentation that supports the check record. I think this should be G = 'Image included, no paper provided' [73-73]
    	$this->setDocumentationTypeIndicator( 'G' );

		// A code that indicates whether the institution that creates the Check Detail Record will or will not support electronic return processing. [74-74]
    	$this->setReturnAcceptanceIndicator( '8' );

    	// A code that indicates the relationship of a missing on-us field and/or the presence of an unreadable character in the MICR line.  See Annex I for more information. See page 41 of the X937 BOOK.
    	// Possible codes are [1 = Good read, On-Us Field present] [2 = Good read and missing On-Us field] [3 = Read Error encountered, On-Us field present] [4 = Missing On-Us field and read error encountered] [75-75]
    	$this->setMicrValidIndicator( ' ' );

    	// A code that indicates whether the ECE institution indicated on the Bundle Header Record (Type 20) is the Bank of First Deposit. [76]
    	// This will be yes unless we do direct deposit to a bank other than the intermediary bank (First Regional).
    	$this->setBofdIndicator( 'Y' );

    	// The number of check detail record Addendum Records to follow this Check Detail Record. [77-78]
    	$this->setAddendumCount( '2' );

    	// An indicator to identify whether and how the MICR line was repaired, for fields other than the Payor Bank Routing Number and Amount.
    	// If we have to manually fix micr data, we will have to dynamically set this data. For now I just put "No Repair = 0" [79]
    	$this->setCorrectionIndicator( ' ' );

    	// A code used to indicate the type of archive that supports this Check Detail Record.  Access method, availability and timeframes shall be defined by clearing arrangements. [80]
    	// B = Image (which should always be the case).  We should always require an image here.
    	$this->setArchiveTypeIndicator( ' ' );
	}

    public function setPaymentDatabase( $objPaymentDatabase ) {
    	$this->m_objPaymentDatabase = $objPaymentDatabase;
    }

	public function setX937Bundle( $objX937Bundle ) {
		$this->m_objX937Bundle = $objX937Bundle;
	}

	public function setRecordType( $strRecordType ) {
	    $this->m_strRecordType = CStrings::strNachaDef( $strRecordType, 2, true, true, '0' );
	}

	public function setAuxiliaryOnUs( $strAuxiliaryOnUs ) {
	    $this->m_strAuxiliaryOnUs = CStrings::strNachaDef( $strAuxiliaryOnUs, 15, true, false, ' ', '\*-' );
	}

	public function setExternalProcessingCode( $strExternalProcessingCode ) {
	    $this->m_strExternalProcessingCode = CStrings::strNachaDef( $strExternalProcessingCode, 1, false, false, ' ', NULL, true );
	}

	public function setPayorBankRoutingNumber( $strPayorBankRoutingNumber ) {
	    $this->m_strPayorBankRoutingNumber = CStrings::strNachaDef( $strPayorBankRoutingNumber, 8, true, true, '0' );
	}

	public function setPayorBankRoutingNumberCheckDigit( $strPayorBankRoutingNumberCheckDigit ) {
	    $this->m_strPayorBankRoutingNumberCheckDigit = CStrings::strNachaDef( $strPayorBankRoutingNumberCheckDigit, 1, false, false );
	}

	public function setOnUs( $strOnUs ) {
	    $this->m_strOnUs = CStrings::strNachaDef( $strOnUs, 20, true, false, ' ', '\*\/\-' );
	}

	public function setFormattedItemAmount( $strFormattedItemAmount ) {

    	// Validate to make sure amount is in $$.cc format
    	if( false == stristr( $strFormattedItemAmount, '.' ) ) {
    		trigger_error( 'Check Detail Record Amount is not in $$.cc format.', E_USER_ERROR );
    		exit;
    	}

	    $this->m_strItemAmount = CStrings::strNachaDef( $strFormattedItemAmount, 10, true, true, '0' );
	}

	public function setEceInstitutionItemSequenceNumber( $strEceInstitutionItemSequenceNumber ) {
	    $this->m_strEceInstitutionItemSequenceNumber = CStrings::strNachaDef( $strEceInstitutionItemSequenceNumber, 15, true, true, '0' );
	}

	public function setDocumentationTypeIndicator( $strDocumentationTypeIndicator ) {
	    $this->m_strDocumentationTypeIndicator = CStrings::strNachaDef( $strDocumentationTypeIndicator, 1, false, false );
	}

	public function setReturnAcceptanceIndicator( $strReturnAcceptanceIndicator ) {
	    $this->m_strReturnAcceptanceIndicator = CStrings::strNachaDef( $strReturnAcceptanceIndicator, 1, false, false );
	}

	public function setMicrValidIndicator( $strMicrValidIndicator ) {
	    $this->m_strMicrValidIndicator = CStrings::strNachaDef( $strMicrValidIndicator, 1, true, true, ' ' );
	}

	public function setBofdIndicator( $strBofdIndicator ) {
	    $this->m_strBofdIndicator = CStrings::strNachaDef( $strBofdIndicator, 1, false, false );
	}

	public function setAddendumCount( $strAddendumCount ) {
	    $this->m_strAddendumCount = CStrings::strNachaDef( $strAddendumCount, 2, true, true, '0' );
	}

	public function setCorrectionIndicator( $strCorrectionIndicator ) {
	    $this->m_strCorrectionIndicator = CStrings::strNachaDef( $strCorrectionIndicator, 1, true, true, ' ' );
	}

	public function setArchiveTypeIndicator( $strArchiveTypeIndicator ) {
	    $this->m_strArchiveTypeIndicator = CStrings::strNachaDef( $strArchiveTypeIndicator, 1, false, false );
	}

	public function setIntermediaryAccountId( $intIntermediaryAccountId ) {
		$this->m_intIntermediaryAccountId = $intIntermediaryAccountId;
	}

	public function setX937CheckDetailAddendumARecord( $objX937CheckDetailAddendumARecord ) {
		$this->m_objX937CheckDetailAddendumARecord = $objX937CheckDetailAddendumARecord;
	}

	public function setX937CheckDetailAddendumBRecord( $objX937CheckDetailAddendumBRecord ) {
		$this->m_objX937CheckDetailAddendumBRecord = $objX937CheckDetailAddendumBRecord;
	}

	public function setX937CheckDetailAddendumCRecord( $objX937CheckDetailAddendumCRecord ) {
		$this->m_objX937CheckDetailAddendumCRecord = $objX937CheckDetailAddendumCRecord;
	}

	public function setFrontX937ViewDetailRecord( $objFrontX937ViewDetailRecord ) {
		$this->m_objFrontX937ViewDetailRecord = $objFrontX937ViewDetailRecord;
	}

	public function setFrontX937ViewDataRecord( $objFrontX937ViewDataRecord ) {
		$this->m_objFrontX937ViewDataRecord = $objFrontX937ViewDataRecord;
	}

	public function setReverseX937ViewDetailRecord( $objReverseX937ViewDetailRecord ) {
		$this->m_objReverseX937ViewDetailRecord = $objReverseX937ViewDetailRecord;
	}

	public function setReverseX937ViewDataRecord( $objReverseX937ViewDataRecord ) {
		$this->m_objReverseX937ViewDataRecord = $objReverseX937ViewDataRecord;
	}

	/**
	 * Get Functions
	 */

	public function getX937CheckDetailAddendumARecord() {
		return $this->m_objX937CheckDetailAddendumARecord;
	}

	public function getX937CheckDetailAddendumBRecord() {
		return $this->m_objX937CheckDetailAddendumBRecord;
	}

	public function getX937CheckDetailAddendumCRecord() {
		return $this->m_objX937CheckDetailAddendumCRecord;
	}

	public function getFrontX937ViewDetailRecord() {
		return $this->m_objFrontX937ViewDetailRecord;
	}

	public function getFrontX937ViewDataRecord() {
		return $this->m_objFrontX937ViewDataRecord;
	}

	public function getReverseX937ViewDetailRecord() {
		return $this->m_objReverseX937ViewDetailRecord;
	}

	public function getReverseX937ViewDataRecord() {
		return $this->m_objReverseX937ViewDataRecord;
	}

	public function getRebuiltItemAmount() {

		$strRebuiltAmount = $this->m_strItemAmount;
		while( '0' == $strRebuiltAmount[0] ) {
			$strRebuiltAmount = substr( $strRebuiltAmount, 1 );
		}

		$strRebuiltAmount = ( $strRebuiltAmount / 100 );

		return $strRebuiltAmount;
	}

	public function getIntermediaryAccountId() {
		return $this->m_intIntermediaryAccountId;
	}

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	/**
	 * Other Functions
	 */

	public function buildRecord( $boolConvertToEbcdic = false ) {

		$strRecord = '';

		$strRecord .= $this->getRecordType();
		$strRecord .= $this->getAuxiliaryOnUs();
		$strRecord .= $this->getExternalProcessingCode();
		$strRecord .= $this->getPayorBankRoutingNumber();
		$strRecord .= $this->getPayorBankRoutingNumberCheckDigit();
		$strRecord .= $this->getOnUs();
		$strRecord .= $this->getItemAmount();
		$strRecord .= $this->getEceInstitutionItemSequenceNumber();
		$strRecord .= $this->getDocumentationTypeIndicator();
		$strRecord .= $this->getReturnAcceptanceIndicator();
		$strRecord .= $this->getMicrValidIndicator();
		$strRecord .= $this->getBofdIndicator();
		$strRecord .= $this->getAddendumCount();
		$strRecord .= $this->getCorrectionIndicator();
		$strRecord .= $this->getArchiveTypeIndicator();

		if( true == $boolConvertToEbcdic ) {
			$strRecord = \Psi\Libraries\UtilStrings\CStrings::createService()->asciiToEbcdic( $strRecord );
		}

		return $strRecord;
	}
}
?>