<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentBillings
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CArPaymentBillings extends CBaseArPaymentBillings {

	public static function fetchArPaymentBillingByCidByArPaymentId( $intCid, $intArPaymentId, $objPaymentDatabase ) {
		$strSql = ' SELECT * FROM ar_payment_billings WHERE cid= ' . ( int ) $intCid . ' AND ar_payment_id=' . ( int ) $intArPaymentId . ' LIMIT 1';
		return self::fetchArPaymentBilling( $strSql, $objPaymentDatabase );
	}

	public static function fetchArPaymentBillingsByArPaymentIds( $arrintArPaymentIds, $objPaymentDatabase ) {
		if( false == valArr( $arrintArPaymentIds ) ) return NULL;
		$strSql = 'SELECT apb.id, apb.check_routing_number, apb.check_account_number_encrypted	FROM ar_payment_billings apb WHERE apb.ar_payment_id IN (' . implode( ',', $arrintArPaymentIds ) . ')';
		return fetchData( $strSql, $objPaymentDatabase );
	}

}
?>