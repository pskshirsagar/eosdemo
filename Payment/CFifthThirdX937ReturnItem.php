<?php

class CFifthThirdX937ReturnItem extends CX937ReturnItem {

	const RETURN_DATA_FIELD_PROCESSING_DATE = 1;
	const RETURN_DATA_FIELD_PAYMENT_NUMBER 	= 4;
	const RETURN_DATA_FIELD_AMOUNT 			= 5;
	const RETURN_DATA_FIELD_ROUTING_NUMBER 	= 6;
	const RETURN_DATA_FIELD_ACCOUNT_NUMBER 	= 7;
	const RETURN_DATA_FIELD_CHECK_NUMBER 	= 8;
	const RETURN_DATA_FIELD_RETURN_TYPE 	= 11;

	public function setBankReturnItemData( $arrmixReturnData, $objAdminDatabase ) {

		$objReturnType = CReturnTypes::fetchReturnTypeById( $this->convertFifthThirdReturnReason( trim( $arrmixReturnData[self::RETURN_DATA_FIELD_RETURN_TYPE] ) ), $objAdminDatabase );

		if( true == valObj( $objReturnType, 'CReturnType' ) ) {
			$this->setReturnTypeId( $objReturnType->getId() );
		}

		if( true == is_null( $this->getReturnTypeId() ) ) {
			$this->setReturnTypeId( CReturnType::GENERIC_CHECK21_RETURN_TYPE );
		}

		if( 0 < strlen( $arrmixReturnData[self::RETURN_DATA_FIELD_PROCESSING_DATE] ) ) {
			$this->setProcessingDate( trim( ( string ) $arrmixReturnData[self::RETURN_DATA_FIELD_PROCESSING_DATE] ) );
		}

		if( 0 < strlen( $arrmixReturnData[self::RETURN_DATA_FIELD_PAYMENT_NUMBER] ) ) {
			$this->setPaymentNumber( trim( ( string ) $arrmixReturnData[self::RETURN_DATA_FIELD_PAYMENT_NUMBER] ) );
		}

		if( 0 < strlen( $arrmixReturnData[self::RETURN_DATA_FIELD_AMOUNT] ) ) {
			$this->setAmount( ( float ) trim( $arrmixReturnData[self::RETURN_DATA_FIELD_AMOUNT] ) / 100 );
		}

		if( 0 < strlen( $arrmixReturnData[self::RETURN_DATA_FIELD_ROUTING_NUMBER] ) ) {
			$this->setCheckRoutingNumber( trim( ( string ) $arrmixReturnData[self::RETURN_DATA_FIELD_ROUTING_NUMBER] ) );
		}

		if( 0 < strlen( $arrmixReturnData[self::RETURN_DATA_FIELD_ACCOUNT_NUMBER] ) ) {
			$this->setCheckAccountNumber( trim( ( string ) $arrmixReturnData[self::RETURN_DATA_FIELD_ACCOUNT_NUMBER] ) );
		}

		if( 0 < strlen( $arrmixReturnData[self::RETURN_DATA_FIELD_CHECK_NUMBER] ) ) {
			$this->setCheckNumber( trim( ( string ) $arrmixReturnData[self::RETURN_DATA_FIELD_CHECK_NUMBER] ) );
		}
	}

	private function convertFifthThirdReturnReason( $strReturnReason ) {

		$intReturnCodeId = NULL;

		switch( $strReturnReason ) {

			case 'ACCT CLOSED':
				$intReturnCodeId = CReturnType::CHECK21_CLOSED_ACCOUNT;
				break;

			case 'COUNTERFEIT':
			case 'FORGERY':
			case 'FRAUD':
				$intReturnCodeId = CReturnType::CHECK21_ALERTED_FICTICIOUS_ITEM;
				break;

			case 'ENDORSE MISSING':
				$intReturnCodeId = CReturnType::CHECK21_ENDORSEMENT_MISSING;
				break;

			case 'ENDORSE IRREGULAR':
				$intReturnCodeId = CReturnType::CHECK21_ENDORSEMENT_IRREGULAR;
				break;

			case 'LOST/STOLEN':
				$intReturnCodeId = CReturnType::CHECK21_NOT_AUTHORIZED;
				break;

			case 'NO ACCOUNT':
			case 'UNABLE TO LOCATE ACCT':
				$intReturnCodeId = CReturnType::CHECK21_UNABLE_TO_LOCATE_ACCOUNT;
				break;

			case 'NSF 1ST':
			case 'NSF 2ND':
				$intReturnCodeId = CReturnType::CHECK21_INSUFFICIENT_FUNDS;
				break;

			case 'REFER MAKER':
				$intReturnCodeId = CReturnType::REFER_TO_MAKER;
				break;

			case 'SIGNATURE':
				$intReturnCodeId = CReturnType::CHECK21_SIGNATURE_MISSING;
				break;

			case 'STALE DATE':
				$intReturnCodeId = CReturnType::CHECK21_STALE_DATED;
				break;

			case 'STOP PAY':
				$intReturnCodeId = CReturnType::CHECK21_STOP_PAYMENT;
				break;

			case 'UNCOLLECT 1ST':
			case 'UNCOLLECT 2ND':
				$intReturnCodeId = CReturnType::CHECK21_UNCOLLECTED_FUNDS_HOLD;
				break;

			case 'GUAR ENDORSE':
			case 'ENDORSE':
			case 'BREACH OF WARRA':
			case 'SEE CHECK 1ST':
			case 'SEE CHECK 2ND':
			default:
				$intReturnCodeId = CReturnType::GENERIC_CHECK21_RETURN_TYPE;
				break;
		}

		return $intReturnCodeId;
	}

}

?>