<?php

class CEftBatch extends CBaseEftBatch {

	protected $m_arrobjBatchedArPayments;
	protected $m_arrobjBatchedCompanyPayments;
	protected $m_arrobjEftBatchTransactions;

    public function __construct() {
        parent::__construct();

        $this->m_fltBatchAmount = 0;

        $this->m_arrobjBatchedArPayments 					= array();
		$this->m_arrobjBatchedCompanyPayments 				= array();
		$this->m_arrobjEftBatchTransactions					= array();

        return;
    }

	/**
	 * Create Functions
	 */

	public function createBatchedArPayment( $objArPayment ) {

		$objBatchedArPayment = new CBatchedArPayment();
		$objBatchedArPayment->setEftBatchId( $this->getId() );
		$objBatchedArPayment->setCid( $objArPayment->getCid() );
		$objBatchedArPayment->setArPaymentId( $objArPayment->getId() );
		$objBatchedArPayment->setAmount( round( $objArPayment->getTotalAmount(), 2 ) );

		return $objBatchedArPayment;
	}

	public function createBatchedCompanyPayment( $objCompanyPayment ) {

		$objBatchedCompanyPayment = new CBatchedCompanyPayment();
		$objBatchedCompanyPayment->setEftBatchId( $this->getId() );
		$objBatchedCompanyPayment->setCid( $objCompanyPayment->getCid() );
		$objBatchedCompanyPayment->setCompanyPaymentId( $objCompanyPayment->getId() );
		$objBatchedCompanyPayment->setAmount( round( $objCompanyPayment->getPaymentAmount(), 2 ) );

		return $objBatchedCompanyPayment;
	}

	public function createEftBatchTransaction() {

		$objEftBatchTransaction = new CEftBatchTransaction();
		$objEftBatchTransaction->setEftBatchId( $this->getId() );
		$objEftBatchTransaction->setTransactionDatetime( $this->getBatchDateTime() );

		return $objEftBatchTransaction;
	}

	public function createReconciliationEntry( $objPaymentDatabase ) {

		$objReconciliationEntry = new CReconciliationEntry();
		$objReconciliationEntry->setProcessingBankAccountId( $this->getProcessingBankAccountId() );
		$objReconciliationEntry->setEftBatchId( $this->getId() );
		$objReconciliationEntry->setTotal( round( $this->getBatchAmount(), 2 ) );
		$objReconciliationEntry->setMemo( $this->getBatchMemo() );

		$intReconciliationEntryTypeId = NULL;

		switch( $this->getEftBatchTypeId() ) {
			case CEftBatchType::COMPANY_PAYMENT_CREDIT_CARDS_VI_MC:
				$intReconciliationEntryTypeId = CReconciliationEntryType::COMPANY_PAYMENT_CREDIT_CARDS_VI_MC;
			    break;

			case CEftBatchType::COMPANY_PAYMENT_CREDIT_CARDS_DISCOVER:
				$intReconciliationEntryTypeId = CReconciliationEntryType::COMPANY_PAYMENT_CREDIT_CARDS_DISCOVER;
			    break;

			case CEftBatchType::COMPANY_PAYMENT_CREDIT_CARDS_AMEX:
				$intReconciliationEntryTypeId = CReconciliationEntryType::COMPANY_PAYMENT_CREDIT_CARDS_AMEX;
			    break;

			case CEftBatchType::AR_PAYMENT_CREDIT_CARDS_VI_MC:
				$intReconciliationEntryTypeId = CReconciliationEntryType::AR_PAYMENT_CREDIT_CARDS_VI_MC;
			    break;

			case CEftBatchType::AR_PAYMENT_CREDIT_CARDS_DISCOVER:
				$intReconciliationEntryTypeId = CReconciliationEntryType::AR_PAYMENT_CREDIT_CARDS_DISCOVER;
			    break;

			case CEftBatchType::AR_PAYMENT_CREDIT_CARDS_AMEX:
				$intReconciliationEntryTypeId = CReconciliationEntryType::AR_PAYMENT_CREDIT_CARDS_AMEX;
			    break;

			case CEftBatchType::AR_PAYMENT_ACH_OLD:
				$intReconciliationEntryTypeId = CReconciliationEntryType::ECHO_ACH_DEPOSIT;
			    break;

			case CEftBatchType::AR_PAYMENT_ACH_RETURN_OLD:
				$intReconciliationEntryTypeId = CReconciliationEntryType::ECHO_ACH_RETURN;
				$objReconciliationEntry->setTotal( round( ( -1 * abs( $this->getBatchAmount() ) ), 2 ) );
			    break;

			/**
			 * New EFT batch types for credit card reconciliation
			 */

			case CEftBatchType::AR_PAYMENT_SETTLEMENT_VI_MC_D:
				$intReconciliationEntryTypeId = CReconciliationEntryType::AR_PAYMENT_CREDIT_CARDS_VI_MC;
			    break;

			// For now we don't generate rec entries from eft ar payment chargebacks. These are generated by the chargeback process in the app.
			case CEftBatchType::AR_PAYMENT_CHARGEBACK_VI_MC_D:
				$intReconciliationEntryTypeId = CReconciliationEntryType::AR_PAYMENTS_CHARGE_BACKS;
			    break;

			case CEftBatchType::AR_PAYMENT_REPRESENTMENT_VI_MC_D:
				$intReconciliationEntryTypeId = CReconciliationEntryType::AR_PAYMENT_CREDIT_CARDS_VI_MC;
			    break;

			case CEftBatchType::COMPANY_PAYMENT_SETTLEMENT_VI_MC_D:
				$intReconciliationEntryTypeId = CReconciliationEntryType::COMPANY_PAYMENT_CREDIT_CARDS_VI_MC;
			    break;

			case CEftBatchType::COMPANY_PAYMENT_CHARGEBACK_VI_MC_D:
				$intReconciliationEntryTypeId = CReconciliationEntryType::COMPANY_PAYMENTS_CHARGE_BACKS;
			    break;

			case CEftBatchType::COMPANY_PAYMENT_REPRESENTMENT_VI_MC_D:
				$intReconciliationEntryTypeId = CReconciliationEntryType::COMPANY_PAYMENT_CREDIT_CARDS_VI_MC;
			    break;

			case CEftBatchType::UNBALANCED_NACHA_FILE_BANK_BALANCING_ENTRY:
				$intReconciliationEntryTypeId = CReconciliationEntryType::UNBALANCED_NACHA_FILE_BANK_BALANCING_ENTRY;
				break;

			case CEftBatchType::CREDITCARD_PAYMENT_BATCH:
			case CEftBatchType::CREDITCARD_CHARGEBACK_ADJUSTMENT_BATCH:
				$intReconciliationEntryTypeId = CReconciliationEntryType::CREDIT_CARD;
				break;

			case CEftBatchType::MONEY_GRAM:
				$intReconciliationEntryTypeId = CReconciliationEntryType::MONEY_GRAM;
				break;

			case CEftBatchType::AR_PAYMENT_PAD:
				$intReconciliationEntryTypeId = CReconciliationEntryType::AR_PAYMENTS;
				break;

			default:
				$intReconciliationEntryTypeId = NULL; // this should trigger error.
		}

		$objReconciliationEntry->setReconciliationEntryTypeId( $intReconciliationEntryTypeId );
		$objReconciliationEntry->setMovementDatetime( $this->getSettlementDate() );

		return $objReconciliationEntry;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchBatchedArPayments( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CBatchedArPayments::createService()->fetchBatchedArPaymentsByEftBatchId( $this->m_intId, $objPaymentDatabase );
	}

	public function fetchBatchedCompanyPayments( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CBatchedCompanyPayments::createService()->fetchBatchedCompanyPaymentsByEftBatchId( $this->m_intId, $objPaymentDatabase );
	}

	/**
	 * Get Functions
	 */

	public function getBatchedArPayments() {
		return $this->m_arrobjBatchedArPayments;
	}

	public function getBatchedCompanyPayments() {
		return $this->m_arrobjBatchedCompanyPayments;
	}

	public function getEftBatchTransactions() {
		return $this->m_arrobjEftBatchTransactions;
	}

	/**
	 * Set Functions
	 */

    public function setDefaults() {

    	$this->setEftBatchStatusTypeId( CEftBatchStatusType::FILE_TRANSMISSION_COMPLETED );
    	$this->setConfirmedOn( NULL );
		$this->setIsReconPrepped( 0 );
    }

	/**
	 * Add Functions
	 */

	public function addBatchedArPayment( $objBatchedArPayment ) {
		// NOTE: We don't increment batch total.  It is assumed that we'll always add an accompanying EFT batch transaction (where we'll increment batch total amount)
		// $this->m_fltBatchAmount += bcadd( $this->m_fltBatchAmount, $objBatchedArPayment->getAmount(), 2 );

		$objBatchedArPayment->setPosition( \Psi\Libraries\UtilFunctions\count( $this->m_arrobjBatchedArPayments ) + 1 );

		if( true == is_null( $objBatchedArPayment->getId() ) || 0 >= $objBatchedArPayment->getId() ) {
			$this->m_arrobjBatchedArPayments[$objBatchedArPayment->getPosition()] = $objBatchedArPayment;
		} else {
			$this->m_arrobjBatchedArPayments[$objBatchedArPayment->getId()] = $objBatchedArPayment;
		}
	}

	public function addBatchedCompanyPayment( $objBatchedCompanyPayment ) {
		// NOTE: We don't increment batch total.  It is assumed that we'll always add an accompanying EFT batch transaction (where we'll increment batch total amount)
		// $this->m_fltBatchAmount = bcadd( $this->m_fltBatchAmount, $objBatchedCompanyPayment->getAmount(), 2 );

		$objBatchedCompanyPayment->setPosition( \Psi\Libraries\UtilFunctions\count( $this->m_arrobjBatchedCompanyPayments ) );

		if( true == is_null( $objBatchedCompanyPayment->getId() ) || 0 >= $objBatchedCompanyPayment->getId() ) {
			$this->m_arrobjBatchedCompanyPayments[$objBatchedCompanyPayment->getPosition()] = $objBatchedCompanyPayment;
		} else {
			$this->m_arrobjBatchedCompanyPayments[$objBatchedCompanyPayment->getId()] = $objBatchedCompanyPayment;
		}
	}

	public function addEftBatchTransaction( $objEftBatchTransaction ) {
		$this->m_fltBatchAmount = bcadd( $this->m_fltBatchAmount, $objEftBatchTransaction->getTransactionAmount(), 2 );

		if( true == is_null( $objEftBatchTransaction->getId() ) || 0 >= $objEftBatchTransaction->getId() ) {
			$this->m_arrobjEftBatchTransactions[] = $objEftBatchTransaction;
		} else {
			$this->m_arrobjEftBatchTransactions[$objEftBatchTransaction->getId()] = $objEftBatchTransaction;
		}
	}

	/**
	 * Remove Functions
	 */

	// function removeBatchedArPayment( $objBatchedArPayment ) {
		// if( true == is_null( $objBatchedArPayment->getId() || 0 >= $objBatchedArPayment->getId() ) ) {
		//	unset( $this->m_arrobjBatchedArPayments[$objBatchedArPayment->getPosition()] );
		// } else {
		//	unset( $this->m_arrobjBatchedArPayments[$objBatchedArPayment->getId()] );
		// }
	// }

	// function removeBatchedCompanyPayment( $objBatchedCompanyPayment ) {
		// if( true == is_null( $objBatchedCompanyPayment->getId() || 0 >= $objBatchedCompanyPayment->getId() ) ) {
		//	unset( $this->m_arrobjBatchedCompanyPayments[$objBatchedCompanyPayment->getPosition()] );
	//	} else {
		//	unset( $this->m_arrobjBatchedCompanyPayments[$objBatchedCompanyPayment->getId()] );
	//	}
	// }

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>