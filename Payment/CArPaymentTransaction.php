<?php

class CArPaymentTransaction extends CBaseArPaymentTransaction {

	protected $m_strUsername;
	protected $m_strNameFull;

	/**
	 * Get Functions
	 */

    public function getNameFull() {
    	return $this->m_strNameFull;
    }

	public function getUsername() {
    	return $this->m_strUsername;
    }

	/**
	 * Set Functions
	 */

	public function setNameFull( $strNameFull ) {
    	$this->m_strNameFull = CStrings::strTrimDef( $strNameFull, 240, NULL, true, false, true );
    }

	public function setUsername( $strUsername ) {
        $this->m_strUsername = CStrings::strTrimDef( $strUsername, 240, NULL, true, false, true );
    }

	public function setTransactionMemo( $strTransactionMemo ) {
        $this->m_strTransactionMemo = CStrings::strTrimDef( $strTransactionMemo, 2000, NULL, true, false, true );
    }

    public function setGatewayResponseText( $strGatewayResponseText ) {
        $this->m_strGatewayResponseText = CStrings::strTrimDef( $strGatewayResponseText, 240, NULL, true, false, true );
    }

    public function setGatewayResponseReasonText( $strGatewayResponseReasonText ) {
        $this->m_strGatewayResponseReasonText = CStrings::strTrimDef( $strGatewayResponseReasonText, 240, NULL, true, false, true );
    }

    public function setGatewayAvsResultText( $strGatewayAvsResultText ) {
        $this->m_strGatewayAvsResultText = CStrings::strTrimDef( $strGatewayAvsResultText, 240, NULL, true, false, true );
    }

    public function setGatewayRawData( $strGatewayRawData ) {
        $this->m_strGatewayRawData = CStrings::strTrimDef( $strGatewayRawData, -1, NULL, true, false, true );
    }

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intId ) ) {
            $boolIsValid = false;
            trigger_error( 'Invalid Ar Payment Transaction Request:  Id required - CArPaymentTransaction::valId()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCid ) || 0 >= ( int ) $this->m_intCid ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Ar Payment Transaction Request:  Management Id required - CArPaymentTransaction::valCid()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

    public function valCompanyMerchantAccountId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCompanyMerchantAccountId ) || 0 >= ( int ) $this->m_intCompanyMerchantAccountId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Ar Payment Transaction Request:  Company Merchant Account Id required - CArPaymentTransaction::valCompanyMerchantAccountId()', E_USER_ERROR );
        }

        return $boolIsValid;
    }

    public function valArPaymentId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intArPaymentId ) || 0 >= ( int ) $this->m_intArPaymentId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Ar Payment Transaction Request:  Ar Payment Id required - CArPaymentTransaction::valArPaymentId()', E_USER_ERROR );
        }

        return $boolIsValid;
    }

    public function valPaymentTransactionTypeId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intPaymentTransactionTypeId ) || 0 >= ( int ) $this->m_intPaymentTransactionTypeId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Ar Payment Transaction Request:  Payment Transaction Type ID required - CArPaymentTransaction::valPaymentTransactionTypeId()', E_USER_ERROR );
        }

        return $boolIsValid;
    }

    public function valTransactionNumber() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_intTransactionNumber ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_number', '' ) );
        // }

        return $boolIsValid;
    }

    public function valTransactionDatetime() {
        $boolIsValid = true;

        if( false == isset( $this->m_strTransactionDatetime ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_datetime', 'Transaction Date is required' ) );
        }

        return $boolIsValid;
    }

    public function valTransactionAmount() {
        $boolIsValid = true;

        if( false == isset( $this->m_fltTransactionAmount ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', 'Transaction Amount is required' ) );
        }

        return $boolIsValid;
    }

    public function valTransactionMemo() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strTransactionMemo ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_memo', '' ) );
        // }

        return $boolIsValid;
    }

    public function valGatewayResponseCode() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valGatewayResponseText() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valGatewayResponseReasonCode() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strGatewayResponseReasonCode ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_response_reason_code', '' ) );
        // }

        return $boolIsValid;
    }

    public function valGatewayResponseReasonText() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strGatewayResponseReasonText ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_response_reason_text', '' ) );
        // }

        return $boolIsValid;
    }

    public function valGatewayTransactionNumber() {
        $boolIsValid = true;

        if( false == isset( $this->m_strGatewayTransactionNumber ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_transaction_number', 'Gateway transaction number is required.' ) );
        }

        return $boolIsValid;
    }

    public function valGatewayApprovalCode() {
        $boolIsValid = true;

        if( false == isset( $this->m_strGatewayApprovalCode ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_approval_code', 'Gateway approval code is required.' ) );
        }

        return $boolIsValid;
    }

    public function valGatewayAvsResultCode() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strGatewayAvsResultCode ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_avs_result_code', '' ) );
        // }

        return $boolIsValid;
    }

    public function valGatewayAvsResultText() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strGatewayAvsResultText ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_avs_result_text', '' ) );
        // }

        return $boolIsValid;
    }

    public function valGatewayRawData() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strGatewayRawData ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_raw_data', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );

        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCompanyMerchantAccountId();
				$boolIsValid &= $this->valArPaymentId();
				$boolIsValid &= $this->valPaymentTransactionTypeId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valTransactionAmount();
				$boolIsValid &= $this->valGatewayResponseCode();
				$boolIsValid &= $this->valGatewayResponseText();
				break;

            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCompanyMerchantAccountId();
				$boolIsValid &= $this->valArPaymentId();
				$boolIsValid &= $this->valPaymentTransactionTypeId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valTransactionAmount();
				$boolIsValid &= $this->valGatewayResponseCode();
				$boolIsValid &= $this->valGatewayResponseText();
				break;

            case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
            	break;

            case 'validate_update_status':
				$boolIsValid &= $this->valGatewayTransactionNumber();
				$boolIsValid &= $this->valGatewayApprovalCode();
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>