<?php

class CX937Bundle extends CBaseX937Bundle {

	protected $m_arrobjX937CheckDetailRecords;
	protected $m_objPaymentDatabase;

    public function __construct() {
        parent::__construct();

        $this->m_arrobjX937CheckDetailRecords = array();
        $this->m_intBundleHeaderSequenceNumberPosition = 1;

        return;
    }

	/**
	 * Add Functions
	 */

	public function addX937CheckDetailRecord( $objX937CheckDetailRecord ) {
		$this->m_arrobjX937CheckDetailRecords[$objX937CheckDetailRecord->getId()] = $objX937CheckDetailRecord;
	}

	/**
	 * Create Functions
	 */

	public function createX937CheckDetailRecord() {

		$objX937CheckDetailRecord = new CX937CheckDetailRecord();
		$objX937CheckDetailRecord->setPaymentDatabase( $this->m_objPaymentDatabase );
		$objX937CheckDetailRecord->setDefaults();

		$objX937CheckDetailRecord->setX937FileId( $this->m_intX937FileId );
		$objX937CheckDetailRecord->setX937CashLetterId( $this->m_intX937CashLetterId );
		$objX937CheckDetailRecord->setX937BundleId( $this->m_intId );
		$objX937CheckDetailRecord->setX937Bundle( $this );

		// I don't think I want this here any more.
    	// A number assigned by the institution that creates the check detail record [58-72]
    	// $objX937CheckDetailRecord->setEceInstitutionItemSequenceNumber( $this->m_intCheckDetailRecordSequenceNumberPosition );
    	// $this->m_intCheckDetailRecordSequenceNumberPosition++;

		$this->m_arrobjX937CheckDetailRecords[] = $objX937CheckDetailRecord;

		return $objX937CheckDetailRecord;
	}

	/**
	 * Set Functions
	 */

	public function setDefaults() {

    	$this->setId( $this->fetchNextId( $this->m_objPaymentDatabase ) );

    	// A code used to identify this type of record. [01-02]
    	$this->setHeaderRecordType( '20' );

    	// A code used to identify the type of bundle. [03-04] (Set in CX937CashLetter)
    	// $this->setHeaderCollectionTypeIndicator();

    	// A number used to identify the institution that receives and processes the cash letter or the bundle. [05-13] (Set in CX937CashLetter)
    	// $this->setHeaderDestinationRoutingNumber();

    	// A number used to identify the institution that creates the bundle header record. [14-22] (Set in CX937CashLetter)
    	// $this->setHeaderEceInstitutionRoutingNumber();

    	// The year, month, and day that designates the business date of the bundle.  The date typically assigned to research this bundle. [23-30] (Set in CX937CashLetter)
    	// $this->setHeaderBusinessDate();

    	// The year, month, and day that designates the date the bundle is created.  The default should be based on GMT. [31-38] (Set in CX937CashLetter)
    	// $this->setHeaderCreationDate();

    	// A number used to identify the bundle, assigned by the institution that creates the bundle.  This number shall be unique within the Cash Letter. [39-48]
    	// Does this have to start at 0 for each cash letter, or can it increment?
    	$this->setHeaderId( $this->m_intId );

    	// A number assigned by the institution that creates the bundle.  Usually denotes the relative position of the bundle within the cash letter. [49-52] (Set in CX937CashLetter)
    	// $this->setHeaderSequenceNumber();

    	// A code assigned by the institution that creates the bundle.  Denotes the cycle under which the bundle is created. [53-54]
    	// $this->setHeaderCycleNumber( '  ' );  // We should set this as a sequence number

    	// A Routing number specified by the Institution that creates the bundle, indicating the location to which returns, final return notifications and preliminary return notifications shall be sent.
    	// This should be First Regional Bank Now (no longer Huntington Bank) I think. [55-63] (122242597 is the routing number of First Regional Bank (AEC actually processes the returns for First Regional--not First Regional) that will be handling returns for us)
    	// I talked to Mark at the Federal Reserve and this field should be blank.
    	$this->setHeaderReturnLocationRoutingNumber( '         ' );

    	// A field used at the discretion of users of the standard. [64-68]
    	$this->setHeaderUserField( '     ' );

    	// A field reserved for future use by the Accredited Standards Committee X9. [69-80]
    	$this->setHeaderReserved( '            ' );

    	// A code used to identify the type of record. [01-02]
    	$this->setControlRecordType( '70' );

    	// The total number of items sent within a bundle, all Check Detail Records (Type 25) or all Return Records (Type 31) [03-06]
    	// $this->setControlItemsCount(); (set in CX937File.class.php)

    	// The amount sum value of the items within the bundle. [07-18]
    	// $this->setControlTotalAmount(); (set in CX937File.class.php)

    	// The amount sum value of all Check Detail Records (Type 25) that contain the Defined Value '1' in the MICR Valid Indicator (Field 10) [19-30]
    	// $this->setControlMicrValidTotalAmount(); (set in CX937File.class.php)

    	// The total number of image view record pairs within a bundle regardless of whether image data is actually present. Each image view is represented
    	// by an Image View Detail Record (Type 50) and an Image View Data Record (Type 52) pair. [31-35]
    	// $this->setControlImagesCount(); (set in CX937File.class.php)

		// A field used at the discretion of users of the standard. [36-55]
    	$this->setControlUserField( '                    ' );

    	// A field reserved for future use by the Accredited Standards Committee X9. [56-80]
    	$this->setControlReserved( '                         ' );
	}

    public function setPaymentDatabase( $objPaymentDatabase ) {
    	$this->m_objPaymentDatabase = $objPaymentDatabase;
    }

	public function setHeaderRecordType( $strHeaderRecordType ) {
	    $this->m_strHeaderRecordType = CStrings::strNachaDef( $strHeaderRecordType, 2, true, true, '0' );
	}

	public function setHeaderCollectionTypeIndicator( $strHeaderCollectionTypeIndicator ) {
	    $this->m_strHeaderCollectionTypeIndicator = CStrings::strNachaDef( $strHeaderCollectionTypeIndicator, 2, true, true, '0' );
	}

	public function setHeaderDestinationRoutingNumber( $strHeaderDestinationRoutingNumber ) {
	    $this->m_strHeaderDestinationRoutingNumber = CStrings::strNachaDef( $strHeaderDestinationRoutingNumber, 9, true, true, '0' );
	}

	public function setHeaderEceInstitutionRoutingNumber( $strHeaderEceInstitutionRoutingNumber ) {
	    $this->m_strHeaderEceInstitutionRoutingNumber = CStrings::strNachaDef( $strHeaderEceInstitutionRoutingNumber, 9, true, true, '0' );
	}

	public function setHeaderBusinessDate( $strHeaderBusinessDate ) {
	    $this->m_strHeaderBusinessDate = CStrings::strNachaDef( $strHeaderBusinessDate, 8, true, true, '0' );
	}

	public function setHeaderCreationDate( $strHeaderCreationDate ) {
	    $this->m_strHeaderCreationDate = CStrings::strNachaDef( $strHeaderCreationDate, 8, true, true, '0' );
	}

	public function setHeaderId( $strHeaderId ) {
	    $this->m_strHeaderId = CStrings::strNachaDef( $strHeaderId, 10, false, false );
	}

	public function setHeaderSequenceNumber( $strHeaderSequenceNumber ) {
	    $this->m_strHeaderSequenceNumber = CStrings::strNachaDef( $strHeaderSequenceNumber, 4, false, false );
	}

	public function setHeaderCycleNumber( $strHeaderCycleNumber = NULL ) {
	    $this->m_strHeaderCycleNumber = CStrings::strNachaDef( $strHeaderCycleNumber, 2, false, false );
	}

	public function setHeaderReturnLocationRoutingNumber( $strHeaderReturnLocationRoutingNumber ) {
	    $this->m_strHeaderReturnLocationRoutingNumber = CStrings::strNachaDef( $strHeaderReturnLocationRoutingNumber, 9, true, true, ' ' );
	}

	public function setHeaderUserField( $strHeaderUserField ) {
	    $this->m_strHeaderUserField = CStrings::strNachaDef( $strHeaderUserField, 5, false, false );
	}

	public function setHeaderReserved( $strHeaderReserved ) {
	    $this->m_strHeaderReserved = CStrings::strNachaDef( $strHeaderReserved, 12, false, false );
	}

	public function setControlRecordType( $strControlRecordType ) {
	    $this->m_strControlRecordType = CStrings::strNachaDef( $strControlRecordType, 2, true, true, '0' );
	}

	public function setControlItemsCount( $strControlItemsCount ) {
	    $this->m_strControlItemsCount = CStrings::strNachaDef( $strControlItemsCount, 4, true, true, '0' );
	}

	public function setFormattedControlTotalAmount( $strFormattedControlTotalAmount ) {

    	// Validate to make sure amount is in $$.cc format
    	if( false == stristr( $strFormattedControlTotalAmount, '.' ) ) {
    		trigger_error( 'Total Bundle Control Total Amount is not in $$.cc format.', E_USER_ERROR );
    		exit;
    	}

	    $this->m_strControlTotalAmount = CStrings::strNachaDef( $strFormattedControlTotalAmount, 12, true, true, '0' );
	}

	public function setFormattedControlMicrValidTotalAmount( $strFormattedControlMicrValidTotalAmount ) {

    	// Validate to make sure amount is in $$.cc format
    	if( false == stristr( $strFormattedControlMicrValidTotalAmount, '.' ) ) {
    		trigger_error( 'Total Bundle Control MICR Total Amount is not in $$.cc format.', E_USER_ERROR );
    		exit;
    	}

	    $this->m_strControlMicrValidTotalAmount = CStrings::strNachaDef( $strFormattedControlMicrValidTotalAmount, 12, true, true, '0' );
	}

	public function setControlImagesCount( $strControlImagesCount ) {
	    $this->m_strControlImagesCount = CStrings::strNachaDef( $strControlImagesCount, 5, true, true, '0' );
	}

	public function setControlUserField( $strControlUserField ) {
	    $this->m_strControlUserField = CStrings::strNachaDef( $strControlUserField, 20, false, false );
	}

	public function setControlReserved( $strControlReserved ) {
	    $this->m_strControlReserved = CStrings::strNachaDef( $strControlReserved, 25, false, false );
	}

	/**
	 * Get Functions
	 */

	public function getX937CheckDetailRecords() {
		return $this->m_arrobjX937CheckDetailRecords;
	}

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	/**
	 * Other Functions
	 */

	public function buildHeaderRecord( $boolConvertToEbcdic = false ) {

		$strRecord = '';

		$strRecord .= $this->getHeaderRecordType();
		$strRecord .= $this->getHeaderCollectionTypeIndicator();
		$strRecord .= $this->getHeaderDestinationRoutingNumber();
		$strRecord .= $this->getHeaderEceInstitutionRoutingNumber();
		$strRecord .= $this->getHeaderBusinessDate();
		$strRecord .= $this->getHeaderCreationDate();
		$strRecord .= $this->getHeaderId();
		$strRecord .= $this->getHeaderSequenceNumber();
		$strRecord .= $this->getHeaderCycleNumber();
		$strRecord .= $this->getHeaderReturnLocationRoutingNumber();
		$strRecord .= $this->getHeaderUserField();
		$strRecord .= $this->getHeaderReserved();

		if( true == $boolConvertToEbcdic ) {
			$strRecord = \Psi\Libraries\UtilStrings\CStrings::createService()->asciiToEbcdic( $strRecord );
		}

		return $strRecord;
	}

	public function buildControlRecord( $boolConvertToEbcdic = false ) {

		$strRecord = '';

		$strRecord .= $this->getControlRecordType();
		$strRecord .= $this->getControlItemsCount();
		$strRecord .= $this->getControlTotalAmount();
		$strRecord .= $this->getControlMicrValidTotalAmount();
		$strRecord .= $this->getControlImagesCount();
		$strRecord .= $this->getControlUserField();
		$strRecord .= $this->getControlReserved();

		if( true == $boolConvertToEbcdic ) {
			$strRecord = \Psi\Libraries\UtilStrings\CStrings::createService()->asciiToEbcdic( $strRecord );
		}

		return $strRecord;
	}
}
?>