<?php

class CX937ReturnImage extends CBaseX937ReturnImage {

	public function getImagePath() {
		return PATH_MOUNTS . $this->m_strImagePath;
	}

    public function valX937ReturnFileId() {
        $boolIsValid = true;

        if( true == is_null( $this->getX937ReturnFileId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'x937_return_file_id', 'X937 return file id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valX937ReturnItemId() {
        $boolIsValid = true;

        if( true == is_null( $this->getX937ReturnItemId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'x937_return_item_id', 'X937 return item id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPaymentImageTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPaymentImageTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_image_type_id', 'X937 image type id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valImagePath() {
        $boolIsValid = true;

        if( true == is_null( $this->getImagePath() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'image_path', 'X937 image path is required.' ) );
        }

        return $boolIsValid;
    }

    public function valImageName() {
        $boolIsValid = true;

        if( true == is_null( $this->getImageName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'image_name', 'X937 image name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getOrderNum() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valX937ReturnFileId();
            	$boolIsValid &= $this->valX937ReturnItemId();
            	$boolIsValid &= $this->valPaymentImageTypeId();
            	$boolIsValid &= $this->valImagePath();
            	$boolIsValid &= $this->valImageName();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Other Functions
     */

    public function convertImageToGif() {

		$strTifFile = $this->getImagePath() . $this->getImageName();
		$strGifFile = $this->getImagePath() . basename( $this->getImageName(), '.TIF' ) . '.gif';

		$strTerminalOutput	= NULL;

		exec( 'convert ' . $strTifFile . $strGifFile, $strTerminalOutput );

		// Get the contents of the newly created gif file.
		$strImageContent = file_get_contents( $strGifFile );

		// Delete image off files system.
		unlink( $strGifFile );

		return $strImageContent;
	}

}
?>