<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaFileTypes
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CNachaFileTypes extends CBaseNachaFileTypes {

	public static function fetchNachaFileTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CNachaFileType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchNachaFileType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CNachaFileType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

    public static function fetchAllNachaFileTypes( $objPaymentDatabase ) {
        return self::fetchNachaFileTypes( 'SELECT * FROM nacha_file_types ORDER BY order_num, name', $objPaymentDatabase );
    }

}
?>