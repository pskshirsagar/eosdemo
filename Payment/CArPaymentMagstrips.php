<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentMagstrips
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CArPaymentMagstrips extends CBaseArPaymentMagstrips {

	public static function fetchArPaymentMagstripByArPaymentId( $intArPaymentId, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM ar_payment_magstrips WHERE ar_payment_id = ' . ( int ) $intArPaymentId;
		return self::fetchArPaymentMagstrip( $strSql, $objPaymentDatabase );
	}

	public static function fetchArPaymentMagstripsByArPaymentIds( $arrintArPaymentIds, $objPaymentDatabase ) {
		if( false == valArr( $arrintArPaymentIds ) ) return NULL;

		$strSql = 'SELECT * FROM ar_payment_magstrips WHERE ar_payment_id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' );';
		return self::fetchArPaymentMagstrips( $strSql, $objPaymentDatabase );
	}

}
?>