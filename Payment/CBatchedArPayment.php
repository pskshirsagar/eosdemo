<?php

class CBatchedArPayment extends CBaseBatchedArPayment {

	protected $m_objArPayment;

    /**
     * Get Functions
     */

	public function getArPayment() {
		return $this->m_objArPayment;
	}

	/**
	 * Set Functions
	 */

	public function setArPayment( $objArPayment ) {
		$this->m_objArPayment = $objArPayment;
	}

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>