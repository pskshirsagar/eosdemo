<?php

class CWesternUnionOutputFile extends CBaseWesternUnionOutputFile {

	protected $m_arrobjWesternUnionOutputRecords;

	/**
	 * Add Functions
	 */

	public function addWesternUnionOutputRecord( $objWesternUnionOutputRecord ) {
		$this->m_arrobjWesternUnionOutputRecords[$objWesternUnionOutputRecord->getMtcn()] = $objWesternUnionOutputRecord;
	}

	/**
	 * Get Functions
	 */

	public function getWesternUnionOutputRecords() {
		return $this->m_arrobjWesternUnionOutputRecords;
	}

	public function getWesternUnionOutputRecordsByCompanyMerchantAccountId( $intCompanyMerchantAccountId ) {

		$arrobjWesternUnionOutputRecords = array();

		// ******************************************************************************************************
		// Loop on records to find all records for this merchant account.
		// Use reset() function to get the first element in the companies array of company merchant accounts
		// ******************************************************************************************************

		foreach( $this->m_arrobjWesternUnionOutputRecords as $objWesternUnionOutputRecord ) {
			$objCompanyMerchantAccount = reset( $objWesternUnionOutputRecord->getClient()->getCompanyMerchantAccounts() );

			if( $intCompanyMerchantAccountId == $objCompanyMerchantAccount->getId() ) {
				$arrobjWesternUnionOutputRecords[$objWesternUnionOutputRecord->getId()] = $objWesternUnionOutputRecord;
			}
		}

		return $arrobjWesternUnionOutputRecords;
	}

	/**
	 * Create Functions
	 */

	public function createReconciliationEntry( $objPaymentDatabase ) {

		$objReconciliationEntry = new CReconciliationEntry();
		$objReconciliationEntry->setProcessingBankAccountId( $this->getProcessingBankAccountId() );
		$objReconciliationEntry->setWesternUnionOutputFileId( $this->getId() );
		$objReconciliationEntry->setTotal( abs( $this->getTrailerTotalDollarAmount() ) );

		$intReconciliationEntryTypeId = NULL;

		switch( $this->getFileTypeId() ) {

			case CWesternUnionFileType::OUTPUT_PAID:
				$intReconciliationEntryTypeId = CReconciliationEntryType::WESTERN_UNION_EMONEY_ORDER;
				break;

			case CWesternUnionFileType::INPUT_STAGE:
			case CWesternUnionFileType::OUTPUT_ERROR:
			case CWesternUnionFileType::OUTPUT_EXPIRY:
			default:
				$intReconciliationEntryTypeId = NULL; // this should trigger error.
		}

		$objReconciliationEntry->setReconciliationEntryTypeId( $intReconciliationEntryTypeId );

		// Make sure that the date is not in the common.transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.

		$intCreatedOn = strtotime( $this->getCreatedOn() );

		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y', $intCreatedOn ), date( 'm/d/Y', ( $intCreatedOn + ( 21 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = array();

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intCreatedOn = strtotime( '+1 day', $intCreatedOn );

		while( 'Sun' == date( 'D', $intCreatedOn ) || 'Sat' == date( 'D', $intCreatedOn ) || true == in_array( $intCreatedOn, $arrintProhibitedTransactionHolidays ) ) {
			$intCreatedOn = strtotime( '+1 day', $intCreatedOn );
		}

		$objReconciliationEntry->setMovementDatetime( date( 'm/d/Y', $intCreatedOn ) );

		return $objReconciliationEntry;
	}

    /**
     * Fetch Functions
     */

	public function fetchWesternUnionOutputRecords( $objPaymentDatabase ) {
		$this->m_arrobjWesternUnionOutputRecords = \Psi\Eos\Payment\CWesternUnionOutputRecords::createService()->fetchWesternUnionOutputRecordsByFileId( $this->getId(), $objPaymentDatabase );
	}

	/**
	 * Val Functions
	 */

    public function valId() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valFileTypeId() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getFileTypeId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_type_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHeaderRecordType() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getHeaderRecordType() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'header_record_type', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHeaderOutputType() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getHeaderOutputType() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'header_output_type', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHeaderDatetime() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getHeaderDatetime() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'header_datetime', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHeaderClientId() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getHeaderClientId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'header_client_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHeaderFiller() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getHeaderFiller() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'header_filler', '' ) );
        // }

        return $boolIsValid;
    }

    public function valTrailerRecordType() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getTrailerRecordType() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'trailer_record_type', '' ) );
        // }

        return $boolIsValid;
    }

    public function valTrailerNumberOfRecords() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getTrailerNumberOfRecords() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'trailer_number_of_records', '' ) );
        // }

        return $boolIsValid;
    }

    public function valTrailerTotalDollarAmount() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getTrailerTotalDollarAmount() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'trailer_total_dollar_amount', '' ) );
        // }

        return $boolIsValid;
    }

    public function valTrailerTotalCustomerFees() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getTrailerTotalCustomerFees() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'trailer_total_customer_fees', '' ) );
        // }

        return $boolIsValid;
    }

    public function valReceivedOn() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getReceivedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'received_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getUpdatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getUpdatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getCreatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

       // Validation example
        // if( true == is_null( $this->getCreatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>