<?php

class CX937ViewDataRecord extends CBaseX937ViewDataRecord {

	protected $m_objX937Bundle;
	protected $m_objX937CheckDetailRecord;
	protected $m_objPaymentDatabase;

    /**
     * Set Functions
     */

	public function setDefaults() {

    	$this->setId( $this->fetchNextId( $this->m_objPaymentDatabase ) );

    	// A code used to identify this type of record. [01-02]
    	$this->setRecordType( '52' );

    	if( false == valObj( $this->m_objX937Bundle, 'CX937Bundle' ) ) {
			trigger_error( 'Bundle was not set.', E_USER_ERROR );
			exit;
		}

    	// A number that identifies the institution that creates the bundle header record. [03-11]
    	$this->setEceInstitutionRoutingNumber( $this->m_objX937Bundle->getHeaderEceInstitutionRoutingNumber() );

		// The year month and day that designates the business date of the bundle.  This number is imported from the Bundle Business Date (Field 5) in the Bundle
		// Header Record (Type 20) associated with the image view conveyed in this Image View Data Record. [12-19]
    	$this->setBundleBusinessDate( $this->m_objX937Bundle->getHeaderBusinessDate() );

		// A code assigned by the institution that creates the bundle.  Denotes the cycle in which the bundle is created.  This number is imported from the
		// Cycle Number (Field 9) in the Bundle Header Record (Type 20) assocaited with the image view conveyed in this Image View Data Record. [20-21]
    	$this->setCycleNumber( $this->m_objX937Bundle->getHeaderCycleNumber() );

    	if( false == valObj( $this->m_objX937CheckDetailRecord, 'CX937CheckDetailRecord' ) ) {
			trigger_error( 'Check Detail Record was not set.', E_USER_ERROR );
			exit;
		}

    	// A number assigned by the institution that creates the Check Detail Record (Type 25) or Return Record (Type 31) or Credit/Reconciliation Record (Type 61).  The
    	// number is imported from the ECE Institiution Item Sequence Number (Field 7) in the Check Detail Record (Type 25). [22-36]
    	$this->setEceInstitutionItemSequenceNumber( $this->m_objX937CheckDetailRecord->getEceInstitutionItemSequenceNumber() );

		// A unique designation of the entity that creates the Digital Signature for data to be exchanged.  See Annex M for more information. [37-52]
    	$this->setSecurityOriginatorName( '                ' );

    	// Unique designation of the entity that may perform verification of the signed images.  This field is typloically only used when there is a
    	// need to identify a signel verification designation.  See Annex M for more information. [53-68]
    	$this->setSecurityAuthenticatorName( '                ' );

    	// A name or character sequence used by the signer (originator) to communicate a key identifier to the recipient (authenticator) so the recipient
    	// can obtain the key needed to validate the signature.  The name is typlically used as an identifier related to the key pair used to sign the image. [69-84]
    	$this->setSecurityKeyName( '                ' );

    	// A code used to define the corner of the conveyed image view that is taken as the reference point (origin) for the clipping coordinates.  Top, bottom, left, and right
    	// references apply to a view that presents a visually correct orientation.  When clipping information is present, the nature of the Area of Interest defined by
    	// the clipping rectangle is determined by the value of the View Descriptor (Field 9) in the related Image View Detail Record (Type 50). [85-85]
    	$this->setClippingOrigin( '0' );

    	// A number that represents the horizontal offset in pixels from the clipping origin to the nearest vertical side of the clipping rectangle. [86-89]
    	$this->setClippingCoordinateH1( '    ' );

    	// A number that represents the horizontal offset in pixels from the clipping origin to the furthermost vertical side of the clipping rectangle. [90-93]
    	$this->setClippingCoordinateH2( '    ' );

    	// A number that represents the vertical offset in pixels from the clipping origin to the nearest horizontal side of the clipping rectangle [94-97]
    	$this->setClippingCoordinateV1( '    ' );

    	// A number that represents the vertical offset in pixels from the clipping origin to the furthermost horizontal side of the clipping rectangle. [98-101]
    	$this->setClippingCoordinateV2( '    ' );

    	// The number of bytes contained in the Image Reference Key (Field 22) in this Image View Data Record. [102-105+x] (this should be dynamic)
    	// this is the length of the url used to access the image.
    	$this->setLengthOfImageReferenceKey( 0 );

    	// A designator assigned by the ECE institution that creates the Check Detail, Return and Image View Records.  The designator, when used,
    	// should uniquely identify the item image to the ECE institution.  The designator could be a key that would be used by the creating
    	// institution to locate the unique associated image or it could provide a full access path and name that would allow direct external look up and access of
    	// the image.  For example, this could be a url.  This field would normally match Image Archive Sequence Number/Image Archive locator (Field 5) in the Check
    	// Detail Addendum B Record (Type 27), or Return Addendum C Record (Type 34), if used. See Annex I for more information in developing the Image Key. [106-(105+x)]
    	$this->setImageReferenceKey( '' );

    	// The number of bytes contained in the Digital Signaure (Field 23) in this Image View Data Record. [106+x - 110+x]
    	$this->setLengthOfDigitalSignature( '0' );

		// The Digital signature created by applying the hash function and cryptographic algorithms and private/secret key to the data to be protected.  The
		// Digital Signature provides user authentication and data integrity. [variable] [111+x - 110+x+y]
    	$this->setDigitalSignature( '' );

		// The number of bytes in the Image Data (Field 19) in this Image View Data Record. [111+x+y - 117+x+y]
    	// $this->setLengthOfImageData( '' );

    	// The Image Data field contains the image view.  The Image data generally consists of an image header and the compressed image raster data.  The image header provides
    	// information that is required to interpret the image raster data.  The image header provdes information that is required to interpret the image raster data.  The image
    	// raster data contains the scanned image of the physical item in raster (line by line) format.  EAch scan line comprises a set of concatenated pixels
    	// The image comprises a set of scan lines.  The image raster data is typlically compressed to reduce the number of bytes needed to transmit and store the image.
    	// The header/image format type is defined by teh Image View Format Indicator (Field 5) in teh corresponding Image View Detail Record. (Type 50).
    	// The syntax and semantics of the image header/image format are understood by referring to the appropriate image format specification.  The compression scheme
    	// used to compress the image raster data is specified in the image view Compression Algorithm Identifier (Field 6) of the associated Image View Detail
    	// record (Type 50), and in the image header portion of the Image Data or by association with the selected image format. [variable]
    	// $this->setImageData(); (should be set dynamically) [118+x+y - 117+x+y+z]
	}

    public function setPaymentDatabase( $objPaymentDatabase ) {
    	$this->m_objPaymentDatabase = $objPaymentDatabase;
    }

	public function setX937Bundle( $objX937Bundle ) {
		$this->m_objX937Bundle = $objX937Bundle;
	}

	public function setX937CheckDetailRecord( $objX937CheckDetailRecord ) {
		$this->m_objX937CheckDetailRecord = $objX937CheckDetailRecord;
	}

	public function setRecordType( $strRecordType ) {
	    $this->m_strRecordType = CStrings::strNachaDef( $strRecordType, 2, true, true, '0' );
	}

	public function setEceInstitutionRoutingNumber( $strEceInstitutionRoutingNumber ) {
	    $this->m_strEceInstitutionRoutingNumber = CStrings::strNachaDef( $strEceInstitutionRoutingNumber, 9, true, true, '0' );
	}

	public function setBundleBusinessDate( $strBundleBusinessDate ) {
	    $this->m_strBundleBusinessDate = CStrings::strNachaDef( $strBundleBusinessDate, 8, true, true, '0' );
	}

	public function setCycleNumber( $strCycleNumber ) {
	    $this->m_strCycleNumber = CStrings::strNachaDef( $strCycleNumber, 2, false, false );
	}

	public function setEceInstitutionItemSequenceNumber( $strEceInstitutionItemSequenceNumber ) {
	    $this->m_strEceInstitutionItemSequenceNumber = CStrings::strNachaDef( $strEceInstitutionItemSequenceNumber, 15, false, false );
	}

	public function setSecurityOriginatorName( $strSecurityOriginatorName ) {
	    $this->m_strSecurityOriginatorName = CStrings::strNachaDef( $strSecurityOriginatorName, 16, false, false );
	}

	public function setSecurityAuthenticatorName( $strSecurityAuthenticatorName ) {
	    $this->m_strSecurityAuthenticatorName = CStrings::strNachaDef( $strSecurityAuthenticatorName, 16, false, false );
	}

	public function setSecurityKeyName( $strSecurityKeyName ) {
	    $this->m_strSecurityKeyName = CStrings::strNachaDef( $strSecurityKeyName, 16, false, false );
	}

	public function setClippingOrigin( $strClippingOrigin ) {
	    $this->m_strClippingOrigin = CStrings::strNachaDef( $strClippingOrigin, 1, false, false );
	}

	public function setClippingCoordinateH1( $strClippingCoordinateH1 ) {
	    $this->m_strClippingCoordinateH1 = CStrings::strNachaDef( $strClippingCoordinateH1, 4, true, true, ' ' );
	}

	public function setClippingCoordinateH2( $strClippingCoordinateH2 ) {
	    $this->m_strClippingCoordinateH2 = CStrings::strNachaDef( $strClippingCoordinateH2, 4, true, true, ' ' );
	}

	public function setClippingCoordinateV1( $strClippingCoordinateV1 ) {
	    $this->m_strClippingCoordinateV1 = CStrings::strNachaDef( $strClippingCoordinateV1, 4, true, true, ' ' );
	}

	public function setClippingCoordinateV2( $strClippingCoordinateV2 ) {
	    $this->m_strClippingCoordinateV2 = CStrings::strNachaDef( $strClippingCoordinateV2, 4, true, true, ' ' );
	}

	public function setLengthOfImageReferenceKey( $strLengthOfImageReferenceKey ) {
	    $this->m_strLengthOfImageReferenceKey = CStrings::strNachaDef( $strLengthOfImageReferenceKey, 4, false, false );
	}

	public function setImageReferenceKey( $strImageReferenceKey ) {
	    $this->m_strImageReferenceKey = CStrings::strNachaDef( $strImageReferenceKey, -1, false, false );
	}

	public function setLengthOfDigitalSignature( $strLengthOfDigitalSignature ) {
	    $this->m_strLengthOfDigitalSignature = CStrings::strNachaDef( $strLengthOfDigitalSignature, 5, false, false );
	}

	public function setDigitalSignature( $strDigitalSignature ) {
	    $this->m_strDigitalSignature = CStrings::strNachaDef( $strDigitalSignature, -1, false, false );
	}

	public function setLengthOfImageData( $strLengthOfImageData ) {
	    $this->m_strLengthOfImageData = CStrings::strNachaDef( $strLengthOfImageData, 7, false, false );
	}

	public function setImageData( $strImageData ) {
	    $this->m_strImageData = CStrings::strNachaDef( $strImageData, -1, false, false, ' ', NULL, true );
	}

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Database Functions
     */

    public function insert( $intCurrentUserId, $objPaymentDatabase, $boolReturnSqlOnly = false ) {
    	$objDataset = $objPaymentDatabase->createDataset();

    	$intId = $this->getId();

    	if( true == is_null( $intId ) ) {
    		$strSql = "SELECT nextval( 'x937_view_data_records_id_seq'::text ) AS id";

    		if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert x937 view data record record. The following error was reported.' ) );
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objPaymentDatabase->errorMsg() ) );

    			$objDataset->cleanup();
    			return false;
    		}

    		$arrValues = $objDataset->fetchArray();
    		$this->setId( $arrValues['id'] );

    		$objDataset->cleanup();
    	}

    	$strSql = 'SELECT * ' .
    	'FROM x937_view_data_records_insert( ' .
    	$this->sqlId() . ', ' .
    	$this->sqlX937CheckDetailRecordId() . ', ' .
    	$this->sqlRecordType() . ', ' .
    	$this->sqlEceInstitutionRoutingNumber() . ', ' .
    	$this->sqlBundleBusinessDate() . ', ' .
    	$this->sqlCycleNumber() . ', ' .
    	$this->sqlEceInstitutionItemSequenceNumber() . ', ' .
    	$this->sqlSecurityOriginatorName() . ', ' .
    	$this->sqlSecurityAuthenticatorName() . ', ' .
    	$this->sqlSecurityKeyName() . ', ' .
    	$this->sqlClippingOrigin() . ', ' .
    	$this->sqlClippingCoordinateH1() . ', ' .
    	$this->sqlClippingCoordinateH2() . ', ' .
    	$this->sqlClippingCoordinateV1() . ', ' .
    	$this->sqlClippingCoordinateV2() . ', ' .
    	$this->sqlLengthOfImageReferenceKey() . ', ' .
    	$this->sqlImageReferenceKey() . ', ' .
    	$this->sqlLengthOfDigitalSignature() . ', ' .
    	$this->sqlDigitalSignature() . ', ' .
    	$this->sqlLengthOfImageData() . ', ' .
    	'NULL, ' .
    	( int ) $intCurrentUserId . ' ) AS result;';

    	if( false == $objDataset->execute( $strSql ) ) {
    		// Reset id on error
    		$this->setId( $intId );

    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert x937 view data record record. The following error was reported.' ) );
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objPaymentDatabase->errorMsg() ) );

    		$objDataset->cleanup();

    		return false;
    	}

    	if( 0 < $objDataset->getRecordCount() ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert x937 view data record record. The following error was reported.' ) );
    		// Reset id on error
    		$this->setId( $intId );

    		while( false == $objDataset->eof() ) {
    			$arrValues = $objDataset->fetchArray();
    			$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
    			$objDataset->next();
    		}

    		$objDataset->cleanup();

    		return false;
    	}

    	$objDataset->cleanup();

    	return true;
    }

    public function update( $intCurrentUserId, $objPaymentDatabase, $boolReturnSqlOnly = false ) {
    	$objDataset = $objPaymentDatabase->createDataset();

    	$strSql = 'SELECT * ' .
    	'FROM x937_view_data_records_update( ' .
    	$this->sqlId() . ', ' .
    	$this->sqlX937CheckDetailRecordId() . ', ' .
    	$this->sqlRecordType() . ', ' .
    	$this->sqlEceInstitutionRoutingNumber() . ', ' .
    	$this->sqlBundleBusinessDate() . ', ' .
    	$this->sqlCycleNumber() . ', ' .
    	$this->sqlEceInstitutionItemSequenceNumber() . ', ' .
    	$this->sqlSecurityOriginatorName() . ', ' .
    	$this->sqlSecurityAuthenticatorName() . ', ' .
    	$this->sqlSecurityKeyName() . ', ' .
    	$this->sqlClippingOrigin() . ', ' .
    	$this->sqlClippingCoordinateH1() . ', ' .
    	$this->sqlClippingCoordinateH2() . ', ' .
    	$this->sqlClippingCoordinateV1() . ', ' .
    	$this->sqlClippingCoordinateV2() . ', ' .
    	$this->sqlLengthOfImageReferenceKey() . ', ' .
    	$this->sqlImageReferenceKey() . ', ' .
    	$this->sqlLengthOfDigitalSignature() . ', ' .
    	$this->sqlDigitalSignature() . ', ' .
    	$this->sqlLengthOfImageData() . ', ' .
    	'NULL, ' .
    	( int ) $intCurrentUserId . ' ) AS result;';

    	if( false == $objDataset->execute( $strSql ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update x937 view data record record. The following error was reported.' ) );
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objPaymentDatabase->errorMsg() ) );

    		$objDataset->cleanup();

    		return false;
    	}

    	if( 0 < $objDataset->getRecordCount() ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update x937 view data record record. The following error was reported.' ) );
    		while( false == $objDataset->eof() ) {
    			$arrValues = $objDataset->fetchArray();
    			$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
    			$objDataset->next();
    		}

    		$objDataset->cleanup();

    		return false;
    	}

    	$objDataset->cleanup();

    	return true;
    }

	/**
	 * Other Functions
	 */

	public function buildRecord( $boolConvertToEbcdic = false ) {

		$strRecord = '';

		$strRecord .= $this->getRecordType();
		$strRecord .= $this->getEceInstitutionRoutingNumber();
		$strRecord .= $this->getBundleBusinessDate();
		$strRecord .= $this->getCycleNumber();
		$strRecord .= $this->getEceInstitutionItemSequenceNumber();

		$strRecord .= $this->getSecurityOriginatorName();
		$strRecord .= $this->getSecurityAuthenticatorName();
		$strRecord .= $this->getSecurityKeyName();

		$strRecord .= $this->getClippingOrigin();
		$strRecord .= $this->getClippingCoordinateH1();
		$strRecord .= $this->getClippingCoordinateH2();
		$strRecord .= $this->getClippingCoordinateV1();
		$strRecord .= $this->getClippingCoordinateV2();

		$strRecord .= $this->getLengthOfImageReferenceKey();
		$strRecord .= $this->getImageReferenceKey();

		$strRecord .= $this->getLengthOfDigitalSignature();
		$strRecord .= $this->getDigitalSignature();
		$strRecord .= $this->getLengthOfImageData();

		if( true == $boolConvertToEbcdic ) {
			$strRecord = \Psi\Libraries\UtilStrings\CStrings::createService()->asciiToEbcdic( $strRecord );
		}

		// This data should be in binary, not ebcdic, so we can convert it later.
		$strRecord .= $this->getImageData();

		return $strRecord;
	}
}
?>