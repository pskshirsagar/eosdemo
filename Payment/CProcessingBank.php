<?php

class CProcessingBank extends CBaseProcessingBank {

	const FIRST_REGIONAL	= 1;
	const ZIONS_BANK 	   	= 2;
	const FIFTH_THIRD 	   	= 3;
	const BANK_OF_MONTREAL	= 4;
	const INTERNATIONAL		= 5;

    /**
     * Get Functions
     */

    public function getEndrsRtrnsAcctNum() {
	    return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getEndrsRtrnsAcctNumEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
    }

    /**
     * Fetch Functions
     */

    public function fetchDailyNachaFilesByDate( $strDate, $objPaymentDatabase ) {
    	return \Psi\Eos\Payment\CNachaFiles::createService()->fetchDailyNachaFilesByProcessingBankIdByDate( $this->getId(), $strDate, $objPaymentDatabase );
    }

    public function fetchDailyX937FilesByDate( $strDate, $objPaymentDatabase ) {
    	return \Psi\Eos\Payment\CX937Files::createService()->fetchDailyX937FilesByProcessingBankIdByDate( $this->getId(), $strDate, $objPaymentDatabase );
    }

}
?>