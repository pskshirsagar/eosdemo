<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentStatistics
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CArPaymentStatistics extends CBaseArPaymentStatistics {

	public static function fetchArPaymentStatisticsFromArPaymentsByMonthDuration( $intPreviousMonthsDuration, $objPaymentDatabase ) {

		$strCsvAcceptablePaymentStatusTypes = CPaymentStatusType::CAPTURED . ', ' . CPaymentStatusType::RECALL . ', ' . CPaymentStatusType::PHONE_CAPTURE_PENDING . ', ' . CPaymentStatusType::AUTHORIZED;

		$strSql = 'SELECT
			       		DATE_TRUNC( \'month\', ap.payment_datetime ) AS month,
                   		SUM( ap.donation_amount ) AS donation_total,
                   		SUM( ap.convenience_fee_amount ) as convenience_fee_total,
                   		SUM( ap.company_charge_amount ) as company_fee_total,
                   		SUM( ap.payment_cost ) as cost_total,
			       		SUM( COALESCE( ap.company_charge_amount, 0 ) + COALESCE( ap.convenience_fee_amount, 0 ) - COALESCE( ap.payment_cost, 0 ) ) as profit_total,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::ACH . ' THEN ap.payment_amount ELSE 0 END ) AS ach_total,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::VISA . ' THEN ap.payment_amount ELSE 0 END ) AS vi_total,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::MASTERCARD . ' THEN ap.payment_amount ELSE 0 END ) AS mc_total,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::AMEX . ' THEN ap.payment_amount ELSE 0 END ) AS amex_total,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::DISCOVER . ' THEN ap.payment_amount ELSE 0 END ) AS di_total,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::CHECK_21 . ' THEN ap.payment_amount ELSE 0 END ) AS ch21_total,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::EMONEY_ORDER . ' THEN ap.payment_amount ELSE 0 END ) AS wu_total,
                   		0 AS pnm_total,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::MONEY_ORDER . ' THEN ap.payment_amount ELSE 0 END ) AS mg_total,
                   		SUM(
                   			CASE
                   				WHEN ap.payment_type_id = ' . CPaymentType::ACH . '
                   				THEN ap.convenience_fee_amount + ap.company_charge_amount
                   				ELSE 0
                   			END
                   		) AS ach_revenue,
                   		SUM(
                   			CASE
                   				WHEN ap.payment_type_id IN ( ' . CPaymentType::VISA . ', ' . CPaymentType::MASTERCARD . ', ' . CPaymentType::AMEX . ', ' . CPaymentType::DISCOVER . ' )
                   				THEN ap.convenience_fee_amount + ap.company_charge_amount
                   				ELSE 0
                   			END
                   		) AS cc_revenue,
                   		SUM(
                   			CASE
                   				WHEN ap.payment_type_id = ' . CPaymentType::CHECK_21 . '
                   				THEN ap.convenience_fee_amount + ap.company_charge_amount
                   				ELSE 0
                   			END
                   		) AS ch21_revenue,
                   		SUM(
                   			CASE
                   				WHEN ap.payment_type_id = ' . CPaymentType::EMONEY_ORDER . '
                   				THEN 0.76
                   				ELSE 0
                   			END
                   		) AS wu_revenue,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::ACH . ' THEN 1 ELSE 0 END ) AS ach_count,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::VISA . ' THEN 1 ELSE 0 END ) AS vi_count,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::MASTERCARD . ' THEN 1 ELSE 0 END ) AS mc_count,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::AMEX . ' THEN 1 ELSE 0 END ) AS amex_count,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::DISCOVER . ' THEN 1 ELSE 0 END ) AS di_count,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::CHECK_21 . ' THEN 1 ELSE 0 END ) AS ch21_count,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::EMONEY_ORDER . ' THEN 1 ELSE 0 END ) AS wu_count,
                   		0 AS pnm_count,
                   		SUM( CASE WHEN ap.payment_type_id = ' . CPaymentType::MONEY_ORDER . ' THEN 1 ELSE 0 END ) AS mg_count,
                   		SUM(
                   			CASE
                   				WHEN ap.return_type_id IS NOT NULL
                   					AND ap.payment_type_id IN ( ' . CPaymentType::VISA . ', ' . CPaymentType::MASTERCARD . ', ' . CPaymentType::AMEX . ', ' . CPaymentType::DISCOVER . ' )
                   				THEN 1
                   				ELSE 0
                   			END
                   		) AS cc_return_count,
                   		SUM(
                   			CASE
                   				WHEN ap.return_type_id IS NOT NULL
                   					AND ap.payment_type_id IN ( ' . CPaymentType::ACH . ' )
                   				THEN 1
                   				ELSE 0
                   			END
                   		) AS ach_return_count,
                   		SUM(
                   			CASE
                   				WHEN ap.return_type_id IN ( ' . CReturnType::R05 . ', ' . CReturnType::R07 . ', ' . CReturnType::R10 . ', ' . CReturnType::R29 . ', ' . CReturnType::R51 . ' )
                   					AND ap.payment_type_id IN ( ' . CPaymentType::ACH . ' )
                   				THEN 1
                   				ELSE 0
                   			END
                   		) AS ach_unauth_return_count,
                   		SUM(
                   			CASE
                   				WHEN ap.return_type_id IS NOT NULL
                   					AND ap.payment_type_id IN ( ' . CPaymentType::CHECK_21 . ' )
                   				THEN 1
                   				ELSE 0
                   			END
                   		) AS check21_return_count,
                   		SUM(
                   			CASE
                   				WHEN ap.return_type_id IN ( ' . CReturnType::CHECK21_NOT_AUTHORIZED . ' )
                   					AND ap.payment_type_id IN ( ' . CPaymentType::CHECK_21 . ' )
                   				THEN 1
                   				ELSE 0
                   			END
                   		) AS check21_unauth_return_count
					FROM
			       		ar_payments ap,
			       		clients c
					WHERE
			       		ap.cid = c.id
						AND ap.company_merchant_account_id IS NOT NULL
			       		AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
			       		AND ap.payment_status_type_id IN ( ' . $strCsvAcceptablePaymentStatusTypes . ' )
			       		AND ap.payment_datetime < date_trunc( \'month\', now() )
			       		AND ap.payment_datetime >= date_trunc( \'month\', now() - INTERVAL \'' . ( int ) $intPreviousMonthsDuration . ' months\' )
					GROUP BY
			      		DATE_TRUNC( \'month\', ap.payment_datetime )
					ORDER BY
			      		DATE_TRUNC( \'month\', ap.payment_datetime ) DESC
				';

		$arrobjArPaymentStatistics = self::fetchArPaymentStatistics( $strSql, $objPaymentDatabase );

		return $arrobjArPaymentStatistics;
	}

}
?>