<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaFileStatusTypes
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CNachaFileStatusTypes extends CBaseNachaFileStatusTypes {

	public static function fetchNachaFileStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CNachaFileStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchNachaFileStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CNachaFileStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllNachaFileStatusTypes( $objPaymentDatabase ) {
		return self::fetchNachaFileStatusTypes( 'SELECT * FROM nacha_file_status_types ORDER BY order_num, name', $objPaymentDatabase );
	}

}
?>