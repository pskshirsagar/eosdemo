<?php

class CMoneyOrderIssuer extends CBaseMoneyOrderIssuer {

    public function valId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ));
        // }

        return $boolIsValid;
    }

    public function valCheckRoutingNumber() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCheckRoutingNumber())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', '' ));
        // }

        return $boolIsValid;
    }

    public function valCheckAccountNumberEncrypted() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCheckAccountNumberEncrypted())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', '' ));
        // }

        return $boolIsValid;
    }

    public function valOccurrences() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getOccurrences())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occurrences', '' ));
        // }

        return $boolIsValid;
    }

    public function valIsExcluded() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getIsExcluded())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_excluded', '' ));
        // }

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getUpdatedBy())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ));
        // }

        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getUpdatedOn())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ));
        // }

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCreatedBy())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ));
        // }

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCreatedOn())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ));
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>