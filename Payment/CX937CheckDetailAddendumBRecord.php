<?php

class CX937CheckDetailAddendumBRecord extends CBaseX937CheckDetailAddendumBRecord {

	protected $m_objPaymentDatabase;

    /**
     * Set Functions
     */

	public function setDefaults() {

    	$this->setId( $this->fetchNextId( $this->m_objPaymentDatabase ) );

    	// A code used to identify this type of record. [01-02]
    	$this->setRecordType( '27' );

    	// A code that identifies whether Field 5 is fixed or variable size. [03]
    	$this->setVariableSizeRecordIndicator( '0' );

    	// A number that identifies the item in the microfilm archive system; it may be different than the ECE institution Item
    	// Sequence Number (Clause 10.8) and the Image Archive Sequence Number (Field 5 - Option 1) [04-18]
    	$this->setMicrofilmArchiveSequenceNumber( '               ' );

    	// The number of characters in the Image Archive Sequence Number/Image Archive Locator field (Field 5).  The
    	// number of characteres can be fixed or variable depending on the value of field 2.  See Annex I for more information. [19-22]
    	$this->setLengthOfImageArchiveLocator( '34' );

		// Enter the locator reference. [23-56]
    	// $this->setImageArchiveLocator( $this->m_intId ); (Set in Check Detail Record)

    	// A field used to describe the transaction. [57-71]
    	$this->setDescription( '               ' );

    	// A field used at the discretion of the users of the standard. [72-75]
    	$this->setUserField( '    ' );

    	// A field reserved for future use by the Accredited Standards Committee X9 [76-80]
    	$this->setReserved( '     ' );
	}

    public function setPaymentDatabase( $objPaymentDatabase ) {
    	$this->m_objPaymentDatabase = $objPaymentDatabase;
    }

	public function setRecordType( $strRecordType ) {
	    $this->m_strRecordType = CStrings::strNachaDef( $strRecordType, 2, true, true, '0' );
	}

	public function setVariableSizeRecordIndicator( $strVariableSizeRecordIndicator ) {
	    $this->m_strVariableSizeRecordIndicator = CStrings::strNachaDef( $strVariableSizeRecordIndicator, 1, true, true, '0' );
	}

	public function setMicrofilmArchiveSequenceNumber( $strMicrofilmArchiveSequenceNumber ) {
	    $this->m_strMicrofilmArchiveSequenceNumber = CStrings::strNachaDef( $strMicrofilmArchiveSequenceNumber, 15, false, false );
	}

	public function setLengthOfImageArchiveLocator( $strLengthOfImageArchiveLocator ) {
	    $this->m_strLengthOfImageArchiveLocator = CStrings::strNachaDef( $strLengthOfImageArchiveLocator, 4, true, true, '0' );
	}

	public function setImageArchiveLocator( $strImageArchiveLocator ) {
	    $this->m_strImageArchiveLocator = CStrings::strNachaDef( $strImageArchiveLocator, 34, false, false );
	}

	public function setDescription( $strDescription ) {
	    $this->m_strDescription = CStrings::strNachaDef( $strDescription, 15, false, false );
	}

	public function setUserField( $strUserField ) {
	    $this->m_strUserField = CStrings::strNachaDef( $strUserField, 4, false, false );
	}

	public function setReserved( $strReserved ) {
	    $this->m_strReserved = CStrings::strNachaDef( $strReserved, 5, false, false );
	}

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	/**
	 * Other Functions
	 */

	public function buildRecord( $boolConvertToEbcdic = false ) {

		$strRecord = '';

		$strRecord .= $this->getRecordType();
		$strRecord .= $this->getVariableSizeRecordIndicator();
		$strRecord .= $this->getMicrofilmArchiveSequenceNumber();
		$strRecord .= $this->getLengthOfImageArchiveLocator();
		$strRecord .= $this->getImageArchiveLocator();
		$strRecord .= $this->getDescription();
		$strRecord .= $this->getUserField();
		$strRecord .= $this->getReserved();

		if( true == $boolConvertToEbcdic ) {
			$strRecord = \Psi\Libraries\UtilStrings\CStrings::createService()->asciiToEbcdic( $strRecord );
		}

		return $strRecord;
	}
}
?>