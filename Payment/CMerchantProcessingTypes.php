<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMerchantProcessingTypes
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CMerchantProcessingTypes extends CBaseMerchantProcessingTypes {

	public static $c_arrstrCountryCodesByMerchantProcessingType = [
		CMerchantProcessingType::STANDARD => [ CCountry::CODE_USA, CCountry::CODE_CANADA ],
		CMerchantProcessingType::INTERNATIONAL => [ CCountry::CODE_SPAIN, CCountry::CODE_FRANCE, CCountry::CODE_IRELAND, CCountry::CODE_MEXICO ],
	];

	public static function fetchMerchantProcessingTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMerchantProcessingType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMerchantProcessingType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMerchantProcessingType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllMerchantProcessingTypes( $objPaymentDatabase ) {
		return parent::fetchMerchantProcessingTypes( 'SELECT * FROM merchant_processing_types WHERE is_published = 1 ORDER BY order_num', $objPaymentDatabase );
	}

}
?>