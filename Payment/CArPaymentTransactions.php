<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentTransactions
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CArPaymentTransactions extends CBaseArPaymentTransactions {

	public static function fetchArPaymentAuthCodeByArPaymentIdPaymentTransactionTypeIds( $intPaymentId, $arrintPaymentTransactionTypeIds, $objPaymentDatabase ) {
        $strAuthCode = NULL;

        $strSql = 'SELECT gateway_transaction_number, gateway_approval_code FROM ar_payment_transactions WHERE ar_payment_id = ' . ( int ) $intPaymentId . ' AND payment_transaction_type_id IN ( ' . implode( ',', $arrintPaymentTransactionTypeIds ) . ') ORDER BY id DESC LIMIT 1;';

        $objPaymentDataset = $objPaymentDatabase->createDataset();

        if( false == $objPaymentDataset->execute( $strSql ) ) {
            $objPaymentDataset->cleanup();
            return NULL;
        }

        if( 0 < $objPaymentDataset->getRecordCount() ) {

            while( false == $objPaymentDataset->eof() ) {
                $arrmixValues = $objPaymentDataset->fetchArray();

                if( true == isset( $arrmixValues['gateway_transaction_number'] ) ) {
					$strAuthCode = $arrmixValues['gateway_transaction_number'];
				} elseif( true == isset( $arrmixValues['gateway_approval_code'] ) ) {
					$strAuthCode = $arrmixValues['gateway_approval_code'];
				}
                $objPaymentDataset->next();
            }
        }

        $objPaymentDataset->cleanup();

        return $strAuthCode;
    }

    public static function fetchArPaymentTransactionsbyArPaymentId( $intArPaymentId, $objPaymentDatabase ) {
    	return self::fetchArPaymentTransactions( 'SELECT * FROM ar_payment_transactions WHERE ar_payment_id = ' . ( int ) $intArPaymentId . ' ORDER BY created_on ASC', $objPaymentDatabase );
    }

    public static function fetchArPaymentTransactionByArPaymentIdByArPaymentTransactionTypeId( $intArPaymentId, $intArPaymentTransactionTypeId, $objPaymentDatabase ) {
    	return self::fetchArPaymentTransaction( 'SELECT * FROM ar_payment_transactions WHERE ar_payment_id = ' . ( int ) $intArPaymentId . ' AND payment_transaction_type_id = ' . ( int ) $intArPaymentTransactionTypeId . ' LIMIT 1', $objPaymentDatabase );
    }

	public static function fetchArPaymentTransactionByArPaymentIdByArPaymentTransactionTypeIds( $intArPaymentId, $arrintArPaymentTransactionTypeIds, $objPaymentDatabase ) {
		return self::fetchArPaymentTransaction( 'SELECT * FROM ar_payment_transactions WHERE ar_payment_id = ' . ( int ) $intArPaymentId . ' AND payment_transaction_type_id IN (' . implode( ',', $arrintArPaymentTransactionTypeIds ) . ' ) LIMIT 1', $objPaymentDatabase );
	}

    public static function fetchTransactionDatetimeByArPaymentIdByArPaymentTransactionTypeId( $intArPaymentId, $intArPaymentTransactionTypeId, $objPaymentDatabase ) {
		$strSql = 'SELECT
	                        transaction_datetime as transaction_datetime
	                    FROM
	                        ar_payment_transactions
	                    WHERE
	                        ar_payment_id = ' . ( int ) $intArPaymentId . '
	                        AND payment_transaction_type_id = ' . ( int ) $intArPaymentTransactionTypeId . '
	                        LIMIT 1';
		return CArPaymentTransactions::fetchColumn( $strSql, 'transaction_datetime', $objPaymentDatabase );
	}

	public static function fetchTransactionIdByArPaymentIdByArPaymentTransactionTypeId( $intArPaymentId, $intArPaymentTransactionTypeId, $objPaymentDatabase ) {
		$strSql = 'SELECT
                        id as transaction_id
                    FROM
                        ar_payment_transactions
                    WHERE
                        ar_payment_id = ' . ( int ) $intArPaymentId . '
                        AND payment_transaction_type_id = ' . ( int ) $intArPaymentTransactionTypeId . '
                        LIMIT 1';

		$arrintTransactionId = fetchData( $strSql, $objPaymentDatabase );
		return $arrintTransactionId[0]['transaction_id'];
	}

	public static function fetchTransactionIdByArPaymentIdByArPaymentTransactionTypeIds( $intArPaymentId, $intArPaymentTransactionTypeIds, $objPaymentDatabase ) {
		$strSql = 'SELECT
                        id as transaction_id
                    FROM
                        ar_payment_transactions
                    WHERE
                        ar_payment_id = ' . ( int ) $intArPaymentId . '
                        AND payment_transaction_type_id IN ( ' . implode( ',', $intArPaymentTransactionTypeIds ) . ' )
                        LIMIT 1';

		$arrintTransactionId = fetchData( $strSql, $objPaymentDatabase );
		return $arrintTransactionId[0]['transaction_id'];
	}

    public static function fetchLastArPaymentTransactionByArPaymentIdByArPaymentTransactionTypeId( $intArPaymentId, $intArPaymentTransactionTypeId, $objPaymentDatabase, $strTransactionMemo = NULL ) {

    	$strSql = 'SELECT
    					*
    				FROM
    					ar_payment_transactions
    				WHERE
    					ar_payment_id = ' . ( int ) $intArPaymentId . '
    					AND payment_transaction_type_id = ' . ( int ) $intArPaymentTransactionTypeId . '
    					' . ( ( NULL != $strTransactionMemo ) ? ' AND transaction_memo ILIKE \'' . addslashes( $strTransactionMemo ) . '%\'' : '' ) . '
    				ORDER BY id DESC LIMIT 1';

    	return self::fetchArPaymentTransaction( $strSql, $objPaymentDatabase );
    }

    // This function is used in CPaymentProcesses to isssue transaction number to payment gateway further we store it at our end as reference.

	public static function fetchNextId( $objPaymentDatabase ) {

        $objPaymentDataset = $objPaymentDatabase->createDataset();

        $strSql = "SELECT nextval('ar_payment_transactions_id_seq'::text) AS id";

        if( ( false == $objPaymentDataset->execute( $strSql ) ) || ( 1 != $objPaymentDataset->getRecordCount() ) ) {
            $objPaymentDataset->cleanup();
            return false;
        }

        $arrmixValues = $objPaymentDataset->fetchArray();
        $intId = $arrmixValues['id'];

        $objPaymentDataset->cleanup();

        return $intId;
    }

    public static function fetchPendingArPaymentTransactionsByArPaymentId( $intArPaymentId, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM ar_payment_transactions WHERE is_pending = TRUE and ar_payment_id = ' . ( int ) $intArPaymentId . ' ORDER BY transaction_datetime DESC';

		return self::fetchArPaymentTransactions( $strSql, $objPaymentDatabase );
	}

}
?>