<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaEntryDetailRecords
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */

class CNachaEntryDetailRecords extends CBaseNachaEntryDetailRecords {

	public static function fetchNachaEntryDetailRecordsBySettlementDistributionIds( $arrintSettlementDistributionIds, $objPaymentDatabase ) {
		if( false == valArr( $arrintSettlementDistributionIds ) ) return NULL;
		$strSql = 'SELECT * FROM nacha_entry_detail_records WHERE	settlement_distribution_id IN( ' . implode( ',', $arrintSettlementDistributionIds ) . ' )';
		return self::fetchNachaEntryDetailRecords( $strSql, $objPaymentDatabase );
	}

	// This query allows us to identify / lookup the original ACH transaction that resulted in the return.

	public static function fetchNachaEntryDetailRecordByTraceNumber( $strTraceNumber, $objPaymentDatabase ) {

		if ( true == is_null( $strTraceNumber ) || 0 == strlen( trim( $strTraceNumber ) ) ) return NULL;

		$strSql = 'SELECT * FROM
						nacha_entry_detail_records nedr
					WHERE
						trace_number = \'' . addslashes( trim( $strTraceNumber ) ) . '\'
					ORDER BY
						created_on DESC
					LIMIT 1';

		return self::fetchNachaEntryDetailRecord( $strSql, $objPaymentDatabase );
	}

	// This query allows us to identify / lookup the original ACH transaction that resulted in the return.

	public static function fetchNachaEntryDetailRecordByTraceNumberByHeaderEffectiveEntryDate( $strTraceNumber, $strHeaderEffectiveEntryDate, $objPaymentDatabase ) {

		// NOTE: The check on effective entry date was removed some time ago. Based on empircal evidence, the effective entry date for the NFB and NRFB can fluctuate +/-1 day (perhaps more).
		//       Its just not a reliable supplemenatary value to match on... Ultimately the trace numbers will repeat at some point in the near future.  The trace suffix uses
		///      the least significant 7 digits of the NEDR.id (range of only 10 million). The query currently limits the result set to 1, and orders descending (so we'll always get the most recent).
		//       This should suffice for any forseeable future need as there is no case I can think of where we would ever want to get the older NEDR.  If it becomes necessary to introduce a
		//       supplementary value to match on, the most promising seems to be: NEDR.check_routing_number == NRDR.check_routing_number.  Base on empirical evidence, these value have always
		//       matched exactly.  So if you must add stronger matching, this is probably the best candidate. -Jad Mar 2012

		// AND nfb.header_effective_entry_date = '" . trim( $strHeaderEffectiveEntryDate ) . "'

		$strSql = 'SELECT
						nedr.*
					FROM
						nacha_entry_detail_records nedr,
						nacha_file_batches nfb
					WHERE
						nedr.nacha_file_batch_id = nfb.id
						AND nedr.trace_number = \'' . addslashes( trim( $strTraceNumber ) ) . '\'
					ORDER BY
						nedr.id DESC
					LIMIT 1';

		return self::fetchNachaEntryDetailRecord( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnReconPreppedNachaEntryDetailRecords( $objPaymentDatabase ) {

		$strSql = 'SELECT
						nedr.*,
						nf.nacha_file_type_id
					FROM
						nacha_entry_detail_records nedr,
						nacha_files nf
					WHERE
						nedr.nacha_file_id = nf.id
						AND nedr.is_recon_prepped <> 1
						AND nedr.is_intermediary = 1
						AND nf.confirmed_on IS NOT NULL
						AND nedr.eft_instruction_id IS NULL
					LIMIT 50000';

		return self::fetchNachaEntryDetailRecords( $strSql, $objPaymentDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByNachaFileId( $intNachaFileId, $objDatabase ) {

		$strSql = 'SELECT * FROM nacha_entry_detail_records WHERE nacha_file_id = ' . ( int ) $intNachaFileId;
		return self::fetchNachaEntryDetailRecords( $strSql, $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsByArPaymentId( $intArPaymentId, $objDatabase ) {

		$strSql = 'SELECT * FROM nacha_entry_detail_records WHERE ar_payment_id = ' . ( int ) $intArPaymentId;
		return self::fetchNachaEntryDetailRecords( $strSql, $objDatabase );
	}

	public static function fetchNachaEntryDetailRecordsBySettlementDistributionId( $intSettlementDistributionId, $objDatabase ) {

		$strSql = 'SELECT * FROM nacha_entry_detail_records WHERE settlement_distribution_id = ' . ( int ) $intSettlementDistributionId;
		return self::fetchNachaEntryDetailRecords( $strSql, $objDatabase );
	}

	public static function fetchTraceNumberById( $intNachaEntryDetailRecordId, $objDatabase ) {
		$strSql = 'SELECT trace_number FROM nacha_entry_detail_records WHERE id = ' . ( int ) $intNachaEntryDetailRecordId;

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixResult ) || false == is_array( $arrmixResult[0] ) || false == isset( $arrmixResult[0]['trace_number'] ) ) {
			return NULL;
		}

		return $arrmixResult[0]['trace_number'];
	}

}

?>