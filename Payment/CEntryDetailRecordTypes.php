<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CEntryDetailRecordTypes
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CEntryDetailRecordTypes extends CBaseEntryDetailRecordTypes {

	public static function fetchEntryDetailRecordTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEntryDetailRecordType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEntryDetailRecordType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEntryDetailRecordType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllEntryDetailRecordTypes( $objPaymentDatabase ) {
        return self::fetchEntryDetailRecordTypes( 'SELECT * FROM entry_detail_record_types ORDER BY order_num, name', $objPaymentDatabase );
	}

}
?>