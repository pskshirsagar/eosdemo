<?php

class CArPaymentBilling extends CBaseArPaymentBilling {

	const IMAGE_CONVERSION_ELIGIBLE_MIN_WIDTH = 5.90;
	const IMAGE_CONVERSION_ELIGIBLE_MAX_WIDTH = 6.85;
	const IMAGE_CONVERSION_ELIGIBLE_MIN_HEIGHT = 2.53;
	const IMAGE_CONVERSION_ELIGIBLE_MAX_HEIGHT = 2.85;

	/**
	 * Set Functions
	 *
	 */

	public function setCcCardNumber( $strCcCardNumber ) {
		$strCcCardNumber = CStrings::strTrimDef( $strCcCardNumber, 20, NULL, true, false, true );
		if( true == \valStr( $strCcCardNumber ) ) {
			$this->setCcCardNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCcCardNumber, CONFIG_SODIUM_KEY_CC_CARD_NUMBER ) );
		}
	}

    public function setCheckBankName( $strCheckBankName ) {
        $this->m_strCheckBankName = CStrings::strTrimDef( $strCheckBankName, 100, NULL, true, false, true );
    }

    public function setCheckNameOnAccount( $strCheckNameOnAccount ) {
        $this->m_strCheckNameOnAccount = CStrings::strTrimDef( \Psi\CStringService::singleton()->ucwords( $strCheckNameOnAccount ), 50, NULL, true, false, true );
    }

    public function setCheckPayableTo( $strCheckPayableTo ) {
        $this->m_strCheckPayableTo = CStrings::strTrimDef( $strCheckPayableTo, 240, NULL, true, false, true );
    }

    public function setCcCardNumberEncrypted( $strCcCardNumberEncrypted ) {
        $this->m_strCcCardNumberEncrypted = CStrings::strTrimDef( $strCcCardNumberEncrypted, 240, NULL, true, false, true );
    }

    public function setCcNameOnCard( $strCcNameOnCard ) {
        $this->m_strCcNameOnCard = CStrings::strTrimDef( $strCcNameOnCard, 50, NULL, true, false, true );
    }

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->m_strCheckAccountNumberEncrypted = CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true );

		if( true == valStr( $this->getCheckAccountNumber() ) ) {
			$this->setCheckAccountNumberBindex( \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( $this->getCheckAccountNumber(), CONFIG_SODIUM_KEY_BLIND_INDEX ) );
		}

		if( false == is_null( $this->m_strCheckAccountNumberEncrypted ) && '' != trim( $this->m_strCheckAccountNumberEncrypted ) && true == \valStr( $this->m_strCheckAccountNumberEncrypted ) ) {
			$strCheckAccountNumberLastfourEncrypted	= $this->getCheckAccountNumber();
			if( true == \valStr( $strCheckAccountNumberLastfourEncrypted ) ) {
				$strCheckAccountNumberLastfourEncrypted = \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( \Psi\CStringService::singleton()->substr( $strCheckAccountNumberLastfourEncrypted, -4 ), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER );
			}
			$this->setCheckAccountNumberLastfourEncrypted( $strCheckAccountNumberLastfourEncrypted );
		}

	}

	/**
	 * Get Functions
	 *
	 */

    public function getCcCardNumber() {
    	if( !valStr( $this->getCcCardNumberEncrypted() ) ) {
    		return NULL;
		}

		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCcCardNumberEncrypted(), CONFIG_SODIUM_KEY_CC_CARD_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CC_CARD_NUMBER ] );
	}

	public function getCheckAccountNumber() {
    	if( !valStr( $this->getCheckAccountNumberEncrypted() ) ) {
    		return NULL;
		}

		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	/**
	 * Validation Functions
	 *
	 */

    public function valId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valArPaymentId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCcExpDateMonth() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCcExpDateYear() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCcNameOnCard() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCheckDate() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCheckPayableTo() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCheckBankName() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCheckNameOnAccount() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCheckAccountTypeId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCheckRoutingNumber() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCheckAccountNumberEncrypted() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function calculateAndSetCheckIsConverted( $objArPaymentImageFront, $intCompanyMerchantAccountId, $intCustomerId, $intCid, $objPaymentDatabase, $objClientDatabase ) {

    	// ***********************************************************************************************************
		// Calculate if this payment is eligible for C21-to-ACH conversion
		// ***********************************************************************************************************

    	$boolIsEligible = true;

    	switch( NULL ) {
            default:
            	$objCompanyMerchantAccount = \Psi\Eos\Payment\CCompanyMerchantAccounts::createService()->fetchCompanyMerchantAccountById( $intCompanyMerchantAccountId, $objPaymentDatabase );

            	// Verify that convert_check21_to_ach is asserted for the specified merchant account
            	if( 0 == $objCompanyMerchantAccount->getConvertCheck21ToAch() ) {
            		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_is_converted', 'Not eligible: Merchant account is not enabled for c21-to-ach conversion', NULL ) );
		    		$boolIsEligible = false;
		    		break;
            	}

            	// Verify check_is_money_order flag is not set
		    	if( 1 == $this->m_intCheckIsMoneyOrder ) {
		    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_is_converted', 'Not eligible: is_money_order is true', NULL ) );
		    		$boolIsEligible = false;
		    		break;
		    	}

		    	// Verify account type is personal checking
		    	if( $this->m_intCheckAccountTypeId != CCheckAccountType::PERSONAL_CHECKING ) {
		    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_is_converted', 'Not eligible: is not a personal checking account type', NULL ) );
		    		$boolIsEligible = false;
		    		break;
		    	}

		    	if( true == \valStr( $this->m_strCheckAccountNumberEncrypted ) ) {
				    $objMoneyOrderIssuer = \Psi\Eos\Payment\CMoneyOrderIssuers::createService()->fetchMoneyOrderIssuerByCheckAccountNumberByCheckRoutingNumber( trim( \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] ) ), trim( $this->m_strCheckRoutingNumber ), $objPaymentDatabase );
			    }

				// Verify routing/account not present in money_order_issuers table
    			if( true == valObj( $objMoneyOrderIssuer, 'CMoneyOrderIssuer' ) ) {
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_is_converted', 'Not eligible: routing/account exists in money_order_issuers', NULL ) );
					$boolIsEligible = false;
		    		break;
    			}

    			if( 0 >= $objArPaymentImageFront->getImageWidth() || 0 >= $objArPaymentImageFront->getImageHeight() || 0 >= $objArPaymentImageFront->getImageXResolution() || 0 >= $objArPaymentImageFront->getImageYResolution() ) {
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_is_converted', 'Not eligible: Image Width/Height and Resolution must be set', NULL ) );
					$boolIsEligible = false;
		    		break;
    			}

		    	$fltWidth  = floatval( $objArPaymentImageFront->getImageWidth() / $objArPaymentImageFront->getImageXResolution() );
		    	$fltHeight = floatval( $objArPaymentImageFront->getImageHeight() / $objArPaymentImageFront->getImageYResolution() );

		    	// Verify image_width BETWEEN 5.9 and 6.85 inches (Allow .10 inches extra for min/max)
		    	if( self::IMAGE_CONVERSION_ELIGIBLE_MIN_WIDTH > $fltWidth || self::IMAGE_CONVERSION_ELIGIBLE_MAX_WIDTH < $fltWidth ) {
		    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_is_converted', 'Not eligible: Image width [' . $fltWidth . '] is outside range', NULL ) );
		    		$boolIsEligible = false;
		    		break;
		    	}

		    	// Verify image_height BETWEEN 2.53 and 2.85 (Allow .10 inches extra for min/max)
    			if( self::IMAGE_CONVERSION_ELIGIBLE_MIN_HEIGHT > $fltHeight || self::IMAGE_CONVERSION_ELIGIBLE_MAX_HEIGHT < $fltHeight ) {
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_is_converted', 'Not eligible: Image height [' . $fltHeight . '] is outside range', NULL ) );
		    		$boolIsEligible = false;
		    		break;
		    	}

		    	// Fetch the customer object and verify whether customer opted in for conversion of Check 21 to ACH
		    	$intCustomerSettingCount = CCustomerSettings::fetchCustomerSettingCountByKeyByCustomerIdByCid( $intCustomerId, $intCid, $objClientDatabase );
		    	if( 1 == $objCompanyMerchantAccount->getConvertCheck21ToAch() && 0 < $intCustomerSettingCount ) {
		    		$boolIsEligible = false;
		    	}
            	break;
    	}

    	$this->m_intCheckIsConverted = ( true == $boolIsEligible ) ? 1 : 0;
    }

}
?>