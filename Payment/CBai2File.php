<?php

class CBai2File extends CBaseBai2File {

	protected $m_arrobjReconciliationEntries;
	protected $m_arrobjProcessingBankAccounts;

    public function __construct() {
        parent::__construct();

        $this->m_arrobjReconciliationEntries = array();

        return;
    }

    /**
     * Create Functions
     */

    public function createReconciliationEntry( $arrstrLineData, $arrstr3RecordLineData, $objPaymentDatabase ) {

    	$intReconciliationEntryTypeId = CReconciliationEntryType::BAI2_BANK_RECONCILIATION_FILE;

    	$objProcessingBankAccount = $this->loadProcessingBankAccount( $arrstr3RecordLineData );

    	$objReconciliationEntry = new CReconciliationEntry();

    	$objReconciliationEntry->setId( NULL );
    	$objReconciliationEntry->setProcessingBankAccountId( $objProcessingBankAccount->getId() );
    	$objReconciliationEntry->setReconciliationEntryTypeId( $intReconciliationEntryTypeId );
    	$objReconciliationEntry->setBai2FileId( $this->getId() );
    	$objReconciliationEntry->setMovementDatetime( $this->m_strFileDatetime );
    	$objReconciliationEntry->loadDataFromBai2FileLineData( $arrstrLineData, $objPaymentDatabase );

    	$this->m_arrobjReconciliationEntries[] = $objReconciliationEntry;
    }

    /**
     * Get Functions
     */

    public function getReconciliationEntries() {
    	return $this->m_arrobjReconciliationEntries;
    }

    /**
     * Set Functions
     */

    public function setReconciliationEntries( $arrobjReconciliationEntries ) {
    	$this->m_arrobjReconciliationEntries = $arrobjReconciliationEntries;
    }

    /**
     * Validate Functions
     */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
		        break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Fetch Functions
     */

    public function fetchProcessingBankAccounts( $objPaymentDatabase ) {

		$this->m_arrobjProcessingBankAccounts = \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchBankIntermediaryProcessingBankAccounts( $objPaymentDatabase );
    	$this->m_arrobjProcessingBankAccounts = rekeyObjects( 'AccountNumberBindex', $this->m_arrobjProcessingBankAccounts );

    	return $this->m_arrobjProcessingBankAccounts;
    }

    /**
     * Other Functions
     */

    public function insert( $intUserId, $objPaymentDatabase, $boolReturnSqlOnly = false ) {

    	if( false == parent::insert( $intUserId, $objPaymentDatabase, $boolReturnSqlOnly ) ) {
    		display( $this );
    		trigger_error( 'Bai2 file failed to insert.', E_USER_ERROR );
    		exit;
    	}

    	if( true == valArr( $this->m_arrobjReconciliationEntries ) ) {
			foreach( $this->m_arrobjReconciliationEntries as $objReconciliationEntry ) {
				if( false == $objReconciliationEntry->insert( $intUserId, $objPaymentDatabase ) ) {
					display( $objReconciliationEntry );
		    		trigger_error( 'Reconciliation entry failed to insert.', E_USER_ERROR );
		    		exit;
				}
			}
    	}

    	return true;
    }

	public function loadProcessingBankAccount( $arrstrLineData ) {

		if( CProcessingBank::FIFTH_THIRD == $this->m_intProcessingBankId ) {
			$strAccountNumber = ltrim( $arrstrLineData[1], '0' );
		} else {
			$strAccountNumber = $arrstrLineData[1];
		}
		$strAccountNumberBIndex = ( valStr( $strAccountNumber ) ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( $strAccountNumber, CONFIG_SODIUM_KEY_BLIND_INDEX ) : NULL;

		if( true == isset ( $this->m_arrobjProcessingBankAccounts[$strAccountNumberBIndex] ) && true == valObj( $this->m_arrobjProcessingBankAccounts[$strAccountNumberBIndex], 'CProcessingBankAccount' ) ) {
			return $this->m_arrobjProcessingBankAccounts[$strAccountNumberBIndex];
		}

		trigger_error( 'Bank account failed to load.', E_USER_ERROR );
		exit;
	}

	public function loadMovementDateTimeFromLineData( $arrstrLineData ) {

		// We get date and time as YYMMDD, HHII
		// So here we converting this format to standard format.

		$intDateArrayIndex 			= 4;
		$intTimeArrayIndex			= 5;
		$this->m_strFileDatetime 	= NULL;

		if( false == is_null( $arrstrLineData[$intDateArrayIndex] ) ) {
			$strBai2TransactionDate	= str_split( $arrstrLineData[$intDateArrayIndex], 2 );
			$this->m_strFileDatetime = implode( '-', $strBai2TransactionDate );
		}

		if( false == is_null( $arrmixBai2TransactionFields[$intTimeArrayIndex] ) ) {
			$strBai2TransactionTime	= str_split( $arrstrLineData[$intTimeArrayIndex], 2 );
			$this->m_strFileDatetime = $this->m_strFileDatetime . ' ' . implode( ':', $strBai2TransactionTime ) . ':00';
		}

		if( false == is_null( $this->m_strFileDatetime ) ) {
			$this->m_strFileDatetime = date( 'Y-m-d H:i:s', strtotime( $this->m_strFileDatetime ) );
		}
	}

	public function moveFileToProcessedFolder( $strBai2FileName, $strUnprocessedBai2FileDir, $strProcessedBai2FileDir ) {

		if( false == is_dir( $strProcessedBai2FileDir ) ) {

			if( false == CFileIO::recursiveMakeDir( $this->m_strProcessedBai2FileDir ) ) {
				$this->cprintln( SCRIPT_WARNING, 'Could not create destination directory: ' . $this->m_strProcessedBai2FileDir );
				return false;
			}

			chmod( $this->m_strProcessedBai2FileDir, 777 );
		}

		$strDestinationPath	= NULL;
		$strAppendedFileName = $strBai2FileName;

		$strDestinationPath = $strProcessedBai2FileDir . $strAppendedFileName;

		if( false == CFileIO::moveFile( $strUnprocessedBai2FileDir . $strBai2FileName, $strDestinationPath ) ) {
			$this->cprintln( SCRIPT_WARNING, 'Could not move file to specified path: ' . $strDestinationPath );
		}

		return true;
	}

}
?>