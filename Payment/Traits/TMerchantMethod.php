<?php

trait TMerchantMethod {

	// region Default Values

	private static $c_arrmixDefaultValues = [
		'default' => [
			// Database Fields
			// id
			// cid
			// company_merchant_account_id
			// processing_bank_account_id
			// merchant_gateway_id
			// payment_type_id
			// payment_medium_id
			// pin
			// gateway_username_encrypted
			// gateway_password_encrypted
			// login_username_encrypted
			// login_password_encrypted
			'max_payment_amount'						=> 5000,
			'undelayed_settlement_ceiling'				=> 25000,
			'max_monthly_process_amount'				=> 100000,
			'minimum_convenience_fee'					=> 0.00,
			'gateway_transaction_fee'					=> 0.25,
			'resident_transaction_fee'					=> 0.00,
			'resident_debit_transaction_fee'			=> 0.00,
			'transaction_fee'							=> 0.00,
			'reverse_transaction_fee'					=> 3.50,
			'waived_transaction_fee'					=> 0.48,
			'waived_debit_transaction_fee'				=> 0.00,
			'company_return_fee'						=> 25.00,
			'company_adjustment_fee'					=> 0.00,
			'auth_success_fee'							=> 0.12,
			'auth_failure_fee'							=> 0.00,
			'downgrade_transaction_fee'					=> 0.00,
			'international_transaction_fee'				=> 0.00,
			'resident_return_fee'						=> 0.00,
			'unauthorized_return_fee'					=> 0.00,
			'company_debit_transaction_fee'				=> 0.00,
			'rebate_transaction_fee'					=> 0.00,
			'provider_discount_rate'					=> 0.0000,
			'resident_discount_rate'					=> 0.0000,
			'resident_debit_discount_rate'				=> 0.0000,
			'company_discount_rate'						=> 0.0000,
			'waived_discount_rate'						=> 0.0000,
			'waived_debit_discount_rate'				=> 0.0000,
			'downgrade_discount_rate'					=> 0.0000,
			'international_discount_rate'				=> 0.0000,
			'company_debit_discount_rate'				=> 0.0000,
			'rebate_discount_rate'						=> 0.0000,
			// tier_one_company_incentive_fee
			// every_payment_fee
			// stored_billing_fee
			// tier_one_company_incentive_percentage
			// tier_two_company_incentive_fee
			// tier_two_company_incentive_percentage
			// tier_three_company_incentive_fee
			// tier_three_company_incentive_percentage
			'distribution_delay_days'					=> 2,
			'reversal_delay_days'						=> 1,
			'cut_off_time'								=> '15:30',
			// calc_fees_on_max_payment
			'phone_auth_required'						=> 0,
			'is_controlled_distribution'				=> true,
			'is_enabled'								=> 0,
			// updated_by
			// updated_on
			// created_by
			// created_on
			// deleted_by
			// deleted_on
			// details
			'resident_commercial_transaction_fee'		=> 0.00,
			'resident_international_transaction_fee'	=> 0.00,
			'resident_commercial_discount_rate'			=> 0.00,
			'resident_international_discount_rate'		=> 0.00,
		],
		'payment_types' => [
			CPaymentType::ACH => [
				'resident_transaction_fee'					=> 1.95,
				'waived_transaction_fee'					=> 1.95,
				'company_return_fee'						=> 7.25,
				'auth_success_fee'							=> 0.25,
				'reversal_delay_days'						=> 5,
			],
			CPaymentType::VISA => [
				'gateway_transaction_fee'					=> 0.48,
				'resident_debit_transaction_fee'			=> 4.95,
				'transaction_fee'							=> 0.48,
				'waived_debit_transaction_fee'				=> 6.95,
				'downgrade_transaction_fee'					=> 0.25,
				'international_transaction_fee'				=> 0.25,
				'provider_discount_rate'					=> 0.0215,
				'company_discount_rate'						=> 0.0248,
				'waived_discount_rate'						=> 0.0249,
				'downgrade_discount_rate'					=> 0.0100,
				'international_discount_rate'				=> 0.0040,
				'distribution_delay_days'					=> 3,
				'phone_auth_required'						=> 1,
				'resident_commercial_discount_rate'			=> 0.0350,
				'resident_international_discount_rate'		=> 0.0400,
			],
			CPaymentType::MASTERCARD => [
				'gateway_transaction_fee'					=> 0.48,
				'resident_transaction_fee'					=> 3.50,
				'resident_debit_transaction_fee'			=> 4.95,
				'waived_debit_transaction_fee'				=> 6.95,
				'downgrade_transaction_fee'					=> 0.25,
				'international_transaction_fee'				=> 0.25,
				'provider_discount_rate'					=> 0.0215,
				'resident_discount_rate'					=> 0.0295,
				'waived_debit_discount_rate'				=> 0.0249,
				'downgrade_discount_rate'					=> 0.0100,
				'international_discount_rate'				=> 0.0040,
				'distribution_delay_days'					=> 3,
				'resident_commercial_discount_rate'			=> 0.0350,
				'resident_international_discount_rate'		=> 0.0400,
			],
			CPaymentType::DISCOVER => [
				'gateway_transaction_fee'					=> 0.48,
				'resident_transaction_fee'					=> 3.50,
				'resident_debit_transaction_fee'			=> 4.95,
				'waived_debit_transaction_fee'				=> 6.95,
				'downgrade_transaction_fee'					=> 0.25,
				'international_transaction_fee'				=> 0.25,
				'provider_discount_rate'					=> 0.0215,
				'resident_discount_rate'					=> 0.0295,
				'waived_debit_discount_rate'				=> 0.0249,
				'downgrade_discount_rate'					=> 0.0100,
				'international_discount_rate'				=> 0.0040,
				'distribution_delay_days'					=> 3,
				'resident_commercial_discount_rate'			=> 0.0350,
				'resident_international_discount_rate'		=> 0.0400,
			],
			CPaymentType::AMEX => [
				'gateway_transaction_fee'					=> 0.48,
				'resident_debit_transaction_fee'			=> 4.95,
				'transaction_fee'							=> 3.50,
				'waived_debit_transaction_fee'				=> 6.95,
				'downgrade_transaction_fee'					=> 0.25,
				'international_transaction_fee'				=> 0.25,
				'company_discount_rate'						=> 0.0350,
				'waived_debit_discount_rate'				=> 0.0249,
				'downgrade_discount_rate'					=> 0.0100,
				'international_discount_rate'				=> 0.0040,
				'distribution_delay_days'					=> 3,
				'resident_commercial_discount_rate'			=> 0.0350,
				'resident_international_discount_rate'		=> 0.0400,
			],
			CPaymentType::CHECK_21 => [
				'transaction_fee'							=> 0.65,
				'waived_transaction_fee'					=> 0.65,
				'company_return_fee'						=> 5.00,
				'reversal_delay_days'						=> 7,
			],
			CPaymentType::EMONEY_ORDER => [
				'transaction_fee'							=> 1.50,
				'reverse_transaction_fee'					=> 0.00,
				'waived_transaction_fee'					=> 1.50,
				'company_return_fee'						=> 0.00,
				'auth_success_fee'							=> 0.00,
				'reversal_delay_days'						=> 7,
			],
			CPaymentType::SEPA_DIRECT_DEBIT => [],
			CPaymentType::PAD => [
				'company_return_fee'						=> 7.25,
				'auth_success_fee'							=> 0.25,
				'distribution_delay_days'					=> 5,
			],
		],
		'payment_mediums' => [
			CPaymentMedium::WEB => [
				CPaymentType::ACH => [
					'is_enabled'								=> 1,
				],
				CPaymentType::VISA => [
					'max_payment_amount'						=> 500,
					'resident_transaction_fee'					=> 3.50,
					'transaction_fee'							=> 0.00,
					'resident_discount_rate'					=> 0.0295,
					'company_discount_rate'						=> 0.0000,
				],
				CPaymentType::MASTERCARD => [],
				CPaymentType::DISCOVER => [],
				CPaymentType::AMEX => [],
				CPaymentType::CHECK_21 => [
					'phone_auth_required'						=> 1,
				],
				CPaymentType::EMONEY_ORDER => [
					'auth_success_fee'							=> 0.25,
				],
				CPaymentType::SEPA_DIRECT_DEBIT => [],
				CPaymentType::PAD => [
					'resident_transaction_fee'					=> 1.95,
					'waived_transaction_fee'					=> 1.95,
				],
			],
			CPaymentMedium::RECURRING => [
				CPaymentType::ACH => [
					'is_enabled'								=> 1,
				],
				CPaymentType::VISA => [
					'transaction_fee'							=> 0.48,
				],
				CPaymentType::MASTERCARD => [],
				CPaymentType::DISCOVER => [],
				CPaymentType::AMEX => [],
				CPaymentType::CHECK_21 => [
					'reverse_transaction_fee'					=> 0.65,
				],
				CPaymentType::EMONEY_ORDER => [],
				CPaymentType::SEPA_DIRECT_DEBIT => [],
				CPaymentType::PAD => [
					'waived_transaction_fee'					=> 0.00,
					'auth_success_fee'							=> 0.00,
				],
			],
			CPaymentMedium::TERMINAL => [
				CPaymentType::ACH => [
					'transaction_fee'							=> 0.95,
					'waived_transaction_fee'					=> 2.50,
					'company_return_fee'						=> 15.00,
				],
				CPaymentType::VISA => [
					'waived_transaction_fee'					=> 2.50,
					'waived_discount_rate'						=> 0.0285,
				],
				CPaymentType::MASTERCARD => [
					'waived_transaction_fee'					=> 2.50,
					'waived_debit_discount_rate'				=> 0.0285,
				],
				CPaymentType::DISCOVER => [
					'waived_transaction_fee'					=> 2.50,
					'waived_debit_discount_rate'				=> 0.0285,
				],
				CPaymentType::AMEX => [
					'waived_transaction_fee'					=> 2.50,
				],
				CPaymentType::CHECK_21 => [
					'company_return_fee'						=> 4.25,
					'is_enabled'								=> 1,
				],
				CPaymentType::EMONEY_ORDER => [
					'is_enabled'								=> 1,
				],
				CPaymentType::SEPA_DIRECT_DEBIT => [],
				CPaymentType::PAD => [
					'transaction_fee'							=> 0.95,
					'waived_transaction_fee'					=> 2.50,
					'company_return_fee'						=> 15.00
				],
			],
		]
	];

	// endregion

	// region Database Field Dependencies

	public abstract function getId();

	public abstract function getCid();

	public abstract function getProcessingBankAccountId();

	public abstract function getMerchantGatewayId();

	public abstract function getPaymentTypeId();

	public abstract function getPaymentMediumId();

	public abstract function getGatewayUsernameEncrypted();

	public abstract function setGatewayUsernameEncrypted( $strUsername );

	public abstract function getGatewayPasswordEncrypted();

	public abstract function setGatewayPasswordEncrypted( $strPassword );

	public abstract function getLoginUsernameEncrypted();

	public abstract function setLoginUsernameEncrypted( $strUsername );

	public abstract function getLoginPasswordEncrypted();

	public abstract function setLoginPasswordEncrypted( $strPassword );

	public abstract function getMaxPaymentAmount();

	public abstract function getUndelayedSettlementCeiling();

	public abstract function getMaxMonthlyProcessAmount();

	public abstract function getMinimumConvenienceFee();

	public abstract function getGatewayTransactionFee();

	public abstract function getResidentTransactionFee();

	public abstract function getResidentDebitTransactionFee();

	public abstract function getTransactionFee();

	public abstract function getReverseTransactionFee();

	public abstract function getWaivedTransactionFee();

	public abstract function getWaivedDebitTransactionFee();

	public abstract function getCompanyReturnFee();

	public abstract function getCompanyAdjustmentFee();

	public abstract function getAuthSuccessFee();

	public abstract function getAuthFailureFee();

	public abstract function getDowngradeTransactionFee();

	public abstract function getInternationalTransactionFee();

	public abstract function getResidentReturnFee();

	public abstract function getUnauthorizedReturnFee();

	public abstract function getCompanyDebitTransactionFee();

	public abstract function getRebateTransactionFee();

	public abstract function getProviderDiscountRate();

	public abstract function getResidentDiscountRate();

	public abstract function getResidentDebitDiscountRate();

	public abstract function getCompanyDiscountRate();

	public abstract function getWaivedDiscountRate();

	public abstract function getWaivedDebitDiscountRate();

	public abstract function getDowngradeDiscountRate();

	public abstract function getInternationalDiscountRate();

	public abstract function getCompanyDebitDiscountRate();

	public abstract function getRebateDiscountRate();

	public abstract function getTierOneCompanyIncentiveFee();

	public abstract function getTierOneCompanyIncentivePercentage();

	public abstract function getTierTwoCompanyIncentiveFee();

	public abstract function getTierTwoCompanyIncentivePercentage();

	public abstract function getTierThreeCompanyIncentiveFee();

	public abstract function getTierThreeCompanyIncentivePercentage();

	public abstract function getEveryPaymentFee();

	public abstract function getStoredBillingFee();

	public abstract function getDistributionDelayDays();

	public abstract function getReversalDelayDays();

	public abstract function getCutOffTime();

	public abstract function getCalcFeesOnMaxPayment();

	public abstract function getPhoneAuthRequired();

	public abstract function getIsControlledDistribution();

	public abstract function getIsEnabled();

	public abstract function getResidentCommercialTransactionFee();

	public abstract function getResidentCommercialDiscountRate();

	public abstract function getResidentInternationalTransactionFee();

	public abstract function getResidentInternationalDiscountRate();

	public abstract function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

	// endregion

	// region Additional Dependencies

	public abstract function getTier();

	public abstract function getErrorMsgs();

	/**
	 * @param CErrorMsg $objErrorMsg
	 */
	public abstract function addErrorMsg( $objErrorMsg );

	/**
	 * @param CErrorMsg[] $arrobjErrorMsgs
	 */
	public abstract function addErrorMsgs( $arrobjErrorMsgs );

	// endregion

	// region Additional Accessors

	// region Gateway Username

	/**
	 * @return string|null
	 * @throws CCryptoErrorException
	 * @throws CCryptoInvalidException
	 */
	public function getGatewayUsername() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_GATEWAY_USERNAME ];
		return $this->getGatewayUsernameEncrypted() ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getGatewayUsernameEncrypted(), CONFIG_SODIUM_KEY_GATEWAY_USERNAME, $arrmixOptions ) : NULL;
	}

	/**
	 * @param $strGatewayUsername
	 * @return $this
	 * @throws CCryptoErrorException
	 * @throws CCryptoInvalidException
	 */
	public function setGatewayUsername( $strGatewayUsername ) {
		$this->setGatewayUsernameEncrypted( valStr( $strGatewayUsername ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strGatewayUsername, CONFIG_SODIUM_KEY_GATEWAY_USERNAME ) : NULL );
		return $this;
	}

	// endregion

	// region Gateway Password

	/**
	 * @return string|null
	 * @throws CCryptoErrorException
	 * @throws CCryptoInvalidException
	 */
	public function getGatewayPassword() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_GATEWAY_PASSWORD ];
		return $this->getGatewayPasswordEncrypted() ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getGatewayPasswordEncrypted(), CONFIG_SODIUM_KEY_GATEWAY_PASSWORD, $arrmixOptions ) : NULL;
	}

	/**
	 * @param $strGatewayPassword
	 * @return $this
	 * @throws CCryptoErrorException
	 * @throws CCryptoInvalidException
	 */
	public function setGatewayPassword( $strGatewayPassword ) {
		$this->setGatewayPasswordEncrypted( valStr( $strGatewayPassword ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strGatewayPassword, CONFIG_SODIUM_KEY_GATEWAY_PASSWORD ) : NULL );
		return $this;
	}

	// endregion

	// region Login Username

	/**
	 * @return string|null
	 * @throws CCryptoErrorException
	 * @throws CCryptoInvalidException
	 */
	public function getLoginUsername() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_USERNAME ];
		return $this->getLoginUsernameEncrypted() ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getLoginUsernameEncrypted(), CONFIG_SODIUM_KEY_LOGIN_USERNAME, $arrmixOptions ) : NULL;
	}

	/**
	 * @param $strLoginUsername
	 * @return $this
	 * @throws CCryptoErrorException
	 * @throws CCryptoInvalidException
	 */
	public function setLoginUsername( $strLoginUsername ) {
		$this->setLoginUsernameEncrypted( valStr( $strLoginUsername ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strLoginUsername, CONFIG_SODIUM_KEY_LOGIN_USERNAME ) : NULL );
		return $this;
	}

	// endregion

	// region Login Password

	public function getLoginPassword() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD ];
		return $this->getLoginPasswordEncrypted() ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getLoginPasswordEncrypted(), CONFIG_SODIUM_KEY_LOGIN_PASSWORD, $arrmixOptions ) : NULL;
	}

	public function setLoginPassword( $strLoginPassword ) {
		$this->setLoginPasswordEncrypted( valStr( $strLoginPassword ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strLoginPassword, CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) : NULL );
		return $this;
	}

	// endregion

	// endregion

	// region Details Field Accessors

	// region Worldpay CC Recurring Username

	public function getWorldpayCcRecurringUsername() {
		return $this->getDetailsField( 'worldpay_cc_recurring_username' );
	}

	public function setWorldpayCcRecurringUsername( $strUsername ) {
		$this->setDetailsField( 'worldpay_cc_recurring_username', $strUsername );
		return $this;
	}

	// endregion

	// region Worldpay CC Recurring Password

	public function getWorldpayCcRecurringPasswordEncrypted() {
		return $this->getDetailsField( 'worldpay_cc_recurring_password_encrypted' );
	}

	public function setWorldpayCcRecurringPasswordEncrypted( $strWorldpayCcRecurringPasswordEncrypted ) {
		$this->setDetailsField( 'worldpay_cc_recurring_password_encrypted', $strWorldpayCcRecurringPasswordEncrypted );
		return $this;
	}

	public function getWorldpayCcRecurringPassword() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_GATEWAY_PASSWORD ];
		return valStr( $this->getWorldpayCcRecurringPasswordEncrypted() ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getWorldpayCcRecurringPasswordEncrypted(), CONFIG_SODIUM_KEY_GATEWAY_PASSWORD, $arrmixOptions ) : NULL;
	}

	public function setWorldpayCcRecurringPassword( $strPassword ) {
		$this->setWorldpayCcRecurringPasswordEncrypted( valStr( $strPassword ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPassword, CONFIG_SODIUM_KEY_GATEWAY_PASSWORD ) : NULL );
		return $this;
	}

	// endregion

	// region Worldpay CC Recurring Merchant Code

	public function getWorldpayCcRecurringMerchantCode() {
		return $this->getDetailsField( 'worldpay_cc_recurring_merchant_code' );
	}

	public function setWorldpayCcRecurringMerchantCode( $strMerchantCode ) {
		$this->setDetailsField( 'worldpay_cc_recurring_merchant_code', $strMerchantCode );
		return $this;
	}

	// endregion

	// region Worldpay CC Moto Username

	public function getWorldpayCcMotoUsername() {
		return $this->getDetailsField( 'worldpay_cc_moto_username' );
	}

	public function setWorldpayCcMotoUsername( $strUsername ) {
		$this->setDetailsField( 'worldpay_cc_moto_username', $strUsername );
		return $this;
	}

	// endregion

	// region Worldpay CC Moto Password

	public function getWorldpayCcMotoPasswordEncrypted() {
		return $this->getDetailsField( 'worldpay_cc_moto_password_encrypted' );
	}

	public function setWorldpayCcMotoPasswordEncrypted( $strWorldpayCcMotoPasswordEncrypted ) {
		$this->setDetailsField( 'worldpay_cc_moto_password_encrypted', $strWorldpayCcMotoPasswordEncrypted );
		return $this;
	}

	public function getWorldpayCcMotoPassword() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_GATEWAY_PASSWORD ];
		return valStr( $this->getWorldpayCcMotoPasswordEncrypted() ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getWorldpayCcMotoPasswordEncrypted(), CONFIG_SODIUM_KEY_GATEWAY_PASSWORD, $arrmixOptions ) : NULL;
	}

	public function setWorldpayCcMotoPassword( $strPassword ) {
		$this->setWorldpayCcMotoPasswordEncrypted( valStr( $strPassword ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPassword, CONFIG_SODIUM_KEY_GATEWAY_PASSWORD ) : NULL );
		return $this;
	}

	// endregion

	// region Worldpay CC Moto Merchant Code

	public function getWorldpayCcMotoMerchantCode() {
		return $this->getDetailsField( 'worldpay_cc_moto_merchant_code' );
	}

	public function setWorldpayCcMotoMerchantCode( $strMerchantCode ) {
		$this->setDetailsField( 'worldpay_cc_moto_merchant_code', $strMerchantCode );
		return $this;
	}

	// endregion

	// region Worldpay CC Ecom Username

	public function getWorldpayCcEcomUsername() {
		return $this->getDetailsField( 'worldpay_cc_ecom_username' );
	}

	public function setWorldpayCcEcomUsername( $strUsername ) {
		$this->setDetailsField( 'worldpay_cc_ecom_username', $strUsername );
		return $this;
	}

	// endregion

	// region Worldpay CC Ecom Password

	public function getWorldpayCcEcomPasswordEncrypted() {
		return $this->getDetailsField( 'worldpay_cc_ecom_password_encrypted' );
	}

	public function setWorldpayCcEcomPasswordEncrypted( $strWorldpayCcEcomPasswordEncrypted ) {
		$this->setDetailsField( 'worldpay_cc_ecom_password_encrypted', $strWorldpayCcEcomPasswordEncrypted );
		return $this;
	}

	public function getWorldpayCcEcomPassword() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_GATEWAY_PASSWORD ];
		return valStr( $this->getWorldpayCcEcomPasswordEncrypted() ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getWorldpayCcEcomPasswordEncrypted(), CONFIG_SODIUM_KEY_GATEWAY_PASSWORD, $arrmixOptions ) : NULL;
	}

	public function setWorldpayCcEcomPassword( $strPassword ) {
		$this->setWorldpayCcEcomPasswordEncrypted( valStr( $strPassword ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPassword, CONFIG_SODIUM_KEY_GATEWAY_PASSWORD ) : NULL );
		return $this;
	}

	// endregion

	// region Worldpay CC Ecom Merchant Code

	public function getWorldpayCcEcomMerchantCode() {
		return $this->getDetailsField( 'worldpay_cc_ecom_merchant_code' );
	}

	public function setWorldpayCcEcomMerchantCode( $strMerchantCode ) {
		$this->setDetailsField( 'worldpay_cc_ecom_merchant_code', $strMerchantCode );
		return $this;
	}

	// endregion

	// region Worldpay Ecom Username

	public function getWorldpayEcomUsername() {
		return $this->getDetailsField( 'worldpay_ecom_username' );
	}

	public function setWorldpayEcomUsername( $strUsername ) {
		$this->setDetailsField( 'worldpay_ecom_username', $strUsername );
		return $this;
	}

	// endregion

	// region Worldpay Ecom Password

	public function getWorldpayEcomPasswordEncrypted() {
		return $this->getDetailsField( 'worldpay_ecom_password' );
	}

	public function setWorldpayEcomPasswordEncrypted( $strWorldpayEcomPassword ) {
		$this->setDetailsField( 'worldpay_ecom_password', $strWorldpayEcomPassword );
	}

	/**
	 * @return string|null
	 * @throws CCryptoErrorException
	 * @throws CCryptoInvalidException
	 */
	public function getWorldpayEcomPassword() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_GATEWAY_PASSWORD ];
		return valStr( $this->getWorldpayEcomPasswordEncrypted() ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getWorldpayEcomPasswordEncrypted(), CONFIG_SODIUM_KEY_GATEWAY_PASSWORD, $arrmixOptions ) : NULL;
	}

	/**
	 * @param $strPassword
	 * @throws CCryptoErrorException
	 * @throws CCryptoInvalidException
	 * @return $this
	 */
	public function setWorldpayEcomPassword( $strPassword ) {
		$this->setWorldpayEcomPasswordEncrypted( valstr( $strPassword ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPassword, CONFIG_SODIUM_KEY_GATEWAY_PASSWORD ) : NULL );
		return $this;
	}

	// endregion

	// region Worldpay Ecom Merchant Code

	public function getWorldpayEcomMerchantCode() {
		return $this->getDetailsField( 'worldpay_ecom_merchant_code' );
	}

	public function setWorldpayEcomMerchantCode( $strMerchantCode ) {
		$this->setDetailsField( 'worldpay_ecom_merchant_code', $strMerchantCode );
		return $this;
	}

	// endregion

	// region Worldpay Ecom Username

	public function getWorldpayMotoUsername() {
		return $this->getDetailsField( 'worldpay_moto_username' );
	}

	public function setWorldpayMotoUsername( $strUsername ) {
		$this->setDetailsField( 'worldpay_moto_username', $strUsername );
		return $this;
	}

	// endregion

	// region Worldpay Moto Password

	public function getWorldpayMotoPasswordEncrypted() {
		return $this->getDetailsField( 'worldpay_moto_password_encrypted' );
	}

	public function setWorldpayMotoPasswordEncrypted( $strWorldpayMotoPasswordEncrypted ) {
		$this->setDetailsField( 'worldpay_moto_password_encrypted', $strWorldpayMotoPasswordEncrypted );
		return $this;
	}

	/**
	 * @return string|null
	 * @throws CCryptoErrorException
	 * @throws CCryptoInvalidException
	 * @return $this
	 */
	public function getWorldpayMotoPassword() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_GATEWAY_PASSWORD ];
		return valStr( $this->getWorldpayMotoPasswordEncrypted() ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getWorldpayMotoPasswordEncrypted(), CONFIG_SODIUM_KEY_GATEWAY_PASSWORD, $arrmixOptions ) : NULL;
	}

	/**
	 * @param $strPassword
	 * @throws CCryptoErrorException
	 * @throws CCryptoInvalidException
	 * @return $this
	 */
	public function setWorldpayMotoPassword( $strPassword ) {
		$this->setWorldpayMotoPasswordEncrypted( valStr( $strPassword ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPassword, CONFIG_SODIUM_KEY_GATEWAY_PASSWORD ) : NULL );
		return $this;
	}

	// endregion

	// region Worldpay Moto Merchant Code

	public function getWorldpayMotoMerchantCode() {
		return $this->getDetailsField( 'worldpay_moto_merchant_code' );
	}

	public function setWorldpayMotoMerchantCode( $strMerchantCode ) {
		$this->setDetailsField( 'worldpay_moto_merchant_code', $strMerchantCode );
		return $this;
	}

	// endregion

	// region Worldpay Recurring Username

	public function getWorldpayRecurringUsername() {
		return $this->getDetailsField( 'worldpay_recurring_username' );
	}

	public function setWorldpayRecurringUsername( $strUsername ) {
		$this->setDetailsField( 'worldpay_recurring_username', $strUsername );
		return $this;
	}

	// endregion

	// region Worldpay Recurring Password

	public function getWorldpayRecurringPasswordEncrypted() {
		return $this->getDetailsField( 'worldpay_recurring_password_encrypted' );
	}

	public function setWorldpayRecurringPasswordEncrypted( $strWorldpayRecurringPasswordEncrypted ) {
		$this->setDetailsField( 'worldpay_recurring_password_encrypted', $strWorldpayRecurringPasswordEncrypted );
		return $this;
	}

	/**
	 * @return string|null
	 * @throws CCryptoErrorException
	 * @throws CCryptoInvalidException
	 */
	public function getWorldpayRecurringPassword() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_GATEWAY_PASSWORD ];
		return valStr( $this->getWorldpayRecurringPasswordEncrypted() ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getWorldpayRecurringPasswordEncrypted(), CONFIG_SODIUM_KEY_GATEWAY_PASSWORD, $arrmixOptions ) : NULL;
	}

	/**
	 * @param $strPassword
	 * @throws CCryptoErrorException
	 * @throws CCryptoInvalidException
	 * @return $this
	 */
	public function setWorldpayRecurringPassword( $strPassword ) {
		$this->setWorldpayRecurringPasswordEncrypted( valStr( $strPassword ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPassword, CONFIG_SODIUM_KEY_GATEWAY_PASSWORD ) : NULL );
		return $this;
	}

	// endregion

	// region Worldpay Recurring Merchant Code

	public function getWorldpayRecurringMerchantCode() {
		return $this->getDetailsField( 'worldpay_recurring_merchant_code' );
	}

	public function setWorldpayRecurringMerchantCode( $strMerchantCode ) {
		$this->setDetailsField( 'worldpay_recurring_merchant_code', $strMerchantCode );
	}

	// endregion

	// region Sepa Creditor ID

	public function getSepaCreditorId() {
		return $this->getDetailsField( 'sepa_creditor_id' );
	}

	public function setSepaCreditorId( $strCreditorId ) {
		$this->setDetailsField( 'sepa_creditor_id', $strCreditorId );
		return $this;
	}

	// endregion

	// region Sepa BIC

	public function getSepaBic() {
		return $this->getDetailsField( 'sepa_bic' );
	}

	public function setSepaBic( $strBic ) {
		$this->setDetailsField( 'sepa_bic', $strBic );
		return $this;
	}

	// endregion

	// region Sepa Company Address

	public function getSepaCompanyAddress() {
		return $this->getDetailsField( 'sepa_company_address' );
	}

	public function setSepaCompanyAddress( $strCompanyAddress ) {
		$this->setDetailsField( 'sepa_company_address', $strCompanyAddress );
		return $this;
	}

	// endregion

	// region Sepa Iban

	public function getSepaIbanEncrypted() {
		return $this->getDetailsField( 'sepa_iban_encrypted' );
	}

	public function setSepaIbanEncrypted( $strSepaIbanEncrypted ) {
		$this->setDetailsField( 'sepa_iban_encrypted', $strSepaIbanEncrypted );
		return $this;
	}

	public function getSepaIban() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_GATEWAY_PASSWORD ];
		return valStr( $this->getSepaIbanEncrypted() ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getSepaIbanEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, $arrmixOptions ) : NULL;
	}

	public function setSepaIban( $strSepaIban ) {
		$this->setSepaIbanEncrypted( valStr( $strSepaIban ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strSepaIban, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) : NULL );
		return $this;
	}

	public function getSepaIbanMasked() {
		$strSepaIban = $this->getSepaIban();
		$intStringLength = strlen( $strSepaIban );
		$strLastFour = \Psi\CStringService::singleton()->substr( $strSepaIban, -4 );
		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	// endregion

	// region Sepa Company Name

	public function getSepaCompanyName() {
		return $this->getDetailsField( 'sepa_company_name' );
	}

	public function setSepaCompanyName( $strCompanyName ) {
		$this->setDetailsField( 'sepa_company_name', $strCompanyName );
		return $this;
	}

	// endregion

	// region Require Business Account

	public function getRequireBusinessAccount() {
		return $this->getDetailsField( 'require_business_account' ) ?: false;
	}

	public function setRequireBusinessAccount( $boolRequireBusinessAccount ) {
		$this->setDetailsField( 'require_business_account', $boolRequireBusinessAccount );
		return $this;
	}

	// endregion

	// endregion

	public function applyDefaults() {
		$arrmixDefaults = array_merge( self::$c_arrmixDefaultValues['default'],
			self::$c_arrmixDefaultValues['payment_types'][$this->getPaymentTypeId()] ?? [],
			self::$c_arrmixDefaultValues['payment_mediums'][$this->getPaymentMediumId()][$this->getPaymentTypeId()] ?? [] );
		$this->setValues( $arrmixDefaults );
	}

}
