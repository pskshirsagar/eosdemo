<?php

trait TMerchantAccount {

	// region Properties, Getters, and Setters

	private $m_arrobjMerchantMethods = [];

	public function getMerchantMethods() {
		return $this->m_arrobjMerchantMethods;
	}

	protected function setMerchantMethods( $arrobjMerchantMethods ) {
		$this->m_arrobjMerchantMethods = $arrobjMerchantMethods;
	}

	// endregion

	// region Dependencies

	public abstract function getId();

	public abstract function getCid();

	/**
	 * Loads a structured array of TMerchantMethod objects and stores them in memory using {@see setMerchantMethods}
	 *
	 * The structure of the array should be the following:
	 *
	 *     [
	 *       payment_medium_id => [
	 *         tier => [
	 *           payment_type_id => TMerchantMethod,
	 *           ...
	 *         ],
	 *         ...
	 *       ],
	 *       ...
	 *     ]
	 *
	 * where:
	 *
	 *   * payment_medium_id is the payment_medium_id of the method, one of {@see CPaymentMedium::WEB}, {@see CPaymentMedium::RECURRING}, or {@see CPaymentMedium::TERMINAL};
	 *
	 *   * tier is the tier level of the method;
	 *
	 *     `row_number() OVER( ORDER BY max_payment_amount PARTITION BY payment_medium_id, payment_type_id ) AS tier`
	 *
	 *   * and payment_type_id is the payment_type_id of the method, one of {@see CPaymentType::$c_arrintMerchantAccountPaymentTypeIds}.
	 *
	 * @return TMerchantMethod[]
	 */
	public abstract function loadMethods();

	/**
	 * @return TMerchantMethod
	 */
	public abstract function createMethod();

	/**
	 * @param CErrorMsg $objErrorMsg
	 */
	public abstract function addErrorMsg( $objErrorMsg );

	/**
	 * @param CErrorMsg[] $arrobjErrorMsgs
	 */
	public abstract function addErrorMsgs( $arrobjErrorMsgs );

	// endregion

	// region Initialization

	public function __clone() {
		parent::__clone();

		// Clone all of the merchant account methods so they don't reference the same objects
		if( !valArr( $this->m_arrobjMerchantMethods ) ) return;
		foreach( $this->m_arrobjMerchantMethods as $intPaymentMethodId => $arrobjMerchantAccountMethodTier ) {
			foreach( $arrobjMerchantAccountMethodTier as $intTier => $arrobjMerchantAccountMethods ) {
				foreach( $arrobjMerchantAccountMethods as $intPaymentTypeId => $objMerchantAccountMethod ) {
					$this->m_arrobjMerchantMethods[$intPaymentMethodId][$intTier][$intPaymentTypeId] = clone $objMerchantAccountMethod;
				}
			}
		}
	}

	// endregion

	// region Merchant Method Operations

	/**
	 * Returns the merchant method with the lowest max_payment_amount that is still greater than $fltPaymentAmount,
	 * for the given paymeent medium and type, or null if a matching method cannot be found.
	 *
	 * Omitting $fltPaymentAmount defaults to a payment amount of 0, and the lowest enabled method of the given
	 * medium and type will be returned.
	 *
	 * Omitting $intPaymentMediumId defaults first to the payment medium set on the merchant account, and then to
	 * {@see CPaymentMedium::WEB}.
	 *
	 * @param $intPaymentTypeId
	 * @param int $fltPaymentAmount
	 * @param $intPaymentMediumId
	 * @return TMerchantMethod|null
	 */
	public function getMethodByAmount( $intPaymentTypeId, $fltPaymentAmount = 0, $intPaymentMediumId = NULL ) {
		$intPaymentMediumId = $intPaymentMediumId ?: $this->getPaymentMediumId() ?: CPaymentMedium::WEB;
		if( !$this->checkPaymentMediumId( $intPaymentMediumId ) ) return NULL;
		if( !$this->checkPaymentTypeId( $intPaymentTypeId ) ) return NULL;
		if( !valArr( $arrobjMethods = $this->getMerchantMethods() ) ) $this->loadMethods();
		if( !valArr( $arrobjMethodTiers = $arrobjMethods[$intPaymentMediumId] ?? [] ) ) return NULL;

		// Go through the tiers for the selected medium in order, returning as soon as we find an enabled method that
		// matches the selected payment type and with a max_payment_amount greater than the selected payment amount.
		foreach( $arrobjMethodTiers as $intTier => $arrobjMethodTier ) {
			$objMethod = $arrobjMethodTier[$intPaymentTypeId] ?? NULL;
			if( $objMethod && $objMethod->getIsEnabled() && $objMethod->getMaxPaymentAmount() >= $fltPaymentAmount ) {
				return $objMethod;
			}
		}

		// No enabled methods for the selected medium and payment type have a max payment amount more than the selected
		// payment amount
		return NULL;
	}

	/**
	 * @param $intPaymentTypeId
	 * @param $intPaymentMediumId
	 * @return TMerchantMethod|null
	 */
	public function getTopTierMethod( $intPaymentTypeId, $intPaymentMediumId = NULL ) {
		$intPaymentMediumId = $intPaymentMediumId ?: $this->getPaymentMediumId() ?: CPaymentMedium::WEB;
		if( !$this->checkPaymentMediumId( $intPaymentMediumId ) ) return NULL;
		if( !$this->checkPaymentTypeId( $intPaymentTypeId ) ) return NULL;
		if( !valArr( $arrobjMethods = $this->getMerchantMethods() ) ) $this->loadMethods();
		if( !valArr( $arrobjMethodTiers = $arrobjMethods[$intPaymentMediumId] ?? [] ) ) return NULL;

		// Go through the tiers for the selected medium in reverse order, returning as soon as we find an enabled method
		// of the selected payment type
		foreach( array_reverse( $arrobjMethodTiers, true ) as $intTier => $arrobjMethodTier ) {
			$objMethod = $arrobjMethodTier[$intPaymentTypeId] ?? NULL;
			if( $objMethod && $objMethod->getIsEnabled() ) return $objMethod;
		}

		// No enabled methods for the selected medium and payment type were found
		return NULL;
	}

	/**
	 * @param $intPaymentTypeId
	 * @param $intPaymentMediumId
	 * @return bool
	 */
	public function methodExists( $intPaymentTypeId, $intPaymentMediumId = NULL ) {
		$intPaymentMediumId = $intPaymentMediumId ?: $this->getPaymentMediumId() ?: CPaymentMedium::WEB;
		if( !$this->checkPaymentMediumId( $intPaymentMediumId ) ) return NULL;
		if( !$this->checkPaymentTypeId( $intPaymentTypeId ) ) return NULL;
		if( !valArr( $arrobjMethods = $this->getMerchantMethods() ) ) $this->loadMethods();
		if( !valArr( $arrobjMethodTiers = $arrobjMethods[$intPaymentMediumId] ?? [] ) ) return NULL;

		// Go through the tiers for the selected medium, returning as soon as we find a method the selected payment type
		foreach( $arrobjMethodTiers as $intTier => $arrobjMethodTier ) {
			$objMethod = $arrobjMethodTier[$intPaymentTypeId] ?? NULL;
			if( $objMethod ) return true;
		}

		// No methods for the selected medium and payment type were found
		return NULL;
	}

	/**
	 * @param $intPaymentTypeId
	 * @param $intPaymentMediumId
	 * @return bool
	 */
	public function methodIsEnabled( $intPaymentTypeId, $intPaymentMediumId = NULL ) {
		return !is_null( $this->getMethodByAmount( $intPaymentTypeId, 0, $intPaymentMediumId ) );
	}

	public function collectMethodErrors() {
		if( !valArr( $arrobjMethods = $this->getMerchantMethods() ) ) return;

		array_walk_recursive( $arrobjMethods, function( $objMethod ) {
			$arrobjErrorMsgs = array_map( function( CErrorMsg $objErrorMsg ) use( $objMethod ) {
				$objNewErrorMsg = clone $objErrorMsg;
				$objNewErrorMsg->setField( implode( '.', [ 'merchant_account_methods', $objMethod->getPaymentMediumId(), $objMethod->getTier(), $objMethod->getPaymentTypeId(), $objErrorMsg->getField() ] ) );
				return $objNewErrorMsg;
			}, $objMethod->getErrorMsgs() );
			$this->addErrorMsgs( $arrobjErrorMsgs );
		} );
	}

	// endregion

	// region Merchant Method Tier Operations

	public function addTier( $intPaymentMediumId ) {
		$this->m_arrobjMerchantMethods[$intPaymentMediumId][] = [];
		$this->populateMissingMethods();
	}

	public function deleteTier( $intPaymentMediumId ) {
		if( 1 >= count( $this->getMerchantMethods()[$intPaymentMediumId] ?? [] ) ) return NULL;
		return array_pop( $this->m_arrobjMerchantMethods[$intPaymentMediumId] );
	}

	public function populateMissingMethods() {
		$arrintPaymentMediums = [ CPaymentMedium::WEB, CPaymentMedium::RECURRING, CPaymentMedium::TERMINAL ];

		foreach( $arrintPaymentMediums as $intPaymentMedium ) {
			if( !valArr( $this->m_arrobjMerchantMethods[$intPaymentMedium] ?? [] ) ) $this->m_arrobjMerchantMethods[$intPaymentMedium] = [ 1 => [] ];
			foreach( $this->m_arrobjMerchantMethods[$intPaymentMedium] as $intTier => $arrobjMerchantAccountMethods ) {
				foreach( CPaymentType::$c_arrintMerchantAccountPaymentTypeIds as $intPaymentTypeId ) {
					if( !isset( $arrobjMerchantAccountMethods[$intPaymentTypeId] ) ) {
						$objMerchantAccountMethod = $this->createMethod();
						$objMerchantAccountMethod->setCid( $this->getCid() );
						$objMerchantAccountMethod->setCompanyMerchantAccountId( $this->getId() );
						$objMerchantAccountMethod->setPaymentMediumId( $intPaymentMedium );
						$objMerchantAccountMethod->setTier( $intTier );
						$objMerchantAccountMethod->setPaymentTypeId( $intPaymentTypeId );
						$objMerchantAccountMethod->applyDefaults();
						$this->m_arrobjMerchantMethods[$intPaymentMedium][$intTier][$intPaymentTypeId] = $objMerchantAccountMethod;
					}
				}
			}
		}
	}

	public function pruneMethods( $arrintEnabledGatewayTypeIds ) {
		$arrmixGatewayToPaymentMap = CPaymentType::$c_arrmixPaymentTypesByGatewayTypeId;
		$arrintAvailablePaymentTypes = [];
		foreach( $arrintEnabledGatewayTypeIds as $intGatewayTypeId ) {
			$arrintAvailablePaymentTypes = array_merge( $arrintAvailablePaymentTypes, $arrmixGatewayToPaymentMap[$intGatewayTypeId] );
		}

		foreach( $this->m_arrobjMerchantMethods as $intPaymentMediumId => $arrobjTiers ) {
			foreach( $arrobjTiers as $intTier => $arrobjTier ) {
				foreach( $arrobjTier as $intPaymentTypeId => $objMethod ) {
					if( !in_array( $intPaymentTypeId, $arrintAvailablePaymentTypes ) ) {
						unset( $this->m_arrobjMerchantMethods[$intPaymentMediumId][$intTier][$intPaymentTypeId] );
					}
				}
			}
		}
	}

	// endregion

	// region Merchant Gateway Operations

	// region Generic Accessors

	public function getGatewayUsername( $intPaymentTypeId ) {
		$objMethod = $this->getMethodByAmount( $intPaymentTypeId );
		return $objMethod ? $objMethod->getGatewayUsername() : NULL;
	}

	public function getGatewayPassword( $intPaymentTypeId ) {
		$objMethod = $this->getMethodByAmount( $intPaymentTypeId );
		return $objMethod ? $objMethod->getGatewayPassword() : NULL;
	}

	public function getLoginUsername( $intPaymentTypeId ) {
		$objMethod = $this->getMethodByAmount( $intPaymentTypeId );
		return $objMethod ? $objMethod->getLoginUsername() : NULL;
	}

	public function getLoginPassword( $intPaymentTypeId ) {
		$objMethod = $this->getMethodByAmount( $intPaymentTypeId );
		return $objMethod ? $objMethod->getLoginPassword() : NULL;
	}

	// endregion

	// region Credit Card

	public function getCcMerchantGatewayId() {
		return ( $this->methodIsEnabled( CPaymentType::VISA ) ) ? $this->getMethodByAmount( CPaymentType::VISA )->getMerchantGatewayId() : NULL;
	}

	public function getCcCutOffTime() {
		return $this->getMethodByAmount( CPaymentType::MASTERCARD )->getCutOffTime();
	}

	public function getCcGatewayUsername() {
		return $this->getGatewayUsername( CPaymentType::VISA );
	}

	public function getCcGatewayPassword() {
		return $this->getGatewayPassword( CPaymentType::VISA );
	}

	public function getCcLoginUsername() {
		return $this->getLoginUsername( CPaymentType::VISA );
	}

	public function getCcLoginPassword() {
		return $this->getLoginPassword( CPaymentType::VISA );
	}

	public function getCcMinimumConvenienceFee() {
		return ( $this->methodIsEnabled( CPaymentType::VISA ) ) ? $this->getMethodByAmount( CPaymentType::VISA )->getMinimumConvenienceFee() : 0.00;
	}

	// endregion

	// region ACH

	public function getAchGatewayUsername() {
		return $this->getGatewayUsername( CPaymentType::ACH );
	}

	public function getAchGatewayPassword() {
		return $this->getGatewayPassword( CPaymentType::ACH );
	}

	public function getAchLoginUsername() {
		return $this->getLoginUsername( CPaymentType::ACH );
	}

	public function getAchLoginPassword() {
		return $this->getLoginPassword( CPaymentType::ACH );
	}

	// endregion

	// region Check 21

	public function getCh21GatewayUsername() {
		return $this->getGatewayUsername( CPaymentType::CHECK_21 );
	}

	public function getCh21GatewayPassword() {
		return $this->getGatewayPassword( CPaymentType::CHECK_21 );
	}

	public function getCh21LoginUsername() {
		return $this->getLoginUsername( CPaymentType::CHECK_21 );
	}

	public function getCh21LoginPassword() {
		return $this->getLoginPassword( CPaymentType::CHECK_21 );
	}

	// endregion

	// region EMoney Order

	public function getEmoGatewayUsername() {
		return $this->getGatewayUsername( CPaymentType::EMONEY_ORDER );
	}

	public function getEmoGatewayPassword() {
		return $this->getGatewayPassword( CPaymentType::EMONEY_ORDER );
	}

	public function getEmoLoginUsername() {
		return $this->getLoginUsername( CPaymentType::EMONEY_ORDER );
	}

	public function getEmoLoginPassword() {
		return $this->getLoginPassword( CPaymentType::EMONEY_ORDER );
	}

	// endregion

	// region Validation

	public function validateMethods() {
		$boolIsValid = true;
		if( 0 == $this->getIsOverrideFeeAmountValidation() ) {
			$arrobjMerchantAccountMethods = $this->getMerchantMethods();
			array_walk_recursive( $arrobjMerchantAccountMethods, function( CMerchantAccountMethod $objMethod ) use( &$boolIsValid ) {
				if( CMerchantProcessingType::ACCOUNTS_PAYABLE == $this->getMerchantProcessingTypeId() && CPaymentType::ACH != $objMethod->getPaymentTypeId() ) return;
				$boolIsValid &= $objMethod->validate( $this->getClient(), $this->getMerchantProcessingTypeId(), $this->getCountryCode(), $this->getEnableSeparateDebitPricing(), $this->getEnableVisaPilot() );
			} );
		}
		return $boolIsValid;
	}

	public function checkPaymentMediumId( $intPaymentMediumId ) {
		if( !in_array( $intPaymentMediumId, [ CPaymentMedium::WEB, CPaymentMedium::RECURRING, CPaymentMedium::TERMINAL ] ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Invalid payment medium ID.' ) );
			return false;
		}
		return true;
	}

	public function checkPaymentTypeId( $intPaymentTypeId ) {
		if( !in_array( $intPaymentTypeId, CPaymentType::$c_arrintMerchantAccountPaymentTypeIds ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Invalid payment type ID for merchant account.' ) );
			return false;
		}
		return true;
	}

	// endregion

}
