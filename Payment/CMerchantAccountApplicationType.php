<?php

class CMerchantAccountApplicationType extends CBaseMerchantAccountApplicationType {

	const STANDARD_UNDERWRITING_APPLICATION  	= 1;
	const BILLPAY_UNDERWRITING_APPLICATION    	= 2;
	const COMMERCIAL_UNDERWRITING_APPLICATION 	= 3;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>