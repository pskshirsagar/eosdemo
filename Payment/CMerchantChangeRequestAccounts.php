<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMerchantChangeRequestAccounts
 * Do not add any new functions to this class.
 */

class CMerchantChangeRequestAccounts extends CBaseMerchantChangeRequestAccounts {

	public static function fetchMerchantChangeRequestAccountsByMerchantChangeRequestId( $intMerchantChangeRequestId, $objDatabase, $strOrderBy = NULL ) {

		$strOrderBySubSql = '';

		if( true == valStr( $strOrderBy ) ) {
			$strOrderBy = $strOrderBy;
		} else {
			$strOrderBy = 'account_name';
		}

		$strSql = 'SELECT * FROM merchant_change_request_accounts WHERE merchant_change_request_id = ' . ( int ) $intMerchantChangeRequestId . ' ORDER BY ' . $strOrderBy;
		return self::fetchMerchantChangeRequestAccounts( $strSql, $objDatabase );
	}

	public static function fetchUnAssociatedMerchantChangeRequestAccountsByMerchantChangeRequestId( $intMerchantChangeRequestId, $objDatabase ) {

		$strSql = 'SELECT * FROM merchant_change_request_accounts WHERE merchant_change_request_account_id IS NULL and merchant_change_request_id = ' . ( int ) $intMerchantChangeRequestId . ' ORDER BY account_name';
		return self::fetchMerchantChangeRequestAccounts( $strSql, $objDatabase );
	}

	public static function fetchParentMerchantChangeRequestAccountsByMerchantChangeRequestId( $intMerchantChangeRequestId, $objDatabase ) {

		$strSql = 'SELECT * FROM merchant_change_request_accounts WHERE merchant_change_request_account_id IS NULL AND merchant_change_request_id = ' . ( int ) $intMerchantChangeRequestId . ' ORDER BY merchant_processing_type_id';
		return self::fetchMerchantChangeRequestAccounts( $strSql, $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountsDataByMerchantChangeRequestIds( $arrintMerchantChangeRequestIds, $objDatabase ) {
		if( false == valArr( $arrintMerchantChangeRequestIds ) )  return false;

		$strSql = 'SELECT merchant_change_request_id,is_new_account_request FROM merchant_change_request_accounts WHERE merchant_change_request_id IN ( ' . implode( ',', $arrintMerchantChangeRequestIds ) . ' )';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountsNamesByMerchantChangeRequestAccountId( $intAccountId, $objDatabase ) {
		$strSql = 'SELECT 
						company_merchant_account_id,
						account_name
					FROM 
						merchant_change_request_accounts 
					WHERE 
						merchant_change_request_account_id = ' . ( int ) $intAccountId;

		$arrstrAccountNames = fetchData( $strSql, $objDatabase );

		$arrintNewAccounts = [];
		if( true == valArr( $arrstrAccountNames ) ) {
			foreach( $arrstrAccountNames as $strKey => $arrstrAccountName ) {
				$arrintNewAccounts[$arrstrAccountName['company_merchant_account_id']] = $arrstrAccountName['account_name'];
			}
		}

		return $arrintNewAccounts;
	}

	public static function fetchMerchantChangeRequestAccountByMerchantChangeRequestId( $intMerchantChangeRequestId, $objDatabase ) {

		$strSql = 'SELECT * FROM merchant_change_request_accounts WHERE merchant_change_request_account_id = ' . ( int ) $intMerchantChangeRequestId . ' LIMIT 1';
		return self::fetchMerchantChangeRequestAccount( $strSql, $objDatabase );
	}

	public static function fetchMerchantChangeRequestAccountsByCidByMerchantProcessingTypeId( $intCid, $intMerchantProcessingTypeId, $objDatabase ) {
		$strSql = 'SELECT * FROM merchant_change_request_accounts WHERE cid = ' . ( int ) $intCid . ' AND merchant_processing_type_id = ' . ( int ) $intMerchantProcessingTypeId;
		return self::fetchMerchantChangeRequestAccounts( $strSql, $objDatabase );
	}

}
?>