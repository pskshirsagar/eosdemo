<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CBatchedArPayments
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CBatchedArPayments extends CBaseBatchedArPayments {

	public static function fetchBatchedArPaymentsByEftBatchId( $intEftBatchId, $objDatabase ) {

		// $strSql = 'SELECT * FROM batched_ar_payments WHERE eft_batch_id = ' . ( int ) $intEftBatchId;
		$strSql = 'SELECT * FROM batched_ar_payments WHERE eft_batch_id = ' . ( int ) $intEftBatchId
				. ' UNION '
				. ' SELECT bap.* FROM batched_ar_payments bap '
				. ' INNER JOIN eft_batches eb ON eb.id = bap.eft_batch_id '
				. ' WHERE eb.aggregate_eft_batch_id = ' . ( int ) $intEftBatchId;

		return self::fetchBatchedArPayments( $strSql, $objDatabase );
	}

	public static function fetchBatchedArPaymentsByArPaymentId( $intArPaymentId, $objDatabase ) {

		$strSql = 'SELECT * FROM batched_ar_payments WHERE ar_payment_id = ' . ( int ) $intArPaymentId;
		return self::fetchBatchedArPayments( $strSql, $objDatabase );
	}

}

?>