<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CEftCharges
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CEftCharges extends CBaseEftCharges {

	public static function fetchEftChargesByChargeCodeIdsByArPaymentId( $arrintChargeCodeIds, $intArPaymentId, $objDatabase ) {

		if( false == valArr( $arrintChargeCodeIds ) )
			return NULL;

		$strSql = 'SELECT
						ec.*
					FROM
						eft_charges ec
					WHERE
						ec.ar_payment_id = ' . ( int ) $intArPaymentId . '
						AND ec.charge_code_id IN ( ' . implode( ',', $arrintChargeCodeIds ) . ' )';

		return self::fetchEftCharges( $strSql, $objDatabase );
	}

	public static function fetchPaginatedEftChargesByCid( $intCid, $objPagination, $objDatabase ) {
		$strSql = 'SELECT
						ec.*
					FROM
						eft_charges ec
					WHERE
						ec.cid = ' . ( int ) $intCid . '
						ORDER BY ec.id DESC
						OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();

		return self::fetchEftCharges( $strSql, $objDatabase );
	}

	public static function fetchEftChargesCountByCid( $intCid, $objDatabase ) {

		$strSql = 'select count( id ) AS count FROM eft_charges WHERE cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchEftChargesByIds( $arrintEftChargeIds, $objDatabase ) {
		if( false == valArr( $arrintEftChargeIds ) ) return NULL;
		return self::fetchEftCharges( 'SELECT * FROM eft_charges WHERE id IN ( ' . implode( ',', $arrintEftChargeIds ) . ' ) ', $objDatabase );
	}

	public static function fetchEftChargeByCidById( $intCid, $intId, $objDatabase ) {
    	$strSql = 'SELECT * FROM eft_charges WHERE cid = ' . ( int ) $intCid . ' AND id = ' . ( int ) $intId;
        return self::fetchEftCharge( $strSql, $objDatabase );
    }

	public static function fetchEftChargeById( $intId, $objDatabase ) {
    	$strSql = 'SELECT * FROM eft_charges WHERE id = ' . ( int ) $intId;
        return self::fetchEftCharge( $strSql, $objDatabase );
    }

	public static function fetchPaginatedEftChargesByAccountId( $intAccountId, $objPagination, $objDatabase ) {
		$strSql = 'SELECT
						ec.*
					FROM
						eft_charges ec
					WHERE
						ec.account_id = ' . ( int ) $intAccountId . '
						ORDER BY ec.id DESC
						OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();

		return self::fetchEftCharges( $strSql, $objDatabase );
	}

	public static function fetchEftChargesCountByAccountId( $intAccountId, $objDatabase ) {

		$strSql = 'select count( id ) AS count FROM eft_charges WHERE account_id = ' . ( int ) $intAccountId;

		return self::fetchColumn( $strSql, 'count', $objDatabase );

	}

	public static function fetchEftChargesCountByTransactionIds( $intCid, $arrintTransactionIds, $objDatabase ) {

		$strSql = 'select count( id ) AS count FROM eft_charges WHERE cid = ' . ( int ) $intCid . ' AND transaction_id IN ( ' . implode( ',', $arrintTransactionIds ) . ' ) ';

		$intCount = self::fetchColumn( $strSql, 'count', $objDatabase );

		return $intCount;
	}

	public static function fetchEftChargesByTransactionIds( $intCid, $arrintTransactionIds, $objDatabase ) {

		$strSql = 'SELECT * FROM eft_charges WHERE cid = ' . ( int ) $intCid . ' AND transaction_id IN ( ' . implode( ',', $arrintTransactionIds ) . ' ) ORDER BY transaction_id, id DESC';

		return self::fetchEftCharges( $strSql, $objDatabase );
	}

	public static function fetchEftChargesWithInvoiceIdByCidAndMonth( $intCid, $strMonth, $objPaymentDatabase, $objAdminDatabase ) {
		$arrEftChargesInfo = array();

		// Get all invoice id's for this client and the supplied month.
		$strSql = 'SELECT
						t.*
						FROM
						invoices i, invoice_batches ib, transactions t
						WHERE ib.id = i.invoice_batch_id
						AND i.id = t.invoice_id
						AND date_trunc( \'month\', ib.batch_datetime ) = date_trunc(\'month\', date \'' . $strMonth . '\')
						AND i.cid = ' . ( int ) $intCid . '
						AND t.charge_code_id in ( ' . implode( ',', CChargeCode::$c_arrintChargeCodesWithDetail ) . ' )
						ORDER BY i.id, t.id;';

		// die($strSql);
		$arrobjTransactions = fetchData( $strSql, $objAdminDatabase );

		$strSql = 'SELECT * FROM charge_codes WHERE id in ( ' . implode( ',', CChargeCode::$c_arrintChargeCodesWithDetail ) . ' );';
		$arrobjChargeCodes = fetchData( $strSql, $objAdminDatabase );

		$strSQLChargeCodeCase = ' CASE ';
		foreach( $arrobjChargeCodes as $objChargeCode ) {
			$strSQLChargeCodeCase .= ' WHEN ec.charge_code_id = ' . $objChargeCode['id'] . ' THEN \' ' . $objChargeCode['name'] . '\' ';
		}
		$strSQLChargeCodeCase .= ' END AS "Charge Code Name", ';

		$arrintTransactionIds = array();
		$intCurrentInvoiceId = 0;

		if( valArr( $arrobjTransactions ) ) {
			// Loop through each transaction.
			foreach( $arrobjTransactions as $objTransaction ) {
				$intInvoiceId = $objTransaction['invoice_id'];

				if( $intCurrentInvoiceId != $intInvoiceId ) {
					// Get EFT Charges for the current invoice.
					$arrEftCharges = array();
					if( 0 < \Psi\Libraries\UtilFunctions\count( $arrintTransactionIds ) ) {
						$strSql = 'SELECT to_char(charge_datetime, \'MM/DD/YYYY\') as "Date",
										' . ( int ) $intCurrentInvoiceId . ' as "Invoice Id", transaction_id as "Transaction Id", ' . $strSQLChargeCodeCase . ' memo as "Memo", charge_amount as "Amount"
										FROM eft_charges ec
										WHERE ec.cid = ' . ( int ) $intCid . '
										AND ec.transaction_id IN ( ' . implode( ',', $arrintTransactionIds ) . ' )
										ORDER BY ec.transaction_id, id DESC';

						$arrEftCharges = fetchData( $strSql, $objPaymentDatabase );
					}

					// Add records to $arrEftChargesInfo
					$arrEftChargesInfo = array_merge( $arrEftChargesInfo, $arrEftCharges );
					// $arrEftChargesInfo = $arrEftCharges;

					// Reset parameters
					$arrintTransactionIds = array();
					$intCurrentInvoiceId = $intInvoiceId;
				}
				$arrintTransactionIds[] = $objTransaction['id'];
			}
		}

		// Return the array.
		return $arrEftChargesInfo;
	}

	public static function updateEftChargesChargeAmountByArPaymentIdByEftChargeTypeId( $intUserId, $fltChargeAmount, $arrintArPaymentIds, $intEftChargeTypeId, $objDatabase ) {

		$strSql = 'UPDATE
						eft_charges
					SET
						charge_amount = ' . $fltChargeAmount . ',
						memo = memo || \' discounted to ' . ( string ) $fltChargeAmount . '\',
						updated_by = ' . ( int ) $intUserId . ',
						updated_on = NOW()
					WHERE
						ar_payment_id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' )
						AND eft_charge_type_id = ' . ( int ) $intEftChargeTypeId;

		if( false == fetchData( $strSql, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public static function fetchEftChargeDetailsByTransactionIds( $arrintTransactionIds, $objDatabase ) {
		$strSql = '
			SELECT
				array_to_string(array_agg( ec.id || \'^\' || e.remote_payment_number || \'^\' || coalesce( ec.charge_amount, 0.0 ) || \'^\' || ec.charge_code_id || \'^\' || ec.transaction_id ), \'~\' ) as payment_details
			FROM
				eft_charges ec
				INNER JOIN eft_instructions ei ON ei.id = ec.eft_instruction_id
				INNER JOIN efts e ON e.id = ei.eft_id
			WHERE
				ec.transaction_id IN ( ' . implode( ',', $arrintTransactionIds ) . ' );';

		return fetchData( $strSql, $objDatabase );
	}

}
