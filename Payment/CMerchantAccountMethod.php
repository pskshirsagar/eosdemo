<?php

use Psi\Libraries\Cryptography\Exceptions\CCryptoErrorException;
use Psi\Libraries\Cryptography\Exceptions\CCryptoInvalidException;

class CMerchantAccountMethod extends CBaseMerchantAccountMethod {

	use TMerchantMethod;

	// region Constants

	const INTERNATIONAL_CC_MANAGEMENT_FEE = 0.50;

	static $c_arrintTerminalOnlyPaymentTypes = [
		CPaymentType::CHECK_21,
		CPaymentType::EMONEY_ORDER
	];

	static $c_arrintRequiresNonzeroProviderDiscountRate = [
		CPaymentType::VISA,
		CPaymentType::MASTERCARD,
		CPaymentType::DISCOVER
	];

	static $c_arrintRequiresZeroProviderDiscountRate = [
		CPaymentType::AMEX,
		CPaymentType::CHECK_21,
		CPaymentType::EMONEY_ORDER
	];

	static $c_arrintZeroRate = [
		CPaymentType::ACH,
		CPaymentType::CHECK_21,
		CPaymentType::EMONEY_ORDER
	];

	const MAX_ID_LINDON = 99999999;  // 100 million - 1
	const MIN_ID_IRELAND = 100000000; // 100 million
	const MAX_ID_IRELAND = 199999999; // 200 million - 1

	// endregion

	private $m_intTier;

	public function __construct() {
		parent::__construct();

		$this->setWaivedDebitDiscountRate( 0 );
	}

	// region Additional Accessors

	// region Tier

	public function getTier() {
		return $this->m_intTier;
	}

	public function setTier( $intTier ) {
		$this->m_intTier = ( int ) $intTier;
		return $this;
	}

	// endregion

	/**
	 * @param $arrmixValues
	 * @param bool $boolStripSlashes
	 * @param bool $boolDirectSet
	 * @throws CCryptoErrorException
	 * @throws CCryptoInvalidException
	 */
	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrmixValues['gateway_username'] ) ) $this->setGatewayUsername( $arrmixValues['gateway_username'] );
		if( isset( $arrmixValues['gateway_password'] ) ) $this->setGatewayPassword( $arrmixValues['gateway_password'] );
		if( isset( $arrmixValues['login_username'] ) ) $this->setLoginUsername( $arrmixValues['login_username'] );
		if( isset( $arrmixValues['login_password'] ) ) $this->setLoginPassword( $arrmixValues['login_password'] );

		// Worldpay
		if( isset( $arrmixValues['worldpay_cc_recurring_username'] ) ) $this->setWorldpayCcRecurringUsername( $arrmixValues['worldpay_cc_recurring_username'] );
		if( isset( $arrmixValues['worldpay_cc_recurring_password'] ) ) $this->setWorldpayCcRecurringPassword( $arrmixValues['worldpay_cc_recurring_password'] );
		if( isset( $arrmixValues['worldpay_cc_recurring_merchant_code'] ) ) $this->setWorldpayCcRecurringMerchantCode( $arrmixValues['worldpay_cc_recurring_merchant_code'] );
		if( isset( $arrmixValues['worldpay_cc_moto_username'] ) ) $this->setWorldpayCcMotoUsername( $arrmixValues['worldpay_cc_moto_username'] );
		if( isset( $arrmixValues['worldpay_cc_moto_password'] ) ) $this->setWorldpayCcMotoPassword( $arrmixValues['worldpay_cc_moto_password'] );
		if( isset( $arrmixValues['worldpay_cc_moto_merchant_code'] ) ) $this->setWorldpayCcMotoMerchantCode( $arrmixValues['worldpay_cc_moto_merchant_code'] );
		if( isset( $arrmixValues['worldpay_cc_ecom_username'] ) ) $this->setWorldpayCcEcomUsername( $arrmixValues['worldpay_cc_ecom_username'] );
		if( isset( $arrmixValues['worldpay_cc_ecom_password'] ) ) $this->setWorldpayCcEcomPassword( $arrmixValues['worldpay_cc_ecom_password'] );
		if( isset( $arrmixValues['worldpay_cc_ecom_merchant_code'] ) ) $this->setWorldpayCcEcomMerchantCode( $arrmixValues['worldpay_cc_ecom_merchant_code'] );
		if( isset( $arrmixValues['worldpay_recurring_password'] ) ) $this->setWorldpayRecurringPassword( $arrmixValues['worldpay_recurring_password'] );
		if( isset( $arrmixValues['worldpay_moto_password'] ) ) $this->setWorldpayMotoPassword( $arrmixValues['worldpay_moto_password'] );
		if( isset( $arrmixValues['worldpay_ecom_password'] ) ) $this->setWorldpayEcomPassword( $arrmixValues['worldpay_ecom_password'] );

		// SEPA
		if( isset( $arrmixValues['sepa_creditor_id'] ) ) $this->setSepaCreditorId( $arrmixValues['sepa_creditor_id'] );
		if( isset( $arrmixValues['sepa_bic'] ) ) $this->setSepaBic( $arrmixValues['sepa_bic'] );
		if( isset( $arrmixValues['sepa_company_address'] ) ) $this->setSepaCompanyAddress( $arrmixValues['sepa_company_address'] );
		if( isset( $arrmixValues['sepa_iban'] ) ) $this->setSepaIban( $arrmixValues['sepa_iban'] );
		if( isset( $arrmixValues['sepa_company_name'] ) ) $this->setSepaCompanyName( $arrmixValues['sepa_company_name'] );

		// Tiers
		if( isset( $arrmixValues['require_business_account'] ) ) $this->setRequireBusinessAccount( $arrmixValues['require_business_account'] );
		if( isset( $arrmixValues['tier'] ) ) $this->setTier( $arrmixValues['tier'] );

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

	// endregion

	// region Validation

	public function valMaxPaymentAmount() {
		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( true == is_null( $this->getMaxPaymentAmount() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_payment_amount', 'Maximum payment amount for ' . $strFormattedName . ' is required when ' . $strFormattedName . ' is enabled.' ) );
			return false;
		}

		if( 0 >= $this->getMaxPaymentAmount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_payment_amount', 'Maximum payment amount for ' . $strFormattedName . ' must contain a positive value.' ) );
			return false;
		}

		return true;
	}

	public function valMaxMonthlyProcessAmount() {
		if( true == is_null( $this->getMaxMonthlyProcessAmount() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_monthly_process_amount', 'Maximum processing volume is required.' ) );
			return false;
		}

		if( 0 >= $this->getMaxMonthlyProcessAmount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_monthly_process_amount', 'Maximum processing volume must contain a positive value.' ) );
			return false;
		}

		return true;
	}

	public function valGatewayTransactionFee() {
		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( 0 == $this->getIsEnabled() ) {
			return true;
		}

		if( NULL == $this->getGatewayTransactionFee() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_transaction_fee', 'Gateway transaction fee for ' . $strFormattedName . ' is required when ' . $strFormattedName . ' is enabled.' ) );
			return false;
		}

		if( 0 >= $this->getGatewayTransactionFee() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_transaction_fee', 'Gateway transaction fee for ' . $strFormattedName . ' must contain a positive value.' ) );
			return false;
		}

		return true;
	}

	public function valResidentTransactionFee() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		// Terminal ACH payments should not have rates.  Enforced to zero in CCOmpanyMerchantAccount::enforceNullRates
		if( CPaymentType::ACH == $this->getPaymentTypeId() && CPaymentMedium::TERMINAL == $this->getPaymentMediumId() ) {
			return true;
		}

		if( true == in_array( $this->getPaymentTypeId(), self::$c_arrintTerminalOnlyPaymentTypes ) ) {
			if( false == is_null( $this->getResidentTransactionFee() ) && 0 < $this->getResidentTransactionFee() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_transaction_fee', $strFormattedName . ' does not allow residents to pay transaction fees.' ) );
			}
		} else {
			if( ( true == is_null( $this->getResidentTransactionFee() ) || 0 == $this->getResidentTransactionFee() ) && ( true == is_null( $this->getTransactionFee() ) || 0 == $this->getTransactionFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_transaction_fee', 'Either the resident or the client must be charged a transaction fee for each ' . $strFormattedName . ' transaction.' ) );
			}

			if( false == is_null( $this->getResidentTransactionFee() ) && 10 < $this->getResidentTransactionFee() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_transaction_fee', $strFormattedName . ' Resident transaction fee cannot exceed $10.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valWaivedTransactionFee() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		// only ACH and CC
		if( false == in_array( $this->getPaymentTypeId(), array_merge( [ CPaymentType::ACH ], CPaymentType::$c_arrintCreditCardPaymentTypes ) ) ) {
			return true;
		}

		if( true == is_null( $this->getWaivedTransactionFee() ) || 0 == $this->getWaivedTransactionFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'waived_transaction_fee', $strFormattedName . ' Waived Transaction Fee must be set.' ) );
		}

		if( 10 < $this->getWaivedTransactionFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'waived_transaction_fee', $strFormattedName . ' Waived Transaction Fee must be less than $10.' ) );
		}

		return $boolIsValid;
	}

	public function valWaivedDebitTransactionFee() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( false == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) ) {
			return true;
		}

		// doesn't apply if resident fee is 0
		if( false == $this->hasResidentDebitFee() ) {
			return true;
		}

		if( true == is_null( $this->getWaivedTransactionFee() ) || 0 == $this->getWaivedTransactionFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'waived_debit_transaction_fee', $strFormattedName . ' Waived Debit Transaction Fee must be set.' ) );
		}

		if( 10 < $this->getWaivedDebitTransactionFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'waived_debit_transaction_fee', $strFormattedName . ' Waived Debit Transaction Fee must be less than $10.' ) );
		}

		return $boolIsValid;
	}

	public function valRebateTransactionFee( $intMerchantProcessingTypeId ) {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( CPaymentMedium::TERMINAL == $this->getPaymentMediumId() || true == in_array( $this->getPaymentTypeId(), self::$c_arrintTerminalOnlyPaymentTypes ) ) {
			return true;
		}

		if( CMerchantProcessingType::STANDARD == $intMerchantProcessingTypeId && $this->getRebateTransactionFee() > $this->getResidentTransactionFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rebate_transaction_fee', $strFormattedName . ' Rebate Fee must be less than or equal to Resident Transaction Fee.' ) );
		} elseif( CMerchantProcessingType::CLEARING == $intMerchantProcessingTypeId && $this->getRebateTransactionFee() > $this->getTransactionFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rebate_transaction_fee', $strFormattedName . ' Rebate Fee must be less than or equal to Company Transaction Fee.' ) );
		}

		return $boolIsValid;
	}

	public function valReverseTransactionFee() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( 25 < $this->getReverseTransactionFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reverse_transaction_fee', $strFormattedName . ' Reverse Transaction fee cannot exceed $25' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyAdjustmentFee() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( CPaymentType::CHECK_21 != $this->getPaymentTypeId() ) {
			return true;
		}

		if( 0 == $this->getCompanyAdjustmentFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_adjustment_fee', $strFormattedName . ' Company Adjustment fee must contain a value greater than 0.' ) );
		}

		if( 35 < $this->getCompanyAdjustmentFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_adjustment_fee', $strFormattedName . ' Company Adjustment fee cannot exceed $15.' ) );
		}

		if( 0 > $this->getCompanyAdjustmentFee() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_adjustment_fee', $strFormattedName . ' Company Adjustment fee cannot contain a negative value.' ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valCompanyReturnFee() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( false == in_array( $this->getPaymentTypeId(), array_merge( [ CPaymentType::ACH, CPaymentType::CHECK_21 ], CPaymentType::$c_arrintElectronicPaymentTypeIds ) ) ) {
			return true;
		}

		// Either the resident or company return fee must be set, NOT both
		if( 0 < $this->getCompanyReturnFee() && 0 < $this->getResidentReturnFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_return_fee', $strFormattedName . ' You cannot set values for both resident and company return fees. Only one must be set.' ) );
		}

		// IF Check 21 we are requiring that it contains a value
		if( CPaymentType::CHECK_21 == $this->getPaymentTypeId() && 0 == $this->getCompanyReturnFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_return_fee', $strFormattedName . ' Company Return fee must contain a value greater than 0.' ) );
		}

		if( true == in_array( $this->getPaymentTypeId(), [ CPaymentType::ACH, CPaymentType::CHECK_21 ] ) && 15 < $this->getCompanyReturnFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_return_fee', $strFormattedName . ' Company Return fee cannot exceed $15.' ) );
		}

		if( true == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) && 25 < $this->getCompanyReturnFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_return_fee', $strFormattedName . ' Company Return fee cannot exceed $25.' ) );
		}

		if( 0 > $this->getCompanyReturnFee() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_return_fee', $strFormattedName . ' Company Return fee cannot contain a negative value.' ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valResidentReturnFee() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( CPaymentType::ACH != $this->getPaymentTypeId() || true == is_null( $this->getResidentReturnFee() ) ) {
			return true;
		}

		// Either the resident or company return fee must be set, NOT both
		if( 0 < $this->getCompanyReturnFee() && 0 < $this->getResidentReturnFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_return_fee', $strFormattedName . ' You cannot set values for both resident and company return fees. Only one must be set.' ) );
		}

		if( 0 < $this->getResidentReturnFee() && 0 == $this->getResidentTransactionFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_return_fee', 'You cannot enter an ACH transaction return fee unless the resident is paying some cost of the ACH transaction.' ) );
		}

		if( 35 < $this->getResidentReturnFee() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_return_fee', $strFormattedName . ' Resident Return fee cannot exceed $35.' ) );
			$boolIsValid = false;
		}

		if( 0 > $this->getResidentReturnFee() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_return_fee', $strFormattedName . ' Resident Return fee cannot contain a negative value.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valUnauthorizedReturnFee() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( CPaymentType::ACH != $this->getPaymentTypeId() || true == is_null( $this->getUnauthorizedReturnFee() ) ) {
			return true;
		}

		if( 0 != $this->getUnauthorizedReturnFee() && 10 != $this->getUnauthorizedReturnFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unauthorized_return_fee', $strFormattedName . ' Unauthorized Return fee must either be $0 or $10.' ) );
		}

		return $boolIsValid;
	}

	public function valAuthFailureFee() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( false == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) || true == is_null( $this->getAuthFailureFee() ) ) {
			return true;
		}

		// ONLY applies if resident fee is 0 and client fee > 0
		// credit or debit

		$boolHasClientFee = $this->hasCompanyFee() || $this->hasCompanyDebitFee();
		$boolHasResidentFee = $this->hasResidentFee() || $this->hasResidentDebitFee();

		if( true == $boolHasResidentFee || false == $boolHasClientFee ) {
			return true;
		}

		if( .48 != $this->getAuthFailureFee() && 0 != $this->getAuthFailureFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'auth_failure_fee', $strFormattedName . ' Auth Failure fee must either be $0.48 or $0.' ) );
		}

		return $boolIsValid;
	}

	public function valProviderDiscountRate() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( 0 == $this->getIsEnabled() ) {
			return true;
		}

		if( NULL == $this->getProviderDiscountRate() && true == in_array( $this->getPaymentTypeId(), self::$c_arrintRequiresNonzeroProviderDiscountRate ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'provider_discount_rate', 'Provider discount rate for ' . $strFormattedName . ' is required when ' . $strFormattedName . ' is enabled.' ) );
			return false;
		}

		if( NULL != $this->getProviderDiscountRate() ) {

			if( 0 < $this->getProviderDiscountRate() && true == in_array( $this->getPaymentTypeId(), self::$c_arrintRequiresZeroProviderDiscountRate ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'provider_discount_rate', 'Provider discount rate for ' . $strFormattedName . ' should always be set to zero.' ) );
				return false;
			}

			$fltCalcDiscountRate = $this->getCompanyDiscountRate() + $this->getResidentDiscountRate();

			if( 0 < $this->getMaxPaymentAmount() ) {
				$fltCalcDiscountRate += $this->getTransactionFee() / $this->getMaxPaymentAmount();
				$fltCalcDiscountRate += $this->getResidentTransactionFee() / $this->getMaxPaymentAmount();
			}

			if( $this->getProviderDiscountRate() > $fltCalcDiscountRate ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'provider_discount_rate', 'Provider discount rate for ' . $strFormattedName . ' is greater than the combined discount rate. To correct this problem increase the ' . $strFormattedName . ' discount rates or transaction fees for the Mgt. Co. or Resident, or lower the maximum ' . $strFormattedName . ' payment amount.' ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valResidentDiscountRate( $strCountryCode ) {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		// ACH, CH21, and EMO must have a 0 rate for resident
		if( true == in_array( $this->getPaymentTypeId(), self::$c_arrintZeroRate ) ) {
			if( false == is_null( $this->getResidentDiscountRate() ) && 0 < $this->getResidentDiscountRate() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_discount_rate', $strFormattedName . ' Resident Discount Rate must be set to zero.' ) );
			}

			return $boolIsValid;
		}

		if( ( true == is_null( $this->getResidentDiscountRate() ) || 0 == $this->getResidentDiscountRate() ) && ( true == is_null( $this->getCompanyDiscountRate() ) || 0 == $this->getCompanyDiscountRate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_discount_rate', $strFormattedName . ' resident discount rate must be greater than zero if the ' . $strFormattedName . ' company discount rate is equal to zero.' ) );
		}

		if( $boolIsValid == true && 0.05 < ( float ) $this->getResidentDiscountRate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_discount_rate', $strFormattedName . ' resident discount rate should not exceed 5%.' ) );
		}

		if( true == $boolIsValid && CCountry::CODE_CANADA == $strCountryCode && -1 == bccomp( CCompanyMerchantAccount::CANADA_CREDIT_DISCOUNT_RATE_CAP, ( float ) $this->getResidentDiscountRate(), 4 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_discount_rate', $strFormattedName . ' Resident Discount Rate cannot be greater than 1.75%' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyDiscountRate( $intMerchantProcessingTypeId ) {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( CPaymentType::EMONEY_ORDER == $this->getPaymentTypeId() ) {
			return true;
		}

		if( true == in_array( $this->getPaymentTypeId(), self::$c_arrintZeroRate ) ) {
			if( false == is_null( $this->getCompanyDiscountRate() ) && 0 < $this->getCompanyDiscountRate() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_discount_rate', $strFormattedName . ' Company Discount Rate must be set to zero.' ) );
			}

			return $boolIsValid;
		}

		if( true == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) && CMerchantProcessingType::CLEARING == $intMerchantProcessingTypeId && .0160 > $this->getCompanyDiscountRate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_discount_rate', $strFormattedName . ' company discount rate must be higher than 1.6% when processing type is set to "Clearing.".  Should be 2.24% or greater usually.' ) );
		}

		if( true == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) && CMerchantProcessingType::CLEARING != $intMerchantProcessingTypeId && .03 < $this->getCompanyDiscountRate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_discount_rate', $strFormattedName . ' company discount rate should not exceed 3 percent.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyDebitDiscountRate( $intMerchantProcessingTypeId ) {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( false == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) ) {
			return true;
		}

		if( true == $this->hasResidentDebitFee() ) {
			return true;
		}

		if( CMerchantProcessingType::CLEARING == $intMerchantProcessingTypeId && .0160 > $this->getCompanyDebitDiscountRate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_debit_discount_rate', $strFormattedName . ' company debit discount rate must be higher than 1.6% when processing type is set to "Clearing.".  Should be 2.24% or greater usually.' ) );
		}

		if( $boolIsValid == true && -1 == bccomp( 0.03, ( float ) $this->getCompanyDebitDiscountRate(), 6 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_debit_discount_rate', $strFormattedName . ' company debit discount rate should not exceed 3 percent.' ) );
		}

		return $boolIsValid;
	}

	public function valWaivedDiscountRate() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		// only ACH and CC
		if( false == in_array( $this->getPaymentTypeId(), array_merge( [ CPaymentType::ACH ], CPaymentType::$c_arrintCreditCardPaymentTypes ) ) ) {
			return true;
		}

		// ACH must be zero
		if( CPaymentType::ACH == $this->getPaymentTypeId() && 0 < ( float ) $this->getWaivedDiscountRate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_discount_rate', $strFormattedName . ' waived discount rate must be zero.' ) );
		}

		// CC must be less than 3%
		if( true == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) && ( true == is_null( $this->getWaivedDiscountRate() ) || 0 == $this->getWaivedDiscountRate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'waived_discount_rate', $strFormattedName . ' waived discount rate must be greater than zero.' ) );
		}

		if( true == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) && .03 < $this->getWaivedDiscountRate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_discount_rate', $strFormattedName . ' waived discount rate must less than 3%.' ) );
		}

		return $boolIsValid;
	}

	public function valWaivedDebitDiscountRate() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( false == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) ) {
			return true;
		}

		// doesn't apply if resident fee is 0
		if( false == $this->hasResidentDebitFee() ) {
			return true;
		}

//		if( true == is_null( $this->getWaivedDebitDiscountRate() ) || 0 == $this->getWaivedDebitDiscountRate() ) {
//			$boolIsValid = false;
//			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'waived_debit_discount_rate', $strFormattedName . ' waived debit discount rate must be greater than zero.' ) );
//		}

		if( 0.03 < ( float ) $this->getWaivedDebitDiscountRate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'waived_debit_discount_rate', $strFormattedName . ' waived debit discount rate should not exceed 3 percent.' ) );
		}

		return $boolIsValid;
	}

	public function valRebateDiscountRate( $intMerchantProcessingTypeId ) {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( CPaymentMedium::TERMINAL == $this->getPaymentMediumId() || true == in_array( $this->getPaymentTypeId(), self::$c_arrintTerminalOnlyPaymentTypes ) ) {
			return true;
		}

		if( CMerchantProcessingType::STANDARD == $intMerchantProcessingTypeId && $this->getRebateDiscountRate() > $this->getResidentDiscountRate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rebate_discount_rate', $strFormattedName . ' Rebate Discount Rate must be less than or equal to Resident Discount Rate.' ) );
		} elseif( CMerchantProcessingType::CLEARING == $intMerchantProcessingTypeId && $this->getRebateDiscountRate() > $this->getCompanyDiscountRate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rebate_discount_rate', $strFormattedName . ' Rebate Discount Rate must be less than or equal to Company Discount Rate.' ) );
		}

		return $boolIsValid;
	}

	public function validateClearingAccountSeparateDebitPricing( $intMerchantProcessingTypeId ) {
		$boolIsValid = true;
		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		// Merchant services suggests that no clearing accounts should have enabled separate debit pricing
		// If this case exists then we need to throw an error
		if( CMerchantProcessingType::CLEARING == $intMerchantProcessingTypeId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'debit_transaction_fee', $strFormattedName . ' Clearing accounts should not have enabled separate debit pricing.' ) );
		}

		return $boolIsValid;
	}

	public function valResidentDebitTransactionFee() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( false == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) ) {
			return true;
		}

		// If a company debit fee is empty, and the resident debit transaction fee is empty, throw an error.  At least one entity must absorb the debit fee
		if( false == $this->hasCompanyDebitFee() && ( true == is_null( $this->getResidentDebitTransactionFee() ) || 0 == $this->getResidentDebitTransactionFee() ) && ( true == is_null( $this->getResidentDebitDiscountRate() ) || 0 == $this->getResidentDebitDiscountRate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_debit_transaction_fee', $strFormattedName . ' resident debit transaction fee must be set if the company debit fee is 0.' ) );
		}

		// Validate that the resident debit fee is less than 10
		if( true == $boolIsValid && ( 3 > $this->getResidentDebitTransactionFee() || 10 < $this->getResidentDebitTransactionFee() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_debit_transaction_fee', $strFormattedName . ' resident debit transaction fee must be greater than $3 and less than $10.' ) );
		}

		$fltCombinedDebitFees = $this->getCompanyDebitTransactionFee() + $this->getResidentDebitTransactionFee();

		// If the combined value of the company and resident debit fee is less than 4.95 OR greater than 10, then throw an error
		if( true == $this->hasResidentDebitFee() && ( 4.95 > $fltCombinedDebitFees || 10 < $fltCombinedDebitFees ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_debit_transaction_fee', 'Combined resident and company debit transaction fees for ' . $strFormattedName . ' should be between $4.95 and $10.' ) );
		}

		return $boolIsValid;
	}

	public function valResidentDebitDiscountRate( $strCountryCode, $boolVisaPilotEnabled, $intIsDebitEnabled = 0 ) {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( false == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) ) {
			return true;
		}

		if( true == $boolVisaPilotEnabled ) {
			if( .03 < $this->getResidentDebitDiscountRate() || 0 > $this->getResidentDebitDiscountRate() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_debit_discount_rate', $strFormattedName . ' resident debit discount rate must be greater than or equal to 0% and less than 3%.' ) );
			}
		} else {
			if( 0 != $this->getResidentDebitDiscountRate() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_debit_discount_rate', $strFormattedName . ' resident debit discount rate must be equal to 0%.' ) );
			}
		}

		if( 1 == $intIsDebitEnabled && 0 < $this->getResidentDebitDiscountRate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_debit_discount_rate', $strFormattedName . ' resident debit discount rate must be set to 0 if separate debit pricing is enabled.' ) );
		}

		if( false == $this->hasCompanyDebitFee() && ( true == is_null( $this->getResidentDebitTransactionFee() ) || 0 == $this->getResidentDebitTransactionFee() ) && ( true == is_null( $this->getResidentDebitDiscountRate() ) || 0 == $this->getResidentDebitDiscountRate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_debit_discount_rate', $strFormattedName . ' resident debit discount rate must be set if company debit fee is 0.' ) );
		}

		if( -1 == bccomp( 0.05, ( float ) $this->getResidentDebitDiscountRate(), 6 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_debit_discount_rate', $strFormattedName . ' resident debit discount rate should not exceed 3%.' ) );
		}

		if( CCountry::CODE_CANADA == $strCountryCode && -1 == bccomp( CCompanyMerchantAccount::CANADA_DEBIT_DISCOUNT_RATE_CAP, ( float ) $this->getResidentDebitDiscountRate(),  4 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_debit_discount_rate', $strFormattedName . ' Resident Debit Discount Rate cannot be greater than 0.75%' ) );
		}

		return $boolIsValid;
	}

	public function valTransactionFee( $intMerchantProcessingTypeId ) {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( CPaymentType::EMONEY_ORDER == $this->getPaymentTypeId() ) {
			return true;
		}

		if( CPaymentType::CHECK_21 == $this->getPaymentTypeId() && .25 > $this->getTransactionFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_fee', $strFormattedName . ' transaction fee must $0.25 or higher.' ) );
		}

		if( true == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) && CMerchantProcessingType::CLEARING == $intMerchantProcessingTypeId && .25 > $this->getTransactionFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_fee', $strFormattedName . ' transaction fee must be $0.25 or higher.  Should be 48 cents or greater usually.' ) );
		}

		if( false == is_null( $this->getTransactionFee() ) && 10 < $this->getTransactionFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_fee', $strFormattedName . ' Company transaction fee cannot exceed $10.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyDebitTransactionFee() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( false == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) ) {
			return true;
		}

		// If a resident debit fee is empty, and the company debit transaction fee is empty, throw an error.  At least one entity must absorb the debit fee
		if( false == $this->hasResidentDebitFee() && ( true == is_null( $this->getCompanyDebitTransactionFee() ) || 0 == $this->getCompanyDebitTransactionFee() ) && ( true == is_null( $this->getCompanyDebitDiscountRate() ) || 0 == $this->getCompanyDebitDiscountRate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'debit_transaction_fee', $strFormattedName . ' company debit transaction fee must be set if the residetn debit fee is 0.' ) );
		}

		$fltCombinedDebitFees = $this->getCompanyDebitTransactionFee() + $this->getResidentDebitTransactionFee();

		// If the combined value of the company and resident debit fee is less than 4.95 OR greater than 10, then throw an error
		if( true == $this->hasCompanyDebitFee() && ( 4.95 > $fltCombinedDebitFees || 10 < $fltCombinedDebitFees ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'debit_transaction_fee', 'Combined resident and company debit transaction fees for ' . $strFormattedName . ' should be between $4.95 and $10.' ) );
		}

		return $boolIsValid;
	}

	public function valDowngradeDiscountRate() {
		$boolIsValid = true;

		// we don't currently validate transaction fee for ACH or AMEX
		if( true == in_array( $this->getPaymentTypeId(), [ CPaymentType::ACH, CPaymentType::AMEX ] ) ) {
			return true;
		}

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( $boolIsValid == true && 0.05 < ( float ) $this->getDowngradeDiscountRate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'downgrade_discount_rate', $strFormattedName . ' downgrade discount rate should not exceed 5 percent.' ) );
		}

		return $boolIsValid;
	}

	public function valInternationalDiscountRate() {
		$boolIsValid = true;

		// we don't currently validate transaction fee for ACH or AMEX
		if( true == in_array( $this->getPaymentTypeId(), [ CPaymentType::ACH, CPaymentType::AMEX ] ) ) {
			return true;
		}

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( $boolIsValid == true && 0.05 < ( float ) $this->getInternationalDiscountRate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'international_discount_rate', $strFormattedName . ' international discount rate should not exceed 5 percent.' ) );
		}

		return $boolIsValid;
	}

	/**
	 * @param CClient $objClient
	 * @return bool
	 */
	public function valMerchantGatewayId( $objClient ) {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( false == is_numeric( $this->getMerchantGatewayId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'merchant_gateway_id', $strFormattedName . ' merchant gateway is required.' ) );
		}

		if( true == valObj( $objClient, 'CClient' ) && CCompanyStatusType::CLIENT == $objClient->getCompanyStatusTypeId() && CMerchantGateway::VITAL == $this->getMerchantGatewayId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'merchant_gateway_id', 'Merchant Gateway can not be set to Vital because Client is of type Client.' ) );
		}

		return $boolIsValid;
	}

	public function valGatewayUsername() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( true == in_array( $this->getMerchantGatewayId(), CMerchantGateway::$c_arrintRequiresGatewayCredentials ) && true == is_null( trim( $this->getGatewayUsername() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_username', $strFormattedName . ' gateway username cannot be empty.' ) );
		}

		return $boolIsValid;
	}

	public function valGatewayPassword() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( true == in_array( $this->getMerchantGatewayId(), CMerchantGateway::$c_arrintRequiresGatewayCredentials ) && true == is_null( trim( $this->getGatewayPassword() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_password', $strFormattedName . ' gateway password cannot be empty.' ) );
		}

		return $boolIsValid;
	}

	public function valLoginUsername() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( true == in_array( $this->getMerchantGatewayId(), CMerchantGateway::$c_arrintRequiresLoginCredentials ) && true == is_null( trim( $this->getLoginUsername() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'login_username', $strFormattedName . ' login username cannot be empty.' ) );
		}

		return $boolIsValid;
	}

	public function valLoginPassword() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( true == in_array( $this->getMerchantGatewayId(), CMerchantGateway::$c_arrintRequiresLoginCredentials ) && true == is_null( trim( $this->getLoginPassword() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'login_password', $strFormattedName . ' login password cannot be empty.' ) );
		}

		return $boolIsValid;
	}

	public function valCutoffTime() {
		$boolIsValid = true;

		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		if( true == is_null( trim( $this->getCutOffTime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cut_off_time', $strFormattedName . ' cut off time cannot be empty.' ) );
		}

		return $boolIsValid;
	}

	private function isVisaPilotEnablementValid( $intIsVisaPilotEnabled ) {
		$boolIsValid = true;

		// If Visa pilot enabled but recurring Visa is disabled
		if( 1 == $intIsVisaPilotEnabled && 0 == $this->getIsEnabled() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_transaction_fee', 'Recurring Visa must be enabled if Visa Pilot is enabled.' ) );
		}

		// If recurring Visa is enabled but Visa Pilot is disabled AND resident is absorbing CC fees
		if( 0 == $intIsVisaPilotEnabled && 1 == $this->getIsEnabled() && 0 < $this->getResidentTransactionFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident_transaction_fee', 'Visa Pilot must be enabled if Recurring Visa is enabled and resident is absorbing fees.' ) );
		}

		return $boolIsValid;
	}

	private function validateInternationalCardTransactionFee() {
		$boolIsValid = true;
		$strFormattedName = CPaymentType::idToFormattedString( $this->getPaymentTypeId() );

		// We'll only validate the transaction fee for credit card types for now
		if( false == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) ) {
			return $boolIsValid;
		}

		// Until we know what the default transaction fee will be, we can allow zero
//		if( 0 > $this->getTransactionFee() || self::INTERNATIONAL_CC_MANAGEMENT_FEE > $this->getTransactionFee() ) {
//			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_fee', $strFormattedName . ' transaction fees for international merchant accounts should be $0.50 or higher.' ) );
//			$boolIsValid = false;
//		}

		return $boolIsValid;
	}

	public function validate( $objClient, $intMerchantProcessingTypeId, $strCountryCode, $intIsDebitEnabled, $intIsVisaPilotEnabled ) {
		$boolIsValid = true;

		/**
		 * Handle validation for internation CC payment types.  Company transaction fees should match $0.50.
		 */
		if( CMerchantProcessingType::INTERNATIONAL == $intMerchantProcessingTypeId ) {
			$boolIsValid &= $this->validateInternationalCardTransactionFee();

			return $boolIsValid;
		}

		$boolIsDebitEnabled = 1 == $intIsDebitEnabled;
		$boolIsVisaPilotEnabled = 1 == $intIsVisaPilotEnabled;

		// We need to validate Visa pilot enable/disabled states
		if( CPaymentMedium::RECURRING == $this->getPaymentMediumId() && CPaymentType::VISA == $this->getPaymentTypeId() && false == $this->isVisaPilotEnablementValid( $intIsVisaPilotEnabled ) ) {
			return false;
		}

		if( 0 == $this->getIsEnabled() || false == is_null( $this->getDeletedOn() ) ) {
			return true;
		}

		$boolIsValid &= $this->valMerchantGatewayId( $objClient );
		$boolIsValid &= $this->valGatewayUsername();
		$boolIsValid &= $this->valGatewayPassword();
		$boolIsValid &= $this->valLoginUsername();
		$boolIsValid &= $this->valLoginPassword();
		$boolIsValid &= $this->valCutOffTime();
		$boolIsValid &= $this->valMaxPaymentAmount();
		$boolIsValid &= $this->valMaxMonthlyProcessAmount();

		switch( $intMerchantProcessingTypeId ) {
			// If AP account, skip fee validation
			case CMerchantProcessingType::ACCOUNTS_PAYABLE:
				break;

			default:
				$boolIsValid &= $this->valResidentTransactionFee();
				$boolIsValid &= $this->valResidentDiscountRate( $strCountryCode );

				$boolIsValid &= $this->valTransactionFee( $intMerchantProcessingTypeId );
				$boolIsValid &= $this->valCompanyDiscountRate( $intMerchantProcessingTypeId );

				$boolIsValid &= $this->valWaivedTransactionFee();
				$boolIsValid &= $this->valWaivedDiscountRate();

				if( true == $boolIsDebitEnabled ) {
					$boolIsValid &= $this->validateClearingAccountSeparateDebitPricing( $intMerchantProcessingTypeId );
					$boolIsValid &= $this->valResidentDebitTransactionFee();
					$boolIsValid &= $this->valResidentDebitDiscountRate( $strCountryCode, $boolIsVisaPilotEnabled, $intIsDebitEnabled );

					$boolIsValid &= $this->valCompanyDebitTransactionFee();
					$boolIsValid &= $this->valCompanyDebitDiscountRate( $intMerchantProcessingTypeId );

					$boolIsValid &= $this->valWaivedDebitTransactionFee();
					$boolIsValid &= $this->valWaivedDebitDiscountRate();
				}

				// not currently used.
				// $boolIsValid &= $this->valDowngradeTransactionFee();
				// $boolIsValid &= $this->valDowngradeDiscountRate();

				// not currently used.
				// $boolIsValid &= $this->valInternationalTransactionFee();
				// $boolIsValid &= $this->valInternationalDiscountRate();

				$boolIsValid &= $this->valRebateTransactionFee( $intMerchantProcessingTypeId );
				$boolIsValid &= $this->valRebateDiscountRate( $intMerchantProcessingTypeId );

				$boolIsValid &= $this->valReverseTransactionFee();
				$boolIsValid &= $this->valCompanyAdjustmentFee();
				$boolIsValid &= $this->valCompanyReturnFee();
				$boolIsValid &= $this->valResidentReturnFee();
				$boolIsValid &= $this->valUnauthorizedReturnFee();
				$boolIsValid &= $this->valAuthFailureFee();
				break;
		}

		// not currently used.
		// $boolIsValid &= $this->valGatewayTransactionFee();
		// $boolIsValid &= $this->valProviderDiscountRate();

		return $boolIsValid;
	}

	// endregion

	// region Database Operations

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setId( $this->fetchNextId( $objDatabase ) );

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function fetchNextId( $objDatabase, $strTableName = NULL ) {
		$this->updateIdSequence( $objDatabase );

		return parent::fetchNextId( $objDatabase );
	}

	public function updateIdSequence( $objDatabase, $intSequenceIncrementValue = 1 ) {
		$strSql = 'SELECT last_value AS id FROM merchant_account_methods_id_seq';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( false != valArr( $arrmixData ) && false != isset( $arrmixData[0]['id'] ) ) {
			$intStartingId = $arrmixData[0]['id'];
			$intEndingId = $intStartingId + $intSequenceIncrementValue;
			if( CONFIG_CLOUD_ID == CCloud::LINDON_ID && self::MAX_ID_LINDON < $intEndingId ) {
				// we're in the Lindon cloud and the payment id is greater than 100 million
				$strSql = 'SELECT id FROM merchant_account_methods WHERE id < ' . self::MAX_ID_LINDON . ' ORDER BY id DESC LIMIT 1';
				$intMinIdForCloud = 1;
			} elseif( CONFIG_CLOUD_ID == CCloud::IRELAND_ID && ( self::MAX_ID_IRELAND < $intEndingId || self::MIN_ID_IRELAND > $intStartingId ) ) {
				// we're in the Ireland cloud and the payment id is not between 100 and 200 million
				$strSql = 'SELECT id FROM merchant_account_methods WHERE id BETWEEN ' . self::MIN_ID_IRELAND . ' AND ' . self::MAX_ID_IRELAND . ' ORDER BY id DESC LIMIT 1';
				$intMinIdForCloud = self::MIN_ID_IRELAND;
			} else {
				// no adjustment necessary
				return $intStartingId;
			}

			$arrmixResult = fetchData( $strSql, $objDatabase );
			if( !valArr( $arrmixResult ) ) {
				$intMaxId = $intMinIdForCloud;
			} else {
				$intMaxId = $arrmixResult[0]['id'];
			}
			$intNextId = $intMaxId + 1;
			$strSql = 'SELECT setval( \'merchant_account_methods_id_seq\', ' . ( int ) $intNextId . ', FALSE )';
			fetchData( $strSql, $objDatabase );

			return $intNextId;
		}
		return NULL;
	}

	// endregion

	// region Utility Methods

	protected function hasCompanyFee() {
		return ( !is_null( $this->getTransactionFee() ) && 0 < $this->getTransactionFee() ) || ( !is_null( $this->getCompanyDiscountRate() ) && 0 < $this->getCompanyDiscountRate() );
	}

	protected function hasCompanyDebitFee() {
		return ( !is_null( $this->getCompanyDebitTransactionFee() ) && 0 < $this->getCompanyDebitTransactionFee() ) || ( !is_null( $this->getCompanyDebitDiscountRate() ) && 0 < $this->getCompanyDebitDiscountRate() );
	}

	protected function hasResidentFee() {
		return ( !is_null( $this->getResidentTransactionFee() ) && 0 < $this->getResidentTransactionFee() ) || ( !is_null( $this->getResidentDiscountRate() ) && 0 < $this->getResidentDiscountRate() );
	}

	protected function hasResidentDebitFee() {
		return ( !is_null( $this->getResidentDebitTransactionFee() ) && 0 < $this->getResidentDebitTransactionFee() ) || ( !is_null( $this->getResidentDebitDiscountRate() ) && 0 < $this->getResidentDebitDiscountRate() );
	}

	// endregion

}
