<?php

class CEftCharge extends CBaseEftCharge {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyMerchantAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intCompanyMerchantAccountId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_merchant_account_id', 'Company Merchant Account Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valArPaymentId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intArPaymentId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_payment_id', 'Ar Payment Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intAccountId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_id', 'Account is required.' ) );
		}

		return $boolIsValid;
	}

	public function valChargeCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intChargeCodeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_code_id', 'Charge code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valChargeDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strChargeDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_datetime', 'Charge datetime is required.' ) );
		}

		if( $boolIsValid == true && 0 < strlen( $this->m_strChargeDatetime ) && false == CValidation::validateDate( date( 'm/d/Y', strtotime( $this->m_strChargeDatetime ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_datetime', 'Charge datetime is not formatted properly.  (MM/DD/YYYY)' ) );
		}

		return $boolIsValid;
	}

	public function valChargeAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->m_fltChargeAmount ) || 0 == $this->m_fltChargeAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_amount', 'Charge amount is required.' ) );
		}

		if( $boolIsValid == true && false == is_numeric( $this->m_fltChargeAmount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_amount', 'Charge amount should be numeric.' ) );
		}

		if( $boolIsValid == true && true == isset( $this->m_fltChargeAmount ) && true == is_numeric( $this->m_fltChargeAmount ) && 1000000 < $this->m_fltChargeAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_amount', 'You cannot enter an amount greater than a million dollars.' ) );
		}

		return $boolIsValid;
	}

	public function valCostAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->m_fltCostAmount ) || 0 == $this->m_fltCostAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cost_amount', 'Cost amount is required.' ) );
		}

		if( $boolIsValid == true && false == is_numeric( $this->m_fltCostAmount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cost_amount', 'Cost amount should be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function valMemo() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strMemo ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'memo', 'Memo is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPostedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAlreadyDeleted() {
    	$boolIsValid = true;

    	if( false == is_null( $this->getEftChargeId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'eft_charge_id', 'You cannot delete a EFT charge that has already been deleted.' ) );
    	}

    	return $boolIsValid;
    }

	public function valCurrencyCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valArPaymentId();
				$boolIsValid &= $this->valCompanyMerchantAccountId();
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valChargeDatetime();
				$boolIsValid &= $this->valChargeCodeId();
				$boolIsValid &= $this->valChargeAmount();
				$boolIsValid &= $this->valMemo();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'delete_eft_charge':
            	$boolIsValid &= $this->valAlreadyDeleted();
            	break;

			case 'post_eft_charge':
			   $boolIsValid &= $this->valAccountId();
			   $boolIsValid &= $this->valChargeDatetime();
			   $boolIsValid &= $this->valChargeCodeId();
			   $boolIsValid &= $this->valChargeAmount();
			   $boolIsValid &= $this->valMemo();
			    break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function createTransaction() {

		$objTransaction = new CTransaction();
		$objTransaction->setDefaults();

		$objTransaction->setCid( $this->getCid() );
		$objTransaction->setAccountId( $this->getAccountId() );
		$objTransaction->setChargeCodeId( $this->getChargeCodeId() );
		$objTransaction->setMemo( $this->getMemo() );
		$objTransaction->setTransactionAmount( $this->getChargeAmount() );
		$objTransaction->setTransactionDatetime( $this->getChargeDatetime() );

		// We may want to set some default values here
		$objTransaction->setIsCommissioned( 0 );
		$objTransaction->setExportBatchId( NULL );
		$objTransaction->setInvoiceId( NULL );
		$objTransaction->setReferenceNumber( NULL );

		return $objTransaction;
	}

	public function getConsolidatedErrorMsg() {
		$strErrorMessage = NULL;

		if( true == valArr( $this->m_arrobjErrorMsgs ) ) {
			foreach( $this->m_arrobjErrorMsgs as $objErrorMsg ) {
				$strErrorMessage .= $objErrorMsg->getMessage() . "\n";
			}
		}
		return $strErrorMessage;
	}

}

?>