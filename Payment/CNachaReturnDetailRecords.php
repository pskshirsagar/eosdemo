<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaReturnDetailRecords
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CNachaReturnDetailRecords extends CBaseNachaReturnDetailRecords {

	public static function fetchNachaReturnDetailRecordsByNachaReturnFileId( $intNachaReturnFileId, $objDatabase ) {

		$strSql = 'SELECT * FROM nacha_return_detail_records WHERE nacha_return_file_id = ' . ( int ) $intNachaReturnFileId;
		return self::fetchNachaReturnDetailRecords( $strSql, $objDatabase );
	}

	public static function fetchUnReturnsProcessedNachaReturnDetailRecordsWithoutEftInstructionId( $objPaymentDatabase ) {
		$strSql = 'SELECT 
						nrdr.* 
					FROM nacha_return_detail_records nrdr
					    JOIN nacha_return_addenda_detail_records nradr ON nradr.nacha_return_detail_record_id = nrdr.id
					WHERE nradr.return_processed_on IS NULL
						AND nradr.eft_instruction_id IS NULL';

		return self::fetchNachaReturnDetailRecords( $strSql, $objPaymentDatabase );
	}

}
?>