<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937FileStatusTypes
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */
class CX937FileStatusTypes extends CBaseX937FileStatusTypes {

	public static function fetchX937FileStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CX937FileStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchX937FileStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CX937FileStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllX937FileStatusTypes( $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM x937_file_status_types ORDER BY order_num';
		return self::fetchX937FileStatusTypes( $strSql, $objPaymentDatabase );
	}

}
?>