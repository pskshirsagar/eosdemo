<?php

use Psi\Libraries\UtilObjectModifiers\CObjectModifiers;

class CNachaReturnFile extends CBaseNachaReturnFile {

	protected $m_arrobjNachaReturnFileBatches;
	protected $m_arrobjNachaReturnDetailRecords;
	protected $m_arrobjNachaReturnAddendaDetailRecords;

	protected $m_arrobjOriginalNachaReturnFileBatches;
	protected $m_arrobjOriginalNachaReturnDetailRecords;
	protected $m_arrobjOriginalNachaReturnAddendaDetailRecords;

	protected $m_fltTotal;
	protected $m_strContents;
	protected $m_strParsedFilePath;

	protected $m_objScriptApp;

    public function __construct() {
        parent::__construct();

		$this->m_arrobjNachaReturnFileBatches                     = array();
	    $this->m_arrobjNachaReturnDetailRecords                   = array();
	    $this->m_arrobjNachaReturnAddendaDetailRecords            = array();

		$this->m_arrobjOriginalNachaReturnFileBatches             = array();
	    $this->m_arrobjOriginalNachaReturnDetailRecords           = array();
	    $this->m_arrobjOriginalNachaReturnAddendaDetailRecords    = array();

		$this->m_fltTotal = 0;

        return;
    }

	/**
	 * Add Functions
	 */

	public function addNachaReturnFileBatch( $objNachaReturnFileBatch ) {
	    $objNachaReturnFileBatch->setNachaReturnFileId( $this->getId() );
	    if( true == valArr( $objNachaReturnFileBatch->getNachaReturnDetailRecords() ) ) {
	        foreach( $objNachaReturnFileBatch->getNachaReturnDetailRecords() as $objNachaReturnDetailRecord ) {
	            $objNachaReturnDetailRecord->setNachaReturnFileId( $this->getId() );
	        }
	    }

		$this->m_arrobjNachaReturnFileBatches[$objNachaReturnFileBatch->getId()] = $objNachaReturnFileBatch;
	}

	/**
	 * Get Functions
	 */

    public function getNachaReturnFileBatches() {
    	return $this->m_arrobjNachaReturnFileBatches;
    }

    public function getTotal() {
    	return $this->m_fltTotal;
    }

	public function getFilePath() {
		return PATH_MOUNTS_NACHA_FILES_RETURNS . $this->m_strFilePath;
	}

	/**
	 * Set Functions
	 */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['total'] ) ) $this->setTotal( $arrmixValues['total'] );

        return;
    }

    public function setDefaults() {

		$this->m_strNachaFileDatetime = date( 'Y-m-d H:i:s' );

		$this->setNachaFileStatusTypeId( CNachaFileStatusType::RETURNS_PARSING );
		$this->setNachaFileTypeId( CNachaFileType::ACH_RETURNS );
    }

    public function setTotal( $fltTotal ) {
		$this->m_fltTotal = $fltTotal;
    }

    public function setNachaReturnFileBatches( $arrobjNachaReturnFileBatches ) {
        $this->m_arrobjNachaReturnFileBatches = $arrobjNachaReturnFileBatches;
    }

    public function setScriptApp( $objScriptApp ) {
    	$this->m_objScriptApp = $objScriptApp;
    }

    /**
     * Create Functions
     */

	public function createReconciliationEntry( $objPaymentDatabase ) {

		$objReconciliationEntry = new CReconciliationEntry();
		$objReconciliationEntry->setProcessingBankAccountId( $this->getProcessingBankAccountId() );
		$objReconciliationEntry->setNachaReturnFileId( $this->getId() );

		$objReconciliationEntry->setTotal( abs( $this->getTotal() ) );

		switch( $this->getNachaFileTypeId() ) {
			case CNachaFileType::AR_PAYMENTS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::AR_PAYMENTS;
			    break;

			case CNachaFileType::COMPANY_PAYMENTS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::COMPANY_PAYMENTS;
			    break;

			case CNachaFileType::SETTLEMENT_DISTRIBUTIONS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::SETTLEMENT_DISTRIBUTIONS;
			    break;

			case CNachaFileType::ACH_RETURNS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::ACH_RETURNS;
			    break;

			case CNachaFileType::NACHA_RETURN_ADDENDA_DETAIL_RECORDS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::NACHA_RETURN_ADDENDA_DETAIL_RECORDS;
			    break;

			case CNachaFileType::ACH_CHECK21_INTERMEDIARY_DISTRIBUTIONS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::ACH_CHECK21_INTERMEDIARY_DISTRIBUTIONS;
			    break;

			case CNachaFileType::ACH_CHECK21_RETURN_INTERMEDIARY_DEBITS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::ACH_CHECK21_RETURN_INTERMEDIARY_DEBITS;
			    break;

			case CNachaFileType::WESTERN_UNION_INTERMEDIARY_CREDITS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::WESTERN_UNION_INTERMEDIARY_CREDITS;
			    break;

			default:
				$intReconciliationEntryTypeId = NULL; // this should trigger error.
		}

		$objReconciliationEntry->setReconciliationEntryTypeId( $intReconciliationEntryTypeId );

		// Make sure that the return date is not in the common.transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.

		$intCreatedOn = strtotime( $this->getCreatedOn() );

		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y', $intCreatedOn ), date( 'm/d/Y', ( $intCreatedOn + ( 21 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = array();

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intCreatedOn = strtotime( '+1 day', $intCreatedOn );

		while( 'Sun' == date( 'D', $intCreatedOn ) || 'Sat' == date( 'D', $intCreatedOn ) || true == in_array( $intCreatedOn, $arrintProhibitedTransactionHolidays ) ) {
			$intCreatedOn = strtotime( '+1 day', $intCreatedOn );
		}

		$objReconciliationEntry->setMovementDatetime( date( 'm/d/Y', $intCreatedOn ) );

		return $objReconciliationEntry;
	}

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	/**
	 * Fetch Functions
	 */

    public function fetchUnTransferredNachaReturnAddendaDetailRecords( $objPaymentDatabase ) {
    	return \Psi\Eos\Payment\CNachaReturnAddendaDetailRecords::createService()->fetchUnTransferredNachaReturnAddendaDetailRecordsByNachaReturnFileId( $this->m_intId, $objPaymentDatabase );
    }

	/**
	 * Database Functions
	 */

    public function insert( $intUserId, $objPaymentDatabase, $boolReturnSqlOnly = false ) {

		if( false == parent::insert( $intUserId, $objPaymentDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		if( true == valArr( $this->m_arrobjNachaReturnFileBatches ) ) {
			foreach( $this->m_arrobjNachaReturnFileBatches as $objNachaReturnFileBatch ) {
				if( false == $objNachaReturnFileBatch->insert( $intUserId, $objPaymentDatabase ) ) {
					trigger_error( 'Nacha return file batch failed to process.', E_USER_WARNING );
					display( $objNachaReturnFileBatch );
					return false;

				} else {
					if( true == valArr( $objNachaReturnFileBatch->getNachaReturnDetailRecords() ) ) {
						foreach( $objNachaReturnFileBatch->getNachaReturnDetailRecords() as $objNachaReturnDetailRecord ) {

							// If the return record has a receiving DFI Identification of 12224259, and the returned on date is set, then this means,
							// we shouldn't actually process this return.  It means that the transaction really happened, but it looks like it failed
							// because we have a bad bank account number entered for the intermediary account.  These types of errors should be notified to
							// processing@entrata.com, and we will have to handle these scenarios manually.  Eventually we should probably find
							// a way to automate this problem.

							// SELECT * FROM nacha_entry_detail_records WHERE returned_on IS NOT NULL AND receiving_dfi_identification = '12224259'

							$objNachaReturnAddendaDetailRecord = $objNachaReturnDetailRecord->getNachaReturnAddendaDetailRecord();
							$objOriginalNachaEntryDetailRecord = $objNachaReturnAddendaDetailRecord->fetchOriginalNachaEntryDetailRecord( $objPaymentDatabase );

							// If this is a failure on an intermediary transaction, we need to notify development.
							if( true == isset( $objOriginalNachaEntryDetailRecord ) && 1 == $objOriginalNachaEntryDetailRecord->getIsIntermediary() && 0 == $objNachaReturnAddendaDetailRecord->getIsNoticeOfChange() ) {
								// Send email with a notification on this one.
								// mail( 'processing@entrata.com, dave@entrata.com, devsupport@entrata.com', 'Serious payment processing error.  Intermediary account number invalid.', 'Here is a funky problem with a return that we could not process.  The nacha entry detail record id is ' . $objOriginalNachaEntryDetailRecord->getId() . '. CID: ' . $objOriginalNachaEntryDetailRecord->getCid() . '. This could happen because of an inaccurate intermediary account number entered by merchant services, set on the clients intermediary account (processing bank account) in client admin.', 'from:system@entrata.com' );

								$strError = sprintf( 'Original NEDR #%d INTERMEDIARY RETURN for: NRDR #%d | NRADR #%d | NRF #%d | Trace number %s | Amount $%s (%s) | Receiving name \'%s\' | Effective entry date \'%s\'',
										$objOriginalNachaEntryDetailRecord->getId(),
										$objNachaReturnDetailRecord->getId(),
										$objNachaReturnAddendaDetailRecord->getId(),
										$objNachaReturnDetailRecord->getNachaReturnFileId(),
										$objNachaReturnAddendaDetailRecord->getOriginalEntryTraceNumber(),
										$objNachaReturnDetailRecord->getFormattedAmount(),
										( CEntryDetailRecordType::CREDIT == $objNachaReturnAddendaDetailRecord->getEntryDetailRecordTypeId() ? 'CREDIT' : 'DEBIT' ),
										$objNachaReturnDetailRecord->getReceivingCompanyName(),
										$objNachaReturnFileBatch->getEffectiveEntryDate() );
								trigger_error( $strError, E_USER_WARNING );
								$objNachaReturnDetailRecord->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, 'is_intermediary_return', $strError ) );
							}

							if( false == $objNachaReturnDetailRecord->insert( $intUserId, $objPaymentDatabase ) ) {
								trigger_error( 'Nacha return detail record failed to insert.', E_USER_WARNING );
								return false;
							}

							if( false == $objNachaReturnDetailRecord->getNachaReturnAddendaDetailRecord()->insert( $intUserId, $objPaymentDatabase ) ) {
								trigger_error( 'Nacha return detail addenda record failed to insert.', E_USER_WARNING );
								return false;
							}
						}
					}
				}
			}
		}

		return true;
	}

	/**
	 * Other Functions
	 */

    public function processReturns( $intUserId, $objAdminDatabase, $objPaymentDatabase, $objEmailDatabase ) {

    	if( true == valArr( $this->m_arrobjNachaReturnFileBatches ) ) {
			foreach( $this->m_arrobjNachaReturnFileBatches as $objNachaReturnFileBatch ) {
				CScriptAppHelpers::callbackLogger( 'Processing returns for NRFB #%d ( Mgmt Comp: #%d, Comp Name: %s, Comp Entry Desc: %s )', array( $objNachaReturnFileBatch->getId(), $objNachaReturnFileBatch->getCid(), $objNachaReturnFileBatch->getCompanyName(), $objNachaReturnFileBatch->getCompanyEntryDescription() ) );
				if( true == valArr( $objNachaReturnFileBatch->getNachaReturnDetailRecords() ) ) {
					foreach( $objNachaReturnFileBatch->getNachaReturnDetailRecords() as $objNachaReturnDetailRecord ) {
						$objNachaReturnAddendaDetailRecord = $objNachaReturnDetailRecord->getNachaReturnAddendaDetailRecord();

						$objReturnedItem = coalesceArrayValues( [ 'ClearingBatchId' => $objNachaReturnAddendaDetailRecord->getClearingBatchId(), 'ArPaymentId' => $objNachaReturnAddendaDetailRecord->getArPaymentId(), 'SettlementDistributionId' => $objNachaReturnAddendaDetailRecord->getSettlementDistributionId(), 'CompanyPaymentId' => $objNachaReturnAddendaDetailRecord->getCompanyPaymentId() ] );

						CScriptAppHelpers::callbackLogger( '%2sProcessing NRDR #%d ( Receiving Comp Name: %s, Trace Number: %s )', array( '', $objNachaReturnDetailRecord->getId(), $objNachaReturnDetailRecord->getReceivingCompanyName(), $objNachaReturnDetailRecord->getTraceNumber() ) );
						CScriptAppHelpers::callbackLogger( '%4sProcessing NRADR #%d ( Entry Detail Record: #%d, Return Reason: %s, Returning Field: %s, Returning Id: #%d )', array( '', $objNachaReturnAddendaDetailRecord->getId(), $objNachaReturnAddendaDetailRecord->getNachaEntryDetailRecordId(), $objNachaReturnAddendaDetailRecord->getReturnReasonCode(), $objReturnedItem->key, $objReturnedItem->value ) );

						$objClientDatabase = NULL;
						if( false == is_null( $objNachaReturnAddendaDetailRecord->getArPaymentId() ) ) {
							$objClientDatabase = $this->m_objScriptApp->findClientDatabaseForCid( $objNachaReturnAddendaDetailRecord->getCid() );
						}

						if( 1 != $objNachaReturnAddendaDetailRecord->getIsNoticeOfChange() ) {
							CScriptAppHelpers::callbackLogger( '%6sProcessing as RETURN...', array( '' ) );
							if( false == $objNachaReturnAddendaDetailRecord->processReturn( $intUserId, $objAdminDatabase, $objPaymentDatabase, $objClientDatabase, $objEmailDatabase ) ) {
								$this->addErrorMsgs( $objNachaReturnAddendaDetailRecord->getErrorMsgs() );
								return false;
							} else {
								$this->addErrorMsgs( $objNachaReturnAddendaDetailRecord->getErrorMsgs() );
							}
						} else {
							CScriptAppHelpers::callbackLogger( '%6sProcessing as NOTICE OF CHANGE...', array( '' ) );
							if( NULL == $objClientDatabase && false == is_null( $objNachaReturnAddendaDetailRecord->getArPaymentId() ) ) {
								CScriptAppHelpers::callbackLogger( ' Failed loading client database for ar payment: ' . $objNachaReturnAddendaDetailRecord->getArPaymentId() . ', nacha return addenda detail record id: ' . $objNachaReturnAddendaDetailRecord->getId() );
							}
							if( false == $objNachaReturnAddendaDetailRecord->processNoticeOfChange( $intUserId, $objAdminDatabase, $objPaymentDatabase, $objClientDatabase, $objEmailDatabase ) ) {
								$this->addErrorMsgs( $objNachaReturnAddendaDetailRecord->getErrorMsgs() );
								return false;
							} else {
								$this->addErrorMsgs( $objNachaReturnAddendaDetailRecord->getErrorMsgs() );
							}
						}
					}
				}
			}
		}

		// Create a return deposit for company payments contained in this file.

		$strSql = 'SELECT
						company_payment_id
					FROM
						nacha_return_addenda_detail_records nradr
						JOIN nacha_return_detail_records nrdr ON ( nrdr.id = nradr.nacha_return_detail_record_id )
						JOIN nacha_return_files nrf ON ( nrf.id = nrdr.nacha_return_file_id )
					WHERE
						nradr.company_payment_id IS NOT NULL
						AND nradr.is_notice_of_change <> 1
						AND nrf.id = ' . $this->getId();

		$arrintReturnedCompanyPaymentIds = fetchData( $strSql, $objPaymentDatabase );

		$arrintCollapsedCompanyPaymentIds = array();

		if( true == valArr( $arrintReturnedCompanyPaymentIds ) ) {
			foreach( $arrintReturnedCompanyPaymentIds as $arrintReturnedCompanyPaymentId ) {
				$arrintCollapsedCompanyPaymentIds[$arrintReturnedCompanyPaymentId['company_payment_id']] = $arrintReturnedCompanyPaymentId['company_payment_id'];
			}
		}

		if( true == valArr( $arrintCollapsedCompanyPaymentIds ) ) {
			$this->generateCompanyPaymentDepositByAccountIds( $intUserId, $arrintCollapsedCompanyPaymentIds, CExportBatchType::RESIDENT_INSURE_MARKEL, CChartOfAccount::PSIA_OPERATING_MARKEL, array( CAccountType::INSURANCE_MARKEL ), NULL, $objAdminDatabase );
			$this->generateCompanyPaymentDepositByAccountIds( $intUserId, $arrintCollapsedCompanyPaymentIds, CExportBatchType::RESIDENT_INSURE_KEMPER, CChartOfAccount::PSIA_OPERATING_KEMPER, array( CAccountType::INSURANCE_KEMPER ), NULL, $objAdminDatabase );
			$this->generateCompanyPaymentDepositByAccountIds( $intUserId, $arrintCollapsedCompanyPaymentIds, CExportBatchType::RESIDENT_INSURE_QBE, CChartOfAccount::PSIA_OPERATING_QBE, array( CAccountType::INSURANCE_QBE ), NULL, $objAdminDatabase );
			$this->generateCompanyPaymentDepositByAccountIds( $intUserId, $arrintCollapsedCompanyPaymentIds, CExportBatchType::RESIDENT_INSURE_IDTHEFT, CChartOfAccount::PSIA_OPERATING_IDTHEFT, array( CAccountType::INSURANCE_IDTHEFT ), NULL, $objAdminDatabase );
			$this->generateCompanyPaymentDepositByAccountIds( $intUserId, $arrintCollapsedCompanyPaymentIds, CExportBatchType::MASTER_POLICY, CChartOfAccount::MASTER_POLICY, array( CAccountType::MASTER_POLICY ), NULL, $objAdminDatabase );
			$this->generateCompanyPaymentDepositByAccountIds( $intUserId, $arrintCollapsedCompanyPaymentIds, CExportBatchType::PROPERTY_SOLUTIONS, CChartOfAccount::REVENUE_ZIONS, NULL, CAccountType::$c_arrintInsuranceAccountTypeIds, $objAdminDatabase );
		}

		return true;
    }

    public function generateCompanyPaymentDepositByAccountIds( $intUserId, $arrintCollapsedCompanyPaymentIds, $intExportBatchTypeId, $intChartOfAccountId, $arrintAccountTypeIds, $arrintBannedAccountTypeIds, $objAdminDatabase ) {

		$strSql = 'SELECT
						cp.*
					FROM
						company_payments cp
						JOIN accounts a ON ( cp.account_id = a.id AND cp.cid = a.cid )
					WHERE
						cp.id IN ( ' . implode( ',', $arrintCollapsedCompanyPaymentIds ) . ' )
						' . ( ( true == valArr( $arrintAccountTypeIds ) ) ? ' AND a.account_type_id IN ( ' . implode( ',', $arrintAccountTypeIds ) . ' ) ' : '' ) . '
						' . ( ( true == valArr( $arrintBannedAccountTypeIds ) ) ? ' AND a.account_type_id NOT IN ( ' . implode( ',', $arrintBannedAccountTypeIds ) . ' ) ' : '' ) . '
						AND cp.returned_on IS NOT NULL
						AND cp.deposit_id IS NULL;';

		$arrobjCompanyPayments = \Psi\Eos\Admin\CCompanyPayments::createService()->fetchCompanyPayments( $strSql, $objAdminDatabase );

		if( true == valArr( $arrobjCompanyPayments ) ) {
			$objDeposit = \Psi\Eos\Admin\CDeposits::createService()->createDeposit();
			$objDeposit->setCompanyPayments( $arrobjCompanyPayments );

			// Calculate the total on the deposit
			$fltDepositTotal = 0;

			if( true == valArr( $arrobjCompanyPayments ) ) {
				foreach( $arrobjCompanyPayments as $objCompanyPayment ) {
					$fltDepositTotal += $objCompanyPayment->getPaymentAmount();
				}
			} else {
				return true;
			}

			$objDeposit->setDepositAmount( $fltDepositTotal );
			$objDeposit->setExportBatchTypeId( $intExportBatchTypeId );
			$objDeposit->setChartOfAccountId( $intChartOfAccountId );
			$objDeposit->setDepositTypeId( CDepositType::ACH );
			$objDeposit->setDepositDate( date( 'm/d/Y' ) );
			$objDeposit->setDepositMemo( 'Automated Deposit Creation From Nacha Return File ' . $this->getId() );

			if( false == $objDeposit->insert( $intUserId, $objAdminDatabase ) ) {
				return true;
			}

			foreach( $arrobjCompanyPayments as $objCompanyPayment ) {
				$objCompanyPayment->setDepositId( $objDeposit->getId(), $objAdminDatabase );
				if( false == $objCompanyPayment->update( $intUserId, $objAdminDatabase ) ) {
					return true;
				}
			}
		}

		return true;
    }

    public function createBatch( $arrstrBatch, $objPaymentDatabase ) {

    	if( true == valArr( $arrstrBatch ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrBatch ) ) {

    		$objNachaReturnFileBatch = new CNachaReturnFileBatch();
			$objNachaReturnFileBatch->setNachaReturnFileId( $this->m_intId );
			$objNachaReturnFileBatch->setNachaReturnFile( $this );
			$objNachaReturnFileBatch->setId( $objNachaReturnFileBatch->fetchNextId( $objPaymentDatabase ) );

			if( false == $objNachaReturnFileBatch->populateData( $arrstrBatch['batch_header'] ) ) {
				return false;
			}

			if( false == $objNachaReturnFileBatch->loadEntryDetailAndAddendaRecords( $arrstrBatch['entries'], $objNachaReturnFileBatch->getEffectiveEntryDate(), $objPaymentDatabase ) ) {
				return false;
			}

			$this->m_arrobjNachaReturnFileBatches[] = $objNachaReturnFileBatch;

			return true;

    	} else {
			return false;
    	}
    }

    public function moveReturnsFileToParsedFolder( $strPendingFilePath ) {

    	$strPendingFileName = $this->m_strFileName;
    	$strParsedFilePath = PATH_MOUNTS_NACHA_FILES_RETURNS . $this->getParsedFilePath();
		$strParsedFileName = $this->m_intId . '_' . $this->m_strFileName;

		if( false == is_dir( $strParsedFilePath ) ) {
			CFileIo::recursiveMakeDir( $strParsedFilePath );
    	}

		if( false == rename( $strPendingFilePath . $strPendingFileName, $strParsedFilePath . $strParsedFileName ) ) {
			return false;
		}

		return true;
    }

    public function getParsedFilePath() {
    	if( true == valStr( $this->m_strParsedFilePath ) ) {
    		return $this->m_strParsedFilePath;
    	}

    	$this->m_strParsedFilePath = str_replace( 'Pending', 'Parsed', $this->m_strFilePath ) . date( 'Y' ) . '/' . date( 'm' ) . '/';
		return $this->m_strParsedFilePath;
    }

    public function parseFileData( $objPaymentDatabase ) {

        if( true == is_null( $this->m_strContents ) ) {

	        $strFileName = $this->getFilePath() . $this->getFileName();
	        $strAlternateFileName = $this->getFilePath() . $this->getId() . '_' . $this->getFileName();

	        if( false == file_exists( $strFileName ) && false == file_exists( $strAlternateFileName ) ) {
	            echo 'File failed to open<br/>';
	            trigger_error( 'File failed to open.', E_USER_ERROR );
	            exit;
	        }

	        if( true == file_exists( $strAlternateFileName ) ) {
	            $strFileName = $strAlternateFileName;
	        }

	    	// Nacha return file format:
	    	//   One record per line of 94 characters. Fixed width fields. File padded with filler records (all 9s) so that
	        //   it is always a multiple of 10.
	    	//
	    	//   Record Types : file header, batch header, return detail, return addenda, batch control, file control, filler
	    	//   Record Id    : 1,           5,            6,             7,              8,             9,            9
	    	//
	        //   The record "Id" is the first digit on the each line. File record types will be sequenced  as 1(5678)9,
	        //   with the 5678 sequence repeating 1 or more times. See example below along with a notation that shows where
	        //   the original trace number is extracted for matching back to a nacha entry detail record in our system.
	    	//
	    	//	1011861072180 1240000541003355509U094101PROPERTY SOLUTIONS     ZIONS BANK
			//	5200Mountain View Ma1112193             9861072180WEBrent      1003331004440001124000050000002
			//	6262222222223333333333       00000527002480931        Trevor Jones          S 1124000058131660
			//	799R03186107212480931      32228030                                            124000058131660
			//	820000000200322280300000000527000000000000009861072180                         124000050000002
			//	5200Southwood Realty3216731             1861072180WEBRent      1203331203330001124000050000002
			//	62633333333344444444444444   00000888008580593        Michelle Boller       S 1124000056551229
			//	799R0318610721858059300000025317704                                            124000056551229
			//	820000000200253177040000000888000000000000001861072180                         124000050000002
			//	9000006000003000000120096510932000000099395000000000000
			//	9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999

			//	      ^^^^^^^^^^^^^^^
			//	      Original Trace#
			//	      ===============
			//	7.....186107212480931.........................................................................
			//	7.....186107218580593.........................................................................

			if( false == ( $resHandle = CFileIo::fileOpen( $strFileName, 'r' ) ) ) {
			    echo 'failed to open';
				return false;
			}

			$this->m_strContents = fread( $resHandle, filesize( $strFileName ) );
        }

		$strContents			= str_replace( chr( 10 ) . chr( 13 ), chr( 10 ), $this->m_strContents );
		$arrstrContents 		= explode( chr( 10 ), $strContents );

		if( 0 == \Psi\Libraries\UtilFunctions\count( $arrstrContents ) ) {
			return true;

		} else {

			$intContentsCount = \Psi\Libraries\UtilFunctions\count( $arrstrContents );

			for( $intX = 0; $intX < $intContentsCount - 1; $intX++ ) {

				$strStrippedValue 		= trim( str_replace( '"', '', $arrstrContents[$intX] ) );
				$strNextStrippedValue 	= trim( str_replace( '"', '', $arrstrContents[( $intX + 1 )] ) );

				if( 1 == $strStrippedValue[0] ) {
					// Ignore File Header
					continue;
				} elseif( 5 == $strStrippedValue[0] ) {
					$arrstrBatch 					= array();
					$arrstrBatch['batch_header'] 	= $strStrippedValue;
					$arrstrBatch['entries'] 		= array();

				} elseif( 6 == $strStrippedValue[0] ) {
					$arrstrBatch['entries'][] = array( 'entry_detail_record' => $strStrippedValue, 'addenda_record' => $strNextStrippedValue );

				} elseif( 7 == $strStrippedValue[0] ) {
					if( true == isset ( $arrstrBatch['batch_header'] ) ) {

						if( false == $this->createBatch( $arrstrBatch, $objPaymentDatabase ) ) {
							continue;
						}
					}

				} elseif( 8 == $strStrippedValue[0] ) {
					// Ignore Batch Control
					continue;
				} elseif( 9 == $strStrippedValue[0] ) {
					// Ignore File Control / File Padding
					continue;
				} else {
					break;
				}
			}
		}

		return true;
    }

    // This function is to reload all the nacha return file data back into the objects for validation & other manipulation when required.

    public function restoreDatabaseData( $objPaymentDatabase ) {

        $this->m_arrobjNachaReturnFileBatches             = \Psi\Eos\Payment\CNachaReturnFileBatches::createService()->fetchNachaReturnFileBatchesByNachaReturnFileId( $this->getId(), $objPaymentDatabase );
	    $this->m_arrobjNachaReturnDetailRecords           = \Psi\Eos\Payment\CNachaReturnDetailRecords::createService()->fetchNachaReturnDetailRecordsByNachaReturnFileId( $this->getId(), $objPaymentDatabase );
	    $this->m_arrobjNachaReturnAddendaDetailRecords    = \Psi\Eos\Payment\CNachaReturnAddendaDetailRecords::createService()->fetchNachaReturnAddendaDetailRecordsByNachaReturnFileId( $this->getId(), $objPaymentDatabase );

		CObjectModifiers::createService()->nestObjects( $this->m_arrobjNachaReturnAddendaDetailRecords, $this->m_arrobjNachaReturnDetailRecords );
		CObjectModifiers::createService()->nestObjects( $this->m_arrobjNachaReturnDetailRecords, $this->m_arrobjNachaReturnFileBatches );

        if( true == valArr( $this->m_arrobjNachaReturnDetailRecords ) ) {
		    foreach( $this->m_arrobjNachaReturnDetailRecords as $objNachaReturnDetailRecord ) {
		        $objNachaReturnDetailRecord->setDfiAccountNumber( $objNachaReturnDetailRecord->getCheckAccountNumber() );
		    }
	    }
    }

    public function verifyDatabaseDataMatchesFileData() {

        $strFileName = $this->getFilePath() . $this->getFileName();
        $strAlternateFileName = $this->getFilePath() . $this->getId() . '_' . $this->getFileName();

        if( false == file_exists( $strFileName ) && false == file_exists( $strAlternateFileName ) ) {
            echo 'File failed to open<br/>';
            trigger_error( 'File failed to open.', E_USER_ERROR );
            exit;
        }

        if( true == file_exists( $strAlternateFileName ) ) {
            $strFileName = $strAlternateFileName;
        }

		if( false == ( $resHandle = CFileIo::fileOpen( $strFileName, 'r' ) ) ) {
            echo 'File failed to load.<br/>';
            trigger_error( 'File failed to load.', E_USER_ERROR );
            exit;
		}

		$this->m_strContents	= fread( $resHandle, filesize( $strFileName ) );
		$strContents			= str_replace( chr( 10 ) . chr( 13 ), chr( 10 ), $this->m_strContents );
		$arrstrContents 		= explode( chr( 10 ), $strContents );
		fclose( $resHandle );

		$intNachaReturnBatchCount = 0;
		$intNachaReturnDetailRecordCount = 0;
		$intNachaReturnAddendaDetailRecordCount = 0;

		// Make sure all the records were inserted into the database
        if( 0 == \Psi\Libraries\UtilFunctions\count( $arrstrContents ) ) {
			return true;

		} else {

			$intContentsCount = \Psi\Libraries\UtilFunctions\count( $arrstrContents );

			for( $intX = 0; $intX < $intContentsCount; $intX++ ) {

				$strStrippedValue = trim( str_replace( '"', '', $arrstrContents[$intX] ) );

				if( 5 == ( int ) substr( $strStrippedValue, 0, 1 ) ) {
					$intNachaReturnBatchCount++;

				} elseif( 6 == ( int ) substr( $strStrippedValue, 0, 1 ) ) {
					$intNachaReturnDetailRecordCount++;

				} elseif( 7 == ( int ) substr( $strStrippedValue, 0, 1 ) ) {
					$intNachaReturnAddendaDetailRecordCount++;

				} else {
				    continue;
				}
			}
		}

		if( \Psi\Libraries\UtilFunctions\count( $this->m_arrobjNachaReturnFileBatches ) != $intNachaReturnBatchCount ) {
            return false;
		}

        if( \Psi\Libraries\UtilFunctions\count( $this->m_arrobjNachaReturnDetailRecords ) != $intNachaReturnDetailRecordCount ) {
            return false;
		}

        if( \Psi\Libraries\UtilFunctions\count( $this->m_arrobjNachaReturnAddendaDetailRecords ) != $intNachaReturnDetailRecordCount ) {
            return false;
		}

		return true;
    }

    // This function was written to add into the database, any records that may have been skipped while inserting, prior to 12/19/2009.
    // I'm writing a script that will allow all the missed insertions to be reinserted.  After much deliberation, I've decided that the best
    // way to reinsert is to just create a new file in the database with only the entries that were originally skipped.  It's kind of sloppy,
    // but was my only real option because there's no way to identify / query which items batches particular nacha entry detail records failed
    // to add to.  Perhaps there was a better way, but this should work just fine.  DJB

    public function insertRepairedFile( $intUserId, $objPaymentDatabase ) {

        $this->m_arrobjOriginalNachaReturnFileBatches = $this->m_arrobjNachaReturnFileBatches;
        $this->m_arrobjOriginalNachaReturnDetailRecords = $this->m_arrobjNachaReturnDetailRecords;
        $this->m_arrobjOriginalNachaReturnAddendaDetailRecords = $this->m_arrobjNachaReturnAddendaDetailRecords;

        $this->m_arrobjNachaReturnFileBatches = NULL;
        $this->m_arrobjNachaReturnDetailRecords = NULL;
        $this->m_arrobjNachaReturnAddendaDetailRecords = NULL;

        $this->parseFileData( $objPaymentDatabase );

        $intNachaReturnAddendaDetailRecords = 0;

        $objNachaReturnFile = clone $this;
        $objNachaReturnFile->setId( NULL );
        $objNachaReturnFile->setId( $objNachaReturnFile->fetchNextId( $objPaymentDatabase ) );
        $objNachaReturnFile->setProcessedOn( NULL );
        $objNachaReturnFile->setReturnsProcessedOn( NULL );
        $objNachaReturnFile->setCentralReturnsProcessedOn( NULL );
        $objNachaReturnFile->setConfirmedOn( date( 'm/d/Y H:i:s' ) );
        $objNachaReturnFile->setIsCorrection( 1 );
        $objNachaReturnFile->setIsReconPrepped( 0 );
        $objNachaReturnFile->setNachaReturnFileBatches( NULL );

        if( true == valArr( $this->m_arrobjNachaReturnFileBatches ) ) {
            foreach( $this->m_arrobjNachaReturnFileBatches as $objNachaReturnFileBatch ) {
                if( true == valArr( $objNachaReturnFileBatch->getNachaReturnDetailRecords() ) ) {
                    foreach( $objNachaReturnFileBatch->getNachaReturnDetailRecords() as $objNachaReturnDetailRecord ) {
                        if( true == valObj( $objNachaReturnDetailRecord->getNachaReturnAddendaDetailRecord(), 'CNachaReturnAddendaDetailRecord' ) ) {
                            if( false == $objNachaReturnDetailRecord->getNachaReturnAddendaDetailRecord()->hasAlreadyPosted( $objPaymentDatabase ) ) {
								$objNachaReturnFile->addNachaReturnFileBatch( $objNachaReturnFileBatch );
							}
                        }
                    }
                }
            }
        }

        if( false == $objNachaReturnFile->insert( $intUserId, $objPaymentDatabase ) ) {
			trigger_error( 'Nacha return file failed to process.', E_USER_ERROR );
		}

		$this->setRepairedOn( date( 'm/d/Y H:i:s' ) );

		if( false == $this->update( $intUserId, $objPaymentDatabase ) ) {
			trigger_error( 'Nacha return file failed to process.', E_USER_ERROR );
		}

        return true;
    }

    public function verifyAllReturnsAreProcessed( $intUserId, $objAdminDatabase, $objPaymentDatabase, $objEmailDatabase ) {

        $boolIsValid = true;

        if( true == valArr( $this->m_arrobjNachaReturnAddendaDetailRecords ) ) {

        	foreach( $this->m_arrobjNachaReturnAddendaDetailRecords as $objNachaReturnAddendaDetailRecord ) {

                if( 1 == $objNachaReturnAddendaDetailRecord->getIsNoticeOfChange() ) {
                    continue;
                }

                $objOriginalNachaEntryDetailRecord = $objNachaReturnAddendaDetailRecord->fetchOriginalNachaEntryDetailRecord( $objPaymentDatabase );

				if( false == valObj( $objOriginalNachaEntryDetailRecord, 'CNachaEntryDetailRecord' ) ) {
					trigger_error( 'Original nacha entry detail record failed to load.', E_USER_WARNING );
					$boolIsValid = false;
					continue;
				}

                // Load corresponding data to verify return has processed.

                $objOriginalObject = NULL;
                $boolIsIntermediaryReturn = false;
				$boolIsEftInstruction = false;
                $strOriginalTransactionDescription = '';

                if( true == is_numeric( $objOriginalNachaEntryDetailRecord->getSettlementDistributionId() ) ) {
                    $strOriginalTransactionDescription = 'settlement distribution';
                    $objOriginalObject = $objOriginalNachaEntryDetailRecord->fetchSettlementDistribution( $objPaymentDatabase );

                } elseif( true == is_numeric( $objOriginalNachaEntryDetailRecord->getCompanyPaymentId() ) ) {
                    $strOriginalTransactionDescription = 'company payment';
                    $objOriginalObject = $objOriginalNachaEntryDetailRecord->fetchCompanyPayment( $objAdminDatabase );

                } elseif( true == is_numeric( $objOriginalNachaEntryDetailRecord->getArPaymentId() ) ) {
					$strOriginalTransactionDescription = 'ar payment';
					/** @var CArPayment $objOriginalObject */
					$objOriginalObject = $objOriginalNachaEntryDetailRecord->fetchArPayment( $objPaymentDatabase );

					// if the payment has been transferred, we need to load the new payment and perform the operation there
					if( true == $objOriginalObject->getIsReversed() && CPaymentStatusType::CAPTURED == $objOriginalObject->getPaymentStatusTypeId() && false == is_null( $objOriginalObject->getBatchedOn() ) ) {
						$intArPaymentId = CArPayments::fetchTransferredArPaymentIdByPaymentIdByCid( $objOriginalObject->getId(), $objOriginalObject->getCid(), $objPaymentDatabase );
						$objOriginalObject = CArPayments::fetchArPaymentByIdByCid( $intArPaymentId, $objOriginalObject->getCId(), $objPaymentDatabase );
					}
				} elseif( true == is_numeric( $objOriginalNachaEntryDetailRecord->getEftInstructionId() ) ) {
					$boolIsEftInstruction = true;
                } else {
                    $boolIsIntermediaryReturn = true;
                }

           		if( true == $boolIsIntermediaryReturn ) {
           			echo 'Original Nacha entry ' . $objOriginalNachaEntryDetailRecord->getId() . ' IS AN INTERMEDIARY RETURN.' . "\n";
            	} elseif( true == $boolIsEftInstruction ) {
					echo 'Original Nacha entry ' . $objOriginalNachaEntryDetailRecord->getId() . ' is for an Eft Instruction.' . "\n";
				} elseif( true == is_null( $objOriginalObject ) ) {
                    trigger_error( 'Original object failed to load.', E_USER_WARNING );
                    $boolIsValid = false;
                    continue;

                } elseif( false == is_numeric( $objOriginalObject->getReturnTypeId() ) || true == is_null( $objOriginalObject->getReturnedOn() ) ) {

                    $objNachaReturnAddendaDetailRecord->loadReturnTypeId( $objPaymentDatabase );

                    if( false == is_null( $objOriginalNachaEntryDetailRecord->getArPaymentId() ) ) {
                    	$objClientDatabase = $this->m_objScriptApp->findClientDatabaseForCid( $objOriginalNachaEntryDetailRecord->getCid() );
                    }

                    if( false == $objNachaReturnAddendaDetailRecord->processReturn( $intUserId, $objAdminDatabase, $objPaymentDatabase, $objClientDatabase, $objEmailDatabase ) ) {
                    	$this->addErrorMsgs( $objNachaReturnAddendaDetailRecord->getErrorMsgs() );
                        trigger_error( 'Unprocessed return [' . $strOriginalTransactionDescription . '] [id: ' . $objOriginalObject->getId() . '] failed to process.', E_USER_WARNING );
                        $boolIsValid = false;
                    	continue;
                    }

                    echo 'Original object [' . $strOriginalTransactionDescription . ' ID: ' . $objOriginalObject->getId() . '] was not returned, but has been repaired.' . "\n";

                } else {
                	echo 'Original Nacha entry ' . $objOriginalNachaEntryDetailRecord->getId() . ' verified returned.' . "\n";

                }
            }
        }

        return $boolIsValid;
    }

	public function addErrorMsgs( $arrobjErrorMsgs ) {
		if( true == valArr( $arrobjErrorMsgs ) ) {
			foreach( $arrobjErrorMsgs as $objErrorMsg ) {
				if( true == valObj( $objErrorMsg, 'CErrorMsg' ) ) {
					$this->addErrorMsg( $objErrorMsg );
				}
			}
		}
	}

	public function loadNachaReturnFileBatches( $objPaymentDatabase ) {
		$arrobjNachaReturnFileBatches = \Psi\Eos\Payment\CNachaReturnFileBatches::createService()->fetchNachaReturnFileBatchesByNachaReturnFileId( $this->getId(), $objPaymentDatabase );
		if( true == valArr( $arrobjNachaReturnFileBatches ) ) {
			foreach( $arrobjNachaReturnFileBatches as $objNachaReturnFileBatch ) {
				if( false != valObj( $objNachaReturnFileBatch, 'CNachaReturnFileBatch' ) ) {
					$objNachaReturnFileBatch->loadNachaReturnDetailRecords( $objNachaReturnFileBatch->getEffectiveEntryDate(), $objPaymentDatabase );
					$this->addNachaReturnFileBatch( $objNachaReturnFileBatch );
				}
			}
		}
	}

	public function unsetNachaReturnDetailRecord( $intNachaReturnFileBatchId, $intNachaReturnDetailRecordId ) {
		$arrobjNachaReturnFileBatches = rekeyObjects( 'id', $this->m_arrobjNachaReturnFileBatches );

		if( true == array_key_exists( $intNachaReturnFileBatchId, $arrobjNachaReturnFileBatches ) ) {
			$boolResult = $arrobjNachaReturnFileBatches[$intNachaReturnFileBatchId]->unsetNachaReturnDetailRecord( $intNachaReturnDetailRecordId );

			if( false == $boolResult ) {
				unset( $arrobjNachaReturnFileBatches[$intNachaReturnFileBatchId] );
				$this->m_arrobjNachaReturnFileBatches = $arrobjNachaReturnFileBatches;
			}
		}
	}

}

?>