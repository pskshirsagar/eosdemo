<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CWesternUnionFileTypes
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CWesternUnionFileTypes extends CBaseWesternUnionFileTypes {

	public static function fetchWesternUnionFileTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CWesternUnionFileType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchWesternUnionFileType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CWesternUnionFileType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>