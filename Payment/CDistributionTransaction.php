<?php

class CDistributionTransaction extends CBaseDistributionTransaction {

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ));
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCid())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ));
        // }

        return $boolIsValid;
    }

    public function valCompanyMerchantAccountId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCompanyMerchantAccountId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_merchant_account_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valSettlementDistributionId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getSettlementDistributionId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'settlement_distribution_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valDistributionTransactionTypeId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getDistributionTransactionTypeId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'distribution_transaction_type_id', '' ));
        // }

        return $boolIsValid;
    }

    public function valDistributionNumber() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getDistributionNumber())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'distribution_number', '' ));
        // }

        return $boolIsValid;
    }

    public function valDistributionDatetime() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getDistributionDatetime())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'distribution_datetime', '' ));
        // }

        return $boolIsValid;
    }

    public function valDistributionAmount() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getDistributionAmount())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'distribution_amount', '' ));
        // }

        return $boolIsValid;
    }

    public function valDistributionMemo() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getDistributionMemo())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'distribution_memo', '' ));
        // }

        return $boolIsValid;
    }

    public function valGatewayResponseCode() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getGatewayResponseCode())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_response_code', '' ));
        // }

        return $boolIsValid;
    }

    public function valGatewayResponseText() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getGatewayResponseText())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_response_text', '' ));
        // }

        return $boolIsValid;
    }

    public function valGatewayResponseReasonCode() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getGatewayResponseReasonCode())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_response_reason_code', '' ));
        // }

        return $boolIsValid;
    }

    public function valGatewayResponseReasonText() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getGatewayResponseReasonText())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_response_reason_text', '' ));
        // }

        return $boolIsValid;
    }

    public function valGatewayTransactionNumber() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getGatewayTransactionNumber())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_transaction_number', '' ));
        // }

        return $boolIsValid;
    }

    public function valGatewayApprovalCode() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getGatewayApprovalCode())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_approval_code', '' ));
        // }

        return $boolIsValid;
    }

    public function valGatewayAvsResultCode() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getGatewayAvsResultCode())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_avs_result_code', '' ));
        // }

        return $boolIsValid;
    }

    public function valGatewayAvsResultText() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getGatewayAvsResultText())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_avs_result_text', '' ));
        // }

        return $boolIsValid;
    }

    public function valGatewayRawData() {
        $boolIsValid = true;

		/**
		 * Validation example
		 */

        // if( true == is_null( $this->getGatewayRawData())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gateway_raw_data', '' ));
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>