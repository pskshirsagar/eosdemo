<?php

class CMerchantChangeRequest extends CBaseMerchantChangeRequest {

	protected $m_intNewMerchantAccountRequestCount;
	protected $m_arrstrMerchantAccountIds;
	protected $m_intMerchantChangeRequestTypeId;
	protected $m_intPropertyId;
	protected $m_objClient;
	protected $m_arrobjMerchantAccounts;
	protected $m_arrintSelectedMerchantAccountIds;

	const MERCHANT_CHANGE_REQUEST_TYPE_UPDATE = 1;
	const MERCHANT_CHANGE_REQUEST_TYPE_NEW = 2;

	const MAX_ID_LINDON = 99999999;  // 100 million - 1
	const MIN_ID_IRELAND = 100000000; // 100 million
	const MAX_ID_IRELAND = 199999999; // 200 million - 1

	/**
	 * Get Functions
	 */

	public function getMerchantChangeRequestTypeId() {
    	return $this->m_intMerchantChangeRequestTypeId;
    }

    public function getNewMerchantAccountRequestCount() {
    	return $this->m_intNewMerchantAccountRequestCount;
    }

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

    public function getMerchantAccountIds() {
    	return $this->m_arrstrMerchantAccountIds;
    }

    public function getSelectedMerchantAccountIds() {
    	return $this->m_arrintSelectedMerchantAccountIds;
    }

    public function getMerchantAccounts() {
    	return $this->m_arrobjMerchantAccounts;
    }

    /**
     * Set Functions
     */

    public function setNewMerchantAccountRequestCount( $intNewMerchantAccountRequestCount ) {
    	$this->m_intNewMerchantAccountRequestCount = $intNewMerchantAccountRequestCount;
    }

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

    public function setMerchantAccountIds( $arrstrMerchantAccountIds ) {
    	$this->m_arrstrMerchantAccountIds = $arrstrMerchantAccountIds;
    }

    public function setMerchantChangeRequestTypeId( $intMerchantChangeRequestTypeId ) {
    	$this->m_intMerchantChangeRequestTypeId = $intMerchantChangeRequestTypeId;
    }

    public function setClient( $objClient ) {
    	$this->m_objClient = $objClient;
    }

    public function setSelectedMerchantAccountIds( $arrintSelectedMerchantAccountIds ) {
    	$this->m_arrintSelectedMerchantAccountIds = $arrintSelectedMerchantAccountIds;
    }

    public function setMerchantAccountsDisplay( $objCompanyUser, $objClient, $objDatabase, $intOrder ) {

        if( true == $objCompanyUser->getIsAdministrator() ) {
    		$this->m_arrobjMerchantAccounts = ( array ) $objClient->fetchMerchantAccounts( $objDatabase );
    	} else {
	    	$arrobjProperties = $objCompanyUser->fetchProperties( $objDatabase );
    		$this->m_arrobjMerchantAccounts = ( true == valArr( $arrobjProperties ) ) ? $objClient->fetchMerchantAccountsByPropertyIds( array_keys( $arrobjProperties ), $objDatabase ) : array();
    	}

    	$this->m_arrstrFilterPositions[$intOrder] = 'merchant_accounts_filter';
    }

    public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );
        if( true == isset( $arrstrValues['merchant_account_ids'] ) ) $this->setMerchantAccountIds( $arrstrValues['merchant_account_ids'] );
        if( true == isset( $arrstrValues['new_merchant_account_request_count'] ) ) $this->setNewMerchantAccountRequestCount( $arrstrValues['new_merchant_account_request_count'] );
        if( true == isset( $arrstrValues['merchant_change_request_type_id'] ) ) $this->setMerchantChangeRequestTypeId( $arrstrValues['merchant_change_request_type_id'] );
    }

    /**
     * Fetch Functions
     */

	public function fetchMerchantChangeRequestAccounts( $objPaymentDatabase, $strOrderBy = NULL ) {

		return CMerchantChangeRequestAccounts::fetchMerchantChangeRequestAccountsByMerchantChangeRequestId( $this->getId(), $objPaymentDatabase, $strOrderBy );
	}

	public function fetchMerchantChangeRequestAccountById( $intMerchangeChangeRequestId, $objPaymentDatabase ) {

		return CMerchantChangeRequestAccounts::fetchMerchantChangeRequestAccountById( $intMerchangeChangeRequestId, $objPaymentDatabase );
	}

    public function fetchClient( $objAdminDatabase ) {

		return CClients::fetchClientById( $this->getCid(), $objAdminDatabase );
	}

	public function fetchUnAssociatedMerchantChangeRequestAccounts( $objPaymentDatabase ) {

		return CMerchantChangeRequestAccounts::fetchUnAssociatedMerchantChangeRequestAccountsByMerchantChangeRequestId( $this->getId(), $objPaymentDatabase );
	}

	public function fetchParentMerchantChangeRequestAccounts( $objPaymentDatabase ) {

		return CMerchantChangeRequestAccounts::fetchParentMerchantChangeRequestAccountsByMerchantChangeRequestId( $this->getId(), $objPaymentDatabase );
	}

	/**
	 * create Functions
	 */

	public function createMerchantChangeRequestAccount() {

		$objMerchantChangeRequestAccount = new CMerchantChangeRequestAccount();

		$objMerchantChangeRequestAccount->setCid( $this->getCid() );

		return $objMerchantChangeRequestAccount;
	}

	/**
	 * other Functions
	 */

	public function delete( $intCurrentUserId, $objPaymentDatabase, $boolReturnSqlOnly = false ) {

		$boolIsValid = $this->validate( VALIDATE_DELETE );

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		if( false == $this->update( $intCurrentUserId, $objPaymentDatabase, $boolReturnSqlOnly ) ) {
			$this->setDeletedBy( NULL );
			$this->setDeletedOn( NULL );
			return false;
		}

		return true;
	}

	/**
	 * Validation Functions
	 */

    public function valCid() {
        $boolIsValid = true;

	/**
	* Validation Example
	*/

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valRequestorName() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strRequestorName ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requestor_name', __( 'Requestor name is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valRequiredFields() {
        $boolIsValid = true;

        if( true == is_null( $this->m_intId ) ) {
	        if( 0 == $this->m_intNewMerchantAccountRequestCount && true == is_null( $this->m_arrstrMerchantAccountIds ) ) {
	            $boolIsValid = false;
	            if( true == is_null( $this->getMerchantChangeRequestTypeId() ) || 0 == $this->getMerchantChangeRequestTypeId() ) {
	            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'required_fields', __( 'Select type of request update/add.' ) ) );
	            } else {
	            	if( 1 == $this->getMerchantChangeRequestTypeId() ) {
	            		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'required_fields', __( 'Please select atleast one merchant account.' ) ) );
	            	} else {
	            		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'required_fields', __( 'Please select number of new merchant accounts would you like to create.' ) ) );
	            	}
	            }
	        }
        }

        return $boolIsValid;
    }

    public function valRequestDescription() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strRequestDescription ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'required_fields', __( 'Description of change is required.' ) ) );
        }

        return $boolIsValid;
    }

	public function valSignatureFirstName() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strSignatureFirstName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'signature_first_name', __( 'First name is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valSignatureLastName() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strSignatureLastName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'signature_last_name', __( 'Last name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSignatureTitle() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strSignatureTitle ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'signature_title', __( 'Title is required.' ) ) );
		}

		return $boolIsValid;
	}

    public function valRequestDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFormReceivedOn() {
        $boolIsValid = true;
        if( false == is_null( $this->m_strFormReceivedOn ) ) {

			$strDate1 = strtotime( date( 'm/d/Y' ) );
			$strDate2 = strtotime( mb_substr( $this->m_strFormReceivedOn, 0, 10 ) );

			if( ( $strDate2 - $strDate1 ) < 0 ) {
            	$boolIsValid = false;
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'form_verified_on', __( 'Form received on should be greater than or equal today\'s date.' ) ) );
			}
        } // elseif ( true == is_null( $this->m_strFormReceivedOn ) ) {
            // $boolIsValid = false;
            // $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'form_received_on', 'Form received on is required.' ) );
        // }

        return $boolIsValid;
    }

    public function valSignatureVerifiedOn() {
        $boolIsValid = true;

        if( false == is_null( $this->m_strSignatureVerifiedOn ) ) {

			$strDate1 = strtotime( date( 'm/d/Y' ) );
			$strDate2 = strtotime( mb_substr( $this->m_strSignatureVerifiedOn, 0, 10 ) );

			if( ( $strDate2 - $strDate1 ) < 0 ) {
            	$boolIsValid = false;
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'signature_verified_on', __( 'Signature verified on should be greater than or equal today\'s date.' ) ) );
			}
        } // elseif ( true == is_null( $this->m_strSignatureVerifiedOn ) ) {
            // $boolIsValid = false;
            // $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'signature_verified_on', 'Signature verified on is required.' ) );
        // }

        return $boolIsValid;
    }

    public function valConfirmedOn() {
        $boolIsValid = true;

        if( false == is_null( $this->m_strConfirmedOn ) ) {

			$strDate1 = strtotime( date( 'm/d/Y' ) );
			$strDate2 = strtotime( mb_substr( $this->m_strConfirmedOn, 0, 10 ) );

			if( ( $strDate2 - $strDate1 ) < 0 ) {
            	$boolIsValid = false;
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'confirmed_on', __( 'Confirmed on should be greater than or equal today\'s date.' ) ) );
			}
        } // elseif( true == is_null( $this->m_strConfirmedOn ) ) {
            // $boolIsValid = false;
            // $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'confirmed_on', 'Confirmed on is required.' ) );
        // }

        return $boolIsValid;
    }

    public function valProcessedOn() {
        $boolIsValid = true;

		if( false == is_null( $this->m_strProcessedOn ) ) {
	        $strDate1 = strtotime( date( 'm/d/Y' ) );
	        $strDate2 = strtotime( mb_substr( $this->m_strProcessedOn, 0, 10 ) );

			if( ( $strDate2 - $strDate1 ) < 0 ) {
	        	$boolIsValid = false;
	            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processed_on', __( 'Processed on should be greater than or equal today\'s date.' ) ) );
			}
		} elseif( true == is_null( $this->m_strProcessedOn ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processed_on', __( 'Processed on is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsUpdateAllowed() {
    	$boolIsValid = true;

    	if( false == is_null( $this->m_strProcessedOn ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'required_fields', __( 'Can not update the Request, its already processed.' ) ) );
    	}

        return $boolIsValid;
    }

	public function valRequestorPhoneNumber() {

    	$boolIsValid = true;

    	$intLength = mb_strlen( $this->m_strRequestorPhoneNumber );

    	if( true == is_null( $this->m_strRequestorPhoneNumber ) || 0 == $intLength ) {
	        $boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requestor_phone_number', __( 'Requestor phone number is required.' ) ) );
        	return $boolIsValid;
    	}

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requestor_phone_number', __( 'Requestor phone number must be between 10 and 15 characters.' ), 504 ) );
			$boolIsValid = false;

		} elseif( true == isset( $this->m_strRequestorPhoneNumber ) && false == ( CValidation::validateFullPhoneNumber( $this->m_strRequestorPhoneNumber, false ) ) ) {

		 	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requestor_phone_number', __( 'Valid requestor phone number is required.' ), 504 ) );
			$boolIsValid = false;
		}

        return $boolIsValid;
    }

 	public function valRequestorEmailAddress() {

        $boolIsValid = true;

        if( false == isset( $this->m_strRequestorEmailAddress ) || 0 == mb_strlen( trim( $this->m_strRequestorEmailAddress ) ) || false == CValidation::validateEmailAddresses( $this->m_strRequestorEmailAddress ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requestor_email_address', __( 'Requestor email is required.' ) ) );
            return $boolIsValid;
		}
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valRequestorName();
            	$boolIsValid &= $this->valRequestorEmailAddress();
            	$boolIsValid &= $this->valRequestorPhoneNumber();
				if( 1 == $this->getMerchantChangeRequestTypeId() ) {
					$boolIsValid &= $this->valRequiredFields();
					$boolIsValid &= $this->valRequestDescription();
				}
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valRequestorName();
            	$boolIsValid &= $this->valRequestorEmailAddress();
            	$boolIsValid &= $this->valRequestorPhoneNumber();
				if( 1 == $this->getMerchantChangeRequestTypeId() ) {
					$boolIsValid &= $this->valRequiredFields();
					$boolIsValid &= $this->valRequestDescription();
				}
            	$boolIsValid &= $this->valIsUpdateAllowed();
            	break;

			case 'validate_international_save':
				$boolIsValid &= $this->valRequestorName();
				$boolIsValid &= $this->valRequestorEmailAddress();
				if( 1 == $this->getMerchantChangeRequestTypeId() ) {
					$boolIsValid &= $this->valRequiredFields();
					$boolIsValid &= $this->valRequestDescription();
				}
				break;

			case 'validate_signature':
				$boolIsValid &= $this->valSignatureFirstName();
				$boolIsValid &= $this->valSignatureLastName();
				$boolIsValid &= $this->valSignatureTitle();
				break;

            case 'confirm_merchant_change_request':
            	$boolIsValid &= $this->valConfirmedOn();
            	break;

            case 'flag_merchant_change_request_as_processed':
            	$boolIsValid &= $this->valProcessedOn();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function sendNotificationEmail( $boolIsProcessed = false, $boolIsBillingChangesRequested, $boolIsReturnChangesRequested, $objAdminDatabase, $objPaymentDatabase, $objEmailDatabase = NULL ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			$objEmailDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::PS_PROPERTYMANAGER );
		}

		$objClient = $this->getOrFetchClient( $objAdminDatabase );
		$arrobjMerchantChangeRequestAccounts = $this->fetchMerchantChangeRequestAccounts( $objPaymentDatabase );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

		$objSmarty->assign_by_ref( 'merchant_change_request', 		$this );
		$objSmarty->assign_by_ref( 'client', 			$objClient );
		$objSmarty->assign_by_ref( 'is_processed', 					$boolIsProcessed );
		$objSmarty->assign_by_ref( 'is_billing_change_requested',	$boolIsBillingChangesRequested );
		$objSmarty->assign_by_ref( 'is_return_changes_requested', 	$boolIsReturnChangesRequested );
		$objSmarty->assign_by_ref( 'merchant_change_request_accounts', $arrobjMerchantChangeRequestAccounts );
		$objSmarty->assign( 'CONFIG_ENTRATA_LOGO_PREFIX', 			CONFIG_ENTRATA_LOGO_PREFIX );
		$objSmarty->assign( 'server_name',							CONFIG_INSECURE_HOST_PREFIX . 'www.' . CONFIG_COMPANY_BASE_DOMAIN );
		$objSmarty->assign( 'HTTP_COMPANY_BASE_URL',				CONFIG_INSECURE_HOST_PREFIX . 'www.' . CONFIG_COMPANY_BASE_DOMAIN );

        $strHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/merchant_change_requests/merchant_change_request_email.tpl' );

		$objSystemEmail = $objClient->createSystemEmail( CSystemEmailType::PS_TO_COMPANY_ACCOUNTING );
		$objSystemEmail->setHtmlContent( $strHtmlEmailOutput );

        $arrobjSystemEmails   = array();

        if( false == $boolIsProcessed ) {

        	$objSystemEmail->setSubject( __( 'New merchant change request {%d, 0} submitted by {%s, 1}', [ $this->getId(), $objClient->getCompanyName() ] ) );

        	if( true == $boolIsBillingChangesRequested ) {
        		$arrstrToEmailAddress = array( CSystemEmail::MERCHANTSERVICES_EMAIL_ADDRESS, CSystemEmail::VTORY_EMAIL_ADDRESS );
        	} else {
        		$arrstrToEmailAddress = array( CSystemEmail::MERCHANTSERVICES_EMAIL_ADDRESS, CSystemEmail::VTORY_EMAIL_ADDRESS );
        	}

        } else {
        	$objSystemEmail->setSubject( __( 'Merchant change request processed #{%d, 0} submitted by {%s, 1}', [ $this->getId(), $objClient->getCompanyName() ] ) );

        	if( 0 < mb_strlen( $this->getRequestorEmailAddress() ) ) {
        		$arrstrGetRequestorEmailAddresses 	 = explode( ',', $this->getRequestorEmailAddress() );
        		$arrstrToEmailAddress = array( CSystemEmail::MERCHANTSERVICES_EMAIL_ADDRESS );
        		$arrstrToEmailAddress = array_merge( $arrstrGetRequestorEmailAddresses, $arrstrToEmailAddress );
        	} else {
        		$arrstrToEmailAddress = array( CSystemEmail::MERCHANTSERVICES_EMAIL_ADDRESS );
        	}

        	// Is New account is processed the add email CSystemEmail::RLOCKWOOD_EMAIL_ADDRESS, dont send email if update request is processed.
        	if( true == valArr( $arrobjMerchantChangeRequestAccounts ) ) {
        		$objMerchantChangeRequestAccount = current( $arrobjMerchantChangeRequestAccounts );
        		if( true == $objMerchantChangeRequestAccount->getIsNewAccountRequest() ) {
        			$arrstrToEmailAddress = array_merge( array( CSystemEmail::RLOCKWOOD_EMAIL_ADDRESS ), $arrstrToEmailAddress );
        		}

        		$arrintCompanyMerchantAccountIds = array_keys( rekeyObjects( 'CompanyMerchantAccountId', $arrobjMerchantChangeRequestAccounts ) );
        		if( true == valArr( $arrintCompanyMerchantAccountIds ) && true == in_array( CMerchantAccount::MA_BROADMOOR_VILLAGE, $arrintCompanyMerchantAccountIds ) ) {
        			array_push( $arrstrToEmailAddress, 'sfbcreditadministration@capitalsourcebank.com' ); // For MA id = 38777,need to send mail sfbcreditadministration@capitalsourcebank.com
        		}

        	}

        	$objPsLeadDetail = $objClient->fetchPsLeadDetail( $objAdminDatabase );

        	// Send Notification email to Implementation manager when merchant change request is processed.
        	if( true == valObj( $objPsLeadDetail, 'CPsLeadDetail' ) ) {

        		$arrintEmployeeIds = array();

        		if( false == is_null( $objPsLeadDetail->getSupportEmployeeId() ) ) {
        			array_push( $arrintEmployeeIds, $objPsLeadDetail->getSupportEmployeeId() );
        		}

        		if( false == is_null( $objPsLeadDetail->getImplementationEmployeeId() ) ) {
        			array_push( $arrintEmployeeIds, $objPsLeadDetail->getImplementationEmployeeId() );
        		}

        		if( true == valArr( $arrintEmployeeIds ) ) {

        			$arrobjEmployees = CEmployees::fetchActiveEmployeesByIds( $arrintEmployeeIds, $objAdminDatabase );

        			if( true == valArr( $arrobjEmployees ) ) {
        	    		$arrstrToEmailAddress = array_filter( array_merge( array_keys( rekeyObjects( 'emailaddress', $arrobjEmployees ) ), $arrstrToEmailAddress ) );
        			}
        		}
        	}
        }

        if( true == valArr( $arrstrToEmailAddress ) ) {
			foreach( $arrstrToEmailAddress as $strEmailAddress ) {
				$objSystemEmail->setToEmailAddress( $strEmailAddress );
				$arrobjSystemEmails[] = clone $objSystemEmail;
			}
        }

        if( true == valArr( $arrobjSystemEmails ) ) {
			foreach( $arrobjSystemEmails as $objSystemEmail ) {
				$objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase );
			}
        }
    }

    public function getOrFetchClient( $objAdminDatabase ) {
    	if( true == valObj( $this->m_objClient, 'CClient' ) ) return $this->m_objClient;

   		$this->m_objClient = $this->fetchClient( $objAdminDatabase );
   		return $this->m_objClient;
    }

	public function buildDefaultSignatureFilePath() {
		return 'merchant_change_request/' . date( 'Y' ) . '/' . date( 'n' ) . '/' . date( 'j' ) . '/';
	}

	public function buildDefaultSignatureFullFilePath() {
		return getMountsPath( $this->getCid(), PATH_MOUNTS_DOCUMENTS ) . $this->buildDefaultSignatureFilePath();
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
    	$this->setId( $this->fetchNextId( $objDatabase ) );

    	return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	// This function is intentionally overridden in order to jump some range of ids from payments.ar_payments table.
	// Do not move this function from here.

	public function fetchNextId( $objDatabase, $strTableName = NULL ) {
		$this->updateIdSequence( $objDatabase );

		return parent::fetchNextId( $objDatabase );
	}

	public function updateIdSequence( $objDatabase, $intSequenceIncrementValue = 1 ) {
		$strSql = 'SELECT last_value AS id FROM merchant_change_requests_id_seq';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( false != valArr( $arrmixData ) && false != isset( $arrmixData[0]['id'] ) ) {
			$intStartingId = $arrmixData[0]['id'];
			$intEndingId = $intStartingId + $intSequenceIncrementValue;
			if( CONFIG_CLOUD_ID == CCloud::LINDON_ID && self::MAX_ID_LINDON < $intEndingId ) {
				// we're in the Lindon cloud and the payment id is greater than 100 million
				$strSql = 'SELECT id FROM merchant_change_requests WHERE id < ' . self::MAX_ID_LINDON . ' ORDER BY id DESC LIMIT 1';
				$intMinIdForCloud = 1;
			} elseif( CONFIG_CLOUD_ID == CCloud::IRELAND_ID && ( self::MAX_ID_IRELAND < $intEndingId || self::MIN_ID_IRELAND > $intStartingId ) ) {
				// we're in the Ireland cloud and the payment id is not between 100 and 200 million
				$strSql = 'SELECT id FROM merchant_change_requests WHERE id BETWEEN ' . self::MIN_ID_IRELAND . ' AND ' . self::MAX_ID_IRELAND . ' ORDER BY id DESC LIMIT 1';
				$intMinIdForCloud = self::MIN_ID_IRELAND;
			} else {
				// no adjustment necessary
				return $intStartingId;
			}

			$arrmixResult = fetchData( $strSql, $objDatabase );
			if( !valArr( $arrmixResult ) ) {
				$intMaxId = $intMinIdForCloud;
			} else {
				$intMaxId = $arrmixResult[0]['id'];
			}
			$intNextId = $intMaxId + 1;
			$strSql = 'SELECT setval( \'merchant_change_requests_id_seq\', ' . ( int ) $intNextId . ', FALSE )';
			fetchData( $strSql, $objDatabase );

			return $intNextId;
		}
	}

}
?>