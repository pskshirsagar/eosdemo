<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaFileBatches
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CNachaFileBatches extends CBaseNachaFileBatches {

	public static function fetchNachaFileBatchesByNachaFileId( $intNachaFileId, $objDatabase ) {

		$strSql = 'SELECT * FROM nacha_file_batches WHERE nacha_file_id = ' . ( int ) $intNachaFileId;
		return self::fetchNachaFileBatches( $strSql, $objDatabase );
	}

}
?>