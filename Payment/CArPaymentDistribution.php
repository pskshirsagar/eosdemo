<?php

class CArPaymentDistribution extends CBaseArPaymentDistribution {

	protected $m_objArPayment;
	protected $m_intPaymentTypeId;
	protected $m_intPropertyId;
	protected $m_boolIsSplitArPayment;

    /**
     * Set Functions
     */

    public function setArPayment( $objArPayment ) {
    	$this->m_objArPayment = $objArPayment;
    }

    public function setIsSplitArPayment( $boolIsSplitArPayment ) {
    	$this->m_boolIsSplitArPayment = $boolIsSplitArPayment;
    }

    public function setPropertyId( $intPropertyId ) {
    	$this->m_intPropertyId = $intPropertyId;
    }

 	public function setPaymentTypeId( $intPaymentTypeId ) {
    	$this->m_intPaymentTypeId = $intPaymentTypeId;
    }

 	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( true == isset( $arrValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrValues['payment_type_id'] );
		return;
    }

    /**
     * Get Functions
     */

    public function getArPayment() {
    	return $this->m_objArPayment;
    }

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

 	public function getPaymentTypeId() {
    	return $this->m_intPaymentTypeId;
    }

 	public function getIsSplitArPayment() {
    	return $this->m_boolIsSplitArPayment;
    }

   /**
    * Fetch Functions
    */

    public function fetchArPayment( $objPaymentDatabase ) {
    	$this->m_objArPayment = CArPayments::fetchCustomArPaymentByIdByCid( $this->m_intArPaymentId, $this->m_intCid, $objPaymentDatabase );
    	return $this->m_objArPayment;
    }

    /**
     * Validation Functions
     */

    public function valSettlementDistributionId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getSettlementDistributionId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'settlement_distribution_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valArPaymentId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getArPaymentId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_payment_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDistributionTypeId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getDistributionTypeId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'distribution_type_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDistributionAmount() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getDistributionAmount() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'distribution_amount', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>