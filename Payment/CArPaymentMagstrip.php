<?php

class CArPaymentMagstrip extends CBaseArPaymentMagstrip {

	const ROUTING_NUMBER_LENGTH = 9;

	const VALIDATE_MICR = 'VALIDATE_MICR';

	protected $m_strCheckAuxillaryOnUs;
	protected $m_strOnUs;
	protected $m_strExternalProcessingCode;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckAccountNumber;
	protected $m_strCheckNumber;
	protected $m_boolCheckNumberMandatory;

    /**
     * Set Functions
     */

	public function setEncryptedMagstripData( $strMagstripData ) {
		$this->m_strMagstripData = CStrings::strTrimDef( $strMagstripData, -1, NULL, true );
		if( !valStr( $this->m_strMagstripData ) ) {
			return;
		}
		$this->m_strMagstripData = \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $this->m_strMagstripData, CONFIG_SODIUM_KEY_MAGSTRIP_DATA );
	}

    public function setCheckAuxillaryOnUs( $strCheckAuxillaryOnUs ) {
		$this->m_strCheckAuxillaryOnUs = $strCheckAuxillaryOnUs;
    }

	public function setOnUs( $strOnUs ) {
		$this->m_strOnUs = $strOnUs;
	}

    public function setEpc( $strExternalProcessingCode ) {
		$this->m_strExternalProcessingCode = $strExternalProcessingCode;
    }

    public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->m_strCheckRoutingNumber = $strCheckRoutingNumber;
    }

    public function setCheckAccountNumber( $strCheckAccountNumber ) {
		$this->m_strCheckAccountNumber = $strCheckAccountNumber;
    }

    public function setCheckNumber( $strCheckNumber ) {
		$this->m_strCheckNumber = $strCheckNumber;
    }

    public function setIsCheckNumberMandatory( $boolCheckNumberMandatory ) {
		$this->m_boolCheckNumberMandatory = $boolCheckNumberMandatory;
    }

    /**
     * Get Functions
     */

    public function getDecryptedMagstripData() {
    	if( false == valStr( $this->m_strMagstripData ) ) {
    		return;
	    }
	    return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strMagstripData, CONFIG_SODIUM_KEY_MAGSTRIP_DATA, [ 'legacy_secret_key' => CONFIG_KEY_MAGSTRIP_DATA ] );
    }

    public function getCheckAuxillaryOnUs() {
		return $this->m_strCheckAuxillaryOnUs;
    }

	public function getOnUs() {
		return $this->m_strOnUs;
	}

    public function getEpc() {
		return $this->m_strExternalProcessingCode;
    }

    public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
    }

    public function getCheckAccountNumber() {
		return $this->m_strCheckAccountNumber;
    }

    public function getCheckNumber() {
		return $this->m_strCheckNumber;
    }

    public function getIsCheckNumberMandatory() {
    	return $this->m_boolCheckNumberMandatory;
    }

    /**
     * Validate Functions
     */

    public function valCheckAuxillaryOnUs() {
    	$boolIsValid = true;

    	if( false == is_null( $this->getCheckAuxillaryOnUs() ) && true == is_numeric( $this->getCheckAuxillaryOnUs() ) ) {

    		if( false == preg_match( '/[0-9]+/', $this->getCheckAuxillaryOnUs() ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_auxillary_number', 'Auxillary number is invalid.', 600 ) );
    		}
    	}
    	return $boolIsValid;
    }

    public function valEpc() {
    	$boolIsValid = true;

    	if( false == is_null( $this->getEpc() ) && true == is_numeric( $this->getEpc() ) ) {
			if( false == preg_match( '/[0-9]{1}/', $this->getEpc() ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'external_processing_code', 'External Processing Code must be a single digit number.', 601 ) );
			}
    	}
    	return $boolIsValid;
    }

    public function valCheckRoutingNumber() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getCheckRoutingNumber() ) ) {

    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number is required.', 602 ) );
    		return false;
    	}

    	if( 0 !== preg_match( '/[^0-9]/', $this->getCheckRoutingNumber() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number must be a number.', 603 ) );
    		return false;
    	}

    	if( self::ROUTING_NUMBER_LENGTH != strlen( $this->getCheckRoutingNumber() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number must be ' . self::ROUTING_NUMBER_LENGTH . ' digits long.', 604 ) );
    		return false;
    	}

    	return $boolIsValid;
    }

    public function valCheckAccountNumber() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getCheckAccountNumber() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', 'Account number is required.', 605 ) );
    	}

    	if( 17 < strlen( $this->getCheckAccountNumber() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', 'Account number should not be more than 17 digits long.', 606 ) );
    	}

    	return $boolIsValid;
    }

    public function valCheckNumber() {
    	$boolIsValid = true;

   		if( false == is_null( $this->m_strCheckNumber ) && 0 < strlen( $this->m_strCheckNumber ) ) {

	    	if( true == valStr( $this->m_strCheckNumber ) && false == preg_match( '/^([A-Za-z0-9]+$)/', $this->m_strCheckNumber ) ) {
	    		$boolIsValid = false;
	    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ach_check_number', 'Check number can be numeric or alphanumeric only.', 607 ) );
	    	}

			if( 20 < strlen( $this->m_strCheckNumber ) ) {

	    		$boolIsValid = false;
	    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ach_check_number', 'Check number cannot exceed 20 characters.', 609 ) );
	    	}
    	}
    	echo 'Check # validation: ';
    	var_dump( $boolIsValid );

    	return $boolIsValid;
    }

    public function valMagstripData() {
        $boolIsValid = true;

        return $boolIsValid;
    }

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case VALIDATE_MICR:
				$boolIsValid &= $this->valCheckAuxillaryOnUs();
				$boolIsValid &= $this->valEpc();
				$boolIsValid &= $this->valCheckRoutingNumber();
				// $boolIsValid &= $this->valCheckAccountNumber();
				// $boolIsValid &= $this->valCheckNumber();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
    }

    /**
     * Helper Functions
     */

	public function parseAuxillaryOnUsFromMagstripData( $strMagstripData ) {
		$strCheckAuxillaryOnUs = '';
		$intUCount = 0;

		$intMagstripDataLength = strlen( $strMagstripData );
		for( $x = 0; $x < $intMagstripDataLength; $x++ ) {

			if( 'U' != $strMagstripData[0] ) {
				break;
			}

			if( 'U' == $strMagstripData[$x] ) {
				$intUCount += 1;
				continue;
			}

			if( 2 > $intUCount ) {
				$strCheckAuxillaryOnUs .= $strMagstripData[$x];
			} else {
				break;
			}
		}

		return $strCheckAuxillaryOnUs;
	}

	public function parseEpcFromMagstripData( $strMagstripData ) {
		$strEpc = '';
		$intUCount = 0;
		$intTCount = 0;

		$intMagstripDataLength = strlen( $strMagstripData );
		for( $x = 0; $x < $intMagstripDataLength; $x++ ) {

			if( 'U' != $strMagstripData[0] ) {
				break;
			}

			if( 'U' == $strMagstripData[$x] ) {
				$intUCount += 1;
				continue;
			}

			if( 'T' == $strMagstripData[$x] ) {
				$intTCount += 1;
				break;
			}

			if( 2 == $intUCount && 0 == $intTCount ) {
				$strEpc .= $strMagstripData[$x];
			}
		}

		return $strEpc;
	}

	public function parseOnUsFromMagstripData( $strMagstripData ) {
		$strOnUs = '';
		$intTCount = 0;
		$intDollarSignCount = 0;

		$intMagstripDataLength = strlen( $strMagstripData );
		for( $x = 0; $x < $intMagstripDataLength; $x++ ) {

			if( 'T' == $strMagstripData[$x] ) {
				$intTCount += 1;
				continue;
			}

			if( '$' == $strMagstripData[$x] ) {
				$intDollarSignCount += 1;
				continue;
			}

			if( 2 == $intTCount && 0 == $intDollarSignCount ) {
				$strOnUs .= $strMagstripData[$x];
			}
		}

		$strOnUs = str_replace( 'U', '/', $strOnUs );

		return $strOnUs;
	}

	public function parseRoutingNumberFromMagstripData( $strMagstripData ) {
		$strCheckRoutingNumber = '';
		$intTCount = 0;

		$intMagstripDataLength = strlen( $strMagstripData );
		for( $x = 0; $x < $intMagstripDataLength; $x++ ) {

			if( 'T' == $strMagstripData[$x] ) {
				$intTCount += 1;
				continue;
			}

			if( 2 == $intTCount ) {
				break;
			}

			if( 1 == $intTCount ) {
				$strCheckRoutingNumber .= $strMagstripData[$x];
			}
		}

		return $strCheckRoutingNumber;
	}

	public function parseAccountNumberFromMagstripOnUsData( $strOnUs ) {
		$strCheckAccountNumber = strstr( $strOnUs, '/', true );

		return $strCheckAccountNumber;
	}

	public function parseCheckNumberFromMagstripOnUsData( $strOnUs ) {
		$strCheckNumber = substr( $strOnUs, ( strpos( $strOnUs, '/' ) + 1 ) );

		return $strCheckNumber;
	}

	public function parseAndValidateMagstripData() {

		if( NULL == $this->getDecryptedMagstripData() ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, NULL, 'Magstrip data not set' ) );
			return false;
		}

		if( false == is_string( $this->getDecryptedMagstripData() ) ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, NULL, 'Magstrip data is not a string' ) );
			return false;
		}

		if( 0 >= strlen( $this->getDecryptedMagstripData() ) ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, NULL, 'Invalid magstrip data' ) );
			return false;
		}

		// Determine the Auxillary On-Us Data
		$strCheckAuxillaryOnUs = $this->parseAuxillaryOnUsFromMagstripData( $this->getDecryptedMagstripData() );

		// Determine EPC
		$strEpc = $this->parseEpcFromMagstripData( $this->getDecryptedMagstripData() );

		// Determine the OnUs data
		$strOnUs = $this->parseOnUsFromMagstripData( $this->getDecryptedMagstripData() );

		$strCheckAccountNumber = $this->parseAccountNumberFromMagstripOnUsData( $strOnUs );
		$strCheckNumber = $this->parseCheckNumberFromMagstripOnUsData( $strOnUs );

		// Determine the Routing data
		$strCheckRoutingNumber = $this->parseRoutingNumberFromMagstripData( $this->getDecryptedMagstripData() );

		$this->setCheckAccountNumber( $strCheckAccountNumber );
		$this->setCheckNumber( $strCheckNumber );
		$this->setCheckAuxillaryOnUs( $strCheckAuxillaryOnUs );
		$this->setCheckRoutingNumber( $strCheckRoutingNumber );
		$this->setEpc( $strEpc );
		$this->setOnUs( $strOnUs );

		return $this->validate( VALIDATE_MICR );
	}

}
