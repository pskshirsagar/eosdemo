<?php

class CReconciliation extends CBaseReconciliation {

	protected $m_arrobjReconciliationEntries;

	/**
	 * Validate Functions
	 */

    public function valId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ));
        // }

        return $boolIsValid;
    }

    public function valMemo() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getMemo())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'memo', '' ));
        // }

        return $boolIsValid;
    }

    public function valFromDate() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getFromDate())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'from_date', '' ));
        // }

        return $boolIsValid;
    }

    public function valToDate() {
        $boolIsValid = true;

        if( true == is_null( $this->getToDate() ) || false == CValidation::validateDate( $this->getToDate() ) ) {
        	$boolIsValid = false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'to_date', "Valid 'To date' is required." ) );
        }

        if( false == is_null( $this->getToDate() ) && false == is_null( $this->getFromDate() ) ) {

			$arrToDate 	 = explode( '/', $this->getToDate() );
			$arrFromDate = explode( '/', $this->getFromDate() );
			$intToDate   = mktime( 0, 0, 0, $arrToDate[0], $arrToDate[1], $arrToDate[2] );
			$intFromDate = mktime( 0, 0, 0, $arrFromDate[0], $arrFromDate[1], $arrFromDate[2] );
        	if( $intFromDate > $intToDate ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'to_date', " 'To Date' must be greater than 'From Date'." ) );
        	}
        }
        return $boolIsValid;
    }

    public function valBeginningBalance() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getBeginningBalance())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'beginning_balance', '' ));
        // }

        return $boolIsValid;
    }

    public function valEndingBalance() {
        $boolIsValid = true;
        if( true == is_null( $this->getEndingBalance() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ending_balance', 'Ending Balance Required' ) );
        }

        return $boolIsValid;
    }

    public function valAdjustmentAmount() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getAdjustmentAmount())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'adjustment_amount', '' ));
        // }

        return $boolIsValid;
    }

  	public function valProcessingBankAccountId() {
  		$boolIsValid = true;

        if( true == is_null( $this->getProcessingBankAccountId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_bank_account', 'Processing Bank Account Required' ) );
        }

        return $boolIsValid;
        return true;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valToDate();
            	$boolIsValid &= $this->valEndingBalance();
            	$boolIsValid &= $this->valProcessingBankAccountId();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Fetch Functions
     */

    public function fetchReconciliationEntries( $objPaymentDatabase ) {
    	return \Psi\Eos\Payment\CReconciliationEntries::createService()->fetchReconciliationEntriesByReconciliationId( $this->getId(), $objPaymentDatabase );
    }

	public function fetchAllReconciledReconciliationEntries( $objPaymentDatabase, $strToDate = NULL ) {
		return \Psi\Eos\Payment\CReconciliationEntries::createService()->fetchAllReconciledReconciliationEntriesByReconciliationId( $this->getId(), $objPaymentDatabase, $strToDate = NULL );
	}

    /**
     * Create Functions
     */

     public function createReconciliationEntry() {

    	$objReconciliationEntry = new CReconciliationEntry();
    	$objReconciliationEntry->setDefaults();
    	$objReconciliationEntry->setProcessingBankAccountId( $this->getProcessingBankAccountId() );

    	return $objReconciliationEntry;
     }

    /**
     * Add Functions
     */

    public function addReconciliationEntry( $objReconciliationEntry ) {

    	if( false == is_null( $objReconciliationEntry ) ) {
    		if( true == valArr( $this->m_arrobjReconciliationEntries ) ) {
    			$this->m_arrobjReconciliationEntries[] 	= $objReconciliationEntry;
    		} else {
    			$this->m_arrobjReconciliationEntries 	= Array();
    			$this->m_arrobjReconciliationEntries[] 	= $objReconciliationEntry;
    		}
    	}
    }

    /**
     * Get Functions
     */

    public function getReconciliationEntries() {
    	return $this->m_arrobjReconciliationEntries;
    }

}
?>