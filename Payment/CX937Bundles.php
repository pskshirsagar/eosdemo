<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937Bundles
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CX937Bundles extends CBaseX937Bundles {

	public static function fetchX937BundlesByX937FileId( $intX937FileId, $objDatabase ) {

		$strSql = 'SELECT * FROM x937_bundles WHERE x937_file_id = ' . ( int ) $intX937FileId;
		return self::fetchX937Bundles( $strSql, $objDatabase );
	}

}
?>