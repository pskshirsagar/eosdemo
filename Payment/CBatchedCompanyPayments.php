<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CBatchedCompanyPayments
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CBatchedCompanyPayments extends CBaseBatchedCompanyPayments {

	public static function fetchCompanyPaymentIdsByEftBatchId( $intEftBatchId, $objDatabase ) {
		$strSql = 'SELECT
						bcp.company_payment_id
					 FROM
						batched_company_payments bcp
					WHERE
						eft_batch_id = ' . ( int ) $intEftBatchId . '
					UNION
				   SELECT
						bcp2.company_payment_id
					 FROM
						batched_company_payments bcp2
			   INNER JOIN
						eft_batches eb ON eb.id = bcp2.eft_batch_id
					WHERE eb.aggregate_eft_batch_id = ' . ( int ) $intEftBatchId;

		$arrintResults = NULL;

        if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( 'Incorrect Database Instance', E_USER_WARNING );
        }

        $objDataset = $objDatabase->createDataset();

        if( false == $objDataset->execute( $strSql ) ) {
            $objDataset->cleanup();
            return NULL;
        }

        if( 0 < $objDataset->getRecordCount() ) {
            $arrobjTargetObjects = array();

            while( false == $objDataset->eof() ) {
                $arrmixValues = $objDataset->fetchArray();

				$arrintResults[] = $arrmixValues['company_payment_id'];

                $objDataset->next();
            }
        }

        $objDataset->cleanup();

        return $arrintResults;
	}

}
?>