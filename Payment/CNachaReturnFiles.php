<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaReturnFiles
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CNachaReturnFiles extends CBaseNachaReturnFiles {

	public static function fetchUnProcessedNachaReturnFilesByProcessingBankId( $intProcessingBankId, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM nacha_return_files WHERE returns_processed_on IS NULL AND processing_bank_id = ' . ( int ) $intProcessingBankId . ' ORDER BY created_on';
		return self::fetchNachaReturnFiles( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnConfirmedNachaReturnFilesByProcessingBankId( $intProcessingBankId, $objPaymentDatabase ) {
	    $strSql = 'SELECT * FROM nacha_return_files WHERE confirmed_on IS NULL AND processing_bank_id = ' . ( int ) $intProcessingBankId . ' ORDER BY id ASC';
	    return self::fetchNachaReturnFiles( $strSql, $objPaymentDatabase );
	}

	public static function fetchPaginatedNachaReturnFiles( $intPageNo, $intPageSize, $objPaymentDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    	$intLimit = ( int ) $intPageSize;

    	$strSql = self::buildPagnationSql() . '
			   		OFFSET ' . ( int ) $intOffset . '
			   		LIMIT ' . $intLimit;

    	return self::fetchNachaReturnFiles( $strSql, $objPaymentDatabase );
	}

	public static function fetchPaginatedNachaReturnFilesCount( $objPaymentDatabase ) {
    	$strSql = 'SELECT count(id) FROM ( ' . self::buildPagnationSql() . ' ) AS sub1';

    	$arrintResponse = fetchData( $strSql, $objPaymentDatabase );

    	if ( true == isset ( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

    	return 0;
	}

	public static function buildPagnationSql() {

		// record transaction code (27,37) is what is set when the nacha entry detail record is a debit.

		$strSql = "SELECT
					    nrf.id as id,
						nrf.nacha_file_type_id,
					    nrf.nacha_file_status_type_id,
					    nrf.merchant_gateway_id,
					    nrf.file_name,
					    nrf.nacha_file_datetime,
					    nrf.returns_processed_on,
					    nrf.confirmed_on,
					    nrf.created_on,
						sum( nedr.formatted_amount ) as total
					FROM
						nacha_return_addenda_detail_records nradr,
						nacha_return_detail_records nrdr,
					    nacha_return_files nrf,
						( SELECT
						        CASE
						            WHEN record_transaction_code IN ( '27', '37' )
						            THEN ( formatted_amount * -1 )
						            ELSE formatted_amount
						        END AS formatted_amount,
						        id
						   FROM
						       nacha_entry_detail_records ) nedr
					WHERE
						nradr.nacha_return_detail_record_id = nrdr.id
				    	AND nrdr.nacha_return_file_id = nrf.id
						AND nradr.nacha_entry_detail_record_id = nedr.id
						AND nradr.is_notice_of_change <> 1
					GROUP BY
						nrf.id,
						nrf.nacha_file_type_id,
						nrf.nacha_file_status_type_id,
						nrf.merchant_gateway_id,
						nrf.nacha_file_datetime,
						nrf.returns_processed_on,
						nrf.confirmed_on,
						nrf.file_name,
						nrf.created_on
				    ORDER BY nrf.created_on DESC";

		return $strSql;
	}

}

?>