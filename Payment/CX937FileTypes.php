<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937FileTypes
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */
class CX937FileTypes extends CBaseX937FileTypes {

	public static function fetchX937FileTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CX937FileType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchX937FileType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CX937FileType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllX937FileTypes( $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM x937_file_types ORDER BY order_num';
		return self::fetchX937FileTypes( $strSql, $objPaymentDatabase );
	}

}
?>