<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CBinFlags
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CBinFlags extends CBaseBinFlags {

	public static function fetchBinFlags( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CBinFlag::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchBinFlag( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CBinFlag::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>