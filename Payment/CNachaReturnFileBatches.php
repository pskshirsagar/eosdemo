<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaReturnFileBatches
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CNachaReturnFileBatches extends CBaseNachaReturnFileBatches {

	public static function fetchNachaReturnFileBatchesByNachaReturnFileId( $intNachaReturnFileId, $objDatabase ) {

		$strSql = 'SELECT * FROM nacha_return_file_batches WHERE nacha_return_file_id = ' . ( int ) $intNachaReturnFileId;
		return self::fetchNachaReturnFileBatches( $strSql, $objDatabase );
	}

}
?>