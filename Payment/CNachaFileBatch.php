<?php

class CNachaFileBatch extends CBaseNachaFileBatch {

	protected $m_arrobjNachaEntryDetailRecords;

    public function __construct() {
        parent::__construct();

        $this->m_arrobjNachaEntryDetailRecords = array();

        return;
    }

	/**
	 * Add Functions
	 */

	public function addNachaEntryDetailRecord( $objNachaEntryDetailRecord ) {
		$this->m_arrobjNachaEntryDetailRecords[$objNachaEntryDetailRecord->getId()] = $objNachaEntryDetailRecord;
	}

	/**
	 * Create Functions
	 */

    public function createEntryDetailRecord() {

    	$objNachaEntryDetailRecord = new CNachaEntryDetailRecord();
    	$objNachaEntryDetailRecord->setDefaults();
    	$objNachaEntryDetailRecord->setNachaFileId( $this->m_intNachaFileId );
    	$objNachaEntryDetailRecord->setNachaFileBatchId( $this->m_intId );

    	return $objNachaEntryDetailRecord;
    }

	/**
	 * Set Functions
	 */

	public function getNachaEntryDetailRecords() {
		return $this->m_arrobjNachaEntryDetailRecords;
	}

	/**
	 * Set Functions
	 */

    public function setDefaults() {

    	// Header Record

    	// Size = 1 : Identifies this as a batch header (Files can have multiple batch headers)
    	$this->setHeaderRecordTypeCode( '5' );

    	// Size = 3 : This has codes that are important.  200 for mixed debits and credits.  220 for credits only, 225 for debits only, and 280 for Ach Automated Accounting Advices (what is this last one) See page OR 76 of ACH book.
    	$this->setHeaderServiceClassCode( '200' );

    	// Size = 16 : Looks like we should always use this as company name.  This shows up on resident bank account. (put psi/Shady Groves)
    	// $this->setHeaderCompanyName( 'PSI/Shady Groves' );

    	// Size = 10 : This is PS EIN
    	// $this->setHeaderCompanyIdentification( '9861072180' );

    	// Size = 3 : This is the standard entry class code.  Looks like has to be consistent across all records in a batch.  We will use CCD on company checking and savings accounts I think.
    	// $this->setHeaderStandardEntryClassCode( 'WEB' );

    	// Size = 10 : This can change depending on whether this is a distribution or original payment.
    	// $this->setHeaderCompanyEntryDescription( 'Rents' );

    	// Size = 6 : This is the date the resident should see on their statement.  We should make it the next banking day. (MMDDYY) (We should leave this blank)
    	$this->setHeaderCompanyDescriptiveDate( date( 'ymd' ) );

    	// Size = 5	: This should be the date when credits and debits post.  It should always be the next banking day. (YYMMDD)
    	$this->setHeaderEffectiveEntryDate( date( 'ymd' ) );

    	// Size = 3 : This field should be left blank to be filled by 1st Regional.  They have ultimate discretion over when things process.
    	$this->setHeaderSettlementDate( '   ' );

    	// Size = 1 : This code identifies the Originator as a depository financial institution which has agreed to be bound by these rules.
    	$this->setHeaderOriginatorStatusCode( '1' );

    	// Size = 8 : This is the routing number of originating depository financial institution (first regional).
    	// $this->setHeaderOriginatingDfiIdentification( '12224259' );

    	// Size = 7 : This should be an ascending number (payment_batch id).  This has to be the same number on the batch control record.
    	// $this->setHeaderBatchNumber();

    	// Control Record

    	// Size = 1 : See Batch header "BATCH_HEADER_SERVICE_CLASS_CODE"
    	$this->setControlRecordTypeCode( '8' );

    	// Size = 3 : See Batch header "BATCH_HEADER_SERVICE_CLASS_CODE"
    	$this->setControlServiceClasscode( '200' );

    	// Size = 6 : This looks like the numbers of records in the batch.  Technical definition: This count is a tally of each Entry Detail Record and each Addenda Record processed, within either the batch or file as appropriate.
    	// $this->setControlEntryAddendaCount();

    	// Size = 10 : ? What is the entry hash and do I have to provide it. See OR 83 add DFI routing number for each detail record.  Use right most 10 ten digits.  Add up routing numbers in the rounting numbers.  Add all routing numbers in detail records.   Only use 8 most digits in routing number.  Pad with zeros to the left.
    	// $this->setControlEntryHash();

    	// Size = 12 : Debit Amount
    	// $this->setControlTotalDebitAmount();

    	// Size = 12 : Credit Amount
    	// $this->setControlTotalCreditAmount();

    	// Size = 10 : PS Info?
    	// $this->setControlCompanyIdentification( '9861072180' );

    	// Size = 19 : This is an eight character code derived from a special key used in conjunction with the DES algorithm.  The purpose of MAC is to authenticate the validity of ACH entries.  Looks like we don't need this ?  Ask Charles. Optional -- Don't use
    	$this->setControlMessageAuthenticationCode( '                   ' );

    	// Size = 6 : Leave Blank
    	$this->setControlReserved( '      ' );

    	// Size = 8 : See Batch Header
    	// $this->setControlOriginatingDfiIdentification( '12224259' );

    	// Size = 7 : This should be an ascending number (payment_batch id) and should match the opening batch control line
    	// $this->setControlBatchNumber();
    }

    public function setHeaderRecordTypeCode( $strHeaderRecordTypeCode ) {
        $this->m_strHeaderRecordTypeCode = CStrings::strNachaDef( $strHeaderRecordTypeCode, 1, true, false );
    }

    public function setHeaderServiceClassCode( $strHeaderServiceClassCode ) {
        $this->m_strHeaderServiceClassCode = CStrings::strNachaDef( $strHeaderServiceClassCode, 3, true, false );
    }

    public function setHeaderCompanyName( $strHeaderCompanyName ) {
        $this->m_strHeaderCompanyName = CStrings::strNachaDef( $strHeaderCompanyName, 16, false, false );
    }

    public function setHeaderCompanyDiscretionaryData( $strHeaderCompanyDiscretionaryData ) {
        $this->m_strHeaderCompanyDiscretionaryData = CStrings::strNachaDef( $strHeaderCompanyDiscretionaryData, 20, false, false );
    }

    public function setHeaderCompanyIdentification( $strHeaderCompanyIdentification ) {
        $this->m_strHeaderCompanyIdentification = CStrings::strNachaDef( $strHeaderCompanyIdentification, 10, false, false );
    }

    public function setHeaderStandardEntryClassCode( $strHeaderStandardEntryClassCode ) {
        $this->m_strHeaderStandardEntryClassCode = CStrings::strNachaDef( $strHeaderStandardEntryClassCode, 3, false, false );
    }

    public function setHeaderCompanyEntryDescription( $strHeaderCompanyEntryDescription ) {
        $this->m_strHeaderCompanyEntryDescription = CStrings::strNachaDef( $strHeaderCompanyEntryDescription, 10, false, false );
    }

    public function setHeaderCompanyDescriptiveDate( $strHeaderCompanyDescriptiveDate ) {
        $this->m_strHeaderCompanyDescriptiveDate = CStrings::strNachaDef( $strHeaderCompanyDescriptiveDate, 6, true, false );
    }

    public function setHeaderEffectiveEntryDate( $strHeaderEffectiveEntryDate ) {
        $this->m_strHeaderEffectiveEntryDate = CStrings::strNachaDef( $strHeaderEffectiveEntryDate, 6, true, false );
    }

    public function setHeaderSettlementDate( $strHeaderSettlementDate ) {
        $this->m_strHeaderSettlementDate = CStrings::strNachaDef( $strHeaderSettlementDate, 3, true, false );
    }

    public function setHeaderOriginatorStatusCode( $strHeaderOriginatorStatusCode ) {
        $this->m_strHeaderOriginatorStatusCode = CStrings::strNachaDef( $strHeaderOriginatorStatusCode, 1, true, false );
    }

    public function setHeaderOriginatingDfiIdentification( $strHeaderOriginatingDfiIdentification ) {
        $this->m_strHeaderOriginatingDfiIdentification = CStrings::strNachaDef( $strHeaderOriginatingDfiIdentification, 8, true, false );
    }

    public function setHeaderBatchNumber( $strHeaderBatchNumber ) {
        $this->m_strHeaderBatchNumber = CStrings::strNachaDef( $strHeaderBatchNumber, 7, true, true, $strPadding = '0' );
    }

    public function setControlRecordTypeCode( $strControlRecordTypeCode ) {
        $this->m_strControlRecordTypeCode = CStrings::strNachaDef( $strControlRecordTypeCode, 1, true, false );
    }

    public function setControlServiceClassCode( $strControlServiceClassCode ) {
        $this->m_strControlServiceClassCode = CStrings::strNachaDef( $strControlServiceClassCode, 3, true, false );
    }

    public function setControlEntryAddendaCount( $strControlEntryAddendaCount ) {
        $this->m_strControlEntryAddendaCount = CStrings::strNachaDef( $strControlEntryAddendaCount, 6, true, false, '0' );
    }

    public function setControlEntryHash( $strControlEntryHash ) {
        $this->m_strControlEntryHash = CStrings::strNachaDef( $strControlEntryHash, 10, true, true, '0' );
    }

    public function setFormattedControlTotalDebitAmount( $strControlTotalDebitAmount ) {
        // Validate to make sure amount is in $$.cc format
    	if( false == stristr( $strControlTotalDebitAmount, '.' ) ) {
    		echo $strControlTotalDebitAmount;
    		trigger_error( 'Total Debit Entry Amount is not in $$.cc format.', E_USER_ERROR );
    		exit;
    	}

    	$this->m_strControlTotalDebitAmount = CStrings::strNachaDef( $strControlTotalDebitAmount, 12, true, false, '0' );
    }

    public function setFormattedControlTotalCreditAmount( $strControlTotalCreditAmount ) {
        // Validate to make sure amount is in $$.cc format
    	if( false == stristr( $strControlTotalCreditAmount, '.' ) ) {
    		trigger_error( 'Total Credit Entry Amount is not in $$.cc format.', E_USER_ERROR );
    		exit;
    	}

        $this->m_strControlTotalCreditAmount = CStrings::strNachaDef( $strControlTotalCreditAmount, 12, true, false, '0' );
    }

    public function setControlCompanyIdentification( $strControlCompanyIdentification ) {
        $this->m_strControlCompanyIdentification = CStrings::strNachaDef( $strControlCompanyIdentification, 10, false, false );
    }

    public function setControlMessageAuthenticationCode( $strControlMessageAuthenticationCode ) {
        $this->m_strControlMessageAuthenticationCode = CStrings::strNachaDef( $strControlMessageAuthenticationCode, 19, true, false );
    }

    public function setControlReserved( $strControlReserved ) {
        $this->m_strControlReserved = CStrings::strNachaDef( $strControlReserved, 6, true, false );
    }

    public function setControlOriginatingDfiIdentification( $strControlOriginatingDfiIdentification ) {
        $this->m_strControlOriginatingDfiIdentification = CStrings::strNachaDef( $strControlOriginatingDfiIdentification, 8, true, false );
    }

    public function setControlBatchNumber( $strControlBatchNumber ) {
        $this->m_strControlBatchNumber = CStrings::strNachaDef( $strControlBatchNumber, 7, true, false, '0' );
    }

	/**
	 * Validate Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>