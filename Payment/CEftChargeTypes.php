<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CEftChargeTypes
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CEftChargeTypes extends CBaseEftChargeTypes {

	public static function fetchEftChargeTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEftChargeType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchEftChargeType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEftChargeType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

}
?>