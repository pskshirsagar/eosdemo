<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CApplicationBankAccounts
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CApplicationBankAccounts extends CBaseApplicationBankAccounts {

     public static function fetchApplicationBankAccountsByNumberRequested( $intApplicationId, $intNumOfAccountsRequested, $objPaymentDatabase ) {

     	$strSql = 'SELECT * FROM application_bank_accounts WHERE merchant_account_application_id = ' . ( int ) $intApplicationId . ' ORDER by order_num';

        $arrobjApplicationBankAccounts = array();
        $objApplicationBankAccount = NULL;

        $objDataset = $objPaymentDatabase->createDataset();

        if( false == $objDataset->execute( $strSql ) ) {
            $objDataset->cleanup();
             $arrobjApplicationBankAccounts = array();

	        while( \Psi\Libraries\UtilFunctions\count( $arrobjApplicationBankAccounts ) < $intNumOfAccountsRequested ) {
	            array_push( $arrobjApplicationBankAccounts, new CApplicationBankAccount() );
	        }

	        return $arrobjApplicationBankAccounts;
        }

        if( 0 < $objDataset->getRecordCount() ) {
            $arrobjApplicationBankAccounts = array();

            while( false == $objDataset->eof() ) {
                if( \Psi\Libraries\UtilFunctions\count( $arrobjApplicationBankAccounts ) < $intNumOfAccountsRequested ) {
                    $arrValues = $objDataset->fetchArray();

                    $objApplicationBankAccount = new CApplicationBankAccount();
                    $objApplicationBankAccount->setValues( $arrValues );
                    $arrobjApplicationBankAccounts[] = $objApplicationBankAccount;
                }

                $objDataset->next();
            }
        }

        $objDataset->cleanup();

        if( false == valArr( $arrobjApplicationBankAccounts ) ) $arrobjApplicationBankAccounts = array();

        while( \Psi\Libraries\UtilFunctions\count( $arrobjApplicationBankAccounts ) < $intNumOfAccountsRequested ) {
            array_push( $arrobjApplicationBankAccounts, new CApplicationBankAccount() );
        }

        return $arrobjApplicationBankAccounts;
     }

    // static function fetchApplicationBankAccounts( $intApplicationId, $intNumOfAccountsRequested, $objPaymentDatabase ) {

        // $strSql="SELECT * FROM application_bank_accounts WHERE merchant_account_application_id='" .  ( int ) $intApplicationId . "' ORDER by order_num";

        // $arrobjApplicationBankAccounts = array();
       // $arrobjApplicationBankAccounts = self::fetchApplicationBankAccountsByNumberRequested( $strSql, $intNumOfAccountsRequested, $objPaymentDatabase );

       // if( false == valArr( $arrobjApplicationBankAccounts ) ) $arrobjApplicationBankAccounts = array();

      //  while( count( $arrobjApplicationBankAccounts ) < $intNumOfAccountsRequested ) {
      //      array_push( $arrobjApplicationBankAccounts, new CApplicationBankAccount() );
      //  }

       // return $arrobjApplicationBankAccounts;
   // }

}
?>