<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CNachaDetailRecordContents
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CNachaDetailRecordContents extends CBaseNachaDetailRecordContents {

	public static function fetchNachaDetailRecordContentsByNachaEntryDetailRecordId( $intNachaEntryDetailRecordId, $objDatabase ) {

		$strSql = 'SELECT * FROM nacha_detail_record_contents WHERE nacha_entry_detail_record_id = ' . ( int ) $intNachaEntryDetailRecordId;
		return self::fetchNachaDetailRecordContents( $strSql, $objDatabase );
	}

	public static function fetchAchReturnDetailsByNachaReturnFileId( $intNachaReturnFileId, $objDatabase ) {
		$strSql = 'SELECT
						nedr.ar_payment_id, nedr.settlement_distribution_id,nedr.company_payment_id,
						CASE
							WHEN
								nedr.entry_detail_record_type_id = 1
							THEN
								nedr.formatted_amount
							ELSE
								( -1 * nedr.formatted_amount )
							END AS Amount,nedr.cid
					FROM
						nacha_return_detail_records nrdr,
						nacha_return_addenda_detail_records nradr,
						nacha_entry_detail_records nedr
					WHERE
                        nrdr.nacha_return_file_id = ' . ( int ) $intNachaReturnFileId . '
					AND
                        nrdr.id = nradr.nacha_return_detail_record_id
					AND
                        nradr.nacha_entry_detail_record_id = nedr.id
					ORDER BY
                        amount';

		return fetchData( $strSql, $objDatabase );
	}

}
?>