<?php

class CAftReturnItem extends CBaseAftReturnItem {

	protected $m_objOriginalAftDetailRecord;
	protected $m_objReturnType;
	protected $m_objSettlementDistribution;

	public function getOriginalAftDetailRecord() : CAftDetailRecord {
		return $this->m_objOriginalAftDetailRecord;
	}

	public function setOriginalAftDetailRecord( CAftDetailRecord $objOriginalAftDetailRecord ) {
		$this->m_objOriginalAftDetailRecord = $objOriginalAftDetailRecord;
	}

	public function getOrFetchReturnType( $objPaymentDatabase ) {
		if( valObj( $this->getReturnType(), CReturnType::class ) ) {
			return $this->getReturnType();
		}

		if( !valId( $this->getReturnTypeId() ) ) {
			return NULL;
		}

		$objReturnType = \Psi\Eos\Payment\CReturnTypes::createService()->fetchReturnTypeById( $this->getReturnTypeId(), $objPaymentDatabase );
		if( !valObj( $objReturnType, CReturnType::class ) ) {
			return NULL;
		}

		$this->m_objReturnType = $objReturnType;

		return $this->m_objReturnType;
	}

	/**
	 * @return CReturnType
	 */
	public function getReturnType() {
		return $this->m_objReturnType;
	}

	public function getOrFetchSettlementDistribution( $objPaymentDatabase ) {
		if( valObj( $this->getSettlementDistribution(), CSettlementDistribution::class ) ) {
			return $this->getSettlementDistribution();
		}

		if( !valId( $this->getSettlementDistribution() ) ) {
			return NULL;
		}

		$objSettlementDistribution = \Psi\Eos\Payment\CSettlementDistributions::createService()->fetchSettlementDistributionById( $this->getSettlementDistributionId(), $objPaymentDatabase );
		if( !valObj( $objSettlementDistribution, CSettlementDistribution::class ) ) {
			return NULL;
		}

		$this->m_objSettlementDistribution = $objSettlementDistribution;

		return $this->m_objSettlementDistribution;
	}

	/**
	 * @return CSettlementDistribution
	 */
	public function getSettlementDistribution() {
		return $this->m_objSettlementDistribution;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAftReturnFileId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSettlementDistributionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEftInstructionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentBlacklistEntryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogicalRecordTypeCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPayableDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDestinationInstitutionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDestinationAccountNumberEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShortName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLongName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameOnAccount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIdentificationNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnInstitutionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnAccountNumberEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnReasonCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCorrectedInstitutionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCorrectedAccountNumberEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsNoticeOfChange() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsReconPrepped() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>