<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\COfacNames
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class COfacNames extends CBaseOfacNames {

	public static function fetchOfacNameTypes( $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT
						ofac_type
					FROM
						ofac_names
					WHERE
						ofac_type IS NOT NULL
					ORDER BY
						ofac_type';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchOfacNamePrograms( $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT
						program
					FROM
						ofac_names
					WHERE
						program IS NOT NULL
					ORDER BY
						program';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedOfacs( $intPageNo = 0, $intPageSize = NULL, $strWhere = NULL, $strOrderByField = NULL, $strOrderField = NULL, $objDatabase ) {

		$strLimitCondition = '';
		if( false == is_null( $intPageSize ) ) $strLimitCondition = ' LIMIT ' . ( int ) $intPageSize;
		$intOffset		= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		$strSql = 'SELECT
						DISTINCT
						ofn.id,
						ofn.name,
						ofn.ofac_type,
						ofn.program,
						oa.address,
						oa.city_or_province
					FROM
						ofac_names ofn
						LEFT JOIN ofac_addresses oa ON( ofn.ofac_id = oa.ofac_id )
						' . $strWhere . '
					ORDER BY ' . $strOrderByField . ' ' . $strOrderField . '  OFFSET ' . ( int ) $intOffset . $strLimitCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchOfacsCount( $strWhere = NULL, $objDatabase ) {

		$strJoinClause = '';
		if( true == valStr( $strWhere ) ) {
			$strJoinClause  = ' LEFT JOIN ofac_addresses oa ON( ofn.ofac_id = oa.ofac_id )';
		}

		$strSql = 'SELECT
						count( ofn.id ) as count
					FROM
						ofac_names ofn' . $strJoinClause . $strWhere;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

}
?>