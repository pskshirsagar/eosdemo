<?php
use Psi\Eos\Admin\CAccounts;

class CNachaEntryDetailRecord extends CBaseNachaEntryDetailRecord {

	protected $m_intNachaFileTypeId;
	protected $m_strOriginalEntryTraceNumber;

	protected $m_strReturnTypeName;
	protected $m_strOffsettingAccountNumber;

	protected $m_objSettlementDistribution;
	protected $m_objCompanyPayment;
	protected $m_objArPayment;
	protected $m_objClearingBatch;

	protected $m_objReturnType;
	protected $m_objNachaReturnAddendaDetailRecord;
	protected $m_objOffsettingNachaEntryDetailRecord;
	protected $m_objClient;
	protected $m_objProcessingBankAccount;
	protected $m_objNachaFile;
	protected $m_strCorrectedRoutingNo;

	protected $m_arrobjNachaDetailRecordContents;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjNachaDetailRecordContents = array();

		return;
	}

	/**
	 * Create Functions
	 */

	public function createReconciliationEntry( $objPaymentDatabase ) {

		$objReconciliationEntry = new CReconciliationEntry();
		$objReconciliationEntry->setDefaults();
		$objReconciliationEntry->setProcessingBankAccountId( $this->getProcessingBankAccountId() );
		$objReconciliationEntry->setNachaEntryDetailRecordId( $this->getId() );
		$objReconciliationEntry->setTraceNumber( $this->getTraceNumber() );

		if( '22' == $this->getRecordTransactionCode() || '32' == $this->getRecordTransactionCode() ) {
			$objReconciliationEntry->setTotal( abs( $this->getFormattedAmount() ) );
		} else {
			$objReconciliationEntry->setTotal( -1 * abs( $this->getFormattedAmount() ) );
		}

		$intReconciliationEntryTypeId = NULL;

		switch( $this->getNachaFileTypeId() ) {
			case CNachaFileType::AR_PAYMENTS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::AR_PAYMENTS;
			    break;

			case CNachaFileType::COMPANY_PAYMENTS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::COMPANY_PAYMENTS;
			    break;

			case CNachaFileType::SETTLEMENT_DISTRIBUTIONS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::SETTLEMENT_DISTRIBUTIONS;
			    break;

			case CNachaFileType::ACH_RETURNS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::ACH_RETURNS;
			    break;

			case CNachaFileType::NACHA_RETURN_ADDENDA_DETAIL_RECORDS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::NACHA_RETURN_ADDENDA_DETAIL_RECORDS;
			    break;

			case CNachaFileType::ACH_CHECK21_INTERMEDIARY_DISTRIBUTIONS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::ACH_CHECK21_INTERMEDIARY_DISTRIBUTIONS;
			    break;

			case CNachaFileType::ACH_CHECK21_RETURN_INTERMEDIARY_DEBITS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::ACH_CHECK21_RETURN_INTERMEDIARY_DEBITS;
			    break;

			case CNachaFileType::WESTERN_UNION_INTERMEDIARY_CREDITS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::WESTERN_UNION_INTERMEDIARY_CREDITS;
			    break;

			case CNachaFileType::CONVENIENCE_FEE_CLEARING:
				$intReconciliationEntryTypeId = CReconciliationEntryType::ACH_CONVENIENCE_FEE_CLEARING;
			    break;

			case CNachaFileType::CHARITY_DONATION_CLEARING:
				$intReconciliationEntryTypeId = CReconciliationEntryType::ACH_CHARITY_DONATION_CLEARING;
			    break;

			case CNachaFileType::INTERNAL_TRANSFERS:
			case CNachaFileType::AP_DISTRIBUTIONS:
			case CNachaFileType::AP_PAYMENTS:
				$intReconciliationEntryTypeId = CReconciliationEntryType::EFT_INSTRUCTIONS;
				break;

			default:
				$intReconciliationEntryTypeId = NULL; // this should trigger error.
		}

		$objReconciliationEntry->setReconciliationEntryTypeId( $intReconciliationEntryTypeId );

		// Make sure that the return date is not in the common.transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.

		$intCreatedOn = strtotime( $this->getCreatedOn() );

		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y', $intCreatedOn ), date( 'm/d/Y', ( $intCreatedOn + ( 21 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = array();

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intCreatedOn = strtotime( '+1 day', $intCreatedOn );

		while( 'Sun' == date( 'D', $intCreatedOn ) || 'Sat' == date( 'D', $intCreatedOn ) || true == in_array( $intCreatedOn, $arrintProhibitedTransactionHolidays ) ) {
			$intCreatedOn = strtotime( '+1 day', $intCreatedOn );
		}

		$objReconciliationEntry->setMovementDatetime( date( 'm/d/Y', $intCreatedOn ) );

		return $objReconciliationEntry;
	}

	public function createNachaDetailRecordContent() {

	    $objNachaDetailRecordContent = new CNachaDetailRecordContent();
	    $objNachaDetailRecordContent->setNachaFileId( $this->getNachaFileId() );
	    $objNachaDetailRecordContent->setNachaEntryDetailRecordId( $this->getId() );

	    $this->m_arrobjNachaDetailRecordContents[] = $objNachaDetailRecordContent;

	    return $objNachaDetailRecordContent;
	}

	/**
	 * Get Functions
	 */

	/**
	 * @return CSettlementDistribution
	 */
	public function getSettlementDistribution() {
		return $this->m_objSettlementDistribution;
	}

	/**
	 * @return CCompanyPayment
	 */
	public function getCompanyPayment() {
		return $this->m_objCompanyPayment;
	}

	/**
	 * @return CArPayment
	 */
	public function getArPayment() {
		return $this->m_objArPayment;
	}

	/**
	 * @return CReturnType
	 */
	public function getReturnType() {
		return $this->m_objReturnType;
	}

	/**
	 * @return CClearingBatch
	 */
	public function getClearingBatch() {
		return $this->m_objClearingBatch;
	}

	public function getRebuiltAmount() {

		$strRebuiltAmount = $this->m_strAmount;
		while( '0' == $strRebuiltAmount[0] ) {
			$strRebuiltAmount = substr( $strRebuiltAmount, 1 );
		}

		$strRebultAmount = ( $strRebuiltAmount / 100 );

		return $strRebultAmount;
	}

	public function getCheckAccountNumberMasked() {
		$strCheckAccountNumber = $this->getCheckAccountNumber();
		$intStringLength = strlen( $strCheckAccountNumber );
		$strLastFour = substr( $strCheckAccountNumber, -4 );
		return str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getCheckAccountNumber() {
		if( false == valStr( $this->getCheckAccountNumberEncrypted() ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

    public function getOffsettingAccountNumber() {
	    if( false == valStr( $this->getOffsettingAccountNumberEncrypted() ) ) {
		    return NULL;
	    }
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getOffsettingAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	public function getNachaFileTypeId() {
		return $this->m_intNachaFileTypeId;
	}

	public function getOriginalEntryTraceNumber() {
        return $this->m_strOriginalEntryTraceNumber;
    }

	public function getReturnTypeName() {
		return $this->m_strReturnTypeName;
	}

	public function getNachaDetailRecordContents() {
		return $this->m_arrobjNachaDetailRecordContents;
	}

	/**
	 * Set Functions
	 */

	public function setDefaults() {

		// Size = 1 : Identifies this as a detail record
		$this->setRecordTypeCode( '6' );

		// Size = 2 : Use 27 (Automated Payments) for Checking Debits, Use 22 (Automated Deposit) for Checking Credits (always 22), 32 (Automated Deposit) for Savings Account Credits, 37 (Automated Payment) for Savings Account Debits  (should always be 27, 37 for debit and 22 for CCD to credit 1st Regional account)
		// $this->setRecordTransactionCode();

		// Size = 8 : Residents Bank Routing Number
		// $this->setReceivingDfiIdentification();

		// Size = 1 : This is a funky calculated Modulus 10 number based on the routing number.  Really Weird.  It is used by Thompson's publishing to determine the receivers' bank routing number.
		// $this->setCheckDigit();

		// Size = 17 : Residents Bank Account Number (this must be 17 characters and left most 17 characters should be used if longer)  we can enter numbers and hyphens (if from a micr) no spaces.  See OR 82.  Format is very important.  Left Justify.
		// $this->setDfiAccountNumber();

		// Size = 10 : Amount (Question about formatting)  Is this $48.02.  Never have a decimal point.
		// $this->setAmount();

		// Size = 15 : This should be the ID of the payment (cust_pa_322323), etc.
		// $this->setIdentificationNumber();

		// Size = 22 : Receiving company's name -- What goes here?  How do we link a payment to a settlement acocunt at 1st Regional? (WEB -- RESIDENTS NAME) (CCD -- COMPANY NAME)
		// $this->setReceivingCompanyName();

		// Size = 2 : ? A code for the ODFI -- Is this always stay as "CS"? (This is an optional field) -- CS is for CCD entries. On PPD, DISCRETIONARY DATA FIELD. Not optional for WEB, Indicates "S" for single entry and "R" for recurring on web. (We should hard code S here)
		$this->setDiscretionaryData( 'S' );

		// Size = 1 : This is always zero unless you add an adenda which we won't do I don't think. (An addenda is some kind of addition to the entry)
		$this->setAddendaRecordIndicator( '0' );

		// Size = 15 : Trace number is routing number of ODFI + batched_ar_payment_id or the position. (TRACE NUMBER first 8 digits of routing number + the position of transactions.)
		// $this->setTraceNumber();
	}

	public function setRecordTypeCode( $strRecordTypeCode ) {
		$this->m_strRecordTypeCode = CStrings::strNachaDef( $strRecordTypeCode, 1, true, false );
	}

	public function setReturnType( $objReturnType ) {
		$this->m_objReturnType = $objReturnType;
	}

	public function setRecordTransactionCode( $strRecordTransactionCode ) {
		$this->m_strRecordTransactionCode = CStrings::strNachaDef( $strRecordTransactionCode, 2, true, false );
	}

	public function setReceivingDfiIdentification( $strReceivingDfiIdentification ) {
		$this->m_strReceivingDfiIdentification = CStrings::strNachaDef( $strReceivingDfiIdentification, 8, true, false );
	}

	public function setCheckDigit( $strCheckDigit ) {
		$this->m_strCheckDigit = CStrings::strNachaDef( $strCheckDigit, 1, true, false );
	}

	public function setDfiAccountNumber( $strDfiAccountNumber ) {
		$this->m_strDfiAccountNumber = CStrings::strNachaDef( $strDfiAccountNumber, 17, false, false );
	}

	public function setArPayment( $objArPayment ) {
		$this->m_objArPayment = $objArPayment;
	}

	public function setSettlementDistribution( $objSettlementDistribution ) {
		$this->m_objSettlementDistribution = $objSettlementDistribution;

		if( NULL != $objSettlementDistribution && $this->getSettlementDistributionId() != $objSettlementDistribution->getId() ) {
			$this->setSettlementDistributionId( $objSettlementDistribution->getId() );
		}
	}

	public function setCompanyPayment( $objCompanyPayment ) {
		$this->m_objCompanyPayment = $objCompanyPayment;

		if( NULL != $objCompanyPayment && $this->getCompanyPaymentId() != $objCompanyPayment->getId() ) {
			$this->setCompanyPaymentId( $objCompanyPayment->getId() );
		}
	}

	public function setClearingBatch( $objClearingBatch ) {
		$this->m_objClearingBatch = $objClearingBatch;

		if( NULL != $objClearingBatch && $this->getClearingBatchId() != $objClearingBatch->getId() ) {
			$this->setClearingBatchId( $objClearingBatch->getId() );
		}
	}

	public function buildAmounts( $fltFormattedAmount ) {
		// Validate to make sure amount is in $$.cc format

		$fltFormattedAmount = number_format( abs( $fltFormattedAmount ), 2, '.', '' );

    	if( false == stristr( $fltFormattedAmount, '.' ) ) {
    		trigger_error( 'Amount is not in $$.cc format.', E_USER_ERROR );
    		exit;
    	}

    	parent::setFormattedAmount( $fltFormattedAmount );
		$this->m_strAmount = CStrings::strNachaDef( $fltFormattedAmount, 10, true, false, '0' );
	}

	public function setIdentificationNumber( $strIdentificationNumber ) {
		$this->m_strIdentificationNumber = CStrings::strNachaDef( $strIdentificationNumber, 15, false, false );
	}

	public function setReceivingCompanyName( $strReceivingCompanyName ) {
		$this->m_strReceivingCompanyName = CStrings::strNachaDef( $strReceivingCompanyName, 22, false, false );
	}

	public function setDiscretionaryData( $strDiscretionaryData ) {
		$this->m_strDiscretionaryData = CStrings::strNachaDef( $strDiscretionaryData, 2, false, false );
	}

	public function setAddendaRecordIndicator( $strAddendaRecordIndicator ) {
		$this->m_strAddendaRecordIndicator = CStrings::strNachaDef( $strAddendaRecordIndicator, 1, true, false );
	}

	public function setTraceNumber( $strTraceNumber ) {
		$this->m_strTraceNumber = CStrings::strNachaDef( $strTraceNumber, 15, true, false );
	}

	public function setNachaFileTypeId( $intNachaFileTypeId ) {
		$this->m_intNachaFileTypeId = $intNachaFileTypeId;
	}

	public function setOriginalEntryTraceNumber( $strOriginalEntryTraceNumber ) {
		$this->m_strOriginalEntryTraceNumber = CStrings::strNachaDef( $strOriginalEntryTraceNumber, 15, true, false );
	}

	public function setReturnTypeName( $strReturnTypeName ) {
		$this->m_strReturnTypeName = CStrings::strTrimDef( $strReturnTypeName, 10, NULL, true );
	}

	public function setOffsettingAccountNumber( $strOffsettingAccountNumber ) {
		if( false == valStr( $strOffsettingAccountNumber ) ) {
			return NULL;
		}
		$strOffsettingAccountNumber = CStrings::strTrimDef( $strOffsettingAccountNumber, 20, NULL, true );
		$this->setOffsettingAccountNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strOffsettingAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
	}

	public function setCheckAccountNumber( $strCheckAccountNumber ) {
		if( false == valStr( $strCheckAccountNumber ) ) {
			return;
		}
		$this->setCheckAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['nacha_file_type_id'] ) ) $this->setNachaFileTypeId( $arrmixValues['nacha_file_type_id'] );
        if( true == isset( $arrmixValues['original_entry_trace_number'] ) ) $this->setOriginalEntryTraceNumber( $arrmixValues['original_entry_trace_number'] );
        if( true == isset( $arrmixValues['return_type_name'] ) ) $this->setReturnTypeName( $arrmixValues['return_type_name'] );
        if( true == isset( $arrmixValues['offsetting_account_number'] ) ) $this->setOffsettingAccountNumber( $arrmixValues['offsetting_account_number'] );

        return;
    }

	/**
	 * Validate Functions
	 */

	public function valOffsettingRoutingNumber( $objPaymentDatabase = NULL ) {
        $boolIsValid = true;

        if( false == isset( $this->m_strOffsettingRoutingNumber ) || 0 == strlen( $this->m_strOffsettingRoutingNumber ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'offsetting_routing_number', 'Offsetting Routing number is required', 605 ) );
        }

        if( 0 !== preg_match( '/[^0-9]/', $this->getOffsettingRoutingNumber() ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'offsetting_routing_number', 'Offsetting Routing number must be a number.', 622 ) );
			return false;
        }

        if( 9 != strlen( $this->getOffsettingRoutingNumber() ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'offsetting_routing_number', 'Offsetting Routing number must be 9 digits long.', 623 ) );
			return false;
        }

		if( false == is_null( $objPaymentDatabase ) ) {
			// If it is 9 numeric characters see if it is a fed ach participant
			$objFedAchParticipant = \Psi\Eos\Payment\CFedAchParticipants::createService()->fetchFedAchParticipantByRoutingNumber( $this->getOffsettingRoutingNumber(), $objPaymentDatabase );

			if( true == is_null( $objFedAchParticipant ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'offsetting_routing_number', 'Offsetting Routing number is not valid.', 624 ) );
			}
		}

        return $boolIsValid;
    }

	public function valOffsettingAccountNumber() {

		$boolIsValid = true;

		if( true == is_null( $this->getOffsettingAccountNumber() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'offsetting_account_number', 'Account number is required', 602 ) );
        }

        return $boolIsValid;
    }

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_offsetting_details':
				$boolIsValid &= $this->valOffsettingRoutingNumber();
				$boolIsValid &= $this->valOffsettingAccountNumber();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Database Functions
	 */

	// These functions were put in child class because dfi_account_number should never go to database in raw text.

	public function sqlDfiAccountNumber() {
		return 'NULL';
	}

	public function fetchSettlementDistribution( $objPaymentDatabase ) {
		if( true == valObj( $this->m_objSettlementDistribution, 'CSettlementDistribution' ) && $this->m_intSettlementDistributionId == $this->m_objSettlementDistribution->getId() ) {
			return $this->m_objSettlementDistribution;
		}
		$this->m_objSettlementDistribution = \Psi\Eos\Payment\CSettlementDistributions::createService()->fetchSettlementDistributionById( $this->m_intSettlementDistributionId, $objPaymentDatabase );
		return $this->m_objSettlementDistribution;
	}

	public function fetchCompanyPayment( $objAdminDatabase ) {
		$this->m_objCompanyPayment = \Psi\Eos\Admin\CCompanyPayments::createService()->fetchCompanyPaymentById( $this->m_intCompanyPaymentId, $objAdminDatabase );
		return $this->m_objCompanyPayment;
	}

	public function fetchArPayment( $objPaymentDatabase ) {
		if( true == valObj( $this->m_objArPayment, 'CArPayment' ) && $this->m_intArPaymentId == $this->m_objArPayment->getId() ) {
			return $this->m_objArPayment;
		}
		$this->m_objArPayment = CArPayments::fetchArPaymentByIdByCid( $this->m_intArPaymentId, $this->getCid(), $objPaymentDatabase );

		if( false == valObj( $this->m_objArPayment, 'CArPayment' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to load ar payment for id: ' . $this->m_intArPaymentId ) );
			$this->addErrorMsgs( $objPaymentDatabase->getErrorMsgs() );
			return false;
		}
		return $this->m_objArPayment;
	}

	public function fetchReturnType( $objPaymentDatabase ) {
		if( true == valObj( $this->m_objReturnType, 'CReturnType' ) && $this->m_intReturnTypeId == $this->m_objReturnType->getId() ) {
			return $this->m_objReturnType;
		}
		$this->m_objReturnType = CReturnTypes::fetchReturnTypeById( $this->m_intReturnTypeId, $objPaymentDatabase );

		if( false == valObj( $this->m_objReturnType, 'CReturnType' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to load return type for id: ' . $this->m_intReturnTypeId ) );
			$this->addErrorMsgs( $objPaymentDatabase->getErrorMsgs() );
			return false;
		}
		return $this->m_objReturnType;
	}

	public function fetchClearingBatch( $objPaymentDatabase ) {
		$this->m_objClearingBatch = \Psi\Eos\Payment\CClearingBatches::createService()->fetchClearingBatchById( $this->m_intClearingBatchId, $objPaymentDatabase );
		return $this->m_objClearingBatch;
	}

	public function fetchNachaReturnAddendaDetailRecord( $objPaymentDatabase ) {
		$this->m_objNachaReturnAddendaDetailRecord = \Psi\Eos\Payment\CNachaReturnAddendaDetailRecords::createService()->fetchNachaReturnAddendaDetailRecordById( $this->m_intNachaReturnAddendaDetailRecordId, $objPaymentDatabase );
		return $this->m_objNachaReturnAddendaDetailRecord;
	}

	public function fetchClient( $objAdminDatabase ) {
		$this->m_objClient = CClients::fetchClientById( $this->m_intCid, $objAdminDatabase );
		return $this->m_objClient;
	}

	public function fetchNachaFile( $objPaymentDatabase ) {
		$this->m_objNachaFile = \Psi\Eos\Payment\CNachaFiles::createService()->fetchNachaFileById( $this->getNachaFileId(), $objPaymentDatabase );
		return $this->m_objNachaFile;
	}

	public function fetchProcessingBankAccount( $objPaymentDatabase ) {
		$this->m_objProcessingBankAccount = \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccountById( $this->getProcessingBankAccountId(), $objPaymentDatabase );
		return $this->m_objProcessingBankAccount;
	}

	/**
	 * Other Functions
	 */

	public function fetchOriginalOffsettingProcessingBankAccountId( $objAdminDatabase, $objPaymentDatabase ) {

		$strToEmailAddresses = CSystemEmail::PROCESSING_EMAIL_ADDRESS;

        if( 1 == $this->getIsIntermediary() && false == is_numeric( $this->m_intCompanyPaymentId ) ) {
            // If this is an intermediary account that the payment failed on (Virginia mis-entered the clients intermediary account #), then we shouldn't
            // process this item as returned.  It was the intermediary transaction that returned.
            mail( $strToEmailAddresses, 'Major Payment Processing Error', 'A transaction on an intermediary account failed.  Nacha Entry Detail Record: ' . $this->getId() . ' (cid' . $this->getCid() . ',ar_payment_id=' . $this->getArPaymentId() . ',company_payment_id=' . $this->getCompanyPaymentId() . ',receiving_company_name=' . $this->getReceivingCompanyName() . ',trace_number=' . $this->getTraceNumber() . ')', 'from:' . CSystemEmail::CRONJOBS_EMAIL_ADDRESS );
            return NULL;
        }

		if( true == is_numeric( $this->m_intSettlementDistributionId ) ) {

			// This is a weird one.  If we reach here, it means this settlement distribution occurred exclusively on
			// one processing bank account.  It means we can just grab any AR payment off of the settlement distribution,
			// and pull it's PBA.  That PBA will represent all other PBA's from trans in the settlement.  I hope this is right.

    		$this->fetchSettlementDistribution( $objPaymentDatabase );

    		if( false == valObj( $this->m_objSettlementDistribution, 'CSettlementDistribution' ) ) {
    			trigger_error( 'Settlement Distro could not be loaded.', E_USER_WARNING );
    			mail( $strToEmailAddresses, 'Major Payment Processing Error', 'Settlement distro could not be loaded. Nacha Entry Detail Record: ' . $this->getId() . ' (cid' . $this->getCid() . ',ar_payment_id=' . $this->getArPaymentId() . ',company_payment_id=' . $this->getCompanyPaymentId() . ',receiving_company_name=' . $this->getReceivingCompanyName() . ',trace_number=' . $this->getTraceNumber() . ')', 'from:' . CSystemEmail::CRONJOBS_EMAIL_ADDRESS );
            	return NULL;
    		}

    		$arrstrProcessingBankAccountTransactions = $this->m_objSettlementDistribution->fetchProcessingBankAccountTransactions( $objPaymentDatabase );

    		if( false == valArr( $arrstrProcessingBankAccountTransactions ) || 1 < \Psi\Libraries\UtilFunctions\count( $arrstrProcessingBankAccountTransactions ) || false == is_numeric( $arrstrProcessingBankAccountTransactions[0]['processing_bank_account_id'] ) ) {
    			trigger_error( 'A split settlement distribution got to a function that is should not have arrived at.  Review with Dave.', E_USER_WARNING );
    			mail( $strToEmailAddresses, 'Major Payment Processing Error', 'A split settlement distribution got to a function that is should not have arrived at.  Review with Dave.  Nacha Entry Detail Record: ' . $this->getId(), 'from:' . CSystemEmail::CRONJOBS_EMAIL_ADDRESS );
            	return NULL;
    		}

    		return $arrstrProcessingBankAccountTransactions[0]['processing_bank_account_id'];

    	} elseif( true == is_numeric( $this->m_intArPaymentId ) ) {

    		$this->fetchArPayment( $objPaymentDatabase );

    		if( false == valObj( $this->m_objArPayment, 'CArPayment' ) || false == is_numeric( $this->m_objArPayment->getProcessingBankAccountId() ) ) {
    			trigger_error( 'Original AR Payment PBA ID could not be found. Review with Dave.', E_USER_WARNING );
    			mail( $strToEmailAddresses, 'Major Payment Processing Error', 'Original AR Payment PBA ID could not be found. Nacha Entry Detail Record: ' . $this->getId(), 'from:' . CSystemEmail::CRONJOBS_EMAIL_ADDRESS );
            	return NULL;
    		}

    		return $this->m_objArPayment->getProcessingBankAccountId();

    	} elseif( true == is_numeric( $this->m_intCompanyPaymentId ) ) {

    		$this->fetchCompanyPayment( $objAdminDatabase );

    		if( false == valObj( $this->m_objCompanyPayment, 'CCompanyPayment' ) || false == is_numeric( $this->m_objCompanyPayment->getPsProcessingBankAccountId() ) ) {
    			trigger_error( 'Original Company Payment PBA ID could not be found. Review with Dave.', E_USER_WARNING );
    			mail( $strToEmailAddresses, 'Major Payment Processing Error', 'Original Company Payment PBA ID could not be found. Nacha Entry Detail Record: ' . $this->getId(), 'from:' . CSystemEmail::CRONJOBS_EMAIL_ADDRESS );
            	return NULL;
    		}

    		// In this scenario, the central processing bank account is the account PS processes through.
			return $this->m_objCompanyPayment->getPsProcessingBankAccountId();

		} elseif( true == is_numeric( $this->getEftInstructionId() ) ) {

			return $this->getProcessingBankAccountId();

		} elseif( true == is_numeric( $this->m_intClearingBatchId ) ) {

    		// Clearing batches are very similar to settlement distributions.  They are offset against multiple PBAs.
    		//   Due to this, we'll return NULL here and expect that the appropriate arra values will be
    		//   calculated by the Nacha controller using CClearingBatches::fetchClearingBatchesProcessingBankAccountDetails(...)

    		$this->fetchClearingBatch( $objPaymentDatabase );

    		if( false == valObj( $this->m_objClearingBatch, 'CClearingBatch' ) ) {
    			trigger_error( 'Clearing Batch could not be loaded.', E_USER_WARNING );
    			mail( $strToEmailAddresses, 'Major Payment Processing Error', 'Clearing Batch could not be loaded. Nacha Entry Detail Record: ' . $this->getId(), 'from:' . CSystemEmail::CRONJOBS_EMAIL_ADDRESS );
            	return NULL;
    		}

			return NULL;
    	} else {
			return true;
    	}
	}

    public function processReturn( $intUserId, $objNachaReturnAddendaDetailRecord, $objAdminDatabase, $objPaymentDatabase, $objClientDatabase, $objEmailDatabase ) {

        if( false == is_numeric( $this->m_intCompanyPaymentId ) && 1 == $this->getIsIntermediary() ) {
            // If this is an intermediary account that the payment failed on (Virginia mis-entered the clients intermediary account #), then we shouldn't
            // process this item as returned.  It was the intermediary transaction that returned.
            mail( CSystemEmail::PROCESSING_EMAIL_ADDRESS, 'Major Payment Processing Error', 'A transaction on an intermediary account failed.  Nacha Entry Detail Record: ' . $this->getId(), 'from:' . CSystemEmail::CRONJOBS_EMAIL_ADDRESS );
            return true;
        }

		// Per NACHA rules, return types R02 and R03 should result in the account being blacklisted
		$arrintBlacklistReturnTypeIds = array( CReturnType::ACCOUNT_CLOSED, CReturnType::NO_ACCOUNT_OR_UNABLE_TO_LOCATE_ACCOUNT );

		// Make sure we have loaded the return type
		$this->fetchReturnType( $objPaymentDatabase );

		if( true == in_array( $this->m_intReturnTypeId, $arrintBlacklistReturnTypeIds ) ) {
			$boolBlacklistResult = $this->blacklistAndNotifyProcessing( $objNachaReturnAddendaDetailRecord, $objAdminDatabase, $objPaymentDatabase, $objEmailDatabase );
		}

    	// If we get a return on a PROCESSING BANK ACCOUNT (DUE TO A DATA ENTRY ERROR BY VIRGINIA), We will allow processing to continue now.  However, nothing should happen
    	//      because the returning item shouldn't have a settlement_distribution_id, ar_payment_id, or company_payment_id attached, this should just return true.
    	//      We do need to find a way to correct the problem and recredit the intermediary.  We need to develop a way to fix the handle central returns processing for
    	//      returns on intermediary accounts.

    	if( true == is_numeric( $this->m_intSettlementDistributionId ) ) {

    		$this->fetchSettlementDistribution( $objPaymentDatabase );

    		if( true == isset( $this->m_objSettlementDistribution )
    				&& true == is_null( $this->m_objSettlementDistribution->getReturnedOn() )
    				&& false == is_int( $this->m_objSettlementDistribution->getReturnTypeId() )
    				&& ( CDistributionStatusType::CREDITED == $this->m_objSettlementDistribution->getDistributionStatusTypeId()
    						|| CDistributionStatusType::DEBITED == $this->m_objSettlementDistribution->getDistributionStatusTypeId() ) ) {

				$this->m_objSettlementDistribution->setReturnTypeId( $this->m_intReturnTypeId );
				$this->m_objSettlementDistribution->setReturnedOn( date( 'm/d/Y' ) );

				if( false == $this->m_objSettlementDistribution->processReturn( $intUserId, $objAdminDatabase, $objPaymentDatabase, $objEmailDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Processing return falied for SD #' . $this->m_objSettlementDistribution->getId() ) );
					$this->addErrorMsgs( $this->m_objSettlementDistribution->getErrorMsgs() );
					return false;
	    		}
    		}

    	} elseif( true == is_numeric( $this->m_intArPaymentId ) ) {

    		$this->fetchArPayment( $objPaymentDatabase );

    		if( false === valObj( $this->m_objArPayment, 'CArPayment' ) ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'm_objArPayment is not set ' ) );

    			return false;
    		}
    		$arrintFailureReturnTypeIds = [
    			CReturnType::INCORRECT_DFI_ACCOUNT_NUMBER,
				CReturnType::INCORRECT_ROUTING_NUMBER_AND_INCORRECT_DFI_ACCOUNT_NUMBER,
				CReturnType::INCORRECT_INDIVIDUAL_NAME_RECEIVING_COMPANY_NAME,
				CReturnType::INCORRECT_DFI_ACCOUNT_NUMBER_AND_INCORRECT_TRANSACTION_CODE,
				CReturnType::INCORRECT_ROUTING_NUMBER_INCORRECT_DFI_ACCOUNT_NUMBER_AND_INCORRECT_TRANSACTION_CODE,
				CReturnType::INVALID_ACCOUNT_NUMBER,
				CReturnType::NON_TRANSACTION_ACCOUNT,
				CReturnType::NO_ACCOUNT_OR_UNABLE_TO_LOCATE_ACCOUNT,
				CReturnType::ACCOUNT_CLOSED
		    ];

			if( true == in_array( $this->m_intReturnTypeId, $arrintFailureReturnTypeIds ) ) {
    			// Delete stored billing info
    			if( NULL == $objCustomerPaymentAccount ) {
    				$objCustomerPaymentAccount = new CCustomerPaymentAccount();
    			}

    			$boolResult = $objCustomerPaymentAccount->clearStoredBillingInfo( $this->m_objArPayment->getCustomerPaymentAccountId(), $this->m_objArPayment->getCustomerId(),
    					$this->m_intCid, $this->m_strCheckRoutingNumber, $strAccountNumber, $intUserId, $objClientDatabase );

    			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Deleting stored billing for customer payment account id (' . $this->m_objArPayment->getCustomerPaymentAccountId() . '): ' . $boolResult ) );

    			if( false == $boolResult ) {
    				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to delete stored billing for customer payment account id: ' . $this->m_objArPayment->getCustomerPaymentAccountId() ) );
    				$this->addErrorMsgs( $objCustomerPaymentAccount->getErrorMsgs() );

    				// continue anyway
    			}

    			// Delete recurring billing
    			if( false == is_null( $this->m_objArPayment ) && false == is_null( $this->m_objArPayment->getScheduledPaymentId() ) ) {
    				$objScheduledPayment = CScheduledPayments::fetchScheduledPaymentByIdByCid( $this->m_objArPayment->getScheduledPaymentId(), $this->getCid(), $objClientDatabase );

    				$boolResult = $objScheduledPayment->delete( SYSTEM_USER_ID, $objClientDatabase, $strMessage = NULL, $objWebsite = NULL, $boolIsResidentPortalPayment = false );

    				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Deleting scheduled payment id: ' . $objScheduledPayment->getId() . ': ' . $boolResult ) );

    				if( false == $boolResult ) {
    					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to delete scheduled payment id: ' . $objScheduledPayment->getId() ) );
    					$this->addErrorMsgs( $objScheduledPayment->getErrorMsgs() );

    					// continue anyway
    				}
    			}
    		}

    		if( true == isset ( $this->m_objArPayment )
    				&& ( CPaymentStatusType::REVERSED == $this->m_objArPayment->getPaymentStatusTypeId() || CPaymentStatusType::CAPTURED == $this->m_objArPayment->getPaymentStatusTypeId() )
    				&& true == is_null( $this->m_objArPayment->getReturnedOn() )
    				&& ( CPaymentType::ACH == $this->m_objArPayment->getPaymentTypeId() || ( CPaymentType::CHECK_21 == $this->m_objArPayment->getPaymentTypeId() && 1 == $this->m_objArPayment->getCheckIsConverted() ) ) ) {

    			$this->m_objArPayment->setReturnTypeId( $this->m_intReturnTypeId );

    			if( CReturnType::NO_ACCOUNT_OR_UNABLE_TO_LOCATE_ACCOUNT == $this->m_intReturnTypeId ) {
					$intArPaymentDistributionCount = \Psi\Eos\Payment\CArPaymentDistributions::createService()->fetchArPaymentDistributionCount( 'WHERE ar_payment_id = ' . ( int ) $this->m_objArPayment->getId() . ' AND cid = ' . ( int ) $this->m_objArPayment->getCid(), $objPaymentDatabase );
					if( 0 == $intArPaymentDistributionCount ) {
						$this->m_objArPayment->setDistributionBlockedOn( date( 'Y-m-d H:i:s' ) );
						$this->addDistributedNote( $this->m_objArPayment, $objPaymentDatabase );
					}
    			}

    			$strReturnReason = '';
    			if( true == valObj( $this->m_objReturnType, 'CReturnType' ) ) {
    				$strReturnReason = $this->m_objReturnType->getName();
    			}

				if( false == $this->m_objArPayment->processPayment( CArPayment::PROCESS_METHOD_RETURN, $intUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase, $boolForceIntegration = false, $boolSendEmails = false, $strReturnReason ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Processing return failed for AP #' . $this->m_objArPayment->getId() ) );
					$this->addErrorMsgs( $this->m_objArPayment->getErrorMsgs() );
					return false;
				}

    		// Handle the case where payment has already been returned.  We just want to update payment with correct return type id
    		} elseif( true == isset( $this->m_objArPayment )
    				&& false == is_numeric( $this->m_objArPayment->getReturnTypeId() )
    				&& ( CPaymentType::ACH == $this->m_objArPayment->getPaymentTypeId() || ( CPaymentType::CHECK_21 == $this->m_objArPayment->getPaymentTypeId() && 1 == $this->m_objArPayment->getCheckIsConverted() ) ) ) {

    			$this->m_objArPayment->setReturnTypeId( $this->m_intReturnTypeId );
				if( CReturnType::NO_ACCOUNT_OR_UNABLE_TO_LOCATE_ACCOUNT == $this->m_intReturnTypeId ) {
					$intArPaymentDistributionCount = \Psi\Eos\Payment\CArPaymentDistributions::createService()->fetchArPaymentDistributionCount( 'WHERE ar_payment_id =' . ( int ) $this->m_objArPayment->getId() . ' AND cid = ' . ( int ) $this->m_objArPayment->getCid(), $objPaymentDatabase );
					if( 0 == $intArPaymentDistributionCount ) {
						$this->m_objArPayment->setDistributionBlockedOn( date( 'Y-m-d H:i:s' ) );
						$this->addDistributedNote( $this->m_objArPayment, $objPaymentDatabase );
					}
				}

				if( false == $this->m_objArPayment->update( $intUserId, $objClientDatabase, $objPaymentDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Update failed for AP #' . $this->m_objArPayment->getId() ) );
					$this->addErrorMsgs( $this->m_objArPayment->getErrorMsgs() );
					return false;
				}
    		}

    	} elseif( true == is_numeric( $this->m_intCompanyPaymentId ) ) {

    		$this->fetchCompanyPayment( $objAdminDatabase );

    		if( true == isset( $this->m_objCompanyPayment ) && CPaymentStatusType::CAPTURED == $this->m_objCompanyPayment->getPaymentStatusTypeId() ) {

				$this->m_objCompanyPayment->setReturnTypeId( $this->m_intReturnTypeId );

    			if( false == $this->m_objCompanyPayment->processReturn( $intUserId, $objAdminDatabase, $objPaymentDatabase, $objEmailDatabase ) ) {
    				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Processing return falied for CP #' . $this->m_objCompanyPayment->getId() ) );
					$this->addErrorMsgs( $this->m_objCompanyPayment->getErrorMsgs() );
					return false;
	    		}
    		}

    	} elseif( true == is_numeric( $this->m_intClearingBatchId ) ) {

    		$this->fetchClearingBatch( $objPaymentDatabase );

    		if( true == isset( $this->m_objClearingBatch ) && true == is_null( $this->m_objClearingBatch->getReturnedOn() ) && false == is_int( $this->m_objClearingBatch->getReturnTypeId() ) ) {

				$this->m_objClearingBatch->setReturnTypeId( $this->m_intReturnTypeId );

    			if( false == $this->m_objClearingBatch->processReturn( $intUserId, $objPaymentDatabase ) ) {
    				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Processing return falied for CB #' . $this->m_objClearingBatch->getId() ) );
					$this->addErrorMsgs( $this->m_objClearingBatch->getErrorMsgs() );
					return false;
	    		}
    		}

    	} else {
			return true;
    	}

    	return true;
    }

    public function processNoticeOfChange( $intUserId, $objNachaReturnAddendaDetailRecord, $objAdminDatabase, $objPaymentDatabase, $objClientDatabase, $objEmailDatabase, $objPaymentEmailer = NULL ) {

    	if( is_null( $this->m_intReturnTypeId ) || is_null( $this->m_strCheckAccountNumberEncrypted ) || is_null( $this->m_strCheckRoutingNumber ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'Missing required info: return type is NULL: ' . is_null( $this->m_intReturnTypeId ) . ', check account number is NULL: ' . is_null( $this->m_strCheckAccountNumberEncrypted ) . ', routing number is NULL ' . is_null( $this->m_strCheckRoutingNumber ) ) );
    		return false;
    	}

    	// email PSI processing
		$strNotifiedEmail = NULL;
		$strNotifiedOn = NULL;

		$this->blacklistAndNotifyProcessing( $objNachaReturnAddendaDetailRecord, $objAdminDatabase, $objPaymentDatabase, $objEmailDatabase );

    	if( true == is_numeric( $this->m_intArPaymentId ) ) {

    		$boolResult = $this->processNoticeOfChangeArPayment( $intUserId, $objNachaReturnAddendaDetailRecord, $objPaymentDatabase, $objClientDatabase, $objEmailDatabase, NULL, $objPaymentEmailer );

			if( false == $boolResult ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to process notice of change ar payment.' ) );
			}
    		return $boolResult;
    	}

    	if( true == is_numeric( $this->m_intSettlementDistributionId ) ) {

    		$boolResult = $this->processNoticeOfChangeSettlementDistribution( $intUserId, $objNachaReturnAddendaDetailRecord, $objPaymentDatabase, $objEmailDatabase, $objAdminDatabase );

    		if( false == $boolResult ) {
    			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to process notice of change settlement distribution.' ) );
    		}
    		return $boolResult;
    	}

    	if( true == is_numeric( $this->m_intCompanyPaymentId ) ) {

    		$boolResult = $this->processNoticeOfChangeCompanyPayment( $intUserId, $objNachaReturnAddendaDetailRecord, $objPaymentDatabase, $objEmailDatabase, $objAdminDatabase );

    		if( false == $boolResult ) {
    			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to process notice of change company payment.' ) );
    		}
    		return $boolResult;
    	}

		return true;
    }

    public function processNoticeOfChangeInvalidCheckAccountTypeForSettlementDistribution( $intUserId, $objNachaReturnAddendaDetailRecord, $objSettlementDistribution, $objReturnType, $objPaymentDatabase, $objEmailDatabase, $objAdminDatabase ) {

    	$boolSuccess = true;
    	$intCorrectedAccountTypeId = $objNachaReturnAddendaDetailRecord->getCorrectedAccountTypeId();

    	if( false == valObj( $objSettlementDistribution, 'CSettlementDistribution' ) ) {
    		$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to load settlement distribution detail for settlement distribution id: ' . $objNachaReturnAddendaDetailRecord->getSettlementDistributionId() ) );
    		return false;
    	}

    	if( 0 === $intCorrectedAccountTypeId ) {
    		// we were unable to get the corrected account type, notify accounting
    		$strProcessingEmail = 'ERROR: The system was unable to automatically update account type id (could not get corrected account type id ) for' . "\n" . 'Corrected Account Type Id: ' . ( int ) $intCorrectedAccountTypeId . "\n" . 'Client: ' . $objSettlementDistribution->getCid() . "\n" . 'Settlement Distribution: ' . $objSettlementDistribution->getId() . "\n\n" . 'The reason for the return was: ' . "\n" . 'Code: ' . $objReturnType->getCode() . "\n" . 'Name: ' . $objReturnType->getName() . "\n" . 'Description: ' . $objReturnType->getDescription();

    		$this->sendProcessingEmailNotification( $strProcessingEmail, $objEmailDatabase );

    		$boolSuccess = false;

    	} else {

    		// Set routing number, update the settlement distribution
    		$objSettlementDistribution->setCheckAccountTypeId( $intCorrectedAccountTypeId );
    		if( false === $objSettlementDistribution->update( $intUserId, $objPaymentDatabase ) ) {
    			$arrobjSettlementDistributionErrorMsgs = $objSettlementDistribution->getErrorMsgs();

    			$this->addErrorMsgs( $arrobjSettlementDistributionErrorMsgs );

    			// Notify accounting
    			$strProcessingEmail = 'ERROR: The system was unable to automatically update the check account type id for settlement distribution: ' . $objSettlementDistribution->getId() . "\n" . 'Corrected Account Type Id : ' . ( int ) $intCorrectedAccountTypeId . "\n" . 'Client: ' . $objSettlementDistribution->getCid() . "\n" . 'Settlement Distribution: ' . $objSettlementDistribution->getId() . "\n\n" . 'The reason for the return was: ' . "\n" . 'Code: ' . $objReturnType->getCode() . "\n" . 'Name: ' . $objReturnType->getName() . "\n" . 'Description: ' . $objReturnType->getDescription();

    			foreach( $arrobjSettlementDistributionErrorMsgs as $objErrorMsg ) {
    				$strProcessingEmail .= "\n" . $objErrorMsg->getMessage();
    			}

    			$this->sendProcessingEmailNotification( $strProcessingEmail, $objEmailDatabase );

    			$boolSuccess = false;
    		}

    		// update the account information( update in admin database accounts table)
    		$objAccount = CAccounts::createService()->fetchAccountByIdAndCid( $objSettlementDistribution->getAccountId(), $objSettlementDistribution->getCid(), $objAdminDatabase );
    		if( true == valObj( $objAccount, 'CAccount' ) ) {
    			$objAccount->setCheckAccountTypeId( $intCorrectedAccountTypeId );
    			if( false === $objAccount->update( $intUserId, $objAdminDatabase ) ) {
    				$arrobjAccountErrorMsgs = $objAccount->getErrorMsgs();

    				$this->addErrorMsgs( $arrobjAccountErrorMsgs );

    				// Notify accounting
    				$strProcessingEmail = 'ERROR: The system was unable to automatically update the routing number for' . "\n" . 'Routing Number: ' . $strCorrectedData . "\n" . 'Client: ' . $objSettlementDistribution->getCid() . "\n" . 'Settlement Distribution: ' . $objSettlementDistribution->getId() . "\n\n" . 'The reason for the return was: ' . "\n" . 'Code: ' . $objReturnType->getCode() . "\n" . 'Name: ' . $objReturnType->getName() . "\n" . 'Description: ' . $objReturnType->getDescription();

    				foreach( $arrobjAccountErrorMsgs as $objErrorMsg ) {
    					$strProcessingEmail .= "\n" . $objErrorMsg->getMessage();
    				}

    				$this->sendProcessingEmailNotification( $strProcessingEmail, $objEmailDatabase );

    				$boolSuccess = false;
    			}
    		}
    	}

    	return $boolSuccess;
    }

    public function processNoticeOfChangeInvalidRoutingNumberForSettlementDistribution( $intUserId, $objNachaReturnAddendaDetailRecord, $objSettlementDistribution, $objReturnType, $objPaymentDatabase, $objEmailDatabase, $objAdminDatabase ) {

    	$boolSuccess = true;

    	$strCorrectedData = $objNachaReturnAddendaDetailRecord->getCorrectedRoutingNumber();

    	if( false == valObj( $objSettlementDistribution, 'CSettlementDistribution' ) ) {
    		return false;
    	}

    	// Set routing number, update the settlement distribution
    	$objSettlementDistribution->setCheckRoutingNumber( $strCorrectedData );
    	if( false === $objSettlementDistribution->update( $intUserId, $objPaymentDatabase ) ) {
    		$arrobjSettlementDistributionErrorMsgs = $objSettlementDistribution->getErrorMsgs();

    		$this->addErrorMsgs( $arrobjSettlementDistributionErrorMsgs );

    		// Notify accounting
    		$strProcessingEmail = 'ERROR: The system was unable to automatically update the routing number for' . "\n" . 'Routing Number: ' . $strCorrectedData . "\n" . 'Client: ' . $objSettlementDistribution->getCid() . "\n" . 'Settlement Distribution: ' . $objSettlementDistribution->getId() . "\n\n" . 'The reason for the return was: ' . "\n" . 'Code: ' . $objReturnType->getCode() . "\n" . 'Name: ' . $objReturnType->getName() . "\n" . 'Description: ' . $objReturnType->getDescription();

    		foreach( $arrobjSettlementDistributionErrorMsgs as $objErrorMsg ) {
    			$strProcessingEmail .= "\n" . $objErrorMsg->getMessage();
    		}

    		$this->sendProcessingEmailNotification( $strProcessingEmail, $objEmailDatabase );

    		$boolSuccess = false;
    	}

    	// update the account information( update in admin database accounts table)
    	$objAccount = CAccounts::createService()->fetchAccountByIdAndCid( $objSettlementDistribution->getAccountId(), $objSettlementDistribution->getCid(), $objAdminDatabase );
    	if( true == valObj( $objAccount, 'CAccount' ) ) {
    		$objAccount->setCheckRoutingNumber( $strCorrectedData );
    		if( false === $objAccount->update( $intUserId, $objAdminDatabase ) ) {
    			$arrobjAccountErrorMsgs = $objAccount->getErrorMsgs();

    			$this->addErrorMsgs( $arrobjAccountErrorMsgs );

    			// Notify accounting
    			$strProcessingEmail = 'ERROR: The system was unable to automatically update the routing number for' . "\n" . 'Routing Number: ' . $strCorrectedData . "\n" . 'Client: ' . $objSettlementDistribution->getCid() . "\n" . 'Settlement Distribution: ' . $objSettlementDistribution->getId() . "\n\n" . 'The reason for the return was: ' . "\n" . 'Code: ' . $objReturnType->getCode() . "\n" . 'Name: ' . $objReturnType->getName() . "\n" . 'Description: ' . $objReturnType->getDescription();

    			foreach( $arrobjAccountErrorMsgs as $objErrorMsg ) {
    				$strProcessingEmail .= "\n" . $objErrorMsg->getMessage();
    			}

    			$this->sendProcessingEmailNotification( $strProcessingEmail, $objEmailDatabase );

    			$boolSuccess = false;
    		}
    	}

    	return $boolSuccess;
    }

	public function processNoticeOfChangeIncorrectTransactionCode( $intUserId, $objNachaReturnAddendaDetailRecord, $objArPayment, $objReturnType, $objClientDatabase, $objEmailDatabase ) {
		$boolSuccess = true;

		// Update stored billing info

		$intCorrectedAccountTypeId = $objNachaReturnAddendaDetailRecord->getCorrectedAccountTypeId();
		$strCorrectedData = $objNachaReturnAddendaDetailRecord->getCorrectedTransactionCode();

		if( 0 === $intCorrectedAccountTypeId ) {
			// we were unable to get the corrected account type, notify accounting
			$strProcessingEmail = 'ERROR: The system was unable to automatically update account type id (could not get corrected account type id ) for' . "\n" . 'Transaction code: ' . $strCorrectedData . "\n" . 'Corrected Account Type Id: ' . ( int ) $intCorrectedAccountTypeId . "\n" . 'Client: ' . $objArPayment->getCid() . "\n" . 'Customer Payment: ' . $objArPayment->getId() . "\n\n" . 'The reason for the return was: ' . "\n" . 'Code: ' . $objReturnType->getCode() . "\n" . 'Name: ' . $objReturnType->getName() . "\n" . 'Description: ' . $objReturnType->getDescription();

			$this->sendProcessingEmailNotification( $strProcessingEmail, $objEmailDatabase );

			$boolSuccess = false;
		} else {
			// update the stored billing

			$objCustomerPaymentAccount = CCustomerPaymentAccounts::fetchCustomerPaymentAccountByIdByCid( $objArPayment->getCustomerPaymentAccountId(), $this->getCid(), $objClientDatabase );
			if( NULL != $objCustomerPaymentAccount ) {
				$objCustomerPaymentAccount->setCheckAccountTypeId( $intCorrectedAccountTypeId );
				if( false === $objCustomerPaymentAccount->update( $intUserId, $objClientDatabase ) ) {
					$arrobjCustomerPaymentAccountErrorMsgs = $objCustomerPaymentAccount->getErrorMsgs();

					$this->addErrorMsgs( $arrobjCustomerPaymentAccountErrorMsgs );

					// Notify accounting
					$strProcessingEmail = 'ERROR: The system was unable to automatically update account type id (could not update customer payment account ) for' . "\n" . 'Transaction code: ' . $strCorrectedData . "\n" . 'Corrected Account Type Id: ' . ( int ) $intCorrectedAccountTypeId . "\n" . 'Client: ' . $objArPayment->getCid() . "\n" . 'Customer Payment: ' . $objArPayment->getId() . "\n\n" . 'The reason for the return was: ' . "\n" . 'Code: ' . $objReturnType->getCode() . "\n" . 'Name: ' . $objReturnType->getName() . "\n" . 'Description: ' . $objReturnType->getDescription();

					foreach( $arrobjCustomerPaymentAccountErrorMsgs as $objErrorMsg ) {
						$strProcessingEmail .= "\n" . $objErrorMsg->getMessage();
					}

					$this->sendProcessingEmailNotification( $strProcessingEmail, $objEmailDatabase );

				}
			}

			// $strDebugEmail .= 'Updated check account type id for customer payment account ' . $objCustomerPaymentAccount->getId() . ' to : ' . ( int ) $intCorrectedAccountTypeId . '<br />';

			// Update recurring billing
			if( false == is_null( $objArPayment ) && false == is_null( $objArPayment->getScheduledPaymentId() ) ) {
				$objScheduledPayment = CScheduledPayments::fetchScheduledPaymentByIdByCid( $objArPayment->getScheduledPaymentId(), $this->getCid(), $objClientDatabase );
				$objScheduledPaymentDetail = $objScheduledPayment->fetchScheduledPaymentDetail( $objClientDatabase );

				$boolResult = true;

				if( false === valObj( $objScheduledPaymentDetail, 'CScheduledPaymentDetail' ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to load scheduled payment detail for scheduled payment id: ' . $objScheduledPayment->getId() ) );

					$boolResult = false;
				} else {
					$objScheduledPaymentDetail->setCheckAccountTypeId( $intCorrectedAccountTypeId );

					$boolResult = $objScheduledPaymentDetail->update( $intUserId, $objClientDatabase );

					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Updating scheduled payment id: ' . $objScheduledPayment->getId() . ': ' . $boolResult ) );
				}

				if( false == $boolResult ) {
					$arrobjScheduledPaymentErrorMsgs = $objScheduledPaymentDetail->getErrorMsgs();

					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to update scheduled payment id: ' . $objScheduledPayment->getId() ) );
					$this->addErrorMsgs( $arrobjScheduledPaymentErrorMsgs );

					// Notify accounting
					$strProcessingEmail = 'ERROR: The system was unable to automatically update the check account type id for scheduled payment: ' . $objScheduledPayment->getId() . "\n" . 'Check Account TYpe : ' . $strCorrectedData . "\n" . 'Client: ' . $objArPayment->getCid() . "\n" . 'Customer Payment: ' . $objArPayment->getId() . "\n\n" . 'The reason for the return was: ' . "\n" . 'Code: ' . $objReturnType->getCode() . "\n" . 'Name: ' . $objReturnType->getName() . "\n" . 'Description: ' . $objReturnType->getDescription();

					foreach( $arrobjScheduledPaymentErrorMsgs as $objErrorMsg ) {
						$strProcessingEmail .= "\n" . $objErrorMsg->getMessage();
					}

					$this->sendProcessingEmailNotification( $strProcessingEmail, $objEmailDatabase );

					$boolSuccess = false;
				}
			}

			$boolSuccess = false;
		}

		return $boolSuccess;
	}

	public function processNoticeOfChangeInvalidRoutingNumber( $intUserId, $objNachaReturnAddendaDetailRecord, $objArPayment, $objReturnType, $objClientDatabase, $objEmailDatabase ) {
		$boolSuccess = true;

		$strCorrectedData = $objNachaReturnAddendaDetailRecord->getCorrectedRoutingNumber();

		$objCustomerPaymentAccount = CCustomerPaymentAccounts::fetchCustomerPaymentAccountByIdByCid( $objArPayment->getCustomerPaymentAccountId(), $this->getCid(), $objClientDatabase );
		if( NULL != $objCustomerPaymentAccount ) {
			$objCustomerPaymentAccount->setCheckRoutingNumber( $strCorrectedData );
			if( false === $objCustomerPaymentAccount->update( $intUserId, $objClientDatabase ) ) {
				$arrobjCustomerPaymentAccountErrorMsgs = $objCustomerPaymentAccount->getErrorMsgs();

				$this->addErrorMsgs( $arrobjCustomerPaymentAccountErrorMsgs );

				// Notify accounting
				$strProcessingEmail = 'ERROR: The system was unable to automatically update the routing number for' . "\n" . 'Routing Number: ' . $strCorrectedData . "\n" . 'Client: ' . $objArPayment->getCid() . "\n" . 'Customer Payment: ' . $objArPayment->getId() . "\n\n" . 'The reason for the return was: ' . "\n" . 'Code: ' . $objReturnType->getCode() . "\n" . 'Name: ' . $objReturnType->getName() . "\n" . 'Description: ' . $objReturnType->getDescription();

				foreach( $arrobjCustomerPaymentAccountErrorMsgs as $objErrorMsg ) {
					$strProcessingEmail .= "\n" . $objErrorMsg->getMessage();
				}

				$this->sendProcessingEmailNotification( $strProcessingEmail, $objEmailDatabase );

				$boolSuccess = false;
			}
		}

		// Update recurring billing
		if( false == is_null( $objArPayment ) && false == is_null( $objArPayment->getScheduledPaymentId() ) ) {
			$objScheduledPayment = CScheduledPayments::fetchScheduledPaymentByIdByCid( $objArPayment->getScheduledPaymentId(), $this->getCid(), $objClientDatabase );

			$objScheduledPayment->setCheckRoutingNumber( $strCorrectedData );

			$boolResult = $objScheduledPayment->update( $intUserId, $objClientDatabase, $boolUpdateDetailRecord = true );

			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Updating scheduled payment id: ' . $objScheduledPayment->getId() . ': ' . $boolResult ) );

			if( false == $boolResult ) {
				$arrobjScheduledPaymentErrorMsgs = $objScheduledPayment->getErrorMsgs();

				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to update scheduled payment id: ' . $objScheduledPayment->getId() ) );
				$this->addErrorMsgs( $arrobjScheduledPaymentErrorMsgs );

				// Notify accounting
				$strProcessingEmail = 'ERROR: The system was unable to automatically update the routing number for scheduled payment: ' . $objScheduledPayment->getId() . "\n" . 'Routing Number: ' . $strCorrectedData . "\n" . 'Client: ' . $objArPayment->getCid() . "\n" . 'Customer Payment: ' . $objArPayment->getId() . "\n\n" . 'The reason for the return was: ' . "\n" . 'Code: ' . $objReturnType->getCode() . "\n" . 'Name: ' . $objReturnType->getName() . "\n" . 'Description: ' . $objReturnType->getDescription();

				foreach( $arrobjScheduledPaymentErrorMsgs as $objErrorMsg ) {
					$strProcessingEmail .= "\n" . $objErrorMsg->getMessage();
				}

				$this->sendProcessingEmailNotification( $strProcessingEmail, $objEmailDatabase );

				$boolSuccess = false;
			}
		}

		return $boolSuccess;
	}

	public function processNoticeOfChangeInvalidRoutingNumberForCompanyPayment( $intUserId, $objNachaReturnAddendaDetailRecord, $objCompanyPayment, $objReturnType, $objPaymentDatabase, $objAdminDatabase, $objEmailDatabase ) {
		$boolSuccess = true;

		$strCorrectedData = $objNachaReturnAddendaDetailRecord->getCorrectedRoutingNumber();

		$objAccount = CAccounts::createService()->fetchAccountById( $objCompanyPayment->getAccountId(), $objAdminDatabase );
		if( true == valObj( $objAccount, 'CAccount' ) ) {
			$objAccount->setCheckRoutingNumber( $strCorrectedData );
			if( false === $objAccount->update( $intUserId, $objAdminDatabase ) ) {
				$arrobjAccountErrorMsgs = $objAccount->getErrorMsgs();
				$this->addErrorMsgs( $arrobjAccountErrorMsgs );
				$boolSuccess = false;
			}
			if( true == in_array( $objAccount->getAccountTypeId(), CAccountType::$c_arrintInsuranceAccountTypeIds ) ) {
				CResidentInsurePaymentProcesses::processNoticeOfChange( $intUserId, $objAccount );
			}
		}

		return $boolSuccess;
	}

	/**
	 * @param $intUserId
	 * @param $objNachaReturnAddendaDetailRecord
	 * @param $objCompanyPayment
	 * @param $objReturnType
	 * @param $objPaymentDatabase
	 * @param $objAdminDatabase
	 * @param $objEmailDatabase
	 * @return bool
	 */
	public function processNoticeOfChangeInvalidCheckAccountTypeForCompanyPayment( $intUserId, $objNachaReturnAddendaDetailRecord, $objCompanyPayment, $objReturnType, $objPaymentDatabase, $objAdminDatabase, $objEmailDatabase ) {
		$boolSuccess = true;

		$intCorrectedAccountTypeId = $objNachaReturnAddendaDetailRecord->getCorrectedAccountTypeId();
		$strCorrectedData = $objNachaReturnAddendaDetailRecord->getCorrectedTransactionCode();

		if( 0 === $intCorrectedAccountTypeId ) {
			// we were unable to get the corrected account type, notify accounting
			$strProcessingEmail = 'ERROR: The system was unable to automatically update account type id (could not get corrected account type id ) for' . "\n" . 'Transaction code: ' . $strCorrectedData . "\n" . 'Corrected Account Type Id: ' . ( int ) $intCorrectedAccountTypeId . "\n" . 'Client: ' . $objCompanyPayment->getCid() . "\n" . 'Customer Payment: ' . $objCompanyPayment->getId() . "\n\n" . 'The reason for the return was: ' . "\n" . 'Code: ' . $objReturnType->getCode() . "\n" . 'Name: ' . $objReturnType->getName() . "\n" . 'Description: ' . $objReturnType->getDescription();

			$this->sendProcessingEmailNotification( $strProcessingEmail, $objEmailDatabase );

			$boolSuccess = false;
		} else {

			$objAccount = CAccounts::createService()->fetchAccountById( $objCompanyPayment->getAccountId(), $objAdminDatabase );
			if( true == valObj( $objAccount, 'CAccount' ) ) {
				$objAccount->setCheckAccountTypeId( $intCorrectedAccountTypeId );
				if( false === $objAccount->update( $intUserId, $objAdminDatabase ) ) {
					$arrobjAccountErrorMsgs = $objAccount->getErrorMsgs();
					$this->addErrorMsgs( $arrobjAccountErrorMsgs );
					$boolSuccess = false;
				}
				if( true == in_array( $objAccount->getAccountTypeId(), CAccountType::$c_arrintInsuranceAccountTypeIds ) ) {
					CResidentInsurePaymentProcesses::processNoticeOfChange( $intUserId, $objAccount );
				}
			}
		}

		return $boolSuccess;
	}

    public function processNoticeOfChangeArPayment( $intUserId, $objNachaReturnAddendaDetailRecord, $objPaymentDatabase, $objClientDatabase, $objEmailDatabase, $objCustomerPaymentAccount = NULL, $objPaymentEmailer = NULL ) {

		// $strDebugEmail = '';

    	// Make sure we have loaded the ar payment
    	$this->fetchArPayment( $objPaymentDatabase );

    	if( false === valObj( $this->m_objArPayment, 'CArPayment' ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'm_objArPayment is not set ' ) );

    		return false;
    	}

    	// Make sure we have loaded the return type
    	$this->fetchReturnType( $objPaymentDatabase );

    	if( false === valObj( $this->m_objReturnType, 'CReturnType' ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'm_objReturnType is not set' ) );

    		return false;
    	}

    	$strAccountNumber = $this->getCheckAccountNumber();

    	switch( $this->m_intReturnTypeId ) {
    		case CReturnType::INCORRECT_DFI_ACCOUNT_NUMBER:
    		case CReturnType::INCORRECT_ROUTING_NUMBER_AND_INCORRECT_DFI_ACCOUNT_NUMBER:
    		case CReturnType::INCORRECT_INDIVIDUAL_NAME_RECEIVING_COMPANY_NAME:
    		case CReturnType::INCORRECT_DFI_ACCOUNT_NUMBER_AND_INCORRECT_TRANSACTION_CODE:
    		case CReturnType::INCORRECT_ROUTING_NUMBER_INCORRECT_DFI_ACCOUNT_NUMBER_AND_INCORRECT_TRANSACTION_CODE:
    			// Delete stored billing info
    			if( NULL == $objCustomerPaymentAccount ) {
    				$objCustomerPaymentAccount = new CCustomerPaymentAccount();
    			}

    			$boolResult = $objCustomerPaymentAccount->clearStoredBillingInfo( $this->m_objArPayment->getCustomerPaymentAccountId(), $this->m_objArPayment->getCustomerId(),
    				$this->m_intCid, $this->m_strCheckRoutingNumber, $strAccountNumber, $intUserId, $objClientDatabase );

					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Deleting stored billing for customer payment account id (' . $this->m_objArPayment->getCustomerPaymentAccountId() . '): ' . $boolResult ) );

				if( false == $boolResult ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to delete stored billing for customer payment account id: ' . $this->m_objArPayment->getCustomerPaymentAccountId() ) );
					$this->addErrorMsgs( $objCustomerPaymentAccount->getErrorMsgs() );

					// continue anyway
				}

    			// Delete recurring billing
    			if( false == is_null( $this->m_objArPayment ) && false == is_null( $this->m_objArPayment->getScheduledPaymentId() ) ) {
    				$objScheduledPayment = CScheduledPayments::fetchScheduledPaymentByIdByCid( $this->m_objArPayment->getScheduledPaymentId(), $this->getCid(), $objClientDatabase );

					$boolResult = $objScheduledPayment->delete( SYSTEM_USER_ID, $objClientDatabase, $strMessage = NULL, $objWebsite = NULL, $boolIsResidentPortalPayment = false );

					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Deleting scheduled payment id: ' . $objScheduledPayment->getId() . ': ' . $boolResult ) );

    				if( false == $boolResult ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to delete scheduled payment id: ' . $objScheduledPayment->getId() ) );
    					$this->addErrorMsgs( $objScheduledPayment->getErrorMsgs() );

    					// continue anyway
    				}
    			}
    			break;

    		case CReturnType::INCORRECT_TRANSACTION_CODE:
				$this->processNoticeOfChangeIncorrectTransactionCode( $intUserId, $objNachaReturnAddendaDetailRecord, $this->m_objArPayment, $this->m_objReturnType, $objClientDatabase, $objEmailDatabase );

//    			// Update stored billing info
//
//    			$intCorrectedAccountTypeId = $objNachaReturnAddendaDetailRecord->getCorrectedAccountTypeId();
//    			$strCorrectedData = $objNachaReturnAddendaDetailRecord->getCorrectedTransactionCode();
//
//    			if( 0 === $intCorrectedAccountTypeId ) {
//    				// we were unable to get the corrected account type, notify accounting
//    				$strProcessingEmail = 'ERROR: The system was unable to automatically update account type id (could not get corrected account type id ) for' . "\n" . 'Transaction code: ' . $strCorrectedData . "\n" . 'Corrected Account Type Id: ' . ( int ) $intCorrectedAccountTypeId . "\n" . 'Client: ' . $this->m_objArPayment->getCid() . "\n" . 'Customer Payment: ' . $this->m_objArPayment->getId() . "\n\n" . 'The reason for the return was: ' . "\n" . 'Code: ' . $this->m_objReturnType->getCode() . "\n" . 'Name: ' . $this->m_objReturnType->getName() . "\n" . 'Description: ' . $this->m_objReturnType->getDescription();
//
//    				$this->sendProcessingEmailNotification( $strProcessingEmail, $objEmailDatabase );
//
//    			} else {
//    				// update the stored billing
//
//    				$objCustomerPaymentAccount = CCustomerPaymentAccounts::fetchCustomerPaymentAccountByIdByCid( $this->m_objArPayment->getCustomerPaymentAccountId(), $this->getCid(), $objClientDatabase );
//					if( NULL != $objCustomerPaymentAccount ) {
//						$objCustomerPaymentAccount->setCheckAccountTypeId( $intCorrectedAccountTypeId );
//						if( false === $objCustomerPaymentAccount->update( $intUserId, $objClientDatabase ) ) {
//							$this->addErrorMsgs( $objCustomerPaymentAccount->getErrorMsgs() );
//
//							//Notify accounting
//							$strProcessingEmail = 'ERROR: The system was unable to automatically update account type id (could not update customer payment account ) for' . "\n" . 'Transaction code: ' . $strCorrectedData . "\n" . 'Corrected Account Type Id: ' . ( int ) $intCorrectedAccountTypeId . "\n" . 'Client: ' . $this->m_objArPayment->getCid() . "\n" . 'Customer Payment: ' . $this->m_objArPayment->getId() . "\n\n" . 'The reason for the return was: ' . "\n" . 'Code: ' . $this->m_objReturnType->getCode() . "\n" . 'Name: ' . $this->m_objReturnType->getName() . "\n" . 'Description: ' . $this->m_objReturnType->getDescription();
//
//							foreach( $objCustomerPaymentAccount->getErrorMsgs() as $objErrorMsg ) {
//								$strProcessingEmail .= "\n" . $objErrorMsg->getMessage();
//							}
//
//							$this->sendProcessingEmailNotification( $strProcessingEmail, $objEmailDatabase );
//
//						}
//					}
//
//    				//$strDebugEmail .= 'Updated check account type id for customer payment account ' . $objCustomerPaymentAccount->getId() . ' to : ' . ( int ) $intCorrectedAccountTypeId . '<br />';
//    			}
    			break;

    		case CReturnType::INCORRECT_ROUTING_NUMBER:
				$this->processNoticeOfChangeInvalidRoutingNumber( $intUserId, $objNachaReturnAddendaDetailRecord, $this->m_objArPayment, $this->m_objReturnType, $objClientDatabase, $objEmailDatabase );
				$this->m_strCorrectedRoutingNo = $objNachaReturnAddendaDetailRecord->getCorrectedRoutingNumber();
				break;

    		case CReturnType::INCORRECT_FOREIGN_RECEIVING_DFI_IDENTIFICATION:
    			return true;

    		case CReturnType::INCORRECT_INDIVIDUAL_IDENTIFICATION_NUMBER:
    			return true;

    		default:
				return false;
    	}

    	// Send email to customer
    	if( NULL == $objPaymentEmailer ) {
    		$objPaymentEmailer = new CPaymentEmailer();
    	}

		$objCustomer = $this->m_objArPayment->getOrFetchCustomer( $objClientDatabase );
		$objProperty = $this->m_objArPayment->getOrFetchProperty( $objClientDatabase );

		if( true == valObj( $objCustomer, 'CCustomer' ) && true == valStr( $objCustomer->getPreferredLocaleCode() ) ) {
			$strLocale = $objCustomer->getPreferredLocaleCode();
		} else {
			$strLocale = ( true == valObj( $objProperty, 'CProperty' ) && true == valStr( $objProperty->getLocaleCode() ) ) ? $objProperty->getLocaleCode() : CLanguage::ENGLISH;
		}

		try{
			CLocaleContainer::createService()->setPreferredLocaleCode( $strLocale, $objClientDatabase, $objProperty );
			$mixEmailResult = $objPaymentEmailer->emailNoticeOfChange( $this->m_objArPayment, $objNachaReturnAddendaDetailRecord->getNocCodeDescriptions(), $objClientDatabase, $objEmailDatabase, NULL, NULL, NULL, $this->m_strCorrectedRoutingNo );
			CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );
		} catch( Exception $objException ) {
			CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );
		}

    	if( false === $mixEmailResult || NULL === $mixEmailResult ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to email notice of change for ar payment: ' . $this->m_objArPayment->getId() ) );
    		$this->addErrorMsgs( $objPaymentEmailer->getErrorMsgs() );

    		return false;
    	}

		return true;

    }

    public function processNoticeOfChangeCompanyPayment( $intUserId, $objNachaReturnAddendaDetailRecord, $objPaymentDatabase, $objEmailDatabase, $objAdminDatabase ) {

    	// Make sure we have loaded the company payment
    	$this->fetchCompanyPayment( $objAdminDatabase );

    	if( false === valObj( $this->m_objCompanyPayment, 'CCompanyPayment' ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'm_objCompanyPayment is not set ' ) );

    		return false;
    	}

    	// Make sure we have loaded the return type
    	$this->fetchReturnType( $objPaymentDatabase );

    	if( false === valObj( $this->m_objReturnType, 'CReturnType' ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'm_objReturnType is not set' ) );

    		return false;
    	}

    	switch( $this->m_intReturnTypeId ) {

    		case CReturnType::INCORRECT_ROUTING_NUMBER:
    			$this->processNoticeOfChangeInvalidRoutingNumberForCompanyPayment( $intUserId, $objNachaReturnAddendaDetailRecord, $this->m_objCompanyPayment, $this->m_objReturnType, $objPaymentDatabase, $objAdminDatabase, $objEmailDatabase );
    			break;

    		case CReturnType::INCORRECT_TRANSACTION_CODE:
    			$this->processNoticeOfChangeInvalidCheckAccountTypeForCompanyPayment( $intUserId, $objNachaReturnAddendaDetailRecord, $this->m_objCompanyPayment, $this->m_objReturnType, $objPaymentDatabase, $objAdminDatabase, $objEmailDatabase );
    			break;

    		case CReturnType::INCORRECT_DFI_ACCOUNT_NUMBER:
    		case CReturnType::INCORRECT_ROUTING_NUMBER_AND_INCORRECT_DFI_ACCOUNT_NUMBER:
    		case CReturnType::INCORRECT_INDIVIDUAL_NAME_RECEIVING_COMPANY_NAME:
    		case CReturnType::INCORRECT_DFI_ACCOUNT_NUMBER_AND_INCORRECT_TRANSACTION_CODE:
    		case CReturnType::INCORRECT_ROUTING_NUMBER_INCORRECT_DFI_ACCOUNT_NUMBER_AND_INCORRECT_TRANSACTION_CODE:
    		case CReturnType::INCORRECT_FOREIGN_RECEIVING_DFI_IDENTIFICATION:
    		case CReturnType::INCORRECT_INDIVIDUAL_IDENTIFICATION_NUMBER:
    			return true;

    		default:
    			return false;
    	}

    	return true;

    }

    public function processNoticeOfChangeSettlementDistribution( $intUserId, $objNachaReturnAddendaDetailRecord, $objPaymentDatabase, $objEmailDatabase, $objAdminDatabase ) {

    	// Make sure we have loaded the settlement distribution
    	$this->fetchSettlementDistribution( $objPaymentDatabase );

    	if( false === valObj( $this->m_objSettlementDistribution, 'CSettlementDistribution' ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'Object CSettlementDistribution is not set ' ) );

    		return false;
    	}

    	// Make sure we have loaded the return type
    	$this->fetchReturnType( $objPaymentDatabase );

    	if( false === valObj( $this->m_objReturnType, 'CReturnType' ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'm_objReturnType is not set' ) );

    		return false;
    	}

    	switch( $this->m_intReturnTypeId ) {

    		case CReturnType::INCORRECT_ROUTING_NUMBER:
    			$this->processNoticeOfChangeInvalidRoutingNumberForSettlementDistribution( $intUserId, $objNachaReturnAddendaDetailRecord, $this->m_objSettlementDistribution, $this->m_objReturnType, $objPaymentDatabase, $objEmailDatabase, $objAdminDatabase );
    			break;

    		case CReturnType::INCORRECT_DFI_ACCOUNT_NUMBER:
    		case CReturnType::INCORRECT_ROUTING_NUMBER_AND_INCORRECT_DFI_ACCOUNT_NUMBER:
    		case CReturnType::INCORRECT_INDIVIDUAL_NAME_RECEIVING_COMPANY_NAME:
    		case CReturnType::INCORRECT_DFI_ACCOUNT_NUMBER_AND_INCORRECT_TRANSACTION_CODE:
    		case CReturnType::INCORRECT_ROUTING_NUMBER_INCORRECT_DFI_ACCOUNT_NUMBER_AND_INCORRECT_TRANSACTION_CODE:
    			return true;

    		case CReturnType::INCORRECT_TRANSACTION_CODE:
				$this->processNoticeOfChangeInvalidCheckAccountTypeForSettlementDistribution( $intUserId, $objNachaReturnAddendaDetailRecord, $this->m_objSettlementDistribution, $this->m_objReturnType, $objPaymentDatabase, $objEmailDatabase, $objAdminDatabase );
    			break;

    		case CReturnType::INCORRECT_FOREIGN_RECEIVING_DFI_IDENTIFICATION:
    			return true;

    		case CReturnType::INCORRECT_INDIVIDUAL_IDENTIFICATION_NUMBER:
    			return true;

    		default:
    			return false;
    	}

    	return true;

    }

    public function blacklistAccountInfo( $intSystemEmailId, $objNachaReturnAddendaDetailRecord, $objPaymentDatabase ) {

		$strAccountNumber = $this->getCheckAccountNumber();

		if( true == empty( $strAccountNumber ) ) {
			return false;
		}

    	$arrmixLookupString = array();
    	$strCorrectedData = NULL;
    	$intPaymentBlacklistTypeId = CPaymentBlacklistType::BANK_ACCOUNT_ROUTING_NUMBER;

    	switch( $this->m_intReturnTypeId ) {
    		case CReturnType::INCORRECT_DFI_ACCOUNT_NUMBER:
    		case CReturnType::INCORRECT_ROUTING_NUMBER_AND_INCORRECT_DFI_ACCOUNT_NUMBER:
			case CReturnType::ACCOUNT_CLOSED:
			case CReturnType::NO_ACCOUNT_OR_UNABLE_TO_LOCATE_ACCOUNT:
				if( true == is_null( $this->m_strCheckRoutingNumber ) ) {
					return false;
				}

    			// Blacklist the account number and routing number

    			$arrmixLookupString = array( $this->m_strCheckRoutingNumber, $strAccountNumber );
    			$intPaymentBlacklistTypeId = CPaymentBlacklistType::BANK_ACCOUNT_ROUTING_NUMBER;
    			break;

    		case CReturnType::INCORRECT_ROUTING_NUMBER:
				if( true == is_null( $this->m_strCheckRoutingNumber ) ) {
					return false;
				}

    			// Blacklist account number and routing number, but include the corrected routing number

    			$arrmixLookupString = array( $this->m_strCheckRoutingNumber, $strAccountNumber );
    			$strCorrectedData = $objNachaReturnAddendaDetailRecord->getCorrectedRoutingNumber();
    			$intPaymentBlacklistTypeId = CPaymentBlacklistType::ROUTING_NUMBER;
    			break;

    		case CReturnType::INCORRECT_INDIVIDUAL_NAME_RECEIVING_COMPANY_NAME:
    			if( true == is_null( $this->m_strCheckRoutingNumber ) || true == empty( $this->m_strReceivingCompanyName ) || 0 == strlen( trim( $this->m_strReceivingCompanyName ) ) ) {
    				return false;
    			}

    			$arrmixLookupString = array( $this->m_strCheckRoutingNumber, $strAccountNumber, $this->m_strReceivingCompanyName );
    			$intPaymentBlacklistTypeId = CPaymentBlacklistType::BANK_ACCOUNT_ROUTING_NUMBER_NAME;
    			break;

    		case CReturnType::INCORRECT_TRANSACTION_CODE:
    		case CReturnType::INCORRECT_DFI_ACCOUNT_NUMBER_AND_INCORRECT_TRANSACTION_CODE:
    		case CReturnType::INCORRECT_ROUTING_NUMBER_INCORRECT_DFI_ACCOUNT_NUMBER_AND_INCORRECT_TRANSACTION_CODE:
    			if( is_null( $this->m_intCheckAccountTypeId ) ) {
    				return false;
    			}

    			$arrmixLookupString = array( $this->m_strCheckRoutingNumber, $strAccountNumber, $this->m_intCheckAccountTypeId );
    			$intPaymentBlacklistTypeId = CPaymentBlacklistType::BANK_ACCOUNT_ROUTING_NUMBER_TYPE;
    			$strCorrectedData = $objNachaReturnAddendaDetailRecord->getCorrectedTransactionCode();
    			break;

    		case CReturnType::INCORRECT_FOREIGN_RECEIVING_DFI_IDENTIFICATION:
    			return true;

    		case CReturnType::INCORRECT_INDIVIDUAL_IDENTIFICATION_NUMBER:
    			return true;

    		default:
				return false;
    	}

    	$strBlacklistReason = $objNachaReturnAddendaDetailRecord->getNocCodeDescription();

		if( '' == $strBlacklistReason && ( CReturnType::ACCOUNT_CLOSED == $this->m_intReturnTypeId || CReturnType::NO_ACCOUNT_OR_UNABLE_TO_LOCATE_ACCOUNT == $this->m_intReturnTypeId ) ) {
			$this->fetchReturnType( $objPaymentDatabase );
			if( true == valObj( $this->m_objReturnType, 'CReturnType' ) ) {
				$strBlacklistReason = $this->m_objReturnType->getDescription();
			}
		}

    	// Create blacklist entry
    	$objBlacklistEntry = CPaymentBlacklistEntry::factory( $arrmixLookupString, $strCorrectedData, $strBlacklistReason, $intSystemEmailId, $intPaymentBlacklistTypeId );

    	if( false == $objBlacklistEntry->insert( SYSTEM_USER_ID, $objPaymentDatabase ) ) {
    		$this->addErrorMsgs( $objBlacklistEntry->getErrorMsgs() );
    		return true;
    	}

    	// Update the nacha addenda detail record with the blacklist entry id
    	$intBlacklistEntryId = $objBlacklistEntry->getId();

    	$objNachaReturnAddendaDetailRecord->setPaymentBlacklistEntryId( $intBlacklistEntryId );

    	if( false == $objNachaReturnAddendaDetailRecord->update( SYSTEM_USER_ID, $objPaymentDatabase ) ) {
    		$this->addErrorMsgs( $objNachaReturnAddendaDetailRecord->getErrorMsgs() );
    	}

    	return true;
    }

	public function sendProcessingEmailNotification( $strMessage, $objEmailDatabase, $objSystemEmail = NULL, $strSubject = 'Notice of Change' ) {
    	if( NULL == $objSystemEmail ) {
    		$objSystemEmail = new CSystemEmail();
    	}

    	$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
    	$objSystemEmail->setSubject( $strSubject );

    	$objSystemEmail->setToEmailAddress( CSystemEmail::PROCESSING_EMAIL_ADDRESS );

    	$objSystemEmail->setReplyToEmailAddress( CSystemEmail::PROCESSING_EMAIL_ADDRESS );

    	$objSystemEmail->setHtmlContent( $strMessage );
    	$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::BANK_NOTIFICATION );

    	if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase, false, true ) ) {
    		$this->addErrorMsgs( $objSystemEmail->getErrorMsgs() );

    		return false;
    	}

    	return $objSystemEmail;
    }

	private function blacklistAndNotifyProcessing( $objNachaReturnAddendaDetailRecord, $objAdminDatabase, $objPaymentDatabase, $objEmailDatabase ) {

		$this->fetchReturnType( $objPaymentDatabase );

		$strNotifiedEmail = ( 1 == $objNachaReturnAddendaDetailRecord->getIsNoticeOfChange() ? 'A notice of change ' : 'A return resulting in an account being blacklisted ' );

		if( true == is_numeric( $this->m_intSettlementDistributionId ) ) {
			$this->fetchSettlementDistribution( $objPaymentDatabase );

			if( true == valObj( $this->m_objSettlementDistribution, 'CSettlementDistribution' ) ) {
				$strNotifiedEmail .= 'has been received from bank on behalf of settlement distribution for Client: ' . $this->m_objSettlementDistribution->getCid() . ', Settlement Distribution: ' . $this->m_objSettlementDistribution->getId() . '.  The reason cited was: ' . $this->m_objReturnType->getCode() . ' : ' . $this->m_objReturnType->getName() . ' : ' . $this->m_objReturnType->getDescription();
			} else {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to load settlement distribution # ' . $this->m_intSettlementDistributionId ) );
				return false;
			}
		} elseif( true == is_numeric( $this->m_intArPaymentId ) ) {
			$this->fetchArPayment( $objPaymentDatabase );

			if( true == valObj( $this->m_objArPayment, 'CArPayment' ) ) {
				$strNotifiedEmail .= 'has been received from bank on behalf of customer payment for Client: ' . $this->m_objArPayment->getCid() .
					', Customer Payment: ' . $this->m_objArPayment->getId() . '.  The reason cited was: ' . $this->m_objReturnType->getCode() . ' : ' . $this->m_objReturnType->getName() .
					' : ' . $this->m_objReturnType->getDescription() . '<br />' . $objNachaReturnAddendaDetailRecord->getNocCodeDescriptions();
			} else {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to load ar payment # ' . $this->m_intArPaymentId ) );
				return false;
			}
		} elseif( true == is_numeric( $this->m_intCompanyPaymentId ) ) {
			$this->fetchCompanyPayment( $objAdminDatabase );

			if( true == valObj( $this->m_objCompanyPayment, 'CCompanyPayment' ) ) {
				$strNotifiedEmail .= 'has been received from bank on behalf of company payment for Client: ' . $this->m_objCompanyPayment->getCid() . ', Company Payment: ' . $this->m_objCompanyPayment->getId() . '.  The reason cited was: ' . $this->m_objReturnType->getCode() . ' : ' . $this->m_objReturnType->getName() . ' : ' . $this->m_objReturnType->getDescription();
			} else {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to load company payment # ' . $this->m_intCompanyPaymentId ) );
				return false;
			}
		} elseif( true == is_numeric( $this->m_intClearingBatchId ) ) {
			$this->fetchClearingBatch( $objPaymentDatabase );

			if( true == valObj( $this->m_objClearingBatch, 'CClearingBatch' ) ) {
				$strNotifiedEmail .= 'has been received from bank on behalf of clearing batch for Client: ' . $this->m_objClearingBatch->getCid() . ', Clearing Batch: ' . $this->m_objClearingBatch->getId() . '.  The reason cited was: ' . $this->m_objReturnType->getCode() . ' : ' . $this->m_objReturnType->getName() . ' : ' . $this->m_objReturnType->getDescription();
			} else {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to load clearing batch # ' . $this->m_intClearingBatchId ) );
				return false;
			}
		} else {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to load payment' ) );
			return false;
		}

		$strSubject = ( 1 == $objNachaReturnAddendaDetailRecord->getIsNoticeOfChange() ? 'Notice of Change' : 'Blacklisted Nacha Return' );

		$mixResult = $this->sendProcessingEmailNotification( $strNotifiedEmail, $objEmailDatabase, $objSystemEmail = NULL, $strSubject );
		if( false === $mixResult ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to process email notification.' ) );
			return false;
		}

		$boolBlacklistResult = $this->blacklistAccountInfo( $mixResult->getId(), $objNachaReturnAddendaDetailRecord, $objPaymentDatabase );

		if( false == $boolBlacklistResult ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to blacklist account info' ) );
		}
		return $boolBlacklistResult;
	}

	// Size = 15 : Trace number is prefix (8 digits) + position (7 digits). (TRACE NUMBER prefix is typically ACH immediate destination *OR* ACH immediate origin defined for processing bank)

	public function buildTraceNumber( $strTraceNumberPrefix ) {

		if( true == is_null( $strTraceNumberPrefix ) ) {
			trigger_error( 'Trace number prefix not properly loaded or is NULL.', E_USER_ERROR );
			exit;
		}

		if( false == isset( $this->m_intId ) ) {
			trigger_error( 'Id of Nacha File Entry was not loaded.', E_USER_ERROR );
			exit;
		}

		$intDefaultNumberOfCharactersToAppend = 7;
		if( strlen( $this->m_intId ) < $intDefaultNumberOfCharactersToAppend ) {
			$intDefaultNumberOfCharactersToAppend = strlen( $this->m_intId );
		}

		$strId = substr( ( string ) $this->m_intId, ( $intDefaultNumberOfCharactersToAppend * -1 ) );
		$strTraceNumber = ( CStrings::strNachaDef( $strTraceNumberPrefix, 8, false, false, '0' ) . str_pad( $strId, 7, '0', STR_PAD_LEFT ) );
		$this->setTraceNumber( $strTraceNumber );

		return $this->getTraceNumber();
	}

    // The check digit field is a funky modulus 10 number that is calculated by multiplying each position of the routing number by a pre-assigned value.
    // Then you add up all the results and subtract that amount from the nearest multiple of 10.

	public function buildCheckDigit() {

		$intCheckDigit = substr( $this->m_strCheckRoutingNumber, -1 );

		if( false == is_numeric( $intCheckDigit ) ) {
			trigger_error( 'Check digit did not validate. Routing Number: \'' . $this->m_strCheckRoutingNumber . '\'', E_USER_ERROR );
			exit;
		}

		$this->setCheckDigit( $intCheckDigit );

		return $this->getCheckDigit();
	}

	public function addErrorMsgs( $arrobjErrorMsgs ) {
		if( true == valArr( $arrobjErrorMsgs ) ) {
			foreach( $arrobjErrorMsgs as $objErrorMsg ) {
				if( true == valObj( $objErrorMsg, 'CErrorMsg' ) ) {
					$this->addErrorMsg( $objErrorMsg );
				}
			}
		}
	}

	public function addDistributedNote( $objArPayment, $objPaymentDatabase ) {
		$objArPaymentNote = $objArPayment->createArPaymentNote();
		$objArPaymentNote->setNote( 'Payment distribution blocked on ' . date( 'm/d/Y' ) . ' because it was returned for R03 - No Account / Bad Account.' );

		if( false === $objArPaymentNote->insert( SYSTEM_USER_ID, $objPaymentDatabase ) ) {
			trigger_error( 'Ar Payment Note was not inserted for Payment Id: ' . $objArPayment->getId() . ', Property Id: ' . $objArPayment->getPropertyId() . ' for Bad Account.', E_USER_WARNING );
		}
	}

}

?>
