<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CApplicationKeyValues
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CApplicationKeyValues extends CBaseApplicationKeyValues {

    /**
     * Fetch Functions
     */

	public static function fetchApplicationKeyValueByKeyByMerchantAccountApplicationId( $strKey, $intMerchantAccountApplicationId, $objPaymentDatabase ) {
    	return self::fetchApplicationKeyValue( 'SELECT * FROM application_key_values WHERE merchant_account_application_id = ' . ( int ) $intMerchantAccountApplicationId . ' AND key = \'' . addslashes( $strKey ) . '\'', $objPaymentDatabase );
    }

    public static function fetchApplicationKeyValuesByMerchantAccountApplicationIds( $arrintMerchantAccountApplicationIds, $objDatabase ) {

    	$strSql = 'SELECT * FROM application_key_values WHERE merchant_account_application_id IN ( ' . implode( ',', $arrintMerchantAccountApplicationIds ) . ')';

    	return self::fetchApplicationKeyValues( $strSql, $objDatabase );
    }

    public static function fetchApplicationKeyValueByMerchantAccountApplicationIds( $arrintMerchantAccountApplicationIds, $objPaymentDatabase ) {

    	$strSql = ' SELECT
    					 ma.id AS merchant_account_application_id,
					     MAX(
    					     CASE
					             WHEN akv.key = \'PACKET_SENT_TO_ECHO_ON\' THEN akv.value
					          END
					         ) AS PACKET_SENT_TO_ECHO_ON,
					     MAX(
    						 CASE
    						 	 WHEN akv.key = \'PIN_RECEIVED_ON\' THEN akv.value
    						 END
    						 ) AS PIN_RECEIVED_ON,
					     MAX(
    						 CASE
    							WHEN akv.key = \'INFO_REQUEST_SENT_ON\' THEN akv.value
    						  END
    			 			 ) AS INFO_REQUEST_SENT_ON,
					     MAX(
    					     CASE
    							WHEN akv.key = \'APPLICATION_COMPLETED_ON\' THEN akv.value
	    					  END
    						) AS APPLICATION_COMPLETED_ON
					FROM
    					merchant_account_applications ma
					    LEFT JOIN application_key_values akv ON ( ma.id = akv.merchant_account_application_id )
					WHERE
    					ma.id IN ( ' . implode( ',', $arrintMerchantAccountApplicationIds ) . ' )
					GROUP
					    BY ma.id';

    	return fetchData( $strSql, $objPaymentDatabase );

    }

	public static function fetchApplicationKeyValuesByMerchantAccountApplicationIdByByKeysByCid( $intMerchantAccountApplicationId, $arrstrKeys, $intCid, $objPaymentDatabase ) {
		if( false == valId( $intMerchantAccountApplicationId ) || false == valArr( $arrstrKeys ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						akv.*
					FROM
						merchant_account_applications maa
						JOIN application_key_values akv ON ( maa.id = akv.merchant_account_application_id AND maa.cid = akv.cid )
					WHERE
						akv.key IN (\'' . implode( '\',\'', $arrstrKeys ) . '\')
						AND akv.cid = ' . ( int ) $intCid . '
						AND akv.merchant_account_application_id = ' . ( int ) $intMerchantAccountApplicationId . '
						AND akv.value IS NOT NULL';

		return self::fetchApplicationKeyValues( $strSql, $objPaymentDatabase );
	}

}
?>