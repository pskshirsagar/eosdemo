<?php

class CMerchantChangeRequestAccount extends CBaseMerchantChangeRequestAccount {

	protected $m_boolIsUpdateBillingAccount;
	protected $m_boolIsUpdateReturnAccount;

	const ACCOUNT_MASKING_PATTERN = '/x{1,}/i';

	const MAX_ID_LINDON = 99999999;  // 100 million - 1
	const MIN_ID_IRELAND = 100000000; // 100 million
	const MAX_ID_IRELAND = 199999999; // 200 million - 1

	/**
	 *
	 * Get Functions
	 */

	public function getIsUpdateBillingAccount() {
		return $this->m_boolIsUpdateBillingAccount;
	}

	public function getIsUpdateReturnAccount() {
		return $this->m_boolIsUpdateReturnAccount;
	}

	public function getCheckAccountNumber() {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getCheckAccountNumberEncrypted() ) ) {
			return NULL;
		}

		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getCheckAccountNumberMasked() {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getCheckAccountNumberEncrypted() ) ) {
			return NULL;
		}

		$strCheckAccountNumber = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
		$intStringLength = mb_strlen( $strCheckAccountNumber );
		$strLastFour = mb_substr( $strCheckAccountNumber, -4 );
		return str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getCheckTaxNumberMasked() {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getCheckTaxNumberEncrypted() ) ) {
			return NULL;
		}

		$strCheckTaxNumber = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCheckTaxNumberEncrypted(), CONFIG_SODIUM_KEY_CHECK_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CHECK_TAX_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
		$intStringLength = mb_strlen( $strCheckTaxNumber );
		$strLastFour = mb_substr( $strCheckTaxNumber, -4 );
		return str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getCheckTaxNumber() {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getCheckTaxNumberEncrypted() ) ) {
			return NULL;
		}

		$strCheckTaxNumber = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCheckTaxNumberEncrypted(), CONFIG_SODIUM_KEY_CHECK_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CHECK_TAX_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
		return $strCheckTaxNumber;
	}

	public function getBillingAccountNumberMasked() {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getBillingAccountNumberEncrypted() ) ) {
			return NULL;
		}

		$strBillingAccountNumber = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getBillingAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
		$intStringLength = mb_strlen( $strBillingAccountNumber );
		$strLastFour = mb_substr( $strBillingAccountNumber, -4 );
		return str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getBillingAccountNumber() {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getBillingAccountNumberEncrypted() ) ) {
			return NULL;
		}

		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getBillingAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getReturnAccountNumber() {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getReturnAccountNumberEncrypted() ) ) {
			return NULL;
		}

		return  \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getReturnAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getReturnAccountNumberMasked() {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getReturnAccountNumberEncrypted() ) ) {
			return NULL;
		}

		$strReturnAccountNumber = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getReturnAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
		$intStringLength = mb_strlen( $strReturnAccountNumber );
		$strLastFour = mb_substr( $strReturnAccountNumber, -4 );
		return str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	/**
	 *
	 * Set Functions
	 */

	public function setIsUpdateBillingAccount( $intIsUpdateBillingAccount ) {
		$this->m_boolIsUpdateBillingAccount = CStrings::strToIntDef( $intIsUpdateBillingAccount, NULL, false );
	}

	public function setIsUpdateReturnAccount( $intIsUpdateReturnAccount ) {
		$this->m_boolIsUpdateReturnAccount = CStrings::strToIntDef( $intIsUpdateReturnAccount, NULL, false );
	}

	public function setCheckAccountNumber( $strCheckAccountNumber ) {
		if ( false == \Psi\Libraries\UtilFunctions\valStr( $strCheckAccountNumber ) || true == preg_match( self::ACCOUNT_MASKING_PATTERN, $strCheckAccountNumber ) ) return;
		$strCheckAccountNumber = CStrings::strTrimDef( $strCheckAccountNumber, 20, NULL, true );
		$this->setCheckAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
	}

	public function setCheckTaxNumber( $strCheckTaxNumber ) {
		if ( false == \Psi\Libraries\UtilFunctions\valStr( $strCheckTaxNumber ) || true == preg_match( self::ACCOUNT_MASKING_PATTERN, $strCheckTaxNumber ) ) return;
		$strCheckTaxNumber = CStrings::strTrimDef( $strCheckTaxNumber, 20, NULL, true );
		$this->setCheckTaxNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCheckTaxNumber, CONFIG_SODIUM_KEY_CHECK_TAX_NUMBER ) );
	}

	public function setBillingAccountNumber( $strBillingAccountNumber ) {
		if ( false == \Psi\Libraries\UtilFunctions\valStr( $strBillingAccountNumber ) || true == preg_match( self::ACCOUNT_MASKING_PATTERN, $strBillingAccountNumber ) ) return;
		$strBillingAccountNumber = CStrings::strTrimDef( $strBillingAccountNumber, 20, NULL, true );
		$this->setBillingAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strBillingAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
	}

	public function setReturnAccountNumber( $strReturnAccountNumber ) {
		if ( false == \Psi\Libraries\UtilFunctions\valStr( $strReturnAccountNumber ) || true == preg_match( self::ACCOUNT_MASKING_PATTERN, $strReturnAccountNumber ) ) return;
		$strReturnAccountNumber = CStrings::strTrimDef( $strReturnAccountNumber, 20, NULL, true );
		$this->setReturnAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strReturnAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['check_account_number'] ) ) 			$this->setCheckAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['check_account_number'] ) : $arrmixValues['check_account_number'] );
		if( true == isset( $arrmixValues['check_tax_number'] ) ) 				$this->setCheckTaxNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['check_tax_number'] ) : $arrmixValues['check_tax_number'] );

		if( true == isset( $arrmixValues['billing_account_number'] ) )			$this->setBillingAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billing_account_number'] ) : $arrmixValues['billing_account_number'] );
		if( true == isset( $arrmixValues['return_account_number'] ) )			$this->setReturnAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['return_account_number'] ) : $arrmixValues['return_account_number'] );
		if( true == isset( $arrmixValues['is_update_return_account'] ) )		$this->setIsUpdateReturnAccount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_update_return_account'] ) : $arrmixValues['is_update_return_account'] );
		if( true == isset( $arrmixValues['is_update_billing_account'] ) )		$this->setIsUpdateBillingAccount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_update_billing_account'] ) : $arrmixValues['is_update_billing_account'] );

	}

    public function valCid() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valMerchantChangeRequestId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCompanyMerchantAccountId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valAccountId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valAccountName() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strAccountName ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_name', __( 'Property name / Merchant Account Name is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valBilltoCompanyName() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoNameFirst() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoNameMiddle() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoNameLast() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoStreetLine1() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoStreetLine2() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoStreetLine3() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoCity() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoStateCode() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoProvince() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoPostalCode() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoCountryCode() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoPhoneNumber() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoEmailAddress() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoTaxNumberEncrypted() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCheckAccountTypeId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCheckNameOnAccount() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strCheckNameOnAccount ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', __( 'Account Holder&#39;s Name is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valCheckBankName() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strCheckBankName ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_bank_name', __( 'Bank name is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valCheckRoutingNumber( $objPaymentDatabase ) {
        $boolIsValid = true;

        $boolIsValid = $this->validateRoutingNumber( $this->getCheckRoutingNumber(), 'check_routing_number', $objPaymentDatabase );

        return $boolIsValid;
    }

    public function valCheckAccountType() {
    	$boolIsValid = true;

    	if( true == is_null( $this->m_intCheckAccountTypeId ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_type_id', __( 'Account Type is required.' ) ) );
    	}

    	return $boolIsValid;
    }

    public function valCheckTaxNumberEncrypted() {
    	$boolIsValid = true;

    	if( true == is_null( $this->m_strCheckTaxNumberEncrypted ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_tax_number_encrypted', __( 'Tax id number is required.' ) ) );
    	} else {
	    	$strTaxIdNumber = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCheckTaxNumberEncrypted(), CONFIG_SODIUM_KEY_CHECK_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CHECK_TAX_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	     	if( 1 === preg_match( '/-{2,}|(.*\-.*){3,}|[^0-9\-]+/', $strTaxIdNumber ) || 0 === preg_match( '/^(\d.*\d)$/', \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCheckTaxNumberEncrypted(), CONFIG_SODIUM_KEY_CHECK_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CHECK_TAX_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] ) ) ) {
	    		$boolIsValid = false;
	    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_tax_number_encrypted', __( 'Tax id number is invalid.' ) ) );
	    	}

			$strTaxIdNumber = str_replace( '-', '', $strTaxIdNumber );
	    	if( 0 === preg_match( '/^(\d{9})$/', $strTaxIdNumber ) ) {
	    		$boolIsValid = false;
	    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_tax_number_encrypted', __( 'Tax id number must be {%d, 0} digits.', [ 9 ] ) ) );
	    	}
    	}

    	return $boolIsValid;
    }

    public function valCheckTaxLegalName() {
    	$boolIsValid = true;

    	if( true == is_null( $this->m_strCheckTaxLegalName ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_tax_legal_name', __( 'Tax legal name is required.' ) ) );
    	}

		if( false == preg_match( '/^[a-z0-9\-\.\&\: ]*$/i', $this->m_strCheckTaxLegalName ) && true == $this->getIsNewAccountRequest() && false == is_null( $this->m_strCheckTaxLegalName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_tax_legal_name', __( 'Special characters are not allowed in Tax legal  name.' ) ) );
		}

    	return $boolIsValid;
    }

    public function validateRoutingNumber( $strRoutingNumber, $strFieldName, $objPaymentDatabase ) {

        if( false == isset ( $objPaymentDatabase ) ) {
        	trigger_error( 'DATABASE IS REQUIRED IN ROUTING NUMBER VALIDATION.', E_USER_WARNING );
        }

    	$boolIsValid = true;

    	$strField = \Psi\CStringService::singleton()->ucfirst( trim( str_replace( 'check', '', str_replace( '_', ' ', $strFieldName ) ) ) );

    	if( true == empty( $strRoutingNumber ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s, 0} is required.', [ $strField ] ), 605 ) );
        }

        if( 0 !== preg_match( '/[^0-9]/', $strRoutingNumber ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s, 0} must be a number.', [ $strField ] ), 622 ) );
			return false;
        }

        if( 9 !== mb_strlen( $strRoutingNumber ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( '{%s, 0} must be {%d, 1} digits long.', [ $strField, 9 ] ), 623 ) );
			return false;
        }

		$objFedAchParticipant = \Psi\Eos\Payment\CFedAchParticipants::createService()->fetchFedAchParticipantByRoutingNumber( $strRoutingNumber, $objPaymentDatabase );

		if( true == is_null( $objFedAchParticipant ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( ' {%s, 0} is not valid.', [ $strField ] ), 624 ) );
		}

		return $boolIsValid;
    }

    public function valBillingRoutingNumber( $objPaymentDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $objPaymentDatabase ) ) {
			$boolIsValid = $this->validateRoutingNumber( $this->getBillingRoutingNumber(), 'billing_routing_number', $objPaymentDatabase );
		}

		return $boolIsValid;
	}

	public function valReturnRoutingNumber( $objPaymentDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $objPaymentDatabase ) ) {
			$boolIsValid = $this->validateRoutingNumber( $this->getReturnRoutingNumber(), 'return_routing_number', $objPaymentDatabase );
		}

		return $boolIsValid;
	}

    public function valCheckAccountNumberEncrypted() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strCheckAccountNumberEncrypted ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', __( 'Account number is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valInvoicesEmailed() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInvoicesMailed() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valProcessedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsNewAccountRequest() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valEffectiveDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) && false == is_null( $this->getEffectiveDate() ) && strtotime( date( 'm/d/y' ) ) > strtotime( $this->getEffectiveDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_Date', __( 'Date must be greater than or equal to today\'s date.' ), 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

  	public function valBillingNameOnAccount() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strBillingNameOnAccount ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_name_on_account', __( 'Billing Property / Account name is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valBillingBankName() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strBillingBankName ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_bank_name', __( 'Billing bank name is required.' ) ) );
        }

        return $boolIsValid;
    }

   public function valBillingAccountNumberEncrypted() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strBillingAccountNumberEncrypted ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reutrn_account_number_encrypted', __( 'Billing account number is required.' ) ) );
        }

        return $boolIsValid;
   }

  	public function valReturnNameOnAccount() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strReturnNameOnAccount ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reutrn_name_on_account', __( 'Return Property / Account name is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valReturnBankName() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strReturnBankName ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reutrn_bank_name', __( 'Return bank name is required.' ) ) );
        }

        return $boolIsValid;
    }

   public function valReturnAccountNumberEncrypted() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strReturnAccountNumberEncrypted ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reutrn_account_number_encrypted', __( 'Return account number is required.' ) ) );
        }

        return $boolIsValid;
   }

    public function validate( $strAction, $objPaymentDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
					 	$boolIsValid &= $this->valAccountName();
					 	$boolIsValid &= $this->valCheckNameOnAccount();
					 	$boolIsValid &= $this->valCheckBankName();
					 	$boolIsValid &= $this->valCheckAccountNumberEncrypted();
					 	$boolIsValid &= $this->valCheckRoutingNumber( $objPaymentDatabase );
					 	$boolIsValid &= $this->valEffectiveDate();
					 	$boolIsValid &= $this->valCheckAccountType();

					 	$boolIsValid &= $this->valCheckTaxNumberEncrypted();
					 	$boolIsValid &= $this->valCheckTaxLegalName();

						if( true == $this->getIsAlternateBillingAccount() ) {
						 	$boolIsValid &= $this->valBillingNameOnAccount();
						 	$boolIsValid &= $this->valBillingBankName();
						 	$boolIsValid &= $this->valBillingAccountNumberEncrypted();
						 	$boolIsValid &= $this->valBillingRoutingNumber( $objPaymentDatabase );
						}

						if( true == $this->getIsAlternateReturnAccount() ) {
						 	$boolIsValid &= $this->valReturnNameOnAccount();
						 	$boolIsValid &= $this->valReturnBankName();
						 	$boolIsValid &= $this->valReturnAccountNumberEncrypted();
						 	$boolIsValid &= $this->valReturnRoutingNumber( $objPaymentDatabase );
						}
				break;

            case 'close_merchant_change_request':
            	break;

            case VALIDATE_DELETE:
            	break;

			case 'INTERNATIONAL_ACCOUNT_INSERT':
			case 'INTERNATIONAL_ACCOUNT_UPDATE':
            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function fetchMerchantAccount( $objDatabase ) {
    	return CMerchantAccounts::fetchCustomMerchantAccountByIdByCid( $this->getCompanyMerchantAccountId(), $this->getCid(), $objDatabase );
    }

    public function fetchCompanyMerchantAccount( $objDatabase ) {
		return \Psi\Eos\Payment\CCompanyMerchantAccounts::createService()->fetchCompanyMerchantAccountById( $this->getCompanyMerchantAccountId(), $objDatabase );
	}

	function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setId( $this->fetchNextId( $objDatabase ) );

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	// This function is intentionally overridden in order to jump some range of ids from payments.ar_payments table.
	// Do not move this function from here.

	public function fetchNextId( $objDatabase, $strTableName = NULL ) {
		$this->updateIdSequence( $objDatabase );

		return parent::fetchNextId( $objDatabase );
	}

	public function updateIdSequence( $objDatabase, $intSequenceIncrementValue = 1 ) {
		$strSql = 'SELECT last_value AS id FROM merchant_change_request_accounts_id_seq';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( false != valArr( $arrmixData ) && false != isset( $arrmixData[0]['id'] ) ) {
			$intStartingId = $arrmixData[0]['id'];
			$intEndingId = $intStartingId + $intSequenceIncrementValue;
			if( CONFIG_CLOUD_ID == CCloud::LINDON_ID && self::MAX_ID_LINDON < $intEndingId ) {
				// we're in the Lindon cloud and the payment id is greater than 100 million
				$strSql = 'SELECT id FROM merchant_change_request_accounts WHERE id < ' . self::MAX_ID_LINDON . ' ORDER BY id DESC LIMIT 1';
				$intMinIdForCloud = 1;
			} elseif( CONFIG_CLOUD_ID == CCloud::IRELAND_ID && ( self::MAX_ID_IRELAND < $intEndingId || self::MIN_ID_IRELAND > $intStartingId ) ) {
				// we're in the Ireland cloud and the payment id is not between 100 and 200 million
				$strSql = 'SELECT id FROM merchant_change_request_accounts WHERE id BETWEEN ' . self::MIN_ID_IRELAND . ' AND ' . self::MAX_ID_IRELAND . ' ORDER BY id DESC LIMIT 1';
				$intMinIdForCloud = self::MIN_ID_IRELAND;
			} else {
				// no adjustment necessary
				return $intStartingId;
			}

			$arrmixResult = fetchData( $strSql, $objDatabase );
			if( !valArr( $arrmixResult ) ) {
				$intMaxId = $intMinIdForCloud;
			} else {
				$intMaxId = $arrmixResult[0]['id'];
			}
			$intNextId = $intMaxId + 1;
			$strSql = 'SELECT setval( \'merchant_change_request_accounts_id_seq\', ' . ( int ) $intNextId . ', FALSE )';
			fetchData( $strSql, $objDatabase );

			return $intNextId;
		}
	}

}
?>