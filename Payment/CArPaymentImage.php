<?php


class CArPaymentImage extends CBaseArPaymentImage {

	/** @var CUser */
	protected $m_intUserId;
	protected $m_objCompanyMerchantAccount;
	protected $m_objClient;
	protected $m_objArPayment;
	protected $m_objProcessingBankAccount;

	protected $m_intProcessingBankAccountId;
	protected $m_intProcessingBankId;

	protected $m_intImageWidth;
	protected $m_intImageHeight;
	protected $m_intImageXResolution;
	protected $m_intImageYResolution;
	protected $m_boolTiffInPaymentObjectStore;

	protected $m_strDecryptedImageName;

	protected $m_strFullFileName;
	protected $m_strFullDecryptedFileName;
	protected $m_strCacheImagePath;
	protected $m_strCacheFileName;
	protected $m_strPaymentDatetime;

	protected $m_arrstrCheckFrontGifImageChildren;
	protected $m_arrstrCheckReverseGifImageChildren;

	/**
	 * @var CDatabase
	 */
	protected $m_objPaymentDatabase;

	const MESSAGE_TIMEOUT = 30;
	const X937_IMAGES_FILE_ID = 7;
	const DEFAULT_THUMB_SIZE = 350;
	const CACHING_IMAGE_TYPE_FULL_GIF = '.full.gif';
	const CACHING_IMAGE_TYPE_THUMBNAIL_GIF = '.thumbnail.gif';
	const CACHING_IMAGE_TYPE_FULL_MASKED_GIF = '.full.masked.gif';
	const CACHING_IMAGE_TYPE_THUMBNAIL_MASKED_GIF = '.thumbnail.masked.gif';
	const CHECK_IMAGES_PATH = 'check_21_images/';
	const CHECK21_LOCAL_DIR = PATH_PAYMENT_MOUNTS; // I know this seems uneeded, but we need to be consistent between getImagePath and objectStoreCleanKey
	const CHECK21_LOCAL_TEMP_DIR = PATH_NON_BACKUP_MOUNTS; // I know this seems uneeded, but we need to be consistent between getImagePath and objectStoreCleanKey
	const VALID_IMAGE_LENGTH = 3;

	public function __construct() {
		parent::__construct();

		$this->m_boolTiffInPaymentObjectStore = false;

		return;
	}

	public function getImagePath() {
//		return CConfig::get( 'cache_directory' ) . self::CHECK_IMAGES_PATH . $this->m_strImagePath; // for migration reasons: this used to be PATH_PAYMENT_MOUNTS_CHECK21_IMAGES . $this->m_strImagePath;

		return self::CHECK21_LOCAL_DIR . self::CHECK_IMAGES_PATH . $this->m_strImagePath;
	}

	public function getImagePathWithoutMountPath() {
		return $this->m_strImagePath; // when all that is needed is: intClientId/2017/01/27
	}

	/**
	 * Set Functions
	 */

	public function setCacheFileName( $strCacheFileName ) {
		$this->m_strCacheFileName = $strCacheFileName;
	}

	// This function overrides the parent class so string functions don't alter image content.

	public function setImageContent( $strImageContent ) {
		$this->m_strImageContent = $strImageContent;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['image_content'] ) ) $this->setImageContent( $arrmixValues['image_content'] );
		if( true == isset( $arrmixValues['processing_bank_account_id'] ) ) $this->setProcessingBankAccountId( $arrmixValues['processing_bank_account_id'] );
		if( true == isset( $arrmixValues['processing_bank_id'] ) ) $this->setProcessingBankId( $arrmixValues['processing_bank_id'] );

		return;
	}

	public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
		$this->m_intProcessingBankAccountId = $intProcessingBankAccountId;
	}

	public function setProcessingBankId( $intProcessingBankId ) {
		$this->m_intProcessingBankId = $intProcessingBankId;
	}

	public function setProcessingBankAccount( $objProcessingBankAccount ) {
		$this->m_objProcessingBankAccount = $objProcessingBankAccount;
	}

	public function setCompanyMerchantAccount( $objCompanyMerchantAccount ) {
		$this->m_objCompanyMerchantAccount = $objCompanyMerchantAccount;
	}

	public function setClient( $objClient ) {
		$this->m_objClient = $objClient;
	}

	public function setArPayment( $objArPayment ) {
		$this->m_objArPayment = $objArPayment;
	}

	public function setPaymentDatetime( $strPaymentDatetime ) {
		$this->m_strPaymentDatetime = CStrings::strTrimDef( $strPaymentDatetime, -1, NULL, true );
	}

	public function setPaymentDatabase( $objPaymentDatabase ) {
		$this->m_objPaymentDatabase = $objPaymentDatabase;
	}

	/**
	 * @return CDatabase
	 */
	public function getPaymentDatabase() {
		return $this->m_objPaymentDatabase;
	}

	/**
	 * @return DateTime
	 * @throws Exception
	 */
	public function getPaymentDatetime() {
		return new DateTime( $this->m_strCreatedOn );
	}

	public function setImageName( $strImageName ) {

		$arrstrImageNameParts = $this->getEncryptedPathParts( $strImageName );
		if( is_null( $arrstrImageNameParts['encryption_extension'] ) ) {
			$arrstrImageNameParts['encryption_extension'] = \Psi\Libraries\Cryptography\Providers\CSodiumCryptoProvider::CRYPTO_EXTENSION_NAME;
		}

		parent::setImageName( $arrstrImageNameParts['unencrypted_path'] . '.' . $arrstrImageNameParts['encryption_extension'] );

		$this->m_strDecryptedImageName = $arrstrImageNameParts['unencrypted_path'];

		$this->m_strFullFileName = $this->getImagePath() . $this->getImageName();
		$this->m_strFullDecryptedFileName = $this->getImagePath() . $this->m_strDecryptedImageName;
	}

	/**
	 * identifies dimensions. sets values before insert and while image exists locally.
	 * This is for then accessed by CArPaymentBilling->calculateAndSetCheckIsConverted() via getters to determine if check image could be used for ACH.
	 * @return bool
	 * @throws Exception
	 */
	public function calculateImageSizeAndResolution() {

		$strLocalPathFileName = $this->m_strFullDecryptedFileName;

		if( false == $this->isFile( $strLocalPathFileName ) && false == $this->writeDecryptedFile( $this->m_strFullFileName, $this->m_strFullDecryptedFileName ) ) {
			$this->handlePaymentErrorMessages( 'Could Not locate File: ' . $strLocalPathFileName );
			return false;
		}

		// Use ImageMagik 'identify' process to get image width, height, xres, yres
		$strOutput = $this->exec( 'identify -quiet -format "%w:%h|%x:%y" ' . $strLocalPathFileName . ' 2>&1', 'ImageMagik identify process Failed' );

		$arrstrImageInfo = explode( '|', $strOutput ); // ["1107:475","200:200"]

		if( false == \Psi\Libraries\UtilFunctions\valArr( $arrstrImageInfo ) ) {
			$this->handlePaymentErrorMessages( 'ImageMagik identify failed. ' . $strLocalPathFileName );
			return false;
		}

		$arrstrSize = explode( ':', $arrstrImageInfo[0] );
		$arrstrResolution = explode( ':', $arrstrImageInfo[1] );

		$intImageWidth = intval( preg_replace( '/[^0-9]/', '', $arrstrSize[0] ) );
		$intImageHeight = intval( preg_replace( '/[^0-9]/', '', $arrstrSize[1] ) );

		$intImageXResolution = intval( preg_replace( '/[^0-9]/', '', $arrstrResolution[0] ) );
		$intImageYResolution = intval( preg_replace( '/[^0-9]/', '', $arrstrResolution[1] ) );

		// using magic methods - adds to details column 'image_width: 123'
		$this->setImageWidth( $intImageWidth );
		$this->setImageHeight( $intImageHeight );
		$this->setImageXResolution( $intImageXResolution );
		$this->setImageYResolution( $intImageYResolution );

		if( true == $this->isFile( $this->m_strFullDecryptedFileName ) && true == $this->isFile( $this->m_strFullFileName ) ) {
			$this->removeTempFile( $this->m_strFullDecryptedFileName ); // need to remove file that was created
		}

		return true;
	}

	/**
	 * @param $strCommand
	 * @param $strErrorMsgToDisplay
	 * @return mixed
	 */
	protected function exec( $strCommand, $strErrorMsgToDisplay ) {
		exec( $strCommand, $strTerminalOutput, $intReturnValue );
		$strTerminalOutput = json_encode( $strTerminalOutput );
		if( 0 !== $intReturnValue ) {
			$this->handlePaymentErrorMessages( $strErrorMsgToDisplay . ' :: ' . $strTerminalOutput );
		}

		return $strTerminalOutput;
	}

	public function setCheckFrontGifImageChild( $strType, $strContent ) {
		$arrstrAllowedTypes = array( 'front', 'frontMasked', 'frontThumb', 'frontThumbMasked' );

		if( true == in_array( $strType, $arrstrAllowedTypes, true ) ) {
			$this->m_arrstrCheckFrontGifImageChildren[$strType] = $strContent;
		}
	}

	public function setCheckReverseGifImageChild( $strType, $strContent ) {
		$strAllowedTypes = array( 'reverse', 'reverseEndorsed', 'reverseThumb', 'reverseThumbEndorsed' );
		if( false != in_array( $strType, $strAllowedTypes ) ) {
			$this->m_arrstrCheckReverseGifImageChildren[$strType] = $strContent;
		}
	}

	/**
	 * Get Functions
	 */

	public function getCacheFileName() {
		return $this->m_strCacheFileName;
	}

	public function getProcessingBankAccount() {
		return $this->m_objProcessingBankAccount;
	}

	public function getProcessingBankAccountId() {
		return $this->m_intProcessingBankAccountId;
	}

	public function getProcessingBankId() {
		return $this->m_intProcessingBankId;
	}

	public function getCompanyMerchantAccount() {
		return $this->m_objCompanyMerchantAccount;
	}

	public function getClient() {
		return $this->m_objClient;
	}

	public function getArPayment() {
		return $this->m_objArPayment;
	}

	public function getImageContent() {
		return $this->m_strImageContent;
	}

	public function getImageFullPath() {
		return $this->getImagePath() . $this->getImageName();
	}

	public function getCheckFrontGifImageChild( $strType ) {
		if( false == isset( $this->m_arrstrCheckFrontGifImageChildren[$strType] ) ) {
			return NULL;
		}
		return $this->m_arrstrCheckFrontGifImageChildren[$strType];
	}

	public function getCheckReverseGifImageChild( $strType ) {
		if( false == isset( $this->m_arrstrCheckReverseGifImageChildren[$strType] ) ) {
			return NULL;
		}
		return $this->m_arrstrCheckReverseGifImageChildren[$strType];
	}

	/**
	 * @param $intUserId
	 * @return $this
	 */
	public function setUserId( $intUserId ) {
		if( true == valId( $intUserId ) ) {
			$this->m_intUserId = $intUserId;
		}
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getUserId() {
		return $this->m_intUserId;
	}

	/**
	 * Sql Functions
	 */

	public function sqlImageContent() {
		// This was orginally programmed to insert image content into database.  We decided not to for front and back images.
		if( $this->getPaymentImageTypeId() == CPaymentImageType::SIGNATURE ) {
			return "'" . pg_escape_bytea( $this->m_strImageContent ) . "'::bytea";
		}
		return "''";
	}

	/**
	 * Validation Functions
	 */

	public function valFileExtensionId() {

		$boolIsValid = true;

		if( true == is_null( $this->getFileExtensionId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_extension_id', 'File extension id is required.' ) );
			return false;
		}

		if( CFileExtension::IMAGE_TIF != $this->m_intFileExtensionId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_extension_id', 'File extension id must be set to .tif only.' ) );
		}

		return $boolIsValid;
	}

	public function valPaymentImageTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPaymentImageTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_image_type_id', 'Payment image type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valImageContent() {
		$boolIsValid = true;

		if( true == is_null( $this->getImageContent() ) || 0 >= mb_strlen( $this->getImageContent() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'image_content', 'Image content is required.' ) );
			return false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valFileExtensionId();
				$boolIsValid &= $this->valPaymentImageTypeId();
				$boolIsValid &= $this->valImageContent();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valFileExtensionId();
				$boolIsValid &= $this->valPaymentImageTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * wrapper Functions
	 */

	/**
	 * @param $strFileSystemPath
	 * @return bool|null
	 */
	public function isDirectory( $strFileSystemPath ) {
		clearstatcache( true, $strFileSystemPath );
		return CFileIo::isDirectory( $strFileSystemPath );
	}

	/**
	 * @param $strFileSystemPathName
	 * @return bool|null
	 */
	public function isFile( $strFileSystemPathName ) {
		clearstatcache( true, $strFileSystemPathName );
		return CFileIo::isFile( $strFileSystemPathName );
	}

	/**
	 * @param $strFileNamePath
	 * @return bool
	 */
	public function fileExists( $strFileNamePath ) {
		clearstatcache( true, $strFileNamePath );
		return \Psi\Libraries\UtilFileIo\CFileIo::createService()->fileExists( $strFileNamePath );
	}

	/**
	 * @param $strFileSystemPathName
	 * @return bool|null
	 */
	public function getFileSize( $strFileSystemPathName ) {
		clearstatcache( true, $strFileSystemPathName );
		return CFileIo::getFileSize( $strFileSystemPathName );
	}

	/**
	 * @param $strFileSystemPath
	 * @return bool
	 */
	public function createRecursiveDir( $strFileSystemPath ) {
		clearstatcache( true, $strFileSystemPath );
		return @CFileIo::createRecursiveDir( $strFileSystemPath );
	}

	public function writeBinaryFile( $strFileName, $strImageData ) {
		return \Psi\Libraries\UtilFileIo\CFileIo::createService()->writeBinaryFile( $strFileName, $strImageData );
	}

	/**
	 * @param $strFileContent
	 * @param $strPathName
	 * @return bool
	 */
	public function encryptFile( $strFileContent, $strPathName ) {
		return \Psi\Libraries\Cryptography\CCrypto::createService()->encryptFile( $strFileContent, CONFIG_SODIUM_KEY_ENCRYPT_DECRYPT_FILE, $strPathName );
	}

	/**
	 * @param $strFullFileName
	 * @return bool
	 */
	public function decryptFile( $strFullFileName ) {
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decryptFile( $strFullFileName, CONFIG_SODIUM_KEY_ENCRYPT_DECRYPT_FILE, [ 'legacy_secret_key' => CONFIG_KEY_ENCRYPT_DECRYPT_FILE ] );
	}

	/**
	 * @param $strFullFileName
	 * @param $strFullDecryptedFileName
	 * @return bool
	 */
	public function writeDecryptedFile( $strFullFileName, $strFullDecryptedFileName ) {
		if( false == $this->isFile( $strFullFileName ) ) {
			return false;
		}
		$strContent = \Psi\Libraries\Cryptography\CCrypto::createService()->decryptFile( $strFullFileName, CONFIG_SODIUM_KEY_ENCRYPT_DECRYPT_FILE, [ 'legacy_secret_key' => CONFIG_KEY_ENCRYPT_DECRYPT_FILE ] );
		if( \Psi\Libraries\UtilFunctions\valStr( $strContent ) ) {
			return $this->writeBinaryFile( $strFullDecryptedFileName, $strContent );
		}
		return false;
	}

	/**
	 * @param $strImagePathName
	 * @return bool|string
	 */
	public function readDecryptedImage( $strImagePathName ) {
		if( 0 >= getimagesize( $strImagePathName ) ) {
			return NULL;
		}

		clearstatcache( $strImagePathName );
		$strDecryptedContent = CFileIo::fileGetContents( $strImagePathName );
		return $strDecryptedContent;
	}

	/**
	 * @param $strFilePath
	 * @return bool|null|string
	 */
	public function readFile( $strFilePath ) {
		clearstatcache( true, $strFilePath );
		return CFileIo::readFile( $strFilePath );
	}

	public function getEncryptedPathParts( string $strFilePath ) : array {

		// Parse out unencrypted path prefix plus (optional) encryption related suffixes
		//  NOTE: +? inverts + quantifier to be non-greedy
		//  NOTE: ?: indicates a non-capturing group
		$strPattern = '#(.+?)(?:\.([a-z]?enc))?(?:\.([0-9]+))?$#i';
		preg_match( $strPattern, $strFilePath, $arrstrMatches );

		return [
			'unencrypted_path' => $arrstrMatches[1] ?? NULL,
			'encryption_extension' => $arrstrMatches[2] ?? NULL,
			'original_length' => $arrstrMatches[3] ?? NULL,
		];
	}

	/**
	 * Fetch Functions
	 */

	public function fetchAndSetProcessingBankId( $objPaymentDatabase ) {

		$strSql = 'SELECT
        				pba.processing_bank_id
        			FROM
        				ar_payments ap,
        				company_merchant_accounts cma,
        				merchant_account_methods mam,
        				processing_bank_accounts pba
        			WHERE
        				ap.company_merchant_account_id = cma.id
        				AND mam.company_merchant_account_id = cma.id
        				AND mam.payment_type_id = ap.payment_type_id
        				AND mam.payment_medium_id = ap.payment_medium_id
        				AND mam.processing_bank_account_id = pba.id
        				AND ap.id = ' . ( int ) $this->getArPaymentId() . ';';

		$arrstrData = fetchData( $strSql, $objPaymentDatabase );

		if( true == valArr( $arrstrData ) && true == is_numeric( $arrstrData[0]['processing_bank_id'] ) ) {
			$this->setProcessingBankId( $arrstrData[0]['processing_bank_id'] );
		}

		return $this->m_intProcessingBankId;
	}

	/**
	 * Other Functions
	 */

	public function displayMsgImageFailedToLoad() {

		$resImage = @imagecreate( 650, 286 );
		if( NULL == $resImage ) {
			$this->handlePaymentErrorMessages( 'displayMsgImageFailedToLoad:: Cannot Initialize new GD image stream' );
		}

		imagecolorallocate( $resImage, 255, 255, 255 );
		$strTextColor = imagecolorallocate( $resImage, 0, 0, 0 );
		imagestring( $resImage, 100, 100, 100,  'Failed to load Image.', $strTextColor );
		imagegif( $resImage );
		imagedestroy( $resImage );
	}

	public function displayMsgImageImportFailed() {

		$resImage = @imagecreate( 650, 286 );
		if( NULL == $resImage ) {
			$this->handlePaymentErrorMessages( 'displayMsgImageImportFailed:: Cannot Initialize new GD image stream' );
		}

		imagecolorallocate( $resImage, 255, 255, 255 );
		$strTextColor = imagecolorallocate( $resImage, 0, 0, 0 );
		imagestring( $resImage, 100, 0, 100,  'We\'re sorry, but there seemed to be a delay when trying to', $strTextColor );
		imagestring( $resImage, 100, 0, 120,  'retrieve your image from our archive. Please Wait.', $strTextColor );
		imagestring( $resImage, 100, 0, 140,  'If this message continues, contact support', $strTextColor );
		imagestring( $resImage, 100, 0, 160,  'with a payment ID to correct this issue .', $strTextColor );

		imagegif( $resImage );
		imagedestroy( $resImage );
	}

	public function displayMsgImageConversionProcessing() {

		$resImage = @imagecreate( 650, 286 );
		if( NULL == $resImage ) {
			$this->handlePaymentErrorMessages( 'displayMsgImageConversionProcessing:: Cannot Initialize new GD image stream' );
		}

		imagecolorallocate( $resImage, 255, 255, 255 );
		$strTextColor = imagecolorallocate( $resImage, 0, 0, 0 );
		imagestring( $resImage, 100, 0, 100,  'We\'re sorry, but we are still processing your', $strTextColor );
		imagestring( $resImage, 100, 0, 120,  'check image. Please try again soon.', $strTextColor );
		imagestring( $resImage, 100, 0, 140,  'If this continues, contact support', $strTextColor );
		imagestring( $resImage, 100, 0, 160,  'with a payment id to correct this issue .', $strTextColor );

		imagegif( $resImage );
		imagedestroy( $resImage );
	}

	/**
	 * Currently, no code calls this method
	 * original endorsement command code is moved to CCheck21ImageEndorsementScript and  payment system.
	 * Perform endorsement by sending rabbit msg to payments system
	 * @return bool
	 */
	public function applyCheck21Endorsement() {

		// this method will eventually call sendMessageToPaymentsConvertImage (which endorses),
		// But will make sure check image exists on file system and is in payment Object store before endorsing
		return $this->writeImageContentToFileSystem();
	}

	/**
	 * "fileSystem" here now meaning payment system Object Store, and "ImageContent" meaning original tiff
	 * and ultimately it is just setting the image content member variable via readImageContentFromFileSystem if it doesn't already exist
	 * @param null $strFileSystemPath
	 * @return bool
	 */
	public function writeImageContentToFileSystem( $strFileSystemPath = NULL ) {

		if( true == is_null( $strFileSystemPath ) ) {
			$strFileSystemPath = $this->getImagePath();
		}

		// todo remove this block when mounts is gone/deprecated (unless using a tmp dir)
		if( false == $this->isDirectory( $strFileSystemPath ) ) {
			if( false == $this->createRecursiveDir( $strFileSystemPath ) ) {
				$strMessage = 'Image directory failed to create' . $strFileSystemPath;
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strMessage ) );
				$this->handlePaymentErrorMessages( $strMessage );
				return false;
			}
		}

		$this->handleWriteToObjectStore( $this->m_strFullDecryptedFileName, $this->m_strImageContent, true );

		if( true == empty( $this->m_strImageContent ) ) {
			// if image content doesn't exist, call to make sure it does
			$boolResult = $this->readImageContentFromFileSystem();
			if( false == $boolResult ) {
				return false;
			}
		}

		if( false == $this->encryptFile( $this->m_strImageContent, $strFileSystemPath . $this->getImageName() ) ) {
			$strMessage = 'Image file failed to be encrypted ' . $strFileSystemPath . $this->getImageName();
			$this->handlePaymentErrorMessages( $strMessage );
			return false;
		}

		$this->handleRemovingDecryptedFile( $this->m_strFullDecryptedFileName );

		return true;
	}

	/**
	 * Puts passed content into Payment system Object Store
	 * @param $strKey
	 * @param $strContentSource
	 * @param $boolSourceIsData
	 * @param $boolAsync
	 * @param $boolOverWriteFlag
	 * @return bool
	 */
	public function handleWriteToObjectStore( $strKey, $strContentSource, $boolSourceIsData ) {
		if( false == $this->validateTifSource( $strContentSource, $boolSourceIsData ) ) {
			$strMessage = 'Check Image file is corrupted or missing : ';
			$this->handlePaymentErrorMessages( $strMessage . 'Tried interfacing with Object Store Bucket: ' . CONFIG_OSG_BUCKET_PAYMENTS );
			return false;
		}

		// If source data is a file path instead of content, then we need to read from the path provided
		// We then need to set the image content
		if( !$boolSourceIsData ) {
			$this->setImageContent( $this->readFile( $strContentSource ) );
		} else {
			// Check if the file exists locally
			if( !$this->fileExists( $this->getImageFullPath() ) ) {
				$strMessage = 'ArPaymentImage: ' . $this->getId() . ' @ ' . $this->getImageFullPath() . ' does not exists locally and cannot be read. ';
				$this->handlePaymentErrorMessages( $strMessage );
				return false;
			}

			// If the image content has not been set in memory, the read the file content
			// then set it on this class.
			if( !valStr( $this->getImageContent() ) || empty( $this->getImageContent() ) ) {
				$this->setImageContent( $this->decryptFile( $this->getImageFullPath() ) );
			}
		}

		try {
			$this->putImageContent(); // Put image content into payment systems object store
			$this->m_boolTiffInPaymentObjectStore = true;
		} catch ( \Exception $objException ) {
			// Image content is ready to return. So catch will NOT add client facing error message and will instead send email about failed put
			$this->handlePaymentErrorMessages( $objException->getMessage(), true );
		}

		return true;
	}

	public function validateTifSource( $strContentSource, $boolSourceIsData ) {

		if( true == $boolSourceIsData && true == empty( $strContentSource ) ) {
			return false;
		}
		if( false == $boolSourceIsData ) {
			if( false == $this->isFile( $strContentSource ) ) {
				return false;
			}
			if( true == empty( $this->readDecryptedImage( $strContentSource ) ) ) {
				return false;
			}
			$intImageType = exif_imagetype( $strContentSource );
			if( IMAGETYPE_TIFF_II !== $intImageType && IMAGETYPE_TIFF_MM !== $intImageType ) {
				return false;
			}
		}
		return true;

	}

	/**
	 * if you think the file exists, and only need image content as quickly as possible, use this method
	 * i.e. CArPaymentsX937FileController->generateX937FileComponents() will call this for bank file batching
	 * function sets image content member variable.
	 * @return bool // call $this->getImageContent() for image content
	 */
	public function readSetImageContentFromMounts() {

		if( false == $this->isFile( $this->m_strFullFileName ) ) {
			$strMessage = 'Image content could not be read - source File does not Exist: ' . $this->m_strFullFileName;
			$this->handlePaymentErrorMessages( $strMessage );
			return false;
		}

		$this->setImageContent( $this->decryptFile( $this->m_strFullFileName ) );  // call $this->getImageContent() after this method

		return true;

	}

	/**
	 * image content meaning original tiff, file system meaning object store (client/system)
	 * function sets image content member variable.
	 * @return bool
	 */
	public function readImageContentFromFileSystem() {

		if( false == $this->isFile( $this->m_strFullFileName ) ) {
			$strMessage = 'Image content could not be read - source File does not Exist: ' . $this->m_strFullFileName;
			$this->handlePaymentErrorMessages( $strMessage );
			return false;
		}

		$boolResult = $this->decryptFilePutImagePaymentObjectStore();
		return $boolResult;
	}

	public function decryptFilePutImagePaymentObjectStore() {

		$strContent = $this->decryptFile( $this->m_strFullFileName );
		if( true == empty( $strContent ) ) {

			$objSixMonthsDateTime = new DateTime( '-6 months' );
			if( $objSixMonthsDateTime > $this->getPaymentDatetime() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Image could not be decrypted - Past 6 month creation mark: ' . $this->m_strFullFileName ) );
			}

			return false;
		}

		$this->setImageContent( $strContent );
		$strKey = $this->getObjectStoreFilePath() . $this->getObjectStoreFileName();
		$this->handleWriteToObjectStore( $strKey, $strContent, true ); // for next time
		return true;
	}

	public function unescapeImageContent() {
		$this->m_strImageContent = pg_unescape_bytea( $this->m_strImageContent );
	}

	/**
	 * if cached file key was not in client object store. check payment object store, if not use source tif.enc, decrypt it, and put it into system objectstore.
	 * makes sure a tiff copy of check image exists within system object store.
	 * @return bool
	 */
	public function verifyTiffExists() {
		// If unencrypted file exists in mounts, write it to payment system bucket
		if( true == $this->isFile( $this->m_strFullDecryptedFileName ) ) {
			$this->handleWriteToObjectStore( $this->m_strFullDecryptedFileName, $this->m_strFullDecryptedFileName, false ); // put into payment system objectstore without overwrite. Dupes will be ignored.

			// If encrypted version is missing, and decrypted Tiff version exists... we need that encrypted file for batch scripts
			if( false == $this->isFile( $this->m_strFullFileName ) ) {
				$this->encryptAndWriteToFileSystem();
			}
			return true;
		}

		// decrypt to add using handlePuttingImageIntoPaymentObjectStore()
		if( false == $this->writeDecryptedFile( $this->m_strFullFileName, $this->m_strFullDecryptedFileName ) ) {
			$this->deleteCheckImageTiffFromClientObjectStore( $this->m_strFullDecryptedFileName ); // on failure delete.
			return false;
		}

		// decrypted file now exists. copy it into object store now
		$this->handleWriteToObjectStore( $this->m_strFullDecryptedFileName, $this->m_strFullDecryptedFileName, false );

		return true;
	}

	/**
	 * also known as the image tiff
	 * @param $strFullDecryptedFileName
	 * @return bool
	 */
	public function handleRemovingDecryptedFile( $strFullDecryptedFileName ) {

		if( false === $this->fileExists( $strFullDecryptedFileName ) || 0 >= $this->getFileSize( $strFullDecryptedFileName ) ) {
			return true;
		}

		// at this point - assume file exists locally and needs to be removed
		if( false == @chmod( $strFullDecryptedFileName, 0777 ) ) {
			$strMessage = 'Could not remove Modify local file for deletion: ' . $strFullDecryptedFileName;
			$this->handlePaymentErrorMessages( $strMessage );
			return false;
		}

		if( false == $this->removeTempFile( $strFullDecryptedFileName ) ) {
			$strMessage = 'Could not remove Local file - Permission denied: ' . $strFullDecryptedFileName;
			$this->handlePaymentErrorMessages( $strMessage );
			return false;
		}

		return true;
	}

	public function encryptAndWriteToFileSystem() {

		// the .enc file is currently important for Batch file scripts. Once these are adjusted, this is no longer needed
		// future todo change batch file scripts to use object store, and remove this whole function or modify it to link to object store
		// future todo when needed, if tif.enc is not on file system. reach out to obj store and retrieve it.

		if( true == $this->isFile( $this->m_strFullFileName ) && 0 < $this->getFileSize( $this->m_strFullFileName ) ) {
			return true;
		}

		if( false == $this->isFile( $this->m_strFullDecryptedFileName ) || 0 >= $this->getFileSize( $this->m_strFullDecryptedFileName ) ) {
			$strDecryptedContent = $this->handleGetDecryptedContentObjectStore();
		} else {
			// Read unencrypted content
			$strDecryptedContent = $this->readDecryptedImage( $this->m_strFullDecryptedFileName );
		}

		if( true == empty( $strDecryptedContent ) ) {
			return false;
		}

		if( false == $this->encryptFile( $strDecryptedContent, $this->m_strFullFileName ) ) {
			$this->deleteCheckImageTiffFromClientObjectStore( $this->m_strFullDecryptedFileName ); // even on failure remove from client object store
			return false;
		}

		// Only delete front images (containing sensitive data)
		if( CPaymentImageType::FRONT == $this->getPaymentImageTypeId() ) {
			$this->removeTempFile( $this->m_strFullDecryptedFileName );
		}
		$this->deleteCheckImageTiffFromClientObjectStore( $this->m_strFullDecryptedFileName );
		return true;
	}

	/**
	 * This function has been remade, a more accurate name would be getGifImage( $boolBlockOut, $intWidth ) [method below]
	 * if your convertImageToGif is wrapped in a foreach, consider using getGifImage()
	 * this is a call to client obj store. and if that doesn't exist, a send message to create gifs.
	 * $boolBlockout = "masked" attribute
	 * Front check images need to be masked, backs needs to be endorsed.
	 * leaving $boolBlockout in params due to heavy use, but getPaymentImageTypeId will be better suited to determine if image should be masked or not.
	 * @param bool $boolBlockOut
	 * @param null $intWidth
	 * @return bool|string
	 * @throws Exception
	 */
	public function convertImageToGif( $boolBlockOut = false, $intWidth = NULL ) {

		if( CPaymentImageType::REVERSE == $this->getPaymentImageTypeId() ) {
			$boolBlockOut = false;
		}

		// Determine the GIF image name dynamically that can be served from cache.
		$this->determineCacheFileName( $boolBlockOut, $intWidth );

		// Get the contents if the file is in client object store.
		$strImageContent = $this->handleGetCachedCheckImageContent( $this->m_strCacheFileName );
		if( false == empty( $strImageContent ) ) {
			// Remove decrypted file so that it does not exist on mounts
			$this->handleRemovingDecryptedFile( $this->m_strFullDecryptedFileName );

			return $strImageContent;
		}

		// if cached file key was not in client object store. check payment object store, if not use source tif.enc, decrypt it, and put it into system objectstore.
		if( false == $this->verifyTiffExists() ) {
			$strErrorMessage = 'Source Image does not exist locally or within Object Store and should not be archived: ' . $this->m_strFullFileName;
			$this->handlePaymentErrorMessages( $strErrorMessage );

			return false; // lets controller handle getting check images from archive
		}

		// Rebuild cache and write converted images locally
		$this->rebuildCache( $boolBlockOut, $intWidth );

		// Remove decrypted file so that it does not exist on mounts
		$this->handleRemovingDecryptedFile( $this->m_strFullDecryptedFileName );

		$strImageContent = NULL;
		$strImageContent = $this->handleGetCachedCheckImageContent( $this->m_strCacheFileName );

		if( true == $this->isFile( $this->m_strCacheFileName ) && 0 < $this->getFileSize( $this->m_strCacheFileName ) ) {

			if( false == $boolBlockOut && CPaymentImageType::FRONT == $this->getPaymentImageTypeId() ) {
				$this->removeTempFile( $this->m_strCacheFileName );
			}
		}

		if( true == empty( $strImageContent ) ) {
			ob_start();
			$this->displayMsgImageFailedToLoad();
			return ob_get_clean();
		}

		return $strImageContent;
	}

	/**
	 * This is meant for when request GIFs in massive amounts. wrapped in a foreach
	 * if Gif doesn't exist, it won't bog down the process by searching and making them
	 * $boolBlockout = "masked" attribute
	 * Front check images need to be masked, backs needs to be endorsed.
	 * @param bool $boolBlockOut
	 * @param null $intWidth
	 * @return bool|string
	 */
	public function getGifImage( $boolBlockOut = false, $intWidth = NULL ) {

		if( CPaymentImageType::REVERSE == $this->getPaymentImageTypeId() ) {
			$boolBlockOut = false;
		}

		// Determine the GIF image name dynamically that can be served from cache.
		$this->determineCacheFileName( $boolBlockOut, $intWidth );

		// Check if file exists, if so then return content
		$strImageContent = $this->handleGetCachedCheckImageContent( $this->m_strCacheFileName );
		if( true == valStr( $strImageContent, self::VALID_IMAGE_LENGTH ) ) {
			return $strImageContent;
		}

		// Verify tif exsits
		if( !$this->verifyTiffExists() ) {
			$strErrorMessage = 'Source Image does not exist locally or within Object Store and should not be archived: ' . $this->m_strFullFileName;
			$this->handlePaymentErrorMessages( $strErrorMessage );

			return false;
		}

		// Perform cache image conversions
		$this->rebuildCache( $boolBlockOut, $intWidth );

		// Remove decrypted file so that it does not exist on mounts
		$this->handleRemovingDecryptedFile( $this->m_strFullDecryptedFileName );

		$strImageContent = $this->handleGetCachedCheckImageContent( $this->m_strCacheFileName );

		if( false == empty( $strImageContent ) ) {
			return $strImageContent;
		}

		return false;
	}

	public function determineCacheImagePath( $boolCreateIfNotExists = false ) {

		if( 0 >= mb_strlen( $this->getImagePath() ) ) {
			$strMessage = 'Image Path is empty. This is required for deriving cache path.';
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Image Path is empty. This is required for deriving cache path.' ) );
			$this->handlePaymentErrorMessages( $strMessage . $this->m_strCacheImagePath );
			return false;
		}

		$this->m_strCacheImagePath = str_replace( self::CHECK_IMAGES_PATH, 'check_21_gif_cache/', $this->getImagePath() );
		$this->m_strCacheImagePath = str_replace( PATH_PAYMENT_MOUNTS, PATH_NON_BACKUP_MOUNTS, $this->m_strCacheImagePath );

		// Try to create a directory if not exists.
		if( true == $boolCreateIfNotExists && false == $this->isDirectory( $this->m_strCacheImagePath ) && false == $this->createRecursiveDir( $this->m_strCacheImagePath ) ) {
			$strMessage = 'Image directory failed to create ' . $this->m_strCacheImagePath;
			$this->handlePaymentErrorMessages( $strMessage );
			return false;
		}

		return true;
	}

	public function determineCacheFileName( $boolBlockOut = false, $intWidth = NULL ) {
		$this->m_strCacheFileName = NULL;

		// Calculate the cache path and create directory if it doesn't already exist
		if( false == $this->determineCacheImagePath( true ) ) {
			$strMessage = 'Cache image path could not be created.';
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strMessage ) );
			$this->handlePaymentErrorMessages( $strMessage );
			return false;
		}

		if( true == is_null( $intWidth ) ) {
			if( true == $boolBlockOut ) {
				$this->m_strCacheFileName = $this->m_strCacheImagePath . basename( $this->m_strDecryptedImageName, '.tif' ) . self::CACHING_IMAGE_TYPE_FULL_MASKED_GIF;
			} else {
				$this->m_strCacheFileName = $this->m_strCacheImagePath . basename( $this->m_strDecryptedImageName, '.tif' ) . self::CACHING_IMAGE_TYPE_FULL_GIF;
			}
		} else {
			if( true == $boolBlockOut ) {
				$this->m_strCacheFileName = $this->m_strCacheImagePath . basename( $this->m_strDecryptedImageName, '.tif' ) . '.' . $intWidth . self::CACHING_IMAGE_TYPE_THUMBNAIL_MASKED_GIF;
			} else {
				$this->m_strCacheFileName = $this->m_strCacheImagePath . basename( $this->m_strDecryptedImageName, '.tif' ) . '.' . $intWidth . self::CACHING_IMAGE_TYPE_THUMBNAIL_GIF;
			}
		}
	}

	/**
	 * This function now will send a message out to payments to run fresh conversions.
	 * @return bool
	 */
	public function rebuildCacheAll( $arrintWidth = array( self::DEFAULT_THUMB_SIZE ) ) {
		// this is needed to get existing check image into payment objStore if it isn't there already.
		if( false == $this->verifyTiffExists() ) {
			$strMessage = 'Source Image does not exist locally or within Object Store: ' . $this->m_strFullFileName;
			$this->handlePaymentErrorMessages( $strMessage );
			return false;
		}

		$boolMsgResult = $this->sendMessageToPaymentsConvertImage( $arrintWidth, false, true );
		return $boolMsgResult;

	}

	public function writePreRenderedFrontCacheFiles( $strGifData, $strThumbGifData, $strMaskedGifData, $strThumbMaskedGifData ) {

		$boolSuccess = true;

		// Full Masked GIF
		if( 0 < mb_strlen( $strMaskedGifData ) ) {
			$this->determineCacheFileName( true, NULL );
			if( false != is_null( $this->m_strCacheFileName ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Full masked GIF front cache image can not be created.' ) );
				return false;
			}

			$boolSuccess &= $this->writeBinaryFile( $this->m_strCacheFileName, $strMaskedGifData );
		}

		// Thumbnail Masked GIF
		if( 0 < mb_strlen( $strThumbMaskedGifData ) ) {
			$this->determineCacheFileName( true, 400 );
			if( false != is_null( $this->m_strCacheFileName ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Thumbnail masked GIF front cache image can not be created.' ) );
				return false;
			}

			$boolSuccess &= $this->writeBinaryFile( $this->m_strCacheFileName, $strThumbMaskedGifData );
		}

		// Full GIF
		if( 0 < mb_strlen( $strGifData ) ) {
			$this->determineCacheFileName( false, NULL );
			if( false != is_null( $this->m_strCacheFileName ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Full GIF front cache image can not be created.' ) );
				return false;
			}

			$boolSuccess &= $this->writeBinaryFile( $this->m_strCacheFileName, $strGifData );
		}

		// Thumbnail GIF
		if( 0 < mb_strlen( $strThumbGifData ) ) {
			$this->determineCacheFileName( false, 400 );
			if( false != is_null( $this->m_strCacheFileName ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Thumbnail GIF front cache image can not be created.' ) );
				return false;
			}

			$boolSuccess &= $this->writeBinaryFile( $this->m_strCacheFileName, $strThumbGifData );
		}

		return $boolSuccess;
	}

	public function writePreRenderedReverseCacheFiles( $strGifData, $strThumbGifData, $strEndorsedGifData, $strThumbEndorsedGifData ) {

		$boolSuccess = true;

		// Full Endorsed GIF
		if( 0 < mb_strlen( $strEndorsedGifData ) ) {
			$this->determineCacheFileName( true, NULL );
			if( false != is_null( $this->m_strCacheFileName ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Full endorsed GIF reverse cache image can not be created.' ) );
				return false;
			}

			$boolSuccess &= $this->writeBinaryFile( $this->m_strCacheFileName, $strEndorsedGifData );
		}

		// Thumbnail Endorsed GIF
		if( 0 < mb_strlen( $strThumbEndorsedGifData ) ) {
			$this->determineCacheFileName( true, 400 );
			if( false != is_null( $this->m_strCacheFileName ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Thumbnail endorsed GIF reverse cache image can not be created.' ) );
				return false;
			}

			$boolSuccess &= $this->writeBinaryFile( $this->m_strCacheFileName, $strThumbEndorsedGifData );
		}

		// Full GIF
		if( 0 < mb_strlen( $strGifData ) ) {
			$this->determineCacheFileName( false, NULL );
			if( false != is_null( $this->m_strCacheFileName ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Full GIF reverse cache image can not be created.' ) );
				return false;
			}

			$boolSuccess &= $this->writeBinaryFile( $this->m_strCacheFileName, $strGifData );
		}

		// Thumbnail GIF
		if( 0 < mb_strlen( $strThumbGifData ) ) {
			$this->determineCacheFileName( false, 400 );
			if( false != is_null( $this->m_strCacheFileName ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Thumbnail GIF reverse cache image can not be created.' ) );
				return false;
			}

			$boolSuccess &= $this->writeBinaryFile( $this->m_strCacheFileName, $strThumbGifData );
		}

		return $boolSuccess;
	}

	public function writePreRenderedTif( $strTifData, $boolIsMasked ) {

		if( false == $this->isDirectory( $this->getImagePath() ) ) {
			if( false == $this->createRecursiveDir( $this->getImagePath() ) ) {
				trigger_error( 'Image directory failed to create : ' . $this->getImagePath(), E_USER_ERROR );
				return false;
			}
		}

		$boolSuccess = true;

		// Write TIF Data
		if( 0 < mb_strlen( $strTifData ) ) {

			// If writing a pre-rendered masked (front) tif, alter extension to .masked.tif
			if( true == $boolIsMasked ) {
				$strFileName = $this->getImagePath() . basename( $this->m_strDecryptedImageName, '.tif' ) . '.masked.tif';
				$boolSuccess &= $this->writeBinaryFile( $strFileName, $strTifData );
			} else {
				$boolSuccess &= $this->writeBinaryFile( $this->m_strFullDecryptedFileName, $strTifData );

				if( false == $this->encryptFile( $strTifData, $this->m_strFullFileName ) ) {
					$strMessage = 'could not encrypt File : ' . $this->m_strFullFileName;
					$this->handlePaymentErrorMessages( $strMessage );
					return false;
				}
			}
		}

		if( false == $boolSuccess ) {
			$strMessage = 'could not write pre rendered tif to file systems : ' . $this->m_strFullFileName;
			$this->handlePaymentErrorMessages( $strMessage );
			return false;
		}

		return $boolSuccess;
	}

	/**
	 * reaches out to client payment object store to get image. if it's not there, checks on system payment object store. then return false
	 * @return string
	 */
	public function handleGetDecryptedContentObjectStore() {
		$strCheckImageContent = NULL;

		if( true == $this->isFile( $this->m_strFullDecryptedFileName ) ) {
			$strCheckImageContent = $this->readDecryptedImage( $this->m_strFullDecryptedFileName );
		}

		if( false == empty( $strCheckImageContent ) ) {
			return $strCheckImageContent;
		}

		try {

			$strKeyFilePathName = $this->getObjectStoreFilePath() . $this->getObjectStoreFileName();

			$strCheckImageContent = $this->handleGetImageContentFromClientObjectStore( $strKeyFilePathName ); // gets image from CLIENT payment cohisity
		} catch ( \Exception $objException ) {
			// catch will NOT add client facing error message and will instead send email about failed transfer
			$this->handlePaymentErrorMessages( $objException->getMessage(), true );
		}

		return $strCheckImageContent;

	}

	/**
	 * checks locally.  then within objectstore,
	 * @param $strKeyFilePathName
	 * @return bool|string
	 */
	protected function handleGetCachedCheckImageContent( $strKeyFilePathName ) {

		if( true == $this->isFile( $strKeyFilePathName ) ) {
			return $this->readDecryptedImage( $strKeyFilePathName );
		}

		return false;
	}

	/**
	 * checks to see if cohesity object storage has image. yes? get it
	 * @param $strKeyFilePathName
	 * @return bool|string
	 */
	protected function handleGetImageContentFromClientObjectStore( $strKeyFilePathName ) {

		$boolCheckForPaymentFileResponse = $this->getPaymentFileContentFromClientObjectStore( $strKeyFilePathName, true );

		if( false == $boolCheckForPaymentFileResponse ) {
			return false;
		}

		$strCheckImageContent = $this->getPaymentFileContentFromClientObjectStore( $strKeyFilePathName, false );

		if( true == empty( $strCheckImageContent ) ) {
			return false;
		}

		return $strCheckImageContent;
	}

	protected function getCryptoSodium() {
		return \Psi\Libraries\Cryptography\CCrypto::createService();
	}

	protected function getObjectStorageGatewayFactory() {
		return CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_COHESITY_S3 );
	}

	/**
	 * @param $strKeyFilePathName
	 * @param $boolCheckExists
	 * @return bool|object
	 */
	protected function getPaymentFileContentFromClientObjectStore( $strKeyFilePathName, $boolCheckExists ) {

		$strKeyFilePathName = $this->objectStoreCleanKey( $strKeyFilePathName );

		$arrmixObjectStore = [
			'cid'        		=> $this->getCid(),
			'objectId'   		=> $this->getId(),
			'container'         => CONFIG_OSG_BUCKET_PAYMENTS,
			'key'               => $strKeyFilePathName,
			'checkExists'		=> $boolCheckExists,
		];

		$objCheckForPaymentFileResponse = $this->getObjectStorageGatewayFactory()->getObject( $arrmixObjectStore );

		if( true == $boolCheckExists ) {
			return $objCheckForPaymentFileResponse['isExists'];
		}

		if( false == $boolCheckExists && true == $objCheckForPaymentFileResponse['hasErrors'] ) {
			$this->handlePaymentErrorMessages( 'Error when interfacing with Object Store Gateway. getObject Error(s): ' . json_encode( $objCheckForPaymentFileResponse['messages'] ), true );
			return false;
		}

		$strCheckImageContent = ( string ) $objCheckForPaymentFileResponse['data'];

		if( '#sodium.secretbox#' == substr( $strCheckImageContent, 0, mb_strlen( '#sodium.secretbox#' ) ) ) {
			try{
				$strCheckImageContent = $this->getCryptoSodium()->decrypt( $strCheckImageContent, CONFIG_SODIUM_KEY_PAYMENT_ENCRYPT_DECRYPT_FILE );
			} catch( \Exception $objException ) {
				return false;
			}
		}

		return $strCheckImageContent;
	}

	/**
	 * Adds (unencrypted) image into cohesity object storage using file path or Content as data
	 * Payment Client Object Store
	 * @param $strKey
	 * @param $strSourceInfo
	 * @param bool $boolSourceIsData
	 * @return bool
	 */
	protected function writeToClientObjectStore( $strKey, $strSourceInfo, $boolSourceIsData = false ) {

		if( ( true == $boolSourceIsData && 0 == mb_strlen( $strSourceInfo ) ) ) {
			$strMessage = 'Image source CONTENT does not exist, cannot be saved';
			$this->handlePaymentErrorMessages( $strMessage );
			trigger_error( $strMessage, E_USER_WARNING );
			return false;
		}

		if( false == $boolSourceIsData && false == $this->isFile( $strSourceInfo ) ) {
			$strMessage = 'Image source CONTENT does not exist, cannot be saved';
			$this->handlePaymentErrorMessages( $strMessage );
			trigger_error( $strMessage, E_USER_WARNING );
			return false;
		}

		$strKey = $this->objectStoreCleanKey( $strKey );

		$arrmixObjectParams = [
			'cid'        		=> $this->getCid(),
			'objectId'   		=> $this->getId(),
			'container'         => CONFIG_OSG_BUCKET_PAYMENTS,
			'uploadMultipart' 	=> false,
			'key'               => $strKey,
		];

		if( false === $boolSourceIsData ) {
			$strSourceInfo = $this->readFile( $strSourceInfo );
		}

		$arrmixObjectParams['data'] = $strSourceInfo; // source info is file content

		$objObjectStorageGatewayResponse = $this->getObjectStorageGatewayFactory()->putObject( $arrmixObjectParams );

		if( true == $objObjectStorageGatewayResponse->hasErrors() ) {
			$strErrorMessage = 'putObject : Image was not saved into Object Store: ' . CONFIG_OSG_BUCKET_PAYMENTS . ' Error(s): ' . json_encode( $objObjectStorageGatewayResponse['messages'] );
			$this->handlePaymentErrorMessages( $strErrorMessage, true );
			return false;
		}

		return true;

	}

	/**
	 * @param $strKeyFilePathName
	 * @return bool|object
	 */
	protected function deleteCheckImageTiffFromClientObjectStore( $strKeyFilePathName ) {

		$strKeyFilePathName = $this->objectStoreCleanKey( $strKeyFilePathName );

		$arrmixObjectStore = [
			'container'         => CONFIG_OSG_BUCKET_PAYMENTS,
			'key'               => $strKeyFilePathName,
		];

		$objCheckForPaymentFileResponse = $this->getObjectStorageGatewayFactory()->deleteObject( $arrmixObjectStore );

		if( true == $objCheckForPaymentFileResponse['hasErrors'] ) {
			$this->handlePaymentErrorMessages( 'Error when interfacing with Object Store Gateway. deleteObject Call message: ' . json_encode( $arrmixObjectStore ) . ' Error(s): ' . json_encode( $objCheckForPaymentFileResponse['messages'] ), true );
			return false;
		}

		return true;
	}

	/**
	 * @param $objMessage
	 * @return \Psi\Libraries\Queue\CSimpleMessageSender
	 */
	protected function getMessageSenderFactory( $objMessage ) {
		return \Psi\Libraries\Queue\Factory\CMessageSenderFactory::createService()->createSimpleSender( \Psi\Libraries\Queue\Factory\CAmqpMessageFactory::createService(), $objMessage::QUEUE_NAME, [], NULL, \Psi\Libraries\Queue\CAmqpConnection::PAYMENTS_VHOST );
	}

	/**
	 * This will send a rabbitMQ message to payments to record and move image into payment systems.
	 * Right now it will always be from CONFIG_OSG_BUCKET_PAYMENTS into CONFIG_OSG_BUCKET_SYSTEM_PAYMENTS
	 * NOTICE: when this call is made, a full size gif will always be made if it wasn't already.
	 * Do not need to add full image size int to create_thumbnail_sizes array.
	 * @param array $arrintWidths
	 * @param bool $boolEndorse
	 * @param bool $boolOverWriteCache
	 * @return bool
	 */
	public function sendMessageToPaymentsConvertImage( $arrintWidths = array(), $boolEndorse = false, $boolOverWriteCache = false ) {

		// setting for Front images
		$boolFrontImage = true;
		if( CPaymentImageType::REVERSE == $this->getPaymentImageTypeId() ) {
			$boolFrontImage = false; // we only need to block out acct info on front check images
		}

		$objPaymentFileConvertCheckImageMessage = \Psi\Libraries\PaymentQueue\Message\Payments\Operations\PaymentFiles\CConvertCheckImageMessage::create();
		$objPaymentFileConvertCheckImageMessage->setCid( $this->getCid() );
		$objPaymentFileConvertCheckImageMessage->setUserId( $this->getUserId() );
		$objPaymentFileConvertCheckImageMessage->setRemoteFileId( $this->getId() );
		$objPaymentFileConvertCheckImageMessage->setPaymentFileTypeId( self::X937_IMAGES_FILE_ID );
		$objPaymentFileConvertCheckImageMessage->setProcessingBankId( $this->m_intProcessingBankId );
		$objPaymentFileConvertCheckImageMessage->setProcessingBankAccountId( $this->m_intProcessingBankAccountId );
		$objPaymentFileConvertCheckImageMessage->setArPaymentId( $this->getArPaymentId() );
		$objPaymentFileConvertCheckImageMessage->setFilePath( $this->getObjectStoreFilePath() );
		$objPaymentFileConvertCheckImageMessage->setFileName( $this->getObjectStoreFileName() ); // key file path will be key to tiff
		$objPaymentFileConvertCheckImageMessage->setThumbnailSizes( $arrintWidths );
		$objPaymentFileConvertCheckImageMessage->setCheckImageFront( $boolFrontImage );
		$objPaymentFileConvertCheckImageMessage->setEndorseFlag( $boolEndorse );
		$objPaymentFileConvertCheckImageMessage->setEndPointContainer( CONFIG_OSG_BUCKET_PAYMENTS );
		$objPaymentFileConvertCheckImageMessage->setOverWriteCacheFlag( $boolOverWriteCache );

		try {
			$objMessageSender = $this->getMessageSenderFactory( $objPaymentFileConvertCheckImageMessage );
			$objMessageSender->sendRpc( $objPaymentFileConvertCheckImageMessage, [], self::MESSAGE_TIMEOUT );
			$arrstrResponses = json_decode( $objMessageSender->getResponse(), true );
		} catch( \Throwable $objException ) {
			$this->handlePaymentErrorMessages( 'AMQP Exception during check image convert: ' . $objException->getMessage() );
			return false;
		}

		$objMessageSender->close();
		$boolResult = ( false == empty( $arrstrResponses['operation_status'] ) ) ? CStrings::strToBool( $arrstrResponses['operation_status'] ) : false;

		if( false == empty( $arrstrResponses['error_messages'] ) ) {
			$this->handlePaymentErrorMessages( json_encode( $arrstrResponses['error_messages'] ) );
		}

		if( false == empty( $arrstrResponses['image_Dimensions'] ) ) {
			// magic methods
			$this->setImageWidth( $arrstrResponses['image_Dimensions']['image_width'] );
			$this->setImageHeight( $arrstrResponses['image_Dimensions']['image_height'] );
			$this->setImageXResolution( $arrstrResponses['image_Dimensions']['x_resolution'] );
			$this->setImageYResolution( $arrstrResponses['image_Dimensions']['y_resolution'] );
		}

		return $boolResult;

	}

	/**
	 * saves check image to temp location for use by special programs.
	 * 1) This SHOULD NEVER be called on an unmasked front image.
	 * 2) Remove tmp file when done using removeTempFile
	 * @param $strImagePathName
	 * @param $strImageContent
	 * @return bool|string
	 */
	public function saveTempFile( $strImagePathName, $strImageContent = NULL ) {

		if( true == is_null( $strImageContent ) ) {
			$strImageContent = $this->handleGetCachedCheckImageContent( $strImagePathName );
		}

		if( true == empty( $strImageContent ) ) {
			$strMessage = 'Could not read Check Image from storage: ' . $strImagePathName;
			$this->handlePaymentErrorMessages( $strMessage );
			return false;
		}

		$strCleanPathName = $this->objectStoreCleanKey( $strImagePathName );

		$strPathOnly = pathinfo( $strCleanPathName, PATHINFO_DIRNAME );

		if( false == $this->createRecursiveDir( CONFIG_CACHE_DIRECTORY . $strPathOnly ) ) {
			return false;
		}

		if( false == $this->writeBinaryFile( CONFIG_CACHE_DIRECTORY . $strCleanPathName, $strImageContent ) ) {
			return false;
		}

		return CONFIG_CACHE_DIRECTORY . $strCleanPathName;
	}

	/**
	 * @param $strImagePath
	 * @return bool
	 */
	public function removeTempFile( $strImagePath ) {

		if( true == file_exists( $strImagePath ) && true == is_file( $strImagePath ) && false == @unlink( $strImagePath ) ) {
			$strMessage = 'Could not remove Local file: ' . $strImagePath;
			$this->handlePaymentErrorMessages( $strMessage );
			return false;
		}
		return true;
	}

	/**
	 * @param $strErrorMessage
	 * @param $boolSendEmail
	 * @return bool
	 */
	protected function handlePaymentErrorMessages( $strErrorMessage, $boolSendEmail = false ) {

		if( true == empty( $strErrorMessage ) ) {
			return false;
		}

		$objLogger = \Psi\Libraries\Logger\Remote\CRemoteLogger::create( CArPaymentImage::class );
		$objLogger->error( $strErrorMessage );

		$strErrorMessage .= "\n\n";
		$strErrorMessage .= 'Backtrace: ' . "\n\n";
		$strErrorMessage .= json_encode( debug_backtrace( 0 ), JSON_PRETTY_PRINT );

		if( true == $boolSendEmail ) {
			$this->sendErrorEmail( $strErrorMessage );
		}

		// trigger_error( $strErrorMessage, E_USER_WARNING );
		return true;
	}

	/**
	 * @param $strErrorMessage
	 */
	private function sendErrorEmail( $strErrorMessage ) {
		$objPaymentEmailer = new CPaymentEmailer();

		$strErrorMessages = 'The following Errors occurred as a Check 21 Image Object Store Call: \n';
		$strErrorMessages .= "\n\n\n";
		$strErrorMessages .= 'Image: ' . $this->getImageName();
		$strErrorMessages .= "\n\n\n";

		$strErrorMessages .= $strErrorMessage;
		$strErrorMessages .= "\n";

		$arrstrErrorEmails = [
			CSystemEmail::RJENSEN_EMAIL_ADDRESS,
			CSystemEmail::TMARKS_EMAIL_ADDRESS
		];

		$objPaymentEmailer->emailEmergencyTeam( $strErrorMessages, 'ERROR during Check21 Image Object Store Call CArPaymentImage Class', $arrstrErrorEmails );

	}

	/**
	 * @return string
	 */
	public function getObjectStoreFilePath() {
		return self::CHECK_IMAGES_PATH . $this->getImagePathWithoutMountPath();
	}

	/**
	 * @return string
	 */
	public function getObjectStoreFileName() {
		$strRegex = '/\.s?enc$/i';
		$strKeyFileName = $this->getImageName();

		// Remove .enc or .senc
		if( preg_match( $strRegex, $strKeyFileName ) ) {
			$strKeyFileName = preg_replace( $strRegex, '', $strKeyFileName );
		}
		return $strKeyFileName;
	}

	/**
	 * @param $strKey
	 * @return string
	 */
	protected function objectStoreCleanKey( $strKey ) {

		$strMountsPathToRemove = strstr( $strKey, self::CHECK21_LOCAL_DIR );
		if( false == empty( $strMountsPathToRemove ) ) {
			$strKey = substr( $strMountsPathToRemove, mb_strlen( self::CHECK21_LOCAL_DIR ) );
		}

		$strNonBackUpMountsPathToRemove = strstr( $strKey, self::CHECK21_LOCAL_TEMP_DIR );
		if( false == empty( $strNonBackUpMountsPathToRemove ) ) {
			$strKey = substr( $strNonBackUpMountsPathToRemove, mb_strlen( self::CHECK21_LOCAL_TEMP_DIR ) );
		}

		if( '/' === substr( $strKey, 0, 1 ) ) {
			$strKey	= ltrim( $strKey, '/' );
		}

		return $strKey;
	}

	/**
	 * Puts the original image content into the payment system bucket
	 *
	 * @throws Exception
	 */
	public function putImageContent() {
		try {
			$objPutImageFile = \Psi\Core\Payment\PaymentFiles\CPutCheckImageFile::create();
			$objPutImageFile->setArPaymentImage( $this );
			$objPutImageFile->setUserId( $this->getUserId() );
			$objPutImageFile->setPaymentDatabase( $this->getPaymentDatabase() );

			$objPutImageFile->paymentSystemPutObject();
		} catch( \Exception $objException ) {
			throw $objException;
		}
	}

	public function rebuildCache( $boolBlockOut = false, $intWidth = NULL, $boolOverwrite = false ) {
		$boolIsSuccessful = true;

		if( true == $boolIsSuccessful ) {
			// Determine the GIF image name dynamically that can be served from cache.
			if( false == ( true == is_file( $this->m_strCacheFileName ) && false == $boolOverwrite ) ) {
				switch( $boolBlockOut ) {
					case true:
						if( true == is_null( $intWidth ) ) {
							if( false == $this->callImageMagickConvertShellCommand( $this->m_strCacheFileName, true ) || false == is_file( $this->m_strCacheFileName ) ) {
								$boolIsSuccessful = false;
							}
						} else {
							if( true == $this->callImageMagickConvertShellCommand( $this->m_strCacheFileName, true ) ) {
								if( false == $this->callGdThumbnailCreationRoutine( $this->m_strCacheFileName, $intWidth ) ) {
									$boolIsSuccessful = false;
								}
							} else {
								$boolIsSuccessful = false;
							}
						}
						break;

					case false:
						if( true == is_null( $intWidth ) ) {
							if( false == $this->callImageMagickConvertShellCommand( $this->m_strCacheFileName ) || false == is_file( $this->m_strCacheFileName ) ) {
								$boolIsSuccessful = false;
							}
						} else {
							if( false == $this->callImageMagickConvertShellCommand( $this->m_strCacheFileName ) ) {
								$boolIsSuccessful = false;
							} else {
								if( false == $this->callGdThumbnailCreationRoutine( $this->m_strCacheFileName, $intWidth ) ) {
									$boolIsSuccessful = false;
								}
							}
						}
						break;

					default:
						// default case
						break;
				}
			}

			if( false == $boolIsSuccessful ) {
				$this->displayMsgImageFailedToLoad();
			}
		}

		return $this->m_strCacheFileName;
	}

	private function callGdThumbnailCreationRoutine( $strGifFile, $intThumbnailWidth = NULL ) {
		if( false == is_file( $strGifFile ) ) {
			return false;
		}

		$resImg = imagecreatefromgif( $strGifFile );

		$intWidth = imagesx( $resImg );
		$intHeight = imagesy( $resImg );

		$intNewWidth = $intThumbnailWidth;
		$intNewHeight = floor( $intHeight * ( $intThumbnailWidth / $intWidth ) );

		// create a new temporary image
		$strTempGifFile = imagecreatetruecolor( $intNewWidth, $intNewHeight );

		// copy and resize old image into new image
		imagecopyresized( $strTempGifFile, $resImg, 0, 0, 0, 0, $intNewWidth, $intNewHeight, $intWidth, $intHeight );

		// save thumbnail into a file
		return imagegif( $strTempGifFile, $strGifFile );
	}

	private function callImageMagickConvertShellCommand( $strGifFile, $boolBlockOut = false ) {
		$boolIsValid = true;

		// If $boolBlockOut is set to true, we block out the bottom 20% of the image.
		if( true == $boolBlockOut ) {
			$arrstrImageData = ( ( true == is_file( $this->m_strFullDecryptedFileName ) ) && 0 < filesize( $this->m_strFullDecryptedFileName ) && true == file_exists( $this->m_strFullDecryptedFileName ) ) ? getimagesize( $this->m_strFullDecryptedFileName ) : NULL;

			$intWidth = $arrstrImageData[0];
			$intHeight = $arrstrImageData[1];

			$intX1 = 0;
			$intY1 = round( ( $intHeight * .88 ), 0 );
			$intX2 = $intWidth;
			$intY2 = $intHeight;

			$intTextX1 = 10;
			$intTextY1 = round( ( $intHeight + $intY1 ) / 2, 0 );

			exec( 'convert -quiet -fill black -draw "rectangle ' . ( int ) $intX1 . ',' . ( int ) $intY1 . ' ' . ( int ) $intX2 . ',' . ( int ) $intY2 . '" -fill white -pointsize 30 -draw "text ' . ( int ) $intTextX1 . ',' . ( int ) $intTextY1 . ' \'This check has been modified for security purposes.  Call 1-801-375-5522 for support.\'" ' . $this->m_strFullDecryptedFileName . ' ' . $strGifFile, $strTerminalOutput, $intReturnValue );
		} else {
			// Here we are using imageMagick from the command line to convert the image to a gif file.
			exec( 'convert -quiet ' . $this->m_strFullDecryptedFileName . ' ' . $strGifFile, $strTerminalOutput, $intReturnValue );
		}

		if( 0 !== $intReturnValue ) {
			$boolIsValid = false;
			$this->displayMsgImageFailedToLoad();
		}

		return $boolIsValid;
	}

}

?>