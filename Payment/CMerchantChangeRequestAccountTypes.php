<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CMerchantChangeRequestAccountTypes
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CMerchantChangeRequestAccountTypes extends CBaseMerchantChangeRequestAccountTypes {

	public static function fetchMerchantChangeRequestAccountTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMerchantChangeRequestAccountType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMerchantChangeRequestAccountType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMerchantChangeRequestAccountType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>