<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CWesternUnionOutputFiles
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CWesternUnionOutputFiles extends CBaseWesternUnionOutputFiles {

	public static function fetchWesternUnionOutputFileByFileNameByHeaderDateTime( $strFileName, $strHeaderDatetime, $objPaymentDatabase ) {

		$strSql = 'SELECT
						*
						FROM
							western_union_output_files f
						WHERE
							f.file_name = \'' . addslashes( $strFileName ) . '\'
							AND f.header_datetime = \'' . $strHeaderDatetime . '\'
						LIMIT 1;';

		return self::fetchWesternUnionOutputFile( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnReconPreppedWesternUnionOutputFiles( $objPaymentDatabase ) {

		$strSql = 'SELECT
							wuof.*
						FROM
							western_union_output_files wuof
						WHERE
							 wuof.is_recon_prepped <> 1
					LIMIT 10000';

		return self::fetchWesternUnionOutputFiles( $strSql, $objPaymentDatabase );
	}

}
?>