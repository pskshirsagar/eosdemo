<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937ReturnFiles
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CX937ReturnFiles extends CBaseX937ReturnFiles {

	public static function fetchX937ReturnFileByProcessingBankIdByFileName( $intProcessingBankId, $strFileName, $objPaymentDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						x937_return_files
					WHERE
						processing_bank_id = ' . ( int ) $intProcessingBankId . '
						AND file_name = \'' . $strFileName . '\'
					LIMIT 1';

		return self::fetchX937ReturnFile( $strSql, $objPaymentDatabase );
	}

}
?>