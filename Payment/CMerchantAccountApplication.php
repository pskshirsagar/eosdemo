<?php

use Psi\Libraries\ExternalFpdf\CFpdf;

class CMerchantAccountApplication extends CBaseMerchantAccountApplication {

	protected $m_boolIsLoggedIn;
	protected $m_boolIsLocked;

	protected $m_arrobjApplicationKeyValues;

	protected $m_intDaysForBankToComplete;
	protected $m_intDaysToCompleteEntireProcess;
	protected $m_intContactPersonType;
	protected $m_intMerchantAccountApplicationTypesId;

	protected $m_strLastUpdatedOn;
	protected $m_strLastUpdate;
	protected $m_strEmailFrom;
	protected $m_strEmailTo;
	protected $m_strEmailMessage;
	protected $m_strEmailSubject;
	protected $m_strContactEmailAddress;
	protected $m_strUnencryptedPassword;

	protected $m_objPdf;
	protected $m_intX;
	protected $m_intY;

	const CURRENCY_MAX_LIMIT	= 99999999999.9;
	const HOME_RENT 			= 1;
	const HOME_OWN 				= 0;
	const POSTAL_CODE_LENGTH	= 4;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsLoggedIn 					= false;
		$this->m_arrobjApplicationKeyValues 		= false;
		$this->m_intDaysForBankToComplete 			= false;
		$this->m_intDaysToCompleteEntireProcess 	= false;
		$this->m_strLastUpdatedOn 					= false;
		$this->m_strLastUpdate 						= false;

		return;
	}

	/**
	 * Get Functions
	 */

	public function getIsLoggedIn() {
		return $this->m_boolIsLoggedIn;
	}

	public function getLastUpdate() {
		return $this->m_strLastUpdate;
	}

	public function getLastUpdatedOn() {
		return $this->m_strLastUpdatedOn;
	}

	public function getDaysForBankToComplete() {
		return $this->m_intDaysForBankToComplete;
	}

	public function getDaysToCompleteEntireProcess() {
		return $this->m_intDaysToCompleteEntireProcess;
	}

	public function getCompanyTaxNumber() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ];
		return( true == isset( $this->m_strCompanyTaxNumberEncrypted ) ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strCompanyTaxNumberEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, $arrmixOptions ) : NULL;
	}

	public function getVpTaxNumber() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ];
		return( true == isset( $this->m_strVpTaxNumberEncrypted ) ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strVpTaxNumberEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, $arrmixOptions ) : NULL;
	}

	public function getTypeOfBusiness() {

		$strSubValue = 'Property Management';

		if( isset( $this->m_strTypeOfBusiness ) && false == empty( $this->m_strTypeOfBusiness ) ) {
			return $this->m_strTypeOfBusiness;
		} else {
			return $strSubValue;
		}
	}

	public function getEmailFrom() {
		return $this->m_strEmailFrom;
	}

	public function getEmailTo() {
		return $this->m_strEmailTo;
	}

	public function getEmailMessage() {
		return $this->m_strEmailMessage;
	}

	public function getEmailSubject() {
		return $this->m_strEmailSubject;
	}

	public function getContactEmailAddress() {
		return $this->m_strContactEmailAddress;
	}

	public function getContactPersonType() {
		return $this->m_intContactPersonType;
	}

	public function getApplicationKeyValues() {
		return $this->m_arrobjApplicationKeyValues;
	}

	public function getIsLocked() {
		return $this->m_boolIsLocked;
	}

	public function getUnencryptedPassword() {
		if( false == valStr( $this->m_strPassword ) ) {
			return;
		}
		$this->m_strUnencryptedPassword = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strPassword, CONFIG_SODIUM_KEY_MERCHANT_ACCOUNT_APPLICATION, [ 'legacy_secret_key' => CONFIG_KEY_MERCHANT_ACCOUNT_APPLICATION ] );
		return $this->m_strUnencryptedPassword;
	}

	public function getMerchantAccountApplicationTypesId() {
		return $this->m_intMerchantAccountApplicationTypesId;
	}

	/**
	 * Set Functions
	 */

	public function setIsLoggedIn( $boolIsLoggedIn ) {
		$this->m_boolIsLoggedIn = ( bool ) $boolIsLoggedIn;
	}

	public function setApplicationKeyValues( $arrobjApplicationKeyValues ) {
		$this->m_arrobjApplicationKeyValues = $arrobjApplicationKeyValues;
	}

	public function addApplicationKeyValue( $objApplicationKeyValue ) {
		$this->m_arrobjApplicationKeyValues[$objApplicationKeyValue->getId()] = $objApplicationKeyValue;
	}

	public function setCompanyTaxNumber( $strCompanyTaxNumber ) {
		if( false == valStr( $strCompanyTaxNumber ) ) {
			$this->setCompanyTaxNumberEncrypted( NULL );

			return NULL;
		}

		$strCompanyTaxNumber = preg_replace( '/[^0-9]/', '', $strCompanyTaxNumber );
		$this->setCompanyTaxNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCompanyTaxNumber, CONFIG_SODIUM_KEY_TAX_NUMBER ) );
	}

	public function setVpTaxNumber( $strVpTaxNumber ) {
		if( false == valStr( $strVpTaxNumber ) ) {
			$this->setVpTaxNumberEncrypted( NULL );

			return NULL;
		}

		$this->setVpTaxNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strVpTaxNumber, CONFIG_SODIUM_KEY_TAX_NUMBER ) );
	}

	public function setIsLocked( $boolIsLocked ) {
		$this->m_boolIsLocked = $boolIsLocked;
	}

    public function setCompanyName( $strCompanyName ) {
        parent::setCompanyName( getSanitizedFormField( $strCompanyName ) );
    }

    public function setCompanyDbaName( $strCompanyDbaName ) {
        parent::setCompanyDbaName( getSanitizedFormField( $strCompanyDbaName ) );
    }

    public function setWebsiteUrl( $strWebsiteUrl ) {
        parent::setWebsiteUrl( getSanitizedWebsiteUrlField( $strWebsiteUrl ) );
    }

    public function setCompanyStreetLine1( $strCompanyStreetLine1 ) {
        parent::setCompanyStreetLine1( getSanitizedFormField( $strCompanyStreetLine1, true ) );
    }

    public function setCompanyCity( $strCompanyCity ) {
        parent::setCompanyCity( getSanitizedFormField( $strCompanyCity ) );
    }

    public function setContactName( $strContactName ) {
        parent::setContactName( getSanitizedFormField( $strContactName, true ) );
    }

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['company_tax_number'] ) )		$this->setCompanyTaxNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['company_tax_number'] ) : $arrstrValues['company_tax_number'] );
		if( true == isset( $arrstrValues['vp_tax_number'] ) ) 			$this->setVPTaxNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['vp_tax_number'] ) : $arrstrValues['vp_tax_number'] );
		if( true == isset( $arrstrValues['email_from'] ) ) 				$this->setEmailFrom( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['email_from'] ) : $arrstrValues['email_from'] );
		if( true == isset( $arrstrValues['email_to'] ) ) 				$this->setEmailTo( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['email_to'] ) : $arrstrValues['email_to'] );
		if( true == isset( $arrstrValues['email_message'] ) ) 			$this->setEmailMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['email_message'] ) : $arrstrValues['email_message'] );
		if( true == isset( $arrstrValues['email_subject'] ) ) 			$this->setEmailSubject( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['email_subject'] ) : $arrstrValues['email_subject'] );
		if( true == isset( $arrstrValues['contact_email_address'] ) )	$this->setContactEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['contact_email_address'] ) : $arrstrValues['contact_email_address'] );
		if( true == isset( $arrstrValues['contact_person_type'] ) ) 	$this->setContactPersonType( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['contact_person_type'] ) : $arrstrValues['contact_person_type'] );
		if( true == isset( $arrstrValues['unencrypted_password'] ) ) 	$this->setUnencryptedPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['unencrypted_password'] ) : $arrstrValues['unencrypted_password'] );
		if( true == isset( $arrstrValues['number_of_owners'] ) ) 		$this->setNumberOfOwners( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['number_of_owners'] ) : $arrstrValues['number_of_owners'] );
		if( true == isset( $arrstrValues['merchant_account_application_types_id'] ) ) 	$this->setMerchantAccountApplicationTypesId( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['merchant_account_application_types_id'] ) : $arrstrValues['merchant_account_application_types_id'] );

		return;
	}

	public function setDefaultApplicationKeyValues() {

		$this->m_arrobjApplicationKeyValues = array();

		// Question 1
		$objApplicationKeyValue = new CApplicationKeyValue();
		$objApplicationKeyValue->setKey( 'QUESTION1_PRODUCTS_SERVICES' );
		$objApplicationKeyValue->setValue( 'Rent, Utilities, and Laundry Service.' );
		array_push( $this->m_arrobjApplicationKeyValues, $objApplicationKeyValue );

		// Question 2
		$objApplicationKeyValue = new CApplicationKeyValue();
		$objApplicationKeyValue->setKey( 'QUESTION2_HOW_ADVERTISE' );
		$objApplicationKeyValue->setValue( 'Phone Book, Newspaper, Apartment Guide.com, Web Site (http://www.**ENTER-YOUR-WEBSITE-NAME**.com), Online Listing Services.' );
		array_push( $this->m_arrobjApplicationKeyValues, $objApplicationKeyValue );

		// Question 3
		$objApplicationKeyValue = new CApplicationKeyValue();
		$objApplicationKeyValue->setKey( 'QUESTION3_WHAT_CHARGE_FOR' );
		$objApplicationKeyValue->setValue( 'Rent, Utilities, Late Fees, Laundry Services, and Day Care.' );
		array_push( $this->m_arrobjApplicationKeyValues, $objApplicationKeyValue );

		// Question 4
		$objApplicationKeyValue = new CApplicationKeyValue();
		$objApplicationKeyValue->setKey( 'QUESTION4_BILLING_POLICIES' );
		$objApplicationKeyValue->setValue( 'We charge rent once per month on the 1st. Rent is late on the 5th. Residents pay a $50 late fee for paying rent after the fifth.' );
		array_push( $this->m_arrobjApplicationKeyValues, $objApplicationKeyValue );

		// Question 5
		$objApplicationKeyValue = new CApplicationKeyValue();
		$objApplicationKeyValue->setKey( 'QUESTION5_CHARGED_IN_ADVANCE' );
		$objApplicationKeyValue->setValue( 'A resident will be charged rent on the first of each month he or she occupies a unit, or the first of each month during the resident\'s contractual period. Residents pay an application fee when applying and pay their deposit (**1 MONTH**) before they move in.' );
		array_push( $this->m_arrobjApplicationKeyValues, $objApplicationKeyValue );

		// Question 6
		$objApplicationKeyValue = new CApplicationKeyValue();
		$objApplicationKeyValue->setKey( 'QUESTION6_LEASE_PERIOD' );
		$objApplicationKeyValue->setValue( 'Most leases are 12-month leases that residents are contractually bound to. Leases convert to "month-to-month" leases after the contractual period has expired. When a lease converts to "month-to-month", a resident can move out with 15 days prior notice.' );
		array_push( $this->m_arrobjApplicationKeyValues, $objApplicationKeyValue );

		// Question 7
		$objApplicationKeyValue = new CApplicationKeyValue();
		$objApplicationKeyValue->setKey( 'QUESTION7_REFUND_POLICY' );
		$objApplicationKeyValue->setValue( 'No refunds on rent are available. When a resident pays rent and occupies a unit for a set period of time, a resident cannot request a refund on rent. If a charge were made erroneously, the charge would refunded immediately.' );
		array_push( $this->m_arrobjApplicationKeyValues, $objApplicationKeyValue );

		return;
	}

	public function setDaysToCompleteEntireProcess( $intDaysToCompleteEntireProcess ) {
		$this->m_intDaysToCompleteEntireProcess = $intDaysToCompleteEntireProcess;
	}

	public function setDaysForBankToComplete( $intDaysForBankToComplete ) {
		$this->m_intDaysForBankToComplete = $intDaysForBankToComplete;
	}

	public function setEmailFrom( $strEmailFrom ) {
		$this->m_strEmailFrom = $strEmailFrom;
	}

	public function setEmailTo( $strEmailTo ) {
		$this->m_strEmailTo = $strEmailTo;
	}

	public function setEmailMessage( $strEmailMessage ) {
		$this->m_strEmailMessage = $strEmailMessage;
	}

	public function setEmailSubject( $strEmailSubject ) {
		$this->m_strEmailSubject = $strEmailSubject;
	}

	public function setContactEmailAddress( $strContactEmailAddress ) {
		$this->m_strContactEmailAddress = $strContactEmailAddress;
	}

	public function setContactPersonType( $intContactPersonType ) {
		$this->m_intContactPersonType = $intContactPersonType;
	}

	public function setUnencryptedPassword( $strUnencryptedPassword ) {
		$this->m_strPassword = $this->m_strUnencryptedPassword = trim( $strUnencryptedPassword );
		$this->m_strPassword = CEncryption::encryptText( $this->m_strUnencryptedPassword, CONFIG_KEY_MERCHANT_ACCOUNT_APPLICATION );
	}

	public function setMerchantAccountApplicationTypesId( $intMerchantAccountApplicationTypesId ) {
		$this->m_intMerchantAccountApplicationTypesId = $intMerchantAccountApplicationTypesId;
	}

	/**
	 * Create Functions
	 */

	public function createApplicationKeyValue() {

		$objApplicationKeyValue = new CApplicationKeyValue();
		$objApplicationKeyValue->setCid( $this->getCid() );
		$objApplicationKeyValue->setMerchantAccountApplicationId( $this->getId() );

		return $objApplicationKeyValue;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchApplicationBankAccounts( $intNumOfAccountsRequested, $objPaymentDatabase ) {

		return \Psi\Eos\Payment\CApplicationBankAccounts::createService()->fetchApplicationBankAccountsByNumberRequested( $this->getId(), $intNumOfAccountsRequested, $objPaymentDatabase );
	}

	public function fetchApplicationKeyValues( $objPaymentDatabase ) {

		return \Psi\Eos\Payment\CApplicationKeyValues::createService()->fetchApplicationKeyValuesByMerchantAccountApplicationId( $this->getId(), $objPaymentDatabase );
	}

	public function fetchApplicationKeyValueByKey( $strKey, $objPaymentDatabase ) {

		return \Psi\Eos\Payment\CApplicationKeyValues::createService()->fetchApplicationKeyValueByKeyByMerchantAccountApplicationId( $strKey, $this->getId(), $objPaymentDatabase );
	}

	public function fetchTaskByTaskTypeId( $intTaskTypeId, $objAdminDatabase ) {
		return \Psi\Eos\Admin\CTasks::createService()->fetchTaskByMerchantAccountApplicationIdByTaskTypeId( $this->getId(), $intTaskTypeId, $objAdminDatabase );
	}

	public function fetchTaskByTaskTypeIds( $arrintTaskTypeIds, $objAdminDatabase ) {
		return \Psi\Eos\Admin\CTasks::createService()->fetchTaskByMerchantAccountApplicationIdByTaskTypeIds( $this->getId(), $arrintTaskTypeIds, $objAdminDatabase );
	}

	/**
	 * Validation Functions
	 */

	public function valCompanyName() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', 'Company name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyStreetLine1() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyStreetLine1() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company street line1', 'Company street is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyCity() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company city', 'Company city is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyStateCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company state code', 'Company state code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyPostalCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company postal code', 'Company zip code is required.' ) );
		} elseif( self::POSTAL_CODE_LENGTH >= strlen( $this->getCompanyPostalCode() ) || false == preg_match( '/^[0-9a-zA-Z\-]+$/', $this->getCompanyPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company postal code', 'Invalid company zip code.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyTaxNumberEncrypted() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyTaxNumber() ) || 0 == strlen( trim( $this->getCompanyTaxNumber() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_tax_number', 'EIN is required.' ) );
		} elseif( false == preg_match( '/^\d{9}$/', $this->getCompanyTaxNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_encrypted', 'Please enter valid EIN.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyPhoneNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Company phone number is required.' ) );
		} elseif( 1 != CValidation::checkPhoneNumberFullValidation( $this->m_strCompanyPhoneNumber, true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Enter valid phone number.', 611 ) );
		}

		return $boolIsValid;
	}

	public function valCompanyFaxNumber() {
		$boolIsValid = true;

		if( false == is_null( $this->getCompanyfaxNumber() ) && 1 != CValidation::checkPhoneNumberFullValidation( $this->getCompanyfaxNumber(), true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', 'Enter valid Fax number.', 611 ) );
		}

		return $boolIsValid;
	}

	public function valOwnershipTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getOwnershipTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ownership_type', 'Ownership type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTypeOfBusiness() {
		$boolIsValid = true;

		if( true == is_null( $this->getTypeOfBusiness() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type of business', 'Type of business is required.' ) );
		}
		return $boolIsValid;
	}

	public function valAgeOfBusiness() {
		$boolIsValid = true;

		if( true == is_null( $this->getAgeOfBusiness() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'age of business', 'Age of business is required.' ) );
		} elseif( 0 == strlen( trim( $this->getAgeOfBusiness() ) ) || 0 > trim( $this->getAgeOfBusiness() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'age of business', 'Age of business is Invalid.' ) );
		}

		return $boolIsValid;
	}

	public function valDateBusinessAcquired() {
		$boolIsValid = true;

		if( false == is_null( $this->getDateBusinessAcquired() ) && false == CValidation::validateDate( $this->getDateBusinessAcquired() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Business_acquired_date', 'Business acquired date is invalid.' ) );
		}

		return $boolIsValid;
	}

	public function valBilltoStreetLine1() {
		$boolIsValid = true;

		if( true == is_null( $this->getBilltoStreetLine1() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing to street line1', 'Billing street is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBilltoCity() {
		$boolIsValid = true;

		if( true == is_null( $this->getBilltoCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing to city', 'Billing city is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBilltoStateCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getBilltoStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing to state code', 'Billing state code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBilltoPostalCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getBilltoPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing to postal code', 'Billing zip code is required.' ) );
		} elseif( false == is_numeric( $this->getBilltoPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company postal code', 'Invalid billing zip code.' ) );
		}

		return $boolIsValid;
	}

	public function valContactName() {
		$boolIsValid = true;

		if( true == is_null( $this->getContactName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact name', 'Contact name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valContactDayTimePhone() {
		$boolIsValid = true;

		if( true == is_null( $this->getContactDayTimePhone() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Contact day time phone number is required.' ) );
		} elseif( 1 != CValidation::checkPhoneNumberFullValidation( $this->getContactDayTimePhone(), true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Enter valid Contact day time phone number.', 611 ) );
		}

		return $boolIsValid;
	}

	public function valContactEveningPhone() {
		$boolIsValid = true;

		if( false == is_null( $this->getContactEveningPhone() ) && 1 != CValidation::checkPhoneNumberFullValidation( $this->getContactEveningPhone(), true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'evening_phone_number', 'Evening phone number must be in xxx-xxx-xxxx format.', 611 ) );
		}

		return $boolIsValid;
	}

	public function valWebsiteUrl() {
		$boolIsValid = true;

		$strLooseUrlValidatorRegEx = '((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.\-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.\-]+)((?:\/[\+~%\/.\w\-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)';

		if( false == is_null( $this->getWebsiteUrl() ) && false == preg_match( $strLooseUrlValidatorRegEx, $this->getWebsiteUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_url', 'Website url is required and needs to resemble a valid URL' ) );
		}

		return $boolIsValid;
	}

	public function valVpName() {
		$boolIsValid = true;

		if( true == is_null( $this->getVpName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary name', 'Secondary name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valVpTitle() {
		$boolIsValid = true;

		if( true == is_null( $this->getVpTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_title', 'Secondary title is required.' ) );
		}

		return $boolIsValid;
	}

	public function valVpTaxNumberEncrypted() {
		$boolIsValid = true;

		if( true == is_null( $this->getVpTaxNumber() ) || 0 == strlen( trim( $this->getVpTaxNumber() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary tax number', 'Secondary SSN is required.' ) );
		}

		return $boolIsValid;
	}

	public function valVpDob() {
		$boolIsValid = true;

		if( true == is_null( $this->getVpDob() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary birth date', 'Secondary birth date is required.' ) );
		} elseif( false == CValidation::validateDate( $this->getVpDob() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary birth date', 'Secondary birth date is not a valid date.' ) );
		} elseif( strtotime( $this->getVpDob() ) >= strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_birth_date', 'Secondary date of birth should be earlier than current date.' ) );
		}

		return $boolIsValid;
	}

	public function valVpStreetLine1() {
		$boolIsValid = true;

		if( true == is_null( $this->getVpStreetLine1() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary street address', 'Secondary street address is required.' ) );
		}

		return $boolIsValid;
	}

	public function valVpCity() {
		$boolIsValid = true;

		if( true == is_null( $this->getVpCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary city', 'Secondary city is required.' ) );
		}

		return $boolIsValid;
	}

	public function valVpStateCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getVpStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary state code', 'Secondary state code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valVpPostalCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getVpPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary zip code', 'Secondary zip code is required.' ) );
		} elseif( false == is_numeric( $this->getVpPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary zip code', 'Invalid secondary zip code.' ) );
		}

		return $boolIsValid;
	}

	public function valVpResidence() {
		$boolIsValid = true;

		if( true == is_null( $this->getVpResidence() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary residence', 'Secondary time spent in current residence is required.' ) );
		}

		return $boolIsValid;
	}

	public function valVpRentOrOwn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valVpHomePhone() {
		$boolIsValid = true;

		if( false == is_null( $this->getVpHomePhone() ) && 1 != CValidation::checkPhoneNumberFullValidation( $this->getVpHomePhone(), true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_phone_number', 'Secondary phone number must be in xxx-xxx-xxxx format.', 611 ) );
		}

		return $boolIsValid;
	}

	public function valAchMaxTransactionsPerDay() {
		$boolIsValid = true;

		if( true == is_null( $this->getAchMaxTransactionsPerDay() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question5', 'Processing volume question5\'s answeris required.' ) );
		}

		if( false == is_null( $this->getAchMaxTransactionsPerDay() ) && false == is_int( $this->getAchMaxTransactionsPerDay() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question5', 'Processing volume question5\'s answer should be number.' ) );
		}

		return $boolIsValid;
	}

	public function valAchMaxAmountPerTransaction() {
		$boolIsValid = true;

		if( true == is_null( $this->getAchMaxAmountPerTransaction() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question3', 'Processing volume question3\'s answer is required.' ) );
		} elseif( false == is_numeric( $this->getAchMaxAmountPerTransaction() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question3', 'Processing volume question3\'s answer should be numeric.' ) );
		} elseif( self::CURRENCY_MAX_LIMIT < $this->getAchMaxAmountPerTransaction() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question3', 'Processing volume question3\'s answer value exceeds the limit.' ) );
		}

		return $boolIsValid;
	}

	public function valAchAvgAmountPerTransaction() {
		$boolIsValid = true;

		if( true == is_null( $this->getAchAvgAmountPerTransaction() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question2', 'Processing volume question2\'s answeris required.' ) );
		} elseif( false == is_numeric( $this->getAchAvgAmountPerTransaction() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question2', 'Processing volume question2\'s answer should be numeric.' ) );
		} elseif( self::CURRENCY_MAX_LIMIT < $this->getAchAvgAmountPerTransaction() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question2', 'Processing volume question2\'s answer value exceeds the limit.' ) );
		}

		return $boolIsValid;
	}

	public function valAchMaxAmountPerDay() {
		$boolIsValid = true;

		if( true == is_null( $this->getAchMaxAmountPerDay() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question1', 'Processing volume question1\'s answeris required.' ) );
		} elseif( false == is_numeric( $this->getAchMaxAmountPerDay() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question1', 'Processing volume question1\'s answer should be numeric.' ) );
		} elseif( self::CURRENCY_MAX_LIMIT < $this->getAchMaxAmountPerDay() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question1', 'Processing volume question1\'s answer value exceeds the limit.' ) );
		}

		return $boolIsValid;
	}

	public function valAchMaxAmountPerMonth() {
		$boolIsValid = true;

		if( true == is_null( $this->getAchMaxAmountPerMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question4', 'Processing volume question4\'s answeris required.' ) );
		} elseif( false == is_numeric( $this->getAchMaxAmountPerMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question4', 'Processing volume question4\'s answer should be numeric.' ) );
		} elseif( self::CURRENCY_MAX_LIMIT < $this->getAchMaxAmountPerMonth() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question4', 'Processing volume question4\'s answer value exceeds the limit.' ) );
		}

		return $boolIsValid;
	}

	public function valCcAvgAmountPerTransaction() {
		$boolIsValid = true;

		if( true == is_null( $this->getCcAvgAmountPerTransaction() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question6', 'Processing volume question6\'s answeris required.' ) );
		} elseif( false == is_numeric( $this->getCcAvgAmountPerTransaction() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question6', 'Processing volume question6\'s answer should be numeric.' ) );
		} elseif( self::CURRENCY_MAX_LIMIT < $this->getCcAvgAmountPerTransaction() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question6', 'Processing volume question6\'s answer value exceeds the limit.' ) );
		}

		return $boolIsValid;
	}

	public function valCcMaxAmountPerTransaction() {
		$boolIsValid = true;

		if( true == is_null( $this->getCcmaxAmountPerTransaction() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question7', 'Processing volume question7\'s answeris required.' ) );
		} elseif( false == is_numeric( $this->getCcmaxAmountPerTransaction() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question7', 'Processing volume question7\'s answer should be numeric.' ) );
		} elseif( self::CURRENCY_MAX_LIMIT < $this->getCcMaxAmountPerTransaction() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question7', 'Processing volume question7\'s answer value exceeds the limit.' ) );
		}

		return $boolIsValid;
	}

	public function valCcMaxAmountPerMonth() {
		$boolIsValid = true;

		if( true == is_null( $this->getCcMaxAmountPerMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question8', 'Processing volume question8\'s answeris required.' ) );
		} elseif( false == is_numeric( $this->getCcMaxAmountPerMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question8', 'Processing volume question8\'s answer should be numeric.' ) );
		} elseif( self::CURRENCY_MAX_LIMIT < $this->getCcMaxAmountPerMonth() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'processing_volume_question8', 'Processing volume question8\'s answer value exceeds the limit.' ) );
		}

		return $boolIsValid;
	}

	public function valContactEmail( $strPanelName = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getContactEmail() ) ) {
			if( 'Admin' != $strPanelName ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email', 'Contact email is required.' ) );
			}

		} elseif( false == CValidation::validateEmailAddresses( $this->getContactEmail() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email', 'Invalid contact email.' ) );
		}

		return $boolIsValid;
	}

	public function valCompareEmails() {
		$boolIsValid = true;
		// I know this is crap but I didn't know how to check the value against a variable not contained in the class.

		return $boolIsValid;
	}

	public function valNumberOfLocations( $strPanelName = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getNumberOfLocations() ) ) {
			if( 'Admin' != $strPanelName ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'management office locations', 'Management office locations is required.' ) );
			}
		} elseif( false == is_numeric( $this->m_intNumOfAccountsRequested ) || false == is_int( $this->m_intNumOfAccountsRequested ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'management office locations', 'Management office locations should be number.' ) );
		}

		return $boolIsValid;
	}

	public function valToEmailAddress() {

		$boolIsValid = true;

		if( true == empty( $this->m_strEmailTo ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'To email address is required.' ) );
		} elseif( false == CValidation::validateEmailAddresses( $this->m_strEmailTo ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'To email address is not a valid email address.' ) );
		}

		return $boolIsValid;
	}

	public function valFromEmailAddress() {

		$boolIsValid = true;

		if( true == empty( $this->m_strEmailFrom ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'from_email_address', 'From email address is required.' ) );
		} elseif( false == CValidation::validateEmailAddresses( $this->m_strEmailFrom ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'from_email_address', 'From email address is not a valid email address.' ) );
		}

		return $boolIsValid;
	}

	public function valSubject() {
		$boolIsValid = true;

		if( true == empty( $this->m_strEmailSubject ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subject', 'Subject is required.' ) );
		}

		return $boolIsValid;
	}

	public function valHtmlContent() {
		$boolIsValid = true;

		if( true == empty( $this->m_strEmailMessage ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'html_content', 'Message is required.' ) );
		}

		return $boolIsValid;
	}

	public function valContactEmailAddress() {
		$boolIsValid = true;

		if( true == empty( $this->m_strContactEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Contact email address', 'Contact email address is required.' ) );
		} elseif( false == CValidation::validateEmailAddresses( $this->m_strContactEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Contact email address', 'Invalid contact email address.' ) );
		}

		return $boolIsValid;
	}

	public function valNumberOfOwners() {
		$boolIsValid = true;

		if( true == is_null( $this->getNumberOfOwners() ) || 0 == strlen( trim( $this->getNumberOfOwners() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_owners', 'Number Of Owners is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $strFormName = NULL, $boolIsFromSend = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valContactEmail( 'Admin' );
				$boolIsValid &= $this->valCompareEmails();
				$boolIsValid &= $this->valNumberOfLocations( 'Admin' );
				$boolIsValid &= $this->valDateBusinessAcquired();

				if( true == $boolIsFromSend ) {
					$boolIsValid &= $this->valContactEmailAddress();
				}
				break;

			case VALIDATE_UPDATE:
				switch( $strFormName ) {
					// all the fields being validated here got moved to form1
					case 'form1':
						$boolIsValid	&= $this->valCompanyName();
						$boolIsValid	&= $this->valCompanyTaxNumberEncrypted();
						$boolIsValid	&= $this->valCompanyPhoneNumber();
						// $boolIsValid	&= $this->valCompanyFaxNumber();
						$boolIsValid	&= $this->valOwnershipTypeId();
						$boolIsValid	&= $this->valWebsiteUrl();
						$boolIsValid 	&= $this->valAgeOfBusiness();
						$boolIsValid 	&= $this->valNumberOfOwners();
						$boolIsValid	&= $this->valCompanyStreetLine1();
						$boolIsValid	&= $this->valCompanyCity();
						$boolIsValid	&= $this->valCompanyStateCode();
						$boolIsValid	&= $this->valCompanyPostalCode();
						$boolIsValid	&= $this->valContactName();
						$boolIsValid	&= $this->valContactDayTimePhone();
						$boolIsValid	&= $this->valContactEmail();
						break;

					case 'form2':
						break;

					case 'form3':
						break;

					case 'form4':
						break;

					default:
						$boolIsValid 	&= $this->valContactEmail( 'Admin' );
						$boolIsValid 	&= $this->valNumberOfLocations( 'Admin' );
						if( true == $boolIsFromSend ) {
							$boolIsValid &= $this->valContactEmailAddress();
						}
						break;
				}
				break;

			case 'validate_additional_authorized_signatories_form':
			 	$boolIsValid &= $this->valToEmailAddress();
				$boolIsValid &= $this->valFromEmailAddress();
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valHtmlContent();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function returnTimestamp( $strTimeStamp ) {

		if( isset( $strTimeStamp ) && false == empty( $strTimeStamp ) ) {
			$strTimeStamp = strtotime( substr( $strTimeStamp, 0, 19 ) );
			return ( isset( $strTimeStamp ) && false == empty( $strTimeStamp ) ) ? date( 'm/d/y', $strTimeStamp ) : '';
		} else {
			return;
		}
	}

	public function applyRequestForm( $arrstrRequestForm, $arrstrFormFields = NULL ) {

		if( true == valArr( $arrstrFormFields ) ) {
			$arrstrRequestForm = mergeIntersectArray( $arrstrFormFields, $arrstrRequestForm );
		}

		$this->setValues( $arrstrRequestForm, false );
	}

	public function delete( $intCurrentUserId, $objPaymentDatabase, $boolReturnSqlOnly = false ) {
		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( date( 'F j Y g:i', mktime() ) );

		return $this->update( $intCurrentUserId, $objPaymentDatabase, $boolReturnSqlOnly );
	}

	public function login( $objPaymentDatabase ) {
		$this->setIsLoggedIn( false );

		$objDataset = $objPaymentDatabase->createDataset();
		$objDataset->initialize();

		$strSql = sprintf( 'SELECT * FROM merchant_account_applications WHERE username = %s AND password = %s',
							$this->sqlUsername(),
							$this->sqlPassword() );

		if( !$objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to login. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objPaymentDatabase->errorMsg() ) );
			$objDataset->cleanup();
			return false;
		}

		if( $objDataset->getRecordCount() <> 1 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Invalid username and/or password.' ) );
			// Trigger an error to log the invalid login attempt
			// trigger_error( sprintf( "Invalid username '%s' and/or password '%s'.", $this->getUsername(), $this->getPassword() ), E_USER_NOTICE );
			$objDataset->cleanup();
			return false;
		}

		$arrstrValues = $objDataset->fetchArray();
		$this->setValues( $arrstrValues );

		$objDataset->cleanup();

		$this->setIsLoggedIn( true );

		return true;
	}

	public function logout() {

		$this->setIsLoggedIn( false );
		return true;
	}

	public function loadLastUpdateInfo( $arrstrMerchantAccountStatusTypes ) {

		$intMostRecentUpdate = 0;
		$strMostRecentUpdate = '';
		$this->m_boolIsLocked = 0;

		if( true == valArr( $this->m_arrobjApplicationKeyValues ) ) {
			foreach( $this->m_arrobjApplicationKeyValues as $objApplicationKeyValue ) {
				if( false == is_null( $objApplicationKeyValue->getValue() ) && $intMostRecentUpdate < strtotime( $objApplicationKeyValue->getValue() ) ) {
					$intMostRecentUpdate = strtotime( $objApplicationKeyValue->getValue() );
					if( true == array_key_exists( $objApplicationKeyValue->getKey(), $arrstrMerchantAccountStatusTypes ) ) {
						$strMostRecentUpdate = $arrstrMerchantAccountStatusTypes[$objApplicationKeyValue->getKey()];
					} else {
						$strMostRecentUpdate = '';
					}
				}

				if( 'APPLICATION_LOCKED_ON' == $objApplicationKeyValue->getKey() ) {
					$this->m_boolIsLocked = 1;
				}
			}
		}

		if( 0 == $intMostRecentUpdate ) return;

		$this->m_strLastUpdate 	= $strMostRecentUpdate;
		$this->m_strLastUpdatedOn = date( 'm/d/Y', $intMostRecentUpdate );

		return;
	}

	public function generateBillPayApplicationPdf( $arrobjApplicationKeyValues ) {
		$strTemp = '';
		$this->m_objPdf = new CFpdf( 'P', 'pt', 'letter' );

		$this->m_objPdf->SetAuthor( CONFIG_COMPANY_NAME );
		$this->m_objPdf->SetCreator( CONFIG_COMPANY_NAME );
		$this->m_objPdf->SetTitle( 'Merchant Account - Bill Pay - Application' );

		$this->m_objPdf->SetXY( 300, 840 );
		$this->m_objPdf->SetFont( 'Arial', 'BU', 9 );
		$this->m_objPdf->setFillColor( 255, 255, 255 );
		$this->m_objPdf->Cell( 10, 30, 'Bill Pay APPLICATION', '', 1, 'C', 1 );

		// Business Information Table
		$this->m_objPdf->Rect( 30, 60, 550, 190 );
		$this->m_objPdf->SetXY( 40, 65 );
		$this->m_objPdf->Cell( 520, 20, 'Business Information', '', 1, 'C', 1 );

		$this->m_intY = 65;

		$this->m_intX = 35; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Legal Name Of Business: ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 115, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getCompanyName(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 115, 95, $this->m_intX + 315, 95 );

		$this->m_intX = 370; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Federal Tax ID#: ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', 'U', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 75, $this->m_intY );
		$this->m_objPdf->Cell( 35, 12, $this->getCompanyTaxNumber(), '', 1, 'L', 1 );

		$this->m_intX = 35; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Doing Business As (dba): ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 115, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getCompanyDbaName(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 115, 115, $this->m_intX + 395, 115 );

		$this->m_intX = 420; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 15, 'Years in Business: ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 90, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getAgeOfBusiness(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 90, 115, $this->m_intX + 120, 115 );

		$this->m_intX = 35; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Type of Ownership: Sole Proprietor ', '', 1, 'L', 1 );

		$this->m_objPdf->SetXY( 145, $this->m_intY );

		if( 1 == $this->getOwnershipTypeId() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', 190, $this->m_intY );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', 190, $this->m_intY );
		}

		$this->m_objPdf->SetXY( 200, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Partnership ', '', 1, 'L', 1 );

		if( 2 == $this->getOwnershipTypeId() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', 255, $this->m_intY );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', 255, $this->m_intY );
		}

		$this->m_objPdf->SetXY( 265, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'LLC ', '', 1, 'L', 1 );

		if( 3 == $this->getOwnershipTypeId() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', 288, $this->m_intY );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', 288, $this->m_intY );
		}

		$this->m_objPdf->SetXY( 298, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Corporation ', '', 1, 'L', 1 );

		if( 4 == $this->getOwnershipTypeId() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', 355, $this->m_intY );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', 355, $this->m_intY );
		}

		$this->m_objPdf->SetXY( 365, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Tax Exempt ', '', 1, 'L', 1 );

		if( 5 == $this->getOwnershipTypeId() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', 420, $this->m_intY );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', 420, $this->m_intY );
		}

		// # of owners
		$this->m_intX = 450; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 15, '# of Owners: ', '', 1, 'L', 1 );

		$strTemp = $this->getNumberOfOwners();
		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 65, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $strTemp, '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 65, $this->m_intY + 10, $this->m_intX + 100, $this->m_intY + 10 );

		$this->m_intX = 35; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Phone Number: ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 75, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getCompanyPhoneNumber(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 75, $this->m_intY + 10, $this->m_intX + 135, $this->m_intY + 10 );

		$this->m_intX = 180; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Website: ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		if( true == $this->longString( $this->getWebsiteurl() ) ) {
			$this->m_objPdf->SetFontSize( 7 );
		}
		$this->m_objPdf->SetXY( $this->m_intX + 40, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getWebsiteurl(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 40, 155, $this->m_intX + 395, 155 );

		$this->m_intX = 35; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Business Address: ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		if( true == $this->longString( $this->getCompanyStreetLine1() ) ) {
			$this->m_objPdf->SetFontSize( 7 );
		}
		$this->m_objPdf->SetXY( $this->m_intX + 85, $this->m_intY );
		$this->m_objPdf->Cell( 30, 10, $this->getCompanyStreetLine1(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 85, $this->m_intY + 10, $this->m_intX + 320, $this->m_intY + 10 );

		$this->m_intX = 355; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 80, 10, 'City:', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		if( true == $this->longString( $this->getCompanyCity() ) ) {
			$this->m_objPdf->SetFontSize( 7 );
		}
		$this->m_objPdf->SetXY( $this->m_intX + 23, $this->m_intY );
		$this->m_objPdf->Cell( 10, 10, $this->getCompanyCity(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 23, $this->m_intY + 10, $this->m_intX + 215, $this->m_intY + 10 );

		$this->m_intX = 35; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'State:', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 30, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getCompanyStateCode(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 30, $this->m_intY + 10, $this->m_intX + 65, $this->m_intY + 10 );

		$this->m_intX = 95; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 18, 10, 'Zip:', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 20, $this->m_intY );
		$this->m_objPdf->Cell( 40, 10, $this->getCompanyPostalCode(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 20, $this->m_intY + 10, $this->m_intX + 60, $this->m_intY + 10 );

		$this->m_intX = 165; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Email Address:', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		if( true == $this->longString( $this->getContactEmail() ) ) {
			$this->m_objPdf->SetFontSize( 7 );
		}
		$this->m_objPdf->SetXY( $this->m_intX + 75, $this->m_intY );
		$this->m_objPdf->Cell( 35, 8, $this->getContactEmail(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 75, $this->m_intY + 10, $this->m_intX + 270, $this->m_intY + 10 );

		$this->m_intX = 35; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Contact Person: ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 75, $this->m_intY );
		$this->m_objPdf->Cell( 35, 9, $this->getContactName(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 75, 215, $this->m_intX + 259, 215 );

		$this->m_intX = 300; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 40, 10, 'Phone #: ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 50, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getContactDayTimePhone(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 50, $this->m_intY + 10, $this->m_intX + 110, $this->m_intY + 10 );

		// Publicly Traded
		$this->m_intX = 35; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 40, 10, 'Publicly Traded: ', '', 1, 'L', 1 );

		$this->m_intX += 75;
		if( true == isset( $arrobjApplicationKeyValues['PUBLICLY_TRADED'] ) && 1 == $arrobjApplicationKeyValues['PUBLICLY_TRADED']->getValue() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', $this->m_intX, $this->m_intY );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', $this->m_intX, $this->m_intY );
		}

		$this->m_intX += 20; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Exchange: ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues['PUBLICLY_TRADED_EXCHANGE'] ) ) {
			$strTemp = $arrobjApplicationKeyValues['PUBLICLY_TRADED_EXCHANGE']->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( $this->m_intX + 55, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( $this->m_intX + 55, $this->m_intY + 10, $this->m_intX + 100, $this->m_intY + 10 );

		$this->m_intX += 110; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Exchange Symbol: ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues['PUBLICLY_TRADED_SYMBOL'] ) ) {
			$strTemp = $arrobjApplicationKeyValues['PUBLICLY_TRADED_SYMBOL']->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( $this->m_intX + 90, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( $this->m_intX + 90, $this->m_intY + 10, $this->m_intX + 150, $this->m_intY + 10 );

		// Note about USA PATRIOT Act - in-between - center
		$this->m_intX = 325; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 30 );
		$this->m_objPdf->Cell( 15, 10, 'ALL Documents and Officer Information below is required to ensure compliance with the', '', 1, 'C', 1 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 15 );
		$this->m_objPdf->Cell( 15, 10, 'USA PATRIOT Act, and bank and credit card underwriting standards', '', 1, 'C', 1 );

		$intCntOfOwner = $this->getNumberOfOwners();
		$intCounter = 0;
		for( $intLoop = 0; $intLoop < $intCntOfOwner; $intLoop++ ) {
			if( $intCounter == 2 ) {
				$this->m_objPdf->AddPage();
				$this->m_intY = 0;
			}

			if( 0 == $intLoop ) {
				$strLoop = '';
			} else {
				$strLoop = $intLoop;
			}

			$this->addBeneficialOwner( $arrobjApplicationKeyValues, $strLoop );
			$intCounter++;

		}

		if( false == empty( $arrobjApplicationKeyValues['OFFICER_NAME4'] ) ) {
			if( 0 == $intCounter % 2 ) {
				$this->m_objPdf->AddPage();
				$this->m_intY = 0;
			}
			$this->addBeneficialOwner( $arrobjApplicationKeyValues, '4' );
			$intCounter++;
		}

		switch( $intCounter ) {

			case 2:
				$boolAddAdditionalPage = false;
				break;

			case 3:
				$this->m_intY = 130;
				$boolAddAdditionalPage = true;
				break;

			case 4:
				$this->m_intY = 280;
				$boolAddAdditionalPage = true;
				break;

			default:
				$boolAddAdditionalPage = true;
				break;
		}

		// Guarantor

		$strGuarantorOption			    = 'OPTION';
		$strGuarantorName 			    = 'GUARANTOR_NAME';
		$strGuarantorTin			    = 'GUARANTOR_TIN';
		$strGuarantorPoliticallyExposed = 'GUARANTOR_POLITICALLY_EXPOSED';
		$strGuarantorAddress		    = 'GUARANTOR_ADDRESS';
		$strGuarantorCity			    = 'GUARANTOR_CITY';
		$strGuarantorStateCode 		    = 'GUARANTOR_STATE_CODE';
		$strGuarantorPostalCode 	    = 'GUARANTOR_POSTAL_CODE';
		$strGuarantorPhoneNumber 	    = 'GUARANTOR_PHONE_NUMBER';
		$strGuarantorEmailAddress 	    = 'GUARANTOR_EMAIL_ADDRESS';

		$this->m_objPdf->Rect( 30, $this->m_intY += 50, 550, 120 );
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 300, $this->m_intY += 15 );
		$this->m_objPdf->Cell( 15, 10, 'Guarantor Information', '', 1, 'C', 1 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Name :', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorName] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorName]->getValue();
			$this->m_objPdf->SetXY( 70, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 70, $this->m_intY + 10, 300, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 305, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'TIN/SSN :', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorTin] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorTin]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 305 + 40, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 305 + 40, $this->m_intY + 10, 430, $this->m_intY + 10 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorOption] ) && '1' == $arrobjApplicationKeyValues[$strGuarantorOption]->getValue() ) {
			$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
			$this->m_objPdf->SetXY( 35, $this->m_intY += 40 );
			$this->m_objPdf->Cell( 15, 10, 'Politically Exposed Person [PEP] :', '', 1, 'L', 1 );

			$strTemp = 'No';
			if( true == isset( $arrobjApplicationKeyValues[$strGuarantorPoliticallyExposed] ) ) {
				$strTemp = ( '1' == $arrobjApplicationKeyValues[$strGuarantorPoliticallyExposed]->getValue() ) ? 'Yes' : 'No';
			}
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 195 + 40, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Address : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorAddress] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorAddress]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			if( true == $this->longString( $strTemp ) ) {
				$this->m_objPdf->SetFontSize( 7 );
			}
			$this->m_objPdf->setXY( 35 + 60, $this->m_intY );
			$this->m_objPdf->Cell( 60, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 35 + 60, $this->m_intY + 10, 35 + 280, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 320, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'City : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorCity] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorCity]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			if( true == $this->longString( $strTemp ) ) {
				$this->m_objPdf->SetFontSize( 7 );
			}
			$this->m_objPdf->SetXY( 320 + 25, $this->m_intY );
			$this->m_objPdf->Cell( 50, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 320 + 25, $this->m_intY + 10, 320 + 150, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 475, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'State : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorStateCode] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorStateCode]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 475 + 30, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 475 + 30, $this->m_intY + 10, 475 + 80, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Zip : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorPostalCode] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorPostalCode]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 35 + 30, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 35 + 30, $this->m_intY + 10, 35 + 110, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 305, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Phone Number:', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorPhoneNumber] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorPhoneNumber]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 305 + 70, $this->m_intY );
			$this->m_objPdf->Cell( 15, 9, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 305 + 70, $this->m_intY + 10, 305 + 265, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Email Address:', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorEmailAddress] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorEmailAddress]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			if( true == $this->longString( $strTemp ) ) {
				$this->m_objPdf->SetFontSize( 7 );
			}
			$this->m_objPdf->SetXY( 35 + 70, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 35 + 70, $this->m_intY + 10, 35 + 365, $this->m_intY + 10 );

		// END OF GUARANTOR
		if( 2 == $intCounter ) {
			$this->m_objPdf->AddPage();
			$this->m_intY = 0;
		}
		// APPROVED SIGNER

		$strApprovedSignerName 			= 'APPROVED_SIGNER_NAME';
		$strApprovedPoliticallyExposed	= 'APPROVED_SIGNER_POLITICALLY_EXPOSED';
		$strApprovedSignerTitle			= 'APPROVED_SIGNER_TITLE';
		$strApprovedSignerTin			= 'APPROVED_SIGNER_TIN';
		$strApprovedSignerAddress		= 'APPROVED_SIGNER_ADDRESS';
		$strApprovedSignerCity			= 'APPROVED_SIGNER_CITY';
		$strApprovedSignerStateCode 	= 'APPROVED_SIGNER_STATE_CODE';
		$strApprovedSignerPostalCode 	= 'APPROVED_SIGNER_POSTAL_CODE';
		$strApprovedSignerPhoneNumber 	= 'APPROVED_SIGNER_PHONE_NUMBER';
		$strApprovedSignerEmailAddress 	= 'APPROVED_SIGNER_EMAIL_ADDRESS';

		$this->m_objPdf->Rect( 30, $this->m_intY += 40, 550, 120 );
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 300, $this->m_intY += 15 );
		$this->m_objPdf->Cell( 15, 10, 'Authorized Signer Information', '', 1, 'C', 1 );

		$this->m_intX = 35;
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Name :', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerName] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerName]->getValue();
			$this->m_objPdf->SetXY( $this->m_intX + 35, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( $this->m_intX + 35, $this->m_intY + 10, $this->m_intX + 235, $this->m_intY + 10 );

		$this->m_intX = 275;
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Title :', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerTitle] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerTitle]->getValue();
			$this->m_objPdf->SetXY( $this->m_intX + 30, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( $this->m_intX + 30, $this->m_intY + 10, $this->m_intX + 152, $this->m_intY + 10 );

		$this->m_intX = 430;
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'TIN/SSN :', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerTin] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerTin]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( $this->m_intX + 45, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( $this->m_intX + 45, $this->m_intY + 10, $this->m_intX + 125, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Politically Exposed Person [PEP] :', '', 1, 'L', 1 );

		$strTemp = 'No';
		if( true == isset( $arrobjApplicationKeyValues[$strApprovedPoliticallyExposed] ) ) {
			$strTemp = ( '1' == $arrobjApplicationKeyValues[$strApprovedPoliticallyExposed]->getValue() ) ? 'Yes' : 'No';
		}
		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( 190, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Address : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerAddress] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerAddress]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			if( true == $this->longString( $strTemp ) ) {
				$this->m_objPdf->SetFontSize( 7 );
			}
			$this->m_objPdf->setXY( 35 + 60, $this->m_intY );
			$this->m_objPdf->Cell( 60, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 35 + 60, $this->m_intY + 10, 35 + 280, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 320, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'City : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerCity] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerCity]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			if( true == $this->longString( $strTemp ) ) {
				$this->m_objPdf->SetFontSize( 7 );
			}
			$this->m_objPdf->SetXY( 320 + 25, $this->m_intY );
			$this->m_objPdf->Cell( 50, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 320 + 25, $this->m_intY + 10, 320 + 150, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 475, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'State : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerStateCode] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerStateCode]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 475 + 30, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 475 + 30, $this->m_intY + 10, 475 + 80, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Zip : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerPostalCode] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerPostalCode]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 35 + 30, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 35 + 30, $this->m_intY + 10, 35 + 110, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 305, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Phone Number:', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerPhoneNumber] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerPhoneNumber]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 305 + 70, $this->m_intY );
			$this->m_objPdf->Cell( 15, 9, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 305 + 70, $this->m_intY + 10, 305 + 265, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Email Address:', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerEmailAddress] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerEmailAddress]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			if( true == $this->longString( $strTemp ) ) {
				$this->m_objPdf->SetFontSize( 7 );
			}
			$this->m_objPdf->SetXY( 35 + 70, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 35 + 70, $this->m_intY + 10, 35 + 365, $this->m_intY + 10 );

		// END OF APPROVED SIGNER
		if( true == $boolAddAdditionalPage ) {
			$this->m_objPdf->AddPage();
			$this->m_intY = 0;
		}

		// Invoice information
		$this->m_objPdf->Rect( 30, $this->m_intY += 30, 550, 120 );

		// title
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 300, $this->m_intY += 5 );
		$this->m_objPdf->Cell( 15, 20, 'Invoice and Bank Account Information', '', 1, 'C', 1 );

		// ROW 1
		$this->m_intX = 35; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 7 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 110, 10, 'Max Daily Invoice Amount:', '', 1, 'L', 1 );

		$strTemp = $arrobjApplicationKeyValues['MAX_DAILY_AMOUNT']->getValue();
		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 95, $this->m_intY );
		$this->m_objPdf->Cell( 110, 10, $strTemp, '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 95, $this->m_intY + 10, $this->m_intX + 260, $this->m_intY + 10 );

		$this->m_intX = 305; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 7 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 105, 10, 'Monthly Invoice Amount:', '', 1, 'L', 1 );

		$strTemp = $arrobjApplicationKeyValues['MONTHLY_INVOICE_AMOUNT']->getValue();
		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 87, $this->m_intY );
		$this->m_objPdf->Cell( 60, 10, $strTemp, '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 87, $this->m_intY + 10, $this->m_intX + 255, $this->m_intY + 10 );

		// ROW 2
		// Daily Invoice
		$this->m_intX = 35; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 7 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 110, 10, 'Avg. Daily Invoice Volume (WK1: 1st-7th):', '', 1, 'L', 1 );

		$strTemp = $arrobjApplicationKeyValues['AVG_DAILY_INVOICE_VOLUME_1']->getValue();
		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 143, $this->m_intY );
		$this->m_objPdf->Cell( 110, 10, $strTemp, '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 143, $this->m_intY + 10, $this->m_intX + 260, $this->m_intY + 10 );

		// Daily Account
		$this->m_intX = 305; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 7 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 60, 10, 'Avg. Daily Account Balance: (WK1: 1st-7th):', '', 1, 'L', 1 );

		$strTemp = $arrobjApplicationKeyValues['AVG_DAILY_ACCOUNT_BALANCE_1']->getValue();
		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 157, $this->m_intY );
		$this->m_objPdf->Cell( 60, 10, $strTemp, '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 157, $this->m_intY + 10, $this->m_intX + 255, $this->m_intY + 10 );

		// ROW  3
		// Daily Invoice
		$this->m_intX = 35; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 7 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 110, 10, 'Avg. Daily Invoice Volume (WK2: 8th-14th):', '', 1, 'L', 1 );

		$strTemp = $arrobjApplicationKeyValues['AVG_DAILY_INVOICE_VOLUME_2']->getValue();
		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 147, $this->m_intY );
		$this->m_objPdf->Cell( 110, 10, $strTemp, '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 147, $this->m_intY + 8, $this->m_intX + 260, $this->m_intY + 8 );

		// Daily Account
		$this->m_intX = 305; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 7 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 60, 10, 'Avg. Daily Account Balance (WK2: 8th-14th):', '', 1, 'L', 1 );

		$strTemp = $arrobjApplicationKeyValues['AVG_DAILY_ACCOUNT_BALANCE_2']->getValue();
		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 157, $this->m_intY );
		$this->m_objPdf->Cell( 110, 10, $strTemp, '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 157, $this->m_intY + 10, $this->m_intX + 255, $this->m_intY + 10 );

		// ROW  4
		// Daily Invoice
		$this->m_intX = 35; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 7 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 110, 10, 'Avg. Daily Invoice Volume (WK3: 15th-21st):', '', 1, 'L', 1 );

		$strTemp = $arrobjApplicationKeyValues['AVG_DAILY_INVOICE_VOLUME_3']->getValue();
		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 150, $this->m_intY );
		$this->m_objPdf->Cell( 100, 10, $strTemp, '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 150, $this->m_intY + 10, $this->m_intX + 260, $this->m_intY + 10 );

		// Daily Account
		$this->m_intX = 305; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 7 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 110, 10, 'Avg. Daily Account Balance (WK3: 15th-21st):', '', 1, 'L', 1 );

		$strTemp = $arrobjApplicationKeyValues['AVG_DAILY_ACCOUNT_BALANCE_3']->getValue();
		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 157, $this->m_intY );
		$this->m_objPdf->Cell( 100, 10, $strTemp, '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 157, $this->m_intY + 10, $this->m_intX + 255, $this->m_intY + 10 );

		// ROW  5
		// Daily Invoice
		$this->m_intX = 35; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 7 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 110, 10, 'Avg. Daily Invoice Volume (WK4: 22nd-EOM):', '', 1, 'L', 1 );

		$strTemp = $arrobjApplicationKeyValues['AVG_DAILY_INVOICE_VOLUME_4']->getValue();
		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 160, $this->m_intY );
		$this->m_objPdf->Cell( 100, 10, $strTemp, '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 160, $this->m_intY + 10, $this->m_intX + 260, $this->m_intY + 10 );

		// Daily Account
		$this->m_intX = 305; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 7 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 110, 10, 'Avg. Daily Account Balance (WK4: 22nd-EOM):', '', 1, 'L', 1 );

		$strTemp = $arrobjApplicationKeyValues['AVG_DAILY_ACCOUNT_BALANCE_4']->getValue();
		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 160, $this->m_intY );
		$this->m_objPdf->Cell( 100, 10, $strTemp, '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 160, $this->m_intY + 10, $this->m_intX + 255, $this->m_intY + 10 );

		// note
		$this->m_intX = 325; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 8 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 25 );
		$this->m_objPdf->Cell( 15, 10, 'Submit the Following Documents with your Application', '', 1, 'C', 1 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 8 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 10 );
		$this->m_objPdf->setTextColor( 222, 71, 40 );
		$this->m_objPdf->Cell( 15, 10, '(All business documents must show the same business name)', '', 1, 'C', 1 );

		// Required documents section
		$this->m_objPdf->Rect( 30, $this->m_intY += 15, 550, 75 );

		$this->m_objPdf->Image( PATH_COMMON . 'images/checkbox.png', 35, $this->m_intY += 5, '', 6 );
		$this->m_objPdf->SetFont( 'Arial', '', 8 );
		$this->m_objPdf->SetXY( 50, $this->m_intY );
		$this->m_objPdf->setTextColor( 0, 0, 0 );
		$this->m_objPdf->Cell( 15, 9, 'Copy of Application signer\'s photo id with signature. (Drivers License or Passport)', '', 1, 'L', 1 );

		$this->m_objPdf->Image( PATH_COMMON . 'images/checkbox.png', 35, $this->m_intY += 15, '', 6 );
		$this->m_objPdf->SetFont( 'Arial', '', 8 );
		$this->m_objPdf->SetXY( 50, $this->m_intY );
		$this->m_objPdf->setTextColor( 0, 0, 0 );
		$this->m_objPdf->Cell( 15, 9, 'Copy of one Fee Management Agreement (if entity fee manages)', '', 1, 'L', 1 );

		$this->m_objPdf->Image( PATH_COMMON . 'images/checkbox.png', 35, $this->m_intY += 15, '', 6 );
		$this->m_objPdf->SetFont( 'Arial', '', 8 );
		$this->m_objPdf->SetXY( 50, $this->m_intY );
		$this->m_objPdf->setTextColor( 0, 0, 0 );
		$this->m_objPdf->Cell( 15, 9, 'Copy of recent bank statements (3 months worth) of Bank account where Bills will be paid', '', 1, 'L', 1 );

		$this->m_objPdf->Image( PATH_COMMON . 'images/checkbox.png', 35, $this->m_intY += 15, '', 6 );
		$this->m_objPdf->SetFont( 'Arial', '', 8 );
		$this->m_objPdf->SetXY( 50, $this->m_intY );
		$this->m_objPdf->setTextColor( 0, 0, 0 );
		$this->m_objPdf->Cell( 15, 9, 'Copy of document showing business formation (Incorporation, LLC, Partnership etc.)', '', 1, 'L', 1 );

		$this->m_objPdf->Image( PATH_COMMON . 'images/checkbox.png', 35, $this->m_intY += 15, '', 6 );
		$this->m_objPdf->SetFont( 'Arial', '', 8 );
		$this->m_objPdf->SetXY( 50, $this->m_intY );
		$this->m_objPdf->setTextColor( 0, 0, 0 );
		$this->m_objPdf->Cell( 15, 9, 'Copy of document showing the legal name and tax id# as filed with IRS (EIN notification or top page of Tax return)', '', 1, 'L', 1 );

		// Signing area - for agreement

		$this->m_objPdf->SetFont( 'Arial', 'B', 10 );
		$this->m_objPdf->setXY( 290, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Agreement', '', 1, 'C', 1 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 6 );
		$this->m_objPdf->setXY( 35, $this->m_intY += 20 );

		$strTemp = 'By signing below, the signatory (i) certifies that to the best of his/her knowledge, all information and documents submitted in connection with this application are true and accurate; and (i) agrees that Entrata has permission to run a credit report on the Guarantor as part of the KYC (know your customer) requirement of the underwriting process. The term "Guarantor" as used herein shall mean the signatory below, in his/her personal capacity, if executed as a personal guarantee; if executed as an entity guarantee, "Guarantor" shall mean the entity on whose behalf the signatory signs. Guarantor acknowledges and agrees that by signing below, Guarantor is guaranteeing the prompt, full, and complete performance of any and all present and future duties, obligations, and indebtedness due to Entrata in connection with this merchant application, as specified in any applicable Statement of Work and the ResidentPay' . urldecode( '%AE' ) . '  terms and conditions attached to the previously executed License Agreement, or incorporated by reference therein. This guarantee shall be construed exclusively in accordance with, and governed by, the laws of the State of Utah, and any dispute arising hereunder may only be brought within the state or federal courts of the State of Utah.';

		$this->m_objPdf->MultiCell( 535, 8, $strTemp );

		$this->m_objPdf->setTextColor( 0, 0, 0 ); // back to black
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->Line( 50, $this->m_intY += 80, 200, $this->m_intY );
		$this->m_objPdf->Line( 250, $this->m_intY, 400, $this->m_intY );
		$this->m_objPdf->Line( 460, $this->m_intY, 550, $this->m_intY );

		$this->m_objPdf->setXY( 125, $this->m_intY += 10 );
		$this->m_objPdf->Cell( 15, 10, 'Authorized Signer (printed)', '', 1, 'C', 1 );

		$strTemp = '';
		if( true == isset( $arrobjApplicationKeyValues['ESIGN_NAME'] ) ) {
			$strTemp = $arrobjApplicationKeyValues['ESIGN_NAME']->getValue();
		}
		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( 50, $this->m_intY - 25 );
		$this->m_objPdf->Cell( 100, 10, $strTemp, '', 1, 'L', 1 );

		$this->m_objPdf->setXY( 325, $this->m_intY );
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->Cell( 15, 10, 'Authorized Signer (signature)', '', 1, 'C', 1 );

		// $strFontName = 'AguafinaScript-Regular';
		// $this->m_objPdf->AddFont( $strFontName, '', $strFontName . '.php' );
		// $this->m_objPdf->SetFont( $strFontName, '', 14 );
		$this->m_objPdf->SetXY( 250, $this->m_intY - 25 );
		$this->m_objPdf->Cell( 100, 10, $strTemp, '', 1, 'L', 1 );

		$this->m_objPdf->setXY( 500, $this->m_intY );
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->Cell( 15, 10, 'Date', '', 1, 'C', 1 );

		// Code to generate additional authorized signatories page

		return $this->m_objPdf;

	}

	// This form generated from application form AND the client admin as requested

	public function generateMerchantAccountApplicationModifiedPdf( $arrobjApplicationKeyValues, $objClient ) {
		$strTemp = '';
		$this->m_objPdf = new CFpdf( 'P', 'pt', 'letter' );

		$this->m_objPdf->SetAuthor( CONFIG_COMPANY_NAME );
		$this->m_objPdf->SetCreator( CONFIG_COMPANY_NAME );
		$this->m_objPdf->SetTitle( 'Merchant Account Application' );

		$this->m_objPdf->SetXY( 300, 840 );
		$this->m_objPdf->SetFont( 'Arial', 'BU', 9 );
		$this->m_objPdf->setFillColor( 255, 255, 255 );
		$this->m_objPdf->Cell( 10, 30, 'MERCHANT APPLICATION', '', 1, 'C', 1 );

		$this->m_objPdf->Rect( 30, 60, 550, 200 );
		$this->m_objPdf->SetXY( 40, 65 );
		$this->m_objPdf->Cell( 520, 20, 'Business Information', '', 1, 'C', 1 );

		$this->m_intY = 65;

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Legal Name Of Business: ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( 150, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getCompanyName(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( 150, $this->m_intY + 10, 350, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 370, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Federal Tax ID#: ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( 445, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getCompanyTaxNumber(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( 447, $this->m_intY + 10, 570, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Doing Business As (dba): ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( 150, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getCompanyDbaName(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( 150, $this->m_intY + 10, 430, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 420, $this->m_intY );
		$this->m_objPdf->Cell( 15, 15, 'Years in Business: ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( 510, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getAgeOfBusiness(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( 515, $this->m_intY + 10, 570, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Business Address (Storefront) : ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		if( true == $this->longString( $this->getCompanyStreetLine1() ) ) {
			$this->m_objPdf->SetFontSize( 7 );
		}
		$this->m_objPdf->SetXY( 175, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getCompanyStreetLine1(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( 180, $this->m_intY + 10, 370, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 370, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'City : ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		if( true == $this->longString( $this->getCompanyCity() ) ) {
			$this->m_objPdf->SetFontSize( 7 );
		}
		$this->m_objPdf->SetXY( 395, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getCompanyCity(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( 397, $this->m_intY + 10, 495, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 500, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'State : ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( 500 + 32, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getCompanyStateCode(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( 500 + 32, $this->m_intY + 10, 500 + 62, $this->m_intY + 10 );

		$this->m_intX = 35;
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Country : ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 50, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getCompanyCountryCode(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 50, $this->m_intY + 10, $this->m_intX + 80, $this->m_intY + 10 );

		$this->m_intX = 125;
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Zip : ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 25, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getCompanyPostalCode(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 27, $this->m_intY + 10, $this->m_intX + 70, $this->m_intY + 10 );

		$this->m_intX = 210;
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Phone Number : ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 72, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getCompanyPhoneNumber(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 72, $this->m_intY + 10, $this->m_intX + 140, $this->m_intY + 10 );

		$this->m_intX = 400;
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Website : ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		if( true == $this->longString( $this->getWebsiteurl() ) ) {
			$this->m_objPdf->SetFontSize( 7 );
		}
		$this->m_objPdf->SetXY( $this->m_intX + 45, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getWebsiteurl(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( $this->m_intX + 48, $this->m_intY + 10, $this->m_intX + 160, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Number of Properties : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues['NUMBER_OF_PROPERTIES'] ) ) {
			$strTemp = $arrobjApplicationKeyValues['NUMBER_OF_PROPERTIES']->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 140, $this->m_intY );
			$this->m_objPdf->Cell( 35, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 142, $this->m_intY + 10, 170, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 180, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Number of Units : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues['NUMBER_OF_UNITS'] ) ) {
			$strTemp = $arrobjApplicationKeyValues['NUMBER_OF_UNITS']->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 260, $this->m_intY );
			$this->m_objPdf->Cell( 35, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 262, $this->m_intY + 10, 290, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 300, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Avg Rent Payment $ ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues['AVERAGE_RENT_PAYMENT'] ) ) {
			$strTemp = $arrobjApplicationKeyValues['AVERAGE_RENT_PAYMENT']->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 400, $this->m_intY );
			$this->m_objPdf->Cell( 35, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 400, $this->m_intY + 10, 430, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 440, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Max Rent Payment $ ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues['MAXIMUM_RENT_PAYMENT'] ) ) {
			$strTemp = $arrobjApplicationKeyValues['MAXIMUM_RENT_PAYMENT']->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 540, $this->m_intY );
			$this->m_objPdf->Cell( 35, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 540, $this->m_intY + 10, 570, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Contact Person : ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( 110, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getContactName(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( 112, $this->m_intY + 10, 320, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 350, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Type of Ownership : ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Contact Person\'s Email : ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		if( true == $this->longString( $this->getContactEmail() ) ) {
			$this->m_objPdf->SetFontSize( 7 );
		}
		$this->m_objPdf->SetXY( 145, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getContactEmail(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( 147, $this->m_intY + 10, 320, $this->m_intY + 10 );

		$this->m_objPdf->SetXY( 350, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Sole Proprietor ', '', 1, 'L', 1 );
		if( 1 == $this->getOwnershipTypeId() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', 420, $this->m_intY );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', 420, $this->m_intY );
		}

		$this->m_objPdf->SetXY( 450, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Partnership ', '', 1, 'L', 1 );
		if( 2 == $this->getOwnershipTypeId() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', 510, $this->m_intY );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', 510, $this->m_intY );
		}

		$this->m_intY1 = 215;
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Contact Person\'s Phone # : ', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( 155, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $this->getContactDayTimePhone(), '', 1, 'L', 1 );
		$this->m_objPdf->Line( 157, $this->m_intY + 10, 320, $this->m_intY + 10 );

		$this->m_objPdf->SetXY( 350, $this->m_intY1 );
		$this->m_objPdf->Cell( 15, 10, 'LLC ', '', 1, 'L', 1 );
		if( 3 == $this->getOwnershipTypeId() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', 420, $this->m_intY1 );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', 420, $this->m_intY1 );
		}

		$this->m_objPdf->SetXY( 450, $this->m_intY1 );
		$this->m_objPdf->Cell( 15, 10, 'Corporation ', '', 1, 'L', 1 );
		if( 4 == $this->getOwnershipTypeId() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', 510, $this->m_intY1 );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', 510, $this->m_intY1 );
		}

		$this->m_objPdf->SetXY( 350, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Tax Exempt ', '', 1, 'L', 1 );
		if( 5 == $this->getOwnershipTypeId() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', 420, $this->m_intY );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', 420, $this->m_intY );
		}

		// Publicly Traded
		$this->m_intX = 35; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 40, 10, 'Publicly Traded: ', '', 1, 'L', 1 );

		$this->m_intX += 75;
		if( true == isset( $arrobjApplicationKeyValues['PUBLICLY_TRADED'] ) && 1 == $arrobjApplicationKeyValues['PUBLICLY_TRADED']->getValue() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', $this->m_intX, $this->m_intY );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', $this->m_intX, $this->m_intY );
		}

		$this->m_intX += 20; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Exchange: ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues['PUBLICLY_TRADED_EXCHANGE'] ) ) {
			$strTemp = $arrobjApplicationKeyValues['PUBLICLY_TRADED_EXCHANGE']->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( $this->m_intX + 55, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( $this->m_intX + 55, $this->m_intY + 10, $this->m_intX + 100, $this->m_intY + 10 );

		$this->m_intX += 110; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Exchange Symbol: ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues['PUBLICLY_TRADED_SYMBOL'] ) ) {
			$strTemp = $arrobjApplicationKeyValues['PUBLICLY_TRADED_SYMBOL']->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( $this->m_intX + 90, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( $this->m_intX + 90, $this->m_intY + 10, $this->m_intX + 150, $this->m_intY + 10 );

		$this->m_intX = 420; // change this to adjust following fields
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, '# of Owners : ', '', 1, 'L', 1 );

		$strTemp = $this->getNumberOfOwners();
		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( $this->m_intX + 80, $this->m_intY );
		$this->m_objPdf->Cell( 35, 10, $strTemp, '', 1, 'L', 1 );

		$this->m_objPdf->Line( $this->m_intX + 80, $this->m_intY + 10, $this->m_intX + 135, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 325, $this->m_intY += 25 );
		$this->m_objPdf->Cell( 15, 10, 'ALL Documents and Officer Information below is required to ensure compliance with the', '', 1, 'C', 1 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 325, $this->m_intY += 10 );
		$this->m_objPdf->Cell( 15, 10, 'USA PATRIOT Act, and bank and credit card underwriting standards', '', 1, 'C', 1 );

		$intCntOfOwner = $this->getNumberOfOwners();
		$intCounter = 0;
		for( $intLoop = 0; $intLoop < $intCntOfOwner; $intLoop++ ) {
			if( $intCounter == 2 ) {
				$this->m_objPdf->AddPage();
				$this->m_intY = 0;
			}

			if( 0 == $intLoop ) {
				$strLoop = '';
			} else {
				$strLoop = $intLoop;
			}

			$this->addBeneficialOwner( $arrobjApplicationKeyValues, $strLoop );

			$intCounter++;
		}

		if( false == empty( $arrobjApplicationKeyValues['OFFICER_NAME4'] ) ) {
			if( 0 == $intCounter % 2 ) {
				$this->m_objPdf->AddPage();
				$this->m_intY = 0;
			}
			$this->addBeneficialOwner( $arrobjApplicationKeyValues, '4' );
			$intCounter++;
		}

		switch( $intCounter ) {

			case 2:
				$boolAddAdditionalPage = false;
				break;

			case 3:
				$this->m_intY = 130;
				$boolAddAdditionalPage = true;
				break;

			case 4:
				$this->m_intY = 280;
				$boolAddAdditionalPage = true;
				break;

			default:
				$boolAddAdditionalPage = true;
				break;
		}

		// Guarantor
		$strGuarantorOption			= 'OPTION';
		$strGuarantorName 			= 'GUARANTOR_NAME';
		$strGuarantorTin			= 'GUARANTOR_TIN';
		$strGuarantorPep            = 'GUARANTOR_POLITICALLY_EXPOSED';
		$strGuarantorAddress		= 'GUARANTOR_ADDRESS';
		$strGuarantorCity			= 'GUARANTOR_CITY';
		$strGuarantorStateCode 		= 'GUARANTOR_STATE_CODE';
		$strGuarantorCountryCode	= 'GUARANTOR_COUNTRY_CODE';
		$strGuarantorPostalCode 	= 'GUARANTOR_POSTAL_CODE';
		$strGuarantorPhoneNumber 	= 'GUARANTOR_PHONE_NUMBER';
		$strGuarantorEmailAddress 	= 'GUARANTOR_EMAIL_ADDRESS';

		if( 2 == $intCounter ) {
			$this->m_objPdf->AddPage();
			$this->m_intY = 0;
		}
		$this->m_objPdf->Rect( 30, $this->m_intY += 30, 550, 130 );
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 300, $this->m_intY += 8 );
		$this->m_objPdf->Cell( 15, 10, 'Guarantor Information', '', 1, 'C', 1 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Name :', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorName] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorName]->getValue();
			$this->m_objPdf->SetXY( 70, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 70, $this->m_intY + 10, 300, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 305, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'TIN/SSN :', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorTin] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorTin]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 305 + 40, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 305 + 40, $this->m_intY + 10, 430, $this->m_intY + 10 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorOption] ) && '1' == $arrobjApplicationKeyValues[$strGuarantorOption]->getValue() ) {
			$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
			$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
			$this->m_objPdf->Cell( 15, 10, 'Politically Exposed Person[PEP] :', '', 1, 'L', 1 );

			$strTemp = 'No';
			if( true == isset( $arrobjApplicationKeyValues[$strGuarantorPep] ) ) {
				$strTemp = $strTemp = ( '1' == $arrobjApplicationKeyValues[$strGuarantorPep]->getValue() ) ? 'Yes' : 'No';
			}
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 190, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Address : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorAddress] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorAddress]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			if( true == $this->longString( $strTemp ) ) {
				$this->m_objPdf->SetFontSize( 7 );
			}
			$this->m_objPdf->setXY( 35 + 60, $this->m_intY );
			$this->m_objPdf->Cell( 60, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 35 + 60, $this->m_intY + 10, 35 + 280, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 320, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'City : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorCity] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorCity]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			if( true == $this->longString( $strTemp ) ) {
				$this->m_objPdf->SetFontSize( 7 );
			}
			$this->m_objPdf->SetXY( 320 + 25, $this->m_intY );
			$this->m_objPdf->Cell( 50, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 320 + 25, $this->m_intY + 10, 320 + 150, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 475, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'State : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorStateCode] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorStateCode]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 475 + 30, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 475 + 30, $this->m_intY + 10, 475 + 80, $this->m_intY + 10 );

		$this->m_intX = 35;
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Country : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorCountryCode] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorCountryCode]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( $this->m_intX + 50, $this->m_intY );
			$this->m_objPdf->Cell( 35, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( $this->m_intX + 50, $this->m_intY + 10, $this->m_intX + 80, $this->m_intY + 10 );

		$this->m_intX = 125;
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Zip : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorPostalCode] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorPostalCode]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( $this->m_intX + 30, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( $this->m_intX + 30, $this->m_intY + 10, $this->m_intX + 110, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 305, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Phone Number:', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorPhoneNumber] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorPhoneNumber]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 305 + 70, $this->m_intY );
			$this->m_objPdf->Cell( 15, 9, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 305 + 70, $this->m_intY + 10, 305 + 265, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Email Address:', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strGuarantorEmailAddress] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strGuarantorEmailAddress]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			if( true == $this->longString( $strTemp ) ) {
				$this->m_objPdf->SetFontSize( 7 );
			}
			$this->m_objPdf->SetXY( 35 + 70, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 35 + 70, $this->m_intY + 10, 35 + 365, $this->m_intY + 10 );

		// END OF GUARANTOR

		// APPROVED SIGNER

		$strApprovedSignerName 			= 'APPROVED_SIGNER_NAME';
		$strApprovedSignerTin			= 'APPROVED_SIGNER_TIN';
		$strApprovedPoliticallyExposed	= 'APPROVED_SIGNER_POLITICALLY_EXPOSED';
		$strApprovedSignerAddress		= 'APPROVED_SIGNER_ADDRESS';
		$strApprovedSignerCity			= 'APPROVED_SIGNER_CITY';
		$strApprovedSignerStateCode 	= 'APPROVED_SIGNER_STATE_CODE';
		$strApprovedSignerCountryCode 	= 'APPROVED_SIGNER_COUNTRY_CODE';
		$strApprovedSignerPostalCode 	= 'APPROVED_SIGNER_POSTAL_CODE';
		$strApprovedSignerPhoneNumber 	= 'APPROVED_SIGNER_PHONE_NUMBER';
		$strApprovedSignerEmailAddress 	= 'APPROVED_SIGNER_EMAIL_ADDRESS';

		$this->m_objPdf->Rect( 30, $this->m_intY += 40, 550, 160 );
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 300, $this->m_intY += 15 );
		$this->m_objPdf->Cell( 15, 10, 'Authorized Signer Information', '', 1, 'C', 1 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Name :', '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerName] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerName]->getValue();
			$this->m_objPdf->SetXY( 70, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 70, $this->m_intY + 10, 300, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 305, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'TIN/SSN :', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerTin] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerTin]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 305 + 40, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 305 + 40, $this->m_intY + 10, 430, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Politically exposed person[PEP]  :', '', 1, 'L', 1 );

		$strTemp = 'No';
		if( true == isset( $arrobjApplicationKeyValues[$strApprovedPoliticallyExposed] ) ) {
			$strTemp = ( '1' == $arrobjApplicationKeyValues[$strApprovedPoliticallyExposed]->getValue() ) ? 'Yes' : 'No';
		}
		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->SetXY( 190, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Address : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerAddress] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerAddress]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			if( true == $this->longString( $strTemp ) ) {
				$this->m_objPdf->SetFontSize( 7 );
			}
			$this->m_objPdf->setXY( 35 + 60, $this->m_intY );
			$this->m_objPdf->Cell( 60, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 35 + 60, $this->m_intY + 10, 35 + 280, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 320, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'City : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerCity] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerCity]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			if( true == $this->longString( $strTemp ) ) {
				$this->m_objPdf->SetFontSize( 7 );
			}
			$this->m_objPdf->SetXY( 320 + 25, $this->m_intY );
			$this->m_objPdf->Cell( 50, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 320 + 25, $this->m_intY + 10, 320 + 150, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 475, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'State : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerStateCode] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerStateCode]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 475 + 30, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 475 + 30, $this->m_intY + 10, 475 + 80, $this->m_intY + 10 );

		$this->m_intX = 35;
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Country : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerCountryCode] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerCountryCode]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( $this->m_intX + 50, $this->m_intY );
			$this->m_objPdf->Cell( 35, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( $this->m_intX + 50, $this->m_intY + 10, $this->m_intX + 80, $this->m_intY + 10 );

		$this->m_intX = 125;
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Zip : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerPostalCode] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerPostalCode]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( $this->m_intX + 30, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( $this->m_intX + 30, $this->m_intY + 10, $this->m_intX + 110, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 305, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Phone Number:', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerPhoneNumber] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerPhoneNumber]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 305 + 70, $this->m_intY );
			$this->m_objPdf->Cell( 15, 9, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 305 + 70, $this->m_intY + 10, 305 + 265, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Email Address:', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerEmailAddress] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerEmailAddress]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			if( true == $this->longString( $strTemp ) ) {
				$this->m_objPdf->SetFontSize( 7 );
			}
			$this->m_objPdf->SetXY( 35 + 70, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 35 + 70, $this->m_intY + 10, 55 + 365, $this->m_intY + 10 );

		// END OF APPROVED SIGNER

		$this->m_intYAxis = $this->m_intY;
		if( true == $boolAddAdditionalPage ) {
			$this->m_objPdf->AddPage();
			$this->m_intYAxis = 0;
		}

		$this->m_objPdf->Rect( 30, $this->m_intYAxis += 80, 550, 200 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 10 );
		$this->m_objPdf->setXY( 290, $this->m_intYAxis += 10 );
		$this->m_objPdf->Cell( 15, 10, 'Submit the Following Documents with your Application', '', 1, 'C', 1 );
		$this->m_objPdf->setXY( 300, $this->m_intYAxis += 10 );
		$this->m_objPdf->Cell( 15, 10, 'ALL documents are required and ALL business documents must show the same business name', '', 1, 'C', 1 );

		$this->m_objPdf->setXY( 60, $this->m_intYAxis += 30 );
		$this->m_objPdf->Image( PATH_COMMON . 'images/checkbox.png', 50, $this->m_intYAxis, '', 6 );
		$this->m_objPdf->SetFont( 'Arial', '', 8 );
		$this->m_objPdf->Cell( 15, 10, 'Copy of Application signer\'s photo id with signature. ( Drivers License or Passport )', '', 1, 'L', 1 );

		$this->m_objPdf->setXY( 60, $this->m_intYAxis += 20 );
		$this->m_objPdf->Image( PATH_COMMON . 'images/checkbox.png', 50, $this->m_intYAxis, '', 6 );
		$this->m_objPdf->SetFont( 'Arial', '', 8 );
		$this->m_objPdf->Cell( 15, 10, 'Copy of document showing business formation ( Incorporation, LLC, Partnership etc. )', '', 1, 'L', 1 );

		$this->m_objPdf->setXY( 60, $this->m_intYAxis += 20 );
		$this->m_objPdf->Image( PATH_COMMON . 'images/checkbox.png', 50, $this->m_intYAxis, '', 6 );
		$this->m_objPdf->SetFont( 'Arial', '', 8 );
		$this->m_objPdf->Cell( 15, 10, 'Submit a copy of a recent bank statement ( for the Applying Entity )', '', 1, 'L', 1 );

		$this->m_objPdf->setXY( 60, $this->m_intYAxis += 20 );
		$this->m_objPdf->Image( PATH_COMMON . 'images/checkbox.png', 50, $this->m_intYAxis, '', 6 );
		$this->m_objPdf->SetFont( 'Arial', '', 8 );
		$this->m_objPdf->Cell( 15, 10, 'Copy of document showing the legal name and tax id# as filed with IRS ( EIN notification or top page of Tax return )', '', 1, 'L', 1 );

		$this->m_objPdf->setXY( 60, $this->m_intYAxis += 20 );
		$this->m_objPdf->Image( PATH_COMMON . 'images/checkbox.png', 50, $this->m_intYAxis, '', 6 );
		$this->m_objPdf->SetFont( 'Arial', '', 8 );
		$this->m_objPdf->Cell( 15, 10, 'Copy of one Fee Management Agreement (if entity fee manages)', '', 1, 'L', 1 );

		$this->m_objPdf->setXY( 60, $this->m_intYAxis += 20 );
		$this->m_objPdf->Image( PATH_COMMON . 'images/checkbox.png', 50, $this->m_intYAxis, '', 6 );
		$this->m_objPdf->SetFont( 'Arial', '', 8 );
		$this->m_objPdf->Cell( 15, 10, 'Last 3 months credit card processing statements (if entity has processed credit card payments)', '', 1, 'L', 1 );

		$this->m_objPdf->Rect( 30, $this->m_intYAxis += 70, 550, 150 );
		$this->m_objPdf->SetFont( 'Arial', 'B', 10 );
		$this->m_objPdf->setXY( 290, $this->m_intYAxis += 10 );
		$this->m_objPdf->Cell( 15, 10, 'Agreement', '', 1, 'C', 1 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 6 );
		$this->m_objPdf->setXY( 35, $this->m_intYAxis += 20 );

		$strTemp = 'By signing below, the signatory (i) certifies that to the best of his/her knowledge, all information and documents submitted in connection with this application are true and accurate; and (i) agrees that Entrata has permission to run a credit report on the Guarantor as part of the KYC (know your customer) requirement of the underwriting process. The term "Guarantor" as used herein shall mean the signatory below, in his/her personal capacity, if executed as a personal guarantee; if executed as an entity guarantee, "Guarantor" shall mean the entity on whose behalf the signatory signs. Guarantor acknowledges and agrees that by signing below, Guarantor is guaranteeing the prompt, full, and complete performance of any and all present and future duties, obligations, and indebtedness due to Entrata in connection with this merchant application, as specified in any applicable Statement of Work and the ResidentPay' . urldecode( '%AE' ) . '  terms and conditions attached to the previously executed License Agreement, or incorporated by reference therein. This guarantee shall be construed exclusively in accordance with, and governed by, the laws of the State of Utah, and any dispute arising hereunder may only be brought within the state or federal courts of the State of Utah.';

		$this->m_objPdf->MultiCell( 535, 8, $strTemp );

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		$this->m_objPdf->setXY( 35, $this->m_intYAxis += 20 );

		// draw three signature lines
		$this->m_objPdf->Line( 50, $this->m_intYAxis += 65, 200, $this->m_intYAxis );
		$this->m_objPdf->Line( 250, $this->m_intYAxis, 400, $this->m_intYAxis );
		$this->m_objPdf->Line( 460, $this->m_intYAxis, 550, $this->m_intYAxis );

		// auto fill first line with officer name if it exists
		$strOfficerPosition = $this->m_intYAxis - 20;

		if( true == isset( $arrobjApplicationKeyValues[$strApprovedSignerName] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strApprovedSignerName]->getValue();
			$this->m_objPdf->setXY( 120, $strOfficerPosition );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'C', 1 );
		}

		$this->m_objPdf->setXY( 120, $this->m_intYAxis += 10 );
		$this->m_objPdf->Cell( 15, 10, 'Authorized Signer (printed)', '', 1, 'C', 1 );

		$this->m_objPdf->setXY( 325, $this->m_intYAxis );
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->Cell( 15, 10, 'Authorized Signer (signature)', '', 1, 'C', 1 );

		$this->m_objPdf->setXY( 500, $this->m_intYAxis );
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->Cell( 15, 10, 'Date', '', 1, 'C', 1 );

		// Code to generate additional authorized signatories page
		$this->m_objPdf = \Psi\Eos\Payment\CMerchantAccountApplications::createService()->generateAdditionalAuthorizedSignatoriesEmail( $objClient, $this, $this->m_objPdf, $arrobjApplicationKeyValues );

		return $this->m_objPdf;
	}

	private function addBeneficialOwner( $arrobjApplicationKeyValues, $strLoop ) {

		$this->m_objPdf->Rect( 30, $this->m_intY += 10, 550, 140 );
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 300, $this->m_intY += 5 );
		$this->m_objPdf->Cell( 15, 10, 'Beneficial Owner', '', 1, 'C', 1 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Name :', '', 1, 'L', 1 );

		$strOfficeName 				    = 'OFFICER_NAME' . $strLoop;
		$strSS 						    = 'SS' . $strLoop;
		$strDOB 					    = 'DOB' . $strLoop;
		$strPoliticallyExposedPerson    = 'POLITICALLY_EXPOSED' . $strLoop;
		$strHomeAddress				    = 'HOME_ADDRESS' . $strLoop;
		$strOfficerCity				    = 'OFFICER_CITY' . $strLoop;
		$strOfficerStateCode 		    = 'OFFICER_STATE_CODE' . $strLoop;
		$strOfficerCountryCode		    = 'OFFICER_COUNTRY_CODE' . $strLoop;
		$strOfficerPostalCode 		    = 'OFFICER_POSTAL_CODE' . $strLoop;
		$strPhoneNumber 			    = 'PHONE_NUMBER' . $strLoop;
		$strOfficerEmailAddress 	    = 'OFFICER_EMAIL_ADDRESS' . $strLoop;
		$strOfficerTypeOption 		    = 'OWNER_TYPE_OPTION' . $strLoop;
		$strEquityOwnership 		    = 'EQUITY_OWNERSHIP' . $strLoop;
		$strControlIndividual 		    = 'CONTROL_INDIVIDUAL' . $strLoop;
		$strControlIndividualTitle	    = 'CONTROL_INDIVIDUAL_TITLE' . $strLoop;

		$this->m_objPdf->SetFont( 'Arial', '', 9 );
		if( true == isset( $arrobjApplicationKeyValues[$strOfficeName] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strOfficeName]->getValue();
			$this->m_objPdf->SetXY( 70, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 70, $this->m_intY + 10, 300, $this->m_intY + 10 );
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 305, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'SS# :', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strSS] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strSS]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 335, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 335, $this->m_intY + 10, 430, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 475, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'DOB :', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strDOB] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strDOB]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 505, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 510, $this->m_intY + 10, 570, $this->m_intY + 10 );

		if( true == isset( $arrobjApplicationKeyValues[$strOfficerTypeOption] ) && 1 == $arrobjApplicationKeyValues[$strOfficerTypeOption]->getvalue() ) {
			$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
			$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
			$this->m_objPdf->Cell( 15, 10, 'Politically Exposed Person [PEP] :', '', 1, 'L', 1 );

			$strTemp = 'No';
			if( true == isset( $arrobjApplicationKeyValues[$strPoliticallyExposedPerson] ) ) {
				$strTemp = ( '1' == $arrobjApplicationKeyValues[$strPoliticallyExposedPerson]->getValue() ) ? 'Yes' : 'No';
			}
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 190, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Home Address : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strHomeAddress] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strHomeAddress]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			if( true == $this->longString( $strTemp ) ) {
				$this->m_objPdf->SetFontSize( 7 );
			}
			$this->m_objPdf->setXY( 35 + 80, $this->m_intY );
			$this->m_objPdf->Cell( 60, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 35 + 80, $this->m_intY + 10, 35 + 280, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 320, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'City : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strOfficerCity] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strOfficerCity]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			if( true == $this->longString( $strTemp ) ) {
				$this->m_objPdf->SetFontSize( 7 );
			}
			$this->m_objPdf->SetXY( 320 + 25, $this->m_intY );
			$this->m_objPdf->Cell( 50, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 320 + 25, $this->m_intY + 10, 320 + 150, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 475, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'State : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strOfficerStateCode] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strOfficerStateCode]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 475 + 30, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}

		$this->m_objPdf->Line( 475 + 30, $this->m_intY + 10, 475 + 80, $this->m_intY + 10 );

		$this->m_intX = 35;
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Country : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strOfficerCountryCode] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strOfficerCountryCode]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( $this->m_intX + 50, $this->m_intY );
			$this->m_objPdf->Cell( 35, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( $this->m_intX + 50, $this->m_intY + 10, $this->m_intX + 80, $this->m_intY + 10 );

		$this->m_intX = 125;
		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( $this->m_intX, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Zip : ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strOfficerPostalCode] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strOfficerPostalCode]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( $this->m_intX + 30, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( $this->m_intX + 30, $this->m_intY + 10, $this->m_intX + 110, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 305, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Phone Number (personal) :', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strPhoneNumber] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strPhoneNumber]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->SetXY( 305 + 125, $this->m_intY );
			$this->m_objPdf->Cell( 15, 9, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 305 + 125, $this->m_intY + 10, 305 + 265, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 15, 10, 'Email Address (personal) :', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strOfficerEmailAddress] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strOfficerEmailAddress]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			if( true == $this->longString( $strTemp ) ) {
				$this->m_objPdf->SetFontSize( 7 );
			}
			$this->m_objPdf->SetXY( 35 + 125, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 35 + 125, $this->m_intY + 10, 35 + 365, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 35, $this->m_intY += 20 );
		$this->m_objPdf->Cell( 45, 10, 'Person ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strOfficerTypeOption] ) && 1 == $arrobjApplicationKeyValues[$strOfficerTypeOption]->getvalue() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', 72, $this->m_intY );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', 72, $this->m_intY );
		}

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 90, $this->m_intY );
		$this->m_objPdf->Cell( 140, 10, 'Entity ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strOfficerTypeOption] ) && 2 == $arrobjApplicationKeyValues[$strOfficerTypeOption]->getvalue() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', 120, $this->m_intY );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', 120, $this->m_intY );
		}

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 140, $this->m_intY );
		$this->m_objPdf->Cell( 140, 10, 'Control Individual ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strControlIndividual] ) && 1 == $arrobjApplicationKeyValues[$strControlIndividual]->getvalue() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', 223, $this->m_intY );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', 223, $this->m_intY );
		}

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 240, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Title :', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strControlIndividualTitle] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strControlIndividualTitle]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->setXY( 270, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 272, $this->m_intY + 10, 380, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 140, $this->m_intY );
		$this->m_objPdf->Cell( 140, 10, 'Control Individual ', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strControlIndividual] ) && 1 == $arrobjApplicationKeyValues[$strControlIndividual]->getvalue() ) {
			$this->m_objPdf->Image( PATH_COMMON . 'images/close_task.gif', 223, $this->m_intY );
		} else {
			$this->m_objPdf->Image( PATH_COMMON . 'images/open_task.gif', 223, $this->m_intY );
		}

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 240, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Title :', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strControlIndividualTitle] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strControlIndividualTitle]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->setXY( 270, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 272, $this->m_intY + 10, 380, $this->m_intY + 10 );

		$this->m_objPdf->SetFont( 'Arial', 'B', 9 );
		$this->m_objPdf->SetXY( 400, $this->m_intY );
		$this->m_objPdf->Cell( 15, 10, 'Equity Ownership (%) :', '', 1, 'L', 1 );

		if( true == isset( $arrobjApplicationKeyValues[$strEquityOwnership] ) ) {
			$strTemp = $arrobjApplicationKeyValues[$strEquityOwnership]->getValue();
			$this->m_objPdf->SetFont( 'Arial', '', 9 );
			$this->m_objPdf->setXY( 495, $this->m_intY );
			$this->m_objPdf->Cell( 15, 10, $strTemp, '', 1, 'L', 1 );
		}
		$this->m_objPdf->Line( 497, $this->m_intY + 10, 570, $this->m_intY + 10 );

		$this->m_objPdf->SetXY( 400, $this->m_intY += 30 );
		$this->m_objPdf->Cell( 15, 10, '', '', 1, '', 1 );
	}

	/*
	 * Pass the function a string, if it is deemed too long for a single field it will return true
	 */

	private function longString( $strFieldValue ) {

		$intCharacterLengthLimit = 40;

		if( $intCharacterLengthLimit < strlen( $strFieldValue ) ) {
			return true;
		}

		return false;

	}

}
?>