<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937CashLetters
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CX937CashLetters extends CBaseX937CashLetters {

	public static function fetchX937CashLettersByX937FileId( $intX937FileId, $objDatabase ) {

		$strSql = 'SELECT * FROM x937_cash_letters WHERE x937_file_id = ' . ( int ) $intX937FileId;
		return self::fetchX937CashLetters( $strSql, $objDatabase );
	}

}
?>