<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CClearingBatchTypes
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */

class CClearingBatchTypes extends CBaseClearingBatchTypes {

	public static function fetchClearingBatchTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CClearingBatchType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchClearingBatchType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CClearingBatchType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>