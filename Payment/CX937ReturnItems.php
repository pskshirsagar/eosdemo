<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CX937ReturnItems
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CX937ReturnItems extends CBaseX937ReturnItems {

	public static function fetchPaginatedX937ReturnItems( $intPageNo, $intPageSize, $objPaymentDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = 'SELECT
						 nri.*
					FROM
						x937_return_items nri
						ORDER BY nri.created_on DESC
						OFFSET ' . ( int ) $intOffset . '
						LIMIT ' . $intLimit;

		return self::fetchX937ReturnItems( $strSql, $objPaymentDatabase );
	}

	public static function fetchPaginatedX937ReturnItemsCount( $objPaymentDatabase ) {

		$strSql = 'SELECT count(id) FROM x937_return_items';

		$arrintResponse = fetchData( $strSql, $objPaymentDatabase );

		if ( true == isset ( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchX937ReturnItemsByX937ReturnFileId( $intX937ReturnFileId, $objDatabase ) {
		return self::fetchX937ReturnItems( sprintf( 'SELECT * FROM x937_return_items WHERE x937_return_file_id = %d', ( int ) $intX937ReturnFileId ), $objDatabase );
	}

	public static function fetchUnReconPreppedX937ReturnItems( $objDatabase ) {

		$strSql = 'SELECT
						xri.*, ap.payment_amount as amount,
						xrf.processing_bank_account_id
					 FROM
						x937_return_items xri
				INNER JOIN
						X937_return_files xrf ON xrf.id = xri.x937_return_file_id
				INNER JOIN
						ar_payments ap ON ap.id = xri.ar_payment_id
					WHERE
						( xri.is_recon_prepped <> TRUE OR xri.is_recon_prepped IS NULL )';

		return self::fetchX937ReturnItems( $strSql, $objDatabase );
	}

	public static function fetchUnProcessedX937ReturnItems( $objDatabase ) {

		$strSql = 'SELECT * FROM x937_return_items WHERE confirmed_on IS NULL';

		return self::fetchX937ReturnItems( $strSql, $objDatabase );
	}

}
?>