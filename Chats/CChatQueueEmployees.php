<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChatQueueEmployees
 * Do not add any new functions to this class.
 */

class CChatQueueEmployees extends CBaseChatQueueEmployees {

	public static function fetchAllChatQueueEmployees( $objDatabase ) {

		$strSql = 'SELECT * FROM chat_queue_employees';

		return parent::fetchChatQueueEmployees( $strSql, $objDatabase );
	}

	public static function fetchChatQueueEmployeeIdsByFilter( $arrmixFilter, $objDatabase, $intPageNo = 1, $intPageSize = DEFAULT_PAGINATION_COUNT, $boolIsCount = false ) {

		$arrstrWhereSql = [];
		if( true == valArr( $arrmixFilter['chat_queue_ids'] ) ) {
			$arrstrWhereSql[] = ' AND chat_queue_id IN ( ' . implode( ',', $arrmixFilter['chat_queue_ids'] ) . ' )';
		}

		if( true == valArr( $arrmixFilter['employee_ids'] ) ) {
			$arrstrWhereSql[] = ' AND employee_id IN ( ' . implode( ',', $arrmixFilter['employee_ids'] ) . ' )';
		}

		$intOffset 	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit 	= ( int ) $intPageSize;

		$strSql = 'SELECT 
						DISTINCT employee_id ';

		if( true == $boolIsCount ) {
			$strSql = 'SELECT 
						COUNT( DISTINCT employee_id ) ';
		}

		$strSql .= ' FROM ' . CChatQueueEmployee::TABLE_NAME . ' 
					 WHERE 
						1 = 1 
						' . implode( ' ', $arrstrWhereSql ) . '';

		if( false == $boolIsCount ) {
			$strSql .= 'OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit . ' ';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchChatQueueEmployeesByChatQueueIds( $arrintChatQueueIds, $objDatabase ) {

		if( false == valArr( $arrintChatQueueIds ) ) return NULL;

		$strSql = 'SELECT * FROM chat_queue_employees WHERE chat_queue_id IN ( ' . implode( ',', $arrintChatQueueIds ) . ' )';

		return parent::fetchChatQueueEmployees( $strSql, $objDatabase );
	}

	public static function fetchChatQueueEmployeesByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = 'SELECT * FROM chat_queue_employees WHERE employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return parent::fetchChatQueueEmployees( $strSql, $objDatabase );
	}

	public static function fetchChatQueueEmployeeCountByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strWhereSql = 'WHERE employee_id = ' . ( int ) $intEmployeeId;
		return parent::fetchChatQueueEmployeeCount( $strWhereSql, $objDatabase );
	}

	public static function fetchChatQueueEmployeesInfoByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = 'SELECT 
						cqe.employee_id,
						array_to_string(array_agg(cq.name), \', \') AS chat_queue_names,
						count( cqe.id ) AS chat_queue_count
					FROM 
						chat_queue_employees AS cqe
					LEFT JOIN
						chat_queues AS cq ON (cqe.chat_queue_id = cq.id)
					WHERE 
						cqe.employee_id IN (' . implode( ',', $arrintEmployeeIds ) . ' )
					GROUP BY
						cqe.employee_id;';

		return self::fetchChatQueueEmployees( $strSql, $objDatabase );
	}

	public static function fetchDistinctChatQueueEmployees( $objDatabase ) {
		$strSql = 'SELECT DISTINCT employee_id from chat_queue_employees';

		return fetchdata( $strSql, $objDatabase );
	}

}
?>