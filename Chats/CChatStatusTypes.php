<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChatStatusTypes
 * Do not add any new functions to this class.
 */

class CChatStatusTypes extends CBaseChatStatusTypes {

	public static function fetchAllChatStatusTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM chat_status_types;';
		return self::fetchObjects( $strSql, 'CChatStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchChatStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CChatStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>