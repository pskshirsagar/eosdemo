<?php

class CChatRouting extends CBaseChatRouting {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChatId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmployeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAgentSessionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChatRoutingsStartDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChatRoutingsEndDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>