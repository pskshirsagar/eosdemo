<?php

class CChatQueue extends CBaseChatQueue {

	const ENTRATA_FIRST_RESPONSE					= 1;
	const ENTRATA_CLIENT_RESOLUTION_SPECIALISTS		= 2;
	const MARKETING_FIRST_RESPONSE					= 3;
	const MARKETING_CLIENT_RESOLUTION_SPECIALISTS	= 4;
	const LEASE_FIRST_RESPONSE						= 5;
	const LEASE_CLIENT_RESOLUTION_SPECIALISTS		= 6;
	const RESIDENTS_FIRST_RESPONSE					= 7;
	const RESIDENTS_CLIENT_RESOLUTION_SPECIALISTS	= 8;
	const LEASING_CENTER							= 9;
	const SMS_CHAT									= 10;
	const SUPPORT_CHAT								= 11;

	const PRIORITY_FIRST_RESPONSE					= 1;
	const PRIORITY_CLIENT_RESOLUTION_SPECIALISTS	= 2;

	const CHAT_QUEUE_SKILL_LEVEL_BEGINNER = 'Beginner';
	const CHAT_QUEUE_SKILL_LEVEL_SPECIALIST = 'Specialist';

	const CHAT_ROOM_PREFIX = 'chat_queue_room';

	public static $c_arrintChatQueueProducts = array(

		CChatQueue::ENTRATA_FIRST_RESPONSE   => array(
			CPsProduct::ENTRATA => CPsProduct::ENTRATA
		),

		CChatQueue::ENTRATA_CLIENT_RESOLUTION_SPECIALISTS	=> array(
			CPsProduct::ENTRATA => CPsProduct::ENTRATA
		),

		CChatQueue::MARKETING_FIRST_RESPONSE => array(
			CPsProduct::PROSPECT_PORTAL	  => CPsProduct::PROSPECT_PORTAL,
			CPsProduct::VACANCY			  => CPsProduct::VACANCY,
			CPsProduct::ILS_PORTAL		   => CPsProduct::ILS_PORTAL,
			CPsProduct::SEO_SERVICES		 => CPsProduct::SEO_SERVICES,
			CPsProduct::CRAIGSLIST_POSTING   => CPsProduct::CRAIGSLIST_POSTING,
			CPsProduct::FACEBOOK_INTEGRATION => CPsProduct::FACEBOOK_INTEGRATION,
			CPsProduct::CUSTOM_DEVELOPMENT 	 => CPsProduct::CUSTOM_DEVELOPMENT,
			CPsProduct::PRICING 			 => CPsProduct::PRICING,
			CPsProduct::SEM_SERVICES 		 => CPsProduct::SEM_SERVICES,
			CPsProduct::DOMAIN_RENEWAL 		 => CPsProduct::DOMAIN_RENEWAL
		),

		CChatQueue::MARKETING_CLIENT_RESOLUTION_SPECIALISTS	=> array(
	  		CPsProduct::PROSPECT_PORTAL 		=> CPsProduct::PROSPECT_PORTAL,
			CPsProduct::VACANCY 				=> CPsProduct::VACANCY,
			CPsProduct::ILS_PORTAL 				=> CPsProduct::ILS_PORTAL,
			CPsProduct::SEO_SERVICES 			=> CPsProduct::SEO_SERVICES,
			CPsProduct::CRAIGSLIST_POSTING 		=> CPsProduct::CRAIGSLIST_POSTING,
			CPsProduct::FACEBOOK_INTEGRATION 	=> CPsProduct::FACEBOOK_INTEGRATION,
			CPsProduct::CUSTOM_DEVELOPMENT 		=> CPsProduct::CUSTOM_DEVELOPMENT,
			CPsProduct::PRICING 				=> CPsProduct::PRICING,
			CPsProduct::SEM_SERVICES 			=> CPsProduct::SEM_SERVICES,
			CPsProduct::DOMAIN_RENEWAL 			=> CPsProduct::DOMAIN_RENEWAL
		),

		CChatQueue::LEASE_FIRST_RESPONSE		=> array(
			CPsProduct::LOBBY_DISPLAY 			=> CPsProduct::LOBBY_DISPLAY,
			CPsProduct::LEASE_EXECUTION 		=> CPsProduct::LEASE_EXECUTION,
			CPsProduct::SITE_TABLET 			=> CPsProduct::SITE_TABLET,
			CPsProduct::CALL_TRACKER 			=> CPsProduct::CALL_TRACKER,
			CPsProduct::LEAD_MANAGEMENT 		=> CPsProduct::LEAD_MANAGEMENT,
			CPsProduct::CALL_ANALYSIS 			=> CPsProduct::CALL_ANALYSIS,
			CPsProduct::RESIDENT_VERIFY 		=> CPsProduct::RESIDENT_VERIFY
		),

		CChatQueue::LEASE_CLIENT_RESOLUTION_SPECIALISTS		=> array(
			CPsProduct::LOBBY_DISPLAY 			=> CPsProduct::LOBBY_DISPLAY,
			CPsProduct::LEASE_EXECUTION 		=> CPsProduct::LEASE_EXECUTION,
			CPsProduct::SITE_TABLET 			=> CPsProduct::SITE_TABLET,
			CPsProduct::CALL_TRACKER 			=> CPsProduct::CALL_TRACKER,
			CPsProduct::LEAD_MANAGEMENT 		=> CPsProduct::LEAD_MANAGEMENT,
			CPsProduct::CALL_ANALYSIS 			=> CPsProduct::CALL_ANALYSIS,
			CPsProduct::RESIDENT_VERIFY 		=> CPsProduct::RESIDENT_VERIFY
		),

		CChatQueue::RESIDENTS_FIRST_RESPONSE	=> array(
			CPsProduct::RESIDENT_PORTAL 		=> CPsProduct::RESIDENT_PORTAL,
			CPsProduct::RESIDENT_PAY 			=> CPsProduct::RESIDENT_PAY,
			CPsProduct::GMAIL_EMAIL 			=> CPsProduct::GMAIL_EMAIL,
			CPsProduct::RESIDENT_PAY_DESKTOP 	=> CPsProduct::RESIDENT_PAY_DESKTOP,
			CPsProduct::MESSAGE_CENTER 			=> CPsProduct::MESSAGE_CENTER,
			CPsProduct::RESIDENT_UTILITY 		=> CPsProduct::RESIDENT_UTILITY,
			CPsProduct::RESIDENT_INSURE 		=> CPsProduct::RESIDENT_INSURE,
			CPsProduct::PARCEL_ALERT 			=> CPsProduct::PARCEL_ALERT,
			CPsProduct::INVOICE_PROCESSING 		=> CPsProduct::INVOICE_PROCESSING,
			CPsProduct::INSPECTION_MANAGER 		=> CPsProduct::INSPECTION_MANAGER
		),

		CChatQueue::RESIDENTS_CLIENT_RESOLUTION_SPECIALISTS	=> array(
			CPsProduct::RESIDENT_PORTAL 		=> CPsProduct::RESIDENT_PORTAL,
			CPsProduct::RESIDENT_PAY 			=> CPsProduct::RESIDENT_PAY,
			CPsProduct::GMAIL_EMAIL 			=> CPsProduct::GMAIL_EMAIL,
			CPsProduct::RESIDENT_PAY_DESKTOP 	=> CPsProduct::RESIDENT_PAY_DESKTOP,
			CPsProduct::MESSAGE_CENTER 			=> CPsProduct::MESSAGE_CENTER,
			CPsProduct::RESIDENT_UTILITY 		=> CPsProduct::RESIDENT_UTILITY,
			CPsProduct::RESIDENT_INSURE 		=> CPsProduct::RESIDENT_INSURE,
			CPsProduct::PARCEL_ALERT 			=> CPsProduct::PARCEL_ALERT,
			CPsProduct::INVOICE_PROCESSING 		=> CPsProduct::INVOICE_PROCESSING,
			CPsProduct::INSPECTION_MANAGER 		=> CPsProduct::INSPECTION_MANAGER
		),

		CChatQueue::LEASING_CENTER	   => array(
			CPsProduct::LEASING_CENTER  => CPsProduct::LEASING_CENTER
		)
	);

	public static $c_arrintChatQueueIds = array(
		CChatQueue::ENTRATA_FIRST_RESPONSE,
		CChatQueue::ENTRATA_CLIENT_RESOLUTION_SPECIALISTS,
		CChatQueue::MARKETING_FIRST_RESPONSE,
		CChatQueue::MARKETING_CLIENT_RESOLUTION_SPECIALISTS,
		CChatQueue::LEASE_FIRST_RESPONSE,
		CChatQueue::LEASE_CLIENT_RESOLUTION_SPECIALISTS,
		CChatQueue::RESIDENTS_FIRST_RESPONSE,
		CChatQueue::RESIDENTS_CLIENT_RESOLUTION_SPECIALISTS,
		CChatQueue::LEASING_CENTER
	);

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriority() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public static function getChatQueueName() {
		return array(
			self::ENTRATA_FIRST_RESPONSE					=> 'Entrata - First Response',
			self::ENTRATA_CLIENT_RESOLUTION_SPECIALISTS		=> 'Entrata - Client Resolution Specialists',
			self::MARKETING_FIRST_RESPONSE					=> 'Marketing - First Response',
			self::MARKETING_CLIENT_RESOLUTION_SPECIALISTS	=> 'Marketing - Client Resolution Specialists',
			self::LEASE_FIRST_RESPONSE						=> 'Lease - First Response',
			self::LEASE_CLIENT_RESOLUTION_SPECIALISTS		=> 'Lease - Client Resolution Specialists',
			self::RESIDENTS_FIRST_RESPONSE					=> 'Residents - First Response',
			self::RESIDENTS_CLIENT_RESOLUTION_SPECIALISTS	=> 'Residents - Client Resolution Specialists',
			self::LEASING_CENTER							=> 'Leasing Center'
		);
	}

	public function getMucRoomName() {
		return self::CHAT_ROOM_PREFIX . '_' . $this->getId();
	}

	public function getAvailableCallAgents( $objChatDatabase, $objVoipDatabase ) {
		$arrobjChatQueueEmployees	= \Psi\Eos\Chats\CChatQueueEmployees::createService()->fetchChatQueueEmployeesByChatQueueId( $this->getId(), $objChatDatabase );
		$arrintEmployeeIds			= ( array ) array_keys( ( array ) rekeyObjects( 'EmployeeId', $arrobjChatQueueEmployees ) );
		$arrobjCallAgent = CCallAgents::fetchAvailableCallAgentByEmployeeIds( $arrintEmployeeIds, $objVoipDatabase );
		return $arrobjCallAgent;
	}

	public static function getChatQueueRoomNamesByIds( $arrintChatQueueIds ) {
		$arrstrChatQueueRoomNames = [];
		if( false == valArr( $arrintChatQueueIds ) ) {
			return [];
		}

		foreach( $arrintChatQueueIds AS $intChatQueueId ) {
			$arrstrChatQueueRoomNames[$intChatQueueId] = self::CHAT_ROOM_PREFIX . '_' . $intChatQueueId;
		}

		return $arrstrChatQueueRoomNames;
	}

}
?>