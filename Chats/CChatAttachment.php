<?php

class CChatAttachment extends CBaseChatAttachment {

	use Psi\Libraries\EosFoundation\TEosStoredObject;

	public static $c_arrstrAllowedFileExtensions = [
		'jpg'       => 'image/pjpeg',
		'jpeg'		=> 'image/jpeg',
		'png'		=> 'image/png',
		'pdf'		=> 'application/pdf',
		'doc'		=> 'application/msword',
		'docx'		=> 'application/msword',
		'xls'		=> 'application/vnd.ms-excel',
		'xlsx'		=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
	];

	public function getCid() {
		return CClient::ID_DEFAULT;
	}

	public function valFileType() {
		$boolIsValid = true;
		if( ! in_array( $this->getFileType(), array_keys( $this::$c_arrstrAllowedFileExtensions ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_type', 'This file format is not allowed.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid = $this->valFileType();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $strReferenceTag = NULL ) {
		$boolReturn = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		if( $boolReturn && false == $boolReturnSqlOnly ) {
			$boolReturn &= $this->insertOrUpdateStoredObject( $intCurrentUserId, $objDatabase, NULL, $strReferenceTag );
		}

		return $boolReturn;
	}

	public function calcStorageKey() {
		$strKey				= '/chat_attachments/'. $this->getChatId() . '/' . date( 'Y/m/d' ) . '/' . $this->getId() . '/' . $this->getFileName();
		return $strKey;
	}

	protected function calcStorageContainer( $strVendor = NULL ) {
		return OSG_BUCKET_MEDIA_LIBRARY;
	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

}
?>