<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChatQueues
 * Do not add any new functions to this class.
 */

class CChatQueues extends CBaseChatQueues {

	public static function fetchAllChatQueues( $objDatabase ) {

		$strSql = 'Select * FROM chat_queues ORDER BY id ASC';

		return parent::fetchChatQueues( $strSql, $objDatabase );
	}

	public static function fetchChatQueueByPsProductId( $intPsProductId, $strSkillLevel, $objChatDatabase ) {
		$strSql = 'SELECT * 
			FROM 
				chat_queues 
			WHERE 
				skill_level = \'' . $strSkillLevel . '\' 
				AND details @> \'{"ps_product_ids": [' . $intPsProductId . ']}\' 
			ORDER BY id 
			ASC LIMIT 1';
		return self::fetchChatQueue( $strSql, $objChatDatabase );
	}

}
?>