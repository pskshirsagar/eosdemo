<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChatBuffers
 * Do not add any new functions to this class.
 */

class CChatBuffers extends CBaseChatBuffers {

	public static function fetchChatBufferByChatIdByTicketId( $intChatId, $intTicketId, $objDatabase ) {

		$strSql = 'SELECT * FROM chat_buffers WHERE chat_id = ' . ( int ) $intChatId . ' AND ticket_id = ' . ( int ) $intTicketId;

		return parent::fetchChatBuffer( $strSql, $objDatabase );
	}

	public static function fetchChatBufferByTicketIdByLevel( $intTicketId, $strLevel, $objDatabase ) {

		$strSql = 'SELECT * FROM chat_buffers WHERE ticket_id = ' . ( int ) $intTicketId . ' AND level = \'' . $strLevel . '\'';

		return parent::fetchChatBuffer( $strSql, $objDatabase );
	}

	public static function fetchUnsentChatBuffers( $objDatabase ) {

		$strSql = 'SELECT
						id,
						resource_id,
						chat_id
					FROM
						chat_buffers
					WHERE
						sent_on is NULL
						AND lock_sequence_number IS NULL
        				AND locked_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchChatBuffersByChatIdByLevel( $intChatId, $strLevel, $objDatabase ) {
		$strSql = 'SELECT 
						* 
					FROM 
						chat_buffers 
					WHERE 
						chat_id = ' . ( int ) $intChatId . '
						 AND level = \'' . $strLevel . '\'';

		return self::fetchChatBuffer( $strSql, $objDatabase );
	}

}
?>