<?php

class CUserSession extends CBaseUserSession {

	const USER_TYPE_AGENT		= 'agent';

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDatabaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSessionData() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public function getXmppPassword() {
		return md5( $this->getCid() . '_' . $this->getUserId() . '_' . $this->getUsername() );
	}

	public function updateChatAgentStatus( $objVoipDatabase, $objFreeSwitchDatabase, $boolUpdateAgentStatusAvailable ) {
		$this->m_objCallAgent = \Psi\Eos\Voip\CCallAgents::createService()->fetchCallAgentByUserId( $this->getUserId(), $objVoipDatabase );
		if( false == valObj( $this->m_objCallAgent, 'CCallAgent' ) ) {
			return false;
		}

		$objAgent = \Psi\Eos\Freeswitch\CAgents::createService()->fetchAgentByCallAgentId( $this->m_objCallAgent->getId(), $objFreeSwitchDatabase );

		// For now we have used the physical phone extension id for checking agent is login to FreeSwitch and
		// the IsPreviousStatusInLeasingChat for checking the agent manually changed the status to In leasing Chat

		if( true == $boolUpdateAgentStatusAvailable && false == $this->m_objCallAgent->getIsPreviousStatusInLeasingChat() && true == $this->valId( $this->m_objCallAgent->getPhysicalPhoneExtensionId() ) ) {
			if( CDepartment::CALL_CENTER == $this->m_objCallAgent->getDepartmentId() || CDepartment::TECHNICAL_SUPPORT == $this->m_objCallAgent->getDepartmentId() ) {
				$this->m_objCallAgent->setCallAgentStatusTypeId( CCallAgentStatusType::AVAILABLE );
				$objAgent->setStatus( CAgent::STATUS_AVAILABLE );
			} else {
				$this->m_objCallAgent->setCallAgentStatusTypeId( CCallAgentStatusType::AVAILABLE_ON_DEMAND );
				$objAgent->setStatus( CAgent::STATUS_AVAILABLE_ON_DEMAND );
			}

			$this->m_objCallAgent->setCallAgentStateTypeId( CCallAgentStateType::WAITING );
			$objAgent->setState( CAgent::STATE_WAITING );
		} else {
			$this->m_objCallAgent->setCallAgentStatusTypeId( CCallAgentStatusType::IN_LEASING_CHAT );
			$this->m_objCallAgent->setCallAgentStateTypeId( CCallAgentStateType::IDLE );
			$objAgent->setStatus( CAgent::STATUS_IN_LEASING_CHAT );
			$objAgent->setState( CAgent::STATE_IDLE );
		}

		$objVoipDatabase->begin();
		$objFreeSwitchDatabase->begin();

		if( false == $this->m_objCallAgent->update( $this->getUserId(), $objVoipDatabase ) || false == $objAgent->update( $this->getUserId(), $objFreeSwitchDatabase ) ) {
			$objVoipDatabase->rollback();
			$objFreeSwitchDatabase->rollback();
			return false;
		}

		$objVoipDatabase->commit();
		$objFreeSwitchDatabase->commit();
		return true;
	}

}
?>