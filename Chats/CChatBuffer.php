<?php

class CChatBuffer extends CBaseChatBuffer {

	const LEVEL_BEGINNER				= 'Beginner';
	const LEVEL_INTERMEDIATE			= 'Intermediate';
	const LEVEL_MULTI_CONTACT_BUTTON    = 'MULTI_CONTACT_BUTTON';

	public static $c_arrintXmppLevelRouting	= array( self::LEVEL_BEGINNER     => CDesignation::FIRST_RESPONSE_SUPPORT_REPRESENTATIVE,
	                                                    self::LEVEL_INTERMEDIATE => CDesignation::CLIENT_RESOLUTION_SPECIALIST );

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChatId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTicketId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valResourceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLevel() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLockSequenceNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLockedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSentOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
        }

        return $boolIsValid;
    }
}
?>