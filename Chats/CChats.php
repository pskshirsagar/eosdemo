<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChats
 * Do not add any new functions to this class.
 */

class CChats extends CBaseChats {

	public static function fetchActiveChatById( $intId, $objDatabase ) {
		return self::fetchChat( sprintf( 'SELECT * FROM chats WHERE id = %d AND chat_end_datetime IS NULL', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCurrentDayChatsByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return self::fetchChats( sprintf( 'SELECT * FROM chats WHERE company_user_id = %d AND DATE_TRUNC ( \'day\', created_on ) = DATE_TRUNC ( \'day\', NOW() )', ( int ) $intCompanyUserId ), $objDatabase );
	}

	public static function fetchAllValidChats( $objDatabase, $intCid = NULL ) {
		$strSql = 'SELECT
						*
					FROM
						chats
					WHERE
						chat_end_datetime IS NOT NULL';

		if( false == is_null( $intCid ) ) {
			$strSql .= ' AND cid = ' . ( int ) $intCid;
		}

		return self::fetchChats( sprintf( $strSql ), $objDatabase );
	}

	public static function fetchPaginatedChatsBySearchParameters( $intPageNo, $intPageSize, $objChatFilter, $objDatabase, $strDateSelected = NULL ) {

		$intOffset 	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		if( false == is_null( $strDateSelected ) ) {
			$strSelectClause = 'DATE_TRUNC ( \'' . $strDateSelected . '\', c.created_on )';
		} elseif( true == is_null( $objChatFilter->getCid() ) ) {
			$strSelectClause = 'c.cid';
		} elseif( true == is_null( $objChatFilter->getPropertyId() ) ) {
			$strSelectClause = 'c.property_id';
		} else {
			$strSelectClause = 'c.id, c.company_user_id ';
		}

		$arrstrSearchParameters = self::getSearchCriteria( $objChatFilter );
		$strWhereClause			= ( ( false == is_null( $arrstrSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrSearchParameters ) : '' );

		$strSql = 'SELECT ' . $strSelectClause . ',
						avg(c.chat_start_datetime - c.created_on) as avg_response_time,
						avg(c.chat_end_datetime - c.chat_start_datetime) as avg_chat_time,
						count(c.chat_start_datetime) as accepted_chat_count,
						(count(c.*) - count(c.chat_start_datetime)) as missed_chat_count
 					FROM chats as c
 					WHERE c.chat_end_datetime IS NOT NULL' . $strWhereClause . '
 					GROUP BY ' . $strSelectClause . '
					ORDER BY ' . $strSelectClause;

		if( false == is_null( $intPageSize ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . '
			   			LIMIT ' . ( int ) $intPageSize;
		}

		$arrmixChats = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixChats ) ) {
			return $arrmixChats;
		}

		return NULL;
	}

	public static function fetchChatsCountBySearchParameters( $objChatFilter, $objDatabase ) {

		if( true == is_null( $objChatFilter->getCid() ) ) {
			$strSelectClause = 'cid';
		} elseif( true == is_null( $objChatFilter->getPropertyId() ) ) {
			$strSelectClause = 'property_id';
		} else {
			$strSelectClause = 'id';
		}

		$arrstrAndSearchParameters 	= self::getSearchCriteria( $objChatFilter );
		$strWhereClause				= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );

		$strSql = 'SELECT
						count(c.' . $strSelectClause . ')
					FROM
						chats as c
					WHERE
						 c.chat_end_datetime IS NOT NULL' . $strWhereClause . '
						 GROUP BY c.' . $strSelectClause;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintResponse ) ) return \Psi\Libraries\UtilFunctions\count( $arrintResponse );

		return 0;
	}

	public static function getSearchCriteria( $objChatFilter ) {

		$arrstrWhereParameters = [];

		// Create SQL parameters.
		if( true == valObj( $objChatFilter, 'CChatFilter' ) ) {
			if( false == is_null( $objChatFilter->getCid() ) )			$arrstrWhereParameters[] = 'c.cid=' . ( int ) $objChatFilter->getCid();
			if( false == is_null( $objChatFilter->getPropertyId() ) )	$arrstrWhereParameters[] = 'c.property_id=' . ( int ) $objChatFilter->getPropertyId();
			if( false == is_null( $objChatFilter->getChatTypeId() ) )	$arrstrWhereParameters[] = 'c.ps_product_id=' . ( int ) $objChatFilter->getChatTypeId();
			if( false == is_null( $objChatFilter->getFromDate() ) )		$arrstrWhereParameters[] = 'c.created_on >= \'' . date( 'Y-m-d', strtotime( $objChatFilter->getFromDate() ) ) . ' 00:00:00\'';
			if( false == is_null( $objChatFilter->getToDate() ) )		$arrstrWhereParameters[] = 'c.created_on <= \'' . date( 'Y-m-d', strtotime( $objChatFilter->getToDate() ) ) . ' 23:59:59\'';
			if( false == is_null( $objChatFilter->getChatId() ) )		$arrstrWhereParameters[] = 'c.id = ' . ( int ) $objChatFilter->getChatId();
			if( true == valStr( $objChatFilter->getUserIds() ) )		$arrstrWhereParameters[] = ' ( c.user_id IN ( ' . $objChatFilter->getUserIds() . ' ) OR c.company_user_id IN ( ' . $objChatFilter->getUserIds() . ' ) ) ';
			if( true == valStr( $objChatFilter->getCids() ) )			$arrstrWhereParameters[] = ' c.cid IN ( ' . $objChatFilter->getCids() . ' ) ';
			if( true == valStr( $objChatFilter->getChatQueueIds() ) )	$arrstrWhereParameters[] = 'c.chat_queue_id IN ( ' . $objChatFilter->getChatQueueIds() . ' ) ';
			if( true == valStr( $objChatFilter->getPropertyIds() ) )	$arrstrWhereParameters[] = ' c.property_id IN ( ' . $objChatFilter->getPropertyIds() . ' ) ';
			if( true == valStr( $objChatFilter->getChatIds() ) )		$arrstrWhereParameters[] = 'c.id IN ( ' . $objChatFilter->getChatids() . ' ) ';
			if( true == valStr( $objChatFilter->getChatSourceIds() ) )	$arrstrWhereParameters[] = 'c.chat_source_id IN ( ' . $objChatFilter->getChatSourceIds() . ')';
			if( true == valId( $objChatFilter->getChatSourceId() ) )	$arrstrWhereParameters[] = 'c.chat_source_id = ' . $objChatFilter->getChatSourceId();
			if( true == valStr( $objChatFilter->getChatStatus() ) )		$arrstrWhereParameters[] = ' c.chat_status_type_id IN ( ' . $objChatFilter->getChatStatus() . ' ) ';
			if( true == valStr( $objChatFilter->getLocation() ) ) {
				if( 'leasing_center' == $objChatFilter->getLocation() ) {
					$arrstrWhereParameters[] = 'c.details->\'is_leasing_center_chat\' = \'true\'';
				}
				if( 'on_site' == $objChatFilter->getLocation() ) {
					$arrstrWhereParameters[] = ' \'false\' =  c.details ? \'is_leasing_center_chat\' ';
				}
			}
		}

		if( true == valArr( $arrstrWhereParameters ) ) {
			return $arrstrWhereParameters;
		}

		return NULL;
	}

	public static function fetchChatInfoByChatId( $intChatId, $objDatabase ) {
		$strSql = 'SELECT
						cl.line_text,
						cb.ticket_id
					FROM
						chats c
						JOIN chat_lines cl ON( c.id = cl.chat_id )
						JOIN chat_buffers cb ON( c.id = cb.chat_id )
					WHERE
						c.id = ' . ( int ) $intChatId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchChatsProperties( $objDatabase ) {
		$strSql = 'SELECT property_id from chats WHERE property_id IS NOT NULL';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchChatsTodaysTotal( $objDatabase ) {
		$objDatabase->validateAndOpen( false );
		$strSql = 'SELECT
						COALESCE(SUM(
							CASE
			 					WHEN
									c.chat_end_datetime IS NOT NULL
									AND c.chat_start_datetime IS NULL
								THEN 1
			 					ELSE 0
		   					END
							),0)missed_chats,
	   					COALESCE(SUM(
							CASE
			 					WHEN
									c.chat_end_datetime IS NOT NULL
									AND c.chat_start_datetime IS NOT NULL
								THEN 1
			 					ELSE 0
		   					END
							),0) chats_taken
					FROM
						chats c
					WHERE
						c.created_on::date = NOW()::date
						AND ( c.details->\'is_leasing_center_chat\' = \'true\' 
						OR ( c.user_id IS NOT NULL
						AND c.ps_product_id !=' . CPsProduct::LEASING_CENTER . ' )
						)';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTodaysLeasingCenterChatStats( $objDatabase ) {

		$strSql = ' SELECT
						COALESCE(sum(case when c.chat_status_type_id = ' . CChatStatusType::CHAT_IN_PROGRESS . ' OR c.chat_status_type_id = ' . CChatStatusType::COMPLETED . ' THEN 1 ELSE 0 END), 0) AS todays_accepted_chats,
						json_agg(case when c.chat_status_type_id = ' . CChatStatusType::CHAT_IN_PROGRESS . ' OR c.chat_status_type_id = ' . CChatStatusType::COMPLETED . ' THEN c.id ELSE NULL END) AS todays_accepted_chat_ids,
						COALESCE(sum(case when c.chat_status_type_id = ' . CChatStatusType::MISSED . ' THEN 1 ELSE 0 END), 0) AS todays_missed_chats,
						json_agg(case when c.chat_status_type_id = ' . CChatStatusType::MISSED . ' THEN c.id ELSE NULL END) AS todays_missed_chat_ids,
						json_agg(case when c.chat_status_type_id = ' . CChatStatusType::CHAT_IN_PROGRESS . ' OR c.chat_status_type_id = ' . CChatStatusType::COMPLETED . ' THEN c.user_id END) AS total_accepted_chats_per_agent
					FROM
						chats c
					WHERE
						c.created_on::date = NOW()::date
						AND c.chat_source_id IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchChatStatsForSupportTvDisplay( $objDatabase ) {

		$objDatabase->validateAndOpen( false );

		$strSql = ' SELECT
							COALESCE(sum( case when c.chat_status_type_id = ' . CChatStatusType::CHAT_IN_PROGRESS . ' THEN 1 ELSE 0 END ),0) AS active_chats,
							COALESCE(sum( CASE WHEN c.chat_status_type_id = ' . CChatStatusType::PENDING . ' THEN 1 ELSE 0 END ),0) AS chats_in_queue,
							TO_CHAR( ( COALESCE (MAX ( wait_time ), 0 ) || \'second\' ) ::interval, \'MI:SS\' ) AS longest_chat_queue_time,
							round( MAX ( wait_time ) ) AS longest_chat_queue_time_in_seconds
						FROM
							chats c
							LEFT JOIN(
									SELECT
										c1.id,
										EXTRACT ( EPOCH FROM( now() - min( c1.created_on ) ) ) AS wait_time
									FROM
										chats c1
									WHERE
										c1.chat_status_type_id = ' . CChatStatusType::PENDING . '
										AND c1.chat_end_datetime IS NULL
										AND c1.created_on BETWEEN (now() - interval \'1 hour\') AND now()
										AND c1.chat_source_id = ' . CChatSource::ENTRATA_SUPPORT . '
									GROUP BY
										c1.id) sub_query ON( sub_query.id = c.id)
						WHERE
							c.chat_end_datetime IS NULL
							AND c.created_on BETWEEN (now() - interval \'1 hour\') AND now()
							AND c.chat_source_id = ' . CChatSource::ENTRATA_SUPPORT;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLastHourLeasingCenterChatStats( $objDatabase ) {

		$strSql = ' SELECT
						COALESCE(sum(case when c.chat_status_type_id = ' . CChatStatusType::CHAT_IN_PROGRESS . ' THEN 1 ELSE 0 END), 0) AS active_chats,
						json_agg(case when c.chat_status_type_id = ' . CChatStatusType::CHAT_IN_PROGRESS . ' THEN c.id ELSE NULL END) AS active_chat_ids,  
						COALESCE(sum(case when c.chat_status_type_id = ' . CChatStatusType::PENDING . ' THEN 1 ELSE 0 END), 0) AS chats_in_queue,
						json_agg(case when c.chat_status_type_id = ' . CChatStatusType::PENDING . ' THEN c.id ELSE NULL END) AS in_queue_chat_ids,
						COALESCE(sum(case when c.chat_status_type_id = ' . CChatStatusType::MISSED . ' THEN 1 ELSE 0 END), 0) AS missed_chats,
						json_agg(case when c.chat_status_type_id = ' . CChatStatusType::MISSED . ' THEN c.id ELSE NULL END) AS missed_chat_ids,  
						COALESCE(sum(case when c.chat_status_type_id = ' . CChatStatusType::COMPLETED . ' THEN 1 ELSE 0 END), 0) AS answered_chats,
						json_agg(case when c.chat_status_type_id = ' . CChatStatusType::COMPLETED . ' THEN c.id ELSE NULL END) AS answered_chat_ids
					FROM
						chats c
					WHERE
						c.created_on BETWEEN (now() - interval \'1 hour\') AND now()
						AND c.chat_source_id IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchChatCountByCallFilter( $objCallFilter, $objDatabase, $boolIsFromDownload = false ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		if( true == $boolIsFromDownload ) {
			switch( $objCallFilter->getIntervalTypeId() ) {

				case NULL:
				case CCallFilter::LEASING_CENTER_REPORT_INTERVAL_HOUR:
					$strGroupSql				= ' sub_query.chat_hour, ';
					$strFieldSql				= 'chat_hour';
					$strFetchGroupByFieldSql	= ' TO_CHAR(c.chat_start_datetime, \'YYYY/MM/DD HH24\' ) AS chat_hour,';
					$strFetchGroupBySql			= ' GROUP BY chat_hour ';
					$strOrderByField			= ' ORDER BY chat_hour';
					break;

				case CCallFilter::LEASING_CENTER_REPORT_INTERVAL_DAILY:
					$strGroupSql				= ' sub_query.chat_date, ';
					$strFieldSql				= 'chat_date';
					$strFetchGroupByFieldSql	= ' TO_CHAR(c.chat_start_datetime, \'YYYY/MM/DD\') AS chat_date,';
					$strFetchGroupBySql			= ' GROUP BY chat_date ';
					$strOrderByField			= ' ORDER BY chat_date';
					break;

				case CCallFilter::LEASING_CENTER_REPORT_INTERVAL_WEEKLY:
					$strGroupSql				= ' sub_query.chat_week, ';
					$strFieldSql				= 'chat_week';
					$strFetchGroupByFieldSql	= ' TO_CHAR(cast(c.chat_start_datetime as date), \'IW\') AS chat_week,';
					$strFetchGroupBySql			= ' GROUP BY chat_week ';
					$strOrderByField			= ' ORDER BY chat_week';
					break;

				case CCallFilter::LEASING_CENTER_REPORT_INTERVAL_MONTHLY:
					$strGroupSql				= ' sub_query.chat_month, ';
					$strFieldSql				= 'chat_month';
					$strFetchGroupByFieldSql	= ' TO_CHAR( c.chat_start_datetime, \'Month\' ) AS chat_month,';
					$strFetchGroupBySql			= ' GROUP BY chat_month ';
					$strOrderByField			= ' ORDER BY chat_month';
					break;

				default:
					// Default empty case
					break;
			}
		}

		$strSql = 'SELECT
						' . $strGroupSql . '
						COALESCE( total_chat_count, 0 ) AS total_chat_count,
						COALESCE( total_answered_chat_count, 0 ) AS total_answered_chat_count,
						COALESCE( average_chat_time_seconds, 0 ) AS average_chat_time_seconds
					FROM
						(
							SELECT
								' . $strFetchGroupByFieldSql . '
								COUNT( CASE WHEN cb.ticket_id IS NULL
											THEN c.id ELSE NULL
										END ) AS total_chat_count,
								COUNT( CASE WHEN cb.ticket_id IS NULL AND c.chat_start_datetime IS NOT NULL
											THEN c.id ELSE NULL
										END ) AS total_answered_chat_count,
								AVG( CASE WHEN chat_start_datetime IS NOT NULL AND chat_end_datetime IS NOT NULL
										 THEN EXTRACT( EPOCH FROM ( chat_end_datetime - chat_start_datetime ) )
										 END ) AS average_chat_time_seconds
							FROM
								chats c
								JOIN chat_buffers cb ON ( c.id = cb.chat_id )
							WHERE
								c.created_by = ' . ( int ) CUser::ID_PROSPECT_PORTAL;

		if( true == valStr( $objCallFilter->getStartDate() ) && true == valStr( $objCallFilter->getEndDate() ) ) {
			$strSql .= ' AND DATE_TRUNC(\'day\', c.created_on) >= \'' . $objCallFilter->getStartDate() . '\' AND DATE_TRUNC(\'day\', c.created_on) <= \'' . $objCallFilter->getEndDate() . '\'';
		}

		if( true == valId( $objCallFilter->getCid() ) ) {
			$strSql .= ' AND c.cid = ' . ( int ) $objCallFilter->getCid();
		}

		if( true == valId( $objCallFilter->getPropertyId() ) ) {
			$strSql .= ' AND c.property_id = ' . ( int ) $objCallFilter->getPropertyId();
		}

		$strSql .= $strFetchGroupBySql . $strOrderByField . ' ) AS sub_query ' . $strOrderByField;

		$arrmixChatData = fetchData( $strSql, $objDatabase );

		if( true == $boolIsFromDownload && true == valArr( $arrmixChatData ) ) {
			$arrmixChatData[]['field'] = $strFieldSql;
		}

		return $arrmixChatData;
	}

	public static function fetchPaginatedChatsByChatFilter( $objChatFilter, $objDatabase, $boolCountOnly = false ) {

		$arrstrSearchParameters = self::getSearchCriteria( $objChatFilter );
		$strWhereClause			= ( ( false == is_null( $arrstrSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrSearchParameters ) : '' );

		if( true == $boolCountOnly ) {
			$strSelectSql = ' COUNT(c.id) ';
		} else {
			$strSelectSql = '
				cst.name AS chat_status_type_name,
				cq.name AS chat_queue_name,
				c.*, 
				CASE
					WHEN c.chat_status_type_id = ' . CChatStatusType::COMPLETED . ' THEN EXTRACT( EPOCH FROM ( c.chat_start_datetime - c.created_on ) )
					ELSE EXTRACT( EPOCH FROM ( c.chat_end_datetime - c.created_on ) ) 
				END AS chat_in_queue_time';
		}

		$intOffset = ( 0 < $objChatFilter->getPageNo() ) ? ( int ) $objChatFilter->getPageSize() * ( $objChatFilter->getPageNo() - 1 ) : 0;

		$strSql = ' SELECT 
						' . $strSelectSql . ' 
					FROM 
						chats c
						LEFT JOIN chat_queues cq ON cq.id = c.chat_queue_id
						LEFT JOIN chat_status_types cst ON cst.id = c.chat_status_type_id
 					WHERE 1 = 1 '
		          . $strWhereClause;

		if( false == $boolCountOnly ) {
			$strSql .= ' ORDER BY id DESC';
		}

		if( false == is_null( $objChatFilter->getPageSize() ) && false == $boolCountOnly ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . '
			   			LIMIT ' . ( int ) $objChatFilter->getPageSize();
		}

		if( true == $boolCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objDatabase );
			return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
		}

		$arrmixChats = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixChats ) ) {
			return $arrmixChats;
		}
		return NULL;
	}

	public static function fetchChatsByIds( $arrintChatIds, $objDatabase ) {
		if( false == valArr( $arrintChatIds ) ) return false;
		$strSql = 'SELECT * FROM ' . CChat::TABLE_NAME . ' WHERE id IN ( ' . implode( ',', $arrintChatIds ) . ' )';
		return self::fetchChats( $strSql, $objDatabase );
	}

	public static function fetchLastHourChatStats( $objDatabase ) {
		$strSql = 'SELECT
						COALESCE(SUM(
							CASE
								WHEN
									c.chat_end_datetime IS NOT NULL
									AND c.chat_start_datetime IS NULL
								THEN 1
								ELSE 0
							END
							),0)missed_chats,
						json_agg(
							CASE
			 					WHEN
									c.chat_end_datetime IS NOT NULL
									AND c.chat_start_datetime IS NULL
								THEN c.id
		   					END
							) AS missed_chat_ids,
	   					COALESCE(SUM(
							CASE
			 					WHEN
									c.chat_end_datetime IS NOT NULL
									AND c.chat_start_datetime IS NOT NULL
								THEN 1
			 					ELSE 0
		   					END
							),0) answered_chats,
						json_agg(
							CASE
			 					WHEN
									c.chat_end_datetime IS NOT NULL
									AND c.chat_start_datetime IS NOT NULL
								THEN c.id
		   					END
						) AS answered_chat_ids
					FROM
						chats c
					WHERE
						c.created_on BETWEEN (now() - interval \'1 hour\') AND now()
						AND c.details->\'is_leasing_center_chat\' = \'true\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchChatsAverageResponseTime( $objDatabase ) {
		$strSql = 'SELECT 
						COALESCE(
						round(
							EXTRACT( EPOCH FROM ( sum( chat_start_datetime::timestamp - created_on::timestamp ) ) ) / count( id )
						), 0) as average_response_time
					FROM
						chats
					WHERE
						created_on::date = NOW()::date
						AND chat_status_type_id IN( ' . CChatStatusType::CHAT_IN_PROGRESS . ', ' . CChatStatusType::COMPLETED . ' )
						AND ( details->\'is_leasing_center_chat\' = \'true\'
						OR ps_product_id !=' . CPsProduct::LEASING_CENTER . ' )';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchChatsCountByChatFilter( $objChatFilter, $intPsProductId, $objDatabase ) {

		$strWhereCondition = ' AND c.details->\'is_leasing_center_chat\' = \'true\'';
		if( CPsProduct::PROSPECT_PORTAL == $intPsProductId ) {
			$strWhereCondition = ' AND c.details->\'is_leasing_center_chat\' IS NULL';
		}

		$strSql = 'SELECT
	   					COALESCE(SUM(
							CASE
			 					WHEN
									c.chat_status_type_id = ' . CChatStatusType::COMPLETED . '
								THEN 1
			 					ELSE 0
		   					END
							),0) completed_chat,
	   					COALESCE(SUM(
							CASE
			 					WHEN
									c.chat_status_type_id = ' . CChatStatusType::MISSED . '
								THEN 1
			 					ELSE 0
		   					END
							),0) missed_chat,
	   					COALESCE(SUM(
							CASE
			 					WHEN
									c.chat_status_type_id = ' . CChatStatusType::OFFLINE_MESSAGE . '
								THEN 1
			 					ELSE 0
		   					END
							),0) offline_message,
						COALESCE(SUM(
							CASE
			 					WHEN
									c.created_on IS NOT NULL
								THEN 1
			 					ELSE 0
		   					END
							),0) total_chat,
						COALESCE( round(
							EXTRACT( EPOCH
								FROM ( sum(
										chat_end_datetime::timestamp - chat_start_datetime::timestamp ) ) ) / count( id )
						), 0) as average_chat_time
					FROM
						chats c
					WHERE
						c.created_on BETWEEN (now() - interval \'100 day\') AND now()' . $strWhereCondition;

		if( true == valId( $objChatFilter->getCid() ) ) {
			$strSql .= ' AND c.cid = ' . $objChatFilter->getCid();
		}

		if( true == valId( $objChatFilter->getPropertyId() ) ) {
			$strSql .= ' AND c.property_id = ' . $objChatFilter->getPropertyId();
		}

		if( true == valStr( $objChatFilter->getFromDate() ) && true == valStr( $objChatFilter->getToDate() ) ) {
			$strSql .= ' AND c.created_on >= \'' . $objChatFilter->getFromDate() . ' 00:00:00\'
						 AND c.created_on <= \'' . $objChatFilter->getToDate() . ' 23:59:59\' ';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMissedChats( $objChatDatabase ) {
		$strSql = 'SELECT 
						c.id,
						c.chat_status_type_id,
						c.chat_start_datetime,
						c.chat_end_datetime,
						c.ps_product_id
					FROM
						chats c 
						LEFT JOIN chat_lines cl ON c.id = cl.chat_id 
					WHERE 
						chat_start_datetime IS NULL 
						AND chat_end_datetime IS NOT NULL 
						AND cl.chat_id IS NULL
						AND c.ps_product_id != ' . CPsProduct::PROSPECT_PORTAL . '
						AND c.details IS NULL
						AND c.created_on >= \'2018-01-01 00:00:00\'
					ORDER BY c.id';

		return CChats::fetchChats( $strSql, $objChatDatabase );
	}

	public static function fetchCompletedChats( $objChatDatabase ) {
		$strSql = 'SELECT 
						c.id,
						c.chat_status_type_id,
						c.chat_start_datetime,
						c.chat_end_datetime,
						c.ps_product_id,
						cr.employee_id
					FROM
						chats c 
						INNER JOIN chat_lines cl ON c.id = cl.chat_id 
						LEFT JOIN chat_routings cr ON c.id = cr.chat_id
					WHERE 
						chat_start_datetime IS NOT NULL 
						AND chat_end_datetime IS NOT NULL
						AND c.ps_product_id != ' . CPsProduct::PROSPECT_PORTAL . '
						AND c.details IS NULL
						AND c.created_on >= \'2018-01-01 00:00:00\'

					ORDER BY c.id';

		return CChats::fetchChats( $strSql, $objChatDatabase );
	}

	public static function fetchOfflineMessages( $objChatDatabase ) {
		$strSql = 'SELECT 
						c.id,
						c.chat_status_type_id,
						c.chat_start_datetime,
						c.chat_end_datetime,
						c.ps_product_id
					FROM
						chats c 
						LEFT JOIN chat_lines cl ON c.id = cl.chat_id 
					WHERE 
						chat_start_datetime IS NULL 
						AND chat_end_datetime IS NOT NULL
						AND cl.chat_id IS NOT NULL
						AND c.ps_product_id != ' . CPsProduct::PROSPECT_PORTAL . '
						AND c.details IS NULL
						AND c.created_on >= \'2018-01-01 00:00:00\'
					ORDER BY c.id';

		return CChats::fetchChats( $strSql, $objChatDatabase );
	}

	public static function fetchAllChatsClients( $objChatDatabase ) {
		$strSql = 'SELECT DISTINCT cid FROM chats';
		return fetchData( $strSql, $objChatDatabase );
	}

	public static function fetchAsyncChatsToEventByCids( $arrintCids, $objChatDatabase, $intRecordLimit ) {
		if( false == valArr( $arrintCids ) ) return NULL;

		$strStartInterval = '2019-01-01 00:00:00';
		$strSql = 'SELECT 
						*
					FROM 
						chats
					WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND details->\'is_synced\' IS NULL
						AND created_on BETWEEN ( \'' . $strStartInterval . '\' ) AND ( NOW() )
						LIMIT ' . ( int ) $intRecordLimit;

		return CChats::fetchChats( $strSql, $objChatDatabase );
	}

	public static function fetchChatsByChatStatusTypeIdsByDuration( $arrintChatStatusTypeIds, $strStartInterval, $strEndInterval, $objChatDatabase ) {
		if( false == valArr( $arrintChatStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
							*
					FROM
						chats
					WHERE
						chat_status_type_id IN ( ' . implode( ',', $arrintChatStatusTypeIds ) . ' ) AND
						created_on BETWEEN ( NOW() - INTERVAL \'' . $strStartInterval . '\' ) AND ( NOW() - INTERVAL \'' . $strEndInterval . '\' )';

		return self::fetchChats( $strSql, $objChatDatabase );
	}

}
?>