<?php

class CSystemMonitor extends CBaseSystemMonitor {

    public static $c_intMonitoringTime = 1;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScriptName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDatabaseName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valServerDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastActiveOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>