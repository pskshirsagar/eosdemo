<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChatLines
 * Do not add any new functions to this class.
 */

class CChatLines extends CBaseChatLines {

	public static function fetchPaginatedChatLinesByChatId( $intChatId, $intPageNo, $intPageSize, $objDatabase ) {

		$intOffset 	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

	    $strSql = 'SELECT
	    				 *
	    			 FROM
	    			 	 chat_lines
	    			WHERE
	    				 chat_id = ' . ( int ) $intChatId;

		$strSql .= ' ORDER BY id OFFSET ' . ( int ) $intOffset . '
					 LIMIT ' . ( int ) $intPageSize;

		return self::fetchChatLines( $strSql, $objDatabase );
	}

	public static function fetchChatLineCountByChatId( $intChatId, $objDatabase ) {

		$strWhere = 'WHERE chat_id = ' . ( int ) $intChatId;

		return self::fetchChatLineCount( $strWhere, $objDatabase );
	}

	public static function fetchRecentChatLinesByChatLineIdByChatIdByCid( $intChatLineId, $intChatId, $intCid, $boolIsAdmin, $objDatabase ) {
		$strQuery = 'SELECT id, line_text, is_admin
					 FROM  chat_lines
					 WHERE chat_id = ' . ( int ) $intChatId . '
					 	AND id > ' . ( int ) $intChatLineId . '
					 	AND is_admin = ' . ( int ) $boolIsAdmin . '
					 	AND id <> ' . ( int ) $intChatLineId . '
					 	AND cid = ' . ( int ) $intCid . '
					 ORDER BY id ASC';

		return fetchData( $strQuery, $objDatabase );
	}

	public static function fetchChatLinesByCidByChatId( $intCid, $intChatId, $objDatabase ) {
		$strQuery = 'SELECT id, line_text, is_admin
					 FROM  chat_lines
					 WHERE chat_id = ' . ( int ) $intChatId . '
					 	AND cid = ' . ( int ) $intCid . '
					 ORDER BY id ASC';

		return fetchData( $strQuery, $objDatabase );
	}

	public static function fetchOrderedChatLinesByChatId( $intChatId, $objDatabase ) {
    	$strSql = 'SELECT
    					*
    			    FROM
    					 chat_lines
    			    WHERE
    			     	 chat_id = ' . ( int ) $intChatId . '
					ORDER BY created_on ASC ';

    	return self::fetchChatLines( $strSql, $objDatabase );
    }

    public static function fetchChatLinesByChatId( $intChatId, $objDatabase ) {
    	return self::fetchChatLines( sprintf( 'SELECT * FROM chat_lines WHERE chat_id = %d', ( int ) $intChatId ), $objDatabase );
    }

	public static function fetchChatLinesByChatIds( $arrintChatIds, $objChatDatabase ) {
		if( false == valArr( $arrintChatIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
							*
					FROM
						chat_lines
					WHERE
						chat_id IN ( ' . implode( ',', $arrintChatIds ) . ' )';

		return self::fetchChatLines( $strSql, $objChatDatabase );
	}

}
?>