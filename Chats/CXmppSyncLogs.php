<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CXmppSyncLogs
 * Do not add any new functions to this class.
 */

class CXmppSyncLogs extends CBaseXmppSyncLogs {

	public static function fetchXmppSyncLogsByCreatedOnByCallIds( $strCreatedOn, $arrstrCallIds, $objChatDatabase ) {

		$strSql = 'SELECT
						xsl.node,
						xsl.node_type,
						REPLACE(split_part( split_part( node, \',\', 1), \':\',2 ),\'"\', \'\') as call_id
					FROM xmpp_sync_logs xsl
					WHERE
						xsl.created_on BETWEEN \'' . $strCreatedOn . '\' AND ( \'' . $strCreatedOn . '\'::timestamp + INTERVAL \'30 minutes\' ) 
						AND xsl.node_type = \'XMPP_POPUP_CALL_LOG\' 
						AND REPLACE(split_part( split_part( node, \',\', 1), \':\',2 ),\'"\', \'\') IN ( \'' . implode( '\',\'', $arrstrCallIds ) . '\' )';

		return fetchData( $strSql, $objChatDatabase );
	}

}
?>