<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChatRoutings
 * Do not add any new functions to this class.
 */

class CChatRoutings extends CBaseChatRoutings {

	public static function fetchChatRoutingByChatIdByEmployeeId( $intChatId, $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						 *
					FROM
						chat_routings
					WHERE
						chat_id = ' . ( int ) $intChatId . '
						AND employee_id = ' . ( int ) $intEmployeeId . '
						AND end_datetime IS NULL';

		return parent::fetchChatRouting( $strSql, $objDatabase );
	}

	public static function fetchChatRoutingsByChatIdByIsChatRouted( $intChatId, $boolIsChatRouted, $objDatabase ) {

		$strCondition = NULL;

		if( true == $boolIsChatRouted ) {
			$strCondition .= ' AND updated_on > ( SELECT
													updated_on
												FROM
													chat_routings
												WHERE
													chat_id = ' . ( int ) $intChatId . '
													AND agent_session_id IS NOT NULL
											)';
		}

		$strSql = 'SELECT
						*
					FROM
						chat_routings
					WHERE
						chat_id = ' . ( int ) $intChatId . ' 
					AND end_datetime IS NOT NULL' . $strCondition;

		return parent::fetchChatRoutings( $strSql, $objDatabase );

	}

}
?>