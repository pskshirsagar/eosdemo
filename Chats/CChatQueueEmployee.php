<?php

class CChatQueueEmployee extends CBaseChatQueueEmployee {

	Protected $m_strChatQueueNames;
	Protected $m_intChatQueueCount;

	const MIN_CHAT_PRIORITY = 5;

	public function setChatQueueNames( $strChatQueueNames ) {
		$this->m_strChatQueueNames = $strChatQueueNames;
	}

	public function setChatQueueCount( $intChatQueueCount ) {
		$this->m_intChatQueueCount = $intChatQueueCount;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( true == isset( $arrmixValues['chat_queue_names'] ) )						$this->setChatQueueNames( $arrmixValues['chat_queue_names'] );
		if( true == isset( $arrmixValues['chat_queue_count'] ) )						$this->setChatQueueCount( $arrmixValues['chat_queue_count'] );
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );
	}

	public function getChatQueueNames() {
		return $this->m_strChatQueueNames;
	}

	public function getChatQueueCount() {
		return $this->m_intChatQueueCount;
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChatQueueId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmployeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>