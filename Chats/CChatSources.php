<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChatSources
 * Do not add any new functions to this class.
 */

class CChatSources extends CBaseChatSources {

	public static function fetchChatSources( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CChatSource::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchChatSource( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CChatSource::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>