<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CUserSessions
 * Do not add any new functions to this class.
 */

class CUserSessions extends CBaseUserSessions {

	public static function fetchActiveUserSessions( $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						user_sessions';

		return self::fetchUserSessions( $strSql, $objDatabase );
	}

	public static function fetchUserSessionByDatabaseId( $intDatabaseId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						user_sessions
					WHERE
						database_id = ' . ( int ) $intDatabaseId;

		return self::fetchUserSession( $strSql, $objDatabase );
	}

}
?>