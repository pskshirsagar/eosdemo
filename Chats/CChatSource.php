<?php

class CChatSource extends CBaseChatSource {

	const PROSPECT_PORTAL_LINK = 1;
	const PROSPECT_PORTAL_MCB = 2;
	const ENTRATA_SUPPORT = 3;
	const SMS_CHAT = 4;

	const CHAT_SOURCE_PROSPECT_PORTAL_LINK_NAME = 'Prospect Portal - Link';
	const CHAT_SOURCE_PROSPECT_PORTAL_MCB_NAME = 'Prospect Portal - MCB';
	const CHAT_SOURCE_ENTRATA_SUPPORT_NAME = 'Entrata Support';
	const CHAT_SOURCE_SMS_CHAT_NAME = 'SMS Text';

}
?>