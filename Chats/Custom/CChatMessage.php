<?php

class CChatMessage {
	protected $m_strMessageId;
	protected $m_strChannel;
	protected $m_strAgentName;
	protected $m_strReadOn;
	protected $m_strSentOn;
	protected $m_strMessageType;
	protected $m_strMessage;
	protected $m_arrobjAttachements;

	const MESSAGE_TYPE_ATTACHMENT = 'attachment';
	const MESSAGE_TYPE_TASK_DESCRIPTION = 'task_description';

	public function __construct( $objJsonMessage = NULL ) {

		$this->m_arrobjAttachements = [];

		if( NULL != $objJsonMessage ) {
			$this->m_strMessageId   = $objJsonMessage->message_id;
			$this->m_strChannel     = $objJsonMessage->channel ?? $objJsonMessage->side;
			$this->m_strAgentName   = $objJsonMessage->agent_name;
			$this->m_strReadOn      = $objJsonMessage->read_on ?? $objJsonMessage->seen_on ?? '';
			$this->m_strSentOn      = $objJsonMessage->sent_on;
			$this->m_strMessageType = $objJsonMessage->message_type ?? '';

			$mixMessage = CStrings::strToJson( $objJsonMessage->message );

			if( false == valObj( $mixMessage, stdClass::class ) ) {
				$this->m_strMessage = $objJsonMessage->message;
			} else {
				$this->m_strMessage = $mixMessage->message;
				$this->m_arrobjAttachements = isset( $mixMessage->attachments ) ? json_decode( $mixMessage->attachments ) : [];
				foreach( $this->m_arrobjAttachements as $objAttachment ) {
					if( 'task_description' == $this->m_strMessageType ) {
						$objAttachment->url = CConfig::get( 'secure_host_prefix' ) . 'clientadmin' . CConfig::get( 'rwx_login_suffix' ) . '/?module=task_attachment-new&action=view_attachment&task_attachment_id=' . $objAttachment->attachment_id;
					}
				}
			}

			if( self::MESSAGE_TYPE_ATTACHMENT === $this->m_strMessageType ) {
				$this->m_strMessage = '';
				$objAttachement = new stdClass();
				$objAttachement->file_type = $objJsonMessage->file_type;
				$objAttachement->name = 'View';
				$objAttachement->url = $objJsonMessage->message;
				$this->m_arrobjAttachements[] = $objAttachement;
			}

		}
	}

	/**
	 * getters
	 */
	public function getMessage() {
		return $this->m_strMessage;
	}

	public function getAttachements() {
		return $this->m_arrobjAttachements;
	}

	public function getChannel() {
		return $this->m_strChannel;
	}

	public function getReadOn() {
		$intReadOnTimestamp = NULL;
		if( true == is_numeric( $this->m_strReadOn ) ) {
			$intReadOnTimestamp = $this->m_strReadOn / 1000;
		} else {
			if( false == valStr( $this->m_strReadOn ) ) {
				return false;
			}
			$intReadOnTimestamp = strtotime( $this->m_strReadOn );
		}
		return date( 'h:i:s A', $intReadOnTimestamp );

	}

	public function getSentOn() {
		$intReadOnTimestamp = NULL;
		if( true == is_numeric( $this->m_strSentOn ) ) {
			$intReadOnTimestamp = $this->m_strSentOn / 1000;
		} else {
			$intReadOnTimestamp = strtotime( $this->m_strSentOn );
		}
		return date( 'h:i:s A', $intReadOnTimestamp );
	}

	public function getMessageId() {
		return $this->m_strMessageId;
	}

	public function getMessageType() {
		return $this->m_strMessageType;
	}

	public function getAgentName() {
		return $this->m_strAgentName;
	}

	/**
	 * setters
	 */
	public function setMessage( $strMessage ) {
		$this->m_strMessage = $strMessage;
	}

	public function setAttachements( $arrobjAttachements ) {
		$this->m_arrobjAttachements = $arrobjAttachements;
	}

	public function setChannel( $strChannel ) {
		$this->m_strChannel = $strChannel;
	}

	public function setReadOn( $strReadOn ) {
		$this->m_strReadOn = $strReadOn;
	}

	public function setSentOn( $strSentOn ) {
		$this->m_strSentOn = $strSentOn;
	}

	public function setMessageId( $strMessageId ) {
		$this->m_strMessageId = $strMessageId;
	}

	public function setMessageType( $strMessageType ) {
		$this->m_strMessageType = $strMessageType;
	}

	public function setAgentName( $strAgentName ) {
		$this->m_strAgentName = $strAgentName;
	}



	public function toArray() {
		$arrMessage = [
			'message' => $this->m_strMessage,
			'attachments' => $this->m_arrobjAttachements,
		];

		return [
			'message_id' => $this->m_strMessageId,
			'channel' => $this->m_strChannel,
			'agent_name' => $this->m_strAgentName,
			'read_on' => $this->m_strReadOn,
			'sent_on' => $this->m_strSentOn,
			'message_type' => $this->m_strMessageType,
			'message' => $arrMessage,
		];
	}

}
