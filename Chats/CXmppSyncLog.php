<?php

class CXmppSyncLog extends CBaseXmppSyncLog {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNodeType() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>