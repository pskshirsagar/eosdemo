<?php

class CChat extends CBaseChat {

	protected $m_intApplicationId;
	protected $m_intApplicantId;
	protected $m_intTaskId;

	protected $m_strChatUserName;
	protected $m_strChatUserResourceId;
	protected $m_strChatUserEmail;
	protected $m_strChatUserPhoneNumber;
	protected $m_strChatAgentUserName;
	protected $m_strChatAgentResourceId;
	protected $m_intEmployeeId;
	protected $m_strChatDuration;
	protected $m_intChatResponseTime;
	protected $m_strCompanyName;
	protected $m_strPropertyName;
	protected $m_strPropertyAddress;
	protected $m_strChatResultName;

	protected $m_objChatLine;

	protected $m_boolIsEndedByAgent;
	protected $m_boolIsLeasingCenterChat;

	const IS_LEGACY_CHAT = false;

	const CHAT_TYPE_LEASING = 'Prospect Portal';
	const CHAT_TYPE_TICKET = 'Ticket Chat';

	const CHAT_STATUS_COMPLETED	= '4';
	const CHAT_STATUS_MISSED	= '3';
	const CHAT_STATUS_OFFLINE_MESSAGE	= '7';

	const ALL_CHATS = 'ALL_CHATS';
	const NO_USER_AVAILABLE = 'NO_USER_AVAILABLE';
	const REDIRECT_TO_LC = 'REDIRECT_TO_LC';

	public function setApplicationId( $intApplicationId ) {
		$this->addToDetails( 'application_id', $intApplicationId );
		$this->m_intApplicationId = $intApplicationId;
	}

	public function setApplicantId( $intApplicantId ) {
		$this->addToDetails( 'applicant_id', $intApplicantId );
		$this->m_intApplicantId = $intApplicantId;
	}

	public function setChatUserName( $strChatUserName ) {
		$this->addToDetails( 'chat_user_name', $strChatUserName );
		$this->m_strChatUserName = $strChatUserName;
	}

	public function setChatUserResourceId( $strChatUserResourceId ) {
		$this->addToDetails( 'chat_user_resource_id', $strChatUserResourceId );
		$this->m_strChatUserResourceId = $strChatUserResourceId;
	}

	public function setChatAgentUserName( $strChatAgentUserName ) {
		$this->addToDetails( 'chat_agent_user_name', $strChatAgentUserName );
		$this->m_strChatAgentUserName = $strChatAgentUserName;
	}

	public function setChatAgentResourceId( $strChatAgentResourceId ) {
		$this->addToDetails( 'chat_agent_resource_id', $strChatAgentResourceId );
		$this->m_strChatAgentResourceId = $strChatAgentResourceId;
	}

	public function setChatUserEmail( $strChatUserEmail ) {
		$this->addToDetails( 'chat_user_email', $strChatUserEmail );
		$this->m_strChatUserEmail = $strChatUserEmail;
	}

	public function setChatUserPhoneNumber( $strChatUserPhoneNumber ) {
		$this->m_strChatUserPhoneNumber = $strChatUserPhoneNumber;
		$this->addToDetails( 'chat_user_phone_number', $this->m_strChatUserPhoneNumber );
	}

	public function setIsEndedByAgent( $boolIsEndedByAgent ) {
		$this->m_boolIsEndedByAgent = $boolIsEndedByAgent;
		$this->addToDetails( 'is_ended_by_agent', $this->m_boolIsEndedByAgent );
	}

	public function setTaskId( $intTaskId ) {
		$this->m_intTaskId = $intTaskId;
		$this->addToDetails( 'task_id', $this->m_intTaskId );
	}

	public function setIsLeasingCenterChat( $boolIsLeasingCenterChat ) {
		$this->m_boolIsLeasingCenterChat = $boolIsLeasingCenterChat;
		$this->addToDetails( 'is_leasing_center_chat', $this->m_boolIsLeasingCenterChat );
	}

	public function setChatLine( $objChatLine ) {
		$this->m_objChatLine = $objChatLine;
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function setChatDuration( $strChatDuration ) {
		$this->m_strChatDuration = $strChatDuration;
	}

	public function setChatResponseTime( $intChatResponseTime ) {
		$this->m_intChatResponseTime = $intChatResponseTime;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
		$this->addToDetails( 'property_name', $this->m_strPropertyName );
	}

	public function setPropertyAddress( $strPropertyAddress ) {
		$this->m_strPropertyAddress = $strPropertyAddress;
		$this->addToDetails( 'property_address', $this->m_strPropertyAddress );
	}

	public function setChatResultName( $strChatResultName ) {
		$this->m_strChatResultName = $strChatResultName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( true == isset( $arrmixValues['task_id'] ) )					$this->setTaskId( $arrmixValues['task_id'] );
		if( true == isset( $arrmixValues['application_id'] ) )			$this->setApplicationId( $arrmixValues['application_id'] );
		if( true == isset( $arrmixValues['applicant_id'] ) )			$this->setApplicantId( $arrmixValues['applicant_id'] );

		if( true == isset( $arrmixValues['chat_user_name'] ) )			$this->setChatUserName( $arrmixValues['chat_user_name'] );
		if( true == isset( $arrmixValues['chat_user_resource_id'] ) )	$this->setChatUserResourceId( $arrmixValues['chat_user_resource_id'] );

		if( true == isset( $arrmixValues['chat_agent_user_name'] ) )	$this->setChatAgentUserName( $arrmixValues['chat_agent_user_name'] );
		if( true == isset( $arrmixValues['chat_agent_resource_id'] ) )	$this->setChatAgentResourceId( $arrmixValues['chat_agent_resource_id'] );

		if( true == isset( $arrmixValues['chat_user_email'] ) )		$this->setChatUserEmail( $arrmixValues['chat_user_email'] );
		if( true == isset( $arrmixValues['chat_user_phone_number'] ) )	$this->setChatUserPhoneNumber( $arrmixValues['chat_user_phone_number'] );
		if( true == isset( $arrmixValues['is_ended_by_agent'] ) )		$this->setIsEndedByAgent( $arrmixValues['is_ended_by_agent'] );
		if( true == isset( $arrmixValues['employee_id'] ) ) 			$this->setEmployeeId( $arrmixValues['employee_id'] );
		if( true == isset( $arrmixValues['chat_duration'] ) ) 			$this->setChatDuration( $arrmixValues['chat_duration'] );
		if( true == isset( $arrmixValues['chat_response_time'] ) )		$this->setChatResponseTime( $arrmixValues['chat_response_time'] );
		if( true == isset( $arrmixValues['chat_result_name'] ) )		$this->setChatResultName( $arrmixValues['chat_result_name'] );

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

	}

	public function getApplicationId() {
		return $this->m_intApplicationId = $this->getFromDetails( 'application_id' );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId = $this->getFromDetails( 'applicant_id' );
	}

	public function getChatUserName() {
		return $this->m_strChatUserName = $this->getFromDetails( 'chat_user_name' );
	}

	public function getChatUserResourceId() {
		return $this->m_strChatUserResourceId = $this->getFromDetails( 'chat_user_resource_id' );
	}

	public function getChatAgentUserName() {
		return $this->m_strChatAgentUserName = $this->getFromDetails( 'chat_agent_user_name' );
	}

	public function getChatUserEmail() {
		return $this->m_strChatUserEmail = $this->getFromDetails( 'chat_user_email' );
	}

	public function getChatUserPhoneNumber() {
		return $this->m_strChatUserPhoneNumber = $this->getFromDetails( 'chat_user_phone_number' );
	}

	public function getIsEndedByAgent() {
		return $this->m_boolIsEndedByAgent = $this->getFromDetails( 'is_ended_by_agent' );
	}

	public function getTaskId() {
		return $this->m_intTaskId = $this->getFromDetails( 'task_id' );
	}

	public function getIsLeasingCenterChat() {
		return $this->m_boolIsLeasingCenterChat = $this->getFromDetails( 'is_leasing_center_chat' );
	}

	public function getChatLine() {
		return $this->m_objChatLine;
	}

	public function getChatStatus() {
		$strChatStaus = '';
		if( true == valStr( $this->getChatStartDatetime() ) && false == valStr( $this->getChatEndDatetime() ) && true == valId( $this->getCompanyUserId() ) ) {
			$strChatStaus = self::CHAT_STATUS_ACTIVE;
		} else if( false == valStr( $this->getChatStartDatetime() ) && true == valStr( $this->getChatEndDatetime() ) ) {
			$strChatStaus = self::CHAT_STATUS_MISSED;
		} else if( true == valStr( $this->getChatStartDatetime() ) && true == valStr( $this->getChatEndDatetime() ) ) {
			$strChatStaus = self::CHAT_STATUS_RECIVED;
		} else {
			$strChatStaus = NULL;
		}
		return $strChatStaus;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function getChatDuration() {
		return $this->m_strChatDuration;
	}

	public function getChatResponseTime() {
		return $this->m_intChatResponseTime;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName = $this->getFromDetails( 'property_name' );
	}

	public function getPropertyAddress() {
		return $this->m_strPropertyAddress = $this->getFromDetails( 'property_address' );
	}

	public function getChatResultName() {
		return $this->m_strChatResultName;
	}

	public function getChatRoom() {
		return sprintf( 'chat_%s', $this->m_intId );
	}

	public function getChatQueueRoomName() {
		return CChatQueue::CHAT_ROOM_PREFIX . '_' . $this->getChatQueueId();
	}

	public function addChatActivityLog( $intChatStatusType, $arrmixParams ) {
		$arrmixChatActivityLogs		= ( array ) json_decode( json_encode( $this->getChatActivityLogs() ), true );
		end( $arrmixChatActivityLogs );
		$intChatActivityLogKey		= key( $arrmixChatActivityLogs );

		if( true == isset( $intChatActivityLogKey ) ) {
			$arrmixChatActivity			= $arrmixChatActivityLogs[$intChatActivityLogKey];
		}

		switch( $intChatStatusType ) {
			case CChatStatusType::PENDING:
				$arrmixChatActivity		= [ 'initiated_on' => time(), 'is_from_guest_card' => $arrmixParams['is_from_guest_card'], 'available_user_ids' => $arrmixParams['available_user_ids'] ];
				$intChatActivityLogKey	= time();
				break;

			case CChat::REDIRECT_TO_LC:
				$arrmixChatActivity['redirect_to_lc'] = time();
				break;

			case CChatStatusType::CHAT_IN_PROGRESS:
				$arrmixChatActivity['accepted_by'] = $this->m_intUserId;
				$arrmixChatActivity['accepted_on'] = time();
				break;

			case CChatStatusType::MISSED:
				$arrmixChatActivity['missed_on'] = time();
				break;

			case CChatStatusType::COMPLETED:
				$arrmixChatActivity['completed_on'] = time();
			default:
				// nothing to do
		}
		$arrmixChatActivityLogs[$intChatActivityLogKey] = $arrmixChatActivity;
		$this->setChatActivityLogs( $arrmixChatActivityLogs );

	}

	/**
	 * Create Functions
	 */

	public function createChatLine( $strLineText, $boolIsAdmin, $objDatabase ) {

		$objChatLine = new CChatLine();
		$objChatLine->setCid( $this->getCid() );
		$objChatLine->setChatId( $this->getId() );
		$objChatLine->setLineText( $strLineText );
		$objChatLine->setLineDatetime( 'NOW()' );
		$objChatLine->setIsAdmin( $boolIsAdmin );

		if( true == $objChatLine->insert( CUser::ID_SYSTEM, $objDatabase ) ) {
			return true;
		}

		return false;
	}

	public function createChatConversation( $strJsonConversation, $strLineText = '' ) {

		if( false == valStr( $strLineText ) ) {
			return false;
		}

		$objChatLine = new CChatLine();
		$objChatLine->setCid( $this->getCid() );
		$objChatLine->setChatId( $this->getId() );
		$objChatLine->setRowText( $strJsonConversation );
		$objChatLine->setLineText( $strLineText );
		return $objChatLine;
	}

	public function createChatBuffer( $arrmixTicketData ) {

		if( false == valArr( $arrmixTicketData ) ) return false;

		$objChatBuffer = new CChatBuffer();
		$objChatBuffer->setChatId( $this->getId() );
		$objChatBuffer->setTicketId( getArrayElementByKey( 'ticket_id', $arrmixTicketData ) );
		$objChatBuffer->setResourceId( getArrayElementByKey( 'resource_id', $arrmixTicketData ) );
		$objChatBuffer->setLevel( CChatBuffer::LEVEL_BEGINNER );

		return $objChatBuffer;
	}

	public function createEventChat() {
		$objEventChat = new CEventChat();
		$objEventChat->setChatId( $this->m_intId );
		$objEventChat->setCid( $this->m_intCid );
		$objEventChat->setPropertyId( $this->m_intPropertyId );
		$objEventChat->setCompanyUserId( $this->m_intCompanyUserId );
		$objEventChat->setUserId( $this->m_intUserId );
		$objEventChat->setChatSourceId( $this->m_intChatSourceId );
		$objEventChat->setChatQueueId( $this->m_intChatQueueId );
		$objEventChat->setChatStatusTypeId( $this->m_intChatStatusTypeId );
		$objEventChat->setChatStartDatetime( $this->m_strChatStartDatetime );
		$objEventChat->setChatEndDatetime( $this->m_strChatEndDatetime );
		$objEventChat->setIsOnSiteAnswered( ( bool ) ( false == $this->getIsLeasingCenterChat() && true == isset( $this->m_intCompanyUserId ) ) );

		return $objEventChat;
	}

	/**
	 * Build Functions.
	 */
	public function buildChatNotification() {

		if( CChatSource::SMS_CHAT == $this->getChatSourceId() ) {
			$arrstrNotification = [
				'accept_chat_url'		=> CONFIG_SECURE_HOST_PREFIX . 'clientadmin.' . CONFIG_COMPANY_BASE_DOMAIN . '/?module=clients-new&action=login_user&client[id]=' . $this->m_intCid . '&return_url=' . urlencode( '/?module=leasing_center_dashboard_systemxxx' ),
				'applicant_id'			=> $this->getApplicantId(),
				'application_id'		=> $this->getApplicationId(),
				'customer_id'			=> $this->getCustomerId(),
				'company_name'			=> $this->getCompanyName(),
				'property_name'			=> $this->getPropertyName(),
				'chat_id'				=> $this->getId()
			];

			return $arrstrNotification;
		}

		$arrstrProductNames = CPsProduct::createService()->getProductNames();

		$arrmixChatNotificationDetails['chat_type_id']		= self::CHAT_TYPE_LEASING;
		$arrmixChatNotificationDetails['level']		= CChatBuffer::LEVEL_BEGINNER;

		if( CChatSource::ENTRATA_SUPPORT == $this->getChatSourceId() ) {
			$arrmixChatNotificationDetails['ticket_id']	= $this->getTaskId();
			$arrmixChatNotificationDetails['level']		= $this->getSkillLevel();
			$arrmixChatNotificationDetails['chat_type_id']	= ( false == valId( $this->getTaskId() ) ? self::CHAT_TYPE_LEASING : self::CHAT_TYPE_TICKET );
			$arrmixChatNotificationDetails['ticket_url']	= CONFIG_SECURE_HOST_PREFIX . 'clientadmin.' . CONFIG_COMPANY_BASE_DOMAIN . '/?module=task&action=view_task&task[id]=' . $this->getTaskId();
		}

		$arrmixChatNotificationDetails['chat_id']			= $this->getId();
		$arrmixChatNotificationDetails['cid']				= $this->getCid();
		$arrmixChatNotificationDetails['ps_product_name']	= $arrstrProductNames[$this->getPsProductId()];
		$arrmixChatNotificationDetails['accept_chat_url']	= '/?module=leasing_center_chat&action=leasing_center_chat_interface&chat[id]=' . $this->getId();

		return $arrmixChatNotificationDetails;
	}

	/**
	 * Validate Functions
	 */

	public function validate( $strAction, $objChatDatabase = NULL ) {
		$boolIsValid = true;

		if( true == valArr( $this->m_arrobjErrorMsgs ) ) {
			$boolIsValid = false;
		}

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'initiate_chat':
				$boolIsValid &= $this->validateInitiateChat();
				break;

			case 'initiate_prospect_portal_chat':
				$boolIsValid &= $this->validateInitiateProspectPortalChat();
				break;

			case 'initiate_entrata_support_chat':
				$boolIsValid &= $this->validateInitiateEntrataSupportChat();
				break;

			case 'route_chat':
				$boolIsValid &= $this->validateRouteChat( $objChatDatabase );
				break;

			case 'accept_chat':
				$boolIsValid &= $this->validateAcceptChat( $objChatDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function validateInitiateChat() {
		$boolIsValid = true;

		if( false === valId( $this->m_intPsProductId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product is not mentioned.' ) );
			$boolIsValid = false;
		}

		if( true == $boolIsValid && false === valStr( $this->getChatUserName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'chat_user_name', 'Chat user name is not mentioned.' ) );
			$boolIsValid = false;
		}

		if( true == $boolIsValid && false === valStr( $this->getChatUserResourceId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'chat_user_resource_id', 'Chat user does not have resource id.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validateInitiateProspectPortalChat() {
		$boolIsValid = true;

		if( false == valId( $this->m_intCid ) ) {
			$this->addErrorMsgs( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client is required.' ) );
			$boolIsValid = false;
		}

		if( true == $boolIsValid && false == valId( $this->m_intPropertyId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
			$boolIsValid = false;
		}

		if( true == $boolIsValid && false == valId( $this->m_intApplicantId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applicant_id', 'Applicant is required.' ) );
			$boolIsValid = false;
		}

		if( true == $boolIsValid && false == valId( $this->m_intApplicationId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_id', 'Application is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validateInitiateEntrataSupportChat() {
		$boolIsValid = true;

		if( false == valId( $this->m_intCid ) ) {
			$this->addErrorMsgs( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client is required.' ) );
			$boolIsValid = false;
		}

		if( true == $boolIsValid && false == valId( $this->m_intTaskId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_id', 'Task is required.' ) );
			$boolIsValid = false;
		}

		if( true == $boolIsValid && false == valId( $this->m_intCompanyUserId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_user_id', 'Company user is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;

	}

	public function validateRouteChat( $objChatDatabase ) {
		$boolIsValid = true;

		if( false == valId( $this->m_intChatQueueId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'chat_queue_id', 'Chat queue is not found.' ) );
			$boolIsValid &= false;
		}

		if( false == CChats::fetchChatCount( ' WHERE id = ' . $this->getId() . ' AND chat_status_type_id IN ( ' . implode( ',', [ CChatStatusType::INITIATED, CChatStatusType::PENDING ] ) . ' )', $objChatDatabase ) ) {
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validateBeforeAcceptChat() {
		$boolIsValid = true;

		if( CChatStatusType::PENDING !== $this->m_intChatStatusTypeId ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'chat_status_type_id', 'Chat has been ended or accepted by another user.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;

	}

	public function validateAcceptChat( $objChatDatabase ) {
		$boolIsValid = true;

		if( false == valId( $this->m_intUserId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'Chat agent is not found.' ) );
			$boolIsValid &= false;
		}

		if( false == valStr( $this->m_strChatAgentUserName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'Chat agent name is not found.' ) );
			$boolIsValid &= false;
		}

		if( false == valStr( $this->m_strChatAgentResourceId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'Chat agent resource is not valid.' ) );
			$boolIsValid &= false;
		}

		if( false == \Psi\Eos\Chats\CChats::createService()->fetchChatCount( 'WHERE id = ' . $this->getId() . ' AND chat_status_type_id = ' . CChatStatusType::PENDING, $objChatDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'chat_status_type_id', 'Chat has been ended or accepted by another user.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchChatLines( $objDatabase ) {
		return \Psi\Eos\Chats\CChatLines::createService()->fetchChatLinesByChatId( $this->getId(), $objDatabase );
	}

	public function fetchOrderedChatLines( $objDatabase ) {
		return \Psi\Eos\Chats\CChatLines::createService()->fetchOrderedChatLinesByChatId( $this->getId(), $objDatabase );
	}

	public function fetchRecentChatLines( $intChatLineId, $boolIsAdmin, $objDatabase ) {

		return \Psi\Eos\Chats\CChatLines::createService()->fetchRecentChatLinesByChatLineIdByChatIdByCid( $intChatLineId, $this->getId(), $this->getCid(), $boolIsAdmin, $objDatabase );
	}

	public function fetchPaginatedChatLines( $intPageNo, $intPageSize, $objDatabase ) {
		return \Psi\Eos\Chats\CChatLines::createService()->fetchPaginatedChatLinesByChatId( $this->getId(), $intPageNo, $intPageSize, $objDatabase );
	}

	public function fetchChatLineCount( $objDatabase ) {
		return \Psi\Eos\Chats\CChatLines::createService()->fetchChatLineCountByChatId( $this->getId(), $objDatabase );
	}

	public function fetchCompanyEmployee( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByCompanyUserIdByCid( $this->getCompanyUserId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplication( $objDatabase ) {
		return CApplications::fetchApplicationByEventTypeIdByDataReferenceIdByCid( CEventType::ONLINE_CHAT, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicantByApplicationId( $intApplicationId, $objDatabase ) {
		$arrobjApplicants = CApplicants::fetchApplicantsByApplicationIdByCid( $intApplicationId, $this->getCid(), $objDatabase );
		return ( true == valArr( $arrobjApplicants ) ) ? array_pop( $arrobjApplicants ) : NULL;
	}

	public function fetchEvent( $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchActiveEventByDataReferenceIdByEventTypeIdByCid( $this->getId(), CEventType::ONLINE_CHAT, $this->getCid(), $objDatabase );
	}

	public function fetchClient( $objDatabase ) {
		return CClients::fetchClientById( $this->getCid(), $objDatabase );
	}

	public function fetchDefaultChatQueue( $objChatDatabase ) {

		if( CChatSource::ENTRATA_SUPPORT == $this->getChatSourceId() ) {
			return \Psi\Eos\Chats\CChatQueues::createService()->fetchChatQueueById( CChatQueue::ENTRATA_FIRST_RESPONSE, $objChatDatabase );
		}

		return \Psi\Eos\Chats\CChatQueues::CreateService()->fetchChatQueueById( CChatQueue::LEASING_CENTER, $objChatDatabase );
	}

	public function fetchChatQueue( $objChatDatabase ) {

		$objChatQueue = \Psi\Eos\Chats\CChatQueues::createService()->fetchChatQueueByPsProductId( $this->getPsProductId(), $this->getSkillLevel(), $objChatDatabase );

		if( true == valObj( $objChatQueue, 'CChatQueue' ) ) {
			return $objChatQueue;
		}

		return $this->fetchDefaultChatQueue( $objChatDatabase );
	}

	public function insert( $intUserId, $objDatabase, $arrmixTicketData = array() ) {

		if( false == valArr( $arrmixTicketData ) ) {

			// return true if already exists.
			if( false == is_null( $this->getId() ) ) {
				return $this;
			}
			if( true == parent::insert( $intUserId, $objDatabase ) ) {
				return true;
			}
		} elseif( true == parent::insert( $intUserId, $objDatabase ) ) {
			// CHAT FROM ENTRATA TO CLIENT ADMIN
			if( true == valObj( $objChatBuffer = $this->createChatBuffer( $arrmixTicketData ), 'CChatBuffer' ) ) {
				return $objChatBuffer->insert( $intUserId, $objDatabase );
			}
		}
		return false;
	}

	public function sendChatRouteMessage() {

		$objChatRouteMessage = new CChatRouteMessage();
		$objChatRouteMessage->fromArray( [
			'cluster_id'	=> CCluster::RAPID,
			'user_id'		=> $this->getCreatedBy(),
			'chat_id'		=> $this->getId(),
			'cluster_name'	=> CCluster::getClusterNameByClusterId( CCluster::RAPID )
		] );

		$objChatRouteMessage->setMessageProperties( [ 'priority' => 1 ] );

		try {
			$objEntrataMessageSender = ( new Psi\Libraries\Queue\Factory\CMessageSenderFactory() )->createEntrataSender( new \Psi\Libraries\Queue\Factory\CAmqpMessageFactory(), CChatRouteMessage::MESSAGE_TYPE,  CCluster::getClusterNameByClusterId( CCluster::RAPID ) );
			$objEntrataMessageSender->send( $objChatRouteMessage );
		} catch( Exception $objException ) {
			return false;
		}

		return true;
	}

	public function createChatRouting() {
		$objChatRouting = new CChatRouting();
		$objChatRouting->setChatId( $this->m_intId );
		$objChatRouting->setPsProductId( $this->m_intPsProductId );
		$objChatRouting->setStartDatetime( 'NOW()' );
		$objChatRouting->setEndDatetime( NULL );

		return $objChatRouting;
	}

	public static function executeCommand( $strCommand, $boolRunInBackground = false ) {
		$objSystemCommand = new CSystemCommand();
		$objSystemCommand->setExecutionTimeout( 0 );
		$objSystemCommand->execute( $strCommand, $boolRunInBackground );
		return getArrayElementByKey( 0, $objSystemCommand->getResponse() );
	}

	public function addToDetails( $strKey, $strValue ) {

		if( NULL != $this->m_strDetails ) {
			$this->m_jsonDetails = json_decode( $this->m_strDetails );
		}

		$arrmixDetails = ( array ) $this->m_jsonDetails;
		$arrmixDetails[$strKey] = $strValue;
		$this->m_jsonDetails = ( object ) $arrmixDetails;
		$this->m_strDetails = json_encode( $this->m_jsonDetails );
	}

	public function getFromDetails( $strKey ) {
		return $this->getDetailsField( $strKey );
	}

}
?>