<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CFastpathChats
 * Do not add any new functions to this class.
 */

class CFastpathChats extends CBaseFastpathChats {

	public static function fetchFastpathChatsByApplicationIdByPropertyIdByCid( $intApplicationId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						fastpath_chats
					WHERE
						application_id = ' . ( int ) $intApplicationId . '
					 AND
						property_id = ' . ( int ) $intPropertyId . '
					 AND
						cid = ' . ( int ) $intCid;

		return self::fetchFastpathChat( $strSql, $objDatabase );
	}
}
?>