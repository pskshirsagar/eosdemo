<?php

class CChatLine extends CBaseChatLine {

	protected $m_strOfflineMessage;

	public function getOfflineMessage() {
		return $this->m_strOfflineMessage = $this->getFromDetails( 'offline_message' );
	}

	public function getFromDetails( $strKey ) {
		return ( true == isset( $this->getRowText()->$strKey ) ? $this->getRowText()->$strKey : false );
	}

	public function getMessages() {
		$arrobjChatMessages = [];
		$arrobjJsonChatMessages = ( array ) $this->getRowText();

		foreach( $arrobjJsonChatMessages as $objJsonMessage ) {
			$arrobjChatMessages[] = new CChatMessage( $objJsonMessage );
		}
		return $arrobjChatMessages;
	}

}
?>