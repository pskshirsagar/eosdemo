<?php

class CFastpathChat extends CBaseFastpathChat {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        if( true == is_null( $this->getPropertyId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'proeprty_id', 'Property id is required.' ) );
        }
        return $boolIsValid;
    }

    public function valApplicantId() {
        $boolIsValid = true;
        if( true == is_null( $this->getApplicantId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applicant_id', 'Applicant id is required.' ) );
        }
        return $boolIsValid;
    }

    public function valApplicationId() {
        $boolIsValid = true;
        if( true == is_null( $this->getApplicationId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_id', 'Application id is required.' ) );
        }
        return $boolIsValid;
    }

    public function valOpenfireSessionId() {
        $boolIsValid = true;
        if( true == is_null( $this->getOpenfireSessionId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'openfire_sessio_id', 'Openfire session id is required.' ) );
        }
        return $boolIsValid;
    }

    public function valLeasingAgentName() {
        $boolIsValid = true;
        if( true == is_null( $this->getLeasingAgentName() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'leasing_agent_name', 'Leasing agent name is required.' ) );
        }
        return $boolIsValid;
    }

    public function valWebchatFormData() {
        $boolIsValid = true;
        if( true == is_null( $this->getWebchatFormData() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'webchat_form_data', 'Webchat form data can\'t be empty.' ) );
        }
        return $boolIsValid;
    }

    public function valChatLog() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChatStartDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChatEndDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valQueueWaitTime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valApplicantId();
            	$boolIsValid &= $this->valApplicationId();
            	$boolIsValid &= $this->valWebchatFormData();
            	$boolIsValid &= $this->valOpenfireSessionId();
            	$boolIsValid &= $this->valLeasingAgentName();
            	$boolIsValid &= $this->valCid();
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

           	default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function fetchFastPathChat( $objDatabase ) {
    	return CFastpathChats::fetchFastpathChatsByApplicationIdByPropertyIdByCid( $this->getApplicationId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
    }

    public function fetchApplicant( $objDatabase ) {
    	return CApplicants::fetchSimpleApplicantByIdByCid( $this->getApplicantId(), $this->getCid(), $objDatabase, $boolLoadFromView = false );
    }

    public function getFastpathChatForm( $objPropertyPreference = NULL ) {
    	if( false == defined( 'CONFIG_XMPP_ENABLED' ) || false == ( bool ) CONFIG_XMPP_ENABLED || false == valObj( $objPropertyPreference, 'CPropertyPreference' )
     	 || ( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && ( 'NONE' == trim( $objPropertyPreference->getValue() ) || true == is_null( $objPropertyPreference->getValue() ) ) ) ) return NULL;

    	// Considering if preference is set at least one user is logged in to xmpp fastpath server.
    	return true;
    }
}
?>