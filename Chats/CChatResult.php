<?php

class CChatResult extends CBaseChatResult {

	const LEAD = 1;
	const RESIDENT = 2;
	const WORK_ORDER = 3;
	const SOLICITOR = 4;
	const ABANDONED = 5;
	const WRONG_CHAT = 6;
	const VENDOR = 7;
	const NO_RESULT = 8;

	public static $c_arrintLeasingCenterOtherContactChatResults = [
		self::SOLICITOR		=> self::SOLICITOR,
		self::ABANDONED		=> self::ABANDONED,
		self::WRONG_CHAT	=> self::WRONG_CHAT,
		self::VENDOR		=> self::VENDOR,
		self::NO_RESULT		=> self::NO_RESULT
	];

	public static $c_arrintChatResultsNotToSendContactSubmissionEmail = [
		self::ABANDONED		=> self::ABANDONED,
		self::WRONG_CHAT	=> self::WRONG_CHAT,
		self::NO_RESULT		=> self::NO_RESULT
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>