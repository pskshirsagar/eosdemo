<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CSystemMonitors
 * Do not add any new functions to this class.
 */

class CSystemMonitors extends CBaseSystemMonitors {

	public static function fetchAllSystemMonitors( $objDatabase ) {
		return parent::fetchSystemMonitors( 'SELECT * FROM system_monitors', $objDatabase );
	}
}
?>