<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChats
 * Do not add any new functions to this class.
 */

class CBaseChats extends CEosPluralBase {

	/**
	 * @return CChat[]
	 */
	public static function fetchChats( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CChat::class, $objDatabase );
	}

	/**
	 * @return CChat
	 */
	public static function fetchChat( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChat::class, $objDatabase );
	}

	public static function fetchChatCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chats', $objDatabase );
	}

	public static function fetchChatById( $intId, $objDatabase ) {
		return self::fetchChat( sprintf( 'SELECT * FROM chats WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchChatsByCid( $intCid, $objDatabase ) {
		return self::fetchChats( sprintf( 'SELECT * FROM chats WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchChatsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchChats( sprintf( 'SELECT * FROM chats WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchChatsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchChats( sprintf( 'SELECT * FROM chats WHERE ps_product_id = %d', $intPsProductId ), $objDatabase );
	}

	public static function fetchChatsByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return self::fetchChats( sprintf( 'SELECT * FROM chats WHERE company_user_id = %d', $intCompanyUserId ), $objDatabase );
	}

	public static function fetchChatsByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchChats( sprintf( 'SELECT * FROM chats WHERE customer_id = %d', $intCustomerId ), $objDatabase );
	}

	public static function fetchChatsByUserId( $intUserId, $objDatabase ) {
		return self::fetchChats( sprintf( 'SELECT * FROM chats WHERE user_id = %d', $intUserId ), $objDatabase );
	}

	public static function fetchChatsByPsLeadId( $intPsLeadId, $objDatabase ) {
		return self::fetchChats( sprintf( 'SELECT * FROM chats WHERE ps_lead_id = %d', $intPsLeadId ), $objDatabase );
	}

	public static function fetchChatsByTaskNoteId( $intTaskNoteId, $objDatabase ) {
		return self::fetchChats( sprintf( 'SELECT * FROM chats WHERE task_note_id = %d', $intTaskNoteId ), $objDatabase );
	}

	public static function fetchChatsByChatQueueId( $intChatQueueId, $objDatabase ) {
		return self::fetchChats( sprintf( 'SELECT * FROM chats WHERE chat_queue_id = %d', $intChatQueueId ), $objDatabase );
	}

	public static function fetchChatsByChatSourceId( $intChatSourceId, $objDatabase ) {
		return self::fetchChats( sprintf( 'SELECT * FROM chats WHERE chat_source_id = %d', $intChatSourceId ), $objDatabase );
	}

	public static function fetchChatsByChatStatusTypeId( $intChatStatusTypeId, $objDatabase ) {
		return self::fetchChats( sprintf( 'SELECT * FROM chats WHERE chat_status_type_id = %d', $intChatStatusTypeId ), $objDatabase );
	}

}
?>