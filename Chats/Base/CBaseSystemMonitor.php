<?php

class CBaseSystemMonitor extends CEosSingularBase {

	const TABLE_NAME = 'public.system_monitors';

	protected $m_intId;
	protected $m_strScriptName;
	protected $m_strDatabaseName;
	protected $m_strServerDetails;
	protected $m_strStartedOn;
	protected $m_strLastActiveOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['script_name'] ) && $boolDirectSet ) $this->set( 'm_strScriptName', trim( stripcslashes( $arrValues['script_name'] ) ) ); elseif( isset( $arrValues['script_name'] ) ) $this->setScriptName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['script_name'] ) : $arrValues['script_name'] );
		if( isset( $arrValues['database_name'] ) && $boolDirectSet ) $this->set( 'm_strDatabaseName', trim( stripcslashes( $arrValues['database_name'] ) ) ); elseif( isset( $arrValues['database_name'] ) ) $this->setDatabaseName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['database_name'] ) : $arrValues['database_name'] );
		if( isset( $arrValues['server_details'] ) && $boolDirectSet ) $this->set( 'm_strServerDetails', trim( stripcslashes( $arrValues['server_details'] ) ) ); elseif( isset( $arrValues['server_details'] ) ) $this->setServerDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['server_details'] ) : $arrValues['server_details'] );
		if( isset( $arrValues['started_on'] ) && $boolDirectSet ) $this->set( 'm_strStartedOn', trim( $arrValues['started_on'] ) ); elseif( isset( $arrValues['started_on'] ) ) $this->setStartedOn( $arrValues['started_on'] );
		if( isset( $arrValues['last_active_on'] ) && $boolDirectSet ) $this->set( 'm_strLastActiveOn', trim( $arrValues['last_active_on'] ) ); elseif( isset( $arrValues['last_active_on'] ) ) $this->setLastActiveOn( $arrValues['last_active_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScriptName( $strScriptName ) {
		$this->set( 'm_strScriptName', CStrings::strTrimDef( $strScriptName, 50, NULL, true ) );
	}

	public function getScriptName() {
		return $this->m_strScriptName;
	}

	public function sqlScriptName() {
		return ( true == isset( $this->m_strScriptName ) ) ? '\'' . addslashes( $this->m_strScriptName ) . '\'' : 'NULL';
	}

	public function setDatabaseName( $strDatabaseName ) {
		$this->set( 'm_strDatabaseName', CStrings::strTrimDef( $strDatabaseName, 50, NULL, true ) );
	}

	public function getDatabaseName() {
		return $this->m_strDatabaseName;
	}

	public function sqlDatabaseName() {
		return ( true == isset( $this->m_strDatabaseName ) ) ? '\'' . addslashes( $this->m_strDatabaseName ) . '\'' : 'NULL';
	}

	public function setServerDetails( $strServerDetails ) {
		$this->set( 'm_strServerDetails', CStrings::strTrimDef( $strServerDetails, 50, NULL, true ) );
	}

	public function getServerDetails() {
		return $this->m_strServerDetails;
	}

	public function sqlServerDetails() {
		return ( true == isset( $this->m_strServerDetails ) ) ? '\'' . addslashes( $this->m_strServerDetails ) . '\'' : 'NULL';
	}

	public function setStartedOn( $strStartedOn ) {
		$this->set( 'm_strStartedOn', CStrings::strTrimDef( $strStartedOn, -1, NULL, true ) );
	}

	public function getStartedOn() {
		return $this->m_strStartedOn;
	}

	public function sqlStartedOn() {
		return ( true == isset( $this->m_strStartedOn ) ) ? '\'' . $this->m_strStartedOn . '\'' : 'NULL';
	}

	public function setLastActiveOn( $strLastActiveOn ) {
		$this->set( 'm_strLastActiveOn', CStrings::strTrimDef( $strLastActiveOn, -1, NULL, true ) );
	}

	public function getLastActiveOn() {
		return $this->m_strLastActiveOn;
	}

	public function sqlLastActiveOn() {
		return ( true == isset( $this->m_strLastActiveOn ) ) ? '\'' . $this->m_strLastActiveOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, script_name, database_name, server_details, started_on, last_active_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlScriptName() . ', ' .
 						$this->sqlDatabaseName() . ', ' .
 						$this->sqlServerDetails() . ', ' .
 						$this->sqlStartedOn() . ', ' .
 						$this->sqlLastActiveOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' script_name = ' . $this->sqlScriptName() . ','; } elseif( true == array_key_exists( 'ScriptName', $this->getChangedColumns() ) ) { $strSql .= ' script_name = ' . $this->sqlScriptName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_name = ' . $this->sqlDatabaseName() . ','; } elseif( true == array_key_exists( 'DatabaseName', $this->getChangedColumns() ) ) { $strSql .= ' database_name = ' . $this->sqlDatabaseName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' server_details = ' . $this->sqlServerDetails() . ','; } elseif( true == array_key_exists( 'ServerDetails', $this->getChangedColumns() ) ) { $strSql .= ' server_details = ' . $this->sqlServerDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' started_on = ' . $this->sqlStartedOn() . ','; } elseif( true == array_key_exists( 'StartedOn', $this->getChangedColumns() ) ) { $strSql .= ' started_on = ' . $this->sqlStartedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_active_on = ' . $this->sqlLastActiveOn() . ','; } elseif( true == array_key_exists( 'LastActiveOn', $this->getChangedColumns() ) ) { $strSql .= ' last_active_on = ' . $this->sqlLastActiveOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'script_name' => $this->getScriptName(),
			'database_name' => $this->getDatabaseName(),
			'server_details' => $this->getServerDetails(),
			'started_on' => $this->getStartedOn(),
			'last_active_on' => $this->getLastActiveOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>