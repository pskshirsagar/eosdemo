<?php

class CBaseChatAttachment extends CEosSingularBase {

	const TABLE_NAME = 'public.chat_attachments';

	protected $m_intId;
	protected $m_intChatId;
	protected $m_strFilePath;
	protected $m_strFileName;
	protected $m_strThumbnailPath;
	protected $m_intFileSize;
	protected $m_strFileType;
	protected $m_strTitle;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['chat_id'] ) && $boolDirectSet ) $this->set( 'm_intChatId', trim( $arrValues['chat_id'] ) ); elseif( isset( $arrValues['chat_id'] ) ) $this->setChatId( $arrValues['chat_id'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( $arrValues['file_path'] ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( $arrValues['file_path'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( $arrValues['file_name'] ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( $arrValues['file_name'] );
		if( isset( $arrValues['thumbnail_path'] ) && $boolDirectSet ) $this->set( 'm_strThumbnailPath', trim( $arrValues['thumbnail_path'] ) ); elseif( isset( $arrValues['thumbnail_path'] ) ) $this->setThumbnailPath( $arrValues['thumbnail_path'] );
		if( isset( $arrValues['file_size'] ) && $boolDirectSet ) $this->set( 'm_intFileSize', trim( $arrValues['file_size'] ) ); elseif( isset( $arrValues['file_size'] ) ) $this->setFileSize( $arrValues['file_size'] );
		if( isset( $arrValues['file_type'] ) && $boolDirectSet ) $this->set( 'm_strFileType', trim( $arrValues['file_type'] ) ); elseif( isset( $arrValues['file_type'] ) ) $this->setFileType( $arrValues['file_type'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setChatId( $intChatId ) {
		$this->set( 'm_intChatId', CStrings::strToIntDef( $intChatId, NULL, false ) );
	}

	public function getChatId() {
		return $this->m_intChatId;
	}

	public function sqlChatId() {
		return ( true == isset( $this->m_intChatId ) ) ? ( string ) $this->m_intChatId : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, -1, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFilePath ) : '\'' . addslashes( $this->m_strFilePath ) . '\'' ) : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, -1, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileName ) : '\'' . addslashes( $this->m_strFileName ) . '\'' ) : 'NULL';
	}

	public function setThumbnailPath( $strThumbnailPath ) {
		$this->set( 'm_strThumbnailPath', CStrings::strTrimDef( $strThumbnailPath, -1, NULL, true ) );
	}

	public function getThumbnailPath() {
		return $this->m_strThumbnailPath;
	}

	public function sqlThumbnailPath() {
		return ( true == isset( $this->m_strThumbnailPath ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strThumbnailPath ) : '\'' . addslashes( $this->m_strThumbnailPath ) . '\'' ) : 'NULL';
	}

	public function setFileSize( $intFileSize ) {
		$this->set( 'm_intFileSize', CStrings::strToIntDef( $intFileSize, NULL, false ) );
	}

	public function getFileSize() {
		return $this->m_intFileSize;
	}

	public function sqlFileSize() {
		return ( true == isset( $this->m_intFileSize ) ) ? ( string ) $this->m_intFileSize : 'NULL';
	}

	public function setFileType( $strFileType ) {
		$this->set( 'm_strFileType', CStrings::strTrimDef( $strFileType, -1, NULL, true ) );
	}

	public function getFileType() {
		return $this->m_strFileType;
	}

	public function sqlFileType() {
		return ( true == isset( $this->m_strFileType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileType ) : '\'' . addslashes( $this->m_strFileType ) . '\'' ) : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, -1, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, chat_id, file_path, file_name, thumbnail_path, file_size, file_type, title, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlChatId() . ', ' .
						$this->sqlFilePath() . ', ' .
						$this->sqlFileName() . ', ' .
						$this->sqlThumbnailPath() . ', ' .
						$this->sqlFileSize() . ', ' .
						$this->sqlFileType() . ', ' .
						$this->sqlTitle() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chat_id = ' . $this->sqlChatId(). ',' ; } elseif( true == array_key_exists( 'ChatId', $this->getChangedColumns() ) ) { $strSql .= ' chat_id = ' . $this->sqlChatId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath(). ',' ; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' thumbnail_path = ' . $this->sqlThumbnailPath(). ',' ; } elseif( true == array_key_exists( 'ThumbnailPath', $this->getChangedColumns() ) ) { $strSql .= ' thumbnail_path = ' . $this->sqlThumbnailPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_size = ' . $this->sqlFileSize(). ',' ; } elseif( true == array_key_exists( 'FileSize', $this->getChangedColumns() ) ) { $strSql .= ' file_size = ' . $this->sqlFileSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_type = ' . $this->sqlFileType(). ',' ; } elseif( true == array_key_exists( 'FileType', $this->getChangedColumns() ) ) { $strSql .= ' file_type = ' . $this->sqlFileType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'chat_id' => $this->getChatId(),
			'file_path' => $this->getFilePath(),
			'file_name' => $this->getFileName(),
			'thumbnail_path' => $this->getThumbnailPath(),
			'file_size' => $this->getFileSize(),
			'file_type' => $this->getFileType(),
			'title' => $this->getTitle(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>