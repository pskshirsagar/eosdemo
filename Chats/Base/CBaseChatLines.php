<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChatLines
 * Do not add any new functions to this class.
 */

class CBaseChatLines extends CEosPluralBase {

	/**
	 * @return CChatLine[]
	 */
	public static function fetchChatLines( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CChatLine::class, $objDatabase );
	}

	/**
	 * @return CChatLine
	 */
	public static function fetchChatLine( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChatLine::class, $objDatabase );
	}

	public static function fetchChatLineCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chat_lines', $objDatabase );
	}

	public static function fetchChatLineById( $intId, $objDatabase ) {
		return self::fetchChatLine( sprintf( 'SELECT * FROM chat_lines WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchChatLinesByCid( $intCid, $objDatabase ) {
		return self::fetchChatLines( sprintf( 'SELECT * FROM chat_lines WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchChatLinesByChatId( $intChatId, $objDatabase ) {
		return self::fetchChatLines( sprintf( 'SELECT * FROM chat_lines WHERE chat_id = %d', $intChatId ), $objDatabase );
	}

}
?>