<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChatRoutings
 * Do not add any new functions to this class.
 */

class CBaseChatRoutings extends CEosPluralBase {

	/**
	 * @return CChatRouting[]
	 */
	public static function fetchChatRoutings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CChatRouting', $objDatabase );
	}

	/**
	 * @return CChatRouting
	 */
	public static function fetchChatRouting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CChatRouting', $objDatabase );
	}

	public static function fetchChatRoutingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chat_routings', $objDatabase );
	}

	public static function fetchChatRoutingById( $intId, $objDatabase ) {
		return self::fetchChatRouting( sprintf( 'SELECT * FROM chat_routings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchChatRoutingsByChatId( $intChatId, $objDatabase ) {
		return self::fetchChatRoutings( sprintf( 'SELECT * FROM chat_routings WHERE chat_id = %d', ( int ) $intChatId ), $objDatabase );
	}

	public static function fetchChatRoutingsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchChatRoutings( sprintf( 'SELECT * FROM chat_routings WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchChatRoutingsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchChatRoutings( sprintf( 'SELECT * FROM chat_routings WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchChatRoutingsByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return self::fetchChatRoutings( sprintf( 'SELECT * FROM chat_routings WHERE company_user_id = %d', ( int ) $intCompanyUserId ), $objDatabase );
	}

	public static function fetchChatRoutingsByCid( $intCid, $objDatabase ) {
		return self::fetchChatRoutings( sprintf( 'SELECT * FROM chat_routings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChatRoutingsByAgentSessionId( $strAgentSessionId, $objDatabase ) {
		return self::fetchChatRoutings( sprintf( 'SELECT * FROM chat_routings WHERE agent_session_id = \'%s\'', $strAgentSessionId ), $objDatabase );
	}

}
?>