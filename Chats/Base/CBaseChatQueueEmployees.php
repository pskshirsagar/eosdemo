<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChatQueueEmployees
 * Do not add any new functions to this class.
 */

class CBaseChatQueueEmployees extends CEosPluralBase {

	/**
	 * @return CChatQueueEmployee[]
	 */
	public static function fetchChatQueueEmployees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CChatQueueEmployee::class, $objDatabase );
	}

	/**
	 * @return CChatQueueEmployee
	 */
	public static function fetchChatQueueEmployee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChatQueueEmployee::class, $objDatabase );
	}

	public static function fetchChatQueueEmployeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chat_queue_employees', $objDatabase );
	}

	public static function fetchChatQueueEmployeeById( $intId, $objDatabase ) {
		return self::fetchChatQueueEmployee( sprintf( 'SELECT * FROM chat_queue_employees WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchChatQueueEmployeesByChatQueueId( $intChatQueueId, $objDatabase ) {
		return self::fetchChatQueueEmployees( sprintf( 'SELECT * FROM chat_queue_employees WHERE chat_queue_id = %d', $intChatQueueId ), $objDatabase );
	}

	public static function fetchChatQueueEmployeesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchChatQueueEmployees( sprintf( 'SELECT * FROM chat_queue_employees WHERE employee_id = %d', $intEmployeeId ), $objDatabase );
	}

}
?>