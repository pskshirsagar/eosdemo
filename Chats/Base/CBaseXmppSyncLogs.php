<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CXmppSyncLogs
 * Do not add any new functions to this class.
 */

class CBaseXmppSyncLogs extends CEosPluralBase {

	/**
	 * @return CXmppSyncLog[]
	 */
	public static function fetchXmppSyncLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CXmppSyncLog', $objDatabase );
	}

	/**
	 * @return CXmppSyncLog
	 */
	public static function fetchXmppSyncLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CXmppSyncLog', $objDatabase );
	}

	public static function fetchXmppSyncLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'xmpp_sync_logs', $objDatabase );
	}

	public static function fetchXmppSyncLogById( $intId, $objDatabase ) {
		return self::fetchXmppSyncLog( sprintf( 'SELECT * FROM xmpp_sync_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>