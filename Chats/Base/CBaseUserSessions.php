<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CUserSessions
 * Do not add any new functions to this class.
 */

class CBaseUserSessions extends CEosPluralBase {

	/**
	 * @return CUserSession[]
	 */
	public static function fetchUserSessions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUserSession', $objDatabase );
	}

	/**
	 * @return CUserSession
	 */
	public static function fetchUserSession( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUserSession', $objDatabase );
	}

	public static function fetchUserSessionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'user_sessions', $objDatabase );
	}

	public static function fetchUserSessionById( $intId, $objDatabase ) {
		return self::fetchUserSession( sprintf( 'SELECT * FROM user_sessions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchUserSessionsByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchUserSessions( sprintf( 'SELECT * FROM user_sessions WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

}
?>