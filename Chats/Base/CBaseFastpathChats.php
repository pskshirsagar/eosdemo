<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CFastpathChats
 * Do not add any new functions to this class.
 */

class CBaseFastpathChats extends CEosPluralBase {

	/**
	 * @return CFastpathChat[]
	 */
	public static function fetchFastpathChats( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CFastpathChat', $objDatabase );
	}

	/**
	 * @return CFastpathChat
	 */
	public static function fetchFastpathChat( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFastpathChat', $objDatabase );
	}

	public static function fetchFastpathChatCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'fastpath_chats', $objDatabase );
	}

	public static function fetchFastpathChatById( $intId, $objDatabase ) {
		return self::fetchFastpathChat( sprintf( 'SELECT * FROM fastpath_chats WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchFastpathChatsByCid( $intCid, $objDatabase ) {
		return self::fetchFastpathChats( sprintf( 'SELECT * FROM fastpath_chats WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFastpathChatsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchFastpathChats( sprintf( 'SELECT * FROM fastpath_chats WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchFastpathChatsByApplicantId( $intApplicantId, $objDatabase ) {
		return self::fetchFastpathChats( sprintf( 'SELECT * FROM fastpath_chats WHERE applicant_id = %d', ( int ) $intApplicantId ), $objDatabase );
	}

	public static function fetchFastpathChatsByApplicationId( $intApplicationId, $objDatabase ) {
		return self::fetchFastpathChats( sprintf( 'SELECT * FROM fastpath_chats WHERE application_id = %d', ( int ) $intApplicationId ), $objDatabase );
	}

	public static function fetchFastpathChatsByOpenfireSessionId( $strOpenfireSessionId, $objDatabase ) {
		return self::fetchFastpathChats( sprintf( 'SELECT * FROM fastpath_chats WHERE openfire_session_id = \'%s\'', $strOpenfireSessionId ), $objDatabase );
	}

}
?>