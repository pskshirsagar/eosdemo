<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChatQueues
 * Do not add any new functions to this class.
 */

class CBaseChatQueues extends CEosPluralBase {

	/**
	 * @return CChatQueue[]
	 */
	public static function fetchChatQueues( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CChatQueue::class, $objDatabase );
	}

	/**
	 * @return CChatQueue
	 */
	public static function fetchChatQueue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChatQueue::class, $objDatabase );
	}

	public static function fetchChatQueueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chat_queues', $objDatabase );
	}

	public static function fetchChatQueueById( $intId, $objDatabase ) {
		return self::fetchChatQueue( sprintf( 'SELECT * FROM chat_queues WHERE id = %d', $intId ), $objDatabase );
	}

}
?>