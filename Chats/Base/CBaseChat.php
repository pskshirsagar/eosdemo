<?php

class CBaseChat extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.chats';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPsProductId;
	protected $m_intCompanyUserId;
	protected $m_intCustomerId;
	protected $m_intUserId;
	protected $m_intPsLeadId;
	protected $m_intTaskNoteId;
	protected $m_strIpAddress;
	protected $m_strChatStartDatetime;
	protected $m_strChatEndDatetime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intChatQueueId;
	protected $m_intChatSourceId;
	protected $m_intChatStatusTypeId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intChatResultId;

	public function __construct() {
		parent::__construct();

		$this->m_intChatQueueId = '9';
		$this->m_intChatStatusTypeId = '5';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['task_note_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskNoteId', trim( $arrValues['task_note_id'] ) ); elseif( isset( $arrValues['task_note_id'] ) ) $this->setTaskNoteId( $arrValues['task_note_id'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( $arrValues['ip_address'] ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( $arrValues['ip_address'] );
		if( isset( $arrValues['chat_start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strChatStartDatetime', trim( $arrValues['chat_start_datetime'] ) ); elseif( isset( $arrValues['chat_start_datetime'] ) ) $this->setChatStartDatetime( $arrValues['chat_start_datetime'] );
		if( isset( $arrValues['chat_end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strChatEndDatetime', trim( $arrValues['chat_end_datetime'] ) ); elseif( isset( $arrValues['chat_end_datetime'] ) ) $this->setChatEndDatetime( $arrValues['chat_end_datetime'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['chat_queue_id'] ) && $boolDirectSet ) $this->set( 'm_intChatQueueId', trim( $arrValues['chat_queue_id'] ) ); elseif( isset( $arrValues['chat_queue_id'] ) ) $this->setChatQueueId( $arrValues['chat_queue_id'] );
		if( isset( $arrValues['chat_source_id'] ) && $boolDirectSet ) $this->set( 'm_intChatSourceId', trim( $arrValues['chat_source_id'] ) ); elseif( isset( $arrValues['chat_source_id'] ) ) $this->setChatSourceId( $arrValues['chat_source_id'] );
		if( isset( $arrValues['chat_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intChatStatusTypeId', trim( $arrValues['chat_status_type_id'] ) ); elseif( isset( $arrValues['chat_status_type_id'] ) ) $this->setChatStatusTypeId( $arrValues['chat_status_type_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['chat_result_id'] ) && $boolDirectSet ) $this->set( 'm_intChatResultId', trim( $arrValues['chat_result_id'] ) ); elseif( isset( $arrValues['chat_result_id'] ) ) $this->setChatResultId( $arrValues['chat_result_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setTaskNoteId( $intTaskNoteId ) {
		$this->set( 'm_intTaskNoteId', CStrings::strToIntDef( $intTaskNoteId, NULL, false ) );
	}

	public function getTaskNoteId() {
		return $this->m_intTaskNoteId;
	}

	public function sqlTaskNoteId() {
		return ( true == isset( $this->m_intTaskNoteId ) ) ? ( string ) $this->m_intTaskNoteId : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 23, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIpAddress ) : '\'' . addslashes( $this->m_strIpAddress ) . '\'' ) : 'NULL';
	}

	public function setChatStartDatetime( $strChatStartDatetime ) {
		$this->set( 'm_strChatStartDatetime', CStrings::strTrimDef( $strChatStartDatetime, -1, NULL, true ) );
	}

	public function getChatStartDatetime() {
		return $this->m_strChatStartDatetime;
	}

	public function sqlChatStartDatetime() {
		return ( true == isset( $this->m_strChatStartDatetime ) ) ? '\'' . $this->m_strChatStartDatetime . '\'' : 'NULL';
	}

	public function setChatEndDatetime( $strChatEndDatetime ) {
		$this->set( 'm_strChatEndDatetime', CStrings::strTrimDef( $strChatEndDatetime, -1, NULL, true ) );
	}

	public function getChatEndDatetime() {
		return $this->m_strChatEndDatetime;
	}

	public function sqlChatEndDatetime() {
		return ( true == isset( $this->m_strChatEndDatetime ) ) ? '\'' . $this->m_strChatEndDatetime . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setChatQueueId( $intChatQueueId ) {
		$this->set( 'm_intChatQueueId', CStrings::strToIntDef( $intChatQueueId, NULL, false ) );
	}

	public function getChatQueueId() {
		return $this->m_intChatQueueId;
	}

	public function sqlChatQueueId() {
		return ( true == isset( $this->m_intChatQueueId ) ) ? ( string ) $this->m_intChatQueueId : '9';
	}

	public function setChatSourceId( $intChatSourceId ) {
		$this->set( 'm_intChatSourceId', CStrings::strToIntDef( $intChatSourceId, NULL, false ) );
	}

	public function getChatSourceId() {
		return $this->m_intChatSourceId;
	}

	public function sqlChatSourceId() {
		return ( true == isset( $this->m_intChatSourceId ) ) ? ( string ) $this->m_intChatSourceId : 'NULL';
	}

	public function setChatStatusTypeId( $intChatStatusTypeId ) {
		$this->set( 'm_intChatStatusTypeId', CStrings::strToIntDef( $intChatStatusTypeId, NULL, false ) );
	}

	public function getChatStatusTypeId() {
		return $this->m_intChatStatusTypeId;
	}

	public function sqlChatStatusTypeId() {
		return ( true == isset( $this->m_intChatStatusTypeId ) ) ? ( string ) $this->m_intChatStatusTypeId : '5';
	}

	public function setChatResultId( $intChatResultId ) {
		$this->set( 'm_intChatResultId', CStrings::strToIntDef( $intChatResultId, NULL, false ) );
	}

	public function getChatResultId() {
		return $this->m_intChatResultId;
	}

	public function sqlChatResultId() {
		return ( true == isset( $this->m_intChatResultId ) ) ? ( string ) $this->m_intChatResultId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, ps_product_id, company_user_id, customer_id, user_id, ps_lead_id, task_note_id, ip_address, chat_start_datetime, chat_end_datetime, updated_by, updated_on, created_by, created_on, chat_queue_id, chat_source_id, chat_status_type_id, details, chat_result_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlUserId() . ', ' .
						$this->sqlPsLeadId() . ', ' .
						$this->sqlTaskNoteId() . ', ' .
						$this->sqlIpAddress() . ', ' .
						$this->sqlChatStartDatetime() . ', ' .
						$this->sqlChatEndDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlChatQueueId() . ', ' .
						$this->sqlChatSourceId() . ', ' .
						$this->sqlChatStatusTypeId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlChatResultId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId(). ',' ; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId(). ',' ; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_note_id = ' . $this->sqlTaskNoteId(). ',' ; } elseif( true == array_key_exists( 'TaskNoteId', $this->getChangedColumns() ) ) { $strSql .= ' task_note_id = ' . $this->sqlTaskNoteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chat_start_datetime = ' . $this->sqlChatStartDatetime(). ',' ; } elseif( true == array_key_exists( 'ChatStartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' chat_start_datetime = ' . $this->sqlChatStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chat_end_datetime = ' . $this->sqlChatEndDatetime(). ',' ; } elseif( true == array_key_exists( 'ChatEndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' chat_end_datetime = ' . $this->sqlChatEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chat_queue_id = ' . $this->sqlChatQueueId(). ',' ; } elseif( true == array_key_exists( 'ChatQueueId', $this->getChangedColumns() ) ) { $strSql .= ' chat_queue_id = ' . $this->sqlChatQueueId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chat_source_id = ' . $this->sqlChatSourceId(). ',' ; } elseif( true == array_key_exists( 'ChatSourceId', $this->getChangedColumns() ) ) { $strSql .= ' chat_source_id = ' . $this->sqlChatSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chat_status_type_id = ' . $this->sqlChatStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ChatStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' chat_status_type_id = ' . $this->sqlChatStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chat_result_id = ' . $this->sqlChatResultId(). ',' ; } elseif( true == array_key_exists( 'ChatResultId', $this->getChangedColumns() ) ) { $strSql .= ' chat_result_id = ' . $this->sqlChatResultId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'ps_product_id' => $this->getPsProductId(),
			'company_user_id' => $this->getCompanyUserId(),
			'customer_id' => $this->getCustomerId(),
			'user_id' => $this->getUserId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'task_note_id' => $this->getTaskNoteId(),
			'ip_address' => $this->getIpAddress(),
			'chat_start_datetime' => $this->getChatStartDatetime(),
			'chat_end_datetime' => $this->getChatEndDatetime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'chat_queue_id' => $this->getChatQueueId(),
			'chat_source_id' => $this->getChatSourceId(),
			'chat_status_type_id' => $this->getChatStatusTypeId(),
			'details' => $this->getDetails(),
			'chat_result_id' => $this->getChatResultId()
		);
	}

}
?>