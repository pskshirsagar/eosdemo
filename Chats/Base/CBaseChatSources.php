<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChatSources
 * Do not add any new functions to this class.
 */

class CBaseChatSources extends CEosPluralBase {

	/**
	 * @return CChatSource[]
	 */
	public static function fetchChatSources( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CChatSource::class, $objDatabase );
	}

	/**
	 * @return CChatSource
	 */
	public static function fetchChatSource( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChatSource::class, $objDatabase );
	}

	public static function fetchChatSourceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chat_sources', $objDatabase );
	}

	public static function fetchChatSourceById( $intId, $objDatabase ) {
		return self::fetchChatSource( sprintf( 'SELECT * FROM chat_sources WHERE id = %d', $intId ), $objDatabase );
	}

}
?>