<?php

class CBaseChatBuffer extends CEosSingularBase {

	const TABLE_NAME = 'public.chat_buffers';

	protected $m_intId;
	protected $m_intChatId;
	protected $m_intTicketId;
	protected $m_strResourceId;
	protected $m_strLevel;
	protected $m_intLockSequenceNumber;
	protected $m_strLockedOn;
	protected $m_strSentOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['chat_id'] ) && $boolDirectSet ) $this->set( 'm_intChatId', trim( $arrValues['chat_id'] ) ); elseif( isset( $arrValues['chat_id'] ) ) $this->setChatId( $arrValues['chat_id'] );
		if( isset( $arrValues['ticket_id'] ) && $boolDirectSet ) $this->set( 'm_intTicketId', trim( $arrValues['ticket_id'] ) ); elseif( isset( $arrValues['ticket_id'] ) ) $this->setTicketId( $arrValues['ticket_id'] );
		if( isset( $arrValues['resource_id'] ) && $boolDirectSet ) $this->set( 'm_strResourceId', trim( stripcslashes( $arrValues['resource_id'] ) ) ); elseif( isset( $arrValues['resource_id'] ) ) $this->setResourceId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['resource_id'] ) : $arrValues['resource_id'] );
		if( isset( $arrValues['level'] ) && $boolDirectSet ) $this->set( 'm_strLevel', trim( stripcslashes( $arrValues['level'] ) ) ); elseif( isset( $arrValues['level'] ) ) $this->setLevel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['level'] ) : $arrValues['level'] );
		if( isset( $arrValues['lock_sequence_number'] ) && $boolDirectSet ) $this->set( 'm_intLockSequenceNumber', trim( $arrValues['lock_sequence_number'] ) ); elseif( isset( $arrValues['lock_sequence_number'] ) ) $this->setLockSequenceNumber( $arrValues['lock_sequence_number'] );
		if( isset( $arrValues['locked_on'] ) && $boolDirectSet ) $this->set( 'm_strLockedOn', trim( $arrValues['locked_on'] ) ); elseif( isset( $arrValues['locked_on'] ) ) $this->setLockedOn( $arrValues['locked_on'] );
		if( isset( $arrValues['sent_on'] ) && $boolDirectSet ) $this->set( 'm_strSentOn', trim( $arrValues['sent_on'] ) ); elseif( isset( $arrValues['sent_on'] ) ) $this->setSentOn( $arrValues['sent_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setChatId( $intChatId ) {
		$this->set( 'm_intChatId', CStrings::strToIntDef( $intChatId, NULL, false ) );
	}

	public function getChatId() {
		return $this->m_intChatId;
	}

	public function sqlChatId() {
		return ( true == isset( $this->m_intChatId ) ) ? ( string ) $this->m_intChatId : 'NULL';
	}

	public function setTicketId( $intTicketId ) {
		$this->set( 'm_intTicketId', CStrings::strToIntDef( $intTicketId, NULL, false ) );
	}

	public function getTicketId() {
		return $this->m_intTicketId;
	}

	public function sqlTicketId() {
		return ( true == isset( $this->m_intTicketId ) ) ? ( string ) $this->m_intTicketId : 'NULL';
	}

	public function setResourceId( $strResourceId ) {
		$this->set( 'm_strResourceId', CStrings::strTrimDef( $strResourceId, 100, NULL, true ) );
	}

	public function getResourceId() {
		return $this->m_strResourceId;
	}

	public function sqlResourceId() {
		return ( true == isset( $this->m_strResourceId ) ) ? '\'' . addslashes( $this->m_strResourceId ) . '\'' : 'NULL';
	}

	public function setLevel( $strLevel ) {
		$this->set( 'm_strLevel', CStrings::strTrimDef( $strLevel, 25, NULL, true ) );
	}

	public function getLevel() {
		return $this->m_strLevel;
	}

	public function sqlLevel() {
		return ( true == isset( $this->m_strLevel ) ) ? '\'' . addslashes( $this->m_strLevel ) . '\'' : 'NULL';
	}

	public function setLockSequenceNumber( $intLockSequenceNumber ) {
		$this->set( 'm_intLockSequenceNumber', CStrings::strToIntDef( $intLockSequenceNumber, NULL, false ) );
	}

	public function getLockSequenceNumber() {
		return $this->m_intLockSequenceNumber;
	}

	public function sqlLockSequenceNumber() {
		return ( true == isset( $this->m_intLockSequenceNumber ) ) ? ( string ) $this->m_intLockSequenceNumber : 'NULL';
	}

	public function setLockedOn( $strLockedOn ) {
		$this->set( 'm_strLockedOn', CStrings::strTrimDef( $strLockedOn, -1, NULL, true ) );
	}

	public function getLockedOn() {
		return $this->m_strLockedOn;
	}

	public function sqlLockedOn() {
		return ( true == isset( $this->m_strLockedOn ) ) ? '\'' . $this->m_strLockedOn . '\'' : 'NULL';
	}

	public function setSentOn( $strSentOn ) {
		$this->set( 'm_strSentOn', CStrings::strTrimDef( $strSentOn, -1, NULL, true ) );
	}

	public function getSentOn() {
		return $this->m_strSentOn;
	}

	public function sqlSentOn() {
		return ( true == isset( $this->m_strSentOn ) ) ? '\'' . $this->m_strSentOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, chat_id, ticket_id, resource_id, level, lock_sequence_number, locked_on, sent_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlChatId() . ', ' .
 						$this->sqlTicketId() . ', ' .
 						$this->sqlResourceId() . ', ' .
 						$this->sqlLevel() . ', ' .
 						$this->sqlLockSequenceNumber() . ', ' .
 						$this->sqlLockedOn() . ', ' .
 						$this->sqlSentOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chat_id = ' . $this->sqlChatId() . ','; } elseif( true == array_key_exists( 'ChatId', $this->getChangedColumns() ) ) { $strSql .= ' chat_id = ' . $this->sqlChatId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ticket_id = ' . $this->sqlTicketId() . ','; } elseif( true == array_key_exists( 'TicketId', $this->getChangedColumns() ) ) { $strSql .= ' ticket_id = ' . $this->sqlTicketId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resource_id = ' . $this->sqlResourceId() . ','; } elseif( true == array_key_exists( 'ResourceId', $this->getChangedColumns() ) ) { $strSql .= ' resource_id = ' . $this->sqlResourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' level = ' . $this->sqlLevel() . ','; } elseif( true == array_key_exists( 'Level', $this->getChangedColumns() ) ) { $strSql .= ' level = ' . $this->sqlLevel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lock_sequence_number = ' . $this->sqlLockSequenceNumber() . ','; } elseif( true == array_key_exists( 'LockSequenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' lock_sequence_number = ' . $this->sqlLockSequenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; } elseif( true == array_key_exists( 'LockedOn', $this->getChangedColumns() ) ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; } elseif( true == array_key_exists( 'SentOn', $this->getChangedColumns() ) ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'chat_id' => $this->getChatId(),
			'ticket_id' => $this->getTicketId(),
			'resource_id' => $this->getResourceId(),
			'level' => $this->getLevel(),
			'lock_sequence_number' => $this->getLockSequenceNumber(),
			'locked_on' => $this->getLockedOn(),
			'sent_on' => $this->getSentOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>