<?php

class CBaseXmppSyncLog extends CEosSingularBase {

	const TABLE_NAME = 'public.xmpp_sync_logs';

	protected $m_intId;
	protected $m_strNodeType;
	protected $m_strNode;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['node_type'] ) && $boolDirectSet ) $this->set( 'm_strNodeType', trim( stripcslashes( $arrValues['node_type'] ) ) ); elseif( isset( $arrValues['node_type'] ) ) $this->setNodeType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['node_type'] ) : $arrValues['node_type'] );
		if( isset( $arrValues['node'] ) && $boolDirectSet ) $this->set( 'm_strNode', trim( stripcslashes( $arrValues['node'] ) ) ); elseif( isset( $arrValues['node'] ) ) $this->setNode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['node'] ) : $arrValues['node'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setNodeType( $strNodeType ) {
		$this->set( 'm_strNodeType', CStrings::strTrimDef( $strNodeType, 25, NULL, true ) );
	}

	public function getNodeType() {
		return $this->m_strNodeType;
	}

	public function sqlNodeType() {
		return ( true == isset( $this->m_strNodeType ) ) ? '\'' . addslashes( $this->m_strNodeType ) . '\'' : 'NULL';
	}

	public function setNode( $strNode ) {
		$this->set( 'm_strNode', CStrings::strTrimDef( $strNode, -1, NULL, true ) );
	}

	public function getNode() {
		return $this->m_strNode;
	}

	public function sqlNode() {
		return ( true == isset( $this->m_strNode ) ) ? '\'' . addslashes( $this->m_strNode ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, node_type, node, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlNodeType() . ', ' .
 						$this->sqlNode() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' node_type = ' . $this->sqlNodeType() . ','; } elseif( true == array_key_exists( 'NodeType', $this->getChangedColumns() ) ) { $strSql .= ' node_type = ' . $this->sqlNodeType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' node = ' . $this->sqlNode() . ','; } elseif( true == array_key_exists( 'Node', $this->getChangedColumns() ) ) { $strSql .= ' node = ' . $this->sqlNode() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'node_type' => $this->getNodeType(),
			'node' => $this->getNode(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>