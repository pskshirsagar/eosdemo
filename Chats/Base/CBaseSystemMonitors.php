<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CSystemMonitors
 * Do not add any new functions to this class.
 */

class CBaseSystemMonitors extends CEosPluralBase {

	/**
	 * @return CSystemMonitor[]
	 */
	public static function fetchSystemMonitors( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSystemMonitor', $objDatabase );
	}

	/**
	 * @return CSystemMonitor
	 */
	public static function fetchSystemMonitor( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSystemMonitor', $objDatabase );
	}

	public static function fetchSystemMonitorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_monitors', $objDatabase );
	}

	public static function fetchSystemMonitorById( $intId, $objDatabase ) {
		return self::fetchSystemMonitor( sprintf( 'SELECT * FROM system_monitors WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>