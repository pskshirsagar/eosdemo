<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChatBuffers
 * Do not add any new functions to this class.
 */

class CBaseChatBuffers extends CEosPluralBase {

	/**
	 * @return CChatBuffer[]
	 */
	public static function fetchChatBuffers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CChatBuffer', $objDatabase );
	}

	/**
	 * @return CChatBuffer
	 */
	public static function fetchChatBuffer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CChatBuffer', $objDatabase );
	}

	public static function fetchChatBufferCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chat_buffers', $objDatabase );
	}

	public static function fetchChatBufferById( $intId, $objDatabase ) {
		return self::fetchChatBuffer( sprintf( 'SELECT * FROM chat_buffers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchChatBuffersByChatId( $intChatId, $objDatabase ) {
		return self::fetchChatBuffers( sprintf( 'SELECT * FROM chat_buffers WHERE chat_id = %d', ( int ) $intChatId ), $objDatabase );
	}

	public static function fetchChatBuffersByTicketId( $intTicketId, $objDatabase ) {
		return self::fetchChatBuffers( sprintf( 'SELECT * FROM chat_buffers WHERE ticket_id = %d', ( int ) $intTicketId ), $objDatabase );
	}

	public static function fetchChatBuffersByResourceId( $strResourceId, $objDatabase ) {
		return self::fetchChatBuffers( sprintf( 'SELECT * FROM chat_buffers WHERE resource_id = \'%s\'', $strResourceId ), $objDatabase );
	}

}
?>