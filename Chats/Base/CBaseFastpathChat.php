<?php

class CBaseFastpathChat extends CEosSingularBase {

	const TABLE_NAME = 'public.fastpath_chats';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intApplicantId;
	protected $m_intApplicationId;
	protected $m_strOpenfireSessionId;
	protected $m_strLeasingAgentName;
	protected $m_strWebchatFormData;
	protected $m_strChatLog;
	protected $m_strChatStartDatetime;
	protected $m_strChatEndDatetime;
	protected $m_strQueueWaitTime;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['openfire_session_id'] ) && $boolDirectSet ) $this->set( 'm_strOpenfireSessionId', trim( stripcslashes( $arrValues['openfire_session_id'] ) ) ); elseif( isset( $arrValues['openfire_session_id'] ) ) $this->setOpenfireSessionId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['openfire_session_id'] ) : $arrValues['openfire_session_id'] );
		if( isset( $arrValues['leasing_agent_name'] ) && $boolDirectSet ) $this->set( 'm_strLeasingAgentName', trim( stripcslashes( $arrValues['leasing_agent_name'] ) ) ); elseif( isset( $arrValues['leasing_agent_name'] ) ) $this->setLeasingAgentName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['leasing_agent_name'] ) : $arrValues['leasing_agent_name'] );
		if( isset( $arrValues['webchat_form_data'] ) && $boolDirectSet ) $this->set( 'm_strWebchatFormData', trim( stripcslashes( $arrValues['webchat_form_data'] ) ) ); elseif( isset( $arrValues['webchat_form_data'] ) ) $this->setWebchatFormData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['webchat_form_data'] ) : $arrValues['webchat_form_data'] );
		if( isset( $arrValues['chat_log'] ) && $boolDirectSet ) $this->set( 'm_strChatLog', trim( stripcslashes( $arrValues['chat_log'] ) ) ); elseif( isset( $arrValues['chat_log'] ) ) $this->setChatLog( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['chat_log'] ) : $arrValues['chat_log'] );
		if( isset( $arrValues['chat_start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strChatStartDatetime', trim( $arrValues['chat_start_datetime'] ) ); elseif( isset( $arrValues['chat_start_datetime'] ) ) $this->setChatStartDatetime( $arrValues['chat_start_datetime'] );
		if( isset( $arrValues['chat_end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strChatEndDatetime', trim( $arrValues['chat_end_datetime'] ) ); elseif( isset( $arrValues['chat_end_datetime'] ) ) $this->setChatEndDatetime( $arrValues['chat_end_datetime'] );
		if( isset( $arrValues['queue_wait_time'] ) && $boolDirectSet ) $this->set( 'm_strQueueWaitTime', trim( $arrValues['queue_wait_time'] ) ); elseif( isset( $arrValues['queue_wait_time'] ) ) $this->setQueueWaitTime( $arrValues['queue_wait_time'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setOpenfireSessionId( $strOpenfireSessionId ) {
		$this->set( 'm_strOpenfireSessionId', CStrings::strTrimDef( $strOpenfireSessionId, 15, NULL, true ) );
	}

	public function getOpenfireSessionId() {
		return $this->m_strOpenfireSessionId;
	}

	public function sqlOpenfireSessionId() {
		return ( true == isset( $this->m_strOpenfireSessionId ) ) ? '\'' . addslashes( $this->m_strOpenfireSessionId ) . '\'' : 'NULL';
	}

	public function setLeasingAgentName( $strLeasingAgentName ) {
		$this->set( 'm_strLeasingAgentName', CStrings::strTrimDef( $strLeasingAgentName, 50, NULL, true ) );
	}

	public function getLeasingAgentName() {
		return $this->m_strLeasingAgentName;
	}

	public function sqlLeasingAgentName() {
		return ( true == isset( $this->m_strLeasingAgentName ) ) ? '\'' . addslashes( $this->m_strLeasingAgentName ) . '\'' : 'NULL';
	}

	public function setWebchatFormData( $strWebchatFormData ) {
		$this->set( 'm_strWebchatFormData', CStrings::strTrimDef( $strWebchatFormData, 200, NULL, true ) );
	}

	public function getWebchatFormData() {
		return $this->m_strWebchatFormData;
	}

	public function sqlWebchatFormData() {
		return ( true == isset( $this->m_strWebchatFormData ) ) ? '\'' . addslashes( $this->m_strWebchatFormData ) . '\'' : 'NULL';
	}

	public function setChatLog( $strChatLog ) {
		$this->set( 'm_strChatLog', CStrings::strTrimDef( $strChatLog, 5000, NULL, true ) );
	}

	public function getChatLog() {
		return $this->m_strChatLog;
	}

	public function sqlChatLog() {
		return ( true == isset( $this->m_strChatLog ) ) ? '\'' . addslashes( $this->m_strChatLog ) . '\'' : 'NULL';
	}

	public function setChatStartDatetime( $strChatStartDatetime ) {
		$this->set( 'm_strChatStartDatetime', CStrings::strTrimDef( $strChatStartDatetime, -1, NULL, true ) );
	}

	public function getChatStartDatetime() {
		return $this->m_strChatStartDatetime;
	}

	public function sqlChatStartDatetime() {
		return ( true == isset( $this->m_strChatStartDatetime ) ) ? '\'' . $this->m_strChatStartDatetime . '\'' : 'NULL';
	}

	public function setChatEndDatetime( $strChatEndDatetime ) {
		$this->set( 'm_strChatEndDatetime', CStrings::strTrimDef( $strChatEndDatetime, -1, NULL, true ) );
	}

	public function getChatEndDatetime() {
		return $this->m_strChatEndDatetime;
	}

	public function sqlChatEndDatetime() {
		return ( true == isset( $this->m_strChatEndDatetime ) ) ? '\'' . $this->m_strChatEndDatetime . '\'' : 'NULL';
	}

	public function setQueueWaitTime( $strQueueWaitTime ) {
		$this->set( 'm_strQueueWaitTime', CStrings::strTrimDef( $strQueueWaitTime, -1, NULL, true ) );
	}

	public function getQueueWaitTime() {
		return $this->m_strQueueWaitTime;
	}

	public function sqlQueueWaitTime() {
		return ( true == isset( $this->m_strQueueWaitTime ) ) ? '\'' . $this->m_strQueueWaitTime . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, applicant_id, application_id, openfire_session_id, leasing_agent_name, webchat_form_data, chat_log, chat_start_datetime, chat_end_datetime, queue_wait_time, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlApplicantId() . ', ' .
 						$this->sqlApplicationId() . ', ' .
 						$this->sqlOpenfireSessionId() . ', ' .
 						$this->sqlLeasingAgentName() . ', ' .
 						$this->sqlWebchatFormData() . ', ' .
 						$this->sqlChatLog() . ', ' .
 						$this->sqlChatStartDatetime() . ', ' .
 						$this->sqlChatEndDatetime() . ', ' .
 						$this->sqlQueueWaitTime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' openfire_session_id = ' . $this->sqlOpenfireSessionId() . ','; } elseif( true == array_key_exists( 'OpenfireSessionId', $this->getChangedColumns() ) ) { $strSql .= ' openfire_session_id = ' . $this->sqlOpenfireSessionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leasing_agent_name = ' . $this->sqlLeasingAgentName() . ','; } elseif( true == array_key_exists( 'LeasingAgentName', $this->getChangedColumns() ) ) { $strSql .= ' leasing_agent_name = ' . $this->sqlLeasingAgentName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' webchat_form_data = ' . $this->sqlWebchatFormData() . ','; } elseif( true == array_key_exists( 'WebchatFormData', $this->getChangedColumns() ) ) { $strSql .= ' webchat_form_data = ' . $this->sqlWebchatFormData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chat_log = ' . $this->sqlChatLog() . ','; } elseif( true == array_key_exists( 'ChatLog', $this->getChangedColumns() ) ) { $strSql .= ' chat_log = ' . $this->sqlChatLog() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chat_start_datetime = ' . $this->sqlChatStartDatetime() . ','; } elseif( true == array_key_exists( 'ChatStartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' chat_start_datetime = ' . $this->sqlChatStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chat_end_datetime = ' . $this->sqlChatEndDatetime() . ','; } elseif( true == array_key_exists( 'ChatEndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' chat_end_datetime = ' . $this->sqlChatEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' queue_wait_time = ' . $this->sqlQueueWaitTime() . ','; } elseif( true == array_key_exists( 'QueueWaitTime', $this->getChangedColumns() ) ) { $strSql .= ' queue_wait_time = ' . $this->sqlQueueWaitTime() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'applicant_id' => $this->getApplicantId(),
			'application_id' => $this->getApplicationId(),
			'openfire_session_id' => $this->getOpenfireSessionId(),
			'leasing_agent_name' => $this->getLeasingAgentName(),
			'webchat_form_data' => $this->getWebchatFormData(),
			'chat_log' => $this->getChatLog(),
			'chat_start_datetime' => $this->getChatStartDatetime(),
			'chat_end_datetime' => $this->getChatEndDatetime(),
			'queue_wait_time' => $this->getQueueWaitTime(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>