<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Chats\CChatStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseChatStatusTypes extends CEosPluralBase {

	/**
	 * @return CChatStatusType[]
	 */
	public static function fetchChatStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CChatStatusType', $objDatabase );
	}

	/**
	 * @return CChatStatusType
	 */
	public static function fetchChatStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CChatStatusType', $objDatabase );
	}

	public static function fetchChatStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chat_status_types', $objDatabase );
	}

	public static function fetchChatStatusTypeById( $intId, $objDatabase ) {
		return self::fetchChatStatusType( sprintf( 'SELECT * FROM chat_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>