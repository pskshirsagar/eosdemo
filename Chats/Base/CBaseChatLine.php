<?php

class CBaseChatLine extends CEosSingularBase {

	const TABLE_NAME = 'public.chat_lines';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intChatId;
	protected $m_strLineText;
	protected $m_strLineDatetime;
	protected $m_intIsAdmin;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strRowText;
	protected $m_jsonRowText;

	public function __construct() {
		parent::__construct();

		$this->m_intIsAdmin = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['chat_id'] ) && $boolDirectSet ) $this->set( 'm_intChatId', trim( $arrValues['chat_id'] ) ); elseif( isset( $arrValues['chat_id'] ) ) $this->setChatId( $arrValues['chat_id'] );
		if( isset( $arrValues['line_text'] ) && $boolDirectSet ) $this->set( 'm_strLineText', trim( stripcslashes( $arrValues['line_text'] ) ) ); elseif( isset( $arrValues['line_text'] ) ) $this->setLineText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['line_text'] ) : $arrValues['line_text'] );
		if( isset( $arrValues['line_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLineDatetime', trim( $arrValues['line_datetime'] ) ); elseif( isset( $arrValues['line_datetime'] ) ) $this->setLineDatetime( $arrValues['line_datetime'] );
		if( isset( $arrValues['is_admin'] ) && $boolDirectSet ) $this->set( 'm_intIsAdmin', trim( $arrValues['is_admin'] ) ); elseif( isset( $arrValues['is_admin'] ) ) $this->setIsAdmin( $arrValues['is_admin'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['row_text'] ) ) $this->set( 'm_strRowText', trim( $arrValues['row_text'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setChatId( $intChatId ) {
		$this->set( 'm_intChatId', CStrings::strToIntDef( $intChatId, NULL, false ) );
	}

	public function getChatId() {
		return $this->m_intChatId;
	}

	public function sqlChatId() {
		return ( true == isset( $this->m_intChatId ) ) ? ( string ) $this->m_intChatId : 'NULL';
	}

	public function setLineText( $strLineText ) {
		$this->set( 'm_strLineText', CStrings::strTrimDef( $strLineText, -1, NULL, true ) );
	}

	public function getLineText() {
		return $this->m_strLineText;
	}

	public function sqlLineText() {
		return ( true == isset( $this->m_strLineText ) ) ? '\'' . addslashes( $this->m_strLineText ) . '\'' : 'NULL';
	}

	public function setLineDatetime( $strLineDatetime ) {
		$this->set( 'm_strLineDatetime', CStrings::strTrimDef( $strLineDatetime, -1, NULL, true ) );
	}

	public function getLineDatetime() {
		return $this->m_strLineDatetime;
	}

	public function sqlLineDatetime() {
		return ( true == isset( $this->m_strLineDatetime ) ) ? '\'' . $this->m_strLineDatetime . '\'' : 'NOW()';
	}

	public function setIsAdmin( $intIsAdmin ) {
		$this->set( 'm_intIsAdmin', CStrings::strToIntDef( $intIsAdmin, NULL, false ) );
	}

	public function getIsAdmin() {
		return $this->m_intIsAdmin;
	}

	public function sqlIsAdmin() {
		return ( true == isset( $this->m_intIsAdmin ) ) ? ( string ) $this->m_intIsAdmin : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setRowText( $jsonRowText ) {
		if( true == valObj( $jsonRowText, 'stdClass' ) ) {
			$this->set( 'm_jsonRowText', $jsonRowText );
		} elseif( true == valJsonString( $jsonRowText ) ) {
			$this->set( 'm_jsonRowText', CStrings::strToJson( $jsonRowText ) );
		} else {
			$this->set( 'm_jsonRowText', NULL ); 
		}
		unset( $this->m_strRowText );
	}

	public function getRowText() {
		if( true == isset( $this->m_strRowText ) ) {
			$this->m_jsonRowText = CStrings::strToJson( $this->m_strRowText );
			unset( $this->m_strRowText );
		}
		return $this->m_jsonRowText;
	}

	public function sqlRowText() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getRowText() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getRowText() ) ) . '\'';
		}
		return 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, chat_id, line_text, line_datetime, is_admin, created_by, created_on, row_text )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlChatId() . ', ' .
 						$this->sqlLineText() . ', ' .
 						$this->sqlLineDatetime() . ', ' .
 						$this->sqlIsAdmin() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlRowText() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chat_id = ' . $this->sqlChatId() . ','; } elseif( true == array_key_exists( 'ChatId', $this->getChangedColumns() ) ) { $strSql .= ' chat_id = ' . $this->sqlChatId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' line_text = ' . $this->sqlLineText() . ','; } elseif( true == array_key_exists( 'LineText', $this->getChangedColumns() ) ) { $strSql .= ' line_text = ' . $this->sqlLineText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' line_datetime = ' . $this->sqlLineDatetime() . ','; } elseif( true == array_key_exists( 'LineDatetime', $this->getChangedColumns() ) ) { $strSql .= ' line_datetime = ' . $this->sqlLineDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_admin = ' . $this->sqlIsAdmin() . ','; } elseif( true == array_key_exists( 'IsAdmin', $this->getChangedColumns() ) ) { $strSql .= ' is_admin = ' . $this->sqlIsAdmin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' row_text = ' . $this->sqlRowText() . ','; } elseif( true == array_key_exists( 'RowText', $this->getChangedColumns() ) ) { $strSql .= ' row_text = ' . $this->sqlRowText() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'chat_id' => $this->getChatId(),
			'line_text' => $this->getLineText(),
			'line_datetime' => $this->getLineDatetime(),
			'is_admin' => $this->getIsAdmin(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'row_text' => $this->getRowText()
		);
	}

}
?>