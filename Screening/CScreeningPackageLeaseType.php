<?php

class CScreeningPackageLeaseType extends CBaseScreeningPackageLeaseType {

	const APPLICATION				= 1;
	const MONTH_TO_MONTH			= 2;
	const RENEWAL					= 3;
	const MID_LEASE_MODIFICATION	= 4;
	const TRANSFER					= 5;

    public static $c_arrintNonApplicationLeaseIntervalTypeIds 	= [ self::RENEWAL, self::MID_LEASE_MODIFICATION, self::TRANSFER ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>