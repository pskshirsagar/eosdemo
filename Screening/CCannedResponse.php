<?php

class CCannedResponse extends CBaseCannedResponse {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCannedResponseName() {
		$boolIsValid = true;

		if( false == valStr( $this->getCannedResponseName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'canned_response_name', 'Canned Response Name Required.' ) );
		}

		return $boolIsValid;
	}

	public function valCannedResponse() {
		$boolIsValid = true;

		if( false == valStr( $this->getCannedResponse() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'canned_response', 'Canned Response Content Required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function setDefaults() {
		$this->setQuickResponseTypeId( CQuickResponseType::DISPUTE_CANNED_RESPONSE );
		return;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			$boolIsValid &= $this->valCannedResponseName();
			$boolIsValid &= $this->valCannedResponse();
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>