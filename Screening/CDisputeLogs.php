<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDisputeLogs
 * Do not add any new functions to this class.
 */

class CDisputeLogs extends CBaseDisputeLogs {

	public static function fetchDisputeLogsByConsumerIdByLogTypeId( $intConsumerId, $arrintDisputeLogTypeIds, $objDatabase ) {
		$strSql = 'SELECT * FROM dispute_logs WHERE consumer_id = ' . ( int ) $intConsumerId . 'AND log_type_id IN( ' . implode( ',', $arrintDisputeLogTypeIds ) . ' ) ' . ' ORDER BY id DESC';

		return parent::fetchDisputeLogs( $strSql, $objDatabase );
	}

	public function fetchTodaysDisputeLogCount( $intUserId, $objDatabase ) {

		$strSql = 'SELECT
    					COUNT ( DISTINCT dl.consumer_id ) AS total_records
				   FROM
    					dispute_logs dl
				   WHERE

    					dl.created_by = ' . ( int ) $intUserId . '
   	 					AND DATE ( dl.created_on ) >= CURRENT_DATE

    							';

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchYesterdaysDisputeLogCount( $intUserId, $objDatabase ) {

		$strSql = 'SELECT
    					COUNT ( DISTINCT dl.consumer_id ) AS total_records
				   FROM
    					dispute_logs dl
				   WHERE
    					dl.created_by = ' . ( int ) $intUserId . '
   	 					AND dl.created_on > TIMESTAMP \'yesterday\' AND DATE ( dl.created_on ) < CURRENT_DATE
    					AND dl.consumer_id NOT IN (
                                                   SELECT
                                                        dl.consumer_id
                                                   FROM
                                                       dispute_logs dl
                                                   WHERE
                                                       DATE ( dl.created_on ) = CURRENT_DATE )';

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchMonthDisputeLogCount( $intUserId, $objDatabase ) {

		$strSql = 'SELECT
        				COUNT ( DISTINCT dl.consumer_id ) AS total_records
     			   FROM
         				dispute_logs dl
     			   WHERE
         				dl.created_by = ' . ( int ) $intUserId . '
         		   AND
        				dl.created_on between   ( ( CURRENT_DATE - 1 ) - INTERVAL \'30 days\' ) AND ( CURRENT_DATE - 1 )
                      AND dl.consumer_id NOT IN (
                                                 SELECT
                                                        dl.consumer_id
                                                   FROM
                                                       dispute_logs dl
                                                   WHERE
                                                       DATE ( dl.created_on ) = CURRENT_DATE OR dl.created_on > TIMESTAMP \'yesterday\' AND DATE ( dl.created_on ) < CURRENT_DATE )';

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchLatestDisputeLogByConsumerId( $intConsumerId, $objDatabase ) {
		$strSql = 'SELECT * FROM dispute_logs WHERE consumer_id = ' . ( int ) $intConsumerId . 'ORDER BY id DESC LIMIT 1';
		return parent::fetchDisputeLog( $strSql, $objDatabase );
	}

}
?>