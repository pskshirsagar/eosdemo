<?php

class CScreeningTransactionCondition extends CBaseScreeningTransactionCondition {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningConditionSetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningConfigPreferenceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>