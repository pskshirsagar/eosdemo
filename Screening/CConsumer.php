<?php

class CConsumer extends CBaseConsumer {

	/*
	 * Validation functions.
	 */

	public function valFirstName() {
		$boolIsValid = true;
		if( true == is_null( $this->getFirstName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_name', 'First name is required.' ) );
		}

		if( true == $boolIsValid && 1 >= strlen( $this->getFirstName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_name', 'First name should contain more than one character.' ) );
		}

		return $boolIsValid;
	}

	public function valLastName() {
		$boolIsValid = true;
		if( true == is_null( $this->getLastName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'last_name', 'Last name is required.' ) );
		}

		if( true == $boolIsValid && 1 >= strlen( $this->getLastName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'last_name', 'Last name should contain more than one character.' ) );
		}

		return $boolIsValid;
	}

	public function valLastFourSsn() {
		$boolIsValid = true;
		if( true == is_null( $this->getLastFourSsn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ssn', 'SSN is required.' ) );
		}

		if( true == $boolIsValid && ( 3 >= strlen( $this->getLastFourSsn() ) || false == is_numeric( $this->getLastFourSsn() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ssn', 'A valid SSN required ' ) );
		}

		return $boolIsValid;
	}

	public function valDateOfBirthEncrypted() {
		$boolIsValid = true;
		if( true == is_null( $this->getDateOfBirthEncrypted() ) || false == CValidation::validateDate( $this->getDateOfBirthEncrypted() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date', 'A valid birth date is required.' ) );
		}

		$intConsumerValidAge = floor( ( strtotime( date( 'm/d/Y' ) ) - strtotime( $this->getDateOfBirthEncrypted() ) ) / 31556926 );
		if( true == $boolIsValid && 15 >= $intConsumerValidAge ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date', 'A birth date is not meeting minimum age criteria of age(15).' ) );
		}

		return $boolIsValid;
	}

	public function valReferenceNumber( $objDataBase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getReferenceNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'refrence_number', 'Reference number is required.' ) );
		}

		if( true == $boolIsValid && ( false == preg_match( '%^[0-9\/]+$%', $this->getReferenceNumber() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'refrence_number', 'A valid Reference number is required.' ) );
		}

		$objScreening = CScreenings::fetchScreeningById( $this->getScreeningId(), $objDataBase );

		if( true == valObj( $objScreening, 'CScreening' ) ) {
			$objScreeningApplicant 	= CScreeningApplicants::fetchScreeningApplicantByIdByScreeningIdByCid( $this->getScreeningApplicantId(), $this->getScreeningId(), $objScreening->getCid(), $objDataBase );
		}

		if( true == $boolIsValid && ( false == valObj( $objScreening, 'CScreening' ) || false == valObj( $objScreeningApplicant, 'CScreeningApplicant' ) ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'screening_id', 'Invalid Reference number.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;
		if( true == is_null( $this->getEmailAddress() ) || false == CValidation::validateEmailAddresses( \Psi\CStringService::singleton()->strtolower( $this->getEmailAddress() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'A valid email address is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is required.' ) );
		}

		if( true == $boolIsValid && false == preg_match( '/^[+\d \d\-\s]+$/', str_replace( '-', '', $this->getPhoneNumber() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'A valid Phone number is required.' ) );
		}

		if( true == $boolIsValid && ( 10 > strlen( str_replace( '-', '', $this->getPhoneNumber() ) ) || 15 < strlen( str_replace( '-', '', $this->getPhoneNumber() ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number must be between 10 and 15 characters.' ) );
		}

		return $boolIsValid;
	}

	public function valContactMethodTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getContactMethodTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'method_of_contact', 'Preferred method of contact is required.' ) );
		}

		return $boolIsValid;
	}

	public function valAddressLine() {
		$boolIsValid = true;
		if( CContactMethodType::CERTIFIED_MAIL == $this->getContactMethodTypeId() && true == is_null( $this->getAddressLine() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address_line', 'Address line is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;
		if( CContactMethodType::CERTIFIED_MAIL == $this->getContactMethodTypeId() && true == is_null( $this->getCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'City is required.' ) );
		}

		return $boolIsValid;
	}

	public function valState() {
		$boolIsValid = true;
		if( CContactMethodType::CERTIFIED_MAIL == $this->getContactMethodTypeId() && true == is_null( $this->getState() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state', 'State is required.' ) );
		}

		return $boolIsValid;
	}

	public function valZipcode() {
		$boolIsValid = true;
		if( CContactMethodType::CERTIFIED_MAIL == $this->getContactMethodTypeId() && true == is_null( $this->getZipcode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'zipcode', 'Zipcode is required.' ) );
		}

		if( true == $boolIsValid && CContactMethodType::CERTIFIED_MAIL == $this->getContactMethodTypeId() && ( 5 > strlen( $this->getZipcode() ) || 15 < strlen( $this->getZipcode() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'zipcode', 'Zipcode must be between 5 and 15 characters.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyName() {
		$boolIsValid = true;
		if( true == is_null( $this->getPropertyName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', 'Property where you applied is required.' ) );
		}

		if( true == $boolIsValid && 1 >= strlen( $this->getPropertyName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', 'Property where you applied should contain more than one character.' ) );
		}

		if( true == $boolIsValid && true == is_numeric( $this->getPropertyName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', 'A valid Property where you applied is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDataBase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valFirstName();
				$boolIsValid &= $this->valLastName();
				$boolIsValid &= $this->valLastFourSsn();
				$boolIsValid &= $this->valDateOfBirthEncrypted();
				$boolIsValid &= $this->valReferenceNumber( $objDataBase );
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valContactMethodTypeId();
				$boolIsValid &= $this->valPropertyName();
				$boolIsValid &= $this->valAddressLine();
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valState();
				$boolIsValid &= $this->valZipcode();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valFirstName();
				$boolIsValid &= $this->valLastName();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function validateConsumer( $arrintScreeningInfo, $arrmixConsumerInfo, $arrmixConsumerPersonalInfo, $objDataBase ) {

		$boolIsValid	= true;
		$this->setScreeningId( $arrintScreeningInfo[0] );
		$this->setScreeningApplicantId( $arrintScreeningInfo[1] );
		$this->setContactMethodTypeId( $arrmixConsumerInfo['contact_method_type_id'] );
		$this->setFirstName( $arrmixConsumerPersonalInfo['first_name'] );
		$this->setLastName( $arrmixConsumerPersonalInfo['last_name'] );
		$this->setReferenceNumber( $arrmixConsumerPersonalInfo['reference_number'] );
		$this->setLastFourSsn( $arrmixConsumerPersonalInfo['last_four_ssn'] );
		$this->setDateOfBirthEncrypted( $arrmixConsumerPersonalInfo['date_of_birth_encrypted'] );
		$this->setEmailAddress( $arrmixConsumerInfo['email_address'] );
		$this->setPhoneNumber( $arrmixConsumerInfo['phone_number'] );
		$this->setPropertyName( $arrmixConsumerInfo['property_name'] );
		$this->setAddressLine( $arrmixConsumerInfo['address_line'] );
		$this->setCity( $arrmixConsumerInfo['city'] );
		$this->setState( $arrmixConsumerInfo['state'] );
		$this->setZipcode( $arrmixConsumerInfo['zipcode'] );

		$boolIsValid	&= $this->validate( VALIDATE_INSERT, $objDataBase );
		if( false == $boolIsValid ) return $arrmixResult['is_valid'] = false;
		$arrmixResult = array(
			'is_success' 	=> $boolIsValid
		);

		return $arrmixResult;
	}

	public function updateConsumer( $arrmixConsumerInfo,  $objDataBase ) {
		$boolIsValid = true;
		if( true == array_key_exists( 'firstname', $arrmixConsumerInfo ) ) {
			$this->setFirstName( $arrmixConsumerInfo['firstname'] );
		}
		if( true == array_key_exists( 'lastname', $arrmixConsumerInfo ) ) {
			$this->setLastName( $arrmixConsumerInfo['lastname'] );
		}
		if( true == array_key_exists( 'email', $arrmixConsumerInfo ) ) {
			$this->setEmailAddress( $arrmixConsumerInfo['email'] );
		}
		if( true == array_key_exists( 'phone', $arrmixConsumerInfo ) ) {
			$this->setPhoneNumber( $arrmixConsumerInfo['phone'] );
		}
		if( true == array_key_exists( 'address_line', $arrmixConsumerInfo ) ) {
			$this->setAddressLine( $arrmixConsumerInfo['address_line'] );
		}
		if( true == array_key_exists( 'city', $arrmixConsumerInfo ) ) {
			$this->setCity( $arrmixConsumerInfo['city'] );
		}
		if( true == array_key_exists( 'state', $arrmixConsumerInfo ) ) {
			$this->setState( $arrmixConsumerInfo['state'] );
		}
		if( true == array_key_exists( 'zipcode', $arrmixConsumerInfo ) ) {
			$this->setZipcode( $arrmixConsumerInfo['zipcode'] );
		}
		if( true == array_key_exists( 'contact_method_type_id', $arrmixConsumerInfo ) ) {
			$this->setContactMethodTypeId( $arrmixConsumerInfo['contact_method_type_id'] );
		}
		$boolIsValid	&= $this->validate( VALIDATE_UPDATE, $objDataBase );

		return $boolIsValid;
	}

}
?>