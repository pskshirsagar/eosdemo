<?php

class CRvTestApplicant extends CBaseRvTestApplicant {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSsn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSsnMasked() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPackageId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameMiddle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBirthDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAlien() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSendCaliforniaConsumerReport() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonthlyIncome() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRegion() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMunicipality() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddressLine() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRvMasterTestCaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>