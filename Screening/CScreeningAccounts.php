<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningAccounts
 * Do not add any new functions to this class.
 */

class CScreeningAccounts extends CBaseScreeningAccounts {

	public static function fetchPublishedScreeningAccountByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    screening_accounts
					WHERE
					    is_active = TRUE 
					    AND cid = ' . ( int ) $intCid . '
					LIMIT
					    1';

		return parent::fetchScreeningAccount( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningAccountByIdByCid( $intScreeningAccountId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    screening_accounts
					WHERE
					    id = ' . ( int ) $intScreeningAccountId . '
					    AND is_active = TRUE
					    AND cid = ' . ( int ) $intCid . '
					LIMIT
					    1';

		return parent::fetchScreeningAccount( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningAccountDetailByIdByCid( $intScreeningAccountId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    sa.*,
					    srt.name AS screening_rate_type_name
					FROM
					    screening_accounts sa,
					    screen_rate_types srt
					WHERE
					    sa.id = ' . ( int ) $intScreeningAccountId . '
					    AND sa.screen_rate_type_id = srt.id
					    AND sa.is_active = TRUE
					    AND sa.cid = ' . ( int ) $intCid;

		return parent::fetchScreeningAccount( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningAccountsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    sa.*,
					    srt.name AS screening_rate_type_name
					FROM
					    screening_accounts sa
					    LEFT JOIN screen_rate_types srt ON( sa.screen_rate_type_id = srt.id )
					WHERE
					    sa.is_active = TRUE
					    AND sa.cid = ' . ( int ) $intCid . '
					ORDER BY srt.id';

		return parent::fetchScreeningAccounts( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningAccountByScreenRateTypeIdByCid( $intScreenRateTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    screening_accounts
					WHERE
					    is_active = TRUE
					    AND screen_rate_type_id = ' . ( int ) $intScreenRateTypeId . '
					    AND cid = ' . ( int ) $intCid . '
					LIMIT
					    1';

		return parent::fetchScreeningAccount( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningAccountsByIdsByCid( $arrintScreeningAccountIds, $intCid, $objDatabase ) {

		$arrintScreeningAccountIds = array_filter( $arrintScreeningAccountIds );

		if( false == valArr( $arrintScreeningAccountIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					    *
					FROM
					    screening_accounts
					WHERE
					    id IN (' . sqlIntImplode( $arrintScreeningAccountIds ) . ' )
					    AND is_active = TRUE
					    AND cid = ' . ( int ) $intCid;

		return parent::fetchScreeningAccounts( $strSql, $objDatabase );
	}

	public static function fetchActiveDistinctScreeningClientIds( $objDatabase ) {
		$strSql = 'SELECT distinct cid FROM screening_accounts WHERE is_active = TRUE';
		return fetchData( $strSql, $objDatabase );
	}

}
?>