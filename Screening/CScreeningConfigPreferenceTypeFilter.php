<?php

class CScreeningConfigPreferenceTypeFilter extends CBaseScreeningConfigPreferenceTypeFilter {

    protected $m_strFilterValue;
	protected $m_strDescription;
	protected $m_intScreeningFilterTypeId;

    public function setFilterValue( $strFilterValue ) {
    	$this->m_strFilterValue = $strFilterValue;
    }

    public function getFilterValue() {
    	return $this->m_strFilterValue;
    }

    public function setDescription( $strDescription ) {
    	$this->m_strDescription = $strDescription;
    }

    public function getDescription() {
    	return $this->m_strDescription;
    }

    public function setScreeningFilterTypeId( $intScreeningFilterTypeId ) {
    	$this->m_intScreeningFilterTypeId = $intScreeningFilterTypeId;
    }

    public function getScreeningFilterTypeId() {
    	return $this->m_intScreeningFilterTypeId;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

    	parent::setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false );
    	if( isset( $arrValues['filter_value'] ) ) $this->setFilterValue( $arrValues['filter_value'] );
    	if( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
    	if( isset( $arrValues['screening_filter_type_id'] ) ) $this->setScreeningFilterTypeId( $arrValues['screening_filter_type_id'] );
    }
}
?>