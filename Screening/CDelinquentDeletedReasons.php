<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDelinquentDeletedReasons
 * Do not add any new functions to this class.
 */

class CDelinquentDeletedReasons extends CBaseDelinquentDeletedReasons {

	public static function fetchDelinquentDeletedReasons( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDelinquentDeletedReason', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDelinquentDeletedReason( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDelinquentDeletedReason', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedDelinquentDeletedReasons( $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    delinquent_deleted_reasons
					WHERE
					    is_published = true
					ORDER BY
					    order_num';

		return self::fetchObjects( $strSql, 'CDelinquentDeletedReason', $objDatabase );
	}

	public static function fetchPublishedNonSystemDelinquentDeletedReasons( $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    delinquent_deleted_reasons
					WHERE
					    is_published = true
					AND is_system = false    
					ORDER BY
					    order_num';

		return self::fetchObjects( $strSql, 'CDelinquentDeletedReason', $objDatabase );
	}

}

?>