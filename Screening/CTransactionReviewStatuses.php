<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CTransactionReviewStatuses
 * Do not add any new functions to this class.
 */

class CTransactionReviewStatuses extends CBaseTransactionReviewStatuses {

	public static function fetchTransactionReviewStatuses( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTransactionReviewStatus', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTransactionReviewStatus( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTransactionReviewStatus', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchActiveTransactionReviewStatuses( $objDatabase ) {
		$strSql = 'select * from transaction_review_statuses where is_published=1';
		return parent::fetchTransactionReviewStatuses( $strSql, $objDatabase );
	}
}
?>