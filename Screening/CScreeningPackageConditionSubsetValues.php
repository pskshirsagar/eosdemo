<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageConditionSubsetValues
 * Do not add any new functions to this class.
 */

class CScreeningPackageConditionSubsetValues extends CBaseScreeningPackageConditionSubsetValues {

	public static function fetchScreeningPackageConditionSubsetValueByAmountConditionsBySubsetIdsByAvailableConditionId( $arrintScreeningPackageConditionSubsetIds, $intAvailableConditionId, $objDatabase ) {
		if( false == valArr( $arrintScreeningPackageConditionSubsetIds ) ) return NULL;

		$strSql = '	SELECT
						spcsv.id
					FROM
						screening_package_condition_subset_values spcsv
						JOIN screening_package_available_conditions spac ON spac.id = spcsv.screening_package_available_condition_id
					WHERE
						spac.screening_package_condition_type_id IN ( ' . implode( CScreeningPackageConditionType::$c_arrintFinancialScreeningConditionTypeIds, ',' ) . ' )
						AND screening_package_condition_subset_id IN ( ' . implode( $arrintScreeningPackageConditionSubsetIds, ',' ) . ' )
						AND screening_package_available_condition_id = ' . ( int ) $intAvailableConditionId . '
					ORDER BY
						spcsv.id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageConditionSubsetValuesByScreeningPackageAvailableConditionId( $intScreeningPackageAvailableConditionId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						screening_package_condition_subset_values
					WHERE
						screening_package_available_condition_id  = ' . ( int ) $intScreeningPackageAvailableConditionId;

		return parent::fetchScreeningPackageConditionSubsetValues( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageConditionSubsetValuesByScreeningPackageConditionSubsetIds( $arrintScreeningPackageConditionSubsetIds, $objDatabase ) {
		if( false == valArr( $arrintScreeningPackageConditionSubsetIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						screening_package_condition_subset_values
					WHERE
						screening_package_condition_subset_id  IN ( ' . implode( ',', $arrintScreeningPackageConditionSubsetIds ) . ' )
					ORDER BY
						id';

		return parent::fetchScreeningPackageConditionSubsetValues( $strSql, $objDatabase );
	}

}
?>