<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningProviderBaseRates
 * Do not add any new functions to this class.
 */

class CScreeningProviderBaseRates extends CBaseScreeningProviderBaseRates {

	public static function fetchScreeningProviderBaseRates( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningProviderBaseRate', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningProviderBaseRate( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningProviderBaseRate', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningProviderBaseRatesBySearchTypeByScreeningDataProviderId( $strState, $strCounty, $intScreenType, $intScreeningDataProviderId, $objDatabase ) {
		$strSql = '	SELECT
							*
					FROM
						screening_provider_base_rates
					WHERE
						screening_data_provider_id = ' . ( int ) $intScreeningDataProviderId;

		$strSql .= ' AND state_code = \'' . $strState . '\'';

		if( true == valStr( $strCounty ) ) {
			$strSql .= ' AND county_name = \'' . $strCounty . '\'';
		}

		return self::fetchScreeningProviderBaseRate( $strSql, $objDatabase );
	}

	public static function fetchScreeningProviderBaseRatesByScreenTypeByDataProvider( $intCriminalScreenType, $intCriminalScreenTypeDataProvider, $objDatabase ) {

		$strSql = 'SELECT
    					spbr.*
					FROM
    					screening_provider_base_rates spbr
						JOIN screening_data_providers sdp ON sdp.id = spbr.screening_data_provider_id
					WHERE
							spbr.screening_data_provider_id = ' . ( int ) $intCriminalScreenTypeDataProvider . '
							AND sdp.id 						= ' . ( int ) $intCriminalScreenTypeDataProvider . '
							AND sdp.screen_type_id 			= ' . ( int ) $intCriminalScreenType;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningProviderBaseRatesIdByScreenTypeByStateOrCountyName( $intCriminalScreenType, $arrstrCriminalSearchareas, $objDatabase, $intCreeningDataProviderId = NULL ) {

		$strSql = 'SELECT
						spbr.id
					FROM
						screening_provider_base_rates spbr
					WHERE ';
		if( $intCriminalScreenType == CScreenType::STATE_CRIMINAL_SEARCH ) {
			$strSql .= ' state_code IN(';
			foreach( $arrstrCriminalSearchareas as $strCriminalSearcharea ) {
				$strSql .= '\'' . $strCriminalSearcharea . '\',';
            }

			$strSql = str_replace( '\'' . $strCriminalSearcharea . '\',', '\'' . $strCriminalSearcharea . '\')', $strSql );
		} else {
			$strSql .= ' county_name IN(';
			foreach( $arrstrCriminalSearchareas as $strCriminalSearcharea ) {
				$strSql .= '\'' . $strCriminalSearcharea . '\',';
            }

			$strSql = str_replace( '\'' . $strCriminalSearcharea . '\',', '\'' . $strCriminalSearcharea . '\')', $strSql );
		}

		if( false == is_null( $intCreeningDataProviderId ) ) {
			$strSql .= ' AND screening_data_provider_id = ' . ( int ) $intCreeningDataProviderId;
		}

		return self::fetchScreeningProviderBaseRates( $strSql, $objDatabase );
	}

	/**
	 * @Description : Fetching the state and county which having max client cost
	 * case : if there are any one state which is associated with multiple data provider then we should have to pick only maximum client cost for that state.
	 * @param $intScreenTypeId
	 * @param $objDatabase
	 * @param $intIsTestClient
	 * @return array
	 */
	public static function fetchScreeningProviderBaseRatesByScreenTypeId( $intScreenTypeId, $objDatabase, $intIsTestClient ) {

		$strSql = 'SELECT
						spbr.*,
						sdp.name AS screening_data_provider_name
					FROM
						screening_provider_base_rates spbr
						JOIN screening_data_providers sdp ON sdp.id = spbr.screening_data_provider_id
						JOIN screening_data_provider_types sdpt ON ( sdpt.id = sdp.screening_data_provider_type_id )
					WHERE ';
		if( true == isset( $intScreenTypeId ) ) {
			$strSql .= ' sdpt.is_test_type = ' . ( int ) $intIsTestClient . ' AND ';
		}

		if( CScreenType::STATE_CRIMINAL_SEARCH == $intScreenTypeId ) {
			$strSql .= ' county_name IS NULL';
		} else {
			$strSql .= ' county_name IS NOT NULL';
		}

		$strSql .= ' AND spbr.is_active = TRUE';

		$strSql .= ' ORDER BY
						spbr.state_code, 
						spbr.client_cost DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningProviderBaseRatesByScreenType( $boolIsCountySearch = false, $intCid = NULL, $objDatabase, $intIsTestClient ) {

		$strSelectCondition = ( $boolIsCountySearch == false ) ? 'distinct on (spbr.state_code) spbr.state_code, ' : '';

		$strSql = 'SELECT	
							' . $strSelectCondition . '
							spbr.*,
							sdp.name as screening_data_provider_name';

		if( true == isset( $intCid ) )
			$strSql .= ' , 	sscr.client_cost as over_ride_client_cost,
 									sscr.cid as cid,
									sscr.is_published as is_published';

		$strSql .= ' FROM
			    					screening_provider_base_rates spbr
									JOIN screening_data_providers sdp ON sdp.id = spbr.screening_data_provider_id';

		if( true == isset( $intCid ) && $boolIsCountySearch == true )
			$strSql .= ' JOIN screening_screen_client_rates sscr ON sscr.county_name = spbr.county_name AND sscr.county_name IS NOT NULL
								JOIN screening_data_provider_types sdpt ON ( sdpt.id = sdp.screening_data_provider_type_id )';

		if( true == isset( $intCid ) && $boolIsCountySearch == false )
			$strSql .= ' JOIN screening_screen_client_rates sscr ON sscr.state_code = spbr.state_code AND sscr.county_name IS NULL
								JOIN screening_data_provider_types sdpt ON ( sdpt.id = sdp.screening_data_provider_type_id )';

		$strSql .= ' WHERE ';

		if( true == isset( $intCid ) )
			$strSql .= ' sscr.cid =' . ( int ) $intCid . '
								AND sdpt.is_test_type = ' . ( int ) $intIsTestClient . '
								AND  sscr.is_published = true AND ';

		if( $boolIsCountySearch == true ) {
			$strSql .= '	spbr.is_active = true
										AND spbr.county_name IS NOT NULL ORDER BY spbr.county_name';
		} else {
			$strSql .= '	spbr.is_active = true
										AND spbr.county_name IS NULL ORDER BY spbr.state_code ASC, spbr.client_cost DESC';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchQuickSearchStateCounty( $arrstrFilteredExplodedSearch, $intIsTestDataProviderTypeId, $objDatabase ) {

		if( false == valArr( $arrstrFilteredExplodedSearch ) ) return NULL;

		$strSql = 'SELECT	spbr.*,
							sdp.name as screening_data_provider_name
						FROM
    						screening_provider_base_rates spbr
							JOIN screening_data_providers sdp ON sdp.id = spbr.screening_data_provider_id
							JOIN screening_data_provider_types sdpt ON ( sdpt.id = sdp.screening_data_provider_type_id )
						WHERE (';

			if( NULL != $intIsTestDataProviderTypeId )
					$strSql .= 'sdpt.is_test_type = ' . ( int ) $intIsTestDataProviderTypeId . ' AND ';

			if( 2 == strlen( $arrstrFilteredExplodedSearch[0] ) && false == is_numeric( $arrstrFilteredExplodedSearch[0] ) ) {
					$strSql	.= '( 	spbr.state_code ILIKE \'%' . implode( '%\' AND spbr.state_code ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )';
			} else {
				$strSql	.= ' spbr.county_name ILIKE \'%' . implode( '%\' AND spbr.county_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'';
			}

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrFilteredExplodedSearch ) && is_numeric( $arrstrFilteredExplodedSearch[0] ) )
					$strSql .= ' 	OR to_char( spbr.id, \'99999999\' ) LIKE \'%' . $arrstrFilteredExplodedSearch[0] . '%\' AND spbr.is_active = TRUE )
									ORDER BY spbr.county_name ASC NULLS FIRST ';
		else {
					$strSql .= ' AND spbr.is_active = TRUE ) 	ORDER BY spbr.county_name ASC NULLS FIRST ';
        }

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchQuickSearchStateCountyResultById( $intId, $objDatabase ) {

		$strSql = 'SELECT
    					spbr.*,
						sdp.name as screening_data_provider_name
					FROM
    					screening_provider_base_rates spbr
						JOIN screening_data_providers sdp ON sdp.id = spbr.screening_data_provider_id
					WHERE
						spbr.id = ' . ( int ) $intId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedScreeningProviderBaseRatesByScreenType( $intPageNo, $intPageSize, $boolIsCountySearch = false, $intCid = NULL, $intIsTestDataProviderTypeId, $objDatabase ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		$strSql = 'SELECT
						spbr.*,
						sdp.name as screening_data_provider_name';

				if( true == isset( $intCid ) )
					$strSql .= ' , 	sscr.client_cost as over_ride_client_cost,
									sscr.cid as cid ';

				$strSql .= ' FROM
			    					screening_provider_base_rates spbr
									JOIN screening_data_providers sdp ON sdp.id = spbr.screening_data_provider_id
									JOIN screening_data_provider_types sdpt ON ( sdpt.id = sdp.screening_data_provider_type_id ) ';

				if( true == isset( $intCid ) && $boolIsCountySearch == true )
						$strSql .= ' JOIN screening_screen_client_rates sscr ON sscr.county_name = spbr.county_name AND sscr.state_code = spbr.state_code AND sscr.county_name IS NOT NULL ';

				if( true == isset( $intCid ) && $boolIsCountySearch == false )
					$strSql .= ' JOIN screening_screen_client_rates sscr ON sscr.state_code = spbr.state_code AND sscr.county_name IS NULL ';

				$strSql .= ' WHERE ';

				if( true == isset( $intCid ) )
					$strSql .= ' sscr.cid =' . ( int ) $intCid . ' AND  sscr.is_published = true AND ';

				if( NULL != $intIsTestDataProviderTypeId )
					$strSql .= ' sdpt.is_test_type =' . ( int ) $intIsTestDataProviderTypeId . ' AND ';

				if( $boolIsCountySearch == true ) {
						$strSql .= ' spbr.is_active = true
									 AND spbr.county_name IS NOT NULL ORDER BY spbr.state_code, spbr.county_name';
				} else {
						$strSql .= ' spbr.is_active = true
									 AND spbr.county_name IS NULL ORDER BY spbr.state_code';
				}

				if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
					$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
				}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchStateOrCountyCount( $boolCountyCount, $intCid, $intIsTestDataProviderTypeId, $objDatabase ) {

		$strSql = 'SELECT
						count(spbr.id)';
		$strSql .= ' FROM
			    		screening_provider_base_rates spbr
						JOIN screening_data_providers sdp ON sdp.id = spbr.screening_data_provider_id
						JOIN screening_data_provider_types sdpt ON ( sdpt.id = sdp.screening_data_provider_type_id ) ';
	if( true == isset( $intCid ) && $boolCountyCount == true )
		   $strSql .= ' JOIN screening_screen_client_rates sscr ON sscr.county_name = spbr.county_name AND sscr.state_code = spbr.state_code AND sscr.county_name IS NOT NULL ';
	if( true == isset( $intCid ) && $boolCountyCount == false )
		   $strSql .= ' JOIN screening_screen_client_rates sscr ON sscr.state_code = spbr.state_code AND sscr.county_name IS NULL ';

		   $strSql .= ' WHERE ';

	if( true == isset( $intCid ) )
		   $strSql .= ' sscr.cid =' . ( int ) $intCid . ' AND  sscr.is_published = true AND ';

	if( NULL != $intIsTestDataProviderTypeId )
			$strSql .= ' sdpt.is_test_type =' . ( int ) $intIsTestDataProviderTypeId . ' AND ';

		$strSql .= ( $boolCountyCount == true ) ? ' spbr.is_active = true AND spbr.county_name IS NOT NULL' : ' spbr.is_active = true AND spbr.county_name IS NULL';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchScreeningProviderBaseRatesByIds( $arrintRateIds, $objDatabase ) {

		if( false == valArr( $arrintRateIds ) ) return;

		$strSql = 'SELECT
						  id,
						  state_code,
						  county_name
					FROM
						  screening_provider_base_rates
					WHERE
						  is_active = true
						  AND id IN( ' . implode( ',', $arrintRateIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningProviderBaseRatesByScreenTypeByDataProviderByClientId( $intCriminalScreenType, $intCriminalScreenTypeDataProvider, $intCid, $boolIsCounty, $objDatabase ) {
		$boolIsCounty = ( bool ) $boolIsCounty;
		$strSql       = 'SELECT
						    DISTINCT ON ( spbr.id ) spbr.id,
						    spbr.state_code,
						    spbr.county_name,
						    spbr.court_fee,
						    spbr.created_by,
						    spbr.created_on,
						    spbr.screening_data_provider_id,
						    spbr.client_cost,
						    spbr.search_fee,
						    spbr.is_active,
						    spbr.search_fee,
						    spbr.updated_on,
						    spbr.updated_by,
						    sscr.client_cost AS client_custom_cost
						FROM
						    screening_provider_base_rates spbr
						    JOIN screening_data_providers sdp ON sdp.id = spbr.screening_data_provider_id
						    LEFT JOIN screening_screen_client_rates sscr ON spbr.state_code = sscr.state_code 
						    ' . ( ( true === $boolIsCounty ) ? ' AND spbr.county_name = sscr.county_name ' : ' AND sscr.county_name IS NULL ' ) . '
						    AND sscr.cid = ' . ( int ) $intCid . ' AND sscr.is_published = TRUE
						WHERE
						    spbr.screening_data_provider_id = ' . ( int ) $intCriminalScreenTypeDataProvider . '
						    AND sdp.id = ' . ( int ) $intCriminalScreenTypeDataProvider . '
						    AND sdp.screen_type_id = ' . ( int ) $intCriminalScreenType . '
						    AND spbr.is_active = TRUE';

		return fetchData( $strSql, $objDatabase );
	}

}
?>