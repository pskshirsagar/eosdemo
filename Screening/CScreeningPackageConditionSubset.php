<?php

class CScreeningPackageConditionSubset extends CBaseScreeningPackageConditionSubset {

	protected $m_intScreeningPackageAvailableConditionId;
	protected $m_intDefaultValue;
    protected $m_intScreeningPackageConditionTypeId;
    protected $m_intScreeningPackageChargeCodeAmountTypeId;

    public function getScreeningPackageAvailableConditionId() {
    	return $this->m_intScreeningPackageAvailableConditionId;
    }

    public function getDefaultValue() {
    	return $this->m_intDefaultValue;
    }

    public function getScreeningPackageConditionTypeId() {
        return $this->m_intScreeningPackageConditionTypeId;
    }

    public function getScreeningPackageChargeCodeAmountTypeId() {
        return $this->m_intScreeningPackageChargeCodeAmountTypeId;
    }

    public function setScreeningPackageAvailableConditionId( $intScreeningPackageAvailableConditionId ) {
    	$this->m_intScreeningPackageAvailableConditionId = $intScreeningPackageAvailableConditionId;
    }

    public function setDefaultValue( $intDefaultValue ) {
    	$this->m_intDefaultValue = $intDefaultValue;
    }

    public function setScreeningPackageConditionTypeId( $intScreeningPackageConditionTypeId ) {
        $this->m_intScreeningPackageConditionTypeId = $intScreeningPackageConditionTypeId;
    }

    public function setScreeningPackageChargeCodeAmountTypeId( $intScreeningPackageChargeCodeAmountTypeId ) {
        $this->m_intScreeningPackageChargeCodeAmountTypeId = $intScreeningPackageChargeCodeAmountTypeId;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrValues['screening_package_available_condition_id'] ) ) $this->setScreeningPackageAvailableConditionId( $arrValues['screening_package_available_condition_id'] );
    	if( true == isset( $arrValues['default_value'] ) ) $this->setDefaultValue( $arrValues['default_value'] );
        if( true == isset( $arrValues['screening_package_condition_type_id'] ) ) $this->setScreeningPackageConditionTypeId( $arrValues['screening_package_condition_type_id'] );
        if( true == isset( $arrValues['screening_package_charge_code_amount_type_id'] ) ) $this->setScreeningPackageChargeCodeAmountTypeId( $arrValues['screening_package_charge_code_amount_type_id'] );

    	return;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>