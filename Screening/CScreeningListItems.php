<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningListItems
 * Do not add any new functions to this class.
 */

class CScreeningListItems extends CBaseScreeningListItems {

	public static function fetchScreeningListItems( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningListItem', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningListItem( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningListItem', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchActiveScreeningApplicantListItemsByScreeningListTypeId( $intScreeningListTypeId, $objDatabase ) {
		$strSql = 'SELECT list_item_name as name, list_item_description as description, screening_list_type_id FROM screening_list_items WHERE screening_list_type_id = ' . ( int ) $intScreeningListTypeId;
		$arrmixResult = executeSql( $strSql, $objDatabase );
		if( 0 != getArrayElementByKey( 'failed', $arrmixResult ) ) return;
		return getArrayElementByKey( 'data', $arrmixResult );
	}
}
?>