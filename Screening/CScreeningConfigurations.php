<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConfigurations
 * Do not add any new functions to this class.
 */

class CScreeningConfigurations extends CBaseScreeningConfigurations {

	public static function fetchPublishedCompanyScreeningConfigsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningConfigurations( sprintf( 'SELECT * FROM %s WHERE cid = %d and is_published = 1 ORDER BY name ASC', 'screening_configurations', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyScreeningConfigurationByIdByCid( $intScreeningConfigId, $intCid, $objDatabase ) {
		return self::fetchScreeningConfiguration( sprintf( 'SELECT * FROM %s WHERE id = %d and cid = %d', 'screening_configurations', ( int ) $intScreeningConfigId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCountPublishedCompanyScreeningConfigsByNameByCid( $intScreeningConfigurationId, $strConfigurationName, $intCid, $objDatabase ) {
		$strSql = 'WHERE name = \'' . addslashes( $strConfigurationName ) . '\'  and id != ' . ( int ) $intScreeningConfigurationId . ' and cid = ' . ( int ) $intCid . ' and is_published = 1';
		return self::fetchScreeningConfigurationCount( $strSql, $objDatabase );
	}

}
?>