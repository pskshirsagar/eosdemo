<?php

class CScreeningCompanyPreferenceType extends CBaseScreeningCompanyPreferenceType {

	const USE_CONDITION_SET_PRIORITIZATION				= 1;
	const ALL_APPLICANT_MUST_PASS_CREDIT_INCOME			= 2;
	const MUST_PASS_CREDIT_FACTORS_BANKRUPTCIES			= 3;
	const MUST_PASS_CREDIT_FACTORS_RENTAL_COLLECTIONS	= 4;
	const ENABLE_GCIC_FORM								= 5;
	const PRECISE_ID_ABANDONED							= 6;
	const HOURS                                         = 1;
	const DAYS                                          = 2;
	const PRIORITIZED_MANUAL_CLIENTS                    = 7;
	const COMBINE_INCOME                                = 8;
	const ALLOW_AVERAGE_VANTAGE_SCORE                   = 9;
	const COMPLETED_VOI_IF_BANK_PROVIDED_AFTER          = 10;
	const COMPANY_SETTING_MAX_DAYS_ALLOW = 3;
	const COMPANY_SETTING_MAX_HOURS_ALLOW = 2;

	public static $c_arrintValidTimePeriods          = array( self::DAYS => 'Days' );

	const DATA_TYPE_BOOLEAN = 'boolean';
	const DATA_TYPE_INTEGER = 'integer';
	const DATA_TYPE_DROPDOWN = 'dropdown';

	public static $c_arrmixPreferenceDataTypeAssociations = [ self::ALL_APPLICANT_MUST_PASS_CREDIT_INCOME => 'boolean', self::USE_CONDITION_SET_PRIORITIZATION => 'boolean', self::PRIORITIZED_MANUAL_CLIENTS => 'boolean',  self::COMBINE_INCOME => 'dropdown', self::COMPLETED_VOI_IF_BANK_PROVIDED_AFTER => 'integer'];
	public static $c_arrmixCreditFactorCompanyPreferenceTypes = [ self::MUST_PASS_CREDIT_FACTORS_BANKRUPTCIES => 'Bankruptcies', self::MUST_PASS_CREDIT_FACTORS_RENTAL_COLLECTIONS => 'Rental Collections' ];

	public static $c_arrintScreeningRecommendationConfigTypeIds = [ self::ALL_APPLICANT_MUST_PASS_CREDIT_INCOME, self::MUST_PASS_CREDIT_FACTORS_BANKRUPTCIES, self::MUST_PASS_CREDIT_FACTORS_RENTAL_COLLECTIONS, self::ENABLE_GCIC_FORM, self::PRECISE_ID_ABANDONED ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDataType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>