<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageConfigPreferenceTypeFilters
 * Do not add any new functions to this class.
 */

class CScreeningPackageConfigPreferenceTypeFilters extends CBaseScreeningPackageConfigPreferenceTypeFilters {

	public static function fetchScreeningPackageConfigPreferenceTypeFiltersByScreeningPackageIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    spcptf.*,
						sf.screening_filter_type_id
					FROM
					    screening_package_config_preference_type_filters spcptf
						JOIN screening_filters sf ON ( sf.id = spcptf.screening_filter_id )
					WHERE
					    screening_package_id = ' . ( int ) $intScreeningPackageId . '
					    AND cid = ' . ( int ) $intCid . '
					    AND spcptf.is_active = 1';

		return self::fetchScreeningPackageConfigPreferenceTypeFilters( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningConfigPreferenceTypeFiltersByScreeningPackageIdByScreenTypeIdsByCid( $intScreeningPackageId, $arrintScreenTypeIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						spcptf.*,
						sf.filter_value AS screening_config_preference_filter_value,
						sf.screening_filter_type_id,
						sf.id as screening_filter_id
					FROM
						screening_package_config_preference_type_filters spcptf
						LEFT JOIN screening_filters sf ON sf.id = spcptf.screening_filter_id
						JOIN screening_config_preference_types scpt ON ( scpt.id = spcptf.screening_config_preference_type_id )
					WHERE
						spcptf.screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND spcptf.cid = ' . ( int ) $intCid . '
						AND spcptf.is_active = 1
						AND scpt.screen_type_id in( ' . implode( ',', $arrintScreenTypeIds ) . ' )';

		return parent::fetchScreeningPackageConfigPreferenceTypeFilters( $strSql, $objDatabase );
	}

	public static function archiveScreeningPackageConfigPreferenceTypeFilters( $intCid, $intPackageId, $intUserId, $objDatabase ) {

		$strSql = ' UPDATE
				       screening_package_config_preference_type_filters
				    SET
				       is_active = 0,
				       updated_by = ' . ( int ) $intUserId . ',
				       updated_on = NOW ( )
				    WHERE
                       cid = ' . ( int ) $intCid . '
				       AND screening_package_id = ' . ( int ) $intPackageId;

		$boolIsSuccess = ( false === fetchData( $strSql, $objDatabase ) ) ? false : true;

		return $boolIsSuccess;
	}

}
?>