<?php

class CScreeningPackageScreenTypeCriteria extends CBaseScreeningPackageScreenTypeCriteria {

	private $m_intScreeningDataProviderId;
	private $m_intScreenTypeId;

	private $m_strScreeningDataProviderName;

	public function setScreeningDataProviderId( $intScreeningDataProviderId ) {
		$this->m_intScreeningDataProviderId = $intScreeningDataProviderId;
	}

	public function setScreenTypeId( $intScreenTypeId ) {
		$this->m_intScreenTypeId = $intScreenTypeId;
	}

	public function setScreeningDataProviderName( $strScreeningDataProviderName ) {
		$this->m_strScreeningDataProviderName = $strScreeningDataProviderName;
	}

	public function getScreeningDataProviderId() {
		return $this->m_intScreeningDataProviderId;
	}

	public function getScreenTypeId() {
		return $this->m_intScreenTypeId;
	}

	public function getScreeningDataProviderName() {
		return $this->m_strScreeningDataProviderName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningPackageId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningPackageScreenTypeAssociationsId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningSearchCriteriaTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCriteria() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDispatchType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrmixValues['screen_type_id'] ) )			   		$this->setScreenTypeId( $arrmixValues['screen_type_id'] );
		if( isset( $arrmixValues['screening_data_provider_id'] ) ) 		$this->setScreeningDataProviderId( $arrmixValues['screening_data_provider_id'] );
		if( isset( $arrmixValues['screening_data_provider_name'] ) )	$this->setScreeningDataProviderName( $arrmixValues['screening_data_provider_name'] );

		return;
	}

	public function createCriteraInJsonFormat( $intCriminalScreenTypeId, $arrintSelectedStatesOrCountyIds, $arrmixKeyScreeningProviderBaseRates ) {

		$arrstrSelectedStateOrCountyNames = array();

		foreach( $arrintSelectedStatesOrCountyIds as $intId ) {
			$arrstrSelectedStateOrCountyNames[] = ( $intCriminalScreenTypeId == CScreenType::STATE_CRIMINAL_SEARCH ) ? $arrmixKeyScreeningProviderBaseRates[$intId]['state_code'] : $arrmixKeyScreeningProviderBaseRates[$intId]['county_name'] . '~' . $arrmixKeyScreeningProviderBaseRates[$intId]['state_code'];
		}

		return json_encode( $arrstrSelectedStateOrCountyNames );
	}

	public function getSearchTypes( $intCurrentUserId, $intScreenTypeId, $objScreeningPropertyAccount, $objDatabase, $objScreeningTransaction = NULL, $arrobjExistingManualCriminalTransactions = NULL, $objScreeningApplicant = NULL ) {

		$objResidentVerifySecureClientLibrary = new CResidentVerifySecureClientLibrary();

		$arrmixAllScreeningSearchCriterias = json_decode( $this->getCriteria(), true );

		if( false == valArr( $arrmixAllScreeningSearchCriterias ) ) return;

		$arrmixSearchTypes = array();

		switch( $this->getScreeningSearchCriteriaTypeId() ) {
			case CScreeningSearchCriteriaType::ALWAYS:
				$arrmixSearchTypes[$this->getScreenTypeId()] = $arrmixAllScreeningSearchCriterias;
				break;

			case CScreeningSearchCriteriaType::PREVIOUS_ADDRESS:
			case CScreeningSearchCriteriaType::WHEN_REPORTED:
				if( false == valObj( $objScreeningTransaction, 'CScreeningTransaction' ) ) break;

				$arrmixOffenseDescriptions = array();

				if( true == $this->getOffenseDescription() ) {
					$arrmixOffenseDescriptions = CScreeningListItems::fetchActiveScreeningApplicantListItemsByScreeningListTypeId( CScreeningListType::OFFENSE_DESCRIPTION, $objDatabase );
				}

				// This function differentiate search criterias in two categories new and existing..

				$arrmixScreeningSearchCriterias = $this->getDifferentiatedSearchCriterias( $arrmixAllScreeningSearchCriterias, $arrobjExistingManualCriminalTransactions );

				if( false == valArr( $arrmixScreeningSearchCriterias['NewSearchCriterias'] ) ) {
					$arrmixSearchTypes = $arrmixScreeningSearchCriterias['ExistingSearchCriterias'];
					break;
				}

				$arrmixNewSearchCriterias[$intScreenTypeId] = $arrmixScreeningSearchCriterias['NewSearchCriterias'];

				$objStdSecureClientRequest = CResidentVerifySecureRequestBuilder::applicantSearchRecommendationRequest( $intCurrentUserId, $objScreeningTransaction, $arrmixNewSearchCriterias, $intScreenTypeId, $arrmixOffenseDescriptions );
				$arrmixSecureResponse 	   = $objResidentVerifySecureClientLibrary->getApplicantSearchRecommendations( $objStdSecureClientRequest );

				if( false == valArr( $arrmixSecureResponse ) ) break;

				$arrmixSearchTypes = ( true == valArr( $arrmixSecureResponse['response']['result']['criminalSearchRecommendations'] ) ) ? $arrmixSecureResponse['response']['result']['criminalSearchRecommendations'] : NULL;

				if( true == valArr( $arrmixSearchTypes ) ) {
					if( true == valArr( $arrmixScreeningSearchCriterias['ExistingSearchCriterias'] ) ) {
						$arrmixSearchTypes += $arrmixScreeningSearchCriterias['ExistingSearchCriterias'];
					}
				} else {
					$arrmixSearchTypes = $arrmixScreeningSearchCriterias['ExistingSearchCriterias'];
				}
				break;

			case CScreeningSearchCriteriaType::CURRENT_ADDRESS:
				// check the Screening applicant's current address

				$arrmixSearchTypes = array();

				$intZipCode = ( int ) \Psi\CStringService::singleton()->substr( $objScreeningApplicant->getZipCode(), 0, 5 );

				$strSceeningApplicantCounty = ( CScreenType::COUNTY_CRIMINAL_SEARCH == $intScreenTypeId ) ? CScreeningUtils::getCounty( $intZipCode, $objScreeningApplicant->getState(), $objScreeningApplicant, $intCurrentUserId, $objResidentVerifySecureClientLibrary ) : NULL;

				foreach( $arrmixAllScreeningSearchCriterias as $strSearchCriteriaType ) {

					if( ( CScreenType::COUNTY_CRIMINAL_SEARCH == $intScreenTypeId && \Psi\CStringService::singleton()->strtolower( trim( $strSceeningApplicantCounty ) . '~' . trim( $objScreeningApplicant->getState() ) ) != trim( \Psi\CStringService::singleton()->strtolower( $strSearchCriteriaType ) ) )
						|| ( CScreenType::STATE_CRIMINAL_SEARCH == $intScreenTypeId && trim( \Psi\CStringService::singleton()->strtolower( $objScreeningApplicant->getState() ) ) != trim( \Psi\CStringService::singleton()->strtolower( $strSearchCriteriaType ) ) ) ) continue;

					$arrmixSearchTypes[$this->getScreenTypeId()] = [ $strSearchCriteriaType => $strSearchCriteriaType ];
				}
				break;

			case CScreeningSearchCriteriaType::PROPERTY_ADDRESS:
				// check the Screening applicant's Property address

				if( false == valObj( $objScreeningPropertyAccount, 'CScreeningPropertyAccount' ) || false == valStr( $objScreeningPropertyAccount->getZipCode() ) || false == valStr( $objScreeningPropertyAccount->getStateCode() ) ) break;

				$arrmixSearchTypes = array();

				$intZipCode     = ( int ) substr( $objScreeningPropertyAccount->getZipCode(), 0, 5 );
				$strStateCode   = $objScreeningPropertyAccount->getStateCode();

				$strSceeningApplicantCounty = ( CScreenType::COUNTY_CRIMINAL_SEARCH == $intScreenTypeId ) ? CScreeningUtils::getCounty( $intZipCode, $strStateCode, $objScreeningApplicant, $intCurrentUserId, $objResidentVerifySecureClientLibrary ) : NULL;

				foreach( $arrmixAllScreeningSearchCriterias as $strSearchCriteriaType ) {

					if( ( CScreenType::COUNTY_CRIMINAL_SEARCH == $intScreenTypeId && strtolower( trim( $strSceeningApplicantCounty ) . '~' . trim( $strStateCode ) ) != trim( strtolower( $strSearchCriteriaType ) ) )
					    || ( CScreenType::STATE_CRIMINAL_SEARCH == $intScreenTypeId && trim( strtolower( $strStateCode ) ) != trim( strtolower( $strSearchCriteriaType ) ) ) ) continue;

					$arrmixSearchTypes[$this->getScreenTypeId()] = [ $strSearchCriteriaType => $strSearchCriteriaType ];
				}
				break;

			default:
				// return blank
				break;
		}

		return $arrmixSearchTypes;
	}

	private function getDifferentiatedSearchCriterias( $arrmixAllScreeningSearchCriterias, $arrobjExistingManualCriminalTransactions ) {

		$arrmixSearchCriterias['NewSearchCriterias'] 	  = array();
		$arrmixSearchCriterias['ExistingSearchCriterias'] = array();

		if( false == valArr( $arrobjExistingManualCriminalTransactions ) ) {
			$arrmixSearchCriterias['NewSearchCriterias'] = $arrmixAllScreeningSearchCriterias;
			return $arrmixSearchCriterias;
		}

		$arrobjExistingManualCriminalTransactions = rekeyObjects( 'SearchType', $arrobjExistingManualCriminalTransactions );

		foreach( $arrmixAllScreeningSearchCriterias as $strSearchCriteriaType ) {
			if( false == array_key_exists( $strSearchCriteriaType, $arrobjExistingManualCriminalTransactions ) || true == in_array( $arrobjExistingManualCriminalTransactions[$strSearchCriteriaType]->getScreeningStatusTypeId(), CScreeningStatusType::$c_arrintScreeningCancelledStatusIds ) ) {
				$arrmixSearchCriterias['NewSearchCriterias'][]  = $strSearchCriteriaType;
			} else {
				$arrmixSearchCriterias['ExistingSearchCriterias'][] = $strSearchCriteriaType;
			}
		}

		return $arrmixSearchCriterias;
	}

}
?>