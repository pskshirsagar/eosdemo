<?php

class CScreeningReportRequestStatus extends CBaseScreeningReportRequestStatus {

	const INPROGRESS        = 1; // request is in progress
	const REPORT_READY      = 2; // request Report Ready
	const REPORT_SENT       = 3; // request Report Sent
	const FAILED            = 4; // Unable to send report
	const DELETED_FAILED    = 5; // Deleted failed request

    public static $c_arrintUnderProcessScreeningApplicantReportRequestStatuses = [
        self::INPROGRESS,
        self::REPORT_READY,
        self::FAILED
    ];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>