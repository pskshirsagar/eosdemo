<?php

class CScreeningTransaction extends CBaseScreeningTransaction {

	protected $m_objParentTransaction;

	protected $m_intExistingScreeningTransactionId;
	protected $m_intScreeningDataProviderTypeId;
	protected $m_intParentScreenTypeId;

	protected $m_boolIsManualCriminalSearchEnabled;
	protected $m_boolIsEvaluationRequired;
	protected $m_boolIsOnMaintenance;

	protected $m_arrmixScreeningSearchCriterias;

	protected $m_arrobjChildTransactions;

	protected $m_objScreeningDataProvider;

	protected $m_strPropertyStateCode;
	protected $m_arrintParentReferenceRecordIds;

	const PROCESSING_STATUS_SKIPPED = 'SKIPPED';

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setParentTransaction( $objParentTransaction ) {
		$this->m_objParentTransaction = $objParentTransaction;
	}

	public function setParentReferenceRecordIds( $strParentReferenceRecordIds ) {
		$this->m_arrintParentReferenceRecordIds = $strParentReferenceRecordIds;
	}

	public function setChildTransactions( $arrobjScreeningTransactions ) {
		$this->m_arrobjChildTransactions = rekeyObjects( 'ScreenTypeId', $arrobjScreeningTransactions );
	}

	public function setParentScreenTypeId( $intParentScreenTypeId ) {
		$this->m_intParentScreenTypeId = $intParentScreenTypeId;
	}

	public function setExistingScreeningTransactionId( $intExistingScreeningTransactionId ) {
		$this->m_intExistingScreeningTransactionId = $intExistingScreeningTransactionId;
	}

	public function setScreeningDataProviderTypeId( $intScreeningDataProviderTypeId ) {
		$this->m_intScreeningDataProviderTypeId = $intScreeningDataProviderTypeId;
	}

	public function setIsManualCriminalSearchEnabled( $boolIsManualCriminalSearchEnabled ) {
		$this->m_boolIsManualCriminalSearchEnabled = $boolIsManualCriminalSearchEnabled;
	}

	public function setScreeningDataProvider( $objScreeningDataProvider ) {
		$this->m_objScreeningDataProvider = $objScreeningDataProvider;
	}

	public function setPropertyStateCode( $strPropertyStateCode ) {
		$this->m_strPropertyStateCode = $strPropertyStateCode;
	}

	public function setIsEvaluationRequired( $boolIsEvaluationRequired ) {
		$this->m_boolIsEvaluationRequired = $boolIsEvaluationRequired;
	}

	public function setOnMaintenance( $boolIsOnMaintenance ) {
		$this->m_boolIsOnMaintenance = $boolIsOnMaintenance;
	}

	public function getParentTransaction() {
		return $this->m_objParentTransaction;
	}

	public function getParentReferenceRecordIds() {
		return $this->m_arrintParentReferenceRecordIds;
	}

	public function getChildTransactions() {
		return $this->m_arrobjChildTransactions;
	}

	public function getParentScreenTypeId() {
		return $this->m_intParentScreenTypeId;
	}

	public function getExistingScreeningTransactionId() {
		return $this->m_intExistingScreeningTransactionId;
	}

	public function getScreeningDataProviderTypeId() {
		return $this->m_intScreeningDataProviderTypeId;
	}

	public function getIsManualCriminalSearchEnabled() {
		return $this->m_boolIsManualCriminalSearchEnabled;
	}

	public function getScreeningDataProvider() {
		return $this->m_objScreeningDataProvider;
	}

	public function getPropertyStateCode() {
		return $this->m_strPropertyStateCode;
	}

	public function getIsEvaluationRequired() {
		return $this->m_boolIsEvaluationRequired;
	}

	public function getOnMaintenance() {
		return $this->m_boolIsOnMaintenance;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrmixValues['screening_data_provider_type_id'] ) ) $this->setScreeningDataProviderTypeId( $arrmixValues['screening_data_provider_type_id'] );
		if( isset( $arrmixValues['parent_screen_type_id'] ) ) $this->setParentScreenTypeId( $arrmixValues['parent_screen_type_id'] );
		if( isset( $arrmixValues['is_evaluation_required'] ) ) $this->setIsEvaluationRequired( $arrmixValues['is_evaluation_required'] );
		if( isset( $arrmixValues['property_state_code'] ) ) $this->setPropertyStateCode( $arrmixValues['property_state_code'] );
		if( isset( $arrmixValues['on_maintenance'] ) ) $this->setOnMaintenance( $arrmixValues['on_maintenance'] );

		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningApplicantId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreenTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyScreeningAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningOrderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function determineScreeningTransactionScreeningRequestType() {

		$intScreeningTransactionScreeningRequestTypeId = NULL;

		switch( $this->getScreeningStatusTypeId() ) {
			case CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR:
			case CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN:
				if( false == is_null( $this->getScreeningOrderId() ) ) {
					if( false == is_null( $this->getScreeningResponseCompletedOn() ) ) {
						$intScreeningTransactionScreeningRequestTypeId = CScreeningRequestType::EVALUATE_RESULT;
					} else {
						$intScreeningTransactionScreeningRequestTypeId = CScreeningRequestType::CHECK_STATUS;
					}
				} else {
					$intScreeningTransactionScreeningRequestTypeId = CScreeningRequestType::UNUSED;
				}
				break;

			case CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED:
				$intScreeningTransactionScreeningRequestTypeId = CScreeningRequestType::CHECK_STATUS;
				break;

			default:
				$intScreeningTransactionScreeningRequestTypeId = CScreeningRequestType::EVALUATE_RESULT;
				break;
		}

		return $intScreeningTransactionScreeningRequestTypeId;
	}

	public function updateScreeningTransactionStatus( $intCurrentUserId, $intScreeningStatusTypeId, $objScreeningDatabase ) {

		$this->setScreeningStatusTypeId( $intScreeningStatusTypeId );

		$objScreeningDatabase->begin();

		if( false == $this->update( $intCurrentUserId, $objScreeningDatabase ) ) {
			$objScreeningDatabase->rollback();
			trigger_error( 'Failed to update screening transaction status for screening transaction id : ' . $this->getId() . ' and cid : ' . $this->getCid(), E_USER_WARNING );
			return false;
		}

		$objScreeningDatabase->commit();

		return;
	}

	public function updateChildScreeningTransactionStatus( $intCurrentUserId, $objScreeningDatabase, $boolActiveChildTransaction = false ) {
		$arrobjChildScreeningTransactions = ( array ) CScreeningTransactions::fetchChildScreeningTransactionsByScreenTypeIdByScreeningIdByScreeningApplicantId( $this->getScreenTypeId(), $this->getScreeningId(), $this->getScreeningApplicantId(), $objScreeningDatabase );

       foreach( $arrobjChildScreeningTransactions as $objChildScreeningTransaction ) {
            if( true == $boolActiveChildTransaction ) {
                if( true == in_array( $objChildScreeningTransaction->getScreenTypeId(), CScreenType::$c_arrintManualCriminalSearchScreenTypeIds ) ) {
					if( true == $objChildScreeningTransaction->checkIsTransactionReviewCompleted( $objScreeningDatabase ) ) {
						$objChildScreeningTransaction->updateScreeningTransactionStatus( $intCurrentUserId, CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED, $objScreeningDatabase );
					} elseif( false == $objChildScreeningTransaction->checkIsTransactionReviewCompleted( $objScreeningDatabase ) && true == $objChildScreeningTransaction->isManualReviewRequired( $objScreeningDatabase ) ) {
						$objChildScreeningTransaction->updateScreeningTransactionStatus( $intCurrentUserId, CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW, $objScreeningDatabase );
					} else {
						$objChildScreeningTransaction->updateScreeningTransactionStatus( $intCurrentUserId, CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN, $objScreeningDatabase );
					}
                } else {
	                $objChildScreeningTransaction->updateScreeningTransactionStatus( $intCurrentUserId, CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED, $objScreeningDatabase );
                }
            } else {
                if( false == is_null( $objChildScreeningTransaction->getScreeningOrderId() ) ) {
	                $objChildScreeningTransaction->updateScreeningTransactionStatus( $intCurrentUserId, CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED, $objScreeningDatabase );
                } else {
	                $objChildScreeningTransaction->updateScreeningTransactionStatus( $intCurrentUserId, CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED, $objScreeningDatabase );
                }
            }
       }
    }

	public function updateScreeningTransactionRecommendation( $intCurrentUserId, $intScreeningRecommendationTypeId, $intScreeningPackageConditionSetId, $objScreeningDatabase ) {
		$this->setScreeningRecommendationTypeId( $intScreeningRecommendationTypeId );
		$this->setScreeningPackageConditionSetId( $intScreeningPackageConditionSetId );

		$objScreeningDatabase->begin();

		if( false == $this->insertOrUpdate( $intCurrentUserId, $objScreeningDatabase ) ) {
			trigger_error( 'Failed to update screening applicant result for screening id :[' . $this->getScreeningId() . '] client id : [' . $this->getCid() . ']', E_USER_WARNING );
			$objScreeningDatabase->rollback();
		}

		$objScreeningDatabase->commit();
	}

	public function updateTransactionRecommendation( $intCurrentUserId, $objScreeningDatabase ) {

		$objScreeningDatabase->begin();

		if( false == $this->update( $intCurrentUserId, $objScreeningDatabase ) ) {
			trigger_error( 'Failed to update screening applicant result for screening id :[' . $this->getScreeningId() . '] client id : [' . $this->getCid() . ']', E_USER_WARNING );
			$objScreeningDatabase->rollback();
		}

		$objScreeningDatabase->commit();
	}

	public function updateScreeningTransactionCompletionStatus( $intCurrentUserId, $intScreeningStatusTypeId, $objScreeningDatabase ) {

		$this->setScreeningStatusTypeId( $intScreeningStatusTypeId );

		if( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED == $intScreeningStatusTypeId ) $this->setScreeningCompletedOn( 'NOW()' );

		$objScreeningDatabase->begin();

		if( false == $this->update( $intCurrentUserId, $objScreeningDatabase ) ) {
			$objScreeningDatabase->rollback();
			trigger_error( 'Failed to update screening transaction completion status for screening transaction id : ' . $this->getId() . ' and cid : ' . $this->getCid(), E_USER_WARNING );
			return false;
		}

		$objScreeningDatabase->commit();

		return;
	}

	public function isManualReviewRequired( $objDatabase ) {

		$strWhere = 'WHERE screening_transaction_id = ' . ( int ) $this->getId() . 'AND cid = ' . ( int ) $this->getCid();

		return ( 0 < CTransactionReviews::fetchTransactionReviewCount( $strWhere, $objDatabase ) ) ? true : false;
	}

	public function getDependantScreeningSearchCriteriaTypeId( $objDatabase ) {

		$arrobjScreeningSearchCriteriaTypes = CScreeningSearchCriteriaTypes::fetchAllScreeningSearchCriteriaTypes( $objDatabase );

		if( false == valArr( $arrobjScreeningSearchCriteriaTypes ) ) return false;

		$intScreeningSearchCriteriaTypeId = NULL;

		foreach( $arrobjScreeningSearchCriteriaTypes as $objScreeningSearchCriteriaType ) {

			if( false == valArr( $objScreeningSearchCriteriaType->getRequiredScreenTypes() ) || false == in_array( $this->getScreenTypeId(), $objScreeningSearchCriteriaType->getRequiredScreenTypes() ) ) continue;

			$intScreeningSearchCriteriaTypeId = $objScreeningSearchCriteriaType->getId();
			break;
		}

		return $intScreeningSearchCriteriaTypeId;
	}

	public function createTransactionReviewRequest( $intCurrentUserId, $intTransactionReviewQueueTypeId, $objDatabase ) {
		$objTransactionReview = $this->fetchTransactionReview( $objDatabase );

        if( true == valObj( $objTransactionReview, 'CTransactionReview' ) ) return $objTransactionReview;

        $arrmixClientScreeningCompanyPreferenceSettings = \Psi\Eos\Screening\CScreeningCompanyPreferences::createService()->fetchActiveScreeningCompanyPreferencesByScreeningCompanyPreferenceTypeIdBYCid( CScreeningCompanyPreferenceType::PRIORITIZED_MANUAL_CLIENTS, $this->getCid(), $objDatabase );
		$arrmixClientScreeningCompanyPreferenceSettings = ( true == valArr( $arrmixClientScreeningCompanyPreferenceSettings ) ) ? rekeyArray( 'screening_company_preference_type_id', $arrmixClientScreeningCompanyPreferenceSettings ) : NULL;

		if( true == valArr( $arrmixClientScreeningCompanyPreferenceSettings ) && true == array_key_exists( CScreeningCompanyPreferenceType::PRIORITIZED_MANUAL_CLIENTS, $arrmixClientScreeningCompanyPreferenceSettings ) ) {
			$intTransactionReviewPriorityId = CTransactionReviewPriority::URGENT;
		} else {
			$intTransactionReviewPriorityId = CTransactionReviewPriority::LOW;
		}

        $objTransactionReview = new CTransactionReview();

        $objTransactionReview->setCid( $this->getCid() );
        $objTransactionReview->setScreeningTransactionId( $this->getId() );
        $objTransactionReview->setTransactionReviewStatusId( CTransactionReviewStatus::REVIEW_STATUS_NEW );
        $objTransactionReview->setTransactionReviewPriorityId( $intTransactionReviewPriorityId );
        $objTransactionReview->setTransactionReviewQueueTypeId( $intTransactionReviewQueueTypeId );
        $objTransactionReview->setCreatedOn( 'NOW()' );
        $objTransactionReview->setUpdatedOn( 'NOW()' );

		if( false == $objTransactionReview->insertOrUpdate( $intCurrentUserId, $objDatabase ) ) {
			trigger_error( 'Failed to create screening transaction review for screening transaction id : ' . $this->getId() . ' cid : ' . $this->getCid(), E_USER_WARNING );
			return false;
		}

		return $objTransactionReview;
	}

	public function getTransactionReviewIds() {

		$arrintTransactionReviewIds = CTransactionReviews::fetchCustomTransactionReviewDataByScreeningTransactionIdsByCid( array( $this->getId() ), $this->getCid(), $this->getDatabase() );
		$arrintTransactionReviewIds = ( true == valArr( $arrintTransactionReviewIds ) ) ? $arrintTransactionReviewIds[0] : array();
		return $arrintTransactionReviewIds;
	}

	/*
	 * fetch functions
	 */

	public function fetchTransactionReview( $objDatabse ) {
		return CTransactionReviews::fetchTransactionReviewByScreeningTransactionIdByCid( $this->getId(), $this->getCid(), $objDatabse );
	}

	/*
	 * ends here
	 */

	public function updateScreeningTransactionAdditionaldetails( $intUserId, $objDatabase, $arrmixAdditionalData ) {

		$objstdAdditionalDetail = ( true == valObj( $this->getAdditionalDetails(), 'stdClass' ) ) ? $this->getAdditionalDetails() : [];

		if( property_exists( $objstdAdditionalDetail, 'reportId' ) ) {
			unset( $objstdAdditionalDetail->reportId );
		}

		if( property_exists( $objstdAdditionalDetail, 'iframeUrl' ) ) {
			unset( $objstdAdditionalDetail->iframeUrl );
		}

		if( property_exists( $objstdAdditionalDetail, 'iframe_expired_on' ) ) {
			unset( $objstdAdditionalDetail->iframe_expired_on );
		}

		if( property_exists( $objstdAdditionalDetail, 'noBankProvided' ) ) {
			unset( $objstdAdditionalDetail->noBankProvided );
		}

		if( true == array_key_exists( 'expired_on', $arrmixAdditionalData ) ) {
			$objstdAdditionalDetail->expired_on = $arrmixAdditionalData['expired_on'];
		}

		$this->setAdditionalDetails( $objstdAdditionalDetail );

		if( true == $this->getIsSkipped() ) {
			$this->setIsSkipped( false );
		}

		if( false == $this->update( $intUserId, $objDatabase ) ) {
			trigger_error( 'Failed to reinitialize for transaction id ' . $this->getId() . ' and screening id ' . $this->getScreeningId() . ' and cid ' . $this->getCid() );
			return false;
		}
		return $this;
	}

	public function reopenScreeningTransaction( $intUserId, $objDatabase, $intReopenScreeningTransactionStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN ) {
		$intScreeningStatusTypeId = $this->getScreeningStatusTypeId();

		$this->setScreeningStatusTypeId( $intReopenScreeningTransactionStatusTypeId );
		$this->setScreeningCompletedOn( NULL );
		$this->setScreeningRecommendationUpdatedOn( NULL );

		if( $intScreeningStatusTypeId != CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW && true == in_array( $this->getScreenTypeId(), CScreenType::$c_arrintSkipManualReviewScreenTypeIds ) ) {
			$this->setOriginalScreeningCompletedOn( ( true == is_null( $this->getOriginalScreeningCompletedOn() ) ) ? $this->getScreeningCompletedOn() : $this->getOriginalScreeningCompletedOn() );
			$this->setScreeningResponseCompletedOn( ( true == is_null( $this->getScreeningResponseCompletedOn() ) ) ? $this->getScreeningCompletedOn() : $this->getScreeningResponseCompletedOn() );
		} else {
			$this->setOriginalScreeningCompletedOn( NULL );
			$this->setScreeningResponseCompletedOn( NULL );
		}

		$this->setScreeningPackageConditionSetId( NULL );
		$this->setScreeningRecommendationTypeId( CScreeningRecommendationType::PENDING_UNKNOWN );

		if( false == $this->update( $intUserId, $objDatabase ) ) {
			trigger_error( 'Failed to reopen screening transaction for screening transaction id : ' . $this->getId() . ' and cid : ' . $this->getCid(), E_USER_WARNING );
			return false;
		}

		return true;
	}

	public function getCost( $objScreeningAccountRate, $objDatabase ) {

		if( false == valObj( $objScreeningAccountRate, 'CScreeningAccountRate' ) ) {
			trigger_error( 'Failed to screening account rate for screen type id : ' . $this->getScreenTypeId() . ' screening applicant id : ' . $this->getScreeningApplicantId() . ' cid : ' . $this->getCid(), E_USER_ERROR );
		}

		$fltCost = 0;

		if( true == in_array( $this->getScreenTypeId(), CScreenType::$c_arrintManualCriminalSearchScreenTypeIds ) ) {

			$arrmixSearchTypes = explode( '~', $this->getSearchType() );
			$strState  = ( CScreenType::COUNTY_CRIMINAL_SEARCH == $this->getScreenTypeId() ) ? $arrmixSearchTypes[1] : $arrmixSearchTypes[0];
			$strCounty = ( CScreenType::COUNTY_CRIMINAL_SEARCH == $this->getScreenTypeId() ) ? $arrmixSearchTypes[0] : '';

			$objScreeningClientRate = CScreeningScreenClientRates::fetchCustomScreeningClientRateBySearchTypeStateBySearchTypeCountyByCId( $strState, $strCounty, $this->getCid(), $objDatabase );

			if( true == valObj( $objScreeningClientRate, 'CScreeningScreenClientRate' ) ) {
				$fltCost = $objScreeningClientRate->getClientCost();
			} else {

				$objScreeningProviderBaseRate = CScreeningProviderBaseRates::fetchScreeningProviderBaseRatesBySearchTypeByScreeningDataProviderId( $strState, $strCounty, $this->getScreenTypeId(), $this->getScreeningDataProviderId(), $objDatabase );

				if( false == valObj( $objScreeningProviderBaseRate, 'CScreeningProviderBaseRate' ) ) {
					trigger_error( 'Failed to load screening provider base rate for search type : - state : ' . $strState . ' : county : ' . $strCounty, E_USER_ERROR );
					return;
				}

				// Change rv fee to client cost..
				$fltCost = $objScreeningAccountRate->getCost() + $objScreeningProviderBaseRate->getClientCost();
			}

		} else {
			$fltCost = $objScreeningAccountRate->getCost();
		}

		return $fltCost;
	}

	public function autoProcessScreeningTransaction( $intCurrentUserId, $intScreeningPackageId, $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		$arrobjScreeningPackageSetting = ( array ) CScreeningCriteriaSettings::fetchScreeningCriteriaSettingsByScreeningPackageIdsByScreeningConfigPreferenceTypeIds( [ $intScreeningPackageId ], array( $intScreeningConfigPreferenceTypeId ), $objDatabase );
		$objScreeningPackageSetting	   = getArrayElementByKey( $intScreeningConfigPreferenceTypeId, rekeyObjects( 'ScreeningConfigPreferenceTypeId', $arrobjScreeningPackageSetting ) );

		if( false == valObj( $objScreeningPackageSetting, 'CScreeningCriteriaSetting' ) ) {
			trigger_error( 'Failed to load screening package setting for screen type : ' . CScreenType::getScreenTypeName( $this->getScreenTypeId() ) . ' and screening applicant id : ' . $this->getScreeningApplicantId() . ' and cid : ' . $this->getCid(), E_USER_ERROR );
			return false;
		}

		$objResidentVerifySecureClientLibrary = new CResidentVerifySecureClientLibrary();

		$this->setScreeningRecommendationTypeId( $objScreeningPackageSetting->getScreeningRecommendationTypeId() );
		$this->setScreeningPackageConditionSetId( $objScreeningPackageSetting->getScreeningConditionSetId() );

		$arrmixScreeningApplicantResult['CScreeningApplicantResultValues'] = getArrayElementByKey( 'screeningApplicantResults', CResidentVerifyServiceUtil::prepareScreeningApplicantResultArray( $this, $intScreeningConfigPreferenceTypeId, 1 ) );

		$objStdSecureUpdateApplicantResultsRequest = CResidentVerifySecureRequestBuilder::archeiveAndInsertOrUpdateScreeningResult( $intCurrentUserId, $this, $arrmixScreeningApplicantResult );

		$arrmixSecureResponse = $objResidentVerifySecureClientLibrary->archiveAndInsertOrUpdateApplicantResults( $objStdSecureUpdateApplicantResultsRequest );

		if( false == valArr( $arrmixSecureResponse ) || false == $arrmixSecureResponse['response']['result']['Success'] ) {
			trigger_error( 'Failed to update result in secure database for screen type id : ' . $this->getScreenTypeId(), E_USER_ERROR );
		}

		// if valid response with success message true then set below values
		$this->setTransactionAmount( 0 );
		$this->setScreeningResponseCompletedOn( 'NOW()' );
		$this->setScreeningOrderId( self::PROCESSING_STATUS_SKIPPED );

        $this->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED );
		$this->setScreeningCompletedOn( 'NOW()' );
        $this->setScreeningRecommendationUpdatedOn( 'NOW()' );

		if( false == $this->update( $this->m_intCurrentUserId, $objDatabase ) ) {
			trigger_error( 'Failed to update screening transaction for no ssn changes for screening id ' . $this->getScreeningId() . ' cid ' . $this->getCid(), E_USER_ERROR );
			return false;
		}

        if( true == valArr( $this->getChildTransactions() ) ) {
            foreach( $this->getChildTransactions() as $objChildTransaction ) {
                $objChildTransaction->setTransactionAmount( 0 );
                $objChildTransaction->setScreeningResponseCompletedOn( 'NOW()' );
                $objChildTransaction->setScreeningOrderId( self::PROCESSING_STATUS_SKIPPED );

                $objChildTransaction->setScreeningRecommendationTypeId( $objScreeningPackageSetting->getScreeningRecommendationTypeId() );
                $objChildTransaction->setScreeningPackageConditionSetId( $objScreeningPackageSetting->getScreeningConditionSetId() );

                $objChildTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED );
                $objChildTransaction->setScreeningCompletedOn( 'NOW()' );
	            $objChildTransaction->setScreeningRecommendationUpdatedOn( 'NOW()' );

                if( false == $objChildTransaction->update( $this->m_intCurrentUserId, $objDatabase ) ) {
                    trigger_error( 'Failed to update screening transaction for no ssn changes for screening id ' . $this->getScreeningId() . ' cid ' . $this->getCid() . ' screening transaction id ' . $objChildTransaction->getId(), E_USER_ERROR );
                    $objDatabase->rollback();
                }
            }
        }

		return;
	}

	public function fetchTransactionReviews( $objDatabse ) {
		return CTransactionReviews::fetchTransactionReviewsByScreeningTransactionIdByCid( $this->getId(), $this->getCid(), $objDatabse );
	}

	public function determineTransactionStatusAction( $intScreeningPackageId, $objScreeningDatabase ) {
		// This will always return false for non criminal screen type
		// If screen types of type criminal further checks are required...
		if( false == in_array( $this->getScreenTypeId(), CScreenType::$c_arrintManualCriminalSearchScreenTypeIds ) ) return false;

		$strSql = 'WHERE screen_type_id = ' . ( int ) CScreenType::CRIMINAL . ' AND screening_package_id = ' . ( int ) $intScreeningPackageId;
		$boolNationalCriminalTransactionExist = ( 0 < CScreeningPackageScreenTypeAssociations::fetchScreeningPackageScreenTypeAssociationCount( $strSql, $objScreeningDatabase ) ) ? true : false;
		if( false == $boolNationalCriminalTransactionExist ) {
			return false;
		} else {
			$boolIsManualSearchAvailableInScreeningPackage = ( true == $this->isManualReviewRequired( $objScreeningDatabase ) && true == $this->checkManualCriminalSearchEnabledByScreeningPackageId( $intScreeningPackageId, $objScreeningDatabase ) ) ? true : false;
			if( false == $boolIsManualSearchAvailableInScreeningPackage ) {
				$strSql = 'WHERE child_transaction_id = ' . ( int ) $this->getId() . 'AND is_active = true AND lower(search_type) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $this->getSearchType() ) ) . '\'';
				return ( 0 < CScreeningApplicantChildTransactions::fetchScreeningApplicantChildTransactionCount( $strSql, $objScreeningDatabase ) ) ? true : false;
			} else {
				return true;
			}
		}
	}

	public function checkManualCriminalSearchEnabledByScreeningPackageId( $intScreeningPackageId, $objScreeningDatabase ) {
		$strSql = 'WHERE screen_type_id = ' . ( int ) $this->getScreenTypeId() . ' AND screening_package_id = ' . ( int ) $intScreeningPackageId;
		return ( 0 < CScreeningPackageScreenTypeAssociations::fetchScreeningPackageScreenTypeAssociationCount( $strSql, $objScreeningDatabase ) ) ? true : false;
	}

	public function isManualCriminalSearchExist( $intScreeningPackageId, $objDatabase ) {
		// This is additional check
		if( false == in_array( $this->getScreenTypeId(), CScreenType::$c_arrintManualCriminalSearchScreenTypeIds ) ) return false;

		$arrobjScreeningPackageScreenTypeCriterias = CScreeningPackageScreenTypeCriterias::fetchScreeningPackageScreenTypeCriteriasByScreenTypeIdByScreeningPackageIdByScreeningSearchCriteriaTypeIds( $this->getScreenTypeId(), $intScreeningPackageId, $objDatabase );
		$objManuallyAddedCriminalSearchScreeningTransaction = CScreeningApplicantChildTransactions::fetchActiveScreeningApplicantChildTransactionByScreeningIdByScreeningApplicantIdByChildTransactionId( $this->getScreeningId(), $this->getScreeningApplicantId(), $this->getId(), $objDatabase );

		$boolStatus = false;
		if( true == valArr( $arrobjScreeningPackageScreenTypeCriterias ) ) {
			foreach( $arrobjScreeningPackageScreenTypeCriterias as $objScreeningPackageScreenTypeCriteria ) {
				$arrmixAllScreeningSearchCriterias = json_decode( $objScreeningPackageScreenTypeCriteria->getCriteria(), true );

				if( false == valArr( $arrmixAllScreeningSearchCriterias ) ) {
					$boolStatus = false;
					break;
				}

				if( true == in_array( $this->getSearchType(), $arrmixAllScreeningSearchCriterias ) ) {
					$boolStatus = true;
					break;
				}
			}
		}

		if( \Psi\CStringService::singleton()->strtolower( 'cocourt~' . $this->getSearchType() ) == \Psi\CStringService::singleton()->strtolower( $this->getScreeningOrderId() ) ) {
			$boolStatus = true;
		}

		if( false == $boolStatus && true == valObj( $objManuallyAddedCriminalSearchScreeningTransaction, 'CScreeningApplicantChildTransaction' ) && \Psi\CStringService::singleton()->strtolower( $this->getSearchType() ) == \Psi\CStringService::singleton()->strtolower( $objManuallyAddedCriminalSearchScreeningTransaction->getSearchType() ) ) {
			$boolStatus = true;
		}

		return $boolStatus;
	}

	public function isForceResubmit() {
		if( false == in_array( $this->getScreeningDataProviderTypeId(), CScreeningDataProviderType::$c_arrintExperianDataProviderTypeIds ) || CScreenType::CREDIT != $this->getScreenTypeId() || false == in_array( $this->getScreeningStatusTypeId(), array( CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW, CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED ) ) ) return false;

		return true;
	}

	public function completeChildTransactions( $arrstrApplicantSecureClientResponse, $intCurrentUserId, $objScreeningDatabase ) {

		if( false == valArr( $this->getChildTransactions() ) ) return;

		$boolIsValid = true;

		foreach( $this->getChildTransactions() as $objScreeningTransaction ) {
			$intScreeningStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED;

            if( CScreeningRecommendationType::PENDING_UNKNOWN != $objScreeningTransaction->getScreeningRecommendationTypeId() ) {
                $objScreeningTransaction->setScreeningRecommendationUpdatedOn( 'NOW()' );
            }

            if( false == $objScreeningTransaction->getIsEvaluationRequired() ) {
				$objScreeningTransaction->setScreeningRecommendationTypeId( CScreeningRecommendationType::PASS );
				$objScreeningTransaction->setScreeningPackageConditionSetId( NULL );
			} else {
				$objScreeningTransaction->setScreeningRecommendationTypeId( $arrstrApplicantSecureClientResponse[$objScreeningTransaction->getId()]['screening_recommendation_type_id'] );
				$objScreeningTransaction->setScreeningPackageConditionSetId( $arrstrApplicantSecureClientResponse[$objScreeningTransaction->getId()]['screening_package_condition_set_id'] );
			}

			$objScreeningTransaction->setScreeningStatusTypeId( $intScreeningStatusTypeId );
			$objScreeningTransaction->setScreeningResponseCompletedOn( 'NOW()' );
			$objScreeningTransaction->setScreeningCompletedOn( 'NOW()' );
			$objScreeningTransaction->setScreeningOrderId( ( self::PROCESSING_STATUS_SKIPPED == $objScreeningTransaction->getScreeningOrderId() ) ? self::PROCESSING_STATUS_SKIPPED : getArrayElementByKey( $objScreeningTransaction->getScreenTypeId(), CScreenType::$c_arrintRVScreenTypesOrderIdsAssociations ) );

			if( false == $objScreeningTransaction->update( $intCurrentUserId, $objScreeningDatabase ) ) {
				trigger_error( 'Failed to update child transaction status for screening id : ' . $this->getScreeningId() . ' And cid : ' . $this->getCid(), E_USER_WARNING );
				$boolIsValid = false;
				break;
			}
		}

		return $boolIsValid;
	}

	public function addChildTransactionRequest( $intCurrentUserId, $intScreeningPackageId, $arrmixCriminalSearchRecommendations, $intScreeningChildTransactionRequestTypeId, $objDatabase ) {
		if( false == valArr( $arrmixCriminalSearchRecommendations ) ) {
			CScreeningApplicantChildTransactions::archieveScreeningApplicantChildTransactionResult( $intCurrentUserId, $this->getId(), $this->getScreeningId(), $this->getScreeningApplicantId(), $objDatabase );
			return false;
		}

		$arrintAllChildTransactionIds = [];

		$arrobjExistingChildTransactions = CScreeningApplicantChildTransactions::fetchAllScreeningApplicantChildTransactionsByScreeningIdByScreeningApplicantIdByScreeningTransactionId( $this->getScreeningId(), $this->getScreeningApplicantId(), $this->getId(), $objDatabase );
		$arrintAllChildTransactionIds    = ( true == valArr( $arrobjExistingChildTransactions ) ) ? array_keys( $arrobjExistingChildTransactions ) : [];
		$arrobjExistingChildTransactions = ( true == valArr( $arrobjExistingChildTransactions ) ) ? array_change_key_case( rekeyObjects( 'SearchType', $arrobjExistingChildTransactions ), CASE_LOWER ) : [];

		$arrobjScreeningDataProviders = CScreeningPackageScreenTypeAssociations::fetchScreeningPackageScreenTypeAssociationsByScreenTypeIdsByScreeningPackageId( array_keys( $arrmixCriminalSearchRecommendations ), $intScreeningPackageId, $objDatabase );
		$arrobjScreeningDataProviders = rekeyObjects( 'ScreenTypeId', $arrobjScreeningDataProviders );

		$objNewScreeningChildTransaction = new CScreeningApplicantChildTransaction();
		$objNewScreeningChildTransaction->setScreeningId( $this->getScreeningId() );
		$objNewScreeningChildTransaction->setScreeningApplicantId( $this->getScreeningApplicantId() );
		$objNewScreeningChildTransaction->setScreeningTransactionId( $this->getId() );
		$objNewScreeningChildTransaction->setScreeningChildTransactionRequestTypeId( $intScreeningChildTransactionRequestTypeId );
		$objNewScreeningChildTransaction->setIsActive( true );

		if( 'production' != CONFIG_ENVIRONMENT ) {
			$arrobjTestDataProviders = CScreeningDataProviders::fetchPublishedScreeningDataProvidersByScreeningDataProviderProductTypeIdByTestTypeId( $objDatabase, CScreeningDataProviderProductType::RESIDENT_VERIFY, 1 );
		}

		$arrintExistingChildTransactionIds = []; // this variable hold the child transaction ids which we have to keep( which were already exists and recommended again ).

		foreach( $arrmixCriminalSearchRecommendations as $intChildScreenTypeId => $arrmixCriminalSearchRecommendation ) {
			foreach( $arrmixCriminalSearchRecommendation as $strSearchTypeName => $arrmixSearchDetails ) {
				$objExistingChildTransaction = getArrayElementByKey( \Psi\CStringService::singleton()->strtolower( $strSearchTypeName ), $arrobjExistingChildTransactions );

				if( false == valObj( $objExistingChildTransaction, 'CScreeningApplicantChildTransaction' ) ) {
					$objScreeningChildTransaction = clone $objNewScreeningChildTransaction;
					$objScreeningChildTransaction->setId( NULL );
				} else {
					$arrintExistingChildTransactionIds[] = $objExistingChildTransaction->getId();
					$objScreeningChildTransaction  = $objExistingChildTransaction;
				}

				$objScreeningDataProvider = getArrayElementByKey( $intChildScreenTypeId, $arrobjScreeningDataProviders );

				// Override the data provider id to test in case of screening performed in lower environment with live data provider_ids
				$intDataProviderId = $objScreeningDataProvider->getScreeningDataProviderId();
				if( 'production' != CONFIG_ENVIRONMENT && false == array_key_exists( $intDataProviderId, $arrobjTestDataProviders ) ) {
					$intDataProviderId = CScreeningUtils::getRVTestCaseDataProviderByScreenTypeId( $arrobjTestDataProviders, $intChildScreenTypeId );
				}

				$objScreeningChildTransaction->setSearchType( $strSearchTypeName );
				$objScreeningChildTransaction->setScreenTypeId( $intChildScreenTypeId );
				$objScreeningChildTransaction->setSearchReasonNote( $arrmixSearchDetails['search_reason'] );
				$objScreeningChildTransaction->setScreeningDataProviderId( $intDataProviderId );
				$objScreeningChildTransaction->setReferenceRecordIds( ( true == valArr( $arrmixSearchDetails ) && true == array_key_exists( 'reference_record_ids', $arrmixSearchDetails ) ) ? $arrmixSearchDetails['reference_record_ids']: NULL );

				if( false == $objScreeningChildTransaction->insertOrUpdate( $intCurrentUserId, $objDatabase ) ) {
					trigger_error( 'Failed to create child transaction requests for search type : ' . $strSearchTypeName . ' screening transaction id ' . $this->getId() . ' And screening id ' . $this->$this->getScreeningId(), E_USER_WARNING );
					return false;
				}
			}
		}

		$arrintChildTransactionDiffIds = array_diff( $arrintAllChildTransactionIds, $arrintExistingChildTransactionIds );

		// Archive old child transaction data
		if( true == valArr( $arrintChildTransactionDiffIds ) ) {
			CScreeningApplicantChildTransactions::archieveScreeningApplicantChildTransactionResult( $intCurrentUserId, $this->getId(), $this->getScreeningId(), $this->getScreeningApplicantId(), $objDatabase, $arrintChildTransactionDiffIds );
		}

		return $this;
	}

	public function setParentScreeningTransaction( $objScreeningApplicant, $objDatabase ) {
		$arrmixChildTransactionDetails = CScreeningApplicantChildTransactions::fetchCustomActiveScreeningApplicantChildTransactionDetailsByChildTransactionIdByScreeningIdByScreeningApplicantId( $this->getId(), $this->getScreeningId(), $this->getScreeningApplicantId(), $objDatabase );
		$arrmixChildTransactionDetails = getArrayElementByKey( 'data', $arrmixChildTransactionDetails );

		$strReferenceRecordIds = ( true == valArr( $arrmixChildTransactionDetails ) ) ? getArrayElementByKey( 'reference_record_ids', current( $arrmixChildTransactionDetails ) ) : [];
		$intParentTransactionId = ( true == valArr( $arrmixChildTransactionDetails ) ) ? getArrayElementByKey( 'parent_transaction_id', current( $arrmixChildTransactionDetails ) ) : NULL;

		// If no data in child transactions table then do nothing..just return

		if( false == valArr( $arrmixChildTransactionDetails ) || false == valStr( $strReferenceRecordIds ) ) return $this;

		$arrobjRekeyedScreeningTransactions = rekeyObjects( 'id', $objScreeningApplicant->getScreeningTransactions() );
		$objParentTransaction = getArrayElementByKey( $intParentTransactionId, $arrobjRekeyedScreeningTransactions );

		if( false == valObj( $objParentTransaction, 'CScreeningTransaction' ) ) return $this;

		// SetParent transaction functionality is only applicable to when reported type of search and not for previous address..

		if( CScreenType::CREDIT == $objParentTransaction->getScreenTypeId() ) return $this;

		$this->setParentTransaction( $objParentTransaction );
		$this->setParentReferenceRecordIds( CStrings::strToArrIntDef( $strReferenceRecordIds, $arrintDefault = NULL ) );

		return $this;
	}

	public function processScreeningTransaction( $intCurrentUserId, $objScreeningApplicant ) {
		$objScreeningProcessor = CResidentVerifyDataProviderFactory::createScreeningDataProvider( $this->getScreenTypeId(), $intCurrentUserId, $objScreeningApplicant, $this, $this->getDatabase() );
		$this->setScreeningResponseCompletedOn( 'NOW()' );
		return $objScreeningProcessor->process();
	}

	public function checkInHouseReviewEnabled( $objDatabase ) {
		if( false == in_array( $this->getScreenTypeId(), CScreenType::$c_arrintInHouseReviewEnabledScreenTypeIds ) ) return false;

		$objScreeningDataProvider = CScreeningDataProviders::fetchScreeningDataProviderById( $this->getScreeningDataProviderId(), $objDatabase );

		if( false == valObj( $objScreeningDataProvider, 'CScreeningDataProvider' ) ) return false;

		$arrmixScreeningVendorDetails = CResidentVerifyServiceUtil::parseDataProviderDetails( $objScreeningDataProvider->getVendorCode() );

		if( false == valArr( $arrmixScreeningVendorDetails ) ) return false;

		return ( true == getArrayElementByKey( CScreeningDataProvider::CRIMINAL_REVIEW_PRODUCT_CODE, $arrmixScreeningVendorDetails ) ) ? true : false;
	}

	public function checkIsTransactionReviewCompleted( $objDatabase ) {
		$strWhere = 'WHERE
						transaction_review_status_id = ' . ( int ) CTransactionReviewStatus::COMPLETE . '
						AND screening_transaction_id = ' . ( int ) $this->getId() . '
						AND cid = ' . ( int ) $this->getCid();

		$intTransactionReviewCount = CTransactionReviews::fetchTransactionReviewCount( $strWhere, $objDatabase );

		return ( 0 < $intTransactionReviewCount ) ? true : false;
	}

	public function isManuallySearchAdded( $objDatabase ) {
		$strWhere = 'WHERE screening_child_transaction_request_type_id = ' . ( int ) CScreeningChildTransactionRequestType::MANUAL . ' AND screening_id = ' . ( int ) $this->getScreeningId() . ' AND screening_applicant_id = ' . ( int ) $this->getScreeningApplicantId() . ' AND
		child_transaction_id = ' . ( int ) $this->getId();

		$intRecordsCcount = CScreeningApplicantChildTransactions::fetchScreeningApplicantChildTransactionCount( $strWhere, $objDatabase );

		return ( 0 < $intRecordsCcount ) ? true : false;
	}

	public function createScreeningTransactionConditions( $arrmixScreeningConditionDetails, $intCurrentUserId, $objDatabase ) {
		if( false == valArr( $arrmixScreeningConditionDetails ) ) {
			trigger_error( 'Failed to Offer condition sets for screening_transaction_id : ' . $this->getId() . ' Cid : ' . $this->getCid(), E_WARNING );
			return false;
		}

		$arrobjExistingScreeningTransactionConditions = CScreeningTransactionConditions::fetchActiveScreeningTransactionConditionsByScreeningTransactionId( $this->getId(), $objDatabase );

		$arrintExistingScreeningConditionData = extractObjectKeyValuePairs( $arrobjExistingScreeningTransactionConditions, 'screeningConfigPreferenceTypeId', 'screeningConditionSetId' );
		$objScreeningTransactionCondition = new CScreeningTransactionCondition();
		$objScreeningTransactionCondition->setScreeningTransactionId( $this->getId() );

		foreach( $arrmixScreeningConditionDetails as $intScreeningConfigPreferenceTypeId => $intScreeningConditionSetId ) {

			if( true == valArr( $arrintExistingScreeningConditionData ) && true == array_key_exists( $intScreeningConfigPreferenceTypeId, $arrintExistingScreeningConditionData ) && $intScreeningConditionSetId == getArrayElementByKey( $intScreeningConfigPreferenceTypeId, $arrintExistingScreeningConditionData ) ) {
					unset( $arrintExistingScreeningConditionData[$intScreeningConfigPreferenceTypeId] );
			} else {
				$objClonedScreeningTransactionCondition = clone $objScreeningTransactionCondition;
				$objClonedScreeningTransactionCondition->setScreeningConditionSetId( $intScreeningConditionSetId );
				$objClonedScreeningTransactionCondition->setScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId );
				$arrobjScreeningTransactionConditions[] = $objClonedScreeningTransactionCondition;
			}
		}

		// archiving Screening Transaction Conditions records
		if( true == valArr( $arrintExistingScreeningConditionData ) ) {
			CScreeningTransactionConditions::archieveScreeningTransactionConditions( $intCurrentUserId, array_flip( $arrintExistingScreeningConditionData ), $this->getId(), $objDatabase );
		}

		return ( true == valArr( $arrobjScreeningTransactionConditions ) ) ? CScreeningTransactionConditions::bulkInsert( $arrobjScreeningTransactionConditions, $intCurrentUserId, $objDatabase ) : true;
	}

	/**
	 * @param $arrobjScreeningCompanyPreferences
	 * @return bool
	 * This function is used to check the recommendation of Bankruptcy and Rental Collections and also the
	 * company preferences and returns the boolean
	 */
	public function checkOverrideMustPassCreditIncomeSetting( $arrobjScreeningPropertyPreferences ) {
		$boolOverrideMustPassCreditAndIncome = false;
		$arrmixAdditionalDetails = $this->getAdditionalDetails();

		foreach( $arrobjScreeningPropertyPreferences as $arrobjScreeningPropertyPreference ) {
			$objStdSettings = $arrobjScreeningPropertyPreference->getValue();
			if( true == $objStdSettings->{CScreeningCompanyPreferenceType::MUST_PASS_CREDIT_FACTORS_RENTAL_COLLECTIONS} ) {
				if( true == valObj( $arrmixAdditionalDetails->{CScreeningJsonDatatype::BANKRUPTCY_AND_RENTAL_COLLECTIONS}, 'stdClass' ) && true == valId( $arrmixAdditionalDetails->{CScreeningJsonDatatype::BANKRUPTCY_AND_RENTAL_COLLECTIONS}->{CScreeningConfigPreferenceType::RENTAL_COLLECTIONS} ) ) {
					$boolOverrideMustPassCreditAndIncome = true;
				}
			} else if( true == $objStdSettings->{CScreeningCompanyPreferenceType::MUST_PASS_CREDIT_FACTORS_BANKRUPTCIES} ) {
				if( true == valObj( $arrmixAdditionalDetails->{CScreeningJsonDatatype::BANKRUPTCY_AND_RENTAL_COLLECTIONS}, 'stdClass' ) && true == valId( $arrmixAdditionalDetails->{CScreeningJsonDatatype::BANKRUPTCY_AND_RENTAL_COLLECTIONS}->{CScreeningConfigPreferenceType::BANKRUPTCY_RECENT_MONTHS} ) ) {
					$boolOverrideMustPassCreditAndIncome = true;
				}
			}
		}

		return $boolOverrideMustPassCreditAndIncome;
	}

	public function createScreeningTransactionErrorLogs( $arrmixScreeningErrorDetails, $intCurrentUserId, $objDatabase ) {

	    if( true == in_array( $this->getScreenTypeId(), CScreenType::$c_arrintPreciseIdScreenTypeIds ) ) {
            $arrmixScreeningTransactions = CScreeningTransactions::fetchScreeningParentScreenTypeTransactionIdByIdByScreeningIdByScreeningApplicantIdByScreenTypeIdByCid( $this->getScreeningId(), $this->getScreeningApplicantId(), CScreenType::PRECISE_ID, $this->getCid(), $objDatabase );
            $intScreeningTransactionId = ( true == valArr( $arrmixScreeningTransactions ) ) ?  $arrmixScreeningTransactions['id'] : $this->getId();
        } else {
            $intScreeningTransactionId = $this->getId();
        }

		$strWhere = ' WHERE screening_transaction_id = ' . ( int ) $intScreeningTransactionId . ' AND cid = ' . ( int ) $this->getCid() . ' AND screening_error_type_id = ' . $arrmixScreeningErrorDetails['error_screening_error_type_id'] . ' AND error_code LIKE \'' . $arrmixScreeningErrorDetails['error_code'] . '\' AND lower( description ) LIKE \'' . addslashes( strtolower( $arrmixScreeningErrorDetails['error_description'] ) ) . '\' AND is_active = true';
		$intCount = CScreeningTransactionErrorLogs::fetchScreeningTransactionErrorLogCount( $strWhere, $objDatabase );

		if( $intCount == 0 ) {
			// Archive existing screening transaction error log
			CScreeningTransactionErrorLogs::archiveScreeningTransactionErrorLogs( $intScreeningTransactionId, $this->getCid(), $intCurrentUserId, $objDatabase );

			// Adding new  screening transaction error log
			$objScreeningTransactionErrorLog = new CScreeningTransactionErrorLog();

			$objScreeningTransactionErrorLog->setCid( $this->getCid() );
			$objScreeningTransactionErrorLog->setScreeningTransactionId( $intScreeningTransactionId );
			$objScreeningTransactionErrorLog->setScreeningDataProviderId( $this->getScreeningDataProviderId() );
			$objScreeningTransactionErrorLog->setScreeningErrorTypeId( $arrmixScreeningErrorDetails['error_screening_error_type_id'] );
			$objScreeningTransactionErrorLog->setErrorCode( $arrmixScreeningErrorDetails['error_code'] );
			$objScreeningTransactionErrorLog->setDescription( $arrmixScreeningErrorDetails['error_description'] );

			if( false == $objScreeningTransactionErrorLog->insert( $intCurrentUserId, $objDatabase ) ) {
				trigger_error( 'Failed to log screening transaction error for screening_transaction id : ' . ( int ) $intScreeningTransactionId . ' cid : ' . ( int ) $this->getCid(), E_USER_WARNING );
				return false;
			}
		}

		return true;
	}

    public function verifyScreeningResult( $intCurrentUserId, $objApplicant, $objScreeningDatabase ) {
        // load criteria id
        $intScreeningCriteriaId = CScreeningPackageScreenTypeAssociations::fetchActiveScreeningCriteriaIdByScreenTypeIdByScreeningPackageId( $this->getScreenTypeId(), $objApplicant->getScreeningPackageId(), $objScreeningDatabase );

        // Code hack to load criminal criteria id for state/county transactions when they don't have record in screening_package_screen_type_associations table
        // added through dispatch screen in client admin.
        if( true == in_array( $this->getScreenTypeId(), CScreenType::$c_arrintManualCriminalSearchScreenTypeIds ) && false == valId( $intScreeningCriteriaId ) ) {
            $intScreeningCriteriaId = CScreeningPackageScreenTypeAssociations::fetchActiveScreeningCriteriaIdByScreenTypeIdByScreeningPackageId( CScreenType::CRIMINAL, $objApplicant->getScreeningPackageId(), $objScreeningDatabase );
        }

        $arrobjScreeningCriteriaSettings = CScreeningCriteriaSettings::fetchScreeningCriteriaSettingsByScreeningCriteriaId( $intScreeningCriteriaId, $objScreeningDatabase );

        $objStdRequest = CResidentVerifySecureRequestBuilder::verifyScreeningRecommendations( $intCurrentUserId, $this, $arrobjScreeningCriteriaSettings );

        $objSecureClientLibrary = new CResidentVerifySecureClientLibrary();
        $arrmixResult = $objSecureClientLibrary->verifyScreeningRecommendations( $objStdRequest );

        return $arrmixResult;
    }

    public function loadTransactionEvaluationSettings( $intPackageId, $objDatabase ) {
        $intScreeningCriteriaId = NULL;
        $arrobjScreeningConditionSets = NULL;

	    if( true == in_array( $this->getScreenTypeId(), CScreenType::$c_arrintPreciseIdScreenTypeIds ) ) {
            $objScreeningCriteria = CScreeningCriterias::fetchScreeningCriteriaByScreenTypeIdByCid( $this->getScreenTypeId(), $this->getCid(), $objDatabase );
            $intScreeningCriteriaId = ( true == valObj( $objScreeningCriteria, 'CScreeningCriteria' ) ) ? $objScreeningCriteria->getId() : NULL;
        } else {
            // Fetch associated screening_criteria_id with screen type
            $intScreeningCriteriaId = CScreeningPackageScreenTypeAssociations::fetchActiveScreeningCriteriaIdByScreenTypeIdByScreeningPackageId( $this->getScreenTypeId(), $intPackageId, $objDatabase );

            // Code hack to load criminal criteria id for state/county transactions when they don't have record in screening_package_screen_type_associations table
            // added through dispatch screen in client admin.
            if( true == in_array( $this->getScreenTypeId(), CScreenType::$c_arrintManualCriminalSearchScreenTypeIds ) && false == valId( $intScreeningCriteriaId ) ) {
                $intScreeningCriteriaId = CScreeningPackageScreenTypeAssociations::fetchActiveScreeningCriteriaIdByScreenTypeIdByScreeningPackageId( CScreenType::CRIMINAL, $intPackageId, $objDatabase );
            }

            $arrobjScreeningConditionSets = CScreeningConditionSets::fetchAllActiveScreeningCriteriaConditionSetByScreeningCriteriaIdByCid( $intScreeningCriteriaId, $this->getCid(), $objDatabase );
	    }

        if( true == in_array( $this->getScreenTypeId(), CScreenType::$c_arrintPreciseIdScreenTypeIds ) ) {
            $arrobjScreeningConditionSets = CScreeningConditionSets::fetchScreeningConditionSetsByScreeningRecommendationTypeIdsByCid( [ CScreeningRecommendationType::FAIL, CScreeningRecommendationType::PASS ], $this->getCid(), $objDatabase );
        } else {
            $arrobjScreeningConditionSets = CScreeningConditionSets::fetchAllActiveScreeningCriteriaConditionSetByScreeningCriteriaIdByCid( $intScreeningCriteriaId, $this->getCid(), $objDatabase );
        }

        // Fetch Filters
        $arrobjScreeningCriteriaConfigPreferenceFilters = CScreeningCriteriaConfigPreferenceTypeFilters::fetchActiveScreeningCriteriaConfigPreferenceTypeFiltersByScreeningCriteriaIdByScreenTypeId( $intScreeningCriteriaId, $this->getScreenTypeid(), $objDatabase );

        // Fetch Screening Condition Sets

        $arrobjScreeningCriteriaSettings = CScreeningCriteriaSettings::fetchScreeningCriteriaSettingsByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase );
        $arrobjCriminalCriteriaSettings = $arrobjClientCriminalClassificationSubtypeKeywords = [];

        if( true == in_array( $this->getScreenTypeId(), CScreenType::$c_arrintCriminalScreenTypeIds ) ) {
            $arrobjCriminalCriteriaSettings = CCriminalCriteriaSettings::fetchActiveCriminalCriteriaSettingsForScreeningByScreeningCriteriaIdByCid( $intScreeningCriteriaId, $this->getCid(), $objDatabase );
            $arrobjClientCriminalClassificationSubtypeKeywords = CClientCriminalClassificationSubtypeKeywords::fetchAllPublishedClientCriminalClassificationSubtypeKeywordsByScreeningCriteriaIdByCid( $intScreeningCriteriaId, $this->getCid(), $objDatabase );
        }

        return array( $arrobjScreeningCriteriaConfigPreferenceFilters, $arrobjScreeningConditionSets, $arrobjScreeningCriteriaSettings, $arrobjCriminalCriteriaSettings, $arrobjClientCriminalClassificationSubtypeKeywords );
    }

    public function handleResponse( $intCurrentUserId, $arrmixScreeningResponse, $objDatabase ) {
	    $objScreeningResponseHandler = CScreeningResponseHandlerFactory::loadScreeningResponseHandler( $intCurrentUserId, $this, $arrmixScreeningResponse, $objDatabase );
        return $objScreeningResponseHandler->handleResponse();
    }

    public function getProcessor( $intCurrentUserId, $objScreeningApplicant, $objScreeningDatabase ) {
        return CResidentVerifyDataProviderFactory::createScreeningDataProvider( $this->getScreenTypeId(), $intCurrentUserId, $objScreeningApplicant, $this, $objScreeningDatabase );
    }

	public function isConcurrentRequest( $objScreeningDatabase, $intMinimumSeconds = 10 ) {

		$boolIsConcurrentRequest = false;
		if( false == in_array( $this->getScreenTypeId(), CScreenType::$c_arrintConcurrentScreenType ) || false == in_array( $this->getScreeningStatusTypeId(), [ CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED, CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED, CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN ] ) ) {
			return $boolIsConcurrentRequest;
		}

		$objScreeningApplicantTransaction = CScreeningTransactions::fetchScreeningTransactionByIdByScreeningIdByScreeningApplicantIdByCid( $this->getId(), $this->getScreeningId(), $this->getScreeningApplicantId(), $this->getCid(), $objScreeningDatabase );

		if( !valStr( $objScreeningApplicantTransaction->getScreeningRequestedOn() ) ) {
			return $boolIsConcurrentRequest;
		}

		$strScreeningRequestedOn = strtotime( $objScreeningApplicantTransaction->getScreeningRequestedOn() );
		$strCurrentTime = time();
		if( $intMinimumSeconds > ( $strCurrentTime - $strScreeningRequestedOn ) ) {
			$boolIsConcurrentRequest = true;
		}

		return $boolIsConcurrentRequest;

	}

	public function insertTransactionDetails( $arrmixTransactionDetails, $intCurrentUserId, $objScreeningDatabase ) {
		if( false == valArr( $arrmixTransactionDetails ) ) return;

		$objScreeningTransactionDetail = new CScreeningTransactionDetail();
		$objScreeningTransactionDetail->setCid( $this->getCid() );
		$objScreeningTransactionDetail->setScreeningId( $this->getScreeningId() );
		$objScreeningTransactionDetail->setScreeningTransactionId( $this->getId() );

		$arrmixResult = mergeIntersectArray( CScreeningTransactionDetail::$c_arrstrScreeningTransactionDetails, $arrmixTransactionDetails );

		$objScreeningTransactionDetail->setValues( $arrmixResult, true, true );

		if( false == $objScreeningTransactionDetail->insert( $intCurrentUserId, $objScreeningDatabase ) ) {
			trigger_error( 'Failed to insert screening transaction summary details for screening_transaction_id[ ' . $this->getId() . ' ] .', E_USER_WARNING );
		}

		return;
	}

}
?>
