<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDelinquentResidents
 * Do not add any new functions to this class.
 */

class CDelinquentResidents extends CBaseDelinquentResidents {

	public static function buildWhereClauseByDnrSearchFilter( $objDnrSearchFilter ) {

		$arrstrWheres = [];
		$strWhere     = '';

		if( false == is_null( $objDnrSearchFilter->getCid() ) ) {
			$arrstrWheres[] = 'dr.cid = ' . ( int ) $objDnrSearchFilter->getCid();
		}

		if( false == is_null( $objDnrSearchFilter->getPropertyId() ) ) {
			$arrstrWheres[] = 'dr.property_id = ' . ( int ) $objDnrSearchFilter->getPropertyId();
		}

		if( false == is_null( $objDnrSearchFilter->getLeaseId() ) && true == valStr( trim( $objDnrSearchFilter->getLeaseId() ) ) ) {
			$arrstrWheres[] = 'dre.lease_id = ' . ( int ) $objDnrSearchFilter->getLeaseId();
		}

		if( false == is_null( $objDnrSearchFilter->getTaxNumberLastFour() ) && true == valStr( trim( $objDnrSearchFilter->getTaxNumberLastFour() ) ) ) {
			$arrstrWheres[] = '( lower( dr.tax_number_last_four ) LIKE lower( \'' . addslashes( $objDnrSearchFilter->getTaxNumberLastFour() ) . '\' ) )';
		}

		if( false == is_null( $objDnrSearchFilter->getApplicantName() ) && true == valStr( trim( $objDnrSearchFilter->getApplicantName() ) ) ) {
			$strApplicantFilterType = \Psi\CStringService::singleton()->strtolower( \Psi\CStringService::singleton()->substr( trim( $objDnrSearchFilter->getApplicantName() ), 0, 2 ) );

			if( '' != $strApplicantFilterType && ( true == in_array( $strApplicantFilterType, [ 'n:', 'f:', 'l:' ] ) ) ) {
				$strApplicantFilterValue = trim( \Psi\CStringService::singleton()->substr( trim( $objDnrSearchFilter->getApplicantName() ), 2 ) );
				switch( $strApplicantFilterType ) {
					case 'n:':
						$arrstrApplicantName = explode( ' ', $strApplicantFilterValue );
						$strFirstName        = implode( ' ', array_slice( $arrstrApplicantName, 0, 1 ) );// first array  item
						$strLastName         = implode( ' ', array_slice( $arrstrApplicantName, 1 ) );// Alla array item except first
						$strReverseFirstName = implode( ' ', array_slice( $arrstrApplicantName, 0, -1 ) );// All array item Except last
						$strReverseLastName  = implode( ' ', array_slice( $arrstrApplicantName, -1 ) );// last array item

						$arrstrWheres[] = '( ( lower( dr.name_first ) LIKE lower( \'' . addslashes( $strFirstName ) . '\' ) AND lower( dr.name_last ) LIKE lower( \'' . addslashes( $strLastName ) . '\' ) ) OR  ( lower( dr.name_first ) LIKE lower( \'' . addslashes( $strReverseFirstName ) . '\' ) AND lower( dr.name_last ) LIKE lower( \'' . addslashes( $strReverseLastName ) . '\' ) ) )';
						break;

					case 'f:':
						$arrstrWheres[] = 'lower( dr.name_first ) LIKE lower( \'' . addslashes( $strApplicantFilterValue ) . '\' )';
						break;

					case 'l:':
						$arrstrWheres[] = 'lower( dr.name_last ) LIKE lower( \'' . addslashes( $strApplicantFilterValue ) . '\' )';
						break;

					default:
						//

				}
			} else {

				$arrstrApplicantNameWheres = [];
				$arrstrApplicantNameItems  = explode( ' ', trim( $objDnrSearchFilter->getApplicantName() ) );

				if( true == valArr( $arrstrApplicantNameItems ) ) {
					foreach( $arrstrApplicantNameItems as $strApplicantNameItem ) {
						$arrstrApplicantNameWheres[] = '( lower( dr.name_first ) LIKE lower( \'%' . addslashes( $strApplicantNameItem ) . '%\' ) OR lower( dr.name_last ) LIKE lower( \'%' . addslashes( $strApplicantNameItem ) . '%\' ) OR lower( dr.name_middle ) LIKE lower( \'%' . addslashes( $strApplicantNameItem ) . '%\' ) )';
					}
					$arrstrWheres[] = ' ( ' . implode( ' OR ', $arrstrApplicantNameWheres ) . ' ) ';

				}

			}

		}

		if( false == is_null( $objDnrSearchFilter->getAddressLine() ) && true == valStr( $objDnrSearchFilter->getAddressLine() ) ) {
			$arrstrAddressLineWheres = [];

			$arrstrAddressLineItems = explode( ' ', trim( $objDnrSearchFilter->getAddressLine() ) );
			if( true == valArr( $arrstrAddressLineItems ) ) {
				foreach( $arrstrAddressLineItems as $strAddressLineItems ) {
					$arrstrAddressLineWheres[] = '( lower( dr.address_line ) LIKE lower( \'%' . addslashes( $strAddressLineItems ) . '%\' ) OR lower( dr.city ) LIKE lower( \'%' . addslashes( $strAddressLineItems ) . '%\' ) OR lower( dr.state ) LIKE lower( \'%' . addslashes( $strAddressLineItems ) . '%\' ) OR lower ( dr.zipcode ) LIKE lower( \'%' . addslashes( $strAddressLineItems ) . '%\' ) )';
				}

				$arrstrWheres[] = '( ' . implode( ' OR ', $arrstrAddressLineWheres ) . ' )';
			}
		}

		if( false == is_null( $objDnrSearchFilter->getBirthDateYear() ) && true == valStr( trim( $objDnrSearchFilter->getBirthDateYear() ) ) ) {
			$arrstrWheres[] = 'dr.birth_date_encrypted = \'' . base64_encode( $objDnrSearchFilter->getBirthDateYear() ) . '\'';
		}

		if( false == is_null( $objDnrSearchFilter->getFromDate() ) && true == valStr( trim( $objDnrSearchFilter->getFromDate() ) ) ) {
			$arrstrWheres[] = 'dr.created_on > \'' . date( 'Y-m-d 00:00:00', strtotime( $objDnrSearchFilter->getFromDate() ) ) . '\'';
		}

		if( false == is_null( $objDnrSearchFilter->getToDate() ) && true == valStr( trim( $objDnrSearchFilter->getToDate() ) ) ) {
			$arrstrWheres[] = 'dr.created_on < \'' . date( 'Y-m-d 23:59:59', strtotime( $objDnrSearchFilter->getToDate() ) ) . '\'';
		}

		if( true == valArr( $arrstrWheres ) ) {
			$strWhere = 'WHERE 
						 dr.cid = dre.cid
						 AND dr.id = dre.delinquent_resident_id
						 AND ' . implode( ' AND ', $arrstrWheres );
		}

		return $strWhere;
	}

	public static function fetchCountDelinquentResidentsRecordsByDnrSearchFilter( $objDnrSearchFilter, $objDatabase ) {

		if( false == valObj( $objDnrSearchFilter, 'CDNRSearchFilter' ) ) {
			return NULL;
		}

		$strWhere = self::buildWhereClauseByDnrSearchFilter( $objDnrSearchFilter );

		$strWhereRecords = ( CDelinquentEventStatusType::ACTIVE == $objDnrSearchFilter->getIsActive() ) ? ' WHERE dr.deleted_on IS NULL ' : ' WHERE dr.total_deleted_on_count = dr.total_record_count
                          AND dr.deleted_on IS NOT NULL ';

		$strWhereRecords .= ( false == is_null( $objDnrSearchFilter->getCid() ) ) ? ' AND dr.cid = ' . ( int ) $objDnrSearchFilter->getCid() : '';

		$strSql = 'SELECT
                             count ( dr.cid )
                       FROM
	                         (
	                            SELECT
	                               DISTINCT ON ( dr.tax_number_encrypted, dr.birth_date_encrypted, dr.name_first, dr.name_last )
	                                dr.cid,
	                                dre.deleted_on,
	                                count ( dr.id ) OVER ( PARTITION BY dr.name_first, dr.name_last, dr.birth_date_encrypted, dr.tax_number_encrypted, dre.deleted_on IS NOT NULL ) AS total_deleted_on_count,
	                                count ( dr.id ) OVER ( PARTITION BY dr.name_first, dr.name_last, dr.birth_date_encrypted, dr.tax_number_encrypted ) AS total_record_count
	                            FROM
	                                delinquent_residents dr
	                                JOIN delinquent_resident_events dre ON ( dre.cid = dr.cid AND dr.id = dre.delinquent_resident_id )
	                                ' . $strWhere . ' 
	                         ) AS dr ' . $strWhereRecords;

		$arrmixResidentDelinquentEvents = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrmixResidentDelinquentEvents ) ) ? $arrmixResidentDelinquentEvents[0]['count'] : 0;
	}

	public static function fetchDelinquentResidentsRecordsByDnrSearchFilter( $objDnrSearchFilter, $objDatabase ) {

		if( false == valObj( $objDnrSearchFilter, 'CDNRSearchFilter' ) ) {
			return NULL;
		}

		$strWhere = self::buildWhereClauseByDnrSearchFilter( $objDnrSearchFilter );

		$intOffset = -1;
		$intLimit  = -1;
		if( NULL != $objDnrSearchFilter->getPagination() ) {
			$intPageNo   = $objDnrSearchFilter->getPagination()->getPageNo();
			$intPageSize = $objDnrSearchFilter->getPagination()->getPageSize();
			$intOffset   = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit    = ( int ) $intPageSize;
		}

		$strSortBy = $objDnrSearchFilter->getSortBy();
		$strSortOn = $objDnrSearchFilter->getSortOn();

		$strField = 'dr.tax_number_encrypted';

		if( '' != $strSortOn ) {
			$strSortOn = \Psi\CStringService::singleton()->strtoupper( $strSortOn );

			switch( $strSortOn ) {
				case 'CID':
					$strField = ' dr.cid';
					break;

				case 'NAME':
					$strField = ' dr.name_first ';
					break;

				case 'TAX':
					$strField = ' dr.tax_number_last_four ';
					break;

				case 'DOB':
					$strField = ' dr.birth_date_year ';
					break;

				case 'EMAIL':
					$strField = ' dr.email_address ';
					break;

				case 'PHONE':
					$strField = ' dr.phone_number ';
					break;

				default:
					$strSortBy = '';
					break;
			}
		}

		$strOrder = ' ORDER BY ' . $strField . ' ' . $strSortBy;
		if( -1 != $intLimit && -1 != $intOffset ) {
			$strOrder .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		}

		$strWhereRecords = ( CDelinquentEventStatusType::ACTIVE == $objDnrSearchFilter->getIsActive() ) ? ' WHERE dr.deleted_on IS NULL ' : ' WHERE dr.total_deleted_on_count = dr.total_record_count
                          AND dr.deleted_on IS NOT NULL ';

		$strWhereRecords .= ( false == is_null( $objDnrSearchFilter->getCid() ) ) ? ' AND dr.cid = ' . ( int ) $objDnrSearchFilter->getCid() : '';

		$strSql = 'SELECT
                          *
                      FROM
                          (
                            SELECT
                                DISTINCT ON ( dr.tax_number_encrypted, dr.birth_date_encrypted, dr.name_first, dr.name_last )
							    dr.id,
							    dr.cid,
							    dr.property_id,
							    dr.name_first,
							    dr.name_last,
							    dr.name_middle,
							    dr.birth_date_encrypted,
							    dr.birth_date_year,
							    dr.tax_number_last_four,
							    dr.tax_number_encrypted,
							    dr.email_address,
							    dr.phone_number,
							    dre.deleted_on,
                                count ( dr.id ) OVER ( PARTITION BY dr.name_first, dr.name_last, dr.birth_date_encrypted, dr.tax_number_encrypted, dre.deleted_on IS NOT NULL ) AS total_deleted_on_count,
                                count ( dr.id ) OVER ( PARTITION BY dr.name_first, dr.name_last, dr.birth_date_encrypted, dr.tax_number_encrypted ) AS total_record_count
                            FROM
                                delinquent_residents dr
                                JOIN delinquent_resident_events dre ON ( dre.cid = dr.cid AND dr.id = dre.delinquent_resident_id )
                            ' . $strWhere . '
                          ) AS dr
                          ' . $strWhereRecords;

		$strSql .= $strOrder;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDelinquentResidentsRecordsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
                        dr.name_first,
                        dr.name_last,
                        dr.name_middle,
                        dr.property_id,
                        dre.lease_id,
                        dr.created_on,
                        dre.delinquent_event_type_id,
                        dre.delinquent_amount,
                        dre.deleted_on,
                        dre.deleted_reason_id,
                        dre.notes,
                        dre.deleted_notes
					FROM
                        delinquent_residents dr
                        JOIN delinquent_resident_events dre ON ( dre.delinquent_resident_id = dr.id AND dre.cid = dr.cid )
					WHERE
						dr.cid = ' . ( int ) $intCid . '
						AND CASE
                               WHEN deleted_on IS NULL THEN dre.created_on < NOW()
                               ELSE dre.created_on > ( NOW() - INTERVAL \'365 days\' )
                            END
                        AND deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLatestDelinquentResidentByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
		                *
		          FROM
					    delinquent_residents
				  WHERE
				        CID = ' . ( int ) $intCid . '
					    AND customer_id = ' . ( int ) $intCustomerId . '
				ORDER BY id DESC
				LIMIT 1';

		return parent::fetchObject( $strSql, 'CDelinquentResident', $objDatabase );
	}

	public static function fetchActiveDelinquentResidentsBySearchCriteria( $arrstrSearchCriteia, $objDatabase ) {

		if( false == valArr( $arrstrSearchCriteia ) ) {
			return NULL;
		}

		$strWhereCondition = '';

		if( true == valArr( $arrstrSearchCriteia ) ) {
			foreach( $arrstrSearchCriteia as $strFieldName => $strSearchValue ) {

				$strWhereCondition .= ' AND ' . $strFieldName . ' = ' . ( ( true == is_numeric( $strSearchValue ) && 'dr.cid' == $strFieldName ) ? $strSearchValue : '\'' . pg_escape_string( $strSearchValue ) . '\'' );
			}
		}

		$strSql = 'SELECT
						dre.*,
						dr.tax_number_encrypted,
						dr.tax_number_last_four
					FROM
						delinquent_residents dr
						JOIN delinquent_resident_events dre ON ( dr.cid = dre.cid AND dr.id = dre.delinquent_resident_id )
					WHERE
						dre.deleted_on IS NULL 
					AND dre.is_dnr_record = TRUE
					AND CASE WHEN dre.dnr_effective_on IS NOT NULL THEN
								dre.dnr_effective_on <= NOW()
							ELSE 
								TRUE
						END';

		if( true == $strWhereCondition ) {
			$strSql .= $strWhereCondition;
		}

		return fetchData( $strSql, $objDatabase );

	}

}

?>