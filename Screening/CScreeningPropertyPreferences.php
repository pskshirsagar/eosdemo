<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPropertyPreferences
 * Do not add any new functions to this class.
 */

class CScreeningPropertyPreferences extends CBaseScreeningPropertyPreferences {

	public static function fetchActiveScreeningPropertyPreferencesByClientIdByScreeningPropertyPreferenceTypeIdByPropertyId( $intClientId, $arrintScreeningCompanyPreferenceTypeIds, $intPropertyId, $objDatabase ) {
		if( false == valId( $intClientId ) || ( false == valArr( $arrintScreeningCompanyPreferenceTypeIds ) ) || ( false == valId( $intPropertyId ) ) ) return;

		$strSql = 'SELECT
						*
					FROM
						screening_property_preferences
					WHERE
						cid = ' . ( int ) $intClientId . '
						AND screening_company_preference_type_id IN ( ' . sqlIntImplode( $arrintScreeningCompanyPreferenceTypeIds ) . ' )
						AND property_id = ' . ( int ) $intPropertyId . '
						AND is_active IS TRUE';

		return parent::fetchScreeningPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningPropertyPreferenceByClientIdByScreeningPropertyPreferenceTypeIdByPropertyId( $intClientId, $intScreeningCompanyPreferenceTypeId, $intPropertyId, $objDatabase ) {
		if( false == valId( $intClientId ) || ( false == valId( $intScreeningCompanyPreferenceTypeId ) ) || ( false == valId( $intPropertyId ) ) ) return;

		$strSql = 'SELECT
						*
					FROM
						screening_property_preferences
					WHERE
						cid = ' . ( int ) $intClientId . '
						AND screening_company_preference_type_id = ' . ( int ) $intScreeningCompanyPreferenceTypeId . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND is_active IS TRUE';

		return parent::fetchScreeningPropertyPreference( $strSql, $objDatabase );
	}

}
?>