<?php

class CScreeningTransactionErrorLog extends CBaseScreeningTransactionErrorLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningDataProviderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningErrorTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valErrorCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>