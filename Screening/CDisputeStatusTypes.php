<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDisputeStatusTypes
 * Do not add any new functions to this class.
 */

class CDisputeStatusTypes extends CBaseDisputeStatusTypes {

	public static function fetchDisputeStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDisputeStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDisputeStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDisputeStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedDisputeStatusTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						dispute_status_types
					WHERE
						is_published = true
					ORDER BY id';

		return self::fetchDisputeStatusTypes( $strSql, $objDatabase );
	}

	public static function fetchMyDisputeStatusTypesCount( $intUserId, $intDisputeSubMenuTypeId, $objDatabase ) {

		if( CDisputeSubMenuType::MY_RECENT_DISPUTE == $intDisputeSubMenuTypeId ) {
			$strCondition = ' dl.updated_by=' . ( int ) $intUserId;
		} else {
			$strCondition = ' cd.assigned_to = ' . ( int ) $intUserId;
		}

		$strSql = ' SELECT
                    	dst.id,
  						dst.name,
                        COUNT( DISTINCT dl.consumer_id ) AS total_records

                   	FROM
                        dispute_status_types dst
                        LEFT JOIN consumer_disputes cd ON dst.id = cd.dispute_status_type_id
						LEFT JOIN consumers c ON c.id=cd.consumer_id
                       	LEFT JOIN dispute_logs dl ON dl.consumer_id = cd.consumer_id AND ' . $strCondition . '
				    WHERE
                       	dst.id NOT IN(' . implode( ', ', CDisputeStatusType::$c_arrintDisputeStatusTypeIds ) . ')
        			GROUP BY
                         dst.id
                    ORDER BY
                         dst.order_number';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDisputeDashboardStatusTypesCount( $intDisputeSubMenuTypeId, $intUserId, $objDatabase ) {

		$strColumnName = ' cd.dispute_status_type_id ';
		$strJoin = '';
		if( CDisputeSubMenuType::REQUEST_INFO == $intDisputeSubMenuTypeId ) {
			$strCondition = ' AND cd.dispute_type_id =' . CDisputeType::OTHER;
		} else {
			$strCondition = ' AND cd.dispute_type_id <>' . CDisputeType::OTHER;
			if( true == is_numeric( $intUserId ) ) {
				$strColumnName = '  DISTINCT dl.consumer_id';
				$strJoin = ' LEFT JOIN dispute_logs dl ON dl.consumer_id = cd.consumer_id AND cd.assigned_to = ' . ( int ) $intUserId;
			}
		}

		$strSql = ' SELECT
                    	dst.id,
  						dst.name,
                        COUNT(' . $strColumnName . ' ) AS total_records
                   	FROM
                        dispute_status_types dst
                        LEFT JOIN consumer_disputes cd ON dst.id = cd.dispute_status_type_id ' . $strCondition . '
						LEFT JOIN consumers c ON c.id=cd.consumer_id ' .
		                $strJoin . '
                    WHERE
                        dst.id NOT IN(' . implode( ', ', CDisputeStatusType::$c_arrintDisputeStatusTypeIds ) . ')
        			GROUP BY
                         dst.id
                    ORDER BY
                         dst.order_number';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchConsumerDisputeFollowupCount( $intFollowupActionTypeId, $objDatabase ) {
		if( CFollowupActionType::DISPUTE_HIGHLIGHT == $intFollowupActionTypeId ) {
			$strCondition = ' AND CASE
                                    WHEN  date( cd.created_on) = date( CURRENT_DATE - dfr.action_days ) 
                                    THEN  date( cd.created_on) = date( CURRENT_DATE -dfr.action_days )
                                          AND to_char(cd.created_on, \'HH24:MI:SS\') <= \'17:00:00\' 
                                    END';
		} else {
			$strCondition = ' AND CASE
                                    WHEN  date( cd.created_on) < date( CURRENT_DATE - dfr.action_days ) THEN date( cd.created_on) < date( CURRENT_DATE -dfr.action_days )
                                    WHEN  date( cd.created_on) = date( CURRENT_DATE - dfr.action_days ) THEN to_char(cd.created_on, \'HH24:MI:SS\') <= \'17:00:00\' 
                                 END';
		}
		$strSql = 'SELECT
		   				dst.id,
					    dst.name,
					    COUNT ( DISTINCT cd.id ) AS total_records
					FROM
					    dispute_status_types dst
					    LEFT JOIN dispute_followup_rules dfr ON dfr.dispute_status_type_id = dst.id AND dfr.followup_action_type_id = ' . ( int ) $intFollowupActionTypeId . '
					    LEFT JOIN consumer_disputes cd ON dst.id = cd.dispute_status_type_id AND cd.dispute_type_id <> ' . CDisputeType::OTHER . $strCondition . '
                    WHERE
					    dst.id NOT IN(' . implode( ', ', CDisputeStatusType::$c_arrintNotIncludeDisputeStatusTypeIds ) . ') AND dst.id <> ' . CDisputeStatusType::DISPUTE_STATUS_TYPE_COMPLETE . '
                    GROUP BY
                        dst.id
                    ORDER BY
                       dst.order_number';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRecievedMailCount( $objDatabase ) {

		$strSql = ' SELECT
                    	dst.id,
  						dst.name,
                        COUNT( DISTINCT dl.consumer_id ) AS total_records

                   	FROM
                        dispute_status_types dst
                        LEFT JOIN consumer_disputes cd ON dst.id = cd.dispute_status_type_id
						LEFT JOIN consumers c ON c.id=cd.consumer_id
                       	LEFT JOIN dispute_logs dl ON dl.consumer_id = cd.consumer_id AND dl.log_type_id = ' . ClogType::OUTBOUND_NOTE . ' AND dl.created_on >=   ( ( CURRENT_DATE) - INTERVAL \'30 days\' )
				    WHERE
                       	dst.id IN(' . implode( ', ', CDisputeStatusType::$c_arrintIncludeDisputeStatusTypeIdsForRecievedMails ) . ')

        			GROUP BY
                         dst.id
                    ORDER BY
                         dst.order_number';

		return fetchData( $strSql, $objDatabase );
	}

}
?>