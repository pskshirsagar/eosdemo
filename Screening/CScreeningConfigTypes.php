<?php

class CScreeningConfigTypes extends CBaseScreeningConfigTypes {

    public static function fetchScreeningConfigTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CScreeningConfigType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchScreeningConfigType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CScreeningConfigType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }
}
?>