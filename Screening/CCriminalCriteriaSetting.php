<?php

class CCriminalCriteriaSetting extends CBaseCriminalCriteriaSetting {

	protected $m_intFelonyScreeningRecommendationTypeId;
	protected $m_intMisdemeanorScreeningRecommendationTypeId;

	public function setFelonyScreeningRecommendationTypeId( $intFelonyScreeningRecommendationTypeId ) {
		$this->m_intFelonyScreeningRecommendationTypeId = $intFelonyScreeningRecommendationTypeId;
	}

	public function getFelonyScreeningRecommendationTypeId() {
		return $this->m_intFelonyScreeningRecommendationTypeId;
	}

	public function setMisdemeanorScreeningRecommendationTypeId( $intMisdemeanorScreeningRecommendationTypeId ) {
		$this->m_intMisdemeanorScreeningRecommendationTypeId = $intMisdemeanorScreeningRecommendationTypeId;
	}

	public function getMisdemeanorScreeningRecommendationTypeId() {
		return $this->m_intMisdemeanorScreeningRecommendationTypeId;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['felony_screening_recommendation_type_id'] ) ) 		$this->setFelonyScreeningRecommendationTypeId( $arrmixValues['felony_screening_recommendation_type_id'] );
		if( true == isset( $arrmixValues['misdemeanor_screening_recommendation_type_id'] ) ) 	$this->setMisdemeanorScreeningRecommendationTypeId( $arrmixValues['misdemeanor_screening_recommendation_type_id'] );

		return;
	}

}
?>