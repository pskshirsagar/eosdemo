<?php

class CScreeningPackageConfigPreferenceTypeFilter extends CBaseScreeningPackageConfigPreferenceTypeFilter {

	protected $m_intScreeningConfigPreferenceFilterValue;
	protected $m_intScreeningFilterTypeId;
	protected $m_intScreeningFilterId;

    public function setScreeningFilterId( $intScreeningFilterId ) {
    	$this->m_intScreeningFilterId = $intScreeningFilterId;
    }

    public function getScreeningFilterId() {
    	return $this->m_intScreeningFilterId;
    }

    public function setScreeningConfigPreferenceFilterValue( $intScreeningConfigPreferenceFilterValue ) {
    	$this->m_intScreeningConfigPreferenceFilterValue = $intScreeningConfigPreferenceFilterValue;
    }

    public function getScreeningConfigPreferenceFilterValue() {
    	return $this->m_intScreeningConfigPreferenceFilterValue;
    }

    public function setScreeningFilterTypeId( $intScreeningFilterTypeId ) {
    	$this->m_intScreeningFilterTypeId = $intScreeningFilterTypeId;
    }

    public function getScreeningFilterTypeId() {
    	return $this->m_intScreeningFilterTypeId;
    }

    public function valId() {
        return true;
    }

    public function valCid() {
        return true;
    }

    public function valScreeningPackageId() {
        return true;
    }

    public function valScreeningConfigPreferenceTypeId() {
        return true;
    }

    public function valScreeningFilterId() {
        return true;
    }

    public function valIsActive() {
        return true;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		if( false == $boolIsValid ) {
        			return $boolIsValid;
        		}

        		$boolIsValid &= $this->valCid();
        		$boolIsValid &= $this->valScreeningPackageId();
        		$boolIsValid &= $this->valScreeningConfigPreferenceTypeId();
        		$boolIsValid &= $this->valScreeningFilterId();
        		break;

        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );
    	if( isset( $arrValues['screening_config_preference_filter_value'] ) ) $this->setScreeningConfigPreferenceFilterValue( $arrValues['screening_config_preference_filter_value'] );
    	if( isset( $arrValues['screening_filter_type_id'] ) ) $this->setScreeningFilterTypeId( $arrValues['screening_filter_type_id'] );
    	if( isset( $arrValues['screening_filter_id'] ) ) $this->setScreeningFilterId( $arrValues['screening_filter_id'] );
    	return;
    }

}
?>