<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\COffenseLevelTypes
 * Do not add any new functions to this class.
 */

class COffenseLevelTypes extends CBaseOffenseLevelTypes {

	public static function fetchOffenseLevelTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'COffenseLevelType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchOffenseLevelType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'COffenseLevelType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCustomActiveOffenseLevelTypes( $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						offense_level_types
					WHERE
						is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}
}
?>