<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningScreenClientRates
 * Do not add any new functions to this class.
 */

class CScreeningScreenClientRates extends CBaseScreeningScreenClientRates {

	public static function fetchStateClientRatesByCid( $intCid, $objDatabase, $boolIsPublished = false ) {

		$strSql = 'SELECT DISTINCT 	sscr.state_code,
									sscr.id,
									sscr.cid,
									sscr.client_cost,
									sscr.note,
									sscr.is_published,
									spbr.id as screening_provider_base_rate_id
							FROM
									screening_screen_client_rates sscr
									JOIN screening_provider_base_rates spbr ON spbr.state_code = sscr.state_code
							WHERE
									spbr.county_name IS NULL
									AND sscr.county_name IS NULL
									AND sscr.cid = ' . ( int ) $intCid;

		if( true == $boolIsPublished )
				$strSql .= ' AND is_published = true';

		return parent::fetchScreeningScreenClientRates( $strSql, $objDatabase );
	}

	public static function fetchCountyClientRatesByCid( $intCid, $objDatabase, $boolIsPublished = false ) {

		$strSql = 'SELECT DISTINCT 	sscr.county_name,
									sscr.id,
									sscr.state_code,
									sscr.cid,sscr.client_cost,
									sscr.note,
									sscr.is_published,
									spbr.id as screening_provider_base_rate_id
							FROM
									screening_screen_client_rates sscr
									JOIN screening_provider_base_rates spbr ON spbr.county_name = sscr.county_name
							WHERE
									spbr.county_name IS NOT NULL
									AND sscr.county_name IS NOT NULL
									AND sscr.cid = ' . ( int ) $intCid;

			if( true == $boolIsPublished )
					$strSql .= ' AND is_published = true';

		return parent::fetchScreeningScreenClientRates( $strSql, $objDatabase );

	}

	public static function fetchCustomScreeningClientRateBySearchTypeStateBySearchTypeCountyByCId( $strState, $strCounty, $intCid, $objDatabase ) {
		$strSql = '	SELECT
							*
					FROM
						screening_screen_client_rates
					WHERE
						is_published = true
						AND cid = ' . ( int ) $intCid;

		$strSql .= ' AND state_code = \'' . $strState . '\'';

		if( true == valStr( $strCounty ) ) {
			$strSql .= ' AND county_name = \'' . $strCounty . '\'';
		} else {
			$strSql .= ' AND county_name IS NULL ';
		}

		$strSql .= 'ORDER BY id DESC LIMIT 1';

		return parent::fetchScreeningScreenClientRate( $strSql, $objDatabase );
	}

	public static function fetchScreeningScreenClientRatesByScreenTypeIdByCid( $intScreenTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						screening_screen_client_rates
					WHERE
						is_published = true
						AND cid = ' . ( int ) $intCid;

		if( CScreenType::COUNTY_CRIMINAL_SEARCH == $intScreenTypeId ) {
			$strSql .= ' AND county_name IS NOT NULL';
		} else {
			$strSql .= ' AND county_name IS NULL';
		}

		return parent::fetchScreeningScreenClientRates( $strSql, $objDatabase );
	}

}
?>