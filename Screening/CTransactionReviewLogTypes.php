<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CTransactionReviewLogTypes
 * Do not add any new functions to this class.
 */

class CTransactionReviewLogTypes extends CBaseTransactionReviewLogTypes {

	public static function fetchTransactionReviewLogTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTransactionReviewLogType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTransactionReviewLogType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTransactionReviewLogType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>