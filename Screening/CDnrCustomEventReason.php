<?php

class CDnrCustomEventReason extends CBaseDnrCustomEventReason {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == valId( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Invalid client id.' ) );
		}
		return $boolIsValid;
	}

	public function valDelinquentEventTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getDelinquentEventTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delinquent_event_type_id', 'Please select event type' ) );
		}
		return $boolIsValid;
	}

	public function valDelinquentEventReasonId() {
		$boolIsValid = true;

		if( false == valId( $this->getDelinquentEventReasonId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delinquent_event_reason_id', 'Please select most similar event reason' ) );
		}
		return $boolIsValid;
	}

	public function valCustomReason( $objScreeningDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getCustomReason() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_reason', 'Please enter custom event reason' ) );
		}

		$strWhere = 'WHERE cid = ' . ( int ) $this->m_intCid . ' AND custom_reason = \'' . $this->getCustomReason() . '\' AND is_published = TRUE' . ( ( true == valId( $this->getId() ) ) ? ' AND id <> ' . ( int ) $this->getId() : '' );
		if( true == $boolIsValid && 0 < CDnrCustomEventReasons::fetchDnrCustomEventReasonCount( $strWhere, $objScreeningDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_reason', 'Custom event reason already exists' ) );
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objScreeningDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCustomReason( $objScreeningDatabase );
				$boolIsValid &= $this->valDelinquentEventTypeId();
				$boolIsValid &= $this->valDelinquentEventReasonId();
				$boolIsValid &= $this->valCid();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>