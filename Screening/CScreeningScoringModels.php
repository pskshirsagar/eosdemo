<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningScoringModels
 * Do not add any new functions to this class.
 */

class CScreeningScoringModels extends CBaseScreeningScoringModels {

	public static function fetchPublishedScreeningScoringModels( $objDatabse ) {
		return self::fetchScreeningScoringModels( 'SELECT * FROM screening_scoring_models WHERE is_published = 1 ', $objDatabse );
	}

}
?>