<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CTransactionReviews
 * Do not add any new functions to this class.
 */

class CTransactionReviews extends CBaseTransactionReviews {

	public static function fetchTransactionReviews( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTransactionReview', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTransactionReview( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTransactionReview', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTransactionReviewByScreeningTransactionIdByCid( $intScreeningTransactionId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM transaction_reviews WHERE screening_transaction_id = ' . ( int ) $intScreeningTransactionId . ' AND cid = ' . ( int ) $intCid;

		return parent::fetchTransactionReview( $strSql, $objDatabase );
	}

	public static function fetchTransactionReviewsByTransactionReviewId( $intTransactionReviewId, $objDatabase ) {
		$strSql = 'SELECT * FROM transaction_reviews WHERE id = ' . ( int ) $intTransactionReviewId;

		return parent:: fetchTransactionReview( $strSql, $objDatabase );
	}

	public static function getNextReviewItemByApplicationTypeIdByScreenTypeIds( $intApplicationTypeId, $arrintScreenTypeIds, $intCurrentUserId, $objDatabase, $intAssignNextOption = NULL ) {

		$strWhere = 'AND sdpt.is_test_type = 0';
		if( CTransactionReview::ASSIGN_NEXT_TEST_CLIENT == $intAssignNextOption ) {
			$strWhere = 'AND sdpt.is_test_type = 1';
		} else if( CTransactionReview::ASSIGN_NEXT_PRIORITY_CLIENT == $intAssignNextOption ) {
			$strWhere .= ' AND tr3.cid IN (' . sqlIntImplode( CTransactionReview::$c_arrintManualReviewPriorityClientIds ) . ') ';
		}

	    $strSql = 'UPDATE transaction_reviews tr2
					Set reviewed_on = CASE
								   WHEN tr2.transaction_review_status_id = ' . CTransactionReviewStatus::REVIEW_STATUS_NEW . ' Then NOW()
                                   ELSE reviewed_on
                                   END,
					transaction_review_status_id = CASE
								   WHEN tr2.transaction_review_status_id = ' . CTransactionReviewStatus::REVIEW_STATUS_NEW . ' Then ' . CTransactionReviewStatus::IN_PROGRESS . '
                                   ELSE tr2.transaction_review_status_id
                                   END,
                    updated_on = NOW(),
                    updated_by = ' . ( int ) $intCurrentUserId . '            
					FROM (
						SELECT tr.id,tr.cid, tr.transaction_review_status_id as previous_review_status_id,
						st2.screen_type_id, st2.screening_id, st2.screening_applicant_id,
						sct.name as screen_type_name, sst.name as screening_status, rt.name as recommendation
						FROM screening_transactions st
						INNER JOIN screening_transactions st2 on st.screening_id = st2.screening_id and st.screening_applicant_id = st2.screening_applicant_id
						INNER JOIN screen_types sct on st2.screen_type_id = sct.id
						INNER JOIN screening_status_types sst on sst.id = st2.screening_status_type_id
						INNER JOIN screening_recommendation_types rt on rt.id = st2.screening_recommendation_type_id
						INNER JOIN transaction_reviews tr on tr.screening_transaction_id = st2.id and tr.cid =st2.cid
						INNER JOIN screenings s on st.screening_id = s.id
						WHERE
						st.id IN ( 	SELECT screening_transaction_id
									FROM transaction_reviews tr3
									JOIN screening_transactions st3 ON st3.id = tr3.screening_transaction_id
									JOIN screenings s1 ON s1.id = st3.screening_id
									JOIN screening_applicants sa on sa.screening_id = s1.id  and sa.cid = s1.cid AND sa.id = st3.screening_applicant_id
									JOIN screening_data_providers sdp ON( sdp.id = st3.screening_data_provider_id AND st3.screen_type_id = sdp.screen_type_id )
									JOIN screening_data_provider_types sdpt ON(sdpt.id = sdp.screening_data_provider_type_id)
									WHERE
									tr3.transaction_review_status_id = ' . CTransactionReviewStatus::REVIEW_STATUS_NEW . '
									AND st3.screening_status_type_id = ' . ( int ) CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW . '
									AND st3.screen_type_id IN ( ' . implode( ',', $arrintScreenTypeIds ) . ' ) ' . '
									AND s1.application_type_id = ' . ( int ) $intApplicationTypeId . '
									AND sa.status_type_id NOT IN ( ' . implode( ',', CScreeningStatusType::$c_arrintCancelledStatusTypeIds ) . ' )
									AND pg_try_advisory_xact_lock( tr3.id )
									' . $strWhere . '
									ORDER BY tr3.transaction_review_priority_id desc, tr3.created_on LIMIT  1 FOR UPDATE
								)
						AND st2.screen_type_id IN ( ' . implode( ',', $arrintScreenTypeIds ) . ' ) ' . '
						AND st2.screening_response_completed_on is not null
						AND st2.screening_status_type_id NOT IN ( ' . implode( ',', CScreeningStatusType::$c_arrintCancelledStatusTypeIds ) . ' )
						AND s.application_type_id =' . ( int ) $intApplicationTypeId . '
					) trsub
 					WHERE trsub.id = tr2.id and trsub.cid= tr2.cid
					RETURNING tr2.*, trsub.previous_review_status_id, trsub.screening_id, trsub.screening_applicant_id, trsub.screen_type_id, trsub.screen_type_name, trsub.screening_status, trsub.recommendation ';

	    return parent::fetchTransactionReviews( $strSql, $objDatabase );
	}

	public static function getNextReviewItemOld( $objDatabase ) {

		$strSql = 'UPDATE
						transaction_reviews tr
					SET
						transaction_review_status_id = ' . CTransactionReviewStatus::IN_PROGRESS . '
					FROM  (
						   SELECT
								id
						   FROM
								transaction_reviews
						   WHERE
								transaction_review_status_id = ' . CTransactionReviewStatus::REVIEW_STATUS_NEW . '
						   		AND pg_try_advisory_xact_lock( id )
						   	Order by
								transaction_review_priority_id desc, created_on
						   LIMIT  1
						   FOR UPDATE
					   ) trsub
					WHERE
						tr.id = trsub.id
					RETURNING tr.*';

		return parent::fetchTransactionReview( $strSql, $objDatabase );
	}

	public static function fetchTransactionReviewsByTransactionReviewIdsByCid( $arrintTransactionReviewId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintTransactionReviewId ) || false == current( $arrintTransactionReviewId ) ) return;

	    $strSql = 'SELECT * FROM transaction_reviews WHERE id IN ( ' . implode( ',', $arrintTransactionReviewId ) . ' ) AND cid = ' . ( int ) $intCid;
	    return parent:: fetchTransactionReviews( $strSql, $objDatabase );
	}

	public static function updateTransactionReviews( $arrintTransactionReviewIds, $intTransactionReviewStatusId, $intUserId, $intCid, $objDatabase, $boolIsTransactionReviewReopened = false ) {
	    $boolIsValid = true;
	    $strSql = 'INSERT INTO transaction_review_logs
					(transaction_review_id, transaction_review_log_type_id, old_status_id, new_status_id, review_notes, updated_by, updated_on, created_by, created_on)
					SELECT id, 1, transaction_review_status_id, ' . ( int ) $intTransactionReviewStatusId . ', \'User Changed Status\', ' . ( int ) $intUserId . ', now(), ' . ( int ) $intUserId . ', now()
					FROM transaction_reviews WHERE id in ( ' . implode( ',', $arrintTransactionReviewIds ) . ' ) AND cid = ' . ( int ) $intCid;

	    if( false == $objDatabase->execute( $strSql ) ) {
	        $boolIsValid = false;
	    } else {
	        $strSql = 'UPDATE transaction_reviews tr
				   SET transaction_review_status_id = ' . ( int ) $intTransactionReviewStatusId;
			$strSql .= ( CTransactionReviewStatus::COMPLETE == $intTransactionReviewStatusId ) ? ', completed_on = NOW() ' : '';
		    $strSql .= ( true == $boolIsTransactionReviewReopened ) ? ', reviewed_on = NOW(), completed_on = NULL ' : '';
		    $strSql .= ', updated_by = ' . ( int ) $intUserId . ',' . '	  
				   updated_on = NOW()
				   WHERE tr.id in ( ' . implode( ',', $arrintTransactionReviewIds ) . ' ) AND cid = ' . ( int ) $intCid;

	        if( false == $objDatabase->execute( $strSql ) ) {
	            $boolIsValid = false;
	        }
	    }

	    return $boolIsValid;
	}

	public static function fetchTransactionReviewsByScreeningTransactionIdsByCidByStatusTypeIds( $arrintScreeningTransactionIds, $intCid, $arrintTransactionReviewStatusIds, $objDatabase ) {

	    $strSql = 'SELECT
	                       *
	               FROM
	                       transaction_reviews
	               WHERE
	                       screening_transaction_id IN ( ' . implode( ',', $arrintScreeningTransactionIds ) . ' )
	                       AND cid = ' . ( int ) $intCid . '
	                       AND transaction_review_status_id IN ( ' . implode( ',', $arrintTransactionReviewStatusIds ) . ' )';

		return parent:: fetchTransactionReviews( $strSql, $objDatabase );
	}

	public static function fetchTransactionReviewsByScreeningTransactionIdByCid( $intScreeningTransactionId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM transaction_reviews WHERE screening_transaction_id = ' . ( int ) $intScreeningTransactionId . ' AND cid = ' . ( int ) $intCid;

		return parent::fetchTransactionReviews( $strSql, $objDatabase );
	}

	public static function fetchTransactionReviewsCountByScreeningTransactionIdsByCidByStatusTypeIds( $arrintScreeningTransactionIds, $intCid, $arrintTransactionReviewStatusIds, $objDatabase ) {
		$strWhere = 'WHERE
	                       screening_transaction_id IN ( ' . implode( ',', $arrintScreeningTransactionIds ) . ' )
	                       AND cid = ' . ( int ) $intCid . '
	                       AND transaction_review_status_id IN ( ' . implode( ',', $arrintTransactionReviewStatusIds ) . ' )';

		return parent:: fetchTransactionReviewCount( $strWhere, $objDatabase );
	}

	public static function fetchTransactionReviewByIdByScreeningTransactionIdByCid( $intTransactionReviewId, $intScreeningTransactionId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM transaction_reviews WHERE id = ' . ( int ) $intTransactionReviewId . ' AND screening_transaction_id = ' . ( int ) $intScreeningTransactionId . ' AND cid = ' . ( int ) $intCid;

		return parent::fetchTransactionReview( $strSql, $objDatabase );
	}

	public static function fetchCriminalReviewByCompletedStatusByUpdatedOn( $strStartDate, $strEndDate, $objDatabase ) {

		if( true == is_null( $strStartDate ) || true == is_null( $strEndDate ) ) {
			return NULL;
		}

		$strSql = ' SELECT
					     trl.updated_by, COUNT(DISTINCT trl.transaction_review_id) AS total_records
					FROM
					    transaction_reviews tr
						INNER JOIN transaction_review_logs trl ON (tr.id = trl.transaction_review_id AND trl.new_status_id = ' . CTransactionReviewStatus::COMPLETE . ')
					WHERE
					    tr.transaction_review_status_id = ' . CTransactionReviewStatus::COMPLETE . '
					    AND DATE_TRUNC(\'day\', trl.updated_on) BETWEEN DATE(\'' . $strStartDate . '\') AND DATE(\'' . $strEndDate . '\')
					GROUP BY trl.updated_by';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAvgCompletionTimeForCriminalReviewByUpdatedOn( $strStartDate, $strEndDate, $objDatabase ) {

		if( true == is_null( $strStartDate ) || true == is_null( $strEndDate ) ) {
			return NULL;
		}

		$strSql = ' SELECT updated_by,
					    ((EXTRACT( DAYS FROM  total_time))*24 + (EXTRACT( HOUR FROM  total_time))) AS hours,
					    EXTRACT( MINUTE FROM  total_time) AS minutes

					FROM (
							SELECT
						           trl.updated_by, AVG(trl.updated_on - tr.created_on) AS total_time
							FROM
						            transaction_reviews tr
									INNER JOIN transaction_review_logs trl ON (tr.id = trl.transaction_review_id AND trl.new_status_id = ' . CTransactionReviewStatus::COMPLETE . ')
							WHERE
						           tr.transaction_review_status_id = ' . CTransactionReviewStatus::COMPLETE . '
						           AND DATE_TRUNC(\'day\', trl.updated_on) BETWEEN DATE(\'' . $strStartDate . '\') AND DATE(\'' . $strEndDate . '\')
						    GROUP BY trl.updated_by
						) AS temp_transaction_reviews';

		return fetchData( $strSql, $objDatabase );
	}

	public static function getNextReviewItemByTransactionReviewQueueTypeId( $intTransactionReviewQueueTypeId, $intCurrentUserId, $objDatabase, $intAssignNextOption = NULL ) {
		$strWhereCondition = ( $intTransactionReviewQueueTypeId != CTransactionReviewQueueType::ICORI_CO_COURT_REVIEW ) ? ' AND st2.screening_response_completed_on is not null ' : '';

		$strWhere = ' AND sdpt.is_test_type = 0';

		$strWhereReviewPriority = ' AND tr3. transaction_review_priority_id = ' . CTransactionReviewPriority::LOW;

		if( CTransactionReview::ASSIGN_NEXT_TEST_CLIENT == $intAssignNextOption ) {
			$strWhere = ' AND sdpt.is_test_type = 1';
		}

		if( CTransactionReview::ASSIGN_NEXT_PRIORITY_CLIENT == $intAssignNextOption ) {
			$strWhere = ' AND ( sdpt.is_test_type = 0 OR sdpt.is_test_type = 1 )';
			$strWhereReviewPriority = ' AND tr3. transaction_review_priority_id = ' . CTransactionReviewPriority::URGENT;
		}

		$strSql = 'UPDATE transaction_reviews tr2
					Set reviewed_on = CASE
								   WHEN tr2.transaction_review_status_id = ' . CTransactionReviewStatus::REVIEW_STATUS_NEW . ' Then NOW()
                                   ELSE reviewed_on
                                   END,
					transaction_review_status_id = CASE
								   WHEN tr2.transaction_review_status_id = ' . CTransactionReviewStatus::REVIEW_STATUS_NEW . ' Then ' . CTransactionReviewStatus::IN_PROGRESS . '
                                   ELSE tr2.transaction_review_status_id
                                   END,
                    updated_on = NOW(),
                    updated_by = ' . ( int ) $intCurrentUserId . '         
					FROM (
						SELECT 
							tr.id,
							tr.cid, 
							tr.transaction_review_status_id as previous_review_status_id,
							st2.screen_type_id, 
							st2.screening_id, 
							st2.screening_applicant_id,
							sct.name as screen_type_name, 
							sst.name as screening_status, 
							rt.name as recommendation
						FROM 
							screening_transactions st
							INNER JOIN screening_transactions st2 on st.screening_id = st2.screening_id and st.screening_applicant_id = st2.screening_applicant_id
							INNER JOIN screen_types sct on st2.screen_type_id = sct.id
							INNER JOIN screening_status_types sst on sst.id = st2.screening_status_type_id
							INNER JOIN screening_recommendation_types rt on rt.id = st2.screening_recommendation_type_id
							INNER JOIN transaction_reviews tr on tr.screening_transaction_id = st2.id and tr.cid =st2.cid						
						WHERE
							st.id IN ( 	SELECT 
											screening_transaction_id
										FROM 
											transaction_reviews tr3
											JOIN screening_transactions st3 ON st3.id = tr3.screening_transaction_id
											JOIN screening_data_providers sdp ON sdp.id = st3.screening_data_provider_id
											JOIN screening_data_provider_types sdpt On ( sdpt.id = sdp.screening_data_provider_type_id )	
											JOIN screening_applicants sa ON sa.id = st3.screening_applicant_id AND sa.screening_id = st3.screening_id										
										WHERE
											tr3.transaction_review_status_id = ' . CTransactionReviewStatus::REVIEW_STATUS_NEW . '
											AND st3.screening_status_type_id = ' . ( int ) CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW . '
											AND tr3.transaction_review_queue_type_id = ' . ( int ) $intTransactionReviewQueueTypeId . $strWhere . $strWhereReviewPriority . '
											AND sa.status_type_id NOT IN ( ' . implode( ',', CScreeningStatusType::$c_arrintCancelledStatusTypeIds ) . ' )
										ORDER BY 
											tr3.transaction_review_priority_id desc, tr3.created_on
										FOR UPDATE SKIP LOCKED 
										LIMIT 1
									)						 
							AND st2.screening_status_type_id NOT IN ( ' . implode( ',', CScreeningStatusType::$c_arrintCancelledStatusTypeIds ) . ' )						
							AND tr.transaction_review_queue_type_id = ' . ( int ) $intTransactionReviewQueueTypeId . '
							AND tr.transaction_review_status_id = ' . CTransactionReviewStatus::REVIEW_STATUS_NEW . '
							' . $strWhereCondition . '
							LIMIT
								1
						
					) trsub
 					WHERE trsub.id = tr2.id and trsub.cid= tr2.cid
					RETURNING tr2.*, trsub.previous_review_status_id, trsub.screening_id, trsub.screening_applicant_id, trsub.screen_type_id, trsub.screen_type_name, trsub.screening_status, trsub.recommendation ';

		return parent::fetchTransactionReviews( $strSql, $objDatabase );
	}

	public static function fetchCustomTransactionReviewDataByScreeningTransactionIdsByCid( $arrintScreeningTransactionIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
	                       id
	               FROM
	                       transaction_reviews
	               WHERE
	                       screening_transaction_id IN ( ' . sqlIntImplode( $arrintScreeningTransactionIds ) . ' )
	                       AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchTotalCoCourtTransactionReviewByScreeningIdByScreeningApplicantIdByCid( $intScreeningId, $intScreeningApplicantId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count(tr.id) as total_coocurt_reviews
					FROM
						transaction_reviews tr 
						JOIN screening_transactions st on st.id = tr.screening_transaction_id and st.cid = tr.cid
						JOIN screening_data_providers sdp on sdp.id = st.screening_data_provider_id 
						JOIN screening_data_provider_types sdpt on sdpt.id = sdp.screening_data_provider_type_id   
					WHERE
						st.cid = ' . ( int ) $intCid . '
						AND st.screening_id = ' . ( int ) $intScreeningId . '
						AND st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND sdpt.id IN ( ' . CScreeningDataProviderType::COCOURT . ',' . CScreeningDataProviderType::COCOURT_TEST . '  )';

		return parent::fetchColumn( $strSql, 'total_coocurt_reviews', $objDatabase );
	}

	public static function fetchUnprocessedTransactionReviewsByScreeningIdByCid( $intScreeningId, $intCid, $arrintInActiveStatusOfTransactionReviews, $objDatabase ) {
		$strSql = 'SELECT 
						count (tr.id) as count 
					FROM 
						transaction_reviews as tr 
					JOIN screening_transactions as st on ( tr.screening_transaction_id = st.id )
					WHERE 
					st.screening_id = ' . ( int ) $intScreeningId . ' 
					AND st.cid = ' . ( int ) $intCid . '
					AND tr.transaction_review_status_id NOT IN ( ' . implode( ',', $arrintInActiveStatusOfTransactionReviews ) . ' )';
		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function getNextRentalCollectionReviewItem( $objDatabase, $intCurrentUserId,  $intAssignNextOption = NULL ) {

		$strWhere = ' AND sdpt.is_test_type = 0';

		if( CTransactionReview::ASSIGN_NEXT_TEST_CLIENT == $intAssignNextOption ) {
			$strWhere = ' AND sdpt.is_test_type = 1';
		} else if( CTransactionReview::ASSIGN_NEXT_PRIORITY_CLIENT == $intAssignNextOption ) {
			$strWhere .= ' AND tr3.cid IN (' . sqlIntImplode( CTransactionReview::$c_arrintManualReviewPriorityClientIds ) . ') ';
		}

		$strSql  = 'UPDATE transaction_reviews tr2
					Set reviewed_on = CASE
								   WHEN tr2.transaction_review_status_id = ' . CTransactionReviewStatus::REVIEW_STATUS_NEW . ' Then NOW()
                                   ELSE reviewed_on
                                   END,
					transaction_review_status_id = CASE
								   WHEN tr2.transaction_review_status_id = ' . CTransactionReviewStatus::REVIEW_STATUS_NEW . ' Then ' . CTransactionReviewStatus::IN_PROGRESS . '
                                   ELSE tr2.transaction_review_status_id
                                   END,
                    updated_on = NOW(),
                    updated_by = ' . ( int ) $intCurrentUserId . '
					FROM (
						SELECT 
							tr.id,
							tr.cid, 
							tr.transaction_review_status_id as previous_review_status_id,
							st2.screen_type_id, 
							st2.screening_id, 
							st2.screening_applicant_id,
							sct.name as screen_type_name, 
							sst.name as screening_status, 
							rt.name as recommendation
						FROM 
							screening_transactions st
							INNER JOIN screening_transactions st2 on st.screening_id = st2.screening_id and st.screening_applicant_id = st2.screening_applicant_id
							INNER JOIN screen_types sct on st2.screen_type_id = sct.id
							INNER JOIN screening_status_types sst on sst.id = st2.screening_status_type_id
							INNER JOIN screening_recommendation_types rt on rt.id = st2.screening_recommendation_type_id
							INNER JOIN transaction_reviews tr on tr.screening_transaction_id = st2.id and tr.cid =st2.cid 
						WHERE
							st.id IN ( 	SELECT 
											screening_transaction_id
										FROM 
											transaction_reviews tr3
											JOIN screening_transactions st3 ON st3.id = tr3.screening_transaction_id
											JOIN screening_data_providers sdp ON sdp.id = st3.screening_data_provider_id
											JOIN screening_data_provider_types sdpt On ( sdpt.id = sdp.screening_data_provider_type_id )											
										WHERE
											tr3.transaction_review_status_id = ' . CTransactionReviewStatus::REVIEW_STATUS_NEW . ' 
											AND tr3.transaction_review_queue_type_id = ' . CTransactionReviewQueueType::RENTAL_COLLECTION_VERIFICATION . $strWhere . ' 
										ORDER BY 
											tr3.transaction_review_priority_id desc, tr3.created_on
										FOR UPDATE SKIP LOCKED 
										LIMIT 1
									)						 
							AND st2.screening_status_type_id NOT IN ( ' . implode( ',', CScreeningStatusType::$c_arrintCancelledStatusTypeIds ) . ' )						
							AND tr.transaction_review_queue_type_id = ' . CTransactionReviewQueueType::RENTAL_COLLECTION_VERIFICATION . '
							AND tr.transaction_review_status_id = ' . CTransactionReviewStatus::REVIEW_STATUS_NEW . '
							AND st2.screening_response_completed_on is not null
							LIMIT
								1
						
					) trsub
 					WHERE trsub.id = tr2.id and trsub.cid= tr2.cid
					RETURNING tr2.*, trsub.previous_review_status_id, trsub.screening_id, trsub.screening_applicant_id, trsub.screen_type_id, trsub.screen_type_name, trsub.screening_status, trsub.recommendation ';
		return parent::fetchTransactionReviews( $strSql, $objDatabase );
	}

}

?>