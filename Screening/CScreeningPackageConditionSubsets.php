<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageConditionSubsets
 * Do not add any new functions to this class.
 */

class CScreeningPackageConditionSubsets extends CBaseScreeningPackageConditionSubsets {

	public static function fetchScreeningPackageConditionSubsetsWithValuesByIds( $arrintScreningPackageConditionSubsetIds, $objDatabase ) {
		if( false == valArr( $arrintScreningPackageConditionSubsetIds ) ) return NULL;

		$strSql = '	SELECT
						spcss.*, spcsv.screening_package_available_condition_id, spcsv.default_value
					FROM
						screening_package_condition_subsets spcss
						JOIN screening_package_condition_subset_values spcsv ON ( spcss.id = spcsv.screening_package_condition_subset_id )
					WHERE
						spcss.id IN ( ' . implode( ',', $arrintScreningPackageConditionSubsetIds ) . ' )
					ORDER BY spcsv.id';

		return parent::fetchObjects( $strSql, 'CScreeningPackageConditionSubset', $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchScreeningPackageConditionSubsetsByIds( $arrintScreeningConditionSubsetIds, $objDatabase ) {
		if( false == valArr( $arrintScreeningConditionSubsetIds ) ) return NULL;

		$strSql = '	SELECT
						spcss.*
					FROM
						screening_package_condition_subsets spcss
					WHERE
						spcss.id IN ( ' . implode( ',', $arrintScreeningConditionSubsetIds ) . ' )
					ORDER BY spcss.id';

		return parent::fetchScreeningPackageConditionSubsets( $strSql, $objDatabase );
	}
}
?>