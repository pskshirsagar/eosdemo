<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDelinquentEventReasons
 * Do not add any new functions to this class.
 */

class CDelinquentEventReasons extends CBaseDelinquentEventReasons {

	public static function fetchDelinquentEventReasons( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDelinquentEventReason', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDelinquentEventReason( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDelinquentEventReason', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDelinquentEventReasonsByDelinquentEventTypeIds( $arrintDelinquentEventTypeIds, $objDatabase ) {

		if( false == valArr( $arrintDelinquentEventTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						delinquent_event_reasons
					WHERE
						delinquent_event_type_id IN (' . implode( ',', $arrintDelinquentEventTypeIds ) . ')
						AND is_published = 1
					ORDER BY delinquent_event_type_id';

		return self::fetchDelinquentEventReasons( $strSql, $objDatabase );
	}

	public static function fetchPublishedDelinquentEventReasons( $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    delinquent_event_reasons
					WHERE
					    is_published = 1
					ORDER BY
					    delinquent_event_type_id';

		return self::fetchDelinquentEventReasons( $strSql, $objDatabase );
	}

}

?>