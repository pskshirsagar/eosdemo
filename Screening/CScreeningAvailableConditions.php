<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningAvailableConditions
 * Do not add any new functions to this class.
 */

class CScreeningAvailableConditions extends CBaseScreeningAvailableConditions {

	public static function fetchPublishedScreeningAvailableConditionsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningAvailableConditions( sprintf( 'SELECT * FROM %s WHERE cid = %d AND is_published = true ', 'screening_available_conditions', $intCid ), $objDatabase );
	}

	public static function fetchScreeningAvailableConditionsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return false;

		$strSql = '	SELECT
						*
					FROM
						screening_available_conditions
					WHERE
						id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND cid = ' . ( int ) $intCid;
		return self::fetchScreeningAvailableConditions( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningAvailableConditionsByIdsByCidAssociatedWithConditionSets( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return false;

		$strSql = 'SELECT
						DISTINCT spac.id, spac.condition_name
					FROM
					    screening_available_conditions AS spac
					    JOIN screening_condition_subset_values spcsubval ON spcsubval.screening_available_condition_id = spac.id
					    JOIN screening_condition_subsets spcsub ON spcsubval.screening_condition_subset_id = spcsub.id
					    JOIN screening_condition_sets spcs ON ( spcs.first_screening_condition_subset_id = spcsub.id OR spcs.second_screening_condition_subset_id = spcsub.id )
					WHERE
						spac.id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND spcs.is_published = True
						AND spac.cid = ' . ( int ) $intCid;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFinancialPublishedScreeningAvailableConditionsByIdsByCid( $arrintAvailableConditionIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintAvailableConditionIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						screening_available_conditions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_published = true
						AND id IN ( ' . implode( ',', $arrintAvailableConditionIds ) . ' )
						AND screening_package_condition_type_id IN ( ' . implode( ',', CScreeningPackageConditionType::$c_arrintFinancialScreeningConditionTypeIds ) . ' ) ';

		return self::fetchScreeningAvailableConditions( $strSql, $objDatabase );
	}

	public static function fetchPublishedAvailableConditionsByScreeningConditionTypeIdsByCid( $arrintScreeningConditionTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScreeningConditionTypeIds ) ) return;

		$strSql = 'SELECT
						id,
						condition_name,
						screening_package_condition_type_id
					FROM
						screening_available_conditions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_published = true
						AND screening_package_condition_type_id IN( ' . sqlIntImplode( $arrintScreeningConditionTypeIds ) . ' )';

		$arrmixResult = executeSql( $strSql, $objDatabase );

		return ( false == getArrayElementByKey( 'failed', $arrmixResult ) && true == valArr( getArrayElementByKey( 'data', $arrmixResult ) ) ) ? getArrayElementByKey( 'data', $arrmixResult ) : [];
	}

	public static function fetchPublishedScreeningAvailableConditionsByCidByLeaseTypeCheck( $intCid, $objScreeningDatabase, $boolLoadMidLeaseTransferRenewalAvailableConditions = false ) {

        if( false == valId( $intCid ) ) return NULL;

        $strWhere = ( true == $boolLoadMidLeaseTransferRenewalAvailableConditions ) ? ' AND screening_package_condition_type_id IN ( ' . sqlIntImplode( CScreeningPackageConditionType::$c_arrintMidLeaseTransferRenewalScreeningConditionTypeIds ) . ' )' : '';

		$strSql = 'SELECT
						id,
						condition_name,
						screening_package_condition_type_id,
						screening_package_charge_code_amount_type_id
					FROM
						screening_available_conditions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_published = true ' . $strWhere;

        return fetchData( $strSql, $objScreeningDatabase );

    }

    public static function fetchPublishedScreeningAvailableConditionsAllDetailsByCidByLeaseTypeCheck( $intCid, $objScreeningDatabase, $boolLoadMidLeaseTransferRenewalAvailableConditions = false ) {

        if( false == valId( $intCid ) ) return NULL;

        $strWhere = ( true == $boolLoadMidLeaseTransferRenewalAvailableConditions ) ? ' AND screening_package_condition_type_id IN ( ' . sqlIntImplode( CScreeningPackageConditionType::$c_arrintMidLeaseTransferRenewalScreeningConditionTypeIds ) . ' )' : '';

        $strSql = 'SELECT
						*
					FROM
						screening_available_conditions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_published = true ' . $strWhere;

        return self::fetchScreeningAvailableConditions( $strSql, $objScreeningDatabase );

    }

}
?>