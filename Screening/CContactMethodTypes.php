<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CContactMethodTypes
 * Do not add any new functions to this class.
 */

class CContactMethodTypes extends CBaseContactMethodTypes {

	public static function fetchContactMethodTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CContactMethodType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchContactMethodType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CContactMethodType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedContactMethodTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						contact_method_types
					WHERE
						is_published = true';

		return self::fetchContactMethodTypes( $strSql, $objDatabase );
	}

}
?>