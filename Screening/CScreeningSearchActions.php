<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningSearchActions
 * Do not add any new functions to this class.
 */

class CScreeningSearchActions extends CBaseScreeningSearchActions {

	public static function fetchScreeningSearchActions( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningSearchAction', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchScreeningSearchAction( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningSearchAction', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedScreeningSearchActions( $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						screening_search_actions
					WHERE
						is_published = 1';

		return parent::fetchScreeningSearchActions( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningSearchActionsData( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_search_actions
					WHERE
						is_published = 1';

		$arrmixResult = executeSql( $strSql, $objDatabase );
		$arrmixResult = ( false == getArrayElementByKey( 'failed', $arrmixResult ) ) ? getArrayElementByKey( 'data', $arrmixResult ) : [];
		return $arrmixResult;
	}

}
?>