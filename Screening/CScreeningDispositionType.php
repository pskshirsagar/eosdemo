<?php

class CScreeningDispositionType extends CBaseScreeningDispositionType {

    public function validate( $strAction, $objScreeningDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
	            $boolIsValid &= $this->valName( $objScreeningDatabase );
	            break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// need to add
        }

        return $boolIsValid;
    }

	public function valName( $objScreeningDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name Is Required.' ) );
		}

		if( true == $boolIsValid && false == preg_match( '/^[A-Za-z -\/_][A-Za-z0-9 -\/_]*(?:-[A-Za-z0-9 -\/_]+)*$/', $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name Should Be Valid.' ) );
		}

		if( true == $boolIsValid ) {

			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );

			$intCount = Psi\Eos\Screening\CScreeningDispositionTypes::createService()->fetchScreeningDispositionTypeCount( ' WHERE lower( name ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) . '\'' . $strSqlCondition, $objScreeningDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Screening Disposition Type already Exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ? '\'' . $this->getMaxScreeningDispositionTypeId( $objDatabase ) . '\'' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, name, description, is_published, order_num )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlDescription() . ', ' .
		          $this->sqlIsPublished() . ', ' .
		          $this->sqlOrderNum() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function getMaxScreeningDispositionTypeId( $objDatabase ) {
		$strSql = 'SELECT MAX(id) AS max_screening_disposition_type_id FROM screening_disposition_types; ';

		$arrmixData = executeSql( $strSql, $objDatabase );

		return ( 0 == $arrmixData['failed'] ) ? $arrmixData['data'][0]['max_screening_disposition_type_id'] + 1 : NULL;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(); } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(); $boolUpdate = true; }

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

}
?>