<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CClientCriminalClassificationSubtypes
 * Do not add any new functions to this class.
 */

class CClientCriminalClassificationSubtypes extends CBaseClientCriminalClassificationSubtypes {

	public static function fetchAllactiveCriminalClassificationSubtypesByCidByClassificationTypeId( $intCriminalClassificationTypeId, $intClientId, $objDatabase ) {
		$strSql = ' SELECT 
                          *
					FROM
					     client_criminal_classification_subtypes
					WHERE
						 is_active = TRUE
						 AND criminal_classification_type_id = ' . ( int ) $intCriminalClassificationTypeId . '
					     AND cid = ' . ( int ) $intClientId . '
					     ORDER BY Id';

		return parent::fetchClientCriminalClassificationSubtypes( $strSql, $objDatabase );
	}

	public static function fetchAllActiveCriminalClassificationSubtypesAssociatedWithSubtypeIdsByCid( $intClientId, $objDatabase ) {
		$strSql = ' SELECT 
                          cccs.*
					FROM
					     client_criminal_classification_subtypes cccs
					     JOIN client_criminal_classification_subtype_keywords cccsk ON ( cccs.id = cccsk.client_criminal_classification_subtype_id AND cccs.cid = cccsk.cid )
					WHERE
						 cccs.is_active = TRUE
						 AND cccsk.is_published = 1
					     AND cccs.cid = ' . ( int ) $intClientId;

		return parent::fetchClientCriminalClassificationSubtypes( $strSql, $objDatabase );
	}

	public static function fetchClientCriminalClassificationSubtypeByIdByCid( $intClientCriminalClassificationSubTypeId, $intClientId, $objDatabase ) {

		$strSql = ' SELECT 
                          *
					FROM
					     client_criminal_classification_subtypes
					WHERE
						 id = ' . ( int ) $intClientCriminalClassificationSubTypeId . ' 
					     AND cid = ' . ( int ) $intClientId;

		return parent::fetchClientCriminalClassificationSubtype( $strSql, $objDatabase );
	}

	public static function fetchAllCriminalClassificationSubtypesByCid( $intClientId, $objDatabase ) {
		$strSql = ' SELECT 
						*
					FROM
					client_criminal_classification_subtypes
					WHERE
					cid = ' . ( int ) $intClientId;

		return parent::fetchClientCriminalClassificationSubtypes( $strSql, $objDatabase );
	}

	public static function fetchAllCustomActiveCriminalClassificationSubtypesByCid( $intClientId, $objDatabase ) {
		$strSql = ' SELECT 
                          id,subtype_name,criminal_classification_type_id
					FROM
					     client_criminal_classification_subtypes
					WHERE
						 is_active = TRUE
					     AND cid = ' . ( int ) $intClientId;

		return parent::fetchClientCriminalClassificationSubtypes( $strSql, $objDatabase );
	}

	public static function fetchClientCriminalClassificationSubtypesByIdsByCriminalClassificationTypeIdByCid( $arrintClientCriminalClassificationSubTypeIds, $intCriminalClassificationTypeId, $intClientId, $objDatabase ) {

		$strSql = ' SELECT 
                          *
					FROM
					     client_criminal_classification_subtypes
					WHERE
						 id IN ( ' . sqlIntImplode( $arrintClientCriminalClassificationSubTypeIds ) . ' ) 
						 AND criminal_classification_type_id = ' . ( int ) $intCriminalClassificationTypeId . '
					     AND cid = ' . ( int ) $intClientId;

		return parent::fetchClientCriminalClassificationSubtypes( $strSql, $objDatabase );
	}

}
?>