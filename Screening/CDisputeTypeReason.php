<?php

class CDisputeTypeReason extends CBaseDisputeTypeReason {
	const NO_KNOWLEDGE_OF_ACCOUNT 	                    = 1;
	const ACCOUNT_BALANCE 			                    = 2;
	const ACCOUNT_STATUS			                    = 3;
	const UNAUTHORIZED_INQUIRIES	                    = 4;
	const BANKRUPTCY				                    = 5;
	const I_AM_MINOR 				                    = 6;
	const CREDIT_OTHER				                    = 7;
	const CRIMINAL_NON_MATCH 		                    = 8;
	const EXPUNGED					                    = 9;
	const CRIMINAL_INCORRECT_DISPOSITION_INFORMATION	= 10;
	const CRIMINAL_OTHER 					            = 11;
	const NON_MATCH 						            = 12;
	const DISMISSED_NO_JUDGEMENT			            = 13;
	const INCORRECT_DISPOSITION_INFORMATION             = 14;
	const JUDGEMENT_BALANCE					            = 15;
	const REPORT_COPY 						            = 16;
	const REASON_FOR_DENIAL					            = 17;
	const REASON_FOR_CREDIT_INQUIRY 		            = 18;
	const OTHER 							            = 19;
	const EVICTION 										= 20;
	const FRAUDULENT_ACTIVITY_IDENTIFICATION			= 21;
	const FRAUDULENT_ACTIVITY_DATE_OF_BIRTH				= 22;
	const FRAUDULENT_ACTIVITY_PROOF_OF_INCOME			= 23;
	const FRAUDULENT_ACTIVITY_PROOF_OF_EMPLOYMENT		= 24;
	const FRAUDULENT_ACTIVITY_PAYMENT					= 25;
	const MOVED_OUT_WITH_BALANCE						= 26;
	const SKIPS											= 27;
	const CONTRACTUAL_AGREEMENT							= 28;
	const DOCUMENT 							            = 29;
    const STATUS 							            = 30;
    const BALANCE 							            = 31;
    const COLLECTION_AMOUNT 							= 32;
    const AMOUNT_COLLECTED 							    = 33;
	const EXPERIAN_CONSUMER_DISPUTE_VERIFICATION_FORM_NOT_REQUIRED = 34;
	const INTERNAL_CRIMINAL_NON_MATCH					= 35;
	const INTERNAL_CRIMINAL_EXPUNGED					= 36;
	const INTERNAL_CRIMINAL_DISPOSITION					= 37;
	const INTERNAL_CRIMINAL_OFFENSE_LEVEL				= 38;
	const INTERNAL_CRIMINAL_OFFENSE_DESCRIPTION			= 39;
	const INTERNAL_EVICTION_NON_MATCH					= 40;
	const INTERNAL_EVICTION_DISMISSED_NO_JUDGEMENT		= 41;
	const INTERNAL_EVICTION_DISPOSITION					= 42;
	const INTERNAL_EVICTION_JUDGEMENT_BALANCE			= 43;
	const NO_PII_NOT_REQUIRED							= 44;
	const MAIL_WITH_PII_NOT_REQUIRED					= 45;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getDisputeTypeReasonText( $intDisputeTypeReasonId ) {
		$strDisputeTypeReasonText = '';

		switch( $intDisputeTypeReasonId ) {
			case self::NO_KNOWLEDGE_OF_ACCOUNT:
				$strDisputeTypeReasonText = 'No knowledge of account';
				break;

			case self::ACCOUNT_BALANCE:
				$strDisputeTypeReasonText = 'Account balance';
				break;

			case self::ACCOUNT_STATUS:
				$strDisputeTypeReasonText = 'Account status';
				break;

			case self::UNAUTHORIZED_INQUIRIES:
				$strDisputeTypeReasonText = 'Unauthorized inquiries';
				break;

			case self::BANKRUPTCY:
				$strDisputeTypeReasonText = 'Bankruptcy';
				break;

			case self::I_AM_MINOR:
				$strDisputeTypeReasonText = 'I am a Minor';
				break;

			case self::CREDIT_OTHER:
				$strDisputeTypeReasonText = 'Other';
				break;

			case self::CRIMINAL_NON_MATCH:
				$strDisputeTypeReasonText = 'Non-Match';
				break;

			case self::EXPUNGED:
				$strDisputeTypeReasonText = 'Expunged';
				break;

			case self::CRIMINAL_INCORRECT_DISPOSITION_INFORMATION:
				$strDisputeTypeReasonText = 'Incorrect Disposition information';
				break;

			case self::CRIMINAL_OTHER:
				$strDisputeTypeReasonText = 'Other';
				break;

			case self::NON_MATCH:
				$strDisputeTypeReasonText = 'Non - Match';
				break;

			case self::DISMISSED_NO_JUDGEMENT:
				$strDisputeTypeReasonText = 'Dismissed / No judgement';
				break;

			case self::INCORRECT_DISPOSITION_INFORMATION:
				$strDisputeTypeReasonText = 'Incorrect Disposition information';
				break;

			case self::JUDGEMENT_BALANCE:
				$strDisputeTypeReasonText = 'Judgement Balance';
				break;

			case self::REPORT_COPY:
				$strDisputeTypeReasonText = 'Report Copy';
				break;

			case self::REASON_FOR_DENIAL:
				$strDisputeTypeReasonText = 'Reason for denial';
				break;

			case self::REASON_FOR_CREDIT_INQUIRY:
				$strDisputeTypeReasonText = 'Reason for credit inquiry';
				break;

			case self::OTHER:
				$strDisputeTypeReasonText = 'Other';
				break;

			case self::DOCUMENT:
				$strDisputeTypeReasonText = 'Document';
				break;

			case self::EVICTION:
				$strDisputeTypeReasonText = 'Eviction';
				break;

			case self::FRAUDULENT_ACTIVITY_IDENTIFICATION:
				$strDisputeTypeReasonText = 'Fraudulent Activity Identification';
				break;

			case self::FRAUDULENT_ACTIVITY_DATE_OF_BIRTH:
				$strDisputeTypeReasonText = 'Fraudulent Activity Date of Birth';
				break;

			case self::FRAUDULENT_ACTIVITY_PROOF_OF_INCOME:
				$strDisputeTypeReasonText = 'Fraudulent Activity Proof of Income';
				break;

			case self::FRAUDULENT_ACTIVITY_PROOF_OF_EMPLOYMENT:
				$strDisputeTypeReasonText = 'Fraudulent Activity Proof of Employment';
				break;

			case self::FRAUDULENT_ACTIVITY_PAYMENT:
				$strDisputeTypeReasonText = 'Fraudulent Activity Payment';
				break;

			case self::MOVED_OUT_WITH_BALANCE:
				$strDisputeTypeReasonText = 'Moved Out With Balance';
				break;

			case self::SKIPS:
				$strDisputeTypeReasonText = 'Skips';
				break;

			case self::CONTRACTUAL_AGREEMENT:
				$strDisputeTypeReasonText = 'Contractual Agreement';
				break;

            case self::STATUS:
                $strDisputeTypeReasonText = 'Status';
                break;

            case self::BALANCE:
                $strDisputeTypeReasonText = 'Balance';
                break;

            case self::COLLECTION_AMOUNT:
                $strDisputeTypeReasonText = 'Collection Amount';
                break;

            case self::AMOUNT_COLLECTED:
                $strDisputeTypeReasonText = 'Amount Collected';
                break;

			case self::EXPERIAN_CONSUMER_DISPUTE_VERIFICATION_FORM_NOT_REQUIRED:
				$strDisputeTypeReasonText = 'Experian Consumer Dispute Verification Form Not Required';
				break;

			case self::INTERNAL_CRIMINAL_NON_MATCH:
				$strDisputeTypeReasonText = 'Internal Criminal Non - Match';
				break;

			case self::INTERNAL_CRIMINAL_EXPUNGED:
				$strDisputeTypeReasonText = 'Internal Criminal Expunged';
				break;

			case self::INTERNAL_CRIMINAL_DISPOSITION:
				$strDisputeTypeReasonText = 'Internal Criminal Disposition';
				break;

			case self::INTERNAL_CRIMINAL_OFFENSE_LEVEL:
				$strDisputeTypeReasonText = 'Internal - Criminal Offense Level';
				break;

			case self::INTERNAL_CRIMINAL_OFFENSE_DESCRIPTION:
				$strDisputeTypeReasonText = 'Internal - Criminal Offense Description';
				break;

			case self::INTERNAL_EVICTION_NON_MATCH:
				$strDisputeTypeReasonText = 'Internal Eviction Non - Match';
				break;

			case self::INTERNAL_EVICTION_DISMISSED_NO_JUDGEMENT:
				$strDisputeTypeReasonText = 'Internal Eviction Dismissed / No judgement';
				break;

			case self::INTERNAL_EVICTION_DISPOSITION:
				$strDisputeTypeReasonText = 'Internal Eviction Disposition';
				break;

			case self::INTERNAL_EVICTION_JUDGEMENT_BALANCE:
				$strDisputeTypeReasonText = 'Internal Eviction Judgement Balance';
				break;

			case self::NO_PII_NOT_REQUIRED:
				$strDisputeTypeReasonText = 'No PII Not Required';
				break;

			case self::MAIL_WITH_PII_NOT_REQUIRED:
				$strDisputeTypeReasonText = 'Mail With PII Not Required';
				break;

			default:
				// added default case
		}

		return $strDisputeTypeReasonText;
	}

}
?>