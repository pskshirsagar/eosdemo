<?php

class CFollowupActionType extends CBaseFollowupActionType {

	const DISPUTE_HIGHLIGHT		= 1;
	const DISPUTE_EMAIL			= 2;
	const DISPUTE_ADD_NOTES		= 3;
	const DISPUTE_SMS			= 4;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>