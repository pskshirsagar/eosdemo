<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningTransactionErrorLogs
 * Do not add any new functions to this class.
 */

class CScreeningTransactionErrorLogs extends CBaseScreeningTransactionErrorLogs {

	public static function fetchActiveScreeningTransactionErrorLogsByScreeningTransactionIdsByCid( $arrintScreeningTransactionIds, $intCid, $objDatabase ) {

		if( !valArr( $arrintScreeningTransactionIds ) ) return false;

		$strSql = 'SELECT
                        id,
                        screening_transaction_id,
                        screening_data_provider_id,
                        screening_error_type_id,
                        error_code,
                        description as error_description
                    FROM
                        screening_transaction_error_logs
                    WHERE
                        screening_transaction_id IN ( ' . sqlIntImplode( $arrintScreeningTransactionIds ) . ' )
                        AND cid = ' . ( int ) $intCid . '
                        AND is_active IS true';

		$arrmixScreeningTransactionErrorLogs = fetchData( $strSql, $objDatabase );

		return $arrmixScreeningTransactionErrorLogs;
	}

	public static function archiveScreeningTransactionErrorLogs( $intScreneingTransactionId, $intCid, $intCurrentUserId, $objDatabase ) {

		if( !valId( $intScreneingTransactionId ) || !valId( $intCid ) ) return false;

		$strUpdateSql = 'UPDATE
                            screening_transaction_error_logs
                         SET
                            is_active = FALSE ,
                            updated_by = ' . ( int ) $intCurrentUserId . ',
                            updated_on = NOW() 
                         WHERE
                            screening_transaction_id = ' . ( int ) $intScreneingTransactionId . '
                            AND cid = ' . ( int ) $intCid;

		return executeSql( $strUpdateSql, $objDatabase );
	}

}

?>