<?php

class CScreeningListItem extends CBaseScreeningListItem {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningListTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valListItemName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valListItemDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>