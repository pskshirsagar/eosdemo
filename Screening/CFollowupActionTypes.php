<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CFollowupActionTypes
 * Do not add any new functions to this class.
 */

class CFollowupActionTypes extends CBaseFollowupActionTypes {

	public static function fetchFollowupActionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CFollowupActionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchFollowupActionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CFollowupActionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>