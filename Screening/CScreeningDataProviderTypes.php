<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningDataProviderTypes
 * Do not add any new functions to this class.
 */

class CScreeningDataProviderTypes extends CBaseScreeningDataProviderTypes {

    public static function fetchScreeningDataProviderTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CScreeningDataProviderType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchScreeningDataProviderType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScreeningDataProviderType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchPublishedScreeningDataProviderTypes( $objDatabase ) {
    	$strSql = 'SELECT
						*
					FROM
						screening_data_provider_types
					WHERE
						is_published = 1';

    	return self::fetchScreeningDataProviderTypes( $strSql, $objDatabase );
    }

	public static function fetchPublishedScreeningDataProviderTypesByTestType( $intTestType, $objDatabase ) {

		$strWhere 		= '';

		if( false == is_null( $intTestType ) && NULL != $intTestType ) {
			$strWhere = ' AND is_test_type = ' . ( int ) $intTestType;
		}

		$strSql = 'SELECT
						*
					FROM
						screening_data_provider_types
					WHERE
						is_published = 1
						' . $strWhere . ' ORDER BY name';

		return self::fetchScreeningDataProviderTypes( $strSql, $objDatabase );
    }

}
?>