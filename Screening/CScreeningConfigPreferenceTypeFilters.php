<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConfigPreferenceTypeFilters
 * Do not add any new functions to this class.
 */

class CScreeningConfigPreferenceTypeFilters extends CBaseScreeningConfigPreferenceTypeFilters {

	public static function fetchScreeningConfigPreferenceTypeFiltersByScreenTypeIds( $arrintScreenTypeIds, $objDatabase ) {

		$strSql = 'SELECT
						scptf.*,
					    sf.filter_value,
						sf.description,
						sf.screening_filter_type_id
					FROM
					    screening_config_preference_type_filters scptf
					    JOIN screening_filters sf ON ( sf.id = scptf.screening_filter_id )
					    LEFT JOIN screening_config_preference_types scpt ON ( scpt.id = scptf.screening_config_preference_type_id )
					WHERE
					    scpt.screen_type_id IN (' . implode( ',', $arrintScreenTypeIds ) . ')
					ORDER BY
					    sf.order_num';

		return self::fetchScreeningConfigPreferenceTypeFilters( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningConfigPreferenceTypeFiltersByScreenTypeId( $intScreenTypeId, $objDatabase ) {

		$strSql = 'SELECT
						scptf.id,
						scptf.screening_config_preference_type_id,
						scptf.screening_filter_id
					FROM
					    screening_config_preference_type_filters scptf
					    LEFT JOIN screening_config_preference_types scpt ON ( scpt.id = scptf.screening_config_preference_type_id )
					WHERE
					    scptf.is_active = 1
					    AND scpt.screen_type_id = ' . ( int ) $intScreenTypeId;

		$arrmixResult = executeSql( $strSql, $objDatabase );
		return ( false == getArrayElementByKey( 'failed', $arrmixResult ) ) ? getArrayElementByKey( 'data', $arrmixResult ) : [];
	}

}
?>