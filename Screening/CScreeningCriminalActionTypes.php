<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCriminalActionTypes
 * Do not add any new functions to this class.
 */

class CScreeningCriminalActionTypes extends CBaseScreeningCriminalActionTypes {

    public static function fetchScreeningCriminalActionTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CScreeningCriminalActionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchScreeningCriminalActionType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CScreeningCriminalActionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>