<?php

class CScreeningCondition extends CBaseScreeningCondition {

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public static function archiveScreeningConditions( $intScreeningId, $intCid, $intCurrentUserId, $objScreeningDatabase ) {
    	$strSql = ' UPDATE
    			     screening_conditions
    			   SET
    				is_active = 0,
    				updated_by = ' . ( int ) $intCurrentUserId . ',
    				updated_on = NOW()
    			   WHERE
    				screening_id = ' . ( int ) $intScreeningId . '
    				AND cid =  ' . ( int ) $intCid;

    	fetchData( $strSql, $objScreeningDatabase );
    }

}
?>