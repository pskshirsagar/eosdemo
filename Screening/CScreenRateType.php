<?php

class CScreenRateType extends CBaseScreenRateType {

	const PACKAGE 			= 1;
	const COMPONENT 		= 2;
	const SUBSCRIPTION 		= 3;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>