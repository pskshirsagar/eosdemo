<?php

class CScreeningAccountRate extends CBaseScreeningAccountRate {

	protected $m_strScreenTypeName;
	protected $m_intScreenRateTypeId;

    /**
     * Get Functions
     */

    public function getScreenTypeName() {
    	return $this->m_strScreenTypeName;
    }

    public function getScreenRateTypeId() {
    	return $this->m_intScreenRateTypeId;
    }

    /**
     * Set Functions
     */

    public function setScreenTypeName( $strScreenTypeName ) {
    	$this->m_strScreenTypeName = $strScreenTypeName;
    }

    public function setScreenRateTypeId( $intScreeningRateTypeId ) {
    	$this->m_intScreenRateTypeId = $intScreeningRateTypeId;
    }

    public function valCost() {
    	$boolIsValid = true;

        if( ( true == in_array( $this->getScreenTypeId(), CScreenType::$c_arrintAllowZeroRateScreenTypeIds ) && true == is_null( $this->getCost() ) ) || ( ( false == in_array( $this->getScreenTypeId(), CScreenType::$c_arrintAllowZeroRateScreenTypeIds ) ) && ( true == is_null( $this->getCost() ) || 0 >= $this->getCost() ) ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cost', 'Cost Is/Are Required.' ) );
    	}

    	return $boolIsValid;
    }

 	public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCost();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

    	parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrstrValues['screen_type_name'] ) ) $this->setScreenTypeName( $arrstrValues['screen_type_name'] );
    	if( true == isset( $arrstrValues['screen_rate_type_id'] ) ) $this->setScreenRateTypeId( $arrstrValues['screen_rate_type_id'] );
    	return;
    }

}
?>