<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCompanyPreferences
 * Do not add any new functions to this class.
 */

class CScreeningCompanyPreferences extends CBaseScreeningCompanyPreferences {

	public static function fetchActiveScreeningCompanyPreferencesByClientId( $intClientId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_company_preferences
					WHERE
						cid = ' . ( int ) $intClientId . '
						AND is_active = TRUE';

		return parent::fetchScreeningCompanyPreferences( $strSql, $objDatabase );
	}

	public static function archiveScreeningCompanyPreferences( $intCompanyUserId, $intCid ) {
		$strUpdateSql = 'UPDATE
							screening_company_preferences
						SET
							is_active = false,
							updated_by = ' . ( int ) $intCompanyUserId . ',
							updated_on = NOW()
						WHERE
							cid = ' . ( int ) $intCid . '
							AND is_active = true';

		return $strUpdateSql;
	}

	public static function fetchActiveScreeningCompanyPreferenceByClientIdByScreeningCompanyPreferenceTypeId( $intClientId, $intScreeningCompanyPreferenceTypeId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_company_preferences
					WHERE
						cid = ' . ( int ) $intClientId . '
						AND screening_company_preference_type_id = ' . ( int ) $intScreeningCompanyPreferenceTypeId . '
						AND is_active IS TRUE';

		return parent::fetchScreeningCompanyPreference( $strSql, $objDatabase );
	}

}
?>