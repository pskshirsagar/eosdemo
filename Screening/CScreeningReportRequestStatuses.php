<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningReportRequestStatuses
 * Do not add any new functions to this class.
 */

class CScreeningReportRequestStatuses extends CBaseScreeningReportRequestStatuses {

	public static function fetchScreeningReportRequestStatus( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningReportRequestStatus', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>