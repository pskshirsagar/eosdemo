<?php

class CRvLeaseNewDetail extends CBaseRvLeaseNewDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseIntervalId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningRecommendationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningDecisionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimaryCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseRenewalCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLatePaymentCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnedPaymentCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveOutBalance() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOpenLedgerBalance() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBadDebtWriteOffBalance() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRvRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRvTotalHouseholdIncome() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRvGuarantorIncome() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRiskPremiumRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRiskPremiumDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLateFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasGuarantor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasSkips() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasEvictions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasDelinquency() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCurrentLease() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsEntrataCore() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsTransfered() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseIntervalType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveInDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveOutDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>