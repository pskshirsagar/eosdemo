<?php

class CScreeningReportRequest extends CBaseScreeningReportRequest {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getScreeningApplicantDetails( $objScreeningDatabase ) {
		$arrmixScreeningApplicant = CScreeningApplicants::fetchScreeningApplicantDetailsByIdByCid( $this->getScreeningApplicantId(), $this->getCid(), $objScreeningDatabase );
		return $arrmixScreeningApplicant;
	}

}
?>