<?php

class CTransactionReviewQueueType extends CBaseTransactionReviewQueueType {

	const CRIMINAL_REVIEW                   = 1;
	const PASSPORT_VISA_VERIFICATION        = 2;
	const CORPORATE_LEAD                    = 3;
	const VENDOR_ACCESS_CRIMINAL            = 4;
	const ICORI_CO_COURT_REVIEW             = 5;
	const RENTAL_COLLECTION_VERIFICATION    = 6;


	public static $c_arrstrTransactionReviewQueueTypes = [
		'CRIMINAL_REVIEW'                   => self::CRIMINAL_REVIEW,
		'PASSPORT_VISA_VERIFICATION'        => self::PASSPORT_VISA_VERIFICATION,
		'VENDOR_ACCESS_CRIMINAL'            => self::VENDOR_ACCESS_CRIMINAL,
		'CORPORATE_LEAD'                    => self::CORPORATE_LEAD,
		'ICORI_CO_COURT_REVIEW'             => self::ICORI_CO_COURT_REVIEW,
		'RENTAL_COLLECTION_VERIFICATION'    => self::RENTAL_COLLECTION_VERIFICATION
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>