<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningDispositionTypes
 * Do not add any new functions to this class.
 */

class CScreeningDispositionTypes extends CBaseScreeningDispositionTypes {

    public static function fetchAllDispostionTypes( $objDatabase ) {
    	$strSql = 'SELECT * FROM screening_disposition_types';
    	return parent::fetchScreeningDispositionTypes( $strSql, $objDatabase );
    }

}
?>