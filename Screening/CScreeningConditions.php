<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConditions
 * Do not add any new functions to this class.
 */

class CScreeningConditions extends CBaseScreeningConditions {

	public static function fetchActiveScreeningConditionSetIdsByScreeningIdByCid( $intScreeningId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT( sc.screening_package_condition_set_id )
					FROM
						screening_conditions sc
                       JOIN screening_condition_sets scs ON scs.id = sc.screening_package_condition_set_id
					WHERE
						sc.screening_id = ' . ( int ) $intScreeningId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.is_active = 1';

		$arrintScreeningConditionSetIds = fetchData( $strSql, $objDatabase );
		$arrintScreeningConditionSetIds = ( true == valArr( $arrintScreeningConditionSetIds ) ) ? rekeyArray( 'screening_package_condition_set_id', $arrintScreeningConditionSetIds ) : NULL;

		return ( true == valArr( $arrintScreeningConditionSetIds ) ) ? array_keys( $arrintScreeningConditionSetIds ) : array();
	}

}
?>