<?php

class CScreening extends CBaseScreening {

    protected $m_arrobjScreeningApplicants;

    const SCREENED_WITH_PACKAGE_SETTINGS = 1;
	const SCREENING_RETENTION_PERIOD = 3;
	const SCREENING_RETENTION_PERIOD_MESSAGE = 'Screening Report Is No Longer Available, Data Retained For ' . self::SCREENING_RETENTION_PERIOD . ' Years.';
	const PRE_SCREENING_LOG_ENTRY = 1;
	const PRE_SCREENING_COMPLETE_STEP_ONE_AND_INITIATE_STEP_TWO = 2;
	const PRE_SCREENING_INITIATE_STEP_TWO = 3;

    public function setScreeningApplicants( $arrobjScreeningApplicants ) {
        $this->m_arrobjScreeningApplicants = $arrobjScreeningApplicants;
    }

    public function getScreeningApplicants() {
        return $this->m_arrobjScreeningApplicants;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
                break;

            default:
                // need to add comment
        }

        return $boolIsValid;
    }

    public function fetchScreeningApplicationResult( $intCurrentUserId, $objScreeningDatabase, $boolAllUnfilteredDataRequired = false, $boolIsRequiredDataForReport = false ) {
        $objStdScreeningResult = new stdClass();

        $objStdScreeningResult->ApplicationId       = $this->getCustomApplicationId();
        $objStdScreeningResult->Rent                = $this->getRent();
	    $objStdScreeningResult->RentCredit          = $this->getRentCredit();
        $objStdScreeningResult->ReferenceId         = $this->getId();
        $objStdScreeningResult->StatusTypeId        = $this->getScreeningStatusTypeId();
	    $objStdScreeningResult->ApplicationTypeId   = $this->getApplicationTypeId();
	    $objStdScreeningResult->IsOutOfScreeningRetentionPeriod  = ( date( 'Y-m-d', strtotime( $this->getCreatedOn() ) ) < date( 'Y-m-d', strtotime( '-' . CScreening::SCREENING_RETENTION_PERIOD . ' years' ) ) ) ? true : false;

        $objStdScreeningResult->GroupRecommendationTypeId = $this->getScreeningRecommendationTypeId();
        $boolIsRvIndexOnForApplication = false;

        $arrobjScreeningDecisionTypes        = CScreeningRecommendationTypes::fetchAllScreeningRecommendationTypes( $objScreeningDatabase );
        $arrobjRekeyedScreeningDecisionTypes = ( true == valArr( $arrobjScreeningDecisionTypes ) ) ? rekeyObjects( 'id', $arrobjScreeningDecisionTypes ) : array();

        $objStdScreeningResult->GroupRecommendation = CResidentVerifyServiceUtil::getScreeningRecommendationText( $this->getScreeningStatusTypeId(), $this->getScreeningRecommendationTypeId() );
        $objStdScreeningResult->GroupResultOverriddenBy = CCustomerType::createService()->customerTypeIdToStr( $this->getGroupResultOverriddenBy() );

		// reverting code as inactive applicant had the issue
	    $arrobjScreeningApplicants  = CScreeningApplicants::fetchActiveScreeningApplicantsByScreeningIdByCid( $this->getId(), $this->getCid(), $objScreeningDatabase );

        $objStdScreeningResult->ScreeningApplicants = array();
        $objStdScreeningResult->ScreenTypeIds       = array();

        $arrobjRekeyedScreeningApplicants    = rekeyObjects( 'ScreeningPackageId', $arrobjScreeningApplicants );
        $arrintApplicantsScreeningPackageIds = ( true == valArr( $arrobjRekeyedScreeningApplicants ) ) ? array_filter( array_keys( $arrobjRekeyedScreeningApplicants ) ) : array();
        $arrobjScreeningPackages             = CScreeningPackages::fetchAllScreeningPackagesByIdsCid( $arrintApplicantsScreeningPackageIds, $this->getCid(), $objScreeningDatabase );

        if( false == valArr( $arrobjScreeningApplicants ) ) {
            return false;
        }

	    $fltCombineIncome = $this->getTotalHouseholdIncome();
        $objStdScreeningResult->combineIncome             = $fltCombineIncome;
        $objStdScreeningResult->combinedRentToIncomeRatio = ( 0 < $fltCombineIncome && 0 < $this->getRent() ) ? $fltCombineIncome / ( float ) $this->getRent() : NULL;

        $arrintAppliedScreeningConditionSetIds = array();

        if( CScreeningRecommendationType::PASSWITHCONDITIONS == $this->getScreeningRecommendationTypeId() ) {
            $arrintAppliedScreeningConditionSetIds = CScreeningConditions::fetchActiveScreeningConditionSetIdsByScreeningIdByCid( $this->getId(), $this->getCid(), $objScreeningDatabase );
        }

        $arrintScreeningPackageIds = ( true == valArr( $arrobjScreeningApplicants ) ) ? array_filter( array_keys( rekeyObjects( 'ScreeningPackageId', $arrobjScreeningApplicants ) ) ) : array();

	    $arrmixScreeningConditionSetDetails = $this->getScreeningPackageConditionSetDetails( $arrintScreeningPackageIds, $objScreeningDatabase );

        $arrintScreeningConditionSetIds = ( true == valArr( $arrmixScreeningConditionSetDetails ) ) ? array_keys( $arrmixScreeningConditionSetDetails ) : array();

        $arrobjScreenTypes = CScreenTypes::fetchPublishedScreenTypes( $objScreeningDatabase );

        $boolIsApplicationPreScreened           = false;
        $boolIsAllowRecalculateResult           = false;

        $arrintPendingPreScreeningApplicantIds  = array();
        $arrmixApplicationDenialReasons         = array();
        $arrintScreenTypeIds                    = array();

        $arrmixApplicationDenialReasons[CScreenType::CREDIT]['ScreenTypeName']      = $arrobjScreenTypes[CScreenType::CREDIT]->getName();
        $arrmixApplicationDenialReasons[CScreenType::CRIMINAL]['ScreenTypeName']    = $arrobjScreenTypes[CScreenType::CRIMINAL]->getName();
        $arrmixApplicationDenialReasons[CScreenType::EVICTION]['ScreenTypeName']    = $arrobjScreenTypes[CScreenType::EVICTION]->getName();
        $arrmixApplicationDenialReasons[CScreenType::INCOME]['ScreenTypeName']      = $arrobjScreenTypes[CScreenType::INCOME]->getName();
        $arrmixApplicationDenialReasons[CScreenType::DO_NOT_RENT]['ScreenTypeName'] = $arrobjScreenTypes[CScreenType::DO_NOT_RENT]->getName();
        $arrmixApplicationDenialReasons[CScreenType::PRECISE_ID]['ScreenTypeName']  = $arrobjScreenTypes[CScreenType::PRECISE_ID]->getName();

        $arrintScreeningTransactionIds                   = array();
        $arrintScreeningApplicantManualCriminalSearchIds = array();

		$arrmixRentalCollectionEnabledScreeningPackages = CScreeningCriteriaSettings::fetchScreeningPackageIdsForScreeningApplicantHavingRentalCollectionCriteriaByScreeningIdByCid( $this->getId(), $this->getCid(), $objScreeningDatabase );
	    $arrintRentalCollectionEnabledScreeningPackages = ( true == valArr( $arrmixRentalCollectionEnabledScreeningPackages ) ) ? array_keys( rekeyArray( 'screening_package_id', $arrmixRentalCollectionEnabledScreeningPackages ) ) : [];
	    $this->m_boolIsOnMaintenanace                   = CScreeningUtils::checkScreeningGlobalSettingBySettingTypeId( CScreeningGlobalSettingType::SCREENING_MAINTENANCE, $objScreeningDatabase );
	    $boolIsOnMaintenanace                           = false;
	    foreach( $arrobjScreeningApplicants as $objScreeningApplicant ) {

            $arrobjScreeningTransactions    = $objScreeningApplicant->getScreeningApplicantResults( $objScreeningDatabase, true );

            $arrobjRekeyedScreeningTransactions    = ( true == valArr( $arrobjScreeningTransactions ) ) ? rekeyObjects( 'ScreenTypeId', $arrobjScreeningTransactions ): array();
            $arrintScreenTypes                     = ( true == valArr( $arrobjRekeyedScreeningTransactions ) ) ? array_keys( $arrobjRekeyedScreeningTransactions ) : array();

            // 1. Do a rekey objects by screen type id
            // 2. Get array keys ... which will be screen type ids
            // 3. Based on the screen type get applicant results

            $arrmixApplicantScreeningData = array();

            if( false == $boolAllUnfilteredDataRequired ) {

                $objStdSecureClientRequest  = new stdClass();
                $objStdSecureClientRequest->screeningApplicantId        = $objScreeningApplicant->getId();
                $objStdSecureClientRequest->cid                         = $objScreeningApplicant->getCid();
                $objStdSecureClientRequest->screeningId                 = $objScreeningApplicant->getScreeningId();
                $objStdSecureClientRequest->ScreeningRequestTypeId      = 15; // don't know what this is ???
                $objStdSecureClientRequest->screenTypes                 = $arrintScreenTypes;
                $objStdSecureClientRequest->allUnfilteredDataRequired   = $boolAllUnfilteredDataRequired;
                $objStdSecureClientRequest->currentUserId               = $intCurrentUserId;
                // @ Todo : define screening data request type constants in a class so that it will be accessible anywhere.
                $objStdSecureClientRequest->screeningDataRequestTypeId  = ( true == $boolIsRequiredDataForReport ) ? 2 : 0;

                $objResidentVerifySecureClientLibrary   = new CResidentVerifySecureClientLibrary();
                $arrmixResponseData                     = $objResidentVerifySecureClientLibrary->getApplicantResults( $objStdSecureClientRequest, $boolAllUnfilteredDataRequired );

                // validate response here.. for now checking only the success parameter
                if( 1 != $arrmixResponseData['response']['result']['Success'] ) {
                    trigger_error( 'Unable to fetch data from Secured database, for Screening Id ' . $objScreeningApplicant->getScreeningId() . ' for applicant ID: ' . $objScreeningApplicant->getId() . ' and Client Id: ' . $objScreeningApplicant->getCid(), E_USER_ERROR );
                }

                $arrmixApplicantScreeningData = ( true == valArr( $arrmixResponseData['response']['result']['applicantResults'] ) ) ? $arrmixResponseData['response']['result']['applicantResults'] : array();
            }

            $objScreeningPackage = NULL;

            if( false == is_null( $objScreeningApplicant->getScreeningPackageId() ) && true == valArr( $arrobjScreeningPackages ) ) $objScreeningPackage = ( true == array_key_exists( $objScreeningApplicant->getScreeningPackageId(), $arrobjScreeningPackages ) ) ? $arrobjScreeningPackages[$objScreeningApplicant->getScreeningPackageId()] : NULL;

			$intApplicantFailedCounts       = 0;
            $intApplicantConditionalCounts  = 0;
            $intApplicantInprogressCounts   = 0;

            $boolIsCreditScreeningRequest                   = false;
            $boolIsCriminalScreeningRequest                 = false;
            $boolIsEvictionScreeningRequest                 = false;
            $boolIsIncomeScreeningRequest                   = false;
            $boolIsVerificationOfIncomeRequest              = false;
            $boolIsBusinessCreditScreeningRequest           = false;
            $boolIsBusinessPremierProfileScreeningRequest   = false;
            $boolIsDoNotRentScreeningRequest                = false;
	        $boolIsPassportVerification                     = false;
	        $boolIsRentalCollectionRequest                  = false;
            $boolIsPreciseIdScreeningRequest                = false;
			$boolIsAttributeBased                           = false;

            $boolIsStateCriminalSearch = false;
            $boolIsCountyCriminalSearch = false;

            $objStdScreeningApplicantDetails = new stdClass();

            $objStdScreeningApplicantDetails->Id             = $objScreeningApplicant->getId();
            $objStdScreeningApplicantDetails->ApplicantId    = $objScreeningApplicant->getCustomApplicantId();
            $objStdScreeningApplicantDetails->StatusTypeId   = $objScreeningApplicant->getStatusTypeId();

            $objStdScreeningApplicantDetails->FirstName                 = $objScreeningApplicant->getNameFirst();
            $objStdScreeningApplicantDetails->MiddleName                = $objScreeningApplicant->getNameMiddle();
            $objStdScreeningApplicantDetails->LastName                  = $objScreeningApplicant->getNameLast();
            $objStdScreeningApplicantDetails->ApplicantIncome           = $objScreeningApplicant->getIncome();
            $objStdScreeningApplicantDetails->ApplicantSSN              = $objScreeningApplicant->getTaxNumberLastFour();
            $objStdScreeningApplicantDetails->ScreeningApplicantTypeId  = $objScreeningApplicant->getScreeningApplicantTypeId();
            $objStdScreeningApplicantDetails->ScreeningInitiatedOn      = $objScreeningApplicant->getCreatedOn();
            $objStdScreeningApplicantDetails->ApplicantPackageName      = ( true == valObj( $objScreeningPackage, 'CScreeningPackage' ) ) ? $objScreeningPackage->getName() : '';
	        $objStdScreeningApplicantDetails->ApplicantPackageTypeId    = ( true == valObj( $objScreeningPackage, 'CScreeningPackage' ) ) ? $objScreeningPackage->getScreeningPackageTypeId() : '';
            $objStdScreeningApplicantDetails->ApplicantBirthDate        = base64_decode( $objScreeningApplicant->getBirthDateEncrypted() );
            $objStdScreeningApplicantDetails->ApplicantAddress          = $objScreeningApplicant->getAddressLine();
            $objStdScreeningApplicantDetails->ApplicantCity             = $objScreeningApplicant->getCity();
            $objStdScreeningApplicantDetails->ApplicantState            = $objScreeningApplicant->getState();
            $objStdScreeningApplicantDetails->ApplicantZipCode          = $objScreeningApplicant->getZipCode();

            if( true == valObj( $objScreeningPackage, 'CScreeningPackage' ) && true === $objScreeningPackage->getIsUseForPreScreening() ) {
                $boolIsApplicationPreScreened = true;

                $intRemainingTransactionsCount  = ( int ) $objScreeningApplicant->fetchRemainingTransactionsCount( $objScreeningDatabase );

                if( 0 < $intRemainingTransactionsCount ) $arrintPendingPreScreeningApplicantIds[$objScreeningApplicant->getId()] = $objScreeningApplicant->getScreeningPackageId();
            }

		    // for check skipped voi transaction
		    $arrmixSkippedScreeningTransaction    = $objScreeningApplicant->getSkippedScreeningApplicantTransaction( $objScreeningDatabase, CScreenType::VERIFICATION_OF_INCOME );
		    $objStdScreeningApplicantDetails->boolIsSkippedVOITransaction = false;
		    if( true == valArr( $arrmixSkippedScreeningTransaction ) ) {
			    $arrmixAdditionalDetail = json_decode( $arrmixSkippedScreeningTransaction['additional_details'], true );
			    if( true == $arrmixSkippedScreeningTransaction['is_skipped'] && true == array_key_exists( 'reinitiate_expired_on', $arrmixAdditionalDetail ) ) {

				    if( time() <= $arrmixAdditionalDetail['reinitiate_expired_on'] ) {
					    $objStdScreeningApplicantDetails->boolIsSkippedVOITransaction = true;
				    }
			    }
		    }

		    // adding applicant wise screening settings to object
            $objStdScreeningApplicantConfigurationDetails = new stdClass();

            $objStdScreeningApplicantConfigurationDetails->isCriminalCategorizationEnabled = ( true == valArr( $arrobjRekeyedScreeningTransactions ) && true == array_key_exists( CScreenType::CRIMINAL_CATEGORIZATION, $arrobjRekeyedScreeningTransactions ) ) ? true : false;
	        $objStdScreeningApplicantConfigurationDetails->isUsedForRVIndexScore = ( true == valObj( $objScreeningPackage, 'CScreeningPackage' ) ) ? $objScreeningPackage->getIsUseForRvIndexScore() : 0;
            $objStdScreeningApplicantDetails->applicantScreeningSettings                   = $objStdScreeningApplicantConfigurationDetails;

            $objStdScreeningApplicantDetails->ScreenResults = array();

		    $intScreeningVendorId = CTransmissionVendor::RESIDENT_VERIFY;
            $intNullDataProviderCnt = 0;

            $arrmixManualCriminalSearchResult       = array();
            $arrintScreeningApplicantTransactionIds = array();

	        $arrintApplicantCompletedTransactionIds = array();
			$arrintScreeningCompletedScreenTypeIds  = array();
	        $arrmixCriminalSearchScope              = [];

			$boolIsForceResubmit = false;
		    $boolIsManualSearchRequestPending   = false;
		    if( true == valArr( $arrobjScreeningTransactions ) ) {

			    $objStdScreeningApplicantDetails->is_conditional_for_rcpp == false;

                foreach( $arrobjScreeningTransactions as $objScreeningTransaction ) {

                    $arrintScreenTypeIds[]                    = $objScreeningTransaction->getScreenTypeId();
                    $arrintScreeningTransactionIds[]          = $objScreeningTransaction->getId();
                    $arrintScreeningApplicantTransactionIds[] = $objScreeningTransaction->getId();

                    if( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED == $objScreeningTransaction->getScreeningStatusTypeId() ) {
	                    $arrintApplicantCompletedTransactionIds[] = $objScreeningTransaction->getId();
						$arrintScreeningCompletedScreenTypeIds[] = $objScreeningTransaction->getScreenTypeId();
                    }

	                if( true == $this->m_boolIsOnMaintenanace ) {
		                $boolIsOnMaintenanace  = ( true == CScreeningUtils::isLiveDataProviderTypeById( $objScreeningTransaction->getScreeningDataProviderTypeId() ) ) ? true : false;
	                }

	                if( false == $boolIsOnMaintenanace ) {
		                $boolIsOnMaintenanace    = $objScreeningTransaction->getOnMaintenance();
	                }

	                $objStdScreenTypeResult  = new stdClass();
                    $objStdScreenTypeResult->ScreenType             = $arrobjScreenTypes[$objScreeningTransaction->getScreenTypeId()]->getName();
                    $objStdScreenTypeResult->RecommendationTypeId   = $objScreeningTransaction->getScreeningRecommendationTypeId();
	                $objStdScreenTypeResult->screeningStatusTypeId  = $objScreeningTransaction->getScreeningStatusTypeId();

	                if( CScreenType::CREDIT == $objScreeningTransaction->getScreenTypeId() && true == $objScreeningTransaction->isForceResubmit() ) $boolIsForceResubmit = true;

                    if( true == is_null( $objScreeningTransaction->getScreeningDataProviderId() ) ) {
                        $intNullDataProviderCnt += 1;
                    }

                    switch( $objScreeningTransaction->getScreeningRecommendationTypeId() ) {
                        case CScreeningRecommendationType::FAIL:
                            $intApplicantFailedCounts += 1;
	                        $arrintScreeningApplicantDenialReasonIds[$objScreeningApplicant->getCustomApplicantId()][] = $objScreeningTransaction->getScreenTypeId();
                            $arrmixApplicationDenialReasons['isFailedScreening'][] = CScreeningDecisionReasonType::getScreeningDecisionReasonTypeIdByScreenTypeId( $objScreeningTransaction->getScreenTypeId() );
                            break;

                        case CScreeningRecommendationType::PASSWITHCONDITIONS:
                            $arrmixApplicationDenialReasons['isPassWithConditionScreening'][] = CScreeningDecisionReasonType::getScreeningDecisionReasonTypeIdByScreenTypeId( $objScreeningTransaction->getScreenTypeId() );
                            $intApplicantConditionalCounts += 1;
                            break;

                        default:
                            // need to add comment
                    }

                    switch( $objScreeningTransaction->getScreenTypeId() ) {
                        case CScreenType::INCOME:
                            $boolIsIncomeScreeningRequest = true;

                            $arrintScreenTypes              = array( CScreenType::INCOME );
                            $arrmixCustomerIncomeData       = ( true == valArr( $arrmixApplicantScreeningData ) && true == array_key_exists( 'ScreeningCustomerIncomeValues', $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningCustomerIncomeValues'] : array();
                            $arrmixIncomeValues             = ( true == valArr( $arrmixCustomerIncomeData ) ) ? $arrmixCustomerIncomeData : array();
                            $arrmixRekeyedIncomeValues      = ( true == valArr( $arrmixIncomeValues ) ) ? rekeyArray( 'id', $arrmixIncomeValues ) : array();
                            $arrmixIncomeConditionSetValues = rekeyArray( 'condition_id', ( array ) CScreeningConditionSets::fetchCustomScreeningConditionSetsByScreenTypeIdByScreeningPackageId( CScreenType::INCOME, $objScreeningApplicant->getScreeningPackageId(), $objScreeningDatabase ) );

                            $objStdScreenTypeResult->IncomeValues               = $arrmixRekeyedIncomeValues;
                            $objStdScreenTypeResult->IncomeConditionSetValues   = $arrmixIncomeConditionSetValues;
                            break;

	                    case CScreenType::VERIFICATION_OF_INCOME:
		                    $boolIsVerificationOfIncomeRequest = true;

		                    $arrmixApplicantVerificationOfIncomeValues                      = ( true == valArr( $arrmixApplicantScreeningData ) && true == array_key_exists( 'ScreeningApplicantVerificationOfIncomeValues', $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantVerificationOfIncomeValues'] : [];
		                    $objStdScreenTypeResult->VerificationOfIncomeValues             = rekeyArray( 'id', $arrmixApplicantVerificationOfIncomeValues );
		                    $objStdScreenTypeResult->VerificationOfIncomeConditionSetValues = rekeyArray( 'condition_id', ( array ) CScreeningConditionSets::fetchCustomScreeningConditionSetsByScreenTypeIdByScreeningPackageId( CScreenType::VERIFICATION_OF_INCOME, $objScreeningApplicant->getScreeningPackageId(), $objScreeningDatabase ) );

		                    // allow reinitiate only before reinitiate expired on
		                    $objStdAdditionalDetail = $arrobjRekeyedScreeningTransactions[CScreenType::VERIFICATION_OF_INCOME]->getAdditionalDetails();
		                    $objStdScreenTypeResult->boolIsAllowReinitiate = false;
		                    if( true == valObj( $objStdAdditionalDetail, 'stdClass' ) && true == property_exists( $objStdAdditionalDetail, 'reinitiate_expired_on' ) ) {

			                    if( time() <= $objStdAdditionalDetail->reinitiate_expired_on ) {
				                    $objStdScreenTypeResult->boolIsAllowReinitiate = true;
			                    }
		                    }
							break;

                        case CScreenType::CRIMINAL:
                            $boolIsCriminalScreeningRequest = true;
                            $arrmixApplicantCriminalData               = ( true == valArr( $arrmixApplicantScreeningData ) && true == array_key_exists( 'ScreeningApplicantCriminalRecords', $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantCriminalRecords'] : array();
                            $objStdScreenTypeResult->CriminalRecords   = ( true == valArr( $arrmixApplicantCriminalData ) ) ? rekeyArray( 'offense_id', $arrmixApplicantCriminalData ) : array();
                            if( true == $boolAllUnfilteredDataRequired ) {
                                $arrmixApplicantAllCriminalData                  = ( true == valArr( $arrmixApplicantScreeningData ) && true == array_key_exists( 'ScreeningApplicantAllCriminalRecords', $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantAllCriminalRecords'] : array();
                                $objStdScreenTypeResult->AllCriminalRecords      = ( true == valArr( $arrmixApplicantAllCriminalData ) ) ? rekeyArray( 'id', $arrmixApplicantAllCriminalData ) : array();
                            }
                            $arrmixCriminalConditionSetValues = rekeyArray( 'condition_id', ( array ) CScreeningPackageConditionSets::fetchCustomScreeningPackageConditionSetsByScreeningPackageIdByCid( $objScreeningApplicant->getScreeningPackageId(), $this->getCid(), $objScreeningDatabase ) );
                            $objStdScreenTypeResult->CriminalConditionSetValues   = $arrmixCriminalConditionSetValues;

                            $objStdManualCriminalResult = new stdClass();

                            $objStdManualCriminalResult->criminalSearchType = 'Criminal National';
                            $objStdManualCriminalResult->searchType = '';
                            $objStdManualCriminalResult->recommendationTypeId = $objScreeningTransaction->getScreeningRecommendationTypeId();

                            $arrmixManualCriminalSearchResult['Criminal National'][$objScreeningTransaction->getId()] = $objStdManualCriminalResult;
                            $arrintScreeningApplicantManualCriminalSearchIds[$objScreeningApplicant->getId()][$objScreeningTransaction->getId()] = $objScreeningTransaction->getScreenTypeId();
	                        $arrmixCriminalSearchScope[] = 'National';
                            break;

                        case CScreenType::STATE_CRIMINAL_SEARCH:
                            $objStdManualCriminalResult = new stdClass();

                            $objStdManualCriminalResult->criminalSearchType = 'Criminal State';
                            $objStdManualCriminalResult->searchType = $objScreeningTransaction->getSearchType();
                            $objStdManualCriminalResult->recommendationTypeId = $objScreeningTransaction->getScreeningRecommendationTypeId();
                            $arrmixManualCriminalSearchResult['Criminal State'][$objScreeningTransaction->getId()] = $objStdManualCriminalResult;
                            $arrintScreeningApplicantManualCriminalSearchIds[$objScreeningApplicant->getId()][$objScreeningTransaction->getId()] = $objScreeningTransaction->getScreenTypeId();
	                        if( true == in_array( $objScreeningTransaction->getScreeningDataProviderTypeId(), [ CScreeningDataProviderType::COCOURT, CScreeningDataProviderType::COCOURT_TEST ] ) ) {
		                        $arrmixCriminalSearchScope[] = 'Co-Court : ' . $objScreeningTransaction->getSearchType();
		                    } else {
		                           $arrmixCriminalSearchScope[] = 'State';
		                    }

	                        if( false == $boolIsManualSearchRequestPending && ( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN == $objScreeningTransaction->getScreeningStatusTypeId() &&
	                                                                            CScreeningRecommendationType::PENDING_UNKNOWN == $objScreeningTransaction->getScreeningRecommendationTypeId() ) ) {
		                        $boolIsManualSearchRequestPending = true;
	                        }
							break;

                        case CScreenType::COUNTY_CRIMINAL_SEARCH:
                            $objStdManualCriminalResult = new stdClass();

                            $objStdManualCriminalResult->criminalSearchType = 'Criminal County';
                            $objStdManualCriminalResult->searchType = ( false == is_null( $objScreeningTransaction->getSearchType() ) ) ? current( explode( '~', $objScreeningTransaction->getSearchType() ) ) : NULL;
                            $objStdManualCriminalResult->recommendationTypeId = $objScreeningTransaction->getScreeningRecommendationTypeId();
                            $arrmixManualCriminalSearchResult['Criminal County'][$objScreeningTransaction->getId()] = $objStdManualCriminalResult;
                            $arrintScreeningApplicantManualCriminalSearchIds[$objScreeningApplicant->getId()][$objScreeningTransaction->getId()] = $objScreeningTransaction->getScreenTypeId();
	                        $arrmixCriminalSearchScope[] = 'County';
	                        if( false == $boolIsManualSearchRequestPending && ( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN == $objScreeningTransaction->getScreeningStatusTypeId() &&
	                                                                            CScreeningRecommendationType::PENDING_UNKNOWN == $objScreeningTransaction->getScreeningRecommendationTypeId() ) ) {
		                        $boolIsManualSearchRequestPending = true;
	                        }
                            break;

                        case CScreenType::EVICTION:
                            $boolIsEvictionScreeningRequest = true;
                            $arrmixApplicantEvictionData             = ( true == valArr( $arrmixApplicantScreeningData ) && true == array_key_exists( 'ScreeningApplicantEvictionRecords', $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantEvictionRecords'] : array();
                            $objStdScreenTypeResult->EvictionRecords = ( true == valArr( $arrmixApplicantEvictionData ) ) ? rekeyArray( 'id', $arrmixApplicantEvictionData ) : array();
                            if( true == $boolAllUnfilteredDataRequired ) {
                                $arrmixApplicantAllEvictionData             = ( true == valArr( $arrmixApplicantScreeningData ) && true == array_key_exists( 'ScreeningApplicantAllEvictionRecords', $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantAllEvictionRecords'] : array();
                                $objStdScreenTypeResult->AllEvictionRecords = ( true == valArr( $arrmixApplicantAllEvictionData ) ) ? rekeyArray( 'id', $arrmixApplicantAllEvictionData ) : array();
                            }

                            $arrmixEvictionConditionSetValues = rekeyArray( 'condition_id', ( array ) CScreeningPackageConditionSets::fetchCustomScreeningPackageConditionSetsByScreeningPackageIdByCid( $objScreeningApplicant->getScreeningPackageId(), $this->getCid(), $objScreeningDatabase ) );
                            $objStdScreenTypeResult->EvictionConditionSetValues   = $arrmixEvictionConditionSetValues;
                            break;

                        case CScreenType::RENTAL_HISTORY:
                            $boolIsRentalHistoryScreeningRequest = true;

                            $arrmixApplicantRentalHistoryData    = ( true == valArr( $arrmixApplicantScreeningData ) && true == array_key_exists( 'ScreeningApplicantRentalHistoryValues', $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantRentalHistoryValues'] : array();
                            $objStdScreenTypeResult->RentalHistoryRecords = ( true == valArr( $arrmixApplicantRentalHistoryData ) ) ? rekeyArray( 'id', $arrmixApplicantRentalHistoryData ) : array();

                            $arrmixRentalHistoryConditionSetValues = rekeyArray( 'condition_id', ( array ) CScreeningPackageConditionSets::fetchCustomScreeningPackageConditionSetsByScreeningPackageIdByCid( $objScreeningApplicant->getScreeningPackageId(), $this->getCid(), $objScreeningDatabase ) );
                            $objStdScreenTypeResult->RentalHistoryConditionSetValues = $arrmixRentalHistoryConditionSetValues;
                            break;

                        case CScreenType::CREDIT:
                            // fetch credit attributes, credit accounts, pub records, credit factors, alerts
                            $boolIsCreditScreeningRequest = true;

                            $arrintScreenTypes = ( true == is_null( $objScreeningApplicant->getScreeningPackageId() ) ) ? array( CScreenType::CREDIT, CScreenType::INCOME ) : array( CScreenType::CREDIT );

                            $boolIsScreeningWithNewSetting  = ( true == valObj( $objScreeningPackage, 'CScreeningPackage' ) && false == is_null( $objScreeningApplicant->getScreeningPackageId() ) ) ? true : false;

                            $arrmixCreditValues             = ( true == valArr( $arrmixApplicantScreeningData ) && true == array_key_exists( 'ScreeningApplicantCreditValues', $arrmixApplicantScreeningData ) && true == valArr( $arrmixApplicantScreeningData['ScreeningApplicantCreditValues'] ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantCreditValues'] : array();

                            $arrmixRekeyedCreditValues      = ( true == valArr( $arrmixCreditValues ) ) ? rekeyArray( 'screening_config_preference_type_id', $arrmixCreditValues ) : array();

                            $arrmixCreditConditionSetValues = rekeyArray( 'condition_id', ( array ) CScreeningConditionSets::fetchCustomScreeningConditionSetsByScreenTypeIdByScreeningPackageId( CScreenType::CREDIT, $objScreeningApplicant->getScreeningPackageId(), $objScreeningDatabase ) );
	                        $objStdScreenTypeResult->creditScreeningCompletedOn = $objScreeningTransaction->getScreeningCompletedOn();
                            $objStdScreenTypeResult->CreditConditionSetValues   = $arrmixCreditConditionSetValues;
                            $arrintCreditResultConfigTypeIds                    = array( CScreeningConfigPreferenceType::CREDIT_SCORE );

                            if( true == valArr( $arrmixRekeyedCreditValues ) ) {

                                if( true == valObj( $objScreeningPackage, 'CScreeningPackage' ) && false == is_null( $objScreeningApplicant->getScreeningPackageId() ) ) {

                                    if( true == array_key_exists( CScreeningConfigPreferenceType::BANKRUPTCY_RECENT_MONTHS, $arrmixRekeyedCreditValues ) ) {

                                        $strBankruptcyResult        = '';
                                        $intBankruptcyRecentMonths  = $arrmixRekeyedCreditValues[CScreeningConfigPreferenceType::BANKRUPTCY_RECENT_MONTHS]['value_filtered'];
                                        $intBankruptcyYear          = ( 0 < $intBankruptcyRecentMonths ) ? round( $intBankruptcyRecentMonths / 12 ) : NULL;
                                        $strBankruptcyResult        = 'Record found within ' . ( int ) $intBankruptcyYear . ' years';

                                        $arrmixRekeyedCreditValues[CScreeningConfigPreferenceType::BANKRUPTCY_RECENT_MONTHS]['value_filtered'] = $strBankruptcyResult;
                                    }

                                    if( true == array_key_exists( CScreeningConfigPreferenceType::FORECLOSURE_RECENT_MONTHS, $arrmixRekeyedCreditValues ) ) {

                                        $strForeClosureResult        = '';
                                        $intForclosureRecentMonths   = $arrmixRekeyedCreditValues[CScreeningConfigPreferenceType::FORECLOSURE_RECENT_MONTHS]['value_filtered'];
                                        $intForeClosureYear          = ( 0 < $intForclosureRecentMonths ) ? round( $intForclosureRecentMonths / 12 ) : NULL;
                                        $strForeClosureResult        = 'Record found within ' . ( int ) $intForeClosureYear . ' years';

                                        $arrmixRekeyedCreditValues[CScreeningConfigPreferenceType::FORECLOSURE_RECENT_MONTHS]['value_filtered'] = $strForeClosureResult;
                                    }

                                    array_push( $arrintCreditResultConfigTypeIds, CScreeningConfigPreferenceType::BANKRUPTCY_RECENT_MONTHS,
                                                                                  CScreeningConfigPreferenceType::COLLECTION_PERCENTAGE,
                                                                                  CScreeningConfigPreferenceType::COLLECTIONS,
                                                                                  CScreeningConfigPreferenceType::DELINQUENT_ACCOUNTS,
                                                                                  CScreeningConfigPreferenceType::UTILITY_COLLECTIONS,
                                                                                  CScreeningConfigPreferenceType::UTILITY_COLLECTION_DOLLARS,
                                                                                  CScreeningConfigPreferenceType::RENTAL_COLLECTIONS,
                                                                                  CScreeningConfigPreferenceType::RENTAL_COLLECTION_DOLLARS,
                                                                                  CScreeningConfigPreferenceType::MEDICAL_COLLECTIONS,
                                                                                  CScreeningConfigPreferenceType::MEDICAL_COLLECTIONS_DOLLAR_AMOUNT,
                                                                                  CScreeningConfigPreferenceType::DELINQUENT_ACCOUNTS_PERCENT,
                                                                                  CScreeningConfigPreferenceType::DEBT_TO_INCOME,
                                                                                  CScreeningConfigPreferenceType::DEBT_WITH_RENT_TO_INCOME,
                                                                                  CScreeningConfigPreferenceType::CREDIT_SCORE_MODEL_TYPE,
                                                                                  CScreeningConfigPreferenceType::TOTAL_ACCOUNT,
                                                                                  CScreeningConfigPreferenceType::PAST_DELINQUENT,
                                                                                  CScreeningConfigPreferenceType::POSITIVE_ACCOUNTS,
                                                                                  CScreeningConfigPreferenceType::CURRENT_DELINQUENT,
                                                                                  CScreeningConfigPreferenceType::PUBLIC_RECORDS,
                                                                                  CScreeningConfigPreferenceType::PUBLIC_RECORDS_DOLLARS,
                                                                                  CScreeningConfigPreferenceType::FORECLOSURE_RECENT_MONTHS );

                                    if( true == $boolAllUnfilteredDataRequired ) {
                                        array_push( $arrintCreditResultConfigTypeIds, CScreeningConfigPreferenceType::NO_CREDIT_RECORD,
                                        CScreeningConfigPreferenceType::NO_CREDIT_SCORE,
                                        CScreeningConfigPreferenceType::CREDIT_ALERT_FACTA );
                                    }

                                    if( false == is_null( $objScreeningPackage->getIsUseForRvIndexScore() ) && 0 < $objScreeningPackage->getIsUseForRvIndexScore() ) {
                                        $arrintCreditRatioTypeIds = array(
                                            CScreeningConfigPreferenceType::DEBT_TO_INCOME,
                                            CScreeningConfigPreferenceType::DEBT_WITH_RENT_TO_INCOME,
                                            CScreeningConfigPreferenceType::INCOME_TO_RENT
                                        );

                                        $arrintCreditResultConfigTypeIds = array_diff( $arrintCreditResultConfigTypeIds, $arrintCreditRatioTypeIds );
                                        array_push( $arrintCreditResultConfigTypeIds, CScreeningConfigPreferenceType::RV_INDEX_SCORE );
                                    }

                                    if( true == is_null( $objScreeningApplicant->getTaxNumberLastFour() ) ) {
                                        $arrintCreditResultConfigTypeIds = array(  CScreeningConfigPreferenceType::NO_SSN );
                                    }
                                } else {
                                    array_push( $arrintCreditResultConfigTypeIds, CScreeningConfigPreferenceType::INCOME_TO_RENT );
                                }
                            }

                            $boolIsRentalCollectionRequest = ( true == valArr( $arrintRentalCollectionEnabledScreeningPackages ) && true == valObj( $objScreeningPackage, 'CScreeningPackage' ) && true == in_array( $objScreeningPackage->getId(), $arrintRentalCollectionEnabledScreeningPackages ) ) ? true : false;

                            // Decision for RV Index display
                            $boolIsRvIndexOnForApplication = ( true == valObj( $objScreeningPackage, 'CScreeningPackage' ) && 0 < $objScreeningPackage->getIsUseForRvIndexScore() ) ? true : false;

                            $arrmixCreditResult                          = ( true == valArr( $arrintCreditResultConfigTypeIds ) ) ? array_intersect_key( $arrmixRekeyedCreditValues, array_flip( $arrintCreditResultConfigTypeIds ) ) : $arrmixRekeyedCreditValues;

                            $arrmixCreditConditionMinMaxValues = CScreeningPackageSettings::fetchScreeningPackageSettingsMinMaxDataByScreeningConfigPreferenceTypeIdsByScreeningPackageId( array_keys( $arrmixRekeyedCreditValues ), $objScreeningApplicant->getScreeningPackageId(), $objScreeningDatabase );

                            $arrmixRekeyCreditConditionMinMaxValues      = rekeyArray( 'screening_config_preference_type_id', $arrmixCreditConditionMinMaxValues );

                            $objStdScreenTypeResult->CreditConditionMinMaxValues    = $arrmixRekeyCreditConditionMinMaxValues;

                            $arrmixScreeningApplicantCreditValues = ( true == $boolAllUnfilteredDataRequired && true == valArr( $arrmixCreditResult ) ) ? rekeyArray( 'id', $arrmixCreditResult ) : $arrmixCreditResult;

                            if( false == is_null( $objScreeningApplicant->getScreeningPackageId() ) && true == valArr( $arrmixScreeningApplicantCreditValues ) ) {
                                $arrmixScreeningApplicantCreditValues = $this->mapAndSetScreeningRecommendationTypeId( $objScreeningApplicant->getCid(), $arrmixScreeningApplicantCreditValues, $objScreeningDatabase );
                            }

                            $objStdScreenTypeResult->CreditValues        = $arrmixScreeningApplicantCreditValues;

                            $objStdScreenTypeResult->CreditAccounts      = ( true == valArr( $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantLiabilityAccounts'] : array();

                            $objStdScreenTypeResult->CreditPublicRecords = ( true == valArr( $arrmixApplicantScreeningData ) ) ?$arrmixApplicantScreeningData['ScreeningApplicantGovRecords'] : array();
                            $arrmixCreditFactors       = ( true == valArr( $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantCreditFactors'] : array();
                            $arrmixCreditFactaAlerts   = ( true == valArr( $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantCreditFactaAlerts'] : array();
                            $arrstrCreditFactors = array();

                            if( true == valArr( $arrmixCreditFactors ) ) {
                                foreach( $arrmixCreditFactors as $arrmixCreditFactor ) {
                                    $arrstrCreditFactors[$arrmixCreditFactor['factor_code']] = $arrmixCreditFactor['factor_description'];
                                }
                            }

                            $objStdScreenTypeResult->CreditFactors                   = ( true == valArr( $arrstrCreditFactors ) ) ? $arrstrCreditFactors : array();
                            $objStdScreenTypeResult->CreditFactaAlerts               = $arrmixCreditFactaAlerts;
                            $objStdScreenTypeResult->CreditAlerts                    = ( true == valArr( $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantCreditAlerts'] : array();
                            $objStdScreenTypeResult->CreditTradelineSummery          = ( true == valArr( $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantCreditTradelineSummary'] : array();
                            $objStdScreenTypeResult->CreditTradelineLateCountHistory = ( true == valArr( $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantCreditTradelineLateCountHistory']: array();
                            $objStdScreenTypeResult->CreditApplicantInquiries        = ( true == valArr( $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantCreditInquiries'] : array();
                            $objStdScreenTypeResult->CreditApplicantAddresses        = ( true == valArr( $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantAddresses'] : array();
                            $objStdScreenTypeResult->CreditApplicantEmployers        = ( true == valArr( $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantEmployer'] : array();
                            break;

                        case CScreenType::BUSINESS_CREDIT:
                            $boolIsBusinessCreditScreeningRequest = true;
                            break;

                        case CScreenType::BUSINESS_PREMIER_PROFILE:
                            $boolIsBusinessPremierProfileScreeningRequest = true;
                            break;

                        case CScreenType::DO_NOT_RENT:
                            $boolIsDoNotRentScreeningRequest                        = true;

                            $arrmixDoNotRentConditionSetValues                      = rekeyArray( 'condition_id', ( array ) CScreeningPackageConditionSets::fetchCustomScreeningPackageConditionSetsByScreeningPackageIdByCid( $objScreeningApplicant->getScreeningPackageId(), $this->getCid(), $objScreeningDatabase ) );
                            $objStdScreenTypeResult->DoNotRentValues                = array();
                            $objStdScreenTypeResult->DoNotRentConditionSetValues    = $arrmixDoNotRentConditionSetValues;
                            break;

	                    case CScreenType::PASSPORT_VISA_VERIFICATION:
		                    $boolIsPassportVerification = true;
	                    	break;

                        case CScreenType::PRECISE_ID:
                            $boolIsPreciseIdScreeningRequest                        = true;
                            break;

						case CScreenType::ATTRIBUTE_BASED:
	                    	$boolIsAttributeBased = true;

                        default:
                             // need to add comment
                            break;
                    }

                    $objStdScreeningApplicantDetails->ScreenResults[$objScreeningTransaction->getScreenTypeId()] = $objStdScreenTypeResult;
                    $objStdScreeningApplicantDetails->ScreenTypeTotalResultValues = ( true == valArr( $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantMixScreenTypeResultValues'] : array();
                    if( $objStdScreeningApplicantDetails->is_conditional_for_rcpp == false ) {
                        $objStdScreeningApplicantDetails->is_conditional_for_rcpp = ( ( true == isset( $objScreeningTransaction->getAdditionalDetails()->{ CScreeningJsonDataType::BANKRUPTCY_AND_RENTAL_COLLECTIONS }->{ CScreeningConfigPreferenceType::RENTAL_COLLECTIONS } ) ) || ( true == isset( $objScreeningTransaction->getAdditionalDetails()->{ CScreeningJsonDataType::BANKRUPTCY_AND_RENTAL_COLLECTIONS }->{ CScreeningConfigPreferenceType::RENTAL_COLLECTION_DOLLARS } ) ) ) ? true : false;
	                }
                }

                // Set Criminal Recommendation.....
                if( true == array_key_exists( CScreenType::CRIMINAL, $objStdScreeningApplicantDetails->ScreenResults ) ) {
                    $objStdScreeningApplicantDetails->ScreenResults[CScreenType::CRIMINAL]->RecommendationTypeId = $this->determineCriminalRecommendation( $arrobjScreeningTransactions );
	                $objStdScreeningApplicantDetails->ScreenResults[CScreenType::CRIMINAL]->SearchScope          = ( true == valArr( $arrmixCriminalSearchScope ) ) ? implode( ', ', array_unique( $arrmixCriminalSearchScope ) ) : NULL;
                }

                $objStdScreeningApplicantDetails->screeningApplicantTransactionIds = new stdClass();

   		        $arrintScreeningApplicantTransactionIds = array();
	            $arrmixCriminalSearchScope              = [];

	            $arrintApplicantCompletedTransactionIds = array();
	            $arrintScreeningCompletedScreenTypeIds  = array();

	            $objStdScreeningApplicantDetails->screeningCompletedScreenTypeIds  = ( true == valArr( $arrintScreeningCompletedScreenTypeIds ) ) ? json_encode( $arrintScreeningCompletedScreenTypeIds ) : '';

                // Set Criminal Recommendation.....
                if( true == array_key_exists( CScreenType::CRIMINAL, $objStdScreeningApplicantDetails->ScreenResults ) && ( true == array_key_exists( CScreenType::STATE_CRIMINAL_SEARCH, $objStdScreeningApplicantDetails->ScreenResults ) || true == array_key_exists( CScreenType::COUNTY_CRIMINAL_SEARCH, $objStdScreeningApplicantDetails->ScreenResults ) ) ) {
                    $objStdScreeningApplicantDetails->manualCriminalResult = new stdClass();
                    $objStdScreeningApplicantDetails->manualCriminalResult = $arrmixManualCriminalSearchResult;
	                $objStdScreeningApplicantDetails->manualSearchRequestPending = $boolIsManualSearchRequestPending;

                    $objStdScreeningApplicantDetails->ScreenResults[CScreenType::CRIMINAL]->RecommendationTypeId = $this->determineCriminalRecommendation( $arrobjScreeningTransactions );
                }

	            $objStdScreeningApplicantDetails->isForceResubmit = ( true == $boolIsForceResubmit ) ? true :false;
			    $intScreeningVendorId = ( $intNullDataProviderCnt > 0 ) ? CScreeningApplicationRequest::VERSION_1B : CTransmissionVendor::RESIDENT_VERIFY;
            }

            $objStdRequestedScreenTypes = new stdClass();

            $objStdRequestedScreenTypes->isCreditScreeningRequest                   = $boolIsCreditScreeningRequest;
            $objStdRequestedScreenTypes->isCriminalScreeningRequest                 = $boolIsCriminalScreeningRequest;
            $objStdRequestedScreenTypes->isEvictionScreeningRequest                 = $boolIsEvictionScreeningRequest;
            $objStdRequestedScreenTypes->isIncomeScreeningRequest                   = $boolIsIncomeScreeningRequest;
		    $objStdRequestedScreenTypes->isVerificationOfIncomeRequest              = $boolIsVerificationOfIncomeRequest;
            $objStdRequestedScreenTypes->isDoNotRentScreeningRequest                = $boolIsDoNotRentScreeningRequest;
            $objStdRequestedScreenTypes->isBusinessCreditScreeningRequest           = $boolIsBusinessCreditScreeningRequest;
            $objStdRequestedScreenTypes->isBusinessPremierProfileScreeningRequest   = $boolIsBusinessPremierProfileScreeningRequest;
            $objStdRequestedScreenTypes->isRentalHistoryScreeningRequest            = $boolIsRentalHistoryScreeningRequest;
            $objStdRequestedScreenTypes->isStateCriminalSearch                      = $boolIsStateCriminalSearch;
            $objStdRequestedScreenTypes->isCountyCriminalSearch                     = $boolIsCountyCriminalSearch;
	        $objStdRequestedScreenTypes->isPassportVerification                     = $boolIsPassportVerification;
	        $objStdRequestedScreenTypes->isRentalCollectionRequest                  = $boolIsRentalCollectionRequest;
            $objStdRequestedScreenTypes->isPreciseIdScreeningRequest                = $boolIsPreciseIdScreeningRequest;
			$objStdRequestedScreenTypes->isAttributeBased                           = $boolIsAttributeBased;

            if( false == $boolIsAllowRecalculateResult && ( true == $boolIsCreditScreeningRequest || true == $boolIsIncomeScreeningRequest ) ) {
                $boolIsAllowRecalculateResult = true;
            }

            if( true == valArr( $objStdScreeningApplicantDetails->ScreenResults ) ) {
                $objStdScreeningApplicantDetails->ScreenResults['requestedScreenTypes'] = $objStdRequestedScreenTypes;
            }

            if( true == $boolIsBusinessCreditScreeningRequest ) {
                $arrmixApplicationDenialReasons[CScreenType::BUSINESS_CREDIT]['ScreenTypeName'] = $arrobjScreenTypes[CScreenType::BUSINESS_CREDIT]->getName();
            }

            if( true == $boolIsBusinessPremierProfileScreeningRequest ) {
                $arrmixApplicationDenialReasons[CScreenType::BUSINESS_PREMIER_PROFILE]['ScreenTypeName'] = $arrobjScreenTypes[CScreenType::BUSINESS_PREMIER_PROFILE]->getName();
            }

            if( true == $boolIsDoNotRentScreeningRequest ) {
                $arrmixApplicationDenialReasons[CScreenType::DO_NOT_RENT]['ScreenTypeName'] = $arrobjScreenTypes[CScreenType::DO_NOT_RENT]->getName();
            }

            if( true == $boolIsPreciseIdScreeningRequest ) {
                $arrmixApplicationDenialReasons[CScreenType::PRECISE_ID]['ScreenTypeName'] = $arrobjScreenTypes[CScreenType::PRECISE_ID]->getName();
            }

            if( CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR == $objScreeningApplicant->getStatusTypeId() ) {
                $objStdScreeningApplicantDetails->RecommendationTypeId = CScreeningRecommendationType::ERROR;
            } elseif( true == in_array( $objScreeningApplicant->getStatusTypeId(), array( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN, CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW, CScreeningStatusType::APPLICANT_RECORD_STATUS_ID_VERIFICATION_PENDING ) ) ) {
                $objStdScreeningApplicantDetails->RecommendationTypeId = CScreeningRecommendationType::PENDING_UNKNOWN;
            } elseif( 0 < $intApplicantFailedCounts ) {
                $objStdScreeningApplicantDetails->RecommendationTypeId = CScreeningRecommendationType::FAIL;
            } elseif( 0 < $intApplicantConditionalCounts ) {
                $objStdScreeningApplicantDetails->RecommendationTypeId = CScreeningRecommendationType::PASSWITHCONDITIONS;
            } else {
                $objStdScreeningApplicantDetails->RecommendationTypeId = CScreeningRecommendationType::PASS;
            }

            $objStdScreeningApplicantDetails->Recommendation = CResidentVerifyServiceUtil::getScreeningRecommendationText( $objScreeningApplicant->getStatusTypeId(), $objStdScreeningApplicantDetails->RecommendationTypeId );

				$objStdScreeningResult->ScreeningApplicants[$objScreeningApplicant->getCustomApplicantId()] = $objStdScreeningApplicantDetails;
		}

        $objStdScreeningResult->isApplicationPreScreened        = $boolIsApplicationPreScreened;
        $objStdScreeningResult->ScreenTypeIds                   = array_unique( $arrintScreenTypeIds );
        $objStdScreeningResult->screeningTransactionIds         = array_unique( $arrintScreeningTransactionIds );
        $objStdScreeningResult->isAllowRecalculateResult        = $boolIsAllowRecalculateResult;
	    $objStdScreeningResult->isEnableSkippTransaction        = ( 0 >= ( int ) $this->checkSkipScreeningTransactions( array_keys( rekeyObjects( 'Id', $arrobjScreeningApplicants ) ), $objScreeningDatabase ) ) ? false : true;

        $objStdScreeningResult->ApplicantManualCriminalScreenTypeIds = json_encode( $arrintScreeningApplicantManualCriminalSearchIds );

	    $arrmixClientCriminalClassificationSubtypes = [];
	    if( true == in_array( CScreenType::CRIMINAL, $objStdScreeningResult->ScreenTypeIds, false ) ) {
		    $arrobjClientCriminalClassificationSubtypes = CClientCriminalClassificationSubtypes::fetchAllCriminalClassificationSubtypesByCid( $this->getCid(), $objScreeningDatabase );
		    $arrmixClientCriminalClassificationSubtypes = extractObjectKeyValuePairs( $arrobjClientCriminalClassificationSubtypes, 'Id', 'SubtypeName' );
	    }

	    $objStdScreeningResult->ClientCriminalClassificationSubtypes = $arrmixClientCriminalClassificationSubtypes;
        $objStdScreeningResult->screeningConditionSets = new stdClass();
        $objStdScreeningResult->screeningConditionSets->screeningApplicationRequestId   = $this->getTransmissionId();
        $objStdScreeningResult->screeningConditionSets->appliedConditionSetIds          = $arrintAppliedScreeningConditionSetIds;
        $objStdScreeningResult->screeningConditionSets->allConditionSetIds              = $arrintScreeningConditionSetIds;

        $arrmixPreparedConditionSetsData = $this->prepareScreeningPackageConditionSetData( $arrmixScreeningConditionSetDetails );
        $objStdScreeningResult->screeningConditionSets->allConditionSetDetails = $arrmixPreparedConditionSetsData;
        $objStdScreeningResult->isCriminalCategorizationEnabled = $objStdScreeningApplicantDetails->applicantScreeningSettings->isCriminalCategorizationEnabled;
        if( true === $boolIsApplicationPreScreened ) $objStdScreeningResult->pendingPreScreeningApplicantIds = $arrintPendingPreScreeningApplicantIds;

        // to show combined rv_index score value only when all applicants screening packages have "rv_index score" enabled otherwise not to be shown.
        $objStdScreeningResult->rvIndexScore = ( false == is_null( $this->getRvIndex() ) && self::SCREENED_WITH_PACKAGE_SETTINGS == $this->getScreeningConfigurationId() && true == $boolIsRvIndexOnForApplication ) ? $this->getRvIndex() : NULL;

        $objStdScreeningResult->DenialReasons = new stdClass();
        $objStdScreeningResult->DenialReasons = $arrmixApplicationDenialReasons;
        $objStdScreeningResult->ScreeningVendorId = $intScreeningVendorId;
	    $objStdScreeningResult->boolIsOnMaintenanace = $boolIsOnMaintenanace;

        return $objStdScreeningResult;
    }

    public function fetchScreeningApplicantResultByScreeningApplicantId( $objScreeningDatabase, $intScreeningApplicantId = 0 ) {
    	$boolIsScreeningReportForInActiveApplicant = true;
		if( 0 == $intScreeningApplicantId ) return false;

    	$objStdScreeningResult = new stdClass();

    	$objStdScreeningResult->ApplicationId   = $this->getCustomApplicationId();
	    $objStdScreeningResult->Rent            = $this->getRent();
	    $objStdScreeningResult->RentCredit      = $this->getRentCredit();
    	$objStdScreeningResult->ReferenceId     = $this->getId();
    	$objStdScreeningResult->StatusTypeId    = $this->getScreeningStatusTypeId();
	    $objStdScreeningResult->IsOutOfScreeningRetentionPeriod  = ( date( 'Y-m-d', strtotime( $this->getCreatedOn() ) ) < date( 'Y-m-d', strtotime( '-' . CScreening::SCREENING_RETENTION_PERIOD . ' years' ) ) ) ? true : false;

    	$objStdScreeningResult->GroupRecommendationTypeId = $this->getScreeningRecommendationTypeId();
    	$boolIsRvIndexOnForApplication = false;

    	$arrobjScreeningDecisionTypes        = CScreeningRecommendationTypes::fetchAllScreeningRecommendationTypes( $objScreeningDatabase );
    	$arrobjRekeyedScreeningDecisionTypes = ( true == valArr( $arrobjScreeningDecisionTypes ) ) ? rekeyObjects( 'id', $arrobjScreeningDecisionTypes ) : array();

    	$objStdScreeningResult->GroupRecommendation = CResidentVerifyServiceUtil::getScreeningRecommendationText( $this->getScreeningStatusTypeId(), $this->getScreeningRecommendationTypeId() );

    	$objScreeningApplicant  = CScreeningApplicants::fetchScreeningApplicantByIdByScreeningIdByCid( $intScreeningApplicantId, $this->getId(), $this->getCid(), $objScreeningDatabase );
   		$arrobjScreeningApplicants  = CScreeningApplicants::fetchActiveScreeningApplicantsByScreeningIdByCid( $this->getId(), $this->getCid(), $objScreeningDatabase );

	    $fltCombineIncome = 0;
	    if( true == valArr( $arrobjScreeningApplicants ) ) {
		    $fltCombineIncome = $this->getTotalHouseholdIncome();
	    }

    	if( false == valObj( $objScreeningApplicant, 'CScreeningApplicant' ) ) {
    		return false;
    	}

    	$objStdScreeningResult->ScreeningApplicants = array();

    	$arrobjScreeningPackages             = CScreeningPackages::fetchAllScreeningPackagesByIdsCid( array( $objScreeningApplicant->getScreeningPackageId() ), $this->getCid(), $objScreeningDatabase );

    	$objStdScreeningResult->combineIncome             = $fltCombineIncome;
    	$objStdScreeningResult->combinedRentToIncomeRatio = ( 0 < $fltCombineIncome && 0 < $this->getRent() ) ? $fltCombineIncome / ( float ) $this->getRent() : NULL;
        $objStdScreeningResult->isSkippedTransaction = \Psi\Eos\Screening\CScreeningTransactions::createService()->fetchIsScreeningTransactionSkippedByScreeningIdByScreeningApplicantIdByCid( $this->getId(), $objScreeningApplicant->getId(), $this->getCid(), $objScreeningDatabase );

    	$arrintScreeningTransactionIds                   = array();
    	$arrintScreeningApplicantManualCriminalSearchIds = array();

   		$arrobjScreeningTransactions    = $objScreeningApplicant->getScreeningApplicantResults( $objScreeningDatabase );

   		$arrobjRekeyedScreeningTransactions    = ( true == valArr( $arrobjScreeningTransactions ) ) ? rekeyObjects( 'ScreenTypeId', $arrobjScreeningTransactions ): array();

    		// 1. Do a rekey objects by screen type id
    		// 2. Get array keys ... which will be screen type ids
    		// 3. Based on the screen type get applicant results

   		$arrmixApplicantScreeningData = array();

   		$objScreeningPackage = NULL;

   		if( false == is_null( $objScreeningApplicant->getScreeningPackageId() ) && true == valArr( $arrobjScreeningPackages ) ) $objScreeningPackage = ( true == array_key_exists( $objScreeningApplicant->getScreeningPackageId(), $arrobjScreeningPackages ) ) ? $arrobjScreeningPackages[$objScreeningApplicant->getScreeningPackageId()] : NULL;

   		$intApplicantFailedCounts       = 0;
   		$intApplicantConditionalCounts  = 0;
   		$intApplicantInprogressCounts   = 0;

   		$boolIsCreditScreeningRequest                   = false;
   		$boolIsCriminalScreeningRequest                 = false;
   		$boolIsEvictionScreeningRequest                 = false;
   		$boolIsIncomeScreeningRequest                   = false;
	    $boolIsVerificationOfIncomeRequest              = false;
   		$boolIsBusinessCreditScreeningRequest           = false;
   		$boolIsBusinessPremierProfileScreeningRequest   = false;
   		$boolIsDoNotRentScreeningRequest                = false;
   		$boolIsPassPortVerification                     = false;
        $boolIsPreciseIdRequest                         = false;
   		$boolIsStateCriminalSearch = false;
   		$boolIsCountyCriminalSearch = false;

   		$objStdScreeningApplicantDetails = new stdClass();

   		$objStdScreeningApplicantDetails->Id             = $objScreeningApplicant->getId();
   		$objStdScreeningApplicantDetails->ApplicantId    = $objScreeningApplicant->getCustomApplicantId();
   		$objStdScreeningApplicantDetails->StatusTypeId   = $objScreeningApplicant->getStatusTypeId();

   		$objStdScreeningApplicantDetails->FirstName                 = $objScreeningApplicant->getNameFirst();
   		$objStdScreeningApplicantDetails->MiddleName                = $objScreeningApplicant->getNameMiddle();
   		$objStdScreeningApplicantDetails->LastName                  = $objScreeningApplicant->getNameLast();
   		$objStdScreeningApplicantDetails->ApplicantIncome           = $objScreeningApplicant->getIncome();
   		$objStdScreeningApplicantDetails->ApplicantSSN              = $objScreeningApplicant->getTaxNumberLastFour();
   		$objStdScreeningApplicantDetails->ScreeningApplicantTypeId  = $objScreeningApplicant->getScreeningApplicantTypeId();
   		$objStdScreeningApplicantDetails->ScreeningInitiatedOn      = $objScreeningApplicant->getCreatedOn();
   		$objStdScreeningApplicantDetails->ApplicantPackageName      = ( true == valObj( $objScreeningPackage, 'CScreeningPackage' ) ) ? $objScreeningPackage->getName() : '';
   		$objStdScreeningApplicantDetails->ApplicantBirthDate        = base64_decode( $objScreeningApplicant->getBirthDateEncrypted() );
   		$objStdScreeningApplicantDetails->ApplicantAddress          = $objScreeningApplicant->getAddressLine();
   		$objStdScreeningApplicantDetails->ApplicantCity             = $objScreeningApplicant->getCity();
   		$objStdScreeningApplicantDetails->ApplicantState            = $objScreeningApplicant->getState();
   		$objStdScreeningApplicantDetails->ApplicantZipCode          = $objScreeningApplicant->getZipCode();
   		$objStdScreeningApplicantDetails->ApplicantStatusTypeId     = $objScreeningApplicant->getStatusTypeId();
	    $objStdScreeningApplicantDetails->ScreeningPackageId        = $objScreeningApplicant->getScreeningPackageId();

   		// adding applicant wise screening settings to object
   		$objStdScreeningApplicantConfigurationDetails = new stdClass();

   		$objStdScreeningApplicantConfigurationDetails->isCriminalCategorizationEnabled = ( true == valArr( $arrobjRekeyedScreeningTransactions ) && true == array_key_exists( CScreenType::CRIMINAL_CATEGORIZATION, $arrobjRekeyedScreeningTransactions ) ) ? true : false;
   		$objStdScreeningApplicantDetails->applicantScreeningSettings                   = $objStdScreeningApplicantConfigurationDetails;

   		$objStdScreeningApplicantDetails->ScreenResults = array();

   		$intNullDataProviderCnt = 0;

   		$arrmixManualCriminalSearchResult       = array();
   		$arrintScreeningApplicantTransactionIds = array();

	    $arrintApplicantCompletedTransactionIds = array();
	    $arrintScreeningCompletedScreenTypeIds  = array();

	    $intScreeningVendorId = CTransmissionVendor::RESIDENT_VERIFY;

	    $arrintScreenTypeIds  = array();

   		if( true == valArr( $arrobjScreeningTransactions ) ) {

   			foreach( $arrobjScreeningTransactions as $objScreeningTransaction ) {

			    $arrintScreenTypeIds[]                    = $objScreeningTransaction->getScreenTypeId();
   				$arrintScreeningTransactionIds[]          = $objScreeningTransaction->getId();
   				$arrintScreeningApplicantTransactionIds[] = $objScreeningTransaction->getId();

   				$objStdScreenTypeResult  = new stdClass();
   				$objStdScreenTypeResult->RecommendationTypeId   = $objScreeningTransaction->getScreeningRecommendationTypeId();

			    if( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED == $objScreeningTransaction->getScreeningStatusTypeId() ) {
				    $arrintApplicantCompletedTransactionIds[] = $objScreeningTransaction->getId();
				    $arrintScreeningCompletedScreenTypeIds[] = $objScreeningTransaction->getScreenTypeId();
                    array_push( $arrintScreeningCompletedScreenTypeIds, CSCreenType::PRECISE_ID );
			    }

   				if( true == is_null( $objScreeningTransaction->getScreeningDataProviderId() ) ) {
   					$intNullDataProviderCnt += 1;
   				}

   				switch( $objScreeningTransaction->getScreeningRecommendationTypeId() ) {
   					case CScreeningRecommendationType::FAIL:
   						$intApplicantFailedCounts += 1;
   						break;

   					case CScreeningRecommendationType::PASSWITHCONDITIONS:
   						$intApplicantConditionalCounts += 1;
   						break;

   					default:
   						// need to add comment
   				}

   				switch( $objScreeningTransaction->getScreenTypeId() ) {
   					case CScreenType::INCOME:
   						$boolIsIncomeScreeningRequest = true;
					    $arrmixIncomeConditionSetValues = rekeyArray( 'condition_id', ( array ) CScreeningConditionSets::fetchCustomScreeningConditionSetsByScreenTypeIdByScreeningPackageId( CScreenType::INCOME, $objScreeningApplicant->getScreeningPackageId(), $objScreeningDatabase ) );

					    $objStdScreenTypeResult->IncomeConditionSetValues   = $arrmixIncomeConditionSetValues;
   						break;

				    case CScreenType::VERIFICATION_OF_INCOME:
					    $boolIsVerificationOfIncomeRequest = true;

					    $objStdScreenTypeResult->VerificationOfIncomeConditionSetValues = rekeyArray( 'condition_id', ( array ) CScreeningConditionSets::fetchCustomScreeningConditionSetsByScreenTypeIdByScreeningPackageId( CScreenType::VERIFICATION_OF_INCOME, $objScreeningApplicant->getScreeningPackageId(), $objScreeningDatabase ) );
					    break;


				    case CScreenType::CRIMINAL:
   						$boolIsCriminalScreeningRequest = true;
					    $arrmixCriminalSearchScope[] = 'National';
   						break;

   					case CScreenType::STATE_CRIMINAL_SEARCH:
   						$objStdManualCriminalResult = new stdClass();

   						$objStdManualCriminalResult->criminalSearchType = 'Criminal State';
   						$objStdManualCriminalResult->searchType = $objScreeningTransaction->getSearchType();
   						$objStdManualCriminalResult->recommendationTypeId = $objScreeningTransaction->getScreeningRecommendationTypeId();
   						$arrmixManualCriminalSearchResult['Criminal State'][$objScreeningTransaction->getId()] = $objStdManualCriminalResult;
   						$arrintScreeningApplicantManualCriminalSearchIds[$objScreeningApplicant->getId()][$objScreeningTransaction->getScreenTypeId()] = $objScreeningTransaction->getScreenTypeId();
					    if( true == in_array( $objScreeningTransaction->getScreeningDataProviderTypeId(), [ CScreeningDataProviderType::COCOURT, CScreeningDataProviderType::COCOURT_TEST ] ) ) {
						    $arrmixCriminalSearchScope[] = 'Co-Court : ' . $objScreeningTransaction->getSearchType();
					    } else {
						    $arrmixCriminalSearchScope[] = 'State';
					    }
   						break;

   					case CScreenType::COUNTY_CRIMINAL_SEARCH:
   						$objStdManualCriminalResult = new stdClass();

   						$objStdManualCriminalResult->criminalSearchType = 'Criminal County';
   						$objStdManualCriminalResult->searchType = ( false == is_null( $objScreeningTransaction->getSearchType() ) ) ? current( explode( '~', $objScreeningTransaction->getSearchType() ) ) : NULL;
   						$objStdManualCriminalResult->recommendationTypeId = $objScreeningTransaction->getScreeningRecommendationTypeId();
   						$arrmixManualCriminalSearchResult['Criminal County'][$objScreeningTransaction->getId()] = $objStdManualCriminalResult;
   						$arrintScreeningApplicantManualCriminalSearchIds[$objScreeningApplicant->getId()][$objScreeningTransaction->getScreenTypeId()] = $objScreeningTransaction->getScreenTypeId();
					    $arrmixCriminalSearchScope[] = 'County';
   						break;

   					case CScreenType::EVICTION:
   						$boolIsEvictionScreeningRequest = true;
   						break;

   					case CScreenType::RENTAL_HISTORY:
   						$boolIsRentalHistoryScreeningRequest = true;
                        $arrmixRentalHistoryConditionSetValues = rekeyArray( 'condition_id', ( array ) CScreeningConditionSets::fetchCustomScreeningConditionSetsByScreenTypeIdByScreeningPackageId( CScreenType::RENTAL_HISTORY, $objScreeningApplicant->getScreeningPackageId(), $objScreeningDatabase ) );
                        $objStdScreenTypeResult->RentalHistoryConditionSetValues   = $arrmixRentalHistoryConditionSetValues;
   						break;

   					case CScreenType::CREDIT:
   						// fetch credit attributes, credit accounts, pub records, credit factors, alerts
   						$boolIsCreditScreeningRequest = true;

					    $arrmixCreditConditionSetValues                    = rekeyArray( 'condition_id', ( array ) CScreeningConditionSets::fetchCustomScreeningConditionSetsByScreenTypeIdByScreeningPackageId( CScreenType::CREDIT, $objScreeningApplicant->getScreeningPackageId(), $objScreeningDatabase ) );
					    $objStdScreenTypeResult->CreditConditionSetValues  = $arrmixCreditConditionSetValues;
					    $objStdScreenTypeResult->ScreeningDataProviderTypeId = $objScreeningTransaction->getScreeningDataProviderTypeId();
   						break;

   					case CScreenType::BUSINESS_CREDIT:
   						$boolIsBusinessCreditScreeningRequest = true;
   						break;

   					case CScreenType::BUSINESS_PREMIER_PROFILE:
   						$boolIsBusinessPremierProfileScreeningRequest = true;
   						break;

   					case CScreenType::DO_NOT_RENT:
   						$boolIsDoNotRentScreeningRequest                        = true;

   						$arrmixDoNotRentConditionSetValues                      = rekeyArray( 'condition_id', ( array ) CScreeningPackageConditionSets::fetchCustomScreeningPackageConditionSetsByScreeningPackageIdByCid( $objScreeningApplicant->getScreeningPackageId(), $this->getCid(), $objScreeningDatabase ) );
   						$objStdScreenTypeResult->DoNotRentValues                = array();
   						$objStdScreenTypeResult->DoNotRentConditionSetValues    = $arrmixDoNotRentConditionSetValues;
   						break;

				    case CScreenType::PASSPORT_VISA_VERIFICATION:
					    $boolIsPassPortVerification = true;
					    break;

                    case CScreenType::PRECISE_ID:
                        $boolIsPreciseIdRequest = true;
                        $arrmixPreciseIdConditionSetValues = rekeyArray( 'condition_id', ( array ) CScreeningConditionSets::fetchCustomScreeningConditionSetsByScreenTypeIdByScreeningPackageId( CScreenType::PRECISE_ID, $objScreeningApplicant->getScreeningPackageId(), $objScreeningDatabase ) );
                        $objStdScreenTypeResult->PreciseIdConditionSetValues   = $arrmixPreciseIdConditionSetValues;
	                    $objStdScreenTypeResult->RecommendationTypeId   = $objScreeningTransaction->getScreeningRecommendationTypeId();
	                    $objStdScreenTypeResult->PreciseIdChild   =  rekeyArray( 'screen_type_id', ( array ) \Psi\Eos\Screening\CScreeningTransactions::createService()->fetchPreciseIdChildScreeningTransactionByScreeningApplicantIdByCidByScreenTypeIds( $objScreeningApplicant->getId(), $this->getCid(), CScreenType::$c_arrintPreciseIdScreenTypeIdsReviewResult, $objScreeningDatabase ) );
                        break;

   					default:
   						// need to add comment
   						break;
   				}

   				$objStdScreeningApplicantDetails->ScreenResults[$objScreeningTransaction->getScreenTypeId()] = $objStdScreenTypeResult;
   				$objStdScreeningApplicantDetails->ScreenTypeTotalResultValues = ( true == valArr( $arrmixApplicantScreeningData ) ) ? $arrmixApplicantScreeningData['ScreeningApplicantMixScreenTypeResultValues'] : array();

   			}

   			// Set Criminal Recommendation.....
   			if( true == array_key_exists( CScreenType::CRIMINAL, $objStdScreeningApplicantDetails->ScreenResults ) ) {
   				$objStdScreeningApplicantDetails->ScreenResults[CScreenType::CRIMINAL]->RecommendationTypeId = $this->determineCriminalRecommendation( $arrobjScreeningTransactions );
			    $objStdScreeningApplicantDetails->ScreenResults[CScreenType::CRIMINAL]->SearchScope          = ( true == valArr( $arrmixCriminalSearchScope ) ) ? implode( ', ', array_unique( $arrmixCriminalSearchScope ) ) : NULL;
   			}

   			$objStdScreeningApplicantDetails->screeningApplicantTransactionIds = new stdClass();
   			$objStdScreeningApplicantDetails->screeningApplicantTransactionIds = $arrintScreeningApplicantTransactionIds;

		    $objStdScreeningApplicantDetails->screeningCompletedTransactionIds  = new stdClass();
		    $objStdScreeningApplicantDetails->screeningCompletedTransactionIds  = ( true == valArr( $arrintApplicantCompletedTransactionIds ) ) ? json_encode( $arrintApplicantCompletedTransactionIds ) : '';

		    $objStdScreeningApplicantDetails->screeningCompletedScreenTypeIds  = new stdClass();
		    $objStdScreeningApplicantDetails->screeningCompletedScreenTypeIds  = ( true == valArr( $arrintScreeningCompletedScreenTypeIds ) ) ? json_encode( $arrintScreeningCompletedScreenTypeIds ) : '';

		    // Set Criminal Recommendation.....
   			if( true == array_key_exists( CScreenType::CRIMINAL, $objStdScreeningApplicantDetails->ScreenResults ) && true == array_intersect_key( $objStdScreeningApplicantDetails->ScreenResults, CScreenType::$c_arrintManualCriminalSearchScreenTypeIds ) ) {
   				$objStdScreeningApplicantDetails->manualCriminalResult = new stdClass();
   				$objStdScreeningApplicantDetails->manualCriminalResult = $arrmixManualCriminalSearchResult;

   				$objStdScreeningApplicantDetails->ScreenResults[CScreenType::CRIMINAL]->RecommendationTypeId = $this->determineCriminalRecommendation( $arrobjScreeningTransactions );
   			}

		    $intScreeningVendorId = ( $intNullDataProviderCnt > 0 ) ? CScreeningApplicationRequest::VERSION_1B : CTransmissionVendor::RESIDENT_VERIFY;
   		}

   		$objStdRequestedScreenTypes = new stdClass();

   		$objStdRequestedScreenTypes->isCreditScreeningRequest                   = $boolIsCreditScreeningRequest;
   		$objStdRequestedScreenTypes->isCriminalScreeningRequest                 = $boolIsCriminalScreeningRequest;
   		$objStdRequestedScreenTypes->isEvictionScreeningRequest                 = $boolIsEvictionScreeningRequest;
   		$objStdRequestedScreenTypes->isIncomeScreeningRequest                   = $boolIsIncomeScreeningRequest;
	    $objStdRequestedScreenTypes->isVerificationOfIncomeRequest              = $boolIsVerificationOfIncomeRequest;
   		$objStdRequestedScreenTypes->isDoNotRentScreeningRequest                = $boolIsDoNotRentScreeningRequest;
   		$objStdRequestedScreenTypes->isBusinessCreditScreeningRequest           = $boolIsBusinessCreditScreeningRequest;
   		$objStdRequestedScreenTypes->isBusinessPremierProfileScreeningRequest   = $boolIsBusinessPremierProfileScreeningRequest;
   		$objStdRequestedScreenTypes->isRentalHistoryScreeningRequest            = $boolIsRentalHistoryScreeningRequest;
   		$objStdRequestedScreenTypes->isStateCriminalSearch                      = $boolIsStateCriminalSearch;
   		$objStdRequestedScreenTypes->isCountyCriminalSearch                     = $boolIsCountyCriminalSearch;
	    $objStdRequestedScreenTypes->isPassportVerification                     = $boolIsPassPortVerification;
        $objStdRequestedScreenTypes->isPreciseIdScreeningRequest                         = $boolIsPreciseIdRequest;

   		if( true == valArr( $objStdScreeningApplicantDetails->ScreenResults ) ) {
   			$objStdScreeningApplicantDetails->ScreenResults['requestedScreenTypes'] = $objStdRequestedScreenTypes;
   		}

	  	if( CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR == $objScreeningApplicant->getStatusTypeId() ) {
   			$objStdScreeningApplicantDetails->RecommendationTypeId = CScreeningRecommendationType::ERROR;
   		} elseif( true == in_array( $objScreeningApplicant->getStatusTypeId(), array( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN, CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW, CScreeningStatusType::APPLICANT_RECORD_STATUS_ID_VERIFICATION_PENDING ) ) ) {
   			$objStdScreeningApplicantDetails->RecommendationTypeId = CScreeningRecommendationType::PENDING_UNKNOWN;
   		} elseif( 0 < $intApplicantFailedCounts ) {
   			$objStdScreeningApplicantDetails->RecommendationTypeId = CScreeningRecommendationType::FAIL;
   		} elseif( 0 < $intApplicantConditionalCounts ) {
   			$objStdScreeningApplicantDetails->RecommendationTypeId = CScreeningRecommendationType::PASSWITHCONDITIONS;
	    } elseif( CScreeningRecommendationType::PENDING_UNKNOWN == $objScreeningApplicant->getScreeningRecommendationTypeId() ) {
		    $objStdScreeningApplicantDetails->RecommendationTypeId = CScreeningRecommendationType::PENDING_UNKNOWN;
	    } else {
		    $objStdScreeningApplicantDetails->RecommendationTypeId = CScreeningRecommendationType::PASS;
	    }

   		$objStdScreeningApplicantDetails->Recommendation = CResidentVerifyServiceUtil::getScreeningRecommendationText( $objScreeningApplicant->getStatusTypeId(), $objStdScreeningApplicantDetails->RecommendationTypeId );

		$objStdScreeningResult->ScreeningApplicants[$objScreeningApplicant->getCustomApplicantId()][$objScreeningApplicant->getId()] = $objStdScreeningApplicantDetails;

    	$objStdScreeningResult->screeningTransactionIds  = array_unique( $arrintScreeningTransactionIds );

    	$objStdScreeningResult->ApplicantManualCriminalScreenTypeIds = json_encode( $arrintScreeningApplicantManualCriminalSearchIds );

    	// to show combined rv_index score value only when all applicants screening packages have "rv_index score" enabled otherwise not to be shown.
    	$objStdScreeningResult->rvIndexScore = ( false == is_null( $this->getRvIndex() ) && self::SCREENED_WITH_PACKAGE_SETTINGS == $this->getScreeningConfigurationId() && true == $boolIsRvIndexOnForApplication ) ? $this->getRvIndex() : NULL;
	    $objStdScreeningResult->ScreenTypeIds            = array_unique( $arrintScreenTypeIds );
	    $objStdScreeningResult->ScreeningVendorId        = $intScreeningVendorId;

    	return $objStdScreeningResult;
    }

    public function determineGroupRecommendation( $intCurrentUserId, $arrobjScreeningApplicants, $objScreeningDatabase, $boolReturnResultOnly = false ) {

        $arrmixScreeningRecommendation = $this->determineRecommendation( $arrobjScreeningApplicants );

        if( false == valArr( $arrmixScreeningRecommendation ) ) return;

        if( true == $boolReturnResultOnly ) return $arrmixScreeningRecommendation;

        $arrmixScreeningRecommendation = $this->updateScreeningApplicationResult( $intCurrentUserId, $objScreeningDatabase );

        return $arrmixScreeningRecommendation;
    }

    public function determineRecommendation( $arrobjScreeningApplicants ) {

        $intScreeningRecommendationTypeId = CScreeningRecommendationType::PENDING_UNKNOWN;
        $intScreeningStatusTypeId         = CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN;
        $arrmixScreeningRecommendation    = array();

        $arrmixScreeningRecommendation['screening_status_type_id']          = $intScreeningStatusTypeId;
        $arrmixScreeningRecommendation['screening_recommendation_type_id']  = $intScreeningRecommendationTypeId;

        if( false == valArr( $arrobjScreeningApplicants ) ) return $arrmixScreeningRecommendation;

        $arrobjRekeyedScreeningApplicants = rekeyObjects( 'ScreeningRecommendationTypeId', $arrobjScreeningApplicants );

        if( true == array_key_exists( CScreeningRecommendationType::ERROR, $arrobjRekeyedScreeningApplicants ) ) {
            $intScreeningRecommendationTypeId = CScreeningRecommendationType::ERROR;
        } elseif( true == array_key_exists( CScreeningRecommendationType::PENDING_UNKNOWN, $arrobjRekeyedScreeningApplicants ) ) {
            $intScreeningRecommendationTypeId = CScreeningRecommendationType::PENDING_UNKNOWN;
        } elseif( true == array_key_exists( CScreeningRecommendationType::FAIL, $arrobjRekeyedScreeningApplicants ) ) {
            $intScreeningRecommendationTypeId = CScreeningRecommendationType::FAIL;
            $intScreeningStatusTypeId         = CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED;
        } elseif( true == array_key_exists( CScreeningRecommendationType::PASSWITHCONDITIONS, $arrobjRekeyedScreeningApplicants ) ) {
            $intScreeningRecommendationTypeId = CScreeningRecommendationType::PASSWITHCONDITIONS;
            $intScreeningStatusTypeId         = CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED;
        } else {
            $intScreeningRecommendationTypeId = CScreeningRecommendationType::PASS;
            $intScreeningStatusTypeId         = CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED;
        }

        $arrmixScreeningRecommendation['screening_status_type_id']          = $intScreeningStatusTypeId;
        $arrmixScreeningRecommendation['screening_recommendation_type_id']  = $intScreeningRecommendationTypeId;

        return $arrmixScreeningRecommendation;
    }

	public function addScreeningConditionSets( $intCurrentUserId, $arrintRecommendationConditionSetIds, $objScreeningDatabase ) {
		// archieving screening conditions
		CScreeningCondition::archiveScreeningConditions( $this->getId(), $this->getCid(), $intCurrentUserId, $objScreeningDatabase );

		if( false == valArr( $arrintRecommendationConditionSetIds ) ) return;

		$arrintAppliedConditionSetIds = [];
		foreach( $arrintRecommendationConditionSetIds as $intScreenTypeId => $arrmixConditionSets ) {
			if( false == valArr( $arrintAppliedConditionSetIds ) ) {
				$arrintAppliedConditionSetIds = $arrmixConditionSets;
			} else {
				$arrintAppliedConditionSetIds = array_replace( $arrintAppliedConditionSetIds, $arrmixConditionSets );
			}
		}

		$arrintAppliedConditionSetIds = ( true == valArr( $arrintAppliedConditionSetIds ) ) ? array_filter( $arrintAppliedConditionSetIds ) : [];

		if( false == valArr( $arrintAppliedConditionSetIds ) ) return;

		$objScreeningCompanyPreference = CScreeningCompanyPreferences::fetchActiveScreeningCompanyPreferenceByClientIdByScreeningCompanyPreferenceTypeId( $this->getCid(), CScreeningCompanyPreferenceType::USE_CONDITION_SET_PRIORITIZATION, $objScreeningDatabase );
		if( true == valObj( $objScreeningCompanyPreference, 'CScreeningCompanyPreference' ) && ( true == is_null( $this->getCreatedOn() ) || ( strtotime( $this->getCreatedOn() ) > strtotime( $objScreeningCompanyPreference->getCreatedOn() ) ) ) ) {
			$arrmixValue = $objScreeningCompanyPreference->getValue();
			if( true == $arrmixValue->{CScreeningCompanyPreferenceType::USE_CONDITION_SET_PRIORITIZATION} ) {
				$arrmixConditionSetIds = CScreeningTransactionConditions::fetchCustomScreeningConditionSetIdsInEvaluationOrderByScreeningTransactionIds( array_keys( $arrintAppliedConditionSetIds ), $objScreeningDatabase );

				if( true == valArr( $arrmixConditionSetIds ) ) {
					$arrintAppliedConditionSetIds = [];
					foreach( $arrmixConditionSetIds as $arrmixData ) {
						$arrintAppliedConditionSetIds[] = $arrmixData['condition_set_id'];
					}
				}
			}
		}

        $arrintAppliedConditionSetIds = ( true == valArr( $arrintAppliedConditionSetIds ) ) ? array_unique( $arrintAppliedConditionSetIds ) : [];

        if( false == valArr( $arrintAppliedConditionSetIds ) ) return;

		$boolIsSuccess = true;

		foreach( $arrintAppliedConditionSetIds as $intAppliedConditionSetId ) {
			$objScreeningCondition = new CScreeningCondition();
			$objScreeningCondition->setScreeningId( $this->getId() );
			$objScreeningCondition->setCid( $this->getCid() );
			$objScreeningCondition->setScreeningPackageConditionSetId( $intAppliedConditionSetId );
			$objScreeningCondition->setIsActive( '1' );

			if( false == $objScreeningCondition->insert( $intCurrentUserId, $objScreeningDatabase ) ) {
				$boolIsSuccess = false;
				break;
			}
		}

		return $boolIsSuccess;
	}

    public function getOverallApplicationRecommendationByScreeningApplicants( $arrobjScreeningApplicants, $objScreeningDatabase ) {

        $objStdApplicationRecommendation = new stdClass();

        $arrmixScreeningApplicantResult = array();
        $boolHasCriminalRecords         = false;

	    $objStdApplicationRecommendation->applicationRecommendationId   = $this->getScreeningRecommendationTypeId();
        $objStdApplicationRecommendation->ScreeningStatusTypeId         = $this->getScreeningStatusTypeId();
        $objStdApplicationRecommendation->screeningApplicantResults     = array();

        $objStdApplicationRecommendation->remoteScreeningId           = $this->getId();
        $objStdApplicationRecommendation->IsScreeningPackageEnabled   = ( self::SCREENED_WITH_PACKAGE_SETTINGS == $this->getScreeningConfigurationId() ) ? true : false;

        $arrintScreeningPackageIds = array();
	    $arrintScreeningDenielReasons = [];

	    $boolAttributeBased = false;

	    // if there are screening applicants
	    if( true == valArr( $arrobjScreeningApplicants ) ) {

	    	$arrintScreeningPackageIds          = array_filter( array_keys( rekeyObjects( 'ScreeningPackageId', $arrobjScreeningApplicants ) ) );

		    if( true == valArr( $arrintScreeningPackageIds ) ) {
		    	$arrmixScreeningApplicantPackages   = CScreeningPackages::fetchPublishedScreeningPackagesByIdsByCid( $arrintScreeningPackageIds, $this->getCid(), $objScreeningDatabase );
		    }

	        foreach( $arrobjScreeningApplicants as $objScreeningApplicant ) {
                    $arrobjScreeningTransactions             = $objScreeningApplicant->getScreeningApplicantResults( $objScreeningDatabase );

                $arrmixScreeningApplicantTransactionsResult = [];

                    foreach( $arrobjScreeningTransactions as $objScreeningTransactions ) {
                        if( true == in_array( $objScreeningTransactions->getScreenTypeId(), CScreenType::$c_arrintManualCriminalSearchScreenTypeIds ) ) {
		                    $arrmixScreeningApplicantTransactionsResult[$objScreeningTransactions->getScreenTypeId()][$objScreeningTransactions->getSearchType()]['ScreeningRecommendationTypeId']  = $objScreeningTransactions->getScreeningRecommendationTypeId();
		                    $arrmixScreeningApplicantTransactionsResult[$objScreeningTransactions->getScreenTypeId()][$objScreeningTransactions->getSearchType()]['ScreeningStatusTypeId']          = $objScreeningTransactions->getScreeningStatusTypeId();
	                    } else {
		                    $arrmixScreeningApplicantTransactionsResult[$objScreeningTransactions->getScreenTypeId()]['ScreeningRecommendationTypeId']  = $objScreeningTransactions->getScreeningRecommendationTypeId();
		                    $arrmixScreeningApplicantTransactionsResult[$objScreeningTransactions->getScreenTypeId()]['ScreeningStatusTypeId']          = $objScreeningTransactions->getScreeningStatusTypeId();
	                    }
                    }

	                $arrobjRekeyedScreeningTransaction       = rekeyObjects( 'ScreeningRecommendationTypeId', $arrobjScreeningTransactions );

			        $arrintScreenTypeIds = getObjectFieldValuesByFieldName( 'ScreenTypeId', $arrobjRekeyedScreeningTransaction );

			        if( false == $boolAttributeBased && CScreeningRecommendationType::PASS != $objScreeningApplicant->getScreeningRecommendationTypeId() &&
			            true == in_array( CScreenType::ATTRIBUTE_BASED, $arrintScreenTypeIds ) ) {
				        $boolAttributeBased = true;
			        }

	                $objStdScreeningApplicantResult         = new stdClass();
	                $intScreeningApplicantRecommendationId  = NULL;
	                $boolHasCriminalRecords                 = false;
		            $arrmixScreeningApplicantErrors         = [];

	                // Evaluating applicant result

	                switch( $objScreeningApplicant->getStatusTypeId() ) {

	                    case CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR:
	                        $intScreeningApplicantRecommendationId = CScreeningRecommendationType::ERROR;
		                    $arrmixScreeningApplicantErrors = ( valArr( $arrobjScreeningTransactions ) ) ? $this->getScreeningApplicantErrorsByScreeningTransactionIds( array_keys( $arrobjScreeningTransactions ), $objScreeningDatabase ) : [];
	                        break;

	                    case CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN:
	                        $intScreeningApplicantRecommendationId = CScreeningRecommendationType::PENDING_UNKNOWN;
	                        break;

	                    case CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED:
	                        if( true == valArr( $arrobjRekeyedScreeningTransaction ) ) {

			                    if( true == array_key_exists( CScreeningRecommendationType::FAIL, $arrobjRekeyedScreeningTransaction ) ) {
			                        $intScreeningApplicantRecommendationId = CScreeningRecommendationType::FAIL;
			                        $objScreeningTransaction = $arrobjRekeyedScreeningTransaction[CScreeningRecommendationType::FAIL];
			                        $boolHasCriminalRecords = ( true == valObj( $objScreeningTransaction, 'CScreeningTransaction' ) && CScreenType::CRIMINAL == $objScreeningTransaction->getScreenTypeId() && CScreeningRecommendationType::FAIL == $objScreeningTransaction->getScreeningRecommendationTypeId() ) ? true : false;
			                    } elseif( true == array_key_exists( CScreeningRecommendationType::PASSWITHCONDITIONS, $arrobjRekeyedScreeningTransaction ) ) {
			                            $intScreeningApplicantRecommendationId = CScreeningRecommendationType::PASSWITHCONDITIONS;

			                    } else {
			                        $intScreeningApplicantRecommendationId = CScreeningRecommendationType::PASS;
			                    }
	                        }
	                        break;

	                    default:
	                        $intScreeningApplicantRecommendationId = CScreeningRecommendationType::PENDING_UNKNOWN;
	                }

			        $objStdScreeningApplicantResult->screeningApplicantRecommendationId = $intScreeningApplicantRecommendationId;
			        $objStdScreeningApplicantResult->screeningApplicantTypeId           = $objScreeningApplicant->getScreeningApplicantTypeId();
			        $objStdScreeningApplicantResult->boolHasCriminalRecords             = $boolHasCriminalRecords;
			        $objStdScreeningApplicantResult->screeningDenialReasons             = $this->getDenialReasons( $arrobjScreeningTransactions );
			        $objStdScreeningApplicantResult->screeningErrors                    = $arrmixScreeningApplicantErrors;
                    $objStdScreeningApplicantResult->screenTypeResult                   = $arrmixScreeningApplicantTransactionsResult;
		            $objStdScreeningApplicantResult->screeningApplicantId               = $objScreeningApplicant->getId();

		            if( ( true == valArr( $arrmixScreeningApplicantPackages ) ) && ( true == array_key_exists( $objScreeningApplicant->getScreeningPackageId(), $arrmixScreeningApplicantPackages ) ) ) {
			            $objStdScreeningApplicantResult->screeningPackageId                     = $objScreeningApplicant->getScreeningPackageId();
			            $objStdScreeningApplicantResult->screeningPackageName                   = $arrmixScreeningApplicantPackages[$objScreeningApplicant->getScreeningPackageId()]->getName();
		            }

		            $objStdScreeningApplicantResult->screeningPackageId                     = $objScreeningApplicant->getScreeningPackageId();
		            $objStdScreeningApplicantResult->screeningApplicantStatusTypeId         = $objScreeningApplicant->getStatusTypeId();
		            if( true == valArr( $objStdScreeningApplicantResult->screeningDenialReasons ) ) {
			            $arrintScreeningDenielReasons = array_merge( $arrintScreeningDenielReasons, array_filter( $objStdScreeningApplicantResult->screeningDenialReasons ) );
		            }

					if( CScreeningApplicationType::VENDOR_SCREENING == $this->getApplicationTypeId() && true == valArr( $arrobjRekeyedScreeningTransaction ) ) {
		                $objScreeningTransaction = current( $arrobjRekeyedScreeningTransaction );
			            $objStdScreeningApplicantResult->screeningCompletedDate = ( false == is_null( $objScreeningTransaction->getOriginalScreeningCompletedOn() ) ) ? $objScreeningTransaction->getOriginalScreeningCompletedOn() : $objScreeningTransaction->getScreeningCompletedOn();
		            }

	            $arrmixScreeningApplicantResult[$objScreeningApplicant->getCustomApplicantId()] = $objStdScreeningApplicantResult;
	        }

	        $objStdApplicationRecommendation->screeningApplicantResults = $arrmixScreeningApplicantResult;

	        if( CScreeningRecommendationType::PASSWITHCONDITIONS == $this->getScreeningRecommendationTypeId() ) {
		        $objStdApplicationRecommendation->screeningConditionSetDetails = new stdClass();
		        $objStdApplicationRecommendation->screeningConditionSetDetails = $this->prepareScreeningPackageConditionSetData( $this->getScreeningPackageConditionSetDetails( array_unique( $arrintScreeningPackageIds ), $objScreeningDatabase, true ) );
	        }

            $objStdApplicationRecommendation->screening_denial_reasons = ( true == valArr( $arrintScreeningDenielReasons ) ) ? array_unique( $arrintScreeningDenielReasons ) : [];
		    $objStdApplicationRecommendation->attributeBased = $boolAttributeBased;
	    }

	    // set compropery preference type setting for PRECISE_ID_ABANDONED.

	    $objStdApplicationRecommendation->preciseIdAbandoned = $this->getPreciseIdAbandoned( $objScreeningDatabase );

        return  $objStdApplicationRecommendation;
    }

    public function getPreciseIdAbandoned( $objScreeningDatabase ) {
		$objScreeningPropertyPreferences = CScreeningPropertyPreferences::fetchActiveScreeningPropertyPreferenceByClientIdByScreeningPropertyPreferenceTypeIdByPropertyId( $this->getCid(), CScreeningCompanyPreferenceType::PRECISE_ID_ABANDONED, $this->getPropertyId(), $objScreeningDatabase );
	    $intPreciseIdAbandonedValue = 0;
		if( true == valobj( $objScreeningPropertyPreferences, 'CScreeningPropertyPreference' ) ) {
			$intArrPreciseIdAbandoned = $objScreeningPropertyPreferences->getValue();
			if( key( $intArrPreciseIdAbandoned ) == CScreeningCompanyPreferenceType::DAYS ) {
				$intPreciseIdAbandonedValue = current( $intArrPreciseIdAbandoned ) . ' days';
			} else if( key( $intArrPreciseIdAbandoned ) == CScreeningCompanyPreferenceType::HOURS ) {
				$intPreciseIdAbandonedValue = current( $intArrPreciseIdAbandoned ) . ' hours';
			}
		}

		return $intPreciseIdAbandonedValue;
	}

	public function prepareScreeningPackageConditionSetData( $arrmixScreeningConditionSetsDetails ) {
		if( false == valArr( $arrmixScreeningConditionSetsDetails ) ) return NULL;

		$arrmixConditionSets            = array();

		foreach( $arrmixScreeningConditionSetsDetails as $arrmixScreeningConditionSetDetails ) {
			foreach( $arrmixScreeningConditionSetDetails as $arrmixScreeningConditionSet ) {
				$arrmixConditionSets[$arrmixScreeningConditionSet['condition_set_id']]['condition_name'] = $arrmixScreeningConditionSet['condition_set_name'];
				$arrmixConditionSets[$arrmixScreeningConditionSet['condition_set_id']]['is_published']   = $arrmixScreeningConditionSet['is_published'];
				$arrmixConditionSets[$arrmixScreeningConditionSet['condition_set_id']]['screening_package_operand_type_id']   = $arrmixScreeningConditionSet['screening_package_operand_type_id'];

				if( false == valArr( $arrmixConditionSets[$arrmixScreeningConditionSet['condition_set_id']]['subset_details'] ) ) {
					$arrmixConditionSets[$arrmixScreeningConditionSet['condition_set_id']]['subset_details'] = [];
				}

				$arrmixConditionSets[$arrmixScreeningConditionSet['condition_set_id']]['subset_details'][$arrmixScreeningConditionSet['condition_subset_id']][] = [
					'condition_subset_id_one'                       => $arrmixScreeningConditionSet['condition_subset_id_one'],
					'condition_subset_id_two'                       => $arrmixScreeningConditionSet['condition_subset_id_two'],
					'required_items'                                => $arrmixScreeningConditionSet['required_items'],
					'condition_subset_value_id'                     => $arrmixScreeningConditionSet['condition_subset_value_id'],
					'screening_package_available_condition_name'    => $arrmixScreeningConditionSet['available_condition_name'],
					'screening_package_condition_type_id'           => $arrmixScreeningConditionSet['screening_package_condition_type_id'],
					'screening_available_condition_id'              => $arrmixScreeningConditionSet['screening_available_condition_id']
				];
			}
		}

		return $arrmixConditionSets;
	}

	public function updateScreeningApplicationResult( $intCurrentUserId, $objScreeningDatabase, $boolIsUpdateOriginalResult = false ) {
		$intScreeningRecommendationTypeId = CScreeningRecommendationType::PENDING_UNKNOWN;
		$intRVScreeningApplicationStatusTypeId   = CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN;

		$arrobjScreeningApplicants  = CScreeningApplicants::fetchActiveScreeningApplicantsByScreeningIdByCid( $this->getId(), $this->getCid(), $objScreeningDatabase );

		if( false == valArr( $arrobjScreeningApplicants ) ) {
			trigger_error( 'Resident Verify: Unable to load Active Screening Applicants for ScreeningId' . $this->getId(), E_USER_WARNING );
			return;
		}

		$arrintScreeningPackageIds = $arrintScreeningApplicantIds = array();

		$boolIsAllMustPassCreditAndIncome  = false;
		$boolGuarantorOveridesCreditIncome = true;  // This is used to set the default behavior to overide Applicant Credit and Income Fail decision when a Guarator is a Pass on these

		$boolOverrideMustPassCreditAndIncome = false;

		// fetch all settings here
		$arrobjScreeningPropertyPreferences = ( array ) CScreeningPropertyPreferences::fetchActiveScreeningPropertyPreferencesByClientIdByScreeningPropertyPreferenceTypeIdByPropertyId( $this->getCid(), CScreeningCompanyPreferenceType::$c_arrintScreeningRecommendationConfigTypeIds, $this->getPropertyId(), $objScreeningDatabase );
		$arrobjScreeningPropertyPreferences = ( true == valArr( $arrobjScreeningPropertyPreferences ) ) ? rekeyObjects( 'ScreeningCompanyPreferenceTypeId', $arrobjScreeningPropertyPreferences ) : [];

		if( true == valArr( $arrobjScreeningPropertyPreferences ) ) {
			$arrobjScreeningPropertyPreferenceValue = $arrobjScreeningPropertyPreferences[CScreeningCompanyPreferenceType::ALL_APPLICANT_MUST_PASS_CREDIT_INCOME]->getValue();
			$boolIsAllMustPassCreditAndIncome = ( true == array_key_exists( CScreeningCompanyPreferenceType::ALL_APPLICANT_MUST_PASS_CREDIT_INCOME, $arrobjScreeningPropertyPreferences ) ) ? ( bool ) $arrobjScreeningPropertyPreferenceValue->{CScreeningCompanyPreferenceType::ALL_APPLICANT_MUST_PASS_CREDIT_INCOME} : true;
		} else {
			$boolIsAllMustPassCreditAndIncome = true; // if there is no setting in the table. Same as existing behaviour
		}

		$intExistingGroupResultOverriddenBy = $this->getGroupResultOverriddenBy();
		$this->setGroupResultOverriddenBy( NULL );

		foreach( $arrobjScreeningApplicants as $objScreeningApplicant ) {

			if( CScreeningApplicantType::CORPORATE_VERIFICATION == $objScreeningApplicant->getScreeningApplicantTypeId() ) continue;

			$arrintScreeningApplicantIds[$objScreeningApplicant->getId()]               = $objScreeningApplicant->getId();
			$arrintScreeningPackageIds[$objScreeningApplicant->getScreeningPackageId()] = $objScreeningApplicant->getScreeningPackageId();
		}

		$arrobjScreeningPackages = CScreeningPackages::fetchScreeningPackagesByIdsCid( $arrintScreeningPackageIds, $this->getCid(), $objScreeningDatabase );

		$arrintRecommendationConditionSetIds      = array();

		$intCombineConditionSetSettingValue = NULL;

		$intErrorApplicantCounts                                = 0;
		$intIncompleteApplicantCounts                           = 0;
		$intConditionalApprovalCounts                           = 0;
		$intCriminalEvictionConditionalCounts                   = 0;

		$intManualReviewApplicantCounts = 0;

		$boolIsRequiredScreenFailed = false; // Must Pass CRIMINAL,EVICTION & DO NOT RENT

		$intApplicantsScreenedWithCredit = 0;
		$intApplicantsScreenedWithIncome = 0;
		$intApplicantsFailedWithCredit   = 0;
		$intApplicantsFailedWithIncome   = 0;

		$intGuarantorsScreenedWithCredit = 0;
		$intGuarantorsScreenedWithIncome = 0;
		$intGuarantorsFailedWithCredit   = 0;
		$intGuarantorsFailedWithIncome   = 0;
		$intGuarantorsPassWithCredit     = 0;
		$intGuarantorsPassWithIncome     = 0;

		$intGuarantorApplicantCount                             = 0;
		$intNonGuarantorApplicantCount                          = 0;

		$intApplicantPassCredit = 0;
		$intApplicantPassIncome = 0;

		$intGuarantorConditionWithCredit = 0;
		$intGuarantorConditionWithIncome = 0;

        $boolOverrideGroupRecommendation = false;

		foreach( $arrobjScreeningApplicants as $objScreeningApplicant ) {
			if( CScreeningApplicantType::CORPORATE_VERIFICATION == $objScreeningApplicant->getScreeningApplicantTypeId() ) continue;

			if( false == is_null( $objScreeningApplicant->getScreeningPackageId() ) && true == valArr( $arrobjScreeningPackages ) && true == array_key_exists( $objScreeningApplicant->getScreeningPackageId(), $arrobjScreeningPackages ) ) {
                $objScreeningPackage = getArrayElementByKey( $objScreeningApplicant->getScreeningPackageId(), $arrobjScreeningPackages );

                if( true == valObj( $objScreeningPackage, 'CScreeningPackage' ) ) {
                    $intCombineConditionSetSettingValue = $objScreeningPackage->getCombineConditionSetValue();

                    if( false == $boolOverrideGroupRecommendation && true == $objScreeningPackage->getIsUseForPrescreening() && 0 < $objScreeningApplicant->fetchRemainingTransactionsCount( $objScreeningDatabase ) ) {
                        $boolOverrideGroupRecommendation = true;
                    }
                }
            }

			// IF completed
			if( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED != $objScreeningApplicant->getStatusTypeId() ) {
				if( CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR == $objScreeningApplicant->getStatusTypeId() ) {
					$intErrorApplicantCounts = $intErrorApplicantCounts + 1;
				} elseif( CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW == $objScreeningApplicant->getStatusTypeId() ) {
					$intManualReviewApplicantCounts = $intManualReviewApplicantCounts + 1;
				} else {
					$intIncompleteApplicantCounts = $intIncompleteApplicantCounts + 1;
				}

			} else {
				$boolIsGuarantor = false;

				$arrobjScreeningTransactions        = $objScreeningApplicant->getScreeningApplicantResults( $objScreeningDatabase );

				$arrobjRekeyedScreeningTransactions = rekeyObjects( 'ScreenTypeId', $arrobjScreeningTransactions );

				if( CScreeningApplicantType::GUARANTOR == $objScreeningApplicant->getScreeningApplicantTypeId() ) {
					$boolIsGuarantor = true;
					$intGuarantorApplicantCount = $intGuarantorApplicantCount + 1;
				}

				foreach( $arrobjScreeningTransactions as $objScreeningTransaction ) {
					$boolIsFailed = false;

					if( false == $boolIsAllMustPassCreditAndIncome && false == $boolOverrideMustPassCreditAndIncome && CScreenType::CREDIT == $objScreeningTransaction->getScreenTypeId() ) {
						$boolOverrideMustPassCreditAndIncome = $objScreeningTransaction->checkOverrideMustPassCreditIncomeSetting( $arrobjScreeningPropertyPreferences );
					}

					if( CScreeningRecommendationType::PASSWITHCONDITIONS == $objScreeningTransaction->getScreeningRecommendationTypeId() ) {
						if( true == in_array( $objScreeningTransaction->getScreenTypeId(), [ CScreenType::CREDIT, CScreenType::EVICTION ] ) ) {
							if( CScreeningPackage::COMBINE_CONDITION_SET == $intCombineConditionSetSettingValue && false == is_null( $objScreeningTransaction->getScreeningPackageConditionSetId() ) ) {
								$arrintRecommendationConditionSetIds[$objScreeningTransaction->getScreenTypeId()][$objScreeningTransaction->getId()] = $objScreeningTransaction->getScreeningPackageConditionSetId();
							} elseif( $intCombineConditionSetSettingValue == $objScreeningTransaction->getScreenTypeId() && false == is_null( $objScreeningTransaction->getScreeningPackageConditionSetId() ) ) {
								$arrintRecommendationConditionSetIds[$objScreeningTransaction->getScreenTypeId()][$objScreeningTransaction->getId()] = $objScreeningTransaction->getScreeningPackageConditionSetId();
							} else {
                                $arrintRecommendationConditionSetIds[$objScreeningTransaction->getScreenTypeId()][$objScreeningTransaction->getId()] = $objScreeningTransaction->getScreeningPackageConditionSetId();
                            }

						} elseif( false == is_null( $objScreeningTransaction->getScreeningPackageConditionSetId() ) ) {
							$arrintRecommendationConditionSetIds[$objScreeningTransaction->getScreenTypeId()][$objScreeningTransaction->getId()] = $objScreeningTransaction->getScreeningPackageConditionSetId();
						}
					}

					if( CScreeningRecommendationType::FAIL == $objScreeningTransaction->getScreeningRecommendationTypeId() ) $boolIsFailed = true;
					if( CScreeningRecommendationType::PASSWITHCONDITIONS == $objScreeningTransaction->getScreeningRecommendationTypeId() ) $intConditionalApprovalCounts = $intConditionalApprovalCounts + 1;

					switch( $objScreeningTransaction->getScreenTypeId() ) {
						case CScreenType::CREDIT:
							if( true == $boolIsGuarantor ) {
								$intGuarantorsScreenedWithCredit += 1;
								if( true == $boolIsFailed ) {
									$intGuarantorsFailedWithCredit += 1;
								} elseif( CScreeningRecommendationType::PASS == $objScreeningTransaction->getScreeningRecommendationTypeId() ) {
									$intGuarantorsPassWithCredit += 1;
								} elseif( CScreeningRecommendationType::PASSWITHCONDITIONS == $objScreeningTransaction->getScreeningRecommendationTypeId() ) {
									$intGuarantorConditionWithCredit += 1;
								}

							} else {
								$intApplicantsScreenedWithCredit += 1;
								if( true == $boolIsFailed ) {
									$intApplicantsFailedWithCredit += 1;
								} elseif( CScreeningRecommendationType::PASS == $objScreeningTransaction->getScreeningRecommendationTypeId() ) {
									$intApplicantPassCredit += 1;
								}
							}
							break;

						case CScreenType::INCOME:
							if( true == $boolIsGuarantor ) {
								$intGuarantorsScreenedWithIncome += 1;
								if( true == $boolIsFailed ) {
									$intGuarantorsFailedWithIncome += 1;
								} elseif( CScreeningRecommendationType::PASS == $objScreeningTransaction->getScreeningRecommendationTypeId() ) {
									$intGuarantorsPassWithIncome += 1;
								} elseif( CScreeningRecommendationType::PASSWITHCONDITIONS == $objScreeningTransaction->getScreeningRecommendationTypeId() ) {
									$intGuarantorConditionWithIncome += 1;
								}

							} else {
								$intApplicantsScreenedWithIncome += 1;
								if( true == $boolIsFailed ) {
									$intApplicantsFailedWithIncome += 1;
								} elseif( CScreeningRecommendationType::PASS == $objScreeningTransaction->getScreeningRecommendationTypeId() ) {
									$intApplicantPassIncome += 1;
								}
							}
							break;

						default:
							if( CScreeningRecommendationType::PASSWITHCONDITIONS == $objScreeningTransaction->getScreeningRecommendationTypeId() ) {
								$intCriminalEvictionConditionalCounts += 1;
							}
							// screen type criminal
							if( true == $boolIsFailed ) {
								$boolIsRequiredScreenFailed = true;
							}
							break;
					}
				}
			}
		}

		$intGuarantorsSatisfiedCredit = $intGuarantorsScreenedWithCredit - $intGuarantorsFailedWithCredit;
		$intGuarantorsSatisfiedIncome = $intGuarantorsScreenedWithIncome - $intGuarantorsFailedWithIncome;

		if( true == $boolOverrideMustPassCreditAndIncome ) {
			$boolIsAllMustPassCreditAndIncome = true;
		}

		// OK When through all applicants
		// If any in error
		if( 0 < $intErrorApplicantCounts ) {
			$intRVScreeningApplicationStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR;
			$intScreeningRecommendationTypeId = CScreeningRecommendationType::ERROR;
		} elseif( 0 < $intIncompleteApplicantCounts ) {
			$intRVScreeningApplicationStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN;
			$intScreeningRecommendationTypeId = CScreeningRecommendationType::PENDING_UNKNOWN;
		} elseif( 0 < $intManualReviewApplicantCounts ) {
			$intRVScreeningApplicationStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW;
			$intScreeningRecommendationTypeId = CScreeningRecommendationType::PENDING_UNKNOWN;
		} else {
			$intRVScreeningApplicationStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED;

			if( true == $boolIsRequiredScreenFailed ) {
				$intScreeningRecommendationTypeId = CScreeningRecommendationType::FAIL;
			} else {

				if( 0 < $intApplicantsFailedWithCredit && 0 == $intGuarantorsSatisfiedCredit && true == $boolGuarantorOveridesCreditIncome ) {
					if( true == $boolIsAllMustPassCreditAndIncome ) {
						$intScreeningRecommendationTypeId = CScreeningRecommendationType::FAIL;
					} elseif( $intApplicantsFailedWithCredit == $intApplicantsScreenedWithCredit ) {
						$intScreeningRecommendationTypeId = CScreeningRecommendationType::FAIL;
					}
				}

				if( 0 < $intApplicantsFailedWithIncome && 0 == $intGuarantorsSatisfiedIncome && true == $boolGuarantorOveridesCreditIncome ) {
					if( true == $boolIsAllMustPassCreditAndIncome ) {
						$intScreeningRecommendationTypeId = CScreeningRecommendationType::FAIL;
					} elseif( $intApplicantsFailedWithIncome == $intApplicantsScreenedWithIncome ) {
						$intScreeningRecommendationTypeId = CScreeningRecommendationType::FAIL;
					}
				}

				// Guarantor : Atlease One Guarantor Must Pass Credit or Income
				if( 0 < $intGuarantorsScreenedWithCredit && $intGuarantorsScreenedWithCredit == $intGuarantorsFailedWithCredit ) {
					$intScreeningRecommendationTypeId = CScreeningRecommendationType::FAIL;
				}

				if( 0 < $intGuarantorsScreenedWithIncome && $intGuarantorsScreenedWithIncome == $intGuarantorsFailedWithIncome ) {
					$intScreeningRecommendationTypeId = CScreeningRecommendationType::FAIL;
				}

				if( CScreeningRecommendationType::FAIL != $intScreeningRecommendationTypeId ) {
					/**
					1. Applicants Screened with credit and income and guarantor satisfies those requirements
					2. Applicants Screened with credit only and not income and guarantor satisfies the credit
					3. Applicants Screened with income only and not credit and guarantor satisfies the income
					 */
					$boolOverrideConditional = false;

					if( false == $boolIsAllMustPassCreditAndIncome ) {
						if( 0 < $intGuarantorConditionWithCredit || 0 < $intGuarantorConditionWithIncome ) {
							$boolOverrideConditional = false;
						} else if( 0 < $intApplicantPassCredit && 0 < $intApplicantPassIncome ) {
							$boolOverrideConditional = true;
						} elseif( 0 < $intApplicantsScreenedWithCredit && 0 == $intApplicantsScreenedWithIncome && 0 < $intApplicantPassCredit ) {
							$boolOverrideConditional = true;
						} elseif( 0 < $intApplicantsScreenedWithIncome && 0 == $intApplicantsScreenedWithCredit && 0 < $intApplicantPassIncome ) {
							$boolOverrideConditional = true;
						}
					}

					if( 0 < $intCriminalEvictionConditionalCounts ) {
						$intScreeningRecommendationTypeId = CScreeningRecommendationType::PASSWITHCONDITIONS;
					} elseif( 0 < $intGuarantorsPassWithCredit && 0 < $intGuarantorsPassWithIncome ) {
						$intScreeningRecommendationTypeId = CScreeningRecommendationType::PASS;
						if( ( 0 < $intApplicantsScreenedWithIncome && 0 < $intApplicantsFailedWithIncome ) || ( 0 < $intApplicantsScreenedWithCredit && 0 < $intApplicantsFailedWithCredit ) ) {
							$this->setGroupResultOverriddenBy( CCustomerType::GUARANTOR );
						}
					} elseif( 0 < $intGuarantorsScreenedWithCredit && 0 < $intGuarantorsPassWithCredit && 0 == $intGuarantorsScreenedWithIncome ) {
						$intScreeningRecommendationTypeId = CScreeningRecommendationType::PASS;
						if( 0 < $intApplicantsScreenedWithCredit && 0 < $intApplicantsFailedWithCredit && 0 == $intApplicantsScreenedWithIncome ) {
							$this->setGroupResultOverriddenBy( CCustomerType::GUARANTOR );
						}
					} elseif( 0 < $intGuarantorsScreenedWithIncome && 0 < $intGuarantorsPassWithIncome && 0 == $intGuarantorsScreenedWithCredit ) {
						$intScreeningRecommendationTypeId = CScreeningRecommendationType::PASS;
						if( 0 < $intApplicantsScreenedWithIncome && 0 < $intApplicantsFailedWithIncome && 0 == $intApplicantsScreenedWithCredit ) {
							$this->setGroupResultOverriddenBy( CCustomerType::GUARANTOR );
						}
					} elseif( 0 < $intConditionalApprovalCounts && false == $boolOverrideConditional ) {
						$intScreeningRecommendationTypeId = CScreeningRecommendationType::PASSWITHCONDITIONS;
					} else {
						$intScreeningRecommendationTypeId = CScreeningRecommendationType::PASS;
					}

					if( false == $boolIsAllMustPassCreditAndIncome && CScreeningRecommendationType::PASSWITHCONDITIONS == $intScreeningRecommendationTypeId ) {
						if( 0 < $intApplicantPassCredit && 0 == $intGuarantorConditionWithCredit ) {
							unset( $arrintRecommendationConditionSetIds[CScreenType::CREDIT] );
						}

						if( 0 < $intApplicantPassIncome && 0 == $intGuarantorConditionWithIncome ) {
							unset( $arrintRecommendationConditionSetIds[CScreenType::INCOME] );
						}
					}
				}
			}
		}

		$arrmixGroupRecommendation = array();

		if( $intRVScreeningApplicationStatusTypeId != $this->getScreeningStatusTypeId() || $intScreeningRecommendationTypeId != $this->getScreeningRecommendationTypeId() || $intExistingGroupResultOverriddenBy != $this->getGroupResultOverriddenBy() ) {

			if( true == $boolIsUpdateOriginalResult ) $this->setOriginalScreeningRecommendationTypeId( $this->getScreeningRecommendationTypeId() );

            // 2394770 - Tier Screening - Option to not auto-continue
			// setting the recommendation type as PENDING review if the package is tiered screening

            $intOriginalScreeningRecommendationTypeId = $intScreeningRecommendationTypeId;

            if( true == $boolOverrideGroupRecommendation && true == in_array( $intScreeningRecommendationTypeId, [ CScreeningRecommendationType::PASS, CScreeningRecommendationType::PASSWITHCONDITIONS ] ) ) {
                $intOriginalScreeningRecommendationTypeId = CScreeningRecommendationType::PENDING_REVIEW;
            }

            $this->setScreeningRecommendationTypeId( $intOriginalScreeningRecommendationTypeId );

			$this->setScreeningStatusTypeId( $intRVScreeningApplicationStatusTypeId );

			if( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED == $intRVScreeningApplicationStatusTypeId ) $this->setScreeningCompletedOn( 'NOW()' );

			$objScreeningDatabase->begin();

			if( false == $this->update( $intCurrentUserId, $objScreeningDatabase ) ) {
				trigger_error( 'Failed to update screening decision and status for screening id :[' . $this->getId() . '] client id: [' . $this->getCid() . ']', E_USER_WARNING );
				$objScreeningDatabase->rollback();
				return false;
			}

			$objScreeningDatabase->commit();
		}

		$this->addScreeningConditionSets( $intCurrentUserId, $arrintRecommendationConditionSetIds, $objScreeningDatabase );

		// update the screening report request status as per overall screening recommendation

        $this->updateScreeningReportRequests( $this->getScreeningStatusTypeId(), $arrintScreeningApplicantIds, $intCurrentUserId, $objScreeningDatabase );

        $arrmixGroupRecommendation['screening_recommendation_type_id']  = $intScreeningRecommendationTypeId;
		$arrmixGroupRecommendation['screening_status_type_id']          = $intRVScreeningApplicationStatusTypeId;

		return $arrmixGroupRecommendation;
	}

	public function updateScreeningReportRequests( $intScreeningStatusTypeId, $arrintScreeningApplicantIds, $intCurrentUserId, $objScreeningDatabase ) {
        if( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED == $intScreeningStatusTypeId ) {
            // from inprogress to report ready
            CScreeningReportRequests::updateScreeningReportRequestStatusByScreeningIdByScreeningApplicantIdsByCid( $this->getId(), $arrintScreeningApplicantIds, $this->getCid(), CScreeningReportRequestStatus::REPORT_READY, $intCurrentUserId, $objScreeningDatabase, true );
        } else {
            // if screening is not completed then update the status of the existing applicant to inprogress
            // from report ready to inprogress
            CScreeningReportRequests::updateScreeningReportRequestStatusByScreeningIdByScreeningApplicantIdsByCid( $this->getId(), $arrintScreeningApplicantIds, $this->getCid(), CScreeningReportRequestStatus::INPROGRESS, $intCurrentUserId, $objScreeningDatabase, false );
        }

		// we need to set is_active field of screening report request logs table to false
		\Psi\Eos\Screening\CScreeningReportRequestLogs::createService()->archiveScreeningReportRequestLogsByScreeningIdByScreeningApplicantIds( $this->getId(), $arrintScreeningApplicantIds, $objScreeningDatabase );

	}

	public function calculateRVIndexScore( $arrobjScreeningApplicants, $arrmixScreeningApplicantsResultValues, $intCurrentUserId, $objScreeningDatabase, $boolIsResultOnly = false ) {

		if( false == valArr( $arrmixScreeningApplicantsResultValues ) ) return NULL;

		$objScreeningUtil = new CScreeningUtils();
		$intConsolidateRVIndexScore = $objScreeningUtil->calculateConsolidateRVIndexScore( $arrobjScreeningApplicants, $this, $arrmixScreeningApplicantsResultValues );

		if( true == is_null( $intConsolidateRVIndexScore ) ) return;

		if( true == $boolIsResultOnly ) return $intConsolidateRVIndexScore;

		$boolIsSucess = $this->updateRVIndex( $intConsolidateRVIndexScore, $intCurrentUserId, $objScreeningDatabase );

		if( true == $boolIsSucess ) {
			return $intConsolidateRVIndexScore;
		}
	}

	public function updateRVIndex( $intConsolidateRVIndexScore, $intCurrentUserId, $objScreeningDatabase ) {

		$this->setRvIndex( $intConsolidateRVIndexScore );

		$objScreeningDatabase->begin();

		if( false == $this->update( $intCurrentUserId, $objScreeningDatabase ) ) {
			trigger_error( 'Failed to update RV Index for screening id :[' . $this->getId() . '] client id: [' . $this->getCid() . ']', E_USER_WARNING );
			$objScreeningDatabase->rollback();
			return false;
		}

		$objScreeningDatabase->commit();
		return true;
	}

	public function determineOverallScreeningStatus( $intCurrentUserId, $arrobjScreeningApplicants, $objScreeningDatabase ) {

		if( false == valArr( $arrobjScreeningApplicants ) ) return;

		$boolIsOverallScreeningCompleted = true;

		foreach( $arrobjScreeningApplicants as $objScreeningApplicant ) {
			if( false == is_null( $objScreeningApplicant->getScreeningCompletedOn() ) ) continue;
			$boolIsOverallScreeningCompleted = false;
			break;
		}

		if( false == $boolIsOverallScreeningCompleted ) return;

		$this->updateOverallScreeningStatus( $intCurrentUserId, $objScreeningDatabase );

		return;
	}

	public function updateOverallScreeningStatus( $intCurrentUserId, $objScreeningDatabase ) {

		$this->setScreeningCompletedOn( 'NOW()' );

		$objScreeningDatabase->begin();

		if( false == $this->update( $intCurrentUserId, $objScreeningDatabase ) ) {
			trigger_error( 'Failed to update overall screening status for screening id :[' . $this->getId() . '] client id: [' . $this->getCid() . ']', E_USER_WARNING );
			$objScreeningDatabase->rollback();
			return false;
		}

		$objScreeningDatabase->commit();

		return;
	}

	public function updateScreeningApplicantsConsolidatedRvIndexResultValue( $intConsolidateRVIndexScore, $arrobjScreeningApplicants, $arrobjScreeningTransactions, $arrmixScreeningApplicantsCreditResultValues, $intCurrentUserId, $objScreeningDatabase ) {

		if( 0 == $intConsolidateRVIndexScore || false == valArr( $arrobjScreeningApplicants ) || false == valArr( $arrmixScreeningApplicantsCreditResultValues ) ) return $arrmixScreeningApplicantsCreditResultValues;

		if( false == valArr( $arrobjScreeningTransactions ) ) return;

		$arrmixScreeningApplicantCreditResultValues = array();

		foreach( $arrobjScreeningApplicants as $objScreeningApplicant ) {

			if( false == array_key_exists( $objScreeningApplicant->getId(), $arrmixScreeningApplicantsCreditResultValues ) ) continue;

			$arrobjRekeyedScreeningTransactions = rekeyObjects( 'ScreeningApplicantId', $arrobjScreeningTransactions, true );

			$arrobjScreeningApplicantTransactions = ( true == array_key_exists( $objScreeningApplicant->getId(), $arrobjRekeyedScreeningTransactions ) ) ? $arrobjRekeyedScreeningTransactions[$objScreeningApplicant->getId()] : array();

			if( false == valArr( $arrobjScreeningApplicantTransactions ) ) continue;

			$arrobjRekeyedScreeningApplicantTransactions = rekeyObjects( 'ScreenTypeId', $arrobjScreeningApplicantTransactions );

			if( false == valArr( $arrobjRekeyedScreeningApplicantTransactions ) || false == array_key_exists( CScreenType::CREDIT, $arrobjRekeyedScreeningApplicantTransactions ) ) continue;

			$objScreeningApplicantCreditTransaction = $arrobjRekeyedScreeningApplicantTransactions[CScreenType::CREDIT];

			if( CScreeningApplicantType::GUARANTOR != $objScreeningApplicant->getScreeningApplicantTypeId() ) {

				$arrmixScreeningApplicantsCreditResultValues[$objScreeningApplicant->getId()] = rekeyArray( 'screening_config_preference_type_id', $arrmixScreeningApplicantsCreditResultValues[$objScreeningApplicant->getId()] );

				if( true == valArr( $arrmixScreeningApplicantsCreditResultValues[$objScreeningApplicant->getId()] ) && false == array_key_exists( CScreeningConfigPreferenceType::RV_INDEX_SCORE, $arrmixScreeningApplicantsCreditResultValues[$objScreeningApplicant->getId()] ) ) continue;

				$arrmixScreeningApplicantsCreditResultValues[$objScreeningApplicant->getId()][CScreeningConfigPreferenceType::RV_INDEX_SCORE]['value_unfiltered'] = $intConsolidateRVIndexScore;
			}

			$arrmixScreeningApplicantEvaluatedResultValues = $objScreeningApplicant->evaluateResult( $arrmixScreeningApplicantsCreditResultValues[$objScreeningApplicant->getId()], $objScreeningDatabase );

			if( false == valArr( $arrmixScreeningApplicantEvaluatedResultValues ) ) continue;

			$arrmixScreeningApplicantsCreditResultValues[$objScreeningApplicant->getId()] = $arrmixScreeningApplicantEvaluatedResultValues;

			$objScreeningApplicant->determineTransactionRecommendation( $intCurrentUserId, $arrmixScreeningApplicantEvaluatedResultValues, $objScreeningApplicantCreditTransaction, $objScreeningDatabase );

			// this code for temporay purpose, when we move all 1b clients to version 2 we will remove this code.
			$objScreeningApplicant->determineAndUpdateScreeningTransactionCompletionStatus( $intCurrentUserId, $objScreeningApplicantCreditTransaction, $objScreeningDatabase, true );
		}

		return array_filter( $arrmixScreeningApplicantsCreditResultValues );
	}

	private function mapAndSetScreeningRecommendationTypeId( $intCid, $arrmixScreeningApplicantCreditValues, $objScreeningDatabase ) {

		if( false == valArr( $arrmixScreeningApplicantCreditValues ) ) return array();

		$arrmixRekeyedScreeningApplicantCreditValues = rekeyArray( 'screening_package_condition_set_id', $arrmixScreeningApplicantCreditValues );

		$arrmixScreeningConditionSetRecommendations = CScreeningPackageConditionSets::fetchCustomScreeningPackageConditionSetsRecommendationsByIdsByCid( array_filter( array_keys( $arrmixRekeyedScreeningApplicantCreditValues ) ), $intCid, $objScreeningDatabase );
		$arrmixScreeningConditionSetRecommendations = rekeyArray( 'id', $arrmixScreeningConditionSetRecommendations );

		foreach( $arrmixScreeningApplicantCreditValues as $intScreeningConfigPreferenceTypeId => $arrmixScreeningApplicantCreditValue ) {
			$arrmixScreeningApplicantCreditValues[$intScreeningConfigPreferenceTypeId]['screening_recommendation_type_id'] = ( true == valArr( $arrmixScreeningConditionSetRecommendations ) && true == array_key_exists( $arrmixScreeningApplicantCreditValue['screening_package_condition_set_id'], $arrmixScreeningConditionSetRecommendations ) ) ? $arrmixScreeningConditionSetRecommendations[$arrmixScreeningApplicantCreditValue['screening_package_condition_set_id']]['screening_recommendation_type_id'] : NULL;
		}

		return $arrmixScreeningApplicantCreditValues;
	}

	public function getCombineIncome( $arrobjScreeningApplicants ) {

		$intCombineIncome = 0;

		if( false == valArr( $arrobjScreeningApplicants ) ) return $intCombineIncome;

		foreach( $arrobjScreeningApplicants as $objScreeningApplicant ) {

			if( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED == $objScreeningApplicant->getStatusTypeId() ) continue;

			if( true == in_array( $objScreeningApplicant->getScreeningApplicantTypeId(), array( CScreeningApplicantType::GUARANTOR, CScreeningApplicantType::CORPORATE ) ) ) continue;

			$intCombineIncome += $objScreeningApplicant->getIncome();

		}

		return $intCombineIncome;
	}

	public function createScreeningApplicantsAndTransactions( $objStdServiceData, $arrobjScreeningAccountRates, $objDatabase ) {

		$boolIsValid = true;

		$intCurrentUserId = $objStdServiceData->authInfo->currentUserId;
		$arrobjStdScreeningApplicants = $objStdServiceData->application->applicants->applicant;

		if( false == valArr( $arrobjStdScreeningApplicants ) ) {
			trigger_error( 'Failed to load screening applicant info for screening id: ' . $this->getId() . ' cid: ' . $this->getCid(), E_USER_WARNING );
			return false;
		}
	    $arrmixPropertyDetails                          = [];
	    $arrmixPropertyDetails['propertyId']            = $objStdServiceData->authInfo->propertyId;
	    $arrmixPropertyDetails['propertyName']          = $objStdServiceData->authInfo->propertyName;
	    $arrmixPropertyDetails['propertyStateCode']     = $objStdServiceData->authInfo->propertyStateCode;
	    $arrmixPropertyDetails['propertyZipCode']       = $objStdServiceData->authInfo->propertyZipCode;

		$arrobjExistingScreeningApplicants = $this->getExistingScreeningApplicants( array_keys( $arrobjStdScreeningApplicants ), $objDatabase );

		$arrobjScreeningApplicants = array();
        $objExistingScreeningApplicant = NULL;

		foreach( $arrobjStdScreeningApplicants as $intCustomApplicantId => $objStdScreeningApplicant ) {
			$objExistingScreeningApplicant = getArrayElementByKey( $intCustomApplicantId, $arrobjExistingScreeningApplicants );

			$boolCanReUseApplicant = false;

			if( true == valObj( $objExistingScreeningApplicant, 'CScreeningApplicant' ) ) {
				$boolCanReUseApplicant 			= $objExistingScreeningApplicant->determineReUseScreeningApplicant( $objStdServiceData->preferenceSettings, $objStdScreeningApplicant );

				if( CVendorAccessScreeningLibrary::SCREENING_REQUEST_SOURCE_ID_RUN == $objStdServiceData->preferenceSettings->screeningRequestSourceId && true == $boolCanReUseApplicant && CScreeningApplicationType::VENDOR_SCREENING == $this->getApplicationTypeId() ) {
					$boolCanReUseApplicant = false;
				}

				if( false == $boolCanReUseApplicant ) {
					$objExistingScreeningApplicant->setStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );

					if( false == $objExistingScreeningApplicant->update( $intCurrentUserId, $objDatabase ) ) {
						$boolIsValid &= false;
						trigger_error( 'Failed to update screening applicant status for applicant id: ' . $objExistingScreeningApplicant->getId() . 'cid: ' . $objExistingScreeningApplicant->getCid(), E_USER_WARNING );
						break;
					}
				}
			}

			if( false == $boolCanReUseApplicant ) {
				$objScreeningApplicant = $this->insertOrUpdateScreeningApplicant( $intCurrentUserId, NULL, $objStdScreeningApplicant, $objDatabase );
			} else {
				$objScreeningApplicant = $this->insertOrUpdateScreeningApplicant( $intCurrentUserId, $objExistingScreeningApplicant, $objStdScreeningApplicant, $objDatabase );
			}

			if( false == valObj( $objScreeningApplicant, 'CScreeningApplicant' ) ) {
				$boolIsValid &= false;
				break;
			}

            /**
             * 1. New Process screening
             * 2. Resubmit within 30 days
             * 3. Resubmit after 30 days
             */

            $boolIsValid = $objScreeningApplicant->createScreeningReportRequest( $objStdScreeningApplicant->applicantInfo->sendCaliforniaConsumerReport, $intCurrentUserId, CScreeningReportRequestStatus::INPROGRESS, CScreeningReportRequestSource::PROSPECT_PORTAL, $objExistingScreeningApplicant, $objDatabase );

			if( CScreeningApplicantType::CORPORATE_VERIFICATION == $objScreeningApplicant->getScreeningApplicantTypeId() ) {
				$objScreeningApplicant->createCorporateVerificationTransaction( $intCurrentUserId, $arrobjScreeningAccountRates, $objDatabase, $objStdScreeningApplicant->applicantInfo->reuse );
			} elseif( CScreeningApplicationType::VENDOR_SCREENING == $this->getApplicationTypeId() ) {
				$objScreeningApplicant->createVendorScreeningTransactions( $objStdServiceData, $intCurrentUserId, $this->getPropertyId(), $arrobjScreeningAccountRates, $objDatabase );
			} else {
				$objScreeningApplicant->createScreeningTransactions( $intCurrentUserId, $this->getPropertyId(), $arrobjScreeningAccountRates, $objDatabase );
			}

			if( false == valArr( $objScreeningApplicant->getScreeningTransactions() ) ) {
				$boolIsValid &= false;
				break;
			}
		}

		return $boolIsValid;
	}

	public function insertOrUpdateScreeningApplicant( $intCurrentUserId, $objExistingScreeningApplicant, $objStdScreeningApplicant, $objDatabase ) {

		if( false == valObj( $objStdScreeningApplicant, 'stdClass' ) )
			return false;

		if( false == valObj( $objExistingScreeningApplicant, 'CScreeningApplicant' ) ) {
			$objScreeningApplicant = new CScreeningApplicant();
		} else {
			$objScreeningApplicant = $objExistingScreeningApplicant;
		}

		$objScreeningApplicant->setCid( $this->getCid() );
		$objScreeningApplicant->setScreeningId( $this->getId() );

		$objScreeningApplicant->setCustomApplicantId( $objStdScreeningApplicant->applicantId );
		$objScreeningApplicant->setScreeningPackageId( $objStdScreeningApplicant->packageId );

		$objScreeningApplicant->setApplicantApplicationTransmissionId( NULL );

		$objStdApplicantBasicInfo = $objStdScreeningApplicant->applicantInfo;

		$objScreeningApplicant->setScreeningApplicantTypeId( $objStdApplicantBasicInfo->applicantTypeId );
		$objScreeningApplicant->setNameFirst( $objStdApplicantBasicInfo->nameFirst );
		$objScreeningApplicant->setNameLast( $objStdApplicantBasicInfo->nameLast );
		$objScreeningApplicant->setNameMiddle( $objStdApplicantBasicInfo->nameMiddle );
		$objScreeningApplicant->setTaxNumberLastFour( false == is_null( $objStdApplicantBasicInfo->ssn ) ? \Psi\CStringService::singleton()->substr( preg_replace( '/[^a-z0-9]/i', '', ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $objStdApplicantBasicInfo->ssn, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] ) ), - 4, 4 ) : NULL );
		$objScreeningApplicant->setTaxNumberEncrypted( $objStdApplicantBasicInfo->ssn );
		$objScreeningApplicant->setBirthDateYear( \Psi\CStringService::singleton()->substr( $objStdApplicantBasicInfo->birthDate, - 4, 4 ) );
		$objScreeningApplicant->setBirthDateEncrypted( base64_encode( $objStdApplicantBasicInfo->birthDate ) );
		$objScreeningApplicant->setIsAlien( $objStdApplicantBasicInfo->isAlien );

		$objScreeningApplicant->setDriverLicense( $objStdApplicantBasicInfo->DriverLicense );
		$objScreeningApplicant->setDriverLicenseState( $objStdApplicantBasicInfo->DriverLicenseStateCode );

		$objScreeningApplicant->setIncome( $objStdApplicantBasicInfo->monthlyIncome );
		$objScreeningApplicant->setEmailAddress( $objStdApplicantBasicInfo->emailAddress );

		$objStdApplicantCurrentAddress = $objStdScreeningApplicant->applicantAddress;

		$objScreeningApplicant->setAddressLine( $objStdApplicantCurrentAddress->addressLine );
		$objScreeningApplicant->setCity( $objStdApplicantCurrentAddress->municipality );
		$objScreeningApplicant->setState( $objStdApplicantCurrentAddress->state );
		$objScreeningApplicant->setTelephone( $objStdApplicantCurrentAddress->phoneNumber );
		$objScreeningApplicant->setZipCode( $objStdApplicantCurrentAddress->postalCode );

		$objStdApplicantEmployer = $objStdScreeningApplicant->applicantEmployers;

		$objScreeningApplicant->setEmployerName( $objStdApplicantEmployer->institutionName );

		$objScreeningApplicant->setStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN );
        $objScreeningApplicant->setScreeningRecommendationTypeId( CScreeningRecommendationType::PENDING_UNKNOWN );

		$objScreeningApplicant->setScreeningCompletedOn( NULL );
		$objScreeningApplicant->setDetails( $objStdApplicantBasicInfo->details );

		if( false == $objScreeningApplicant->insertOrUpdate( $intCurrentUserId, $objDatabase ) ) {
			trigger_error( 'Failed to create screening applicant for screening id: ' . $this->getId(), E_USER_ERROR );
			return false;
		}

		// previous address
		if( true == valStr( $objStdScreeningApplicant->applicantPreviousAddress->addressLine ) && true == valStr( $objStdScreeningApplicant->applicantPreviousAddress->state ) ) {
			$objScreeningApplicantAddress = $objScreeningApplicant->fetchOrCreateScreeningApplicantAddress( $objDatabase );
			$objScreeningApplicantAddress->setAddressTypeId( CScreeningApplicantAddressType::SCREENING_APPLICANT_PREVIOUS_ADDRESS );
			$objScreeningApplicantAddress->setCid( $objScreeningApplicant->getCid() );
			$objScreeningApplicantAddress->setAddressLine( $objStdScreeningApplicant->applicantPreviousAddress->addressLine );
			$objScreeningApplicantAddress->setCity( $objStdScreeningApplicant->applicantPreviousAddress->municipality );
			$objScreeningApplicantAddress->setState( $objStdScreeningApplicant->applicantPreviousAddress->state );
			$objScreeningApplicantAddress->setZipcode( $objStdScreeningApplicant->applicantPreviousAddress->postalCode );
			$objScreeningApplicantAddress->setScreeningApplicantId( $objScreeningApplicant->getId() );

			if( false == $objScreeningApplicantAddress->insertOrUpdate( $intCurrentUserId, $objDatabase ) ) {
				trigger_error( 'Failed to add screening applicant previous address for screening applicant id: ' . $objScreeningApplicant->getId(), E_USER_ERROR );
				return false;
			}
		}
        return $objScreeningApplicant;
	}

	/*
	 * fetch functions.
	 */

	public function fetchActiveScreeningApplicants( $objDatabase ) {
		$arrobjScreeningApplicants = CScreeningApplicants::fetchActiveScreeningApplicantsByScreeningIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		return $arrobjScreeningApplicants;
	}

	public function fetchActiveScreeningApplicantsByCustomApplicantIds( $arrintCustomApplicantIds, $objDatabase ) {
		if( false == valArr( $arrintCustomApplicantIds ) ) return;
		$arrobjScreeningApplicants = CScreeningApplicants::fetchActiveScreeningApplicantByScreeningIdByCustomApplicantIdsByCid( $this->getId(), $arrintCustomApplicantIds, $this->getCid(), $objDatabase );
		$arrobjScreeningApplicants = rekeyObjects( 'CustomApplicantId', $arrobjScreeningApplicants );
		return $arrobjScreeningApplicants;
	}

	/*
	 * ends here
	 *
	 */

	/*
	 * Other functions
	 */

	public function loadScreeningData( $objDatabase, $arrintScreeningApplicantIds = [], $boolIsForceReLoad = false ) {

		// This is to prevent it from loading same data again.
		if( ( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED == $this->getScreeningStatusTypeId() || true == valArr( $this->getScreeningApplicants() ) ) && false == $boolIsForceReLoad ) return;

		if( true == valArr( $arrintScreeningApplicantIds ) ) {
			$arrobjScreeningApplicants = CScreeningApplicants::fetchScreeningApplicantsByIdsByScreeningIdByCid( $arrintScreeningApplicantIds, $this->getId(), $this->getCid(), $objDatabase );
		} else {
			$arrobjScreeningApplicants = $this->fetchActiveScreeningApplicants( $objDatabase );
		}

		// Need to fetch only those transactions which we consider in recommendation.
		// For exm. no need to fetch crim cat,
		if( true == valArr( $arrobjScreeningApplicants ) ) {
			$arrobjScreeningTransactions = CScreeningTransactions::fetchActiveScreeningTransactionsByScreeningIdByScreeningApplicantIdsByCid( $this->getId(), array_keys( $arrobjScreeningApplicants ), $this->getCid(), $objDatabase );

			if( false == valArr( $arrobjScreeningTransactions ) ) {
				trigger_error( 'Failed to load screening transactions for screening id[' . $this->getId() . '] and cid[' . $this->getCid() . ']', E_USER_ERROR );
				return;
			}

			$arrobjScreeningTransactions = rekeyObjects( 'screeningApplicantId', $arrobjScreeningTransactions, true );

			foreach( $arrobjScreeningApplicants as $objScreeningApplicant ) {
				if( false == array_key_exists( $objScreeningApplicant->getId(), $arrobjScreeningTransactions ) ) {
					continue;
				}

				$arrobjRekeyedScreeningApplicantTransactions = getArrayElementByKey( $objScreeningApplicant->getId(), $arrobjScreeningTransactions );
				$arrobjScreeningApplicantTransactions        = $objScreeningApplicant->setChildScreeningTransactions( $arrobjRekeyedScreeningApplicantTransactions, $objDatabase );
				$objScreeningApplicant->setScreeningTransactions( $arrobjScreeningApplicantTransactions );
			}

			// check if applicants are not keyed by id then do it.
			$this->setScreeningApplicants( $arrobjScreeningApplicants );
		}

		return;
	}

	public function determineCriminalRecommendation( $arrobjScreeningTransactions ) {
		if( false == valArr( $arrobjScreeningTransactions ) ) return;

		$arrintCriminalRecommedationTypeIds = array();

		foreach( $arrobjScreeningTransactions as $objScreeningTransaction ) {
			if( false == in_array( $objScreeningTransaction->getScreenTypeId(), CScreenType::$c_arrintCriminalScreenTypeIds ) ) continue;
			$arrintCriminalRecommedationTypeIds[] = $objScreeningTransaction->getScreeningRecommendationTypeId();
		}

		$intCriminalRecommendationTypeId = NULL;

		if( true == in_array( CScreeningRecommendationType::ERROR, $arrintCriminalRecommedationTypeIds ) ) {
			$intCriminalRecommendationTypeId = CScreeningRecommendationType::ERROR;
		} elseif( true == in_array( CScreeningRecommendationType::PENDING_UNKNOWN, $arrintCriminalRecommedationTypeIds ) ) {
			$intCriminalRecommendationTypeId = CScreeningRecommendationType::PENDING_UNKNOWN;
		} elseif( true == in_array( CScreeningRecommendationType::FAIL, $arrintCriminalRecommedationTypeIds ) ) {
			$intCriminalRecommendationTypeId = CScreeningRecommendationType::FAIL;
		} elseif( true == in_array( CScreeningRecommendationType::PASSWITHCONDITIONS, $arrintCriminalRecommedationTypeIds ) ) {
			$intCriminalRecommendationTypeId = CScreeningRecommendationType::PASSWITHCONDITIONS;
		} else {
			$intCriminalRecommendationTypeId = CScreeningRecommendationType::PASS;
		}

		return $intCriminalRecommendationTypeId;
	}

	private function getExistingScreeningApplicants( $arrintCustomApplicantIds, $objDatabase ) {
		$arrobjExistingScreeningApplicants = array();

		switch( $this->getApplicationTypeId() ) {
			case CScreeningApplicationType::TENANT:
			case CScreeningApplicationType::BUSINESS:
			case CScreeningApplicationType::VENDOR_SCREENING:
			case CScreeningApplicationType::RV_TEST_CASES:
				$arrobjExistingScreeningApplicants = $this->fetchActiveScreeningApplicantsByCustomApplicantIds( $arrintCustomApplicantIds, $objDatabase );
				break;

			default:
				// handle this
				break;
		}

		return $arrobjExistingScreeningApplicants;
	}

	public function getExistingScreeningTransaction( $intScreenTypeId, $objScreeningApplicant, $objDatabase ) {
		if( CScreeningApplicationType::VENDOR_SCREENING != $this->getApplicationTypeId() ) return;

		$objExistingApplicant = CScreeningApplicants::fetchActiveVendorScreeningApplicantByCustomApplicantId( $objScreeningApplicant->getCustomApplicantId(), $objDatabase );

		if( false == valObj( $objExistingApplicant, 'CScreeningApplicant' ) ) {
			$arrobjScreeningTransactions = CScreeningTransactions::fetchCompletedScreeningTransactionsByScreenTypeIdsByScreeningIdByScreeningApplicantIdByCid( array( $intScreenTypeId ), $objScreeningApplicant->getScreeningId(), $objScreeningApplicant->getId(), $objScreeningApplicant->getCid(), $objDatabase );
			return ( true == valArr( $arrobjScreeningTransactions ) ) ? current( $arrobjScreeningTransactions ) : NULL;
		}

    	return $objScreeningApplicant->getExistingVendorScreeningTransaction( $objExistingApplicant, $objDatabase );
    }

	private function getDenialReasons( $arrobjScreeningTransactions ) {
		if( false == valArr( $arrobjScreeningTransactions ) ) return;

		$arrmixApplicationDenialReasons = array();

		foreach( $arrobjScreeningTransactions as $objScreeningTransaction ) {
			switch( $objScreeningTransaction->getScreeningRecommendationTypeId() ) {
				case CScreeningRecommendationType::FAIL:
					$arrmixApplicationDenialReasons[] = CScreeningDecisionReasonType::getScreeningDecisionReasonTypeIdByScreenTypeId( $objScreeningTransaction->getScreenTypeId() );
					break;

				case CScreeningRecommendationType::PASSWITHCONDITIONS:
					$arrmixApplicationDenialReasons[] = CScreeningDecisionReasonType::getScreeningDecisionReasonTypeIdByScreenTypeId( $objScreeningTransaction->getScreenTypeId() );
					break;

				default:
					// need to add comment
			}
		}

		return $arrmixApplicationDenialReasons;
	}

	private function getScreeningApplicantErrorsByScreeningTransactionIds( $arrintScreeningTransactionIds, $objScreeningDatabase ) {
		return ( array ) CScreeningTransactionErrorLogs::fetchActiveScreeningTransactionErrorLogsByScreeningTransactionIdsByCid( $arrintScreeningTransactionIds, $this->getCid(), $objScreeningDatabase );
	}

	public function calculateConsolidatedResults( $intCurrentUserId, $objDatabase ) {

		if( false == valArr( $this->getScreeningApplicants() ) ) {
			trigger_error( 'Failed to load screening applicants for screening id : ' . $this->getId() . ' And cid : ' . $this->getCid(), E_USER_WARNING );
			return false;
		}

		$arrmixScreeningApplicantDetails = [];

		$arrintSelectedScreenTypeIds = CResidentVerifyServiceUtil::getGroupResultScreenTypeIds( $this );

		if( false == valArr( $arrintSelectedScreenTypeIds ) ) return false;

		$objScreeningCompanyPreference = \Psi\Eos\Screening\CScreeningCompanyPreferences::createService()->fetchActiveScreeningCompanyPreferenceByClientIdByScreeningCompanyPreferenceTypeId( $this->getCid(), CScreeningCompanyPreferenceType::COMBINE_INCOME, $objDatabase );
		$intCombineIncomeSettingValue  = ( true == valObj( $objScreeningCompanyPreference, 'CScreeningCompanyPreference' ) && true == valId( $objScreeningCompanyPreference->getValue()->{CScreeningCompanyPreferenceType::COMBINE_INCOME} ) ) ? $objScreeningCompanyPreference->getValue()->{CScreeningCompanyPreferenceType::COMBINE_INCOME} : CScreeningCumulationType::EVALUATE_GUARANTOR_SEPERATELY;

		if( CScreeningCumulationType::COMBINE_GUARANTOR_INCOME == $intCombineIncomeSettingValue ) {
			$intGuarantorCombinedTotalIncome = \Psi\Eos\Screening\CScreeningApplicants::createService()->fetchCustomGuarantorCombinedIncome( $this->getId(), $this->getCid(), $objDatabase );
		}

		foreach( $this->getScreeningApplicants() as $objScreeningApplicant ) {
			if( false == in_array( $objScreeningApplicant->getScreeningApplicantTypeId(), [ CScreeningApplicantType::GUARANTOR, CScreeningApplicantType::CORPORATE ] ) ) {
				$intApplicantIncome = $this->getTotalHouseholdIncome();
			} else if( CScreeningCumulationType::COMBINE_GUARANTOR_INCOME == $intCombineIncomeSettingValue && $objScreeningApplicant->getScreeningApplicantTypeId() == CScreeningApplicantType::GUARANTOR ) {
				$intApplicantIncome = $intGuarantorCombinedTotalIncome;
			} else {
				$intApplicantIncome = $objScreeningApplicant->getIncome();
			}

			$objScreeningApplicant->setScreeningCumulationTypeId( $intCombineIncomeSettingValue );
			$arrmixScreeningApplicantDetail = CResidentVerifyServiceUtil::prepareScreeningApplicantsDetails( $objScreeningApplicant, $this->getRent(), $objScreeningApplicant->getIncome(), $this->getPropertyId(), $intApplicantIncome );
			if( true == valArr( $arrmixScreeningApplicantDetail ) ) {
				$arrmixScreeningApplicantDetails[$objScreeningApplicant->getId()] = $arrmixScreeningApplicantDetail;
			}
		}

		if( false == valArr( $arrmixScreeningApplicantDetails ) || false == valArr( array_filter( $arrmixScreeningApplicantDetails ) ) ) {
			return false;
		}

		$objScreeningCompanyPreference = \Psi\Eos\Screening\CScreeningCompanyPreferences::createService()->fetchScreeningCompanyPreferencesByScreeningCompanyPreferenceTypeIdBYCid( CScreeningCompanyPreferenceType::ALLOW_AVERAGE_VANTAGE_SCORE, $this->getCid(), $objDatabase );

		$arrmixScreeningDetails['isReCalculateRvIndexScore'] 	   	= ( true == in_array( CScreenType::CREDIT, $arrintSelectedScreenTypeIds ) ) ? true : false;
		$arrmixScreeningDetails['isReCalculateAverageVantageScore'] = ( true == in_array( CScreenType::CREDIT, $arrintSelectedScreenTypeIds ) && true == valObj( $objScreeningCompanyPreference, 'CScreeningCompanyPreference' ) ) ? true : false;
		$arrmixScreeningDetails['isReCalculateRentToIncomeRatio'] 	= ( true == in_array( CScreenType::INCOME, $arrintSelectedScreenTypeIds ) ) ? true : false;
		$arrmixScreeningDetails['isReCalculateRentToIncomeMatchPercentage'] = ( true == in_array( CScreenType::VERIFICATION_OF_INCOME, $arrintSelectedScreenTypeIds ) ) ? true : false;
		$arrmixScreeningDetails['screeningApplicantDetails']		= $arrmixScreeningApplicantDetails;

		$objResidentVerifySecureClientLibrary = new CResidentVerifySecureClientLibrary();
		$objStdSecureRequest = CResidentVerifySecureRequestBuilder::getCalculateConsolidateResultRequest( $intCurrentUserId, $this, $arrmixScreeningDetails );

		if( false == valObj( $objStdSecureRequest, 'stdClass' ) ) {
			// add appropriate error message
			return false;
		}

		$arrstrRawSecureClientResponse 	  = $objResidentVerifySecureClientLibrary->processCalculateConsolidatedResults( $objStdSecureRequest );
		$fltConsolidatedRvIndexScore	  = $arrstrRawSecureClientResponse['response']['result']['consolidatedRVIndexScore'];
		$fltIndividualRvIndexScore	  = $arrstrRawSecureClientResponse['response']['result']['individualRVIndexScore'];

		$this->setRvIndex( $fltConsolidatedRvIndexScore );

		if( false == $this->update( $intCurrentUserId, $objDatabase ) ) {
			trigger_error( 'Failed to update rv index score for screening id [' . $this->getId() . '] and cid [' . $this->getCid() . ']', E_USER_WARNING );
			return false;
		}

		foreach( $this->getScreeningApplicants() as $objScreeningApplicant ) {
			$fltIndividualRvIndexScoreValue = ( isset( $fltIndividualRvIndexScore[$objScreeningApplicant->getId()] ) ) ? reset( $fltIndividualRvIndexScore[$objScreeningApplicant->getId()]['individualRvIndexScore'] ) : NULL;
			$objScreeningApplicant->setRvIndexScore( $fltIndividualRvIndexScoreValue );
			if( false == $objScreeningApplicant->update( $intCurrentUserId, $objDatabase ) ) {
				trigger_error( 'Failed to update rv index score for screening id [' . $this->getScreeningId() . '] and screening applicant id [' . $this->getId() . ']  and cid [' . $this->getCid() . ']', E_USER_WARNING );
				return false;
			}
		}


		return true;
	}

	public function fetchApplicantScreeningRecommendationAndScreenTypesDetails( $arrobjScreeningApplicantDetails, $objScreeningDatabase ) {

		if( false == valArr( $arrobjScreeningApplicantDetails ) ) return;

		$objStdScreeningResult = new stdClass();
		$arrobjScreeningApplicantDetails      = rekeyObjects( 'Id', $arrobjScreeningApplicantDetails );

		$arrobjScreeningApplicantTransactions = CscreeningTransactions::fetchActiveScreeningTransactionsByScreeningIdByScreeningApplicantIdsByCid( $this->getId(), array_keys( $arrobjScreeningApplicantDetails ), $this->getCid(), $objScreeningDatabase, true );
		$arrobjScreeningApplicantTransactions = ( true == valArr( $arrobjScreeningApplicantTransactions ) ) ? rekeyObjects( 'ScreeningApplicantId', $arrobjScreeningApplicantTransactions, true ) : NULL;

		if( true == valArr( $arrobjScreeningApplicantTransactions ) ) {
			$objStdScreeningResult->ScreeningApplicants   = array();
			foreach( $arrobjScreeningApplicantDetails as $objScreeningApplicantDetails ) {
				if( true == array_key_exists( $objScreeningApplicantDetails->getCustomApplicantId(), $objStdScreeningResult->ScreeningApplicants ) ) continue;

				$objStdScreeningApplicantDetails      = new stdClass();
				$objStdScreeningApplicantDetails->RecommendationTypeId                                             = $objScreeningApplicantDetails->getScreeningRecommendationTypeId();
				$objStdScreeningApplicantDetails->Recommendation                                                   = CResidentVerifyServiceUtil::getScreeningRecommendationText( $objScreeningApplicantDetails->getStatusTypeId(), $objScreeningApplicantDetails->getScreeningRecommendationTypeId() );
				$objStdScreeningApplicantDetails->ScreenTypeIds                                                    = array_keys( rekeyObjects( 'ScreenTypeId', $arrobjScreeningApplicantTransactions[$objScreeningApplicantDetails->getId()] ) );
				$objStdScreeningResult->ScreeningApplicants[$objScreeningApplicantDetails->getCustomApplicantId()] = $objStdScreeningApplicantDetails;
			}
		}

		return $objStdScreeningResult;
	}

	public function getScreeningPackageConditionSetDetails( $arrintScreeningPackageIds, $objScreeningDatabase, $boolReturnAppliedConditionsOnly = false ) {

		// Corporate lead does not have screening package id, which was fetching blank record.
		// adding check on array $arrintScreeningPackageIds to filter out the blank values. Task #1665325 - User Warning in Array for module=corporate_leadsxxx
		$arrintScreeningPackageIds = array_filter( $arrintScreeningPackageIds );

		if( false == $boolReturnAppliedConditionsOnly ) {
			$arrmixScreeningConditionSetDetails = CScreeningConditionSets::fetchAllScreeningConditionSetsByScreeningIdScreeningPackageIdsByCid( $this->getId(), $arrintScreeningPackageIds, $this->getCid(), $objScreeningDatabase );
		} else {
			$arrintAppliedScreeningConditionSetIds = CScreeningConditions::fetchActiveScreeningConditionSetIdsByScreeningIdByCid( $this->getId(), $this->getCid(), $objScreeningDatabase );
			$arrmixScreeningConditionSetDetails    = CScreeningConditionSets::fetchAllScreeningConditionSetsByScreeningIdScreeningPackageIdsByCid( $this->getId(), $arrintScreeningPackageIds, $this->getCid(), $objScreeningDatabase, $arrintAppliedScreeningConditionSetIds );
		}

		$arrmixScreeningConditionSetDetails = ( true == valArr( $arrmixScreeningConditionSetDetails ) ) ? rekeyArray( 'condition_set_id', $arrmixScreeningConditionSetDetails, false, true ) : array();

		return $arrmixScreeningConditionSetDetails;
	}

	public function activateOrDeactivateScreeningApplicants( $objStdServiceData, $arrintCustomApplicantIds, $objScreeningDatabase ) {

		$boolIsValid = true;

		switch( NULL ) {
			default:
				$objScreeningDatabase->begin();
				/**
				 * Update the status of screening applicant ids
				 */
				if( false == CScreeningApplicants::activateOrDeactivateScreeningApplicants( $objStdServiceData->currentUserId, $objStdServiceData->screeningApplicantStatusType, $objStdServiceData->application->screeningId, $arrintCustomApplicantIds, $objStdServiceData->authInfo->Cid, $objScreeningDatabase ) ) {
					$objScreeningDatabase->rollback();
					$boolIsValid = false;
					break;
				}

				/**
				 * Reopen the screening records which are already screened and are being reactivated.
				 */
				if( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN == $objStdServiceData->screeningApplicantStatusType ) {
					$this->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN );
				}

				$this->loadScreeningData( $objScreeningDatabase );

				$arrobjScreeningApplicants = $this->getScreeningApplicants();

				if( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED != $this->getScreeningStatusTypeId() ) {
					/**
					 * update the income of active applicant after current applicant is deleted
					 */
					if( $this->getTotalHouseholdIncome() != $objStdServiceData->application->totalHouseholdIncome ) {
						$this->setTotalHouseholdIncome( $objStdServiceData->application->totalHouseholdIncome );
					}
				}

				if( false == valArr( $arrobjScreeningApplicants ) ) {
					$this->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );
				}

				// update only house hold income and return true
				if( false == $this->update( $objStdServiceData->currentUserId, $objScreeningDatabase ) ) {
					$objScreeningDatabase->rollback();
					$boolIsValid = false;
					break;
				}

				$objScreeningDatabase->commit();
				break;
		}

		if( false == $boolIsValid ) {
			trigger_error( 'Failed to update screening applicant data for application id [' . $this->getCustomApplicationId() . '] client id [' . $this->getCid() . ']', E_USER_WARNING );
			return false;
		}
		return true;
	}

	public function fetchPropertyPreferences( $objScreeningDatabase ) {

		$arrobjScreeningPropertyPreferenceSetting = CScreeningPropertyPreferences::fetchActiveScreeningPropertyPreferencesByClientIdByScreeningPropertyPreferenceTypeIdByPropertyId( $this->getCid(), CScreeningCompanyPreferenceType::$c_arrintScreeningRecommendationConfigTypeIds, $this->getPropertyId(), $objScreeningDatabase );
		$arrobjScreeningPropertyPreferenceSetting = ( true == valArr( $arrobjScreeningPropertyPreferenceSetting ) ) ? rekeyObjects( 'ScreeningCompanyPreferenceTypeId', $arrobjScreeningPropertyPreferenceSetting ) : [];

		$arrobjScreeningPropertyPreferences = [];
		$boolMustPassBankruptcy = $boolMustPassRentalCollection = false;
		if( true == valArr( $arrobjScreeningPropertyPreferenceSetting ) ) {
			if( isset( $arrobjScreeningPropertyPreferenceSetting[CScreeningCompanyPreferenceType::MUST_PASS_CREDIT_FACTORS_BANKRUPTCIES] ) ) {
				$arrmixBankruptcy = $arrobjScreeningPropertyPreferenceSetting[CScreeningCompanyPreferenceType::MUST_PASS_CREDIT_FACTORS_BANKRUPTCIES]->getValue();
				$boolMustPassBankruptcy = $arrmixBankruptcy->{CScreeningCompanyPreferenceType::MUST_PASS_CREDIT_FACTORS_BANKRUPTCIES};
				if( true == $boolMustPassBankruptcy ) {
					$arrobjScreeningPropertyPreferences[] = $arrmixBankruptcy;
				}
			}
			if( isset( $arrobjScreeningPropertyPreferenceSetting[CScreeningCompanyPreferenceType::MUST_PASS_CREDIT_FACTORS_RENTAL_COLLECTIONS] ) ) {
				$arrmixRentalCollection = $arrobjScreeningPropertyPreferenceSetting[CScreeningCompanyPreferenceType::MUST_PASS_CREDIT_FACTORS_RENTAL_COLLECTIONS]->getValue();
				$boolMustPassRentalCollection = $arrmixRentalCollection->{CScreeningCompanyPreferenceType::MUST_PASS_CREDIT_FACTORS_RENTAL_COLLECTIONS};
				if( true == $boolMustPassRentalCollection ) {
					$arrobjScreeningPropertyPreferences[] = $arrmixRentalCollection;
				}
			}
		}
		return $arrobjScreeningPropertyPreferences;
	}

	public function skipPendingTransactions( $intCurrentUserId, $intScreeningApplicantId = NULL, $intScreenTypeId = NULL ) {
		$arrobjScreeningApplicants = $this->getScreeningApplicants();

		if( false == valArr( $arrobjScreeningApplicants ) ) {
			trigger_error( 'Failed to load screening applicants for screening id : ' . $this->getId() . ' And cid : ' . $this->getCid(), E_USER_WARNING );
			return;
		}

		$this->getDatabase()->begin();
		$intAllApplicantCancelledCount          = 0;
		$arrmixScreeningTransactionSkipped      = array();
		$arrmixScreeningApplicantResult         = array();
		$intSubstractIncome = 0;

		$arrintScreeningPackageIds          = array_filter( array_keys( rekeyObjects( 'ScreeningPackageId', $arrobjScreeningApplicants ) ) );

		if( true == valArr( $arrintScreeningPackageIds ) ) {
			$arrmixScreeningApplicantPackages   = CScreeningPackages::fetchPublishedScreeningPackagesByIdsByCid( $arrintScreeningPackageIds, $this->getCid(), $this->getDatabase() );
		}

		foreach( $arrobjScreeningApplicants as $objScreeningApplicant ) {

			$arrobjScreeningTransactions = $objScreeningApplicant->getScreeningTransactions();

			if( false == valArr( $arrobjScreeningTransactions ) ) {
				trigger_error( 'Failed to screening transaction for screening id[' . $objScreeningApplicant->getScreeningId() . '] client id [' . $objScreeningApplicant->getCid() . ']', E_USER_WARNING );

				return false;
			}

			if( true == valId( $intScreeningApplicantId ) && $intScreeningApplicantId != $objScreeningApplicant->getId() ) {
				continue;
			}

			$intCancelledTransactionsCount                          = 0;
			$arrintSkippedScreenTypes                               = array();
			$arrintTransactionReviewIds                             = array();

            $arrmixScreeningApplicantTransactionsResult = [];
			foreach( $arrobjScreeningTransactions as $objScreeningTransaction ) {

				if( true == valId( $intScreenTypeId ) && $intScreenTypeId != $objScreeningTransaction->getScreenTypeId() ) {
					continue;
				}

				$arrmixScreeningApplicantTransactionsResult[$objScreeningTransaction->getScreenTypeId()]['ScreeningRecommendationTypeId']  = CScreeningRecommendationType::PENDING_UNKNOWN;
				$arrmixScreeningApplicantTransactionsResult[$objScreeningTransaction->getScreenTypeId()]['ScreeningStatusTypeId']          = CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED;

				switch( $objScreeningTransaction->getScreenTypeId() ) {
					case CScreenType::CREDIT:
					if( false == in_array( $objScreeningTransaction->getScreeningStatusTypeId(), CScreenType::$c_arrintSkipScreenTypeIds[CScreenType::CREDIT] ) ) break;
						if( true == is_null( $objScreeningTransaction->getScreeningOrderId() ) ) {
							$objScreeningTransaction->setTransactionAmount( 0 );
							$objScreeningTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );
						} else {
							$objScreeningTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED );
						}
						$objScreeningTransaction->setIsSkipped( true );

						if( true == valArr( $objScreeningTransaction->getChildTransactions() ) ) {
							foreach( $objScreeningTransaction->getChildTransactions() as $objChildTransaction ) {
								if( true == is_null( $objScreeningTransaction->getScreeningOrderId() ) ) {
									$objChildTransaction->setTransactionAmount( 0 );
									$objChildTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );
								} else {
									$objChildTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED );
								}
								$objChildTransaction->setIsSkipped( true );

								$arrintSkippedScreenTypes[] = CScreenType::getScreenTypeName( $objChildTransaction->getScreenTypeId() );

								if( false == $objChildTransaction->update( $intCurrentUserId, $this->getDatabase() ) ) {
									$this->getDatabase()->rollback();
								}
                                $arrmixScreeningApplicantTransactionsResult[$objChildTransaction->getScreenTypeId()]['ScreeningRecommendationTypeId']  = CScreeningRecommendationType::PENDING_UNKNOWN;
                                $arrmixScreeningApplicantTransactionsResult[$objChildTransaction->getScreenTypeId()]['ScreeningStatusTypeId']          = $objChildTransaction->getScreeningStatusTypeId();
							}
						}

						$intCancelledTransactionsCount++;
						$arrintSkippedScreenTypes[] = CScreenType::getScreenTypeName( $objScreeningTransaction->getScreenTypeId() );
						break;

					case CScreenType::STATE_CRIMINAL_SEARCH:
					case CScreenType::COUNTY_CRIMINAL_SEARCH:

						if( true == in_array( $objScreeningTransaction->getScreeningStatusTypeId(), [ CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN, CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR ] ) ) {
							$arrintTransactionReviewIds = $objScreeningTransaction->getTransactionReviewIds();
							if( true == is_null( $objScreeningTransaction->getScreeningOrderId() ) ) {
								$objScreeningTransaction->setTransactionAmount( 0 );
								$objScreeningTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );
							} else {
								$objScreeningTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED );
							}
							$objScreeningTransaction->setIsSkipped( true );

							if( true == valArr( $arrintTransactionReviewIds ) ) {
								CTransactionReviews::updateTransactionReviews( $arrintTransactionReviewIds, CTransactionReviewStatus::CANCELLED, $intCurrentUserId, $this->getCid(), $this->getDatabase(), $boolIsTransactionReviewReopened = false );
							}
							$intCancelledTransactionsCount++;
							$arrintSkippedScreenTypes[] = $objScreeningTransaction->getSearchType() . ' ' . CScreenType::getScreenTypeName( $objScreeningTransaction->getScreenTypeId() );
						}
						break;

                    case CScreenType::PRECISE_ID:
                        if( false == in_array( $objScreeningTransaction->getScreeningStatusTypeId(), CScreenType::$c_arrintSkipScreenTypeIds[CScreenType::PRECISE_ID] ) ) break;

                        $boolSetParentTransactionCancelled = false;
                        if( false == valArr( $objScreeningTransaction->getChildTransactions() ) ) {
                            $objScreeningTransaction->setTransactionAmount( 0 );
                            $boolSetParentTransactionCancelled = true;
                        } else {
                            $intChildTransactions = 0;
                            foreach( $objScreeningTransaction->getChildTransactions() as $objChildTransaction ) {
                                if( true == in_array( $objChildTransaction->getScreeningStatusTypeId(), CScreenType::$c_arrintSkipScreenTypeIds[CScreenType::PRECISE_ID] ) ) {
	                                if( true == is_null( $objScreeningTransaction->getScreeningOrderId() ) ) {
		                                $objChildTransaction->setTransactionAmount( 0 );
		                                $objChildTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );
	                                } else {
		                                $objChildTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED );
	                                }
                                    $objChildTransaction->setIsSkipped( true );

                                    $arrintSkippedScreenTypes[] = CScreenType::getScreenTypeName( $objChildTransaction->getScreenTypeId() );

                                    if( false == $objChildTransaction->update( $intCurrentUserId, $this->getDatabase() ) ) {
                                        $this->getDatabase()->rollback();
                                    }
                                    $arrmixScreeningApplicantTransactionsResult[$objChildTransaction->getScreenTypeId()]['ScreeningRecommendationTypeId']  = CScreeningRecommendationType::PENDING_UNKNOWN;
                                    $arrmixScreeningApplicantTransactionsResult[$objChildTransaction->getScreenTypeId()]['ScreeningStatusTypeId']          = $objChildTransaction->getScreeningStatusTypeId();
                                    $intChildTransactions += 1;
                                }
                            }

                            if( $intChildTransactions == \Psi\Libraries\UtilFunctions\count( $objScreeningTransaction->getChildTransactions() ) ) {
                                $boolSetParentTransactionCancelled = true;
                            } else {
                                $objScreeningTransaction = $objScreeningApplicant->evaluatePreciseIdTransactions( $intCurrentUserId, $this->getDatabase() );
                            }
                        }

                        if( true == $boolSetParentTransactionCancelled ) {
	                        if( true == is_null( $objScreeningTransaction->getScreeningOrderId() ) ) {
		                        $objScreeningTransaction->setTransactionAmount( 0 );
		                        $objScreeningTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );
	                        } else {
		                        $objScreeningTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED );
	                        }
	                        $objScreeningTransaction->setIsSkipped( true );
                            $intCancelledTransactionsCount++;
                        }
                        // As Precise Id screening transaction is returned after evaluation so here again adding log
                        $arrmixScreeningApplicantTransactionsResult[$objScreeningTransaction->getScreenTypeId()]['ScreeningRecommendationTypeId']  = $objScreeningTransaction->getScreeningRecommendationTypeId();
                        $arrmixScreeningApplicantTransactionsResult[$objScreeningTransaction->getScreenTypeId()]['ScreeningStatusTypeId']          = $objScreeningTransaction->getScreeningStatusTypeId();
                        $arrintSkippedScreenTypes[] = CScreenType::getScreenTypeName( $objScreeningTransaction->getScreenTypeId() );
                        break;

					case CScreenType::VERIFICATION_OF_INCOME:
						if( true == in_array( $objScreeningTransaction->getScreeningStatusTypeId(), CScreenType::$c_arrintSkipScreenTypeIds[CScreenType::VERIFICATION_OF_INCOME] ) ) {
							$objScreeningTransaction->setIsSkipped( true );

							if( true == is_null( $objScreeningTransaction->getScreeningOrderId() ) ) {
								$objScreeningTransaction->setTransactionAmount( 0 );
								$objScreeningTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );
							} else {
								$objScreeningTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED );
							}

							$intCancelledTransactionsCount++;
							$arrintSkippedScreenTypes[] = CScreenType::getScreenTypeName( $objScreeningTransaction->getScreenTypeId() );
						}
						break;

					default:
						if( CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR == $objScreeningTransaction->getScreeningStatusTypeId() ) {
							$objScreeningTransaction->setIsSkipped( true );

							if( true == is_null( $objScreeningTransaction->getScreeningOrderId() ) ) {
								$objScreeningTransaction->setTransactionAmount( 0 );
								$objScreeningTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );
							} else {
								$objScreeningTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED );
							}

							if( true == valArr( $objScreeningTransaction->getChildTransactions() ) ) {
								foreach( $objScreeningTransaction->getChildTransactions() as $objChildTransaction ) {

									if( true == is_null( $objScreeningTransaction->getScreeningOrderId() ) ) {
										$objChildTransaction->setTransactionAmount( 0 );
										$objChildTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );
									} else {
										$objChildTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED );
									}
									$objChildTransaction->setIsSkipped( true );

									$arrintSkippedScreenTypes[] = CScreenType::getScreenTypeName( $objChildTransaction->getScreenTypeId() );

									if( false == $objChildTransaction->update( $intCurrentUserId, $this->getDatabase() ) ) {
										$this->getDatabase()->rollback();
									}
									$arrmixScreeningApplicantTransactionsResult[$objChildTransaction->getScreenTypeId()]['ScreeningRecommendationTypeId']  = CScreeningRecommendationType::PENDING_UNKNOWN;
									$arrmixScreeningApplicantTransactionsResult[$objChildTransaction->getScreenTypeId()]['ScreeningStatusTypeId']          = $objChildTransaction->getScreeningStatusTypeId();
								}
							}

							if( true == valArr( $objScreeningTransaction->getTransactionReviewIds() ) ) {
								CTransactionReviews::updateTransactionReviews( $objScreeningTransaction->getTransactionReviewIds(), CTransactionReviewStatus::CANCELLED, $intCurrentUserId, $this->getCid(), $this->getDatabase(), $boolIsTransactionReviewReopened = false );
							}

							$intCancelledTransactionsCount++;
							$arrintSkippedScreenTypes[] = CScreenType::getScreenTypeName( $objScreeningTransaction->getScreenTypeId() );
						}
						break;
						// handle this
				}

				if( false == $objScreeningTransaction->update( $intCurrentUserId, $this->getDatabase() ) ) {
					$this->getDatabase()->rollback();
				}
			}

			if( $intCancelledTransactionsCount == \Psi\Libraries\UtilFunctions\count( $arrobjScreeningTransactions ) ) {

				$objScreeningApplicant->setStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );
				$objScreeningApplicant->setScreeningRecommendationTypeId( NULL );

				$intSubstractIncome += $objScreeningApplicant->getIncome();

				if( false == $objScreeningApplicant->update( $intCurrentUserId, $this->getDatabase() ) ) {
					$this->getDatabase()->rollback();
				}

				$intAllApplicantCancelledCount++;
			}

            $objStdScreeningApplicantResult         = new stdClass();
            $objStdScreeningApplicantResult->screeningApplicantStatusTypeId     = $objScreeningApplicant->getStatusTypeId();
            $objStdScreeningApplicantResult->screenTypeResult                   = $arrmixScreeningApplicantTransactionsResult;
            if( ( true == valArr( $arrmixScreeningApplicantPackages ) ) && ( true == array_key_exists( $objScreeningApplicant->getScreeningPackageId(), $arrmixScreeningApplicantPackages ) ) ) {
                $objStdScreeningApplicantResult->screeningPackageId             = $objScreeningApplicant->getScreeningPackageId();
                $objStdScreeningApplicantResult->screeningPackageName           = $arrmixScreeningApplicantPackages[$objScreeningApplicant->getScreeningPackageId()]->getName();
            }
            $objStdScreeningApplicantResult->screeningApplicantRecommendationId      = $objScreeningApplicant->getScreeningRecommendationTypeId();
            $arrmixScreeningApplicantResult[$objScreeningApplicant->getCustomApplicantId()]     = $objStdScreeningApplicantResult;

			if( true == valArr( $arrintSkippedScreenTypes ) ) {
				$strSkippedScreenTypeName = '';

				foreach( $arrintSkippedScreenTypes as $strScreenTypeName ) {
					if( false == valStr( $strSkippedScreenTypeName ) ) {
						$strSkippedScreenTypeName = $strScreenTypeName;
					} else {
						$strSkippedScreenTypeName .= ', ' . $strScreenTypeName;
					}

				}

				$arrmixScreeningTransactions                                                            = array();
				$arrmixScreeningTransactions['skippedScreenTypes']                                      = $strSkippedScreenTypeName;
				$arrmixScreeningTransactions['applicantFullName']                                       = $objScreeningApplicant->getNameFirst() . ' ' . $objScreeningApplicant->getNameLast();
				$arrmixScreeningTransactionSkipped[$objScreeningApplicant->getCustomApplicantId()]      = $arrmixScreeningTransactions;
			}

		}

		// subtract the applicant income from total household income if the applicant is cancelled
		if( $intSubstractIncome > 0 ) {
			$intTotalHouseholdIncome = $this->getTotalHouseholdIncome();
			$intTotalIncome = $intTotalHouseholdIncome - $intSubstractIncome;

			$this->setTotalHouseholdIncome( $intTotalIncome );
			if( false == $this->update( $intCurrentUserId, $this->getDatabase() ) ) {
				$this->getDatabase()->rollback();
			}
		}
		if( $intAllApplicantCancelledCount == \Psi\Libraries\UtilFunctions\count( $arrobjScreeningApplicants ) ) {

			$this->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );
			if( false == $this->update( $intCurrentUserId, $this->getDatabase() ) ) {
				$this->getDatabase()->rollback();
			}

		}

		$this->getDatabase()->commit();
		$arrmixSkippedTransactionDetails                                 = array();
		$arrmixSkippedTransactionDetails['ScreeningStatusTypeId']       = $this->getScreeningStatusTypeId();
		$arrmixSkippedTransactionDetails['isSkippedTransaction']        = true;
		$arrmixSkippedTransactionDetails['remoteScreeningId']           = $this->getId();
		$arrmixSkippedTransactionDetails['screeningApplicantResults']   = $arrmixScreeningApplicantResult;
		$arrmixSkippedTransactionDetails['screeningTransactionSkipped'] = $arrmixScreeningTransactionSkipped;

		return $arrmixSkippedTransactionDetails;
	}

	public function getOrFetchPropertyDetails( $objDatabase ) {
		return CScreeningPropertyAccounts::fetchPublishedScreeningPropertyAccountByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	/*
	 * ends here
	 */

    public function processPreciseIdScreening( $intCurrentUserId, $objStdServiceData, $objScreeningDatabase ) {
        $intCustomApplicantId = $objStdServiceData->application->applicants->applicantId;

        $arrobjScreeningApplicants = $this->getScreeningApplicants();
        $arrobjScreeningApplicants = rekeyObjects( 'CustomApplicantId', $arrobjScreeningApplicants );

        $objScreeningApplicant = getArrayElementByKey( $intCustomApplicantId, $arrobjScreeningApplicants );

        if( false == valObj( $objScreeningApplicant, 'CScreeningApplicant' ) ) {
            trigger_error( 'Failed to load screening applicants for application id : ' . $objStdServiceData->application->customApplicationId . ' and cid : ' . $objStdServiceData->authInfo->Cid, E_USER_WARNING );
            return false;
        }

        $objScreeningTransaction = $objScreeningApplicant->processPreciseIdTransactions( $intCurrentUserId, $objStdServiceData, $objScreeningDatabase );

        return $objScreeningTransaction;
    }

	public static function getScreeningRetentionPeriodMessage() {
		return self::SCREENING_RETENTION_PERIOD_MESSAGE;
	}

	public function getScreeningApplicantTransactionByApplicantIdByScreenTypeId( $intApplicantId, $intScreenType ) {

		$arrobjScreeningApplicants = $this->getScreeningApplicants();
		$arrobjScreeningApplicants = rekeyObjects( 'CustomApplicantId', $arrobjScreeningApplicants );

		$objScreeningApplicant = getArrayElementByKey( $intApplicantId, $arrobjScreeningApplicants );

		$arrobjScreeningTransactions = $objScreeningApplicant->getScreeningTransactions();
		$arrobjScreeningTransactions = rekeyObjects( 'ScreenTypeId', $arrobjScreeningTransactions );

		$arrmixResult['screening_applicant'] = $objScreeningApplicant;
		$arrmixResult['screening_applicant_transaction'] = getArrayElementByKey( $intScreenType, $arrobjScreeningTransactions );
		return $arrmixResult;
	}

    protected function checkSkipScreeningTransactions( $arrintScreeningApplicantIds, $objScreeningDatabase ) {
        $arrstrSkipScreenTypes = '';
        foreach( CScreenType::$c_arrintSkipScreenTypeIds as $intScreenTypeId => $arrintScreeningStatusTypeIds ) {
            foreach( $arrintScreeningStatusTypeIds as $intScreeningStatusTypeId ) {
                $arrstrSkipScreenTypes .= ( '' != $arrstrSkipScreenTypes ) ? ', ( ' . ( int ) $intScreenTypeId . ',' . $intScreeningStatusTypeId . ' )' : ' ( ' . ( int ) $intScreenTypeId . ',' . $intScreeningStatusTypeId . ' )';
            }
        }
        return ( int ) \Psi\Eos\Screening\CScreeningTransactions::createService()->fetchskipTransactionCountByScreeningIdByCidByScreeningApplicantIdByScreenTypeIds( $this->getId(), $this->getCid(), $arrintScreeningApplicantIds, $arrstrSkipScreenTypes, $objScreeningDatabase );
    }

	public function evaluateAttributeBasedApplicants( $intCurrentUserId, $objScreeningDatabase ) {
		if( false == valArr( $this->getScreeningApplicants() ) ) {
			trigger_error( 'Failed to load screening applicants', E_USER_WARNING );
			return false;
		}

		foreach( $this->getScreeningApplicants() as $objScreeningApplicant ) {
			if( false == valArr( $objScreeningApplicant->getScreeningTransactions() ) ) {
				trigger_error( 'Failed to load screening transactions for applicant id:' . $objScreeningApplicant->getId(), E_USER_WARNING );
				break;
			}

			$arrobjScreeningApplicantTransactions = rekeyObjects( 'ScreenTypeId', $objScreeningApplicant->getScreeningTransactions() );
			$objAttributeBasedTransaction = getArrayElementByKey( CScreenType::ATTRIBUTE_BASED, $arrobjScreeningApplicantTransactions );

			if( false == valObj( $objAttributeBasedTransaction, 'CScreeningTransaction' ) ) {
				continue;
			}

			$objAttributeBasedTransaction->processScreeningTransaction( $intCurrentUserId, $objScreeningApplicant );

			$objScreeningApplicant->updateScreeningApplicantResult( $intCurrentUserId, $objScreeningApplicant->fetchActiveScreeningApplicantTransactions( $objScreeningDatabase ), $objScreeningDatabase );
		}
		return true;
	}

}
?>