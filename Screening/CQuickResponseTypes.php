<?php

class CQuickResponseTypes extends CBaseQuickResponseTypes {

	public static function fetchQuickResponseTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CQuickResponseType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchQuickResponseType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CQuickResponseType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>