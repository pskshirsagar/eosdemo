<?php

class CCriminalClassificationSetting extends CBaseCriminalClassificationSetting {

	protected $m_intFelonyScreeningRecommendationTypeId;
	protected $m_intMisdemeanorScreeningRecommendationTypeId;

    public function setFelonyScreeningRecommendationTypeId( $intFelonyScreeningRecommendationTypeId ) {
    	$this->m_intFelonyScreeningRecommendationTypeId = $intFelonyScreeningRecommendationTypeId;
    }

    public function getFelonyScreeningRecommendationTypeId() {
    	return $this->m_intFelonyScreeningRecommendationTypeId;
    }

    public function setMisdemeanorScreeningRecommendationTypeId( $intMisdemeanorScreeningRecommendationTypeId ) {
		$this->m_intMisdemeanorScreeningRecommendationTypeId = $intMisdemeanorScreeningRecommendationTypeId;
    }

   	public function getMisdemeanorScreeningRecommendationTypeId() {
   		return $this->m_intMisdemeanorScreeningRecommendationTypeId;
   	}

    public function valCriminalClassificationTypeId( $objDatabase ) {
        $boolIsValid = true;

        if( true == is_null( $this->getCriminalClassificationTypeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'criminal_classification_type_id', __( 'Classification type is required.' ) ) );
        } else {
        	$objCriminalClassificationType = CCriminalClassificationTypes::fetchCriminalClassificationTypeById( $this->getCriminalClassificationTypeId(), $objDatabase );

        	if( false == valObj( $objCriminalClassificationType, 'CCriminalClassificationType' ) ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'criminal_classification_type_id', __( 'Classification type should be valid.' ) ) );
        	}
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCriminalClassificationTypeId( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	break;
        }

        return $boolIsValid;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['felony_screening_recommendation_type_id'] ) ) 		$this->setFelonyScreeningRecommendationTypeId( $arrmixValues['felony_screening_recommendation_type_id'] );
    	if( true == isset( $arrmixValues['misdemeanor_screening_recommendation_type_id'] ) ) 	$this->setMisdemeanorScreeningRecommendationTypeId( $arrmixValues['misdemeanor_screening_recommendation_type_id'] );

    	return;
    }

}
?>