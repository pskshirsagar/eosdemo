<?php

class CScreeningWorkflowStep extends CBaseScreeningWorkflowStep {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningWorkflowSetupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreenTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCriteriaId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNextWorkflowScreenTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>