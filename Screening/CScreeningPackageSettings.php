<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageSettings
 * Do not add any new functions to this class.
 */

class CScreeningPackageSettings extends CBaseScreeningPackageSettings {

	public static function fetchScreeningPackageSettingByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {

		$strSql = ' SELECT
							*
					FROM
						screening_package_settings
					WHERE
						screening_package_id = ' . ( int ) $intScreeningPackageId . '
					ORDER BY
    					id DESC';

		return parent::fetchScreeningPackageSettings( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageSettingsByScreeningConfigPreferenceTypeIdsByScreeningPackageId( $arrintScreeningConfigPreferenceTypeIds, $intScreeningPackageId, $objDatabase ) {

		if( false == valArr( $arrintScreeningConfigPreferenceTypeIds ) ) return NULL;

		$strSql = 'SELECT
						sps.*,
						spcs.evaluation_order,
					   	spcs.screening_recommendation_type_id,
						scpt.screening_config_value_type_id
				   FROM
						screening_package_settings sps
						JOIN screening_package_condition_sets spcs ON spcs.id = sps.screening_package_condition_set_id
						JOIN screening_config_preference_types scpt ON scpt.id = sps.screening_config_preference_type_id
				   WHERE
						sps.screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND scpt.id IN ( ' . implode( ',', $arrintScreeningConfigPreferenceTypeIds ) . ' )
				   ORDER BY
						spcs.evaluation_order';

		return parent::fetchScreeningPackageSettings( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageSettingsByScreeningPackageConditionSetIdByScreeningPackageId( $intScreeningPackageConditionSetId, $intScreeningPackageId, $objDatabase ) {

		$strSql = 'SELECT
					    sps.*
				   FROM
						screening_package_settings sps
				   WHERE
						sps.screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND sps.screening_package_condition_set_id = ' . ( int ) $intScreeningPackageConditionSetId;

		return parent::fetchScreeningPackageSettings( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageSettingsMinMaxDataByScreeningConfigPreferenceTypeIdsByScreeningPackageId( $arrintScreeningConfigPreferenceTypeIds, $intScreeningPackageId, $objDatabase ) {

		if( false == valArr( $arrintScreeningConfigPreferenceTypeIds ) ) return NULL;

		$strSql = 'SELECT
						sps.*
				   FROM
						screening_package_settings sps
						JOIN screening_package_condition_sets spcs ON spcs.id = sps.screening_package_condition_set_id
						JOIN screening_config_preference_types scpt ON scpt.id = sps.screening_config_preference_type_id
				   WHERE
						sps.screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND scpt.id IN ( ' . implode( ',', $arrintScreeningConfigPreferenceTypeIds ) . ' )
				   ORDER BY
						spcs.evaluation_order';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageSettingsByScreeningPackageIdByScreeningConfigPreferenceTypeIds( $intScreeningPackageId, $arrintScreeningConfigPreferenceTypeIds, $objDatabase ) {

		if( false == valArr( $arrintScreeningConfigPreferenceTypeIds ) ) return;

		$strSql = 'SELECT
						sps.*,
						spcs.screening_recommendation_type_id
					FROM
						screening_package_settings sps
						JOIN screening_package_condition_sets spcs ON spcs.id = sps.screening_package_condition_set_id
					WHERE
						sps.screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND sps.screening_config_preference_type_id IN( ' . implode( ',', $arrintScreeningConfigPreferenceTypeIds ) . ' )';

		return parent::fetchScreeningPackageSettings( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageSettingsAndScreenTypeIdByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {

		$strSql = ' SELECT
						sps.*,
						scpt.screen_type_id
					FROM
						screening_package_settings as sps
						INNER JOIN screening_config_preference_types as scpt ON sps.screening_config_preference_type_id = scpt.id
					WHERE
						sps.screening_package_id = ' . ( int ) $intScreeningPackageId . '
					ORDER BY
    					sps.id DESC';

		return parent::fetchScreeningPackageSettings( $strSql, $objDatabase );
	}

	public static function isPackageRentalCollectionEnabled( $intScreeningPackageId, $objDatabase ) {

		$arrintScreeningConfigPreferenceTypeIds = [ CScreeningConfigPreferenceType::RENTAL_COLLECTIONS, CScreeningConfigPreferenceType::RENTAL_COLLECTION_DOLLARS ];

		$strSql = '	SELECT
						count( screening_config_preference_type_id )
					FROM
						screening_package_settings
					WHERE
						screening_package_id = ' . ( int ) $intScreeningPackageId . ' 
					AND	screening_config_preference_type_id IN( ' . implode( ',', $arrintScreeningConfigPreferenceTypeIds ) . ' )';

		$arrstrData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrstrData ) && 0 < $arrstrData[0]['count'] ) ? true : false;
	}

}
?>