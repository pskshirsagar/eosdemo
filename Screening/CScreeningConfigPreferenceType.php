<?php

class CScreeningConfigPreferenceType extends CBaseScreeningConfigPreferenceType {

	const CREDIT_SCORE 									= 8;
	const INCOME_TO_RENT								= 9;
	const DEBT_TO_INCOME								= 10;
	const DEBT_WITH_RENT_TO_INCOME						= 11;
	const TOTAL_ACCOUNT 								= 12;
	const POSITIVE_ACCOUNTS 							= 13;
	const CURRENT_DELINQUENT							= 16;
	const PAST_DELINQUENT								= 17;
	const DELINQUENT_ACCOUNTS							= 18;
	const DELINQUENT_ACCOUNTS_PERCENT 					= 19;
	const COLLECTIONS									= 20;
	const RENTAL_COLLECTIONS							= 21;
	const UTILITY_COLLECTIONS							= 22;
	const PUBLIC_RECORDS								= 24;
	const BANKRUPTCY									= 28;
	const FORECLOSURE									= 29;
	const TOTAL_MONTHLY_LIABILITY 						= 30;
	const CREDIT_SCORE_MODEL_TYPE						= 31;
	const RV_INDEX_SCORE								= 32;
	const NO_CREDIT_RECORD								= 33;
	const CREDIT_ALERT_TYPE								= 34;
	const CREDIT_ERROR_INVALID_SSN 						= 35;
	const NO_CREDIT_SCORE								= 36;
	const BANKRUPTCY_RECENT_MONTHS						= 37;
	const FORECLOSURE_RECENT_MONTHS						= 38;
	const EVICTIONS_JUDGEMENT  							= 39;
	const EVICTIONS_FILED 								= 40;
	const EVICTIONS_DISMISSED							= 41;
	const EVICTIONS_ZERO_DOLLAR_JUDGEMENT				= 42;
	const ALLOFFENSES_WITHOUT_CRIMCAT 					= 45;
 	const TOTAL_CRIMINAL_RECORDS						= 44;
 	const TOTAL_EVICTION_RECORDS						= 43;
 	const FELONIES										= 46;
 	const NO_SSN										= 47;
 	const COLLECTION_PERCENTAGE							= 48;
 	const RENTAL_COLLECTION_DOLLARS						= 49;
 	const UTILITY_COLLECTION_DOLLARS					= 50;
 	const CREDIT_ALERT_FACTA							= 51;
 	const MEDICAL_COLLECTIONS 							= 52;
 	const MEDICAL_COLLECTIONS_DOLLAR_AMOUNT 			= 53;
 	const PUBLIC_RECORDS_DOLLARS 						= 54;
	const INTELLISCORE									= 55;
	const NO_TIN										= 56;
 	const NO_INTELLISCORE								= 57;
 	const NO_BUSINESS_CREDIT_RECORD						= 58;
 	const PREMIER_PROFILE_INTELLISCORE					= 59;
 	const PREMIER_PROFILE_FINANCIAL_STABILITY_RISK_SCORE = 60;
 	const PREMIER_PROFILE_NO_TIN						= 61;
 	const PREMIER_PROFILE_NO_INTELLISCORE				= 62;
 	const NO_BUSINESS_CREDIT_DETAILS					= 63;
 	const YEARS_IN_BUSINESS								= 64;
 	const ANNUAL_REVENUE								= 65;
 	const ORGANIZATION_SIZE								= 66;
 	const PREMIER_PROFILE_COLLECTIONS					= 67;
 	const PREMIER_PROFILE_JUDGEMENTS					= 68;
 	const PREMIER_PROFILE_BANKRUPTCY					= 69;
 	const EVICTION										= 70;
 	const EVICTION_NON_ZERO_DOLLAR						= 71;
 	const FRAUDULENT_ACTIVITY_IDENTIFICATION			= 72;
 	const FRAUDULENT_ACTIVITY_DATE_OF_BIRTH				= 73;
 	const FRAUDULENT_ACTIVITY_PROOF_OF_INCOME			= 74;
 	const FRAUDULENT_ACTIVITY_PROOF_OF_EMPLOYMENT		= 75;
 	const FRAUDULENT_ACTIVITY_PAYMENT					= 76;
 	const MOVED_OUT_WITH_BALANCE						= 77;
 	const SKIPS											= 78;
 	const NO_DO_NOT_RENT_RECORD							= 79;
 	const NO_EVICTION_SEARCH							= 80;

 	const TOTAL_LEASE_RECORDS							= 81;
 	const TOTAL_NEGATIVE_LEASE_RECORDS					= 82;
 	const LATE_COUNT									= 83;
 	const NSF_COUNT										= 84;
	const LMO_COUNT                                     = 85;
 	const ACTIVE_LEASE_ALERT							= 86;
 	const COLLECTION_COUNTS								= 87;
 	const TOTAL_COLLECTION_AMOUNT						= 88;
 	const NO_RENTAL_HISTORY_RECORDS						= 89;
 	const MINOR_CREDIT									= 90;
 	const MINOR_EVICTION								= 91;
 	const NO_RV_INDEX_SCORE								= 92;

 	const IS_VALID_DOCUMENT                             = 93;
	const FRAUD_SHIELD							        = 94;
	const MOVED_OUT_BALANCE_WITH_WRITE_OFF			    = 95;
	const NO_SCORE_RENT_TO_INCOME      					= 96;
	const EVICTIONS_ZERO_DOLLAR_FILING                  = 97;
	const REPLACE_FAILED_GUARANTOR                      = 98;
	const AO_SCORE                                      = 99;
	const PHONE_VERIFICATION                            = 100;
	const KIQ_SCORE                                     = 101;
    const ID_VERIFICATION                               = 102;
    const NO_KIQ_QUESTION                               = 103;
    const AO_SCORE_ALERT                                = 104;
    const NO_AO_SCORE                                   = 105;
    const NO_OTP_GENERATED                              = 106;
    const FROZEN_AO_SCORE                               = 107;
	const REQUIRE_ADDITIONAL_IDENTIFIER                 = 108;
	const CREDIT_FILE_LOCKED                            = 109;
	const TOTAL_COLLECTION_DOLLAR_AMOUNT                = 110;
	const LEASE_VIOLATION                               = 111;
	const NON_RENEWAL                                   = 112;
	const AVERAGE_VANTAGE_SCORE                         = 113;
	const EMPLOYER_UNVERIFIED                           = 114;
	const ADDRESS_UNVERIFIED                            = 115;
	const NAME_UNVERIFIED                               = 116;
	const NO_BANK_PROVIDED                              = 117;
	const INCOME_UNVERIFIED                             = 118;
	const REPORTED_INCOME_MATCH_PERCENTAGE              = 119;

 	public static $c_arrintNoSsnConfigMappedWithScreenType = array(
 		CScreenType::CREDIT => self::NO_SSN,
 		CScreenType::EVICTION => self::NO_EVICTION_SEARCH
 	);

 	public static $c_arrintMinorConfigMappedWithScreenType = array(
 		CScreenType::CREDIT => self::MINOR_CREDIT,
 		CScreenType::EVICTION => self::MINOR_EVICTION
 	);

 	public static $c_arrintConfigTypeMappedWithScreenType = array(
 		self::IS_VALID_DOCUMENT => CScreenType::PASSPORT_VISA_VERIFICATION
    );

	public static $c_arrintFlagConfigPreferenceTypes = [ self::BANKRUPTCY, self::FORECLOSURE, self::FRAUD_SHIELD, self::TOTAL_CRIMINAL_RECORDS, self::FELONIES ];

	public static $c_arrintVariableRangeConfigPreferenceTypes = [ self::RENTAL_COLLECTIONS, self::RENTAL_COLLECTION_DOLLARS, self::DELINQUENT_ACCOUNTS_PERCENT, self::COLLECTION_COUNTS, self::COLLECTIONS, self::EVICTION, self::EVICTIONS_JUDGEMENT, self::AO_SCORE, self::KIQ_SCORE ];

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
           		break;

            default:

        }

        return $boolIsValid;
    }

}
?>