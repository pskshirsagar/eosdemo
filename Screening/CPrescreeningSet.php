<?php

class CPrescreeningSet extends CBasePrescreeningSet {

    const SET1 = 1;
    const SET2 = 2;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public static function getPrescreeningSetName( $intPrescreeningSetId ) {
        $strPrescreeningSetName = '';

        switch( $intPrescreeningSetId ) {
            case self::SET1:
                $strPrescreeningSetName = 'Set1';
                break;

            case self::SET2:
                $strPrescreeningSetName = 'Set2';
                break;

            default:
                // default case
                break;
        }

        return $strPrescreeningSetName;
    }

}
?>