<?php

class CSelectionModeType extends CBaseSelectionModeType {

    const USER_SELECT             = 1;
    const AUTO_SELECT             = 2;
    const AUTO_SELECT_DISABLED    = 3;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

    public static function getSelectionModeTypeName( $intSelectionModeTypeId ) {

        $strSelectionModeTypeName = '';

        switch( $intSelectionModeTypeId ) {
            case self::USER_SELECT:
                $strSelectionModeTypeName = __( 'User Select' );
                break;

            case self::AUTO_SELECT:
                $strSelectionModeTypeName = __( 'Auto Select' );
                break;

            case self::AUTO_SELECT_DISABLED:
                $strSelectionModeTypeName = __( 'Auto Select Disabled' );
                break;

            default:
                // default case
                break;
        }

        return $strSelectionModeTypeName;
    }

}
?>