<?php

class CScreeningInvoiceDetail extends CBaseScreeningInvoiceDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningInvoiceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreenTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInvoiceAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningOrderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDuplicateTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>