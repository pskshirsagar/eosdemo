<?php

class CDataSource extends CBaseDataSource {

	const EXPERIAN 			= 1;
	const TRADEHOUSE_DATA 	= 2;
	const IDS 				= 3;
	const OMNI 		        = 4;
	const DEXTER 		    = 5;
	const SJV               = 6;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getDataSourceName( $intDataSourceId ) {
		$strDataSourceName = '';

		switch( $intDataSourceId ) {
			case self::EXPERIAN:
				$strDataSourceName = 'Experian';
				break;

			case self::TRADEHOUSE_DATA:
				$strDataSourceName = 'TradeHouse Data';
				break;

			case self::IDS:
				$strDataSourceName = 'IDS';
				break;

			case self::OMNI:
				$strDataSourceName = 'Omni';
				break;

			case self::DEXTER:
				$strDataSourceName = 'Dexter';
				break;

			case self::SJV:
				$strDataSourceName = 'SJV';
				break;

			default:
				// added default case
		}

		return $strDataSourceName;
	}

}
?>