<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CCannedResponses
 * Do not add any new functions to this class.
 */

class CCannedResponses extends CBaseCannedResponses {

	public static function fetchAllCannedResponses( $objDatabase ) {
		return self::fetchCannedResponses( sprintf( 'SELECT * FROM canned_responses WHERE is_published = TRUE ORDER BY id DESC' ), $objDatabase );
	}

	public static function fetchCannedResponseById( $intCannedResponseId, $objDatabase ) {
		return self::fetchCannedResponse( sprintf( 'SELECT * FROM canned_responses WHERE id = %d ORDER BY id DESC', $intCannedResponseId ), $objDatabase );
	}

}
?>