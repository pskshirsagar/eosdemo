<?php

class CScreeningLogType extends CBaseScreeningLogType {

	const STEP_ONE_COMPLETE                   = 1;
	const CONTINGENT_EMAIL_SEND               = 2;
	const STEP_TWO_INITIATE                   = 3;
	const STEP_TWO_COMPLETE                   = 4;
	const SEND_VERIFICATION_OF_INCOME_EMAIL   = 5;
	const RESEND_VERIFICATION_OF_INCOME_EMAIL = 6;
	const VOI_IFRAME_LOADED                   = 7;
	const VOI_VERIFICATION_PROCESS_ERROR      = 8;
	const VOI_VERIFICATION_PROCESS_CANCELLED  = 9;
	const VOI_VERIFICATION_COMPLETED          = 10;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getScreeningLogTypeText( $intScreeningLogTypeId ) {
		$strScreeningLogTypeText = '';

		switch( $intScreeningLogTypeId ) {
			case self::STEP_ONE_COMPLETE:
				$strScreeningLogTypeText = __( 'Step 1 Completed' );
				break;

			case self::CONTINGENT_EMAIL_SEND:
				$strScreeningLogTypeText = __( 'Contingent Approval Email Sent' );
				break;

			case self::STEP_TWO_INITIATE:
				$strScreeningLogTypeText = __( 'Step 2 Initiated' );
				break;

			case self::STEP_TWO_COMPLETE:
				$strScreeningLogTypeText = __( 'Step 2 Completed' );
				break;

			case self::SEND_VERIFICATION_OF_INCOME_EMAIL:
				$strScreeningLogTypeText = __( 'Send Verification of Income Email' );
				break;

			case self::RESEND_VERIFICATION_OF_INCOME_EMAIL:
				$strScreeningLogTypeText = __( 'Re-Send Verification of Income Email' );
				break;

			case self::VOI_IFRAME_LOADED:
				$strScreeningLogTypeText = __( 'VOI - iframe rendered successfully' );
				break;

			case self::VOI_VERIFICATION_COMPLETED:
				$strScreeningLogTypeText = __( 'VOI - verification completed' );
				break;

			case self::VOI_VERIFICATION_PROCESS_CANCELLED:
				$strScreeningLogTypeText = __( 'VOI - verification process cancelled by user' );
				break;

			case self::VOI_VERIFICATION_PROCESS_ERROR:
				$strScreeningLogTypeText = __( 'VOI - Got Error in verification process' );
				break;

			default:
				// added default case
		}

		return $strScreeningLogTypeText;
	}

}
?>
