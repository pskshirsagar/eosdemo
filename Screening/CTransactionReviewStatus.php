<?php

class CTransactionReviewStatus extends CBaseTransactionReviewStatus {

	const REVIEW_STATUS_NEW	= 1;
	const IN_PROGRESS 		= 2;
	const UNDER_REVIEW 		= 3;
	const COMPLETE 			= 4;
	const CANCELLED         = 5;

	public static $c_arrintNonCompleteTransactionReviewStatuses = array(
	    self::REVIEW_STATUS_NEW,
	    self::IN_PROGRESS,
	    self::UNDER_REVIEW
	);

	public static $c_arrintTransactionReviewStatuses = array(
	    self::REVIEW_STATUS_NEW,
	    self::UNDER_REVIEW,
	    self::IN_PROGRESS,
	    self::COMPLETE
	);

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getTransactionReviewStatusName( $intTransactionReviewStatusId ) {

		$strStatusName = '';

		switch( $intTransactionReviewStatusId ) {
			case self::REVIEW_STATUS_NEW:
				$strStatusName = 'Pending Review';
				break;

			case self::COMPLETE:
				$strStatusName = 'Complete';
				break;

			case self::UNDER_REVIEW:
				$strStatusName = 'Under Review';
				break;

			default:
				$strStatusName = 'In progress';
				break;
		}

		return $strStatusName;
	}

}
?>