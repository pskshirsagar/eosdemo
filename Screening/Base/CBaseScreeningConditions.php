<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConditions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningConditions extends CEosPluralBase {

	/**
	 * @return CScreeningCondition[]
	 */
	public static function fetchScreeningConditions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningCondition', $objDatabase );
	}

	/**
	 * @return CScreeningCondition
	 */
	public static function fetchScreeningCondition( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningCondition', $objDatabase );
	}

	public static function fetchScreeningConditionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_conditions', $objDatabase );
	}

	public static function fetchScreeningConditionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningCondition( sprintf( 'SELECT * FROM screening_conditions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningConditionsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningConditions( sprintf( 'SELECT * FROM screening_conditions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningConditionsByScreeningIdByCid( $intScreeningId, $intCid, $objDatabase ) {
		return self::fetchScreeningConditions( sprintf( 'SELECT * FROM screening_conditions WHERE screening_id = %d AND cid = %d', ( int ) $intScreeningId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningConditionsByScreeningPackageConditionSetIdByCid( $intScreeningPackageConditionSetId, $intCid, $objDatabase ) {
		return self::fetchScreeningConditions( sprintf( 'SELECT * FROM screening_conditions WHERE screening_package_condition_set_id = %d AND cid = %d', ( int ) $intScreeningPackageConditionSetId, ( int ) $intCid ), $objDatabase );
	}

}
?>