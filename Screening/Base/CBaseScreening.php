<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreening
 * Do not add any new functions to this class.
 */

class CBaseScreening extends CEosSingularBase {

	const TABLE_NAME = 'public.screenings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intScreeningConfigurationId;
	protected $m_intTransmissionId;
	protected $m_intCustomApplicationId;
	protected $m_intApplicationTypeId;
	protected $m_intScreeningStatusTypeId;
	protected $m_intOriginalScreeningRecommendationTypeId;
	protected $m_intScreeningRecommendationTypeId;
	protected $m_intScreeningDecisionTypeId;
	protected $m_intRent;
	protected $m_fltRentCredit;
	protected $m_fltRvIndex;
	protected $m_fltTotalHouseholdIncome;
	protected $m_strDecisionOn;
	protected $m_strScreeningCompletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intLeadSourceId;
	protected $m_intGroupResultOverriddenBy;
	protected $m_intLeaseId;
	protected $m_intLeaseIntervalTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_intApplicationTypeId = '1';
		$this->m_intRent = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['screening_configuration_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningConfigurationId', trim( $arrValues['screening_configuration_id'] ) ); elseif( isset( $arrValues['screening_configuration_id'] ) ) $this->setScreeningConfigurationId( $arrValues['screening_configuration_id'] );
		if( isset( $arrValues['transmission_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionId', trim( $arrValues['transmission_id'] ) ); elseif( isset( $arrValues['transmission_id'] ) ) $this->setTransmissionId( $arrValues['transmission_id'] );
		if( isset( $arrValues['custom_application_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomApplicationId', trim( $arrValues['custom_application_id'] ) ); elseif( isset( $arrValues['custom_application_id'] ) ) $this->setCustomApplicationId( $arrValues['custom_application_id'] );
		if( isset( $arrValues['application_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationTypeId', trim( $arrValues['application_type_id'] ) ); elseif( isset( $arrValues['application_type_id'] ) ) $this->setApplicationTypeId( $arrValues['application_type_id'] );
		if( isset( $arrValues['screening_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningStatusTypeId', trim( $arrValues['screening_status_type_id'] ) ); elseif( isset( $arrValues['screening_status_type_id'] ) ) $this->setScreeningStatusTypeId( $arrValues['screening_status_type_id'] );
		if( isset( $arrValues['original_screening_recommendation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalScreeningRecommendationTypeId', trim( $arrValues['original_screening_recommendation_type_id'] ) ); elseif( isset( $arrValues['original_screening_recommendation_type_id'] ) ) $this->setOriginalScreeningRecommendationTypeId( $arrValues['original_screening_recommendation_type_id'] );
		if( isset( $arrValues['screening_recommendation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningRecommendationTypeId', trim( $arrValues['screening_recommendation_type_id'] ) ); elseif( isset( $arrValues['screening_recommendation_type_id'] ) ) $this->setScreeningRecommendationTypeId( $arrValues['screening_recommendation_type_id'] );
		if( isset( $arrValues['screening_decision_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningDecisionTypeId', trim( $arrValues['screening_decision_type_id'] ) ); elseif( isset( $arrValues['screening_decision_type_id'] ) ) $this->setScreeningDecisionTypeId( $arrValues['screening_decision_type_id'] );
		if( isset( $arrValues['rent'] ) && $boolDirectSet ) $this->set( 'm_intRent', trim( $arrValues['rent'] ) ); elseif( isset( $arrValues['rent'] ) ) $this->setRent( $arrValues['rent'] );
		if( isset( $arrValues['rent_credit'] ) && $boolDirectSet ) $this->set( 'm_fltRentCredit', trim( $arrValues['rent_credit'] ) ); elseif( isset( $arrValues['rent_credit'] ) ) $this->setRentCredit( $arrValues['rent_credit'] );
		if( isset( $arrValues['rv_index'] ) && $boolDirectSet ) $this->set( 'm_fltRvIndex', trim( $arrValues['rv_index'] ) ); elseif( isset( $arrValues['rv_index'] ) ) $this->setRvIndex( $arrValues['rv_index'] );
		if( isset( $arrValues['total_household_income'] ) && $boolDirectSet ) $this->set( 'm_fltTotalHouseholdIncome', trim( $arrValues['total_household_income'] ) ); elseif( isset( $arrValues['total_household_income'] ) ) $this->setTotalHouseholdIncome( $arrValues['total_household_income'] );
		if( isset( $arrValues['decision_on'] ) && $boolDirectSet ) $this->set( 'm_strDecisionOn', trim( $arrValues['decision_on'] ) ); elseif( isset( $arrValues['decision_on'] ) ) $this->setDecisionOn( $arrValues['decision_on'] );
		if( isset( $arrValues['screening_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strScreeningCompletedOn', trim( $arrValues['screening_completed_on'] ) ); elseif( isset( $arrValues['screening_completed_on'] ) ) $this->setScreeningCompletedOn( $arrValues['screening_completed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['lead_source_id'] ) && $boolDirectSet ) $this->set( 'm_intLeadSourceId', trim( $arrValues['lead_source_id'] ) ); elseif( isset( $arrValues['lead_source_id'] ) ) $this->setLeadSourceId( $arrValues['lead_source_id'] );
		if( isset( $arrValues['group_result_overridden_by'] ) && $boolDirectSet ) $this->set( 'm_intGroupResultOverriddenBy', trim( $arrValues['group_result_overridden_by'] ) ); elseif( isset( $arrValues['group_result_overridden_by'] ) ) $this->setGroupResultOverriddenBy( $arrValues['group_result_overridden_by'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['lease_interval_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalTypeId', trim( $arrValues['lease_interval_type_id'] ) ); elseif( isset( $arrValues['lease_interval_type_id'] ) ) $this->setLeaseIntervalTypeId( $arrValues['lease_interval_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setScreeningConfigurationId( $intScreeningConfigurationId ) {
		$this->set( 'm_intScreeningConfigurationId', CStrings::strToIntDef( $intScreeningConfigurationId, NULL, false ) );
	}

	public function getScreeningConfigurationId() {
		return $this->m_intScreeningConfigurationId;
	}

	public function sqlScreeningConfigurationId() {
		return ( true == isset( $this->m_intScreeningConfigurationId ) ) ? ( string ) $this->m_intScreeningConfigurationId : 'NULL';
	}

	public function setTransmissionId( $intTransmissionId ) {
		$this->set( 'm_intTransmissionId', CStrings::strToIntDef( $intTransmissionId, NULL, false ) );
	}

	public function getTransmissionId() {
		return $this->m_intTransmissionId;
	}

	public function sqlTransmissionId() {
		return ( true == isset( $this->m_intTransmissionId ) ) ? ( string ) $this->m_intTransmissionId : 'NULL';
	}

	public function setCustomApplicationId( $intCustomApplicationId ) {
		$this->set( 'm_intCustomApplicationId', CStrings::strToIntDef( $intCustomApplicationId, NULL, false ) );
	}

	public function getCustomApplicationId() {
		return $this->m_intCustomApplicationId;
	}

	public function sqlCustomApplicationId() {
		return ( true == isset( $this->m_intCustomApplicationId ) ) ? ( string ) $this->m_intCustomApplicationId : 'NULL';
	}

	public function setApplicationTypeId( $intApplicationTypeId ) {
		$this->set( 'm_intApplicationTypeId', CStrings::strToIntDef( $intApplicationTypeId, NULL, false ) );
	}

	public function getApplicationTypeId() {
		return $this->m_intApplicationTypeId;
	}

	public function sqlApplicationTypeId() {
		return ( true == isset( $this->m_intApplicationTypeId ) ) ? ( string ) $this->m_intApplicationTypeId : '1';
	}

	public function setScreeningStatusTypeId( $intScreeningStatusTypeId ) {
		$this->set( 'm_intScreeningStatusTypeId', CStrings::strToIntDef( $intScreeningStatusTypeId, NULL, false ) );
	}

	public function getScreeningStatusTypeId() {
		return $this->m_intScreeningStatusTypeId;
	}

	public function sqlScreeningStatusTypeId() {
		return ( true == isset( $this->m_intScreeningStatusTypeId ) ) ? ( string ) $this->m_intScreeningStatusTypeId : 'NULL';
	}

	public function setOriginalScreeningRecommendationTypeId( $intOriginalScreeningRecommendationTypeId ) {
		$this->set( 'm_intOriginalScreeningRecommendationTypeId', CStrings::strToIntDef( $intOriginalScreeningRecommendationTypeId, NULL, false ) );
	}

	public function getOriginalScreeningRecommendationTypeId() {
		return $this->m_intOriginalScreeningRecommendationTypeId;
	}

	public function sqlOriginalScreeningRecommendationTypeId() {
		return ( true == isset( $this->m_intOriginalScreeningRecommendationTypeId ) ) ? ( string ) $this->m_intOriginalScreeningRecommendationTypeId : 'NULL';
	}

	public function setScreeningRecommendationTypeId( $intScreeningRecommendationTypeId ) {
		$this->set( 'm_intScreeningRecommendationTypeId', CStrings::strToIntDef( $intScreeningRecommendationTypeId, NULL, false ) );
	}

	public function getScreeningRecommendationTypeId() {
		return $this->m_intScreeningRecommendationTypeId;
	}

	public function sqlScreeningRecommendationTypeId() {
		return ( true == isset( $this->m_intScreeningRecommendationTypeId ) ) ? ( string ) $this->m_intScreeningRecommendationTypeId : 'NULL';
	}

	public function setScreeningDecisionTypeId( $intScreeningDecisionTypeId ) {
		$this->set( 'm_intScreeningDecisionTypeId', CStrings::strToIntDef( $intScreeningDecisionTypeId, NULL, false ) );
	}

	public function getScreeningDecisionTypeId() {
		return $this->m_intScreeningDecisionTypeId;
	}

	public function sqlScreeningDecisionTypeId() {
		return ( true == isset( $this->m_intScreeningDecisionTypeId ) ) ? ( string ) $this->m_intScreeningDecisionTypeId : 'NULL';
	}

	public function setRent( $intRent ) {
		$this->set( 'm_intRent', CStrings::strToIntDef( $intRent, NULL, false ) );
	}

	public function getRent() {
		return $this->m_intRent;
	}

	public function sqlRent() {
		return ( true == isset( $this->m_intRent ) ) ? ( string ) $this->m_intRent : '0';
	}

	public function setRentCredit( $fltRentCredit ) {
		$this->set( 'm_fltRentCredit', CStrings::strToFloatDef( $fltRentCredit, NULL, false, 3 ) );
	}

	public function getRentCredit() {
		return $this->m_fltRentCredit;
	}

	public function sqlRentCredit() {
		return ( true == isset( $this->m_fltRentCredit ) ) ? ( string ) $this->m_fltRentCredit : 'NULL';
	}

	public function setRvIndex( $fltRvIndex ) {
		$this->set( 'm_fltRvIndex', CStrings::strToFloatDef( $fltRvIndex, NULL, false, 3 ) );
	}

	public function getRvIndex() {
		return $this->m_fltRvIndex;
	}

	public function sqlRvIndex() {
		return ( true == isset( $this->m_fltRvIndex ) ) ? ( string ) $this->m_fltRvIndex : 'NULL';
	}

	public function setTotalHouseholdIncome( $fltTotalHouseholdIncome ) {
		$this->set( 'm_fltTotalHouseholdIncome', CStrings::strToFloatDef( $fltTotalHouseholdIncome, NULL, false, 4 ) );
	}

	public function getTotalHouseholdIncome() {
		return $this->m_fltTotalHouseholdIncome;
	}

	public function sqlTotalHouseholdIncome() {
		return ( true == isset( $this->m_fltTotalHouseholdIncome ) ) ? ( string ) $this->m_fltTotalHouseholdIncome : 'NULL';
	}

	public function setDecisionOn( $strDecisionOn ) {
		$this->set( 'm_strDecisionOn', CStrings::strTrimDef( $strDecisionOn, -1, NULL, true ) );
	}

	public function getDecisionOn() {
		return $this->m_strDecisionOn;
	}

	public function sqlDecisionOn() {
		return ( true == isset( $this->m_strDecisionOn ) ) ? '\'' . $this->m_strDecisionOn . '\'' : 'NULL';
	}

	public function setScreeningCompletedOn( $strScreeningCompletedOn ) {
		$this->set( 'm_strScreeningCompletedOn', CStrings::strTrimDef( $strScreeningCompletedOn, -1, NULL, true ) );
	}

	public function getScreeningCompletedOn() {
		return $this->m_strScreeningCompletedOn;
	}

	public function sqlScreeningCompletedOn() {
		return ( true == isset( $this->m_strScreeningCompletedOn ) ) ? '\'' . $this->m_strScreeningCompletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setLeadSourceId( $intLeadSourceId ) {
		$this->set( 'm_intLeadSourceId', CStrings::strToIntDef( $intLeadSourceId, NULL, false ) );
	}

	public function getLeadSourceId() {
		return $this->m_intLeadSourceId;
	}

	public function sqlLeadSourceId() {
		return ( true == isset( $this->m_intLeadSourceId ) ) ? ( string ) $this->m_intLeadSourceId : 'NULL';
	}

	public function setGroupResultOverriddenBy( $intGroupResultOverriddenBy ) {
		$this->set( 'm_intGroupResultOverriddenBy', CStrings::strToIntDef( $intGroupResultOverriddenBy, NULL, false ) );
	}

	public function getGroupResultOverriddenBy() {
		return $this->m_intGroupResultOverriddenBy;
	}

	public function sqlGroupResultOverriddenBy() {
		return ( true == isset( $this->m_intGroupResultOverriddenBy ) ) ? ( string ) $this->m_intGroupResultOverriddenBy : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
		$this->set( 'm_intLeaseIntervalTypeId', CStrings::strToIntDef( $intLeaseIntervalTypeId, NULL, false ) );
	}

	public function getLeaseIntervalTypeId() {
		return $this->m_intLeaseIntervalTypeId;
	}

	public function sqlLeaseIntervalTypeId() {
		return ( true == isset( $this->m_intLeaseIntervalTypeId ) ) ? ( string ) $this->m_intLeaseIntervalTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, screening_configuration_id, transmission_id, custom_application_id, application_type_id, screening_status_type_id, original_screening_recommendation_type_id, screening_recommendation_type_id, screening_decision_type_id, rent, rent_credit, rv_index, total_household_income, decision_on, screening_completed_on, updated_by, updated_on, created_by, created_on, lead_source_id, group_result_overridden_by, lease_id, lease_interval_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlScreeningConfigurationId() . ', ' .
						$this->sqlTransmissionId() . ', ' .
						$this->sqlCustomApplicationId() . ', ' .
						$this->sqlApplicationTypeId() . ', ' .
						$this->sqlScreeningStatusTypeId() . ', ' .
						$this->sqlOriginalScreeningRecommendationTypeId() . ', ' .
						$this->sqlScreeningRecommendationTypeId() . ', ' .
						$this->sqlScreeningDecisionTypeId() . ', ' .
						$this->sqlRent() . ', ' .
						$this->sqlRentCredit() . ', ' .
						$this->sqlRvIndex() . ', ' .
						$this->sqlTotalHouseholdIncome() . ', ' .
						$this->sqlDecisionOn() . ', ' .
						$this->sqlScreeningCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlLeadSourceId() . ', ' .
						$this->sqlGroupResultOverriddenBy() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlLeaseIntervalTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_configuration_id = ' . $this->sqlScreeningConfigurationId(). ',' ; } elseif( true == array_key_exists( 'ScreeningConfigurationId', $this->getChangedColumns() ) ) { $strSql .= ' screening_configuration_id = ' . $this->sqlScreeningConfigurationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_id = ' . $this->sqlTransmissionId(). ',' ; } elseif( true == array_key_exists( 'TransmissionId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_id = ' . $this->sqlTransmissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_application_id = ' . $this->sqlCustomApplicationId(). ',' ; } elseif( true == array_key_exists( 'CustomApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' custom_application_id = ' . $this->sqlCustomApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_type_id = ' . $this->sqlApplicationTypeId(). ',' ; } elseif( true == array_key_exists( 'ApplicationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' application_type_id = ' . $this->sqlApplicationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_status_type_id = ' . $this->sqlScreeningStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_status_type_id = ' . $this->sqlScreeningStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_screening_recommendation_type_id = ' . $this->sqlOriginalScreeningRecommendationTypeId(). ',' ; } elseif( true == array_key_exists( 'OriginalScreeningRecommendationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' original_screening_recommendation_type_id = ' . $this->sqlOriginalScreeningRecommendationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningRecommendationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_decision_type_id = ' . $this->sqlScreeningDecisionTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningDecisionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_decision_type_id = ' . $this->sqlScreeningDecisionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent = ' . $this->sqlRent(). ',' ; } elseif( true == array_key_exists( 'Rent', $this->getChangedColumns() ) ) { $strSql .= ' rent = ' . $this->sqlRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent_credit = ' . $this->sqlRentCredit(). ',' ; } elseif( true == array_key_exists( 'RentCredit', $this->getChangedColumns() ) ) { $strSql .= ' rent_credit = ' . $this->sqlRentCredit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rv_index = ' . $this->sqlRvIndex(). ',' ; } elseif( true == array_key_exists( 'RvIndex', $this->getChangedColumns() ) ) { $strSql .= ' rv_index = ' . $this->sqlRvIndex() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_household_income = ' . $this->sqlTotalHouseholdIncome(). ',' ; } elseif( true == array_key_exists( 'TotalHouseholdIncome', $this->getChangedColumns() ) ) { $strSql .= ' total_household_income = ' . $this->sqlTotalHouseholdIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' decision_on = ' . $this->sqlDecisionOn(). ',' ; } elseif( true == array_key_exists( 'DecisionOn', $this->getChangedColumns() ) ) { $strSql .= ' decision_on = ' . $this->sqlDecisionOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_completed_on = ' . $this->sqlScreeningCompletedOn(). ',' ; } elseif( true == array_key_exists( 'ScreeningCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' screening_completed_on = ' . $this->sqlScreeningCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_source_id = ' . $this->sqlLeadSourceId(). ',' ; } elseif( true == array_key_exists( 'LeadSourceId', $this->getChangedColumns() ) ) { $strSql .= ' lead_source_id = ' . $this->sqlLeadSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' group_result_overridden_by = ' . $this->sqlGroupResultOverriddenBy(). ',' ; } elseif( true == array_key_exists( 'GroupResultOverriddenBy', $this->getChangedColumns() ) ) { $strSql .= ' group_result_overridden_by = ' . $this->sqlGroupResultOverriddenBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_type_id = ' . $this->sqlLeaseIntervalTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_type_id = ' . $this->sqlLeaseIntervalTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'screening_configuration_id' => $this->getScreeningConfigurationId(),
			'transmission_id' => $this->getTransmissionId(),
			'custom_application_id' => $this->getCustomApplicationId(),
			'application_type_id' => $this->getApplicationTypeId(),
			'screening_status_type_id' => $this->getScreeningStatusTypeId(),
			'original_screening_recommendation_type_id' => $this->getOriginalScreeningRecommendationTypeId(),
			'screening_recommendation_type_id' => $this->getScreeningRecommendationTypeId(),
			'screening_decision_type_id' => $this->getScreeningDecisionTypeId(),
			'rent' => $this->getRent(),
			'rent_credit' => $this->getRentCredit(),
			'rv_index' => $this->getRvIndex(),
			'total_household_income' => $this->getTotalHouseholdIncome(),
			'decision_on' => $this->getDecisionOn(),
			'screening_completed_on' => $this->getScreeningCompletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'lead_source_id' => $this->getLeadSourceId(),
			'group_result_overridden_by' => $this->getGroupResultOverriddenBy(),
			'lease_id' => $this->getLeaseId(),
			'lease_interval_type_id' => $this->getLeaseIntervalTypeId()
		);
	}

}
?>