<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningApplicantConfigurations
 * Do not add any new functions to this class.
 */

class CBaseScreeningApplicantConfigurations extends CEosPluralBase {

	/**
	 * @return CScreeningApplicantConfiguration[]
	 */
	public static function fetchScreeningApplicantConfigurations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningApplicantConfiguration', $objDatabase );
	}

	/**
	 * @return CScreeningApplicantConfiguration
	 */
	public static function fetchScreeningApplicantConfiguration( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningApplicantConfiguration', $objDatabase );
	}

	public static function fetchScreeningApplicantConfigurationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_applicant_configurations', $objDatabase );
	}

	public static function fetchScreeningApplicantConfigurationById( $intId, $objDatabase ) {
		return self::fetchScreeningApplicantConfiguration( sprintf( 'SELECT * FROM screening_applicant_configurations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningApplicantConfigurationsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningApplicantConfigurations( sprintf( 'SELECT * FROM screening_applicant_configurations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantConfigurationsByScreeningConfigurationId( $intScreeningConfigurationId, $objDatabase ) {
		return self::fetchScreeningApplicantConfigurations( sprintf( 'SELECT * FROM screening_applicant_configurations WHERE screening_configuration_id = %d', ( int ) $intScreeningConfigurationId ), $objDatabase );
	}

	public static function fetchScreeningApplicantConfigurationsByScreeningApplicantTypeId( $intScreeningApplicantTypeId, $objDatabase ) {
		return self::fetchScreeningApplicantConfigurations( sprintf( 'SELECT * FROM screening_applicant_configurations WHERE screening_applicant_type_id = %d', ( int ) $intScreeningApplicantTypeId ), $objDatabase );
	}

	public static function fetchScreeningApplicantConfigurationsByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreeningApplicantConfigurations( sprintf( 'SELECT * FROM screening_applicant_configurations WHERE screen_type_id = %d', ( int ) $intScreenTypeId ), $objDatabase );
	}

	public static function fetchScreeningApplicantConfigurationsByScreeningManualCriminalActionTypeId( $intScreeningManualCriminalActionTypeId, $objDatabase ) {
		return self::fetchScreeningApplicantConfigurations( sprintf( 'SELECT * FROM screening_applicant_configurations WHERE screening_manual_criminal_action_type_id = %d', ( int ) $intScreeningManualCriminalActionTypeId ), $objDatabase );
	}

}
?>