<?php

class CBaseSubscriptionBillingConsumptions extends CEosPluralBase {

	/**
	 * @return CSubscriptionBillingConsumption[]
	 */
	public static function fetchSubscriptionBillingConsumptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubscriptionBillingConsumption::class, $objDatabase );
	}

	/**
	 * @return CSubscriptionBillingConsumption
	 */
	public static function fetchSubscriptionBillingConsumption( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubscriptionBillingConsumption::class, $objDatabase );
	}

	public static function fetchSubscriptionBillingConsumptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subscription_billing_consumptions', $objDatabase );
	}

	public static function fetchSubscriptionBillingConsumptionById( $intId, $objDatabase ) {
		return self::fetchSubscriptionBillingConsumption( sprintf( 'SELECT * FROM subscription_billing_consumptions WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchSubscriptionBillingConsumptionsByCid( $intCid, $objDatabase ) {
		return self::fetchSubscriptionBillingConsumptions( sprintf( 'SELECT * FROM subscription_billing_consumptions WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchSubscriptionBillingConsumptionsBySubscriptionBillingContractPropertyId( $intSubscriptionBillingContractPropertyId, $objDatabase ) {
		return self::fetchSubscriptionBillingConsumptions( sprintf( 'SELECT * FROM subscription_billing_consumptions WHERE subscription_billing_contract_property_id = %d', $intSubscriptionBillingContractPropertyId ), $objDatabase );
	}

	public static function fetchSubscriptionBillingConsumptionsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchSubscriptionBillingConsumptions( sprintf( 'SELECT * FROM subscription_billing_consumptions WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchSubscriptionBillingConsumptionsByBillingAccountId( $intBillingAccountId, $objDatabase ) {
		return self::fetchSubscriptionBillingConsumptions( sprintf( 'SELECT * FROM subscription_billing_consumptions WHERE billing_account_id = %d', $intBillingAccountId ), $objDatabase );
	}

}
?>