<?php

class CBaseScreeningInvoiceSummaryTransaction extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_invoice_summary_transactions';

	protected $m_intId;
	protected $m_intScreenTypeId;
	protected $m_intScreeningInvoiceId;
	protected $m_intTransactionChargeTypeId;
	protected $m_intTotalTransactionCount;
	protected $m_fltTotalInvoiceAmount;
	protected $m_fltTotalTransactionAmount;
	protected $m_intReconciliationStatusTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_fltTotalInvoiceAmount = '0';
		$this->m_fltTotalTransactionAmount = '0';
		$this->m_intReconciliationStatusTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screen_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreenTypeId', trim( $arrValues['screen_type_id'] ) ); elseif( isset( $arrValues['screen_type_id'] ) ) $this->setScreenTypeId( $arrValues['screen_type_id'] );
		if( isset( $arrValues['screening_invoice_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningInvoiceId', trim( $arrValues['screening_invoice_id'] ) ); elseif( isset( $arrValues['screening_invoice_id'] ) ) $this->setScreeningInvoiceId( $arrValues['screening_invoice_id'] );
		if( isset( $arrValues['transaction_charge_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionChargeTypeId', trim( $arrValues['transaction_charge_type_id'] ) ); elseif( isset( $arrValues['transaction_charge_type_id'] ) ) $this->setTransactionChargeTypeId( $arrValues['transaction_charge_type_id'] );
		if( isset( $arrValues['total_transaction_count'] ) && $boolDirectSet ) $this->set( 'm_intTotalTransactionCount', trim( $arrValues['total_transaction_count'] ) ); elseif( isset( $arrValues['total_transaction_count'] ) ) $this->setTotalTransactionCount( $arrValues['total_transaction_count'] );
		if( isset( $arrValues['total_invoice_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalInvoiceAmount', trim( $arrValues['total_invoice_amount'] ) ); elseif( isset( $arrValues['total_invoice_amount'] ) ) $this->setTotalInvoiceAmount( $arrValues['total_invoice_amount'] );
		if( isset( $arrValues['total_transaction_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalTransactionAmount', trim( $arrValues['total_transaction_amount'] ) ); elseif( isset( $arrValues['total_transaction_amount'] ) ) $this->setTotalTransactionAmount( $arrValues['total_transaction_amount'] );
		if( isset( $arrValues['reconciliation_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReconciliationStatusTypeId', trim( $arrValues['reconciliation_status_type_id'] ) ); elseif( isset( $arrValues['reconciliation_status_type_id'] ) ) $this->setReconciliationStatusTypeId( $arrValues['reconciliation_status_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreenTypeId( $intScreenTypeId ) {
		$this->set( 'm_intScreenTypeId', CStrings::strToIntDef( $intScreenTypeId, NULL, false ) );
	}

	public function getScreenTypeId() {
		return $this->m_intScreenTypeId;
	}

	public function sqlScreenTypeId() {
		return ( true == isset( $this->m_intScreenTypeId ) ) ? ( string ) $this->m_intScreenTypeId : 'NULL';
	}

	public function setScreeningInvoiceId( $intScreeningInvoiceId ) {
		$this->set( 'm_intScreeningInvoiceId', CStrings::strToIntDef( $intScreeningInvoiceId, NULL, false ) );
	}

	public function getScreeningInvoiceId() {
		return $this->m_intScreeningInvoiceId;
	}

	public function sqlScreeningInvoiceId() {
		return ( true == isset( $this->m_intScreeningInvoiceId ) ) ? ( string ) $this->m_intScreeningInvoiceId : 'NULL';
	}

	public function setTransactionChargeTypeId( $intTransactionChargeTypeId ) {
		$this->set( 'm_intTransactionChargeTypeId', CStrings::strToIntDef( $intTransactionChargeTypeId, NULL, false ) );
	}

	public function getTransactionChargeTypeId() {
		return $this->m_intTransactionChargeTypeId;
	}

	public function sqlTransactionChargeTypeId() {
		return ( true == isset( $this->m_intTransactionChargeTypeId ) ) ? ( string ) $this->m_intTransactionChargeTypeId : 'NULL';
	}

	public function setTotalTransactionCount( $intTotalTransactionCount ) {
		$this->set( 'm_intTotalTransactionCount', CStrings::strToIntDef( $intTotalTransactionCount, NULL, false ) );
	}

	public function getTotalTransactionCount() {
		return $this->m_intTotalTransactionCount;
	}

	public function sqlTotalTransactionCount() {
		return ( true == isset( $this->m_intTotalTransactionCount ) ) ? ( string ) $this->m_intTotalTransactionCount : 'NULL';
	}

	public function setTotalInvoiceAmount( $fltTotalInvoiceAmount ) {
		$this->set( 'm_fltTotalInvoiceAmount', CStrings::strToFloatDef( $fltTotalInvoiceAmount, NULL, false, 2 ) );
	}

	public function getTotalInvoiceAmount() {
		return $this->m_fltTotalInvoiceAmount;
	}

	public function sqlTotalInvoiceAmount() {
		return ( true == isset( $this->m_fltTotalInvoiceAmount ) ) ? ( string ) $this->m_fltTotalInvoiceAmount : '0';
	}

	public function setTotalTransactionAmount( $fltTotalTransactionAmount ) {
		$this->set( 'm_fltTotalTransactionAmount', CStrings::strToFloatDef( $fltTotalTransactionAmount, NULL, false, 2 ) );
	}

	public function getTotalTransactionAmount() {
		return $this->m_fltTotalTransactionAmount;
	}

	public function sqlTotalTransactionAmount() {
		return ( true == isset( $this->m_fltTotalTransactionAmount ) ) ? ( string ) $this->m_fltTotalTransactionAmount : '0';
	}

	public function setReconciliationStatusTypeId( $intReconciliationStatusTypeId ) {
		$this->set( 'm_intReconciliationStatusTypeId', CStrings::strToIntDef( $intReconciliationStatusTypeId, NULL, false ) );
	}

	public function getReconciliationStatusTypeId() {
		return $this->m_intReconciliationStatusTypeId;
	}

	public function sqlReconciliationStatusTypeId() {
		return ( true == isset( $this->m_intReconciliationStatusTypeId ) ) ? ( string ) $this->m_intReconciliationStatusTypeId : '1';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screen_type_id' => $this->getScreenTypeId(),
			'screening_invoice_id' => $this->getScreeningInvoiceId(),
			'transaction_charge_type_id' => $this->getTransactionChargeTypeId(),
			'total_transaction_count' => $this->getTotalTransactionCount(),
			'total_invoice_amount' => $this->getTotalInvoiceAmount(),
			'total_transaction_amount' => $this->getTotalTransactionAmount(),
			'reconciliation_status_type_id' => $this->getReconciliationStatusTypeId()
		);
	}

}
?>