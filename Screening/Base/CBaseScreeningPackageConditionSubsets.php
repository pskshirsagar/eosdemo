<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageConditionSubsets
 * Do not add any new functions to this class.
 */

class CBaseScreeningPackageConditionSubsets extends CEosPluralBase {

	/**
	 * @return CScreeningPackageConditionSubset[]
	 */
	public static function fetchScreeningPackageConditionSubsets( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageConditionSubset', $objDatabase );
	}

	/**
	 * @return CScreeningPackageConditionSubset
	 */
	public static function fetchScreeningPackageConditionSubset( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageConditionSubset', $objDatabase );
	}

	public static function fetchScreeningPackageConditionSubsetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_condition_subsets', $objDatabase );
	}

	public static function fetchScreeningPackageConditionSubsetById( $intId, $objDatabase ) {
		return self::fetchScreeningPackageConditionSubset( sprintf( 'SELECT * FROM screening_package_condition_subsets WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>