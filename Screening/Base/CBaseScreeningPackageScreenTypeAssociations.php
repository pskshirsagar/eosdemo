<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageScreenTypeAssociations
 * Do not add any new functions to this class.
 */

class CBaseScreeningPackageScreenTypeAssociations extends CEosPluralBase {

	/**
	 * @return CScreeningPackageScreenTypeAssociation[]
	 */
	public static function fetchScreeningPackageScreenTypeAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningPackageScreenTypeAssociation::class, $objDatabase );
	}

	/**
	 * @return CScreeningPackageScreenTypeAssociation
	 */
	public static function fetchScreeningPackageScreenTypeAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningPackageScreenTypeAssociation::class, $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_screen_type_associations', $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeAssociationById( $intId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeAssociation( sprintf( 'SELECT * FROM screening_package_screen_type_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeAssociationsByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeAssociations( sprintf( 'SELECT * FROM screening_package_screen_type_associations WHERE screening_package_id = %d', ( int ) $intScreeningPackageId ), $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeAssociationsByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeAssociations( sprintf( 'SELECT * FROM screening_package_screen_type_associations WHERE screen_type_id = %d', ( int ) $intScreenTypeId ), $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeAssociationsByPrescreeningSetId( $strPrescreeningSetId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeAssociations( sprintf( 'SELECT * FROM screening_package_screen_type_associations WHERE prescreening_set_id = \'%s\'', $strPrescreeningSetId ), $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeAssociationsByScreeningDataProviderId( $intScreeningDataProviderId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeAssociations( sprintf( 'SELECT * FROM screening_package_screen_type_associations WHERE screening_data_provider_id = %d', ( int ) $intScreeningDataProviderId ), $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeAssociationsByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeAssociations( sprintf( 'SELECT * FROM screening_package_screen_type_associations WHERE screening_criteria_id = %d', ( int ) $intScreeningCriteriaId ), $objDatabase );
	}

}
?>