<?php

class CBaseScreeningAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_accounts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_strName;
	protected $m_intScreenRateTypeId;
	protected $m_fltPackageRate;
	protected $m_strCertificationDate;
	protected $m_boolIsActive;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strClientName;

	public function __construct() {
		parent::__construct();

		$this->m_intScreenRateTypeId = '0';
		$this->m_boolIsActive = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['screen_rate_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreenRateTypeId', trim( $arrValues['screen_rate_type_id'] ) ); elseif( isset( $arrValues['screen_rate_type_id'] ) ) $this->setScreenRateTypeId( $arrValues['screen_rate_type_id'] );
		if( isset( $arrValues['package_rate'] ) && $boolDirectSet ) $this->set( 'm_fltPackageRate', trim( $arrValues['package_rate'] ) ); elseif( isset( $arrValues['package_rate'] ) ) $this->setPackageRate( $arrValues['package_rate'] );
		if( isset( $arrValues['certification_date'] ) && $boolDirectSet ) $this->set( 'm_strCertificationDate', trim( $arrValues['certification_date'] ) ); elseif( isset( $arrValues['certification_date'] ) ) $this->setCertificationDate( $arrValues['certification_date'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['client_name'] ) && $boolDirectSet ) $this->set( 'm_strClientName', trim( stripcslashes( $arrValues['client_name'] ) ) ); elseif( isset( $arrValues['client_name'] ) ) $this->setClientName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_name'] ) : $arrValues['client_name'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, -1, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setScreenRateTypeId( $intScreenRateTypeId ) {
		$this->set( 'm_intScreenRateTypeId', CStrings::strToIntDef( $intScreenRateTypeId, NULL, false ) );
	}

	public function getScreenRateTypeId() {
		return $this->m_intScreenRateTypeId;
	}

	public function sqlScreenRateTypeId() {
		return ( true == isset( $this->m_intScreenRateTypeId ) ) ? ( string ) $this->m_intScreenRateTypeId : '0';
	}

	public function setPackageRate( $fltPackageRate ) {
		$this->set( 'm_fltPackageRate', CStrings::strToFloatDef( $fltPackageRate, NULL, false, 2 ) );
	}

	public function getPackageRate() {
		return $this->m_fltPackageRate;
	}

	public function sqlPackageRate() {
		return ( true == isset( $this->m_fltPackageRate ) ) ? ( string ) $this->m_fltPackageRate : 'NULL';
	}

	public function setCertificationDate( $strCertificationDate ) {
		$this->set( 'm_strCertificationDate', CStrings::strTrimDef( $strCertificationDate, -1, NULL, true ) );
	}

	public function getCertificationDate() {
		return $this->m_strCertificationDate;
	}

	public function sqlCertificationDate() {
		return ( true == isset( $this->m_strCertificationDate ) ) ? '\'' . $this->m_strCertificationDate . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setClientName( $strClientName ) {
		$this->set( 'm_strClientName', CStrings::strTrimDef( $strClientName, 100, NULL, true ) );
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function sqlClientName() {
		return ( true == isset( $this->m_strClientName ) ) ? '\'' . addslashes( $this->m_strClientName ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, name, screen_rate_type_id, package_rate, certification_date, is_active, updated_by, updated_on, created_by, created_on, client_name )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlScreenRateTypeId() . ', ' .
 						$this->sqlPackageRate() . ', ' .
 						$this->sqlCertificationDate() . ', ' .
 						$this->sqlIsActive() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlClientName() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screen_rate_type_id = ' . $this->sqlScreenRateTypeId() . ','; } elseif( true == array_key_exists( 'ScreenRateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screen_rate_type_id = ' . $this->sqlScreenRateTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' package_rate = ' . $this->sqlPackageRate() . ','; } elseif( true == array_key_exists( 'PackageRate', $this->getChangedColumns() ) ) { $strSql .= ' package_rate = ' . $this->sqlPackageRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' certification_date = ' . $this->sqlCertificationDate() . ','; } elseif( true == array_key_exists( 'CertificationDate', $this->getChangedColumns() ) ) { $strSql .= ' certification_date = ' . $this->sqlCertificationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_name = ' . $this->sqlClientName() . ','; } elseif( true == array_key_exists( 'ClientName', $this->getChangedColumns() ) ) { $strSql .= ' client_name = ' . $this->sqlClientName() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'name' => $this->getName(),
			'screen_rate_type_id' => $this->getScreenRateTypeId(),
			'package_rate' => $this->getPackageRate(),
			'certification_date' => $this->getCertificationDate(),
			'is_active' => $this->getIsActive(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'client_name' => $this->getClientName()
		);
	}

}
?>