<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageConditionValueTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningPackageConditionValueTypes extends CEosPluralBase {

	/**
	 * @return CScreeningPackageConditionValueType[]
	 */
	public static function fetchScreeningPackageConditionValueTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageConditionValueType', $objDatabase );
	}

	/**
	 * @return CScreeningPackageConditionValueType
	 */
	public static function fetchScreeningPackageConditionValueType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageConditionValueType', $objDatabase );
	}

	public static function fetchScreeningPackageConditionValueTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_condition_value_types', $objDatabase );
	}

	public static function fetchScreeningPackageConditionValueTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningPackageConditionValueType( sprintf( 'SELECT * FROM screening_package_condition_value_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>