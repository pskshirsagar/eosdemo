<?php

class CBaseSubscriptionBillingContractProperty extends CEosSingularBase {

	const TABLE_NAME = 'public.subscription_billing_contract_properties';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intContractId;
	protected $m_intBillingAccountId;
	protected $m_strBillingAccountName;
	protected $m_intContractedUnits;
	protected $m_strBillingStartDate;
	protected $m_strDeactivationDate;
	protected $m_boolIsActive;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strContractPeriodStartDate;
	protected $m_strContractPeriodEndDate;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['billing_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingAccountId', trim( $arrValues['billing_account_id'] ) ); elseif( isset( $arrValues['billing_account_id'] ) ) $this->setBillingAccountId( $arrValues['billing_account_id'] );
		if( isset( $arrValues['billing_account_name'] ) && $boolDirectSet ) $this->set( 'm_strBillingAccountName', trim( $arrValues['billing_account_name'] ) ); elseif( isset( $arrValues['billing_account_name'] ) ) $this->setBillingAccountName( $arrValues['billing_account_name'] );
		if( isset( $arrValues['contracted_units'] ) && $boolDirectSet ) $this->set( 'm_intContractedUnits', trim( $arrValues['contracted_units'] ) ); elseif( isset( $arrValues['contracted_units'] ) ) $this->setContractedUnits( $arrValues['contracted_units'] );
		if( isset( $arrValues['billing_start_date'] ) && $boolDirectSet ) $this->set( 'm_strBillingStartDate', trim( $arrValues['billing_start_date'] ) ); elseif( isset( $arrValues['billing_start_date'] ) ) $this->setBillingStartDate( $arrValues['billing_start_date'] );
		if( isset( $arrValues['deactivation_date'] ) && $boolDirectSet ) $this->set( 'm_strDeactivationDate', trim( $arrValues['deactivation_date'] ) ); elseif( isset( $arrValues['deactivation_date'] ) ) $this->setDeactivationDate( $arrValues['deactivation_date'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['contract_period_start_date'] ) && $boolDirectSet ) $this->set( 'm_strContractPeriodStartDate', trim( $arrValues['contract_period_start_date'] ) ); elseif( isset( $arrValues['contract_period_start_date'] ) ) $this->setContractPeriodStartDate( $arrValues['contract_period_start_date'] );
		if( isset( $arrValues['contract_period_end_date'] ) && $boolDirectSet ) $this->set( 'm_strContractPeriodEndDate', trim( $arrValues['contract_period_end_date'] ) ); elseif( isset( $arrValues['contract_period_end_date'] ) ) $this->setContractPeriodEndDate( $arrValues['contract_period_end_date'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setBillingAccountId( $intBillingAccountId ) {
		$this->set( 'm_intBillingAccountId', CStrings::strToIntDef( $intBillingAccountId, NULL, false ) );
	}

	public function getBillingAccountId() {
		return $this->m_intBillingAccountId;
	}

	public function sqlBillingAccountId() {
		return ( true == isset( $this->m_intBillingAccountId ) ) ? ( string ) $this->m_intBillingAccountId : 'NULL';
	}

	public function setBillingAccountName( $strBillingAccountName ) {
		$this->set( 'm_strBillingAccountName', CStrings::strTrimDef( $strBillingAccountName, 100, NULL, true ) );
	}

	public function getBillingAccountName() {
		return $this->m_strBillingAccountName;
	}

	public function sqlBillingAccountName() {
		return ( true == isset( $this->m_strBillingAccountName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBillingAccountName ) : '\'' . addslashes( $this->m_strBillingAccountName ) . '\'' ) : 'NULL';
	}

	public function setContractedUnits( $intContractedUnits ) {
		$this->set( 'm_intContractedUnits', CStrings::strToIntDef( $intContractedUnits, NULL, false ) );
	}

	public function getContractedUnits() {
		return $this->m_intContractedUnits;
	}

	public function sqlContractedUnits() {
		return ( true == isset( $this->m_intContractedUnits ) ) ? ( string ) $this->m_intContractedUnits : 'NULL';
	}

	public function setBillingStartDate( $strBillingStartDate ) {
		$this->set( 'm_strBillingStartDate', CStrings::strTrimDef( $strBillingStartDate, -1, NULL, true ) );
	}

	public function getBillingStartDate() {
		return $this->m_strBillingStartDate;
	}

	public function sqlBillingStartDate() {
		return ( true == isset( $this->m_strBillingStartDate ) ) ? '\'' . $this->m_strBillingStartDate . '\'' : 'NOW()';
	}

	public function setDeactivationDate( $strDeactivationDate ) {
		$this->set( 'm_strDeactivationDate', CStrings::strTrimDef( $strDeactivationDate, -1, NULL, true ) );
	}

	public function getDeactivationDate() {
		return $this->m_strDeactivationDate;
	}

	public function sqlDeactivationDate() {
		return ( true == isset( $this->m_strDeactivationDate ) ) ? '\'' . $this->m_strDeactivationDate . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setContractPeriodStartDate( $strContractPeriodStartDate ) {
		$this->set( 'm_strContractPeriodStartDate', CStrings::strTrimDef( $strContractPeriodStartDate, -1, NULL, true ) );
	}

	public function getContractPeriodStartDate() {
		return $this->m_strContractPeriodStartDate;
	}

	public function sqlContractPeriodStartDate() {
		return ( true == isset( $this->m_strContractPeriodStartDate ) ) ? '\'' . $this->m_strContractPeriodStartDate . '\'' : 'NULL';
	}

	public function setContractPeriodEndDate( $strContractPeriodEndDate ) {
		$this->set( 'm_strContractPeriodEndDate', CStrings::strTrimDef( $strContractPeriodEndDate, -1, NULL, true ) );
	}

	public function getContractPeriodEndDate() {
		return $this->m_strContractPeriodEndDate;
	}

	public function sqlContractPeriodEndDate() {
		return ( true == isset( $this->m_strContractPeriodEndDate ) ) ? '\'' . $this->m_strContractPeriodEndDate . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, contract_id, billing_account_id, billing_account_name, contracted_units, billing_start_date, deactivation_date, is_active, updated_by, updated_on, created_by, created_on, contract_period_start_date, contract_period_end_date )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlContractId() . ', ' .
						$this->sqlBillingAccountId() . ', ' .
						$this->sqlBillingAccountName() . ', ' .
						$this->sqlContractedUnits() . ', ' .
						$this->sqlBillingStartDate() . ', ' .
						$this->sqlDeactivationDate() . ', ' .
						$this->sqlIsActive() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlContractPeriodStartDate() . ', ' .
						$this->sqlContractPeriodEndDate() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId(). ',' ; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_account_id = ' . $this->sqlBillingAccountId(). ',' ; } elseif( true == array_key_exists( 'BillingAccountId', $this->getChangedColumns() ) ) { $strSql .= ' billing_account_id = ' . $this->sqlBillingAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_account_name = ' . $this->sqlBillingAccountName(). ',' ; } elseif( true == array_key_exists( 'BillingAccountName', $this->getChangedColumns() ) ) { $strSql .= ' billing_account_name = ' . $this->sqlBillingAccountName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contracted_units = ' . $this->sqlContractedUnits(). ',' ; } elseif( true == array_key_exists( 'ContractedUnits', $this->getChangedColumns() ) ) { $strSql .= ' contracted_units = ' . $this->sqlContractedUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_start_date = ' . $this->sqlBillingStartDate(). ',' ; } elseif( true == array_key_exists( 'BillingStartDate', $this->getChangedColumns() ) ) { $strSql .= ' billing_start_date = ' . $this->sqlBillingStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deactivation_date = ' . $this->sqlDeactivationDate(). ',' ; } elseif( true == array_key_exists( 'DeactivationDate', $this->getChangedColumns() ) ) { $strSql .= ' deactivation_date = ' . $this->sqlDeactivationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_period_start_date = ' . $this->sqlContractPeriodStartDate(). ',' ; } elseif( true == array_key_exists( 'ContractPeriodStartDate', $this->getChangedColumns() ) ) { $strSql .= ' contract_period_start_date = ' . $this->sqlContractPeriodStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_period_end_date = ' . $this->sqlContractPeriodEndDate(). ',' ; } elseif( true == array_key_exists( 'ContractPeriodEndDate', $this->getChangedColumns() ) ) { $strSql .= ' contract_period_end_date = ' . $this->sqlContractPeriodEndDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'contract_id' => $this->getContractId(),
			'billing_account_id' => $this->getBillingAccountId(),
			'billing_account_name' => $this->getBillingAccountName(),
			'contracted_units' => $this->getContractedUnits(),
			'billing_start_date' => $this->getBillingStartDate(),
			'deactivation_date' => $this->getDeactivationDate(),
			'is_active' => $this->getIsActive(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'contract_period_start_date' => $this->getContractPeriodStartDate(),
			'contract_period_end_date' => $this->getContractPeriodEndDate()
		);
	}

}
?>