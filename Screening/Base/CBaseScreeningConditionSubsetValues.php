<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConditionSubsetValues
 * Do not add any new functions to this class.
 */

class CBaseScreeningConditionSubsetValues extends CEosPluralBase {

	/**
	 * @return CScreeningConditionSubsetValue[]
	 */
	public static function fetchScreeningConditionSubsetValues( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningConditionSubsetValue::class, $objDatabase );
	}

	/**
	 * @return CScreeningConditionSubsetValue
	 */
	public static function fetchScreeningConditionSubsetValue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningConditionSubsetValue::class, $objDatabase );
	}

	public static function fetchScreeningConditionSubsetValueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_condition_subset_values', $objDatabase );
	}

	public static function fetchScreeningConditionSubsetValueById( $intId, $objDatabase ) {
		return self::fetchScreeningConditionSubsetValue( sprintf( 'SELECT * FROM screening_condition_subset_values WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningConditionSubsetValuesByScreeningConditionSubsetId( $intScreeningConditionSubsetId, $objDatabase ) {
		return self::fetchScreeningConditionSubsetValues( sprintf( 'SELECT * FROM screening_condition_subset_values WHERE screening_condition_subset_id = %d', ( int ) $intScreeningConditionSubsetId ), $objDatabase );
	}

	public static function fetchScreeningConditionSubsetValuesByScreeningAvailableConditionId( $intScreeningAvailableConditionId, $objDatabase ) {
		return self::fetchScreeningConditionSubsetValues( sprintf( 'SELECT * FROM screening_condition_subset_values WHERE screening_available_condition_id = %d', ( int ) $intScreeningAvailableConditionId ), $objDatabase );
	}

}
?>