<?php

class CBaseScreeningTableLog extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_table_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScreeningId;
	protected $m_intScreeningApplicantId;
	protected $m_strTableName;
	protected $m_intReferenceNumber;
	protected $m_strAction;
	protected $m_strDescription;
	protected $m_strOldData;
	protected $m_strNewData;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intScreeningId = '1';
		$this->m_intScreeningApplicantId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screening_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningId', trim( $arrValues['screening_id'] ) ); elseif( isset( $arrValues['screening_id'] ) ) $this->setScreeningId( $arrValues['screening_id'] );
		if( isset( $arrValues['screening_applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicantId', trim( $arrValues['screening_applicant_id'] ) ); elseif( isset( $arrValues['screening_applicant_id'] ) ) $this->setScreeningApplicantId( $arrValues['screening_applicant_id'] );
		if( isset( $arrValues['table_name'] ) && $boolDirectSet ) $this->set( 'm_strTableName', trim( stripcslashes( $arrValues['table_name'] ) ) ); elseif( isset( $arrValues['table_name'] ) ) $this->setTableName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['table_name'] ) : $arrValues['table_name'] );
		if( isset( $arrValues['reference_number'] ) && $boolDirectSet ) $this->set( 'm_intReferenceNumber', trim( $arrValues['reference_number'] ) ); elseif( isset( $arrValues['reference_number'] ) ) $this->setReferenceNumber( $arrValues['reference_number'] );
		if( isset( $arrValues['action'] ) && $boolDirectSet ) $this->set( 'm_strAction', trim( stripcslashes( $arrValues['action'] ) ) ); elseif( isset( $arrValues['action'] ) ) $this->setAction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action'] ) : $arrValues['action'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['old_data'] ) && $boolDirectSet ) $this->set( 'm_strOldData', trim( stripcslashes( $arrValues['old_data'] ) ) ); elseif( isset( $arrValues['old_data'] ) ) $this->setOldData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['old_data'] ) : $arrValues['old_data'] );
		if( isset( $arrValues['new_data'] ) && $boolDirectSet ) $this->set( 'm_strNewData', trim( stripcslashes( $arrValues['new_data'] ) ) ); elseif( isset( $arrValues['new_data'] ) ) $this->setNewData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_data'] ) : $arrValues['new_data'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreeningId( $intScreeningId ) {
		$this->set( 'm_intScreeningId', CStrings::strToIntDef( $intScreeningId, NULL, false ) );
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function sqlScreeningId() {
		return ( true == isset( $this->m_intScreeningId ) ) ? ( string ) $this->m_intScreeningId : '1';
	}

	public function setScreeningApplicantId( $intScreeningApplicantId ) {
		$this->set( 'm_intScreeningApplicantId', CStrings::strToIntDef( $intScreeningApplicantId, NULL, false ) );
	}

	public function getScreeningApplicantId() {
		return $this->m_intScreeningApplicantId;
	}

	public function sqlScreeningApplicantId() {
		return ( true == isset( $this->m_intScreeningApplicantId ) ) ? ( string ) $this->m_intScreeningApplicantId : '1';
	}

	public function setTableName( $strTableName ) {
		$this->set( 'm_strTableName', CStrings::strTrimDef( $strTableName, 50, NULL, true ) );
	}

	public function getTableName() {
		return $this->m_strTableName;
	}

	public function sqlTableName() {
		return ( true == isset( $this->m_strTableName ) ) ? '\'' . addslashes( $this->m_strTableName ) . '\'' : 'NULL';
	}

	public function setReferenceNumber( $intReferenceNumber ) {
		$this->set( 'm_intReferenceNumber', CStrings::strToIntDef( $intReferenceNumber, NULL, false ) );
	}

	public function getReferenceNumber() {
		return $this->m_intReferenceNumber;
	}

	public function sqlReferenceNumber() {
		return ( true == isset( $this->m_intReferenceNumber ) ) ? ( string ) $this->m_intReferenceNumber : 'NULL';
	}

	public function setAction( $strAction ) {
		$this->set( 'm_strAction', CStrings::strTrimDef( $strAction, 30, NULL, true ) );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function sqlAction() {
		return ( true == isset( $this->m_strAction ) ) ? '\'' . addslashes( $this->m_strAction ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setOldData( $strOldData ) {
		$this->set( 'm_strOldData', CStrings::strTrimDef( $strOldData, -1, NULL, true ) );
	}

	public function getOldData() {
		return $this->m_strOldData;
	}

	public function sqlOldData() {
		return ( true == isset( $this->m_strOldData ) ) ? '\'' . addslashes( $this->m_strOldData ) . '\'' : 'NULL';
	}

	public function setNewData( $strNewData ) {
		$this->set( 'm_strNewData', CStrings::strTrimDef( $strNewData, -1, NULL, true ) );
	}

	public function getNewData() {
		return $this->m_strNewData;
	}

	public function sqlNewData() {
		return ( true == isset( $this->m_strNewData ) ) ? '\'' . addslashes( $this->m_strNewData ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, screening_id, screening_applicant_id, table_name, reference_number, action, description, old_data, new_data, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlScreeningId() . ', ' .
 						$this->sqlScreeningApplicantId() . ', ' .
 						$this->sqlTableName() . ', ' .
 						$this->sqlReferenceNumber() . ', ' .
 						$this->sqlAction() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlOldData() . ', ' .
 						$this->sqlNewData() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; } elseif( true == array_key_exists( 'ScreeningId', $this->getChangedColumns() ) ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId() . ','; } elseif( true == array_key_exists( 'ScreeningApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' table_name = ' . $this->sqlTableName() . ','; } elseif( true == array_key_exists( 'TableName', $this->getChangedColumns() ) ) { $strSql .= ' table_name = ' . $this->sqlTableName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; } elseif( true == array_key_exists( 'ReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action = ' . $this->sqlAction() . ','; } elseif( true == array_key_exists( 'Action', $this->getChangedColumns() ) ) { $strSql .= ' action = ' . $this->sqlAction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_data = ' . $this->sqlOldData() . ','; } elseif( true == array_key_exists( 'OldData', $this->getChangedColumns() ) ) { $strSql .= ' old_data = ' . $this->sqlOldData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_data = ' . $this->sqlNewData() . ','; } elseif( true == array_key_exists( 'NewData', $this->getChangedColumns() ) ) { $strSql .= ' new_data = ' . $this->sqlNewData() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'screening_id' => $this->getScreeningId(),
			'screening_applicant_id' => $this->getScreeningApplicantId(),
			'table_name' => $this->getTableName(),
			'reference_number' => $this->getReferenceNumber(),
			'action' => $this->getAction(),
			'description' => $this->getDescription(),
			'old_data' => $this->getOldData(),
			'new_data' => $this->getNewData(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>