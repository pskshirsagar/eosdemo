<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningTransactions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningTransactions extends CEosPluralBase {

	/**
	 * @return CScreeningTransaction[]
	 */
	public static function fetchScreeningTransactions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningTransaction', $objDatabase );
	}

	/**
	 * @return CScreeningTransaction
	 */
	public static function fetchScreeningTransaction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningTransaction', $objDatabase );
	}

	public static function fetchScreeningTransactionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_transactions', $objDatabase );
	}

	public static function fetchScreeningTransactionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningTransaction( sprintf( 'SELECT * FROM screening_transactions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningTransactionsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningTransactions( sprintf( 'SELECT * FROM screening_transactions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningTransactionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchScreeningTransactions( sprintf( 'SELECT * FROM screening_transactions WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningTransactionsByScreeningIdByCid( $intScreeningId, $intCid, $objDatabase ) {
		return self::fetchScreeningTransactions( sprintf( 'SELECT * FROM screening_transactions WHERE screening_id = %d AND cid = %d', ( int ) $intScreeningId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningTransactionsByScreeningApplicantIdByCid( $intScreeningApplicantId, $intCid, $objDatabase ) {
		return self::fetchScreeningTransactions( sprintf( 'SELECT * FROM screening_transactions WHERE screening_applicant_id = %d AND cid = %d', ( int ) $intScreeningApplicantId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningTransactionsByScreenTypeIdByCid( $intScreenTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningTransactions( sprintf( 'SELECT * FROM screening_transactions WHERE screen_type_id = %d AND cid = %d', ( int ) $intScreenTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningTransactionsByScreeningBatchIdByCid( $intScreeningBatchId, $intCid, $objDatabase ) {
		return self::fetchScreeningTransactions( sprintf( 'SELECT * FROM screening_transactions WHERE screening_batch_id = %d AND cid = %d', ( int ) $intScreeningBatchId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningTransactionsByCompanyScreeningAccountIdByCid( $intCompanyScreeningAccountId, $intCid, $objDatabase ) {
		return self::fetchScreeningTransactions( sprintf( 'SELECT * FROM screening_transactions WHERE company_screening_account_id = %d AND cid = %d', ( int ) $intCompanyScreeningAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningTransactionsByScreeningDataProviderIdByCid( $intScreeningDataProviderId, $intCid, $objDatabase ) {
		return self::fetchScreeningTransactions( sprintf( 'SELECT * FROM screening_transactions WHERE screening_data_provider_id = %d AND cid = %d', ( int ) $intScreeningDataProviderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningTransactionsByScreeningOrderIdByCid( $intScreeningOrderId, $intCid, $objDatabase ) {
		return self::fetchScreeningTransactions( sprintf( 'SELECT * FROM screening_transactions WHERE screening_order_id = %d AND cid = %d', ( int ) $intScreeningOrderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningTransactionsByScreeningStatusTypeIdByCid( $intScreeningStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningTransactions( sprintf( 'SELECT * FROM screening_transactions WHERE screening_status_type_id = %d AND cid = %d', ( int ) $intScreeningStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningTransactionsByOriginalScreeningRecommendationTypeIdByCid( $intOriginalScreeningRecommendationTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningTransactions( sprintf( 'SELECT * FROM screening_transactions WHERE original_screening_recommendation_type_id = %d AND cid = %d', ( int ) $intOriginalScreeningRecommendationTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningTransactionsByScreeningRecommendationTypeIdByCid( $intScreeningRecommendationTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningTransactions( sprintf( 'SELECT * FROM screening_transactions WHERE screening_recommendation_type_id = %d AND cid = %d', ( int ) $intScreeningRecommendationTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningTransactionsByScreeningPackageConditionSetIdByCid( $intScreeningPackageConditionSetId, $intCid, $objDatabase ) {
		return self::fetchScreeningTransactions( sprintf( 'SELECT * FROM screening_transactions WHERE screening_package_condition_set_id = %d AND cid = %d', ( int ) $intScreeningPackageConditionSetId, ( int ) $intCid ), $objDatabase );
	}

}
?>