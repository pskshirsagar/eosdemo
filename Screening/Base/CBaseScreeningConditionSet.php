<?php

class CBaseScreeningConditionSet extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_condition_sets';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScreeningRecommendationTypeId;
	protected $m_intFirstScreeningConditionSubsetId;
	protected $m_intSecondScreeningConditionSubsetId;
	protected $m_intScreeningPackageOperandTypeId;
	protected $m_strConditionName;
	protected $m_intEvaluationOrder;
	protected $m_boolIsTemplate;
	protected $m_boolIsPublished;
	protected $m_boolIsAlwaysApply;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_arrintScreeningPackageLeaseTypeIds;
	protected $m_intScreeningConditionSetTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsTemplate = false;
		$this->m_boolIsPublished = true;
		$this->m_boolIsAlwaysApply = false;
		$this->m_intScreeningConditionSetTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screening_recommendation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningRecommendationTypeId', trim( $arrValues['screening_recommendation_type_id'] ) ); elseif( isset( $arrValues['screening_recommendation_type_id'] ) ) $this->setScreeningRecommendationTypeId( $arrValues['screening_recommendation_type_id'] );
		if( isset( $arrValues['first_screening_condition_subset_id'] ) && $boolDirectSet ) $this->set( 'm_intFirstScreeningConditionSubsetId', trim( $arrValues['first_screening_condition_subset_id'] ) ); elseif( isset( $arrValues['first_screening_condition_subset_id'] ) ) $this->setFirstScreeningConditionSubsetId( $arrValues['first_screening_condition_subset_id'] );
		if( isset( $arrValues['second_screening_condition_subset_id'] ) && $boolDirectSet ) $this->set( 'm_intSecondScreeningConditionSubsetId', trim( $arrValues['second_screening_condition_subset_id'] ) ); elseif( isset( $arrValues['second_screening_condition_subset_id'] ) ) $this->setSecondScreeningConditionSubsetId( $arrValues['second_screening_condition_subset_id'] );
		if( isset( $arrValues['screening_package_operand_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageOperandTypeId', trim( $arrValues['screening_package_operand_type_id'] ) ); elseif( isset( $arrValues['screening_package_operand_type_id'] ) ) $this->setScreeningPackageOperandTypeId( $arrValues['screening_package_operand_type_id'] );
		if( isset( $arrValues['condition_name'] ) && $boolDirectSet ) $this->set( 'm_strConditionName', trim( $arrValues['condition_name'] ) ); elseif( isset( $arrValues['condition_name'] ) ) $this->setConditionName( $arrValues['condition_name'] );
		if( isset( $arrValues['evaluation_order'] ) && $boolDirectSet ) $this->set( 'm_intEvaluationOrder', trim( $arrValues['evaluation_order'] ) ); elseif( isset( $arrValues['evaluation_order'] ) ) $this->setEvaluationOrder( $arrValues['evaluation_order'] );
		if( isset( $arrValues['is_template'] ) && $boolDirectSet ) $this->set( 'm_boolIsTemplate', trim( stripcslashes( $arrValues['is_template'] ) ) ); elseif( isset( $arrValues['is_template'] ) ) $this->setIsTemplate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_template'] ) : $arrValues['is_template'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['is_always_apply'] ) && $boolDirectSet ) $this->set( 'm_boolIsAlwaysApply', trim( stripcslashes( $arrValues['is_always_apply'] ) ) ); elseif( isset( $arrValues['is_always_apply'] ) ) $this->setIsAlwaysApply( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_always_apply'] ) : $arrValues['is_always_apply'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['screening_package_lease_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintScreeningPackageLeaseTypeIds', trim( $arrValues['screening_package_lease_type_ids'] ) ); elseif( isset( $arrValues['screening_package_lease_type_ids'] ) ) $this->setScreeningPackageLeaseTypeIds( $arrValues['screening_package_lease_type_ids'] );
		if( isset( $arrValues['screening_condition_set_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningConditionSetTypeId', trim( $arrValues['screening_condition_set_type_id'] ) ); elseif( isset( $arrValues['screening_condition_set_type_id'] ) ) $this->setScreeningConditionSetTypeId( $arrValues['screening_condition_set_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreeningRecommendationTypeId( $intScreeningRecommendationTypeId ) {
		$this->set( 'm_intScreeningRecommendationTypeId', CStrings::strToIntDef( $intScreeningRecommendationTypeId, NULL, false ) );
	}

	public function getScreeningRecommendationTypeId() {
		return $this->m_intScreeningRecommendationTypeId;
	}

	public function sqlScreeningRecommendationTypeId() {
		return ( true == isset( $this->m_intScreeningRecommendationTypeId ) ) ? ( string ) $this->m_intScreeningRecommendationTypeId : 'NULL';
	}

	public function setFirstScreeningConditionSubsetId( $intFirstScreeningConditionSubsetId ) {
		$this->set( 'm_intFirstScreeningConditionSubsetId', CStrings::strToIntDef( $intFirstScreeningConditionSubsetId, NULL, false ) );
	}

	public function getFirstScreeningConditionSubsetId() {
		return $this->m_intFirstScreeningConditionSubsetId;
	}

	public function sqlFirstScreeningConditionSubsetId() {
		return ( true == isset( $this->m_intFirstScreeningConditionSubsetId ) ) ? ( string ) $this->m_intFirstScreeningConditionSubsetId : 'NULL';
	}

	public function setSecondScreeningConditionSubsetId( $intSecondScreeningConditionSubsetId ) {
		$this->set( 'm_intSecondScreeningConditionSubsetId', CStrings::strToIntDef( $intSecondScreeningConditionSubsetId, NULL, false ) );
	}

	public function getSecondScreeningConditionSubsetId() {
		return $this->m_intSecondScreeningConditionSubsetId;
	}

	public function sqlSecondScreeningConditionSubsetId() {
		return ( true == isset( $this->m_intSecondScreeningConditionSubsetId ) ) ? ( string ) $this->m_intSecondScreeningConditionSubsetId : 'NULL';
	}

	public function setScreeningPackageOperandTypeId( $intScreeningPackageOperandTypeId ) {
		$this->set( 'm_intScreeningPackageOperandTypeId', CStrings::strToIntDef( $intScreeningPackageOperandTypeId, NULL, false ) );
	}

	public function getScreeningPackageOperandTypeId() {
		return $this->m_intScreeningPackageOperandTypeId;
	}

	public function sqlScreeningPackageOperandTypeId() {
		return ( true == isset( $this->m_intScreeningPackageOperandTypeId ) ) ? ( string ) $this->m_intScreeningPackageOperandTypeId : 'NULL';
	}

	public function setConditionName( $strConditionName ) {
		$this->set( 'm_strConditionName', CStrings::strTrimDef( $strConditionName, -1, NULL, true ) );
	}

	public function getConditionName() {
		return $this->m_strConditionName;
	}

	public function sqlConditionName() {
		return ( true == isset( $this->m_strConditionName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strConditionName ) : '\'' . addslashes( $this->m_strConditionName ) . '\'' ) : 'NULL';
	}

	public function setEvaluationOrder( $intEvaluationOrder ) {
		$this->set( 'm_intEvaluationOrder', CStrings::strToIntDef( $intEvaluationOrder, NULL, false ) );
	}

	public function getEvaluationOrder() {
		return $this->m_intEvaluationOrder;
	}

	public function sqlEvaluationOrder() {
		return ( true == isset( $this->m_intEvaluationOrder ) ) ? ( string ) $this->m_intEvaluationOrder : 'NULL';
	}

	public function setIsTemplate( $boolIsTemplate ) {
		$this->set( 'm_boolIsTemplate', CStrings::strToBool( $boolIsTemplate ) );
	}

	public function getIsTemplate() {
		return $this->m_boolIsTemplate;
	}

	public function sqlIsTemplate() {
		return ( true == isset( $this->m_boolIsTemplate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTemplate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAlwaysApply( $boolIsAlwaysApply ) {
		$this->set( 'm_boolIsAlwaysApply', CStrings::strToBool( $boolIsAlwaysApply ) );
	}

	public function getIsAlwaysApply() {
		return $this->m_boolIsAlwaysApply;
	}

	public function sqlIsAlwaysApply() {
		return ( true == isset( $this->m_boolIsAlwaysApply ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAlwaysApply ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setScreeningPackageLeaseTypeIds( $arrintScreeningPackageLeaseTypeIds ) {
		$this->set( 'm_arrintScreeningPackageLeaseTypeIds', CStrings::strToArrIntDef( $arrintScreeningPackageLeaseTypeIds, NULL ) );
	}

	public function getScreeningPackageLeaseTypeIds() {
		return $this->m_arrintScreeningPackageLeaseTypeIds;
	}

	public function sqlScreeningPackageLeaseTypeIds() {
		return ( true == isset( $this->m_arrintScreeningPackageLeaseTypeIds ) && true == valArr( $this->m_arrintScreeningPackageLeaseTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintScreeningPackageLeaseTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function setScreeningConditionSetTypeId( $intScreeningConditionSetTypeId ) {
		$this->set( 'm_intScreeningConditionSetTypeId', CStrings::strToIntDef( $intScreeningConditionSetTypeId, NULL, false ) );
	}

	public function getScreeningConditionSetTypeId() {
		return $this->m_intScreeningConditionSetTypeId;
	}

	public function sqlScreeningConditionSetTypeId() {
		return ( true == isset( $this->m_intScreeningConditionSetTypeId ) ) ? ( string ) $this->m_intScreeningConditionSetTypeId : '1';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, screening_recommendation_type_id, first_screening_condition_subset_id, second_screening_condition_subset_id, screening_package_operand_type_id, condition_name, evaluation_order, is_template, is_published, is_always_apply, updated_by, updated_on, created_by, created_on, screening_package_lease_type_ids, screening_condition_set_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlScreeningRecommendationTypeId() . ', ' .
						$this->sqlFirstScreeningConditionSubsetId() . ', ' .
						$this->sqlSecondScreeningConditionSubsetId() . ', ' .
						$this->sqlScreeningPackageOperandTypeId() . ', ' .
						$this->sqlConditionName() . ', ' .
						$this->sqlEvaluationOrder() . ', ' .
						$this->sqlIsTemplate() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsAlwaysApply() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlScreeningPackageLeaseTypeIds() . ', ' .
						$this->sqlScreeningConditionSetTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningRecommendationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_screening_condition_subset_id = ' . $this->sqlFirstScreeningConditionSubsetId(). ',' ; } elseif( true == array_key_exists( 'FirstScreeningConditionSubsetId', $this->getChangedColumns() ) ) { $strSql .= ' first_screening_condition_subset_id = ' . $this->sqlFirstScreeningConditionSubsetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' second_screening_condition_subset_id = ' . $this->sqlSecondScreeningConditionSubsetId(). ',' ; } elseif( true == array_key_exists( 'SecondScreeningConditionSubsetId', $this->getChangedColumns() ) ) { $strSql .= ' second_screening_condition_subset_id = ' . $this->sqlSecondScreeningConditionSubsetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_operand_type_id = ' . $this->sqlScreeningPackageOperandTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningPackageOperandTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_operand_type_id = ' . $this->sqlScreeningPackageOperandTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_name = ' . $this->sqlConditionName(). ',' ; } elseif( true == array_key_exists( 'ConditionName', $this->getChangedColumns() ) ) { $strSql .= ' condition_name = ' . $this->sqlConditionName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' evaluation_order = ' . $this->sqlEvaluationOrder(). ',' ; } elseif( true == array_key_exists( 'EvaluationOrder', $this->getChangedColumns() ) ) { $strSql .= ' evaluation_order = ' . $this->sqlEvaluationOrder() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_template = ' . $this->sqlIsTemplate(). ',' ; } elseif( true == array_key_exists( 'IsTemplate', $this->getChangedColumns() ) ) { $strSql .= ' is_template = ' . $this->sqlIsTemplate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_always_apply = ' . $this->sqlIsAlwaysApply(). ',' ; } elseif( true == array_key_exists( 'IsAlwaysApply', $this->getChangedColumns() ) ) { $strSql .= ' is_always_apply = ' . $this->sqlIsAlwaysApply() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_lease_type_ids = ' . $this->sqlScreeningPackageLeaseTypeIds(). ',' ; } elseif( true == array_key_exists( 'ScreeningPackageLeaseTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_lease_type_ids = ' . $this->sqlScreeningPackageLeaseTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_condition_set_type_id = ' . $this->sqlScreeningConditionSetTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningConditionSetTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_condition_set_type_id = ' . $this->sqlScreeningConditionSetTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'screening_recommendation_type_id' => $this->getScreeningRecommendationTypeId(),
			'first_screening_condition_subset_id' => $this->getFirstScreeningConditionSubsetId(),
			'second_screening_condition_subset_id' => $this->getSecondScreeningConditionSubsetId(),
			'screening_package_operand_type_id' => $this->getScreeningPackageOperandTypeId(),
			'condition_name' => $this->getConditionName(),
			'evaluation_order' => $this->getEvaluationOrder(),
			'is_template' => $this->getIsTemplate(),
			'is_published' => $this->getIsPublished(),
			'is_always_apply' => $this->getIsAlwaysApply(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'screening_package_lease_type_ids' => $this->getScreeningPackageLeaseTypeIds(),
			'screening_condition_set_type_id' => $this->getScreeningConditionSetTypeId()
		);
	}

}
?>