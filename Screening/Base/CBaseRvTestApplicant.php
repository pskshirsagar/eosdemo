<?php

class CBaseRvTestApplicant extends CEosSingularBase {

	const TABLE_NAME = 'public.rv_test_applicants';

	protected $m_intId;
	protected $m_intPropertyId;
	protected $m_intSsn;
	protected $m_strSsnMasked;
	protected $m_strPropertyStateCode;
	protected $m_intApplicantId;
	protected $m_intPackageId;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strNameMiddle;
	protected $m_strBirthDate;
	protected $m_boolIsAlien;
	protected $m_boolSendCaliforniaConsumerReport;
	protected $m_intApplicantTypeId;
	protected $m_strDetails;
	protected $m_intMonthlyIncome;
	protected $m_strEmailAddress;
	protected $m_strPhoneNumber;
	protected $m_strPostalCode;
	protected $m_strRegion;
	protected $m_strMunicipality;
	protected $m_strAddressLine;
	protected $m_strStateCode;
	protected $m_intRvMasterTestCaseId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsAlien = false;
		$this->m_boolSendCaliforniaConsumerReport = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ssn'] ) && $boolDirectSet ) $this->set( 'm_intSsn', trim( $arrValues['ssn'] ) ); elseif( isset( $arrValues['ssn'] ) ) $this->setSsn( $arrValues['ssn'] );
		if( isset( $arrValues['ssn_masked'] ) && $boolDirectSet ) $this->set( 'm_strSsnMasked', trim( stripcslashes( $arrValues['ssn_masked'] ) ) ); elseif( isset( $arrValues['ssn_masked'] ) ) $this->setSsnMasked( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ssn_masked'] ) : $arrValues['ssn_masked'] );
		if( isset( $arrValues['property_state_code'] ) && $boolDirectSet ) $this->set( 'm_strPropertyStateCode', trim( stripcslashes( $arrValues['property_state_code'] ) ) ); elseif( isset( $arrValues['property_state_code'] ) ) $this->setPropertyStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_state_code'] ) : $arrValues['property_state_code'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['package_id'] ) && $boolDirectSet ) $this->set( 'm_intPackageId', trim( $arrValues['package_id'] ) ); elseif( isset( $arrValues['package_id'] ) ) $this->setPackageId( $arrValues['package_id'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( stripcslashes( $arrValues['name_first'] ) ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_first'] ) : $arrValues['name_first'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( stripcslashes( $arrValues['name_last'] ) ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_last'] ) : $arrValues['name_last'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( stripcslashes( $arrValues['name_middle'] ) ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_middle'] ) : $arrValues['name_middle'] );
		if( isset( $arrValues['birth_date'] ) && $boolDirectSet ) $this->set( 'm_strBirthDate', trim( stripcslashes( $arrValues['birth_date'] ) ) ); elseif( isset( $arrValues['birth_date'] ) ) $this->setBirthDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['birth_date'] ) : $arrValues['birth_date'] );
		if( isset( $arrValues['is_alien'] ) && $boolDirectSet ) $this->set( 'm_boolIsAlien', trim( stripcslashes( $arrValues['is_alien'] ) ) ); elseif( isset( $arrValues['is_alien'] ) ) $this->setIsAlien( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_alien'] ) : $arrValues['is_alien'] );
		if( isset( $arrValues['send_california_consumer_report'] ) && $boolDirectSet ) $this->set( 'm_boolSendCaliforniaConsumerReport', trim( stripcslashes( $arrValues['send_california_consumer_report'] ) ) ); elseif( isset( $arrValues['send_california_consumer_report'] ) ) $this->setSendCaliforniaConsumerReport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['send_california_consumer_report'] ) : $arrValues['send_california_consumer_report'] );
		if( isset( $arrValues['applicant_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantTypeId', trim( $arrValues['applicant_type_id'] ) ); elseif( isset( $arrValues['applicant_type_id'] ) ) $this->setApplicantTypeId( $arrValues['applicant_type_id'] );
		if( isset( $arrValues['details'] ) && $boolDirectSet ) $this->set( 'm_strDetails', trim( stripcslashes( $arrValues['details'] ) ) ); elseif( isset( $arrValues['details'] ) ) $this->setDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['details'] ) : $arrValues['details'] );
		if( isset( $arrValues['monthly_income'] ) && $boolDirectSet ) $this->set( 'm_intMonthlyIncome', trim( $arrValues['monthly_income'] ) ); elseif( isset( $arrValues['monthly_income'] ) ) $this->setMonthlyIncome( $arrValues['monthly_income'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['region'] ) && $boolDirectSet ) $this->set( 'm_strRegion', trim( stripcslashes( $arrValues['region'] ) ) ); elseif( isset( $arrValues['region'] ) ) $this->setRegion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['region'] ) : $arrValues['region'] );
		if( isset( $arrValues['municipality'] ) && $boolDirectSet ) $this->set( 'm_strMunicipality', trim( stripcslashes( $arrValues['municipality'] ) ) ); elseif( isset( $arrValues['municipality'] ) ) $this->setMunicipality( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['municipality'] ) : $arrValues['municipality'] );
		if( isset( $arrValues['address_line'] ) && $boolDirectSet ) $this->set( 'm_strAddressLine', trim( stripcslashes( $arrValues['address_line'] ) ) ); elseif( isset( $arrValues['address_line'] ) ) $this->setAddressLine( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['address_line'] ) : $arrValues['address_line'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['rv_master_test_case_id'] ) && $boolDirectSet ) $this->set( 'm_intRvMasterTestCaseId', trim( $arrValues['rv_master_test_case_id'] ) ); elseif( isset( $arrValues['rv_master_test_case_id'] ) ) $this->setRvMasterTestCaseId( $arrValues['rv_master_test_case_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSsn( $intSsn ) {
		$this->set( 'm_intSsn', CStrings::strToIntDef( $intSsn, NULL, false ) );
	}

	public function getSsn() {
		return $this->m_intSsn;
	}

	public function sqlSsn() {
		return ( true == isset( $this->m_intSsn ) ) ? ( string ) $this->m_intSsn : 'NULL';
	}

	public function setSsnMasked( $strSsnMasked ) {
		$this->set( 'm_strSsnMasked', CStrings::strTrimDef( $strSsnMasked, 10, NULL, true ) );
	}

	public function getSsnMasked() {
		return $this->m_strSsnMasked;
	}

	public function sqlSsnMasked() {
		return ( true == isset( $this->m_strSsnMasked ) ) ? '\'' . addslashes( $this->m_strSsnMasked ) . '\'' : 'NULL';
	}

	public function setPropertyStateCode( $strPropertyStateCode ) {
		$this->set( 'm_strPropertyStateCode', CStrings::strTrimDef( $strPropertyStateCode, 4, NULL, true ) );
	}

	public function getPropertyStateCode() {
		return $this->m_strPropertyStateCode;
	}

	public function sqlPropertyStateCode() {
		return ( true == isset( $this->m_strPropertyStateCode ) ) ? '\'' . addslashes( $this->m_strPropertyStateCode ) . '\'' : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setPackageId( $intPackageId ) {
		$this->set( 'm_intPackageId', CStrings::strToIntDef( $intPackageId, NULL, false ) );
	}

	public function getPackageId() {
		return $this->m_intPackageId;
	}

	public function sqlPackageId() {
		return ( true == isset( $this->m_intPackageId ) ) ? ( string ) $this->m_intPackageId : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 100, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? '\'' . addslashes( $this->m_strNameFirst ) . '\'' : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 100, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? '\'' . addslashes( $this->m_strNameLast ) . '\'' : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 100, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? '\'' . addslashes( $this->m_strNameMiddle ) . '\'' : 'NULL';
	}

	public function setBirthDate( $strBirthDate ) {
		$this->set( 'm_strBirthDate', CStrings::strTrimDef( $strBirthDate, 25, NULL, true ) );
	}

	public function getBirthDate() {
		return $this->m_strBirthDate;
	}

	public function sqlBirthDate() {
		return ( true == isset( $this->m_strBirthDate ) ) ? '\'' . addslashes( $this->m_strBirthDate ) . '\'' : 'NULL';
	}

	public function setIsAlien( $boolIsAlien ) {
		$this->set( 'm_boolIsAlien', CStrings::strToBool( $boolIsAlien ) );
	}

	public function getIsAlien() {
		return $this->m_boolIsAlien;
	}

	public function sqlIsAlien() {
		return ( true == isset( $this->m_boolIsAlien ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAlien ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSendCaliforniaConsumerReport( $boolSendCaliforniaConsumerReport ) {
		$this->set( 'm_boolSendCaliforniaConsumerReport', CStrings::strToBool( $boolSendCaliforniaConsumerReport ) );
	}

	public function getSendCaliforniaConsumerReport() {
		return $this->m_boolSendCaliforniaConsumerReport;
	}

	public function sqlSendCaliforniaConsumerReport() {
		return ( true == isset( $this->m_boolSendCaliforniaConsumerReport ) ) ? '\'' . ( true == ( bool ) $this->m_boolSendCaliforniaConsumerReport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setApplicantTypeId( $intApplicantTypeId ) {
		$this->set( 'm_intApplicantTypeId', CStrings::strToIntDef( $intApplicantTypeId, NULL, false ) );
	}

	public function getApplicantTypeId() {
		return $this->m_intApplicantTypeId;
	}

	public function sqlApplicantTypeId() {
		return ( true == isset( $this->m_intApplicantTypeId ) ) ? ( string ) $this->m_intApplicantTypeId : 'NULL';
	}

	public function setDetails( $strDetails ) {
		$this->set( 'm_strDetails', CStrings::strTrimDef( $strDetails, 100, NULL, true ) );
	}

	public function getDetails() {
		return $this->m_strDetails;
	}

	public function sqlDetails() {
		return ( true == isset( $this->m_strDetails ) ) ? '\'' . addslashes( $this->m_strDetails ) . '\'' : 'NULL';
	}

	public function setMonthlyIncome( $intMonthlyIncome ) {
		$this->set( 'm_intMonthlyIncome', CStrings::strToIntDef( $intMonthlyIncome, NULL, false ) );
	}

	public function getMonthlyIncome() {
		return $this->m_intMonthlyIncome;
	}

	public function sqlMonthlyIncome() {
		return ( true == isset( $this->m_intMonthlyIncome ) ) ? ( string ) $this->m_intMonthlyIncome : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 50, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 50, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 25, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setRegion( $strRegion ) {
		$this->set( 'm_strRegion', CStrings::strTrimDef( $strRegion, 25, NULL, true ) );
	}

	public function getRegion() {
		return $this->m_strRegion;
	}

	public function sqlRegion() {
		return ( true == isset( $this->m_strRegion ) ) ? '\'' . addslashes( $this->m_strRegion ) . '\'' : 'NULL';
	}

	public function setMunicipality( $strMunicipality ) {
		$this->set( 'm_strMunicipality', CStrings::strTrimDef( $strMunicipality, 25, NULL, true ) );
	}

	public function getMunicipality() {
		return $this->m_strMunicipality;
	}

	public function sqlMunicipality() {
		return ( true == isset( $this->m_strMunicipality ) ) ? '\'' . addslashes( $this->m_strMunicipality ) . '\'' : 'NULL';
	}

	public function setAddressLine( $strAddressLine ) {
		$this->set( 'm_strAddressLine', CStrings::strTrimDef( $strAddressLine, 25, NULL, true ) );
	}

	public function getAddressLine() {
		return $this->m_strAddressLine;
	}

	public function sqlAddressLine() {
		return ( true == isset( $this->m_strAddressLine ) ) ? '\'' . addslashes( $this->m_strAddressLine ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 25, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setRvMasterTestCaseId( $intRvMasterTestCaseId ) {
		$this->set( 'm_intRvMasterTestCaseId', CStrings::strToIntDef( $intRvMasterTestCaseId, NULL, false ) );
	}

	public function getRvMasterTestCaseId() {
		return $this->m_intRvMasterTestCaseId;
	}

	public function sqlRvMasterTestCaseId() {
		return ( true == isset( $this->m_intRvMasterTestCaseId ) ) ? ( string ) $this->m_intRvMasterTestCaseId : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'property_id' => $this->getPropertyId(),
			'ssn' => $this->getSsn(),
			'ssn_masked' => $this->getSsnMasked(),
			'property_state_code' => $this->getPropertyStateCode(),
			'applicant_id' => $this->getApplicantId(),
			'package_id' => $this->getPackageId(),
			'name_first' => $this->getNameFirst(),
			'name_last' => $this->getNameLast(),
			'name_middle' => $this->getNameMiddle(),
			'birth_date' => $this->getBirthDate(),
			'is_alien' => $this->getIsAlien(),
			'send_california_consumer_report' => $this->getSendCaliforniaConsumerReport(),
			'applicant_type_id' => $this->getApplicantTypeId(),
			'details' => $this->getDetails(),
			'monthly_income' => $this->getMonthlyIncome(),
			'email_address' => $this->getEmailAddress(),
			'phone_number' => $this->getPhoneNumber(),
			'postal_code' => $this->getPostalCode(),
			'region' => $this->getRegion(),
			'municipality' => $this->getMunicipality(),
			'address_line' => $this->getAddressLine(),
			'state_code' => $this->getStateCode(),
			'rv_master_test_case_id' => $this->getRvMasterTestCaseId()
		);
	}

}
?>