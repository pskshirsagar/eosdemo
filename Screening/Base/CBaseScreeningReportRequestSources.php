<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningReportRequestSources
 * Do not add any new functions to this class.
 */

class CBaseScreeningReportRequestSources extends CEosPluralBase {

	/**
	 * @return CScreeningReportRequestSource[]
	 */
	public static function fetchScreeningReportRequestSources( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningReportRequestSource::class, $objDatabase );
	}

	/**
	 * @return CScreeningReportRequestSource
	 */
	public static function fetchScreeningReportRequestSource( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningReportRequestSource::class, $objDatabase );
	}

	public static function fetchScreeningReportRequestSourceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_report_request_sources', $objDatabase );
	}

	public static function fetchScreeningReportRequestSourceById( $intId, $objDatabase ) {
		return self::fetchScreeningReportRequestSource( sprintf( 'SELECT * FROM screening_report_request_sources WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>