<?php

class CBaseScreeningProviderBaseRate extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_provider_base_rates';

	protected $m_intId;
	protected $m_intScreeningDataProviderId;
	protected $m_strStateCode;
	protected $m_strCountyName;
	protected $m_fltSearchFee;
	protected $m_fltCourtFee;
	protected $m_fltClientCost;
	protected $m_boolIsActive;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_data_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningDataProviderId', trim( $arrValues['screening_data_provider_id'] ) ); elseif( isset( $arrValues['screening_data_provider_id'] ) ) $this->setScreeningDataProviderId( $arrValues['screening_data_provider_id'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['county_name'] ) && $boolDirectSet ) $this->set( 'm_strCountyName', trim( stripcslashes( $arrValues['county_name'] ) ) ); elseif( isset( $arrValues['county_name'] ) ) $this->setCountyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['county_name'] ) : $arrValues['county_name'] );
		if( isset( $arrValues['search_fee'] ) && $boolDirectSet ) $this->set( 'm_fltSearchFee', trim( $arrValues['search_fee'] ) ); elseif( isset( $arrValues['search_fee'] ) ) $this->setSearchFee( $arrValues['search_fee'] );
		if( isset( $arrValues['court_fee'] ) && $boolDirectSet ) $this->set( 'm_fltCourtFee', trim( $arrValues['court_fee'] ) ); elseif( isset( $arrValues['court_fee'] ) ) $this->setCourtFee( $arrValues['court_fee'] );
		if( isset( $arrValues['client_cost'] ) && $boolDirectSet ) $this->set( 'm_fltClientCost', trim( $arrValues['client_cost'] ) ); elseif( isset( $arrValues['client_cost'] ) ) $this->setClientCost( $arrValues['client_cost'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningDataProviderId( $intScreeningDataProviderId ) {
		$this->set( 'm_intScreeningDataProviderId', CStrings::strToIntDef( $intScreeningDataProviderId, NULL, false ) );
	}

	public function getScreeningDataProviderId() {
		return $this->m_intScreeningDataProviderId;
	}

	public function sqlScreeningDataProviderId() {
		return ( true == isset( $this->m_intScreeningDataProviderId ) ) ? ( string ) $this->m_intScreeningDataProviderId : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, -1, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setCountyName( $strCountyName ) {
		$this->set( 'm_strCountyName', CStrings::strTrimDef( $strCountyName, -1, NULL, true ) );
	}

	public function getCountyName() {
		return $this->m_strCountyName;
	}

	public function sqlCountyName() {
		return ( true == isset( $this->m_strCountyName ) ) ? '\'' . addslashes( $this->m_strCountyName ) . '\'' : 'NULL';
	}

	public function setSearchFee( $fltSearchFee ) {
		$this->set( 'm_fltSearchFee', CStrings::strToFloatDef( $fltSearchFee, NULL, false, 2 ) );
	}

	public function getSearchFee() {
		return $this->m_fltSearchFee;
	}

	public function sqlSearchFee() {
		return ( true == isset( $this->m_fltSearchFee ) ) ? ( string ) $this->m_fltSearchFee : 'NULL';
	}

	public function setCourtFee( $fltCourtFee ) {
		$this->set( 'm_fltCourtFee', CStrings::strToFloatDef( $fltCourtFee, NULL, false, 2 ) );
	}

	public function getCourtFee() {
		return $this->m_fltCourtFee;
	}

	public function sqlCourtFee() {
		return ( true == isset( $this->m_fltCourtFee ) ) ? ( string ) $this->m_fltCourtFee : 'NULL';
	}

	public function setClientCost( $fltClientCost ) {
		$this->set( 'm_fltClientCost', CStrings::strToFloatDef( $fltClientCost, NULL, false, 2 ) );
	}

	public function getClientCost() {
		return $this->m_fltClientCost;
	}

	public function sqlClientCost() {
		return ( true == isset( $this->m_fltClientCost ) ) ? ( string ) $this->m_fltClientCost : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, screening_data_provider_id, state_code, county_name, search_fee, court_fee, client_cost, is_active, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlScreeningDataProviderId() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlCountyName() . ', ' .
 						$this->sqlSearchFee() . ', ' .
 						$this->sqlCourtFee() . ', ' .
 						$this->sqlClientCost() . ', ' .
 						$this->sqlIsActive() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_data_provider_id = ' . $this->sqlScreeningDataProviderId() . ','; } elseif( true == array_key_exists( 'ScreeningDataProviderId', $this->getChangedColumns() ) ) { $strSql .= ' screening_data_provider_id = ' . $this->sqlScreeningDataProviderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' county_name = ' . $this->sqlCountyName() . ','; } elseif( true == array_key_exists( 'CountyName', $this->getChangedColumns() ) ) { $strSql .= ' county_name = ' . $this->sqlCountyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' search_fee = ' . $this->sqlSearchFee() . ','; } elseif( true == array_key_exists( 'SearchFee', $this->getChangedColumns() ) ) { $strSql .= ' search_fee = ' . $this->sqlSearchFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' court_fee = ' . $this->sqlCourtFee() . ','; } elseif( true == array_key_exists( 'CourtFee', $this->getChangedColumns() ) ) { $strSql .= ' court_fee = ' . $this->sqlCourtFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_cost = ' . $this->sqlClientCost() . ','; } elseif( true == array_key_exists( 'ClientCost', $this->getChangedColumns() ) ) { $strSql .= ' client_cost = ' . $this->sqlClientCost() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_data_provider_id' => $this->getScreeningDataProviderId(),
			'state_code' => $this->getStateCode(),
			'county_name' => $this->getCountyName(),
			'search_fee' => $this->getSearchFee(),
			'court_fee' => $this->getCourtFee(),
			'client_cost' => $this->getClientCost(),
			'is_active' => $this->getIsActive(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>