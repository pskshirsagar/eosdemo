<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningAccountRates
 * Do not add any new functions to this class.
 */

class CBaseScreeningAccountRates extends CEosPluralBase {

	/**
	 * @return CScreeningAccountRate[]
	 */
	public static function fetchScreeningAccountRates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningAccountRate', $objDatabase );
	}

	/**
	 * @return CScreeningAccountRate
	 */
	public static function fetchScreeningAccountRate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningAccountRate', $objDatabase );
	}

	public static function fetchScreeningAccountRateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_account_rates', $objDatabase );
	}

	public static function fetchScreeningAccountRateById( $intId, $objDatabase ) {
		return self::fetchScreeningAccountRate( sprintf( 'SELECT * FROM screening_account_rates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningAccountRatesByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningAccountRates( sprintf( 'SELECT * FROM screening_account_rates WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningAccountRatesByScreeningAccountId( $intScreeningAccountId, $objDatabase ) {
		return self::fetchScreeningAccountRates( sprintf( 'SELECT * FROM screening_account_rates WHERE screening_account_id = %d', ( int ) $intScreeningAccountId ), $objDatabase );
	}

	public static function fetchScreeningAccountRatesByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreeningAccountRates( sprintf( 'SELECT * FROM screening_account_rates WHERE screen_type_id = %d', ( int ) $intScreenTypeId ), $objDatabase );
	}

	public static function fetchScreeningAccountRatesByScreeningDataProviderId( $intScreeningDataProviderId, $objDatabase ) {
		return self::fetchScreeningAccountRates( sprintf( 'SELECT * FROM screening_account_rates WHERE screening_data_provider_id = %d', ( int ) $intScreeningDataProviderId ), $objDatabase );
	}

}
?>