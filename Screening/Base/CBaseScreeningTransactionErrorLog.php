<?php

class CBaseScreeningTransactionErrorLog extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_transaction_error_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScreeningTransactionId;
	protected $m_intScreeningDataProviderId;
	protected $m_intScreeningErrorTypeId;
	protected $m_strErrorCode;
	protected $m_strDescription;
	protected $m_boolIsActive;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screening_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningTransactionId', trim( $arrValues['screening_transaction_id'] ) ); elseif( isset( $arrValues['screening_transaction_id'] ) ) $this->setScreeningTransactionId( $arrValues['screening_transaction_id'] );
		if( isset( $arrValues['screening_data_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningDataProviderId', trim( $arrValues['screening_data_provider_id'] ) ); elseif( isset( $arrValues['screening_data_provider_id'] ) ) $this->setScreeningDataProviderId( $arrValues['screening_data_provider_id'] );
		if( isset( $arrValues['screening_error_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningErrorTypeId', trim( $arrValues['screening_error_type_id'] ) ); elseif( isset( $arrValues['screening_error_type_id'] ) ) $this->setScreeningErrorTypeId( $arrValues['screening_error_type_id'] );
		if( isset( $arrValues['error_code'] ) && $boolDirectSet ) $this->set( 'm_strErrorCode', trim( stripcslashes( $arrValues['error_code'] ) ) ); elseif( isset( $arrValues['error_code'] ) ) $this->setErrorCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['error_code'] ) : $arrValues['error_code'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreeningTransactionId( $intScreeningTransactionId ) {
		$this->set( 'm_intScreeningTransactionId', CStrings::strToIntDef( $intScreeningTransactionId, NULL, false ) );
	}

	public function getScreeningTransactionId() {
		return $this->m_intScreeningTransactionId;
	}

	public function sqlScreeningTransactionId() {
		return ( true == isset( $this->m_intScreeningTransactionId ) ) ? ( string ) $this->m_intScreeningTransactionId : 'NULL';
	}

	public function setScreeningDataProviderId( $intScreeningDataProviderId ) {
		$this->set( 'm_intScreeningDataProviderId', CStrings::strToIntDef( $intScreeningDataProviderId, NULL, false ) );
	}

	public function getScreeningDataProviderId() {
		return $this->m_intScreeningDataProviderId;
	}

	public function sqlScreeningDataProviderId() {
		return ( true == isset( $this->m_intScreeningDataProviderId ) ) ? ( string ) $this->m_intScreeningDataProviderId : 'NULL';
	}

	public function setScreeningErrorTypeId( $intScreeningErrorTypeId ) {
		$this->set( 'm_intScreeningErrorTypeId', CStrings::strToIntDef( $intScreeningErrorTypeId, NULL, false ) );
	}

	public function getScreeningErrorTypeId() {
		return $this->m_intScreeningErrorTypeId;
	}

	public function sqlScreeningErrorTypeId() {
		return ( true == isset( $this->m_intScreeningErrorTypeId ) ) ? ( string ) $this->m_intScreeningErrorTypeId : 'NULL';
	}

	public function setErrorCode( $strErrorCode ) {
		$this->set( 'm_strErrorCode', CStrings::strTrimDef( $strErrorCode, 50, NULL, true ) );
	}

	public function getErrorCode() {
		return $this->m_strErrorCode;
	}

	public function sqlErrorCode() {
		return ( true == isset( $this->m_strErrorCode ) ) ? '\'' . addslashes( $this->m_strErrorCode ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, screening_transaction_id, screening_data_provider_id, screening_error_type_id, error_code, description, is_active, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlScreeningTransactionId() . ', ' .
 						$this->sqlScreeningDataProviderId() . ', ' .
 						$this->sqlScreeningErrorTypeId() . ', ' .
 						$this->sqlErrorCode() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlIsActive() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_transaction_id = ' . $this->sqlScreeningTransactionId() . ','; } elseif( true == array_key_exists( 'ScreeningTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' screening_transaction_id = ' . $this->sqlScreeningTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_data_provider_id = ' . $this->sqlScreeningDataProviderId() . ','; } elseif( true == array_key_exists( 'ScreeningDataProviderId', $this->getChangedColumns() ) ) { $strSql .= ' screening_data_provider_id = ' . $this->sqlScreeningDataProviderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_error_type_id = ' . $this->sqlScreeningErrorTypeId() . ','; } elseif( true == array_key_exists( 'ScreeningErrorTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_error_type_id = ' . $this->sqlScreeningErrorTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_code = ' . $this->sqlErrorCode() . ','; } elseif( true == array_key_exists( 'ErrorCode', $this->getChangedColumns() ) ) { $strSql .= ' error_code = ' . $this->sqlErrorCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'screening_transaction_id' => $this->getScreeningTransactionId(),
			'screening_data_provider_id' => $this->getScreeningDataProviderId(),
			'screening_error_type_id' => $this->getScreeningErrorTypeId(),
			'error_code' => $this->getErrorCode(),
			'description' => $this->getDescription(),
			'is_active' => $this->getIsActive(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>