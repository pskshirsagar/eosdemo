<?php

class CBaseConsumerDoc extends CEosSingularBase {

	const TABLE_NAME = 'public.consumer_docs';

	protected $m_intId;
	protected $m_intConsumerId;
	protected $m_intConsumerDisputeId;
	protected $m_intConsumerDocumentTypeId;
	protected $m_intDisputeLogId;
	protected $m_strDocumentPath;
	protected $m_strDescription;
	protected $m_intIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['consumer_id'] ) && $boolDirectSet ) $this->set( 'm_intConsumerId', trim( $arrValues['consumer_id'] ) ); elseif( isset( $arrValues['consumer_id'] ) ) $this->setConsumerId( $arrValues['consumer_id'] );
		if( isset( $arrValues['consumer_dispute_id'] ) && $boolDirectSet ) $this->set( 'm_intConsumerDisputeId', trim( $arrValues['consumer_dispute_id'] ) ); elseif( isset( $arrValues['consumer_dispute_id'] ) ) $this->setConsumerDisputeId( $arrValues['consumer_dispute_id'] );
		if( isset( $arrValues['consumer_document_type_id'] ) && $boolDirectSet ) $this->set( 'm_intConsumerDocumentTypeId', trim( $arrValues['consumer_document_type_id'] ) ); elseif( isset( $arrValues['consumer_document_type_id'] ) ) $this->setConsumerDocumentTypeId( $arrValues['consumer_document_type_id'] );
		if( isset( $arrValues['dispute_log_id'] ) && $boolDirectSet ) $this->set( 'm_intDisputeLogId', trim( $arrValues['dispute_log_id'] ) ); elseif( isset( $arrValues['dispute_log_id'] ) ) $this->setDisputeLogId( $arrValues['dispute_log_id'] );
		if( isset( $arrValues['document_path'] ) && $boolDirectSet ) $this->set( 'm_strDocumentPath', trim( stripcslashes( $arrValues['document_path'] ) ) ); elseif( isset( $arrValues['document_path'] ) ) $this->setDocumentPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['document_path'] ) : $arrValues['document_path'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setConsumerId( $intConsumerId ) {
		$this->set( 'm_intConsumerId', CStrings::strToIntDef( $intConsumerId, NULL, false ) );
	}

	public function getConsumerId() {
		return $this->m_intConsumerId;
	}

	public function sqlConsumerId() {
		return ( true == isset( $this->m_intConsumerId ) ) ? ( string ) $this->m_intConsumerId : 'NULL';
	}

	public function setConsumerDisputeId( $intConsumerDisputeId ) {
		$this->set( 'm_intConsumerDisputeId', CStrings::strToIntDef( $intConsumerDisputeId, NULL, false ) );
	}

	public function getConsumerDisputeId() {
		return $this->m_intConsumerDisputeId;
	}

	public function sqlConsumerDisputeId() {
		return ( true == isset( $this->m_intConsumerDisputeId ) ) ? ( string ) $this->m_intConsumerDisputeId : 'NULL';
	}

	public function setConsumerDocumentTypeId( $intConsumerDocumentTypeId ) {
		$this->set( 'm_intConsumerDocumentTypeId', CStrings::strToIntDef( $intConsumerDocumentTypeId, NULL, false ) );
	}

	public function getConsumerDocumentTypeId() {
		return $this->m_intConsumerDocumentTypeId;
	}

	public function sqlConsumerDocumentTypeId() {
		return ( true == isset( $this->m_intConsumerDocumentTypeId ) ) ? ( string ) $this->m_intConsumerDocumentTypeId : 'NULL';
	}

	public function setDisputeLogId( $intDisputeLogId ) {
		$this->set( 'm_intDisputeLogId', CStrings::strToIntDef( $intDisputeLogId, NULL, false ) );
	}

	public function getDisputeLogId() {
		return $this->m_intDisputeLogId;
	}

	public function sqlDisputeLogId() {
		return ( true == isset( $this->m_intDisputeLogId ) ) ? ( string ) $this->m_intDisputeLogId : 'NULL';
	}

	public function setDocumentPath( $strDocumentPath ) {
		$this->set( 'm_strDocumentPath', CStrings::strTrimDef( $strDocumentPath, 240, NULL, true ) );
	}

	public function getDocumentPath() {
		return $this->m_strDocumentPath;
	}

	public function sqlDocumentPath() {
		return ( true == isset( $this->m_strDocumentPath ) ) ? '\'' . addslashes( $this->m_strDocumentPath ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, consumer_id, consumer_dispute_id, consumer_document_type_id, dispute_log_id, document_path, description, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlConsumerId() . ', ' .
 						$this->sqlConsumerDisputeId() . ', ' .
 						$this->sqlConsumerDocumentTypeId() . ', ' .
 						$this->sqlDisputeLogId() . ', ' .
 						$this->sqlDocumentPath() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumer_id = ' . $this->sqlConsumerId() . ','; } elseif( true == array_key_exists( 'ConsumerId', $this->getChangedColumns() ) ) { $strSql .= ' consumer_id = ' . $this->sqlConsumerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumer_dispute_id = ' . $this->sqlConsumerDisputeId() . ','; } elseif( true == array_key_exists( 'ConsumerDisputeId', $this->getChangedColumns() ) ) { $strSql .= ' consumer_dispute_id = ' . $this->sqlConsumerDisputeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumer_document_type_id = ' . $this->sqlConsumerDocumentTypeId() . ','; } elseif( true == array_key_exists( 'ConsumerDocumentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' consumer_document_type_id = ' . $this->sqlConsumerDocumentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dispute_log_id = ' . $this->sqlDisputeLogId() . ','; } elseif( true == array_key_exists( 'DisputeLogId', $this->getChangedColumns() ) ) { $strSql .= ' dispute_log_id = ' . $this->sqlDisputeLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_path = ' . $this->sqlDocumentPath() . ','; } elseif( true == array_key_exists( 'DocumentPath', $this->getChangedColumns() ) ) { $strSql .= ' document_path = ' . $this->sqlDocumentPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'consumer_id' => $this->getConsumerId(),
			'consumer_dispute_id' => $this->getConsumerDisputeId(),
			'consumer_document_type_id' => $this->getConsumerDocumentTypeId(),
			'dispute_log_id' => $this->getDisputeLogId(),
			'document_path' => $this->getDocumentPath(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>