<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CTransactionReviewLogTypes
 * Do not add any new functions to this class.
 */

class CBaseTransactionReviewLogTypes extends CEosPluralBase {

	/**
	 * @return CTransactionReviewLogType[]
	 */
	public static function fetchTransactionReviewLogTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTransactionReviewLogType', $objDatabase );
	}

	/**
	 * @return CTransactionReviewLogType
	 */
	public static function fetchTransactionReviewLogType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTransactionReviewLogType', $objDatabase );
	}

	public static function fetchTransactionReviewLogTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transaction_review_log_types', $objDatabase );
	}

	public static function fetchTransactionReviewLogTypeById( $intId, $objDatabase ) {
		return self::fetchTransactionReviewLogType( sprintf( 'SELECT * FROM transaction_review_log_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>