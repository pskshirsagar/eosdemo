<?php

class CBaseCriminalClassificationKeyword extends CEosSingularBase {

	const TABLE_NAME = 'public.criminal_classification_keywords';

	protected $m_intId;
	protected $m_intKeywordTypeId;
	protected $m_intClassificationTypeId;
	protected $m_strKeyword;
	protected $m_intSortOrder;
	protected $m_intHighThreshold;
	protected $m_intLowThreshold;
	protected $m_intIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['keyword_type_id'] ) && $boolDirectSet ) $this->set( 'm_intKeywordTypeId', trim( $arrValues['keyword_type_id'] ) ); elseif( isset( $arrValues['keyword_type_id'] ) ) $this->setKeywordTypeId( $arrValues['keyword_type_id'] );
		if( isset( $arrValues['classification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intClassificationTypeId', trim( $arrValues['classification_type_id'] ) ); elseif( isset( $arrValues['classification_type_id'] ) ) $this->setClassificationTypeId( $arrValues['classification_type_id'] );
		if( isset( $arrValues['keyword'] ) && $boolDirectSet ) $this->set( 'm_strKeyword', trim( stripcslashes( $arrValues['keyword'] ) ) ); elseif( isset( $arrValues['keyword'] ) ) $this->setKeyword( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['keyword'] ) : $arrValues['keyword'] );
		if( isset( $arrValues['sort_order'] ) && $boolDirectSet ) $this->set( 'm_intSortOrder', trim( $arrValues['sort_order'] ) ); elseif( isset( $arrValues['sort_order'] ) ) $this->setSortOrder( $arrValues['sort_order'] );
		if( isset( $arrValues['high_threshold'] ) && $boolDirectSet ) $this->set( 'm_intHighThreshold', trim( $arrValues['high_threshold'] ) ); elseif( isset( $arrValues['high_threshold'] ) ) $this->setHighThreshold( $arrValues['high_threshold'] );
		if( isset( $arrValues['low_threshold'] ) && $boolDirectSet ) $this->set( 'm_intLowThreshold', trim( $arrValues['low_threshold'] ) ); elseif( isset( $arrValues['low_threshold'] ) ) $this->setLowThreshold( $arrValues['low_threshold'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setKeywordTypeId( $intKeywordTypeId ) {
		$this->set( 'm_intKeywordTypeId', CStrings::strToIntDef( $intKeywordTypeId, NULL, false ) );
	}

	public function getKeywordTypeId() {
		return $this->m_intKeywordTypeId;
	}

	public function sqlKeywordTypeId() {
		return ( true == isset( $this->m_intKeywordTypeId ) ) ? ( string ) $this->m_intKeywordTypeId : 'NULL';
	}

	public function setClassificationTypeId( $intClassificationTypeId ) {
		$this->set( 'm_intClassificationTypeId', CStrings::strToIntDef( $intClassificationTypeId, NULL, false ) );
	}

	public function getClassificationTypeId() {
		return $this->m_intClassificationTypeId;
	}

	public function sqlClassificationTypeId() {
		return ( true == isset( $this->m_intClassificationTypeId ) ) ? ( string ) $this->m_intClassificationTypeId : 'NULL';
	}

	public function setKeyword( $strKeyword ) {
		$this->set( 'm_strKeyword', CStrings::strTrimDef( $strKeyword, -1, NULL, true ) );
	}

	public function getKeyword() {
		return $this->m_strKeyword;
	}

	public function sqlKeyword() {
		return ( true == isset( $this->m_strKeyword ) ) ? '\'' . addslashes( $this->m_strKeyword ) . '\'' : 'NULL';
	}

	public function setSortOrder( $intSortOrder ) {
		$this->set( 'm_intSortOrder', CStrings::strToIntDef( $intSortOrder, NULL, false ) );
	}

	public function getSortOrder() {
		return $this->m_intSortOrder;
	}

	public function sqlSortOrder() {
		return ( true == isset( $this->m_intSortOrder ) ) ? ( string ) $this->m_intSortOrder : 'NULL';
	}

	public function setHighThreshold( $intHighThreshold ) {
		$this->set( 'm_intHighThreshold', CStrings::strToIntDef( $intHighThreshold, NULL, false ) );
	}

	public function getHighThreshold() {
		return $this->m_intHighThreshold;
	}

	public function sqlHighThreshold() {
		return ( true == isset( $this->m_intHighThreshold ) ) ? ( string ) $this->m_intHighThreshold : 'NULL';
	}

	public function setLowThreshold( $intLowThreshold ) {
		$this->set( 'm_intLowThreshold', CStrings::strToIntDef( $intLowThreshold, NULL, false ) );
	}

	public function getLowThreshold() {
		return $this->m_intLowThreshold;
	}

	public function sqlLowThreshold() {
		return ( true == isset( $this->m_intLowThreshold ) ) ? ( string ) $this->m_intLowThreshold : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, keyword_type_id, classification_type_id, keyword, sort_order, high_threshold, low_threshold, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlKeywordTypeId() . ', ' .
 						$this->sqlClassificationTypeId() . ', ' .
 						$this->sqlKeyword() . ', ' .
 						$this->sqlSortOrder() . ', ' .
 						$this->sqlHighThreshold() . ', ' .
 						$this->sqlLowThreshold() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' keyword_type_id = ' . $this->sqlKeywordTypeId() . ','; } elseif( true == array_key_exists( 'KeywordTypeId', $this->getChangedColumns() ) ) { $strSql .= ' keyword_type_id = ' . $this->sqlKeywordTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' classification_type_id = ' . $this->sqlClassificationTypeId() . ','; } elseif( true == array_key_exists( 'ClassificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' classification_type_id = ' . $this->sqlClassificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' keyword = ' . $this->sqlKeyword() . ','; } elseif( true == array_key_exists( 'Keyword', $this->getChangedColumns() ) ) { $strSql .= ' keyword = ' . $this->sqlKeyword() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sort_order = ' . $this->sqlSortOrder() . ','; } elseif( true == array_key_exists( 'SortOrder', $this->getChangedColumns() ) ) { $strSql .= ' sort_order = ' . $this->sqlSortOrder() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' high_threshold = ' . $this->sqlHighThreshold() . ','; } elseif( true == array_key_exists( 'HighThreshold', $this->getChangedColumns() ) ) { $strSql .= ' high_threshold = ' . $this->sqlHighThreshold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' low_threshold = ' . $this->sqlLowThreshold() . ','; } elseif( true == array_key_exists( 'LowThreshold', $this->getChangedColumns() ) ) { $strSql .= ' low_threshold = ' . $this->sqlLowThreshold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'keyword_type_id' => $this->getKeywordTypeId(),
			'classification_type_id' => $this->getClassificationTypeId(),
			'keyword' => $this->getKeyword(),
			'sort_order' => $this->getSortOrder(),
			'high_threshold' => $this->getHighThreshold(),
			'low_threshold' => $this->getLowThreshold(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>