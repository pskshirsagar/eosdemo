<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDelinquentEventTypes
 * Do not add any new functions to this class.
 */

class CBaseDelinquentEventTypes extends CEosPluralBase {

	/**
	 * @return CDelinquentEventType[]
	 */
	public static function fetchDelinquentEventTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDelinquentEventType', $objDatabase );
	}

	/**
	 * @return CDelinquentEventType
	 */
	public static function fetchDelinquentEventType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDelinquentEventType', $objDatabase );
	}

	public static function fetchDelinquentEventTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'delinquent_event_types', $objDatabase );
	}

	public static function fetchDelinquentEventTypeById( $intId, $objDatabase ) {
		return self::fetchDelinquentEventType( sprintf( 'SELECT * FROM delinquent_event_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>