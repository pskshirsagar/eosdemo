<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CFollowupActionTypes
 * Do not add any new functions to this class.
 */

class CBaseFollowupActionTypes extends CEosPluralBase {

	/**
	 * @return CFollowupActionType[]
	 */
	public static function fetchFollowupActionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CFollowupActionType', $objDatabase );
	}

	/**
	 * @return CFollowupActionType
	 */
	public static function fetchFollowupActionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFollowupActionType', $objDatabase );
	}

	public static function fetchFollowupActionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'followup_action_types', $objDatabase );
	}

	public static function fetchFollowupActionTypeById( $intId, $objDatabase ) {
		return self::fetchFollowupActionType( sprintf( 'SELECT * FROM followup_action_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>