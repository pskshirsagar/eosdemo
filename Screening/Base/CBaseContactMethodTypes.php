<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CContactMethodTypes
 * Do not add any new functions to this class.
 */

class CBaseContactMethodTypes extends CEosPluralBase {

	/**
	 * @return CContactMethodType[]
	 */
	public static function fetchContactMethodTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CContactMethodType', $objDatabase );
	}

	/**
	 * @return CContactMethodType
	 */
	public static function fetchContactMethodType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CContactMethodType', $objDatabase );
	}

	public static function fetchContactMethodTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'contact_method_types', $objDatabase );
	}

	public static function fetchContactMethodTypeById( $intId, $objDatabase ) {
		return self::fetchContactMethodType( sprintf( 'SELECT * FROM contact_method_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>