<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningApplicantChildTransactions
 * Do not add any new functions to this class.
 */

class CBaseScreeningApplicantChildTransactions extends CEosPluralBase {

	/**
	 * @return CScreeningApplicantChildTransaction[]
	 */
	public static function fetchScreeningApplicantChildTransactions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningApplicantChildTransaction', $objDatabase );
	}

	/**
	 * @return CScreeningApplicantChildTransaction
	 */
	public static function fetchScreeningApplicantChildTransaction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningApplicantChildTransaction', $objDatabase );
	}

	public static function fetchScreeningApplicantChildTransactionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_applicant_child_transactions', $objDatabase );
	}

	public static function fetchScreeningApplicantChildTransactionById( $intId, $objDatabase ) {
		return self::fetchScreeningApplicantChildTransaction( sprintf( 'SELECT * FROM screening_applicant_child_transactions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningApplicantChildTransactionsByScreeningChildTransactionRequestTypeId( $intScreeningChildTransactionRequestTypeId, $objDatabase ) {
		return self::fetchScreeningApplicantChildTransactions( sprintf( 'SELECT * FROM screening_applicant_child_transactions WHERE screening_child_transaction_request_type_id = %d', ( int ) $intScreeningChildTransactionRequestTypeId ), $objDatabase );
	}

	public static function fetchScreeningApplicantChildTransactionsByScreeningId( $intScreeningId, $objDatabase ) {
		return self::fetchScreeningApplicantChildTransactions( sprintf( 'SELECT * FROM screening_applicant_child_transactions WHERE screening_id = %d', ( int ) $intScreeningId ), $objDatabase );
	}

	public static function fetchScreeningApplicantChildTransactionsByScreeningApplicantId( $intScreeningApplicantId, $objDatabase ) {
		return self::fetchScreeningApplicantChildTransactions( sprintf( 'SELECT * FROM screening_applicant_child_transactions WHERE screening_applicant_id = %d', ( int ) $intScreeningApplicantId ), $objDatabase );
	}

	public static function fetchScreeningApplicantChildTransactionsByScreeningTransactionId( $intScreeningTransactionId, $objDatabase ) {
		return self::fetchScreeningApplicantChildTransactions( sprintf( 'SELECT * FROM screening_applicant_child_transactions WHERE screening_transaction_id = %d', ( int ) $intScreeningTransactionId ), $objDatabase );
	}

	public static function fetchScreeningApplicantChildTransactionsByScreeningDataProviderId( $intScreeningDataProviderId, $objDatabase ) {
		return self::fetchScreeningApplicantChildTransactions( sprintf( 'SELECT * FROM screening_applicant_child_transactions WHERE screening_data_provider_id = %d', ( int ) $intScreeningDataProviderId ), $objDatabase );
	}

	public static function fetchScreeningApplicantChildTransactionsByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreeningApplicantChildTransactions( sprintf( 'SELECT * FROM screening_applicant_child_transactions WHERE screen_type_id = %d', ( int ) $intScreenTypeId ), $objDatabase );
	}

	public static function fetchScreeningApplicantChildTransactionsByChildTransactionId( $intChildTransactionId, $objDatabase ) {
		return self::fetchScreeningApplicantChildTransactions( sprintf( 'SELECT * FROM screening_applicant_child_transactions WHERE child_transaction_id = %d', ( int ) $intChildTransactionId ), $objDatabase );
	}

}
?>