<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\COffenseLevelTypes
 * Do not add any new functions to this class.
 */

class CBaseOffenseLevelTypes extends CEosPluralBase {

	/**
	 * @return COffenseLevelType[]
	 */
	public static function fetchOffenseLevelTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'COffenseLevelType', $objDatabase );
	}

	/**
	 * @return COffenseLevelType
	 */
	public static function fetchOffenseLevelType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'COffenseLevelType', $objDatabase );
	}

	public static function fetchOffenseLevelTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'offense_level_types', $objDatabase );
	}

	public static function fetchOffenseLevelTypeById( $intId, $objDatabase ) {
		return self::fetchOffenseLevelType( sprintf( 'SELECT * FROM offense_level_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>