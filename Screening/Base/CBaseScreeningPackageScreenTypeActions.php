<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageScreenTypeActions
 * Do not add any new functions to this class.
 */

class CBaseScreeningPackageScreenTypeActions extends CEosPluralBase {

	/**
	 * @return CScreeningPackageScreenTypeAction[]
	 */
	public static function fetchScreeningPackageScreenTypeActions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageScreenTypeAction', $objDatabase );
	}

	/**
	 * @return CScreeningPackageScreenTypeAction
	 */
	public static function fetchScreeningPackageScreenTypeAction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageScreenTypeAction', $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeActionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_screen_type_actions', $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeActionById( $intId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeAction( sprintf( 'SELECT * FROM screening_package_screen_type_actions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeActionsByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeActions( sprintf( 'SELECT * FROM screening_package_screen_type_actions WHERE screening_package_id = %d', ( int ) $intScreeningPackageId ), $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeActionsByScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeActions( sprintf( 'SELECT * FROM screening_package_screen_type_actions WHERE screening_config_preference_type_id = %d', ( int ) $intScreeningConfigPreferenceTypeId ), $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeActionsByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeActions( sprintf( 'SELECT * FROM screening_package_screen_type_actions WHERE screen_type_id = %d', ( int ) $intScreenTypeId ), $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeActionsByScreeningSearchActionId( $intScreeningSearchActionId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeActions( sprintf( 'SELECT * FROM screening_package_screen_type_actions WHERE screening_search_action_id = %d', ( int ) $intScreeningSearchActionId ), $objDatabase );
	}

}
?>