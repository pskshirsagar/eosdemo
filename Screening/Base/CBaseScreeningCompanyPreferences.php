<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCompanyPreferences
 * Do not add any new functions to this class.
 */

class CBaseScreeningCompanyPreferences extends CEosPluralBase {

	/**
	 * @return CScreeningCompanyPreference[]
	 */
	public static function fetchScreeningCompanyPreferences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningCompanyPreference::class, $objDatabase );
	}

	/**
	 * @return CScreeningCompanyPreference
	 */
	public static function fetchScreeningCompanyPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningCompanyPreference::class, $objDatabase );
	}

	public static function fetchScreeningCompanyPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_company_preferences', $objDatabase );
	}

	public static function fetchScreeningCompanyPreferenceById( $intId, $objDatabase ) {
		return self::fetchScreeningCompanyPreference( sprintf( 'SELECT * FROM screening_company_preferences WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningCompanyPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningCompanyPreferences( sprintf( 'SELECT * FROM screening_company_preferences WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningCompanyPreferencesByScreeningCompanyPreferenceTypeId( $intScreeningCompanyPreferenceTypeId, $objDatabase ) {
		return self::fetchScreeningCompanyPreferences( sprintf( 'SELECT * FROM screening_company_preferences WHERE screening_company_preference_type_id = %d', ( int ) $intScreeningCompanyPreferenceTypeId ), $objDatabase );
	}

}
?>