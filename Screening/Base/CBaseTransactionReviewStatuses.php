<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CTransactionReviewStatuses
 * Do not add any new functions to this class.
 */

class CBaseTransactionReviewStatuses extends CEosPluralBase {

	/**
	 * @return CTransactionReviewStatus[]
	 */
	public static function fetchTransactionReviewStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTransactionReviewStatus', $objDatabase );
	}

	/**
	 * @return CTransactionReviewStatus
	 */
	public static function fetchTransactionReviewStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTransactionReviewStatus', $objDatabase );
	}

	public static function fetchTransactionReviewStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transaction_review_statuses', $objDatabase );
	}

	public static function fetchTransactionReviewStatusById( $intId, $objDatabase ) {
		return self::fetchTransactionReviewStatus( sprintf( 'SELECT * FROM transaction_review_statuses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>