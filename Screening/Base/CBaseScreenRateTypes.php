<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreenRateTypes
 * Do not add any new functions to this class.
 */

class CBaseScreenRateTypes extends CEosPluralBase {

	/**
	 * @return CScreenRateType[]
	 */
	public static function fetchScreenRateTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreenRateType', $objDatabase );
	}

	/**
	 * @return CScreenRateType
	 */
	public static function fetchScreenRateType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreenRateType', $objDatabase );
	}

	public static function fetchScreenRateTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screen_rate_types', $objDatabase );
	}

	public static function fetchScreenRateTypeById( $intId, $objDatabase ) {
		return self::fetchScreenRateType( sprintf( 'SELECT * FROM screen_rate_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>