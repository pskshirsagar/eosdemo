<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CRvScreeningLeaseDetails
 * Do not add any new functions to this class.
 */

class CBaseRvScreeningLeaseDetails extends CEosPluralBase {

	/**
	 * @return CRvScreeningLeaseDetail[]
	 */
	public static function fetchRvScreeningLeaseDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRvScreeningLeaseDetail::class, $objDatabase );
	}

	/**
	 * @return CRvScreeningLeaseDetail
	 */
	public static function fetchRvScreeningLeaseDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRvScreeningLeaseDetail::class, $objDatabase );
	}

	public static function fetchRvScreeningLeaseDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'rv_screening_lease_details', $objDatabase );
	}

	public static function fetchRvScreeningLeaseDetailById( $intId, $objDatabase ) {
		return self::fetchRvScreeningLeaseDetail( sprintf( 'SELECT * FROM rv_screening_lease_details WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchRvScreeningLeaseDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchRvScreeningLeaseDetails( sprintf( 'SELECT * FROM rv_screening_lease_details WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchRvScreeningLeaseDetailsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchRvScreeningLeaseDetails( sprintf( 'SELECT * FROM rv_screening_lease_details WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchRvScreeningLeaseDetailsByPropertyTypeId( $intPropertyTypeId, $objDatabase ) {
		return self::fetchRvScreeningLeaseDetails( sprintf( 'SELECT * FROM rv_screening_lease_details WHERE property_type_id = %d', $intPropertyTypeId ), $objDatabase );
	}

	public static function fetchRvScreeningLeaseDetailsByLeaseId( $intLeaseId, $objDatabase ) {
		return self::fetchRvScreeningLeaseDetails( sprintf( 'SELECT * FROM rv_screening_lease_details WHERE lease_id = %d', $intLeaseId ), $objDatabase );
	}

	public static function fetchRvScreeningLeaseDetailsByScreeningId( $intScreeningId, $objDatabase ) {
		return self::fetchRvScreeningLeaseDetails( sprintf( 'SELECT * FROM rv_screening_lease_details WHERE screening_id = %d', $intScreeningId ), $objDatabase );
	}

	public static function fetchRvScreeningLeaseDetailsByApplicationId( $intApplicationId, $objDatabase ) {
		return self::fetchRvScreeningLeaseDetails( sprintf( 'SELECT * FROM rv_screening_lease_details WHERE application_id = %d', $intApplicationId ), $objDatabase );
	}

	public static function fetchRvScreeningLeaseDetailsByScreeningRecommendationTypeId( $intScreeningRecommendationTypeId, $objDatabase ) {
		return self::fetchRvScreeningLeaseDetails( sprintf( 'SELECT * FROM rv_screening_lease_details WHERE screening_recommendation_type_id = %d', $intScreeningRecommendationTypeId ), $objDatabase );
	}

	public static function fetchRvScreeningLeaseDetailsByScreeningDecisionTypeId( $intScreeningDecisionTypeId, $objDatabase ) {
		return self::fetchRvScreeningLeaseDetails( sprintf( 'SELECT * FROM rv_screening_lease_details WHERE screening_decision_type_id = %d', $intScreeningDecisionTypeId ), $objDatabase );
	}

	public static function fetchRvScreeningLeaseDetailsByPrimaryCustomerId( $intPrimaryCustomerId, $objDatabase ) {
		return self::fetchRvScreeningLeaseDetails( sprintf( 'SELECT * FROM rv_screening_lease_details WHERE primary_customer_id = %d', $intPrimaryCustomerId ), $objDatabase );
	}

}
?>