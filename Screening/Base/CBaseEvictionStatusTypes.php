<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CEvictionStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseEvictionStatusTypes extends CEosPluralBase {

	/**
	 * @return CEvictionStatusType[]
	 */
	public static function fetchEvictionStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEvictionStatusType', $objDatabase );
	}

	/**
	 * @return CEvictionStatusType
	 */
	public static function fetchEvictionStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEvictionStatusType', $objDatabase );
	}

	public static function fetchEvictionStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'eviction_status_types', $objDatabase );
	}

	public static function fetchEvictionStatusTypeById( $intId, $objDatabase ) {
		return self::fetchEvictionStatusType( sprintf( 'SELECT * FROM eviction_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>