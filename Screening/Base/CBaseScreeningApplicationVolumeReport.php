<?php

class CBaseScreeningApplicationVolumeReport extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_application_volume_reports';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_fltRevenue;
	protected $m_strReportMonth;
	protected $m_intTotalApplications;
	protected $m_intTotalApplicants;
	protected $m_intTotalPass;
	protected $m_intTotalConditional;
	protected $m_intTotalFail;
	protected $m_intTotalInprogress;
	protected $m_intTotalError;
	protected $m_intTotalPassDecision;
	protected $m_intTotalConditionalDecision;
	protected $m_intTotalFailDecision;
	protected $m_intTotalPassOverrideDecision;
	protected $m_intTotalConditionalOverrideDecision;
	protected $m_intTotalFailOverrideDecision;
	protected $m_fltAverageRent;
	protected $m_fltAverageIncome;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltRevenue = '0';
		$this->m_intTotalApplications = '0';
		$this->m_intTotalApplicants = '0';
		$this->m_intTotalPass = '0';
		$this->m_intTotalConditional = '0';
		$this->m_intTotalFail = '0';
		$this->m_intTotalInprogress = '0';
		$this->m_intTotalError = '0';
		$this->m_intTotalPassDecision = '0';
		$this->m_intTotalConditionalDecision = '0';
		$this->m_intTotalFailDecision = '0';
		$this->m_intTotalPassOverrideDecision = '0';
		$this->m_intTotalConditionalOverrideDecision = '0';
		$this->m_intTotalFailOverrideDecision = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['revenue'] ) && $boolDirectSet ) $this->set( 'm_fltRevenue', trim( $arrValues['revenue'] ) ); elseif( isset( $arrValues['revenue'] ) ) $this->setRevenue( $arrValues['revenue'] );
		if( isset( $arrValues['report_month'] ) && $boolDirectSet ) $this->set( 'm_strReportMonth', trim( $arrValues['report_month'] ) ); elseif( isset( $arrValues['report_month'] ) ) $this->setReportMonth( $arrValues['report_month'] );
		if( isset( $arrValues['total_applications'] ) && $boolDirectSet ) $this->set( 'm_intTotalApplications', trim( $arrValues['total_applications'] ) ); elseif( isset( $arrValues['total_applications'] ) ) $this->setTotalApplications( $arrValues['total_applications'] );
		if( isset( $arrValues['total_applicants'] ) && $boolDirectSet ) $this->set( 'm_intTotalApplicants', trim( $arrValues['total_applicants'] ) ); elseif( isset( $arrValues['total_applicants'] ) ) $this->setTotalApplicants( $arrValues['total_applicants'] );
		if( isset( $arrValues['total_pass'] ) && $boolDirectSet ) $this->set( 'm_intTotalPass', trim( $arrValues['total_pass'] ) ); elseif( isset( $arrValues['total_pass'] ) ) $this->setTotalPass( $arrValues['total_pass'] );
		if( isset( $arrValues['total_conditional'] ) && $boolDirectSet ) $this->set( 'm_intTotalConditional', trim( $arrValues['total_conditional'] ) ); elseif( isset( $arrValues['total_conditional'] ) ) $this->setTotalConditional( $arrValues['total_conditional'] );
		if( isset( $arrValues['total_fail'] ) && $boolDirectSet ) $this->set( 'm_intTotalFail', trim( $arrValues['total_fail'] ) ); elseif( isset( $arrValues['total_fail'] ) ) $this->setTotalFail( $arrValues['total_fail'] );
		if( isset( $arrValues['total_inprogress'] ) && $boolDirectSet ) $this->set( 'm_intTotalInprogress', trim( $arrValues['total_inprogress'] ) ); elseif( isset( $arrValues['total_inprogress'] ) ) $this->setTotalInprogress( $arrValues['total_inprogress'] );
		if( isset( $arrValues['total_error'] ) && $boolDirectSet ) $this->set( 'm_intTotalError', trim( $arrValues['total_error'] ) ); elseif( isset( $arrValues['total_error'] ) ) $this->setTotalError( $arrValues['total_error'] );
		if( isset( $arrValues['total_pass_decision'] ) && $boolDirectSet ) $this->set( 'm_intTotalPassDecision', trim( $arrValues['total_pass_decision'] ) ); elseif( isset( $arrValues['total_pass_decision'] ) ) $this->setTotalPassDecision( $arrValues['total_pass_decision'] );
		if( isset( $arrValues['total_conditional_decision'] ) && $boolDirectSet ) $this->set( 'm_intTotalConditionalDecision', trim( $arrValues['total_conditional_decision'] ) ); elseif( isset( $arrValues['total_conditional_decision'] ) ) $this->setTotalConditionalDecision( $arrValues['total_conditional_decision'] );
		if( isset( $arrValues['total_fail_decision'] ) && $boolDirectSet ) $this->set( 'm_intTotalFailDecision', trim( $arrValues['total_fail_decision'] ) ); elseif( isset( $arrValues['total_fail_decision'] ) ) $this->setTotalFailDecision( $arrValues['total_fail_decision'] );
		if( isset( $arrValues['total_pass_override_decision'] ) && $boolDirectSet ) $this->set( 'm_intTotalPassOverrideDecision', trim( $arrValues['total_pass_override_decision'] ) ); elseif( isset( $arrValues['total_pass_override_decision'] ) ) $this->setTotalPassOverrideDecision( $arrValues['total_pass_override_decision'] );
		if( isset( $arrValues['total_conditional_override_decision'] ) && $boolDirectSet ) $this->set( 'm_intTotalConditionalOverrideDecision', trim( $arrValues['total_conditional_override_decision'] ) ); elseif( isset( $arrValues['total_conditional_override_decision'] ) ) $this->setTotalConditionalOverrideDecision( $arrValues['total_conditional_override_decision'] );
		if( isset( $arrValues['total_fail_override_decision'] ) && $boolDirectSet ) $this->set( 'm_intTotalFailOverrideDecision', trim( $arrValues['total_fail_override_decision'] ) ); elseif( isset( $arrValues['total_fail_override_decision'] ) ) $this->setTotalFailOverrideDecision( $arrValues['total_fail_override_decision'] );
		if( isset( $arrValues['average_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAverageRent', trim( $arrValues['average_rent'] ) ); elseif( isset( $arrValues['average_rent'] ) ) $this->setAverageRent( $arrValues['average_rent'] );
		if( isset( $arrValues['average_income'] ) && $boolDirectSet ) $this->set( 'm_fltAverageIncome', trim( $arrValues['average_income'] ) ); elseif( isset( $arrValues['average_income'] ) ) $this->setAverageIncome( $arrValues['average_income'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setRevenue( $fltRevenue ) {
		$this->set( 'm_fltRevenue', CStrings::strToFloatDef( $fltRevenue, NULL, false, 3 ) );
	}

	public function getRevenue() {
		return $this->m_fltRevenue;
	}

	public function sqlRevenue() {
		return ( true == isset( $this->m_fltRevenue ) ) ? ( string ) $this->m_fltRevenue : '0';
	}

	public function setReportMonth( $strReportMonth ) {
		$this->set( 'm_strReportMonth', CStrings::strTrimDef( $strReportMonth, -1, NULL, true ) );
	}

	public function getReportMonth() {
		return $this->m_strReportMonth;
	}

	public function sqlReportMonth() {
		return ( true == isset( $this->m_strReportMonth ) ) ? '\'' . $this->m_strReportMonth . '\'' : 'NOW()';
	}

	public function setTotalApplications( $intTotalApplications ) {
		$this->set( 'm_intTotalApplications', CStrings::strToIntDef( $intTotalApplications, NULL, false ) );
	}

	public function getTotalApplications() {
		return $this->m_intTotalApplications;
	}

	public function sqlTotalApplications() {
		return ( true == isset( $this->m_intTotalApplications ) ) ? ( string ) $this->m_intTotalApplications : '0';
	}

	public function setTotalApplicants( $intTotalApplicants ) {
		$this->set( 'm_intTotalApplicants', CStrings::strToIntDef( $intTotalApplicants, NULL, false ) );
	}

	public function getTotalApplicants() {
		return $this->m_intTotalApplicants;
	}

	public function sqlTotalApplicants() {
		return ( true == isset( $this->m_intTotalApplicants ) ) ? ( string ) $this->m_intTotalApplicants : '0';
	}

	public function setTotalPass( $intTotalPass ) {
		$this->set( 'm_intTotalPass', CStrings::strToIntDef( $intTotalPass, NULL, false ) );
	}

	public function getTotalPass() {
		return $this->m_intTotalPass;
	}

	public function sqlTotalPass() {
		return ( true == isset( $this->m_intTotalPass ) ) ? ( string ) $this->m_intTotalPass : '0';
	}

	public function setTotalConditional( $intTotalConditional ) {
		$this->set( 'm_intTotalConditional', CStrings::strToIntDef( $intTotalConditional, NULL, false ) );
	}

	public function getTotalConditional() {
		return $this->m_intTotalConditional;
	}

	public function sqlTotalConditional() {
		return ( true == isset( $this->m_intTotalConditional ) ) ? ( string ) $this->m_intTotalConditional : '0';
	}

	public function setTotalFail( $intTotalFail ) {
		$this->set( 'm_intTotalFail', CStrings::strToIntDef( $intTotalFail, NULL, false ) );
	}

	public function getTotalFail() {
		return $this->m_intTotalFail;
	}

	public function sqlTotalFail() {
		return ( true == isset( $this->m_intTotalFail ) ) ? ( string ) $this->m_intTotalFail : '0';
	}

	public function setTotalInprogress( $intTotalInprogress ) {
		$this->set( 'm_intTotalInprogress', CStrings::strToIntDef( $intTotalInprogress, NULL, false ) );
	}

	public function getTotalInprogress() {
		return $this->m_intTotalInprogress;
	}

	public function sqlTotalInprogress() {
		return ( true == isset( $this->m_intTotalInprogress ) ) ? ( string ) $this->m_intTotalInprogress : '0';
	}

	public function setTotalError( $intTotalError ) {
		$this->set( 'm_intTotalError', CStrings::strToIntDef( $intTotalError, NULL, false ) );
	}

	public function getTotalError() {
		return $this->m_intTotalError;
	}

	public function sqlTotalError() {
		return ( true == isset( $this->m_intTotalError ) ) ? ( string ) $this->m_intTotalError : '0';
	}

	public function setTotalPassDecision( $intTotalPassDecision ) {
		$this->set( 'm_intTotalPassDecision', CStrings::strToIntDef( $intTotalPassDecision, NULL, false ) );
	}

	public function getTotalPassDecision() {
		return $this->m_intTotalPassDecision;
	}

	public function sqlTotalPassDecision() {
		return ( true == isset( $this->m_intTotalPassDecision ) ) ? ( string ) $this->m_intTotalPassDecision : '0';
	}

	public function setTotalConditionalDecision( $intTotalConditionalDecision ) {
		$this->set( 'm_intTotalConditionalDecision', CStrings::strToIntDef( $intTotalConditionalDecision, NULL, false ) );
	}

	public function getTotalConditionalDecision() {
		return $this->m_intTotalConditionalDecision;
	}

	public function sqlTotalConditionalDecision() {
		return ( true == isset( $this->m_intTotalConditionalDecision ) ) ? ( string ) $this->m_intTotalConditionalDecision : '0';
	}

	public function setTotalFailDecision( $intTotalFailDecision ) {
		$this->set( 'm_intTotalFailDecision', CStrings::strToIntDef( $intTotalFailDecision, NULL, false ) );
	}

	public function getTotalFailDecision() {
		return $this->m_intTotalFailDecision;
	}

	public function sqlTotalFailDecision() {
		return ( true == isset( $this->m_intTotalFailDecision ) ) ? ( string ) $this->m_intTotalFailDecision : '0';
	}

	public function setTotalPassOverrideDecision( $intTotalPassOverrideDecision ) {
		$this->set( 'm_intTotalPassOverrideDecision', CStrings::strToIntDef( $intTotalPassOverrideDecision, NULL, false ) );
	}

	public function getTotalPassOverrideDecision() {
		return $this->m_intTotalPassOverrideDecision;
	}

	public function sqlTotalPassOverrideDecision() {
		return ( true == isset( $this->m_intTotalPassOverrideDecision ) ) ? ( string ) $this->m_intTotalPassOverrideDecision : '0';
	}

	public function setTotalConditionalOverrideDecision( $intTotalConditionalOverrideDecision ) {
		$this->set( 'm_intTotalConditionalOverrideDecision', CStrings::strToIntDef( $intTotalConditionalOverrideDecision, NULL, false ) );
	}

	public function getTotalConditionalOverrideDecision() {
		return $this->m_intTotalConditionalOverrideDecision;
	}

	public function sqlTotalConditionalOverrideDecision() {
		return ( true == isset( $this->m_intTotalConditionalOverrideDecision ) ) ? ( string ) $this->m_intTotalConditionalOverrideDecision : '0';
	}

	public function setTotalFailOverrideDecision( $intTotalFailOverrideDecision ) {
		$this->set( 'm_intTotalFailOverrideDecision', CStrings::strToIntDef( $intTotalFailOverrideDecision, NULL, false ) );
	}

	public function getTotalFailOverrideDecision() {
		return $this->m_intTotalFailOverrideDecision;
	}

	public function sqlTotalFailOverrideDecision() {
		return ( true == isset( $this->m_intTotalFailOverrideDecision ) ) ? ( string ) $this->m_intTotalFailOverrideDecision : '0';
	}

	public function setAverageRent( $fltAverageRent ) {
		$this->set( 'm_fltAverageRent', CStrings::strToFloatDef( $fltAverageRent, NULL, false, 3 ) );
	}

	public function getAverageRent() {
		return $this->m_fltAverageRent;
	}

	public function sqlAverageRent() {
		return ( true == isset( $this->m_fltAverageRent ) ) ? ( string ) $this->m_fltAverageRent : 'NULL';
	}

	public function setAverageIncome( $fltAverageIncome ) {
		$this->set( 'm_fltAverageIncome', CStrings::strToFloatDef( $fltAverageIncome, NULL, false, 3 ) );
	}

	public function getAverageIncome() {
		return $this->m_fltAverageIncome;
	}

	public function sqlAverageIncome() {
		return ( true == isset( $this->m_fltAverageIncome ) ) ? ( string ) $this->m_fltAverageIncome : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, revenue, report_month, total_applications, total_applicants, total_pass, total_conditional, total_fail, total_inprogress, total_error, total_pass_decision, total_conditional_decision, total_fail_decision, total_pass_override_decision, total_conditional_override_decision, total_fail_override_decision, average_rent, average_income, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlRevenue() . ', ' .
 						$this->sqlReportMonth() . ', ' .
 						$this->sqlTotalApplications() . ', ' .
 						$this->sqlTotalApplicants() . ', ' .
 						$this->sqlTotalPass() . ', ' .
 						$this->sqlTotalConditional() . ', ' .
 						$this->sqlTotalFail() . ', ' .
 						$this->sqlTotalInprogress() . ', ' .
 						$this->sqlTotalError() . ', ' .
 						$this->sqlTotalPassDecision() . ', ' .
 						$this->sqlTotalConditionalDecision() . ', ' .
 						$this->sqlTotalFailDecision() . ', ' .
 						$this->sqlTotalPassOverrideDecision() . ', ' .
 						$this->sqlTotalConditionalOverrideDecision() . ', ' .
 						$this->sqlTotalFailOverrideDecision() . ', ' .
 						$this->sqlAverageRent() . ', ' .
 						$this->sqlAverageIncome() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' revenue = ' . $this->sqlRevenue() . ','; } elseif( true == array_key_exists( 'Revenue', $this->getChangedColumns() ) ) { $strSql .= ' revenue = ' . $this->sqlRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_month = ' . $this->sqlReportMonth() . ','; } elseif( true == array_key_exists( 'ReportMonth', $this->getChangedColumns() ) ) { $strSql .= ' report_month = ' . $this->sqlReportMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_applications = ' . $this->sqlTotalApplications() . ','; } elseif( true == array_key_exists( 'TotalApplications', $this->getChangedColumns() ) ) { $strSql .= ' total_applications = ' . $this->sqlTotalApplications() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_applicants = ' . $this->sqlTotalApplicants() . ','; } elseif( true == array_key_exists( 'TotalApplicants', $this->getChangedColumns() ) ) { $strSql .= ' total_applicants = ' . $this->sqlTotalApplicants() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_pass = ' . $this->sqlTotalPass() . ','; } elseif( true == array_key_exists( 'TotalPass', $this->getChangedColumns() ) ) { $strSql .= ' total_pass = ' . $this->sqlTotalPass() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_conditional = ' . $this->sqlTotalConditional() . ','; } elseif( true == array_key_exists( 'TotalConditional', $this->getChangedColumns() ) ) { $strSql .= ' total_conditional = ' . $this->sqlTotalConditional() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_fail = ' . $this->sqlTotalFail() . ','; } elseif( true == array_key_exists( 'TotalFail', $this->getChangedColumns() ) ) { $strSql .= ' total_fail = ' . $this->sqlTotalFail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_inprogress = ' . $this->sqlTotalInprogress() . ','; } elseif( true == array_key_exists( 'TotalInprogress', $this->getChangedColumns() ) ) { $strSql .= ' total_inprogress = ' . $this->sqlTotalInprogress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_error = ' . $this->sqlTotalError() . ','; } elseif( true == array_key_exists( 'TotalError', $this->getChangedColumns() ) ) { $strSql .= ' total_error = ' . $this->sqlTotalError() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_pass_decision = ' . $this->sqlTotalPassDecision() . ','; } elseif( true == array_key_exists( 'TotalPassDecision', $this->getChangedColumns() ) ) { $strSql .= ' total_pass_decision = ' . $this->sqlTotalPassDecision() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_conditional_decision = ' . $this->sqlTotalConditionalDecision() . ','; } elseif( true == array_key_exists( 'TotalConditionalDecision', $this->getChangedColumns() ) ) { $strSql .= ' total_conditional_decision = ' . $this->sqlTotalConditionalDecision() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_fail_decision = ' . $this->sqlTotalFailDecision() . ','; } elseif( true == array_key_exists( 'TotalFailDecision', $this->getChangedColumns() ) ) { $strSql .= ' total_fail_decision = ' . $this->sqlTotalFailDecision() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_pass_override_decision = ' . $this->sqlTotalPassOverrideDecision() . ','; } elseif( true == array_key_exists( 'TotalPassOverrideDecision', $this->getChangedColumns() ) ) { $strSql .= ' total_pass_override_decision = ' . $this->sqlTotalPassOverrideDecision() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_conditional_override_decision = ' . $this->sqlTotalConditionalOverrideDecision() . ','; } elseif( true == array_key_exists( 'TotalConditionalOverrideDecision', $this->getChangedColumns() ) ) { $strSql .= ' total_conditional_override_decision = ' . $this->sqlTotalConditionalOverrideDecision() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_fail_override_decision = ' . $this->sqlTotalFailOverrideDecision() . ','; } elseif( true == array_key_exists( 'TotalFailOverrideDecision', $this->getChangedColumns() ) ) { $strSql .= ' total_fail_override_decision = ' . $this->sqlTotalFailOverrideDecision() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_rent = ' . $this->sqlAverageRent() . ','; } elseif( true == array_key_exists( 'AverageRent', $this->getChangedColumns() ) ) { $strSql .= ' average_rent = ' . $this->sqlAverageRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_income = ' . $this->sqlAverageIncome() . ','; } elseif( true == array_key_exists( 'AverageIncome', $this->getChangedColumns() ) ) { $strSql .= ' average_income = ' . $this->sqlAverageIncome() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'revenue' => $this->getRevenue(),
			'report_month' => $this->getReportMonth(),
			'total_applications' => $this->getTotalApplications(),
			'total_applicants' => $this->getTotalApplicants(),
			'total_pass' => $this->getTotalPass(),
			'total_conditional' => $this->getTotalConditional(),
			'total_fail' => $this->getTotalFail(),
			'total_inprogress' => $this->getTotalInprogress(),
			'total_error' => $this->getTotalError(),
			'total_pass_decision' => $this->getTotalPassDecision(),
			'total_conditional_decision' => $this->getTotalConditionalDecision(),
			'total_fail_decision' => $this->getTotalFailDecision(),
			'total_pass_override_decision' => $this->getTotalPassOverrideDecision(),
			'total_conditional_override_decision' => $this->getTotalConditionalOverrideDecision(),
			'total_fail_override_decision' => $this->getTotalFailOverrideDecision(),
			'average_rent' => $this->getAverageRent(),
			'average_income' => $this->getAverageIncome(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>