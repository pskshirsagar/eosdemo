<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningMetaDataTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningMetaDataTypes extends CEosPluralBase {

	/**
	 * @return CScreeningMetaDataType[]
	 */
	public static function fetchScreeningMetaDataTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningMetaDataType', $objDatabase );
	}

	/**
	 * @return CScreeningMetaDataType
	 */
	public static function fetchScreeningMetaDataType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningMetaDataType', $objDatabase );
	}

	public static function fetchScreeningMetaDataTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_meta_data_types', $objDatabase );
	}

	public static function fetchScreeningMetaDataTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningMetaDataType( sprintf( 'SELECT * FROM screening_meta_data_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>