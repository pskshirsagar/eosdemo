<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningListTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningListTypes extends CEosPluralBase {

	/**
	 * @return CScreeningListType[]
	 */
	public static function fetchScreeningListTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningListType', $objDatabase );
	}

	/**
	 * @return CScreeningListType
	 */
	public static function fetchScreeningListType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningListType', $objDatabase );
	}

	public static function fetchScreeningListTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_list_types', $objDatabase );
	}

	public static function fetchScreeningListTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningListType( sprintf( 'SELECT * FROM screening_list_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>