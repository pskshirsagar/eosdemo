<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningChildTransactionRequestTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningChildTransactionRequestTypes extends CEosPluralBase {

	/**
	 * @return CScreeningChildTransactionRequestType[]
	 */
	public static function fetchScreeningChildTransactionRequestTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningChildTransactionRequestType', $objDatabase );
	}

	/**
	 * @return CScreeningChildTransactionRequestType
	 */
	public static function fetchScreeningChildTransactionRequestType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningChildTransactionRequestType', $objDatabase );
	}

	public static function fetchScreeningChildTransactionRequestTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_child_transaction_request_types', $objDatabase );
	}

	public static function fetchScreeningChildTransactionRequestTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningChildTransactionRequestType( sprintf( 'SELECT * FROM screening_child_transaction_request_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>