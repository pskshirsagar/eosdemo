<?php

class CBaseScreeningPackageSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_package_settings';

	protected $m_intId;
	protected $m_intScreeningPackageId;
	protected $m_intScreeningPackageConditionSetId;
	protected $m_intScreeningConfigPreferenceTypeId;
	protected $m_fltRequiredMaxValue;
	protected $m_fltRequiredMinValue;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_package_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageId', trim( $arrValues['screening_package_id'] ) ); elseif( isset( $arrValues['screening_package_id'] ) ) $this->setScreeningPackageId( $arrValues['screening_package_id'] );
		if( isset( $arrValues['screening_package_condition_set_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageConditionSetId', trim( $arrValues['screening_package_condition_set_id'] ) ); elseif( isset( $arrValues['screening_package_condition_set_id'] ) ) $this->setScreeningPackageConditionSetId( $arrValues['screening_package_condition_set_id'] );
		if( isset( $arrValues['screening_config_preference_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningConfigPreferenceTypeId', trim( $arrValues['screening_config_preference_type_id'] ) ); elseif( isset( $arrValues['screening_config_preference_type_id'] ) ) $this->setScreeningConfigPreferenceTypeId( $arrValues['screening_config_preference_type_id'] );
		if( isset( $arrValues['required_max_value'] ) && $boolDirectSet ) $this->set( 'm_fltRequiredMaxValue', trim( $arrValues['required_max_value'] ) ); elseif( isset( $arrValues['required_max_value'] ) ) $this->setRequiredMaxValue( $arrValues['required_max_value'] );
		if( isset( $arrValues['required_min_value'] ) && $boolDirectSet ) $this->set( 'm_fltRequiredMinValue', trim( $arrValues['required_min_value'] ) ); elseif( isset( $arrValues['required_min_value'] ) ) $this->setRequiredMinValue( $arrValues['required_min_value'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningPackageId( $intScreeningPackageId ) {
		$this->set( 'm_intScreeningPackageId', CStrings::strToIntDef( $intScreeningPackageId, NULL, false ) );
	}

	public function getScreeningPackageId() {
		return $this->m_intScreeningPackageId;
	}

	public function sqlScreeningPackageId() {
		return ( true == isset( $this->m_intScreeningPackageId ) ) ? ( string ) $this->m_intScreeningPackageId : 'NULL';
	}

	public function setScreeningPackageConditionSetId( $intScreeningPackageConditionSetId ) {
		$this->set( 'm_intScreeningPackageConditionSetId', CStrings::strToIntDef( $intScreeningPackageConditionSetId, NULL, false ) );
	}

	public function getScreeningPackageConditionSetId() {
		return $this->m_intScreeningPackageConditionSetId;
	}

	public function sqlScreeningPackageConditionSetId() {
		return ( true == isset( $this->m_intScreeningPackageConditionSetId ) ) ? ( string ) $this->m_intScreeningPackageConditionSetId : 'NULL';
	}

	public function setScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId ) {
		$this->set( 'm_intScreeningConfigPreferenceTypeId', CStrings::strToIntDef( $intScreeningConfigPreferenceTypeId, NULL, false ) );
	}

	public function getScreeningConfigPreferenceTypeId() {
		return $this->m_intScreeningConfigPreferenceTypeId;
	}

	public function sqlScreeningConfigPreferenceTypeId() {
		return ( true == isset( $this->m_intScreeningConfigPreferenceTypeId ) ) ? ( string ) $this->m_intScreeningConfigPreferenceTypeId : 'NULL';
	}

	public function setRequiredMaxValue( $fltRequiredMaxValue ) {
		$this->set( 'm_fltRequiredMaxValue', CStrings::strToFloatDef( $fltRequiredMaxValue, NULL, false, 2 ) );
	}

	public function getRequiredMaxValue() {
		return $this->m_fltRequiredMaxValue;
	}

	public function sqlRequiredMaxValue() {
		return ( true == isset( $this->m_fltRequiredMaxValue ) ) ? ( string ) $this->m_fltRequiredMaxValue : 'NULL';
	}

	public function setRequiredMinValue( $fltRequiredMinValue ) {
		$this->set( 'm_fltRequiredMinValue', CStrings::strToFloatDef( $fltRequiredMinValue, NULL, false, 2 ) );
	}

	public function getRequiredMinValue() {
		return $this->m_fltRequiredMinValue;
	}

	public function sqlRequiredMinValue() {
		return ( true == isset( $this->m_fltRequiredMinValue ) ) ? ( string ) $this->m_fltRequiredMinValue : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, screening_package_id, screening_package_condition_set_id, screening_config_preference_type_id, required_max_value, required_min_value, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlScreeningPackageId() . ', ' .
 						$this->sqlScreeningPackageConditionSetId() . ', ' .
 						$this->sqlScreeningConfigPreferenceTypeId() . ', ' .
 						$this->sqlRequiredMaxValue() . ', ' .
 						$this->sqlRequiredMinValue() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_id = ' . $this->sqlScreeningPackageId() . ','; } elseif( true == array_key_exists( 'ScreeningPackageId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_id = ' . $this->sqlScreeningPackageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_condition_set_id = ' . $this->sqlScreeningPackageConditionSetId() . ','; } elseif( true == array_key_exists( 'ScreeningPackageConditionSetId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_condition_set_id = ' . $this->sqlScreeningPackageConditionSetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_config_preference_type_id = ' . $this->sqlScreeningConfigPreferenceTypeId() . ','; } elseif( true == array_key_exists( 'ScreeningConfigPreferenceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_config_preference_type_id = ' . $this->sqlScreeningConfigPreferenceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_max_value = ' . $this->sqlRequiredMaxValue() . ','; } elseif( true == array_key_exists( 'RequiredMaxValue', $this->getChangedColumns() ) ) { $strSql .= ' required_max_value = ' . $this->sqlRequiredMaxValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_min_value = ' . $this->sqlRequiredMinValue() . ','; } elseif( true == array_key_exists( 'RequiredMinValue', $this->getChangedColumns() ) ) { $strSql .= ' required_min_value = ' . $this->sqlRequiredMinValue() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_package_id' => $this->getScreeningPackageId(),
			'screening_package_condition_set_id' => $this->getScreeningPackageConditionSetId(),
			'screening_config_preference_type_id' => $this->getScreeningConfigPreferenceTypeId(),
			'required_max_value' => $this->getRequiredMaxValue(),
			'required_min_value' => $this->getRequiredMinValue(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>