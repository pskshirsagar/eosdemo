<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningRecordFilterTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningRecordFilterTypes extends CEosPluralBase {

	/**
	 * @return CScreeningRecordFilterType[]
	 */
	public static function fetchScreeningRecordFilterTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningRecordFilterType', $objDatabase );
	}

	/**
	 * @return CScreeningRecordFilterType
	 */
	public static function fetchScreeningRecordFilterType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningRecordFilterType', $objDatabase );
	}

	public static function fetchScreeningRecordFilterTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_record_filter_types', $objDatabase );
	}

	public static function fetchScreeningRecordFilterTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningRecordFilterType( sprintf( 'SELECT * FROM screening_record_filter_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>