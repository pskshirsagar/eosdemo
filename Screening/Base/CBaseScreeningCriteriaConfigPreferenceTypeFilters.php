<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCriteriaConfigPreferenceTypeFilters
 * Do not add any new functions to this class.
 */

class CBaseScreeningCriteriaConfigPreferenceTypeFilters extends CEosPluralBase {

	/**
	 * @return CScreeningCriteriaConfigPreferenceTypeFilter[]
	 */
	public static function fetchScreeningCriteriaConfigPreferenceTypeFilters( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningCriteriaConfigPreferenceTypeFilter::class, $objDatabase );
	}

	/**
	 * @return CScreeningCriteriaConfigPreferenceTypeFilter
	 */
	public static function fetchScreeningCriteriaConfigPreferenceTypeFilter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningCriteriaConfigPreferenceTypeFilter::class, $objDatabase );
	}

	public static function fetchScreeningCriteriaConfigPreferenceTypeFilterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_criteria_config_preference_type_filters', $objDatabase );
	}

	public static function fetchScreeningCriteriaConfigPreferenceTypeFilterById( $intId, $objDatabase ) {
		return self::fetchScreeningCriteriaConfigPreferenceTypeFilter( sprintf( 'SELECT * FROM screening_criteria_config_preference_type_filters WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningCriteriaConfigPreferenceTypeFiltersByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {
		return self::fetchScreeningCriteriaConfigPreferenceTypeFilters( sprintf( 'SELECT * FROM screening_criteria_config_preference_type_filters WHERE screening_criteria_id = %d', ( int ) $intScreeningCriteriaId ), $objDatabase );
	}

	public static function fetchScreeningCriteriaConfigPreferenceTypeFiltersByScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		return self::fetchScreeningCriteriaConfigPreferenceTypeFilters( sprintf( 'SELECT * FROM screening_criteria_config_preference_type_filters WHERE screening_config_preference_type_id = %d', ( int ) $intScreeningConfigPreferenceTypeId ), $objDatabase );
	}

	public static function fetchScreeningCriteriaConfigPreferenceTypeFiltersByScreeningFilterId( $intScreeningFilterId, $objDatabase ) {
		return self::fetchScreeningCriteriaConfigPreferenceTypeFilters( sprintf( 'SELECT * FROM screening_criteria_config_preference_type_filters WHERE screening_filter_id = %d', ( int ) $intScreeningFilterId ), $objDatabase );
	}

}
?>