<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningApplicationResults
 * Do not add any new functions to this class.
 */

class CBaseScreeningApplicationResults extends CEosPluralBase {

	/**
	 * @return CScreeningApplicationResult[]
	 */
	public static function fetchScreeningApplicationResults( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningApplicationResult', $objDatabase );
	}

	/**
	 * @return CScreeningApplicationResult
	 */
	public static function fetchScreeningApplicationResult( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningApplicationResult', $objDatabase );
	}

	public static function fetchScreeningApplicationResultCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_application_results', $objDatabase );
	}

	public static function fetchScreeningApplicationResultById( $intId, $objDatabase ) {
		return self::fetchScreeningApplicationResult( sprintf( 'SELECT * FROM screening_application_results WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningApplicationResultsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationResults( sprintf( 'SELECT * FROM screening_application_results WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationResultsByScreeningId( $intScreeningId, $objDatabase ) {
		return self::fetchScreeningApplicationResults( sprintf( 'SELECT * FROM screening_application_results WHERE screening_id = %d', ( int ) $intScreeningId ), $objDatabase );
	}

	public static function fetchScreeningApplicationResultsByCustomApplicationId( $intCustomApplicationId, $objDatabase ) {
		return self::fetchScreeningApplicationResults( sprintf( 'SELECT * FROM screening_application_results WHERE custom_application_id = %d', ( int ) $intCustomApplicationId ), $objDatabase );
	}

	public static function fetchScreeningApplicationResultsByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreeningApplicationResults( sprintf( 'SELECT * FROM screening_application_results WHERE screen_type_id = %d', ( int ) $intScreenTypeId ), $objDatabase );
	}

	public static function fetchScreeningApplicationResultsByScreeningRecommendationTypeId( $intScreeningRecommendationTypeId, $objDatabase ) {
		return self::fetchScreeningApplicationResults( sprintf( 'SELECT * FROM screening_application_results WHERE screening_recommendation_type_id = %d', ( int ) $intScreeningRecommendationTypeId ), $objDatabase );
	}

}
?>