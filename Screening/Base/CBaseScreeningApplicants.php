<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningApplicants
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningApplicants extends CEosPluralBase {

	/**
	 * @return CScreeningApplicant[]
	 */
	public static function fetchScreeningApplicants( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningApplicant::class, $objDatabase );
	}

	/**
	 * @return CScreeningApplicant
	 */
	public static function fetchScreeningApplicant( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningApplicant::class, $objDatabase );
	}

	public static function fetchScreeningApplicantCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_applicants', $objDatabase );
	}

	public static function fetchScreeningApplicantByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicant( sprintf( 'SELECT * FROM screening_applicants WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningApplicants( sprintf( 'SELECT * FROM screening_applicants WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantsByScreeningIdByCid( $intScreeningId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicants( sprintf( 'SELECT * FROM screening_applicants WHERE screening_id = %d AND cid = %d', $intScreeningId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantsByCustomApplicantIdByCid( $intCustomApplicantId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicants( sprintf( 'SELECT * FROM screening_applicants WHERE custom_applicant_id = %d AND cid = %d', $intCustomApplicantId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantsByScreeningApplicantTypeIdByCid( $intScreeningApplicantTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicants( sprintf( 'SELECT * FROM screening_applicants WHERE screening_applicant_type_id = %d AND cid = %d', $intScreeningApplicantTypeId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantsByApplicantApplicationTransmissionIdByCid( $intApplicantApplicationTransmissionId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicants( sprintf( 'SELECT * FROM screening_applicants WHERE applicant_application_transmission_id = %d AND cid = %d', $intApplicantApplicationTransmissionId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantsByScreeningPackageIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicants( sprintf( 'SELECT * FROM screening_applicants WHERE screening_package_id = %d AND cid = %d', $intScreeningPackageId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantsByLeadSourceIdByCid( $intLeadSourceId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicants( sprintf( 'SELECT * FROM screening_applicants WHERE lead_source_id = %d AND cid = %d', $intLeadSourceId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantsByGuarantorForApplicationIdByCid( $intGuarantorForApplicationId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicants( sprintf( 'SELECT * FROM screening_applicants WHERE guarantor_for_application_id = %d AND cid = %d', $intGuarantorForApplicationId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantsByStatusTypeIdByCid( $intStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicants( sprintf( 'SELECT * FROM screening_applicants WHERE status_type_id = %d AND cid = %d', $intStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantsByOriginalScreeningRecommendationTypeIdByCid( $intOriginalScreeningRecommendationTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicants( sprintf( 'SELECT * FROM screening_applicants WHERE original_screening_recommendation_type_id = %d AND cid = %d', $intOriginalScreeningRecommendationTypeId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantsByScreeningRecommendationTypeIdByCid( $intScreeningRecommendationTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicants( sprintf( 'SELECT * FROM screening_applicants WHERE screening_recommendation_type_id = %d AND cid = %d', $intScreeningRecommendationTypeId, $intCid ), $objDatabase );
	}

}
?>