<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDisputeFollowupRules
 * Do not add any new functions to this class.
 */

class CBaseDisputeFollowupRules extends CEosPluralBase {

	/**
	 * @return CDisputeFollowupRule[]
	 */
	public static function fetchDisputeFollowupRules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDisputeFollowupRule', $objDatabase );
	}

	/**
	 * @return CDisputeFollowupRule
	 */
	public static function fetchDisputeFollowupRule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDisputeFollowupRule', $objDatabase );
	}

	public static function fetchDisputeFollowupRuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'dispute_followup_rules', $objDatabase );
	}

	public static function fetchDisputeFollowupRuleById( $intId, $objDatabase ) {
		return self::fetchDisputeFollowupRule( sprintf( 'SELECT * FROM dispute_followup_rules WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDisputeFollowupRulesByDisputeStatusTypeId( $intDisputeStatusTypeId, $objDatabase ) {
		return self::fetchDisputeFollowupRules( sprintf( 'SELECT * FROM dispute_followup_rules WHERE dispute_status_type_id = %d', ( int ) $intDisputeStatusTypeId ), $objDatabase );
	}

	public static function fetchDisputeFollowupRulesByFollowupActionTypeId( $intFollowupActionTypeId, $objDatabase ) {
		return self::fetchDisputeFollowupRules( sprintf( 'SELECT * FROM dispute_followup_rules WHERE followup_action_type_id = %d', ( int ) $intFollowupActionTypeId ), $objDatabase );
	}

}
?>