<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CLogTypes
 * Do not add any new functions to this class.
 */

class CBaseLogTypes extends CEosPluralBase {

	/**
	 * @return CLogType[]
	 */
	public static function fetchLogTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CLogType', $objDatabase );
	}

	/**
	 * @return CLogType
	 */
	public static function fetchLogType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLogType', $objDatabase );
	}

	public static function fetchLogTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'log_types', $objDatabase );
	}

	public static function fetchLogTypeById( $intId, $objDatabase ) {
		return self::fetchLogType( sprintf( 'SELECT * FROM log_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>