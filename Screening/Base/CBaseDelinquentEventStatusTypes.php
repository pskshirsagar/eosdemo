<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDelinquentEventStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseDelinquentEventStatusTypes extends CEosPluralBase {

	/**
	 * @return CDelinquentEventStatusType[]
	 */
	public static function fetchDelinquentEventStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDelinquentEventStatusType', $objDatabase );
	}

	/**
	 * @return CDelinquentEventStatusType
	 */
	public static function fetchDelinquentEventStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDelinquentEventStatusType', $objDatabase );
	}

	public static function fetchDelinquentEventStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'delinquent_event_status_types', $objDatabase );
	}

	public static function fetchDelinquentEventStatusTypeById( $intId, $objDatabase ) {
		return self::fetchDelinquentEventStatusType( sprintf( 'SELECT * FROM delinquent_event_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>