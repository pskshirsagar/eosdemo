<?php

class CBaseScreeningListItem extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_list_items';

	protected $m_intId;
	protected $m_intScreeningListTypeId;
	protected $m_strListItemName;
	protected $m_strListItemDescription;
	protected $m_intIsPublished;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_list_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningListTypeId', trim( $arrValues['screening_list_type_id'] ) ); elseif( isset( $arrValues['screening_list_type_id'] ) ) $this->setScreeningListTypeId( $arrValues['screening_list_type_id'] );
		if( isset( $arrValues['list_item_name'] ) && $boolDirectSet ) $this->set( 'm_strListItemName', trim( stripcslashes( $arrValues['list_item_name'] ) ) ); elseif( isset( $arrValues['list_item_name'] ) ) $this->setListItemName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['list_item_name'] ) : $arrValues['list_item_name'] );
		if( isset( $arrValues['list_item_description'] ) && $boolDirectSet ) $this->set( 'm_strListItemDescription', trim( stripcslashes( $arrValues['list_item_description'] ) ) ); elseif( isset( $arrValues['list_item_description'] ) ) $this->setListItemDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['list_item_description'] ) : $arrValues['list_item_description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningListTypeId( $intScreeningListTypeId ) {
		$this->set( 'm_intScreeningListTypeId', CStrings::strToIntDef( $intScreeningListTypeId, NULL, false ) );
	}

	public function getScreeningListTypeId() {
		return $this->m_intScreeningListTypeId;
	}

	public function sqlScreeningListTypeId() {
		return ( true == isset( $this->m_intScreeningListTypeId ) ) ? ( string ) $this->m_intScreeningListTypeId : 'NULL';
	}

	public function setListItemName( $strListItemName ) {
		$this->set( 'm_strListItemName', CStrings::strTrimDef( $strListItemName, -1, NULL, true ) );
	}

	public function getListItemName() {
		return $this->m_strListItemName;
	}

	public function sqlListItemName() {
		return ( true == isset( $this->m_strListItemName ) ) ? '\'' . addslashes( $this->m_strListItemName ) . '\'' : 'NULL';
	}

	public function setListItemDescription( $strListItemDescription ) {
		$this->set( 'm_strListItemDescription', CStrings::strTrimDef( $strListItemDescription, -1, NULL, true ) );
	}

	public function getListItemDescription() {
		return $this->m_strListItemDescription;
	}

	public function sqlListItemDescription() {
		return ( true == isset( $this->m_strListItemDescription ) ) ? '\'' . addslashes( $this->m_strListItemDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_list_type_id' => $this->getScreeningListTypeId(),
			'list_item_name' => $this->getListItemName(),
			'list_item_description' => $this->getListItemDescription(),
			'is_published' => $this->getIsPublished()
		);
	}

}
?>