<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CConsumers
 * Do not add any new functions to this class.
 */

class CBaseConsumers extends CEosPluralBase {

	/**
	 * @return CConsumer[]
	 */
	public static function fetchConsumers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CConsumer', $objDatabase );
	}

	/**
	 * @return CConsumer
	 */
	public static function fetchConsumer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CConsumer', $objDatabase );
	}

	public static function fetchConsumerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'consumers', $objDatabase );
	}

	public static function fetchConsumerById( $intId, $objDatabase ) {
		return self::fetchConsumer( sprintf( 'SELECT * FROM consumers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchConsumersByScreeningId( $intScreeningId, $objDatabase ) {
		return self::fetchConsumers( sprintf( 'SELECT * FROM consumers WHERE screening_id = %d', ( int ) $intScreeningId ), $objDatabase );
	}

	public static function fetchConsumersByScreeningApplicantId( $intScreeningApplicantId, $objDatabase ) {
		return self::fetchConsumers( sprintf( 'SELECT * FROM consumers WHERE screening_applicant_id = %d', ( int ) $intScreeningApplicantId ), $objDatabase );
	}

	public static function fetchConsumersByContactMethodTypeId( $intContactMethodTypeId, $objDatabase ) {
		return self::fetchConsumers( sprintf( 'SELECT * FROM consumers WHERE contact_method_type_id = %d', ( int ) $intContactMethodTypeId ), $objDatabase );
	}

}
?>