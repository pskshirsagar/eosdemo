<?php

class CBaseScreeningRequestTypes extends CEosPluralBase {

    const TABLE_SCREENING_REQUEST_TYPES = 'public.screening_request_types';

    public static function fetchScreeningRequestTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CScreeningRequestType', $objDatabase );
    }

    public static function fetchScreeningRequestType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CScreeningRequestType', $objDatabase );
    }

    public static function fetchScreeningRequestTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'screening_request_types', $objDatabase );
    }

    public static function fetchScreeningRequestTypeById( $intId, $objDatabase ) {
        return self::fetchScreeningRequestType( sprintf( 'SELECT * FROM screening_request_types WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>