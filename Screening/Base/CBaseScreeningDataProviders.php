<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningDataProviders
 * Do not add any new functions to this class.
 */

class CBaseScreeningDataProviders extends CEosPluralBase {

	/**
	 * @return CScreeningDataProvider[]
	 */
	public static function fetchScreeningDataProviders( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningDataProvider', $objDatabase );
	}

	/**
	 * @return CScreeningDataProvider
	 */
	public static function fetchScreeningDataProvider( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningDataProvider', $objDatabase );
	}

	public static function fetchScreeningDataProviderCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_data_providers', $objDatabase );
	}

	public static function fetchScreeningDataProviderById( $intId, $objDatabase ) {
		return self::fetchScreeningDataProvider( sprintf( 'SELECT * FROM screening_data_providers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningDataProvidersByScreeningDataProviderTypeId( $intScreeningDataProviderTypeId, $objDatabase ) {
		return self::fetchScreeningDataProviders( sprintf( 'SELECT * FROM screening_data_providers WHERE screening_data_provider_type_id = %d', ( int ) $intScreeningDataProviderTypeId ), $objDatabase );
	}

	public static function fetchScreeningDataProvidersByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreeningDataProviders( sprintf( 'SELECT * FROM screening_data_providers WHERE screen_type_id = %d', ( int ) $intScreenTypeId ), $objDatabase );
	}

}
?>