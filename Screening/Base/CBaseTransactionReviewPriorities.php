<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CTransactionReviewPriorities
 * Do not add any new functions to this class.
 */

class CBaseTransactionReviewPriorities extends CEosPluralBase {

	/**
	 * @return CTransactionReviewPriority[]
	 */
	public static function fetchTransactionReviewPriorities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTransactionReviewPriority', $objDatabase );
	}

	/**
	 * @return CTransactionReviewPriority
	 */
	public static function fetchTransactionReviewPriority( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTransactionReviewPriority', $objDatabase );
	}

	public static function fetchTransactionReviewPriorityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transaction_review_priorities', $objDatabase );
	}

	public static function fetchTransactionReviewPriorityById( $intId, $objDatabase ) {
		return self::fetchTransactionReviewPriority( sprintf( 'SELECT * FROM transaction_review_priorities WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>