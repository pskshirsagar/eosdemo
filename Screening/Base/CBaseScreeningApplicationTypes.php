<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningApplicationTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningApplicationTypes extends CEosPluralBase {

	public static function fetchScreeningApplicationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningApplicationType', $objDatabase );
	}

	public static function fetchScreeningApplicationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningApplicationType', $objDatabase );
	}

	public static function fetchScreeningApplicationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_application_types', $objDatabase );
	}

	public static function fetchScreeningApplicationTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningApplicationType( sprintf( 'SELECT * FROM screening_application_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>