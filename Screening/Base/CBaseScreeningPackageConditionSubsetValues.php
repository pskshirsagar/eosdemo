<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageConditionSubsetValues
 * Do not add any new functions to this class.
 */

class CBaseScreeningPackageConditionSubsetValues extends CEosPluralBase {

	/**
	 * @return CScreeningPackageConditionSubsetValue[]
	 */
	public static function fetchScreeningPackageConditionSubsetValues( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageConditionSubsetValue', $objDatabase );
	}

	/**
	 * @return CScreeningPackageConditionSubsetValue
	 */
	public static function fetchScreeningPackageConditionSubsetValue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageConditionSubsetValue', $objDatabase );
	}

	public static function fetchScreeningPackageConditionSubsetValueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_condition_subset_values', $objDatabase );
	}

	public static function fetchScreeningPackageConditionSubsetValueById( $intId, $objDatabase ) {
		return self::fetchScreeningPackageConditionSubsetValue( sprintf( 'SELECT * FROM screening_package_condition_subset_values WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningPackageConditionSubsetValuesByScreeningPackageConditionSubsetId( $intScreeningPackageConditionSubsetId, $objDatabase ) {
		return self::fetchScreeningPackageConditionSubsetValues( sprintf( 'SELECT * FROM screening_package_condition_subset_values WHERE screening_package_condition_subset_id = %d', ( int ) $intScreeningPackageConditionSubsetId ), $objDatabase );
	}

	public static function fetchScreeningPackageConditionSubsetValuesByScreeningPackageAvailableConditionId( $intScreeningPackageAvailableConditionId, $objDatabase ) {
		return self::fetchScreeningPackageConditionSubsetValues( sprintf( 'SELECT * FROM screening_package_condition_subset_values WHERE screening_package_available_condition_id = %d', ( int ) $intScreeningPackageAvailableConditionId ), $objDatabase );
	}

}
?>