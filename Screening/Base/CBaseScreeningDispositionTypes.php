<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningDispositionTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningDispositionTypes extends CEosPluralBase {

	/**
	 * @return CScreeningDispositionType[]
	 */
	public static function fetchScreeningDispositionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningDispositionType', $objDatabase );
	}

	/**
	 * @return CScreeningDispositionType
	 */
	public static function fetchScreeningDispositionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningDispositionType', $objDatabase );
	}

	public static function fetchScreeningDispositionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_disposition_types', $objDatabase );
	}

	public static function fetchScreeningDispositionTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningDispositionType( sprintf( 'SELECT * FROM screening_disposition_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>