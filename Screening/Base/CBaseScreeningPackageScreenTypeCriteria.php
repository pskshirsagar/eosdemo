<?php

class CBaseScreeningPackageScreenTypeCriteria extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_package_screen_type_criterias';

	protected $m_intId;
	protected $m_intScreeningPackageId;
	protected $m_intScreeningPackageScreenTypeAssociationsId;
	protected $m_intScreeningSearchCriteriaTypeId;
	protected $m_strCriteria;
	protected $m_boolDispatchType;
	protected $m_boolOffenseDescription;
	protected $m_intIsPublished;
	protected $m_strUpdatedOn;
	protected $m_intUpdatedBy;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_package_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageId', trim( $arrValues['screening_package_id'] ) ); elseif( isset( $arrValues['screening_package_id'] ) ) $this->setScreeningPackageId( $arrValues['screening_package_id'] );
		if( isset( $arrValues['screening_package_screen_type_associations_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageScreenTypeAssociationsId', trim( $arrValues['screening_package_screen_type_associations_id'] ) ); elseif( isset( $arrValues['screening_package_screen_type_associations_id'] ) ) $this->setScreeningPackageScreenTypeAssociationsId( $arrValues['screening_package_screen_type_associations_id'] );
		if( isset( $arrValues['screening_search_criteria_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningSearchCriteriaTypeId', trim( $arrValues['screening_search_criteria_type_id'] ) ); elseif( isset( $arrValues['screening_search_criteria_type_id'] ) ) $this->setScreeningSearchCriteriaTypeId( $arrValues['screening_search_criteria_type_id'] );
		if( isset( $arrValues['criteria'] ) && $boolDirectSet ) $this->set( 'm_strCriteria', trim( stripcslashes( $arrValues['criteria'] ) ) ); elseif( isset( $arrValues['criteria'] ) ) $this->setCriteria( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['criteria'] ) : $arrValues['criteria'] );
		if( isset( $arrValues['dispatch_type'] ) && $boolDirectSet ) $this->set( 'm_boolDispatchType', trim( stripcslashes( $arrValues['dispatch_type'] ) ) ); elseif( isset( $arrValues['dispatch_type'] ) ) $this->setDispatchType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dispatch_type'] ) : $arrValues['dispatch_type'] );
		if( isset( $arrValues['offense_description'] ) && $boolDirectSet ) $this->set( 'm_boolOffenseDescription', trim( stripcslashes( $arrValues['offense_description'] ) ) ); elseif( isset( $arrValues['offense_description'] ) ) $this->setOffenseDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['offense_description'] ) : $arrValues['offense_description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningPackageId( $intScreeningPackageId ) {
		$this->set( 'm_intScreeningPackageId', CStrings::strToIntDef( $intScreeningPackageId, NULL, false ) );
	}

	public function getScreeningPackageId() {
		return $this->m_intScreeningPackageId;
	}

	public function sqlScreeningPackageId() {
		return ( true == isset( $this->m_intScreeningPackageId ) ) ? ( string ) $this->m_intScreeningPackageId : 'NULL';
	}

	public function setScreeningPackageScreenTypeAssociationsId( $intScreeningPackageScreenTypeAssociationsId ) {
		$this->set( 'm_intScreeningPackageScreenTypeAssociationsId', CStrings::strToIntDef( $intScreeningPackageScreenTypeAssociationsId, NULL, false ) );
	}

	public function getScreeningPackageScreenTypeAssociationsId() {
		return $this->m_intScreeningPackageScreenTypeAssociationsId;
	}

	public function sqlScreeningPackageScreenTypeAssociationsId() {
		return ( true == isset( $this->m_intScreeningPackageScreenTypeAssociationsId ) ) ? ( string ) $this->m_intScreeningPackageScreenTypeAssociationsId : 'NULL';
	}

	public function setScreeningSearchCriteriaTypeId( $intScreeningSearchCriteriaTypeId ) {
		$this->set( 'm_intScreeningSearchCriteriaTypeId', CStrings::strToIntDef( $intScreeningSearchCriteriaTypeId, NULL, false ) );
	}

	public function getScreeningSearchCriteriaTypeId() {
		return $this->m_intScreeningSearchCriteriaTypeId;
	}

	public function sqlScreeningSearchCriteriaTypeId() {
		return ( true == isset( $this->m_intScreeningSearchCriteriaTypeId ) ) ? ( string ) $this->m_intScreeningSearchCriteriaTypeId : 'NULL';
	}

	public function setCriteria( $strCriteria ) {
		$this->set( 'm_strCriteria', CStrings::strTrimDef( $strCriteria, -1, NULL, true ) );
	}

	public function getCriteria() {
		return $this->m_strCriteria;
	}

	public function sqlCriteria() {
		return ( true == isset( $this->m_strCriteria ) ) ? '\'' . addslashes( $this->m_strCriteria ) . '\'' : 'NULL';
	}

	public function setDispatchType( $boolDispatchType ) {
		$this->set( 'm_boolDispatchType', CStrings::strToBool( $boolDispatchType ) );
	}

	public function getDispatchType() {
		return $this->m_boolDispatchType;
	}

	public function sqlDispatchType() {
		return ( true == isset( $this->m_boolDispatchType ) ) ? '\'' . ( true == ( bool ) $this->m_boolDispatchType ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOffenseDescription( $boolOffenseDescription ) {
		$this->set( 'm_boolOffenseDescription', CStrings::strToBool( $boolOffenseDescription ) );
	}

	public function getOffenseDescription() {
		return $this->m_boolOffenseDescription;
	}

	public function sqlOffenseDescription() {
		return ( true == isset( $this->m_boolOffenseDescription ) ) ? '\'' . ( true == ( bool ) $this->m_boolOffenseDescription ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, screening_package_id, screening_package_screen_type_associations_id, screening_search_criteria_type_id, criteria, dispatch_type, offense_description, is_published, updated_on, updated_by, created_on, created_by )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlScreeningPackageId() . ', ' .
 						$this->sqlScreeningPackageScreenTypeAssociationsId() . ', ' .
 						$this->sqlScreeningSearchCriteriaTypeId() . ', ' .
 						$this->sqlCriteria() . ', ' .
 						$this->sqlDispatchType() . ', ' .
 						$this->sqlOffenseDescription() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_id = ' . $this->sqlScreeningPackageId() . ','; } elseif( true == array_key_exists( 'ScreeningPackageId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_id = ' . $this->sqlScreeningPackageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_screen_type_associations_id = ' . $this->sqlScreeningPackageScreenTypeAssociationsId() . ','; } elseif( true == array_key_exists( 'ScreeningPackageScreenTypeAssociationsId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_screen_type_associations_id = ' . $this->sqlScreeningPackageScreenTypeAssociationsId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_search_criteria_type_id = ' . $this->sqlScreeningSearchCriteriaTypeId() . ','; } elseif( true == array_key_exists( 'ScreeningSearchCriteriaTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_search_criteria_type_id = ' . $this->sqlScreeningSearchCriteriaTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' criteria = ' . $this->sqlCriteria() . ','; } elseif( true == array_key_exists( 'Criteria', $this->getChangedColumns() ) ) { $strSql .= ' criteria = ' . $this->sqlCriteria() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dispatch_type = ' . $this->sqlDispatchType() . ','; } elseif( true == array_key_exists( 'DispatchType', $this->getChangedColumns() ) ) { $strSql .= ' dispatch_type = ' . $this->sqlDispatchType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offense_description = ' . $this->sqlOffenseDescription() . ','; } elseif( true == array_key_exists( 'OffenseDescription', $this->getChangedColumns() ) ) { $strSql .= ' offense_description = ' . $this->sqlOffenseDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_package_id' => $this->getScreeningPackageId(),
			'screening_package_screen_type_associations_id' => $this->getScreeningPackageScreenTypeAssociationsId(),
			'screening_search_criteria_type_id' => $this->getScreeningSearchCriteriaTypeId(),
			'criteria' => $this->getCriteria(),
			'dispatch_type' => $this->getDispatchType(),
			'offense_description' => $this->getOffenseDescription(),
			'is_published' => $this->getIsPublished(),
			'updated_on' => $this->getUpdatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy()
		);
	}

}
?>