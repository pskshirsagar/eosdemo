<?php

class CBaseScreeningCriteriaConfigPreferenceTypeFilter extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_criteria_config_preference_type_filters';

	protected $m_intId;
	protected $m_intScreeningCriteriaId;
	protected $m_intScreeningConfigPreferenceTypeId;
	protected $m_intScreeningFilterId;
	protected $m_boolIsActive;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strExcludeFrom;
	protected $m_strExcludeTo;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_criteria_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningCriteriaId', trim( $arrValues['screening_criteria_id'] ) ); elseif( isset( $arrValues['screening_criteria_id'] ) ) $this->setScreeningCriteriaId( $arrValues['screening_criteria_id'] );
		if( isset( $arrValues['screening_config_preference_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningConfigPreferenceTypeId', trim( $arrValues['screening_config_preference_type_id'] ) ); elseif( isset( $arrValues['screening_config_preference_type_id'] ) ) $this->setScreeningConfigPreferenceTypeId( $arrValues['screening_config_preference_type_id'] );
		if( isset( $arrValues['screening_filter_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningFilterId', trim( $arrValues['screening_filter_id'] ) ); elseif( isset( $arrValues['screening_filter_id'] ) ) $this->setScreeningFilterId( $arrValues['screening_filter_id'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['exclude_from'] ) && $boolDirectSet ) $this->set( 'm_strExcludeFrom', trim( $arrValues['exclude_from'] ) ); elseif( isset( $arrValues['exclude_from'] ) ) $this->setExcludeFrom( $arrValues['exclude_from'] );
		if( isset( $arrValues['exclude_to'] ) && $boolDirectSet ) $this->set( 'm_strExcludeTo', trim( $arrValues['exclude_to'] ) ); elseif( isset( $arrValues['exclude_to'] ) ) $this->setExcludeTo( $arrValues['exclude_to'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningCriteriaId( $intScreeningCriteriaId ) {
		$this->set( 'm_intScreeningCriteriaId', CStrings::strToIntDef( $intScreeningCriteriaId, NULL, false ) );
	}

	public function getScreeningCriteriaId() {
		return $this->m_intScreeningCriteriaId;
	}

	public function sqlScreeningCriteriaId() {
		return ( true == isset( $this->m_intScreeningCriteriaId ) ) ? ( string ) $this->m_intScreeningCriteriaId : 'NULL';
	}

	public function setScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId ) {
		$this->set( 'm_intScreeningConfigPreferenceTypeId', CStrings::strToIntDef( $intScreeningConfigPreferenceTypeId, NULL, false ) );
	}

	public function getScreeningConfigPreferenceTypeId() {
		return $this->m_intScreeningConfigPreferenceTypeId;
	}

	public function sqlScreeningConfigPreferenceTypeId() {
		return ( true == isset( $this->m_intScreeningConfigPreferenceTypeId ) ) ? ( string ) $this->m_intScreeningConfigPreferenceTypeId : 'NULL';
	}

	public function setScreeningFilterId( $intScreeningFilterId ) {
		$this->set( 'm_intScreeningFilterId', CStrings::strToIntDef( $intScreeningFilterId, NULL, false ) );
	}

	public function getScreeningFilterId() {
		return $this->m_intScreeningFilterId;
	}

	public function sqlScreeningFilterId() {
		return ( true == isset( $this->m_intScreeningFilterId ) ) ? ( string ) $this->m_intScreeningFilterId : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setExcludeFrom( $strExcludeFrom ) {
		$this->set( 'm_strExcludeFrom', CStrings::strTrimDef( $strExcludeFrom, -1, NULL, true ) );
	}

	public function getExcludeFrom() {
		return $this->m_strExcludeFrom;
	}

	public function sqlExcludeFrom() {
		return ( true == isset( $this->m_strExcludeFrom ) ) ? '\'' . $this->m_strExcludeFrom . '\'' : 'NULL';
	}

	public function setExcludeTo( $strExcludeTo ) {
		$this->set( 'm_strExcludeTo', CStrings::strTrimDef( $strExcludeTo, -1, NULL, true ) );
	}

	public function getExcludeTo() {
		return $this->m_strExcludeTo;
	}

	public function sqlExcludeTo() {
		return ( true == isset( $this->m_strExcludeTo ) ) ? '\'' . $this->m_strExcludeTo . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, screening_criteria_id, screening_config_preference_type_id, screening_filter_id, is_active, updated_by, updated_on, created_by, created_on, exclude_from, exclude_to )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlScreeningCriteriaId() . ', ' .
		          $this->sqlScreeningConfigPreferenceTypeId() . ', ' .
		          $this->sqlScreeningFilterId() . ', ' .
		          $this->sqlIsActive() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlExcludeFrom() . ', ' .
		          $this->sqlExcludeTo() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_criteria_id = ' . $this->sqlScreeningCriteriaId(). ',' ; } elseif( true == array_key_exists( 'ScreeningCriteriaId', $this->getChangedColumns() ) ) { $strSql .= ' screening_criteria_id = ' . $this->sqlScreeningCriteriaId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_config_preference_type_id = ' . $this->sqlScreeningConfigPreferenceTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningConfigPreferenceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_config_preference_type_id = ' . $this->sqlScreeningConfigPreferenceTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_filter_id = ' . $this->sqlScreeningFilterId(). ',' ; } elseif( true == array_key_exists( 'ScreeningFilterId', $this->getChangedColumns() ) ) { $strSql .= ' screening_filter_id = ' . $this->sqlScreeningFilterId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exclude_from = ' . $this->sqlExcludeFrom(). ',' ; } elseif( true == array_key_exists( 'ExcludeFrom', $this->getChangedColumns() ) ) { $strSql .= ' exclude_from = ' . $this->sqlExcludeFrom() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exclude_to = ' . $this->sqlExcludeTo(). ',' ; } elseif( true == array_key_exists( 'ExcludeTo', $this->getChangedColumns() ) ) { $strSql .= ' exclude_to = ' . $this->sqlExcludeTo() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_criteria_id' => $this->getScreeningCriteriaId(),
			'screening_config_preference_type_id' => $this->getScreeningConfigPreferenceTypeId(),
			'screening_filter_id' => $this->getScreeningFilterId(),
			'is_active' => $this->getIsActive(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'exclude_from' => $this->getExcludeFrom(),
			'exclude_to' => $this->getExcludeTo()
		);
	}

}
?>