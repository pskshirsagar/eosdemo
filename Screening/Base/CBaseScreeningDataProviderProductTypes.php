<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningDataProviderProductTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningDataProviderProductTypes extends CEosPluralBase {

	/**
	 * @return CScreeningDataProviderProductType[]
	 */
	public static function fetchScreeningDataProviderProductTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningDataProviderProductType::class, $objDatabase );
	}

	/**
	 * @return CScreeningDataProviderProductType
	 */
	public static function fetchScreeningDataProviderProductType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningDataProviderProductType::class, $objDatabase );
	}

	public static function fetchScreeningDataProviderProductTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_data_provider_product_types', $objDatabase );
	}

	public static function fetchScreeningDataProviderProductTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningDataProviderProductType( sprintf( 'SELECT * FROM screening_data_provider_product_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>