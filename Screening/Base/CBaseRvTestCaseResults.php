<?php

class CBaseRvTestCaseResults extends CEosPluralBase {

	/**
	 * @return CRvTestCaseResult[]
	 */
	public static function fetchRvTestCaseResults( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRvTestCaseResult::class, $objDatabase );
	}

	/**
	 * @return CRvTestCaseResult
	 */
	public static function fetchRvTestCaseResult( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRvTestCaseResult::class, $objDatabase );
	}

	public static function fetchRvTestCaseResultCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'rv_test_case_results', $objDatabase );
	}

	public static function fetchRvTestCaseResultById( $intId, $objDatabase ) {
		return self::fetchRvTestCaseResult( sprintf( 'SELECT * FROM rv_test_case_results WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchRvTestCaseResultsByRvMasterTestCaseId( $intRvMasterTestCaseId, $objDatabase ) {
		return self::fetchRvTestCaseResults( sprintf( 'SELECT * FROM rv_test_case_results WHERE rv_master_test_case_id = %d', $intRvMasterTestCaseId ), $objDatabase );
	}

	public static function fetchRvTestCaseResultsByRvTestRunBatchId( $intRvTestRunBatchId, $objDatabase ) {
		return self::fetchRvTestCaseResults( sprintf( 'SELECT * FROM rv_test_case_results WHERE rv_test_run_batch_id = %d', $intRvTestRunBatchId ), $objDatabase );
	}

	public static function fetchRvTestCaseResultsByScreeningId( $intScreeningId, $objDatabase ) {
		return self::fetchRvTestCaseResults( sprintf( 'SELECT * FROM rv_test_case_results WHERE screening_id = %d', $intScreeningId ), $objDatabase );
	}

}
?>