<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CCriminalKeywordTypes
 * Do not add any new functions to this class.
 */

class CBaseCriminalKeywordTypes extends CEosPluralBase {

	/**
	 * @return CCriminalKeywordType[]
	 */
	public static function fetchCriminalKeywordTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCriminalKeywordType::class, $objDatabase );
	}

	/**
	 * @return CCriminalKeywordType
	 */
	public static function fetchCriminalKeywordType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCriminalKeywordType::class, $objDatabase );
	}

	public static function fetchCriminalKeywordTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'criminal_keyword_types', $objDatabase );
	}

	public static function fetchCriminalKeywordTypeById( $intId, $objDatabase ) {
		return self::fetchCriminalKeywordType( sprintf( 'SELECT * FROM criminal_keyword_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>