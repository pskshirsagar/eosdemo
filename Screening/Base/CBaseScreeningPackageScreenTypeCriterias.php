<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageScreenTypeCriterias
 * Do not add any new functions to this class.
 */

class CBaseScreeningPackageScreenTypeCriterias extends CEosPluralBase {

	/**
	 * @return CScreeningPackageScreenTypeCriteria[]
	 */
	public static function fetchScreeningPackageScreenTypeCriterias( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageScreenTypeCriteria', $objDatabase );
	}

	/**
	 * @return CScreeningPackageScreenTypeCriteria
	 */
	public static function fetchScreeningPackageScreenTypeCriteria( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageScreenTypeCriteria', $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeCriteriaCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_screen_type_criterias', $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeCriteriaById( $intId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeCriteria( sprintf( 'SELECT * FROM screening_package_screen_type_criterias WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeCriteriasByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeCriterias( sprintf( 'SELECT * FROM screening_package_screen_type_criterias WHERE screening_package_id = %d', ( int ) $intScreeningPackageId ), $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeCriteriasByScreeningPackageScreenTypeAssociationsId( $intScreeningPackageScreenTypeAssociationsId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeCriterias( sprintf( 'SELECT * FROM screening_package_screen_type_criterias WHERE screening_package_screen_type_associations_id = %d', ( int ) $intScreeningPackageScreenTypeAssociationsId ), $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeCriteriasByScreeningSearchCriteriaTypeId( $intScreeningSearchCriteriaTypeId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeCriterias( sprintf( 'SELECT * FROM screening_package_screen_type_criterias WHERE screening_search_criteria_type_id = %d', ( int ) $intScreeningSearchCriteriaTypeId ), $objDatabase );
	}

}
?>