<?php

class CBaseTimelineFilter extends CEosSingularBase {

    protected $m_intId;
    protected $m_intFilterValue;

    public function __construct() {
        parent::__construct();

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['filter_value'] ) && $boolDirectSet ) $this->m_intFilterValue = trim( $arrValues['filter_value'] ); else if( isset( $arrValues['filter_value'] ) ) $this->setFilterValue( $arrValues['filter_value'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setFilterValue( $intFilterValue ) {
        $this->m_intFilterValue = CStrings::strToIntDef( $intFilterValue, NULL, false );
    }

    public function getFilterValue() {
        return $this->m_intFilterValue;
    }

    public function sqlFilterValue() {
        return ( true == isset( $this->m_intFilterValue ) ) ? (string) $this->m_intFilterValue : 'NULL';
    }

}
?>