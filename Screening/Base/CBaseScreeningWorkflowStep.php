<?php

class CBaseScreeningWorkflowStep extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_workflow_steps';

	protected $m_intId;
	protected $m_intScreeningWorkflowSetupId;
	protected $m_intScreenTypeId;
	protected $m_intCriteriaId;
	protected $m_intNextWorkflowScreenTypeId;
	protected $m_boolIsPublished;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_workflow_setup_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningWorkflowSetupId', trim( $arrValues['screening_workflow_setup_id'] ) ); elseif( isset( $arrValues['screening_workflow_setup_id'] ) ) $this->setScreeningWorkflowSetupId( $arrValues['screening_workflow_setup_id'] );
		if( isset( $arrValues['screen_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreenTypeId', trim( $arrValues['screen_type_id'] ) ); elseif( isset( $arrValues['screen_type_id'] ) ) $this->setScreenTypeId( $arrValues['screen_type_id'] );
		if( isset( $arrValues['criteria_id'] ) && $boolDirectSet ) $this->set( 'm_intCriteriaId', trim( $arrValues['criteria_id'] ) ); elseif( isset( $arrValues['criteria_id'] ) ) $this->setCriteriaId( $arrValues['criteria_id'] );
		if( isset( $arrValues['next_workflow_screen_type_id'] ) && $boolDirectSet ) $this->set( 'm_intNextWorkflowScreenTypeId', trim( $arrValues['next_workflow_screen_type_id'] ) ); elseif( isset( $arrValues['next_workflow_screen_type_id'] ) ) $this->setNextWorkflowScreenTypeId( $arrValues['next_workflow_screen_type_id'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningWorkflowSetupId( $intScreeningWorkflowSetupId ) {
		$this->set( 'm_intScreeningWorkflowSetupId', CStrings::strToIntDef( $intScreeningWorkflowSetupId, NULL, false ) );
	}

	public function getScreeningWorkflowSetupId() {
		return $this->m_intScreeningWorkflowSetupId;
	}

	public function sqlScreeningWorkflowSetupId() {
		return ( true == isset( $this->m_intScreeningWorkflowSetupId ) ) ? ( string ) $this->m_intScreeningWorkflowSetupId : 'NULL';
	}

	public function setScreenTypeId( $intScreenTypeId ) {
		$this->set( 'm_intScreenTypeId', CStrings::strToIntDef( $intScreenTypeId, NULL, false ) );
	}

	public function getScreenTypeId() {
		return $this->m_intScreenTypeId;
	}

	public function sqlScreenTypeId() {
		return ( true == isset( $this->m_intScreenTypeId ) ) ? ( string ) $this->m_intScreenTypeId : 'NULL';
	}

	public function setCriteriaId( $intCriteriaId ) {
		$this->set( 'm_intCriteriaId', CStrings::strToIntDef( $intCriteriaId, NULL, false ) );
	}

	public function getCriteriaId() {
		return $this->m_intCriteriaId;
	}

	public function sqlCriteriaId() {
		return ( true == isset( $this->m_intCriteriaId ) ) ? ( string ) $this->m_intCriteriaId : 'NULL';
	}

	public function setNextWorkflowScreenTypeId( $intNextWorkflowScreenTypeId ) {
		$this->set( 'm_intNextWorkflowScreenTypeId', CStrings::strToIntDef( $intNextWorkflowScreenTypeId, NULL, false ) );
	}

	public function getNextWorkflowScreenTypeId() {
		return $this->m_intNextWorkflowScreenTypeId;
	}

	public function sqlNextWorkflowScreenTypeId() {
		return ( true == isset( $this->m_intNextWorkflowScreenTypeId ) ) ? ( string ) $this->m_intNextWorkflowScreenTypeId : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_workflow_setup_id' => $this->getScreeningWorkflowSetupId(),
			'screen_type_id' => $this->getScreenTypeId(),
			'criteria_id' => $this->getCriteriaId(),
			'next_workflow_screen_type_id' => $this->getNextWorkflowScreenTypeId(),
			'is_published' => $this->getIsPublished()
		);
	}

}
?>