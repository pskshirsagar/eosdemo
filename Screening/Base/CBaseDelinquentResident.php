<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDelinquentResident extends CEosSingularBase {

	const TABLE_NAME = 'public.delinquent_residents';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCustomerId;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strNameMiddle;
	protected $m_strEmailAddress;
	protected $m_strPhoneNumber;
	protected $m_strTaxNumberLastFour;
	protected $m_strTaxNumberEncrypted;
	protected $m_intBirthDateYear;
	protected $m_strBirthDateEncrypted;
	protected $m_strDriverLicense;
	protected $m_strDriverLicenseState;
	protected $m_strAddressLine;
	protected $m_strCity;
	protected $m_strState;
	protected $m_strZipcode;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( stripcslashes( $arrValues['name_first'] ) ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_first'] ) : $arrValues['name_first'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( stripcslashes( $arrValues['name_last'] ) ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_last'] ) : $arrValues['name_last'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( stripcslashes( $arrValues['name_middle'] ) ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_middle'] ) : $arrValues['name_middle'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['tax_number_last_four'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberLastFour', trim( stripcslashes( $arrValues['tax_number_last_four'] ) ) ); elseif( isset( $arrValues['tax_number_last_four'] ) ) $this->setTaxNumberLastFour( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_number_last_four'] ) : $arrValues['tax_number_last_four'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( stripcslashes( $arrValues['tax_number_encrypted'] ) ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_number_encrypted'] ) : $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['birth_date_year'] ) && $boolDirectSet ) $this->set( 'm_intBirthDateYear', trim( $arrValues['birth_date_year'] ) ); elseif( isset( $arrValues['birth_date_year'] ) ) $this->setBirthDateYear( $arrValues['birth_date_year'] );
		if( isset( $arrValues['birth_date_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBirthDateEncrypted', trim( stripcslashes( $arrValues['birth_date_encrypted'] ) ) ); elseif( isset( $arrValues['birth_date_encrypted'] ) ) $this->setBirthDateEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['birth_date_encrypted'] ) : $arrValues['birth_date_encrypted'] );
		if( isset( $arrValues['driver_license'] ) && $boolDirectSet ) $this->set( 'm_strDriverLicense', trim( stripcslashes( $arrValues['driver_license'] ) ) ); elseif( isset( $arrValues['driver_license'] ) ) $this->setDriverLicense( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['driver_license'] ) : $arrValues['driver_license'] );
		if( isset( $arrValues['driver_license_state'] ) && $boolDirectSet ) $this->set( 'm_strDriverLicenseState', trim( stripcslashes( $arrValues['driver_license_state'] ) ) ); elseif( isset( $arrValues['driver_license_state'] ) ) $this->setDriverLicenseState( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['driver_license_state'] ) : $arrValues['driver_license_state'] );
		if( isset( $arrValues['address_line'] ) && $boolDirectSet ) $this->set( 'm_strAddressLine', trim( stripcslashes( $arrValues['address_line'] ) ) ); elseif( isset( $arrValues['address_line'] ) ) $this->setAddressLine( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['address_line'] ) : $arrValues['address_line'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state'] ) && $boolDirectSet ) $this->set( 'm_strState', trim( stripcslashes( $arrValues['state'] ) ) ); elseif( isset( $arrValues['state'] ) ) $this->setState( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state'] ) : $arrValues['state'] );
		if( isset( $arrValues['zipcode'] ) && $boolDirectSet ) $this->set( 'm_strZipcode', trim( stripcslashes( $arrValues['zipcode'] ) ) ); elseif( isset( $arrValues['zipcode'] ) ) $this->setZipcode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['zipcode'] ) : $arrValues['zipcode'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 50, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? '\'' . addslashes( $this->m_strNameFirst ) . '\'' : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? '\'' . addslashes( $this->m_strNameLast ) . '\'' : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 50, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? '\'' . addslashes( $this->m_strNameMiddle ) . '\'' : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 20, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setTaxNumberLastFour( $strTaxNumberLastFour ) {
		$this->set( 'm_strTaxNumberLastFour', CStrings::strTrimDef( $strTaxNumberLastFour, 10, NULL, true ) );
	}

	public function getTaxNumberLastFour() {
		return $this->m_strTaxNumberLastFour;
	}

	public function sqlTaxNumberLastFour() {
		return ( true == isset( $this->m_strTaxNumberLastFour ) ) ? '\'' . addslashes( $this->m_strTaxNumberLastFour ) . '\'' : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 255, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setBirthDateYear( $intBirthDateYear ) {
		$this->set( 'm_intBirthDateYear', CStrings::strToIntDef( $intBirthDateYear, NULL, false ) );
	}

	public function getBirthDateYear() {
		return $this->m_intBirthDateYear;
	}

	public function sqlBirthDateYear() {
		return ( true == isset( $this->m_intBirthDateYear ) ) ? ( string ) $this->m_intBirthDateYear : 'NULL';
	}

	public function setBirthDateEncrypted( $strBirthDateEncrypted ) {
		$this->set( 'm_strBirthDateEncrypted', CStrings::strTrimDef( $strBirthDateEncrypted, 255, NULL, true ) );
	}

	public function getBirthDateEncrypted() {
		return $this->m_strBirthDateEncrypted;
	}

	public function sqlBirthDateEncrypted() {
		return ( true == isset( $this->m_strBirthDateEncrypted ) ) ? '\'' . addslashes( $this->m_strBirthDateEncrypted ) . '\'' : 'NULL';
	}

	public function setDriverLicense( $strDriverLicense ) {
		$this->set( 'm_strDriverLicense', CStrings::strTrimDef( $strDriverLicense, 50, NULL, true ) );
	}

	public function getDriverLicense() {
		return $this->m_strDriverLicense;
	}

	public function sqlDriverLicense() {
		return ( true == isset( $this->m_strDriverLicense ) ) ? '\'' . addslashes( $this->m_strDriverLicense ) . '\'' : 'NULL';
	}

	public function setDriverLicenseState( $strDriverLicenseState ) {
		$this->set( 'm_strDriverLicenseState', CStrings::strTrimDef( $strDriverLicenseState, 2, NULL, true ) );
	}

	public function getDriverLicenseState() {
		return $this->m_strDriverLicenseState;
	}

	public function sqlDriverLicenseState() {
		return ( true == isset( $this->m_strDriverLicenseState ) ) ? '\'' . addslashes( $this->m_strDriverLicenseState ) . '\'' : 'NULL';
	}

	public function setAddressLine( $strAddressLine ) {
		$this->set( 'm_strAddressLine', CStrings::strTrimDef( $strAddressLine, 100, NULL, true ) );
	}

	public function getAddressLine() {
		return $this->m_strAddressLine;
	}

	public function sqlAddressLine() {
		return ( true == isset( $this->m_strAddressLine ) ) ? '\'' . addslashes( $this->m_strAddressLine ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setState( $strState ) {
		$this->set( 'm_strState', CStrings::strTrimDef( $strState, 50, NULL, true ) );
	}

	public function getState() {
		return $this->m_strState;
	}

	public function sqlState() {
		return ( true == isset( $this->m_strState ) ) ? '\'' . addslashes( $this->m_strState ) . '\'' : 'NULL';
	}

	public function setZipcode( $strZipcode ) {
		$this->set( 'm_strZipcode', CStrings::strTrimDef( $strZipcode, 20, NULL, true ) );
	}

	public function getZipcode() {
		return $this->m_strZipcode;
	}

	public function sqlZipcode() {
		return ( true == isset( $this->m_strZipcode ) ) ? '\'' . addslashes( $this->m_strZipcode ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, customer_id, name_first, name_last, name_middle, email_address, phone_number, tax_number_last_four, tax_number_encrypted, birth_date_year, birth_date_encrypted, driver_license, driver_license_state, address_line, city, state, zipcode, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlNameFirst() . ', ' .
 						$this->sqlNameLast() . ', ' .
 						$this->sqlNameMiddle() . ', ' .
 						$this->sqlEmailAddress() . ', ' .
 						$this->sqlPhoneNumber() . ', ' .
 						$this->sqlTaxNumberLastFour() . ', ' .
 						$this->sqlTaxNumberEncrypted() . ', ' .
 						$this->sqlBirthDateYear() . ', ' .
 						$this->sqlBirthDateEncrypted() . ', ' .
 						$this->sqlDriverLicense() . ', ' .
 						$this->sqlDriverLicenseState() . ', ' .
 						$this->sqlAddressLine() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlState() . ', ' .
 						$this->sqlZipcode() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_last_four = ' . $this->sqlTaxNumberLastFour() . ','; } elseif( true == array_key_exists( 'TaxNumberLastFour', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_last_four = ' . $this->sqlTaxNumberLastFour() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' birth_date_year = ' . $this->sqlBirthDateYear() . ','; } elseif( true == array_key_exists( 'BirthDateYear', $this->getChangedColumns() ) ) { $strSql .= ' birth_date_year = ' . $this->sqlBirthDateYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' birth_date_encrypted = ' . $this->sqlBirthDateEncrypted() . ','; } elseif( true == array_key_exists( 'BirthDateEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' birth_date_encrypted = ' . $this->sqlBirthDateEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' driver_license = ' . $this->sqlDriverLicense() . ','; } elseif( true == array_key_exists( 'DriverLicense', $this->getChangedColumns() ) ) { $strSql .= ' driver_license = ' . $this->sqlDriverLicense() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' driver_license_state = ' . $this->sqlDriverLicenseState() . ','; } elseif( true == array_key_exists( 'DriverLicenseState', $this->getChangedColumns() ) ) { $strSql .= ' driver_license_state = ' . $this->sqlDriverLicenseState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address_line = ' . $this->sqlAddressLine() . ','; } elseif( true == array_key_exists( 'AddressLine', $this->getChangedColumns() ) ) { $strSql .= ' address_line = ' . $this->sqlAddressLine() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state = ' . $this->sqlState() . ','; } elseif( true == array_key_exists( 'State', $this->getChangedColumns() ) ) { $strSql .= ' state = ' . $this->sqlState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' zipcode = ' . $this->sqlZipcode() . ','; } elseif( true == array_key_exists( 'Zipcode', $this->getChangedColumns() ) ) { $strSql .= ' zipcode = ' . $this->sqlZipcode() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'customer_id' => $this->getCustomerId(),
			'name_first' => $this->getNameFirst(),
			'name_last' => $this->getNameLast(),
			'name_middle' => $this->getNameMiddle(),
			'email_address' => $this->getEmailAddress(),
			'phone_number' => $this->getPhoneNumber(),
			'tax_number_last_four' => $this->getTaxNumberLastFour(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'birth_date_year' => $this->getBirthDateYear(),
			'birth_date_encrypted' => $this->getBirthDateEncrypted(),
			'driver_license' => $this->getDriverLicense(),
			'driver_license_state' => $this->getDriverLicenseState(),
			'address_line' => $this->getAddressLine(),
			'city' => $this->getCity(),
			'state' => $this->getState(),
			'zipcode' => $this->getZipcode(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>