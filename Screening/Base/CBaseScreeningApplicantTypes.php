<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningApplicantTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningApplicantTypes extends CEosPluralBase {

	/**
	 * @return CScreeningApplicantType[]
	 */
	public static function fetchScreeningApplicantTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningApplicantType', $objDatabase );
	}

	/**
	 * @return CScreeningApplicantType
	 */
	public static function fetchScreeningApplicantType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningApplicantType', $objDatabase );
	}

	public static function fetchScreeningApplicantTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_applicant_types', $objDatabase );
	}

	public static function fetchScreeningApplicantTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningApplicantType( sprintf( 'SELECT * FROM screening_applicant_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>