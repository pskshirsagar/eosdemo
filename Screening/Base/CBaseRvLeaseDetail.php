<?php

class CBaseRvLeaseDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.rv_lease_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyTypeId;
	protected $m_intLeaseId;
	protected $m_intLeaseIntervalId;
	protected $m_intApplicationId;
	protected $m_intScreeningId;
	protected $m_intScreeningRecommendationTypeId;
	protected $m_intScreeningDecisionTypeId;
	protected $m_intPrimaryCustomerId;
	protected $m_intLeaseRenewalCount;
	protected $m_intLatePaymentCount;
	protected $m_intReturnedPaymentCount;
	protected $m_fltMoveOutBalance;
	protected $m_fltOpenLedgerBalance;
	protected $m_fltBadDebtWriteOffBalance;
	protected $m_fltRvRent;
	protected $m_fltDeposit;
	protected $m_fltRvTotalHouseholdIncome;
	protected $m_fltRvGuarantorIncome;
	protected $m_fltRiskPremiumRent;
	protected $m_fltRiskPremiumDeposit;
	protected $m_fltLateFee;
	protected $m_boolHasGuarantor;
	protected $m_boolHasSkips;
	protected $m_boolHasEvictions;
	protected $m_boolHasDelinquency;
	protected $m_boolIsCurrentLease;
	protected $m_boolIsEntrataCore;
	protected $m_boolIsTransfered;
	protected $m_strPropertyPostalCode;
	protected $m_strPropertyStateCode;
	protected $m_strLeaseIntervalType;
	protected $m_strLeaseStartDate;
	protected $m_strLeaseEndDate;
	protected $m_strMoveInDate;
	protected $m_strMoveOutDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_fltRent;

	public function __construct() {
		parent::__construct();

		$this->m_intLeaseRenewalCount = '0';
		$this->m_intLatePaymentCount = '0';
		$this->m_intReturnedPaymentCount = '0';
		$this->m_fltRvRent = '0';
		$this->m_fltDeposit = '0';
		$this->m_fltRvTotalHouseholdIncome = '0';
		$this->m_fltRvGuarantorIncome = '0';
		$this->m_fltLateFee = '0';
		$this->m_boolHasGuarantor = false;
		$this->m_boolHasSkips = false;
		$this->m_boolHasEvictions = false;
		$this->m_boolHasDelinquency = false;
		$this->m_boolIsCurrentLease = true;
		$this->m_boolIsEntrataCore = false;
		$this->m_boolIsTransfered = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyTypeId', trim( $arrValues['property_type_id'] ) ); elseif( isset( $arrValues['property_type_id'] ) ) $this->setPropertyTypeId( $arrValues['property_type_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['lease_interval_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalId', trim( $arrValues['lease_interval_id'] ) ); elseif( isset( $arrValues['lease_interval_id'] ) ) $this->setLeaseIntervalId( $arrValues['lease_interval_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['screening_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningId', trim( $arrValues['screening_id'] ) ); elseif( isset( $arrValues['screening_id'] ) ) $this->setScreeningId( $arrValues['screening_id'] );
		if( isset( $arrValues['screening_recommendation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningRecommendationTypeId', trim( $arrValues['screening_recommendation_type_id'] ) ); elseif( isset( $arrValues['screening_recommendation_type_id'] ) ) $this->setScreeningRecommendationTypeId( $arrValues['screening_recommendation_type_id'] );
		if( isset( $arrValues['screening_decision_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningDecisionTypeId', trim( $arrValues['screening_decision_type_id'] ) ); elseif( isset( $arrValues['screening_decision_type_id'] ) ) $this->setScreeningDecisionTypeId( $arrValues['screening_decision_type_id'] );
		if( isset( $arrValues['primary_customer_id'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryCustomerId', trim( $arrValues['primary_customer_id'] ) ); elseif( isset( $arrValues['primary_customer_id'] ) ) $this->setPrimaryCustomerId( $arrValues['primary_customer_id'] );
		if( isset( $arrValues['lease_renewal_count'] ) && $boolDirectSet ) $this->set( 'm_intLeaseRenewalCount', trim( $arrValues['lease_renewal_count'] ) ); elseif( isset( $arrValues['lease_renewal_count'] ) ) $this->setLeaseRenewalCount( $arrValues['lease_renewal_count'] );
		if( isset( $arrValues['late_payment_count'] ) && $boolDirectSet ) $this->set( 'm_intLatePaymentCount', trim( $arrValues['late_payment_count'] ) ); elseif( isset( $arrValues['late_payment_count'] ) ) $this->setLatePaymentCount( $arrValues['late_payment_count'] );
		if( isset( $arrValues['returned_payment_count'] ) && $boolDirectSet ) $this->set( 'm_intReturnedPaymentCount', trim( $arrValues['returned_payment_count'] ) ); elseif( isset( $arrValues['returned_payment_count'] ) ) $this->setReturnedPaymentCount( $arrValues['returned_payment_count'] );
		if( isset( $arrValues['move_out_balance'] ) && $boolDirectSet ) $this->set( 'm_fltMoveOutBalance', trim( $arrValues['move_out_balance'] ) ); elseif( isset( $arrValues['move_out_balance'] ) ) $this->setMoveOutBalance( $arrValues['move_out_balance'] );
		if( isset( $arrValues['open_ledger_balance'] ) && $boolDirectSet ) $this->set( 'm_fltOpenLedgerBalance', trim( $arrValues['open_ledger_balance'] ) ); elseif( isset( $arrValues['open_ledger_balance'] ) ) $this->setOpenLedgerBalance( $arrValues['open_ledger_balance'] );
		if( isset( $arrValues['bad_debt_write_off_balance'] ) && $boolDirectSet ) $this->set( 'm_fltBadDebtWriteOffBalance', trim( $arrValues['bad_debt_write_off_balance'] ) ); elseif( isset( $arrValues['bad_debt_write_off_balance'] ) ) $this->setBadDebtWriteOffBalance( $arrValues['bad_debt_write_off_balance'] );
		if( isset( $arrValues['rv_rent'] ) && $boolDirectSet ) $this->set( 'm_fltRvRent', trim( $arrValues['rv_rent'] ) ); elseif( isset( $arrValues['rv_rent'] ) ) $this->setRvRent( $arrValues['rv_rent'] );
		if( isset( $arrValues['deposit'] ) && $boolDirectSet ) $this->set( 'm_fltDeposit', trim( $arrValues['deposit'] ) ); elseif( isset( $arrValues['deposit'] ) ) $this->setDeposit( $arrValues['deposit'] );
		if( isset( $arrValues['rv_total_household_income'] ) && $boolDirectSet ) $this->set( 'm_fltRvTotalHouseholdIncome', trim( $arrValues['rv_total_household_income'] ) ); elseif( isset( $arrValues['rv_total_household_income'] ) ) $this->setRvTotalHouseholdIncome( $arrValues['rv_total_household_income'] );
		if( isset( $arrValues['rv_guarantor_income'] ) && $boolDirectSet ) $this->set( 'm_fltRvGuarantorIncome', trim( $arrValues['rv_guarantor_income'] ) ); elseif( isset( $arrValues['rv_guarantor_income'] ) ) $this->setRvGuarantorIncome( $arrValues['rv_guarantor_income'] );
		if( isset( $arrValues['risk_premium_rent'] ) && $boolDirectSet ) $this->set( 'm_fltRiskPremiumRent', trim( $arrValues['risk_premium_rent'] ) ); elseif( isset( $arrValues['risk_premium_rent'] ) ) $this->setRiskPremiumRent( $arrValues['risk_premium_rent'] );
		if( isset( $arrValues['risk_premium_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltRiskPremiumDeposit', trim( $arrValues['risk_premium_deposit'] ) ); elseif( isset( $arrValues['risk_premium_deposit'] ) ) $this->setRiskPremiumDeposit( $arrValues['risk_premium_deposit'] );
		if( isset( $arrValues['late_fee'] ) && $boolDirectSet ) $this->set( 'm_fltLateFee', trim( $arrValues['late_fee'] ) ); elseif( isset( $arrValues['late_fee'] ) ) $this->setLateFee( $arrValues['late_fee'] );
		if( isset( $arrValues['has_guarantor'] ) && $boolDirectSet ) $this->set( 'm_boolHasGuarantor', trim( stripcslashes( $arrValues['has_guarantor'] ) ) ); elseif( isset( $arrValues['has_guarantor'] ) ) $this->setHasGuarantor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_guarantor'] ) : $arrValues['has_guarantor'] );
		if( isset( $arrValues['has_skips'] ) && $boolDirectSet ) $this->set( 'm_boolHasSkips', trim( stripcslashes( $arrValues['has_skips'] ) ) ); elseif( isset( $arrValues['has_skips'] ) ) $this->setHasSkips( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_skips'] ) : $arrValues['has_skips'] );
		if( isset( $arrValues['has_evictions'] ) && $boolDirectSet ) $this->set( 'm_boolHasEvictions', trim( stripcslashes( $arrValues['has_evictions'] ) ) ); elseif( isset( $arrValues['has_evictions'] ) ) $this->setHasEvictions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_evictions'] ) : $arrValues['has_evictions'] );
		if( isset( $arrValues['has_delinquency'] ) && $boolDirectSet ) $this->set( 'm_boolHasDelinquency', trim( stripcslashes( $arrValues['has_delinquency'] ) ) ); elseif( isset( $arrValues['has_delinquency'] ) ) $this->setHasDelinquency( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_delinquency'] ) : $arrValues['has_delinquency'] );
		if( isset( $arrValues['is_current_lease'] ) && $boolDirectSet ) $this->set( 'm_boolIsCurrentLease', trim( stripcslashes( $arrValues['is_current_lease'] ) ) ); elseif( isset( $arrValues['is_current_lease'] ) ) $this->setIsCurrentLease( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_current_lease'] ) : $arrValues['is_current_lease'] );
		if( isset( $arrValues['is_entrata_core'] ) && $boolDirectSet ) $this->set( 'm_boolIsEntrataCore', trim( stripcslashes( $arrValues['is_entrata_core'] ) ) ); elseif( isset( $arrValues['is_entrata_core'] ) ) $this->setIsEntrataCore( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_entrata_core'] ) : $arrValues['is_entrata_core'] );
		if( isset( $arrValues['is_transfered'] ) && $boolDirectSet ) $this->set( 'm_boolIsTransfered', trim( stripcslashes( $arrValues['is_transfered'] ) ) ); elseif( isset( $arrValues['is_transfered'] ) ) $this->setIsTransfered( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_transfered'] ) : $arrValues['is_transfered'] );
		if( isset( $arrValues['property_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPropertyPostalCode', trim( $arrValues['property_postal_code'] ) ); elseif( isset( $arrValues['property_postal_code'] ) ) $this->setPropertyPostalCode( $arrValues['property_postal_code'] );
		if( isset( $arrValues['property_state_code'] ) && $boolDirectSet ) $this->set( 'm_strPropertyStateCode', trim( $arrValues['property_state_code'] ) ); elseif( isset( $arrValues['property_state_code'] ) ) $this->setPropertyStateCode( $arrValues['property_state_code'] );
		if( isset( $arrValues['lease_interval_type'] ) && $boolDirectSet ) $this->set( 'm_strLeaseIntervalType', trim( $arrValues['lease_interval_type'] ) ); elseif( isset( $arrValues['lease_interval_type'] ) ) $this->setLeaseIntervalType( $arrValues['lease_interval_type'] );
		if( isset( $arrValues['lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartDate', trim( $arrValues['lease_start_date'] ) ); elseif( isset( $arrValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrValues['lease_start_date'] );
		if( isset( $arrValues['lease_end_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseEndDate', trim( $arrValues['lease_end_date'] ) ); elseif( isset( $arrValues['lease_end_date'] ) ) $this->setLeaseEndDate( $arrValues['lease_end_date'] );
		if( isset( $arrValues['move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDate', trim( $arrValues['move_in_date'] ) ); elseif( isset( $arrValues['move_in_date'] ) ) $this->setMoveInDate( $arrValues['move_in_date'] );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['rent'] ) && $boolDirectSet ) $this->set( 'm_fltRent', trim( $arrValues['rent'] ) ); elseif( isset( $arrValues['rent'] ) ) $this->setRent( $arrValues['rent'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyTypeId( $intPropertyTypeId ) {
		$this->set( 'm_intPropertyTypeId', CStrings::strToIntDef( $intPropertyTypeId, NULL, false ) );
	}

	public function getPropertyTypeId() {
		return $this->m_intPropertyTypeId;
	}

	public function sqlPropertyTypeId() {
		return ( true == isset( $this->m_intPropertyTypeId ) ) ? ( string ) $this->m_intPropertyTypeId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setLeaseIntervalId( $intLeaseIntervalId ) {
		$this->set( 'm_intLeaseIntervalId', CStrings::strToIntDef( $intLeaseIntervalId, NULL, false ) );
	}

	public function getLeaseIntervalId() {
		return $this->m_intLeaseIntervalId;
	}

	public function sqlLeaseIntervalId() {
		return ( true == isset( $this->m_intLeaseIntervalId ) ) ? ( string ) $this->m_intLeaseIntervalId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setScreeningId( $intScreeningId ) {
		$this->set( 'm_intScreeningId', CStrings::strToIntDef( $intScreeningId, NULL, false ) );
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function sqlScreeningId() {
		return ( true == isset( $this->m_intScreeningId ) ) ? ( string ) $this->m_intScreeningId : 'NULL';
	}

	public function setScreeningRecommendationTypeId( $intScreeningRecommendationTypeId ) {
		$this->set( 'm_intScreeningRecommendationTypeId', CStrings::strToIntDef( $intScreeningRecommendationTypeId, NULL, false ) );
	}

	public function getScreeningRecommendationTypeId() {
		return $this->m_intScreeningRecommendationTypeId;
	}

	public function sqlScreeningRecommendationTypeId() {
		return ( true == isset( $this->m_intScreeningRecommendationTypeId ) ) ? ( string ) $this->m_intScreeningRecommendationTypeId : 'NULL';
	}

	public function setScreeningDecisionTypeId( $intScreeningDecisionTypeId ) {
		$this->set( 'm_intScreeningDecisionTypeId', CStrings::strToIntDef( $intScreeningDecisionTypeId, NULL, false ) );
	}

	public function getScreeningDecisionTypeId() {
		return $this->m_intScreeningDecisionTypeId;
	}

	public function sqlScreeningDecisionTypeId() {
		return ( true == isset( $this->m_intScreeningDecisionTypeId ) ) ? ( string ) $this->m_intScreeningDecisionTypeId : 'NULL';
	}

	public function setPrimaryCustomerId( $intPrimaryCustomerId ) {
		$this->set( 'm_intPrimaryCustomerId', CStrings::strToIntDef( $intPrimaryCustomerId, NULL, false ) );
	}

	public function getPrimaryCustomerId() {
		return $this->m_intPrimaryCustomerId;
	}

	public function sqlPrimaryCustomerId() {
		return ( true == isset( $this->m_intPrimaryCustomerId ) ) ? ( string ) $this->m_intPrimaryCustomerId : 'NULL';
	}

	public function setLeaseRenewalCount( $intLeaseRenewalCount ) {
		$this->set( 'm_intLeaseRenewalCount', CStrings::strToIntDef( $intLeaseRenewalCount, NULL, false ) );
	}

	public function getLeaseRenewalCount() {
		return $this->m_intLeaseRenewalCount;
	}

	public function sqlLeaseRenewalCount() {
		return ( true == isset( $this->m_intLeaseRenewalCount ) ) ? ( string ) $this->m_intLeaseRenewalCount : '0';
	}

	public function setLatePaymentCount( $intLatePaymentCount ) {
		$this->set( 'm_intLatePaymentCount', CStrings::strToIntDef( $intLatePaymentCount, NULL, false ) );
	}

	public function getLatePaymentCount() {
		return $this->m_intLatePaymentCount;
	}

	public function sqlLatePaymentCount() {
		return ( true == isset( $this->m_intLatePaymentCount ) ) ? ( string ) $this->m_intLatePaymentCount : '0';
	}

	public function setReturnedPaymentCount( $intReturnedPaymentCount ) {
		$this->set( 'm_intReturnedPaymentCount', CStrings::strToIntDef( $intReturnedPaymentCount, NULL, false ) );
	}

	public function getReturnedPaymentCount() {
		return $this->m_intReturnedPaymentCount;
	}

	public function sqlReturnedPaymentCount() {
		return ( true == isset( $this->m_intReturnedPaymentCount ) ) ? ( string ) $this->m_intReturnedPaymentCount : '0';
	}

	public function setMoveOutBalance( $fltMoveOutBalance ) {
		$this->set( 'm_fltMoveOutBalance', CStrings::strToFloatDef( $fltMoveOutBalance, NULL, false, 2 ) );
	}

	public function getMoveOutBalance() {
		return $this->m_fltMoveOutBalance;
	}

	public function sqlMoveOutBalance() {
		return ( true == isset( $this->m_fltMoveOutBalance ) ) ? ( string ) $this->m_fltMoveOutBalance : 'NULL';
	}

	public function setOpenLedgerBalance( $fltOpenLedgerBalance ) {
		$this->set( 'm_fltOpenLedgerBalance', CStrings::strToFloatDef( $fltOpenLedgerBalance, NULL, false, 2 ) );
	}

	public function getOpenLedgerBalance() {
		return $this->m_fltOpenLedgerBalance;
	}

	public function sqlOpenLedgerBalance() {
		return ( true == isset( $this->m_fltOpenLedgerBalance ) ) ? ( string ) $this->m_fltOpenLedgerBalance : 'NULL';
	}

	public function setBadDebtWriteOffBalance( $fltBadDebtWriteOffBalance ) {
		$this->set( 'm_fltBadDebtWriteOffBalance', CStrings::strToFloatDef( $fltBadDebtWriteOffBalance, NULL, false, 2 ) );
	}

	public function getBadDebtWriteOffBalance() {
		return $this->m_fltBadDebtWriteOffBalance;
	}

	public function sqlBadDebtWriteOffBalance() {
		return ( true == isset( $this->m_fltBadDebtWriteOffBalance ) ) ? ( string ) $this->m_fltBadDebtWriteOffBalance : 'NULL';
	}

	public function setRvRent( $fltRvRent ) {
		$this->set( 'm_fltRvRent', CStrings::strToFloatDef( $fltRvRent, NULL, false, 2 ) );
	}

	public function getRvRent() {
		return $this->m_fltRvRent;
	}

	public function sqlRvRent() {
		return ( true == isset( $this->m_fltRvRent ) ) ? ( string ) $this->m_fltRvRent : '0';
	}

	public function setDeposit( $fltDeposit ) {
		$this->set( 'm_fltDeposit', CStrings::strToFloatDef( $fltDeposit, NULL, false, 2 ) );
	}

	public function getDeposit() {
		return $this->m_fltDeposit;
	}

	public function sqlDeposit() {
		return ( true == isset( $this->m_fltDeposit ) ) ? ( string ) $this->m_fltDeposit : '0';
	}

	public function setRvTotalHouseholdIncome( $fltRvTotalHouseholdIncome ) {
		$this->set( 'm_fltRvTotalHouseholdIncome', CStrings::strToFloatDef( $fltRvTotalHouseholdIncome, NULL, false, 2 ) );
	}

	public function getRvTotalHouseholdIncome() {
		return $this->m_fltRvTotalHouseholdIncome;
	}

	public function sqlRvTotalHouseholdIncome() {
		return ( true == isset( $this->m_fltRvTotalHouseholdIncome ) ) ? ( string ) $this->m_fltRvTotalHouseholdIncome : '0';
	}

	public function setRvGuarantorIncome( $fltRvGuarantorIncome ) {
		$this->set( 'm_fltRvGuarantorIncome', CStrings::strToFloatDef( $fltRvGuarantorIncome, NULL, false, 2 ) );
	}

	public function getRvGuarantorIncome() {
		return $this->m_fltRvGuarantorIncome;
	}

	public function sqlRvGuarantorIncome() {
		return ( true == isset( $this->m_fltRvGuarantorIncome ) ) ? ( string ) $this->m_fltRvGuarantorIncome : '0';
	}

	public function setRiskPremiumRent( $fltRiskPremiumRent ) {
		$this->set( 'm_fltRiskPremiumRent', CStrings::strToFloatDef( $fltRiskPremiumRent, NULL, false, 2 ) );
	}

	public function getRiskPremiumRent() {
		return $this->m_fltRiskPremiumRent;
	}

	public function sqlRiskPremiumRent() {
		return ( true == isset( $this->m_fltRiskPremiumRent ) ) ? ( string ) $this->m_fltRiskPremiumRent : 'NULL';
	}

	public function setRiskPremiumDeposit( $fltRiskPremiumDeposit ) {
		$this->set( 'm_fltRiskPremiumDeposit', CStrings::strToFloatDef( $fltRiskPremiumDeposit, NULL, false, 2 ) );
	}

	public function getRiskPremiumDeposit() {
		return $this->m_fltRiskPremiumDeposit;
	}

	public function sqlRiskPremiumDeposit() {
		return ( true == isset( $this->m_fltRiskPremiumDeposit ) ) ? ( string ) $this->m_fltRiskPremiumDeposit : 'NULL';
	}

	public function setLateFee( $fltLateFee ) {
		$this->set( 'm_fltLateFee', CStrings::strToFloatDef( $fltLateFee, NULL, false, 2 ) );
	}

	public function getLateFee() {
		return $this->m_fltLateFee;
	}

	public function sqlLateFee() {
		return ( true == isset( $this->m_fltLateFee ) ) ? ( string ) $this->m_fltLateFee : '0';
	}

	public function setHasGuarantor( $boolHasGuarantor ) {
		$this->set( 'm_boolHasGuarantor', CStrings::strToBool( $boolHasGuarantor ) );
	}

	public function getHasGuarantor() {
		return $this->m_boolHasGuarantor;
	}

	public function sqlHasGuarantor() {
		return ( true == isset( $this->m_boolHasGuarantor ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasGuarantor ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasSkips( $boolHasSkips ) {
		$this->set( 'm_boolHasSkips', CStrings::strToBool( $boolHasSkips ) );
	}

	public function getHasSkips() {
		return $this->m_boolHasSkips;
	}

	public function sqlHasSkips() {
		return ( true == isset( $this->m_boolHasSkips ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasSkips ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasEvictions( $boolHasEvictions ) {
		$this->set( 'm_boolHasEvictions', CStrings::strToBool( $boolHasEvictions ) );
	}

	public function getHasEvictions() {
		return $this->m_boolHasEvictions;
	}

	public function sqlHasEvictions() {
		return ( true == isset( $this->m_boolHasEvictions ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasEvictions ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasDelinquency( $boolHasDelinquency ) {
		$this->set( 'm_boolHasDelinquency', CStrings::strToBool( $boolHasDelinquency ) );
	}

	public function getHasDelinquency() {
		return $this->m_boolHasDelinquency;
	}

	public function sqlHasDelinquency() {
		return ( true == isset( $this->m_boolHasDelinquency ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasDelinquency ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCurrentLease( $boolIsCurrentLease ) {
		$this->set( 'm_boolIsCurrentLease', CStrings::strToBool( $boolIsCurrentLease ) );
	}

	public function getIsCurrentLease() {
		return $this->m_boolIsCurrentLease;
	}

	public function sqlIsCurrentLease() {
		return ( true == isset( $this->m_boolIsCurrentLease ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCurrentLease ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsEntrataCore( $boolIsEntrataCore ) {
		$this->set( 'm_boolIsEntrataCore', CStrings::strToBool( $boolIsEntrataCore ) );
	}

	public function getIsEntrataCore() {
		return $this->m_boolIsEntrataCore;
	}

	public function sqlIsEntrataCore() {
		return ( true == isset( $this->m_boolIsEntrataCore ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEntrataCore ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTransfered( $boolIsTransfered ) {
		$this->set( 'm_boolIsTransfered', CStrings::strToBool( $boolIsTransfered ) );
	}

	public function getIsTransfered() {
		return $this->m_boolIsTransfered;
	}

	public function sqlIsTransfered() {
		return ( true == isset( $this->m_boolIsTransfered ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTransfered ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPropertyPostalCode( $strPropertyPostalCode ) {
		$this->set( 'm_strPropertyPostalCode', CStrings::strTrimDef( $strPropertyPostalCode, -1, NULL, true ) );
	}

	public function getPropertyPostalCode() {
		return $this->m_strPropertyPostalCode;
	}

	public function sqlPropertyPostalCode() {
		return ( true == isset( $this->m_strPropertyPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPropertyPostalCode ) : '\'' . addslashes( $this->m_strPropertyPostalCode ) . '\'' ) : 'NULL';
	}

	public function setPropertyStateCode( $strPropertyStateCode ) {
		$this->set( 'm_strPropertyStateCode', CStrings::strTrimDef( $strPropertyStateCode, -1, NULL, true ) );
	}

	public function getPropertyStateCode() {
		return $this->m_strPropertyStateCode;
	}

	public function sqlPropertyStateCode() {
		return ( true == isset( $this->m_strPropertyStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPropertyStateCode ) : '\'' . addslashes( $this->m_strPropertyStateCode ) . '\'' ) : 'NULL';
	}

	public function setLeaseIntervalType( $strLeaseIntervalType ) {
		$this->set( 'm_strLeaseIntervalType', CStrings::strTrimDef( $strLeaseIntervalType, -1, NULL, true ) );
	}

	public function getLeaseIntervalType() {
		return $this->m_strLeaseIntervalType;
	}

	public function sqlLeaseIntervalType() {
		return ( true == isset( $this->m_strLeaseIntervalType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeaseIntervalType ) : '\'' . addslashes( $this->m_strLeaseIntervalType ) . '\'' ) : 'NULL';
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->set( 'm_strLeaseStartDate', CStrings::strTrimDef( $strLeaseStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function sqlLeaseStartDate() {
		return ( true == isset( $this->m_strLeaseStartDate ) ) ? '\'' . $this->m_strLeaseStartDate . '\'' : 'NULL';
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->set( 'm_strLeaseEndDate', CStrings::strTrimDef( $strLeaseEndDate, -1, NULL, true ) );
	}

	public function getLeaseEndDate() {
		return $this->m_strLeaseEndDate;
	}

	public function sqlLeaseEndDate() {
		return ( true == isset( $this->m_strLeaseEndDate ) ) ? '\'' . $this->m_strLeaseEndDate . '\'' : 'NULL';
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->set( 'm_strMoveInDate', CStrings::strTrimDef( $strMoveInDate, -1, NULL, true ) );
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function sqlMoveInDate() {
		return ( true == isset( $this->m_strMoveInDate ) ) ? '\'' . $this->m_strMoveInDate . '\'' : 'NULL';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setRent( $fltRent ) {
		$this->set( 'm_fltRent', CStrings::strToFloatDef( $fltRent, NULL, false, 2 ) );
	}

	public function getRent() {
		return $this->m_fltRent;
	}

	public function sqlRent() {
		return ( true == isset( $this->m_fltRent ) ) ? ( string ) $this->m_fltRent : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_type_id, lease_id, lease_interval_id, application_id, screening_id, screening_recommendation_type_id, screening_decision_type_id, primary_customer_id, lease_renewal_count, late_payment_count, returned_payment_count, move_out_balance, open_ledger_balance, bad_debt_write_off_balance, rv_rent, deposit, rv_total_household_income, rv_guarantor_income, risk_premium_rent, risk_premium_deposit, late_fee, has_guarantor, has_skips, has_evictions, has_delinquency, is_current_lease, is_entrata_core, is_transfered, property_postal_code, property_state_code, lease_interval_type, lease_start_date, lease_end_date, move_in_date, move_out_date, updated_by, updated_on, created_by, created_on, rent )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyTypeId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlLeaseIntervalId() . ', ' .
						$this->sqlApplicationId() . ', ' .
						$this->sqlScreeningId() . ', ' .
						$this->sqlScreeningRecommendationTypeId() . ', ' .
						$this->sqlScreeningDecisionTypeId() . ', ' .
						$this->sqlPrimaryCustomerId() . ', ' .
						$this->sqlLeaseRenewalCount() . ', ' .
						$this->sqlLatePaymentCount() . ', ' .
						$this->sqlReturnedPaymentCount() . ', ' .
						$this->sqlMoveOutBalance() . ', ' .
						$this->sqlOpenLedgerBalance() . ', ' .
						$this->sqlBadDebtWriteOffBalance() . ', ' .
						$this->sqlRvRent() . ', ' .
						$this->sqlDeposit() . ', ' .
						$this->sqlRvTotalHouseholdIncome() . ', ' .
						$this->sqlRvGuarantorIncome() . ', ' .
						$this->sqlRiskPremiumRent() . ', ' .
						$this->sqlRiskPremiumDeposit() . ', ' .
						$this->sqlLateFee() . ', ' .
						$this->sqlHasGuarantor() . ', ' .
						$this->sqlHasSkips() . ', ' .
						$this->sqlHasEvictions() . ', ' .
						$this->sqlHasDelinquency() . ', ' .
						$this->sqlIsCurrentLease() . ', ' .
						$this->sqlIsEntrataCore() . ', ' .
						$this->sqlIsTransfered() . ', ' .
						$this->sqlPropertyPostalCode() . ', ' .
						$this->sqlPropertyStateCode() . ', ' .
						$this->sqlLeaseIntervalType() . ', ' .
						$this->sqlLeaseStartDate() . ', ' .
						$this->sqlLeaseEndDate() . ', ' .
						$this->sqlMoveInDate() . ', ' .
						$this->sqlMoveOutDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlRent() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_type_id = ' . $this->sqlPropertyTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_type_id = ' . $this->sqlPropertyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalId', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId(). ',' ; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId(). ',' ; } elseif( true == array_key_exists( 'ScreeningId', $this->getChangedColumns() ) ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningRecommendationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_decision_type_id = ' . $this->sqlScreeningDecisionTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningDecisionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_decision_type_id = ' . $this->sqlScreeningDecisionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_customer_id = ' . $this->sqlPrimaryCustomerId(). ',' ; } elseif( true == array_key_exists( 'PrimaryCustomerId', $this->getChangedColumns() ) ) { $strSql .= ' primary_customer_id = ' . $this->sqlPrimaryCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_renewal_count = ' . $this->sqlLeaseRenewalCount(). ',' ; } elseif( true == array_key_exists( 'LeaseRenewalCount', $this->getChangedColumns() ) ) { $strSql .= ' lease_renewal_count = ' . $this->sqlLeaseRenewalCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_payment_count = ' . $this->sqlLatePaymentCount(). ',' ; } elseif( true == array_key_exists( 'LatePaymentCount', $this->getChangedColumns() ) ) { $strSql .= ' late_payment_count = ' . $this->sqlLatePaymentCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_payment_count = ' . $this->sqlReturnedPaymentCount(). ',' ; } elseif( true == array_key_exists( 'ReturnedPaymentCount', $this->getChangedColumns() ) ) { $strSql .= ' returned_payment_count = ' . $this->sqlReturnedPaymentCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_balance = ' . $this->sqlMoveOutBalance(). ',' ; } elseif( true == array_key_exists( 'MoveOutBalance', $this->getChangedColumns() ) ) { $strSql .= ' move_out_balance = ' . $this->sqlMoveOutBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' open_ledger_balance = ' . $this->sqlOpenLedgerBalance(). ',' ; } elseif( true == array_key_exists( 'OpenLedgerBalance', $this->getChangedColumns() ) ) { $strSql .= ' open_ledger_balance = ' . $this->sqlOpenLedgerBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bad_debt_write_off_balance = ' . $this->sqlBadDebtWriteOffBalance(). ',' ; } elseif( true == array_key_exists( 'BadDebtWriteOffBalance', $this->getChangedColumns() ) ) { $strSql .= ' bad_debt_write_off_balance = ' . $this->sqlBadDebtWriteOffBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rv_rent = ' . $this->sqlRvRent(). ',' ; } elseif( true == array_key_exists( 'RvRent', $this->getChangedColumns() ) ) { $strSql .= ' rv_rent = ' . $this->sqlRvRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit = ' . $this->sqlDeposit(). ',' ; } elseif( true == array_key_exists( 'Deposit', $this->getChangedColumns() ) ) { $strSql .= ' deposit = ' . $this->sqlDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rv_total_household_income = ' . $this->sqlRvTotalHouseholdIncome(). ',' ; } elseif( true == array_key_exists( 'RvTotalHouseholdIncome', $this->getChangedColumns() ) ) { $strSql .= ' rv_total_household_income = ' . $this->sqlRvTotalHouseholdIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rv_guarantor_income = ' . $this->sqlRvGuarantorIncome(). ',' ; } elseif( true == array_key_exists( 'RvGuarantorIncome', $this->getChangedColumns() ) ) { $strSql .= ' rv_guarantor_income = ' . $this->sqlRvGuarantorIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' risk_premium_rent = ' . $this->sqlRiskPremiumRent(). ',' ; } elseif( true == array_key_exists( 'RiskPremiumRent', $this->getChangedColumns() ) ) { $strSql .= ' risk_premium_rent = ' . $this->sqlRiskPremiumRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' risk_premium_deposit = ' . $this->sqlRiskPremiumDeposit(). ',' ; } elseif( true == array_key_exists( 'RiskPremiumDeposit', $this->getChangedColumns() ) ) { $strSql .= ' risk_premium_deposit = ' . $this->sqlRiskPremiumDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_fee = ' . $this->sqlLateFee(). ',' ; } elseif( true == array_key_exists( 'LateFee', $this->getChangedColumns() ) ) { $strSql .= ' late_fee = ' . $this->sqlLateFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_guarantor = ' . $this->sqlHasGuarantor(). ',' ; } elseif( true == array_key_exists( 'HasGuarantor', $this->getChangedColumns() ) ) { $strSql .= ' has_guarantor = ' . $this->sqlHasGuarantor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_skips = ' . $this->sqlHasSkips(). ',' ; } elseif( true == array_key_exists( 'HasSkips', $this->getChangedColumns() ) ) { $strSql .= ' has_skips = ' . $this->sqlHasSkips() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_evictions = ' . $this->sqlHasEvictions(). ',' ; } elseif( true == array_key_exists( 'HasEvictions', $this->getChangedColumns() ) ) { $strSql .= ' has_evictions = ' . $this->sqlHasEvictions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_delinquency = ' . $this->sqlHasDelinquency(). ',' ; } elseif( true == array_key_exists( 'HasDelinquency', $this->getChangedColumns() ) ) { $strSql .= ' has_delinquency = ' . $this->sqlHasDelinquency() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_current_lease = ' . $this->sqlIsCurrentLease(). ',' ; } elseif( true == array_key_exists( 'IsCurrentLease', $this->getChangedColumns() ) ) { $strSql .= ' is_current_lease = ' . $this->sqlIsCurrentLease() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_entrata_core = ' . $this->sqlIsEntrataCore(). ',' ; } elseif( true == array_key_exists( 'IsEntrataCore', $this->getChangedColumns() ) ) { $strSql .= ' is_entrata_core = ' . $this->sqlIsEntrataCore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_transfered = ' . $this->sqlIsTransfered(). ',' ; } elseif( true == array_key_exists( 'IsTransfered', $this->getChangedColumns() ) ) { $strSql .= ' is_transfered = ' . $this->sqlIsTransfered() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_postal_code = ' . $this->sqlPropertyPostalCode(). ',' ; } elseif( true == array_key_exists( 'PropertyPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' property_postal_code = ' . $this->sqlPropertyPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_state_code = ' . $this->sqlPropertyStateCode(). ',' ; } elseif( true == array_key_exists( 'PropertyStateCode', $this->getChangedColumns() ) ) { $strSql .= ' property_state_code = ' . $this->sqlPropertyStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_type = ' . $this->sqlLeaseIntervalType(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalType', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_type = ' . $this->sqlLeaseIntervalType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate(). ',' ; } elseif( true == array_key_exists( 'LeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate(). ',' ; } elseif( true == array_key_exists( 'LeaseEndDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate(). ',' ; } elseif( true == array_key_exists( 'MoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate(). ',' ; } elseif( true == array_key_exists( 'MoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent = ' . $this->sqlRent(). ',' ; } elseif( true == array_key_exists( 'Rent', $this->getChangedColumns() ) ) { $strSql .= ' rent = ' . $this->sqlRent() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_type_id' => $this->getPropertyTypeId(),
			'lease_id' => $this->getLeaseId(),
			'lease_interval_id' => $this->getLeaseIntervalId(),
			'application_id' => $this->getApplicationId(),
			'screening_id' => $this->getScreeningId(),
			'screening_recommendation_type_id' => $this->getScreeningRecommendationTypeId(),
			'screening_decision_type_id' => $this->getScreeningDecisionTypeId(),
			'primary_customer_id' => $this->getPrimaryCustomerId(),
			'lease_renewal_count' => $this->getLeaseRenewalCount(),
			'late_payment_count' => $this->getLatePaymentCount(),
			'returned_payment_count' => $this->getReturnedPaymentCount(),
			'move_out_balance' => $this->getMoveOutBalance(),
			'open_ledger_balance' => $this->getOpenLedgerBalance(),
			'bad_debt_write_off_balance' => $this->getBadDebtWriteOffBalance(),
			'rv_rent' => $this->getRvRent(),
			'deposit' => $this->getDeposit(),
			'rv_total_household_income' => $this->getRvTotalHouseholdIncome(),
			'rv_guarantor_income' => $this->getRvGuarantorIncome(),
			'risk_premium_rent' => $this->getRiskPremiumRent(),
			'risk_premium_deposit' => $this->getRiskPremiumDeposit(),
			'late_fee' => $this->getLateFee(),
			'has_guarantor' => $this->getHasGuarantor(),
			'has_skips' => $this->getHasSkips(),
			'has_evictions' => $this->getHasEvictions(),
			'has_delinquency' => $this->getHasDelinquency(),
			'is_current_lease' => $this->getIsCurrentLease(),
			'is_entrata_core' => $this->getIsEntrataCore(),
			'is_transfered' => $this->getIsTransfered(),
			'property_postal_code' => $this->getPropertyPostalCode(),
			'property_state_code' => $this->getPropertyStateCode(),
			'lease_interval_type' => $this->getLeaseIntervalType(),
			'lease_start_date' => $this->getLeaseStartDate(),
			'lease_end_date' => $this->getLeaseEndDate(),
			'move_in_date' => $this->getMoveInDate(),
			'move_out_date' => $this->getMoveOutDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'rent' => $this->getRent()
		);
	}

}
?>