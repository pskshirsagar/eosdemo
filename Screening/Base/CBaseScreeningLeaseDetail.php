<?php

class CBaseScreeningLeaseDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_lease_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyTypeId;
	protected $m_intLeaseId;
	protected $m_intLeaseIntervalId;
	protected $m_intApplicationId;
	protected $m_intScreeningId;
	protected $m_intScreeningApplicantId;
	protected $m_intScreeningApplicantTypeId;
	protected $m_fltRent;
	protected $m_fltGuarantorIncome;
	protected $m_fltTotalHouseholdIncome;
	protected $m_fltMoveOutBalance;
	protected $m_fltOpenLedgerBalance;
	protected $m_fltBadDebtWriteOffBalance;
	protected $m_fltDeposit;
	protected $m_fltLateFee;
	protected $m_boolHasSkips;
	protected $m_boolHasEvictions;
	protected $m_boolHasDelinquency;
	protected $m_boolIsProcessed;
	protected $m_boolIsEntrataCore;
	protected $m_boolIsCurrentLease;
	protected $m_strLeaseIntervalType;
	protected $m_strMoveInDate;
	protected $m_strMoveOutDate;
	protected $m_strLeaseStartDate;
	protected $m_strLeaseEndDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolHasSkips = false;
		$this->m_boolHasEvictions = false;
		$this->m_boolHasDelinquency = false;
		$this->m_boolIsProcessed = false;
		$this->m_boolIsEntrataCore = false;
		$this->m_boolIsCurrentLease = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyTypeId', trim( $arrValues['property_type_id'] ) ); elseif( isset( $arrValues['property_type_id'] ) ) $this->setPropertyTypeId( $arrValues['property_type_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['lease_interval_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalId', trim( $arrValues['lease_interval_id'] ) ); elseif( isset( $arrValues['lease_interval_id'] ) ) $this->setLeaseIntervalId( $arrValues['lease_interval_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['screening_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningId', trim( $arrValues['screening_id'] ) ); elseif( isset( $arrValues['screening_id'] ) ) $this->setScreeningId( $arrValues['screening_id'] );
		if( isset( $arrValues['screening_applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicantId', trim( $arrValues['screening_applicant_id'] ) ); elseif( isset( $arrValues['screening_applicant_id'] ) ) $this->setScreeningApplicantId( $arrValues['screening_applicant_id'] );
		if( isset( $arrValues['screening_applicant_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicantTypeId', trim( $arrValues['screening_applicant_type_id'] ) ); elseif( isset( $arrValues['screening_applicant_type_id'] ) ) $this->setScreeningApplicantTypeId( $arrValues['screening_applicant_type_id'] );
		if( isset( $arrValues['rent'] ) && $boolDirectSet ) $this->set( 'm_fltRent', trim( $arrValues['rent'] ) ); elseif( isset( $arrValues['rent'] ) ) $this->setRent( $arrValues['rent'] );
		if( isset( $arrValues['guarantor_income'] ) && $boolDirectSet ) $this->set( 'm_fltGuarantorIncome', trim( $arrValues['guarantor_income'] ) ); elseif( isset( $arrValues['guarantor_income'] ) ) $this->setGuarantorIncome( $arrValues['guarantor_income'] );
		if( isset( $arrValues['total_household_income'] ) && $boolDirectSet ) $this->set( 'm_fltTotalHouseholdIncome', trim( $arrValues['total_household_income'] ) ); elseif( isset( $arrValues['total_household_income'] ) ) $this->setTotalHouseholdIncome( $arrValues['total_household_income'] );
		if( isset( $arrValues['move_out_balance'] ) && $boolDirectSet ) $this->set( 'm_fltMoveOutBalance', trim( $arrValues['move_out_balance'] ) ); elseif( isset( $arrValues['move_out_balance'] ) ) $this->setMoveOutBalance( $arrValues['move_out_balance'] );
		if( isset( $arrValues['open_ledger_balance'] ) && $boolDirectSet ) $this->set( 'm_fltOpenLedgerBalance', trim( $arrValues['open_ledger_balance'] ) ); elseif( isset( $arrValues['open_ledger_balance'] ) ) $this->setOpenLedgerBalance( $arrValues['open_ledger_balance'] );
		if( isset( $arrValues['bad_debt_write_off_balance'] ) && $boolDirectSet ) $this->set( 'm_fltBadDebtWriteOffBalance', trim( $arrValues['bad_debt_write_off_balance'] ) ); elseif( isset( $arrValues['bad_debt_write_off_balance'] ) ) $this->setBadDebtWriteOffBalance( $arrValues['bad_debt_write_off_balance'] );
		if( isset( $arrValues['deposit'] ) && $boolDirectSet ) $this->set( 'm_fltDeposit', trim( $arrValues['deposit'] ) ); elseif( isset( $arrValues['deposit'] ) ) $this->setDeposit( $arrValues['deposit'] );
		if( isset( $arrValues['late_fee'] ) && $boolDirectSet ) $this->set( 'm_fltLateFee', trim( $arrValues['late_fee'] ) ); elseif( isset( $arrValues['late_fee'] ) ) $this->setLateFee( $arrValues['late_fee'] );
		if( isset( $arrValues['has_skips'] ) && $boolDirectSet ) $this->set( 'm_boolHasSkips', trim( stripcslashes( $arrValues['has_skips'] ) ) ); elseif( isset( $arrValues['has_skips'] ) ) $this->setHasSkips( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_skips'] ) : $arrValues['has_skips'] );
		if( isset( $arrValues['has_evictions'] ) && $boolDirectSet ) $this->set( 'm_boolHasEvictions', trim( stripcslashes( $arrValues['has_evictions'] ) ) ); elseif( isset( $arrValues['has_evictions'] ) ) $this->setHasEvictions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_evictions'] ) : $arrValues['has_evictions'] );
		if( isset( $arrValues['has_delinquency'] ) && $boolDirectSet ) $this->set( 'm_boolHasDelinquency', trim( stripcslashes( $arrValues['has_delinquency'] ) ) ); elseif( isset( $arrValues['has_delinquency'] ) ) $this->setHasDelinquency( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_delinquency'] ) : $arrValues['has_delinquency'] );
		if( isset( $arrValues['is_processed'] ) && $boolDirectSet ) $this->set( 'm_boolIsProcessed', trim( stripcslashes( $arrValues['is_processed'] ) ) ); elseif( isset( $arrValues['is_processed'] ) ) $this->setIsProcessed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_processed'] ) : $arrValues['is_processed'] );
		if( isset( $arrValues['is_entrata_core'] ) && $boolDirectSet ) $this->set( 'm_boolIsEntrataCore', trim( stripcslashes( $arrValues['is_entrata_core'] ) ) ); elseif( isset( $arrValues['is_entrata_core'] ) ) $this->setIsEntrataCore( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_entrata_core'] ) : $arrValues['is_entrata_core'] );
		if( isset( $arrValues['is_current_lease'] ) && $boolDirectSet ) $this->set( 'm_boolIsCurrentLease', trim( stripcslashes( $arrValues['is_current_lease'] ) ) ); elseif( isset( $arrValues['is_current_lease'] ) ) $this->setIsCurrentLease( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_current_lease'] ) : $arrValues['is_current_lease'] );
		if( isset( $arrValues['lease_interval_type'] ) && $boolDirectSet ) $this->set( 'm_strLeaseIntervalType', trim( $arrValues['lease_interval_type'] ) ); elseif( isset( $arrValues['lease_interval_type'] ) ) $this->setLeaseIntervalType( $arrValues['lease_interval_type'] );
		if( isset( $arrValues['move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDate', trim( $arrValues['move_in_date'] ) ); elseif( isset( $arrValues['move_in_date'] ) ) $this->setMoveInDate( $arrValues['move_in_date'] );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartDate', trim( $arrValues['lease_start_date'] ) ); elseif( isset( $arrValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrValues['lease_start_date'] );
		if( isset( $arrValues['lease_end_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseEndDate', trim( $arrValues['lease_end_date'] ) ); elseif( isset( $arrValues['lease_end_date'] ) ) $this->setLeaseEndDate( $arrValues['lease_end_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyTypeId( $intPropertyTypeId ) {
		$this->set( 'm_intPropertyTypeId', CStrings::strToIntDef( $intPropertyTypeId, NULL, false ) );
	}

	public function getPropertyTypeId() {
		return $this->m_intPropertyTypeId;
	}

	public function sqlPropertyTypeId() {
		return ( true == isset( $this->m_intPropertyTypeId ) ) ? ( string ) $this->m_intPropertyTypeId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setLeaseIntervalId( $intLeaseIntervalId ) {
		$this->set( 'm_intLeaseIntervalId', CStrings::strToIntDef( $intLeaseIntervalId, NULL, false ) );
	}

	public function getLeaseIntervalId() {
		return $this->m_intLeaseIntervalId;
	}

	public function sqlLeaseIntervalId() {
		return ( true == isset( $this->m_intLeaseIntervalId ) ) ? ( string ) $this->m_intLeaseIntervalId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setScreeningId( $intScreeningId ) {
		$this->set( 'm_intScreeningId', CStrings::strToIntDef( $intScreeningId, NULL, false ) );
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function sqlScreeningId() {
		return ( true == isset( $this->m_intScreeningId ) ) ? ( string ) $this->m_intScreeningId : 'NULL';
	}

	public function setScreeningApplicantId( $intScreeningApplicantId ) {
		$this->set( 'm_intScreeningApplicantId', CStrings::strToIntDef( $intScreeningApplicantId, NULL, false ) );
	}

	public function getScreeningApplicantId() {
		return $this->m_intScreeningApplicantId;
	}

	public function sqlScreeningApplicantId() {
		return ( true == isset( $this->m_intScreeningApplicantId ) ) ? ( string ) $this->m_intScreeningApplicantId : 'NULL';
	}

	public function setScreeningApplicantTypeId( $intScreeningApplicantTypeId ) {
		$this->set( 'm_intScreeningApplicantTypeId', CStrings::strToIntDef( $intScreeningApplicantTypeId, NULL, false ) );
	}

	public function getScreeningApplicantTypeId() {
		return $this->m_intScreeningApplicantTypeId;
	}

	public function sqlScreeningApplicantTypeId() {
		return ( true == isset( $this->m_intScreeningApplicantTypeId ) ) ? ( string ) $this->m_intScreeningApplicantTypeId : 'NULL';
	}

	public function setRent( $fltRent ) {
		$this->set( 'm_fltRent', CStrings::strToFloatDef( $fltRent, NULL, false, 2 ) );
	}

	public function getRent() {
		return $this->m_fltRent;
	}

	public function sqlRent() {
		return ( true == isset( $this->m_fltRent ) ) ? ( string ) $this->m_fltRent : 'NULL';
	}

	public function setGuarantorIncome( $fltGuarantorIncome ) {
		$this->set( 'm_fltGuarantorIncome', CStrings::strToFloatDef( $fltGuarantorIncome, NULL, false, 2 ) );
	}

	public function getGuarantorIncome() {
		return $this->m_fltGuarantorIncome;
	}

	public function sqlGuarantorIncome() {
		return ( true == isset( $this->m_fltGuarantorIncome ) ) ? ( string ) $this->m_fltGuarantorIncome : 'NULL';
	}

	public function setTotalHouseholdIncome( $fltTotalHouseholdIncome ) {
		$this->set( 'm_fltTotalHouseholdIncome', CStrings::strToFloatDef( $fltTotalHouseholdIncome, NULL, false, 2 ) );
	}

	public function getTotalHouseholdIncome() {
		return $this->m_fltTotalHouseholdIncome;
	}

	public function sqlTotalHouseholdIncome() {
		return ( true == isset( $this->m_fltTotalHouseholdIncome ) ) ? ( string ) $this->m_fltTotalHouseholdIncome : 'NULL';
	}

	public function setMoveOutBalance( $fltMoveOutBalance ) {
		$this->set( 'm_fltMoveOutBalance', CStrings::strToFloatDef( $fltMoveOutBalance, NULL, false, 2 ) );
	}

	public function getMoveOutBalance() {
		return $this->m_fltMoveOutBalance;
	}

	public function sqlMoveOutBalance() {
		return ( true == isset( $this->m_fltMoveOutBalance ) ) ? ( string ) $this->m_fltMoveOutBalance : 'NULL';
	}

	public function setOpenLedgerBalance( $fltOpenLedgerBalance ) {
		$this->set( 'm_fltOpenLedgerBalance', CStrings::strToFloatDef( $fltOpenLedgerBalance, NULL, false, 2 ) );
	}

	public function getOpenLedgerBalance() {
		return $this->m_fltOpenLedgerBalance;
	}

	public function sqlOpenLedgerBalance() {
		return ( true == isset( $this->m_fltOpenLedgerBalance ) ) ? ( string ) $this->m_fltOpenLedgerBalance : 'NULL';
	}

	public function setBadDebtWriteOffBalance( $fltBadDebtWriteOffBalance ) {
		$this->set( 'm_fltBadDebtWriteOffBalance', CStrings::strToFloatDef( $fltBadDebtWriteOffBalance, NULL, false, 2 ) );
	}

	public function getBadDebtWriteOffBalance() {
		return $this->m_fltBadDebtWriteOffBalance;
	}

	public function sqlBadDebtWriteOffBalance() {
		return ( true == isset( $this->m_fltBadDebtWriteOffBalance ) ) ? ( string ) $this->m_fltBadDebtWriteOffBalance : 'NULL';
	}

	public function setDeposit( $fltDeposit ) {
		$this->set( 'm_fltDeposit', CStrings::strToFloatDef( $fltDeposit, NULL, false, 2 ) );
	}

	public function getDeposit() {
		return $this->m_fltDeposit;
	}

	public function sqlDeposit() {
		return ( true == isset( $this->m_fltDeposit ) ) ? ( string ) $this->m_fltDeposit : 'NULL';
	}

	public function setLateFee( $fltLateFee ) {
		$this->set( 'm_fltLateFee', CStrings::strToFloatDef( $fltLateFee, NULL, false, 2 ) );
	}

	public function getLateFee() {
		return $this->m_fltLateFee;
	}

	public function sqlLateFee() {
		return ( true == isset( $this->m_fltLateFee ) ) ? ( string ) $this->m_fltLateFee : 'NULL';
	}

	public function setHasSkips( $boolHasSkips ) {
		$this->set( 'm_boolHasSkips', CStrings::strToBool( $boolHasSkips ) );
	}

	public function getHasSkips() {
		return $this->m_boolHasSkips;
	}

	public function sqlHasSkips() {
		return ( true == isset( $this->m_boolHasSkips ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasSkips ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasEvictions( $boolHasEvictions ) {
		$this->set( 'm_boolHasEvictions', CStrings::strToBool( $boolHasEvictions ) );
	}

	public function getHasEvictions() {
		return $this->m_boolHasEvictions;
	}

	public function sqlHasEvictions() {
		return ( true == isset( $this->m_boolHasEvictions ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasEvictions ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasDelinquency( $boolHasDelinquency ) {
		$this->set( 'm_boolHasDelinquency', CStrings::strToBool( $boolHasDelinquency ) );
	}

	public function getHasDelinquency() {
		return $this->m_boolHasDelinquency;
	}

	public function sqlHasDelinquency() {
		return ( true == isset( $this->m_boolHasDelinquency ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasDelinquency ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsProcessed( $boolIsProcessed ) {
		$this->set( 'm_boolIsProcessed', CStrings::strToBool( $boolIsProcessed ) );
	}

	public function getIsProcessed() {
		return $this->m_boolIsProcessed;
	}

	public function sqlIsProcessed() {
		return ( true == isset( $this->m_boolIsProcessed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsProcessed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsEntrataCore( $boolIsEntrataCore ) {
		$this->set( 'm_boolIsEntrataCore', CStrings::strToBool( $boolIsEntrataCore ) );
	}

	public function getIsEntrataCore() {
		return $this->m_boolIsEntrataCore;
	}

	public function sqlIsEntrataCore() {
		return ( true == isset( $this->m_boolIsEntrataCore ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEntrataCore ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCurrentLease( $boolIsCurrentLease ) {
		$this->set( 'm_boolIsCurrentLease', CStrings::strToBool( $boolIsCurrentLease ) );
	}

	public function getIsCurrentLease() {
		return $this->m_boolIsCurrentLease;
	}

	public function sqlIsCurrentLease() {
		return ( true == isset( $this->m_boolIsCurrentLease ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCurrentLease ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLeaseIntervalType( $strLeaseIntervalType ) {
		$this->set( 'm_strLeaseIntervalType', CStrings::strTrimDef( $strLeaseIntervalType, -1, NULL, true ) );
	}

	public function getLeaseIntervalType() {
		return $this->m_strLeaseIntervalType;
	}

	public function sqlLeaseIntervalType() {
		return ( true == isset( $this->m_strLeaseIntervalType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeaseIntervalType ) : '\'' . addslashes( $this->m_strLeaseIntervalType ) . '\'' ) : 'NULL';
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->set( 'm_strMoveInDate', CStrings::strTrimDef( $strMoveInDate, -1, NULL, true ) );
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function sqlMoveInDate() {
		return ( true == isset( $this->m_strMoveInDate ) ) ? '\'' . $this->m_strMoveInDate . '\'' : 'NULL';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->set( 'm_strLeaseStartDate', CStrings::strTrimDef( $strLeaseStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function sqlLeaseStartDate() {
		return ( true == isset( $this->m_strLeaseStartDate ) ) ? '\'' . $this->m_strLeaseStartDate . '\'' : 'NULL';
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->set( 'm_strLeaseEndDate', CStrings::strTrimDef( $strLeaseEndDate, -1, NULL, true ) );
	}

	public function getLeaseEndDate() {
		return $this->m_strLeaseEndDate;
	}

	public function sqlLeaseEndDate() {
		return ( true == isset( $this->m_strLeaseEndDate ) ) ? '\'' . $this->m_strLeaseEndDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_type_id, lease_id, lease_interval_id, application_id, screening_id, screening_applicant_id, screening_applicant_type_id, rent, guarantor_income, total_household_income, move_out_balance, open_ledger_balance, bad_debt_write_off_balance, deposit, late_fee, has_skips, has_evictions, has_delinquency, is_processed, is_entrata_core, is_current_lease, lease_interval_type, move_in_date, move_out_date, lease_start_date, lease_end_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyTypeId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlLeaseIntervalId() . ', ' .
						$this->sqlApplicationId() . ', ' .
						$this->sqlScreeningId() . ', ' .
						$this->sqlScreeningApplicantId() . ', ' .
						$this->sqlScreeningApplicantTypeId() . ', ' .
						$this->sqlRent() . ', ' .
						$this->sqlGuarantorIncome() . ', ' .
						$this->sqlTotalHouseholdIncome() . ', ' .
						$this->sqlMoveOutBalance() . ', ' .
						$this->sqlOpenLedgerBalance() . ', ' .
						$this->sqlBadDebtWriteOffBalance() . ', ' .
						$this->sqlDeposit() . ', ' .
						$this->sqlLateFee() . ', ' .
						$this->sqlHasSkips() . ', ' .
						$this->sqlHasEvictions() . ', ' .
						$this->sqlHasDelinquency() . ', ' .
						$this->sqlIsProcessed() . ', ' .
						$this->sqlIsEntrataCore() . ', ' .
						$this->sqlIsCurrentLease() . ', ' .
						$this->sqlLeaseIntervalType() . ', ' .
						$this->sqlMoveInDate() . ', ' .
						$this->sqlMoveOutDate() . ', ' .
						$this->sqlLeaseStartDate() . ', ' .
						$this->sqlLeaseEndDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_type_id = ' . $this->sqlPropertyTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_type_id = ' . $this->sqlPropertyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalId', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId(). ',' ; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId(). ',' ; } elseif( true == array_key_exists( 'ScreeningId', $this->getChangedColumns() ) ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId(). ',' ; } elseif( true == array_key_exists( 'ScreeningApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_type_id = ' . $this->sqlScreeningApplicantTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningApplicantTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_type_id = ' . $this->sqlScreeningApplicantTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent = ' . $this->sqlRent(). ',' ; } elseif( true == array_key_exists( 'Rent', $this->getChangedColumns() ) ) { $strSql .= ' rent = ' . $this->sqlRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' guarantor_income = ' . $this->sqlGuarantorIncome(). ',' ; } elseif( true == array_key_exists( 'GuarantorIncome', $this->getChangedColumns() ) ) { $strSql .= ' guarantor_income = ' . $this->sqlGuarantorIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_household_income = ' . $this->sqlTotalHouseholdIncome(). ',' ; } elseif( true == array_key_exists( 'TotalHouseholdIncome', $this->getChangedColumns() ) ) { $strSql .= ' total_household_income = ' . $this->sqlTotalHouseholdIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_balance = ' . $this->sqlMoveOutBalance(). ',' ; } elseif( true == array_key_exists( 'MoveOutBalance', $this->getChangedColumns() ) ) { $strSql .= ' move_out_balance = ' . $this->sqlMoveOutBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' open_ledger_balance = ' . $this->sqlOpenLedgerBalance(). ',' ; } elseif( true == array_key_exists( 'OpenLedgerBalance', $this->getChangedColumns() ) ) { $strSql .= ' open_ledger_balance = ' . $this->sqlOpenLedgerBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bad_debt_write_off_balance = ' . $this->sqlBadDebtWriteOffBalance(). ',' ; } elseif( true == array_key_exists( 'BadDebtWriteOffBalance', $this->getChangedColumns() ) ) { $strSql .= ' bad_debt_write_off_balance = ' . $this->sqlBadDebtWriteOffBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit = ' . $this->sqlDeposit(). ',' ; } elseif( true == array_key_exists( 'Deposit', $this->getChangedColumns() ) ) { $strSql .= ' deposit = ' . $this->sqlDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_fee = ' . $this->sqlLateFee(). ',' ; } elseif( true == array_key_exists( 'LateFee', $this->getChangedColumns() ) ) { $strSql .= ' late_fee = ' . $this->sqlLateFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_skips = ' . $this->sqlHasSkips(). ',' ; } elseif( true == array_key_exists( 'HasSkips', $this->getChangedColumns() ) ) { $strSql .= ' has_skips = ' . $this->sqlHasSkips() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_evictions = ' . $this->sqlHasEvictions(). ',' ; } elseif( true == array_key_exists( 'HasEvictions', $this->getChangedColumns() ) ) { $strSql .= ' has_evictions = ' . $this->sqlHasEvictions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_delinquency = ' . $this->sqlHasDelinquency(). ',' ; } elseif( true == array_key_exists( 'HasDelinquency', $this->getChangedColumns() ) ) { $strSql .= ' has_delinquency = ' . $this->sqlHasDelinquency() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_processed = ' . $this->sqlIsProcessed(). ',' ; } elseif( true == array_key_exists( 'IsProcessed', $this->getChangedColumns() ) ) { $strSql .= ' is_processed = ' . $this->sqlIsProcessed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_entrata_core = ' . $this->sqlIsEntrataCore(). ',' ; } elseif( true == array_key_exists( 'IsEntrataCore', $this->getChangedColumns() ) ) { $strSql .= ' is_entrata_core = ' . $this->sqlIsEntrataCore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_current_lease = ' . $this->sqlIsCurrentLease(). ',' ; } elseif( true == array_key_exists( 'IsCurrentLease', $this->getChangedColumns() ) ) { $strSql .= ' is_current_lease = ' . $this->sqlIsCurrentLease() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_type = ' . $this->sqlLeaseIntervalType(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalType', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_type = ' . $this->sqlLeaseIntervalType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate(). ',' ; } elseif( true == array_key_exists( 'MoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate(). ',' ; } elseif( true == array_key_exists( 'MoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate(). ',' ; } elseif( true == array_key_exists( 'LeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate(). ',' ; } elseif( true == array_key_exists( 'LeaseEndDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_type_id' => $this->getPropertyTypeId(),
			'lease_id' => $this->getLeaseId(),
			'lease_interval_id' => $this->getLeaseIntervalId(),
			'application_id' => $this->getApplicationId(),
			'screening_id' => $this->getScreeningId(),
			'screening_applicant_id' => $this->getScreeningApplicantId(),
			'screening_applicant_type_id' => $this->getScreeningApplicantTypeId(),
			'rent' => $this->getRent(),
			'guarantor_income' => $this->getGuarantorIncome(),
			'total_household_income' => $this->getTotalHouseholdIncome(),
			'move_out_balance' => $this->getMoveOutBalance(),
			'open_ledger_balance' => $this->getOpenLedgerBalance(),
			'bad_debt_write_off_balance' => $this->getBadDebtWriteOffBalance(),
			'deposit' => $this->getDeposit(),
			'late_fee' => $this->getLateFee(),
			'has_skips' => $this->getHasSkips(),
			'has_evictions' => $this->getHasEvictions(),
			'has_delinquency' => $this->getHasDelinquency(),
			'is_processed' => $this->getIsProcessed(),
			'is_entrata_core' => $this->getIsEntrataCore(),
			'is_current_lease' => $this->getIsCurrentLease(),
			'lease_interval_type' => $this->getLeaseIntervalType(),
			'move_in_date' => $this->getMoveInDate(),
			'move_out_date' => $this->getMoveOutDate(),
			'lease_start_date' => $this->getLeaseStartDate(),
			'lease_end_date' => $this->getLeaseEndDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>