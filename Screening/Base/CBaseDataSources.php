<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDataSources
 * Do not add any new functions to this class.
 */

class CBaseDataSources extends CEosPluralBase {

	/**
	 * @return CDataSource[]
	 */
	public static function fetchDataSources( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDataSource', $objDatabase );
	}

	/**
	 * @return CDataSource
	 */
	public static function fetchDataSource( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDataSource', $objDatabase );
	}

	public static function fetchDataSourceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'data_sources', $objDatabase );
	}

	public static function fetchDataSourceById( $intId, $objDatabase ) {
		return self::fetchDataSource( sprintf( 'SELECT * FROM data_sources WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>