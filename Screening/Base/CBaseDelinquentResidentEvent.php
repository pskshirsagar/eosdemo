<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDelinquentResidentEvent extends CEosSingularBase {

	const TABLE_NAME = 'public.delinquent_resident_events';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intDelinquentResidentId;
	protected $m_intDelinquentEventTypeId;
	protected $m_intDelinquentEventStatusTypeId;
	protected $m_intDelinquentEventReasonId;
	protected $m_fltDelinquentAmount;
	protected $m_fltWriteoffAmount;
	protected $m_strDelinquentEventDate;
	protected $m_strLeaseStartDate;
	protected $m_strLeaseEndDate;
	protected $m_strMoveOutDate;
	protected $m_strNotes;
	protected $m_strConsumerStatement;
	protected $m_strLastPaidOn;
	protected $m_boolIsActive;
	protected $m_boolIsDnrRecord;
	protected $m_strDnrEffectiveOn;
	protected $m_intDeletedReasonId;
	protected $m_strDeletedNotes;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['delinquent_resident_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquentResidentId', trim( $arrValues['delinquent_resident_id'] ) ); elseif( isset( $arrValues['delinquent_resident_id'] ) ) $this->setDelinquentResidentId( $arrValues['delinquent_resident_id'] );
		if( isset( $arrValues['delinquent_event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquentEventTypeId', trim( $arrValues['delinquent_event_type_id'] ) ); elseif( isset( $arrValues['delinquent_event_type_id'] ) ) $this->setDelinquentEventTypeId( $arrValues['delinquent_event_type_id'] );
		if( isset( $arrValues['delinquent_event_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquentEventStatusTypeId', trim( $arrValues['delinquent_event_status_type_id'] ) ); elseif( isset( $arrValues['delinquent_event_status_type_id'] ) ) $this->setDelinquentEventStatusTypeId( $arrValues['delinquent_event_status_type_id'] );
		if( isset( $arrValues['delinquent_event_reason_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquentEventReasonId', trim( $arrValues['delinquent_event_reason_id'] ) ); elseif( isset( $arrValues['delinquent_event_reason_id'] ) ) $this->setDelinquentEventReasonId( $arrValues['delinquent_event_reason_id'] );
		if( isset( $arrValues['delinquent_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDelinquentAmount', trim( $arrValues['delinquent_amount'] ) ); elseif( isset( $arrValues['delinquent_amount'] ) ) $this->setDelinquentAmount( $arrValues['delinquent_amount'] );
		if( isset( $arrValues['writeoff_amount'] ) && $boolDirectSet ) $this->set( 'm_fltWriteoffAmount', trim( $arrValues['writeoff_amount'] ) ); elseif( isset( $arrValues['writeoff_amount'] ) ) $this->setWriteoffAmount( $arrValues['writeoff_amount'] );
		if( isset( $arrValues['delinquent_event_date'] ) && $boolDirectSet ) $this->set( 'm_strDelinquentEventDate', trim( $arrValues['delinquent_event_date'] ) ); elseif( isset( $arrValues['delinquent_event_date'] ) ) $this->setDelinquentEventDate( $arrValues['delinquent_event_date'] );
		if( isset( $arrValues['lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartDate', trim( $arrValues['lease_start_date'] ) ); elseif( isset( $arrValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrValues['lease_start_date'] );
		if( isset( $arrValues['lease_end_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseEndDate', trim( $arrValues['lease_end_date'] ) ); elseif( isset( $arrValues['lease_end_date'] ) ) $this->setLeaseEndDate( $arrValues['lease_end_date'] );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['consumer_statement'] ) && $boolDirectSet ) $this->set( 'm_strConsumerStatement', trim( stripcslashes( $arrValues['consumer_statement'] ) ) ); elseif( isset( $arrValues['consumer_statement'] ) ) $this->setConsumerStatement( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['consumer_statement'] ) : $arrValues['consumer_statement'] );
		if( isset( $arrValues['last_paid_on'] ) && $boolDirectSet ) $this->set( 'm_strLastPaidOn', trim( $arrValues['last_paid_on'] ) ); elseif( isset( $arrValues['last_paid_on'] ) ) $this->setLastPaidOn( $arrValues['last_paid_on'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['is_dnr_record'] ) && $boolDirectSet ) $this->set( 'm_boolIsDnrRecord', trim( stripcslashes( $arrValues['is_dnr_record'] ) ) ); elseif( isset( $arrValues['is_dnr_record'] ) ) $this->setIsDnrRecord( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_dnr_record'] ) : $arrValues['is_dnr_record'] );
		if( isset( $arrValues['dnr_effective_on'] ) && $boolDirectSet ) $this->set( 'm_strDnrEffectiveOn', trim( $arrValues['dnr_effective_on'] ) ); elseif( isset( $arrValues['dnr_effective_on'] ) ) $this->setDnrEffectiveOn( $arrValues['dnr_effective_on'] );
		if( isset( $arrValues['deleted_reason_id'] ) && $boolDirectSet ) $this->set( 'm_intDeletedReasonId', trim( $arrValues['deleted_reason_id'] ) ); elseif( isset( $arrValues['deleted_reason_id'] ) ) $this->setDeletedReasonId( $arrValues['deleted_reason_id'] );
		if( isset( $arrValues['deleted_notes'] ) && $boolDirectSet ) $this->set( 'm_strDeletedNotes', trim( stripcslashes( $arrValues['deleted_notes'] ) ) ); elseif( isset( $arrValues['deleted_notes'] ) ) $this->setDeletedNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['deleted_notes'] ) : $arrValues['deleted_notes'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setDelinquentResidentId( $intDelinquentResidentId ) {
		$this->set( 'm_intDelinquentResidentId', CStrings::strToIntDef( $intDelinquentResidentId, NULL, false ) );
	}

	public function getDelinquentResidentId() {
		return $this->m_intDelinquentResidentId;
	}

	public function sqlDelinquentResidentId() {
		return ( true == isset( $this->m_intDelinquentResidentId ) ) ? ( string ) $this->m_intDelinquentResidentId : 'NULL';
	}

	public function setDelinquentEventTypeId( $intDelinquentEventTypeId ) {
		$this->set( 'm_intDelinquentEventTypeId', CStrings::strToIntDef( $intDelinquentEventTypeId, NULL, false ) );
	}

	public function getDelinquentEventTypeId() {
		return $this->m_intDelinquentEventTypeId;
	}

	public function sqlDelinquentEventTypeId() {
		return ( true == isset( $this->m_intDelinquentEventTypeId ) ) ? ( string ) $this->m_intDelinquentEventTypeId : 'NULL';
	}

	public function setDelinquentEventStatusTypeId( $intDelinquentEventStatusTypeId ) {
		$this->set( 'm_intDelinquentEventStatusTypeId', CStrings::strToIntDef( $intDelinquentEventStatusTypeId, NULL, false ) );
	}

	public function getDelinquentEventStatusTypeId() {
		return $this->m_intDelinquentEventStatusTypeId;
	}

	public function sqlDelinquentEventStatusTypeId() {
		return ( true == isset( $this->m_intDelinquentEventStatusTypeId ) ) ? ( string ) $this->m_intDelinquentEventStatusTypeId : 'NULL';
	}

	public function setDelinquentEventReasonId( $intDelinquentEventReasonId ) {
		$this->set( 'm_intDelinquentEventReasonId', CStrings::strToIntDef( $intDelinquentEventReasonId, NULL, false ) );
	}

	public function getDelinquentEventReasonId() {
		return $this->m_intDelinquentEventReasonId;
	}

	public function sqlDelinquentEventReasonId() {
		return ( true == isset( $this->m_intDelinquentEventReasonId ) ) ? ( string ) $this->m_intDelinquentEventReasonId : 'NULL';
	}

	public function setDelinquentAmount( $fltDelinquentAmount ) {
		$this->set( 'm_fltDelinquentAmount', CStrings::strToFloatDef( $fltDelinquentAmount, NULL, false, 2 ) );
	}

	public function getDelinquentAmount() {
		return $this->m_fltDelinquentAmount;
	}

	public function sqlDelinquentAmount() {
		return ( true == isset( $this->m_fltDelinquentAmount ) ) ? ( string ) $this->m_fltDelinquentAmount : 'NULL';
	}

	public function setWriteoffAmount( $fltWriteoffAmount ) {
		$this->set( 'm_fltWriteoffAmount', CStrings::strToFloatDef( $fltWriteoffAmount, NULL, false, 2 ) );
	}

	public function getWriteoffAmount() {
		return $this->m_fltWriteoffAmount;
	}

	public function sqlWriteoffAmount() {
		return ( true == isset( $this->m_fltWriteoffAmount ) ) ? ( string ) $this->m_fltWriteoffAmount : 'NULL';
	}

	public function setDelinquentEventDate( $strDelinquentEventDate ) {
		$this->set( 'm_strDelinquentEventDate', CStrings::strTrimDef( $strDelinquentEventDate, -1, NULL, true ) );
	}

	public function getDelinquentEventDate() {
		return $this->m_strDelinquentEventDate;
	}

	public function sqlDelinquentEventDate() {
		return ( true == isset( $this->m_strDelinquentEventDate ) ) ? '\'' . $this->m_strDelinquentEventDate . '\'' : 'NOW()';
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->set( 'm_strLeaseStartDate', CStrings::strTrimDef( $strLeaseStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function sqlLeaseStartDate() {
		return ( true == isset( $this->m_strLeaseStartDate ) ) ? '\'' . $this->m_strLeaseStartDate . '\'' : 'NULL';
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->set( 'm_strLeaseEndDate', CStrings::strTrimDef( $strLeaseEndDate, -1, NULL, true ) );
	}

	public function getLeaseEndDate() {
		return $this->m_strLeaseEndDate;
	}

	public function sqlLeaseEndDate() {
		return ( true == isset( $this->m_strLeaseEndDate ) ) ? '\'' . $this->m_strLeaseEndDate . '\'' : 'NULL';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 200, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setConsumerStatement( $strConsumerStatement ) {
		$this->set( 'm_strConsumerStatement', CStrings::strTrimDef( $strConsumerStatement, 200, NULL, true ) );
	}

	public function getConsumerStatement() {
		return $this->m_strConsumerStatement;
	}

	public function sqlConsumerStatement() {
		return ( true == isset( $this->m_strConsumerStatement ) ) ? '\'' . addslashes( $this->m_strConsumerStatement ) . '\'' : 'NULL';
	}

	public function setLastPaidOn( $strLastPaidOn ) {
		$this->set( 'm_strLastPaidOn', CStrings::strTrimDef( $strLastPaidOn, -1, NULL, true ) );
	}

	public function getLastPaidOn() {
		return $this->m_strLastPaidOn;
	}

	public function sqlLastPaidOn() {
		return ( true == isset( $this->m_strLastPaidOn ) ) ? '\'' . $this->m_strLastPaidOn . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDnrRecord( $boolIsDnrRecord ) {
		$this->set( 'm_boolIsDnrRecord', CStrings::strToBool( $boolIsDnrRecord ) );
	}

	public function getIsDnrRecord() {
		return $this->m_boolIsDnrRecord;
	}

	public function sqlIsDnrRecord() {
		return ( true == isset( $this->m_boolIsDnrRecord ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDnrRecord ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDnrEffectiveOn( $strDnrEffectiveOn ) {
		$this->set( 'm_strDnrEffectiveOn', CStrings::strTrimDef( $strDnrEffectiveOn, -1, NULL, true ) );
	}

	public function getDnrEffectiveOn() {
		return $this->m_strDnrEffectiveOn;
	}

	public function sqlDnrEffectiveOn() {
		return ( true == isset( $this->m_strDnrEffectiveOn ) ) ? '\'' . $this->m_strDnrEffectiveOn . '\'' : 'NULL';
	}

	public function setDeletedReasonId( $intDeletedReasonId ) {
		$this->set( 'm_intDeletedReasonId', CStrings::strToIntDef( $intDeletedReasonId, NULL, false ) );
	}

	public function getDeletedReasonId() {
		return $this->m_intDeletedReasonId;
	}

	public function sqlDeletedReasonId() {
		return ( true == isset( $this->m_intDeletedReasonId ) ) ? ( string ) $this->m_intDeletedReasonId : 'NULL';
	}

	public function setDeletedNotes( $strDeletedNotes ) {
		$this->set( 'm_strDeletedNotes', CStrings::strTrimDef( $strDeletedNotes, 200, NULL, true ) );
	}

	public function getDeletedNotes() {
		return $this->m_strDeletedNotes;
	}

	public function sqlDeletedNotes() {
		return ( true == isset( $this->m_strDeletedNotes ) ) ? '\'' . addslashes( $this->m_strDeletedNotes ) . '\'' : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_id, delinquent_resident_id, delinquent_event_type_id, delinquent_event_status_type_id, delinquent_event_reason_id, delinquent_amount, writeoff_amount, delinquent_event_date, lease_start_date, lease_end_date, move_out_date, notes, consumer_statement, last_paid_on, is_active, is_dnr_record, dnr_effective_on, deleted_reason_id, deleted_notes, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlDelinquentResidentId() . ', ' .
 						$this->sqlDelinquentEventTypeId() . ', ' .
 						$this->sqlDelinquentEventStatusTypeId() . ', ' .
 						$this->sqlDelinquentEventReasonId() . ', ' .
 						$this->sqlDelinquentAmount() . ', ' .
 						$this->sqlWriteoffAmount() . ', ' .
 						$this->sqlDelinquentEventDate() . ', ' .
 						$this->sqlLeaseStartDate() . ', ' .
 						$this->sqlLeaseEndDate() . ', ' .
 						$this->sqlMoveOutDate() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlConsumerStatement() . ', ' .
 						$this->sqlLastPaidOn() . ', ' .
 						$this->sqlIsActive() . ', ' .
 						$this->sqlIsDnrRecord() . ', ' .
 						$this->sqlDnrEffectiveOn() . ', ' .
 						$this->sqlDeletedReasonId() . ', ' .
 						$this->sqlDeletedNotes() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquent_resident_id = ' . $this->sqlDelinquentResidentId() . ','; } elseif( true == array_key_exists( 'DelinquentResidentId', $this->getChangedColumns() ) ) { $strSql .= ' delinquent_resident_id = ' . $this->sqlDelinquentResidentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquent_event_type_id = ' . $this->sqlDelinquentEventTypeId() . ','; } elseif( true == array_key_exists( 'DelinquentEventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' delinquent_event_type_id = ' . $this->sqlDelinquentEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquent_event_status_type_id = ' . $this->sqlDelinquentEventStatusTypeId() . ','; } elseif( true == array_key_exists( 'DelinquentEventStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' delinquent_event_status_type_id = ' . $this->sqlDelinquentEventStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquent_event_reason_id = ' . $this->sqlDelinquentEventReasonId() . ','; } elseif( true == array_key_exists( 'DelinquentEventReasonId', $this->getChangedColumns() ) ) { $strSql .= ' delinquent_event_reason_id = ' . $this->sqlDelinquentEventReasonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquent_amount = ' . $this->sqlDelinquentAmount() . ','; } elseif( true == array_key_exists( 'DelinquentAmount', $this->getChangedColumns() ) ) { $strSql .= ' delinquent_amount = ' . $this->sqlDelinquentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' writeoff_amount = ' . $this->sqlWriteoffAmount() . ','; } elseif( true == array_key_exists( 'WriteoffAmount', $this->getChangedColumns() ) ) { $strSql .= ' writeoff_amount = ' . $this->sqlWriteoffAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquent_event_date = ' . $this->sqlDelinquentEventDate() . ','; } elseif( true == array_key_exists( 'DelinquentEventDate', $this->getChangedColumns() ) ) { $strSql .= ' delinquent_event_date = ' . $this->sqlDelinquentEventDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; } elseif( true == array_key_exists( 'LeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate() . ','; } elseif( true == array_key_exists( 'LeaseEndDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; } elseif( true == array_key_exists( 'MoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumer_statement = ' . $this->sqlConsumerStatement() . ','; } elseif( true == array_key_exists( 'ConsumerStatement', $this->getChangedColumns() ) ) { $strSql .= ' consumer_statement = ' . $this->sqlConsumerStatement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_paid_on = ' . $this->sqlLastPaidOn() . ','; } elseif( true == array_key_exists( 'LastPaidOn', $this->getChangedColumns() ) ) { $strSql .= ' last_paid_on = ' . $this->sqlLastPaidOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_dnr_record = ' . $this->sqlIsDnrRecord() . ','; } elseif( true == array_key_exists( 'IsDnrRecord', $this->getChangedColumns() ) ) { $strSql .= ' is_dnr_record = ' . $this->sqlIsDnrRecord() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dnr_effective_on = ' . $this->sqlDnrEffectiveOn() . ','; } elseif( true == array_key_exists( 'DnrEffectiveOn', $this->getChangedColumns() ) ) { $strSql .= ' dnr_effective_on = ' . $this->sqlDnrEffectiveOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_reason_id = ' . $this->sqlDeletedReasonId() . ','; } elseif( true == array_key_exists( 'DeletedReasonId', $this->getChangedColumns() ) ) { $strSql .= ' deleted_reason_id = ' . $this->sqlDeletedReasonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_notes = ' . $this->sqlDeletedNotes() . ','; } elseif( true == array_key_exists( 'DeletedNotes', $this->getChangedColumns() ) ) { $strSql .= ' deleted_notes = ' . $this->sqlDeletedNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lease_id' => $this->getLeaseId(),
			'delinquent_resident_id' => $this->getDelinquentResidentId(),
			'delinquent_event_type_id' => $this->getDelinquentEventTypeId(),
			'delinquent_event_status_type_id' => $this->getDelinquentEventStatusTypeId(),
			'delinquent_event_reason_id' => $this->getDelinquentEventReasonId(),
			'delinquent_amount' => $this->getDelinquentAmount(),
			'writeoff_amount' => $this->getWriteoffAmount(),
			'delinquent_event_date' => $this->getDelinquentEventDate(),
			'lease_start_date' => $this->getLeaseStartDate(),
			'lease_end_date' => $this->getLeaseEndDate(),
			'move_out_date' => $this->getMoveOutDate(),
			'notes' => $this->getNotes(),
			'consumer_statement' => $this->getConsumerStatement(),
			'last_paid_on' => $this->getLastPaidOn(),
			'is_active' => $this->getIsActive(),
			'is_dnr_record' => $this->getIsDnrRecord(),
			'dnr_effective_on' => $this->getDnrEffectiveOn(),
			'deleted_reason_id' => $this->getDeletedReasonId(),
			'deleted_notes' => $this->getDeletedNotes(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>