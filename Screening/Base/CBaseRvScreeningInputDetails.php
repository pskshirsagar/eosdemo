<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CRvScreeningInputDetails
 * Do not add any new functions to this class.
 */

class CBaseRvScreeningInputDetails extends CEosPluralBase {

	/**
	 * @return CRvScreeningInputDetail[]
	 */
	public static function fetchRvScreeningInputDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRvScreeningInputDetail::class, $objDatabase );
	}

	/**
	 * @return CRvScreeningInputDetail
	 */
	public static function fetchRvScreeningInputDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRvScreeningInputDetail::class, $objDatabase );
	}

	public static function fetchRvScreeningInputDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'rv_screening_input_details', $objDatabase );
	}

	public static function fetchRvScreeningInputDetailById( $intId, $objDatabase ) {
		return self::fetchRvScreeningInputDetail( sprintf( 'SELECT * FROM rv_screening_input_details WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchRvScreeningInputDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchRvScreeningInputDetails( sprintf( 'SELECT * FROM rv_screening_input_details WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>