<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CConsumerDocumentTypes
 * Do not add any new functions to this class.
 */

class CBaseConsumerDocumentTypes extends CEosPluralBase {

	/**
	 * @return CConsumerDocumentType[]
	 */
	public static function fetchConsumerDocumentTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CConsumerDocumentType', $objDatabase );
	}

	/**
	 * @return CConsumerDocumentType
	 */
	public static function fetchConsumerDocumentType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CConsumerDocumentType', $objDatabase );
	}

	public static function fetchConsumerDocumentTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'consumer_document_types', $objDatabase );
	}

	public static function fetchConsumerDocumentTypeById( $intId, $objDatabase ) {
		return self::fetchConsumerDocumentType( sprintf( 'SELECT * FROM consumer_document_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>