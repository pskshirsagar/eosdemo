<?php

class CBaseTimelineFilters extends CEosPluralBase {

    const TABLE_TIMELINE_FILTERS = 'public.timeline_filters';

    public static function fetchTimelineFilters( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CTimelineFilter', $objDatabase );
    }

    public static function fetchTimelineFilter( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CTimelineFilter', $objDatabase );
    }

    public static function fetchTimelineFilterCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'timeline_filters', $objDatabase );
    }

    public static function fetchTimelineFilterById( $intId, $objDatabase ) {
        return self::fetchTimelineFilter( sprintf( 'SELECT * FROM timeline_filters WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>