<?php

class CBaseCannedResponse extends CEosSingularBase {

	const TABLE_NAME = 'public.canned_responses';

	protected $m_intId;
	protected $m_intQuickResponseTypeId;
	protected $m_strCannedResponseName;
	protected $m_strCannedResponse;
	protected $m_boolIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['quick_response_type_id'] ) && $boolDirectSet ) $this->set( 'm_intQuickResponseTypeId', trim( $arrValues['quick_response_type_id'] ) ); elseif( isset( $arrValues['quick_response_type_id'] ) ) $this->setQuickResponseTypeId( $arrValues['quick_response_type_id'] );
		if( isset( $arrValues['canned_response_name'] ) && $boolDirectSet ) $this->set( 'm_strCannedResponseName', trim( stripcslashes( $arrValues['canned_response_name'] ) ) ); elseif( isset( $arrValues['canned_response_name'] ) ) $this->setCannedResponseName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['canned_response_name'] ) : $arrValues['canned_response_name'] );
		if( isset( $arrValues['canned_response'] ) && $boolDirectSet ) $this->set( 'm_strCannedResponse', trim( stripcslashes( $arrValues['canned_response'] ) ) ); elseif( isset( $arrValues['canned_response'] ) ) $this->setCannedResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['canned_response'] ) : $arrValues['canned_response'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setQuickResponseTypeId( $intQuickResponseTypeId ) {
		$this->set( 'm_intQuickResponseTypeId', CStrings::strToIntDef( $intQuickResponseTypeId, NULL, false ) );
	}

	public function getQuickResponseTypeId() {
		return $this->m_intQuickResponseTypeId;
	}

	public function sqlQuickResponseTypeId() {
		return ( true == isset( $this->m_intQuickResponseTypeId ) ) ? ( string ) $this->m_intQuickResponseTypeId : 'NULL';
	}

	public function setCannedResponseName( $strCannedResponseName ) {
		$this->set( 'm_strCannedResponseName', CStrings::strTrimDef( $strCannedResponseName, 100, NULL, true ) );
	}

	public function getCannedResponseName() {
		return $this->m_strCannedResponseName;
	}

	public function sqlCannedResponseName() {
		return ( true == isset( $this->m_strCannedResponseName ) ) ? '\'' . addslashes( $this->m_strCannedResponseName ) . '\'' : 'NULL';
	}

	public function setCannedResponse( $strCannedResponse ) {
		$this->set( 'm_strCannedResponse', CStrings::strTrimDef( $strCannedResponse, -1, NULL, true ) );
	}

	public function getCannedResponse() {
		return $this->m_strCannedResponse;
	}

	public function sqlCannedResponse() {
		return ( true == isset( $this->m_strCannedResponse ) ) ? '\'' . addslashes( $this->m_strCannedResponse ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, quick_response_type_id, canned_response_name, canned_response, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlQuickResponseTypeId() . ', ' .
						$this->sqlCannedResponseName() . ', ' .
						$this->sqlCannedResponse() . ', ' .
						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quick_response_type_id = ' . $this->sqlQuickResponseTypeId(). ',' ; } elseif( true == array_key_exists( 'QuickResponseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' quick_response_type_id = ' . $this->sqlQuickResponseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' canned_response_name = ' . $this->sqlCannedResponseName(). ',' ; } elseif( true == array_key_exists( 'CannedResponseName', $this->getChangedColumns() ) ) { $strSql .= ' canned_response_name = ' . $this->sqlCannedResponseName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' canned_response = ' . $this->sqlCannedResponse(). ',' ; } elseif( true == array_key_exists( 'CannedResponse', $this->getChangedColumns() ) ) { $strSql .= ' canned_response = ' . $this->sqlCannedResponse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'quick_response_type_id' => $this->getQuickResponseTypeId(),
			'canned_response_name' => $this->getCannedResponseName(),
			'canned_response' => $this->getCannedResponse(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>