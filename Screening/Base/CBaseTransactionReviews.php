<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CTransactionReviews
 * Do not add any new functions to this class.
 */

class CBaseTransactionReviews extends CEosPluralBase {

	/**
	 * @return CTransactionReview[]
	 */
	public static function fetchTransactionReviews( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTransactionReview::class, $objDatabase );
	}

	/**
	 * @return CTransactionReview
	 */
	public static function fetchTransactionReview( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTransactionReview::class, $objDatabase );
	}

	public static function fetchTransactionReviewCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transaction_reviews', $objDatabase );
	}

	public static function fetchTransactionReviewById( $intId, $objDatabase ) {
		return self::fetchTransactionReview( sprintf( 'SELECT * FROM transaction_reviews WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTransactionReviewsByCid( $intCid, $objDatabase ) {
		return self::fetchTransactionReviews( sprintf( 'SELECT * FROM transaction_reviews WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransactionReviewsByScreeningTransactionId( $intScreeningTransactionId, $objDatabase ) {
		return self::fetchTransactionReviews( sprintf( 'SELECT * FROM transaction_reviews WHERE screening_transaction_id = %d', ( int ) $intScreeningTransactionId ), $objDatabase );
	}

	public static function fetchTransactionReviewsByTransactionReviewStatusId( $intTransactionReviewStatusId, $objDatabase ) {
		return self::fetchTransactionReviews( sprintf( 'SELECT * FROM transaction_reviews WHERE transaction_review_status_id = %d', ( int ) $intTransactionReviewStatusId ), $objDatabase );
	}

	public static function fetchTransactionReviewsByTransactionReviewPriorityId( $intTransactionReviewPriorityId, $objDatabase ) {
		return self::fetchTransactionReviews( sprintf( 'SELECT * FROM transaction_reviews WHERE transaction_review_priority_id = %d', ( int ) $intTransactionReviewPriorityId ), $objDatabase );
	}

	public static function fetchTransactionReviewsByTransactionReviewQueueTypeId( $intTransactionReviewQueueTypeId, $objDatabase ) {
		return self::fetchTransactionReviews( sprintf( 'SELECT * FROM transaction_reviews WHERE transaction_review_queue_type_id = %d', ( int ) $intTransactionReviewQueueTypeId ), $objDatabase );
	}

}
?>