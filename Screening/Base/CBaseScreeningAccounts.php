<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningAccounts
 * Do not add any new functions to this class.
 */

class CBaseScreeningAccounts extends CEosPluralBase {

	/**
	 * @return CScreeningAccount[]
	 */
	public static function fetchScreeningAccounts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningAccount', $objDatabase );
	}

	/**
	 * @return CScreeningAccount
	 */
	public static function fetchScreeningAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningAccount', $objDatabase );
	}

	public static function fetchScreeningAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_accounts', $objDatabase );
	}

	public static function fetchScreeningAccountById( $intId, $objDatabase ) {
		return self::fetchScreeningAccount( sprintf( 'SELECT * FROM screening_accounts WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningAccounts( sprintf( 'SELECT * FROM screening_accounts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningAccountsByScreenRateTypeId( $intScreenRateTypeId, $objDatabase ) {
		return self::fetchScreeningAccounts( sprintf( 'SELECT * FROM screening_accounts WHERE screen_rate_type_id = %d', ( int ) $intScreenRateTypeId ), $objDatabase );
	}

}
?>