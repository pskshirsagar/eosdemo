<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CTransactionReviewQueueTypes
 * Do not add any new functions to this class.
 */

class CBaseTransactionReviewQueueTypes extends CEosPluralBase {

	/**
	 * @return CTransactionReviewQueueType[]
	 */
	public static function fetchTransactionReviewQueueTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTransactionReviewQueueType::class, $objDatabase );
	}

	/**
	 * @return CTransactionReviewQueueType
	 */
	public static function fetchTransactionReviewQueueType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTransactionReviewQueueType::class, $objDatabase );
	}

	public static function fetchTransactionReviewQueueTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transaction_review_queue_types', $objDatabase );
	}

	public static function fetchTransactionReviewQueueTypeById( $intId, $objDatabase ) {
		return self::fetchTransactionReviewQueueType( sprintf( 'SELECT * FROM transaction_review_queue_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>