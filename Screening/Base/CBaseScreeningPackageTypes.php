<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningPackageTypes extends CEosPluralBase {

	/**
	 * @return CScreeningPackageType[]
	 */
	public static function fetchScreeningPackageTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningPackageType::class, $objDatabase );
	}

	/**
	 * @return CScreeningPackageType
	 */
	public static function fetchScreeningPackageType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningPackageType::class, $objDatabase );
	}

	public static function fetchScreeningPackageTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_types', $objDatabase );
	}

	public static function fetchScreeningPackageTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningPackageType( sprintf( 'SELECT * FROM screening_package_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>