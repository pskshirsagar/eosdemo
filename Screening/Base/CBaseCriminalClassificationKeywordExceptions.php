<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CCriminalClassificationKeywordExceptions
 * Do not add any new functions to this class.
 */

class CBaseCriminalClassificationKeywordExceptions extends CEosPluralBase {

	/**
	 * @return CCriminalClassificationKeywordException[]
	 */
	public static function fetchCriminalClassificationKeywordExceptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCriminalClassificationKeywordException::class, $objDatabase );
	}

	/**
	 * @return CCriminalClassificationKeywordException
	 */
	public static function fetchCriminalClassificationKeywordException( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCriminalClassificationKeywordException::class, $objDatabase );
	}

	public static function fetchCriminalClassificationKeywordExceptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'criminal_classification_keyword_exceptions', $objDatabase );
	}

	public static function fetchCriminalClassificationKeywordExceptionById( $intId, $objDatabase ) {
		return self::fetchCriminalClassificationKeywordException( sprintf( 'SELECT * FROM criminal_classification_keyword_exceptions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCriminalClassificationKeywordExceptionsByCriminalKeywordExceptionTypeId( $intCriminalKeywordExceptionTypeId, $objDatabase ) {
		return self::fetchCriminalClassificationKeywordExceptions( sprintf( 'SELECT * FROM criminal_classification_keyword_exceptions WHERE criminal_keyword_exception_type_id = %d', ( int ) $intCriminalKeywordExceptionTypeId ), $objDatabase );
	}

	public static function fetchCriminalClassificationKeywordExceptionsByCriminalClassificationKeywordId( $intCriminalClassificationKeywordId, $objDatabase ) {
		return self::fetchCriminalClassificationKeywordExceptions( sprintf( 'SELECT * FROM criminal_classification_keyword_exceptions WHERE criminal_classification_keyword_id = %d', ( int ) $intCriminalClassificationKeywordId ), $objDatabase );
	}

	public static function fetchCriminalClassificationKeywordExceptionsByClassificationTypeId( $intClassificationTypeId, $objDatabase ) {
		return self::fetchCriminalClassificationKeywordExceptions( sprintf( 'SELECT * FROM criminal_classification_keyword_exceptions WHERE classification_type_id = %d', ( int ) $intClassificationTypeId ), $objDatabase );
	}

}
?>