<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCriteriaConditionSets
 * Do not add any new functions to this class.
 */

class CBaseScreeningCriteriaConditionSets extends CEosPluralBase {

	/**
	 * @return CScreeningCriteriaConditionSet[]
	 */
	public static function fetchScreeningCriteriaConditionSets( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningCriteriaConditionSet::class, $objDatabase );
	}

	/**
	 * @return CScreeningCriteriaConditionSet
	 */
	public static function fetchScreeningCriteriaConditionSet( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningCriteriaConditionSet::class, $objDatabase );
	}

	public static function fetchScreeningCriteriaConditionSetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_criteria_condition_sets', $objDatabase );
	}

	public static function fetchScreeningCriteriaConditionSetById( $intId, $objDatabase ) {
		return self::fetchScreeningCriteriaConditionSet( sprintf( 'SELECT * FROM screening_criteria_condition_sets WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningCriteriaConditionSetsByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {
		return self::fetchScreeningCriteriaConditionSets( sprintf( 'SELECT * FROM screening_criteria_condition_sets WHERE screening_criteria_id = %d', ( int ) $intScreeningCriteriaId ), $objDatabase );
	}

	public static function fetchScreeningCriteriaConditionSetsByScreeningConditionSetId( $intScreeningConditionSetId, $objDatabase ) {
		return self::fetchScreeningCriteriaConditionSets( sprintf( 'SELECT * FROM screening_criteria_condition_sets WHERE screening_condition_set_id = %d', ( int ) $intScreeningConditionSetId ), $objDatabase );
	}

	public static function fetchScreeningCriteriaConditionSetsByScreeningConditionSetIdByCriteriaId( $intScreeningConditionSetId, $intScreeningCriteriaId, $objDatabase ) {
		return self::fetchScreeningCriteriaConditionSets( sprintf( 'SELECT * FROM screening_criteria_condition_sets WHERE screening_condition_set_id = %d AND screening_criteria_id = %d', ( int ) $intScreeningConditionSetId, ( int ) $intScreeningCriteriaId  ), $objDatabase );
	}

}
?>