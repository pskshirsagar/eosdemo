<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDisputeTypeReasons
 * Do not add any new functions to this class.
 */

class CBaseDisputeTypeReasons extends CEosPluralBase {

	/**
	 * @return CDisputeTypeReason[]
	 */
	public static function fetchDisputeTypeReasons( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDisputeTypeReason', $objDatabase );
	}

	/**
	 * @return CDisputeTypeReason
	 */
	public static function fetchDisputeTypeReason( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDisputeTypeReason', $objDatabase );
	}

	public static function fetchDisputeTypeReasonCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'dispute_type_reasons', $objDatabase );
	}

	public static function fetchDisputeTypeReasonById( $intId, $objDatabase ) {
		return self::fetchDisputeTypeReason( sprintf( 'SELECT * FROM dispute_type_reasons WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDisputeTypeReasonsByDisputeTypeId( $intDisputeTypeId, $objDatabase ) {
		return self::fetchDisputeTypeReasons( sprintf( 'SELECT * FROM dispute_type_reasons WHERE dispute_type_id = %d', ( int ) $intDisputeTypeId ), $objDatabase );
	}

}
?>