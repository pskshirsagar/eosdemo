<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningApplicationVolumeReports
 * Do not add any new functions to this class.
 */

class CBaseScreeningApplicationVolumeReports extends CEosPluralBase {

	/**
	 * @return CScreeningApplicationVolumeReport[]
	 */
	public static function fetchScreeningApplicationVolumeReports( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningApplicationVolumeReport::class, $objDatabase );
	}

	/**
	 * @return CScreeningApplicationVolumeReport
	 */
	public static function fetchScreeningApplicationVolumeReport( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningApplicationVolumeReport::class, $objDatabase );
	}

	public static function fetchScreeningApplicationVolumeReportCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_application_volume_reports', $objDatabase );
	}

	public static function fetchScreeningApplicationVolumeReportById( $intId, $objDatabase ) {
		return self::fetchScreeningApplicationVolumeReport( sprintf( 'SELECT * FROM screening_application_volume_reports WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningApplicationVolumeReportsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationVolumeReports( sprintf( 'SELECT * FROM screening_application_volume_reports WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationVolumeReportsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchScreeningApplicationVolumeReports( sprintf( 'SELECT * FROM screening_application_volume_reports WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>