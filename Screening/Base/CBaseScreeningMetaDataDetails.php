<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningMetaDataDetails
 * Do not add any new functions to this class.
 */

class CBaseScreeningMetaDataDetails extends CEosPluralBase {

	/**
	 * @return CScreeningMetaDataDetail[]
	 */
	public static function fetchScreeningMetaDataDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningMetaDataDetail', $objDatabase );
	}

	/**
	 * @return CScreeningMetaDataDetail
	 */
	public static function fetchScreeningMetaDataDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningMetaDataDetail', $objDatabase );
	}

	public static function fetchScreeningMetaDataDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_meta_data_details', $objDatabase );
	}

	public static function fetchScreeningMetaDataDetailById( $intId, $objDatabase ) {
		return self::fetchScreeningMetaDataDetail( sprintf( 'SELECT * FROM screening_meta_data_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningMetaDataDetailsByScreeningTradelineTypeId( $intScreeningTradelineTypeId, $objDatabase ) {
		return self::fetchScreeningMetaDataDetails( sprintf( 'SELECT * FROM screening_meta_data_details WHERE screening_tradeline_type_id = %d', ( int ) $intScreeningTradelineTypeId ), $objDatabase );
	}

	public static function fetchScreeningMetaDataDetailsByScreeningMetaDataTypeId( $intScreeningMetaDataTypeId, $objDatabase ) {
		return self::fetchScreeningMetaDataDetails( sprintf( 'SELECT * FROM screening_meta_data_details WHERE screening_meta_data_type_id = %d', ( int ) $intScreeningMetaDataTypeId ), $objDatabase );
	}

}
?>