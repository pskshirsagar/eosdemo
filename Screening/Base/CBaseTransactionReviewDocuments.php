<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CTransactionReviewDocuments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseTransactionReviewDocuments extends CEosPluralBase {

	/**
	 * @return CTransactionReviewDocument[]
	 */
	public static function fetchTransactionReviewDocuments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTransactionReviewDocument::class, $objDatabase );
	}

	/**
	 * @return CTransactionReviewDocument
	 */
	public static function fetchTransactionReviewDocument( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTransactionReviewDocument::class, $objDatabase );
	}

	public static function fetchTransactionReviewDocumentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transaction_review_documents', $objDatabase );
	}

	public static function fetchTransactionReviewDocumentByIdByCid( $intId, $intCid, $objDatabase ) {

		return self::fetchTransactionReviewDocument( sprintf( 'SELECT * FROM transaction_review_documents WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransactionReviewDocumentsByCid( $intCid, $objDatabase ) {
		return self::fetchTransactionReviewDocuments( sprintf( 'SELECT * FROM transaction_review_documents WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransactionReviewDocumentsByScreeningApplicantIdByCid( $intScreeningApplicantId, $intCid, $objDatabase ) {
		return self::fetchTransactionReviewDocuments( sprintf( 'SELECT * FROM transaction_review_documents WHERE screening_applicant_id = %d AND cid = %d', ( int ) $intScreeningApplicantId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransactionReviewDocumentsByTransactionReviewIdByCid( $intTransactionReviewId, $intCid, $objDatabase ) {
		return self::fetchTransactionReviewDocuments( sprintf( 'SELECT * FROM transaction_review_documents WHERE transaction_review_id = %d AND cid = %d', ( int ) $intTransactionReviewId, ( int ) $intCid ), $objDatabase );
	}

}
?>