<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningSearchCriteriaTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningSearchCriteriaTypes extends CEosPluralBase {

	/**
	 * @return CScreeningSearchCriteriaType[]
	 */
	public static function fetchScreeningSearchCriteriaTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningSearchCriteriaType', $objDatabase );
	}

	/**
	 * @return CScreeningSearchCriteriaType
	 */
	public static function fetchScreeningSearchCriteriaType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningSearchCriteriaType', $objDatabase );
	}

	public static function fetchScreeningSearchCriteriaTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_search_criteria_types', $objDatabase );
	}

	public static function fetchScreeningSearchCriteriaTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningSearchCriteriaType( sprintf( 'SELECT * FROM screening_search_criteria_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>