<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningErrorTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningErrorTypes extends CEosPluralBase {

	/**
	 * @return CScreeningErrorType[]
	 */
	public static function fetchScreeningErrorTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningErrorType', $objDatabase );
	}

	/**
	 * @return CScreeningErrorType
	 */
	public static function fetchScreeningErrorType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningErrorType', $objDatabase );
	}

	public static function fetchScreeningErrorTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_error_types', $objDatabase );
	}

	public static function fetchScreeningErrorTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningErrorType( sprintf( 'SELECT * FROM screening_error_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>