<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CCriminalClassificationTypes
 * Do not add any new functions to this class.
 */

class CBaseCriminalClassificationTypes extends CEosPluralBase {

	/**
	 * @return CCriminalClassificationType[]
	 */
	public static function fetchCriminalClassificationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCriminalClassificationType', $objDatabase );
	}

	/**
	 * @return CCriminalClassificationType
	 */
	public static function fetchCriminalClassificationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCriminalClassificationType', $objDatabase );
	}

	public static function fetchCriminalClassificationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'criminal_classification_types', $objDatabase );
	}

	public static function fetchCriminalClassificationTypeById( $intId, $objDatabase ) {
		return self::fetchCriminalClassificationType( sprintf( 'SELECT * FROM criminal_classification_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>