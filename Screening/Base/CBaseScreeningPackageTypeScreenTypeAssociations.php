<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageTypeScreenTypeAssociations
 * Do not add any new functions to this class.
 */

class CBaseScreeningPackageTypeScreenTypeAssociations extends CEosPluralBase {

	/**
	 * @return CScreeningPackageTypeScreenTypeAssociation[]
	 */
	public static function fetchScreeningPackageTypeScreenTypeAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageTypeScreenTypeAssociation', $objDatabase );
	}

	/**
	 * @return CScreeningPackageTypeScreenTypeAssociation
	 */
	public static function fetchScreeningPackageTypeScreenTypeAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageTypeScreenTypeAssociation', $objDatabase );
	}

	public static function fetchScreeningPackageTypeScreenTypeAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_type_screen_type_associations', $objDatabase );
	}

	public static function fetchScreeningPackageTypeScreenTypeAssociationById( $intId, $objDatabase ) {
		return self::fetchScreeningPackageTypeScreenTypeAssociation( sprintf( 'SELECT * FROM screening_package_type_screen_type_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningPackageTypeScreenTypeAssociationsByScreeningPackageTypeId( $intScreeningPackageTypeId, $objDatabase ) {
		return self::fetchScreeningPackageTypeScreenTypeAssociations( sprintf( 'SELECT * FROM screening_package_type_screen_type_associations WHERE screening_package_type_id = %d', ( int ) $intScreeningPackageTypeId ), $objDatabase );
	}

	public static function fetchScreeningPackageTypeScreenTypeAssociationsByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreeningPackageTypeScreenTypeAssociations( sprintf( 'SELECT * FROM screening_package_type_screen_type_associations WHERE screen_type_id = %d', ( int ) $intScreenTypeId ), $objDatabase );
	}

}
?>