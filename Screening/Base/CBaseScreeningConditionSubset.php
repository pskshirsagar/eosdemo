<?php

class CBaseScreeningConditionSubset extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_condition_subsets';

	protected $m_intId;
	protected $m_intScreeningConditionSetId;
	protected $m_intRequiredItems;
	protected $m_intSortOrderId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_condition_set_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningConditionSetId', trim( $arrValues['screening_condition_set_id'] ) ); elseif( isset( $arrValues['screening_condition_set_id'] ) ) $this->setScreeningConditionSetId( $arrValues['screening_condition_set_id'] );
		if( isset( $arrValues['required_items'] ) && $boolDirectSet ) $this->set( 'm_intRequiredItems', trim( $arrValues['required_items'] ) ); elseif( isset( $arrValues['required_items'] ) ) $this->setRequiredItems( $arrValues['required_items'] );
		if( isset( $arrValues['sort_order_id'] ) && $boolDirectSet ) $this->set( 'm_intSortOrderId', trim( $arrValues['sort_order_id'] ) ); elseif( isset( $arrValues['sort_order_id'] ) ) $this->setSortOrderId( $arrValues['sort_order_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningConditionSetId( $intScreeningConditionSetId ) {
		$this->set( 'm_intScreeningConditionSetId', CStrings::strToIntDef( $intScreeningConditionSetId, NULL, false ) );
	}

	public function getScreeningConditionSetId() {
		return $this->m_intScreeningConditionSetId;
	}

	public function sqlScreeningConditionSetId() {
		return ( true == isset( $this->m_intScreeningConditionSetId ) ) ? ( string ) $this->m_intScreeningConditionSetId : 'NULL';
	}

	public function setRequiredItems( $intRequiredItems ) {
		$this->set( 'm_intRequiredItems', CStrings::strToIntDef( $intRequiredItems, NULL, false ) );
	}

	public function getRequiredItems() {
		return $this->m_intRequiredItems;
	}

	public function sqlRequiredItems() {
		return ( true == isset( $this->m_intRequiredItems ) ) ? ( string ) $this->m_intRequiredItems : 'NULL';
	}

	public function setSortOrderId( $intSortOrderId ) {
		$this->set( 'm_intSortOrderId', CStrings::strToIntDef( $intSortOrderId, NULL, false ) );
	}

	public function getSortOrderId() {
		return $this->m_intSortOrderId;
	}

	public function sqlSortOrderId() {
		return ( true == isset( $this->m_intSortOrderId ) ) ? ( string ) $this->m_intSortOrderId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, screening_condition_set_id, required_items, sort_order_id, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlScreeningConditionSetId() . ', ' .
 						$this->sqlRequiredItems() . ', ' .
 						$this->sqlSortOrderId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_condition_set_id = ' . $this->sqlScreeningConditionSetId() . ','; } elseif( true == array_key_exists( 'ScreeningConditionSetId', $this->getChangedColumns() ) ) { $strSql .= ' screening_condition_set_id = ' . $this->sqlScreeningConditionSetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_items = ' . $this->sqlRequiredItems() . ','; } elseif( true == array_key_exists( 'RequiredItems', $this->getChangedColumns() ) ) { $strSql .= ' required_items = ' . $this->sqlRequiredItems() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sort_order_id = ' . $this->sqlSortOrderId() . ','; } elseif( true == array_key_exists( 'SortOrderId', $this->getChangedColumns() ) ) { $strSql .= ' sort_order_id = ' . $this->sqlSortOrderId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_condition_set_id' => $this->getScreeningConditionSetId(),
			'required_items' => $this->getRequiredItems(),
			'sort_order_id' => $this->getSortOrderId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>