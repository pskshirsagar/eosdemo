<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CRvReportCriteriaRangeTypes
 * Do not add any new functions to this class.
 */

class CBaseRvReportCriteriaRangeTypes extends CEosPluralBase {

	/**
	 * @return CRvReportCriteriaRangeType[]
	 */
	public static function fetchRvReportCriteriaRangeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRvReportCriteriaRangeType::class, $objDatabase );
	}

	/**
	 * @return CRvReportCriteriaRangeType
	 */
	public static function fetchRvReportCriteriaRangeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRvReportCriteriaRangeType::class, $objDatabase );
	}

	public static function fetchRvReportCriteriaRangeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'rv_report_criteria_range_types', $objDatabase );
	}

	public static function fetchRvReportCriteriaRangeTypeById( $intId, $objDatabase ) {
		return self::fetchRvReportCriteriaRangeType( sprintf( 'SELECT * FROM rv_report_criteria_range_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchRvReportCriteriaRangeTypesByScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		return self::fetchRvReportCriteriaRangeTypes( sprintf( 'SELECT * FROM rv_report_criteria_range_types WHERE screening_config_preference_type_id = %d', ( int ) $intScreeningConfigPreferenceTypeId ), $objDatabase );
	}

}
?>