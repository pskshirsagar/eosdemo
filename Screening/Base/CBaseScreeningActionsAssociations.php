<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningActionsAssociations
 * Do not add any new functions to this class.
 */

class CBaseScreeningActionsAssociations extends CEosPluralBase {

	/**
	 * @return CScreeningActionsAssociation[]
	 */
	public static function fetchScreeningActionsAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningActionsAssociation', $objDatabase );
	}

	/**
	 * @return CScreeningActionsAssociation
	 */
	public static function fetchScreeningActionsAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningActionsAssociation', $objDatabase );
	}

	public static function fetchScreeningActionsAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_actions_associations', $objDatabase );
	}

	public static function fetchScreeningActionsAssociationById( $intId, $objDatabase ) {
		return self::fetchScreeningActionsAssociation( sprintf( 'SELECT * FROM screening_actions_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningActionsAssociationsByScreeningDataProviderTypeId( $intScreeningDataProviderTypeId, $objDatabase ) {
		return self::fetchScreeningActionsAssociations( sprintf( 'SELECT * FROM screening_actions_associations WHERE screening_data_provider_type_id = %d', ( int ) $intScreeningDataProviderTypeId ), $objDatabase );
	}

	public static function fetchScreeningActionsAssociationsByScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		return self::fetchScreeningActionsAssociations( sprintf( 'SELECT * FROM screening_actions_associations WHERE screening_config_preference_type_id = %d', ( int ) $intScreeningConfigPreferenceTypeId ), $objDatabase );
	}

	public static function fetchScreeningActionsAssociationsByScreeningSearchActionId( $intScreeningSearchActionId, $objDatabase ) {
		return self::fetchScreeningActionsAssociations( sprintf( 'SELECT * FROM screening_actions_associations WHERE screening_search_action_id = %d', ( int ) $intScreeningSearchActionId ), $objDatabase );
	}

}
?>