<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningPackageConditionSet extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_package_condition_sets';

	protected $m_intId;
	protected $m_intScreeningPackageId;
	protected $m_intCid;
	protected $m_intScreeningRecommendationTypeId;
	protected $m_intFirstScreeningPackageConditionSubsetId;
	protected $m_intSecondScreeningPackageConditionSubsetId;
	protected $m_intScreeningPackageOperandTypeId;
	protected $m_strConditionName;
	protected $m_intEvaluationOrder;
	protected $m_intIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_package_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageId', trim( $arrValues['screening_package_id'] ) ); elseif( isset( $arrValues['screening_package_id'] ) ) $this->setScreeningPackageId( $arrValues['screening_package_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screening_recommendation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningRecommendationTypeId', trim( $arrValues['screening_recommendation_type_id'] ) ); elseif( isset( $arrValues['screening_recommendation_type_id'] ) ) $this->setScreeningRecommendationTypeId( $arrValues['screening_recommendation_type_id'] );
		if( isset( $arrValues['first_screening_package_condition_subset_id'] ) && $boolDirectSet ) $this->set( 'm_intFirstScreeningPackageConditionSubsetId', trim( $arrValues['first_screening_package_condition_subset_id'] ) ); elseif( isset( $arrValues['first_screening_package_condition_subset_id'] ) ) $this->setFirstScreeningPackageConditionSubsetId( $arrValues['first_screening_package_condition_subset_id'] );
		if( isset( $arrValues['second_screening_package_condition_subset_id'] ) && $boolDirectSet ) $this->set( 'm_intSecondScreeningPackageConditionSubsetId', trim( $arrValues['second_screening_package_condition_subset_id'] ) ); elseif( isset( $arrValues['second_screening_package_condition_subset_id'] ) ) $this->setSecondScreeningPackageConditionSubsetId( $arrValues['second_screening_package_condition_subset_id'] );
		if( isset( $arrValues['screening_package_operand_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageOperandTypeId', trim( $arrValues['screening_package_operand_type_id'] ) ); elseif( isset( $arrValues['screening_package_operand_type_id'] ) ) $this->setScreeningPackageOperandTypeId( $arrValues['screening_package_operand_type_id'] );
		if( isset( $arrValues['condition_name'] ) && $boolDirectSet ) $this->set( 'm_strConditionName', trim( stripcslashes( $arrValues['condition_name'] ) ) ); elseif( isset( $arrValues['condition_name'] ) ) $this->setConditionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['condition_name'] ) : $arrValues['condition_name'] );
		if( isset( $arrValues['evaluation_order'] ) && $boolDirectSet ) $this->set( 'm_intEvaluationOrder', trim( $arrValues['evaluation_order'] ) ); elseif( isset( $arrValues['evaluation_order'] ) ) $this->setEvaluationOrder( $arrValues['evaluation_order'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningPackageId( $intScreeningPackageId ) {
		$this->set( 'm_intScreeningPackageId', CStrings::strToIntDef( $intScreeningPackageId, NULL, false ) );
	}

	public function getScreeningPackageId() {
		return $this->m_intScreeningPackageId;
	}

	public function sqlScreeningPackageId() {
		return ( true == isset( $this->m_intScreeningPackageId ) ) ? ( string ) $this->m_intScreeningPackageId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreeningRecommendationTypeId( $intScreeningRecommendationTypeId ) {
		$this->set( 'm_intScreeningRecommendationTypeId', CStrings::strToIntDef( $intScreeningRecommendationTypeId, NULL, false ) );
	}

	public function getScreeningRecommendationTypeId() {
		return $this->m_intScreeningRecommendationTypeId;
	}

	public function sqlScreeningRecommendationTypeId() {
		return ( true == isset( $this->m_intScreeningRecommendationTypeId ) ) ? ( string ) $this->m_intScreeningRecommendationTypeId : 'NULL';
	}

	public function setFirstScreeningPackageConditionSubsetId( $intFirstScreeningPackageConditionSubsetId ) {
		$this->set( 'm_intFirstScreeningPackageConditionSubsetId', CStrings::strToIntDef( $intFirstScreeningPackageConditionSubsetId, NULL, false ) );
	}

	public function getFirstScreeningPackageConditionSubsetId() {
		return $this->m_intFirstScreeningPackageConditionSubsetId;
	}

	public function sqlFirstScreeningPackageConditionSubsetId() {
		return ( true == isset( $this->m_intFirstScreeningPackageConditionSubsetId ) ) ? ( string ) $this->m_intFirstScreeningPackageConditionSubsetId : 'NULL';
	}

	public function setSecondScreeningPackageConditionSubsetId( $intSecondScreeningPackageConditionSubsetId ) {
		$this->set( 'm_intSecondScreeningPackageConditionSubsetId', CStrings::strToIntDef( $intSecondScreeningPackageConditionSubsetId, NULL, false ) );
	}

	public function getSecondScreeningPackageConditionSubsetId() {
		return $this->m_intSecondScreeningPackageConditionSubsetId;
	}

	public function sqlSecondScreeningPackageConditionSubsetId() {
		return ( true == isset( $this->m_intSecondScreeningPackageConditionSubsetId ) ) ? ( string ) $this->m_intSecondScreeningPackageConditionSubsetId : 'NULL';
	}

	public function setScreeningPackageOperandTypeId( $intScreeningPackageOperandTypeId ) {
		$this->set( 'm_intScreeningPackageOperandTypeId', CStrings::strToIntDef( $intScreeningPackageOperandTypeId, NULL, false ) );
	}

	public function getScreeningPackageOperandTypeId() {
		return $this->m_intScreeningPackageOperandTypeId;
	}

	public function sqlScreeningPackageOperandTypeId() {
		return ( true == isset( $this->m_intScreeningPackageOperandTypeId ) ) ? ( string ) $this->m_intScreeningPackageOperandTypeId : 'NULL';
	}

	public function setConditionName( $strConditionName ) {
		$this->set( 'm_strConditionName', CStrings::strTrimDef( $strConditionName, -1, NULL, true ) );
	}

	public function getConditionName() {
		return $this->m_strConditionName;
	}

	public function sqlConditionName() {
		return ( true == isset( $this->m_strConditionName ) ) ? '\'' . addslashes( $this->m_strConditionName ) . '\'' : 'NULL';
	}

	public function setEvaluationOrder( $intEvaluationOrder ) {
		$this->set( 'm_intEvaluationOrder', CStrings::strToIntDef( $intEvaluationOrder, NULL, false ) );
	}

	public function getEvaluationOrder() {
		return $this->m_intEvaluationOrder;
	}

	public function sqlEvaluationOrder() {
		return ( true == isset( $this->m_intEvaluationOrder ) ) ? ( string ) $this->m_intEvaluationOrder : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, screening_package_id, cid, screening_recommendation_type_id, first_screening_package_condition_subset_id, second_screening_package_condition_subset_id, screening_package_operand_type_id, condition_name, evaluation_order, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlScreeningPackageId() . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlScreeningRecommendationTypeId() . ', ' .
 						$this->sqlFirstScreeningPackageConditionSubsetId() . ', ' .
 						$this->sqlSecondScreeningPackageConditionSubsetId() . ', ' .
 						$this->sqlScreeningPackageOperandTypeId() . ', ' .
 						$this->sqlConditionName() . ', ' .
 						$this->sqlEvaluationOrder() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_id = ' . $this->sqlScreeningPackageId() . ','; } elseif( true == array_key_exists( 'ScreeningPackageId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_id = ' . $this->sqlScreeningPackageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId() . ','; } elseif( true == array_key_exists( 'ScreeningRecommendationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_screening_package_condition_subset_id = ' . $this->sqlFirstScreeningPackageConditionSubsetId() . ','; } elseif( true == array_key_exists( 'FirstScreeningPackageConditionSubsetId', $this->getChangedColumns() ) ) { $strSql .= ' first_screening_package_condition_subset_id = ' . $this->sqlFirstScreeningPackageConditionSubsetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' second_screening_package_condition_subset_id = ' . $this->sqlSecondScreeningPackageConditionSubsetId() . ','; } elseif( true == array_key_exists( 'SecondScreeningPackageConditionSubsetId', $this->getChangedColumns() ) ) { $strSql .= ' second_screening_package_condition_subset_id = ' . $this->sqlSecondScreeningPackageConditionSubsetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_operand_type_id = ' . $this->sqlScreeningPackageOperandTypeId() . ','; } elseif( true == array_key_exists( 'ScreeningPackageOperandTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_operand_type_id = ' . $this->sqlScreeningPackageOperandTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_name = ' . $this->sqlConditionName() . ','; } elseif( true == array_key_exists( 'ConditionName', $this->getChangedColumns() ) ) { $strSql .= ' condition_name = ' . $this->sqlConditionName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' evaluation_order = ' . $this->sqlEvaluationOrder() . ','; } elseif( true == array_key_exists( 'EvaluationOrder', $this->getChangedColumns() ) ) { $strSql .= ' evaluation_order = ' . $this->sqlEvaluationOrder() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_package_id' => $this->getScreeningPackageId(),
			'cid' => $this->getCid(),
			'screening_recommendation_type_id' => $this->getScreeningRecommendationTypeId(),
			'first_screening_package_condition_subset_id' => $this->getFirstScreeningPackageConditionSubsetId(),
			'second_screening_package_condition_subset_id' => $this->getSecondScreeningPackageConditionSubsetId(),
			'screening_package_operand_type_id' => $this->getScreeningPackageOperandTypeId(),
			'condition_name' => $this->getConditionName(),
			'evaluation_order' => $this->getEvaluationOrder(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>