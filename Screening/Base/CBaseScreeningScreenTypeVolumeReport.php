<?php

class CBaseScreeningScreenTypeVolumeReport extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_screen_type_volume_reports';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intScreenTypeId;
	protected $m_fltRevenue;
	protected $m_strReportMonth;
	protected $m_intTotalTransactions;
	protected $m_intTotalPass;
	protected $m_intTotalConditional;
	protected $m_intTotalFail;
	protected $m_intTotalInprogress;
	protected $m_intTotalError;
	protected $m_strAverageRequestedOn;
	protected $m_strAverageResponseCompletedOn;
	protected $m_strAverageCompletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltRevenue = '0';
		$this->m_intTotalTransactions = '0';
		$this->m_intTotalPass = '0';
		$this->m_intTotalConditional = '0';
		$this->m_intTotalFail = '0';
		$this->m_intTotalInprogress = '0';
		$this->m_intTotalError = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['screen_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreenTypeId', trim( $arrValues['screen_type_id'] ) ); elseif( isset( $arrValues['screen_type_id'] ) ) $this->setScreenTypeId( $arrValues['screen_type_id'] );
		if( isset( $arrValues['revenue'] ) && $boolDirectSet ) $this->set( 'm_fltRevenue', trim( $arrValues['revenue'] ) ); elseif( isset( $arrValues['revenue'] ) ) $this->setRevenue( $arrValues['revenue'] );
		if( isset( $arrValues['report_month'] ) && $boolDirectSet ) $this->set( 'm_strReportMonth', trim( $arrValues['report_month'] ) ); elseif( isset( $arrValues['report_month'] ) ) $this->setReportMonth( $arrValues['report_month'] );
		if( isset( $arrValues['total_transactions'] ) && $boolDirectSet ) $this->set( 'm_intTotalTransactions', trim( $arrValues['total_transactions'] ) ); elseif( isset( $arrValues['total_transactions'] ) ) $this->setTotalTransactions( $arrValues['total_transactions'] );
		if( isset( $arrValues['total_pass'] ) && $boolDirectSet ) $this->set( 'm_intTotalPass', trim( $arrValues['total_pass'] ) ); elseif( isset( $arrValues['total_pass'] ) ) $this->setTotalPass( $arrValues['total_pass'] );
		if( isset( $arrValues['total_conditional'] ) && $boolDirectSet ) $this->set( 'm_intTotalConditional', trim( $arrValues['total_conditional'] ) ); elseif( isset( $arrValues['total_conditional'] ) ) $this->setTotalConditional( $arrValues['total_conditional'] );
		if( isset( $arrValues['total_fail'] ) && $boolDirectSet ) $this->set( 'm_intTotalFail', trim( $arrValues['total_fail'] ) ); elseif( isset( $arrValues['total_fail'] ) ) $this->setTotalFail( $arrValues['total_fail'] );
		if( isset( $arrValues['total_inprogress'] ) && $boolDirectSet ) $this->set( 'm_intTotalInprogress', trim( $arrValues['total_inprogress'] ) ); elseif( isset( $arrValues['total_inprogress'] ) ) $this->setTotalInprogress( $arrValues['total_inprogress'] );
		if( isset( $arrValues['total_error'] ) && $boolDirectSet ) $this->set( 'm_intTotalError', trim( $arrValues['total_error'] ) ); elseif( isset( $arrValues['total_error'] ) ) $this->setTotalError( $arrValues['total_error'] );
		if( isset( $arrValues['average_requested_on'] ) && $boolDirectSet ) $this->set( 'm_strAverageRequestedOn', trim( $arrValues['average_requested_on'] ) ); elseif( isset( $arrValues['average_requested_on'] ) ) $this->setAverageRequestedOn( $arrValues['average_requested_on'] );
		if( isset( $arrValues['average_response_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strAverageResponseCompletedOn', trim( $arrValues['average_response_completed_on'] ) ); elseif( isset( $arrValues['average_response_completed_on'] ) ) $this->setAverageResponseCompletedOn( $arrValues['average_response_completed_on'] );
		if( isset( $arrValues['average_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strAverageCompletedOn', trim( $arrValues['average_completed_on'] ) ); elseif( isset( $arrValues['average_completed_on'] ) ) $this->setAverageCompletedOn( $arrValues['average_completed_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setScreenTypeId( $intScreenTypeId ) {
		$this->set( 'm_intScreenTypeId', CStrings::strToIntDef( $intScreenTypeId, NULL, false ) );
	}

	public function getScreenTypeId() {
		return $this->m_intScreenTypeId;
	}

	public function sqlScreenTypeId() {
		return ( true == isset( $this->m_intScreenTypeId ) ) ? ( string ) $this->m_intScreenTypeId : 'NULL';
	}

	public function setRevenue( $fltRevenue ) {
		$this->set( 'm_fltRevenue', CStrings::strToFloatDef( $fltRevenue, NULL, false, 3 ) );
	}

	public function getRevenue() {
		return $this->m_fltRevenue;
	}

	public function sqlRevenue() {
		return ( true == isset( $this->m_fltRevenue ) ) ? ( string ) $this->m_fltRevenue : '0';
	}

	public function setReportMonth( $strReportMonth ) {
		$this->set( 'm_strReportMonth', CStrings::strTrimDef( $strReportMonth, -1, NULL, true ) );
	}

	public function getReportMonth() {
		return $this->m_strReportMonth;
	}

	public function sqlReportMonth() {
		return ( true == isset( $this->m_strReportMonth ) ) ? '\'' . $this->m_strReportMonth . '\'' : 'NOW()';
	}

	public function setTotalTransactions( $intTotalTransactions ) {
		$this->set( 'm_intTotalTransactions', CStrings::strToIntDef( $intTotalTransactions, NULL, false ) );
	}

	public function getTotalTransactions() {
		return $this->m_intTotalTransactions;
	}

	public function sqlTotalTransactions() {
		return ( true == isset( $this->m_intTotalTransactions ) ) ? ( string ) $this->m_intTotalTransactions : '0';
	}

	public function setTotalPass( $intTotalPass ) {
		$this->set( 'm_intTotalPass', CStrings::strToIntDef( $intTotalPass, NULL, false ) );
	}

	public function getTotalPass() {
		return $this->m_intTotalPass;
	}

	public function sqlTotalPass() {
		return ( true == isset( $this->m_intTotalPass ) ) ? ( string ) $this->m_intTotalPass : '0';
	}

	public function setTotalConditional( $intTotalConditional ) {
		$this->set( 'm_intTotalConditional', CStrings::strToIntDef( $intTotalConditional, NULL, false ) );
	}

	public function getTotalConditional() {
		return $this->m_intTotalConditional;
	}

	public function sqlTotalConditional() {
		return ( true == isset( $this->m_intTotalConditional ) ) ? ( string ) $this->m_intTotalConditional : '0';
	}

	public function setTotalFail( $intTotalFail ) {
		$this->set( 'm_intTotalFail', CStrings::strToIntDef( $intTotalFail, NULL, false ) );
	}

	public function getTotalFail() {
		return $this->m_intTotalFail;
	}

	public function sqlTotalFail() {
		return ( true == isset( $this->m_intTotalFail ) ) ? ( string ) $this->m_intTotalFail : '0';
	}

	public function setTotalInprogress( $intTotalInprogress ) {
		$this->set( 'm_intTotalInprogress', CStrings::strToIntDef( $intTotalInprogress, NULL, false ) );
	}

	public function getTotalInprogress() {
		return $this->m_intTotalInprogress;
	}

	public function sqlTotalInprogress() {
		return ( true == isset( $this->m_intTotalInprogress ) ) ? ( string ) $this->m_intTotalInprogress : '0';
	}

	public function setTotalError( $intTotalError ) {
		$this->set( 'm_intTotalError', CStrings::strToIntDef( $intTotalError, NULL, false ) );
	}

	public function getTotalError() {
		return $this->m_intTotalError;
	}

	public function sqlTotalError() {
		return ( true == isset( $this->m_intTotalError ) ) ? ( string ) $this->m_intTotalError : '0';
	}

	public function setAverageRequestedOn( $strAverageRequestedOn ) {
		$this->set( 'm_strAverageRequestedOn', CStrings::strTrimDef( $strAverageRequestedOn, NULL, NULL, true ) );
	}

	public function getAverageRequestedOn() {
		return $this->m_strAverageRequestedOn;
	}

	public function sqlAverageRequestedOn() {
		return ( true == isset( $this->m_strAverageRequestedOn ) ) ? '\'' . addslashes( $this->m_strAverageRequestedOn ) . '\'' : 'NULL';
	}

	public function setAverageResponseCompletedOn( $strAverageResponseCompletedOn ) {
		$this->set( 'm_strAverageResponseCompletedOn', CStrings::strTrimDef( $strAverageResponseCompletedOn, NULL, NULL, true ) );
	}

	public function getAverageResponseCompletedOn() {
		return $this->m_strAverageResponseCompletedOn;
	}

	public function sqlAverageResponseCompletedOn() {
		return ( true == isset( $this->m_strAverageResponseCompletedOn ) ) ? '\'' . addslashes( $this->m_strAverageResponseCompletedOn ) . '\'' : 'NULL';
	}

	public function setAverageCompletedOn( $strAverageCompletedOn ) {
		$this->set( 'm_strAverageCompletedOn', CStrings::strTrimDef( $strAverageCompletedOn, NULL, NULL, true ) );
	}

	public function getAverageCompletedOn() {
		return $this->m_strAverageCompletedOn;
	}

	public function sqlAverageCompletedOn() {
		return ( true == isset( $this->m_strAverageCompletedOn ) ) ? '\'' . addslashes( $this->m_strAverageCompletedOn ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, screen_type_id, revenue, report_month, total_transactions, total_pass, total_conditional, total_fail, total_inprogress, total_error, average_requested_on, average_response_completed_on, average_completed_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlScreenTypeId() . ', ' .
 						$this->sqlRevenue() . ', ' .
 						$this->sqlReportMonth() . ', ' .
 						$this->sqlTotalTransactions() . ', ' .
 						$this->sqlTotalPass() . ', ' .
 						$this->sqlTotalConditional() . ', ' .
 						$this->sqlTotalFail() . ', ' .
 						$this->sqlTotalInprogress() . ', ' .
 						$this->sqlTotalError() . ', ' .
 						$this->sqlAverageRequestedOn() . ', ' .
 						$this->sqlAverageResponseCompletedOn() . ', ' .
 						$this->sqlAverageCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId() . ','; } elseif( true == array_key_exists( 'ScreenTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' revenue = ' . $this->sqlRevenue() . ','; } elseif( true == array_key_exists( 'Revenue', $this->getChangedColumns() ) ) { $strSql .= ' revenue = ' . $this->sqlRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_month = ' . $this->sqlReportMonth() . ','; } elseif( true == array_key_exists( 'ReportMonth', $this->getChangedColumns() ) ) { $strSql .= ' report_month = ' . $this->sqlReportMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_transactions = ' . $this->sqlTotalTransactions() . ','; } elseif( true == array_key_exists( 'TotalTransactions', $this->getChangedColumns() ) ) { $strSql .= ' total_transactions = ' . $this->sqlTotalTransactions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_pass = ' . $this->sqlTotalPass() . ','; } elseif( true == array_key_exists( 'TotalPass', $this->getChangedColumns() ) ) { $strSql .= ' total_pass = ' . $this->sqlTotalPass() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_conditional = ' . $this->sqlTotalConditional() . ','; } elseif( true == array_key_exists( 'TotalConditional', $this->getChangedColumns() ) ) { $strSql .= ' total_conditional = ' . $this->sqlTotalConditional() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_fail = ' . $this->sqlTotalFail() . ','; } elseif( true == array_key_exists( 'TotalFail', $this->getChangedColumns() ) ) { $strSql .= ' total_fail = ' . $this->sqlTotalFail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_inprogress = ' . $this->sqlTotalInprogress() . ','; } elseif( true == array_key_exists( 'TotalInprogress', $this->getChangedColumns() ) ) { $strSql .= ' total_inprogress = ' . $this->sqlTotalInprogress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_error = ' . $this->sqlTotalError() . ','; } elseif( true == array_key_exists( 'TotalError', $this->getChangedColumns() ) ) { $strSql .= ' total_error = ' . $this->sqlTotalError() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_requested_on = ' . $this->sqlAverageRequestedOn() . ','; } elseif( true == array_key_exists( 'AverageRequestedOn', $this->getChangedColumns() ) ) { $strSql .= ' average_requested_on = ' . $this->sqlAverageRequestedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_response_completed_on = ' . $this->sqlAverageResponseCompletedOn() . ','; } elseif( true == array_key_exists( 'AverageResponseCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' average_response_completed_on = ' . $this->sqlAverageResponseCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_completed_on = ' . $this->sqlAverageCompletedOn() . ','; } elseif( true == array_key_exists( 'AverageCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' average_completed_on = ' . $this->sqlAverageCompletedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'screen_type_id' => $this->getScreenTypeId(),
			'revenue' => $this->getRevenue(),
			'report_month' => $this->getReportMonth(),
			'total_transactions' => $this->getTotalTransactions(),
			'total_pass' => $this->getTotalPass(),
			'total_conditional' => $this->getTotalConditional(),
			'total_fail' => $this->getTotalFail(),
			'total_inprogress' => $this->getTotalInprogress(),
			'total_error' => $this->getTotalError(),
			'average_requested_on' => $this->getAverageRequestedOn(),
			'average_response_completed_on' => $this->getAverageResponseCompletedOn(),
			'average_completed_on' => $this->getAverageCompletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>