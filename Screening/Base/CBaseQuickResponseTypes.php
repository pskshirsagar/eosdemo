<?php

class CBaseQuickResponseTypes extends CEosPluralBase {

	/**
	 * @return CQuickResponseType[]
	 */
	public static function fetchQuickResponseTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CQuickResponseType::class, $objDatabase );
	}

	/**
	 * @return CQuickResponseType
	 */
	public static function fetchQuickResponseType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CQuickResponseType::class, $objDatabase );
	}

	public static function fetchQuickResponseTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'quick_response_types', $objDatabase );
	}

	public static function fetchQuickResponseTypeById( $intId, $objDatabase ) {
		return self::fetchQuickResponseType( sprintf( 'SELECT * FROM quick_response_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>