<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningWebServices
 * Do not add any new functions to this class.
 */

class CBaseScreeningWebServices extends CEosPluralBase {

	/**
	 * @return CScreeningWebService[]
	 */
	public static function fetchScreeningWebServices( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningWebService', $objDatabase );
	}

	/**
	 * @return CScreeningWebService
	 */
	public static function fetchScreeningWebService( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningWebService', $objDatabase );
	}

	public static function fetchScreeningWebServiceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_web_services', $objDatabase );
	}

	public static function fetchScreeningWebServiceById( $intId, $objDatabase ) {
		return self::fetchScreeningWebService( sprintf( 'SELECT * FROM screening_web_services WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>