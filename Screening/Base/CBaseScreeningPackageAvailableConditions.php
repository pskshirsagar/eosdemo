<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageAvailableConditions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningPackageAvailableConditions extends CEosPluralBase {

	/**
	 * @return CScreeningPackageAvailableCondition[]
	 */
	public static function fetchScreeningPackageAvailableConditions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageAvailableCondition', $objDatabase );
	}

	/**
	 * @return CScreeningPackageAvailableCondition
	 */
	public static function fetchScreeningPackageAvailableCondition( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageAvailableCondition', $objDatabase );
	}

	public static function fetchScreeningPackageAvailableConditionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_available_conditions', $objDatabase );
	}

	public static function fetchScreeningPackageAvailableConditionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageAvailableCondition( sprintf( 'SELECT * FROM screening_package_available_conditions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageAvailableConditionsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningPackageAvailableConditions( sprintf( 'SELECT * FROM screening_package_available_conditions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageAvailableConditionsByScreeningPackageConditionTypeIdByCid( $intScreeningPackageConditionTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageAvailableConditions( sprintf( 'SELECT * FROM screening_package_available_conditions WHERE screening_package_condition_type_id = %d AND cid = %d', ( int ) $intScreeningPackageConditionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageAvailableConditionsByScreeningPackageChargeCodeAmountTypeIdByCid( $intScreeningPackageChargeCodeAmountTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageAvailableConditions( sprintf( 'SELECT * FROM screening_package_available_conditions WHERE screening_package_charge_code_amount_type_id = %d AND cid = %d', ( int ) $intScreeningPackageChargeCodeAmountTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageAvailableConditionsByBaseChargeCodeIdByCid( $intBaseChargeCodeId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageAvailableConditions( sprintf( 'SELECT * FROM screening_package_available_conditions WHERE base_charge_code_id = %d AND cid = %d', ( int ) $intBaseChargeCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageAvailableConditionsByApplyToChargeCodeIdByCid( $intApplyToChargeCodeId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageAvailableConditions( sprintf( 'SELECT * FROM screening_package_available_conditions WHERE apply_to_charge_code_id = %d AND cid = %d', ( int ) $intApplyToChargeCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageAvailableConditionsByChargeTypeIdByCid( $intChargeTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageAvailableConditions( sprintf( 'SELECT * FROM screening_package_available_conditions WHERE charge_type_id = %d AND cid = %d', ( int ) $intChargeTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>