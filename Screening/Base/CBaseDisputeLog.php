<?php

class CBaseDisputeLog extends CEosSingularBase {

	const TABLE_NAME = 'public.dispute_logs';

	protected $m_intId;
	protected $m_intConsumerId;
	protected $m_intConsumerDisputeId;
	protected $m_intLogTypeId;
	protected $m_strOldData;
	protected $m_strNewData;
	protected $m_strNotes;
	protected $m_strReferenceNumber;
	protected $m_intAssignedBy;
	protected $m_intAssignedTo;
	protected $m_boolIsSharedNote;
	protected $m_boolIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['consumer_id'] ) && $boolDirectSet ) $this->set( 'm_intConsumerId', trim( $arrValues['consumer_id'] ) ); elseif( isset( $arrValues['consumer_id'] ) ) $this->setConsumerId( $arrValues['consumer_id'] );
		if( isset( $arrValues['consumer_dispute_id'] ) && $boolDirectSet ) $this->set( 'm_intConsumerDisputeId', trim( $arrValues['consumer_dispute_id'] ) ); elseif( isset( $arrValues['consumer_dispute_id'] ) ) $this->setConsumerDisputeId( $arrValues['consumer_dispute_id'] );
		if( isset( $arrValues['log_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLogTypeId', trim( $arrValues['log_type_id'] ) ); elseif( isset( $arrValues['log_type_id'] ) ) $this->setLogTypeId( $arrValues['log_type_id'] );
		if( isset( $arrValues['old_data'] ) && $boolDirectSet ) $this->set( 'm_strOldData', trim( stripcslashes( $arrValues['old_data'] ) ) ); elseif( isset( $arrValues['old_data'] ) ) $this->setOldData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['old_data'] ) : $arrValues['old_data'] );
		if( isset( $arrValues['new_data'] ) && $boolDirectSet ) $this->set( 'm_strNewData', trim( stripcslashes( $arrValues['new_data'] ) ) ); elseif( isset( $arrValues['new_data'] ) ) $this->setNewData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_data'] ) : $arrValues['new_data'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['reference_number'] ) && $boolDirectSet ) $this->set( 'm_strReferenceNumber', trim( stripcslashes( $arrValues['reference_number'] ) ) ); elseif( isset( $arrValues['reference_number'] ) ) $this->setReferenceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reference_number'] ) : $arrValues['reference_number'] );
		if( isset( $arrValues['assigned_by'] ) && $boolDirectSet ) $this->set( 'm_intAssignedBy', trim( $arrValues['assigned_by'] ) ); elseif( isset( $arrValues['assigned_by'] ) ) $this->setAssignedBy( $arrValues['assigned_by'] );
		if( isset( $arrValues['assigned_to'] ) && $boolDirectSet ) $this->set( 'm_intAssignedTo', trim( $arrValues['assigned_to'] ) ); elseif( isset( $arrValues['assigned_to'] ) ) $this->setAssignedTo( $arrValues['assigned_to'] );
		if( isset( $arrValues['is_shared_note'] ) && $boolDirectSet ) $this->set( 'm_boolIsSharedNote', trim( stripcslashes( $arrValues['is_shared_note'] ) ) ); elseif( isset( $arrValues['is_shared_note'] ) ) $this->setIsSharedNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_shared_note'] ) : $arrValues['is_shared_note'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setConsumerId( $intConsumerId ) {
		$this->set( 'm_intConsumerId', CStrings::strToIntDef( $intConsumerId, NULL, false ) );
	}

	public function getConsumerId() {
		return $this->m_intConsumerId;
	}

	public function sqlConsumerId() {
		return ( true == isset( $this->m_intConsumerId ) ) ? ( string ) $this->m_intConsumerId : 'NULL';
	}

	public function setConsumerDisputeId( $intConsumerDisputeId ) {
		$this->set( 'm_intConsumerDisputeId', CStrings::strToIntDef( $intConsumerDisputeId, NULL, false ) );
	}

	public function getConsumerDisputeId() {
		return $this->m_intConsumerDisputeId;
	}

	public function sqlConsumerDisputeId() {
		return ( true == isset( $this->m_intConsumerDisputeId ) ) ? ( string ) $this->m_intConsumerDisputeId : 'NULL';
	}

	public function setLogTypeId( $intLogTypeId ) {
		$this->set( 'm_intLogTypeId', CStrings::strToIntDef( $intLogTypeId, NULL, false ) );
	}

	public function getLogTypeId() {
		return $this->m_intLogTypeId;
	}

	public function sqlLogTypeId() {
		return ( true == isset( $this->m_intLogTypeId ) ) ? ( string ) $this->m_intLogTypeId : 'NULL';
	}

	public function setOldData( $strOldData ) {
		$this->set( 'm_strOldData', CStrings::strTrimDef( $strOldData, 500, NULL, true ) );
	}

	public function getOldData() {
		return $this->m_strOldData;
	}

	public function sqlOldData() {
		return ( true == isset( $this->m_strOldData ) ) ? '\'' . addslashes( $this->m_strOldData ) . '\'' : 'NULL';
	}

	public function setNewData( $strNewData ) {
		$this->set( 'm_strNewData', CStrings::strTrimDef( $strNewData, 500, NULL, true ) );
	}

	public function getNewData() {
		return $this->m_strNewData;
	}

	public function sqlNewData() {
		return ( true == isset( $this->m_strNewData ) ) ? '\'' . addslashes( $this->m_strNewData ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setReferenceNumber( $strReferenceNumber ) {
		$this->set( 'm_strReferenceNumber', CStrings::strTrimDef( $strReferenceNumber, -1, NULL, true ) );
	}

	public function getReferenceNumber() {
		return $this->m_strReferenceNumber;
	}

	public function sqlReferenceNumber() {
		return ( true == isset( $this->m_strReferenceNumber ) ) ? '\'' . addslashes( $this->m_strReferenceNumber ) . '\'' : 'NULL';
	}

	public function setAssignedBy( $intAssignedBy ) {
		$this->set( 'm_intAssignedBy', CStrings::strToIntDef( $intAssignedBy, NULL, false ) );
	}

	public function getAssignedBy() {
		return $this->m_intAssignedBy;
	}

	public function sqlAssignedBy() {
		return ( true == isset( $this->m_intAssignedBy ) ) ? ( string ) $this->m_intAssignedBy : 'NULL';
	}

	public function setAssignedTo( $intAssignedTo ) {
		$this->set( 'm_intAssignedTo', CStrings::strToIntDef( $intAssignedTo, NULL, false ) );
	}

	public function getAssignedTo() {
		return $this->m_intAssignedTo;
	}

	public function sqlAssignedTo() {
		return ( true == isset( $this->m_intAssignedTo ) ) ? ( string ) $this->m_intAssignedTo : 'NULL';
	}

	public function setIsSharedNote( $boolIsSharedNote ) {
		$this->set( 'm_boolIsSharedNote', CStrings::strToBool( $boolIsSharedNote ) );
	}

	public function getIsSharedNote() {
		return $this->m_boolIsSharedNote;
	}

	public function sqlIsSharedNote() {
		return ( true == isset( $this->m_boolIsSharedNote ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSharedNote ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, consumer_id, consumer_dispute_id, log_type_id, old_data, new_data, notes, reference_number, assigned_by, assigned_to, is_shared_note, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlConsumerId() . ', ' .
 						$this->sqlConsumerDisputeId() . ', ' .
 						$this->sqlLogTypeId() . ', ' .
 						$this->sqlOldData() . ', ' .
 						$this->sqlNewData() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlReferenceNumber() . ', ' .
 						$this->sqlAssignedBy() . ', ' .
 						$this->sqlAssignedTo() . ', ' .
 						$this->sqlIsSharedNote() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumer_id = ' . $this->sqlConsumerId() . ','; } elseif( true == array_key_exists( 'ConsumerId', $this->getChangedColumns() ) ) { $strSql .= ' consumer_id = ' . $this->sqlConsumerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumer_dispute_id = ' . $this->sqlConsumerDisputeId() . ','; } elseif( true == array_key_exists( 'ConsumerDisputeId', $this->getChangedColumns() ) ) { $strSql .= ' consumer_dispute_id = ' . $this->sqlConsumerDisputeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_type_id = ' . $this->sqlLogTypeId() . ','; } elseif( true == array_key_exists( 'LogTypeId', $this->getChangedColumns() ) ) { $strSql .= ' log_type_id = ' . $this->sqlLogTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_data = ' . $this->sqlOldData() . ','; } elseif( true == array_key_exists( 'OldData', $this->getChangedColumns() ) ) { $strSql .= ' old_data = ' . $this->sqlOldData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_data = ' . $this->sqlNewData() . ','; } elseif( true == array_key_exists( 'NewData', $this->getChangedColumns() ) ) { $strSql .= ' new_data = ' . $this->sqlNewData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; } elseif( true == array_key_exists( 'ReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assigned_by = ' . $this->sqlAssignedBy() . ','; } elseif( true == array_key_exists( 'AssignedBy', $this->getChangedColumns() ) ) { $strSql .= ' assigned_by = ' . $this->sqlAssignedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assigned_to = ' . $this->sqlAssignedTo() . ','; } elseif( true == array_key_exists( 'AssignedTo', $this->getChangedColumns() ) ) { $strSql .= ' assigned_to = ' . $this->sqlAssignedTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_shared_note = ' . $this->sqlIsSharedNote() . ','; } elseif( true == array_key_exists( 'IsSharedNote', $this->getChangedColumns() ) ) { $strSql .= ' is_shared_note = ' . $this->sqlIsSharedNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'consumer_id' => $this->getConsumerId(),
			'consumer_dispute_id' => $this->getConsumerDisputeId(),
			'log_type_id' => $this->getLogTypeId(),
			'old_data' => $this->getOldData(),
			'new_data' => $this->getNewData(),
			'notes' => $this->getNotes(),
			'reference_number' => $this->getReferenceNumber(),
			'assigned_by' => $this->getAssignedBy(),
			'assigned_to' => $this->getAssignedTo(),
			'is_shared_note' => $this->getIsSharedNote(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>