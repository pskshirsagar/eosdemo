<?php

class CBaseRvTestRunBatches extends CEosPluralBase {

	/**
	 * @return CRvTestRunBatch[]
	 */
	public static function fetchRvTestRunBatches( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRvTestRunBatch::class, $objDatabase );
	}

	/**
	 * @return CRvTestRunBatch
	 */
	public static function fetchRvTestRunBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRvTestRunBatch::class, $objDatabase );
	}

	public static function fetchRvTestRunBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'rv_test_run_batches', $objDatabase );
	}

	public static function fetchRvTestRunBatchById( $intId, $objDatabase ) {
		return self::fetchRvTestRunBatch( sprintf( 'SELECT * FROM rv_test_run_batches WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchRvTestRunBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchRvTestRunBatches( sprintf( 'SELECT * FROM rv_test_run_batches WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>