<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConfigPreferenceTypeFilters
 * Do not add any new functions to this class.
 */

class CBaseScreeningConfigPreferenceTypeFilters extends CEosPluralBase {

	/**
	 * @return CScreeningConfigPreferenceTypeFilter[]
	 */
	public static function fetchScreeningConfigPreferenceTypeFilters( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningConfigPreferenceTypeFilter', $objDatabase );
	}

	/**
	 * @return CScreeningConfigPreferenceTypeFilter
	 */
	public static function fetchScreeningConfigPreferenceTypeFilter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningConfigPreferenceTypeFilter', $objDatabase );
	}

	public static function fetchScreeningConfigPreferenceTypeFilterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_config_preference_type_filters', $objDatabase );
	}

	public static function fetchScreeningConfigPreferenceTypeFilterById( $intId, $objDatabase ) {
		return self::fetchScreeningConfigPreferenceTypeFilter( sprintf( 'SELECT * FROM screening_config_preference_type_filters WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningConfigPreferenceTypeFiltersByScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		return self::fetchScreeningConfigPreferenceTypeFilters( sprintf( 'SELECT * FROM screening_config_preference_type_filters WHERE screening_config_preference_type_id = %d', ( int ) $intScreeningConfigPreferenceTypeId ), $objDatabase );
	}

	public static function fetchScreeningConfigPreferenceTypeFiltersByScreeningFilterId( $intScreeningFilterId, $objDatabase ) {
		return self::fetchScreeningConfigPreferenceTypeFilters( sprintf( 'SELECT * FROM screening_config_preference_type_filters WHERE screening_filter_id = %d', ( int ) $intScreeningFilterId ), $objDatabase );
	}

}
?>