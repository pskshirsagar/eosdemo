<?php

class CBaseRvLeaseApplicantNewDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.rv_lease_applicant_new_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intRvLeaseNewDetailsId;
	protected $m_intScreeningApplicantId;
	protected $m_intScreeningApplicantTypeId;
	protected $m_fltRvApplicantIncome;
	protected $m_boolIsTransfered;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsScreenedForCredit;
	protected $m_strScreenedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltRvApplicantIncome = '0';
		$this->m_boolIsTransfered = false;
		$this->m_boolIsScreenedForCredit = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['rv_lease_new_details_id'] ) && $boolDirectSet ) $this->set( 'm_intRvLeaseNewDetailsId', trim( $arrValues['rv_lease_new_details_id'] ) ); elseif( isset( $arrValues['rv_lease_new_details_id'] ) ) $this->setRvLeaseNewDetailsId( $arrValues['rv_lease_new_details_id'] );
		if( isset( $arrValues['screening_applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicantId', trim( $arrValues['screening_applicant_id'] ) ); elseif( isset( $arrValues['screening_applicant_id'] ) ) $this->setScreeningApplicantId( $arrValues['screening_applicant_id'] );
		if( isset( $arrValues['screening_applicant_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicantTypeId', trim( $arrValues['screening_applicant_type_id'] ) ); elseif( isset( $arrValues['screening_applicant_type_id'] ) ) $this->setScreeningApplicantTypeId( $arrValues['screening_applicant_type_id'] );
		if( isset( $arrValues['rv_applicant_income'] ) && $boolDirectSet ) $this->set( 'm_fltRvApplicantIncome', trim( $arrValues['rv_applicant_income'] ) ); elseif( isset( $arrValues['rv_applicant_income'] ) ) $this->setRvApplicantIncome( $arrValues['rv_applicant_income'] );
		if( isset( $arrValues['is_transfered'] ) && $boolDirectSet ) $this->set( 'm_boolIsTransfered', trim( stripcslashes( $arrValues['is_transfered'] ) ) ); elseif( isset( $arrValues['is_transfered'] ) ) $this->setIsTransfered( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_transfered'] ) : $arrValues['is_transfered'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_screened_for_credit'] ) && $boolDirectSet ) $this->set( 'm_boolIsScreenedForCredit', trim( stripcslashes( $arrValues['is_screened_for_credit'] ) ) ); elseif( isset( $arrValues['is_screened_for_credit'] ) ) $this->setIsScreenedForCredit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_screened_for_credit'] ) : $arrValues['is_screened_for_credit'] );
		if( isset( $arrValues['screened_on'] ) && $boolDirectSet ) $this->set( 'm_strScreenedOn', trim( $arrValues['screened_on'] ) ); elseif( isset( $arrValues['screened_on'] ) ) $this->setScreenedOn( $arrValues['screened_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setRvLeaseNewDetailsId( $intRvLeaseNewDetailsId ) {
		$this->set( 'm_intRvLeaseNewDetailsId', CStrings::strToIntDef( $intRvLeaseNewDetailsId, NULL, false ) );
	}

	public function getRvLeaseNewDetailsId() {
		return $this->m_intRvLeaseNewDetailsId;
	}

	public function sqlRvLeaseNewDetailsId() {
		return ( true == isset( $this->m_intRvLeaseNewDetailsId ) ) ? ( string ) $this->m_intRvLeaseNewDetailsId : 'NULL';
	}

	public function setScreeningApplicantId( $intScreeningApplicantId ) {
		$this->set( 'm_intScreeningApplicantId', CStrings::strToIntDef( $intScreeningApplicantId, NULL, false ) );
	}

	public function getScreeningApplicantId() {
		return $this->m_intScreeningApplicantId;
	}

	public function sqlScreeningApplicantId() {
		return ( true == isset( $this->m_intScreeningApplicantId ) ) ? ( string ) $this->m_intScreeningApplicantId : 'NULL';
	}

	public function setScreeningApplicantTypeId( $intScreeningApplicantTypeId ) {
		$this->set( 'm_intScreeningApplicantTypeId', CStrings::strToIntDef( $intScreeningApplicantTypeId, NULL, false ) );
	}

	public function getScreeningApplicantTypeId() {
		return $this->m_intScreeningApplicantTypeId;
	}

	public function sqlScreeningApplicantTypeId() {
		return ( true == isset( $this->m_intScreeningApplicantTypeId ) ) ? ( string ) $this->m_intScreeningApplicantTypeId : 'NULL';
	}

	public function setRvApplicantIncome( $fltRvApplicantIncome ) {
		$this->set( 'm_fltRvApplicantIncome', CStrings::strToFloatDef( $fltRvApplicantIncome, NULL, false, 2 ) );
	}

	public function getRvApplicantIncome() {
		return $this->m_fltRvApplicantIncome;
	}

	public function sqlRvApplicantIncome() {
		return ( true == isset( $this->m_fltRvApplicantIncome ) ) ? ( string ) $this->m_fltRvApplicantIncome : '0';
	}

	public function setIsTransfered( $boolIsTransfered ) {
		$this->set( 'm_boolIsTransfered', CStrings::strToBool( $boolIsTransfered ) );
	}

	public function getIsTransfered() {
		return $this->m_boolIsTransfered;
	}

	public function sqlIsTransfered() {
		return ( true == isset( $this->m_boolIsTransfered ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTransfered ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsScreenedForCredit( $boolIsScreenedForCredit ) {
		$this->set( 'm_boolIsScreenedForCredit', CStrings::strToBool( $boolIsScreenedForCredit ) );
	}

	public function getIsScreenedForCredit() {
		return $this->m_boolIsScreenedForCredit;
	}

	public function sqlIsScreenedForCredit() {
		return ( true == isset( $this->m_boolIsScreenedForCredit ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsScreenedForCredit ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setScreenedOn( $strScreenedOn ) {
		$this->set( 'm_strScreenedOn', CStrings::strTrimDef( $strScreenedOn, -1, NULL, true ) );
	}

	public function getScreenedOn() {
		return $this->m_strScreenedOn;
	}

	public function sqlScreenedOn() {
		return ( true == isset( $this->m_strScreenedOn ) ) ? '\'' . $this->m_strScreenedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, rv_lease_new_details_id, screening_applicant_id, screening_applicant_type_id, rv_applicant_income, is_transfered, updated_by, updated_on, created_by, created_on, is_screened_for_credit, screened_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlRvLeaseNewDetailsId() . ', ' .
						$this->sqlScreeningApplicantId() . ', ' .
						$this->sqlScreeningApplicantTypeId() . ', ' .
						$this->sqlRvApplicantIncome() . ', ' .
						$this->sqlIsTransfered() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsScreenedForCredit() . ', ' .
						$this->sqlScreenedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rv_lease_new_details_id = ' . $this->sqlRvLeaseNewDetailsId(). ',' ; } elseif( true == array_key_exists( 'RvLeaseNewDetailsId', $this->getChangedColumns() ) ) { $strSql .= ' rv_lease_new_details_id = ' . $this->sqlRvLeaseNewDetailsId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId(). ',' ; } elseif( true == array_key_exists( 'ScreeningApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_type_id = ' . $this->sqlScreeningApplicantTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningApplicantTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_type_id = ' . $this->sqlScreeningApplicantTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rv_applicant_income = ' . $this->sqlRvApplicantIncome(). ',' ; } elseif( true == array_key_exists( 'RvApplicantIncome', $this->getChangedColumns() ) ) { $strSql .= ' rv_applicant_income = ' . $this->sqlRvApplicantIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_transfered = ' . $this->sqlIsTransfered(). ',' ; } elseif( true == array_key_exists( 'IsTransfered', $this->getChangedColumns() ) ) { $strSql .= ' is_transfered = ' . $this->sqlIsTransfered() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_screened_for_credit = ' . $this->sqlIsScreenedForCredit(). ',' ; } elseif( true == array_key_exists( 'IsScreenedForCredit', $this->getChangedColumns() ) ) { $strSql .= ' is_screened_for_credit = ' . $this->sqlIsScreenedForCredit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screened_on = ' . $this->sqlScreenedOn(). ',' ; } elseif( true == array_key_exists( 'ScreenedOn', $this->getChangedColumns() ) ) { $strSql .= ' screened_on = ' . $this->sqlScreenedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'rv_lease_new_details_id' => $this->getRvLeaseNewDetailsId(),
			'screening_applicant_id' => $this->getScreeningApplicantId(),
			'screening_applicant_type_id' => $this->getScreeningApplicantTypeId(),
			'rv_applicant_income' => $this->getRvApplicantIncome(),
			'is_transfered' => $this->getIsTransfered(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_screened_for_credit' => $this->getIsScreenedForCredit(),
			'screened_on' => $this->getScreenedOn()
		);
	}

}
?>