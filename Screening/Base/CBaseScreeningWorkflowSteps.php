<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningWorkflowSteps
 * Do not add any new functions to this class.
 */

class CBaseScreeningWorkflowSteps extends CEosPluralBase {

	/**
	 * @return CScreeningWorkflowStep[]
	 */
	public static function fetchScreeningWorkflowSteps( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningWorkflowStep::class, $objDatabase );
	}

	/**
	 * @return CScreeningWorkflowStep
	 */
	public static function fetchScreeningWorkflowStep( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningWorkflowStep::class, $objDatabase );
	}

	public static function fetchScreeningWorkflowStepCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_workflow_steps', $objDatabase );
	}

	public static function fetchScreeningWorkflowStepById( $intId, $objDatabase ) {
		return self::fetchScreeningWorkflowStep( sprintf( 'SELECT * FROM screening_workflow_steps WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchScreeningWorkflowStepsByScreeningWorkflowSetupId( $intScreeningWorkflowSetupId, $objDatabase ) {
		return self::fetchScreeningWorkflowSteps( sprintf( 'SELECT * FROM screening_workflow_steps WHERE screening_workflow_setup_id = %d', $intScreeningWorkflowSetupId ), $objDatabase );
	}

	public static function fetchScreeningWorkflowStepsByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreeningWorkflowSteps( sprintf( 'SELECT * FROM screening_workflow_steps WHERE screen_type_id = %d', $intScreenTypeId ), $objDatabase );
	}

	public static function fetchScreeningWorkflowStepsByCriteriaId( $intCriteriaId, $objDatabase ) {
		return self::fetchScreeningWorkflowSteps( sprintf( 'SELECT * FROM screening_workflow_steps WHERE criteria_id = %d', $intCriteriaId ), $objDatabase );
	}

	public static function fetchScreeningWorkflowStepsByNextWorkflowScreenTypeId( $intNextWorkflowScreenTypeId, $objDatabase ) {
		return self::fetchScreeningWorkflowSteps( sprintf( 'SELECT * FROM screening_workflow_steps WHERE next_workflow_screen_type_id = %d', $intNextWorkflowScreenTypeId ), $objDatabase );
	}

}
?>