<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConfigValueTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningConfigValueTypes extends CEosPluralBase {

	/**
	 * @return CScreeningConfigValueType[]
	 */
	public static function fetchScreeningConfigValueTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningConfigValueType', $objDatabase );
	}

	/**
	 * @return CScreeningConfigValueType
	 */
	public static function fetchScreeningConfigValueType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningConfigValueType', $objDatabase );
	}

	public static function fetchScreeningConfigValueTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_config_value_types', $objDatabase );
	}

	public static function fetchScreeningConfigValueTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningConfigValueType( sprintf( 'SELECT * FROM screening_config_value_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>