<?php

class CBaseScreeningPackageTypeScreenTypeAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_package_type_screen_type_associations';

	protected $m_intId;
	protected $m_intScreeningPackageTypeId;
	protected $m_intScreenTypeId;
	protected $m_intIsPublished;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
        $this->m_intOrderNum = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_package_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageTypeId', trim( $arrValues['screening_package_type_id'] ) ); elseif( isset( $arrValues['screening_package_type_id'] ) ) $this->setScreeningPackageTypeId( $arrValues['screening_package_type_id'] );
		if( isset( $arrValues['screen_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreenTypeId', trim( $arrValues['screen_type_id'] ) ); elseif( isset( $arrValues['screen_type_id'] ) ) $this->setScreenTypeId( $arrValues['screen_type_id'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningPackageTypeId( $intScreeningPackageTypeId ) {
		$this->set( 'm_intScreeningPackageTypeId', CStrings::strToIntDef( $intScreeningPackageTypeId, NULL, false ) );
	}

	public function getScreeningPackageTypeId() {
		return $this->m_intScreeningPackageTypeId;
	}

	public function sqlScreeningPackageTypeId() {
		return ( true == isset( $this->m_intScreeningPackageTypeId ) ) ? ( string ) $this->m_intScreeningPackageTypeId : 'NULL';
	}

	public function setScreenTypeId( $intScreenTypeId ) {
		$this->set( 'm_intScreenTypeId', CStrings::strToIntDef( $intScreenTypeId, NULL, false ) );
	}

	public function getScreenTypeId() {
		return $this->m_intScreenTypeId;
	}

	public function sqlScreenTypeId() {
		return ( true == isset( $this->m_intScreenTypeId ) ) ? ( string ) $this->m_intScreenTypeId : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '1';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_package_type_id' => $this->getScreeningPackageTypeId(),
			'screen_type_id' => $this->getScreenTypeId(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>