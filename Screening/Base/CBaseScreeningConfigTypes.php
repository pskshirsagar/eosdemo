<?php

class CBaseScreeningConfigTypes extends CEosPluralBase {

    const TABLE_SCREENING_CONFIG_TYPES = 'public.screening_config_types';

    public static function fetchScreeningConfigTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CScreeningConfigType', $objDatabase );
    }

    public static function fetchScreeningConfigType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CScreeningConfigType', $objDatabase );
    }

    public static function fetchScreeningConfigTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'screening_config_types', $objDatabase );
    }

    public static function fetchScreeningConfigTypeById( $intId, $objDatabase ) {
        return self::fetchScreeningConfigType( sprintf( 'SELECT * FROM screening_config_types WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>