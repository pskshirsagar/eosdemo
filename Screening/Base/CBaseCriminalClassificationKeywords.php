<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CCriminalClassificationKeywords
 * Do not add any new functions to this class.
 */

class CBaseCriminalClassificationKeywords extends CEosPluralBase {

	/**
	 * @return CCriminalClassificationKeyword[]
	 */
	public static function fetchCriminalClassificationKeywords( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCriminalClassificationKeyword::class, $objDatabase );
	}

	/**
	 * @return CCriminalClassificationKeyword
	 */
	public static function fetchCriminalClassificationKeyword( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCriminalClassificationKeyword::class, $objDatabase );
	}

	public static function fetchCriminalClassificationKeywordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'criminal_classification_keywords', $objDatabase );
	}

	public static function fetchCriminalClassificationKeywordById( $intId, $objDatabase ) {
		return self::fetchCriminalClassificationKeyword( sprintf( 'SELECT * FROM criminal_classification_keywords WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCriminalClassificationKeywordsByKeywordTypeId( $intKeywordTypeId, $objDatabase ) {
		return self::fetchCriminalClassificationKeywords( sprintf( 'SELECT * FROM criminal_classification_keywords WHERE keyword_type_id = %d', ( int ) $intKeywordTypeId ), $objDatabase );
	}

	public static function fetchCriminalClassificationKeywordsByClassificationTypeId( $intClassificationTypeId, $objDatabase ) {
		return self::fetchCriminalClassificationKeywords( sprintf( 'SELECT * FROM criminal_classification_keywords WHERE classification_type_id = %d', ( int ) $intClassificationTypeId ), $objDatabase );
	}

}
?>