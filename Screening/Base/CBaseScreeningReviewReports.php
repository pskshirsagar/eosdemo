<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningReviewReports
 * Do not add any new functions to this class.
 */

class CBaseScreeningReviewReports extends CEosPluralBase {

	/**
	 * @return CScreeningReviewReport[]
	 */
	public static function fetchScreeningReviewReports( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningReviewReport::class, $objDatabase );
	}

	/**
	 * @return CScreeningReviewReport
	 */
	public static function fetchScreeningReviewReport( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningReviewReport::class, $objDatabase );
	}

	public static function fetchScreeningReviewReportCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_review_reports', $objDatabase );
	}

	public static function fetchScreeningReviewReportById( $intId, $objDatabase ) {
		return self::fetchScreeningReviewReport( sprintf( 'SELECT * FROM screening_review_reports WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningReviewReportsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningReviewReports( sprintf( 'SELECT * FROM screening_review_reports WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningReviewReportsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchScreeningReviewReports( sprintf( 'SELECT * FROM screening_review_reports WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchScreeningReviewReportsByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreeningReviewReports( sprintf( 'SELECT * FROM screening_review_reports WHERE screen_type_id = %d', ( int ) $intScreenTypeId ), $objDatabase );
	}

}
?>