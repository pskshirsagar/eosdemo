<?php

class CBaseCriminalCriteriaSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.criminal_criteria_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScreeningCriteriaId;
	protected $m_intCriminalClassificationTypeId;
	protected $m_intFelonyScreeningPackageConditionSetId;
	protected $m_intMisdemeanorScreeningPackageConditionSetId;
	protected $m_fltFelonyYears;
	protected $m_fltMisdemeanorYears;
	protected $m_intClientCriminalClassificationSubtypeId;
	protected $m_intIsActive;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsActive = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screening_criteria_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningCriteriaId', trim( $arrValues['screening_criteria_id'] ) ); elseif( isset( $arrValues['screening_criteria_id'] ) ) $this->setScreeningCriteriaId( $arrValues['screening_criteria_id'] );
		if( isset( $arrValues['criminal_classification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCriminalClassificationTypeId', trim( $arrValues['criminal_classification_type_id'] ) ); elseif( isset( $arrValues['criminal_classification_type_id'] ) ) $this->setCriminalClassificationTypeId( $arrValues['criminal_classification_type_id'] );
		if( isset( $arrValues['felony_screening_package_condition_set_id'] ) && $boolDirectSet ) $this->set( 'm_intFelonyScreeningPackageConditionSetId', trim( $arrValues['felony_screening_package_condition_set_id'] ) ); elseif( isset( $arrValues['felony_screening_package_condition_set_id'] ) ) $this->setFelonyScreeningPackageConditionSetId( $arrValues['felony_screening_package_condition_set_id'] );
		if( isset( $arrValues['misdemeanor_screening_package_condition_set_id'] ) && $boolDirectSet ) $this->set( 'm_intMisdemeanorScreeningPackageConditionSetId', trim( $arrValues['misdemeanor_screening_package_condition_set_id'] ) ); elseif( isset( $arrValues['misdemeanor_screening_package_condition_set_id'] ) ) $this->setMisdemeanorScreeningPackageConditionSetId( $arrValues['misdemeanor_screening_package_condition_set_id'] );
		if( isset( $arrValues['felony_years'] ) && $boolDirectSet ) $this->set( 'm_fltFelonyYears', trim( $arrValues['felony_years'] ) ); elseif( isset( $arrValues['felony_years'] ) ) $this->setFelonyYears( $arrValues['felony_years'] );
		if( isset( $arrValues['misdemeanor_years'] ) && $boolDirectSet ) $this->set( 'm_fltMisdemeanorYears', trim( $arrValues['misdemeanor_years'] ) ); elseif( isset( $arrValues['misdemeanor_years'] ) ) $this->setMisdemeanorYears( $arrValues['misdemeanor_years'] );
		if( isset( $arrValues['client_criminal_classification_subtype_id'] ) && $boolDirectSet ) $this->set( 'm_intClientCriminalClassificationSubtypeId', trim( $arrValues['client_criminal_classification_subtype_id'] ) ); elseif( isset( $arrValues['client_criminal_classification_subtype_id'] ) ) $this->setClientCriminalClassificationSubtypeId( $arrValues['client_criminal_classification_subtype_id'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_intIsActive', trim( $arrValues['is_active'] ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( $arrValues['is_active'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreeningCriteriaId( $intScreeningCriteriaId ) {
		$this->set( 'm_intScreeningCriteriaId', CStrings::strToIntDef( $intScreeningCriteriaId, NULL, false ) );
	}

	public function getScreeningCriteriaId() {
		return $this->m_intScreeningCriteriaId;
	}

	public function sqlScreeningCriteriaId() {
		return ( true == isset( $this->m_intScreeningCriteriaId ) ) ? ( string ) $this->m_intScreeningCriteriaId : 'NULL';
	}

	public function setCriminalClassificationTypeId( $intCriminalClassificationTypeId ) {
		$this->set( 'm_intCriminalClassificationTypeId', CStrings::strToIntDef( $intCriminalClassificationTypeId, NULL, false ) );
	}

	public function getCriminalClassificationTypeId() {
		return $this->m_intCriminalClassificationTypeId;
	}

	public function sqlCriminalClassificationTypeId() {
		return ( true == isset( $this->m_intCriminalClassificationTypeId ) ) ? ( string ) $this->m_intCriminalClassificationTypeId : 'NULL';
	}

	public function setFelonyScreeningPackageConditionSetId( $intFelonyScreeningPackageConditionSetId ) {
		$this->set( 'm_intFelonyScreeningPackageConditionSetId', CStrings::strToIntDef( $intFelonyScreeningPackageConditionSetId, NULL, false ) );
	}

	public function getFelonyScreeningPackageConditionSetId() {
		return $this->m_intFelonyScreeningPackageConditionSetId;
	}

	public function sqlFelonyScreeningPackageConditionSetId() {
		return ( true == isset( $this->m_intFelonyScreeningPackageConditionSetId ) ) ? ( string ) $this->m_intFelonyScreeningPackageConditionSetId : 'NULL';
	}

	public function setMisdemeanorScreeningPackageConditionSetId( $intMisdemeanorScreeningPackageConditionSetId ) {
		$this->set( 'm_intMisdemeanorScreeningPackageConditionSetId', CStrings::strToIntDef( $intMisdemeanorScreeningPackageConditionSetId, NULL, false ) );
	}

	public function getMisdemeanorScreeningPackageConditionSetId() {
		return $this->m_intMisdemeanorScreeningPackageConditionSetId;
	}

	public function sqlMisdemeanorScreeningPackageConditionSetId() {
		return ( true == isset( $this->m_intMisdemeanorScreeningPackageConditionSetId ) ) ? ( string ) $this->m_intMisdemeanorScreeningPackageConditionSetId : 'NULL';
	}

	public function setFelonyYears( $fltFelonyYears ) {
		$this->set( 'm_fltFelonyYears', CStrings::strToFloatDef( $fltFelonyYears, NULL, false, 2 ) );
	}

	public function getFelonyYears() {
		return $this->m_fltFelonyYears;
	}

	public function sqlFelonyYears() {
		return ( true == isset( $this->m_fltFelonyYears ) ) ? ( string ) $this->m_fltFelonyYears : 'NULL';
	}

	public function setMisdemeanorYears( $fltMisdemeanorYears ) {
		$this->set( 'm_fltMisdemeanorYears', CStrings::strToFloatDef( $fltMisdemeanorYears, NULL, false, 2 ) );
	}

	public function getMisdemeanorYears() {
		return $this->m_fltMisdemeanorYears;
	}

	public function sqlMisdemeanorYears() {
		return ( true == isset( $this->m_fltMisdemeanorYears ) ) ? ( string ) $this->m_fltMisdemeanorYears : 'NULL';
	}

	public function setClientCriminalClassificationSubtypeId( $intClientCriminalClassificationSubtypeId ) {
		$this->set( 'm_intClientCriminalClassificationSubtypeId', CStrings::strToIntDef( $intClientCriminalClassificationSubtypeId, NULL, false ) );
	}

	public function getClientCriminalClassificationSubtypeId() {
		return $this->m_intClientCriminalClassificationSubtypeId;
	}

	public function sqlClientCriminalClassificationSubtypeId() {
		return ( true == isset( $this->m_intClientCriminalClassificationSubtypeId ) ) ? ( string ) $this->m_intClientCriminalClassificationSubtypeId : 'NULL';
	}

	public function setIsActive( $intIsActive ) {
		$this->set( 'm_intIsActive', CStrings::strToIntDef( $intIsActive, NULL, false ) );
	}

	public function getIsActive() {
		return $this->m_intIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_intIsActive ) ) ? ( string ) $this->m_intIsActive : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, screening_criteria_id, criminal_classification_type_id, felony_screening_package_condition_set_id, misdemeanor_screening_package_condition_set_id, felony_years, misdemeanor_years, client_criminal_classification_subtype_id, is_active, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlScreeningCriteriaId() . ', ' .
 						$this->sqlCriminalClassificationTypeId() . ', ' .
 						$this->sqlFelonyScreeningPackageConditionSetId() . ', ' .
 						$this->sqlMisdemeanorScreeningPackageConditionSetId() . ', ' .
 						$this->sqlFelonyYears() . ', ' .
 						$this->sqlMisdemeanorYears() . ', ' .
 						$this->sqlClientCriminalClassificationSubtypeId() . ', ' .
 						$this->sqlIsActive() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_criteria_id = ' . $this->sqlScreeningCriteriaId() . ','; } elseif( true == array_key_exists( 'ScreeningCriteriaId', $this->getChangedColumns() ) ) { $strSql .= ' screening_criteria_id = ' . $this->sqlScreeningCriteriaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' criminal_classification_type_id = ' . $this->sqlCriminalClassificationTypeId() . ','; } elseif( true == array_key_exists( 'CriminalClassificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' criminal_classification_type_id = ' . $this->sqlCriminalClassificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' felony_screening_package_condition_set_id = ' . $this->sqlFelonyScreeningPackageConditionSetId() . ','; } elseif( true == array_key_exists( 'FelonyScreeningPackageConditionSetId', $this->getChangedColumns() ) ) { $strSql .= ' felony_screening_package_condition_set_id = ' . $this->sqlFelonyScreeningPackageConditionSetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' misdemeanor_screening_package_condition_set_id = ' . $this->sqlMisdemeanorScreeningPackageConditionSetId() . ','; } elseif( true == array_key_exists( 'MisdemeanorScreeningPackageConditionSetId', $this->getChangedColumns() ) ) { $strSql .= ' misdemeanor_screening_package_condition_set_id = ' . $this->sqlMisdemeanorScreeningPackageConditionSetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' felony_years = ' . $this->sqlFelonyYears() . ','; } elseif( true == array_key_exists( 'FelonyYears', $this->getChangedColumns() ) ) { $strSql .= ' felony_years = ' . $this->sqlFelonyYears() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' misdemeanor_years = ' . $this->sqlMisdemeanorYears() . ','; } elseif( true == array_key_exists( 'MisdemeanorYears', $this->getChangedColumns() ) ) { $strSql .= ' misdemeanor_years = ' . $this->sqlMisdemeanorYears() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_criminal_classification_subtype_id = ' . $this->sqlClientCriminalClassificationSubtypeId() . ','; } elseif( true == array_key_exists( 'ClientCriminalClassificationSubtypeId', $this->getChangedColumns() ) ) { $strSql .= ' client_criminal_classification_subtype_id = ' . $this->sqlClientCriminalClassificationSubtypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'screening_criteria_id' => $this->getScreeningCriteriaId(),
			'criminal_classification_type_id' => $this->getCriminalClassificationTypeId(),
			'felony_screening_package_condition_set_id' => $this->getFelonyScreeningPackageConditionSetId(),
			'misdemeanor_screening_package_condition_set_id' => $this->getMisdemeanorScreeningPackageConditionSetId(),
			'felony_years' => $this->getFelonyYears(),
			'misdemeanor_years' => $this->getMisdemeanorYears(),
			'client_criminal_classification_subtype_id' => $this->getClientCriminalClassificationSubtypeId(),
			'is_active' => $this->getIsActive(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>