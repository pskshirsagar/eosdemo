<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CRvScreeningApplicantDetails
 * Do not add any new functions to this class.
 */

class CBaseRvScreeningApplicantDetails extends CEosPluralBase {

	/**
	 * @return CRvScreeningApplicantDetail[]
	 */
	public static function fetchRvScreeningApplicantDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRvScreeningApplicantDetail::class, $objDatabase );
	}

	/**
	 * @return CRvScreeningApplicantDetail
	 */
	public static function fetchRvScreeningApplicantDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRvScreeningApplicantDetail::class, $objDatabase );
	}

	public static function fetchRvScreeningApplicantDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'rv_screening_applicant_details', $objDatabase );
	}

	public static function fetchRvScreeningApplicantDetailById( $intId, $objDatabase ) {
		return self::fetchRvScreeningApplicantDetail( sprintf( 'SELECT * FROM rv_screening_applicant_details WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchRvScreeningApplicantDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchRvScreeningApplicantDetails( sprintf( 'SELECT * FROM rv_screening_applicant_details WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchRvScreeningApplicantDetailsByScreeningId( $intScreeningId, $objDatabase ) {
		return self::fetchRvScreeningApplicantDetails( sprintf( 'SELECT * FROM rv_screening_applicant_details WHERE screening_id = %d', $intScreeningId ), $objDatabase );
	}

	public static function fetchRvScreeningApplicantDetailsByScreeningApplicantId( $intScreeningApplicantId, $objDatabase ) {
		return self::fetchRvScreeningApplicantDetails( sprintf( 'SELECT * FROM rv_screening_applicant_details WHERE screening_applicant_id = %d', $intScreeningApplicantId ), $objDatabase );
	}

	public static function fetchRvScreeningApplicantDetailsByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchRvScreeningApplicantDetails( sprintf( 'SELECT * FROM rv_screening_applicant_details WHERE customer_id = %d', $intCustomerId ), $objDatabase );
	}

	public static function fetchRvScreeningApplicantDetailsByCustomerTypeId( $intCustomerTypeId, $objDatabase ) {
		return self::fetchRvScreeningApplicantDetails( sprintf( 'SELECT * FROM rv_screening_applicant_details WHERE customer_type_id = %d', $intCustomerTypeId ), $objDatabase );
	}

	public static function fetchRvScreeningApplicantDetailsByRvScreeningLeaseDetailsId( $intRvScreeningLeaseDetailsId, $objDatabase ) {
		return self::fetchRvScreeningApplicantDetails( sprintf( 'SELECT * FROM rv_screening_applicant_details WHERE rv_screening_lease_details_id = %d', $intRvScreeningLeaseDetailsId ), $objDatabase );
	}

}
?>