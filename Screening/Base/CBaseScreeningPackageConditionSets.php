<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageConditionSets
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningPackageConditionSets extends CEosPluralBase {

	/**
	 * @return CScreeningPackageConditionSet[]
	 */
	public static function fetchScreeningPackageConditionSets( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageConditionSet', $objDatabase );
	}

	/**
	 * @return CScreeningPackageConditionSet
	 */
	public static function fetchScreeningPackageConditionSet( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageConditionSet', $objDatabase );
	}

	public static function fetchScreeningPackageConditionSetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_condition_sets', $objDatabase );
	}

	public static function fetchScreeningPackageConditionSetByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageConditionSet( sprintf( 'SELECT * FROM screening_package_condition_sets WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageConditionSetsByScreeningPackageIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageConditionSets( sprintf( 'SELECT * FROM screening_package_condition_sets WHERE screening_package_id = %d AND cid = %d', ( int ) $intScreeningPackageId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageConditionSetsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningPackageConditionSets( sprintf( 'SELECT * FROM screening_package_condition_sets WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageConditionSetsByScreeningRecommendationTypeIdByCid( $intScreeningRecommendationTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageConditionSets( sprintf( 'SELECT * FROM screening_package_condition_sets WHERE screening_recommendation_type_id = %d AND cid = %d', ( int ) $intScreeningRecommendationTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageConditionSetsByFirstScreeningPackageConditionSubsetIdByCid( $intFirstScreeningPackageConditionSubsetId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageConditionSets( sprintf( 'SELECT * FROM screening_package_condition_sets WHERE first_screening_package_condition_subset_id = %d AND cid = %d', ( int ) $intFirstScreeningPackageConditionSubsetId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageConditionSetsBySecondScreeningPackageConditionSubsetIdByCid( $intSecondScreeningPackageConditionSubsetId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageConditionSets( sprintf( 'SELECT * FROM screening_package_condition_sets WHERE second_screening_package_condition_subset_id = %d AND cid = %d', ( int ) $intSecondScreeningPackageConditionSubsetId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageConditionSetsByScreeningPackageOperandTypeIdByCid( $intScreeningPackageOperandTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageConditionSets( sprintf( 'SELECT * FROM screening_package_condition_sets WHERE screening_package_operand_type_id = %d AND cid = %d', ( int ) $intScreeningPackageOperandTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>