<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCompanyPreferenceTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningCompanyPreferenceTypes extends CEosPluralBase {

	/**
	 * @return CScreeningCompanyPreferenceType[]
	 */
	public static function fetchScreeningCompanyPreferenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningCompanyPreferenceType::class, $objDatabase );
	}

	/**
	 * @return CScreeningCompanyPreferenceType
	 */
	public static function fetchScreeningCompanyPreferenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningCompanyPreferenceType::class, $objDatabase );
	}

	public static function fetchScreeningCompanyPreferenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_company_preference_types', $objDatabase );
	}

	public static function fetchScreeningCompanyPreferenceTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningCompanyPreferenceType( sprintf( 'SELECT * FROM screening_company_preference_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>