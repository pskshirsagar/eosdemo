<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CConsumerDisputes
 * Do not add any new functions to this class.
 */

class CBaseConsumerDisputes extends CEosPluralBase {

	/**
	 * @return CConsumerDispute[]
	 */
	public static function fetchConsumerDisputes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CConsumerDispute::class, $objDatabase );
	}

	/**
	 * @return CConsumerDispute
	 */
	public static function fetchConsumerDispute( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CConsumerDispute::class, $objDatabase );
	}

	public static function fetchConsumerDisputeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'consumer_disputes', $objDatabase );
	}

	public static function fetchConsumerDisputeById( $intId, $objDatabase ) {
		return self::fetchConsumerDispute( sprintf( 'SELECT * FROM consumer_disputes WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchConsumerDisputesByConsumerId( $intConsumerId, $objDatabase ) {
		return self::fetchConsumerDisputes( sprintf( 'SELECT * FROM consumer_disputes WHERE consumer_id = %d', $intConsumerId ), $objDatabase );
	}

	public static function fetchConsumerDisputesByDisputeParentId( $intDisputeParentId, $objDatabase ) {
		return self::fetchConsumerDisputes( sprintf( 'SELECT * FROM consumer_disputes WHERE dispute_parent_id = %d', $intDisputeParentId ), $objDatabase );
	}

	public static function fetchConsumerDisputesByDisputeTypeId( $intDisputeTypeId, $objDatabase ) {
		return self::fetchConsumerDisputes( sprintf( 'SELECT * FROM consumer_disputes WHERE dispute_type_id = %d', $intDisputeTypeId ), $objDatabase );
	}

	public static function fetchConsumerDisputesByDisputeTypeReasonId( $intDisputeTypeReasonId, $objDatabase ) {
		return self::fetchConsumerDisputes( sprintf( 'SELECT * FROM consumer_disputes WHERE dispute_type_reason_id = %d', $intDisputeTypeReasonId ), $objDatabase );
	}

	public static function fetchConsumerDisputesByDisputeStatusTypeId( $intDisputeStatusTypeId, $objDatabase ) {
		return self::fetchConsumerDisputes( sprintf( 'SELECT * FROM consumer_disputes WHERE dispute_status_type_id = %d', $intDisputeStatusTypeId ), $objDatabase );
	}

	public static function fetchConsumerDisputesByDataSourceId( $intDataSourceId, $objDatabase ) {
		return self::fetchConsumerDisputes( sprintf( 'SELECT * FROM consumer_disputes WHERE data_source_id = %d', $intDataSourceId ), $objDatabase );
	}

}
?>