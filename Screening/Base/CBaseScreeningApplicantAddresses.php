<?php

class CBaseScreeningApplicantAddresses extends CEosPluralBase {

	/**
	 * @return CScreeningApplicantAddress[]
	 */
	public static function fetchScreeningApplicantAddresses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningApplicantAddress::class, $objDatabase );
	}

	/**
	 * @return CScreeningApplicantAddress
	 */
	public static function fetchScreeningApplicantAddress( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningApplicantAddress::class, $objDatabase );
	}

	public static function fetchScreeningApplicantAddressCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_applicant_addresses', $objDatabase );
	}

	public static function fetchScreeningApplicantAddressById( $intId, $objDatabase ) {
		return self::fetchScreeningApplicantAddress( sprintf( 'SELECT * FROM screening_applicant_addresses WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchScreeningApplicantAddressesByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningApplicantAddresses( sprintf( 'SELECT * FROM screening_applicant_addresses WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantAddressesByScreeningApplicantId( $intScreeningApplicantId, $objDatabase ) {
		return self::fetchScreeningApplicantAddresses( sprintf( 'SELECT * FROM screening_applicant_addresses WHERE screening_applicant_id = %d', $intScreeningApplicantId ), $objDatabase );
	}

	public static function fetchScreeningApplicantAddressesByAddressTypeId( $intAddressTypeId, $objDatabase ) {
		return self::fetchScreeningApplicantAddresses( sprintf( 'SELECT * FROM screening_applicant_addresses WHERE address_type_id = %d', $intAddressTypeId ), $objDatabase );
	}

}
?>