<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageConfigPreferenceTypeFilters
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningPackageConfigPreferenceTypeFilters extends CEosPluralBase {

	/**
	 * @return CScreeningPackageConfigPreferenceTypeFilter[]
	 */
	public static function fetchScreeningPackageConfigPreferenceTypeFilters( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageConfigPreferenceTypeFilter', $objDatabase );
	}

	/**
	 * @return CScreeningPackageConfigPreferenceTypeFilter
	 */
	public static function fetchScreeningPackageConfigPreferenceTypeFilter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageConfigPreferenceTypeFilter', $objDatabase );
	}

	public static function fetchScreeningPackageConfigPreferenceTypeFilterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_config_preference_type_filters', $objDatabase );
	}

	public static function fetchScreeningPackageConfigPreferenceTypeFilterByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageConfigPreferenceTypeFilter( sprintf( 'SELECT * FROM screening_package_config_preference_type_filters WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageConfigPreferenceTypeFiltersByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningPackageConfigPreferenceTypeFilters( sprintf( 'SELECT * FROM screening_package_config_preference_type_filters WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageConfigPreferenceTypeFiltersByScreeningPackageIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageConfigPreferenceTypeFilters( sprintf( 'SELECT * FROM screening_package_config_preference_type_filters WHERE screening_package_id = %d AND cid = %d', ( int ) $intScreeningPackageId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageConfigPreferenceTypeFiltersByScreeningConfigPreferenceTypeIdByCid( $intScreeningConfigPreferenceTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageConfigPreferenceTypeFilters( sprintf( 'SELECT * FROM screening_package_config_preference_type_filters WHERE screening_config_preference_type_id = %d AND cid = %d', ( int ) $intScreeningConfigPreferenceTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageConfigPreferenceTypeFiltersByScreeningFilterIdByCid( $intScreeningFilterId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageConfigPreferenceTypeFilters( sprintf( 'SELECT * FROM screening_package_config_preference_type_filters WHERE screening_filter_id = %d AND cid = %d', ( int ) $intScreeningFilterId, ( int ) $intCid ), $objDatabase );
	}

}
?>