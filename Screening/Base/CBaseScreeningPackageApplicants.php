<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageApplicants
 * Do not add any new functions to this class.
 */

class CBaseScreeningPackageApplicants extends CEosPluralBase {

	/**
	 * @return CScreeningPackageApplicant[]
	 */
	public static function fetchScreeningPackageApplicants( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageApplicant', $objDatabase );
	}

	/**
	 * @return CScreeningPackageApplicant
	 */
	public static function fetchScreeningPackageApplicant( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageApplicant', $objDatabase );
	}

	public static function fetchScreeningPackageApplicantCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_applicants', $objDatabase );
	}

	public static function fetchScreeningPackageApplicantById( $intId, $objDatabase ) {
		return self::fetchScreeningPackageApplicant( sprintf( 'SELECT * FROM screening_package_applicants WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningPackageApplicantsByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {
		return self::fetchScreeningPackageApplicants( sprintf( 'SELECT * FROM screening_package_applicants WHERE screening_package_id = %d', ( int ) $intScreeningPackageId ), $objDatabase );
	}

	public static function fetchScreeningPackageApplicantsByScreeningApplicantTypeId( $intScreeningApplicantTypeId, $objDatabase ) {
		return self::fetchScreeningPackageApplicants( sprintf( 'SELECT * FROM screening_package_applicants WHERE screening_applicant_type_id = %d', ( int ) $intScreeningApplicantTypeId ), $objDatabase );
	}

}
?>