<?php

class CBaseScreeningResponse extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_responses';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScreeningId;
	protected $m_intScreeningApplicantId;
	protected $m_strResponseDatetime;
	protected $m_strXmlResponse;
	protected $m_intHasCreditAlert;
	protected $m_intHasCriminalAlert;
	protected $m_intHasEvictionAlert;
	protected $m_intHasFelonyAlert;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intHasCreditAlert = '0';
		$this->m_intHasCriminalAlert = '0';
		$this->m_intHasEvictionAlert = '0';
		$this->m_intHasFelonyAlert = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screening_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningId', trim( $arrValues['screening_id'] ) ); elseif( isset( $arrValues['screening_id'] ) ) $this->setScreeningId( $arrValues['screening_id'] );
		if( isset( $arrValues['screening_applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicantId', trim( $arrValues['screening_applicant_id'] ) ); elseif( isset( $arrValues['screening_applicant_id'] ) ) $this->setScreeningApplicantId( $arrValues['screening_applicant_id'] );
		if( isset( $arrValues['response_datetime'] ) && $boolDirectSet ) $this->set( 'm_strResponseDatetime', trim( $arrValues['response_datetime'] ) ); elseif( isset( $arrValues['response_datetime'] ) ) $this->setResponseDatetime( $arrValues['response_datetime'] );
		if( isset( $arrValues['xml_response'] ) && $boolDirectSet ) $this->set( 'm_strXmlResponse', trim( stripcslashes( $arrValues['xml_response'] ) ) ); elseif( isset( $arrValues['xml_response'] ) ) $this->setXmlResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['xml_response'] ) : $arrValues['xml_response'] );
		if( isset( $arrValues['has_credit_alert'] ) && $boolDirectSet ) $this->set( 'm_intHasCreditAlert', trim( $arrValues['has_credit_alert'] ) ); elseif( isset( $arrValues['has_credit_alert'] ) ) $this->setHasCreditAlert( $arrValues['has_credit_alert'] );
		if( isset( $arrValues['has_criminal_alert'] ) && $boolDirectSet ) $this->set( 'm_intHasCriminalAlert', trim( $arrValues['has_criminal_alert'] ) ); elseif( isset( $arrValues['has_criminal_alert'] ) ) $this->setHasCriminalAlert( $arrValues['has_criminal_alert'] );
		if( isset( $arrValues['has_eviction_alert'] ) && $boolDirectSet ) $this->set( 'm_intHasEvictionAlert', trim( $arrValues['has_eviction_alert'] ) ); elseif( isset( $arrValues['has_eviction_alert'] ) ) $this->setHasEvictionAlert( $arrValues['has_eviction_alert'] );
		if( isset( $arrValues['has_felony_alert'] ) && $boolDirectSet ) $this->set( 'm_intHasFelonyAlert', trim( $arrValues['has_felony_alert'] ) ); elseif( isset( $arrValues['has_felony_alert'] ) ) $this->setHasFelonyAlert( $arrValues['has_felony_alert'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreeningId( $intScreeningId ) {
		$this->set( 'm_intScreeningId', CStrings::strToIntDef( $intScreeningId, NULL, false ) );
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function sqlScreeningId() {
		return ( true == isset( $this->m_intScreeningId ) ) ? ( string ) $this->m_intScreeningId : 'NULL';
	}

	public function setScreeningApplicantId( $intScreeningApplicantId ) {
		$this->set( 'm_intScreeningApplicantId', CStrings::strToIntDef( $intScreeningApplicantId, NULL, false ) );
	}

	public function getScreeningApplicantId() {
		return $this->m_intScreeningApplicantId;
	}

	public function sqlScreeningApplicantId() {
		return ( true == isset( $this->m_intScreeningApplicantId ) ) ? ( string ) $this->m_intScreeningApplicantId : 'NULL';
	}

	public function setResponseDatetime( $strResponseDatetime ) {
		$this->set( 'm_strResponseDatetime', CStrings::strTrimDef( $strResponseDatetime, -1, NULL, true ) );
	}

	public function getResponseDatetime() {
		return $this->m_strResponseDatetime;
	}

	public function sqlResponseDatetime() {
		return ( true == isset( $this->m_strResponseDatetime ) ) ? '\'' . $this->m_strResponseDatetime . '\'' : 'NOW()';
	}

	public function setXmlResponse( $strXmlResponse ) {
		$this->set( 'm_strXmlResponse', CStrings::strTrimDef( $strXmlResponse, -1, NULL, true ) );
	}

	public function getXmlResponse() {
		return $this->m_strXmlResponse;
	}

	public function sqlXmlResponse() {
		return ( true == isset( $this->m_strXmlResponse ) ) ? '\'' . addslashes( $this->m_strXmlResponse ) . '\'' : 'NULL';
	}

	public function setHasCreditAlert( $intHasCreditAlert ) {
		$this->set( 'm_intHasCreditAlert', CStrings::strToIntDef( $intHasCreditAlert, NULL, false ) );
	}

	public function getHasCreditAlert() {
		return $this->m_intHasCreditAlert;
	}

	public function sqlHasCreditAlert() {
		return ( true == isset( $this->m_intHasCreditAlert ) ) ? ( string ) $this->m_intHasCreditAlert : '0';
	}

	public function setHasCriminalAlert( $intHasCriminalAlert ) {
		$this->set( 'm_intHasCriminalAlert', CStrings::strToIntDef( $intHasCriminalAlert, NULL, false ) );
	}

	public function getHasCriminalAlert() {
		return $this->m_intHasCriminalAlert;
	}

	public function sqlHasCriminalAlert() {
		return ( true == isset( $this->m_intHasCriminalAlert ) ) ? ( string ) $this->m_intHasCriminalAlert : '0';
	}

	public function setHasEvictionAlert( $intHasEvictionAlert ) {
		$this->set( 'm_intHasEvictionAlert', CStrings::strToIntDef( $intHasEvictionAlert, NULL, false ) );
	}

	public function getHasEvictionAlert() {
		return $this->m_intHasEvictionAlert;
	}

	public function sqlHasEvictionAlert() {
		return ( true == isset( $this->m_intHasEvictionAlert ) ) ? ( string ) $this->m_intHasEvictionAlert : '0';
	}

	public function setHasFelonyAlert( $intHasFelonyAlert ) {
		$this->set( 'm_intHasFelonyAlert', CStrings::strToIntDef( $intHasFelonyAlert, NULL, false ) );
	}

	public function getHasFelonyAlert() {
		return $this->m_intHasFelonyAlert;
	}

	public function sqlHasFelonyAlert() {
		return ( true == isset( $this->m_intHasFelonyAlert ) ) ? ( string ) $this->m_intHasFelonyAlert : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, screening_id, screening_applicant_id, response_datetime, xml_response, has_credit_alert, has_criminal_alert, has_eviction_alert, has_felony_alert, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlScreeningId() . ', ' .
 						$this->sqlScreeningApplicantId() . ', ' .
 						$this->sqlResponseDatetime() . ', ' .
 						$this->sqlXmlResponse() . ', ' .
 						$this->sqlHasCreditAlert() . ', ' .
 						$this->sqlHasCriminalAlert() . ', ' .
 						$this->sqlHasEvictionAlert() . ', ' .
 						$this->sqlHasFelonyAlert() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; } elseif( true == array_key_exists( 'ScreeningId', $this->getChangedColumns() ) ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId() . ','; } elseif( true == array_key_exists( 'ScreeningApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_datetime = ' . $this->sqlResponseDatetime() . ','; } elseif( true == array_key_exists( 'ResponseDatetime', $this->getChangedColumns() ) ) { $strSql .= ' response_datetime = ' . $this->sqlResponseDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' xml_response = ' . $this->sqlXmlResponse() . ','; } elseif( true == array_key_exists( 'XmlResponse', $this->getChangedColumns() ) ) { $strSql .= ' xml_response = ' . $this->sqlXmlResponse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_credit_alert = ' . $this->sqlHasCreditAlert() . ','; } elseif( true == array_key_exists( 'HasCreditAlert', $this->getChangedColumns() ) ) { $strSql .= ' has_credit_alert = ' . $this->sqlHasCreditAlert() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_criminal_alert = ' . $this->sqlHasCriminalAlert() . ','; } elseif( true == array_key_exists( 'HasCriminalAlert', $this->getChangedColumns() ) ) { $strSql .= ' has_criminal_alert = ' . $this->sqlHasCriminalAlert() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_eviction_alert = ' . $this->sqlHasEvictionAlert() . ','; } elseif( true == array_key_exists( 'HasEvictionAlert', $this->getChangedColumns() ) ) { $strSql .= ' has_eviction_alert = ' . $this->sqlHasEvictionAlert() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_felony_alert = ' . $this->sqlHasFelonyAlert() . ','; } elseif( true == array_key_exists( 'HasFelonyAlert', $this->getChangedColumns() ) ) { $strSql .= ' has_felony_alert = ' . $this->sqlHasFelonyAlert() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'screening_id' => $this->getScreeningId(),
			'screening_applicant_id' => $this->getScreeningApplicantId(),
			'response_datetime' => $this->getResponseDatetime(),
			'xml_response' => $this->getXmlResponse(),
			'has_credit_alert' => $this->getHasCreditAlert(),
			'has_criminal_alert' => $this->getHasCriminalAlert(),
			'has_eviction_alert' => $this->getHasEvictionAlert(),
			'has_felony_alert' => $this->getHasFelonyAlert(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>