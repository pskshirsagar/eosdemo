<?php

class CBaseScreeningInvoiceDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_invoice_details';

	protected $m_intId;
	protected $m_intScreeningInvoiceId;
	protected $m_intScreenTypeId;
	protected $m_intScreeningTransactionId;
	protected $m_fltTransactionAmount;
	protected $m_fltInvoiceAmount;
	protected $m_strScreeningOrderId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsDuplicateTransaction;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDuplicateTransaction = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_invoice_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningInvoiceId', trim( $arrValues['screening_invoice_id'] ) ); elseif( isset( $arrValues['screening_invoice_id'] ) ) $this->setScreeningInvoiceId( $arrValues['screening_invoice_id'] );
		if( isset( $arrValues['screen_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreenTypeId', trim( $arrValues['screen_type_id'] ) ); elseif( isset( $arrValues['screen_type_id'] ) ) $this->setScreenTypeId( $arrValues['screen_type_id'] );
		if( isset( $arrValues['screening_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningTransactionId', trim( $arrValues['screening_transaction_id'] ) ); elseif( isset( $arrValues['screening_transaction_id'] ) ) $this->setScreeningTransactionId( $arrValues['screening_transaction_id'] );
		if( isset( $arrValues['transaction_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmount', trim( $arrValues['transaction_amount'] ) ); elseif( isset( $arrValues['transaction_amount'] ) ) $this->setTransactionAmount( $arrValues['transaction_amount'] );
		if( isset( $arrValues['invoice_amount'] ) && $boolDirectSet ) $this->set( 'm_fltInvoiceAmount', trim( $arrValues['invoice_amount'] ) ); elseif( isset( $arrValues['invoice_amount'] ) ) $this->setInvoiceAmount( $arrValues['invoice_amount'] );
		if( isset( $arrValues['screening_order_id'] ) && $boolDirectSet ) $this->set( 'm_strScreeningOrderId', trim( $arrValues['screening_order_id'] ) ); elseif( isset( $arrValues['screening_order_id'] ) ) $this->setScreeningOrderId( $arrValues['screening_order_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_duplicate_transaction'] ) && $boolDirectSet ) $this->set( 'm_boolIsDuplicateTransaction', trim( stripcslashes( $arrValues['is_duplicate_transaction'] ) ) ); elseif( isset( $arrValues['is_duplicate_transaction'] ) ) $this->setIsDuplicateTransaction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_duplicate_transaction'] ) : $arrValues['is_duplicate_transaction'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningInvoiceId( $intScreeningInvoiceId ) {
		$this->set( 'm_intScreeningInvoiceId', CStrings::strToIntDef( $intScreeningInvoiceId, NULL, false ) );
	}

	public function getScreeningInvoiceId() {
		return $this->m_intScreeningInvoiceId;
	}

	public function sqlScreeningInvoiceId() {
		return ( true == isset( $this->m_intScreeningInvoiceId ) ) ? ( string ) $this->m_intScreeningInvoiceId : 'NULL';
	}

	public function setScreenTypeId( $intScreenTypeId ) {
		$this->set( 'm_intScreenTypeId', CStrings::strToIntDef( $intScreenTypeId, NULL, false ) );
	}

	public function getScreenTypeId() {
		return $this->m_intScreenTypeId;
	}

	public function sqlScreenTypeId() {
		return ( true == isset( $this->m_intScreenTypeId ) ) ? ( string ) $this->m_intScreenTypeId : 'NULL';
	}

	public function setScreeningTransactionId( $intScreeningTransactionId ) {
		$this->set( 'm_intScreeningTransactionId', CStrings::strToIntDef( $intScreeningTransactionId, NULL, false ) );
	}

	public function getScreeningTransactionId() {
		return $this->m_intScreeningTransactionId;
	}

	public function sqlScreeningTransactionId() {
		return ( true == isset( $this->m_intScreeningTransactionId ) ) ? ( string ) $this->m_intScreeningTransactionId : 'NULL';
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->set( 'm_fltTransactionAmount', CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 2 ) );
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function sqlTransactionAmount() {
		return ( true == isset( $this->m_fltTransactionAmount ) ) ? ( string ) $this->m_fltTransactionAmount : 'NULL';
	}

	public function setInvoiceAmount( $fltInvoiceAmount ) {
		$this->set( 'm_fltInvoiceAmount', CStrings::strToFloatDef( $fltInvoiceAmount, NULL, false, 2 ) );
	}

	public function getInvoiceAmount() {
		return $this->m_fltInvoiceAmount;
	}

	public function sqlInvoiceAmount() {
		return ( true == isset( $this->m_fltInvoiceAmount ) ) ? ( string ) $this->m_fltInvoiceAmount : 'NULL';
	}

	public function setScreeningOrderId( $strScreeningOrderId ) {
		$this->set( 'm_strScreeningOrderId', CStrings::strTrimDef( $strScreeningOrderId, -1, NULL, true ) );
	}

	public function getScreeningOrderId() {
		return $this->m_strScreeningOrderId;
	}

	public function sqlScreeningOrderId() {
		return ( true == isset( $this->m_strScreeningOrderId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strScreeningOrderId ) : '\'' . addslashes( $this->m_strScreeningOrderId ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsDuplicateTransaction( $boolIsDuplicateTransaction ) {
		$this->set( 'm_boolIsDuplicateTransaction', CStrings::strToBool( $boolIsDuplicateTransaction ) );
	}

	public function getIsDuplicateTransaction() {
		return $this->m_boolIsDuplicateTransaction;
	}

	public function sqlIsDuplicateTransaction() {
		return ( true == isset( $this->m_boolIsDuplicateTransaction ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDuplicateTransaction ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, screening_invoice_id, screen_type_id, screening_transaction_id, transaction_amount, invoice_amount, screening_order_id, updated_by, updated_on, created_by, created_on, is_duplicate_transaction )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlScreeningInvoiceId() . ', ' .
						$this->sqlScreenTypeId() . ', ' .
						$this->sqlScreeningTransactionId() . ', ' .
						$this->sqlTransactionAmount() . ', ' .
						$this->sqlInvoiceAmount() . ', ' .
						$this->sqlScreeningOrderId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsDuplicateTransaction() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_invoice_id = ' . $this->sqlScreeningInvoiceId(). ',' ; } elseif( true == array_key_exists( 'ScreeningInvoiceId', $this->getChangedColumns() ) ) { $strSql .= ' screening_invoice_id = ' . $this->sqlScreeningInvoiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreenTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_transaction_id = ' . $this->sqlScreeningTransactionId(). ',' ; } elseif( true == array_key_exists( 'ScreeningTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' screening_transaction_id = ' . $this->sqlScreeningTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount(). ',' ; } elseif( true == array_key_exists( 'TransactionAmount', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_amount = ' . $this->sqlInvoiceAmount(). ',' ; } elseif( true == array_key_exists( 'InvoiceAmount', $this->getChangedColumns() ) ) { $strSql .= ' invoice_amount = ' . $this->sqlInvoiceAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_order_id = ' . $this->sqlScreeningOrderId(). ',' ; } elseif( true == array_key_exists( 'ScreeningOrderId', $this->getChangedColumns() ) ) { $strSql .= ' screening_order_id = ' . $this->sqlScreeningOrderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_duplicate_transaction = ' . $this->sqlIsDuplicateTransaction(). ',' ; } elseif( true == array_key_exists( 'IsDuplicateTransaction', $this->getChangedColumns() ) ) { $strSql .= ' is_duplicate_transaction = ' . $this->sqlIsDuplicateTransaction() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_invoice_id' => $this->getScreeningInvoiceId(),
			'screen_type_id' => $this->getScreenTypeId(),
			'screening_transaction_id' => $this->getScreeningTransactionId(),
			'transaction_amount' => $this->getTransactionAmount(),
			'invoice_amount' => $this->getInvoiceAmount(),
			'screening_order_id' => $this->getScreeningOrderId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_duplicate_transaction' => $this->getIsDuplicateTransaction()
		);
	}

}
?>