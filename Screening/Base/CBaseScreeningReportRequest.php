<?php

class CBaseScreeningReportRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_report_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScreeningId;
	protected $m_intScreeningApplicantId;
	protected $m_intScreeningReportRequestStatusId;
	protected $m_intReportSentBy;
	protected $m_strReportSentOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intScreeningReportRequestSourceId;
	protected $m_arrintSystemEmailIds;
	protected $m_strAlternateEmailAddress;

	public function __construct() {
		parent::__construct();

		$this->m_intScreeningReportRequestSourceId = '4';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screening_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningId', trim( $arrValues['screening_id'] ) ); elseif( isset( $arrValues['screening_id'] ) ) $this->setScreeningId( $arrValues['screening_id'] );
		if( isset( $arrValues['screening_applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicantId', trim( $arrValues['screening_applicant_id'] ) ); elseif( isset( $arrValues['screening_applicant_id'] ) ) $this->setScreeningApplicantId( $arrValues['screening_applicant_id'] );
		if( isset( $arrValues['screening_report_request_status_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningReportRequestStatusId', trim( $arrValues['screening_report_request_status_id'] ) ); elseif( isset( $arrValues['screening_report_request_status_id'] ) ) $this->setScreeningReportRequestStatusId( $arrValues['screening_report_request_status_id'] );
		if( isset( $arrValues['report_sent_by'] ) && $boolDirectSet ) $this->set( 'm_intReportSentBy', trim( $arrValues['report_sent_by'] ) ); elseif( isset( $arrValues['report_sent_by'] ) ) $this->setReportSentBy( $arrValues['report_sent_by'] );
		if( isset( $arrValues['report_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strReportSentOn', trim( $arrValues['report_sent_on'] ) ); elseif( isset( $arrValues['report_sent_on'] ) ) $this->setReportSentOn( $arrValues['report_sent_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['screening_report_request_source_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningReportRequestSourceId', trim( $arrValues['screening_report_request_source_id'] ) ); elseif( isset( $arrValues['screening_report_request_source_id'] ) ) $this->setScreeningReportRequestSourceId( $arrValues['screening_report_request_source_id'] );
		if( isset( $arrValues['system_email_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintSystemEmailIds', trim( $arrValues['system_email_ids'] ) ); elseif( isset( $arrValues['system_email_ids'] ) ) $this->setSystemEmailIds( $arrValues['system_email_ids'] );
		if( isset( $arrValues['alternate_email_address'] ) && $boolDirectSet ) $this->set( 'm_strAlternateEmailAddress', trim( stripcslashes( $arrValues['alternate_email_address'] ) ) ); elseif( isset( $arrValues['alternate_email_address'] ) ) $this->setAlternateEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['alternate_email_address'] ) : $arrValues['alternate_email_address'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreeningId( $intScreeningId ) {
		$this->set( 'm_intScreeningId', CStrings::strToIntDef( $intScreeningId, NULL, false ) );
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function sqlScreeningId() {
		return ( true == isset( $this->m_intScreeningId ) ) ? ( string ) $this->m_intScreeningId : 'NULL';
	}

	public function setScreeningApplicantId( $intScreeningApplicantId ) {
		$this->set( 'm_intScreeningApplicantId', CStrings::strToIntDef( $intScreeningApplicantId, NULL, false ) );
	}

	public function getScreeningApplicantId() {
		return $this->m_intScreeningApplicantId;
	}

	public function sqlScreeningApplicantId() {
		return ( true == isset( $this->m_intScreeningApplicantId ) ) ? ( string ) $this->m_intScreeningApplicantId : 'NULL';
	}

	public function setScreeningReportRequestStatusId( $intScreeningReportRequestStatusId ) {
		$this->set( 'm_intScreeningReportRequestStatusId', CStrings::strToIntDef( $intScreeningReportRequestStatusId, NULL, false ) );
	}

	public function getScreeningReportRequestStatusId() {
		return $this->m_intScreeningReportRequestStatusId;
	}

	public function sqlScreeningReportRequestStatusId() {
		return ( true == isset( $this->m_intScreeningReportRequestStatusId ) ) ? ( string ) $this->m_intScreeningReportRequestStatusId : 'NULL';
	}

	public function setReportSentBy( $intReportSentBy ) {
		$this->set( 'm_intReportSentBy', CStrings::strToIntDef( $intReportSentBy, NULL, false ) );
	}

	public function getReportSentBy() {
		return $this->m_intReportSentBy;
	}

	public function sqlReportSentBy() {
		return ( true == isset( $this->m_intReportSentBy ) ) ? ( string ) $this->m_intReportSentBy : 'NULL';
	}

	public function setReportSentOn( $strReportSentOn ) {
		$this->set( 'm_strReportSentOn', CStrings::strTrimDef( $strReportSentOn, -1, NULL, true ) );
	}

	public function getReportSentOn() {
		return $this->m_strReportSentOn;
	}

	public function sqlReportSentOn() {
		return ( true == isset( $this->m_strReportSentOn ) ) ? '\'' . $this->m_strReportSentOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setScreeningReportRequestSourceId( $intScreeningReportRequestSourceId ) {
		$this->set( 'm_intScreeningReportRequestSourceId', CStrings::strToIntDef( $intScreeningReportRequestSourceId, NULL, false ) );
	}

	public function getScreeningReportRequestSourceId() {
		return $this->m_intScreeningReportRequestSourceId;
	}

	public function sqlScreeningReportRequestSourceId() {
		return ( true == isset( $this->m_intScreeningReportRequestSourceId ) ) ? ( string ) $this->m_intScreeningReportRequestSourceId : '4';
	}

	public function setSystemEmailIds( $arrintSystemEmailIds ) {
		$this->set( 'm_arrintSystemEmailIds', CStrings::strToArrIntDef( $arrintSystemEmailIds, NULL ) );
	}

	public function getSystemEmailIds() {
		return $this->m_arrintSystemEmailIds;
	}

	public function sqlSystemEmailIds() {
		return ( true == isset( $this->m_arrintSystemEmailIds ) && true == valArr( $this->m_arrintSystemEmailIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintSystemEmailIds, NULL ) . '\'' : 'NULL';
	}

	public function setAlternateEmailAddress( $strAlternateEmailAddress ) {
		$this->set( 'm_strAlternateEmailAddress', CStrings::strTrimDef( $strAlternateEmailAddress, 100, NULL, true ) );
	}

	public function getAlternateEmailAddress() {
		return $this->m_strAlternateEmailAddress;
	}

	public function sqlAlternateEmailAddress() {
		return ( true == isset( $this->m_strAlternateEmailAddress ) ) ? '\'' . addslashes( $this->m_strAlternateEmailAddress ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, screening_id, screening_applicant_id, screening_report_request_status_id, report_sent_by, report_sent_on, updated_by, updated_on, created_by, created_on, screening_report_request_source_id, system_email_ids, alternate_email_address )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlScreeningId() . ', ' .
						$this->sqlScreeningApplicantId() . ', ' .
						$this->sqlScreeningReportRequestStatusId() . ', ' .
						$this->sqlReportSentBy() . ', ' .
						$this->sqlReportSentOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlScreeningReportRequestSourceId() . ', ' .
						$this->sqlSystemEmailIds() . ', ' .
		                $this->sqlAlternateEmailAddress() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId(). ',' ; } elseif( true == array_key_exists( 'ScreeningId', $this->getChangedColumns() ) ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId(). ',' ; } elseif( true == array_key_exists( 'ScreeningApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_report_request_status_id = ' . $this->sqlScreeningReportRequestStatusId(). ',' ; } elseif( true == array_key_exists( 'ScreeningReportRequestStatusId', $this->getChangedColumns() ) ) { $strSql .= ' screening_report_request_status_id = ' . $this->sqlScreeningReportRequestStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_sent_by = ' . $this->sqlReportSentBy(). ',' ; } elseif( true == array_key_exists( 'ReportSentBy', $this->getChangedColumns() ) ) { $strSql .= ' report_sent_by = ' . $this->sqlReportSentBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_sent_on = ' . $this->sqlReportSentOn(). ',' ; } elseif( true == array_key_exists( 'ReportSentOn', $this->getChangedColumns() ) ) { $strSql .= ' report_sent_on = ' . $this->sqlReportSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_report_request_source_id = ' . $this->sqlScreeningReportRequestSourceId(). ',' ; } elseif( true == array_key_exists( 'ScreeningReportRequestSourceId', $this->getChangedColumns() ) ) { $strSql .= ' screening_report_request_source_id = ' . $this->sqlScreeningReportRequestSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_ids = ' . $this->sqlSystemEmailIds(). ',' ; } elseif( true == array_key_exists( 'SystemEmailIds', $this->getChangedColumns() ) ) { $strSql .= ' system_email_ids = ' . $this->sqlSystemEmailIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alternate_email_address = ' . $this->sqlAlternateEmailAddress(). ',' ; } elseif( true == array_key_exists( 'AlternateEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' alternate_email_address = ' . $this->sqlAlternateEmailAddress() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'screening_id' => $this->getScreeningId(),
			'screening_applicant_id' => $this->getScreeningApplicantId(),
			'screening_report_request_status_id' => $this->getScreeningReportRequestStatusId(),
			'report_sent_by' => $this->getReportSentBy(),
			'report_sent_on' => $this->getReportSentOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'screening_report_request_source_id' => $this->getScreeningReportRequestSourceId(),
			'system_email_ids' => $this->getSystemEmailIds(),
			'alternate_email_address' => $this->getAlternateEmailAddress()
		);
	}

}
?>