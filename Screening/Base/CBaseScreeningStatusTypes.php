<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningStatusTypes extends CEosPluralBase {

	/**
	 * @return CScreeningStatusType[]
	 */
	public static function fetchScreeningStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningStatusType', $objDatabase );
	}

	/**
	 * @return CScreeningStatusType
	 */
	public static function fetchScreeningStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningStatusType', $objDatabase );
	}

	public static function fetchScreeningStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_status_types', $objDatabase );
	}

	public static function fetchScreeningStatusTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningStatusType( sprintf( 'SELECT * FROM screening_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>