<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningScoringModels
 * Do not add any new functions to this class.
 */

class CBaseScreeningScoringModels extends CEosPluralBase {

	/**
	 * @return CScreeningScoringModel[]
	 */
	public static function fetchScreeningScoringModels( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningScoringModel', $objDatabase );
	}

	/**
	 * @return CScreeningScoringModel
	 */
	public static function fetchScreeningScoringModel( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningScoringModel', $objDatabase );
	}

	public static function fetchScreeningScoringModelCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_scoring_models', $objDatabase );
	}

	public static function fetchScreeningScoringModelById( $intId, $objDatabase ) {
		return self::fetchScreeningScoringModel( sprintf( 'SELECT * FROM screening_scoring_models WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>