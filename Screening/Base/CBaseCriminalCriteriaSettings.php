<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CCriminalCriteriaSettings
 * Do not add any new functions to this class.
 */

class CBaseCriminalCriteriaSettings extends CEosPluralBase {

	/**
	 * @return CCriminalCriteriaSetting[]
	 */
	public static function fetchCriminalCriteriaSettings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCriminalCriteriaSetting::class, $objDatabase );
	}

	/**
	 * @return CCriminalCriteriaSetting
	 */
	public static function fetchCriminalCriteriaSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCriminalCriteriaSetting::class, $objDatabase );
	}

	public static function fetchCriminalCriteriaSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'criminal_criteria_settings', $objDatabase );
	}

	public static function fetchCriminalCriteriaSettingById( $intId, $objDatabase ) {
		return self::fetchCriminalCriteriaSetting( sprintf( 'SELECT * FROM criminal_criteria_settings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCriminalCriteriaSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchCriminalCriteriaSettings( sprintf( 'SELECT * FROM criminal_criteria_settings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCriminalCriteriaSettingsByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {
		return self::fetchCriminalCriteriaSettings( sprintf( 'SELECT * FROM criminal_criteria_settings WHERE screening_criteria_id = %d', ( int ) $intScreeningCriteriaId ), $objDatabase );
	}

	public static function fetchCriminalCriteriaSettingsByCriminalClassificationTypeId( $intCriminalClassificationTypeId, $objDatabase ) {
		return self::fetchCriminalCriteriaSettings( sprintf( 'SELECT * FROM criminal_criteria_settings WHERE criminal_classification_type_id = %d', ( int ) $intCriminalClassificationTypeId ), $objDatabase );
	}

	public static function fetchCriminalCriteriaSettingsByFelonyScreeningPackageConditionSetId( $intFelonyScreeningPackageConditionSetId, $objDatabase ) {
		return self::fetchCriminalCriteriaSettings( sprintf( 'SELECT * FROM criminal_criteria_settings WHERE felony_screening_package_condition_set_id = %d', ( int ) $intFelonyScreeningPackageConditionSetId ), $objDatabase );
	}

	public static function fetchCriminalCriteriaSettingsByMisdemeanorScreeningPackageConditionSetId( $intMisdemeanorScreeningPackageConditionSetId, $objDatabase ) {
		return self::fetchCriminalCriteriaSettings( sprintf( 'SELECT * FROM criminal_criteria_settings WHERE misdemeanor_screening_package_condition_set_id = %d', ( int ) $intMisdemeanorScreeningPackageConditionSetId ), $objDatabase );
	}

	public static function fetchCriminalCriteriaSettingsByClientCriminalClassificationSubtypeId( $intClientCriminalClassificationSubtypeId, $objDatabase ) {
		return self::fetchCriminalCriteriaSettings( sprintf( 'SELECT * FROM criminal_criteria_settings WHERE client_criminal_classification_subtype_id = %d', ( int ) $intClientCriminalClassificationSubtypeId ), $objDatabase );
	}

}
?>