<?php

class CBaseRvLeaseApplicantSecureNewDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.rv_lease_applicant_secure_new_details';

	protected $m_intId;
	protected $m_intRvLeaseNewOutcomesId;
	protected $m_intCollectionCount;
	protected $m_intCollectionCountLastOneMonth;
	protected $m_intCollectionCountLastOneYear;
	protected $m_intCollectionCountLastTwoYear;
	protected $m_intRentalCollectionCount;
	protected $m_intRentalCollectionCountLastOneMonth;
	protected $m_intRentalCollectionCountLastOneYear;
	protected $m_intRentalCollectionCountLastTwoYear;
	protected $m_intMedicalCollectionCount;
	protected $m_intMedicalCollectionCountLastOneYear;
	protected $m_intMedicalCollectionCountLastTwoYear;
	protected $m_intUtilityCollectionCount;
	protected $m_intUtilityCollectionCountLastOneYear;
	protected $m_intUtilityCollectionCountLastTwoYear;
	protected $m_intStudentCollectionCount;
	protected $m_intStudentCollectionCountLastOneYear;
	protected $m_intStudentCollectionCountLastTwoYear;
	protected $m_intInquiryCountLastNintyDays;
	protected $m_intInquiryCountLastOneEightyDays;
	protected $m_intInquiryCountLastOneYear;
	protected $m_intInquiryCountLastTwoYear;
	protected $m_fltRvRent;
	protected $m_fltRvApplicantIncome;
	protected $m_fltCollectionAmount;
	protected $m_fltRentalCollectionAmount;
	protected $m_fltMedicalCollectionAmount;
	protected $m_fltUtilityCollectionAmount;
	protected $m_fltStudentCollectionAmount;
	protected $m_fltDebtToIncome;
	protected $m_fltDelinquentAccountPercentage;
	protected $m_fltTotalMonthlyDebt;
	protected $m_fltMonthlyDebtAmount;
	protected $m_strCreditScore;
	protected $m_strRvIndex;
	protected $m_boolHadBankruptcy;
	protected $m_boolHadForeclosure;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intScreeningApplicantTypeId;
	protected $m_intTotalTradelineAccounts;
	protected $m_intTotalTradelinesCountLastTwoYears;
	protected $m_intPositiveTradelineAccounts;
	protected $m_intPositiveTradelinesCountLastTwoYears;
	protected $m_intNegativeTradelinesCount;
	protected $m_intNegativeTradelinesCountLastTwoYears;
	protected $m_fltCollectionAmountLastTwoYears;
	protected $m_fltRentalCollectionAmountLastTwoYears;
	protected $m_fltMedicalCollectionAmountLastTwoYears;
	protected $m_fltUtilityCollectionAmountLastTwoYears;
	protected $m_fltStudentCollectionAmountLastTwoYears;
	protected $m_intOpenCollectionCount;
	protected $m_intClosedCollectionCount;
	protected $m_strRvIndexRecalculated;
	protected $m_boolIsScreenedForCredit;
	protected $m_strScreenedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCollectionCount = '0';
		$this->m_intCollectionCountLastOneMonth = '0';
		$this->m_intCollectionCountLastOneYear = '0';
		$this->m_intCollectionCountLastTwoYear = '0';
		$this->m_intRentalCollectionCount = '0';
		$this->m_intRentalCollectionCountLastOneMonth = '0';
		$this->m_intRentalCollectionCountLastOneYear = '0';
		$this->m_intRentalCollectionCountLastTwoYear = '0';
		$this->m_intMedicalCollectionCount = '0';
		$this->m_intMedicalCollectionCountLastOneYear = '0';
		$this->m_intMedicalCollectionCountLastTwoYear = '0';
		$this->m_intUtilityCollectionCount = '0';
		$this->m_intUtilityCollectionCountLastOneYear = '0';
		$this->m_intUtilityCollectionCountLastTwoYear = '0';
		$this->m_intStudentCollectionCount = '0';
		$this->m_intStudentCollectionCountLastOneYear = '0';
		$this->m_intStudentCollectionCountLastTwoYear = '0';
		$this->m_intInquiryCountLastNintyDays = '0';
		$this->m_intInquiryCountLastOneEightyDays = '0';
		$this->m_intInquiryCountLastOneYear = '0';
		$this->m_intInquiryCountLastTwoYear = '0';
		$this->m_fltRvRent = '0';
		$this->m_fltRvApplicantIncome = '0';
		$this->m_fltCollectionAmount = '0';
		$this->m_fltRentalCollectionAmount = '0';
		$this->m_fltMedicalCollectionAmount = '0';
		$this->m_fltUtilityCollectionAmount = '0';
		$this->m_fltStudentCollectionAmount = '0';
		$this->m_fltDebtToIncome = '0';
		$this->m_fltDelinquentAccountPercentage = '0';
		$this->m_fltTotalMonthlyDebt = '0';
		$this->m_fltMonthlyDebtAmount = '0';
		$this->m_boolHadBankruptcy = false;
		$this->m_boolHadForeclosure = false;
		$this->m_boolIsScreenedForCredit = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['rv_lease_new_outcomes_id'] ) && $boolDirectSet ) $this->set( 'm_intRvLeaseNewOutcomesId', trim( $arrValues['rv_lease_new_outcomes_id'] ) ); elseif( isset( $arrValues['rv_lease_new_outcomes_id'] ) ) $this->setRvLeaseNewOutcomesId( $arrValues['rv_lease_new_outcomes_id'] );
		if( isset( $arrValues['collection_count'] ) && $boolDirectSet ) $this->set( 'm_intCollectionCount', trim( $arrValues['collection_count'] ) ); elseif( isset( $arrValues['collection_count'] ) ) $this->setCollectionCount( $arrValues['collection_count'] );
		if( isset( $arrValues['collection_count_last_one_month'] ) && $boolDirectSet ) $this->set( 'm_intCollectionCountLastOneMonth', trim( $arrValues['collection_count_last_one_month'] ) ); elseif( isset( $arrValues['collection_count_last_one_month'] ) ) $this->setCollectionCountLastOneMonth( $arrValues['collection_count_last_one_month'] );
		if( isset( $arrValues['collection_count_last_one_year'] ) && $boolDirectSet ) $this->set( 'm_intCollectionCountLastOneYear', trim( $arrValues['collection_count_last_one_year'] ) ); elseif( isset( $arrValues['collection_count_last_one_year'] ) ) $this->setCollectionCountLastOneYear( $arrValues['collection_count_last_one_year'] );
		if( isset( $arrValues['collection_count_last_two_year'] ) && $boolDirectSet ) $this->set( 'm_intCollectionCountLastTwoYear', trim( $arrValues['collection_count_last_two_year'] ) ); elseif( isset( $arrValues['collection_count_last_two_year'] ) ) $this->setCollectionCountLastTwoYear( $arrValues['collection_count_last_two_year'] );
		if( isset( $arrValues['rental_collection_count'] ) && $boolDirectSet ) $this->set( 'm_intRentalCollectionCount', trim( $arrValues['rental_collection_count'] ) ); elseif( isset( $arrValues['rental_collection_count'] ) ) $this->setRentalCollectionCount( $arrValues['rental_collection_count'] );
		if( isset( $arrValues['rental_collection_count_last_one_month'] ) && $boolDirectSet ) $this->set( 'm_intRentalCollectionCountLastOneMonth', trim( $arrValues['rental_collection_count_last_one_month'] ) ); elseif( isset( $arrValues['rental_collection_count_last_one_month'] ) ) $this->setRentalCollectionCountLastOneMonth( $arrValues['rental_collection_count_last_one_month'] );
		if( isset( $arrValues['rental_collection_count_last_one_year'] ) && $boolDirectSet ) $this->set( 'm_intRentalCollectionCountLastOneYear', trim( $arrValues['rental_collection_count_last_one_year'] ) ); elseif( isset( $arrValues['rental_collection_count_last_one_year'] ) ) $this->setRentalCollectionCountLastOneYear( $arrValues['rental_collection_count_last_one_year'] );
		if( isset( $arrValues['rental_collection_count_last_two_year'] ) && $boolDirectSet ) $this->set( 'm_intRentalCollectionCountLastTwoYear', trim( $arrValues['rental_collection_count_last_two_year'] ) ); elseif( isset( $arrValues['rental_collection_count_last_two_year'] ) ) $this->setRentalCollectionCountLastTwoYear( $arrValues['rental_collection_count_last_two_year'] );
		if( isset( $arrValues['medical_collection_count'] ) && $boolDirectSet ) $this->set( 'm_intMedicalCollectionCount', trim( $arrValues['medical_collection_count'] ) ); elseif( isset( $arrValues['medical_collection_count'] ) ) $this->setMedicalCollectionCount( $arrValues['medical_collection_count'] );
		if( isset( $arrValues['medical_collection_count_last_one_year'] ) && $boolDirectSet ) $this->set( 'm_intMedicalCollectionCountLastOneYear', trim( $arrValues['medical_collection_count_last_one_year'] ) ); elseif( isset( $arrValues['medical_collection_count_last_one_year'] ) ) $this->setMedicalCollectionCountLastOneYear( $arrValues['medical_collection_count_last_one_year'] );
		if( isset( $arrValues['medical_collection_count_last_two_year'] ) && $boolDirectSet ) $this->set( 'm_intMedicalCollectionCountLastTwoYear', trim( $arrValues['medical_collection_count_last_two_year'] ) ); elseif( isset( $arrValues['medical_collection_count_last_two_year'] ) ) $this->setMedicalCollectionCountLastTwoYear( $arrValues['medical_collection_count_last_two_year'] );
		if( isset( $arrValues['utility_collection_count'] ) && $boolDirectSet ) $this->set( 'm_intUtilityCollectionCount', trim( $arrValues['utility_collection_count'] ) ); elseif( isset( $arrValues['utility_collection_count'] ) ) $this->setUtilityCollectionCount( $arrValues['utility_collection_count'] );
		if( isset( $arrValues['utility_collection_count_last_one_year'] ) && $boolDirectSet ) $this->set( 'm_intUtilityCollectionCountLastOneYear', trim( $arrValues['utility_collection_count_last_one_year'] ) ); elseif( isset( $arrValues['utility_collection_count_last_one_year'] ) ) $this->setUtilityCollectionCountLastOneYear( $arrValues['utility_collection_count_last_one_year'] );
		if( isset( $arrValues['utility_collection_count_last_two_year'] ) && $boolDirectSet ) $this->set( 'm_intUtilityCollectionCountLastTwoYear', trim( $arrValues['utility_collection_count_last_two_year'] ) ); elseif( isset( $arrValues['utility_collection_count_last_two_year'] ) ) $this->setUtilityCollectionCountLastTwoYear( $arrValues['utility_collection_count_last_two_year'] );
		if( isset( $arrValues['student_collection_count'] ) && $boolDirectSet ) $this->set( 'm_intStudentCollectionCount', trim( $arrValues['student_collection_count'] ) ); elseif( isset( $arrValues['student_collection_count'] ) ) $this->setStudentCollectionCount( $arrValues['student_collection_count'] );
		if( isset( $arrValues['student_collection_count_last_one_year'] ) && $boolDirectSet ) $this->set( 'm_intStudentCollectionCountLastOneYear', trim( $arrValues['student_collection_count_last_one_year'] ) ); elseif( isset( $arrValues['student_collection_count_last_one_year'] ) ) $this->setStudentCollectionCountLastOneYear( $arrValues['student_collection_count_last_one_year'] );
		if( isset( $arrValues['student_collection_count_last_two_year'] ) && $boolDirectSet ) $this->set( 'm_intStudentCollectionCountLastTwoYear', trim( $arrValues['student_collection_count_last_two_year'] ) ); elseif( isset( $arrValues['student_collection_count_last_two_year'] ) ) $this->setStudentCollectionCountLastTwoYear( $arrValues['student_collection_count_last_two_year'] );
		if( isset( $arrValues['inquiry_count_last_ninty_days'] ) && $boolDirectSet ) $this->set( 'm_intInquiryCountLastNintyDays', trim( $arrValues['inquiry_count_last_ninty_days'] ) ); elseif( isset( $arrValues['inquiry_count_last_ninty_days'] ) ) $this->setInquiryCountLastNintyDays( $arrValues['inquiry_count_last_ninty_days'] );
		if( isset( $arrValues['inquiry_count_last_one_eighty_days'] ) && $boolDirectSet ) $this->set( 'm_intInquiryCountLastOneEightyDays', trim( $arrValues['inquiry_count_last_one_eighty_days'] ) ); elseif( isset( $arrValues['inquiry_count_last_one_eighty_days'] ) ) $this->setInquiryCountLastOneEightyDays( $arrValues['inquiry_count_last_one_eighty_days'] );
		if( isset( $arrValues['inquiry_count_last_one_year'] ) && $boolDirectSet ) $this->set( 'm_intInquiryCountLastOneYear', trim( $arrValues['inquiry_count_last_one_year'] ) ); elseif( isset( $arrValues['inquiry_count_last_one_year'] ) ) $this->setInquiryCountLastOneYear( $arrValues['inquiry_count_last_one_year'] );
		if( isset( $arrValues['inquiry_count_last_two_year'] ) && $boolDirectSet ) $this->set( 'm_intInquiryCountLastTwoYear', trim( $arrValues['inquiry_count_last_two_year'] ) ); elseif( isset( $arrValues['inquiry_count_last_two_year'] ) ) $this->setInquiryCountLastTwoYear( $arrValues['inquiry_count_last_two_year'] );
		if( isset( $arrValues['rv_rent'] ) && $boolDirectSet ) $this->set( 'm_fltRvRent', trim( $arrValues['rv_rent'] ) ); elseif( isset( $arrValues['rv_rent'] ) ) $this->setRvRent( $arrValues['rv_rent'] );
		if( isset( $arrValues['rv_applicant_income'] ) && $boolDirectSet ) $this->set( 'm_fltRvApplicantIncome', trim( $arrValues['rv_applicant_income'] ) ); elseif( isset( $arrValues['rv_applicant_income'] ) ) $this->setRvApplicantIncome( $arrValues['rv_applicant_income'] );
		if( isset( $arrValues['collection_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCollectionAmount', trim( $arrValues['collection_amount'] ) ); elseif( isset( $arrValues['collection_amount'] ) ) $this->setCollectionAmount( $arrValues['collection_amount'] );
		if( isset( $arrValues['rental_collection_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRentalCollectionAmount', trim( $arrValues['rental_collection_amount'] ) ); elseif( isset( $arrValues['rental_collection_amount'] ) ) $this->setRentalCollectionAmount( $arrValues['rental_collection_amount'] );
		if( isset( $arrValues['medical_collection_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMedicalCollectionAmount', trim( $arrValues['medical_collection_amount'] ) ); elseif( isset( $arrValues['medical_collection_amount'] ) ) $this->setMedicalCollectionAmount( $arrValues['medical_collection_amount'] );
		if( isset( $arrValues['utility_collection_amount'] ) && $boolDirectSet ) $this->set( 'm_fltUtilityCollectionAmount', trim( $arrValues['utility_collection_amount'] ) ); elseif( isset( $arrValues['utility_collection_amount'] ) ) $this->setUtilityCollectionAmount( $arrValues['utility_collection_amount'] );
		if( isset( $arrValues['student_collection_amount'] ) && $boolDirectSet ) $this->set( 'm_fltStudentCollectionAmount', trim( $arrValues['student_collection_amount'] ) ); elseif( isset( $arrValues['student_collection_amount'] ) ) $this->setStudentCollectionAmount( $arrValues['student_collection_amount'] );
		if( isset( $arrValues['debt_to_income'] ) && $boolDirectSet ) $this->set( 'm_fltDebtToIncome', trim( $arrValues['debt_to_income'] ) ); elseif( isset( $arrValues['debt_to_income'] ) ) $this->setDebtToIncome( $arrValues['debt_to_income'] );
		if( isset( $arrValues['delinquent_account_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltDelinquentAccountPercentage', trim( $arrValues['delinquent_account_percentage'] ) ); elseif( isset( $arrValues['delinquent_account_percentage'] ) ) $this->setDelinquentAccountPercentage( $arrValues['delinquent_account_percentage'] );
		if( isset( $arrValues['total_monthly_debt'] ) && $boolDirectSet ) $this->set( 'm_fltTotalMonthlyDebt', trim( $arrValues['total_monthly_debt'] ) ); elseif( isset( $arrValues['total_monthly_debt'] ) ) $this->setTotalMonthlyDebt( $arrValues['total_monthly_debt'] );
		if( isset( $arrValues['monthly_debt_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyDebtAmount', trim( $arrValues['monthly_debt_amount'] ) ); elseif( isset( $arrValues['monthly_debt_amount'] ) ) $this->setMonthlyDebtAmount( $arrValues['monthly_debt_amount'] );
		if( isset( $arrValues['credit_score'] ) && $boolDirectSet ) $this->set( 'm_strCreditScore', trim( $arrValues['credit_score'] ) ); elseif( isset( $arrValues['credit_score'] ) ) $this->setCreditScore( $arrValues['credit_score'] );
		if( isset( $arrValues['rv_index'] ) && $boolDirectSet ) $this->set( 'm_strRvIndex', trim( $arrValues['rv_index'] ) ); elseif( isset( $arrValues['rv_index'] ) ) $this->setRvIndex( $arrValues['rv_index'] );
		if( isset( $arrValues['had_bankruptcy'] ) && $boolDirectSet ) $this->set( 'm_boolHadBankruptcy', trim( stripcslashes( $arrValues['had_bankruptcy'] ) ) ); elseif( isset( $arrValues['had_bankruptcy'] ) ) $this->setHadBankruptcy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['had_bankruptcy'] ) : $arrValues['had_bankruptcy'] );
		if( isset( $arrValues['had_foreclosure'] ) && $boolDirectSet ) $this->set( 'm_boolHadForeclosure', trim( stripcslashes( $arrValues['had_foreclosure'] ) ) ); elseif( isset( $arrValues['had_foreclosure'] ) ) $this->setHadForeclosure( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['had_foreclosure'] ) : $arrValues['had_foreclosure'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['screening_applicant_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicantTypeId', trim( $arrValues['screening_applicant_type_id'] ) ); elseif( isset( $arrValues['screening_applicant_type_id'] ) ) $this->setScreeningApplicantTypeId( $arrValues['screening_applicant_type_id'] );
		if( isset( $arrValues['total_tradeline_accounts'] ) && $boolDirectSet ) $this->set( 'm_intTotalTradelineAccounts', trim( $arrValues['total_tradeline_accounts'] ) ); elseif( isset( $arrValues['total_tradeline_accounts'] ) ) $this->setTotalTradelineAccounts( $arrValues['total_tradeline_accounts'] );
		if( isset( $arrValues['total_tradelines_count_last_two_years'] ) && $boolDirectSet ) $this->set( 'm_intTotalTradelinesCountLastTwoYears', trim( $arrValues['total_tradelines_count_last_two_years'] ) ); elseif( isset( $arrValues['total_tradelines_count_last_two_years'] ) ) $this->setTotalTradelinesCountLastTwoYears( $arrValues['total_tradelines_count_last_two_years'] );
		if( isset( $arrValues['positive_tradeline_accounts'] ) && $boolDirectSet ) $this->set( 'm_intPositiveTradelineAccounts', trim( $arrValues['positive_tradeline_accounts'] ) ); elseif( isset( $arrValues['positive_tradeline_accounts'] ) ) $this->setPositiveTradelineAccounts( $arrValues['positive_tradeline_accounts'] );
		if( isset( $arrValues['positive_tradelines_count_last_two_years'] ) && $boolDirectSet ) $this->set( 'm_intPositiveTradelinesCountLastTwoYears', trim( $arrValues['positive_tradelines_count_last_two_years'] ) ); elseif( isset( $arrValues['positive_tradelines_count_last_two_years'] ) ) $this->setPositiveTradelinesCountLastTwoYears( $arrValues['positive_tradelines_count_last_two_years'] );
		if( isset( $arrValues['negative_tradelines_count'] ) && $boolDirectSet ) $this->set( 'm_intNegativeTradelinesCount', trim( $arrValues['negative_tradelines_count'] ) ); elseif( isset( $arrValues['negative_tradelines_count'] ) ) $this->setNegativeTradelinesCount( $arrValues['negative_tradelines_count'] );
		if( isset( $arrValues['negative_tradelines_count_last_two_years'] ) && $boolDirectSet ) $this->set( 'm_intNegativeTradelinesCountLastTwoYears', trim( $arrValues['negative_tradelines_count_last_two_years'] ) ); elseif( isset( $arrValues['negative_tradelines_count_last_two_years'] ) ) $this->setNegativeTradelinesCountLastTwoYears( $arrValues['negative_tradelines_count_last_two_years'] );
		if( isset( $arrValues['collection_amount_last_two_years'] ) && $boolDirectSet ) $this->set( 'm_fltCollectionAmountLastTwoYears', trim( $arrValues['collection_amount_last_two_years'] ) ); elseif( isset( $arrValues['collection_amount_last_two_years'] ) ) $this->setCollectionAmountLastTwoYears( $arrValues['collection_amount_last_two_years'] );
		if( isset( $arrValues['rental_collection_amount_last_two_years'] ) && $boolDirectSet ) $this->set( 'm_fltRentalCollectionAmountLastTwoYears', trim( $arrValues['rental_collection_amount_last_two_years'] ) ); elseif( isset( $arrValues['rental_collection_amount_last_two_years'] ) ) $this->setRentalCollectionAmountLastTwoYears( $arrValues['rental_collection_amount_last_two_years'] );
		if( isset( $arrValues['medical_collection_amount_last_two_years'] ) && $boolDirectSet ) $this->set( 'm_fltMedicalCollectionAmountLastTwoYears', trim( $arrValues['medical_collection_amount_last_two_years'] ) ); elseif( isset( $arrValues['medical_collection_amount_last_two_years'] ) ) $this->setMedicalCollectionAmountLastTwoYears( $arrValues['medical_collection_amount_last_two_years'] );
		if( isset( $arrValues['utility_collection_amount_last_two_years'] ) && $boolDirectSet ) $this->set( 'm_fltUtilityCollectionAmountLastTwoYears', trim( $arrValues['utility_collection_amount_last_two_years'] ) ); elseif( isset( $arrValues['utility_collection_amount_last_two_years'] ) ) $this->setUtilityCollectionAmountLastTwoYears( $arrValues['utility_collection_amount_last_two_years'] );
		if( isset( $arrValues['student_collection_amount_last_two_years'] ) && $boolDirectSet ) $this->set( 'm_fltStudentCollectionAmountLastTwoYears', trim( $arrValues['student_collection_amount_last_two_years'] ) ); elseif( isset( $arrValues['student_collection_amount_last_two_years'] ) ) $this->setStudentCollectionAmountLastTwoYears( $arrValues['student_collection_amount_last_two_years'] );
		if( isset( $arrValues['open_collection_count'] ) && $boolDirectSet ) $this->set( 'm_intOpenCollectionCount', trim( $arrValues['open_collection_count'] ) ); elseif( isset( $arrValues['open_collection_count'] ) ) $this->setOpenCollectionCount( $arrValues['open_collection_count'] );
		if( isset( $arrValues['closed_collection_count'] ) && $boolDirectSet ) $this->set( 'm_intClosedCollectionCount', trim( $arrValues['closed_collection_count'] ) ); elseif( isset( $arrValues['closed_collection_count'] ) ) $this->setClosedCollectionCount( $arrValues['closed_collection_count'] );
		if( isset( $arrValues['rv_index_recalculated'] ) && $boolDirectSet ) $this->set( 'm_strRvIndexRecalculated', trim( $arrValues['rv_index_recalculated'] ) ); elseif( isset( $arrValues['rv_index_recalculated'] ) ) $this->setRvIndexRecalculated( $arrValues['rv_index_recalculated'] );
		if( isset( $arrValues['is_screened_for_credit'] ) && $boolDirectSet ) $this->set( 'm_boolIsScreenedForCredit', trim( stripcslashes( $arrValues['is_screened_for_credit'] ) ) ); elseif( isset( $arrValues['is_screened_for_credit'] ) ) $this->setIsScreenedForCredit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_screened_for_credit'] ) : $arrValues['is_screened_for_credit'] );
		if( isset( $arrValues['screened_on'] ) && $boolDirectSet ) $this->set( 'm_strScreenedOn', trim( $arrValues['screened_on'] ) ); elseif( isset( $arrValues['screened_on'] ) ) $this->setScreenedOn( $arrValues['screened_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRvLeaseNewOutcomesId( $intRvLeaseNewOutcomesId ) {
		$this->set( 'm_intRvLeaseNewOutcomesId', CStrings::strToIntDef( $intRvLeaseNewOutcomesId, NULL, false ) );
	}

	public function getRvLeaseNewOutcomesId() {
		return $this->m_intRvLeaseNewOutcomesId;
	}

	public function sqlRvLeaseNewOutcomesId() {
		return ( true == isset( $this->m_intRvLeaseNewOutcomesId ) ) ? ( string ) $this->m_intRvLeaseNewOutcomesId : 'NULL';
	}

	public function setCollectionCount( $intCollectionCount ) {
		$this->set( 'm_intCollectionCount', CStrings::strToIntDef( $intCollectionCount, NULL, false ) );
	}

	public function getCollectionCount() {
		return $this->m_intCollectionCount;
	}

	public function sqlCollectionCount() {
		return ( true == isset( $this->m_intCollectionCount ) ) ? ( string ) $this->m_intCollectionCount : '0';
	}

	public function setCollectionCountLastOneMonth( $intCollectionCountLastOneMonth ) {
		$this->set( 'm_intCollectionCountLastOneMonth', CStrings::strToIntDef( $intCollectionCountLastOneMonth, NULL, false ) );
	}

	public function getCollectionCountLastOneMonth() {
		return $this->m_intCollectionCountLastOneMonth;
	}

	public function sqlCollectionCountLastOneMonth() {
		return ( true == isset( $this->m_intCollectionCountLastOneMonth ) ) ? ( string ) $this->m_intCollectionCountLastOneMonth : '0';
	}

	public function setCollectionCountLastOneYear( $intCollectionCountLastOneYear ) {
		$this->set( 'm_intCollectionCountLastOneYear', CStrings::strToIntDef( $intCollectionCountLastOneYear, NULL, false ) );
	}

	public function getCollectionCountLastOneYear() {
		return $this->m_intCollectionCountLastOneYear;
	}

	public function sqlCollectionCountLastOneYear() {
		return ( true == isset( $this->m_intCollectionCountLastOneYear ) ) ? ( string ) $this->m_intCollectionCountLastOneYear : '0';
	}

	public function setCollectionCountLastTwoYear( $intCollectionCountLastTwoYear ) {
		$this->set( 'm_intCollectionCountLastTwoYear', CStrings::strToIntDef( $intCollectionCountLastTwoYear, NULL, false ) );
	}

	public function getCollectionCountLastTwoYear() {
		return $this->m_intCollectionCountLastTwoYear;
	}

	public function sqlCollectionCountLastTwoYear() {
		return ( true == isset( $this->m_intCollectionCountLastTwoYear ) ) ? ( string ) $this->m_intCollectionCountLastTwoYear : '0';
	}

	public function setRentalCollectionCount( $intRentalCollectionCount ) {
		$this->set( 'm_intRentalCollectionCount', CStrings::strToIntDef( $intRentalCollectionCount, NULL, false ) );
	}

	public function getRentalCollectionCount() {
		return $this->m_intRentalCollectionCount;
	}

	public function sqlRentalCollectionCount() {
		return ( true == isset( $this->m_intRentalCollectionCount ) ) ? ( string ) $this->m_intRentalCollectionCount : '0';
	}

	public function setRentalCollectionCountLastOneMonth( $intRentalCollectionCountLastOneMonth ) {
		$this->set( 'm_intRentalCollectionCountLastOneMonth', CStrings::strToIntDef( $intRentalCollectionCountLastOneMonth, NULL, false ) );
	}

	public function getRentalCollectionCountLastOneMonth() {
		return $this->m_intRentalCollectionCountLastOneMonth;
	}

	public function sqlRentalCollectionCountLastOneMonth() {
		return ( true == isset( $this->m_intRentalCollectionCountLastOneMonth ) ) ? ( string ) $this->m_intRentalCollectionCountLastOneMonth : '0';
	}

	public function setRentalCollectionCountLastOneYear( $intRentalCollectionCountLastOneYear ) {
		$this->set( 'm_intRentalCollectionCountLastOneYear', CStrings::strToIntDef( $intRentalCollectionCountLastOneYear, NULL, false ) );
	}

	public function getRentalCollectionCountLastOneYear() {
		return $this->m_intRentalCollectionCountLastOneYear;
	}

	public function sqlRentalCollectionCountLastOneYear() {
		return ( true == isset( $this->m_intRentalCollectionCountLastOneYear ) ) ? ( string ) $this->m_intRentalCollectionCountLastOneYear : '0';
	}

	public function setRentalCollectionCountLastTwoYear( $intRentalCollectionCountLastTwoYear ) {
		$this->set( 'm_intRentalCollectionCountLastTwoYear', CStrings::strToIntDef( $intRentalCollectionCountLastTwoYear, NULL, false ) );
	}

	public function getRentalCollectionCountLastTwoYear() {
		return $this->m_intRentalCollectionCountLastTwoYear;
	}

	public function sqlRentalCollectionCountLastTwoYear() {
		return ( true == isset( $this->m_intRentalCollectionCountLastTwoYear ) ) ? ( string ) $this->m_intRentalCollectionCountLastTwoYear : '0';
	}

	public function setMedicalCollectionCount( $intMedicalCollectionCount ) {
		$this->set( 'm_intMedicalCollectionCount', CStrings::strToIntDef( $intMedicalCollectionCount, NULL, false ) );
	}

	public function getMedicalCollectionCount() {
		return $this->m_intMedicalCollectionCount;
	}

	public function sqlMedicalCollectionCount() {
		return ( true == isset( $this->m_intMedicalCollectionCount ) ) ? ( string ) $this->m_intMedicalCollectionCount : '0';
	}

	public function setMedicalCollectionCountLastOneYear( $intMedicalCollectionCountLastOneYear ) {
		$this->set( 'm_intMedicalCollectionCountLastOneYear', CStrings::strToIntDef( $intMedicalCollectionCountLastOneYear, NULL, false ) );
	}

	public function getMedicalCollectionCountLastOneYear() {
		return $this->m_intMedicalCollectionCountLastOneYear;
	}

	public function sqlMedicalCollectionCountLastOneYear() {
		return ( true == isset( $this->m_intMedicalCollectionCountLastOneYear ) ) ? ( string ) $this->m_intMedicalCollectionCountLastOneYear : '0';
	}

	public function setMedicalCollectionCountLastTwoYear( $intMedicalCollectionCountLastTwoYear ) {
		$this->set( 'm_intMedicalCollectionCountLastTwoYear', CStrings::strToIntDef( $intMedicalCollectionCountLastTwoYear, NULL, false ) );
	}

	public function getMedicalCollectionCountLastTwoYear() {
		return $this->m_intMedicalCollectionCountLastTwoYear;
	}

	public function sqlMedicalCollectionCountLastTwoYear() {
		return ( true == isset( $this->m_intMedicalCollectionCountLastTwoYear ) ) ? ( string ) $this->m_intMedicalCollectionCountLastTwoYear : '0';
	}

	public function setUtilityCollectionCount( $intUtilityCollectionCount ) {
		$this->set( 'm_intUtilityCollectionCount', CStrings::strToIntDef( $intUtilityCollectionCount, NULL, false ) );
	}

	public function getUtilityCollectionCount() {
		return $this->m_intUtilityCollectionCount;
	}

	public function sqlUtilityCollectionCount() {
		return ( true == isset( $this->m_intUtilityCollectionCount ) ) ? ( string ) $this->m_intUtilityCollectionCount : '0';
	}

	public function setUtilityCollectionCountLastOneYear( $intUtilityCollectionCountLastOneYear ) {
		$this->set( 'm_intUtilityCollectionCountLastOneYear', CStrings::strToIntDef( $intUtilityCollectionCountLastOneYear, NULL, false ) );
	}

	public function getUtilityCollectionCountLastOneYear() {
		return $this->m_intUtilityCollectionCountLastOneYear;
	}

	public function sqlUtilityCollectionCountLastOneYear() {
		return ( true == isset( $this->m_intUtilityCollectionCountLastOneYear ) ) ? ( string ) $this->m_intUtilityCollectionCountLastOneYear : '0';
	}

	public function setUtilityCollectionCountLastTwoYear( $intUtilityCollectionCountLastTwoYear ) {
		$this->set( 'm_intUtilityCollectionCountLastTwoYear', CStrings::strToIntDef( $intUtilityCollectionCountLastTwoYear, NULL, false ) );
	}

	public function getUtilityCollectionCountLastTwoYear() {
		return $this->m_intUtilityCollectionCountLastTwoYear;
	}

	public function sqlUtilityCollectionCountLastTwoYear() {
		return ( true == isset( $this->m_intUtilityCollectionCountLastTwoYear ) ) ? ( string ) $this->m_intUtilityCollectionCountLastTwoYear : '0';
	}

	public function setStudentCollectionCount( $intStudentCollectionCount ) {
		$this->set( 'm_intStudentCollectionCount', CStrings::strToIntDef( $intStudentCollectionCount, NULL, false ) );
	}

	public function getStudentCollectionCount() {
		return $this->m_intStudentCollectionCount;
	}

	public function sqlStudentCollectionCount() {
		return ( true == isset( $this->m_intStudentCollectionCount ) ) ? ( string ) $this->m_intStudentCollectionCount : '0';
	}

	public function setStudentCollectionCountLastOneYear( $intStudentCollectionCountLastOneYear ) {
		$this->set( 'm_intStudentCollectionCountLastOneYear', CStrings::strToIntDef( $intStudentCollectionCountLastOneYear, NULL, false ) );
	}

	public function getStudentCollectionCountLastOneYear() {
		return $this->m_intStudentCollectionCountLastOneYear;
	}

	public function sqlStudentCollectionCountLastOneYear() {
		return ( true == isset( $this->m_intStudentCollectionCountLastOneYear ) ) ? ( string ) $this->m_intStudentCollectionCountLastOneYear : '0';
	}

	public function setStudentCollectionCountLastTwoYear( $intStudentCollectionCountLastTwoYear ) {
		$this->set( 'm_intStudentCollectionCountLastTwoYear', CStrings::strToIntDef( $intStudentCollectionCountLastTwoYear, NULL, false ) );
	}

	public function getStudentCollectionCountLastTwoYear() {
		return $this->m_intStudentCollectionCountLastTwoYear;
	}

	public function sqlStudentCollectionCountLastTwoYear() {
		return ( true == isset( $this->m_intStudentCollectionCountLastTwoYear ) ) ? ( string ) $this->m_intStudentCollectionCountLastTwoYear : '0';
	}

	public function setInquiryCountLastNintyDays( $intInquiryCountLastNintyDays ) {
		$this->set( 'm_intInquiryCountLastNintyDays', CStrings::strToIntDef( $intInquiryCountLastNintyDays, NULL, false ) );
	}

	public function getInquiryCountLastNintyDays() {
		return $this->m_intInquiryCountLastNintyDays;
	}

	public function sqlInquiryCountLastNintyDays() {
		return ( true == isset( $this->m_intInquiryCountLastNintyDays ) ) ? ( string ) $this->m_intInquiryCountLastNintyDays : '0';
	}

	public function setInquiryCountLastOneEightyDays( $intInquiryCountLastOneEightyDays ) {
		$this->set( 'm_intInquiryCountLastOneEightyDays', CStrings::strToIntDef( $intInquiryCountLastOneEightyDays, NULL, false ) );
	}

	public function getInquiryCountLastOneEightyDays() {
		return $this->m_intInquiryCountLastOneEightyDays;
	}

	public function sqlInquiryCountLastOneEightyDays() {
		return ( true == isset( $this->m_intInquiryCountLastOneEightyDays ) ) ? ( string ) $this->m_intInquiryCountLastOneEightyDays : '0';
	}

	public function setInquiryCountLastOneYear( $intInquiryCountLastOneYear ) {
		$this->set( 'm_intInquiryCountLastOneYear', CStrings::strToIntDef( $intInquiryCountLastOneYear, NULL, false ) );
	}

	public function getInquiryCountLastOneYear() {
		return $this->m_intInquiryCountLastOneYear;
	}

	public function sqlInquiryCountLastOneYear() {
		return ( true == isset( $this->m_intInquiryCountLastOneYear ) ) ? ( string ) $this->m_intInquiryCountLastOneYear : '0';
	}

	public function setInquiryCountLastTwoYear( $intInquiryCountLastTwoYear ) {
		$this->set( 'm_intInquiryCountLastTwoYear', CStrings::strToIntDef( $intInquiryCountLastTwoYear, NULL, false ) );
	}

	public function getInquiryCountLastTwoYear() {
		return $this->m_intInquiryCountLastTwoYear;
	}

	public function sqlInquiryCountLastTwoYear() {
		return ( true == isset( $this->m_intInquiryCountLastTwoYear ) ) ? ( string ) $this->m_intInquiryCountLastTwoYear : '0';
	}

	public function setRvRent( $fltRvRent ) {
		$this->set( 'm_fltRvRent', CStrings::strToFloatDef( $fltRvRent, NULL, false, 2 ) );
	}

	public function getRvRent() {
		return $this->m_fltRvRent;
	}

	public function sqlRvRent() {
		return ( true == isset( $this->m_fltRvRent ) ) ? ( string ) $this->m_fltRvRent : '0';
	}

	public function setRvApplicantIncome( $fltRvApplicantIncome ) {
		$this->set( 'm_fltRvApplicantIncome', CStrings::strToFloatDef( $fltRvApplicantIncome, NULL, false, 2 ) );
	}

	public function getRvApplicantIncome() {
		return $this->m_fltRvApplicantIncome;
	}

	public function sqlRvApplicantIncome() {
		return ( true == isset( $this->m_fltRvApplicantIncome ) ) ? ( string ) $this->m_fltRvApplicantIncome : '0';
	}

	public function setCollectionAmount( $fltCollectionAmount ) {
		$this->set( 'm_fltCollectionAmount', CStrings::strToFloatDef( $fltCollectionAmount, NULL, false, 2 ) );
	}

	public function getCollectionAmount() {
		return $this->m_fltCollectionAmount;
	}

	public function sqlCollectionAmount() {
		return ( true == isset( $this->m_fltCollectionAmount ) ) ? ( string ) $this->m_fltCollectionAmount : '0';
	}

	public function setRentalCollectionAmount( $fltRentalCollectionAmount ) {
		$this->set( 'm_fltRentalCollectionAmount', CStrings::strToFloatDef( $fltRentalCollectionAmount, NULL, false, 2 ) );
	}

	public function getRentalCollectionAmount() {
		return $this->m_fltRentalCollectionAmount;
	}

	public function sqlRentalCollectionAmount() {
		return ( true == isset( $this->m_fltRentalCollectionAmount ) ) ? ( string ) $this->m_fltRentalCollectionAmount : '0';
	}

	public function setMedicalCollectionAmount( $fltMedicalCollectionAmount ) {
		$this->set( 'm_fltMedicalCollectionAmount', CStrings::strToFloatDef( $fltMedicalCollectionAmount, NULL, false, 2 ) );
	}

	public function getMedicalCollectionAmount() {
		return $this->m_fltMedicalCollectionAmount;
	}

	public function sqlMedicalCollectionAmount() {
		return ( true == isset( $this->m_fltMedicalCollectionAmount ) ) ? ( string ) $this->m_fltMedicalCollectionAmount : '0';
	}

	public function setUtilityCollectionAmount( $fltUtilityCollectionAmount ) {
		$this->set( 'm_fltUtilityCollectionAmount', CStrings::strToFloatDef( $fltUtilityCollectionAmount, NULL, false, 2 ) );
	}

	public function getUtilityCollectionAmount() {
		return $this->m_fltUtilityCollectionAmount;
	}

	public function sqlUtilityCollectionAmount() {
		return ( true == isset( $this->m_fltUtilityCollectionAmount ) ) ? ( string ) $this->m_fltUtilityCollectionAmount : '0';
	}

	public function setStudentCollectionAmount( $fltStudentCollectionAmount ) {
		$this->set( 'm_fltStudentCollectionAmount', CStrings::strToFloatDef( $fltStudentCollectionAmount, NULL, false, 2 ) );
	}

	public function getStudentCollectionAmount() {
		return $this->m_fltStudentCollectionAmount;
	}

	public function sqlStudentCollectionAmount() {
		return ( true == isset( $this->m_fltStudentCollectionAmount ) ) ? ( string ) $this->m_fltStudentCollectionAmount : '0';
	}

	public function setDebtToIncome( $fltDebtToIncome ) {
		$this->set( 'm_fltDebtToIncome', CStrings::strToFloatDef( $fltDebtToIncome, NULL, false, 2 ) );
	}

	public function getDebtToIncome() {
		return $this->m_fltDebtToIncome;
	}

	public function sqlDebtToIncome() {
		return ( true == isset( $this->m_fltDebtToIncome ) ) ? ( string ) $this->m_fltDebtToIncome : '0';
	}

	public function setDelinquentAccountPercentage( $fltDelinquentAccountPercentage ) {
		$this->set( 'm_fltDelinquentAccountPercentage', CStrings::strToFloatDef( $fltDelinquentAccountPercentage, NULL, false, 2 ) );
	}

	public function getDelinquentAccountPercentage() {
		return $this->m_fltDelinquentAccountPercentage;
	}

	public function sqlDelinquentAccountPercentage() {
		return ( true == isset( $this->m_fltDelinquentAccountPercentage ) ) ? ( string ) $this->m_fltDelinquentAccountPercentage : '0';
	}

	public function setTotalMonthlyDebt( $fltTotalMonthlyDebt ) {
		$this->set( 'm_fltTotalMonthlyDebt', CStrings::strToFloatDef( $fltTotalMonthlyDebt, NULL, false, 2 ) );
	}

	public function getTotalMonthlyDebt() {
		return $this->m_fltTotalMonthlyDebt;
	}

	public function sqlTotalMonthlyDebt() {
		return ( true == isset( $this->m_fltTotalMonthlyDebt ) ) ? ( string ) $this->m_fltTotalMonthlyDebt : '0';
	}

	public function setMonthlyDebtAmount( $fltMonthlyDebtAmount ) {
		$this->set( 'm_fltMonthlyDebtAmount', CStrings::strToFloatDef( $fltMonthlyDebtAmount, NULL, false, 2 ) );
	}

	public function getMonthlyDebtAmount() {
		return $this->m_fltMonthlyDebtAmount;
	}

	public function sqlMonthlyDebtAmount() {
		return ( true == isset( $this->m_fltMonthlyDebtAmount ) ) ? ( string ) $this->m_fltMonthlyDebtAmount : '0';
	}

	public function setCreditScore( $strCreditScore ) {
		$this->set( 'm_strCreditScore', CStrings::strTrimDef( $strCreditScore, -1, NULL, true ) );
	}

	public function getCreditScore() {
		return $this->m_strCreditScore;
	}

	public function sqlCreditScore() {
		return ( true == isset( $this->m_strCreditScore ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCreditScore ) : '\'' . addslashes( $this->m_strCreditScore ) . '\'' ) : 'NULL';
	}

	public function setRvIndex( $strRvIndex ) {
		$this->set( 'm_strRvIndex', CStrings::strTrimDef( $strRvIndex, -1, NULL, true ) );
	}

	public function getRvIndex() {
		return $this->m_strRvIndex;
	}

	public function sqlRvIndex() {
		return ( true == isset( $this->m_strRvIndex ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRvIndex ) : '\'' . addslashes( $this->m_strRvIndex ) . '\'' ) : 'NULL';
	}

	public function setHadBankruptcy( $boolHadBankruptcy ) {
		$this->set( 'm_boolHadBankruptcy', CStrings::strToBool( $boolHadBankruptcy ) );
	}

	public function getHadBankruptcy() {
		return $this->m_boolHadBankruptcy;
	}

	public function sqlHadBankruptcy() {
		return ( true == isset( $this->m_boolHadBankruptcy ) ) ? '\'' . ( true == ( bool ) $this->m_boolHadBankruptcy ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHadForeclosure( $boolHadForeclosure ) {
		$this->set( 'm_boolHadForeclosure', CStrings::strToBool( $boolHadForeclosure ) );
	}

	public function getHadForeclosure() {
		return $this->m_boolHadForeclosure;
	}

	public function sqlHadForeclosure() {
		return ( true == isset( $this->m_boolHadForeclosure ) ) ? '\'' . ( true == ( bool ) $this->m_boolHadForeclosure ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setScreeningApplicantTypeId( $intScreeningApplicantTypeId ) {
		$this->set( 'm_intScreeningApplicantTypeId', CStrings::strToIntDef( $intScreeningApplicantTypeId, NULL, false ) );
	}

	public function getScreeningApplicantTypeId() {
		return $this->m_intScreeningApplicantTypeId;
	}

	public function sqlScreeningApplicantTypeId() {
		return ( true == isset( $this->m_intScreeningApplicantTypeId ) ) ? ( string ) $this->m_intScreeningApplicantTypeId : 'NULL';
	}

	public function setTotalTradelineAccounts( $intTotalTradelineAccounts ) {
		$this->set( 'm_intTotalTradelineAccounts', CStrings::strToIntDef( $intTotalTradelineAccounts, NULL, false ) );
	}

	public function getTotalTradelineAccounts() {
		return $this->m_intTotalTradelineAccounts;
	}

	public function sqlTotalTradelineAccounts() {
		return ( true == isset( $this->m_intTotalTradelineAccounts ) ) ? ( string ) $this->m_intTotalTradelineAccounts : 'NULL';
	}

	public function setTotalTradelinesCountLastTwoYears( $intTotalTradelinesCountLastTwoYears ) {
		$this->set( 'm_intTotalTradelinesCountLastTwoYears', CStrings::strToIntDef( $intTotalTradelinesCountLastTwoYears, NULL, false ) );
	}

	public function getTotalTradelinesCountLastTwoYears() {
		return $this->m_intTotalTradelinesCountLastTwoYears;
	}

	public function sqlTotalTradelinesCountLastTwoYears() {
		return ( true == isset( $this->m_intTotalTradelinesCountLastTwoYears ) ) ? ( string ) $this->m_intTotalTradelinesCountLastTwoYears : 'NULL';
	}

	public function setPositiveTradelineAccounts( $intPositiveTradelineAccounts ) {
		$this->set( 'm_intPositiveTradelineAccounts', CStrings::strToIntDef( $intPositiveTradelineAccounts, NULL, false ) );
	}

	public function getPositiveTradelineAccounts() {
		return $this->m_intPositiveTradelineAccounts;
	}

	public function sqlPositiveTradelineAccounts() {
		return ( true == isset( $this->m_intPositiveTradelineAccounts ) ) ? ( string ) $this->m_intPositiveTradelineAccounts : 'NULL';
	}

	public function setPositiveTradelinesCountLastTwoYears( $intPositiveTradelinesCountLastTwoYears ) {
		$this->set( 'm_intPositiveTradelinesCountLastTwoYears', CStrings::strToIntDef( $intPositiveTradelinesCountLastTwoYears, NULL, false ) );
	}

	public function getPositiveTradelinesCountLastTwoYears() {
		return $this->m_intPositiveTradelinesCountLastTwoYears;
	}

	public function sqlPositiveTradelinesCountLastTwoYears() {
		return ( true == isset( $this->m_intPositiveTradelinesCountLastTwoYears ) ) ? ( string ) $this->m_intPositiveTradelinesCountLastTwoYears : 'NULL';
	}

	public function setNegativeTradelinesCount( $intNegativeTradelinesCount ) {
		$this->set( 'm_intNegativeTradelinesCount', CStrings::strToIntDef( $intNegativeTradelinesCount, NULL, false ) );
	}

	public function getNegativeTradelinesCount() {
		return $this->m_intNegativeTradelinesCount;
	}

	public function sqlNegativeTradelinesCount() {
		return ( true == isset( $this->m_intNegativeTradelinesCount ) ) ? ( string ) $this->m_intNegativeTradelinesCount : 'NULL';
	}

	public function setNegativeTradelinesCountLastTwoYears( $intNegativeTradelinesCountLastTwoYears ) {
		$this->set( 'm_intNegativeTradelinesCountLastTwoYears', CStrings::strToIntDef( $intNegativeTradelinesCountLastTwoYears, NULL, false ) );
	}

	public function getNegativeTradelinesCountLastTwoYears() {
		return $this->m_intNegativeTradelinesCountLastTwoYears;
	}

	public function sqlNegativeTradelinesCountLastTwoYears() {
		return ( true == isset( $this->m_intNegativeTradelinesCountLastTwoYears ) ) ? ( string ) $this->m_intNegativeTradelinesCountLastTwoYears : 'NULL';
	}

	public function setCollectionAmountLastTwoYears( $fltCollectionAmountLastTwoYears ) {
		$this->set( 'm_fltCollectionAmountLastTwoYears', CStrings::strToFloatDef( $fltCollectionAmountLastTwoYears, NULL, false, 2 ) );
	}

	public function getCollectionAmountLastTwoYears() {
		return $this->m_fltCollectionAmountLastTwoYears;
	}

	public function sqlCollectionAmountLastTwoYears() {
		return ( true == isset( $this->m_fltCollectionAmountLastTwoYears ) ) ? ( string ) $this->m_fltCollectionAmountLastTwoYears : 'NULL';
	}

	public function setRentalCollectionAmountLastTwoYears( $fltRentalCollectionAmountLastTwoYears ) {
		$this->set( 'm_fltRentalCollectionAmountLastTwoYears', CStrings::strToFloatDef( $fltRentalCollectionAmountLastTwoYears, NULL, false, 2 ) );
	}

	public function getRentalCollectionAmountLastTwoYears() {
		return $this->m_fltRentalCollectionAmountLastTwoYears;
	}

	public function sqlRentalCollectionAmountLastTwoYears() {
		return ( true == isset( $this->m_fltRentalCollectionAmountLastTwoYears ) ) ? ( string ) $this->m_fltRentalCollectionAmountLastTwoYears : 'NULL';
	}

	public function setMedicalCollectionAmountLastTwoYears( $fltMedicalCollectionAmountLastTwoYears ) {
		$this->set( 'm_fltMedicalCollectionAmountLastTwoYears', CStrings::strToFloatDef( $fltMedicalCollectionAmountLastTwoYears, NULL, false, 2 ) );
	}

	public function getMedicalCollectionAmountLastTwoYears() {
		return $this->m_fltMedicalCollectionAmountLastTwoYears;
	}

	public function sqlMedicalCollectionAmountLastTwoYears() {
		return ( true == isset( $this->m_fltMedicalCollectionAmountLastTwoYears ) ) ? ( string ) $this->m_fltMedicalCollectionAmountLastTwoYears : 'NULL';
	}

	public function setUtilityCollectionAmountLastTwoYears( $fltUtilityCollectionAmountLastTwoYears ) {
		$this->set( 'm_fltUtilityCollectionAmountLastTwoYears', CStrings::strToFloatDef( $fltUtilityCollectionAmountLastTwoYears, NULL, false, 2 ) );
	}

	public function getUtilityCollectionAmountLastTwoYears() {
		return $this->m_fltUtilityCollectionAmountLastTwoYears;
	}

	public function sqlUtilityCollectionAmountLastTwoYears() {
		return ( true == isset( $this->m_fltUtilityCollectionAmountLastTwoYears ) ) ? ( string ) $this->m_fltUtilityCollectionAmountLastTwoYears : 'NULL';
	}

	public function setStudentCollectionAmountLastTwoYears( $fltStudentCollectionAmountLastTwoYears ) {
		$this->set( 'm_fltStudentCollectionAmountLastTwoYears', CStrings::strToFloatDef( $fltStudentCollectionAmountLastTwoYears, NULL, false, 2 ) );
	}

	public function getStudentCollectionAmountLastTwoYears() {
		return $this->m_fltStudentCollectionAmountLastTwoYears;
	}

	public function sqlStudentCollectionAmountLastTwoYears() {
		return ( true == isset( $this->m_fltStudentCollectionAmountLastTwoYears ) ) ? ( string ) $this->m_fltStudentCollectionAmountLastTwoYears : 'NULL';
	}

	public function setOpenCollectionCount( $intOpenCollectionCount ) {
		$this->set( 'm_intOpenCollectionCount', CStrings::strToIntDef( $intOpenCollectionCount, NULL, false ) );
	}

	public function getOpenCollectionCount() {
		return $this->m_intOpenCollectionCount;
	}

	public function sqlOpenCollectionCount() {
		return ( true == isset( $this->m_intOpenCollectionCount ) ) ? ( string ) $this->m_intOpenCollectionCount : 'NULL';
	}

	public function setClosedCollectionCount( $intClosedCollectionCount ) {
		$this->set( 'm_intClosedCollectionCount', CStrings::strToIntDef( $intClosedCollectionCount, NULL, false ) );
	}

	public function getClosedCollectionCount() {
		return $this->m_intClosedCollectionCount;
	}

	public function sqlClosedCollectionCount() {
		return ( true == isset( $this->m_intClosedCollectionCount ) ) ? ( string ) $this->m_intClosedCollectionCount : 'NULL';
	}

	public function setRvIndexRecalculated( $strRvIndexRecalculated ) {
		$this->set( 'm_strRvIndexRecalculated', CStrings::strTrimDef( $strRvIndexRecalculated, -1, NULL, true ) );
	}

	public function getRvIndexRecalculated() {
		return $this->m_strRvIndexRecalculated;
	}

	public function sqlRvIndexRecalculated() {
		return ( true == isset( $this->m_strRvIndexRecalculated ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRvIndexRecalculated ) : '\'' . addslashes( $this->m_strRvIndexRecalculated ) . '\'' ) : 'NULL';
	}

	public function setIsScreenedForCredit( $boolIsScreenedForCredit ) {
		$this->set( 'm_boolIsScreenedForCredit', CStrings::strToBool( $boolIsScreenedForCredit ) );
	}

	public function getIsScreenedForCredit() {
		return $this->m_boolIsScreenedForCredit;
	}

	public function sqlIsScreenedForCredit() {
		return ( true == isset( $this->m_boolIsScreenedForCredit ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsScreenedForCredit ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setScreenedOn( $strScreenedOn ) {
		$this->set( 'm_strScreenedOn', CStrings::strTrimDef( $strScreenedOn, -1, NULL, true ) );
	}

	public function getScreenedOn() {
		return $this->m_strScreenedOn;
	}

	public function sqlScreenedOn() {
		return ( true == isset( $this->m_strScreenedOn ) ) ? '\'' . $this->m_strScreenedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, rv_lease_new_outcomes_id, collection_count, collection_count_last_one_month, collection_count_last_one_year, collection_count_last_two_year, rental_collection_count, rental_collection_count_last_one_month, rental_collection_count_last_one_year, rental_collection_count_last_two_year, medical_collection_count, medical_collection_count_last_one_year, medical_collection_count_last_two_year, utility_collection_count, utility_collection_count_last_one_year, utility_collection_count_last_two_year, student_collection_count, student_collection_count_last_one_year, student_collection_count_last_two_year, inquiry_count_last_ninty_days, inquiry_count_last_one_eighty_days, inquiry_count_last_one_year, inquiry_count_last_two_year, rv_rent, rv_applicant_income, collection_amount, rental_collection_amount, medical_collection_amount, utility_collection_amount, student_collection_amount, debt_to_income, delinquent_account_percentage, total_monthly_debt, monthly_debt_amount, credit_score, rv_index, had_bankruptcy, had_foreclosure, updated_by, updated_on, created_by, created_on, screening_applicant_type_id, total_tradeline_accounts, total_tradelines_count_last_two_years, positive_tradeline_accounts, positive_tradelines_count_last_two_years, negative_tradelines_count, negative_tradelines_count_last_two_years, collection_amount_last_two_years, rental_collection_amount_last_two_years, medical_collection_amount_last_two_years, utility_collection_amount_last_two_years, student_collection_amount_last_two_years, open_collection_count, closed_collection_count, rv_index_recalculated, is_screened_for_credit, screened_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlRvLeaseNewOutcomesId() . ', ' .
						$this->sqlCollectionCount() . ', ' .
						$this->sqlCollectionCountLastOneMonth() . ', ' .
						$this->sqlCollectionCountLastOneYear() . ', ' .
						$this->sqlCollectionCountLastTwoYear() . ', ' .
						$this->sqlRentalCollectionCount() . ', ' .
						$this->sqlRentalCollectionCountLastOneMonth() . ', ' .
						$this->sqlRentalCollectionCountLastOneYear() . ', ' .
						$this->sqlRentalCollectionCountLastTwoYear() . ', ' .
						$this->sqlMedicalCollectionCount() . ', ' .
						$this->sqlMedicalCollectionCountLastOneYear() . ', ' .
						$this->sqlMedicalCollectionCountLastTwoYear() . ', ' .
						$this->sqlUtilityCollectionCount() . ', ' .
						$this->sqlUtilityCollectionCountLastOneYear() . ', ' .
						$this->sqlUtilityCollectionCountLastTwoYear() . ', ' .
						$this->sqlStudentCollectionCount() . ', ' .
						$this->sqlStudentCollectionCountLastOneYear() . ', ' .
						$this->sqlStudentCollectionCountLastTwoYear() . ', ' .
						$this->sqlInquiryCountLastNintyDays() . ', ' .
						$this->sqlInquiryCountLastOneEightyDays() . ', ' .
						$this->sqlInquiryCountLastOneYear() . ', ' .
						$this->sqlInquiryCountLastTwoYear() . ', ' .
						$this->sqlRvRent() . ', ' .
						$this->sqlRvApplicantIncome() . ', ' .
						$this->sqlCollectionAmount() . ', ' .
						$this->sqlRentalCollectionAmount() . ', ' .
						$this->sqlMedicalCollectionAmount() . ', ' .
						$this->sqlUtilityCollectionAmount() . ', ' .
						$this->sqlStudentCollectionAmount() . ', ' .
						$this->sqlDebtToIncome() . ', ' .
						$this->sqlDelinquentAccountPercentage() . ', ' .
						$this->sqlTotalMonthlyDebt() . ', ' .
						$this->sqlMonthlyDebtAmount() . ', ' .
						$this->sqlCreditScore() . ', ' .
						$this->sqlRvIndex() . ', ' .
						$this->sqlHadBankruptcy() . ', ' .
						$this->sqlHadForeclosure() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlScreeningApplicantTypeId() . ', ' .
						$this->sqlTotalTradelineAccounts() . ', ' .
						$this->sqlTotalTradelinesCountLastTwoYears() . ', ' .
						$this->sqlPositiveTradelineAccounts() . ', ' .
						$this->sqlPositiveTradelinesCountLastTwoYears() . ', ' .
						$this->sqlNegativeTradelinesCount() . ', ' .
						$this->sqlNegativeTradelinesCountLastTwoYears() . ', ' .
						$this->sqlCollectionAmountLastTwoYears() . ', ' .
						$this->sqlRentalCollectionAmountLastTwoYears() . ', ' .
						$this->sqlMedicalCollectionAmountLastTwoYears() . ', ' .
						$this->sqlUtilityCollectionAmountLastTwoYears() . ', ' .
						$this->sqlStudentCollectionAmountLastTwoYears() . ', ' .
						$this->sqlOpenCollectionCount() . ', ' .
						$this->sqlClosedCollectionCount() . ', ' .
						$this->sqlRvIndexRecalculated() . ', ' .
						$this->sqlIsScreenedForCredit() . ', ' .
						$this->sqlScreenedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rv_lease_new_outcomes_id = ' . $this->sqlRvLeaseNewOutcomesId(). ',' ; } elseif( true == array_key_exists( 'RvLeaseNewOutcomesId', $this->getChangedColumns() ) ) { $strSql .= ' rv_lease_new_outcomes_id = ' . $this->sqlRvLeaseNewOutcomesId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' collection_count = ' . $this->sqlCollectionCount(). ',' ; } elseif( true == array_key_exists( 'CollectionCount', $this->getChangedColumns() ) ) { $strSql .= ' collection_count = ' . $this->sqlCollectionCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' collection_count_last_one_month = ' . $this->sqlCollectionCountLastOneMonth(). ',' ; } elseif( true == array_key_exists( 'CollectionCountLastOneMonth', $this->getChangedColumns() ) ) { $strSql .= ' collection_count_last_one_month = ' . $this->sqlCollectionCountLastOneMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' collection_count_last_one_year = ' . $this->sqlCollectionCountLastOneYear(). ',' ; } elseif( true == array_key_exists( 'CollectionCountLastOneYear', $this->getChangedColumns() ) ) { $strSql .= ' collection_count_last_one_year = ' . $this->sqlCollectionCountLastOneYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' collection_count_last_two_year = ' . $this->sqlCollectionCountLastTwoYear(). ',' ; } elseif( true == array_key_exists( 'CollectionCountLastTwoYear', $this->getChangedColumns() ) ) { $strSql .= ' collection_count_last_two_year = ' . $this->sqlCollectionCountLastTwoYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rental_collection_count = ' . $this->sqlRentalCollectionCount(). ',' ; } elseif( true == array_key_exists( 'RentalCollectionCount', $this->getChangedColumns() ) ) { $strSql .= ' rental_collection_count = ' . $this->sqlRentalCollectionCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rental_collection_count_last_one_month = ' . $this->sqlRentalCollectionCountLastOneMonth(). ',' ; } elseif( true == array_key_exists( 'RentalCollectionCountLastOneMonth', $this->getChangedColumns() ) ) { $strSql .= ' rental_collection_count_last_one_month = ' . $this->sqlRentalCollectionCountLastOneMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rental_collection_count_last_one_year = ' . $this->sqlRentalCollectionCountLastOneYear(). ',' ; } elseif( true == array_key_exists( 'RentalCollectionCountLastOneYear', $this->getChangedColumns() ) ) { $strSql .= ' rental_collection_count_last_one_year = ' . $this->sqlRentalCollectionCountLastOneYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rental_collection_count_last_two_year = ' . $this->sqlRentalCollectionCountLastTwoYear(). ',' ; } elseif( true == array_key_exists( 'RentalCollectionCountLastTwoYear', $this->getChangedColumns() ) ) { $strSql .= ' rental_collection_count_last_two_year = ' . $this->sqlRentalCollectionCountLastTwoYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' medical_collection_count = ' . $this->sqlMedicalCollectionCount(). ',' ; } elseif( true == array_key_exists( 'MedicalCollectionCount', $this->getChangedColumns() ) ) { $strSql .= ' medical_collection_count = ' . $this->sqlMedicalCollectionCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' medical_collection_count_last_one_year = ' . $this->sqlMedicalCollectionCountLastOneYear(). ',' ; } elseif( true == array_key_exists( 'MedicalCollectionCountLastOneYear', $this->getChangedColumns() ) ) { $strSql .= ' medical_collection_count_last_one_year = ' . $this->sqlMedicalCollectionCountLastOneYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' medical_collection_count_last_two_year = ' . $this->sqlMedicalCollectionCountLastTwoYear(). ',' ; } elseif( true == array_key_exists( 'MedicalCollectionCountLastTwoYear', $this->getChangedColumns() ) ) { $strSql .= ' medical_collection_count_last_two_year = ' . $this->sqlMedicalCollectionCountLastTwoYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_collection_count = ' . $this->sqlUtilityCollectionCount(). ',' ; } elseif( true == array_key_exists( 'UtilityCollectionCount', $this->getChangedColumns() ) ) { $strSql .= ' utility_collection_count = ' . $this->sqlUtilityCollectionCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_collection_count_last_one_year = ' . $this->sqlUtilityCollectionCountLastOneYear(). ',' ; } elseif( true == array_key_exists( 'UtilityCollectionCountLastOneYear', $this->getChangedColumns() ) ) { $strSql .= ' utility_collection_count_last_one_year = ' . $this->sqlUtilityCollectionCountLastOneYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_collection_count_last_two_year = ' . $this->sqlUtilityCollectionCountLastTwoYear(). ',' ; } elseif( true == array_key_exists( 'UtilityCollectionCountLastTwoYear', $this->getChangedColumns() ) ) { $strSql .= ' utility_collection_count_last_two_year = ' . $this->sqlUtilityCollectionCountLastTwoYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' student_collection_count = ' . $this->sqlStudentCollectionCount(). ',' ; } elseif( true == array_key_exists( 'StudentCollectionCount', $this->getChangedColumns() ) ) { $strSql .= ' student_collection_count = ' . $this->sqlStudentCollectionCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' student_collection_count_last_one_year = ' . $this->sqlStudentCollectionCountLastOneYear(). ',' ; } elseif( true == array_key_exists( 'StudentCollectionCountLastOneYear', $this->getChangedColumns() ) ) { $strSql .= ' student_collection_count_last_one_year = ' . $this->sqlStudentCollectionCountLastOneYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' student_collection_count_last_two_year = ' . $this->sqlStudentCollectionCountLastTwoYear(). ',' ; } elseif( true == array_key_exists( 'StudentCollectionCountLastTwoYear', $this->getChangedColumns() ) ) { $strSql .= ' student_collection_count_last_two_year = ' . $this->sqlStudentCollectionCountLastTwoYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inquiry_count_last_ninty_days = ' . $this->sqlInquiryCountLastNintyDays(). ',' ; } elseif( true == array_key_exists( 'InquiryCountLastNintyDays', $this->getChangedColumns() ) ) { $strSql .= ' inquiry_count_last_ninty_days = ' . $this->sqlInquiryCountLastNintyDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inquiry_count_last_one_eighty_days = ' . $this->sqlInquiryCountLastOneEightyDays(). ',' ; } elseif( true == array_key_exists( 'InquiryCountLastOneEightyDays', $this->getChangedColumns() ) ) { $strSql .= ' inquiry_count_last_one_eighty_days = ' . $this->sqlInquiryCountLastOneEightyDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inquiry_count_last_one_year = ' . $this->sqlInquiryCountLastOneYear(). ',' ; } elseif( true == array_key_exists( 'InquiryCountLastOneYear', $this->getChangedColumns() ) ) { $strSql .= ' inquiry_count_last_one_year = ' . $this->sqlInquiryCountLastOneYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inquiry_count_last_two_year = ' . $this->sqlInquiryCountLastTwoYear(). ',' ; } elseif( true == array_key_exists( 'InquiryCountLastTwoYear', $this->getChangedColumns() ) ) { $strSql .= ' inquiry_count_last_two_year = ' . $this->sqlInquiryCountLastTwoYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rv_rent = ' . $this->sqlRvRent(). ',' ; } elseif( true == array_key_exists( 'RvRent', $this->getChangedColumns() ) ) { $strSql .= ' rv_rent = ' . $this->sqlRvRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rv_applicant_income = ' . $this->sqlRvApplicantIncome(). ',' ; } elseif( true == array_key_exists( 'RvApplicantIncome', $this->getChangedColumns() ) ) { $strSql .= ' rv_applicant_income = ' . $this->sqlRvApplicantIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' collection_amount = ' . $this->sqlCollectionAmount(). ',' ; } elseif( true == array_key_exists( 'CollectionAmount', $this->getChangedColumns() ) ) { $strSql .= ' collection_amount = ' . $this->sqlCollectionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rental_collection_amount = ' . $this->sqlRentalCollectionAmount(). ',' ; } elseif( true == array_key_exists( 'RentalCollectionAmount', $this->getChangedColumns() ) ) { $strSql .= ' rental_collection_amount = ' . $this->sqlRentalCollectionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' medical_collection_amount = ' . $this->sqlMedicalCollectionAmount(). ',' ; } elseif( true == array_key_exists( 'MedicalCollectionAmount', $this->getChangedColumns() ) ) { $strSql .= ' medical_collection_amount = ' . $this->sqlMedicalCollectionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_collection_amount = ' . $this->sqlUtilityCollectionAmount(). ',' ; } elseif( true == array_key_exists( 'UtilityCollectionAmount', $this->getChangedColumns() ) ) { $strSql .= ' utility_collection_amount = ' . $this->sqlUtilityCollectionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' student_collection_amount = ' . $this->sqlStudentCollectionAmount(). ',' ; } elseif( true == array_key_exists( 'StudentCollectionAmount', $this->getChangedColumns() ) ) { $strSql .= ' student_collection_amount = ' . $this->sqlStudentCollectionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' debt_to_income = ' . $this->sqlDebtToIncome(). ',' ; } elseif( true == array_key_exists( 'DebtToIncome', $this->getChangedColumns() ) ) { $strSql .= ' debt_to_income = ' . $this->sqlDebtToIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquent_account_percentage = ' . $this->sqlDelinquentAccountPercentage(). ',' ; } elseif( true == array_key_exists( 'DelinquentAccountPercentage', $this->getChangedColumns() ) ) { $strSql .= ' delinquent_account_percentage = ' . $this->sqlDelinquentAccountPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_monthly_debt = ' . $this->sqlTotalMonthlyDebt(). ',' ; } elseif( true == array_key_exists( 'TotalMonthlyDebt', $this->getChangedColumns() ) ) { $strSql .= ' total_monthly_debt = ' . $this->sqlTotalMonthlyDebt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_debt_amount = ' . $this->sqlMonthlyDebtAmount(). ',' ; } elseif( true == array_key_exists( 'MonthlyDebtAmount', $this->getChangedColumns() ) ) { $strSql .= ' monthly_debt_amount = ' . $this->sqlMonthlyDebtAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_score = ' . $this->sqlCreditScore(). ',' ; } elseif( true == array_key_exists( 'CreditScore', $this->getChangedColumns() ) ) { $strSql .= ' credit_score = ' . $this->sqlCreditScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rv_index = ' . $this->sqlRvIndex(). ',' ; } elseif( true == array_key_exists( 'RvIndex', $this->getChangedColumns() ) ) { $strSql .= ' rv_index = ' . $this->sqlRvIndex() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' had_bankruptcy = ' . $this->sqlHadBankruptcy(). ',' ; } elseif( true == array_key_exists( 'HadBankruptcy', $this->getChangedColumns() ) ) { $strSql .= ' had_bankruptcy = ' . $this->sqlHadBankruptcy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' had_foreclosure = ' . $this->sqlHadForeclosure(). ',' ; } elseif( true == array_key_exists( 'HadForeclosure', $this->getChangedColumns() ) ) { $strSql .= ' had_foreclosure = ' . $this->sqlHadForeclosure() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_type_id = ' . $this->sqlScreeningApplicantTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningApplicantTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_type_id = ' . $this->sqlScreeningApplicantTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_tradeline_accounts = ' . $this->sqlTotalTradelineAccounts(). ',' ; } elseif( true == array_key_exists( 'TotalTradelineAccounts', $this->getChangedColumns() ) ) { $strSql .= ' total_tradeline_accounts = ' . $this->sqlTotalTradelineAccounts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_tradelines_count_last_two_years = ' . $this->sqlTotalTradelinesCountLastTwoYears(). ',' ; } elseif( true == array_key_exists( 'TotalTradelinesCountLastTwoYears', $this->getChangedColumns() ) ) { $strSql .= ' total_tradelines_count_last_two_years = ' . $this->sqlTotalTradelinesCountLastTwoYears() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' positive_tradeline_accounts = ' . $this->sqlPositiveTradelineAccounts(). ',' ; } elseif( true == array_key_exists( 'PositiveTradelineAccounts', $this->getChangedColumns() ) ) { $strSql .= ' positive_tradeline_accounts = ' . $this->sqlPositiveTradelineAccounts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' positive_tradelines_count_last_two_years = ' . $this->sqlPositiveTradelinesCountLastTwoYears(). ',' ; } elseif( true == array_key_exists( 'PositiveTradelinesCountLastTwoYears', $this->getChangedColumns() ) ) { $strSql .= ' positive_tradelines_count_last_two_years = ' . $this->sqlPositiveTradelinesCountLastTwoYears() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' negative_tradelines_count = ' . $this->sqlNegativeTradelinesCount(). ',' ; } elseif( true == array_key_exists( 'NegativeTradelinesCount', $this->getChangedColumns() ) ) { $strSql .= ' negative_tradelines_count = ' . $this->sqlNegativeTradelinesCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' negative_tradelines_count_last_two_years = ' . $this->sqlNegativeTradelinesCountLastTwoYears(). ',' ; } elseif( true == array_key_exists( 'NegativeTradelinesCountLastTwoYears', $this->getChangedColumns() ) ) { $strSql .= ' negative_tradelines_count_last_two_years = ' . $this->sqlNegativeTradelinesCountLastTwoYears() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' collection_amount_last_two_years = ' . $this->sqlCollectionAmountLastTwoYears(). ',' ; } elseif( true == array_key_exists( 'CollectionAmountLastTwoYears', $this->getChangedColumns() ) ) { $strSql .= ' collection_amount_last_two_years = ' . $this->sqlCollectionAmountLastTwoYears() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rental_collection_amount_last_two_years = ' . $this->sqlRentalCollectionAmountLastTwoYears(). ',' ; } elseif( true == array_key_exists( 'RentalCollectionAmountLastTwoYears', $this->getChangedColumns() ) ) { $strSql .= ' rental_collection_amount_last_two_years = ' . $this->sqlRentalCollectionAmountLastTwoYears() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' medical_collection_amount_last_two_years = ' . $this->sqlMedicalCollectionAmountLastTwoYears(). ',' ; } elseif( true == array_key_exists( 'MedicalCollectionAmountLastTwoYears', $this->getChangedColumns() ) ) { $strSql .= ' medical_collection_amount_last_two_years = ' . $this->sqlMedicalCollectionAmountLastTwoYears() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_collection_amount_last_two_years = ' . $this->sqlUtilityCollectionAmountLastTwoYears(). ',' ; } elseif( true == array_key_exists( 'UtilityCollectionAmountLastTwoYears', $this->getChangedColumns() ) ) { $strSql .= ' utility_collection_amount_last_two_years = ' . $this->sqlUtilityCollectionAmountLastTwoYears() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' student_collection_amount_last_two_years = ' . $this->sqlStudentCollectionAmountLastTwoYears(). ',' ; } elseif( true == array_key_exists( 'StudentCollectionAmountLastTwoYears', $this->getChangedColumns() ) ) { $strSql .= ' student_collection_amount_last_two_years = ' . $this->sqlStudentCollectionAmountLastTwoYears() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' open_collection_count = ' . $this->sqlOpenCollectionCount(). ',' ; } elseif( true == array_key_exists( 'OpenCollectionCount', $this->getChangedColumns() ) ) { $strSql .= ' open_collection_count = ' . $this->sqlOpenCollectionCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' closed_collection_count = ' . $this->sqlClosedCollectionCount(). ',' ; } elseif( true == array_key_exists( 'ClosedCollectionCount', $this->getChangedColumns() ) ) { $strSql .= ' closed_collection_count = ' . $this->sqlClosedCollectionCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rv_index_recalculated = ' . $this->sqlRvIndexRecalculated(). ',' ; } elseif( true == array_key_exists( 'RvIndexRecalculated', $this->getChangedColumns() ) ) { $strSql .= ' rv_index_recalculated = ' . $this->sqlRvIndexRecalculated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_screened_for_credit = ' . $this->sqlIsScreenedForCredit(). ',' ; } elseif( true == array_key_exists( 'IsScreenedForCredit', $this->getChangedColumns() ) ) { $strSql .= ' is_screened_for_credit = ' . $this->sqlIsScreenedForCredit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screened_on = ' . $this->sqlScreenedOn(). ',' ; } elseif( true == array_key_exists( 'ScreenedOn', $this->getChangedColumns() ) ) { $strSql .= ' screened_on = ' . $this->sqlScreenedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'rv_lease_new_outcomes_id' => $this->getRvLeaseNewOutcomesId(),
			'collection_count' => $this->getCollectionCount(),
			'collection_count_last_one_month' => $this->getCollectionCountLastOneMonth(),
			'collection_count_last_one_year' => $this->getCollectionCountLastOneYear(),
			'collection_count_last_two_year' => $this->getCollectionCountLastTwoYear(),
			'rental_collection_count' => $this->getRentalCollectionCount(),
			'rental_collection_count_last_one_month' => $this->getRentalCollectionCountLastOneMonth(),
			'rental_collection_count_last_one_year' => $this->getRentalCollectionCountLastOneYear(),
			'rental_collection_count_last_two_year' => $this->getRentalCollectionCountLastTwoYear(),
			'medical_collection_count' => $this->getMedicalCollectionCount(),
			'medical_collection_count_last_one_year' => $this->getMedicalCollectionCountLastOneYear(),
			'medical_collection_count_last_two_year' => $this->getMedicalCollectionCountLastTwoYear(),
			'utility_collection_count' => $this->getUtilityCollectionCount(),
			'utility_collection_count_last_one_year' => $this->getUtilityCollectionCountLastOneYear(),
			'utility_collection_count_last_two_year' => $this->getUtilityCollectionCountLastTwoYear(),
			'student_collection_count' => $this->getStudentCollectionCount(),
			'student_collection_count_last_one_year' => $this->getStudentCollectionCountLastOneYear(),
			'student_collection_count_last_two_year' => $this->getStudentCollectionCountLastTwoYear(),
			'inquiry_count_last_ninty_days' => $this->getInquiryCountLastNintyDays(),
			'inquiry_count_last_one_eighty_days' => $this->getInquiryCountLastOneEightyDays(),
			'inquiry_count_last_one_year' => $this->getInquiryCountLastOneYear(),
			'inquiry_count_last_two_year' => $this->getInquiryCountLastTwoYear(),
			'rv_rent' => $this->getRvRent(),
			'rv_applicant_income' => $this->getRvApplicantIncome(),
			'collection_amount' => $this->getCollectionAmount(),
			'rental_collection_amount' => $this->getRentalCollectionAmount(),
			'medical_collection_amount' => $this->getMedicalCollectionAmount(),
			'utility_collection_amount' => $this->getUtilityCollectionAmount(),
			'student_collection_amount' => $this->getStudentCollectionAmount(),
			'debt_to_income' => $this->getDebtToIncome(),
			'delinquent_account_percentage' => $this->getDelinquentAccountPercentage(),
			'total_monthly_debt' => $this->getTotalMonthlyDebt(),
			'monthly_debt_amount' => $this->getMonthlyDebtAmount(),
			'credit_score' => $this->getCreditScore(),
			'rv_index' => $this->getRvIndex(),
			'had_bankruptcy' => $this->getHadBankruptcy(),
			'had_foreclosure' => $this->getHadForeclosure(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'screening_applicant_type_id' => $this->getScreeningApplicantTypeId(),
			'total_tradeline_accounts' => $this->getTotalTradelineAccounts(),
			'total_tradelines_count_last_two_years' => $this->getTotalTradelinesCountLastTwoYears(),
			'positive_tradeline_accounts' => $this->getPositiveTradelineAccounts(),
			'positive_tradelines_count_last_two_years' => $this->getPositiveTradelinesCountLastTwoYears(),
			'negative_tradelines_count' => $this->getNegativeTradelinesCount(),
			'negative_tradelines_count_last_two_years' => $this->getNegativeTradelinesCountLastTwoYears(),
			'collection_amount_last_two_years' => $this->getCollectionAmountLastTwoYears(),
			'rental_collection_amount_last_two_years' => $this->getRentalCollectionAmountLastTwoYears(),
			'medical_collection_amount_last_two_years' => $this->getMedicalCollectionAmountLastTwoYears(),
			'utility_collection_amount_last_two_years' => $this->getUtilityCollectionAmountLastTwoYears(),
			'student_collection_amount_last_two_years' => $this->getStudentCollectionAmountLastTwoYears(),
			'open_collection_count' => $this->getOpenCollectionCount(),
			'closed_collection_count' => $this->getClosedCollectionCount(),
			'rv_index_recalculated' => $this->getRvIndexRecalculated(),
			'is_screened_for_credit' => $this->getIsScreenedForCredit(),
			'screened_on' => $this->getScreenedOn()
		);
	}

}
?>