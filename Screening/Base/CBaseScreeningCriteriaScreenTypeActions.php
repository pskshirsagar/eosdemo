<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCriteriaScreenTypeActions
 * Do not add any new functions to this class.
 */

class CBaseScreeningCriteriaScreenTypeActions extends CEosPluralBase {

	/**
	 * @return CScreeningCriteriaScreenTypeAction[]
	 */
	public static function fetchScreeningCriteriaScreenTypeActions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningCriteriaScreenTypeAction::class, $objDatabase );
	}

	/**
	 * @return CScreeningCriteriaScreenTypeAction
	 */
	public static function fetchScreeningCriteriaScreenTypeAction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningCriteriaScreenTypeAction::class, $objDatabase );
	}

	public static function fetchScreeningCriteriaScreenTypeActionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_criteria_screen_type_actions', $objDatabase );
	}

	public static function fetchScreeningCriteriaScreenTypeActionById( $intId, $objDatabase ) {
		return self::fetchScreeningCriteriaScreenTypeAction( sprintf( 'SELECT * FROM screening_criteria_screen_type_actions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningCriteriaScreenTypeActionsByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {
		return self::fetchScreeningCriteriaScreenTypeActions( sprintf( 'SELECT * FROM screening_criteria_screen_type_actions WHERE screening_criteria_id = %d', ( int ) $intScreeningCriteriaId ), $objDatabase );
	}

	public static function fetchScreeningCriteriaScreenTypeActionsByScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		return self::fetchScreeningCriteriaScreenTypeActions( sprintf( 'SELECT * FROM screening_criteria_screen_type_actions WHERE screening_config_preference_type_id = %d', ( int ) $intScreeningConfigPreferenceTypeId ), $objDatabase );
	}

	public static function fetchScreeningCriteriaScreenTypeActionsByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreeningCriteriaScreenTypeActions( sprintf( 'SELECT * FROM screening_criteria_screen_type_actions WHERE screen_type_id = %d', ( int ) $intScreenTypeId ), $objDatabase );
	}

	public static function fetchScreeningCriteriaScreenTypeActionsByScreeningSearchActionId( $intScreeningSearchActionId, $objDatabase ) {
		return self::fetchScreeningCriteriaScreenTypeActions( sprintf( 'SELECT * FROM screening_criteria_screen_type_actions WHERE screening_search_action_id = %d', ( int ) $intScreeningSearchActionId ), $objDatabase );
	}

}
?>