<?php

class CBaseSubscriptionBillingConsumption extends CEosSingularBase {

	const TABLE_NAME = 'public.subscription_billing_consumptions';

	protected $m_intId;
	protected $m_intSubscriptionBillingContractPropertyId;
	protected $m_intScreeningCompletedCount;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intScreeningCompletedCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['subscription_billing_contract_property_id'] ) && $boolDirectSet ) $this->set( 'm_intSubscriptionBillingContractPropertyId', trim( $arrValues['subscription_billing_contract_property_id'] ) ); elseif( isset( $arrValues['subscription_billing_contract_property_id'] ) ) $this->setSubscriptionBillingContractPropertyId( $arrValues['subscription_billing_contract_property_id'] );
		if( isset( $arrValues['screening_completed_count'] ) && $boolDirectSet ) $this->set( 'm_intScreeningCompletedCount', trim( $arrValues['screening_completed_count'] ) ); elseif( isset( $arrValues['screening_completed_count'] ) ) $this->setScreeningCompletedCount( $arrValues['screening_completed_count'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSubscriptionBillingContractPropertyId( $intSubscriptionBillingContractPropertyId ) {
		$this->set( 'm_intSubscriptionBillingContractPropertyId', CStrings::strToIntDef( $intSubscriptionBillingContractPropertyId, NULL, false ) );
	}

	public function getSubscriptionBillingContractPropertyId() {
		return $this->m_intSubscriptionBillingContractPropertyId;
	}

	public function sqlSubscriptionBillingContractPropertyId() {
		return ( true == isset( $this->m_intSubscriptionBillingContractPropertyId ) ) ? ( string ) $this->m_intSubscriptionBillingContractPropertyId : 'NULL';
	}

	public function setScreeningCompletedCount( $intScreeningCompletedCount ) {
		$this->set( 'm_intScreeningCompletedCount', CStrings::strToIntDef( $intScreeningCompletedCount, NULL, false ) );
	}

	public function getScreeningCompletedCount() {
		return $this->m_intScreeningCompletedCount;
	}

	public function sqlScreeningCompletedCount() {
		return ( true == isset( $this->m_intScreeningCompletedCount ) ) ? ( string ) $this->m_intScreeningCompletedCount : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, subscription_billing_contract_property_id, screening_completed_count, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlSubscriptionBillingContractPropertyId() . ', ' .
						$this->sqlScreeningCompletedCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subscription_billing_contract_property_id = ' . $this->sqlSubscriptionBillingContractPropertyId(). ',' ; } elseif( true == array_key_exists( 'SubscriptionBillingContractPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' subscription_billing_contract_property_id = ' . $this->sqlSubscriptionBillingContractPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_completed_count = ' . $this->sqlScreeningCompletedCount() ; } elseif( true == array_key_exists( 'ScreeningCompletedCount', $this->getChangedColumns() ) ) { $strSql .= ' screening_completed_count = ' . $this->sqlScreeningCompletedCount() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'subscription_billing_contract_property_id' => $this->getSubscriptionBillingContractPropertyId(),
			'screening_completed_count' => $this->getScreeningCompletedCount(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>