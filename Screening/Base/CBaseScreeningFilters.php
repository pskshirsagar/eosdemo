<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningFilters
 * Do not add any new functions to this class.
 */

class CBaseScreeningFilters extends CEosPluralBase {

	/**
	 * @return CScreeningFilter[]
	 */
	public static function fetchScreeningFilters( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningFilter', $objDatabase );
	}

	/**
	 * @return CScreeningFilter
	 */
	public static function fetchScreeningFilter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningFilter', $objDatabase );
	}

	public static function fetchScreeningFilterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_filters', $objDatabase );
	}

	public static function fetchScreeningFilterById( $intId, $objDatabase ) {
		return self::fetchScreeningFilter( sprintf( 'SELECT * FROM screening_filters WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningFiltersByScreeningFilterTypeId( $intScreeningFilterTypeId, $objDatabase ) {
		return self::fetchScreeningFilters( sprintf( 'SELECT * FROM screening_filters WHERE screening_filter_type_id = %d', ( int ) $intScreeningFilterTypeId ), $objDatabase );
	}

}
?>