<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDisputeTypes
 * Do not add any new functions to this class.
 */

class CBaseDisputeTypes extends CEosPluralBase {

	/**
	 * @return CDisputeType[]
	 */
	public static function fetchDisputeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDisputeType', $objDatabase );
	}

	/**
	 * @return CDisputeType
	 */
	public static function fetchDisputeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDisputeType', $objDatabase );
	}

	public static function fetchDisputeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'dispute_types', $objDatabase );
	}

	public static function fetchDisputeTypeById( $intId, $objDatabase ) {
		return self::fetchDisputeType( sprintf( 'SELECT * FROM dispute_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDisputeTypesByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchDisputeTypes( sprintf( 'SELECT * FROM dispute_types WHERE screen_type_id = %d', ( int ) $intScreenTypeId ), $objDatabase );
	}

}
?>