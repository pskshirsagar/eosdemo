<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CCannedResponses
 * Do not add any new functions to this class.
 */

class CBaseCannedResponses extends CEosPluralBase {

	/**
	 * @return CCannedResponse[]
	 */
	public static function fetchCannedResponses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCannedResponse::class, $objDatabase );
	}

	/**
	 * @return CCannedResponse
	 */
	public static function fetchCannedResponse( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCannedResponse::class, $objDatabase );
	}

	public static function fetchCannedResponseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'canned_responses', $objDatabase );
	}

	public static function fetchCannedResponseById( $intId, $objDatabase ) {
		return self::fetchCannedResponse( sprintf( 'SELECT * FROM canned_responses WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCannedResponsesByQuickResponseTypeId( $intQuickResponseTypeId, $objDatabase ) {
		return self::fetchCannedResponses( sprintf( 'SELECT * FROM canned_responses WHERE quick_response_type_id = %d', $intQuickResponseTypeId ), $objDatabase );
	}

}
?>