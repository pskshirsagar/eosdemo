<?php

class CBaseScreeningDataProvider extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_data_providers';

	protected $m_intId;
	protected $m_intScreeningDataProviderTypeId;
	protected $m_intScreenTypeId;
	protected $m_strName;
	protected $m_strVendorCode;
	protected $m_strProductCode;
	protected $m_strUsernameEncrypted;
	protected $m_strPasswordEncrypted;
	protected $m_strLookupUrl;
	protected $m_strRequestUrl;
	protected $m_strResponseUrl;
	protected $m_intIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intScreeningDataProviderProductTypeId;
	protected $m_boolOnMaintenance;
	protected $m_boolIsDefault;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_data_provider_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningDataProviderTypeId', trim( $arrValues['screening_data_provider_type_id'] ) ); elseif( isset( $arrValues['screening_data_provider_type_id'] ) ) $this->setScreeningDataProviderTypeId( $arrValues['screening_data_provider_type_id'] );
		if( isset( $arrValues['screen_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreenTypeId', trim( $arrValues['screen_type_id'] ) ); elseif( isset( $arrValues['screen_type_id'] ) ) $this->setScreenTypeId( $arrValues['screen_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['vendor_code'] ) && $boolDirectSet ) $this->set( 'm_strVendorCode', trim( $arrValues['vendor_code'] ) ); elseif( isset( $arrValues['vendor_code'] ) ) $this->setVendorCode( $arrValues['vendor_code'] );
		if( isset( $arrValues['product_code'] ) && $boolDirectSet ) $this->set( 'm_strProductCode', trim( $arrValues['product_code'] ) ); elseif( isset( $arrValues['product_code'] ) ) $this->setProductCode( $arrValues['product_code'] );
		if( isset( $arrValues['username_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strUsernameEncrypted', trim( $arrValues['username_encrypted'] ) ); elseif( isset( $arrValues['username_encrypted'] ) ) $this->setUsernameEncrypted( $arrValues['username_encrypted'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( $arrValues['password_encrypted'] ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( $arrValues['password_encrypted'] );
		if( isset( $arrValues['lookup_url'] ) && $boolDirectSet ) $this->set( 'm_strLookupUrl', trim( $arrValues['lookup_url'] ) ); elseif( isset( $arrValues['lookup_url'] ) ) $this->setLookupUrl( $arrValues['lookup_url'] );
		if( isset( $arrValues['request_url'] ) && $boolDirectSet ) $this->set( 'm_strRequestUrl', trim( $arrValues['request_url'] ) ); elseif( isset( $arrValues['request_url'] ) ) $this->setRequestUrl( $arrValues['request_url'] );
		if( isset( $arrValues['response_url'] ) && $boolDirectSet ) $this->set( 'm_strResponseUrl', trim( $arrValues['response_url'] ) ); elseif( isset( $arrValues['response_url'] ) ) $this->setResponseUrl( $arrValues['response_url'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['screening_data_provider_product_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningDataProviderProductTypeId', trim( $arrValues['screening_data_provider_product_type_id'] ) ); elseif( isset( $arrValues['screening_data_provider_product_type_id'] ) ) $this->setScreeningDataProviderProductTypeId( $arrValues['screening_data_provider_product_type_id'] );
		if( isset( $arrValues['on_maintenance'] ) && $boolDirectSet ) $this->set( 'm_boolOnMaintenance', trim( stripcslashes( $arrValues['on_maintenance'] ) ) ); elseif( isset( $arrValues['on_maintenance'] ) ) $this->setOnMaintenance( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['on_maintenance'] ) : $arrValues['on_maintenance'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefault', trim( stripcslashes( $arrValues['is_default'] ) ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default'] ) : $arrValues['is_default'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningDataProviderTypeId( $intScreeningDataProviderTypeId ) {
		$this->set( 'm_intScreeningDataProviderTypeId', CStrings::strToIntDef( $intScreeningDataProviderTypeId, NULL, false ) );
	}

	public function getScreeningDataProviderTypeId() {
		return $this->m_intScreeningDataProviderTypeId;
	}

	public function sqlScreeningDataProviderTypeId() {
		return ( true == isset( $this->m_intScreeningDataProviderTypeId ) ) ? ( string ) $this->m_intScreeningDataProviderTypeId : 'NULL';
	}

	public function setScreenTypeId( $intScreenTypeId ) {
		$this->set( 'm_intScreenTypeId', CStrings::strToIntDef( $intScreenTypeId, NULL, false ) );
	}

	public function getScreenTypeId() {
		return $this->m_intScreenTypeId;
	}

	public function sqlScreenTypeId() {
		return ( true == isset( $this->m_intScreenTypeId ) ) ? ( string ) $this->m_intScreenTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, -1, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setVendorCode( $strVendorCode ) {
		$this->set( 'm_strVendorCode', CStrings::strTrimDef( $strVendorCode, -1, NULL, true ) );
	}

	public function getVendorCode() {
		return $this->m_strVendorCode;
	}

	public function sqlVendorCode() {
		return ( true == isset( $this->m_strVendorCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strVendorCode ) : '\'' . addslashes( $this->m_strVendorCode ) . '\'' ) : 'NULL';
	}

	public function setProductCode( $strProductCode ) {
		$this->set( 'm_strProductCode', CStrings::strTrimDef( $strProductCode, -1, NULL, true ) );
	}

	public function getProductCode() {
		return $this->m_strProductCode;
	}

	public function sqlProductCode() {
		return ( true == isset( $this->m_strProductCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strProductCode ) : '\'' . addslashes( $this->m_strProductCode ) . '\'' ) : 'NULL';
	}

	public function setUsernameEncrypted( $strUsernameEncrypted ) {
		$this->set( 'm_strUsernameEncrypted', CStrings::strTrimDef( $strUsernameEncrypted, -1, NULL, true ) );
	}

	public function getUsernameEncrypted() {
		return $this->m_strUsernameEncrypted;
	}

	public function sqlUsernameEncrypted() {
		return ( true == isset( $this->m_strUsernameEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUsernameEncrypted ) : '\'' . addslashes( $this->m_strUsernameEncrypted ) . '\'' ) : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, -1, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPasswordEncrypted ) : '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' ) : 'NULL';
	}

	public function setLookupUrl( $strLookupUrl ) {
		$this->set( 'm_strLookupUrl', CStrings::strTrimDef( $strLookupUrl, -1, NULL, true ) );
	}

	public function getLookupUrl() {
		return $this->m_strLookupUrl;
	}

	public function sqlLookupUrl() {
		return ( true == isset( $this->m_strLookupUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLookupUrl ) : '\'' . addslashes( $this->m_strLookupUrl ) . '\'' ) : 'NULL';
	}

	public function setRequestUrl( $strRequestUrl ) {
		$this->set( 'm_strRequestUrl', CStrings::strTrimDef( $strRequestUrl, -1, NULL, true ) );
	}

	public function getRequestUrl() {
		return $this->m_strRequestUrl;
	}

	public function sqlRequestUrl() {
		return ( true == isset( $this->m_strRequestUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRequestUrl ) : '\'' . addslashes( $this->m_strRequestUrl ) . '\'' ) : 'NULL';
	}

	public function setResponseUrl( $strResponseUrl ) {
		$this->set( 'm_strResponseUrl', CStrings::strTrimDef( $strResponseUrl, -1, NULL, true ) );
	}

	public function getResponseUrl() {
		return $this->m_strResponseUrl;
	}

	public function sqlResponseUrl() {
		return ( true == isset( $this->m_strResponseUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResponseUrl ) : '\'' . addslashes( $this->m_strResponseUrl ) . '\'' ) : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setScreeningDataProviderProductTypeId( $intScreeningDataProviderProductTypeId ) {
		$this->set( 'm_intScreeningDataProviderProductTypeId', CStrings::strToIntDef( $intScreeningDataProviderProductTypeId, NULL, false ) );
	}

	public function getScreeningDataProviderProductTypeId() {
		return $this->m_intScreeningDataProviderProductTypeId;
	}

	public function sqlScreeningDataProviderProductTypeId() {
		return ( true == isset( $this->m_intScreeningDataProviderProductTypeId ) ) ? ( string ) $this->m_intScreeningDataProviderProductTypeId : 'NULL';
	}

	public function setOnMaintenance( $boolOnMaintenance ) {
		$this->set( 'm_boolOnMaintenance', CStrings::strToBool( $boolOnMaintenance ) );
	}

	public function getOnMaintenance() {
		return $this->m_boolOnMaintenance;
	}

	public function sqlOnMaintenance() {
		return ( true == isset( $this->m_boolOnMaintenance ) ) ? '\'' . ( true == ( bool ) $this->m_boolOnMaintenance ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->set( 'm_boolIsDefault', CStrings::strToBool( $boolIsDefault ) );
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_boolIsDefault ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefault ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, screening_data_provider_type_id, screen_type_id, name, vendor_code, product_code, username_encrypted, password_encrypted, lookup_url, request_url, response_url, is_published, updated_by, updated_on, created_by, created_on, screening_data_provider_product_type_id, on_maintenance, is_default )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlScreeningDataProviderTypeId() . ', ' .
						$this->sqlScreenTypeId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlVendorCode() . ', ' .
						$this->sqlProductCode() . ', ' .
						$this->sqlUsernameEncrypted() . ', ' .
						$this->sqlPasswordEncrypted() . ', ' .
						$this->sqlLookupUrl() . ', ' .
						$this->sqlRequestUrl() . ', ' .
						$this->sqlResponseUrl() . ', ' .
						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlScreeningDataProviderProductTypeId() . ', ' .
						$this->sqlOnMaintenance() . ', ' .
						$this->sqlIsDefault() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_data_provider_type_id = ' . $this->sqlScreeningDataProviderTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningDataProviderTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_data_provider_type_id = ' . $this->sqlScreeningDataProviderTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreenTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_code = ' . $this->sqlVendorCode(). ',' ; } elseif( true == array_key_exists( 'VendorCode', $this->getChangedColumns() ) ) { $strSql .= ' vendor_code = ' . $this->sqlVendorCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' product_code = ' . $this->sqlProductCode(). ',' ; } elseif( true == array_key_exists( 'ProductCode', $this->getChangedColumns() ) ) { $strSql .= ' product_code = ' . $this->sqlProductCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username_encrypted = ' . $this->sqlUsernameEncrypted(). ',' ; } elseif( true == array_key_exists( 'UsernameEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' username_encrypted = ' . $this->sqlUsernameEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted(). ',' ; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lookup_url = ' . $this->sqlLookupUrl(). ',' ; } elseif( true == array_key_exists( 'LookupUrl', $this->getChangedColumns() ) ) { $strSql .= ' lookup_url = ' . $this->sqlLookupUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_url = ' . $this->sqlRequestUrl(). ',' ; } elseif( true == array_key_exists( 'RequestUrl', $this->getChangedColumns() ) ) { $strSql .= ' request_url = ' . $this->sqlRequestUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_url = ' . $this->sqlResponseUrl(). ',' ; } elseif( true == array_key_exists( 'ResponseUrl', $this->getChangedColumns() ) ) { $strSql .= ' response_url = ' . $this->sqlResponseUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_data_provider_product_type_id = ' . $this->sqlScreeningDataProviderProductTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningDataProviderProductTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_data_provider_product_type_id = ' . $this->sqlScreeningDataProviderProductTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' on_maintenance = ' . $this->sqlOnMaintenance(). ',' ; } elseif( true == array_key_exists( 'OnMaintenance', $this->getChangedColumns() ) ) { $strSql .= ' on_maintenance = ' . $this->sqlOnMaintenance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault(). ',' ; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_data_provider_type_id' => $this->getScreeningDataProviderTypeId(),
			'screen_type_id' => $this->getScreenTypeId(),
			'name' => $this->getName(),
			'vendor_code' => $this->getVendorCode(),
			'product_code' => $this->getProductCode(),
			'username_encrypted' => $this->getUsernameEncrypted(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'lookup_url' => $this->getLookupUrl(),
			'request_url' => $this->getRequestUrl(),
			'response_url' => $this->getResponseUrl(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'screening_data_provider_product_type_id' => $this->getScreeningDataProviderProductTypeId(),
			'on_maintenance' => $this->getOnMaintenance(),
			'is_default' => $this->getIsDefault()
		);
	}

}
?>