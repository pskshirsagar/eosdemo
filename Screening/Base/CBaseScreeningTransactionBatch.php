<?php

class CBaseScreeningTransactionBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_transaction_batches';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intTransactionId;
	protected $m_intScreeningPropertyAccountId;
	protected $m_intPropertyId;
	protected $m_strBatchDate;
	protected $m_fltBatchTotal;
	protected $m_intBatchCount;
	protected $m_strBatchPostDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsTestBatch;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsTestBatch = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionId', trim( $arrValues['transaction_id'] ) ); elseif( isset( $arrValues['transaction_id'] ) ) $this->setTransactionId( $arrValues['transaction_id'] );
		if( isset( $arrValues['screening_property_account_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPropertyAccountId', trim( $arrValues['screening_property_account_id'] ) ); elseif( isset( $arrValues['screening_property_account_id'] ) ) $this->setScreeningPropertyAccountId( $arrValues['screening_property_account_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['batch_date'] ) && $boolDirectSet ) $this->set( 'm_strBatchDate', trim( $arrValues['batch_date'] ) ); elseif( isset( $arrValues['batch_date'] ) ) $this->setBatchDate( $arrValues['batch_date'] );
		if( isset( $arrValues['batch_total'] ) && $boolDirectSet ) $this->set( 'm_fltBatchTotal', trim( $arrValues['batch_total'] ) ); elseif( isset( $arrValues['batch_total'] ) ) $this->setBatchTotal( $arrValues['batch_total'] );
		if( isset( $arrValues['batch_count'] ) && $boolDirectSet ) $this->set( 'm_intBatchCount', trim( $arrValues['batch_count'] ) ); elseif( isset( $arrValues['batch_count'] ) ) $this->setBatchCount( $arrValues['batch_count'] );
		if( isset( $arrValues['batch_post_date'] ) && $boolDirectSet ) $this->set( 'm_strBatchPostDate', trim( $arrValues['batch_post_date'] ) ); elseif( isset( $arrValues['batch_post_date'] ) ) $this->setBatchPostDate( $arrValues['batch_post_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_test_batch'] ) && $boolDirectSet ) $this->set( 'm_boolIsTestBatch', trim( stripcslashes( $arrValues['is_test_batch'] ) ) ); elseif( isset( $arrValues['is_test_batch'] ) ) $this->setIsTestBatch( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_test_batch'] ) : $arrValues['is_test_batch'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setTransactionId( $intTransactionId ) {
		$this->set( 'm_intTransactionId', CStrings::strToIntDef( $intTransactionId, NULL, false ) );
	}

	public function getTransactionId() {
		return $this->m_intTransactionId;
	}

	public function sqlTransactionId() {
		return ( true == isset( $this->m_intTransactionId ) ) ? ( string ) $this->m_intTransactionId : 'NULL';
	}

	public function setScreeningPropertyAccountId( $intScreeningPropertyAccountId ) {
		$this->set( 'm_intScreeningPropertyAccountId', CStrings::strToIntDef( $intScreeningPropertyAccountId, NULL, false ) );
	}

	public function getScreeningPropertyAccountId() {
		return $this->m_intScreeningPropertyAccountId;
	}

	public function sqlScreeningPropertyAccountId() {
		return ( true == isset( $this->m_intScreeningPropertyAccountId ) ) ? ( string ) $this->m_intScreeningPropertyAccountId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setBatchDate( $strBatchDate ) {
		$this->set( 'm_strBatchDate', CStrings::strTrimDef( $strBatchDate, -1, NULL, true ) );
	}

	public function getBatchDate() {
		return $this->m_strBatchDate;
	}

	public function sqlBatchDate() {
		return ( true == isset( $this->m_strBatchDate ) ) ? '\'' . $this->m_strBatchDate . '\'' : 'NOW()';
	}

	public function setBatchTotal( $fltBatchTotal ) {
		$this->set( 'm_fltBatchTotal', CStrings::strToFloatDef( $fltBatchTotal, NULL, false, 2 ) );
	}

	public function getBatchTotal() {
		return $this->m_fltBatchTotal;
	}

	public function sqlBatchTotal() {
		return ( true == isset( $this->m_fltBatchTotal ) ) ? ( string ) $this->m_fltBatchTotal : 'NULL';
	}

	public function setBatchCount( $intBatchCount ) {
		$this->set( 'm_intBatchCount', CStrings::strToIntDef( $intBatchCount, NULL, false ) );
	}

	public function getBatchCount() {
		return $this->m_intBatchCount;
	}

	public function sqlBatchCount() {
		return ( true == isset( $this->m_intBatchCount ) ) ? ( string ) $this->m_intBatchCount : 'NULL';
	}

	public function setBatchPostDate( $strBatchPostDate ) {
		$this->set( 'm_strBatchPostDate', CStrings::strTrimDef( $strBatchPostDate, -1, NULL, true ) );
	}

	public function getBatchPostDate() {
		return $this->m_strBatchPostDate;
	}

	public function sqlBatchPostDate() {
		return ( true == isset( $this->m_strBatchPostDate ) ) ? '\'' . $this->m_strBatchPostDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsTestBatch( $boolIsTestBatch ) {
		$this->set( 'm_boolIsTestBatch', CStrings::strToBool( $boolIsTestBatch ) );
	}

	public function getIsTestBatch() {
		return $this->m_boolIsTestBatch;
	}

	public function sqlIsTestBatch() {
		return ( true == isset( $this->m_boolIsTestBatch ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTestBatch ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, transaction_id, screening_property_account_id, property_id, batch_date, batch_total, batch_count, batch_post_date, updated_by, updated_on, created_by, created_on, is_test_batch )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlTransactionId() . ', ' .
						$this->sqlScreeningPropertyAccountId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlBatchDate() . ', ' .
						$this->sqlBatchTotal() . ', ' .
						$this->sqlBatchCount() . ', ' .
						$this->sqlBatchPostDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsTestBatch() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId(). ',' ; } elseif( true == array_key_exists( 'TransactionId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_property_account_id = ' . $this->sqlScreeningPropertyAccountId(). ',' ; } elseif( true == array_key_exists( 'ScreeningPropertyAccountId', $this->getChangedColumns() ) ) { $strSql .= ' screening_property_account_id = ' . $this->sqlScreeningPropertyAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_date = ' . $this->sqlBatchDate(). ',' ; } elseif( true == array_key_exists( 'BatchDate', $this->getChangedColumns() ) ) { $strSql .= ' batch_date = ' . $this->sqlBatchDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_total = ' . $this->sqlBatchTotal(). ',' ; } elseif( true == array_key_exists( 'BatchTotal', $this->getChangedColumns() ) ) { $strSql .= ' batch_total = ' . $this->sqlBatchTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_count = ' . $this->sqlBatchCount(). ',' ; } elseif( true == array_key_exists( 'BatchCount', $this->getChangedColumns() ) ) { $strSql .= ' batch_count = ' . $this->sqlBatchCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_post_date = ' . $this->sqlBatchPostDate(). ',' ; } elseif( true == array_key_exists( 'BatchPostDate', $this->getChangedColumns() ) ) { $strSql .= ' batch_post_date = ' . $this->sqlBatchPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_test_batch = ' . $this->sqlIsTestBatch(). ',' ; } elseif( true == array_key_exists( 'IsTestBatch', $this->getChangedColumns() ) ) { $strSql .= ' is_test_batch = ' . $this->sqlIsTestBatch() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'transaction_id' => $this->getTransactionId(),
			'screening_property_account_id' => $this->getScreeningPropertyAccountId(),
			'property_id' => $this->getPropertyId(),
			'batch_date' => $this->getBatchDate(),
			'batch_total' => $this->getBatchTotal(),
			'batch_count' => $this->getBatchCount(),
			'batch_post_date' => $this->getBatchPostDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_test_batch' => $this->getIsTestBatch()
		);
	}

}
?>