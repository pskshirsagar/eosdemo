<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningJsonDataTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningJsonDataTypes extends CEosPluralBase {

	/**
	 * @return CScreeningJsonDataType[]
	 */
	public static function fetchScreeningJsonDataTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningJsonDataType::class, $objDatabase );
	}

	/**
	 * @return CScreeningJsonDataType
	 */
	public static function fetchScreeningJsonDataType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningJsonDataType::class, $objDatabase );
	}

	public static function fetchScreeningJsonDataTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_json_data_types', $objDatabase );
	}

	public static function fetchScreeningJsonDataTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningJsonDataType( sprintf( 'SELECT * FROM screening_json_data_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>