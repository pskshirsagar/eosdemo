<?php

class CBaseConsumer extends CEosSingularBase {

	const TABLE_NAME = 'public.consumers';

	protected $m_intId;
	protected $m_intScreeningId;
	protected $m_intScreeningApplicantId;
	protected $m_intContactMethodTypeId;
	protected $m_strFirstName;
	protected $m_strLastName;
	protected $m_strReferenceNumber;
	protected $m_strLastFourSsn;
	protected $m_strDateOfBirthEncrypted;
	protected $m_strEmailAddress;
	protected $m_strPhoneNumber;
	protected $m_strAddressLine;
	protected $m_strCity;
	protected $m_strState;
	protected $m_strZipcode;
	protected $m_strPropertyName;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningId', trim( $arrValues['screening_id'] ) ); elseif( isset( $arrValues['screening_id'] ) ) $this->setScreeningId( $arrValues['screening_id'] );
		if( isset( $arrValues['screening_applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicantId', trim( $arrValues['screening_applicant_id'] ) ); elseif( isset( $arrValues['screening_applicant_id'] ) ) $this->setScreeningApplicantId( $arrValues['screening_applicant_id'] );
		if( isset( $arrValues['contact_method_type_id'] ) && $boolDirectSet ) $this->set( 'm_intContactMethodTypeId', trim( $arrValues['contact_method_type_id'] ) ); elseif( isset( $arrValues['contact_method_type_id'] ) ) $this->setContactMethodTypeId( $arrValues['contact_method_type_id'] );
		if( isset( $arrValues['first_name'] ) && $boolDirectSet ) $this->set( 'm_strFirstName', trim( stripcslashes( $arrValues['first_name'] ) ) ); elseif( isset( $arrValues['first_name'] ) ) $this->setFirstName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['first_name'] ) : $arrValues['first_name'] );
		if( isset( $arrValues['last_name'] ) && $boolDirectSet ) $this->set( 'm_strLastName', trim( stripcslashes( $arrValues['last_name'] ) ) ); elseif( isset( $arrValues['last_name'] ) ) $this->setLastName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['last_name'] ) : $arrValues['last_name'] );
		if( isset( $arrValues['reference_number'] ) && $boolDirectSet ) $this->set( 'm_strReferenceNumber', trim( stripcslashes( $arrValues['reference_number'] ) ) ); elseif( isset( $arrValues['reference_number'] ) ) $this->setReferenceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reference_number'] ) : $arrValues['reference_number'] );
		if( isset( $arrValues['last_four_ssn'] ) && $boolDirectSet ) $this->set( 'm_strLastFourSsn', trim( stripcslashes( $arrValues['last_four_ssn'] ) ) ); elseif( isset( $arrValues['last_four_ssn'] ) ) $this->setLastFourSsn( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['last_four_ssn'] ) : $arrValues['last_four_ssn'] );
		if( isset( $arrValues['date_of_birth_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strDateOfBirthEncrypted', trim( stripcslashes( $arrValues['date_of_birth_encrypted'] ) ) ); elseif( isset( $arrValues['date_of_birth_encrypted'] ) ) $this->setDateOfBirthEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['date_of_birth_encrypted'] ) : $arrValues['date_of_birth_encrypted'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['address_line'] ) && $boolDirectSet ) $this->set( 'm_strAddressLine', trim( stripcslashes( $arrValues['address_line'] ) ) ); elseif( isset( $arrValues['address_line'] ) ) $this->setAddressLine( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['address_line'] ) : $arrValues['address_line'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state'] ) && $boolDirectSet ) $this->set( 'm_strState', trim( stripcslashes( $arrValues['state'] ) ) ); elseif( isset( $arrValues['state'] ) ) $this->setState( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state'] ) : $arrValues['state'] );
		if( isset( $arrValues['zipcode'] ) && $boolDirectSet ) $this->set( 'm_strZipcode', trim( stripcslashes( $arrValues['zipcode'] ) ) ); elseif( isset( $arrValues['zipcode'] ) ) $this->setZipcode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['zipcode'] ) : $arrValues['zipcode'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( stripcslashes( $arrValues['property_name'] ) ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_name'] ) : $arrValues['property_name'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningId( $intScreeningId ) {
		$this->set( 'm_intScreeningId', CStrings::strToIntDef( $intScreeningId, NULL, false ) );
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function sqlScreeningId() {
		return ( true == isset( $this->m_intScreeningId ) ) ? ( string ) $this->m_intScreeningId : 'NULL';
	}

	public function setScreeningApplicantId( $intScreeningApplicantId ) {
		$this->set( 'm_intScreeningApplicantId', CStrings::strToIntDef( $intScreeningApplicantId, NULL, false ) );
	}

	public function getScreeningApplicantId() {
		return $this->m_intScreeningApplicantId;
	}

	public function sqlScreeningApplicantId() {
		return ( true == isset( $this->m_intScreeningApplicantId ) ) ? ( string ) $this->m_intScreeningApplicantId : 'NULL';
	}

	public function setContactMethodTypeId( $intContactMethodTypeId ) {
		$this->set( 'm_intContactMethodTypeId', CStrings::strToIntDef( $intContactMethodTypeId, NULL, false ) );
	}

	public function getContactMethodTypeId() {
		return $this->m_intContactMethodTypeId;
	}

	public function sqlContactMethodTypeId() {
		return ( true == isset( $this->m_intContactMethodTypeId ) ) ? ( string ) $this->m_intContactMethodTypeId : 'NULL';
	}

	public function setFirstName( $strFirstName ) {
		$this->set( 'm_strFirstName', CStrings::strTrimDef( $strFirstName, 50, NULL, true ) );
	}

	public function getFirstName() {
		return $this->m_strFirstName;
	}

	public function sqlFirstName() {
		return ( true == isset( $this->m_strFirstName ) ) ? '\'' . addslashes( $this->m_strFirstName ) . '\'' : 'NULL';
	}

	public function setLastName( $strLastName ) {
		$this->set( 'm_strLastName', CStrings::strTrimDef( $strLastName, 50, NULL, true ) );
	}

	public function getLastName() {
		return $this->m_strLastName;
	}

	public function sqlLastName() {
		return ( true == isset( $this->m_strLastName ) ) ? '\'' . addslashes( $this->m_strLastName ) . '\'' : 'NULL';
	}

	public function setReferenceNumber( $strReferenceNumber ) {
		$this->set( 'm_strReferenceNumber', CStrings::strTrimDef( $strReferenceNumber, 240, NULL, true ) );
	}

	public function getReferenceNumber() {
		return $this->m_strReferenceNumber;
	}

	public function sqlReferenceNumber() {
		return ( true == isset( $this->m_strReferenceNumber ) ) ? '\'' . addslashes( $this->m_strReferenceNumber ) . '\'' : 'NULL';
	}

	public function setLastFourSsn( $strLastFourSsn ) {
		$this->set( 'm_strLastFourSsn', CStrings::strTrimDef( $strLastFourSsn, 10, NULL, true ) );
	}

	public function getLastFourSsn() {
		return $this->m_strLastFourSsn;
	}

	public function sqlLastFourSsn() {
		return ( true == isset( $this->m_strLastFourSsn ) ) ? '\'' . addslashes( $this->m_strLastFourSsn ) . '\'' : 'NULL';
	}

	public function setDateOfBirthEncrypted( $strDateOfBirthEncrypted ) {
		$this->set( 'm_strDateOfBirthEncrypted', CStrings::strTrimDef( $strDateOfBirthEncrypted, 240, NULL, true ) );
	}

	public function getDateOfBirthEncrypted() {
		return $this->m_strDateOfBirthEncrypted;
	}

	public function sqlDateOfBirthEncrypted() {
		return ( true == isset( $this->m_strDateOfBirthEncrypted ) ) ? '\'' . addslashes( $this->m_strDateOfBirthEncrypted ) . '\'' : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setAddressLine( $strAddressLine ) {
		$this->set( 'm_strAddressLine', CStrings::strTrimDef( $strAddressLine, 100, NULL, true ) );
	}

	public function getAddressLine() {
		return $this->m_strAddressLine;
	}

	public function sqlAddressLine() {
		return ( true == isset( $this->m_strAddressLine ) ) ? '\'' . addslashes( $this->m_strAddressLine ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setState( $strState ) {
		$this->set( 'm_strState', CStrings::strTrimDef( $strState, 50, NULL, true ) );
	}

	public function getState() {
		return $this->m_strState;
	}

	public function sqlState() {
		return ( true == isset( $this->m_strState ) ) ? '\'' . addslashes( $this->m_strState ) . '\'' : 'NULL';
	}

	public function setZipcode( $strZipcode ) {
		$this->set( 'm_strZipcode', CStrings::strTrimDef( $strZipcode, 50, NULL, true ) );
	}

	public function getZipcode() {
		return $this->m_strZipcode;
	}

	public function sqlZipcode() {
		return ( true == isset( $this->m_strZipcode ) ) ? '\'' . addslashes( $this->m_strZipcode ) . '\'' : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 240, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? '\'' . addslashes( $this->m_strPropertyName ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, screening_id, screening_applicant_id, contact_method_type_id, first_name, last_name, reference_number, last_four_ssn, date_of_birth_encrypted, email_address, phone_number, address_line, city, state, zipcode, property_name, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlScreeningId() . ', ' .
 						$this->sqlScreeningApplicantId() . ', ' .
 						$this->sqlContactMethodTypeId() . ', ' .
 						$this->sqlFirstName() . ', ' .
 						$this->sqlLastName() . ', ' .
 						$this->sqlReferenceNumber() . ', ' .
 						$this->sqlLastFourSsn() . ', ' .
 						$this->sqlDateOfBirthEncrypted() . ', ' .
 						$this->sqlEmailAddress() . ', ' .
 						$this->sqlPhoneNumber() . ', ' .
 						$this->sqlAddressLine() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlState() . ', ' .
 						$this->sqlZipcode() . ', ' .
 						$this->sqlPropertyName() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; } elseif( true == array_key_exists( 'ScreeningId', $this->getChangedColumns() ) ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId() . ','; } elseif( true == array_key_exists( 'ScreeningApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_method_type_id = ' . $this->sqlContactMethodTypeId() . ','; } elseif( true == array_key_exists( 'ContactMethodTypeId', $this->getChangedColumns() ) ) { $strSql .= ' contact_method_type_id = ' . $this->sqlContactMethodTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_name = ' . $this->sqlFirstName() . ','; } elseif( true == array_key_exists( 'FirstName', $this->getChangedColumns() ) ) { $strSql .= ' first_name = ' . $this->sqlFirstName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_name = ' . $this->sqlLastName() . ','; } elseif( true == array_key_exists( 'LastName', $this->getChangedColumns() ) ) { $strSql .= ' last_name = ' . $this->sqlLastName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; } elseif( true == array_key_exists( 'ReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_four_ssn = ' . $this->sqlLastFourSsn() . ','; } elseif( true == array_key_exists( 'LastFourSsn', $this->getChangedColumns() ) ) { $strSql .= ' last_four_ssn = ' . $this->sqlLastFourSsn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_of_birth_encrypted = ' . $this->sqlDateOfBirthEncrypted() . ','; } elseif( true == array_key_exists( 'DateOfBirthEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' date_of_birth_encrypted = ' . $this->sqlDateOfBirthEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address_line = ' . $this->sqlAddressLine() . ','; } elseif( true == array_key_exists( 'AddressLine', $this->getChangedColumns() ) ) { $strSql .= ' address_line = ' . $this->sqlAddressLine() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state = ' . $this->sqlState() . ','; } elseif( true == array_key_exists( 'State', $this->getChangedColumns() ) ) { $strSql .= ' state = ' . $this->sqlState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' zipcode = ' . $this->sqlZipcode() . ','; } elseif( true == array_key_exists( 'Zipcode', $this->getChangedColumns() ) ) { $strSql .= ' zipcode = ' . $this->sqlZipcode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_id' => $this->getScreeningId(),
			'screening_applicant_id' => $this->getScreeningApplicantId(),
			'contact_method_type_id' => $this->getContactMethodTypeId(),
			'first_name' => $this->getFirstName(),
			'last_name' => $this->getLastName(),
			'reference_number' => $this->getReferenceNumber(),
			'last_four_ssn' => $this->getLastFourSsn(),
			'date_of_birth_encrypted' => $this->getDateOfBirthEncrypted(),
			'email_address' => $this->getEmailAddress(),
			'phone_number' => $this->getPhoneNumber(),
			'address_line' => $this->getAddressLine(),
			'city' => $this->getCity(),
			'state' => $this->getState(),
			'zipcode' => $this->getZipcode(),
			'property_name' => $this->getPropertyName(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>