<?php

class CBaseScreeningConfiguration extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_configurations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_strName;
	protected $m_intScreeningCumulationTypeId;
	protected $m_intScreeningScoringModelId;
	protected $m_intScreeningConfigTypeId;
	protected $m_intIsManualCriminalEnabled;
	protected $m_intIsDefault;
	protected $m_intIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsManualCriminalEnabled = '0';
		$this->m_intIsDefault = '0';
		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['screening_cumulation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningCumulationTypeId', trim( $arrValues['screening_cumulation_type_id'] ) ); elseif( isset( $arrValues['screening_cumulation_type_id'] ) ) $this->setScreeningCumulationTypeId( $arrValues['screening_cumulation_type_id'] );
		if( isset( $arrValues['screening_scoring_model_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningScoringModelId', trim( $arrValues['screening_scoring_model_id'] ) ); elseif( isset( $arrValues['screening_scoring_model_id'] ) ) $this->setScreeningScoringModelId( $arrValues['screening_scoring_model_id'] );
		if( isset( $arrValues['screening_config_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningConfigTypeId', trim( $arrValues['screening_config_type_id'] ) ); elseif( isset( $arrValues['screening_config_type_id'] ) ) $this->setScreeningConfigTypeId( $arrValues['screening_config_type_id'] );
		if( isset( $arrValues['is_manual_criminal_enabled'] ) && $boolDirectSet ) $this->set( 'm_intIsManualCriminalEnabled', trim( $arrValues['is_manual_criminal_enabled'] ) ); elseif( isset( $arrValues['is_manual_criminal_enabled'] ) ) $this->setIsManualCriminalEnabled( $arrValues['is_manual_criminal_enabled'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_intIsDefault', trim( $arrValues['is_default'] ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( $arrValues['is_default'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setScreeningCumulationTypeId( $intScreeningCumulationTypeId ) {
		$this->set( 'm_intScreeningCumulationTypeId', CStrings::strToIntDef( $intScreeningCumulationTypeId, NULL, false ) );
	}

	public function getScreeningCumulationTypeId() {
		return $this->m_intScreeningCumulationTypeId;
	}

	public function sqlScreeningCumulationTypeId() {
		return ( true == isset( $this->m_intScreeningCumulationTypeId ) ) ? ( string ) $this->m_intScreeningCumulationTypeId : 'NULL';
	}

	public function setScreeningScoringModelId( $intScreeningScoringModelId ) {
		$this->set( 'm_intScreeningScoringModelId', CStrings::strToIntDef( $intScreeningScoringModelId, NULL, false ) );
	}

	public function getScreeningScoringModelId() {
		return $this->m_intScreeningScoringModelId;
	}

	public function sqlScreeningScoringModelId() {
		return ( true == isset( $this->m_intScreeningScoringModelId ) ) ? ( string ) $this->m_intScreeningScoringModelId : 'NULL';
	}

	public function setScreeningConfigTypeId( $intScreeningConfigTypeId ) {
		$this->set( 'm_intScreeningConfigTypeId', CStrings::strToIntDef( $intScreeningConfigTypeId, NULL, false ) );
	}

	public function getScreeningConfigTypeId() {
		return $this->m_intScreeningConfigTypeId;
	}

	public function sqlScreeningConfigTypeId() {
		return ( true == isset( $this->m_intScreeningConfigTypeId ) ) ? ( string ) $this->m_intScreeningConfigTypeId : 'NULL';
	}

	public function setIsManualCriminalEnabled( $intIsManualCriminalEnabled ) {
		$this->set( 'm_intIsManualCriminalEnabled', CStrings::strToIntDef( $intIsManualCriminalEnabled, NULL, false ) );
	}

	public function getIsManualCriminalEnabled() {
		return $this->m_intIsManualCriminalEnabled;
	}

	public function sqlIsManualCriminalEnabled() {
		return ( true == isset( $this->m_intIsManualCriminalEnabled ) ) ? ( string ) $this->m_intIsManualCriminalEnabled : '0';
	}

	public function setIsDefault( $intIsDefault ) {
		$this->set( 'm_intIsDefault', CStrings::strToIntDef( $intIsDefault, NULL, false ) );
	}

	public function getIsDefault() {
		return $this->m_intIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_intIsDefault ) ) ? ( string ) $this->m_intIsDefault : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, name, screening_cumulation_type_id, screening_scoring_model_id, screening_config_type_id, is_manual_criminal_enabled, is_default, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlScreeningCumulationTypeId() . ', ' .
 						$this->sqlScreeningScoringModelId() . ', ' .
 						$this->sqlScreeningConfigTypeId() . ', ' .
 						$this->sqlIsManualCriminalEnabled() . ', ' .
 						$this->sqlIsDefault() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_cumulation_type_id = ' . $this->sqlScreeningCumulationTypeId() . ','; } elseif( true == array_key_exists( 'ScreeningCumulationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_cumulation_type_id = ' . $this->sqlScreeningCumulationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_scoring_model_id = ' . $this->sqlScreeningScoringModelId() . ','; } elseif( true == array_key_exists( 'ScreeningScoringModelId', $this->getChangedColumns() ) ) { $strSql .= ' screening_scoring_model_id = ' . $this->sqlScreeningScoringModelId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_config_type_id = ' . $this->sqlScreeningConfigTypeId() . ','; } elseif( true == array_key_exists( 'ScreeningConfigTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_config_type_id = ' . $this->sqlScreeningConfigTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manual_criminal_enabled = ' . $this->sqlIsManualCriminalEnabled() . ','; } elseif( true == array_key_exists( 'IsManualCriminalEnabled', $this->getChangedColumns() ) ) { $strSql .= ' is_manual_criminal_enabled = ' . $this->sqlIsManualCriminalEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'name' => $this->getName(),
			'screening_cumulation_type_id' => $this->getScreeningCumulationTypeId(),
			'screening_scoring_model_id' => $this->getScreeningScoringModelId(),
			'screening_config_type_id' => $this->getScreeningConfigTypeId(),
			'is_manual_criminal_enabled' => $this->getIsManualCriminalEnabled(),
			'is_default' => $this->getIsDefault(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>