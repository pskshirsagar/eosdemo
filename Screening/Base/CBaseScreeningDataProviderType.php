<?php

class CBaseScreeningDataProviderType extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_data_provider_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strTechnicalContactName;
	protected $m_strTechnicalContactEmail;
	protected $m_intIsPublished;
	protected $m_intIsTestType;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_intIsTestType = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['technical_contact_name'] ) && $boolDirectSet ) $this->set( 'm_strTechnicalContactName', trim( stripcslashes( $arrValues['technical_contact_name'] ) ) ); elseif( isset( $arrValues['technical_contact_name'] ) ) $this->setTechnicalContactName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['technical_contact_name'] ) : $arrValues['technical_contact_name'] );
		if( isset( $arrValues['technical_contact_email'] ) && $boolDirectSet ) $this->set( 'm_strTechnicalContactEmail', trim( stripcslashes( $arrValues['technical_contact_email'] ) ) ); elseif( isset( $arrValues['technical_contact_email'] ) ) $this->setTechnicalContactEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['technical_contact_email'] ) : $arrValues['technical_contact_email'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_test_type'] ) && $boolDirectSet ) $this->set( 'm_intIsTestType', trim( $arrValues['is_test_type'] ) ); elseif( isset( $arrValues['is_test_type'] ) ) $this->setIsTestType( $arrValues['is_test_type'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 150, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setTechnicalContactName( $strTechnicalContactName ) {
		$this->set( 'm_strTechnicalContactName', CStrings::strTrimDef( $strTechnicalContactName, 50, NULL, true ) );
	}

	public function getTechnicalContactName() {
		return $this->m_strTechnicalContactName;
	}

	public function sqlTechnicalContactName() {
		return ( true == isset( $this->m_strTechnicalContactName ) ) ? '\'' . addslashes( $this->m_strTechnicalContactName ) . '\'' : 'NULL';
	}

	public function setTechnicalContactEmail( $strTechnicalContactEmail ) {
		$this->set( 'm_strTechnicalContactEmail', CStrings::strTrimDef( $strTechnicalContactEmail, 100, NULL, true ) );
	}

	public function getTechnicalContactEmail() {
		return $this->m_strTechnicalContactEmail;
	}

	public function sqlTechnicalContactEmail() {
		return ( true == isset( $this->m_strTechnicalContactEmail ) ) ? '\'' . addslashes( $this->m_strTechnicalContactEmail ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsTestType( $intIsTestType ) {
		$this->set( 'm_intIsTestType', CStrings::strToIntDef( $intIsTestType, NULL, false ) );
	}

	public function getIsTestType() {
		return $this->m_intIsTestType;
	}

	public function sqlIsTestType() {
		return ( true == isset( $this->m_intIsTestType ) ) ? ( string ) $this->m_intIsTestType : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'technical_contact_name' => $this->getTechnicalContactName(),
			'technical_contact_email' => $this->getTechnicalContactEmail(),
			'is_published' => $this->getIsPublished(),
			'is_test_type' => $this->getIsTestType(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>