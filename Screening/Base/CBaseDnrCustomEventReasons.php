<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDnrCustomEventReasons
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDnrCustomEventReasons extends CEosPluralBase {

	/**
	 * @return CDnrCustomEventReason[]
	 */
	public static function fetchDnrCustomEventReasons( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDnrCustomEventReason::class, $objDatabase );
	}

	/**
	 * @return CDnrCustomEventReason
	 */
	public static function fetchDnrCustomEventReason( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDnrCustomEventReason::class, $objDatabase );
	}

	public static function fetchDnrCustomEventReasonCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'dnr_custom_event_reasons', $objDatabase );
	}

	public static function fetchDnrCustomEventReasonByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDnrCustomEventReason( sprintf( 'SELECT * FROM dnr_custom_event_reasons WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDnrCustomEventReasonsByCid( $intCid, $objDatabase ) {
		return self::fetchDnrCustomEventReasons( sprintf( 'SELECT * FROM dnr_custom_event_reasons WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDnrCustomEventReasonsByDelinquentEventTypeIdByCid( $intDelinquentEventTypeId, $intCid, $objDatabase ) {
		return self::fetchDnrCustomEventReasons( sprintf( 'SELECT * FROM dnr_custom_event_reasons WHERE delinquent_event_type_id = %d AND cid = %d', ( int ) $intDelinquentEventTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDnrCustomEventReasonsByDelinquentEventReasonIdByCid( $intDelinquentEventReasonId, $intCid, $objDatabase ) {
		return self::fetchDnrCustomEventReasons( sprintf( 'SELECT * FROM dnr_custom_event_reasons WHERE delinquent_event_reason_id = %d AND cid = %d', ( int ) $intDelinquentEventReasonId, ( int ) $intCid ), $objDatabase );
	}

}
?>