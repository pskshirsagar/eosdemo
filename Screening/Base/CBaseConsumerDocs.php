<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CConsumerDocs
 * Do not add any new functions to this class.
 */

class CBaseConsumerDocs extends CEosPluralBase {

	/**
	 * @return CConsumerDoc[]
	 */
	public static function fetchConsumerDocs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CConsumerDoc', $objDatabase );
	}

	/**
	 * @return CConsumerDoc
	 */
	public static function fetchConsumerDoc( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CConsumerDoc', $objDatabase );
	}

	public static function fetchConsumerDocCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'consumer_docs', $objDatabase );
	}

	public static function fetchConsumerDocById( $intId, $objDatabase ) {
		return self::fetchConsumerDoc( sprintf( 'SELECT * FROM consumer_docs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchConsumerDocsByConsumerId( $intConsumerId, $objDatabase ) {
		return self::fetchConsumerDocs( sprintf( 'SELECT * FROM consumer_docs WHERE consumer_id = %d', ( int ) $intConsumerId ), $objDatabase );
	}

	public static function fetchConsumerDocsByConsumerDisputeId( $intConsumerDisputeId, $objDatabase ) {
		return self::fetchConsumerDocs( sprintf( 'SELECT * FROM consumer_docs WHERE consumer_dispute_id = %d', ( int ) $intConsumerDisputeId ), $objDatabase );
	}

	public static function fetchConsumerDocsByConsumerDocumentTypeId( $intConsumerDocumentTypeId, $objDatabase ) {
		return self::fetchConsumerDocs( sprintf( 'SELECT * FROM consumer_docs WHERE consumer_document_type_id = %d', ( int ) $intConsumerDocumentTypeId ), $objDatabase );
	}

}
?>