<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPropertyPreferences
 * Do not add any new functions to this class.
 */

class CBaseScreeningPropertyPreferences extends CEosPluralBase {

	/**
	 * @return CScreeningPropertyPreference[]
	 */
	public static function fetchScreeningPropertyPreferences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningPropertyPreference::class, $objDatabase );
	}

	/**
	 * @return CScreeningPropertyPreference
	 */
	public static function fetchScreeningPropertyPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningPropertyPreference::class, $objDatabase );
	}

	public static function fetchScreeningPropertyPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_property_preferences', $objDatabase );
	}

	public static function fetchScreeningPropertyPreferenceById( $intId, $objDatabase ) {
		return self::fetchScreeningPropertyPreference( sprintf( 'SELECT * FROM screening_property_preferences WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningPropertyPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningPropertyPreferences( sprintf( 'SELECT * FROM screening_property_preferences WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPropertyPreferencesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchScreeningPropertyPreferences( sprintf( 'SELECT * FROM screening_property_preferences WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchScreeningPropertyPreferencesByScreeningCompanyPreferenceTypeId( $intScreeningCompanyPreferenceTypeId, $objDatabase ) {
		return self::fetchScreeningPropertyPreferences( sprintf( 'SELECT * FROM screening_property_preferences WHERE screening_company_preference_type_id = %d', ( int ) $intScreeningCompanyPreferenceTypeId ), $objDatabase );
	}

}
?>