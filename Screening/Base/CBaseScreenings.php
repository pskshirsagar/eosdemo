<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreenings
 * Do not add any new functions to this class.
 */

class CBaseScreenings extends CEosPluralBase {

	/**
	 * @return CScreening[]
	 */
	public static function fetchScreenings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreening::class, $objDatabase );
	}

	/**
	 * @return CScreening
	 */
	public static function fetchScreening( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreening::class, $objDatabase );
	}

	public static function fetchScreeningCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screenings', $objDatabase );
	}

	public static function fetchScreeningById( $intId, $objDatabase ) {
		return self::fetchScreening( sprintf( 'SELECT * FROM screenings WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchScreeningsByCid( $intCid, $objDatabase ) {
		return self::fetchScreenings( sprintf( 'SELECT * FROM screenings WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchScreeningsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchScreenings( sprintf( 'SELECT * FROM screenings WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchScreeningsByScreeningConfigurationId( $intScreeningConfigurationId, $objDatabase ) {
		return self::fetchScreenings( sprintf( 'SELECT * FROM screenings WHERE screening_configuration_id = %d', $intScreeningConfigurationId ), $objDatabase );
	}

	public static function fetchScreeningsByTransmissionId( $intTransmissionId, $objDatabase ) {
		return self::fetchScreenings( sprintf( 'SELECT * FROM screenings WHERE transmission_id = %d', $intTransmissionId ), $objDatabase );
	}

	public static function fetchScreeningsByCustomApplicationId( $intCustomApplicationId, $objDatabase ) {
		return self::fetchScreenings( sprintf( 'SELECT * FROM screenings WHERE custom_application_id = %d', $intCustomApplicationId ), $objDatabase );
	}

	public static function fetchScreeningsByApplicationTypeId( $intApplicationTypeId, $objDatabase ) {
		return self::fetchScreenings( sprintf( 'SELECT * FROM screenings WHERE application_type_id = %d', $intApplicationTypeId ), $objDatabase );
	}

	public static function fetchScreeningsByScreeningStatusTypeId( $intScreeningStatusTypeId, $objDatabase ) {
		return self::fetchScreenings( sprintf( 'SELECT * FROM screenings WHERE screening_status_type_id = %d', $intScreeningStatusTypeId ), $objDatabase );
	}

	public static function fetchScreeningsByOriginalScreeningRecommendationTypeId( $intOriginalScreeningRecommendationTypeId, $objDatabase ) {
		return self::fetchScreenings( sprintf( 'SELECT * FROM screenings WHERE original_screening_recommendation_type_id = %d', $intOriginalScreeningRecommendationTypeId ), $objDatabase );
	}

	public static function fetchScreeningsByScreeningRecommendationTypeId( $intScreeningRecommendationTypeId, $objDatabase ) {
		return self::fetchScreenings( sprintf( 'SELECT * FROM screenings WHERE screening_recommendation_type_id = %d', $intScreeningRecommendationTypeId ), $objDatabase );
	}

	public static function fetchScreeningsByScreeningDecisionTypeId( $intScreeningDecisionTypeId, $objDatabase ) {
		return self::fetchScreenings( sprintf( 'SELECT * FROM screenings WHERE screening_decision_type_id = %d', $intScreeningDecisionTypeId ), $objDatabase );
	}

	public static function fetchScreeningsByLeadSourceId( $intLeadSourceId, $objDatabase ) {
		return self::fetchScreenings( sprintf( 'SELECT * FROM screenings WHERE lead_source_id = %d', $intLeadSourceId ), $objDatabase );
	}

}
?>