<?php

class CBaseScreeningMetaDataDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_meta_data_details';

	protected $m_intId;
	protected $m_intScreeningTradelineTypeId;
	protected $m_intScreeningMetaDataTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intIsPublished;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_tradeline_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningTradelineTypeId', trim( $arrValues['screening_tradeline_type_id'] ) ); elseif( isset( $arrValues['screening_tradeline_type_id'] ) ) $this->setScreeningTradelineTypeId( $arrValues['screening_tradeline_type_id'] );
		if( isset( $arrValues['screening_meta_data_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningMetaDataTypeId', trim( $arrValues['screening_meta_data_type_id'] ) ); elseif( isset( $arrValues['screening_meta_data_type_id'] ) ) $this->setScreeningMetaDataTypeId( $arrValues['screening_meta_data_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningTradelineTypeId( $intScreeningTradelineTypeId ) {
		$this->set( 'm_intScreeningTradelineTypeId', CStrings::strToIntDef( $intScreeningTradelineTypeId, NULL, false ) );
	}

	public function getScreeningTradelineTypeId() {
		return $this->m_intScreeningTradelineTypeId;
	}

	public function sqlScreeningTradelineTypeId() {
		return ( true == isset( $this->m_intScreeningTradelineTypeId ) ) ? ( string ) $this->m_intScreeningTradelineTypeId : 'NULL';
	}

	public function setScreeningMetaDataTypeId( $intScreeningMetaDataTypeId ) {
		$this->set( 'm_intScreeningMetaDataTypeId', CStrings::strToIntDef( $intScreeningMetaDataTypeId, NULL, false ) );
	}

	public function getScreeningMetaDataTypeId() {
		return $this->m_intScreeningMetaDataTypeId;
	}

	public function sqlScreeningMetaDataTypeId() {
		return ( true == isset( $this->m_intScreeningMetaDataTypeId ) ) ? ( string ) $this->m_intScreeningMetaDataTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, -1, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_tradeline_type_id' => $this->getScreeningTradelineTypeId(),
			'screening_meta_data_type_id' => $this->getScreeningMetaDataTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished()
		);
	}

}
?>