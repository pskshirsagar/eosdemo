<?php

class CBaseScreenType extends CEosSingularBase {

	const TABLE_NAME = 'public.screen_types';

	protected $m_intId;
	protected $m_strChargeCodeId;
	protected $m_intParentScreenTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolIsEvaluationRequired;
	protected $m_boolIsUseForPrescreening;
	protected $m_intIsPublished;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsEvaluationRequired = true;
		$this->m_boolIsUseForPrescreening = false;
		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_strChargeCodeId', trim( stripcslashes( $arrValues['charge_code_id'] ) ) ); elseif( isset( $arrValues['charge_code_id'] ) ) $this->setChargeCodeId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['charge_code_id'] ) : $arrValues['charge_code_id'] );
		if( isset( $arrValues['parent_screen_type_id'] ) && $boolDirectSet ) $this->set( 'm_intParentScreenTypeId', trim( $arrValues['parent_screen_type_id'] ) ); elseif( isset( $arrValues['parent_screen_type_id'] ) ) $this->setParentScreenTypeId( $arrValues['parent_screen_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_evaluation_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsEvaluationRequired', trim( stripcslashes( $arrValues['is_evaluation_required'] ) ) ); elseif( isset( $arrValues['is_evaluation_required'] ) ) $this->setIsEvaluationRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_evaluation_required'] ) : $arrValues['is_evaluation_required'] );
		if( isset( $arrValues['is_use_for_prescreening'] ) && $boolDirectSet ) $this->set( 'm_boolIsUseForPrescreening', trim( stripcslashes( $arrValues['is_use_for_prescreening'] ) ) ); elseif( isset( $arrValues['is_use_for_prescreening'] ) ) $this->setIsUseForPrescreening( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_use_for_prescreening'] ) : $arrValues['is_use_for_prescreening'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setChargeCodeId( $strChargeCodeId ) {
		$this->set( 'm_strChargeCodeId', CStrings::strTrimDef( $strChargeCodeId, -1, NULL, true ) );
	}

	public function getChargeCodeId() {
		return $this->m_strChargeCodeId;
	}

	public function sqlChargeCodeId() {
		return ( true == isset( $this->m_strChargeCodeId ) ) ? '\'' . addslashes( $this->m_strChargeCodeId ) . '\'' : 'NULL';
	}

	public function setParentScreenTypeId( $intParentScreenTypeId ) {
		$this->set( 'm_intParentScreenTypeId', CStrings::strToIntDef( $intParentScreenTypeId, NULL, false ) );
	}

	public function getParentScreenTypeId() {
		return $this->m_intParentScreenTypeId;
	}

	public function sqlParentScreenTypeId() {
		return ( true == isset( $this->m_intParentScreenTypeId ) ) ? ( string ) $this->m_intParentScreenTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, -1, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsEvaluationRequired( $boolIsEvaluationRequired ) {
		$this->set( 'm_boolIsEvaluationRequired', CStrings::strToBool( $boolIsEvaluationRequired ) );
	}

	public function getIsEvaluationRequired() {
		return $this->m_boolIsEvaluationRequired;
	}

	public function sqlIsEvaluationRequired() {
		return ( true == isset( $this->m_boolIsEvaluationRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEvaluationRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsUseForPrescreening( $boolIsUseForPrescreening ) {
		$this->set( 'm_boolIsUseForPrescreening', CStrings::strToBool( $boolIsUseForPrescreening ) );
	}

	public function getIsUseForPrescreening() {
		return $this->m_boolIsUseForPrescreening;
	}

	public function sqlIsUseForPrescreening() {
		return ( true == isset( $this->m_boolIsUseForPrescreening ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsUseForPrescreening ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'charge_code_id' => $this->getChargeCodeId(),
			'parent_screen_type_id' => $this->getParentScreenTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_evaluation_required' => $this->getIsEvaluationRequired(),
			'is_use_for_prescreening' => $this->getIsUseForPrescreening(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>