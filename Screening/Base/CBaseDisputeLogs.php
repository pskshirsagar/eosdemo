<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDisputeLogs
 * Do not add any new functions to this class.
 */

class CBaseDisputeLogs extends CEosPluralBase {

	/**
	 * @return CDisputeLog[]
	 */
	public static function fetchDisputeLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDisputeLog', $objDatabase );
	}

	/**
	 * @return CDisputeLog
	 */
	public static function fetchDisputeLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDisputeLog', $objDatabase );
	}

	public static function fetchDisputeLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'dispute_logs', $objDatabase );
	}

	public static function fetchDisputeLogById( $intId, $objDatabase ) {
		return self::fetchDisputeLog( sprintf( 'SELECT * FROM dispute_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDisputeLogsByConsumerId( $intConsumerId, $objDatabase ) {
		return self::fetchDisputeLogs( sprintf( 'SELECT * FROM dispute_logs WHERE consumer_id = %d', ( int ) $intConsumerId ), $objDatabase );
	}

	public static function fetchDisputeLogsByConsumerDisputeId( $intConsumerDisputeId, $objDatabase ) {
		return self::fetchDisputeLogs( sprintf( 'SELECT * FROM dispute_logs WHERE consumer_dispute_id = %d', ( int ) $intConsumerDisputeId ), $objDatabase );
	}

	public static function fetchDisputeLogsByLogTypeId( $intLogTypeId, $objDatabase ) {
		return self::fetchDisputeLogs( sprintf( 'SELECT * FROM dispute_logs WHERE log_type_id = %d', ( int ) $intLogTypeId ), $objDatabase );
	}

}
?>