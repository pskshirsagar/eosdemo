<?php

class CBaseScreeningAccountRate extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_account_rates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScreeningAccountId;
	protected $m_intScreenTypeId;
	protected $m_intScreeningDataProviderId;
	protected $m_fltCost;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intDefaultAccountId;
    protected $m_intSelectionModeTypeId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screening_account_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningAccountId', trim( $arrValues['screening_account_id'] ) ); elseif( isset( $arrValues['screening_account_id'] ) ) $this->setScreeningAccountId( $arrValues['screening_account_id'] );
		if( isset( $arrValues['screen_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreenTypeId', trim( $arrValues['screen_type_id'] ) ); elseif( isset( $arrValues['screen_type_id'] ) ) $this->setScreenTypeId( $arrValues['screen_type_id'] );
		if( isset( $arrValues['screening_data_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningDataProviderId', trim( $arrValues['screening_data_provider_id'] ) ); elseif( isset( $arrValues['screening_data_provider_id'] ) ) $this->setScreeningDataProviderId( $arrValues['screening_data_provider_id'] );
		if( isset( $arrValues['cost'] ) && $boolDirectSet ) $this->set( 'm_fltCost', trim( $arrValues['cost'] ) ); elseif( isset( $arrValues['cost'] ) ) $this->setCost( $arrValues['cost'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['default_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultAccountId', trim( $arrValues['default_account_id'] ) ); elseif( isset( $arrValues['default_account_id'] ) ) $this->setDefaultAccountId( $arrValues['default_account_id'] );
        if( isset( $arrValues['selection_mode_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSelectionModeTypeId', trim( $arrValues['selection_mode_type_id'] ) ); elseif( isset( $arrValues['selection_mode_type_id'] ) ) $this->setSelectionModeTypeId( $arrValues['selection_mode_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreeningAccountId( $intScreeningAccountId ) {
		$this->set( 'm_intScreeningAccountId', CStrings::strToIntDef( $intScreeningAccountId, NULL, false ) );
	}

	public function getScreeningAccountId() {
		return $this->m_intScreeningAccountId;
	}

	public function sqlScreeningAccountId() {
		return ( true == isset( $this->m_intScreeningAccountId ) ) ? ( string ) $this->m_intScreeningAccountId : 'NULL';
	}

	public function setScreenTypeId( $intScreenTypeId ) {
		$this->set( 'm_intScreenTypeId', CStrings::strToIntDef( $intScreenTypeId, NULL, false ) );
	}

	public function getScreenTypeId() {
		return $this->m_intScreenTypeId;
	}

	public function sqlScreenTypeId() {
		return ( true == isset( $this->m_intScreenTypeId ) ) ? ( string ) $this->m_intScreenTypeId : 'NULL';
	}

	public function setScreeningDataProviderId( $intScreeningDataProviderId ) {
		$this->set( 'm_intScreeningDataProviderId', CStrings::strToIntDef( $intScreeningDataProviderId, NULL, false ) );
	}

	public function getScreeningDataProviderId() {
		return $this->m_intScreeningDataProviderId;
	}

	public function sqlScreeningDataProviderId() {
		return ( true == isset( $this->m_intScreeningDataProviderId ) ) ? ( string ) $this->m_intScreeningDataProviderId : 'NULL';
	}

	public function setCost( $fltCost ) {
		$this->set( 'm_fltCost', CStrings::strToFloatDef( $fltCost, NULL, false, 2 ) );
	}

	public function getCost() {
		return $this->m_fltCost;
	}

	public function sqlCost() {
		return ( true == isset( $this->m_fltCost ) ) ? ( string ) $this->m_fltCost : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDefaultAccountId( $intDefaultAccountId ) {
		$this->set( 'm_intDefaultAccountId', CStrings::strToIntDef( $intDefaultAccountId, NULL, false ) );
	}

	public function getDefaultAccountId() {
		return $this->m_intDefaultAccountId;
	}

	public function sqlDefaultAccountId() {
		return ( true == isset( $this->m_intDefaultAccountId ) ) ? ( string ) $this->m_intDefaultAccountId : 'NULL';
	}

    public function setSelectionModeTypeId( $intSelectionModeTypeId ) {
        $this->set( 'm_intSelectionModeTypeId', CStrings::strToIntDef( $intSelectionModeTypeId, NULL, false ) );
    }

    public function getSelectionModeTypeId() {
        return $this->m_intSelectionModeTypeId;
    }

    public function sqlSelectionModeTypeId() {
        return ( true == isset( $this->m_intSelectionModeTypeId ) ) ? ( string ) $this->m_intSelectionModeTypeId : 'NULL';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, screening_account_id, screen_type_id, screening_data_provider_id, cost, updated_by, updated_on, created_by, created_on, selection_mode_type_id, default_account_id )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlScreeningAccountId() . ', ' .
 						$this->sqlScreenTypeId() . ', ' .
 						$this->sqlScreeningDataProviderId() . ', ' .
 						$this->sqlCost() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
                        $this->sqlSelectionModeTypeId() . ' , ' .
 						$this->sqlDefaultAccountId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_account_id = ' . $this->sqlScreeningAccountId() . ','; } elseif( true == array_key_exists( 'ScreeningAccountId', $this->getChangedColumns() ) ) { $strSql .= ' screening_account_id = ' . $this->sqlScreeningAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId() . ','; } elseif( true == array_key_exists( 'ScreenTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_data_provider_id = ' . $this->sqlScreeningDataProviderId() . ','; } elseif( true == array_key_exists( 'ScreeningDataProviderId', $this->getChangedColumns() ) ) { $strSql .= ' screening_data_provider_id = ' . $this->sqlScreeningDataProviderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cost = ' . $this->sqlCost() . ','; } elseif( true == array_key_exists( 'Cost', $this->getChangedColumns() ) ) { $strSql .= ' cost = ' . $this->sqlCost() . ','; $boolUpdate = true; }
                        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' selection_mode_type_id = ' . $this->sqlSelectionModeTypeId(). ',' ; } elseif( true == array_key_exists( 'SelectionModeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' selection_mode_type_id = ' . $this->sqlSelectionModeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_account_id = ' . $this->sqlDefaultAccountId() . ','; } elseif( true == array_key_exists( 'DefaultAccountId', $this->getChangedColumns() ) ) { $strSql .= ' default_account_id = ' . $this->sqlDefaultAccountId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'screening_account_id' => $this->getScreeningAccountId(),
			'screen_type_id' => $this->getScreenTypeId(),
			'screening_data_provider_id' => $this->getScreeningDataProviderId(),
			'cost' => $this->getCost(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
            'selection_mode_type_id' => $this->getSelectionModeTypeId(),
			'default_account_id' => $this->getDefaultAccountId()
		);
	}

}
?>