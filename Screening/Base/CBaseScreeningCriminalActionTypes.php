<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCriminalActionTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningCriminalActionTypes extends CEosPluralBase {

	/**
	 * @return CScreeningCriminalActionType[]
	 */
	public static function fetchScreeningCriminalActionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningCriminalActionType', $objDatabase );
	}

	/**
	 * @return CScreeningCriminalActionType
	 */
	public static function fetchScreeningCriminalActionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningCriminalActionType', $objDatabase );
	}

	public static function fetchScreeningCriminalActionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_criminal_action_types', $objDatabase );
	}

	public static function fetchScreeningCriminalActionTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningCriminalActionType( sprintf( 'SELECT * FROM screening_criminal_action_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>