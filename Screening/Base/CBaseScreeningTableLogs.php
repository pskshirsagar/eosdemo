<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningTableLogs
 * Do not add any new functions to this class.
 */

class CBaseScreeningTableLogs extends CEosPluralBase {

	/**
	 * @return CScreeningTableLog[]
	 */
	public static function fetchScreeningTableLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningTableLog', $objDatabase );
	}

	/**
	 * @return CScreeningTableLog
	 */
	public static function fetchScreeningTableLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningTableLog', $objDatabase );
	}

	public static function fetchScreeningTableLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_table_logs', $objDatabase );
	}

	public static function fetchScreeningTableLogById( $intId, $objDatabase ) {
		return self::fetchScreeningTableLog( sprintf( 'SELECT * FROM screening_table_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningTableLogsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningTableLogs( sprintf( 'SELECT * FROM screening_table_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningTableLogsByScreeningId( $intScreeningId, $objDatabase ) {
		return self::fetchScreeningTableLogs( sprintf( 'SELECT * FROM screening_table_logs WHERE screening_id = %d', ( int ) $intScreeningId ), $objDatabase );
	}

	public static function fetchScreeningTableLogsByScreeningApplicantId( $intScreeningApplicantId, $objDatabase ) {
		return self::fetchScreeningTableLogs( sprintf( 'SELECT * FROM screening_table_logs WHERE screening_applicant_id = %d', ( int ) $intScreeningApplicantId ), $objDatabase );
	}

}
?>