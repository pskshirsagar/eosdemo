<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreenTypeConfigPreferenceDataProviderRules
 * Do not add any new functions to this class.
 */

class CBaseScreenTypeConfigPreferenceDataProviderRules extends CEosPluralBase {

	/**
	 * @return CScreenTypeConfigPreferenceDataProviderRule[]
	 */
	public static function fetchScreenTypeConfigPreferenceDataProviderRules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreenTypeConfigPreferenceDataProviderRule', $objDatabase );
	}

	/**
	 * @return CScreenTypeConfigPreferenceDataProviderRule
	 */
	public static function fetchScreenTypeConfigPreferenceDataProviderRule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreenTypeConfigPreferenceDataProviderRule', $objDatabase );
	}

	public static function fetchScreenTypeConfigPreferenceDataProviderRuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screen_type_config_preference_data_provider_rules', $objDatabase );
	}

	public static function fetchScreenTypeConfigPreferenceDataProviderRuleById( $intId, $objDatabase ) {
		return self::fetchScreenTypeConfigPreferenceDataProviderRule( sprintf( 'SELECT * FROM screen_type_config_preference_data_provider_rules WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreenTypeConfigPreferenceDataProviderRulesByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreenTypeConfigPreferenceDataProviderRules( sprintf( 'SELECT * FROM screen_type_config_preference_data_provider_rules WHERE screen_type_id = %d', ( int ) $intScreenTypeId ), $objDatabase );
	}

	public static function fetchScreenTypeConfigPreferenceDataProviderRulesByScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		return self::fetchScreenTypeConfigPreferenceDataProviderRules( sprintf( 'SELECT * FROM screen_type_config_preference_data_provider_rules WHERE screening_config_preference_type_id = %d', ( int ) $intScreeningConfigPreferenceTypeId ), $objDatabase );
	}

	public static function fetchScreenTypeConfigPreferenceDataProviderRulesByScreeningDataProviderTypeId( $intScreeningDataProviderTypeId, $objDatabase ) {
		return self::fetchScreenTypeConfigPreferenceDataProviderRules( sprintf( 'SELECT * FROM screen_type_config_preference_data_provider_rules WHERE screening_data_provider_type_id = %d', ( int ) $intScreeningDataProviderTypeId ), $objDatabase );
	}

	public static function fetchScreenTypeConfigPreferenceDataProviderRulesByScreeningSearchActionId( $intScreeningSearchActionId, $objDatabase ) {
		return self::fetchScreenTypeConfigPreferenceDataProviderRules( sprintf( 'SELECT * FROM screen_type_config_preference_data_provider_rules WHERE screening_search_action_id = %d', ( int ) $intScreeningSearchActionId ), $objDatabase );
	}

}
?>