<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CTransactionReviewLogs
 * Do not add any new functions to this class.
 */

class CBaseTransactionReviewLogs extends CEosPluralBase {

	/**
	 * @return CTransactionReviewLog[]
	 */
	public static function fetchTransactionReviewLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTransactionReviewLog', $objDatabase );
	}

	/**
	 * @return CTransactionReviewLog
	 */
	public static function fetchTransactionReviewLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTransactionReviewLog', $objDatabase );
	}

	public static function fetchTransactionReviewLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transaction_review_logs', $objDatabase );
	}

	public static function fetchTransactionReviewLogById( $intId, $objDatabase ) {
		return self::fetchTransactionReviewLog( sprintf( 'SELECT * FROM transaction_review_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTransactionReviewLogsByTransactionReviewId( $intTransactionReviewId, $objDatabase ) {
		return self::fetchTransactionReviewLogs( sprintf( 'SELECT * FROM transaction_review_logs WHERE transaction_review_id = %d', ( int ) $intTransactionReviewId ), $objDatabase );
	}

	public static function fetchTransactionReviewLogsByTransactionReviewLogTypeId( $intTransactionReviewLogTypeId, $objDatabase ) {
		return self::fetchTransactionReviewLogs( sprintf( 'SELECT * FROM transaction_review_logs WHERE transaction_review_log_type_id = %d', ( int ) $intTransactionReviewLogTypeId ), $objDatabase );
	}

	public static function fetchTransactionReviewLogsByOldStatusId( $intOldStatusId, $objDatabase ) {
		return self::fetchTransactionReviewLogs( sprintf( 'SELECT * FROM transaction_review_logs WHERE old_status_id = %d', ( int ) $intOldStatusId ), $objDatabase );
	}

	public static function fetchTransactionReviewLogsByNewStatusId( $intNewStatusId, $objDatabase ) {
		return self::fetchTransactionReviewLogs( sprintf( 'SELECT * FROM transaction_review_logs WHERE new_status_id = %d', ( int ) $intNewStatusId ), $objDatabase );
	}

}
?>