<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CResidentDelinquentEvents
 * Do not add any new functions to this class.
 */

class CBaseResidentDelinquentEvents extends CEosPluralBase {

	/**
	 * @return CResidentDelinquentEvent[]
	 */
	public static function fetchResidentDelinquentEvents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CResidentDelinquentEvent', $objDatabase );
	}

	/**
	 * @return CResidentDelinquentEvent
	 */
	public static function fetchResidentDelinquentEvent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CResidentDelinquentEvent', $objDatabase );
	}

	public static function fetchResidentDelinquentEventCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'resident_delinquent_events', $objDatabase );
	}

	public static function fetchResidentDelinquentEventById( $intId, $objDatabase ) {
		return self::fetchResidentDelinquentEvent( sprintf( 'SELECT * FROM resident_delinquent_events WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchResidentDelinquentEventsByCid( $intCid, $objDatabase ) {
		return self::fetchResidentDelinquentEvents( sprintf( 'SELECT * FROM resident_delinquent_events WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentDelinquentEventsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchResidentDelinquentEvents( sprintf( 'SELECT * FROM resident_delinquent_events WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchResidentDelinquentEventsByLeaseId( $intLeaseId, $objDatabase ) {
		return self::fetchResidentDelinquentEvents( sprintf( 'SELECT * FROM resident_delinquent_events WHERE lease_id = %d', ( int ) $intLeaseId ), $objDatabase );
	}

	public static function fetchResidentDelinquentEventsByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchResidentDelinquentEvents( sprintf( 'SELECT * FROM resident_delinquent_events WHERE customer_id = %d', ( int ) $intCustomerId ), $objDatabase );
	}

	public static function fetchResidentDelinquentEventsByDelinquentEventTypeId( $intDelinquentEventTypeId, $objDatabase ) {
		return self::fetchResidentDelinquentEvents( sprintf( 'SELECT * FROM resident_delinquent_events WHERE delinquent_event_type_id = %d', ( int ) $intDelinquentEventTypeId ), $objDatabase );
	}

	public static function fetchResidentDelinquentEventsByDelinquentEventReasonId( $intDelinquentEventReasonId, $objDatabase ) {
		return self::fetchResidentDelinquentEvents( sprintf( 'SELECT * FROM resident_delinquent_events WHERE delinquent_event_reason_id = %d', ( int ) $intDelinquentEventReasonId ), $objDatabase );
	}

	public static function fetchResidentDelinquentEventsByDelinquentEventStatusTypeId( $intDelinquentEventStatusTypeId, $objDatabase ) {
		return self::fetchResidentDelinquentEvents( sprintf( 'SELECT * FROM resident_delinquent_events WHERE delinquent_event_status_type_id = %d', ( int ) $intDelinquentEventStatusTypeId ), $objDatabase );
	}

}
?>