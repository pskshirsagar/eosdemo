<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDelinquentResidentEvents
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDelinquentResidentEvents extends CEosPluralBase {

	/**
	 * @return CDelinquentResidentEvent[]
	 */
	public static function fetchDelinquentResidentEvents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDelinquentResidentEvent::class, $objDatabase );
	}

	/**
	 * @return CDelinquentResidentEvent
	 */
	public static function fetchDelinquentResidentEvent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDelinquentResidentEvent::class, $objDatabase );
	}

	public static function fetchDelinquentResidentEventCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'delinquent_resident_events', $objDatabase );
	}

	public static function fetchDelinquentResidentEventByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDelinquentResidentEvent( sprintf( 'SELECT * FROM delinquent_resident_events WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDelinquentResidentEventsByCid( $intCid, $objDatabase ) {
		return self::fetchDelinquentResidentEvents( sprintf( 'SELECT * FROM delinquent_resident_events WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDelinquentResidentEventsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchDelinquentResidentEvents( sprintf( 'SELECT * FROM delinquent_resident_events WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDelinquentResidentEventsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchDelinquentResidentEvents( sprintf( 'SELECT * FROM delinquent_resident_events WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDelinquentResidentEventsByDelinquentResidentIdByCid( $intDelinquentResidentId, $intCid, $objDatabase ) {
		return self::fetchDelinquentResidentEvents( sprintf( 'SELECT * FROM delinquent_resident_events WHERE delinquent_resident_id = %d AND cid = %d', ( int ) $intDelinquentResidentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDelinquentResidentEventsByDelinquentEventTypeIdByCid( $intDelinquentEventTypeId, $intCid, $objDatabase ) {
		return self::fetchDelinquentResidentEvents( sprintf( 'SELECT * FROM delinquent_resident_events WHERE delinquent_event_type_id = %d AND cid = %d', ( int ) $intDelinquentEventTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDelinquentResidentEventsByDelinquentEventStatusTypeIdByCid( $intDelinquentEventStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchDelinquentResidentEvents( sprintf( 'SELECT * FROM delinquent_resident_events WHERE delinquent_event_status_type_id = %d AND cid = %d', ( int ) $intDelinquentEventStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDelinquentResidentEventsByDelinquentEventReasonIdByCid( $intDelinquentEventReasonId, $intCid, $objDatabase ) {
		return self::fetchDelinquentResidentEvents( sprintf( 'SELECT * FROM delinquent_resident_events WHERE delinquent_event_reason_id = %d AND cid = %d', ( int ) $intDelinquentEventReasonId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDelinquentResidentEventsByDeletedReasonIdByCid( $intDeletedReasonId, $intCid, $objDatabase ) {
		return self::fetchDelinquentResidentEvents( sprintf( 'SELECT * FROM delinquent_resident_events WHERE deleted_reason_id = %d AND cid = %d', ( int ) $intDeletedReasonId, ( int ) $intCid ), $objDatabase );
	}

}
?>