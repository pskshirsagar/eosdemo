<?php

class CBaseScreeningReviewReport extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_review_reports';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intScreenTypeId;
	protected $m_strReportMonth;
	protected $m_intTotalTransactions;
	protected $m_intTotalReviews;
	protected $m_strTotalReviewTime;
	protected $m_strAverageReviewTime;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intTotalTransactions = '0';
		$this->m_intTotalReviews = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['screen_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreenTypeId', trim( $arrValues['screen_type_id'] ) ); elseif( isset( $arrValues['screen_type_id'] ) ) $this->setScreenTypeId( $arrValues['screen_type_id'] );
		if( isset( $arrValues['report_month'] ) && $boolDirectSet ) $this->set( 'm_strReportMonth', trim( $arrValues['report_month'] ) ); elseif( isset( $arrValues['report_month'] ) ) $this->setReportMonth( $arrValues['report_month'] );
		if( isset( $arrValues['total_transactions'] ) && $boolDirectSet ) $this->set( 'm_intTotalTransactions', trim( $arrValues['total_transactions'] ) ); elseif( isset( $arrValues['total_transactions'] ) ) $this->setTotalTransactions( $arrValues['total_transactions'] );
		if( isset( $arrValues['total_reviews'] ) && $boolDirectSet ) $this->set( 'm_intTotalReviews', trim( $arrValues['total_reviews'] ) ); elseif( isset( $arrValues['total_reviews'] ) ) $this->setTotalReviews( $arrValues['total_reviews'] );
		if( isset( $arrValues['total_review_time'] ) && $boolDirectSet ) $this->set( 'm_strTotalReviewTime', trim( $arrValues['total_review_time'] ) ); elseif( isset( $arrValues['total_review_time'] ) ) $this->setTotalReviewTime( $arrValues['total_review_time'] );
		if( isset( $arrValues['average_review_time'] ) && $boolDirectSet ) $this->set( 'm_strAverageReviewTime', trim( $arrValues['average_review_time'] ) ); elseif( isset( $arrValues['average_review_time'] ) ) $this->setAverageReviewTime( $arrValues['average_review_time'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setScreenTypeId( $intScreenTypeId ) {
		$this->set( 'm_intScreenTypeId', CStrings::strToIntDef( $intScreenTypeId, NULL, false ) );
	}

	public function getScreenTypeId() {
		return $this->m_intScreenTypeId;
	}

	public function sqlScreenTypeId() {
		return ( true == isset( $this->m_intScreenTypeId ) ) ? ( string ) $this->m_intScreenTypeId : 'NULL';
	}

	public function setReportMonth( $strReportMonth ) {
		$this->set( 'm_strReportMonth', CStrings::strTrimDef( $strReportMonth, -1, NULL, true ) );
	}

	public function getReportMonth() {
		return $this->m_strReportMonth;
	}

	public function sqlReportMonth() {
		return ( true == isset( $this->m_strReportMonth ) ) ? '\'' . $this->m_strReportMonth . '\'' : 'NOW()';
	}

	public function setTotalTransactions( $intTotalTransactions ) {
		$this->set( 'm_intTotalTransactions', CStrings::strToIntDef( $intTotalTransactions, NULL, false ) );
	}

	public function getTotalTransactions() {
		return $this->m_intTotalTransactions;
	}

	public function sqlTotalTransactions() {
		return ( true == isset( $this->m_intTotalTransactions ) ) ? ( string ) $this->m_intTotalTransactions : '0';
	}

	public function setTotalReviews( $intTotalReviews ) {
		$this->set( 'm_intTotalReviews', CStrings::strToIntDef( $intTotalReviews, NULL, false ) );
	}

	public function getTotalReviews() {
		return $this->m_intTotalReviews;
	}

	public function sqlTotalReviews() {
		return ( true == isset( $this->m_intTotalReviews ) ) ? ( string ) $this->m_intTotalReviews : '0';
	}

	public function setTotalReviewTime( $strTotalReviewTime ) {
		$this->set( 'm_strTotalReviewTime', CStrings::strTrimDef( $strTotalReviewTime, NULL, NULL, true ) );
	}

	public function getTotalReviewTime() {
		return $this->m_strTotalReviewTime;
	}

	public function sqlTotalReviewTime() {
		return ( true == isset( $this->m_strTotalReviewTime ) ) ? '\'' . addslashes( $this->m_strTotalReviewTime ) . '\'' : 'NULL';
	}

	public function setAverageReviewTime( $strAverageReviewTime ) {
		$this->set( 'm_strAverageReviewTime', CStrings::strTrimDef( $strAverageReviewTime, NULL, NULL, true ) );
	}

	public function getAverageReviewTime() {
		return $this->m_strAverageReviewTime;
	}

	public function sqlAverageReviewTime() {
		return ( true == isset( $this->m_strAverageReviewTime ) ) ? '\'' . addslashes( $this->m_strAverageReviewTime ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, screen_type_id, report_month, total_transactions, total_reviews, total_review_time, average_review_time, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlScreenTypeId() . ', ' .
 						$this->sqlReportMonth() . ', ' .
 						$this->sqlTotalTransactions() . ', ' .
 						$this->sqlTotalReviews() . ', ' .
 						$this->sqlTotalReviewTime() . ', ' .
 						$this->sqlAverageReviewTime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId() . ','; } elseif( true == array_key_exists( 'ScreenTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_month = ' . $this->sqlReportMonth() . ','; } elseif( true == array_key_exists( 'ReportMonth', $this->getChangedColumns() ) ) { $strSql .= ' report_month = ' . $this->sqlReportMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_transactions = ' . $this->sqlTotalTransactions() . ','; } elseif( true == array_key_exists( 'TotalTransactions', $this->getChangedColumns() ) ) { $strSql .= ' total_transactions = ' . $this->sqlTotalTransactions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_reviews = ' . $this->sqlTotalReviews() . ','; } elseif( true == array_key_exists( 'TotalReviews', $this->getChangedColumns() ) ) { $strSql .= ' total_reviews = ' . $this->sqlTotalReviews() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_review_time = ' . $this->sqlTotalReviewTime() . ','; } elseif( true == array_key_exists( 'TotalReviewTime', $this->getChangedColumns() ) ) { $strSql .= ' total_review_time = ' . $this->sqlTotalReviewTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_review_time = ' . $this->sqlAverageReviewTime() . ','; } elseif( true == array_key_exists( 'AverageReviewTime', $this->getChangedColumns() ) ) { $strSql .= ' average_review_time = ' . $this->sqlAverageReviewTime() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'screen_type_id' => $this->getScreenTypeId(),
			'report_month' => $this->getReportMonth(),
			'total_transactions' => $this->getTotalTransactions(),
			'total_reviews' => $this->getTotalReviews(),
			'total_review_time' => $this->getTotalReviewTime(),
			'average_review_time' => $this->getAverageReviewTime(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>