<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageConditionTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningPackageConditionTypes extends CEosPluralBase {

	/**
	 * @return CScreeningPackageConditionType[]
	 */
	public static function fetchScreeningPackageConditionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageConditionType', $objDatabase );
	}

	/**
	 * @return CScreeningPackageConditionType
	 */
	public static function fetchScreeningPackageConditionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageConditionType', $objDatabase );
	}

	public static function fetchScreeningPackageConditionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_condition_types', $objDatabase );
	}

	public static function fetchScreeningPackageConditionTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningPackageConditionType( sprintf( 'SELECT * FROM screening_package_condition_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningPackageConditionTypesByScreeningValueTypeId( $intScreeningValueTypeId, $objDatabase ) {
		return self::fetchScreeningPackageConditionTypes( sprintf( 'SELECT * FROM screening_package_condition_types WHERE screening_value_type_id = %d', ( int ) $intScreeningValueTypeId ), $objDatabase );
	}

}
?>