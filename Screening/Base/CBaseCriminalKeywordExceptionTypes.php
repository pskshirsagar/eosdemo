<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CCriminalKeywordExceptionTypes
 * Do not add any new functions to this class.
 */

class CBaseCriminalKeywordExceptionTypes extends CEosPluralBase {

	/**
	 * @return CCriminalKeywordExceptionType[]
	 */
	public static function fetchCriminalKeywordExceptionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCriminalKeywordExceptionType::class, $objDatabase );
	}

	/**
	 * @return CCriminalKeywordExceptionType
	 */
	public static function fetchCriminalKeywordExceptionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCriminalKeywordExceptionType::class, $objDatabase );
	}

	public static function fetchCriminalKeywordExceptionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'criminal_keyword_exception_types', $objDatabase );
	}

	public static function fetchCriminalKeywordExceptionTypeById( $intId, $objDatabase ) {
		return self::fetchCriminalKeywordExceptionType( sprintf( 'SELECT * FROM criminal_keyword_exception_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>