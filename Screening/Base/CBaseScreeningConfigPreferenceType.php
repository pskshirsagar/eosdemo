<?php

class CBaseScreeningConfigPreferenceType extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_config_preference_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_intScreenTypeId;
	protected $m_intScreeningConfigValueTypeId;
	protected $m_intIsUseForCondition;
	protected $m_intIsPublished;
	protected $m_intSortOrder;
	protected $m_strDescription;

	public function __construct() {
		parent::__construct();

		$this->m_intIsUseForCondition = '0';
		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['screen_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreenTypeId', trim( $arrValues['screen_type_id'] ) ); elseif( isset( $arrValues['screen_type_id'] ) ) $this->setScreenTypeId( $arrValues['screen_type_id'] );
		if( isset( $arrValues['screening_config_value_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningConfigValueTypeId', trim( $arrValues['screening_config_value_type_id'] ) ); elseif( isset( $arrValues['screening_config_value_type_id'] ) ) $this->setScreeningConfigValueTypeId( $arrValues['screening_config_value_type_id'] );
		if( isset( $arrValues['is_use_for_condition'] ) && $boolDirectSet ) $this->set( 'm_intIsUseForCondition', trim( $arrValues['is_use_for_condition'] ) ); elseif( isset( $arrValues['is_use_for_condition'] ) ) $this->setIsUseForCondition( $arrValues['is_use_for_condition'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['sort_order'] ) && $boolDirectSet ) $this->set( 'm_intSortOrder', trim( $arrValues['sort_order'] ) ); elseif( isset( $arrValues['sort_order'] ) ) $this->setSortOrder( $arrValues['sort_order'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, -1, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setScreenTypeId( $intScreenTypeId ) {
		$this->set( 'm_intScreenTypeId', CStrings::strToIntDef( $intScreenTypeId, NULL, false ) );
	}

	public function getScreenTypeId() {
		return $this->m_intScreenTypeId;
	}

	public function sqlScreenTypeId() {
		return ( true == isset( $this->m_intScreenTypeId ) ) ? ( string ) $this->m_intScreenTypeId : 'NULL';
	}

	public function setScreeningConfigValueTypeId( $intScreeningConfigValueTypeId ) {
		$this->set( 'm_intScreeningConfigValueTypeId', CStrings::strToIntDef( $intScreeningConfigValueTypeId, NULL, false ) );
	}

	public function getScreeningConfigValueTypeId() {
		return $this->m_intScreeningConfigValueTypeId;
	}

	public function sqlScreeningConfigValueTypeId() {
		return ( true == isset( $this->m_intScreeningConfigValueTypeId ) ) ? ( string ) $this->m_intScreeningConfigValueTypeId : 'NULL';
	}

	public function setIsUseForCondition( $intIsUseForCondition ) {
		$this->set( 'm_intIsUseForCondition', CStrings::strToIntDef( $intIsUseForCondition, NULL, false ) );
	}

	public function getIsUseForCondition() {
		return $this->m_intIsUseForCondition;
	}

	public function sqlIsUseForCondition() {
		return ( true == isset( $this->m_intIsUseForCondition ) ) ? ( string ) $this->m_intIsUseForCondition : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setSortOrder( $intSortOrder ) {
		$this->set( 'm_intSortOrder', CStrings::strToIntDef( $intSortOrder, NULL, false ) );
	}

	public function getSortOrder() {
		return $this->m_intSortOrder;
	}

	public function sqlSortOrder() {
		return ( true == isset( $this->m_intSortOrder ) ) ? ( string ) $this->m_intSortOrder : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'screen_type_id' => $this->getScreenTypeId(),
			'screening_config_value_type_id' => $this->getScreeningConfigValueTypeId(),
			'is_use_for_condition' => $this->getIsUseForCondition(),
			'is_published' => $this->getIsPublished(),
			'sort_order' => $this->getSortOrder(),
			'description' => $this->getDescription()
		);
	}

}
?>