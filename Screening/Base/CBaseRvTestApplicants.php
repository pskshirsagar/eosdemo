<?php

class CBaseRvTestApplicants extends CEosPluralBase {

	/**
	 * @return CRvTestApplicant[]
	 */
	public static function fetchRvTestApplicants( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRvTestApplicant::class, $objDatabase );
	}

	/**
	 * @return CRvTestApplicant
	 */
	public static function fetchRvTestApplicant( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRvTestApplicant::class, $objDatabase );
	}

	public static function fetchRvTestApplicantCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'rv_test_applicants', $objDatabase );
	}

	public static function fetchRvTestApplicantById( $intId, $objDatabase ) {
		return self::fetchRvTestApplicant( sprintf( 'SELECT * FROM rv_test_applicants WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchRvTestApplicantsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchRvTestApplicants( sprintf( 'SELECT * FROM rv_test_applicants WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchRvTestApplicantsByApplicantId( $intApplicantId, $objDatabase ) {
		return self::fetchRvTestApplicants( sprintf( 'SELECT * FROM rv_test_applicants WHERE applicant_id = %d', $intApplicantId ), $objDatabase );
	}

	public static function fetchRvTestApplicantsByPackageId( $intPackageId, $objDatabase ) {
		return self::fetchRvTestApplicants( sprintf( 'SELECT * FROM rv_test_applicants WHERE package_id = %d', $intPackageId ), $objDatabase );
	}

	public static function fetchRvTestApplicantsByApplicantTypeId( $intApplicantTypeId, $objDatabase ) {
		return self::fetchRvTestApplicants( sprintf( 'SELECT * FROM rv_test_applicants WHERE applicant_type_id = %d', $intApplicantTypeId ), $objDatabase );
	}

	public static function fetchRvTestApplicantsByRvMasterTestCaseId( $intRvMasterTestCaseId, $objDatabase ) {
		return self::fetchRvTestApplicants( sprintf( 'SELECT * FROM rv_test_applicants WHERE rv_master_test_case_id = %d', $intRvMasterTestCaseId ), $objDatabase );
	}

}
?>