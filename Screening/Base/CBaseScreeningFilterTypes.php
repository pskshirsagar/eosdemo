<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningFilterTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningFilterTypes extends CEosPluralBase {

	/**
	 * @return CScreeningFilterType[]
	 */
	public static function fetchScreeningFilterTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningFilterType', $objDatabase );
	}

	/**
	 * @return CScreeningFilterType
	 */
	public static function fetchScreeningFilterType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningFilterType', $objDatabase );
	}

	public static function fetchScreeningFilterTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_filter_types', $objDatabase );
	}

	public static function fetchScreeningFilterTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningFilterType( sprintf( 'SELECT * FROM screening_filter_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>