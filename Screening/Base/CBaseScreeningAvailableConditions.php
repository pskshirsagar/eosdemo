<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningAvailableConditions
 * Do not add any new functions to this class.
 */

class CBaseScreeningAvailableConditions extends CEosPluralBase {

	/**
	 * @return CScreeningAvailableCondition[]
	 */
	public static function fetchScreeningAvailableConditions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningAvailableCondition::class, $objDatabase );
	}

	/**
	 * @return CScreeningAvailableCondition
	 */
	public static function fetchScreeningAvailableCondition( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningAvailableCondition::class, $objDatabase );
	}

	public static function fetchScreeningAvailableConditionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_available_conditions', $objDatabase );
	}

	public static function fetchScreeningAvailableConditionById( $intId, $objDatabase ) {
		return self::fetchScreeningAvailableCondition( sprintf( 'SELECT * FROM screening_available_conditions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningAvailableConditionsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningAvailableConditions( sprintf( 'SELECT * FROM screening_available_conditions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningAvailableConditionsByRateId( $intRateId, $objDatabase ) {
		return self::fetchScreeningAvailableConditions( sprintf( 'SELECT * FROM screening_available_conditions WHERE rate_id = %d', ( int ) $intRateId ), $objDatabase );
	}

	public static function fetchScreeningAvailableConditionsByScreeningPackageAvailableConditionId( $intScreeningPackageAvailableConditionId, $objDatabase ) {
		return self::fetchScreeningAvailableConditions( sprintf( 'SELECT * FROM screening_available_conditions WHERE screening_package_available_condition_id = %d', ( int ) $intScreeningPackageAvailableConditionId ), $objDatabase );
	}

	public static function fetchScreeningAvailableConditionsByScreeningPackageConditionTypeId( $intScreeningPackageConditionTypeId, $objDatabase ) {
		return self::fetchScreeningAvailableConditions( sprintf( 'SELECT * FROM screening_available_conditions WHERE screening_package_condition_type_id = %d', ( int ) $intScreeningPackageConditionTypeId ), $objDatabase );
	}

	public static function fetchScreeningAvailableConditionsByScreeningPackageChargeCodeAmountTypeId( $intScreeningPackageChargeCodeAmountTypeId, $objDatabase ) {
		return self::fetchScreeningAvailableConditions( sprintf( 'SELECT * FROM screening_available_conditions WHERE screening_package_charge_code_amount_type_id = %d', ( int ) $intScreeningPackageChargeCodeAmountTypeId ), $objDatabase );
	}

	public static function fetchScreeningAvailableConditionsByBaseChargeCodeId( $intBaseChargeCodeId, $objDatabase ) {
		return self::fetchScreeningAvailableConditions( sprintf( 'SELECT * FROM screening_available_conditions WHERE base_charge_code_id = %d', ( int ) $intBaseChargeCodeId ), $objDatabase );
	}

	public static function fetchScreeningAvailableConditionsByApplyToChargeCodeId( $intApplyToChargeCodeId, $objDatabase ) {
		return self::fetchScreeningAvailableConditions( sprintf( 'SELECT * FROM screening_available_conditions WHERE apply_to_charge_code_id = %d', ( int ) $intApplyToChargeCodeId ), $objDatabase );
	}

	public static function fetchScreeningAvailableConditionsByChargeTypeId( $intChargeTypeId, $objDatabase ) {
		return self::fetchScreeningAvailableConditions( sprintf( 'SELECT * FROM screening_available_conditions WHERE charge_type_id = %d', ( int ) $intChargeTypeId ), $objDatabase );
	}

}
?>