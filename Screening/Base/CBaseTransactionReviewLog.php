<?php

class CBaseTransactionReviewLog extends CEosSingularBase {

	const TABLE_NAME = 'public.transaction_review_logs';

	protected $m_intId;
	protected $m_intTransactionReviewId;
	protected $m_intTransactionReviewLogTypeId;
	protected $m_intOldStatusId;
	protected $m_intNewStatusId;
	protected $m_strReviewNotes;
	protected $m_intIsArchived;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['transaction_review_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionReviewId', trim( $arrValues['transaction_review_id'] ) ); elseif( isset( $arrValues['transaction_review_id'] ) ) $this->setTransactionReviewId( $arrValues['transaction_review_id'] );
		if( isset( $arrValues['transaction_review_log_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionReviewLogTypeId', trim( $arrValues['transaction_review_log_type_id'] ) ); elseif( isset( $arrValues['transaction_review_log_type_id'] ) ) $this->setTransactionReviewLogTypeId( $arrValues['transaction_review_log_type_id'] );
		if( isset( $arrValues['old_status_id'] ) && $boolDirectSet ) $this->set( 'm_intOldStatusId', trim( $arrValues['old_status_id'] ) ); elseif( isset( $arrValues['old_status_id'] ) ) $this->setOldStatusId( $arrValues['old_status_id'] );
		if( isset( $arrValues['new_status_id'] ) && $boolDirectSet ) $this->set( 'm_intNewStatusId', trim( $arrValues['new_status_id'] ) ); elseif( isset( $arrValues['new_status_id'] ) ) $this->setNewStatusId( $arrValues['new_status_id'] );
		if( isset( $arrValues['review_notes'] ) && $boolDirectSet ) $this->set( 'm_strReviewNotes', trim( stripcslashes( $arrValues['review_notes'] ) ) ); elseif( isset( $arrValues['review_notes'] ) ) $this->setReviewNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['review_notes'] ) : $arrValues['review_notes'] );
		if( isset( $arrValues['is_archived'] ) && $boolDirectSet ) $this->set( 'm_intIsArchived', trim( $arrValues['is_archived'] ) ); elseif( isset( $arrValues['is_archived'] ) ) $this->setIsArchived( $arrValues['is_archived'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTransactionReviewId( $intTransactionReviewId ) {
		$this->set( 'm_intTransactionReviewId', CStrings::strToIntDef( $intTransactionReviewId, NULL, false ) );
	}

	public function getTransactionReviewId() {
		return $this->m_intTransactionReviewId;
	}

	public function sqlTransactionReviewId() {
		return ( true == isset( $this->m_intTransactionReviewId ) ) ? ( string ) $this->m_intTransactionReviewId : 'NULL';
	}

	public function setTransactionReviewLogTypeId( $intTransactionReviewLogTypeId ) {
		$this->set( 'm_intTransactionReviewLogTypeId', CStrings::strToIntDef( $intTransactionReviewLogTypeId, NULL, false ) );
	}

	public function getTransactionReviewLogTypeId() {
		return $this->m_intTransactionReviewLogTypeId;
	}

	public function sqlTransactionReviewLogTypeId() {
		return ( true == isset( $this->m_intTransactionReviewLogTypeId ) ) ? ( string ) $this->m_intTransactionReviewLogTypeId : 'NULL';
	}

	public function setOldStatusId( $intOldStatusId ) {
		$this->set( 'm_intOldStatusId', CStrings::strToIntDef( $intOldStatusId, NULL, false ) );
	}

	public function getOldStatusId() {
		return $this->m_intOldStatusId;
	}

	public function sqlOldStatusId() {
		return ( true == isset( $this->m_intOldStatusId ) ) ? ( string ) $this->m_intOldStatusId : 'NULL';
	}

	public function setNewStatusId( $intNewStatusId ) {
		$this->set( 'm_intNewStatusId', CStrings::strToIntDef( $intNewStatusId, NULL, false ) );
	}

	public function getNewStatusId() {
		return $this->m_intNewStatusId;
	}

	public function sqlNewStatusId() {
		return ( true == isset( $this->m_intNewStatusId ) ) ? ( string ) $this->m_intNewStatusId : 'NULL';
	}

	public function setReviewNotes( $strReviewNotes ) {
		$this->set( 'm_strReviewNotes', CStrings::strTrimDef( $strReviewNotes, -1, NULL, true ) );
	}

	public function getReviewNotes() {
		return $this->m_strReviewNotes;
	}

	public function sqlReviewNotes() {
		return ( true == isset( $this->m_strReviewNotes ) ) ? '\'' . addslashes( $this->m_strReviewNotes ) . '\'' : 'NULL';
	}

	public function setIsArchived( $intIsArchived ) {
		$this->set( 'm_intIsArchived', CStrings::strToIntDef( $intIsArchived, NULL, false ) );
	}

	public function getIsArchived() {
		return $this->m_intIsArchived;
	}

	public function sqlIsArchived() {
		return ( true == isset( $this->m_intIsArchived ) ) ? ( string ) $this->m_intIsArchived : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, transaction_review_id, transaction_review_log_type_id, old_status_id, new_status_id, review_notes, is_archived, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTransactionReviewId() . ', ' .
 						$this->sqlTransactionReviewLogTypeId() . ', ' .
 						$this->sqlOldStatusId() . ', ' .
 						$this->sqlNewStatusId() . ', ' .
 						$this->sqlReviewNotes() . ', ' .
 						$this->sqlIsArchived() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_review_id = ' . $this->sqlTransactionReviewId() . ','; } elseif( true == array_key_exists( 'TransactionReviewId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_review_id = ' . $this->sqlTransactionReviewId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_review_log_type_id = ' . $this->sqlTransactionReviewLogTypeId() . ','; } elseif( true == array_key_exists( 'TransactionReviewLogTypeId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_review_log_type_id = ' . $this->sqlTransactionReviewLogTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_status_id = ' . $this->sqlOldStatusId() . ','; } elseif( true == array_key_exists( 'OldStatusId', $this->getChangedColumns() ) ) { $strSql .= ' old_status_id = ' . $this->sqlOldStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_status_id = ' . $this->sqlNewStatusId() . ','; } elseif( true == array_key_exists( 'NewStatusId', $this->getChangedColumns() ) ) { $strSql .= ' new_status_id = ' . $this->sqlNewStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_notes = ' . $this->sqlReviewNotes() . ','; } elseif( true == array_key_exists( 'ReviewNotes', $this->getChangedColumns() ) ) { $strSql .= ' review_notes = ' . $this->sqlReviewNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; } elseif( true == array_key_exists( 'IsArchived', $this->getChangedColumns() ) ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'transaction_review_id' => $this->getTransactionReviewId(),
			'transaction_review_log_type_id' => $this->getTransactionReviewLogTypeId(),
			'old_status_id' => $this->getOldStatusId(),
			'new_status_id' => $this->getNewStatusId(),
			'review_notes' => $this->getReviewNotes(),
			'is_archived' => $this->getIsArchived(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>