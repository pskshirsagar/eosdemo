<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageOperandTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningPackageOperandTypes extends CEosPluralBase {

	/**
	 * @return CScreeningPackageOperandType[]
	 */
	public static function fetchScreeningPackageOperandTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageOperandType', $objDatabase );
	}

	/**
	 * @return CScreeningPackageOperandType
	 */
	public static function fetchScreeningPackageOperandType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageOperandType', $objDatabase );
	}

	public static function fetchScreeningPackageOperandTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_operand_types', $objDatabase );
	}

	public static function fetchScreeningPackageOperandTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningPackageOperandType( sprintf( 'SELECT * FROM screening_package_operand_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>