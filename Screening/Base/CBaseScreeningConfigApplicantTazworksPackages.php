<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConfigApplicantTazworksPackages
 * Do not add any new functions to this class.
 */

class CBaseScreeningConfigApplicantTazworksPackages extends CEosPluralBase {

	/**
	 * @return CScreeningConfigApplicantTazworksPackage[]
	 */
	public static function fetchScreeningConfigApplicantTazworksPackages( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningConfigApplicantTazworksPackage', $objDatabase );
	}

	/**
	 * @return CScreeningConfigApplicantTazworksPackage
	 */
	public static function fetchScreeningConfigApplicantTazworksPackage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningConfigApplicantTazworksPackage', $objDatabase );
	}

	public static function fetchScreeningConfigApplicantTazworksPackageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_config_applicant_tazworks_packages', $objDatabase );
	}

	public static function fetchScreeningConfigApplicantTazworksPackageById( $intId, $objDatabase ) {
		return self::fetchScreeningConfigApplicantTazworksPackage( sprintf( 'SELECT * FROM screening_config_applicant_tazworks_packages WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningConfigApplicantTazworksPackagesByScreeningConfigurationId( $intScreeningConfigurationId, $objDatabase ) {
		return self::fetchScreeningConfigApplicantTazworksPackages( sprintf( 'SELECT * FROM screening_config_applicant_tazworks_packages WHERE screening_configuration_id = %d', ( int ) $intScreeningConfigurationId ), $objDatabase );
	}

	public static function fetchScreeningConfigApplicantTazworksPackagesByScreeningApplicantTypeId( $intScreeningApplicantTypeId, $objDatabase ) {
		return self::fetchScreeningConfigApplicantTazworksPackages( sprintf( 'SELECT * FROM screening_config_applicant_tazworks_packages WHERE screening_applicant_type_id = %d', ( int ) $intScreeningApplicantTypeId ), $objDatabase );
	}

}
?>