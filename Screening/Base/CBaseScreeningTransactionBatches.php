<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningTransactionBatches
 * Do not add any new functions to this class.
 */

class CBaseScreeningTransactionBatches extends CEosPluralBase {

	/**
	 * @return CScreeningTransactionBatch[]
	 */
	public static function fetchScreeningTransactionBatches( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningTransactionBatch', $objDatabase );
	}

	/**
	 * @return CScreeningTransactionBatch
	 */
	public static function fetchScreeningTransactionBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningTransactionBatch', $objDatabase );
	}

	public static function fetchScreeningTransactionBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_transaction_batches', $objDatabase );
	}

	public static function fetchScreeningTransactionBatchById( $intId, $objDatabase ) {
		return self::fetchScreeningTransactionBatch( sprintf( 'SELECT * FROM screening_transaction_batches WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningTransactionBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningTransactionBatches( sprintf( 'SELECT * FROM screening_transaction_batches WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningTransactionBatchesByTransactionId( $intTransactionId, $objDatabase ) {
		return self::fetchScreeningTransactionBatches( sprintf( 'SELECT * FROM screening_transaction_batches WHERE transaction_id = %d', ( int ) $intTransactionId ), $objDatabase );
	}

	public static function fetchScreeningTransactionBatchesByScreeningPropertyAccountId( $intScreeningPropertyAccountId, $objDatabase ) {
		return self::fetchScreeningTransactionBatches( sprintf( 'SELECT * FROM screening_transaction_batches WHERE screening_property_account_id = %d', ( int ) $intScreeningPropertyAccountId ), $objDatabase );
	}

	public static function fetchScreeningTransactionBatchesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchScreeningTransactionBatches( sprintf( 'SELECT * FROM screening_transaction_batches WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>