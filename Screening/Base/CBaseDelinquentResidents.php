<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDelinquentResidents
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDelinquentResidents extends CEosPluralBase {

	/**
	 * @return CDelinquentResident[]
	 */
	public static function fetchDelinquentResidents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDelinquentResident', $objDatabase );
	}

	/**
	 * @return CDelinquentResident
	 */
	public static function fetchDelinquentResident( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDelinquentResident', $objDatabase );
	}

	public static function fetchDelinquentResidentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'delinquent_residents', $objDatabase );
	}

	public static function fetchDelinquentResidentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDelinquentResident( sprintf( 'SELECT * FROM delinquent_residents WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDelinquentResidentsByCid( $intCid, $objDatabase ) {
		return self::fetchDelinquentResidents( sprintf( 'SELECT * FROM delinquent_residents WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDelinquentResidentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchDelinquentResidents( sprintf( 'SELECT * FROM delinquent_residents WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDelinquentResidentsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchDelinquentResidents( sprintf( 'SELECT * FROM delinquent_residents WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

}
?>