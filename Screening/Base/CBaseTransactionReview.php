<?php

class CBaseTransactionReview extends CEosSingularBase {

	const TABLE_NAME = 'public.transaction_reviews';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScreeningTransactionId;
	protected $m_intTransactionReviewStatusId;
	protected $m_intTransactionReviewPriorityId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strCompletedOn;
	protected $m_strReviewedOn;
	protected $m_intTransactionReviewQueueTypeId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screening_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningTransactionId', trim( $arrValues['screening_transaction_id'] ) ); elseif( isset( $arrValues['screening_transaction_id'] ) ) $this->setScreeningTransactionId( $arrValues['screening_transaction_id'] );
		if( isset( $arrValues['transaction_review_status_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionReviewStatusId', trim( $arrValues['transaction_review_status_id'] ) ); elseif( isset( $arrValues['transaction_review_status_id'] ) ) $this->setTransactionReviewStatusId( $arrValues['transaction_review_status_id'] );
		if( isset( $arrValues['transaction_review_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionReviewPriorityId', trim( $arrValues['transaction_review_priority_id'] ) ); elseif( isset( $arrValues['transaction_review_priority_id'] ) ) $this->setTransactionReviewPriorityId( $arrValues['transaction_review_priority_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['reviewed_on'] ) && $boolDirectSet ) $this->set( 'm_strReviewedOn', trim( $arrValues['reviewed_on'] ) ); elseif( isset( $arrValues['reviewed_on'] ) ) $this->setReviewedOn( $arrValues['reviewed_on'] );
		if( isset( $arrValues['transaction_review_queue_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionReviewQueueTypeId', trim( $arrValues['transaction_review_queue_type_id'] ) ); elseif( isset( $arrValues['transaction_review_queue_type_id'] ) ) $this->setTransactionReviewQueueTypeId( $arrValues['transaction_review_queue_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreeningTransactionId( $intScreeningTransactionId ) {
		$this->set( 'm_intScreeningTransactionId', CStrings::strToIntDef( $intScreeningTransactionId, NULL, false ) );
	}

	public function getScreeningTransactionId() {
		return $this->m_intScreeningTransactionId;
	}

	public function sqlScreeningTransactionId() {
		return ( true == isset( $this->m_intScreeningTransactionId ) ) ? ( string ) $this->m_intScreeningTransactionId : 'NULL';
	}

	public function setTransactionReviewStatusId( $intTransactionReviewStatusId ) {
		$this->set( 'm_intTransactionReviewStatusId', CStrings::strToIntDef( $intTransactionReviewStatusId, NULL, false ) );
	}

	public function getTransactionReviewStatusId() {
		return $this->m_intTransactionReviewStatusId;
	}

	public function sqlTransactionReviewStatusId() {
		return ( true == isset( $this->m_intTransactionReviewStatusId ) ) ? ( string ) $this->m_intTransactionReviewStatusId : 'NULL';
	}

	public function setTransactionReviewPriorityId( $intTransactionReviewPriorityId ) {
		$this->set( 'm_intTransactionReviewPriorityId', CStrings::strToIntDef( $intTransactionReviewPriorityId, NULL, false ) );
	}

	public function getTransactionReviewPriorityId() {
		return $this->m_intTransactionReviewPriorityId;
	}

	public function sqlTransactionReviewPriorityId() {
		return ( true == isset( $this->m_intTransactionReviewPriorityId ) ) ? ( string ) $this->m_intTransactionReviewPriorityId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setReviewedOn( $strReviewedOn ) {
		$this->set( 'm_strReviewedOn', CStrings::strTrimDef( $strReviewedOn, -1, NULL, true ) );
	}

	public function getReviewedOn() {
		return $this->m_strReviewedOn;
	}

	public function sqlReviewedOn() {
		return ( true == isset( $this->m_strReviewedOn ) ) ? '\'' . $this->m_strReviewedOn . '\'' : 'NULL';
	}

	public function setTransactionReviewQueueTypeId( $intTransactionReviewQueueTypeId ) {
		$this->set( 'm_intTransactionReviewQueueTypeId', CStrings::strToIntDef( $intTransactionReviewQueueTypeId, NULL, false ) );
	}

	public function getTransactionReviewQueueTypeId() {
		return $this->m_intTransactionReviewQueueTypeId;
	}

	public function sqlTransactionReviewQueueTypeId() {
		return ( true == isset( $this->m_intTransactionReviewQueueTypeId ) ) ? ( string ) $this->m_intTransactionReviewQueueTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, screening_transaction_id, transaction_review_status_id, transaction_review_priority_id, updated_by, updated_on, created_by, created_on, completed_on, reviewed_on, transaction_review_queue_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlScreeningTransactionId() . ', ' .
						$this->sqlTransactionReviewStatusId() . ', ' .
						$this->sqlTransactionReviewPriorityId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						$this->sqlReviewedOn() . ', ' .
                        $this->sqlTransactionReviewQueueTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_transaction_id = ' . $this->sqlScreeningTransactionId(). ',' ; } elseif( true == array_key_exists( 'ScreeningTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' screening_transaction_id = ' . $this->sqlScreeningTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_review_status_id = ' . $this->sqlTransactionReviewStatusId(). ',' ; } elseif( true == array_key_exists( 'TransactionReviewStatusId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_review_status_id = ' . $this->sqlTransactionReviewStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_review_priority_id = ' . $this->sqlTransactionReviewPriorityId(). ',' ; } elseif( true == array_key_exists( 'TransactionReviewPriorityId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_review_priority_id = ' . $this->sqlTransactionReviewPriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn(). ',' ; } elseif( true == array_key_exists( 'ReviewedOn', $this->getChangedColumns() ) ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_review_queue_type_id = ' . $this->sqlTransactionReviewQueueTypeId(). ',' ; } elseif( true == array_key_exists( 'TransactionReviewQueueTypeId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_review_queue_type_id = ' . $this->sqlTransactionReviewQueueTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'screening_transaction_id' => $this->getScreeningTransactionId(),
			'transaction_review_status_id' => $this->getTransactionReviewStatusId(),
			'transaction_review_priority_id' => $this->getTransactionReviewPriorityId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'completed_on' => $this->getCompletedOn(),
			'reviewed_on' => $this->getReviewedOn(),
			'transaction_review_queue_type_id' => $this->getTransactionReviewQueueTypeId()
		);
	}

}
?>