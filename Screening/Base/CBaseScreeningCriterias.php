<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCriterias
 * Do not add any new functions to this class.
 */

class CBaseScreeningCriterias extends CEosPluralBase {

	/**
	 * @return CScreeningCriteria[]
	 */
	public static function fetchScreeningCriterias( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningCriteria::class, $objDatabase );
	}

	/**
	 * @return CScreeningCriteria
	 */
	public static function fetchScreeningCriteria( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningCriteria::class, $objDatabase );
	}

	public static function fetchScreeningCriteriaCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_criterias', $objDatabase );
	}

	public static function fetchScreeningCriteriaById( $intId, $objDatabase ) {
		return self::fetchScreeningCriteria( sprintf( 'SELECT * FROM screening_criterias WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningCriteriasByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningCriterias( sprintf( 'SELECT * FROM screening_criterias WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningCriteriasByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreeningCriterias( sprintf( 'SELECT * FROM screening_criterias WHERE screen_type_id = %d', ( int ) $intScreenTypeId ), $objDatabase );
	}

	public static function fetchScreeningCriteriasByParentCriteriaId( $intParentCriteriaId, $objDatabase ) {
		return self::fetchScreeningCriterias( sprintf( 'SELECT * FROM screening_criterias WHERE parent_criteria_id = %d', ( int ) $intParentCriteriaId ), $objDatabase );
	}

	public static function fetchScreeningCriteriasByParentPackageId( $intParentPackageId, $objDatabase ) {
		return self::fetchScreeningCriterias( sprintf( 'SELECT * FROM screening_criterias WHERE parent_package_id = %d', ( int ) $intParentPackageId ), $objDatabase );
	}

	public static function fetchScreeningCriteriasByDefaultScreeningDataProviderId( $intDefaultScreeningDataProviderId, $objDatabase ) {
		return self::fetchScreeningCriterias( sprintf( 'SELECT * FROM screening_criterias WHERE default_screening_data_provider_id = %d', ( int ) $intDefaultScreeningDataProviderId ), $objDatabase );
	}

}
?>