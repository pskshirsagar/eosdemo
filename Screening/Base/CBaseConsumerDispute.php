<?php

class CBaseConsumerDispute extends CEosSingularBase {

	const TABLE_NAME = 'public.consumer_disputes';

	protected $m_intId;
	protected $m_intConsumerId;
	protected $m_intDisputeParentId;
	protected $m_intDisputeTypeId;
	protected $m_intDisputeTypeReasonId;
	protected $m_intDisputeStatusTypeId;
	protected $m_strProviderReferenceNumber;
	protected $m_intDisputeReferenceNumber;
	protected $m_intDataSourceId;
	protected $m_intAssignedTo;
	protected $m_strCompletionNotes;
	protected $m_intCompletedBy;
	protected $m_strCompletedOn;
	protected $m_strConsumerStatementRequest;
	protected $m_strConsumerStatement;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsCompletionDueDateExtended;
	protected $m_strExtendCompletionDueDateReason;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsCompletionDueDateExtended = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['consumer_id'] ) && $boolDirectSet ) $this->set( 'm_intConsumerId', trim( $arrValues['consumer_id'] ) ); elseif( isset( $arrValues['consumer_id'] ) ) $this->setConsumerId( $arrValues['consumer_id'] );
		if( isset( $arrValues['dispute_parent_id'] ) && $boolDirectSet ) $this->set( 'm_intDisputeParentId', trim( $arrValues['dispute_parent_id'] ) ); elseif( isset( $arrValues['dispute_parent_id'] ) ) $this->setDisputeParentId( $arrValues['dispute_parent_id'] );
		if( isset( $arrValues['dispute_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDisputeTypeId', trim( $arrValues['dispute_type_id'] ) ); elseif( isset( $arrValues['dispute_type_id'] ) ) $this->setDisputeTypeId( $arrValues['dispute_type_id'] );
		if( isset( $arrValues['dispute_type_reason_id'] ) && $boolDirectSet ) $this->set( 'm_intDisputeTypeReasonId', trim( $arrValues['dispute_type_reason_id'] ) ); elseif( isset( $arrValues['dispute_type_reason_id'] ) ) $this->setDisputeTypeReasonId( $arrValues['dispute_type_reason_id'] );
		if( isset( $arrValues['dispute_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDisputeStatusTypeId', trim( $arrValues['dispute_status_type_id'] ) ); elseif( isset( $arrValues['dispute_status_type_id'] ) ) $this->setDisputeStatusTypeId( $arrValues['dispute_status_type_id'] );
		if( isset( $arrValues['provider_reference_number'] ) && $boolDirectSet ) $this->set( 'm_strProviderReferenceNumber', trim( stripcslashes( $arrValues['provider_reference_number'] ) ) ); elseif( isset( $arrValues['provider_reference_number'] ) ) $this->setProviderReferenceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['provider_reference_number'] ) : $arrValues['provider_reference_number'] );
		if( isset( $arrValues['dispute_reference_number'] ) && $boolDirectSet ) $this->set( 'm_intDisputeReferenceNumber', trim( $arrValues['dispute_reference_number'] ) ); elseif( isset( $arrValues['dispute_reference_number'] ) ) $this->setDisputeReferenceNumber( $arrValues['dispute_reference_number'] );
		if( isset( $arrValues['data_source_id'] ) && $boolDirectSet ) $this->set( 'm_intDataSourceId', trim( $arrValues['data_source_id'] ) ); elseif( isset( $arrValues['data_source_id'] ) ) $this->setDataSourceId( $arrValues['data_source_id'] );
		if( isset( $arrValues['assigned_to'] ) && $boolDirectSet ) $this->set( 'm_intAssignedTo', trim( $arrValues['assigned_to'] ) ); elseif( isset( $arrValues['assigned_to'] ) ) $this->setAssignedTo( $arrValues['assigned_to'] );
		if( isset( $arrValues['completion_notes'] ) && $boolDirectSet ) $this->set( 'm_strCompletionNotes', trim( stripcslashes( $arrValues['completion_notes'] ) ) ); elseif( isset( $arrValues['completion_notes'] ) ) $this->setCompletionNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['completion_notes'] ) : $arrValues['completion_notes'] );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['consumer_statement_request'] ) && $boolDirectSet ) $this->set( 'm_strConsumerStatementRequest', trim( stripcslashes( $arrValues['consumer_statement_request'] ) ) ); elseif( isset( $arrValues['consumer_statement_request'] ) ) $this->setConsumerStatementRequest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['consumer_statement_request'] ) : $arrValues['consumer_statement_request'] );
		if( isset( $arrValues['consumer_statement'] ) && $boolDirectSet ) $this->set( 'm_strConsumerStatement', trim( stripcslashes( $arrValues['consumer_statement'] ) ) ); elseif( isset( $arrValues['consumer_statement'] ) ) $this->setConsumerStatement( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['consumer_statement'] ) : $arrValues['consumer_statement'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_completion_due_date_extended'] ) && $boolDirectSet ) $this->set( 'm_boolIsCompletionDueDateExtended', trim( stripcslashes( $arrValues['is_completion_due_date_extended'] ) ) ); elseif( isset( $arrValues['is_completion_due_date_extended'] ) ) $this->setIsCompletionDueDateExtended( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_completion_due_date_extended'] ) : $arrValues['is_completion_due_date_extended'] );
		if( isset( $arrValues['extend_completion_due_date_reason'] ) && $boolDirectSet ) $this->set( 'm_strExtendCompletionDueDateReason', trim( stripcslashes( $arrValues['extend_completion_due_date_reason'] ) ) ); elseif( isset( $arrValues['extend_completion_due_date_reason'] ) ) $this->setExtendCompletionDueDateReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['extend_completion_due_date_reason'] ) : $arrValues['extend_completion_due_date_reason'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setConsumerId( $intConsumerId ) {
		$this->set( 'm_intConsumerId', CStrings::strToIntDef( $intConsumerId, NULL, false ) );
	}

	public function getConsumerId() {
		return $this->m_intConsumerId;
	}

	public function sqlConsumerId() {
		return ( true == isset( $this->m_intConsumerId ) ) ? ( string ) $this->m_intConsumerId : 'NULL';
	}

	public function setDisputeParentId( $intDisputeParentId ) {
		$this->set( 'm_intDisputeParentId', CStrings::strToIntDef( $intDisputeParentId, NULL, false ) );
	}

	public function getDisputeParentId() {
		return $this->m_intDisputeParentId;
	}

	public function sqlDisputeParentId() {
		return ( true == isset( $this->m_intDisputeParentId ) ) ? ( string ) $this->m_intDisputeParentId : 'NULL';
	}

	public function setDisputeTypeId( $intDisputeTypeId ) {
		$this->set( 'm_intDisputeTypeId', CStrings::strToIntDef( $intDisputeTypeId, NULL, false ) );
	}

	public function getDisputeTypeId() {
		return $this->m_intDisputeTypeId;
	}

	public function sqlDisputeTypeId() {
		return ( true == isset( $this->m_intDisputeTypeId ) ) ? ( string ) $this->m_intDisputeTypeId : 'NULL';
	}

	public function setDisputeTypeReasonId( $intDisputeTypeReasonId ) {
		$this->set( 'm_intDisputeTypeReasonId', CStrings::strToIntDef( $intDisputeTypeReasonId, NULL, false ) );
	}

	public function getDisputeTypeReasonId() {
		return $this->m_intDisputeTypeReasonId;
	}

	public function sqlDisputeTypeReasonId() {
		return ( true == isset( $this->m_intDisputeTypeReasonId ) ) ? ( string ) $this->m_intDisputeTypeReasonId : 'NULL';
	}

	public function setDisputeStatusTypeId( $intDisputeStatusTypeId ) {
		$this->set( 'm_intDisputeStatusTypeId', CStrings::strToIntDef( $intDisputeStatusTypeId, NULL, false ) );
	}

	public function getDisputeStatusTypeId() {
		return $this->m_intDisputeStatusTypeId;
	}

	public function sqlDisputeStatusTypeId() {
		return ( true == isset( $this->m_intDisputeStatusTypeId ) ) ? ( string ) $this->m_intDisputeStatusTypeId : 'NULL';
	}

	public function setProviderReferenceNumber( $strProviderReferenceNumber ) {
		$this->set( 'm_strProviderReferenceNumber', CStrings::strTrimDef( $strProviderReferenceNumber, 50, NULL, true ) );
	}

	public function getProviderReferenceNumber() {
		return $this->m_strProviderReferenceNumber;
	}

	public function sqlProviderReferenceNumber() {
		return ( true == isset( $this->m_strProviderReferenceNumber ) ) ? '\'' . addslashes( $this->m_strProviderReferenceNumber ) . '\'' : 'NULL';
	}

	public function setDisputeReferenceNumber( $intDisputeReferenceNumber ) {
		$this->set( 'm_intDisputeReferenceNumber', CStrings::strToIntDef( $intDisputeReferenceNumber, NULL, false ) );
	}

	public function getDisputeReferenceNumber() {
		return $this->m_intDisputeReferenceNumber;
	}

	public function sqlDisputeReferenceNumber() {
		return ( true == isset( $this->m_intDisputeReferenceNumber ) ) ? ( string ) $this->m_intDisputeReferenceNumber : 'NULL';
	}

	public function setDataSourceId( $intDataSourceId ) {
		$this->set( 'm_intDataSourceId', CStrings::strToIntDef( $intDataSourceId, NULL, false ) );
	}

	public function getDataSourceId() {
		return $this->m_intDataSourceId;
	}

	public function sqlDataSourceId() {
		return ( true == isset( $this->m_intDataSourceId ) ) ? ( string ) $this->m_intDataSourceId : 'NULL';
	}

	public function setAssignedTo( $intAssignedTo ) {
		$this->set( 'm_intAssignedTo', CStrings::strToIntDef( $intAssignedTo, NULL, false ) );
	}

	public function getAssignedTo() {
		return $this->m_intAssignedTo;
	}

	public function sqlAssignedTo() {
		return ( true == isset( $this->m_intAssignedTo ) ) ? ( string ) $this->m_intAssignedTo : 'NULL';
	}

	public function setCompletionNotes( $strCompletionNotes ) {
		$this->set( 'm_strCompletionNotes', CStrings::strTrimDef( $strCompletionNotes, -1, NULL, true ) );
	}

	public function getCompletionNotes() {
		return $this->m_strCompletionNotes;
	}

	public function sqlCompletionNotes() {
		return ( true == isset( $this->m_strCompletionNotes ) ) ? '\'' . addslashes( $this->m_strCompletionNotes ) . '\'' : 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setConsumerStatementRequest( $strConsumerStatementRequest ) {
		$this->set( 'm_strConsumerStatementRequest', CStrings::strTrimDef( $strConsumerStatementRequest, -1, NULL, true ) );
	}

	public function getConsumerStatementRequest() {
		return $this->m_strConsumerStatementRequest;
	}

	public function sqlConsumerStatementRequest() {
		return ( true == isset( $this->m_strConsumerStatementRequest ) ) ? '\'' . addslashes( $this->m_strConsumerStatementRequest ) . '\'' : 'NULL';
	}

	public function setConsumerStatement( $strConsumerStatement ) {
		$this->set( 'm_strConsumerStatement', CStrings::strTrimDef( $strConsumerStatement, -1, NULL, true ) );
	}

	public function getConsumerStatement() {
		return $this->m_strConsumerStatement;
	}

	public function sqlConsumerStatement() {
		return ( true == isset( $this->m_strConsumerStatement ) ) ? '\'' . addslashes( $this->m_strConsumerStatement ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsCompletionDueDateExtended( $boolIsCompletionDueDateExtended ) {
		$this->set( 'm_boolIsCompletionDueDateExtended', CStrings::strToBool( $boolIsCompletionDueDateExtended ) );
	}

	public function getIsCompletionDueDateExtended() {
		return $this->m_boolIsCompletionDueDateExtended;
	}

	public function sqlIsCompletionDueDateExtended() {
		return ( true == isset( $this->m_boolIsCompletionDueDateExtended ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCompletionDueDateExtended ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setExtendCompletionDueDateReason( $strExtendCompletionDueDateReason ) {
		$this->set( 'm_strExtendCompletionDueDateReason', CStrings::strTrimDef( $strExtendCompletionDueDateReason, 200, NULL, true ) );
	}

	public function getExtendCompletionDueDateReason() {
		return $this->m_strExtendCompletionDueDateReason;
	}

	public function sqlExtendCompletionDueDateReason() {
		return ( true == isset( $this->m_strExtendCompletionDueDateReason ) ) ? '\'' . addslashes( $this->m_strExtendCompletionDueDateReason ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, consumer_id, dispute_parent_id, dispute_type_id, dispute_type_reason_id, dispute_status_type_id, provider_reference_number, dispute_reference_number, data_source_id, assigned_to, completion_notes, completed_by, completed_on, consumer_statement_request, consumer_statement, updated_by, updated_on, created_by, created_on, is_completion_due_date_extended, extend_completion_due_date_reason )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlConsumerId() . ', ' .
 						$this->sqlDisputeParentId() . ', ' .
 						$this->sqlDisputeTypeId() . ', ' .
 						$this->sqlDisputeTypeReasonId() . ', ' .
 						$this->sqlDisputeStatusTypeId() . ', ' .
 						$this->sqlProviderReferenceNumber() . ', ' .
 						$this->sqlDisputeReferenceNumber() . ', ' .
 						$this->sqlDataSourceId() . ', ' .
 						$this->sqlAssignedTo() . ', ' .
 						$this->sqlCompletionNotes() . ', ' .
 						$this->sqlCompletedBy() . ', ' .
 						$this->sqlCompletedOn() . ', ' .
 						$this->sqlConsumerStatementRequest() . ', ' .
 						$this->sqlConsumerStatement() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlIsCompletionDueDateExtended() . ', ' .
 						$this->sqlExtendCompletionDueDateReason() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumer_id = ' . $this->sqlConsumerId() . ','; } elseif( true == array_key_exists( 'ConsumerId', $this->getChangedColumns() ) ) { $strSql .= ' consumer_id = ' . $this->sqlConsumerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dispute_parent_id = ' . $this->sqlDisputeParentId() . ','; } elseif( true == array_key_exists( 'DisputeParentId', $this->getChangedColumns() ) ) { $strSql .= ' dispute_parent_id = ' . $this->sqlDisputeParentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dispute_type_id = ' . $this->sqlDisputeTypeId() . ','; } elseif( true == array_key_exists( 'DisputeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' dispute_type_id = ' . $this->sqlDisputeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dispute_type_reason_id = ' . $this->sqlDisputeTypeReasonId() . ','; } elseif( true == array_key_exists( 'DisputeTypeReasonId', $this->getChangedColumns() ) ) { $strSql .= ' dispute_type_reason_id = ' . $this->sqlDisputeTypeReasonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dispute_status_type_id = ' . $this->sqlDisputeStatusTypeId() . ','; } elseif( true == array_key_exists( 'DisputeStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' dispute_status_type_id = ' . $this->sqlDisputeStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' provider_reference_number = ' . $this->sqlProviderReferenceNumber() . ','; } elseif( true == array_key_exists( 'ProviderReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' provider_reference_number = ' . $this->sqlProviderReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dispute_reference_number = ' . $this->sqlDisputeReferenceNumber() . ','; } elseif( true == array_key_exists( 'DisputeReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' dispute_reference_number = ' . $this->sqlDisputeReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_source_id = ' . $this->sqlDataSourceId() . ','; } elseif( true == array_key_exists( 'DataSourceId', $this->getChangedColumns() ) ) { $strSql .= ' data_source_id = ' . $this->sqlDataSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assigned_to = ' . $this->sqlAssignedTo() . ','; } elseif( true == array_key_exists( 'AssignedTo', $this->getChangedColumns() ) ) { $strSql .= ' assigned_to = ' . $this->sqlAssignedTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completion_notes = ' . $this->sqlCompletionNotes() . ','; } elseif( true == array_key_exists( 'CompletionNotes', $this->getChangedColumns() ) ) { $strSql .= ' completion_notes = ' . $this->sqlCompletionNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumer_statement_request = ' . $this->sqlConsumerStatementRequest() . ','; } elseif( true == array_key_exists( 'ConsumerStatementRequest', $this->getChangedColumns() ) ) { $strSql .= ' consumer_statement_request = ' . $this->sqlConsumerStatementRequest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumer_statement = ' . $this->sqlConsumerStatement() . ','; } elseif( true == array_key_exists( 'ConsumerStatement', $this->getChangedColumns() ) ) { $strSql .= ' consumer_statement = ' . $this->sqlConsumerStatement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_completion_due_date_extended = ' . $this->sqlIsCompletionDueDateExtended() . ','; } elseif( true == array_key_exists( 'IsCompletionDueDateExtended', $this->getChangedColumns() ) ) { $strSql .= ' is_completion_due_date_extended = ' . $this->sqlIsCompletionDueDateExtended() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' extend_completion_due_date_reason = ' . $this->sqlExtendCompletionDueDateReason() . ','; } elseif( true == array_key_exists( 'ExtendCompletionDueDateReason', $this->getChangedColumns() ) ) { $strSql .= ' extend_completion_due_date_reason = ' . $this->sqlExtendCompletionDueDateReason() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'consumer_id' => $this->getConsumerId(),
			'dispute_parent_id' => $this->getDisputeParentId(),
			'dispute_type_id' => $this->getDisputeTypeId(),
			'dispute_type_reason_id' => $this->getDisputeTypeReasonId(),
			'dispute_status_type_id' => $this->getDisputeStatusTypeId(),
			'provider_reference_number' => $this->getProviderReferenceNumber(),
			'dispute_reference_number' => $this->getDisputeReferenceNumber(),
			'data_source_id' => $this->getDataSourceId(),
			'assigned_to' => $this->getAssignedTo(),
			'completion_notes' => $this->getCompletionNotes(),
			'completed_by' => $this->getCompletedBy(),
			'completed_on' => $this->getCompletedOn(),
			'consumer_statement_request' => $this->getConsumerStatementRequest(),
			'consumer_statement' => $this->getConsumerStatement(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_completion_due_date_extended' => $this->getIsCompletionDueDateExtended(),
			'extend_completion_due_date_reason' => $this->getExtendCompletionDueDateReason()
		);
	}

}
?>