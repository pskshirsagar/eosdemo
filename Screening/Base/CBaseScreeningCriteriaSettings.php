<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCriteriaSettings
 * Do not add any new functions to this class.
 */

class CBaseScreeningCriteriaSettings extends CEosPluralBase {

	/**
	 * @return CScreeningCriteriaSetting[]
	 */
	public static function fetchScreeningCriteriaSettings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningCriteriaSetting::class, $objDatabase );
	}

	/**
	 * @return CScreeningCriteriaSetting
	 */
	public static function fetchScreeningCriteriaSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningCriteriaSetting::class, $objDatabase );
	}

	public static function fetchScreeningCriteriaSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_criteria_settings', $objDatabase );
	}

	public static function fetchScreeningCriteriaSettingById( $intId, $objDatabase ) {
		return self::fetchScreeningCriteriaSetting( sprintf( 'SELECT * FROM screening_criteria_settings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningCriteriaSettingsByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {
		return self::fetchScreeningCriteriaSettings( sprintf( 'SELECT * FROM screening_criteria_settings WHERE screening_criteria_id = %d', ( int ) $intScreeningCriteriaId ), $objDatabase );
	}

	public static function fetchScreeningCriteriaSettingsByScreeningConditionSetId( $intScreeningConditionSetId, $objDatabase ) {
		return self::fetchScreeningCriteriaSettings( sprintf( 'SELECT * FROM screening_criteria_settings WHERE screening_condition_set_id = %d', ( int ) $intScreeningConditionSetId ), $objDatabase );
	}

	public static function fetchScreeningCriteriaSettingsByScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		return self::fetchScreeningCriteriaSettings( sprintf( 'SELECT * FROM screening_criteria_settings WHERE screening_config_preference_type_id = %d', ( int ) $intScreeningConfigPreferenceTypeId ), $objDatabase );
	}

}
?>