<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningApplicant extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.screening_applicants';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScreeningId;
	protected $m_intCustomApplicantId;
	protected $m_intScreeningApplicantTypeId;
	protected $m_intApplicantApplicationTransmissionId;
	protected $m_intScreeningPackageId;
	protected $m_intLeadSourceId;
	protected $m_intGuarantorForApplicationId;
	protected $m_intStatusTypeId;
	protected $m_intOriginalScreeningRecommendationTypeId;
	protected $m_intScreeningRecommendationTypeId;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strNameMiddle;
	protected $m_strTaxNumberLastFour;
	protected $m_strTaxNumberEncrypted;
	protected $m_strBirthDateYear;
	protected $m_strBirthDateEncrypted;
	protected $m_strDriverLicense;
	protected $m_strDriverLicenseState;
	protected $m_strEmployerName;
	protected $m_fltIncome;
	protected $m_strAssets;
	protected $m_strAddressLine;
	protected $m_strCity;
	protected $m_strState;
	protected $m_strZipcode;
	protected $m_strEmailAddress;
	protected $m_strTelephone;
	protected $m_strScreeningCompletedOn;
	protected $m_boolIsAlien;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_fltRvIndexScore;

	public function __construct() {
		parent::__construct();

		$this->m_intStatusTypeId = '0';
		$this->m_boolIsAlien = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screening_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningId', trim( $arrValues['screening_id'] ) ); elseif( isset( $arrValues['screening_id'] ) ) $this->setScreeningId( $arrValues['screening_id'] );
		if( isset( $arrValues['custom_applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomApplicantId', trim( $arrValues['custom_applicant_id'] ) ); elseif( isset( $arrValues['custom_applicant_id'] ) ) $this->setCustomApplicantId( $arrValues['custom_applicant_id'] );
		if( isset( $arrValues['screening_applicant_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicantTypeId', trim( $arrValues['screening_applicant_type_id'] ) ); elseif( isset( $arrValues['screening_applicant_type_id'] ) ) $this->setScreeningApplicantTypeId( $arrValues['screening_applicant_type_id'] );
		if( isset( $arrValues['applicant_application_transmission_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantApplicationTransmissionId', trim( $arrValues['applicant_application_transmission_id'] ) ); elseif( isset( $arrValues['applicant_application_transmission_id'] ) ) $this->setApplicantApplicationTransmissionId( $arrValues['applicant_application_transmission_id'] );
		if( isset( $arrValues['screening_package_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageId', trim( $arrValues['screening_package_id'] ) ); elseif( isset( $arrValues['screening_package_id'] ) ) $this->setScreeningPackageId( $arrValues['screening_package_id'] );
		if( isset( $arrValues['lead_source_id'] ) && $boolDirectSet ) $this->set( 'm_intLeadSourceId', trim( $arrValues['lead_source_id'] ) ); elseif( isset( $arrValues['lead_source_id'] ) ) $this->setLeadSourceId( $arrValues['lead_source_id'] );
		if( isset( $arrValues['guarantor_for_application_id'] ) && $boolDirectSet ) $this->set( 'm_intGuarantorForApplicationId', trim( $arrValues['guarantor_for_application_id'] ) ); elseif( isset( $arrValues['guarantor_for_application_id'] ) ) $this->setGuarantorForApplicationId( $arrValues['guarantor_for_application_id'] );
		if( isset( $arrValues['status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intStatusTypeId', trim( $arrValues['status_type_id'] ) ); elseif( isset( $arrValues['status_type_id'] ) ) $this->setStatusTypeId( $arrValues['status_type_id'] );
		if( isset( $arrValues['original_screening_recommendation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalScreeningRecommendationTypeId', trim( $arrValues['original_screening_recommendation_type_id'] ) ); elseif( isset( $arrValues['original_screening_recommendation_type_id'] ) ) $this->setOriginalScreeningRecommendationTypeId( $arrValues['original_screening_recommendation_type_id'] );
		if( isset( $arrValues['screening_recommendation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningRecommendationTypeId', trim( $arrValues['screening_recommendation_type_id'] ) ); elseif( isset( $arrValues['screening_recommendation_type_id'] ) ) $this->setScreeningRecommendationTypeId( $arrValues['screening_recommendation_type_id'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( $arrValues['name_middle'] ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( $arrValues['name_middle'] );
		if( isset( $arrValues['tax_number_last_four'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberLastFour', trim( $arrValues['tax_number_last_four'] ) ); elseif( isset( $arrValues['tax_number_last_four'] ) ) $this->setTaxNumberLastFour( $arrValues['tax_number_last_four'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( $arrValues['tax_number_encrypted'] ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['birth_date_year'] ) && $boolDirectSet ) $this->set( 'm_strBirthDateYear', trim( $arrValues['birth_date_year'] ) ); elseif( isset( $arrValues['birth_date_year'] ) ) $this->setBirthDateYear( $arrValues['birth_date_year'] );
		if( isset( $arrValues['birth_date_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBirthDateEncrypted', trim( $arrValues['birth_date_encrypted'] ) ); elseif( isset( $arrValues['birth_date_encrypted'] ) ) $this->setBirthDateEncrypted( $arrValues['birth_date_encrypted'] );
		if( isset( $arrValues['driver_license'] ) && $boolDirectSet ) $this->set( 'm_strDriverLicense', trim( $arrValues['driver_license'] ) ); elseif( isset( $arrValues['driver_license'] ) ) $this->setDriverLicense( $arrValues['driver_license'] );
		if( isset( $arrValues['driver_license_state'] ) && $boolDirectSet ) $this->set( 'm_strDriverLicenseState', trim( $arrValues['driver_license_state'] ) ); elseif( isset( $arrValues['driver_license_state'] ) ) $this->setDriverLicenseState( $arrValues['driver_license_state'] );
		if( isset( $arrValues['employer_name'] ) && $boolDirectSet ) $this->set( 'm_strEmployerName', trim( $arrValues['employer_name'] ) ); elseif( isset( $arrValues['employer_name'] ) ) $this->setEmployerName( $arrValues['employer_name'] );
		if( isset( $arrValues['income'] ) && $boolDirectSet ) $this->set( 'm_fltIncome', trim( $arrValues['income'] ) ); elseif( isset( $arrValues['income'] ) ) $this->setIncome( $arrValues['income'] );
		if( isset( $arrValues['assets'] ) && $boolDirectSet ) $this->set( 'm_strAssets', trim( $arrValues['assets'] ) ); elseif( isset( $arrValues['assets'] ) ) $this->setAssets( $arrValues['assets'] );
		if( isset( $arrValues['address_line'] ) && $boolDirectSet ) $this->set( 'm_strAddressLine', trim( $arrValues['address_line'] ) ); elseif( isset( $arrValues['address_line'] ) ) $this->setAddressLine( $arrValues['address_line'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( $arrValues['city'] ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( $arrValues['city'] );
		if( isset( $arrValues['state'] ) && $boolDirectSet ) $this->set( 'm_strState', trim( $arrValues['state'] ) ); elseif( isset( $arrValues['state'] ) ) $this->setState( $arrValues['state'] );
		if( isset( $arrValues['zipcode'] ) && $boolDirectSet ) $this->set( 'm_strZipcode', trim( $arrValues['zipcode'] ) ); elseif( isset( $arrValues['zipcode'] ) ) $this->setZipcode( $arrValues['zipcode'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['telephone'] ) && $boolDirectSet ) $this->set( 'm_strTelephone', trim( $arrValues['telephone'] ) ); elseif( isset( $arrValues['telephone'] ) ) $this->setTelephone( $arrValues['telephone'] );
		if( isset( $arrValues['screening_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strScreeningCompletedOn', trim( $arrValues['screening_completed_on'] ) ); elseif( isset( $arrValues['screening_completed_on'] ) ) $this->setScreeningCompletedOn( $arrValues['screening_completed_on'] );
		if( isset( $arrValues['is_alien'] ) && $boolDirectSet ) $this->set( 'm_boolIsAlien', trim( stripcslashes( $arrValues['is_alien'] ) ) ); elseif( isset( $arrValues['is_alien'] ) ) $this->setIsAlien( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_alien'] ) : $arrValues['is_alien'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['rv_index_score'] ) && $boolDirectSet ) $this->set( 'm_fltRvIndexScore', trim( $arrValues['rv_index_score'] ) ); elseif( isset( $arrValues['rv_index_score'] ) ) $this->setRvIndexScore( $arrValues['rv_index_score'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreeningId( $intScreeningId ) {
		$this->set( 'm_intScreeningId', CStrings::strToIntDef( $intScreeningId, NULL, false ) );
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function sqlScreeningId() {
		return ( true == isset( $this->m_intScreeningId ) ) ? ( string ) $this->m_intScreeningId : 'NULL';
	}

	public function setCustomApplicantId( $intCustomApplicantId ) {
		$this->set( 'm_intCustomApplicantId', CStrings::strToIntDef( $intCustomApplicantId, NULL, false ) );
	}

	public function getCustomApplicantId() {
		return $this->m_intCustomApplicantId;
	}

	public function sqlCustomApplicantId() {
		return ( true == isset( $this->m_intCustomApplicantId ) ) ? ( string ) $this->m_intCustomApplicantId : 'NULL';
	}

	public function setScreeningApplicantTypeId( $intScreeningApplicantTypeId ) {
		$this->set( 'm_intScreeningApplicantTypeId', CStrings::strToIntDef( $intScreeningApplicantTypeId, NULL, false ) );
	}

	public function getScreeningApplicantTypeId() {
		return $this->m_intScreeningApplicantTypeId;
	}

	public function sqlScreeningApplicantTypeId() {
		return ( true == isset( $this->m_intScreeningApplicantTypeId ) ) ? ( string ) $this->m_intScreeningApplicantTypeId : 'NULL';
	}

	public function setApplicantApplicationTransmissionId( $intApplicantApplicationTransmissionId ) {
		$this->set( 'm_intApplicantApplicationTransmissionId', CStrings::strToIntDef( $intApplicantApplicationTransmissionId, NULL, false ) );
	}

	public function getApplicantApplicationTransmissionId() {
		return $this->m_intApplicantApplicationTransmissionId;
	}

	public function sqlApplicantApplicationTransmissionId() {
		return ( true == isset( $this->m_intApplicantApplicationTransmissionId ) ) ? ( string ) $this->m_intApplicantApplicationTransmissionId : 'NULL';
	}

	public function setScreeningPackageId( $intScreeningPackageId ) {
		$this->set( 'm_intScreeningPackageId', CStrings::strToIntDef( $intScreeningPackageId, NULL, false ) );
	}

	public function getScreeningPackageId() {
		return $this->m_intScreeningPackageId;
	}

	public function sqlScreeningPackageId() {
		return ( true == isset( $this->m_intScreeningPackageId ) ) ? ( string ) $this->m_intScreeningPackageId : 'NULL';
	}

	public function setLeadSourceId( $intLeadSourceId ) {
		$this->set( 'm_intLeadSourceId', CStrings::strToIntDef( $intLeadSourceId, NULL, false ) );
	}

	public function getLeadSourceId() {
		return $this->m_intLeadSourceId;
	}

	public function sqlLeadSourceId() {
		return ( true == isset( $this->m_intLeadSourceId ) ) ? ( string ) $this->m_intLeadSourceId : 'NULL';
	}

	public function setGuarantorForApplicationId( $intGuarantorForApplicationId ) {
		$this->set( 'm_intGuarantorForApplicationId', CStrings::strToIntDef( $intGuarantorForApplicationId, NULL, false ) );
	}

	public function getGuarantorForApplicationId() {
		return $this->m_intGuarantorForApplicationId;
	}

	public function sqlGuarantorForApplicationId() {
		return ( true == isset( $this->m_intGuarantorForApplicationId ) ) ? ( string ) $this->m_intGuarantorForApplicationId : 'NULL';
	}

	public function setStatusTypeId( $intStatusTypeId ) {
		$this->set( 'm_intStatusTypeId', CStrings::strToIntDef( $intStatusTypeId, NULL, false ) );
	}

	public function getStatusTypeId() {
		return $this->m_intStatusTypeId;
	}

	public function sqlStatusTypeId() {
		return ( true == isset( $this->m_intStatusTypeId ) ) ? ( string ) $this->m_intStatusTypeId : '0';
	}

	public function setOriginalScreeningRecommendationTypeId( $intOriginalScreeningRecommendationTypeId ) {
		$this->set( 'm_intOriginalScreeningRecommendationTypeId', CStrings::strToIntDef( $intOriginalScreeningRecommendationTypeId, NULL, false ) );
	}

	public function getOriginalScreeningRecommendationTypeId() {
		return $this->m_intOriginalScreeningRecommendationTypeId;
	}

	public function sqlOriginalScreeningRecommendationTypeId() {
		return ( true == isset( $this->m_intOriginalScreeningRecommendationTypeId ) ) ? ( string ) $this->m_intOriginalScreeningRecommendationTypeId : 'NULL';
	}

	public function setScreeningRecommendationTypeId( $intScreeningRecommendationTypeId ) {
		$this->set( 'm_intScreeningRecommendationTypeId', CStrings::strToIntDef( $intScreeningRecommendationTypeId, NULL, false ) );
	}

	public function getScreeningRecommendationTypeId() {
		return $this->m_intScreeningRecommendationTypeId;
	}

	public function sqlScreeningRecommendationTypeId() {
		return ( true == isset( $this->m_intScreeningRecommendationTypeId ) ) ? ( string ) $this->m_intScreeningRecommendationTypeId : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 50, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 50, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMiddle ) : '\'' . addslashes( $this->m_strNameMiddle ) . '\'' ) : 'NULL';
	}

	public function setTaxNumberLastFour( $strTaxNumberLastFour ) {
		$this->set( 'm_strTaxNumberLastFour', CStrings::strTrimDef( $strTaxNumberLastFour, 10, NULL, true ) );
	}

	public function getTaxNumberLastFour() {
		return $this->m_strTaxNumberLastFour;
	}

	public function sqlTaxNumberLastFour() {
		return ( true == isset( $this->m_strTaxNumberLastFour ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberLastFour ) : '\'' . addslashes( $this->m_strTaxNumberLastFour ) . '\'' ) : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 255, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberEncrypted ) : '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setBirthDateYear( $strBirthDateYear ) {
		$this->set( 'm_strBirthDateYear', CStrings::strTrimDef( $strBirthDateYear, 4, NULL, true ) );
	}

	public function getBirthDateYear() {
		return $this->m_strBirthDateYear;
	}

	public function sqlBirthDateYear() {
		return ( true == isset( $this->m_strBirthDateYear ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBirthDateYear ) : '\'' . addslashes( $this->m_strBirthDateYear ) . '\'' ) : 'NULL';
	}

	public function setBirthDateEncrypted( $strBirthDateEncrypted ) {
		$this->set( 'm_strBirthDateEncrypted', CStrings::strTrimDef( $strBirthDateEncrypted, 255, NULL, true ) );
	}

	public function getBirthDateEncrypted() {
		return $this->m_strBirthDateEncrypted;
	}

	public function sqlBirthDateEncrypted() {
		return ( true == isset( $this->m_strBirthDateEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBirthDateEncrypted ) : '\'' . addslashes( $this->m_strBirthDateEncrypted ) . '\'' ) : 'NULL';
	}

	public function setDriverLicense( $strDriverLicense ) {
		$this->set( 'm_strDriverLicense', CStrings::strTrimDef( $strDriverLicense, 50, NULL, true ) );
	}

	public function getDriverLicense() {
		return $this->m_strDriverLicense;
	}

	public function sqlDriverLicense() {
		return ( true == isset( $this->m_strDriverLicense ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDriverLicense ) : '\'' . addslashes( $this->m_strDriverLicense ) . '\'' ) : 'NULL';
	}

	public function setDriverLicenseState( $strDriverLicenseState ) {
		$this->set( 'm_strDriverLicenseState', CStrings::strTrimDef( $strDriverLicenseState, 20, NULL, true ) );
	}

	public function getDriverLicenseState() {
		return $this->m_strDriverLicenseState;
	}

	public function sqlDriverLicenseState() {
		return ( true == isset( $this->m_strDriverLicenseState ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDriverLicenseState ) : '\'' . addslashes( $this->m_strDriverLicenseState ) . '\'' ) : 'NULL';
	}

	public function setEmployerName( $strEmployerName ) {
		$this->set( 'm_strEmployerName', CStrings::strTrimDef( $strEmployerName, 50, NULL, true ) );
	}

	public function getEmployerName() {
		return $this->m_strEmployerName;
	}

	public function sqlEmployerName() {
		return ( true == isset( $this->m_strEmployerName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmployerName ) : '\'' . addslashes( $this->m_strEmployerName ) . '\'' ) : 'NULL';
	}

	public function setIncome( $fltIncome ) {
		$this->set( 'm_fltIncome', CStrings::strToFloatDef( $fltIncome, NULL, false, 4 ) );
	}

	public function getIncome() {
		return $this->m_fltIncome;
	}

	public function sqlIncome() {
		return ( true == isset( $this->m_fltIncome ) ) ? ( string ) $this->m_fltIncome : 'NULL';
	}

	public function setAssets( $strAssets ) {
		$this->set( 'm_strAssets', CStrings::strTrimDef( $strAssets, -1, NULL, true ) );
	}

	public function getAssets() {
		return $this->m_strAssets;
	}

	public function sqlAssets() {
		return ( true == isset( $this->m_strAssets ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAssets ) : '\'' . addslashes( $this->m_strAssets ) . '\'' ) : 'NULL';
	}

	public function setAddressLine( $strAddressLine ) {
		$this->set( 'm_strAddressLine', CStrings::strTrimDef( $strAddressLine, 100, NULL, true ) );
	}

	public function getAddressLine() {
		return $this->m_strAddressLine;
	}

	public function sqlAddressLine() {
		return ( true == isset( $this->m_strAddressLine ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAddressLine ) : '\'' . addslashes( $this->m_strAddressLine ) . '\'' ) : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCity ) : '\'' . addslashes( $this->m_strCity ) . '\'' ) : 'NULL';
	}

	public function setState( $strState ) {
		$this->set( 'm_strState', CStrings::strTrimDef( $strState, 50, NULL, true ) );
	}

	public function getState() {
		return $this->m_strState;
	}

	public function sqlState() {
		return ( true == isset( $this->m_strState ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strState ) : '\'' . addslashes( $this->m_strState ) . '\'' ) : 'NULL';
	}

	public function setZipcode( $strZipcode ) {
		$this->set( 'm_strZipcode', CStrings::strTrimDef( $strZipcode, 50, NULL, true ) );
	}

	public function getZipcode() {
		return $this->m_strZipcode;
	}

	public function sqlZipcode() {
		return ( true == isset( $this->m_strZipcode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strZipcode ) : '\'' . addslashes( $this->m_strZipcode ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 100, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setTelephone( $strTelephone ) {
		$this->set( 'm_strTelephone', CStrings::strTrimDef( $strTelephone, 20, NULL, true ) );
	}

	public function getTelephone() {
		return $this->m_strTelephone;
	}

	public function sqlTelephone() {
		return ( true == isset( $this->m_strTelephone ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTelephone ) : '\'' . addslashes( $this->m_strTelephone ) . '\'' ) : 'NULL';
	}

	public function setScreeningCompletedOn( $strScreeningCompletedOn ) {
		$this->set( 'm_strScreeningCompletedOn', CStrings::strTrimDef( $strScreeningCompletedOn, -1, NULL, true ) );
	}

	public function getScreeningCompletedOn() {
		return $this->m_strScreeningCompletedOn;
	}

	public function sqlScreeningCompletedOn() {
		return ( true == isset( $this->m_strScreeningCompletedOn ) ) ? '\'' . $this->m_strScreeningCompletedOn . '\'' : 'NULL';
	}

	public function setIsAlien( $boolIsAlien ) {
		$this->set( 'm_boolIsAlien', CStrings::strToBool( $boolIsAlien ) );
	}

	public function getIsAlien() {
		return $this->m_boolIsAlien;
	}

	public function sqlIsAlien() {
		return ( true == isset( $this->m_boolIsAlien ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAlien ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setRvIndexScore( $fltRvIndexScore ) {
		$this->set( 'm_fltRvIndexScore', CStrings::strToFloatDef( $fltRvIndexScore, NULL, false, 3 ) );
	}

	public function getRvIndexScore() {
		return $this->m_fltRvIndexScore;
	}

	public function sqlRvIndexScore() {
		return ( true == isset( $this->m_fltRvIndexScore ) ) ? ( string ) $this->m_fltRvIndexScore : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, screening_id, custom_applicant_id, screening_applicant_type_id, applicant_application_transmission_id, screening_package_id, lead_source_id, guarantor_for_application_id, status_type_id, original_screening_recommendation_type_id, screening_recommendation_type_id, name_first, name_last, name_middle, tax_number_last_four, tax_number_encrypted, birth_date_year, birth_date_encrypted, driver_license, driver_license_state, employer_name, income, assets, address_line, city, state, zipcode, email_address, telephone, screening_completed_on, is_alien, updated_by, updated_on, created_by, created_on, details, rv_index_score )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlScreeningId() . ', ' .
						$this->sqlCustomApplicantId() . ', ' .
						$this->sqlScreeningApplicantTypeId() . ', ' .
						$this->sqlApplicantApplicationTransmissionId() . ', ' .
						$this->sqlScreeningPackageId() . ', ' .
						$this->sqlLeadSourceId() . ', ' .
						$this->sqlGuarantorForApplicationId() . ', ' .
						$this->sqlStatusTypeId() . ', ' .
						$this->sqlOriginalScreeningRecommendationTypeId() . ', ' .
						$this->sqlScreeningRecommendationTypeId() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlNameMiddle() . ', ' .
						$this->sqlTaxNumberLastFour() . ', ' .
						$this->sqlTaxNumberEncrypted() . ', ' .
						$this->sqlBirthDateYear() . ', ' .
						$this->sqlBirthDateEncrypted() . ', ' .
						$this->sqlDriverLicense() . ', ' .
						$this->sqlDriverLicenseState() . ', ' .
						$this->sqlEmployerName() . ', ' .
						$this->sqlIncome() . ', ' .
						$this->sqlAssets() . ', ' .
						$this->sqlAddressLine() . ', ' .
						$this->sqlCity() . ', ' .
						$this->sqlState() . ', ' .
						$this->sqlZipcode() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlTelephone() . ', ' .
						$this->sqlScreeningCompletedOn() . ', ' .
						$this->sqlIsAlien() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlRvIndexScore() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId(). ',' ; } elseif( true == array_key_exists( 'ScreeningId', $this->getChangedColumns() ) ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_applicant_id = ' . $this->sqlCustomApplicantId(). ',' ; } elseif( true == array_key_exists( 'CustomApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' custom_applicant_id = ' . $this->sqlCustomApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_type_id = ' . $this->sqlScreeningApplicantTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningApplicantTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_type_id = ' . $this->sqlScreeningApplicantTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_application_transmission_id = ' . $this->sqlApplicantApplicationTransmissionId(). ',' ; } elseif( true == array_key_exists( 'ApplicantApplicationTransmissionId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_application_transmission_id = ' . $this->sqlApplicantApplicationTransmissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_id = ' . $this->sqlScreeningPackageId(). ',' ; } elseif( true == array_key_exists( 'ScreeningPackageId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_id = ' . $this->sqlScreeningPackageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_source_id = ' . $this->sqlLeadSourceId(). ',' ; } elseif( true == array_key_exists( 'LeadSourceId', $this->getChangedColumns() ) ) { $strSql .= ' lead_source_id = ' . $this->sqlLeadSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' guarantor_for_application_id = ' . $this->sqlGuarantorForApplicationId(). ',' ; } elseif( true == array_key_exists( 'GuarantorForApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' guarantor_for_application_id = ' . $this->sqlGuarantorForApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status_type_id = ' . $this->sqlStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'StatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' status_type_id = ' . $this->sqlStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_screening_recommendation_type_id = ' . $this->sqlOriginalScreeningRecommendationTypeId(). ',' ; } elseif( true == array_key_exists( 'OriginalScreeningRecommendationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' original_screening_recommendation_type_id = ' . $this->sqlOriginalScreeningRecommendationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningRecommendationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle(). ',' ; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_last_four = ' . $this->sqlTaxNumberLastFour(). ',' ; } elseif( true == array_key_exists( 'TaxNumberLastFour', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_last_four = ' . $this->sqlTaxNumberLastFour() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' birth_date_year = ' . $this->sqlBirthDateYear(). ',' ; } elseif( true == array_key_exists( 'BirthDateYear', $this->getChangedColumns() ) ) { $strSql .= ' birth_date_year = ' . $this->sqlBirthDateYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' birth_date_encrypted = ' . $this->sqlBirthDateEncrypted(). ',' ; } elseif( true == array_key_exists( 'BirthDateEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' birth_date_encrypted = ' . $this->sqlBirthDateEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' driver_license = ' . $this->sqlDriverLicense(). ',' ; } elseif( true == array_key_exists( 'DriverLicense', $this->getChangedColumns() ) ) { $strSql .= ' driver_license = ' . $this->sqlDriverLicense() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' driver_license_state = ' . $this->sqlDriverLicenseState(). ',' ; } elseif( true == array_key_exists( 'DriverLicenseState', $this->getChangedColumns() ) ) { $strSql .= ' driver_license_state = ' . $this->sqlDriverLicenseState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employer_name = ' . $this->sqlEmployerName(). ',' ; } elseif( true == array_key_exists( 'EmployerName', $this->getChangedColumns() ) ) { $strSql .= ' employer_name = ' . $this->sqlEmployerName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' income = ' . $this->sqlIncome(). ',' ; } elseif( true == array_key_exists( 'Income', $this->getChangedColumns() ) ) { $strSql .= ' income = ' . $this->sqlIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assets = ' . $this->sqlAssets(). ',' ; } elseif( true == array_key_exists( 'Assets', $this->getChangedColumns() ) ) { $strSql .= ' assets = ' . $this->sqlAssets() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address_line = ' . $this->sqlAddressLine(). ',' ; } elseif( true == array_key_exists( 'AddressLine', $this->getChangedColumns() ) ) { $strSql .= ' address_line = ' . $this->sqlAddressLine() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity(). ',' ; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state = ' . $this->sqlState(). ',' ; } elseif( true == array_key_exists( 'State', $this->getChangedColumns() ) ) { $strSql .= ' state = ' . $this->sqlState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' zipcode = ' . $this->sqlZipcode(). ',' ; } elseif( true == array_key_exists( 'Zipcode', $this->getChangedColumns() ) ) { $strSql .= ' zipcode = ' . $this->sqlZipcode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' telephone = ' . $this->sqlTelephone(). ',' ; } elseif( true == array_key_exists( 'Telephone', $this->getChangedColumns() ) ) { $strSql .= ' telephone = ' . $this->sqlTelephone() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_completed_on = ' . $this->sqlScreeningCompletedOn(). ',' ; } elseif( true == array_key_exists( 'ScreeningCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' screening_completed_on = ' . $this->sqlScreeningCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_alien = ' . $this->sqlIsAlien(). ',' ; } elseif( true == array_key_exists( 'IsAlien', $this->getChangedColumns() ) ) { $strSql .= ' is_alien = ' . $this->sqlIsAlien() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rv_index_score = ' . $this->sqlRvIndexScore(). ',' ; } elseif( true == array_key_exists( 'RvIndexScore', $this->getChangedColumns() ) ) { $strSql .= ' rv_index_score = ' . $this->sqlRvIndexScore() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'screening_id' => $this->getScreeningId(),
			'custom_applicant_id' => $this->getCustomApplicantId(),
			'screening_applicant_type_id' => $this->getScreeningApplicantTypeId(),
			'applicant_application_transmission_id' => $this->getApplicantApplicationTransmissionId(),
			'screening_package_id' => $this->getScreeningPackageId(),
			'lead_source_id' => $this->getLeadSourceId(),
			'guarantor_for_application_id' => $this->getGuarantorForApplicationId(),
			'status_type_id' => $this->getStatusTypeId(),
			'original_screening_recommendation_type_id' => $this->getOriginalScreeningRecommendationTypeId(),
			'screening_recommendation_type_id' => $this->getScreeningRecommendationTypeId(),
			'name_first' => $this->getNameFirst(),
			'name_last' => $this->getNameLast(),
			'name_middle' => $this->getNameMiddle(),
			'tax_number_last_four' => $this->getTaxNumberLastFour(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'birth_date_year' => $this->getBirthDateYear(),
			'birth_date_encrypted' => $this->getBirthDateEncrypted(),
			'driver_license' => $this->getDriverLicense(),
			'driver_license_state' => $this->getDriverLicenseState(),
			'employer_name' => $this->getEmployerName(),
			'income' => $this->getIncome(),
			'assets' => $this->getAssets(),
			'address_line' => $this->getAddressLine(),
			'city' => $this->getCity(),
			'state' => $this->getState(),
			'zipcode' => $this->getZipcode(),
			'email_address' => $this->getEmailAddress(),
			'telephone' => $this->getTelephone(),
			'screening_completed_on' => $this->getScreeningCompletedOn(),
			'is_alien' => $this->getIsAlien(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'rv_index_score' => $this->getRvIndexScore()
		);
	}

}
?>