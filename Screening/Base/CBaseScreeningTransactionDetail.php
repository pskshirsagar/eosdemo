<?php

class CBaseScreeningTransactionDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_transaction_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScreeningId;
	protected $m_intScreeningTransactionId;
	protected $m_boolHasCreditRecord;
	protected $m_boolHasCreditScore;
	protected $m_boolHasProfileSummary;
	protected $m_boolHasFraudHit;
	protected $m_boolIsOtpGenerated;
	protected $m_boolIsKiqGenerated;
	protected $m_strUpdatedOn;
	protected $m_intUpdatedBy;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screening_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningId', trim( $arrValues['screening_id'] ) ); elseif( isset( $arrValues['screening_id'] ) ) $this->setScreeningId( $arrValues['screening_id'] );
		if( isset( $arrValues['screening_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningTransactionId', trim( $arrValues['screening_transaction_id'] ) ); elseif( isset( $arrValues['screening_transaction_id'] ) ) $this->setScreeningTransactionId( $arrValues['screening_transaction_id'] );
		if( isset( $arrValues['has_credit_record'] ) && $boolDirectSet ) $this->set( 'm_boolHasCreditRecord', trim( stripcslashes( $arrValues['has_credit_record'] ) ) ); elseif( isset( $arrValues['has_credit_record'] ) ) $this->setHasCreditRecord( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_credit_record'] ) : $arrValues['has_credit_record'] );
		if( isset( $arrValues['has_credit_score'] ) && $boolDirectSet ) $this->set( 'm_boolHasCreditScore', trim( stripcslashes( $arrValues['has_credit_score'] ) ) ); elseif( isset( $arrValues['has_credit_score'] ) ) $this->setHasCreditScore( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_credit_score'] ) : $arrValues['has_credit_score'] );
		if( isset( $arrValues['has_profile_summary'] ) && $boolDirectSet ) $this->set( 'm_boolHasProfileSummary', trim( stripcslashes( $arrValues['has_profile_summary'] ) ) ); elseif( isset( $arrValues['has_profile_summary'] ) ) $this->setHasProfileSummary( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_profile_summary'] ) : $arrValues['has_profile_summary'] );
		if( isset( $arrValues['has_fraud_hit'] ) && $boolDirectSet ) $this->set( 'm_boolHasFraudHit', trim( stripcslashes( $arrValues['has_fraud_hit'] ) ) ); elseif( isset( $arrValues['has_fraud_hit'] ) ) $this->setHasFraudHit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_fraud_hit'] ) : $arrValues['has_fraud_hit'] );
		if( isset( $arrValues['is_otp_generated'] ) && $boolDirectSet ) $this->set( 'm_boolIsOtpGenerated', trim( stripcslashes( $arrValues['is_otp_generated'] ) ) ); elseif( isset( $arrValues['is_otp_generated'] ) ) $this->setIsOtpGenerated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_otp_generated'] ) : $arrValues['is_otp_generated'] );
		if( isset( $arrValues['is_kiq_generated'] ) && $boolDirectSet ) $this->set( 'm_boolIsKiqGenerated', trim( stripcslashes( $arrValues['is_kiq_generated'] ) ) ); elseif( isset( $arrValues['is_kiq_generated'] ) ) $this->setIsKiqGenerated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_kiq_generated'] ) : $arrValues['is_kiq_generated'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreeningId( $intScreeningId ) {
		$this->set( 'm_intScreeningId', CStrings::strToIntDef( $intScreeningId, NULL, false ) );
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function sqlScreeningId() {
		return ( true == isset( $this->m_intScreeningId ) ) ? ( string ) $this->m_intScreeningId : 'NULL';
	}

	public function setScreeningTransactionId( $intScreeningTransactionId ) {
		$this->set( 'm_intScreeningTransactionId', CStrings::strToIntDef( $intScreeningTransactionId, NULL, false ) );
	}

	public function getScreeningTransactionId() {
		return $this->m_intScreeningTransactionId;
	}

	public function sqlScreeningTransactionId() {
		return ( true == isset( $this->m_intScreeningTransactionId ) ) ? ( string ) $this->m_intScreeningTransactionId : 'NULL';
	}

	public function setHasCreditRecord( $boolHasCreditRecord ) {
		$this->set( 'm_boolHasCreditRecord', CStrings::strToBool( $boolHasCreditRecord ) );
	}

	public function getHasCreditRecord() {
		return $this->m_boolHasCreditRecord;
	}

	public function sqlHasCreditRecord() {
		return ( true == isset( $this->m_boolHasCreditRecord ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasCreditRecord ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasCreditScore( $boolHasCreditScore ) {
		$this->set( 'm_boolHasCreditScore', CStrings::strToBool( $boolHasCreditScore ) );
	}

	public function getHasCreditScore() {
		return $this->m_boolHasCreditScore;
	}

	public function sqlHasCreditScore() {
		return ( true == isset( $this->m_boolHasCreditScore ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasCreditScore ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasProfileSummary( $boolHasProfileSummary ) {
		$this->set( 'm_boolHasProfileSummary', CStrings::strToBool( $boolHasProfileSummary ) );
	}

	public function getHasProfileSummary() {
		return $this->m_boolHasProfileSummary;
	}

	public function sqlHasProfileSummary() {
		return ( true == isset( $this->m_boolHasProfileSummary ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasProfileSummary ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasFraudHit( $boolHasFraudHit ) {
		$this->set( 'm_boolHasFraudHit', CStrings::strToBool( $boolHasFraudHit ) );
	}

	public function getHasFraudHit() {
		return $this->m_boolHasFraudHit;
	}

	public function sqlHasFraudHit() {
		return ( true == isset( $this->m_boolHasFraudHit ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasFraudHit ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOtpGenerated( $boolIsOtpGenerated ) {
		$this->set( 'm_boolIsOtpGenerated', CStrings::strToBool( $boolIsOtpGenerated ) );
	}

	public function getIsOtpGenerated() {
		return $this->m_boolIsOtpGenerated;
	}

	public function sqlIsOtpGenerated() {
		return ( true == isset( $this->m_boolIsOtpGenerated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOtpGenerated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsKiqGenerated( $boolIsKiqGenerated ) {
		$this->set( 'm_boolIsKiqGenerated', CStrings::strToBool( $boolIsKiqGenerated ) );
	}

	public function getIsKiqGenerated() {
		return $this->m_boolIsKiqGenerated;
	}

	public function sqlIsKiqGenerated() {
		return ( true == isset( $this->m_boolIsKiqGenerated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsKiqGenerated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, screening_id, screening_transaction_id, has_credit_record, has_credit_score, has_profile_summary, has_fraud_hit, is_otp_generated, is_kiq_generated, updated_on, updated_by, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlScreeningId() . ', ' .
						$this->sqlScreeningTransactionId() . ', ' .
						$this->sqlHasCreditRecord() . ', ' .
						$this->sqlHasCreditScore() . ', ' .
						$this->sqlHasProfileSummary() . ', ' .
						$this->sqlHasFraudHit() . ', ' .
						$this->sqlIsOtpGenerated() . ', ' .
						$this->sqlIsKiqGenerated() . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId(). ',' ; } elseif( true == array_key_exists( 'ScreeningId', $this->getChangedColumns() ) ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_transaction_id = ' . $this->sqlScreeningTransactionId(). ',' ; } elseif( true == array_key_exists( 'ScreeningTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' screening_transaction_id = ' . $this->sqlScreeningTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_credit_record = ' . $this->sqlHasCreditRecord(). ',' ; } elseif( true == array_key_exists( 'HasCreditRecord', $this->getChangedColumns() ) ) { $strSql .= ' has_credit_record = ' . $this->sqlHasCreditRecord() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_credit_score = ' . $this->sqlHasCreditScore(). ',' ; } elseif( true == array_key_exists( 'HasCreditScore', $this->getChangedColumns() ) ) { $strSql .= ' has_credit_score = ' . $this->sqlHasCreditScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_profile_summary = ' . $this->sqlHasProfileSummary(). ',' ; } elseif( true == array_key_exists( 'HasProfileSummary', $this->getChangedColumns() ) ) { $strSql .= ' has_profile_summary = ' . $this->sqlHasProfileSummary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_fraud_hit = ' . $this->sqlHasFraudHit(). ',' ; } elseif( true == array_key_exists( 'HasFraudHit', $this->getChangedColumns() ) ) { $strSql .= ' has_fraud_hit = ' . $this->sqlHasFraudHit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_otp_generated = ' . $this->sqlIsOtpGenerated(). ',' ; } elseif( true == array_key_exists( 'IsOtpGenerated', $this->getChangedColumns() ) ) { $strSql .= ' is_otp_generated = ' . $this->sqlIsOtpGenerated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_kiq_generated = ' . $this->sqlIsKiqGenerated(). ',' ; } elseif( true == array_key_exists( 'IsKiqGenerated', $this->getChangedColumns() ) ) { $strSql .= ' is_kiq_generated = ' . $this->sqlIsKiqGenerated() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'screening_id' => $this->getScreeningId(),
			'screening_transaction_id' => $this->getScreeningTransactionId(),
			'has_credit_record' => $this->getHasCreditRecord(),
			'has_credit_score' => $this->getHasCreditScore(),
			'has_profile_summary' => $this->getHasProfileSummary(),
			'has_fraud_hit' => $this->getHasFraudHit(),
			'is_otp_generated' => $this->getIsOtpGenerated(),
			'is_kiq_generated' => $this->getIsKiqGenerated(),
			'updated_on' => $this->getUpdatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>