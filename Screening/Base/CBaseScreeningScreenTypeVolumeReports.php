<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningScreenTypeVolumeReports
 * Do not add any new functions to this class.
 */

class CBaseScreeningScreenTypeVolumeReports extends CEosPluralBase {

	/**
	 * @return CScreeningScreenTypeVolumeReport[]
	 */
	public static function fetchScreeningScreenTypeVolumeReports( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningScreenTypeVolumeReport::class, $objDatabase );
	}

	/**
	 * @return CScreeningScreenTypeVolumeReport
	 */
	public static function fetchScreeningScreenTypeVolumeReport( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningScreenTypeVolumeReport::class, $objDatabase );
	}

	public static function fetchScreeningScreenTypeVolumeReportCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_screen_type_volume_reports', $objDatabase );
	}

	public static function fetchScreeningScreenTypeVolumeReportById( $intId, $objDatabase ) {
		return self::fetchScreeningScreenTypeVolumeReport( sprintf( 'SELECT * FROM screening_screen_type_volume_reports WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningScreenTypeVolumeReportsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningScreenTypeVolumeReports( sprintf( 'SELECT * FROM screening_screen_type_volume_reports WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningScreenTypeVolumeReportsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchScreeningScreenTypeVolumeReports( sprintf( 'SELECT * FROM screening_screen_type_volume_reports WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchScreeningScreenTypeVolumeReportsByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreeningScreenTypeVolumeReports( sprintf( 'SELECT * FROM screening_screen_type_volume_reports WHERE screen_type_id = %d', ( int ) $intScreenTypeId ), $objDatabase );
	}

}
?>