<?php

class CBaseScreeningTransaction extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_transactions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intScreeningId;
	protected $m_intScreeningApplicantId;
	protected $m_intScreenTypeId;
	protected $m_intScreeningBatchId;
	protected $m_intCompanyScreeningAccountId;
	protected $m_intScreeningDataProviderId;
	protected $m_strScreeningOrderId;
	protected $m_intScreeningStatusTypeId;
	protected $m_intOriginalScreeningRecommendationTypeId;
	protected $m_intScreeningRecommendationTypeId;
	protected $m_intScreeningPackageConditionSetId;
	protected $m_strVendorOrderStatus;
	protected $m_fltTransactionAmount;
	protected $m_strSearchType;
	protected $m_strScreeningRequestedOn;
	protected $m_strScreeningResponseCompletedOn;
	protected $m_strScreeningCompletedOn;
	protected $m_strOriginalScreeningCompletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strAdditionalDetails;
	protected $m_jsonAdditionalDetails;
	protected $m_strScreeningRecommendationUpdatedOn;
	protected $m_boolIsSkipped;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['screening_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningId', trim( $arrValues['screening_id'] ) ); elseif( isset( $arrValues['screening_id'] ) ) $this->setScreeningId( $arrValues['screening_id'] );
		if( isset( $arrValues['screening_applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicantId', trim( $arrValues['screening_applicant_id'] ) ); elseif( isset( $arrValues['screening_applicant_id'] ) ) $this->setScreeningApplicantId( $arrValues['screening_applicant_id'] );
		if( isset( $arrValues['screen_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreenTypeId', trim( $arrValues['screen_type_id'] ) ); elseif( isset( $arrValues['screen_type_id'] ) ) $this->setScreenTypeId( $arrValues['screen_type_id'] );
		if( isset( $arrValues['screening_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningBatchId', trim( $arrValues['screening_batch_id'] ) ); elseif( isset( $arrValues['screening_batch_id'] ) ) $this->setScreeningBatchId( $arrValues['screening_batch_id'] );
		if( isset( $arrValues['company_screening_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyScreeningAccountId', trim( $arrValues['company_screening_account_id'] ) ); elseif( isset( $arrValues['company_screening_account_id'] ) ) $this->setCompanyScreeningAccountId( $arrValues['company_screening_account_id'] );
		if( isset( $arrValues['screening_data_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningDataProviderId', trim( $arrValues['screening_data_provider_id'] ) ); elseif( isset( $arrValues['screening_data_provider_id'] ) ) $this->setScreeningDataProviderId( $arrValues['screening_data_provider_id'] );
		if( isset( $arrValues['screening_order_id'] ) && $boolDirectSet ) $this->set( 'm_strScreeningOrderId', trim( $arrValues['screening_order_id'] ) ); elseif( isset( $arrValues['screening_order_id'] ) ) $this->setScreeningOrderId( $arrValues['screening_order_id'] );
		if( isset( $arrValues['screening_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningStatusTypeId', trim( $arrValues['screening_status_type_id'] ) ); elseif( isset( $arrValues['screening_status_type_id'] ) ) $this->setScreeningStatusTypeId( $arrValues['screening_status_type_id'] );
		if( isset( $arrValues['original_screening_recommendation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalScreeningRecommendationTypeId', trim( $arrValues['original_screening_recommendation_type_id'] ) ); elseif( isset( $arrValues['original_screening_recommendation_type_id'] ) ) $this->setOriginalScreeningRecommendationTypeId( $arrValues['original_screening_recommendation_type_id'] );
		if( isset( $arrValues['screening_recommendation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningRecommendationTypeId', trim( $arrValues['screening_recommendation_type_id'] ) ); elseif( isset( $arrValues['screening_recommendation_type_id'] ) ) $this->setScreeningRecommendationTypeId( $arrValues['screening_recommendation_type_id'] );
		if( isset( $arrValues['screening_package_condition_set_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageConditionSetId', trim( $arrValues['screening_package_condition_set_id'] ) ); elseif( isset( $arrValues['screening_package_condition_set_id'] ) ) $this->setScreeningPackageConditionSetId( $arrValues['screening_package_condition_set_id'] );
		if( isset( $arrValues['vendor_order_status'] ) && $boolDirectSet ) $this->set( 'm_strVendorOrderStatus', trim( $arrValues['vendor_order_status'] ) ); elseif( isset( $arrValues['vendor_order_status'] ) ) $this->setVendorOrderStatus( $arrValues['vendor_order_status'] );
		if( isset( $arrValues['transaction_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmount', trim( $arrValues['transaction_amount'] ) ); elseif( isset( $arrValues['transaction_amount'] ) ) $this->setTransactionAmount( $arrValues['transaction_amount'] );
		if( isset( $arrValues['search_type'] ) && $boolDirectSet ) $this->set( 'm_strSearchType', trim( $arrValues['search_type'] ) ); elseif( isset( $arrValues['search_type'] ) ) $this->setSearchType( $arrValues['search_type'] );
		if( isset( $arrValues['screening_requested_on'] ) && $boolDirectSet ) $this->set( 'm_strScreeningRequestedOn', trim( $arrValues['screening_requested_on'] ) ); elseif( isset( $arrValues['screening_requested_on'] ) ) $this->setScreeningRequestedOn( $arrValues['screening_requested_on'] );
		if( isset( $arrValues['screening_response_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strScreeningResponseCompletedOn', trim( $arrValues['screening_response_completed_on'] ) ); elseif( isset( $arrValues['screening_response_completed_on'] ) ) $this->setScreeningResponseCompletedOn( $arrValues['screening_response_completed_on'] );
		if( isset( $arrValues['screening_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strScreeningCompletedOn', trim( $arrValues['screening_completed_on'] ) ); elseif( isset( $arrValues['screening_completed_on'] ) ) $this->setScreeningCompletedOn( $arrValues['screening_completed_on'] );
		if( isset( $arrValues['original_screening_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strOriginalScreeningCompletedOn', trim( $arrValues['original_screening_completed_on'] ) ); elseif( isset( $arrValues['original_screening_completed_on'] ) ) $this->setOriginalScreeningCompletedOn( $arrValues['original_screening_completed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['additional_details'] ) ) $this->set( 'm_strAdditionalDetails', trim( $arrValues['additional_details'] ) );
		if( isset( $arrValues['screening_recommendation_updated_on'] ) && $boolDirectSet ) $this->set( 'm_strScreeningRecommendationUpdatedOn', trim( $arrValues['screening_recommendation_updated_on'] ) ); elseif( isset( $arrValues['screening_recommendation_updated_on'] ) ) $this->setScreeningRecommendationUpdatedOn( $arrValues['screening_recommendation_updated_on'] );
		if( isset( $arrValues['is_skipped'] ) && $boolDirectSet ) $this->set( 'm_boolIsSkipped', trim( stripcslashes( $arrValues['is_skipped'] ) ) ); elseif( isset( $arrValues['is_skipped'] ) ) $this->setIsSkipped( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_skipped'] ) : $arrValues['is_skipped'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setScreeningId( $intScreeningId ) {
		$this->set( 'm_intScreeningId', CStrings::strToIntDef( $intScreeningId, NULL, false ) );
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function sqlScreeningId() {
		return ( true == isset( $this->m_intScreeningId ) ) ? ( string ) $this->m_intScreeningId : 'NULL';
	}

	public function setScreeningApplicantId( $intScreeningApplicantId ) {
		$this->set( 'm_intScreeningApplicantId', CStrings::strToIntDef( $intScreeningApplicantId, NULL, false ) );
	}

	public function getScreeningApplicantId() {
		return $this->m_intScreeningApplicantId;
	}

	public function sqlScreeningApplicantId() {
		return ( true == isset( $this->m_intScreeningApplicantId ) ) ? ( string ) $this->m_intScreeningApplicantId : 'NULL';
	}

	public function setScreenTypeId( $intScreenTypeId ) {
		$this->set( 'm_intScreenTypeId', CStrings::strToIntDef( $intScreenTypeId, NULL, false ) );
	}

	public function getScreenTypeId() {
		return $this->m_intScreenTypeId;
	}

	public function sqlScreenTypeId() {
		return ( true == isset( $this->m_intScreenTypeId ) ) ? ( string ) $this->m_intScreenTypeId : 'NULL';
	}

	public function setScreeningBatchId( $intScreeningBatchId ) {
		$this->set( 'm_intScreeningBatchId', CStrings::strToIntDef( $intScreeningBatchId, NULL, false ) );
	}

	public function getScreeningBatchId() {
		return $this->m_intScreeningBatchId;
	}

	public function sqlScreeningBatchId() {
		return ( true == isset( $this->m_intScreeningBatchId ) ) ? ( string ) $this->m_intScreeningBatchId : 'NULL';
	}

	public function setCompanyScreeningAccountId( $intCompanyScreeningAccountId ) {
		$this->set( 'm_intCompanyScreeningAccountId', CStrings::strToIntDef( $intCompanyScreeningAccountId, NULL, false ) );
	}

	public function getCompanyScreeningAccountId() {
		return $this->m_intCompanyScreeningAccountId;
	}

	public function sqlCompanyScreeningAccountId() {
		return ( true == isset( $this->m_intCompanyScreeningAccountId ) ) ? ( string ) $this->m_intCompanyScreeningAccountId : 'NULL';
	}

	public function setScreeningDataProviderId( $intScreeningDataProviderId ) {
		$this->set( 'm_intScreeningDataProviderId', CStrings::strToIntDef( $intScreeningDataProviderId, NULL, false ) );
	}

	public function getScreeningDataProviderId() {
		return $this->m_intScreeningDataProviderId;
	}

	public function sqlScreeningDataProviderId() {
		return ( true == isset( $this->m_intScreeningDataProviderId ) ) ? ( string ) $this->m_intScreeningDataProviderId : 'NULL';
	}

	public function setScreeningOrderId( $strScreeningOrderId ) {
		$this->set( 'm_strScreeningOrderId', CStrings::strTrimDef( $strScreeningOrderId, 128, NULL, true ) );
	}

	public function getScreeningOrderId() {
		return $this->m_strScreeningOrderId;
	}

	public function sqlScreeningOrderId() {
		return ( true == isset( $this->m_strScreeningOrderId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strScreeningOrderId ) : '\'' . addslashes( $this->m_strScreeningOrderId ) . '\'' ) : 'NULL';
	}

	public function setScreeningStatusTypeId( $intScreeningStatusTypeId ) {
		$this->set( 'm_intScreeningStatusTypeId', CStrings::strToIntDef( $intScreeningStatusTypeId, NULL, false ) );
	}

	public function getScreeningStatusTypeId() {
		return $this->m_intScreeningStatusTypeId;
	}

	public function sqlScreeningStatusTypeId() {
		return ( true == isset( $this->m_intScreeningStatusTypeId ) ) ? ( string ) $this->m_intScreeningStatusTypeId : 'NULL';
	}

	public function setOriginalScreeningRecommendationTypeId( $intOriginalScreeningRecommendationTypeId ) {
		$this->set( 'm_intOriginalScreeningRecommendationTypeId', CStrings::strToIntDef( $intOriginalScreeningRecommendationTypeId, NULL, false ) );
	}

	public function getOriginalScreeningRecommendationTypeId() {
		return $this->m_intOriginalScreeningRecommendationTypeId;
	}

	public function sqlOriginalScreeningRecommendationTypeId() {
		return ( true == isset( $this->m_intOriginalScreeningRecommendationTypeId ) ) ? ( string ) $this->m_intOriginalScreeningRecommendationTypeId : 'NULL';
	}

	public function setScreeningRecommendationTypeId( $intScreeningRecommendationTypeId ) {
		$this->set( 'm_intScreeningRecommendationTypeId', CStrings::strToIntDef( $intScreeningRecommendationTypeId, NULL, false ) );
	}

	public function getScreeningRecommendationTypeId() {
		return $this->m_intScreeningRecommendationTypeId;
	}

	public function sqlScreeningRecommendationTypeId() {
		return ( true == isset( $this->m_intScreeningRecommendationTypeId ) ) ? ( string ) $this->m_intScreeningRecommendationTypeId : 'NULL';
	}

	public function setScreeningPackageConditionSetId( $intScreeningPackageConditionSetId ) {
		$this->set( 'm_intScreeningPackageConditionSetId', CStrings::strToIntDef( $intScreeningPackageConditionSetId, NULL, false ) );
	}

	public function getScreeningPackageConditionSetId() {
		return $this->m_intScreeningPackageConditionSetId;
	}

	public function sqlScreeningPackageConditionSetId() {
		return ( true == isset( $this->m_intScreeningPackageConditionSetId ) ) ? ( string ) $this->m_intScreeningPackageConditionSetId : 'NULL';
	}

	public function setVendorOrderStatus( $strVendorOrderStatus ) {
		$this->set( 'm_strVendorOrderStatus', CStrings::strTrimDef( $strVendorOrderStatus, 100, NULL, true ) );
	}

	public function getVendorOrderStatus() {
		return $this->m_strVendorOrderStatus;
	}

	public function sqlVendorOrderStatus() {
		return ( true == isset( $this->m_strVendorOrderStatus ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strVendorOrderStatus ) : '\'' . addslashes( $this->m_strVendorOrderStatus ) . '\'' ) : 'NULL';
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->set( 'm_fltTransactionAmount', CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 2 ) );
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function sqlTransactionAmount() {
		return ( true == isset( $this->m_fltTransactionAmount ) ) ? ( string ) $this->m_fltTransactionAmount : 'NULL';
	}

	public function setSearchType( $strSearchType ) {
		$this->set( 'm_strSearchType', CStrings::strTrimDef( $strSearchType, -1, NULL, true ) );
	}

	public function getSearchType() {
		return $this->m_strSearchType;
	}

	public function sqlSearchType() {
		return ( true == isset( $this->m_strSearchType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSearchType ) : '\'' . addslashes( $this->m_strSearchType ) . '\'' ) : 'NULL';
	}

	public function setScreeningRequestedOn( $strScreeningRequestedOn ) {
		$this->set( 'm_strScreeningRequestedOn', CStrings::strTrimDef( $strScreeningRequestedOn, -1, NULL, true ) );
	}

	public function getScreeningRequestedOn() {
		return $this->m_strScreeningRequestedOn;
	}

	public function sqlScreeningRequestedOn() {
		return ( true == isset( $this->m_strScreeningRequestedOn ) ) ? '\'' . $this->m_strScreeningRequestedOn . '\'' : 'NULL';
	}

	public function setScreeningResponseCompletedOn( $strScreeningResponseCompletedOn ) {
		$this->set( 'm_strScreeningResponseCompletedOn', CStrings::strTrimDef( $strScreeningResponseCompletedOn, -1, NULL, true ) );
	}

	public function getScreeningResponseCompletedOn() {
		return $this->m_strScreeningResponseCompletedOn;
	}

	public function sqlScreeningResponseCompletedOn() {
		return ( true == isset( $this->m_strScreeningResponseCompletedOn ) ) ? '\'' . $this->m_strScreeningResponseCompletedOn . '\'' : 'NULL';
	}

	public function setScreeningCompletedOn( $strScreeningCompletedOn ) {
		$this->set( 'm_strScreeningCompletedOn', CStrings::strTrimDef( $strScreeningCompletedOn, -1, NULL, true ) );
	}

	public function getScreeningCompletedOn() {
		return $this->m_strScreeningCompletedOn;
	}

	public function sqlScreeningCompletedOn() {
		return ( true == isset( $this->m_strScreeningCompletedOn ) ) ? '\'' . $this->m_strScreeningCompletedOn . '\'' : 'NULL';
	}

	public function setOriginalScreeningCompletedOn( $strOriginalScreeningCompletedOn ) {
		$this->set( 'm_strOriginalScreeningCompletedOn', CStrings::strTrimDef( $strOriginalScreeningCompletedOn, -1, NULL, true ) );
	}

	public function getOriginalScreeningCompletedOn() {
		return $this->m_strOriginalScreeningCompletedOn;
	}

	public function sqlOriginalScreeningCompletedOn() {
		return ( true == isset( $this->m_strOriginalScreeningCompletedOn ) ) ? '\'' . $this->m_strOriginalScreeningCompletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAdditionalDetails( $jsonAdditionalDetails ) {
		if( true == valObj( $jsonAdditionalDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonAdditionalDetails', $jsonAdditionalDetails );
		} elseif( true == valJsonString( $jsonAdditionalDetails ) ) {
			$this->set( 'm_jsonAdditionalDetails', CStrings::strToJson( $jsonAdditionalDetails ) );
		} else {
			$this->set( 'm_jsonAdditionalDetails', NULL ); 
		}
		unset( $this->m_strAdditionalDetails );
	}

	public function getAdditionalDetails() {
		if( true == isset( $this->m_strAdditionalDetails ) ) {
			$this->m_jsonAdditionalDetails = CStrings::strToJson( $this->m_strAdditionalDetails );
			unset( $this->m_strAdditionalDetails );
		}
		return $this->m_jsonAdditionalDetails;
	}

	public function sqlAdditionalDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getAdditionalDetails() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getAdditionalDetails() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getAdditionalDetails() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setScreeningRecommendationUpdatedOn( $strScreeningRecommendationUpdatedOn ) {
		$this->set( 'm_strScreeningRecommendationUpdatedOn', CStrings::strTrimDef( $strScreeningRecommendationUpdatedOn, -1, NULL, true ) );
	}

	public function getScreeningRecommendationUpdatedOn() {
		return $this->m_strScreeningRecommendationUpdatedOn;
	}

	public function sqlScreeningRecommendationUpdatedOn() {
		return ( true == isset( $this->m_strScreeningRecommendationUpdatedOn ) ) ? '\'' . $this->m_strScreeningRecommendationUpdatedOn . '\'' : 'NULL';
	}

	public function setIsSkipped( $boolIsSkipped ) {
		$this->set( 'm_boolIsSkipped', CStrings::strToBool( $boolIsSkipped ) );
	}

	public function getIsSkipped() {
		return $this->m_boolIsSkipped;
	}

	public function sqlIsSkipped() {
		return ( true == isset( $this->m_boolIsSkipped ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSkipped ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, screening_id, screening_applicant_id, screen_type_id, screening_batch_id, company_screening_account_id, screening_data_provider_id, screening_order_id, screening_status_type_id, original_screening_recommendation_type_id, screening_recommendation_type_id, screening_package_condition_set_id, vendor_order_status, transaction_amount, search_type, screening_requested_on, screening_response_completed_on, screening_completed_on, original_screening_completed_on, updated_by, updated_on, created_by, created_on, additional_details, screening_recommendation_updated_on, is_skipped )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlScreeningId() . ', ' .
						$this->sqlScreeningApplicantId() . ', ' .
						$this->sqlScreenTypeId() . ', ' .
						$this->sqlScreeningBatchId() . ', ' .
						$this->sqlCompanyScreeningAccountId() . ', ' .
						$this->sqlScreeningDataProviderId() . ', ' .
						$this->sqlScreeningOrderId() . ', ' .
						$this->sqlScreeningStatusTypeId() . ', ' .
						$this->sqlOriginalScreeningRecommendationTypeId() . ', ' .
						$this->sqlScreeningRecommendationTypeId() . ', ' .
						$this->sqlScreeningPackageConditionSetId() . ', ' .
						$this->sqlVendorOrderStatus() . ', ' .
						$this->sqlTransactionAmount() . ', ' .
						$this->sqlSearchType() . ', ' .
						$this->sqlScreeningRequestedOn() . ', ' .
						$this->sqlScreeningResponseCompletedOn() . ', ' .
						$this->sqlScreeningCompletedOn() . ', ' .
						$this->sqlOriginalScreeningCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlAdditionalDetails() . ', ' .
						$this->sqlScreeningRecommendationUpdatedOn() . ', ' .
						$this->sqlIsSkipped() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId(). ',' ; } elseif( true == array_key_exists( 'ScreeningId', $this->getChangedColumns() ) ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId(). ',' ; } elseif( true == array_key_exists( 'ScreeningApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreenTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_batch_id = ' . $this->sqlScreeningBatchId(). ',' ; } elseif( true == array_key_exists( 'ScreeningBatchId', $this->getChangedColumns() ) ) { $strSql .= ' screening_batch_id = ' . $this->sqlScreeningBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_screening_account_id = ' . $this->sqlCompanyScreeningAccountId(). ',' ; } elseif( true == array_key_exists( 'CompanyScreeningAccountId', $this->getChangedColumns() ) ) { $strSql .= ' company_screening_account_id = ' . $this->sqlCompanyScreeningAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_data_provider_id = ' . $this->sqlScreeningDataProviderId(). ',' ; } elseif( true == array_key_exists( 'ScreeningDataProviderId', $this->getChangedColumns() ) ) { $strSql .= ' screening_data_provider_id = ' . $this->sqlScreeningDataProviderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_order_id = ' . $this->sqlScreeningOrderId(). ',' ; } elseif( true == array_key_exists( 'ScreeningOrderId', $this->getChangedColumns() ) ) { $strSql .= ' screening_order_id = ' . $this->sqlScreeningOrderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_status_type_id = ' . $this->sqlScreeningStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_status_type_id = ' . $this->sqlScreeningStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_screening_recommendation_type_id = ' . $this->sqlOriginalScreeningRecommendationTypeId(). ',' ; } elseif( true == array_key_exists( 'OriginalScreeningRecommendationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' original_screening_recommendation_type_id = ' . $this->sqlOriginalScreeningRecommendationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningRecommendationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_condition_set_id = ' . $this->sqlScreeningPackageConditionSetId(). ',' ; } elseif( true == array_key_exists( 'ScreeningPackageConditionSetId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_condition_set_id = ' . $this->sqlScreeningPackageConditionSetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_order_status = ' . $this->sqlVendorOrderStatus(). ',' ; } elseif( true == array_key_exists( 'VendorOrderStatus', $this->getChangedColumns() ) ) { $strSql .= ' vendor_order_status = ' . $this->sqlVendorOrderStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount(). ',' ; } elseif( true == array_key_exists( 'TransactionAmount', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' search_type = ' . $this->sqlSearchType(). ',' ; } elseif( true == array_key_exists( 'SearchType', $this->getChangedColumns() ) ) { $strSql .= ' search_type = ' . $this->sqlSearchType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_requested_on = ' . $this->sqlScreeningRequestedOn(). ',' ; } elseif( true == array_key_exists( 'ScreeningRequestedOn', $this->getChangedColumns() ) ) { $strSql .= ' screening_requested_on = ' . $this->sqlScreeningRequestedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_response_completed_on = ' . $this->sqlScreeningResponseCompletedOn(). ',' ; } elseif( true == array_key_exists( 'ScreeningResponseCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' screening_response_completed_on = ' . $this->sqlScreeningResponseCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_completed_on = ' . $this->sqlScreeningCompletedOn(). ',' ; } elseif( true == array_key_exists( 'ScreeningCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' screening_completed_on = ' . $this->sqlScreeningCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_screening_completed_on = ' . $this->sqlOriginalScreeningCompletedOn(). ',' ; } elseif( true == array_key_exists( 'OriginalScreeningCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' original_screening_completed_on = ' . $this->sqlOriginalScreeningCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_details = ' . $this->sqlAdditionalDetails(). ',' ; } elseif( true == array_key_exists( 'AdditionalDetails', $this->getChangedColumns() ) ) { $strSql .= ' additional_details = ' . $this->sqlAdditionalDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_recommendation_updated_on = ' . $this->sqlScreeningRecommendationUpdatedOn(). ',' ; } elseif( true == array_key_exists( 'ScreeningRecommendationUpdatedOn', $this->getChangedColumns() ) ) { $strSql .= ' screening_recommendation_updated_on = ' . $this->sqlScreeningRecommendationUpdatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_skipped = ' . $this->sqlIsSkipped(). ',' ; } elseif( true == array_key_exists( 'IsSkipped', $this->getChangedColumns() ) ) { $strSql .= ' is_skipped = ' . $this->sqlIsSkipped() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'screening_id' => $this->getScreeningId(),
			'screening_applicant_id' => $this->getScreeningApplicantId(),
			'screen_type_id' => $this->getScreenTypeId(),
			'screening_batch_id' => $this->getScreeningBatchId(),
			'company_screening_account_id' => $this->getCompanyScreeningAccountId(),
			'screening_data_provider_id' => $this->getScreeningDataProviderId(),
			'screening_order_id' => $this->getScreeningOrderId(),
			'screening_status_type_id' => $this->getScreeningStatusTypeId(),
			'original_screening_recommendation_type_id' => $this->getOriginalScreeningRecommendationTypeId(),
			'screening_recommendation_type_id' => $this->getScreeningRecommendationTypeId(),
			'screening_package_condition_set_id' => $this->getScreeningPackageConditionSetId(),
			'vendor_order_status' => $this->getVendorOrderStatus(),
			'transaction_amount' => $this->getTransactionAmount(),
			'search_type' => $this->getSearchType(),
			'screening_requested_on' => $this->getScreeningRequestedOn(),
			'screening_response_completed_on' => $this->getScreeningResponseCompletedOn(),
			'screening_completed_on' => $this->getScreeningCompletedOn(),
			'original_screening_completed_on' => $this->getOriginalScreeningCompletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'additional_details' => $this->getAdditionalDetails(),
			'screening_recommendation_updated_on' => $this->getScreeningRecommendationUpdatedOn(),
			'is_skipped' => $this->getIsSkipped()
		);
	}

}
?>