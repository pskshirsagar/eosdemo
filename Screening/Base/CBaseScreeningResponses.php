<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningResponses
 * Do not add any new functions to this class.
 */

class CBaseScreeningResponses extends CEosPluralBase {

	/**
	 * @return CScreeningResponse[]
	 */
	public static function fetchScreeningResponses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningResponse', $objDatabase );
	}

	/**
	 * @return CScreeningResponse
	 */
	public static function fetchScreeningResponse( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningResponse', $objDatabase );
	}

	public static function fetchScreeningResponseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_responses', $objDatabase );
	}

	public static function fetchScreeningResponseById( $intId, $objDatabase ) {
		return self::fetchScreeningResponse( sprintf( 'SELECT * FROM screening_responses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningResponsesByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningResponses( sprintf( 'SELECT * FROM screening_responses WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningResponsesByScreeningId( $intScreeningId, $objDatabase ) {
		return self::fetchScreeningResponses( sprintf( 'SELECT * FROM screening_responses WHERE screening_id = %d', ( int ) $intScreeningId ), $objDatabase );
	}

	public static function fetchScreeningResponsesByScreeningApplicantId( $intScreeningApplicantId, $objDatabase ) {
		return self::fetchScreeningResponses( sprintf( 'SELECT * FROM screening_responses WHERE screening_applicant_id = %d', ( int ) $intScreeningApplicantId ), $objDatabase );
	}

}
?>