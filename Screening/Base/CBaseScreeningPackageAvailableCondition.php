<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningPackageAvailableCondition extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_package_available_conditions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScreeningPackageConditionTypeId;
	protected $m_intScreeningPackageChargeCodeAmountTypeId;
	protected $m_intBaseChargeCodeId;
	protected $m_intApplyToChargeCodeId;
	protected $m_intChargeTypeId;
	protected $m_strScreeningPackageAvailableConditionName;
	protected $m_intIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screening_package_condition_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageConditionTypeId', trim( $arrValues['screening_package_condition_type_id'] ) ); elseif( isset( $arrValues['screening_package_condition_type_id'] ) ) $this->setScreeningPackageConditionTypeId( $arrValues['screening_package_condition_type_id'] );
		if( isset( $arrValues['screening_package_charge_code_amount_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageChargeCodeAmountTypeId', trim( $arrValues['screening_package_charge_code_amount_type_id'] ) ); elseif( isset( $arrValues['screening_package_charge_code_amount_type_id'] ) ) $this->setScreeningPackageChargeCodeAmountTypeId( $arrValues['screening_package_charge_code_amount_type_id'] );
		if( isset( $arrValues['base_charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intBaseChargeCodeId', trim( $arrValues['base_charge_code_id'] ) ); elseif( isset( $arrValues['base_charge_code_id'] ) ) $this->setBaseChargeCodeId( $arrValues['base_charge_code_id'] );
		if( isset( $arrValues['apply_to_charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intApplyToChargeCodeId', trim( $arrValues['apply_to_charge_code_id'] ) ); elseif( isset( $arrValues['apply_to_charge_code_id'] ) ) $this->setApplyToChargeCodeId( $arrValues['apply_to_charge_code_id'] );
		if( isset( $arrValues['charge_type_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeTypeId', trim( $arrValues['charge_type_id'] ) ); elseif( isset( $arrValues['charge_type_id'] ) ) $this->setChargeTypeId( $arrValues['charge_type_id'] );
		if( isset( $arrValues['screening_package_available_condition_name'] ) && $boolDirectSet ) $this->set( 'm_strScreeningPackageAvailableConditionName', trim( stripcslashes( $arrValues['screening_package_available_condition_name'] ) ) ); elseif( isset( $arrValues['screening_package_available_condition_name'] ) ) $this->setScreeningPackageAvailableConditionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['screening_package_available_condition_name'] ) : $arrValues['screening_package_available_condition_name'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreeningPackageConditionTypeId( $intScreeningPackageConditionTypeId ) {
		$this->set( 'm_intScreeningPackageConditionTypeId', CStrings::strToIntDef( $intScreeningPackageConditionTypeId, NULL, false ) );
	}

	public function getScreeningPackageConditionTypeId() {
		return $this->m_intScreeningPackageConditionTypeId;
	}

	public function sqlScreeningPackageConditionTypeId() {
		return ( true == isset( $this->m_intScreeningPackageConditionTypeId ) ) ? ( string ) $this->m_intScreeningPackageConditionTypeId : 'NULL';
	}

	public function setScreeningPackageChargeCodeAmountTypeId( $intScreeningPackageChargeCodeAmountTypeId ) {
		$this->set( 'm_intScreeningPackageChargeCodeAmountTypeId', CStrings::strToIntDef( $intScreeningPackageChargeCodeAmountTypeId, NULL, false ) );
	}

	public function getScreeningPackageChargeCodeAmountTypeId() {
		return $this->m_intScreeningPackageChargeCodeAmountTypeId;
	}

	public function sqlScreeningPackageChargeCodeAmountTypeId() {
		return ( true == isset( $this->m_intScreeningPackageChargeCodeAmountTypeId ) ) ? ( string ) $this->m_intScreeningPackageChargeCodeAmountTypeId : 'NULL';
	}

	public function setBaseChargeCodeId( $intBaseChargeCodeId ) {
		$this->set( 'm_intBaseChargeCodeId', CStrings::strToIntDef( $intBaseChargeCodeId, NULL, false ) );
	}

	public function getBaseChargeCodeId() {
		return $this->m_intBaseChargeCodeId;
	}

	public function sqlBaseChargeCodeId() {
		return ( true == isset( $this->m_intBaseChargeCodeId ) ) ? ( string ) $this->m_intBaseChargeCodeId : 'NULL';
	}

	public function setApplyToChargeCodeId( $intApplyToChargeCodeId ) {
		$this->set( 'm_intApplyToChargeCodeId', CStrings::strToIntDef( $intApplyToChargeCodeId, NULL, false ) );
	}

	public function getApplyToChargeCodeId() {
		return $this->m_intApplyToChargeCodeId;
	}

	public function sqlApplyToChargeCodeId() {
		return ( true == isset( $this->m_intApplyToChargeCodeId ) ) ? ( string ) $this->m_intApplyToChargeCodeId : 'NULL';
	}

	public function setChargeTypeId( $intChargeTypeId ) {
		$this->set( 'm_intChargeTypeId', CStrings::strToIntDef( $intChargeTypeId, NULL, false ) );
	}

	public function getChargeTypeId() {
		return $this->m_intChargeTypeId;
	}

	public function sqlChargeTypeId() {
		return ( true == isset( $this->m_intChargeTypeId ) ) ? ( string ) $this->m_intChargeTypeId : 'NULL';
	}

	public function setScreeningPackageAvailableConditionName( $strScreeningPackageAvailableConditionName ) {
		$this->set( 'm_strScreeningPackageAvailableConditionName', CStrings::strTrimDef( $strScreeningPackageAvailableConditionName, -1, NULL, true ) );
	}

	public function getScreeningPackageAvailableConditionName() {
		return $this->m_strScreeningPackageAvailableConditionName;
	}

	public function sqlScreeningPackageAvailableConditionName() {
		return ( true == isset( $this->m_strScreeningPackageAvailableConditionName ) ) ? '\'' . addslashes( $this->m_strScreeningPackageAvailableConditionName ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, screening_package_condition_type_id, screening_package_charge_code_amount_type_id, base_charge_code_id, apply_to_charge_code_id, charge_type_id, screening_package_available_condition_name, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlScreeningPackageConditionTypeId() . ', ' .
 						$this->sqlScreeningPackageChargeCodeAmountTypeId() . ', ' .
 						$this->sqlBaseChargeCodeId() . ', ' .
 						$this->sqlApplyToChargeCodeId() . ', ' .
 						$this->sqlChargeTypeId() . ', ' .
 						$this->sqlScreeningPackageAvailableConditionName() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_condition_type_id = ' . $this->sqlScreeningPackageConditionTypeId() . ','; } elseif( true == array_key_exists( 'ScreeningPackageConditionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_condition_type_id = ' . $this->sqlScreeningPackageConditionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_charge_code_amount_type_id = ' . $this->sqlScreeningPackageChargeCodeAmountTypeId() . ','; } elseif( true == array_key_exists( 'ScreeningPackageChargeCodeAmountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_charge_code_amount_type_id = ' . $this->sqlScreeningPackageChargeCodeAmountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_charge_code_id = ' . $this->sqlBaseChargeCodeId() . ','; } elseif( true == array_key_exists( 'BaseChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' base_charge_code_id = ' . $this->sqlBaseChargeCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_to_charge_code_id = ' . $this->sqlApplyToChargeCodeId() . ','; } elseif( true == array_key_exists( 'ApplyToChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' apply_to_charge_code_id = ' . $this->sqlApplyToChargeCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_type_id = ' . $this->sqlChargeTypeId() . ','; } elseif( true == array_key_exists( 'ChargeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' charge_type_id = ' . $this->sqlChargeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_available_condition_name = ' . $this->sqlScreeningPackageAvailableConditionName() . ','; } elseif( true == array_key_exists( 'ScreeningPackageAvailableConditionName', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_available_condition_name = ' . $this->sqlScreeningPackageAvailableConditionName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'screening_package_condition_type_id' => $this->getScreeningPackageConditionTypeId(),
			'screening_package_charge_code_amount_type_id' => $this->getScreeningPackageChargeCodeAmountTypeId(),
			'base_charge_code_id' => $this->getBaseChargeCodeId(),
			'apply_to_charge_code_id' => $this->getApplyToChargeCodeId(),
			'charge_type_id' => $this->getChargeTypeId(),
			'screening_package_available_condition_name' => $this->getScreeningPackageAvailableConditionName(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>