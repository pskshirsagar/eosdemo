<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningReportRequestStatuses
 * Do not add any new functions to this class.
 */

class CBaseScreeningReportRequestStatuses extends CEosPluralBase {

	/**
	 * @return CScreeningReportRequestStatus[]
	 */
	public static function fetchScreeningReportRequestStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningReportRequestStatus', $objDatabase );
	}

	/**
	 * @return CScreeningReportRequestStatus
	 */
	public static function fetchScreeningReportRequestStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningReportRequestStatus', $objDatabase );
	}

	public static function fetchScreeningReportRequestStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_report_request_statuses', $objDatabase );
	}

	public static function fetchScreeningReportRequestStatusById( $intId, $objDatabase ) {
		return self::fetchScreeningReportRequestStatus( sprintf( 'SELECT * FROM screening_report_request_statuses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>