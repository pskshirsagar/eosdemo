<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageLeaseTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningPackageLeaseTypes extends CEosPluralBase {

	/**
	 * @return CScreeningPackageLeaseType[]
	 */
	public static function fetchScreeningPackageLeaseTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningPackageLeaseType::class, $objDatabase );
	}

	/**
	 * @return CScreeningPackageLeaseType
	 */
	public static function fetchScreeningPackageLeaseType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningPackageLeaseType::class, $objDatabase );
	}

	public static function fetchScreeningPackageLeaseTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_lease_types', $objDatabase );
	}

	public static function fetchScreeningPackageLeaseTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningPackageLeaseType( sprintf( 'SELECT * FROM screening_package_lease_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>