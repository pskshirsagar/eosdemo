<?php

class CBaseScreeningFilterType extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_filter_types';

	protected $m_intId;
	protected $m_strFilterType;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['filter_type'] ) && $boolDirectSet ) $this->set( 'm_strFilterType', trim( stripcslashes( $arrValues['filter_type'] ) ) ); elseif( isset( $arrValues['filter_type'] ) ) $this->setFilterType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['filter_type'] ) : $arrValues['filter_type'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setFilterType( $strFilterType ) {
		$this->set( 'm_strFilterType', CStrings::strTrimDef( $strFilterType, -1, NULL, true ) );
	}

	public function getFilterType() {
		return $this->m_strFilterType;
	}

	public function sqlFilterType() {
		return ( true == isset( $this->m_strFilterType ) ) ? '\'' . addslashes( $this->m_strFilterType ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'filter_type' => $this->getFilterType()
		);
	}

}
?>