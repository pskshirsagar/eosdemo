<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreenTypes
 * Do not add any new functions to this class.
 */

class CBaseScreenTypes extends CEosPluralBase {

	/**
	 * @return CScreenType[]
	 */
	public static function fetchScreenTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreenType::class, $objDatabase );
	}

	/**
	 * @return CScreenType
	 */
	public static function fetchScreenType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreenType::class, $objDatabase );
	}

	public static function fetchScreenTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screen_types', $objDatabase );
	}

	public static function fetchScreenTypeById( $intId, $objDatabase ) {
		return self::fetchScreenType( sprintf( 'SELECT * FROM screen_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreenTypesByChargeCodeId( $strChargeCodeId, $objDatabase ) {
		return self::fetchScreenTypes( sprintf( 'SELECT * FROM screen_types WHERE charge_code_id = \'%s\'', $strChargeCodeId ), $objDatabase );
	}

	public static function fetchScreenTypesByParentScreenTypeId( $intParentScreenTypeId, $objDatabase ) {
		return self::fetchScreenTypes( sprintf( 'SELECT * FROM screen_types WHERE parent_screen_type_id = %d', ( int ) $intParentScreenTypeId ), $objDatabase );
	}

}
?>