<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningSearchActions
 * Do not add any new functions to this class.
 */

class CBaseScreeningSearchActions extends CEosPluralBase {

	/**
	 * @return CScreeningSearchAction[]
	 */
	public static function fetchScreeningSearchActions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningSearchAction', $objDatabase );
	}

	/**
	 * @return CScreeningSearchAction
	 */
	public static function fetchScreeningSearchAction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningSearchAction', $objDatabase );
	}

	public static function fetchScreeningSearchActionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_search_actions', $objDatabase );
	}

	public static function fetchScreeningSearchActionById( $intId, $objDatabase ) {
		return self::fetchScreeningSearchAction( sprintf( 'SELECT * FROM screening_search_actions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>