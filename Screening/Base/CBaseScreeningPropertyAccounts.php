<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPropertyAccounts
 * Do not add any new functions to this class.
 */

class CBaseScreeningPropertyAccounts extends CEosPluralBase {

	/**
	 * @return CScreeningPropertyAccount[]
	 */
	public static function fetchScreeningPropertyAccounts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningPropertyAccount', $objDatabase );
	}

	/**
	 * @return CScreeningPropertyAccount
	 */
	public static function fetchScreeningPropertyAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPropertyAccount', $objDatabase );
	}

	public static function fetchScreeningPropertyAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_property_accounts', $objDatabase );
	}

	public static function fetchScreeningPropertyAccountById( $intId, $objDatabase ) {
		return self::fetchScreeningPropertyAccount( sprintf( 'SELECT * FROM screening_property_accounts WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningPropertyAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningPropertyAccounts( sprintf( 'SELECT * FROM screening_property_accounts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPropertyAccountsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchScreeningPropertyAccounts( sprintf( 'SELECT * FROM screening_property_accounts WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchScreeningPropertyAccountsByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchScreeningPropertyAccounts( sprintf( 'SELECT * FROM screening_property_accounts WHERE account_id = %d', ( int ) $intAccountId ), $objDatabase );
	}

}
?>