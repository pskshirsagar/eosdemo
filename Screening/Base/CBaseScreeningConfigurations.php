<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConfigurations
 * Do not add any new functions to this class.
 */

class CBaseScreeningConfigurations extends CEosPluralBase {

	/**
	 * @return CScreeningConfiguration[]
	 */
	public static function fetchScreeningConfigurations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningConfiguration', $objDatabase );
	}

	/**
	 * @return CScreeningConfiguration
	 */
	public static function fetchScreeningConfiguration( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningConfiguration', $objDatabase );
	}

	public static function fetchScreeningConfigurationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_configurations', $objDatabase );
	}

	public static function fetchScreeningConfigurationById( $intId, $objDatabase ) {
		return self::fetchScreeningConfiguration( sprintf( 'SELECT * FROM screening_configurations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningConfigurationsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningConfigurations( sprintf( 'SELECT * FROM screening_configurations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningConfigurationsByScreeningCumulationTypeId( $intScreeningCumulationTypeId, $objDatabase ) {
		return self::fetchScreeningConfigurations( sprintf( 'SELECT * FROM screening_configurations WHERE screening_cumulation_type_id = %d', ( int ) $intScreeningCumulationTypeId ), $objDatabase );
	}

	public static function fetchScreeningConfigurationsByScreeningScoringModelId( $intScreeningScoringModelId, $objDatabase ) {
		return self::fetchScreeningConfigurations( sprintf( 'SELECT * FROM screening_configurations WHERE screening_scoring_model_id = %d', ( int ) $intScreeningScoringModelId ), $objDatabase );
	}

	public static function fetchScreeningConfigurationsByScreeningConfigTypeId( $intScreeningConfigTypeId, $objDatabase ) {
		return self::fetchScreeningConfigurations( sprintf( 'SELECT * FROM screening_configurations WHERE screening_config_type_id = %d', ( int ) $intScreeningConfigTypeId ), $objDatabase );
	}

}
?>