<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CClientCriminalClassificationSubtypeKeywords
 * Do not add any new functions to this class.
 */

class CBaseClientCriminalClassificationSubtypeKeywords extends CEosPluralBase {

	/**
	 * @return CClientCriminalClassificationSubtypeKeyword[]
	 */
	public static function fetchClientCriminalClassificationSubtypeKeywords( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CClientCriminalClassificationSubtypeKeyword::class, $objDatabase );
	}

	/**
	 * @return CClientCriminalClassificationSubtypeKeyword
	 */
	public static function fetchClientCriminalClassificationSubtypeKeyword( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CClientCriminalClassificationSubtypeKeyword::class, $objDatabase );
	}

	public static function fetchClientCriminalClassificationSubtypeKeywordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'client_criminal_classification_subtype_keywords', $objDatabase );
	}

	public static function fetchClientCriminalClassificationSubtypeKeywordById( $intId, $objDatabase ) {
		return self::fetchClientCriminalClassificationSubtypeKeyword( sprintf( 'SELECT * FROM client_criminal_classification_subtype_keywords WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchClientCriminalClassificationSubtypeKeywordsByCid( $intCid, $objDatabase ) {
		return self::fetchClientCriminalClassificationSubtypeKeywords( sprintf( 'SELECT * FROM client_criminal_classification_subtype_keywords WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchClientCriminalClassificationSubtypeKeywordsByClientCriminalClassificationSubtypeId( $intClientCriminalClassificationSubtypeId, $objDatabase ) {
		return self::fetchClientCriminalClassificationSubtypeKeywords( sprintf( 'SELECT * FROM client_criminal_classification_subtype_keywords WHERE client_criminal_classification_subtype_id = %d', ( int ) $intClientCriminalClassificationSubtypeId ), $objDatabase );
	}

	public static function fetchClientCriminalClassificationSubtypeKeywordsByCriminalClassificationKeywordId( $intCriminalClassificationKeywordId, $objDatabase ) {
		return self::fetchClientCriminalClassificationSubtypeKeywords( sprintf( 'SELECT * FROM client_criminal_classification_subtype_keywords WHERE criminal_classification_keyword_id = %d', ( int ) $intCriminalClassificationKeywordId ), $objDatabase );
	}

}
?>