<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCreditAlertTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningCreditAlertTypes extends CEosPluralBase {

	/**
	 * @return CScreeningCreditAlertType[]
	 */
	public static function fetchScreeningCreditAlertTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningCreditAlertType', $objDatabase );
	}

	/**
	 * @return CScreeningCreditAlertType
	 */
	public static function fetchScreeningCreditAlertType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningCreditAlertType', $objDatabase );
	}

	public static function fetchScreeningCreditAlertTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_credit_alert_types', $objDatabase );
	}

	public static function fetchScreeningCreditAlertTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningCreditAlertType( sprintf( 'SELECT * FROM screening_credit_alert_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>