<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningTradelineTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningTradelineTypes extends CEosPluralBase {

	/**
	 * @return CScreeningTradelineType[]
	 */
	public static function fetchScreeningTradelineTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningTradelineType', $objDatabase );
	}

	/**
	 * @return CScreeningTradelineType
	 */
	public static function fetchScreeningTradelineType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningTradelineType', $objDatabase );
	}

	public static function fetchScreeningTradelineTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_tradeline_types', $objDatabase );
	}

	public static function fetchScreeningTradelineTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningTradelineType( sprintf( 'SELECT * FROM screening_tradeline_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>