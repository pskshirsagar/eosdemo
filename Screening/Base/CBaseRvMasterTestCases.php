<?php

class CBaseRvMasterTestCases extends CEosPluralBase {

	/**
	 * @return CRvMasterTestCase[]
	 */
	public static function fetchRvMasterTestCases( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRvMasterTestCase::class, $objDatabase );
	}

	/**
	 * @return CRvMasterTestCase
	 */
	public static function fetchRvMasterTestCase( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRvMasterTestCase::class, $objDatabase );
	}

	public static function fetchRvMasterTestCaseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'rv_master_test_cases', $objDatabase );
	}

	public static function fetchRvMasterTestCaseById( $intId, $objDatabase ) {
		return self::fetchRvMasterTestCase( sprintf( 'SELECT * FROM rv_master_test_cases WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchRvMasterTestCasesByCid( $intCid, $objDatabase ) {
		return self::fetchRvMasterTestCases( sprintf( 'SELECT * FROM rv_master_test_cases WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchRvMasterTestCasesByRvTestRunBatchId( $intRvTestRunBatchId, $objDatabase ) {
		return self::fetchRvMasterTestCases( sprintf( 'SELECT * FROM rv_master_test_cases WHERE rv_test_run_batch_id = %d', $intRvTestRunBatchId ), $objDatabase );
	}

	public static function fetchRvMasterTestCasesByScreeningId( $intScreeningId, $objDatabase ) {
		return self::fetchRvMasterTestCases( sprintf( 'SELECT * FROM rv_master_test_cases WHERE screening_id = %d', $intScreeningId ), $objDatabase );
	}

}
?>