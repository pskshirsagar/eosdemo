<?php

class CScreeningPackageConditionTypes extends CBaseScreeningPackageConditionTypes {

    public static function fetchScreeningPackageConditionTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CScreeningPackageConditionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchScreeningPackageConditionType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScreeningPackageConditionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchUnCachedScreeningPackageConditionTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CScreeningPackageConditionType', $objDatabase, false );
    }

    public static function fetchPublishedScreeningPackageConditionTypesAndAvailableConditionsByCid( $intCid, $objDatabase ) {
        $strSql = ' SELECT
                        spct.*,
                        spac.id AS screening_available_condition_id,
                        spac.cid,
                        spac.screening_package_condition_type_id,
                        spac.screening_package_charge_code_amount_type_id,
                        spac.base_charge_code_id,
                        spac.apply_to_charge_code_id,
                        spac.charge_type_id,
                        spac.condition_name as screening_available_condition_name
                    FROM
                        screening_package_condition_types spct
                        LEFT JOIN screening_available_conditions spac ON ( spac.cid = ' . ( int ) $intCid . ' AND spac.screening_package_condition_type_id = spct.id AND spac.is_published = 1 )
                    WHERE
                        spct.is_published = 1
                    ORDER BY
                        spct.screening_value_type_id,
                        spct.id,
                        spac.id ASC';

        return self::fetchUnCachedScreeningPackageConditionTypes( $strSql, $objDatabase );
    }
}
?>