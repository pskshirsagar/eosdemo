<?php

class CBaseScreeningApplicantChildTransaction extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_applicant_child_transactions';

	protected $m_intId;
	protected $m_intScreeningChildTransactionRequestTypeId;
	protected $m_intScreeningId;
	protected $m_intScreeningApplicantId;
	protected $m_intScreeningTransactionId;
	protected $m_intScreeningDataProviderId;
	protected $m_intScreenTypeId;
	protected $m_intChildTransactionId;
	protected $m_arrintReferenceRecordIds;
	protected $m_strSearchType;
	protected $m_strSearchReasonNote;
	protected $m_boolIsActive;
	protected $m_strUpdatedOn;
	protected $m_intUpdatedBy;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_child_transaction_request_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningChildTransactionRequestTypeId', trim( $arrValues['screening_child_transaction_request_type_id'] ) ); elseif( isset( $arrValues['screening_child_transaction_request_type_id'] ) ) $this->setScreeningChildTransactionRequestTypeId( $arrValues['screening_child_transaction_request_type_id'] );
		if( isset( $arrValues['screening_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningId', trim( $arrValues['screening_id'] ) ); elseif( isset( $arrValues['screening_id'] ) ) $this->setScreeningId( $arrValues['screening_id'] );
		if( isset( $arrValues['screening_applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicantId', trim( $arrValues['screening_applicant_id'] ) ); elseif( isset( $arrValues['screening_applicant_id'] ) ) $this->setScreeningApplicantId( $arrValues['screening_applicant_id'] );
		if( isset( $arrValues['screening_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningTransactionId', trim( $arrValues['screening_transaction_id'] ) ); elseif( isset( $arrValues['screening_transaction_id'] ) ) $this->setScreeningTransactionId( $arrValues['screening_transaction_id'] );
		if( isset( $arrValues['screening_data_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningDataProviderId', trim( $arrValues['screening_data_provider_id'] ) ); elseif( isset( $arrValues['screening_data_provider_id'] ) ) $this->setScreeningDataProviderId( $arrValues['screening_data_provider_id'] );
		if( isset( $arrValues['screen_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreenTypeId', trim( $arrValues['screen_type_id'] ) ); elseif( isset( $arrValues['screen_type_id'] ) ) $this->setScreenTypeId( $arrValues['screen_type_id'] );
		if( isset( $arrValues['child_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intChildTransactionId', trim( $arrValues['child_transaction_id'] ) ); elseif( isset( $arrValues['child_transaction_id'] ) ) $this->setChildTransactionId( $arrValues['child_transaction_id'] );
		if( isset( $arrValues['reference_record_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintReferenceRecordIds', trim( $arrValues['reference_record_ids'] ) ); elseif( isset( $arrValues['reference_record_ids'] ) ) $this->setReferenceRecordIds( $arrValues['reference_record_ids'] );
		if( isset( $arrValues['search_type'] ) && $boolDirectSet ) $this->set( 'm_strSearchType', trim( stripcslashes( $arrValues['search_type'] ) ) ); elseif( isset( $arrValues['search_type'] ) ) $this->setSearchType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['search_type'] ) : $arrValues['search_type'] );
		if( isset( $arrValues['search_reason_note'] ) && $boolDirectSet ) $this->set( 'm_strSearchReasonNote', trim( stripcslashes( $arrValues['search_reason_note'] ) ) ); elseif( isset( $arrValues['search_reason_note'] ) ) $this->setSearchReasonNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['search_reason_note'] ) : $arrValues['search_reason_note'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningChildTransactionRequestTypeId( $intScreeningChildTransactionRequestTypeId ) {
		$this->set( 'm_intScreeningChildTransactionRequestTypeId', CStrings::strToIntDef( $intScreeningChildTransactionRequestTypeId, NULL, false ) );
	}

	public function getScreeningChildTransactionRequestTypeId() {
		return $this->m_intScreeningChildTransactionRequestTypeId;
	}

	public function sqlScreeningChildTransactionRequestTypeId() {
		return ( true == isset( $this->m_intScreeningChildTransactionRequestTypeId ) ) ? ( string ) $this->m_intScreeningChildTransactionRequestTypeId : 'NULL';
	}

	public function setScreeningId( $intScreeningId ) {
		$this->set( 'm_intScreeningId', CStrings::strToIntDef( $intScreeningId, NULL, false ) );
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function sqlScreeningId() {
		return ( true == isset( $this->m_intScreeningId ) ) ? ( string ) $this->m_intScreeningId : 'NULL';
	}

	public function setScreeningApplicantId( $intScreeningApplicantId ) {
		$this->set( 'm_intScreeningApplicantId', CStrings::strToIntDef( $intScreeningApplicantId, NULL, false ) );
	}

	public function getScreeningApplicantId() {
		return $this->m_intScreeningApplicantId;
	}

	public function sqlScreeningApplicantId() {
		return ( true == isset( $this->m_intScreeningApplicantId ) ) ? ( string ) $this->m_intScreeningApplicantId : 'NULL';
	}

	public function setScreeningTransactionId( $intScreeningTransactionId ) {
		$this->set( 'm_intScreeningTransactionId', CStrings::strToIntDef( $intScreeningTransactionId, NULL, false ) );
	}

	public function getScreeningTransactionId() {
		return $this->m_intScreeningTransactionId;
	}

	public function sqlScreeningTransactionId() {
		return ( true == isset( $this->m_intScreeningTransactionId ) ) ? ( string ) $this->m_intScreeningTransactionId : 'NULL';
	}

	public function setScreeningDataProviderId( $intScreeningDataProviderId ) {
		$this->set( 'm_intScreeningDataProviderId', CStrings::strToIntDef( $intScreeningDataProviderId, NULL, false ) );
	}

	public function getScreeningDataProviderId() {
		return $this->m_intScreeningDataProviderId;
	}

	public function sqlScreeningDataProviderId() {
		return ( true == isset( $this->m_intScreeningDataProviderId ) ) ? ( string ) $this->m_intScreeningDataProviderId : 'NULL';
	}

	public function setScreenTypeId( $intScreenTypeId ) {
		$this->set( 'm_intScreenTypeId', CStrings::strToIntDef( $intScreenTypeId, NULL, false ) );
	}

	public function getScreenTypeId() {
		return $this->m_intScreenTypeId;
	}

	public function sqlScreenTypeId() {
		return ( true == isset( $this->m_intScreenTypeId ) ) ? ( string ) $this->m_intScreenTypeId : 'NULL';
	}

	public function setChildTransactionId( $intChildTransactionId ) {
		$this->set( 'm_intChildTransactionId', CStrings::strToIntDef( $intChildTransactionId, NULL, false ) );
	}

	public function getChildTransactionId() {
		return $this->m_intChildTransactionId;
	}

	public function sqlChildTransactionId() {
		return ( true == isset( $this->m_intChildTransactionId ) ) ? ( string ) $this->m_intChildTransactionId : 'NULL';
	}

	public function setReferenceRecordIds( $arrintReferenceRecordIds ) {
		$this->set( 'm_arrintReferenceRecordIds', CStrings::strToArrIntDef( $arrintReferenceRecordIds, NULL ) );
	}

	public function getReferenceRecordIds() {
		return $this->m_arrintReferenceRecordIds;
	}

	public function sqlReferenceRecordIds() {
		return ( true == isset( $this->m_arrintReferenceRecordIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintReferenceRecordIds, NULL ) . '\'' : 'NULL';
	}

	public function setSearchType( $strSearchType ) {
		$this->set( 'm_strSearchType', CStrings::strTrimDef( $strSearchType, 100, NULL, true ) );
	}

	public function getSearchType() {
		return $this->m_strSearchType;
	}

	public function sqlSearchType() {
		return ( true == isset( $this->m_strSearchType ) ) ? '\'' . addslashes( $this->m_strSearchType ) . '\'' : 'NULL';
	}

	public function setSearchReasonNote( $strSearchReasonNote ) {
		$this->set( 'm_strSearchReasonNote', CStrings::strTrimDef( $strSearchReasonNote, 250, NULL, true ) );
	}

	public function getSearchReasonNote() {
		return $this->m_strSearchReasonNote;
	}

	public function sqlSearchReasonNote() {
		return ( true == isset( $this->m_strSearchReasonNote ) ) ? '\'' . addslashes( $this->m_strSearchReasonNote ) . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, screening_child_transaction_request_type_id, screening_id, screening_applicant_id, screening_transaction_id, screening_data_provider_id, screen_type_id, child_transaction_id, reference_record_ids, search_type, search_reason_note, is_active, updated_on, updated_by, created_on, created_by )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlScreeningChildTransactionRequestTypeId() . ', ' .
 						$this->sqlScreeningId() . ', ' .
 						$this->sqlScreeningApplicantId() . ', ' .
 						$this->sqlScreeningTransactionId() . ', ' .
 						$this->sqlScreeningDataProviderId() . ', ' .
 						$this->sqlScreenTypeId() . ', ' .
 						$this->sqlChildTransactionId() . ', ' .
 						$this->sqlReferenceRecordIds() . ', ' .
 						$this->sqlSearchType() . ', ' .
 						$this->sqlSearchReasonNote() . ', ' .
 						$this->sqlIsActive() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_child_transaction_request_type_id = ' . $this->sqlScreeningChildTransactionRequestTypeId() . ','; } elseif( true == array_key_exists( 'ScreeningChildTransactionRequestTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_child_transaction_request_type_id = ' . $this->sqlScreeningChildTransactionRequestTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; } elseif( true == array_key_exists( 'ScreeningId', $this->getChangedColumns() ) ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId() . ','; } elseif( true == array_key_exists( 'ScreeningApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_id = ' . $this->sqlScreeningApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_transaction_id = ' . $this->sqlScreeningTransactionId() . ','; } elseif( true == array_key_exists( 'ScreeningTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' screening_transaction_id = ' . $this->sqlScreeningTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_data_provider_id = ' . $this->sqlScreeningDataProviderId() . ','; } elseif( true == array_key_exists( 'ScreeningDataProviderId', $this->getChangedColumns() ) ) { $strSql .= ' screening_data_provider_id = ' . $this->sqlScreeningDataProviderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId() . ','; } elseif( true == array_key_exists( 'ScreenTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' child_transaction_id = ' . $this->sqlChildTransactionId() . ','; } elseif( true == array_key_exists( 'ChildTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' child_transaction_id = ' . $this->sqlChildTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_record_ids = ' . $this->sqlReferenceRecordIds() . ','; } elseif( true == array_key_exists( 'ReferenceRecordIds', $this->getChangedColumns() ) ) { $strSql .= ' reference_record_ids = ' . $this->sqlReferenceRecordIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' search_type = ' . $this->sqlSearchType() . ','; } elseif( true == array_key_exists( 'SearchType', $this->getChangedColumns() ) ) { $strSql .= ' search_type = ' . $this->sqlSearchType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' search_reason_note = ' . $this->sqlSearchReasonNote() . ','; } elseif( true == array_key_exists( 'SearchReasonNote', $this->getChangedColumns() ) ) { $strSql .= ' search_reason_note = ' . $this->sqlSearchReasonNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_child_transaction_request_type_id' => $this->getScreeningChildTransactionRequestTypeId(),
			'screening_id' => $this->getScreeningId(),
			'screening_applicant_id' => $this->getScreeningApplicantId(),
			'screening_transaction_id' => $this->getScreeningTransactionId(),
			'screening_data_provider_id' => $this->getScreeningDataProviderId(),
			'screen_type_id' => $this->getScreenTypeId(),
			'child_transaction_id' => $this->getChildTransactionId(),
			'reference_record_ids' => $this->getReferenceRecordIds(),
			'search_type' => $this->getSearchType(),
			'search_reason_note' => $this->getSearchReasonNote(),
			'is_active' => $this->getIsActive(),
			'updated_on' => $this->getUpdatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy()
		);
	}

}
?>