<?php

class CBaseScreeningPackageConditionSubsetValue extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_package_condition_subset_values';

	protected $m_intId;
	protected $m_intScreeningPackageConditionSubsetId;
	protected $m_intScreeningPackageAvailableConditionId;
	protected $m_strDefaultValue;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_package_condition_subset_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageConditionSubsetId', trim( $arrValues['screening_package_condition_subset_id'] ) ); elseif( isset( $arrValues['screening_package_condition_subset_id'] ) ) $this->setScreeningPackageConditionSubsetId( $arrValues['screening_package_condition_subset_id'] );
		if( isset( $arrValues['screening_package_available_condition_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageAvailableConditionId', trim( $arrValues['screening_package_available_condition_id'] ) ); elseif( isset( $arrValues['screening_package_available_condition_id'] ) ) $this->setScreeningPackageAvailableConditionId( $arrValues['screening_package_available_condition_id'] );
		if( isset( $arrValues['default_value'] ) && $boolDirectSet ) $this->set( 'm_strDefaultValue', trim( stripcslashes( $arrValues['default_value'] ) ) ); elseif( isset( $arrValues['default_value'] ) ) $this->setDefaultValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['default_value'] ) : $arrValues['default_value'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningPackageConditionSubsetId( $intScreeningPackageConditionSubsetId ) {
		$this->set( 'm_intScreeningPackageConditionSubsetId', CStrings::strToIntDef( $intScreeningPackageConditionSubsetId, NULL, false ) );
	}

	public function getScreeningPackageConditionSubsetId() {
		return $this->m_intScreeningPackageConditionSubsetId;
	}

	public function sqlScreeningPackageConditionSubsetId() {
		return ( true == isset( $this->m_intScreeningPackageConditionSubsetId ) ) ? ( string ) $this->m_intScreeningPackageConditionSubsetId : 'NULL';
	}

	public function setScreeningPackageAvailableConditionId( $intScreeningPackageAvailableConditionId ) {
		$this->set( 'm_intScreeningPackageAvailableConditionId', CStrings::strToIntDef( $intScreeningPackageAvailableConditionId, NULL, false ) );
	}

	public function getScreeningPackageAvailableConditionId() {
		return $this->m_intScreeningPackageAvailableConditionId;
	}

	public function sqlScreeningPackageAvailableConditionId() {
		return ( true == isset( $this->m_intScreeningPackageAvailableConditionId ) ) ? ( string ) $this->m_intScreeningPackageAvailableConditionId : 'NULL';
	}

	public function setDefaultValue( $strDefaultValue ) {
		$this->set( 'm_strDefaultValue', CStrings::strTrimDef( $strDefaultValue, -1, NULL, true ) );
	}

	public function getDefaultValue() {
		return $this->m_strDefaultValue;
	}

	public function sqlDefaultValue() {
		return ( true == isset( $this->m_strDefaultValue ) ) ? '\'' . addslashes( $this->m_strDefaultValue ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, screening_package_condition_subset_id, screening_package_available_condition_id, default_value, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlScreeningPackageConditionSubsetId() . ', ' .
 						$this->sqlScreeningPackageAvailableConditionId() . ', ' .
 						$this->sqlDefaultValue() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_condition_subset_id = ' . $this->sqlScreeningPackageConditionSubsetId() . ','; } elseif( true == array_key_exists( 'ScreeningPackageConditionSubsetId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_condition_subset_id = ' . $this->sqlScreeningPackageConditionSubsetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_available_condition_id = ' . $this->sqlScreeningPackageAvailableConditionId() . ','; } elseif( true == array_key_exists( 'ScreeningPackageAvailableConditionId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_available_condition_id = ' . $this->sqlScreeningPackageAvailableConditionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_value = ' . $this->sqlDefaultValue() . ','; } elseif( true == array_key_exists( 'DefaultValue', $this->getChangedColumns() ) ) { $strSql .= ' default_value = ' . $this->sqlDefaultValue() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_package_condition_subset_id' => $this->getScreeningPackageConditionSubsetId(),
			'screening_package_available_condition_id' => $this->getScreeningPackageAvailableConditionId(),
			'default_value' => $this->getDefaultValue(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>