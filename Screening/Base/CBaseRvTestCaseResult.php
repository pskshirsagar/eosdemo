<?php

class CBaseRvTestCaseResult extends CEosSingularBase {

	const TABLE_NAME = 'public.rv_test_case_results';

	protected $m_intId;
	protected $m_intRvMasterTestCaseId;
	protected $m_intRvTestRunBatchId;
	protected $m_intScreeningId;
	protected $m_strScreeningResultDetails;
	protected $m_jsonScreeningResultDetails;
	protected $m_boolStatus;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolStatus = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['rv_master_test_case_id'] ) && $boolDirectSet ) $this->set( 'm_intRvMasterTestCaseId', trim( $arrValues['rv_master_test_case_id'] ) ); elseif( isset( $arrValues['rv_master_test_case_id'] ) ) $this->setRvMasterTestCaseId( $arrValues['rv_master_test_case_id'] );
		if( isset( $arrValues['rv_test_run_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intRvTestRunBatchId', trim( $arrValues['rv_test_run_batch_id'] ) ); elseif( isset( $arrValues['rv_test_run_batch_id'] ) ) $this->setRvTestRunBatchId( $arrValues['rv_test_run_batch_id'] );
		if( isset( $arrValues['screening_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningId', trim( $arrValues['screening_id'] ) ); elseif( isset( $arrValues['screening_id'] ) ) $this->setScreeningId( $arrValues['screening_id'] );
		if( isset( $arrValues['screening_result_details'] ) ) $this->set( 'm_strScreeningResultDetails', trim( $arrValues['screening_result_details'] ) );
		if( isset( $arrValues['status'] ) && $boolDirectSet ) $this->set( 'm_boolStatus', trim( stripcslashes( $arrValues['status'] ) ) ); elseif( isset( $arrValues['status'] ) ) $this->setStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status'] ) : $arrValues['status'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRvMasterTestCaseId( $intRvMasterTestCaseId ) {
		$this->set( 'm_intRvMasterTestCaseId', CStrings::strToIntDef( $intRvMasterTestCaseId, NULL, false ) );
	}

	public function getRvMasterTestCaseId() {
		return $this->m_intRvMasterTestCaseId;
	}

	public function sqlRvMasterTestCaseId() {
		return ( true == isset( $this->m_intRvMasterTestCaseId ) ) ? ( string ) $this->m_intRvMasterTestCaseId : 'NULL';
	}

	public function setRvTestRunBatchId( $intRvTestRunBatchId ) {
		$this->set( 'm_intRvTestRunBatchId', CStrings::strToIntDef( $intRvTestRunBatchId, NULL, false ) );
	}

	public function getRvTestRunBatchId() {
		return $this->m_intRvTestRunBatchId;
	}

	public function sqlRvTestRunBatchId() {
		return ( true == isset( $this->m_intRvTestRunBatchId ) ) ? ( string ) $this->m_intRvTestRunBatchId : 'NULL';
	}

	public function setScreeningId( $intScreeningId ) {
		$this->set( 'm_intScreeningId', CStrings::strToIntDef( $intScreeningId, NULL, false ) );
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function sqlScreeningId() {
		return ( true == isset( $this->m_intScreeningId ) ) ? ( string ) $this->m_intScreeningId : 'NULL';
	}

	public function setScreeningResultDetails( $jsonScreeningResultDetails ) {
		if( true == valObj( $jsonScreeningResultDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonScreeningResultDetails', $jsonScreeningResultDetails );
		} elseif( true == valJsonString( $jsonScreeningResultDetails ) ) {
			$this->set( 'm_jsonScreeningResultDetails', CStrings::strToJson( $jsonScreeningResultDetails ) );
		} else {
			$this->set( 'm_jsonScreeningResultDetails', NULL ); 
		}
		unset( $this->m_strScreeningResultDetails );
	}

	public function getScreeningResultDetails() {
		if( true == isset( $this->m_strScreeningResultDetails ) ) {
			$this->m_jsonScreeningResultDetails = CStrings::strToJson( $this->m_strScreeningResultDetails );
			unset( $this->m_strScreeningResultDetails );
		}
		return $this->m_jsonScreeningResultDetails;
	}

	public function sqlScreeningResultDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getScreeningResultDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getScreeningResultDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setStatus( $boolStatus ) {
		$this->set( 'm_boolStatus', CStrings::strToBool( $boolStatus ) );
	}

	public function getStatus() {
		return $this->m_boolStatus;
	}

	public function sqlStatus() {
		return ( true == isset( $this->m_boolStatus ) ) ? '\'' . ( true == ( bool ) $this->m_boolStatus ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, rv_master_test_case_id, rv_test_run_batch_id, screening_id, screening_result_details, status, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlRvMasterTestCaseId() . ', ' .
						$this->sqlRvTestRunBatchId() . ', ' .
						$this->sqlScreeningId() . ', ' .
						$this->sqlScreeningResultDetails() . ', ' .
						$this->sqlStatus() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rv_master_test_case_id = ' . $this->sqlRvMasterTestCaseId(). ',' ; } elseif( true == array_key_exists( 'RvMasterTestCaseId', $this->getChangedColumns() ) ) { $strSql .= ' rv_master_test_case_id = ' . $this->sqlRvMasterTestCaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rv_test_run_batch_id = ' . $this->sqlRvTestRunBatchId(). ',' ; } elseif( true == array_key_exists( 'RvTestRunBatchId', $this->getChangedColumns() ) ) { $strSql .= ' rv_test_run_batch_id = ' . $this->sqlRvTestRunBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId(). ',' ; } elseif( true == array_key_exists( 'ScreeningId', $this->getChangedColumns() ) ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_result_details = ' . $this->sqlScreeningResultDetails(). ',' ; } elseif( true == array_key_exists( 'ScreeningResultDetails', $this->getChangedColumns() ) ) { $strSql .= ' screening_result_details = ' . $this->sqlScreeningResultDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status = ' . $this->sqlStatus(). ',' ; } elseif( true == array_key_exists( 'Status', $this->getChangedColumns() ) ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'rv_master_test_case_id' => $this->getRvMasterTestCaseId(),
			'rv_test_run_batch_id' => $this->getRvTestRunBatchId(),
			'screening_id' => $this->getScreeningId(),
			'screening_result_details' => $this->getScreeningResultDetails(),
			'status' => $this->getStatus(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>