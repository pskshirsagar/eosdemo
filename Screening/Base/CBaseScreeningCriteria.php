<?php

class CBaseScreeningCriteria extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_criterias';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScreenTypeId;
	protected $m_strCriteriaName;
	protected $m_boolIsGlobal;
	protected $m_intParentCriteriaId;
	protected $m_intParentPackageId;
	protected $m_intDefaultScreeningDataProviderId;
	protected $m_boolIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsGlobal = true;
		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screen_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreenTypeId', trim( $arrValues['screen_type_id'] ) ); elseif( isset( $arrValues['screen_type_id'] ) ) $this->setScreenTypeId( $arrValues['screen_type_id'] );
		if( isset( $arrValues['criteria_name'] ) && $boolDirectSet ) $this->set( 'm_strCriteriaName', trim( stripcslashes( $arrValues['criteria_name'] ) ) ); elseif( isset( $arrValues['criteria_name'] ) ) $this->setCriteriaName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['criteria_name'] ) : $arrValues['criteria_name'] );
		if( isset( $arrValues['is_global'] ) && $boolDirectSet ) $this->set( 'm_boolIsGlobal', trim( stripcslashes( $arrValues['is_global'] ) ) ); elseif( isset( $arrValues['is_global'] ) ) $this->setIsGlobal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_global'] ) : $arrValues['is_global'] );
		if( isset( $arrValues['parent_criteria_id'] ) && $boolDirectSet ) $this->set( 'm_intParentCriteriaId', trim( $arrValues['parent_criteria_id'] ) ); elseif( isset( $arrValues['parent_criteria_id'] ) ) $this->setParentCriteriaId( $arrValues['parent_criteria_id'] );
		if( isset( $arrValues['parent_package_id'] ) && $boolDirectSet ) $this->set( 'm_intParentPackageId', trim( $arrValues['parent_package_id'] ) ); elseif( isset( $arrValues['parent_package_id'] ) ) $this->setParentPackageId( $arrValues['parent_package_id'] );
		if( isset( $arrValues['default_screening_data_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultScreeningDataProviderId', trim( $arrValues['default_screening_data_provider_id'] ) ); elseif( isset( $arrValues['default_screening_data_provider_id'] ) ) $this->setDefaultScreeningDataProviderId( $arrValues['default_screening_data_provider_id'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreenTypeId( $intScreenTypeId ) {
		$this->set( 'm_intScreenTypeId', CStrings::strToIntDef( $intScreenTypeId, NULL, false ) );
	}

	public function getScreenTypeId() {
		return $this->m_intScreenTypeId;
	}

	public function sqlScreenTypeId() {
		return ( true == isset( $this->m_intScreenTypeId ) ) ? ( string ) $this->m_intScreenTypeId : 'NULL';
	}

	public function setCriteriaName( $strCriteriaName ) {
		$this->set( 'm_strCriteriaName', CStrings::strTrimDef( $strCriteriaName, 250, NULL, true ) );
	}

	public function getCriteriaName() {
		return $this->m_strCriteriaName;
	}

	public function sqlCriteriaName() {
		return ( true == isset( $this->m_strCriteriaName ) ) ? '\'' . addslashes( $this->m_strCriteriaName ) . '\'' : 'NULL';
	}

	public function setIsGlobal( $boolIsGlobal ) {
		$this->set( 'm_boolIsGlobal', CStrings::strToBool( $boolIsGlobal ) );
	}

	public function getIsGlobal() {
		return $this->m_boolIsGlobal;
	}

	public function sqlIsGlobal() {
		return ( true == isset( $this->m_boolIsGlobal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsGlobal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setParentCriteriaId( $intParentCriteriaId ) {
		$this->set( 'm_intParentCriteriaId', CStrings::strToIntDef( $intParentCriteriaId, NULL, false ) );
	}

	public function getParentCriteriaId() {
		return $this->m_intParentCriteriaId;
	}

	public function sqlParentCriteriaId() {
		return ( true == isset( $this->m_intParentCriteriaId ) ) ? ( string ) $this->m_intParentCriteriaId : 'NULL';
	}

	public function setParentPackageId( $intParentPackageId ) {
		$this->set( 'm_intParentPackageId', CStrings::strToIntDef( $intParentPackageId, NULL, false ) );
	}

	public function getParentPackageId() {
		return $this->m_intParentPackageId;
	}

	public function sqlParentPackageId() {
		return ( true == isset( $this->m_intParentPackageId ) ) ? ( string ) $this->m_intParentPackageId : 'NULL';
	}

	public function setDefaultScreeningDataProviderId( $intDefaultScreeningDataProviderId ) {
		$this->set( 'm_intDefaultScreeningDataProviderId', CStrings::strToIntDef( $intDefaultScreeningDataProviderId, NULL, false ) );
	}

	public function getDefaultScreeningDataProviderId() {
		return $this->m_intDefaultScreeningDataProviderId;
	}

	public function sqlDefaultScreeningDataProviderId() {
		return ( true == isset( $this->m_intDefaultScreeningDataProviderId ) ) ? ( string ) $this->m_intDefaultScreeningDataProviderId : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, screen_type_id, criteria_name, is_global, parent_criteria_id, parent_package_id, default_screening_data_provider_id, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlScreenTypeId() . ', ' .
 						$this->sqlCriteriaName() . ', ' .
 						$this->sqlIsGlobal() . ', ' .
 						$this->sqlParentCriteriaId() . ', ' .
 						$this->sqlParentPackageId() . ', ' .
 						$this->sqlDefaultScreeningDataProviderId() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId() . ','; } elseif( true == array_key_exists( 'ScreenTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' criteria_name = ' . $this->sqlCriteriaName() . ','; } elseif( true == array_key_exists( 'CriteriaName', $this->getChangedColumns() ) ) { $strSql .= ' criteria_name = ' . $this->sqlCriteriaName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_global = ' . $this->sqlIsGlobal() . ','; } elseif( true == array_key_exists( 'IsGlobal', $this->getChangedColumns() ) ) { $strSql .= ' is_global = ' . $this->sqlIsGlobal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_criteria_id = ' . $this->sqlParentCriteriaId() . ','; } elseif( true == array_key_exists( 'ParentCriteriaId', $this->getChangedColumns() ) ) { $strSql .= ' parent_criteria_id = ' . $this->sqlParentCriteriaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_package_id = ' . $this->sqlParentPackageId() . ','; } elseif( true == array_key_exists( 'ParentPackageId', $this->getChangedColumns() ) ) { $strSql .= ' parent_package_id = ' . $this->sqlParentPackageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_screening_data_provider_id = ' . $this->sqlDefaultScreeningDataProviderId() . ','; } elseif( true == array_key_exists( 'DefaultScreeningDataProviderId', $this->getChangedColumns() ) ) { $strSql .= ' default_screening_data_provider_id = ' . $this->sqlDefaultScreeningDataProviderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'screen_type_id' => $this->getScreenTypeId(),
			'criteria_name' => $this->getCriteriaName(),
			'is_global' => $this->getIsGlobal(),
			'parent_criteria_id' => $this->getParentCriteriaId(),
			'parent_package_id' => $this->getParentPackageId(),
			'default_screening_data_provider_id' => $this->getDefaultScreeningDataProviderId(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>