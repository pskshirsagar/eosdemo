<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningScreenClientRates
 * Do not add any new functions to this class.
 */

class CBaseScreeningScreenClientRates extends CEosPluralBase {

	/**
	 * @return CScreeningScreenClientRate[]
	 */
	public static function fetchScreeningScreenClientRates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningScreenClientRate', $objDatabase );
	}

	/**
	 * @return CScreeningScreenClientRate
	 */
	public static function fetchScreeningScreenClientRate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningScreenClientRate', $objDatabase );
	}

	public static function fetchScreeningScreenClientRateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_screen_client_rates', $objDatabase );
	}

	public static function fetchScreeningScreenClientRateById( $intId, $objDatabase ) {
		return self::fetchScreeningScreenClientRate( sprintf( 'SELECT * FROM screening_screen_client_rates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningScreenClientRatesByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningScreenClientRates( sprintf( 'SELECT * FROM screening_screen_client_rates WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>