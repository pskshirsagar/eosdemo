<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConfigPreferenceTimeFilters
 * Do not add any new functions to this class.
 */

class CBaseScreeningConfigPreferenceTimeFilters extends CEosPluralBase {

	/**
	 * @return CScreeningConfigPreferenceTimeFilter[]
	 */
	public static function fetchScreeningConfigPreferenceTimeFilters( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningConfigPreferenceTimeFilter', $objDatabase );
	}

	/**
	 * @return CScreeningConfigPreferenceTimeFilter
	 */
	public static function fetchScreeningConfigPreferenceTimeFilter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningConfigPreferenceTimeFilter', $objDatabase );
	}

	public static function fetchScreeningConfigPreferenceTimeFilterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_config_preference_time_filters', $objDatabase );
	}

	public static function fetchScreeningConfigPreferenceTimeFilterById( $intId, $objDatabase ) {
		return self::fetchScreeningConfigPreferenceTimeFilter( sprintf( 'SELECT * FROM screening_config_preference_time_filters WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningConfigPreferenceTimeFiltersByScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		return self::fetchScreeningConfigPreferenceTimeFilters( sprintf( 'SELECT * FROM screening_config_preference_time_filters WHERE screening_config_preference_type_id = %d', ( int ) $intScreeningConfigPreferenceTypeId ), $objDatabase );
	}

}
?>