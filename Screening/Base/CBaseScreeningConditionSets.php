<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConditionSets
 * Do not add any new functions to this class.
 */

class CBaseScreeningConditionSets extends CEosPluralBase {

	/**
	 * @return CScreeningConditionSet[]
	 */
	public static function fetchScreeningConditionSets( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningConditionSet::class, $objDatabase );
	}

	/**
	 * @return CScreeningConditionSet
	 */
	public static function fetchScreeningConditionSet( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningConditionSet::class, $objDatabase );
	}

	public static function fetchScreeningConditionSetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_condition_sets', $objDatabase );
	}

	public static function fetchScreeningConditionSetById( $intId, $objDatabase ) {
		return self::fetchScreeningConditionSet( sprintf( 'SELECT * FROM screening_condition_sets WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningConditionSetsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningConditionSets( sprintf( 'SELECT * FROM screening_condition_sets WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningConditionSetsByScreeningRecommendationTypeId( $intScreeningRecommendationTypeId, $objDatabase ) {
		return self::fetchScreeningConditionSets( sprintf( 'SELECT * FROM screening_condition_sets WHERE screening_recommendation_type_id = %d', ( int ) $intScreeningRecommendationTypeId ), $objDatabase );
	}

	public static function fetchScreeningConditionSetsByFirstScreeningConditionSubsetId( $intFirstScreeningConditionSubsetId, $objDatabase ) {
		return self::fetchScreeningConditionSets( sprintf( 'SELECT * FROM screening_condition_sets WHERE first_screening_condition_subset_id = %d', ( int ) $intFirstScreeningConditionSubsetId ), $objDatabase );
	}

	public static function fetchScreeningConditionSetsBySecondScreeningConditionSubsetId( $intSecondScreeningConditionSubsetId, $objDatabase ) {
		return self::fetchScreeningConditionSets( sprintf( 'SELECT * FROM screening_condition_sets WHERE second_screening_condition_subset_id = %d', ( int ) $intSecondScreeningConditionSubsetId ), $objDatabase );
	}

	public static function fetchScreeningConditionSetsByScreeningPackageOperandTypeId( $intScreeningPackageOperandTypeId, $objDatabase ) {
		return self::fetchScreeningConditionSets( sprintf( 'SELECT * FROM screening_condition_sets WHERE screening_package_operand_type_id = %d', ( int ) $intScreeningPackageOperandTypeId ), $objDatabase );
	}

}
?>