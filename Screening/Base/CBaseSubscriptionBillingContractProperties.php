<?php

class CBaseSubscriptionBillingContractProperties extends CEosPluralBase {

	/**
	 * @return CSubscriptionBillingContractProperty[]
	 */
	public static function fetchSubscriptionBillingContractProperties( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubscriptionBillingContractProperty::class, $objDatabase );
	}

	/**
	 * @return CSubscriptionBillingContractProperty
	 */
	public static function fetchSubscriptionBillingContractProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubscriptionBillingContractProperty::class, $objDatabase );
	}

	public static function fetchSubscriptionBillingContractPropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subscription_billing_contract_properties', $objDatabase );
	}

	public static function fetchSubscriptionBillingContractPropertyById( $intId, $objDatabase ) {
		return self::fetchSubscriptionBillingContractProperty( sprintf( 'SELECT * FROM subscription_billing_contract_properties WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchSubscriptionBillingContractPropertiesByCid( $intCid, $objDatabase ) {
		return self::fetchSubscriptionBillingContractProperties( sprintf( 'SELECT * FROM subscription_billing_contract_properties WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchSubscriptionBillingContractPropertiesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchSubscriptionBillingContractProperties( sprintf( 'SELECT * FROM subscription_billing_contract_properties WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchSubscriptionBillingContractPropertiesByContractId( $intContractId, $objDatabase ) {
		return self::fetchSubscriptionBillingContractProperties( sprintf( 'SELECT * FROM subscription_billing_contract_properties WHERE contract_id = %d', $intContractId ), $objDatabase );
	}

}
?>