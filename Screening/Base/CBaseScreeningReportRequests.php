<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningReportRequests
 * Do not add any new functions to this class.
 */

class CBaseScreeningReportRequests extends CEosPluralBase {

	/**
	 * @return CScreeningReportRequest[]
	 */
	public static function fetchScreeningReportRequests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningReportRequest::class, $objDatabase );
	}

	/**
	 * @return CScreeningReportRequest
	 */
	public static function fetchScreeningReportRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningReportRequest::class, $objDatabase );
	}

	public static function fetchScreeningReportRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_report_requests', $objDatabase );
	}

	public static function fetchScreeningReportRequestById( $intId, $objDatabase ) {
		return self::fetchScreeningReportRequest( sprintf( 'SELECT * FROM screening_report_requests WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchScreeningReportRequestsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningReportRequests( sprintf( 'SELECT * FROM screening_report_requests WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchScreeningReportRequestsByScreeningId( $intScreeningId, $objDatabase ) {
		return self::fetchScreeningReportRequests( sprintf( 'SELECT * FROM screening_report_requests WHERE screening_id = %d', $intScreeningId ), $objDatabase );
	}

	public static function fetchScreeningReportRequestsByScreeningApplicantId( $intScreeningApplicantId, $objDatabase ) {
		return self::fetchScreeningReportRequests( sprintf( 'SELECT * FROM screening_report_requests WHERE screening_applicant_id = %d', $intScreeningApplicantId ), $objDatabase );
	}

	public static function fetchScreeningReportRequestsByScreeningReportRequestStatusId( $intScreeningReportRequestStatusId, $objDatabase ) {
		return self::fetchScreeningReportRequests( sprintf( 'SELECT * FROM screening_report_requests WHERE screening_report_request_status_id = %d', $intScreeningReportRequestStatusId ), $objDatabase );
	}

	public static function fetchScreeningReportRequestsByScreeningReportRequestSourceId( $intScreeningReportRequestSourceId, $objDatabase ) {
		return self::fetchScreeningReportRequests( sprintf( 'SELECT * FROM screening_report_requests WHERE screening_report_request_source_id = %d', $intScreeningReportRequestSourceId ), $objDatabase );
	}

}
?>