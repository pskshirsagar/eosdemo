<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CSelectionModeTypes
 * Do not add any new functions to this class.
 */

class CBaseSelectionModeTypes extends CEosPluralBase {

	/**
	 * @return CSelectionModeType[]
	 */
	public static function fetchSelectionModeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSelectionModeType::class, $objDatabase );
	}

	/**
	 * @return CSelectionModeType
	 */
	public static function fetchSelectionModeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSelectionModeType::class, $objDatabase );
	}

	public static function fetchSelectionModeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'selection_mode_types', $objDatabase );
	}

	public static function fetchSelectionModeTypeById( $intId, $objDatabase ) {
		return self::fetchSelectionModeType( sprintf( 'SELECT * FROM selection_mode_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>