<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CCriminalClassificationSettings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCriminalClassificationSettings extends CEosPluralBase {

	/**
	 * @return CCriminalClassificationSetting[]
	 */
	public static function fetchCriminalClassificationSettings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCriminalClassificationSetting', $objDatabase );
	}

	/**
	 * @return CCriminalClassificationSetting
	 */
	public static function fetchCriminalClassificationSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCriminalClassificationSetting', $objDatabase );
	}

	public static function fetchCriminalClassificationSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'criminal_classification_settings', $objDatabase );
	}

	public static function fetchCriminalClassificationSettingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCriminalClassificationSetting( sprintf( 'SELECT * FROM criminal_classification_settings WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCriminalClassificationSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchCriminalClassificationSettings( sprintf( 'SELECT * FROM criminal_classification_settings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCriminalClassificationSettingsByScreeningPackageIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {
		return self::fetchCriminalClassificationSettings( sprintf( 'SELECT * FROM criminal_classification_settings WHERE screening_package_id = %d AND cid = %d', ( int ) $intScreeningPackageId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCriminalClassificationSettingsByCriminalClassificationTypeIdByCid( $intCriminalClassificationTypeId, $intCid, $objDatabase ) {
		return self::fetchCriminalClassificationSettings( sprintf( 'SELECT * FROM criminal_classification_settings WHERE criminal_classification_type_id = %d AND cid = %d', ( int ) $intCriminalClassificationTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCriminalClassificationSettingsByFelonyScreeningPackageConditionSetIdByCid( $intFelonyScreeningPackageConditionSetId, $intCid, $objDatabase ) {
		return self::fetchCriminalClassificationSettings( sprintf( 'SELECT * FROM criminal_classification_settings WHERE felony_screening_package_condition_set_id = %d AND cid = %d', ( int ) $intFelonyScreeningPackageConditionSetId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCriminalClassificationSettingsByMisdemeanorScreeningPackageConditionSetIdByCid( $intMisdemeanorScreeningPackageConditionSetId, $intCid, $objDatabase ) {
		return self::fetchCriminalClassificationSettings( sprintf( 'SELECT * FROM criminal_classification_settings WHERE misdemeanor_screening_package_condition_set_id = %d AND cid = %d', ( int ) $intMisdemeanorScreeningPackageConditionSetId, ( int ) $intCid ), $objDatabase );
	}

}
?>