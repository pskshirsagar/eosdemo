<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageSettings
 * Do not add any new functions to this class.
 */

class CBaseScreeningPackageSettings extends CEosPluralBase {

	/**
	 * @return CScreeningPackageSetting[]
	 */
	public static function fetchScreeningPackageSettings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageSetting', $objDatabase );
	}

	/**
	 * @return CScreeningPackageSetting
	 */
	public static function fetchScreeningPackageSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageSetting', $objDatabase );
	}

	public static function fetchScreeningPackageSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_settings', $objDatabase );
	}

	public static function fetchScreeningPackageSettingById( $intId, $objDatabase ) {
		return self::fetchScreeningPackageSetting( sprintf( 'SELECT * FROM screening_package_settings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningPackageSettingsByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {
		return self::fetchScreeningPackageSettings( sprintf( 'SELECT * FROM screening_package_settings WHERE screening_package_id = %d', ( int ) $intScreeningPackageId ), $objDatabase );
	}

	public static function fetchScreeningPackageSettingsByScreeningPackageConditionSetId( $intScreeningPackageConditionSetId, $objDatabase ) {
		return self::fetchScreeningPackageSettings( sprintf( 'SELECT * FROM screening_package_settings WHERE screening_package_condition_set_id = %d', ( int ) $intScreeningPackageConditionSetId ), $objDatabase );
	}

	public static function fetchScreeningPackageSettingsByScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		return self::fetchScreeningPackageSettings( sprintf( 'SELECT * FROM screening_package_settings WHERE screening_config_preference_type_id = %d', ( int ) $intScreeningConfigPreferenceTypeId ), $objDatabase );
	}

}
?>