<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningDataProviderTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningDataProviderTypes extends CEosPluralBase {

	/**
	 * @return CScreeningDataProviderType[]
	 */
	public static function fetchScreeningDataProviderTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningDataProviderType', $objDatabase );
	}

	/**
	 * @return CScreeningDataProviderType
	 */
	public static function fetchScreeningDataProviderType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningDataProviderType', $objDatabase );
	}

	public static function fetchScreeningDataProviderTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_data_provider_types', $objDatabase );
	}

	public static function fetchScreeningDataProviderTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningDataProviderType( sprintf( 'SELECT * FROM screening_data_provider_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>