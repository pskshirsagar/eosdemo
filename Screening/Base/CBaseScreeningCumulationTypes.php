<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCumulationTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningCumulationTypes extends CEosPluralBase {

	/**
	 * @return CScreeningCumulationType[]
	 */
	public static function fetchScreeningCumulationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningCumulationType', $objDatabase );
	}

	/**
	 * @return CScreeningCumulationType
	 */
	public static function fetchScreeningCumulationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningCumulationType', $objDatabase );
	}

	public static function fetchScreeningCumulationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_cumulation_types', $objDatabase );
	}

	public static function fetchScreeningCumulationTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningCumulationType( sprintf( 'SELECT * FROM screening_cumulation_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>