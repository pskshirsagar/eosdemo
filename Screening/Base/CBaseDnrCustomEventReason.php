<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDnrCustomEventReason extends CEosSingularBase {

	const TABLE_NAME = 'public.dnr_custom_event_reasons';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDelinquentEventTypeId;
	protected $m_intDelinquentEventReasonId;
	protected $m_strCustomReason;
	protected $m_boolIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['delinquent_event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquentEventTypeId', trim( $arrValues['delinquent_event_type_id'] ) ); elseif( isset( $arrValues['delinquent_event_type_id'] ) ) $this->setDelinquentEventTypeId( $arrValues['delinquent_event_type_id'] );
		if( isset( $arrValues['delinquent_event_reason_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquentEventReasonId', trim( $arrValues['delinquent_event_reason_id'] ) ); elseif( isset( $arrValues['delinquent_event_reason_id'] ) ) $this->setDelinquentEventReasonId( $arrValues['delinquent_event_reason_id'] );
		if( isset( $arrValues['custom_reason'] ) && $boolDirectSet ) $this->set( 'm_strCustomReason', trim( stripcslashes( $arrValues['custom_reason'] ) ) ); elseif( isset( $arrValues['custom_reason'] ) ) $this->setCustomReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['custom_reason'] ) : $arrValues['custom_reason'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDelinquentEventTypeId( $intDelinquentEventTypeId ) {
		$this->set( 'm_intDelinquentEventTypeId', CStrings::strToIntDef( $intDelinquentEventTypeId, NULL, false ) );
	}

	public function getDelinquentEventTypeId() {
		return $this->m_intDelinquentEventTypeId;
	}

	public function sqlDelinquentEventTypeId() {
		return ( true == isset( $this->m_intDelinquentEventTypeId ) ) ? ( string ) $this->m_intDelinquentEventTypeId : 'NULL';
	}

	public function setDelinquentEventReasonId( $intDelinquentEventReasonId ) {
		$this->set( 'm_intDelinquentEventReasonId', CStrings::strToIntDef( $intDelinquentEventReasonId, NULL, false ) );
	}

	public function getDelinquentEventReasonId() {
		return $this->m_intDelinquentEventReasonId;
	}

	public function sqlDelinquentEventReasonId() {
		return ( true == isset( $this->m_intDelinquentEventReasonId ) ) ? ( string ) $this->m_intDelinquentEventReasonId : 'NULL';
	}

	public function setCustomReason( $strCustomReason ) {
		$this->set( 'm_strCustomReason', CStrings::strTrimDef( $strCustomReason, 150, NULL, true ) );
	}

	public function getCustomReason() {
		return $this->m_strCustomReason;
	}

	public function sqlCustomReason() {
		return ( true == isset( $this->m_strCustomReason ) ) ? '\'' . addslashes( $this->m_strCustomReason ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, delinquent_event_type_id, delinquent_event_reason_id, custom_reason, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlDelinquentEventTypeId() . ', ' .
 						$this->sqlDelinquentEventReasonId() . ', ' .
 						$this->sqlCustomReason() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquent_event_type_id = ' . $this->sqlDelinquentEventTypeId() . ','; } elseif( true == array_key_exists( 'DelinquentEventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' delinquent_event_type_id = ' . $this->sqlDelinquentEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquent_event_reason_id = ' . $this->sqlDelinquentEventReasonId() . ','; } elseif( true == array_key_exists( 'DelinquentEventReasonId', $this->getChangedColumns() ) ) { $strSql .= ' delinquent_event_reason_id = ' . $this->sqlDelinquentEventReasonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_reason = ' . $this->sqlCustomReason() . ','; } elseif( true == array_key_exists( 'CustomReason', $this->getChangedColumns() ) ) { $strSql .= ' custom_reason = ' . $this->sqlCustomReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'delinquent_event_type_id' => $this->getDelinquentEventTypeId(),
			'delinquent_event_reason_id' => $this->getDelinquentEventReasonId(),
			'custom_reason' => $this->getCustomReason(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>