<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningStates
 * Do not add any new functions to this class.
 */

class CBaseScreeningStates extends CEosPluralBase {

	/**
	 * @return CScreeningState[]
	 */
	public static function fetchScreeningStates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningState', $objDatabase );
	}

	/**
	 * @return CScreeningState
	 */
	public static function fetchScreeningState( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningState', $objDatabase );
	}

	public static function fetchScreeningStateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_states', $objDatabase );
	}

	public static function fetchScreeningStateById( $intId, $objDatabase ) {
		return self::fetchScreeningState( sprintf( 'SELECT * FROM screening_states WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>