<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageChargeCodeAmountTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningPackageChargeCodeAmountTypes extends CEosPluralBase {

	/**
	 * @return CScreeningPackageChargeCodeAmountType[]
	 */
	public static function fetchScreeningPackageChargeCodeAmountTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageChargeCodeAmountType', $objDatabase );
	}

	/**
	 * @return CScreeningPackageChargeCodeAmountType
	 */
	public static function fetchScreeningPackageChargeCodeAmountType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageChargeCodeAmountType', $objDatabase );
	}

	public static function fetchScreeningPackageChargeCodeAmountTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_charge_code_amount_types', $objDatabase );
	}

	public static function fetchScreeningPackageChargeCodeAmountTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningPackageChargeCodeAmountType( sprintf( 'SELECT * FROM screening_package_charge_code_amount_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>