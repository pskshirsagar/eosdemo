<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDisputeStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseDisputeStatusTypes extends CEosPluralBase {

	/**
	 * @return CDisputeStatusType[]
	 */
	public static function fetchDisputeStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDisputeStatusType', $objDatabase );
	}

	/**
	 * @return CDisputeStatusType
	 */
	public static function fetchDisputeStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDisputeStatusType', $objDatabase );
	}

	public static function fetchDisputeStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'dispute_status_types', $objDatabase );
	}

	public static function fetchDisputeStatusTypeById( $intId, $objDatabase ) {
		return self::fetchDisputeStatusType( sprintf( 'SELECT * FROM dispute_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>