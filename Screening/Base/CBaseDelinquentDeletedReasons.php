<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDelinquentDeletedReasons
 * Do not add any new functions to this class.
 */

class CBaseDelinquentDeletedReasons extends CEosPluralBase {

	/**
	 * @return CDelinquentDeletedReason[]
	 */
	public static function fetchDelinquentDeletedReasons( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDelinquentDeletedReason', $objDatabase );
	}

	/**
	 * @return CDelinquentDeletedReason
	 */
	public static function fetchDelinquentDeletedReason( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDelinquentDeletedReason', $objDatabase );
	}

	public static function fetchDelinquentDeletedReasonCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'delinquent_deleted_reasons', $objDatabase );
	}

	public static function fetchDelinquentDeletedReasonById( $intId, $objDatabase ) {
		return self::fetchDelinquentDeletedReason( sprintf( 'SELECT * FROM delinquent_deleted_reasons WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>