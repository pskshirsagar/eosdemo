<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningProviderBaseRates
 * Do not add any new functions to this class.
 */

class CBaseScreeningProviderBaseRates extends CEosPluralBase {

	/**
	 * @return CScreeningProviderBaseRate[]
	 */
	public static function fetchScreeningProviderBaseRates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningProviderBaseRate', $objDatabase );
	}

	/**
	 * @return CScreeningProviderBaseRate
	 */
	public static function fetchScreeningProviderBaseRate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningProviderBaseRate', $objDatabase );
	}

	public static function fetchScreeningProviderBaseRateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_provider_base_rates', $objDatabase );
	}

	public static function fetchScreeningProviderBaseRateById( $intId, $objDatabase ) {
		return self::fetchScreeningProviderBaseRate( sprintf( 'SELECT * FROM screening_provider_base_rates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningProviderBaseRatesByScreeningDataProviderId( $intScreeningDataProviderId, $objDatabase ) {
		return self::fetchScreeningProviderBaseRates( sprintf( 'SELECT * FROM screening_provider_base_rates WHERE screening_data_provider_id = %d', ( int ) $intScreeningDataProviderId ), $objDatabase );
	}

}
?>