<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningTransactionConditions
 * Do not add any new functions to this class.
 */

class CBaseScreeningTransactionConditions extends CEosPluralBase {

	/**
	 * @return CScreeningTransactionCondition[]
	 */
	public static function fetchScreeningTransactionConditions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningTransactionCondition::class, $objDatabase );
	}

	/**
	 * @return CScreeningTransactionCondition
	 */
	public static function fetchScreeningTransactionCondition( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningTransactionCondition::class, $objDatabase );
	}

	public static function fetchScreeningTransactionConditionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_transaction_conditions', $objDatabase );
	}

	public static function fetchScreeningTransactionConditionById( $intId, $objDatabase ) {
		return self::fetchScreeningTransactionCondition( sprintf( 'SELECT * FROM screening_transaction_conditions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningTransactionConditionsByScreeningTransactionId( $intScreeningTransactionId, $objDatabase ) {
		return self::fetchScreeningTransactionConditions( sprintf( 'SELECT * FROM screening_transaction_conditions WHERE screening_transaction_id = %d', ( int ) $intScreeningTransactionId ), $objDatabase );
	}

	public static function fetchScreeningTransactionConditionsByScreeningConditionSetId( $intScreeningConditionSetId, $objDatabase ) {
		return self::fetchScreeningTransactionConditions( sprintf( 'SELECT * FROM screening_transaction_conditions WHERE screening_condition_set_id = %d', ( int ) $intScreeningConditionSetId ), $objDatabase );
	}

	public static function fetchScreeningTransactionConditionsByScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		return self::fetchScreeningTransactionConditions( sprintf( 'SELECT * FROM screening_transaction_conditions WHERE screening_config_preference_type_id = %d', ( int ) $intScreeningConfigPreferenceTypeId ), $objDatabase );
	}

}
?>