<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConditionSubsets
 * Do not add any new functions to this class.
 */

class CBaseScreeningConditionSubsets extends CEosPluralBase {

	/**
	 * @return CScreeningConditionSubset[]
	 */
	public static function fetchScreeningConditionSubsets( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningConditionSubset::class, $objDatabase );
	}

	/**
	 * @return CScreeningConditionSubset
	 */
	public static function fetchScreeningConditionSubset( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningConditionSubset::class, $objDatabase );
	}

	public static function fetchScreeningConditionSubsetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_condition_subsets', $objDatabase );
	}

	public static function fetchScreeningConditionSubsetById( $intId, $objDatabase ) {
		return self::fetchScreeningConditionSubset( sprintf( 'SELECT * FROM screening_condition_subsets WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningConditionSubsetsByScreeningConditionSetId( $intScreeningConditionSetId, $objDatabase ) {
		return self::fetchScreeningConditionSubsets( sprintf( 'SELECT * FROM screening_condition_subsets WHERE screening_condition_set_id = %d', ( int ) $intScreeningConditionSetId ), $objDatabase );
	}

	public static function fetchScreeningConditionSubsetsBySortOrderId( $intSortOrderId, $objDatabase ) {
		return self::fetchScreeningConditionSubsets( sprintf( 'SELECT * FROM screening_condition_subsets WHERE sort_order_id = %d', ( int ) $intSortOrderId ), $objDatabase );
	}

}
?>