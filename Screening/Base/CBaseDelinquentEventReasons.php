<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDelinquentEventReasons
 * Do not add any new functions to this class.
 */

class CBaseDelinquentEventReasons extends CEosPluralBase {

	/**
	 * @return CDelinquentEventReason[]
	 */
	public static function fetchDelinquentEventReasons( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDelinquentEventReason', $objDatabase );
	}

	/**
	 * @return CDelinquentEventReason
	 */
	public static function fetchDelinquentEventReason( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDelinquentEventReason', $objDatabase );
	}

	public static function fetchDelinquentEventReasonCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'delinquent_event_reasons', $objDatabase );
	}

	public static function fetchDelinquentEventReasonById( $intId, $objDatabase ) {
		return self::fetchDelinquentEventReason( sprintf( 'SELECT * FROM delinquent_event_reasons WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDelinquentEventReasonsByDelinquentEventTypeId( $intDelinquentEventTypeId, $objDatabase ) {
		return self::fetchDelinquentEventReasons( sprintf( 'SELECT * FROM delinquent_event_reasons WHERE delinquent_event_type_id = %d', ( int ) $intDelinquentEventTypeId ), $objDatabase );
	}

}
?>