<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningTransactionErrorLogs
 * Do not add any new functions to this class.
 */

class CBaseScreeningTransactionErrorLogs extends CEosPluralBase {

	/**
	 * @return CScreeningTransactionErrorLog[]
	 */
	public static function fetchScreeningTransactionErrorLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningTransactionErrorLog::class, $objDatabase );
	}

	/**
	 * @return CScreeningTransactionErrorLog
	 */
	public static function fetchScreeningTransactionErrorLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningTransactionErrorLog::class, $objDatabase );
	}

	public static function fetchScreeningTransactionErrorLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_transaction_error_logs', $objDatabase );
	}

	public static function fetchScreeningTransactionErrorLogById( $intId, $objDatabase ) {
		return self::fetchScreeningTransactionErrorLog( sprintf( 'SELECT * FROM screening_transaction_error_logs WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchScreeningTransactionErrorLogsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningTransactionErrorLogs( sprintf( 'SELECT * FROM screening_transaction_error_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchScreeningTransactionErrorLogsByScreeningTransactionId( $intScreeningTransactionId, $objDatabase ) {
		return self::fetchScreeningTransactionErrorLogs( sprintf( 'SELECT * FROM screening_transaction_error_logs WHERE screening_transaction_id = %d', $intScreeningTransactionId ), $objDatabase );
	}

	public static function fetchScreeningTransactionErrorLogsByScreeningDataProviderId( $intScreeningDataProviderId, $objDatabase ) {
		return self::fetchScreeningTransactionErrorLogs( sprintf( 'SELECT * FROM screening_transaction_error_logs WHERE screening_data_provider_id = %d', $intScreeningDataProviderId ), $objDatabase );
	}

	public static function fetchScreeningTransactionErrorLogsByScreeningErrorTypeId( $intScreeningErrorTypeId, $objDatabase ) {
		return self::fetchScreeningTransactionErrorLogs( sprintf( 'SELECT * FROM screening_transaction_error_logs WHERE screening_error_type_id = %d', $intScreeningErrorTypeId ), $objDatabase );
	}

}
?>