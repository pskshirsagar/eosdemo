<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConfigPreferences
 * Do not add any new functions to this class.
 */

class CBaseScreeningConfigPreferences extends CEosPluralBase {

	/**
	 * @return CScreeningConfigPreference[]
	 */
	public static function fetchScreeningConfigPreferences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningConfigPreference', $objDatabase );
	}

	/**
	 * @return CScreeningConfigPreference
	 */
	public static function fetchScreeningConfigPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningConfigPreference', $objDatabase );
	}

	public static function fetchScreeningConfigPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_config_preferences', $objDatabase );
	}

	public static function fetchScreeningConfigPreferenceById( $intId, $objDatabase ) {
		return self::fetchScreeningConfigPreference( sprintf( 'SELECT * FROM screening_config_preferences WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningConfigPreferencesByScreeningConfigurationId( $intScreeningConfigurationId, $objDatabase ) {
		return self::fetchScreeningConfigPreferences( sprintf( 'SELECT * FROM screening_config_preferences WHERE screening_configuration_id = %d', ( int ) $intScreeningConfigurationId ), $objDatabase );
	}

	public static function fetchScreeningConfigPreferencesByScreeningApplicantTypeId( $intScreeningApplicantTypeId, $objDatabase ) {
		return self::fetchScreeningConfigPreferences( sprintf( 'SELECT * FROM screening_config_preferences WHERE screening_applicant_type_id = %d', ( int ) $intScreeningApplicantTypeId ), $objDatabase );
	}

	public static function fetchScreeningConfigPreferencesByScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		return self::fetchScreeningConfigPreferences( sprintf( 'SELECT * FROM screening_config_preferences WHERE screening_config_preference_type_id = %d', ( int ) $intScreeningConfigPreferenceTypeId ), $objDatabase );
	}

	public static function fetchScreeningConfigPreferencesByScreeningRecommendationTypeId( $intScreeningRecommendationTypeId, $objDatabase ) {
		return self::fetchScreeningConfigPreferences( sprintf( 'SELECT * FROM screening_config_preferences WHERE screening_recommendation_type_id = %d', ( int ) $intScreeningRecommendationTypeId ), $objDatabase );
	}

}
?>