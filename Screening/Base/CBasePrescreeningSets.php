<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CPrescreeningSets
 * Do not add any new functions to this class.
 */

class CBasePrescreeningSets extends CEosPluralBase {

	/**
	 * @return CPrescreeningSet[]
	 */
	public static function fetchPrescreeningSets( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPrescreeningSet', $objDatabase );
	}

	/**
	 * @return CPrescreeningSet
	 */
	public static function fetchPrescreeningSet( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPrescreeningSet', $objDatabase );
	}

	public static function fetchPrescreeningSetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'prescreening_sets', $objDatabase );
	}

	public static function fetchPrescreeningSetById( $intId, $objDatabase ) {
		return self::fetchPrescreeningSet( sprintf( 'SELECT * FROM prescreening_sets WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>