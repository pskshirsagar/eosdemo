<?php

class CBaseRvReportCriteriaRangeType extends CEosSingularBase {

	const TABLE_NAME = 'public.rv_report_criteria_range_types';

	protected $m_intId;
	protected $m_intScreeningConfigPreferenceTypeId;
	protected $m_intMinValue;
	protected $m_intMaxValue;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screening_config_preference_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningConfigPreferenceTypeId', trim( $arrValues['screening_config_preference_type_id'] ) ); elseif( isset( $arrValues['screening_config_preference_type_id'] ) ) $this->setScreeningConfigPreferenceTypeId( $arrValues['screening_config_preference_type_id'] );
		if( isset( $arrValues['min_value'] ) && $boolDirectSet ) $this->set( 'm_intMinValue', trim( $arrValues['min_value'] ) ); elseif( isset( $arrValues['min_value'] ) ) $this->setMinValue( $arrValues['min_value'] );
		if( isset( $arrValues['max_value'] ) && $boolDirectSet ) $this->set( 'm_intMaxValue', trim( $arrValues['max_value'] ) ); elseif( isset( $arrValues['max_value'] ) ) $this->setMaxValue( $arrValues['max_value'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId ) {
		$this->set( 'm_intScreeningConfigPreferenceTypeId', CStrings::strToIntDef( $intScreeningConfigPreferenceTypeId, NULL, false ) );
	}

	public function getScreeningConfigPreferenceTypeId() {
		return $this->m_intScreeningConfigPreferenceTypeId;
	}

	public function sqlScreeningConfigPreferenceTypeId() {
		return ( true == isset( $this->m_intScreeningConfigPreferenceTypeId ) ) ? ( string ) $this->m_intScreeningConfigPreferenceTypeId : 'NULL';
	}

	public function setMinValue( $intMinValue ) {
		$this->set( 'm_intMinValue', CStrings::strToIntDef( $intMinValue, NULL, false ) );
	}

	public function getMinValue() {
		return $this->m_intMinValue;
	}

	public function sqlMinValue() {
		return ( true == isset( $this->m_intMinValue ) ) ? ( string ) $this->m_intMinValue : 'NULL';
	}

	public function setMaxValue( $intMaxValue ) {
		$this->set( 'm_intMaxValue', CStrings::strToIntDef( $intMaxValue, NULL, false ) );
	}

	public function getMaxValue() {
		return $this->m_intMaxValue;
	}

	public function sqlMaxValue() {
		return ( true == isset( $this->m_intMaxValue ) ) ? ( string ) $this->m_intMaxValue : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screening_config_preference_type_id' => $this->getScreeningConfigPreferenceTypeId(),
			'min_value' => $this->getMinValue(),
			'max_value' => $this->getMaxValue()
		);
	}

}
?>