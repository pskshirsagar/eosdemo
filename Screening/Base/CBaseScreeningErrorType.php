<?php

class CBaseScreeningErrorType extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_error_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intSeverityLevel;
	protected $m_boolIsPublished;
	protected $m_intOrderNumber;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->m_strName = trim( stripcslashes( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->m_strDescription = trim( stripcslashes( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['severity_level'] ) && $boolDirectSet ) $this->m_intSeverityLevel = trim( $arrValues['severity_level'] ); elseif( isset( $arrValues['severity_level'] ) ) $this->setSeverityLevel( $arrValues['severity_level'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->m_boolIsPublished = trim( stripcslashes( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_number'] ) && $boolDirectSet ) $this->m_intOrderNumber = trim( $arrValues['order_number'] ); elseif( isset( $arrValues['order_number'] ) ) $this->setOrderNumber( $arrValues['order_number'] );
	}

	public function setId( $intId ) {
		$this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->m_strName = CStrings::strTrimDef( $strName, -1, NULL, true );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->m_strDescription = CStrings::strTrimDef( $strDescription, -1, NULL, true );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setSeverityLevel( $intSeverityLevel ) {
		$this->m_intSeverityLevel = CStrings::strToIntDef( $intSeverityLevel, NULL, false );
	}

	public function getSeverityLevel() {
		return $this->m_intSeverityLevel;
	}

	public function sqlSeverityLevel() {
		return ( true == isset( $this->m_intSeverityLevel ) ) ? ( string ) $this->m_intSeverityLevel : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->m_boolIsPublished = CStrings::strToBool( $boolIsPublished );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNumber( $intOrderNumber ) {
		$this->m_intOrderNumber = CStrings::strToIntDef( $intOrderNumber, NULL, false );
	}

	public function getOrderNumber() {
		return $this->m_intOrderNumber;
	}

	public function sqlOrderNumber() {
		return ( true == isset( $this->m_intOrderNumber ) ) ? ( string ) $this->m_intOrderNumber : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'severity_level' => $this->getSeverityLevel(),
			'is_published' => $this->getIsPublished(),
			'order_number' => $this->getOrderNumber()
		);
	}

}
?>