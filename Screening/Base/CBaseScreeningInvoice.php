<?php

class CBaseScreeningInvoice extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_invoices';

	protected $m_intId;
	protected $m_intScreenTypeId;
	protected $m_intScreeningDataProviderTypeId;
	protected $m_fltTotalInvoiceAmount;
	protected $m_fltTotalTransactionAmount;
	protected $m_fltInvoiceDueAmount;
	protected $m_strInvoiceDate;
	protected $m_strInvoiceDueDate;
	protected $m_intReconciliationStatusTypeId;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_intReconciledBy;
	protected $m_strReconciledOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltTotalInvoiceAmount = '0';
		$this->m_fltTotalTransactionAmount = '0';
		$this->m_fltInvoiceDueAmount = '0';
		$this->m_intReconciliationStatusTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['screen_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreenTypeId', trim( $arrValues['screen_type_id'] ) ); elseif( isset( $arrValues['screen_type_id'] ) ) $this->setScreenTypeId( $arrValues['screen_type_id'] );
		if( isset( $arrValues['screening_data_provider_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningDataProviderTypeId', trim( $arrValues['screening_data_provider_type_id'] ) ); elseif( isset( $arrValues['screening_data_provider_type_id'] ) ) $this->setScreeningDataProviderTypeId( $arrValues['screening_data_provider_type_id'] );
		if( isset( $arrValues['total_invoice_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalInvoiceAmount', trim( $arrValues['total_invoice_amount'] ) ); elseif( isset( $arrValues['total_invoice_amount'] ) ) $this->setTotalInvoiceAmount( $arrValues['total_invoice_amount'] );
		if( isset( $arrValues['total_transaction_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalTransactionAmount', trim( $arrValues['total_transaction_amount'] ) ); elseif( isset( $arrValues['total_transaction_amount'] ) ) $this->setTotalTransactionAmount( $arrValues['total_transaction_amount'] );
		if( isset( $arrValues['invoice_due_amount'] ) && $boolDirectSet ) $this->set( 'm_fltInvoiceDueAmount', trim( $arrValues['invoice_due_amount'] ) ); elseif( isset( $arrValues['invoice_due_amount'] ) ) $this->setInvoiceDueAmount( $arrValues['invoice_due_amount'] );
		if( isset( $arrValues['invoice_date'] ) && $boolDirectSet ) $this->set( 'm_strInvoiceDate', trim( $arrValues['invoice_date'] ) ); elseif( isset( $arrValues['invoice_date'] ) ) $this->setInvoiceDate( $arrValues['invoice_date'] );
		if( isset( $arrValues['invoice_due_date'] ) && $boolDirectSet ) $this->set( 'm_strInvoiceDueDate', trim( $arrValues['invoice_due_date'] ) ); elseif( isset( $arrValues['invoice_due_date'] ) ) $this->setInvoiceDueDate( $arrValues['invoice_due_date'] );
		if( isset( $arrValues['reconciliation_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReconciliationStatusTypeId', trim( $arrValues['reconciliation_status_type_id'] ) ); elseif( isset( $arrValues['reconciliation_status_type_id'] ) ) $this->setReconciliationStatusTypeId( $arrValues['reconciliation_status_type_id'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( $arrValues['file_name'] ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( $arrValues['file_path'] ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( $arrValues['file_path'] );
		if( isset( $arrValues['reconciled_by'] ) && $boolDirectSet ) $this->set( 'm_intReconciledBy', trim( $arrValues['reconciled_by'] ) ); elseif( isset( $arrValues['reconciled_by'] ) ) $this->setReconciledBy( $arrValues['reconciled_by'] );
		if( isset( $arrValues['reconciled_on'] ) && $boolDirectSet ) $this->set( 'm_strReconciledOn', trim( $arrValues['reconciled_on'] ) ); elseif( isset( $arrValues['reconciled_on'] ) ) $this->setReconciledOn( $arrValues['reconciled_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScreenTypeId( $intScreenTypeId ) {
		$this->set( 'm_intScreenTypeId', CStrings::strToIntDef( $intScreenTypeId, NULL, false ) );
	}

	public function getScreenTypeId() {
		return $this->m_intScreenTypeId;
	}

	public function sqlScreenTypeId() {
		return ( true == isset( $this->m_intScreenTypeId ) ) ? ( string ) $this->m_intScreenTypeId : 'NULL';
	}

	public function setScreeningDataProviderTypeId( $intScreeningDataProviderTypeId ) {
		$this->set( 'm_intScreeningDataProviderTypeId', CStrings::strToIntDef( $intScreeningDataProviderTypeId, NULL, false ) );
	}

	public function getScreeningDataProviderTypeId() {
		return $this->m_intScreeningDataProviderTypeId;
	}

	public function sqlScreeningDataProviderTypeId() {
		return ( true == isset( $this->m_intScreeningDataProviderTypeId ) ) ? ( string ) $this->m_intScreeningDataProviderTypeId : 'NULL';
	}

	public function setTotalInvoiceAmount( $fltTotalInvoiceAmount ) {
		$this->set( 'm_fltTotalInvoiceAmount', CStrings::strToFloatDef( $fltTotalInvoiceAmount, NULL, false, 2 ) );
	}

	public function getTotalInvoiceAmount() {
		return $this->m_fltTotalInvoiceAmount;
	}

	public function sqlTotalInvoiceAmount() {
		return ( true == isset( $this->m_fltTotalInvoiceAmount ) ) ? ( string ) $this->m_fltTotalInvoiceAmount : '0';
	}

	public function setTotalTransactionAmount( $fltTotalTransactionAmount ) {
		$this->set( 'm_fltTotalTransactionAmount', CStrings::strToFloatDef( $fltTotalTransactionAmount, NULL, false, 2 ) );
	}

	public function getTotalTransactionAmount() {
		return $this->m_fltTotalTransactionAmount;
	}

	public function sqlTotalTransactionAmount() {
		return ( true == isset( $this->m_fltTotalTransactionAmount ) ) ? ( string ) $this->m_fltTotalTransactionAmount : '0';
	}

	public function setInvoiceDueAmount( $fltInvoiceDueAmount ) {
		$this->set( 'm_fltInvoiceDueAmount', CStrings::strToFloatDef( $fltInvoiceDueAmount, NULL, false, 2 ) );
	}

	public function getInvoiceDueAmount() {
		return $this->m_fltInvoiceDueAmount;
	}

	public function sqlInvoiceDueAmount() {
		return ( true == isset( $this->m_fltInvoiceDueAmount ) ) ? ( string ) $this->m_fltInvoiceDueAmount : '0';
	}

	public function setInvoiceDate( $strInvoiceDate ) {
		$this->set( 'm_strInvoiceDate', CStrings::strTrimDef( $strInvoiceDate, -1, NULL, true ) );
	}

	public function getInvoiceDate() {
		return $this->m_strInvoiceDate;
	}

	public function sqlInvoiceDate() {
		return ( true == isset( $this->m_strInvoiceDate ) ) ? '\'' . $this->m_strInvoiceDate . '\'' : 'NULL';
	}

	public function setInvoiceDueDate( $strInvoiceDueDate ) {
		$this->set( 'm_strInvoiceDueDate', CStrings::strTrimDef( $strInvoiceDueDate, -1, NULL, true ) );
	}

	public function getInvoiceDueDate() {
		return $this->m_strInvoiceDueDate;
	}

	public function sqlInvoiceDueDate() {
		return ( true == isset( $this->m_strInvoiceDueDate ) ) ? '\'' . $this->m_strInvoiceDueDate . '\'' : 'NULL';
	}

	public function setReconciliationStatusTypeId( $intReconciliationStatusTypeId ) {
		$this->set( 'm_intReconciliationStatusTypeId', CStrings::strToIntDef( $intReconciliationStatusTypeId, NULL, false ) );
	}

	public function getReconciliationStatusTypeId() {
		return $this->m_intReconciliationStatusTypeId;
	}

	public function sqlReconciliationStatusTypeId() {
		return ( true == isset( $this->m_intReconciliationStatusTypeId ) ) ? ( string ) $this->m_intReconciliationStatusTypeId : '1';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileName ) : '\'' . addslashes( $this->m_strFileName ) . '\'' ) : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFilePath ) : '\'' . addslashes( $this->m_strFilePath ) . '\'' ) : 'NULL';
	}

	public function setReconciledBy( $intReconciledBy ) {
		$this->set( 'm_intReconciledBy', CStrings::strToIntDef( $intReconciledBy, NULL, false ) );
	}

	public function getReconciledBy() {
		return $this->m_intReconciledBy;
	}

	public function sqlReconciledBy() {
		return ( true == isset( $this->m_intReconciledBy ) ) ? ( string ) $this->m_intReconciledBy : 'NULL';
	}

	public function setReconciledOn( $strReconciledOn ) {
		$this->set( 'm_strReconciledOn', CStrings::strTrimDef( $strReconciledOn, -1, NULL, true ) );
	}

	public function getReconciledOn() {
		return $this->m_strReconciledOn;
	}

	public function sqlReconciledOn() {
		return ( true == isset( $this->m_strReconciledOn ) ) ? '\'' . $this->m_strReconciledOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, screen_type_id, screening_data_provider_type_id, total_invoice_amount, total_transaction_amount, invoice_due_amount, invoice_date, invoice_due_date, reconciliation_status_type_id, file_name, file_path, reconciled_by, reconciled_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlScreenTypeId() . ', ' .
						$this->sqlScreeningDataProviderTypeId() . ', ' .
						$this->sqlTotalInvoiceAmount() . ', ' .
						$this->sqlTotalTransactionAmount() . ', ' .
						$this->sqlInvoiceDueAmount() . ', ' .
						$this->sqlInvoiceDate() . ', ' .
						$this->sqlInvoiceDueDate() . ', ' .
						$this->sqlReconciliationStatusTypeId() . ', ' .
						$this->sqlFileName() . ', ' .
						$this->sqlFilePath() . ', ' .
						$this->sqlReconciledBy() . ', ' .
						$this->sqlReconciledOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreenTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screen_type_id = ' . $this->sqlScreenTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_data_provider_type_id = ' . $this->sqlScreeningDataProviderTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningDataProviderTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_data_provider_type_id = ' . $this->sqlScreeningDataProviderTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_invoice_amount = ' . $this->sqlTotalInvoiceAmount(). ',' ; } elseif( true == array_key_exists( 'TotalInvoiceAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_invoice_amount = ' . $this->sqlTotalInvoiceAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_transaction_amount = ' . $this->sqlTotalTransactionAmount(). ',' ; } elseif( true == array_key_exists( 'TotalTransactionAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_transaction_amount = ' . $this->sqlTotalTransactionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_due_amount = ' . $this->sqlInvoiceDueAmount(). ',' ; } elseif( true == array_key_exists( 'InvoiceDueAmount', $this->getChangedColumns() ) ) { $strSql .= ' invoice_due_amount = ' . $this->sqlInvoiceDueAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_date = ' . $this->sqlInvoiceDate(). ',' ; } elseif( true == array_key_exists( 'InvoiceDate', $this->getChangedColumns() ) ) { $strSql .= ' invoice_date = ' . $this->sqlInvoiceDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_due_date = ' . $this->sqlInvoiceDueDate(). ',' ; } elseif( true == array_key_exists( 'InvoiceDueDate', $this->getChangedColumns() ) ) { $strSql .= ' invoice_due_date = ' . $this->sqlInvoiceDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reconciliation_status_type_id = ' . $this->sqlReconciliationStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ReconciliationStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' reconciliation_status_type_id = ' . $this->sqlReconciliationStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath(). ',' ; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reconciled_by = ' . $this->sqlReconciledBy(). ',' ; } elseif( true == array_key_exists( 'ReconciledBy', $this->getChangedColumns() ) ) { $strSql .= ' reconciled_by = ' . $this->sqlReconciledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reconciled_on = ' . $this->sqlReconciledOn(). ',' ; } elseif( true == array_key_exists( 'ReconciledOn', $this->getChangedColumns() ) ) { $strSql .= ' reconciled_on = ' . $this->sqlReconciledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'screen_type_id' => $this->getScreenTypeId(),
			'screening_data_provider_type_id' => $this->getScreeningDataProviderTypeId(),
			'total_invoice_amount' => $this->getTotalInvoiceAmount(),
			'total_transaction_amount' => $this->getTotalTransactionAmount(),
			'invoice_due_amount' => $this->getInvoiceDueAmount(),
			'invoice_date' => $this->getInvoiceDate(),
			'invoice_due_date' => $this->getInvoiceDueDate(),
			'reconciliation_status_type_id' => $this->getReconciliationStatusTypeId(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'reconciled_by' => $this->getReconciledBy(),
			'reconciled_on' => $this->getReconciledOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>