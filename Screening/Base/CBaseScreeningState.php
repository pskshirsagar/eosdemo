<?php

class CBaseScreeningState extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_states';

	protected $m_intId;
	protected $m_strStateCode;
	protected $m_fltManualCriminalCost;
	protected $m_intHasManualCriminal;

	public function __construct() {
		parent::__construct();

		$this->m_intHasManualCriminal = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['manual_criminal_cost'] ) && $boolDirectSet ) $this->set( 'm_fltManualCriminalCost', trim( $arrValues['manual_criminal_cost'] ) ); elseif( isset( $arrValues['manual_criminal_cost'] ) ) $this->setManualCriminalCost( $arrValues['manual_criminal_cost'] );
		if( isset( $arrValues['has_manual_criminal'] ) && $boolDirectSet ) $this->set( 'm_intHasManualCriminal', trim( $arrValues['has_manual_criminal'] ) ); elseif( isset( $arrValues['has_manual_criminal'] ) ) $this->setHasManualCriminal( $arrValues['has_manual_criminal'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setManualCriminalCost( $fltManualCriminalCost ) {
		$this->set( 'm_fltManualCriminalCost', CStrings::strToFloatDef( $fltManualCriminalCost, NULL, false, 2 ) );
	}

	public function getManualCriminalCost() {
		return $this->m_fltManualCriminalCost;
	}

	public function sqlManualCriminalCost() {
		return ( true == isset( $this->m_fltManualCriminalCost ) ) ? ( string ) $this->m_fltManualCriminalCost : 'NULL';
	}

	public function setHasManualCriminal( $intHasManualCriminal ) {
		$this->set( 'm_intHasManualCriminal', CStrings::strToIntDef( $intHasManualCriminal, NULL, false ) );
	}

	public function getHasManualCriminal() {
		return $this->m_intHasManualCriminal;
	}

	public function sqlHasManualCriminal() {
		return ( true == isset( $this->m_intHasManualCriminal ) ) ? ( string ) $this->m_intHasManualCriminal : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'state_code' => $this->getStateCode(),
			'manual_criminal_cost' => $this->getManualCriminalCost(),
			'has_manual_criminal' => $this->getHasManualCriminal()
		);
	}

}
?>