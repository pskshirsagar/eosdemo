<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningListItems
 * Do not add any new functions to this class.
 */

class CBaseScreeningListItems extends CEosPluralBase {

	/**
	 * @return CScreeningListItem[]
	 */
	public static function fetchScreeningListItems( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningListItem', $objDatabase );
	}

	/**
	 * @return CScreeningListItem
	 */
	public static function fetchScreeningListItem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningListItem', $objDatabase );
	}

	public static function fetchScreeningListItemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_list_items', $objDatabase );
	}

	public static function fetchScreeningListItemById( $intId, $objDatabase ) {
		return self::fetchScreeningListItem( sprintf( 'SELECT * FROM screening_list_items WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningListItemsByScreeningListTypeId( $intScreeningListTypeId, $objDatabase ) {
		return self::fetchScreeningListItems( sprintf( 'SELECT * FROM screening_list_items WHERE screening_list_type_id = %d', ( int ) $intScreeningListTypeId ), $objDatabase );
	}

}
?>