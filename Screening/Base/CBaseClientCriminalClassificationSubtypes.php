<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CClientCriminalClassificationSubtypes
 * Do not add any new functions to this class.
 */

class CBaseClientCriminalClassificationSubtypes extends CEosPluralBase {

	/**
	 * @return CClientCriminalClassificationSubtype[]
	 */
	public static function fetchClientCriminalClassificationSubtypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CClientCriminalClassificationSubtype::class, $objDatabase );
	}

	/**
	 * @return CClientCriminalClassificationSubtype
	 */
	public static function fetchClientCriminalClassificationSubtype( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CClientCriminalClassificationSubtype::class, $objDatabase );
	}

	public static function fetchClientCriminalClassificationSubtypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'client_criminal_classification_subtypes', $objDatabase );
	}

	public static function fetchClientCriminalClassificationSubtypeById( $intId, $objDatabase ) {
		return self::fetchClientCriminalClassificationSubtype( sprintf( 'SELECT * FROM client_criminal_classification_subtypes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchClientCriminalClassificationSubtypesByCid( $intCid, $objDatabase ) {
		return self::fetchClientCriminalClassificationSubtypes( sprintf( 'SELECT * FROM client_criminal_classification_subtypes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchClientCriminalClassificationSubtypesByCriminalClassificationTypeId( $intCriminalClassificationTypeId, $objDatabase ) {
		return self::fetchClientCriminalClassificationSubtypes( sprintf( 'SELECT * FROM client_criminal_classification_subtypes WHERE criminal_classification_type_id = %d', ( int ) $intCriminalClassificationTypeId ), $objDatabase );
	}

}
?>