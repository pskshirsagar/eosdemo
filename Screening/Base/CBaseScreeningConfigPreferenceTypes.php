<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConfigPreferenceTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningConfigPreferenceTypes extends CEosPluralBase {

	/**
	 * @return CScreeningConfigPreferenceType[]
	 */
	public static function fetchScreeningConfigPreferenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScreeningConfigPreferenceType', $objDatabase );
	}

	/**
	 * @return CScreeningConfigPreferenceType
	 */
	public static function fetchScreeningConfigPreferenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningConfigPreferenceType', $objDatabase );
	}

	public static function fetchScreeningConfigPreferenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_config_preference_types', $objDatabase );
	}

	public static function fetchScreeningConfigPreferenceTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningConfigPreferenceType( sprintf( 'SELECT * FROM screening_config_preference_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScreeningConfigPreferenceTypesByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreeningConfigPreferenceTypes( sprintf( 'SELECT * FROM screening_config_preference_types WHERE screen_type_id = %d', ( int ) $intScreenTypeId ), $objDatabase );
	}

	public static function fetchScreeningConfigPreferenceTypesByScreeningConfigValueTypeId( $intScreeningConfigValueTypeId, $objDatabase ) {
		return self::fetchScreeningConfigPreferenceTypes( sprintf( 'SELECT * FROM screening_config_preference_types WHERE screening_config_value_type_id = %d', ( int ) $intScreeningConfigValueTypeId ), $objDatabase );
	}

}
?>