<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningTransactions
 * Do not add any new functions to this class.
 */

class CScreeningTransactions extends CBaseScreeningTransactions {

	public static function fetchUnPostedCompleteScreeningTransactionsCountByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

			$strSql = 'SELECT
							count(st.id) as total_transactions_count
						FROM
							screening_transactions st INNER JOIN screenings s ON ( s.cid = st.cid and s.id = st.screening_id and s.screening_status_type_id =  ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . ' )
						WHERE
							st.screening_batch_id IS NULL
							AND st.screening_order_id IS NOT NULL
							AND st.cid = ' . ( int ) $intCid . '
							AND s.application_type_id <> ' . CScreeningApplicationType::VENDOR_SCREENING . '
							AND st.property_id = ' . ( int ) $intPropertyId . '
							AND st.transaction_amount > 0 ';

		return self::fetchColumn( $strSql, 'total_transactions_count', $objDatabase );
	}

	public static function fetchTotalScreeningTransactionsByScreeningBatchIdByCid( $intScreeningBatchId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						COUNT(st.id) AS total_count,
						SUM(st.transaction_amount) AS total_amount
					FROM
						screening_transactions st
						JOIN screenings s ON ( s.cid = st.cid AND s.property_id = st.property_id AND s.id = st.screening_id )
					WHERE
						st.screening_batch_id = ' . ( int ) $intScreeningBatchId . '
						AND s.application_type_id <> ' . CScreeningApplicationType::VENDOR_SCREENING . '
						AND st.cid = ' . ( int ) $intCid;

		$arrintData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintData[0] ) ) {
			return $arrintData[0];
		}

		return NULL;
	}

	public static function fetchScreeningTransactionsByScreeningIdByScreeningApplicantIdByCid( $intScreeningId, $intScreeningApplicantId, $intCid, $objDatabase, $arrintScreenTypeIds = NULL, $boolIncludeCompletedTransaction = false ) {

		$strSubSql = '';
		if( true == valArr( $arrintScreenTypeIds ) ) {
			$strSubSql = ' AND st.screen_type_id IN( ' . implode( ',', $arrintScreenTypeIds ) . ' )';
		}

        if( true == $boolIncludeCompletedTransaction ) {
            $strSubSql .= ' AND st.screening_status_type_id = ' . ( int ) CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED;
        }

        $strSql = 'SELECT
						st.*,
						sdp.screening_data_provider_type_id,
						stypes.parent_screen_type_id
					FROM
						screening_transactions st
						LEFT JOIN screening_data_providers sdp ON sdp.id = st.screening_data_provider_id
						JOIN screen_types stypes ON stypes.id = st.screen_type_id AND stypes.is_published = 1
					WHERE
						st.screening_id = ' . ( int ) $intScreeningId . '
						AND st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND st.cid = ' . ( int ) $intCid . $strSubSql;

		return parent::fetchScreeningTransactions( $strSql, $objDatabase );
	}

	public static function fetchScreeningTransactionsByScreeningIdByScreeningApplicantIdsByCid( $intScreeningId, $arrintScreeningApplicantIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintScreeningApplicantIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						screening_transactions
					WHERE screening_id = ' . ( int ) $intScreeningId . '
						AND screening_applicant_id IN ( ' . implode( ',', $arrintScreeningApplicantIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return parent::fetchScreeningTransactions( $strSql, $objDatabase );
	}

	public static function fetchScreeningTransactionsByIdsByScreeningIdByScreeningApplicantIdByCid( $arrintScreeningTransactionsIds, $intScreeningId, $intScreeningApplicantId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintScreeningTransactionsIds ) ) return;

		$strSql = 'SELECT
							*
					FROM
						screening_transactions
					WHERE
						screening_id = ' . ( int ) $intScreeningId . '
						AND screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintScreeningTransactionsIds ) . ' )';

		return parent::fetchScreeningTransactions( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningTransactionsByScreeningIdByScreeningApplicantIdByCid( $intScreeningId, $intScreeningApplicantId, $intCid, $objDatabase, $boolExcludeFromScreeningDetails = false ) {

		$arrintCancelledStatusTypeIds = array( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED, CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED );

		$strSubSql = '';
		if( true == $boolExcludeFromScreeningDetails ) {
			$strSubSql = ' AND st.screen_type_id NOT IN ( ' . sqlintImplode( CScreenType::$c_arrintExcludeFromScreeningDetails ) . ' ) ';
		}

		$strSql = 'SELECT
						st.*,
						sdp.screening_data_provider_type_id,
						stypes.parent_screen_type_id,
						sdp.on_maintenance
					FROM
						screening_transactions st
						LEFT JOIN screening_data_providers sdp ON sdp.id = st.screening_data_provider_id
						JOIN screen_types stypes ON stypes.id = st.screen_type_id
					WHERE
						st.screening_id = ' . ( int ) $intScreeningId . '
						AND st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND st.screening_status_type_id NOT IN ( ' . implode( ',', $arrintCancelledStatusTypeIds ) . ' )
						AND st.cid = ' . ( int ) $intCid . '
						AND stypes.is_published = 1' . $strSubSql;

		return parent::fetchScreeningTransactions( $strSql, $objDatabase );
	}

	public static function fetchScreeningTransactionsWithScreenTypesByScreeningIdByScreeningTransactionIdsByCid( $intScreeningId, $intScreeningTransactionIds, $intCid, $objDatabase ) {

		if( false == valArr( $intScreeningTransactionIds ) ) return;

		$strSql = 'SELECT
						st.id,
						st.screen_type_id,
						stypes.name screen_type
					FROM
						screening_transactions st
					JOIN screen_types stypes ON stypes.id = st.screen_type_id
					WHERE
						st.screening_id = ' . ( int ) $intScreeningId . '
						AND st.cid = ' . ( int ) $intCid . '
						AND st.id in ( ' . implode( ',', $intScreeningTransactionIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function getScreeningTransactionsCountByScreenTypeIds( $intApplicantId, $arrintScreenTypeIds, $objDatabase ) {

		if( false == valArr( $arrintScreenTypeIds ) ) return false;

		$strSql = '	SELECT count(id) as transactions_count
					FROM screening_transactions
					WHERE screening_applicant_id = ' . ( int ) $intApplicantId . '
						AND screen_type_id IN ( ' . implode( ',', $arrintScreenTypeIds ) . ' )';

		$arrstrData = fetchData( $strSql, $objDatabase );

		$intTransactionsCount 	= ( int ) ( true == valArr( $arrstrData ) && true == array_key_exists( 'transactions_count', $arrstrData[0] ) ) ? $arrstrData[0][transactions_count] : 0;

		return $intTransactionsCount;
	}

	public static function fetchScreeningTransactionsWithScreenTypesByScreeningIdByScreeningRecommendationTypeIdByCid( $intScreeningId, $intScreeningRecommendationTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT st.screen_type_id,
						st.id,
						stypes.name screen_type,
						sa.custom_applicant_id
					FROM
						screening_transactions st
						JOIN screen_types stypes ON stypes.id = st.screen_type_id
						JOIN screening_applicants sa ON sa.id = st.screening_applicant_id AND sa.status_type_id NOT IN( ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED . ' )
					WHERE
						st.cid = ' . ( int ) $intCid . '
						AND st.screening_id = ' . ( int ) $intScreeningId . '
						AND st.screening_status_type_id NOT IN( ' . implode( ',', CScreeningStatusType::$c_arrintCancelledScreeningStatusTypeIds ) . ' )
						AND st.screening_recommendation_type_id = ' . ( int ) $intScreeningRecommendationTypeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchExistingScreeningTransactionByScreeningApplicantIdByOtherDetailsByCid( $intScreeningApplicantId, $objScreeningTransaction, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    st.*
					FROM
    					screening_transactions st
						inner join screening_data_providers sdp on st.screen_type_id = sdp.screen_type_id
					WHERE
    					st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND st.screening_status_type_id = ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . '
						AND st.cid = ' . ( int ) $intCid . '
						AND st.screen_type_id =  ' . ( int ) $objScreeningTransaction->getScreenTypeId() . '
						AND sdp.screening_data_provider_type_id = (
						      SELECT
						          screening_data_provider_type_id
						      FROM
						          screening_transactions st
						          INNER JOIN screening_data_providers sdp ON st.screening_data_provider_id = sdp.id
						          INNER JOIN screening_data_provider_types sdpt ON sdpt.id = sdp.screening_data_provider_type_id
						      WHERE
						          st.screen_type_id = ' . ( int ) $objScreeningTransaction->getScreenTypeId() . '
						          AND st.cid = ' . ( int ) $objScreeningTransaction->getCId() . '
						          AND st.id = ' . ( int ) $objScreeningTransaction->getId() . ' )
						AND st.transaction_amount <> 0
						ORDER BY st.id DESC LIMIT 1';

		return parent::fetchScreeningTransaction( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningTransactionsByScreeningIdByScreeningApplicantIdsByCid( $intScreeningId, $arrintScreeningApplicantIds, $intCid, $objDatabase, $boolExcludeFromScreeningDetails = false ) {

		if( false == valArr( $arrintScreeningApplicantIds ) ) return;

		$arrintCancelledStatusTypeIds = array( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED, CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED );

        $strSubSql = '';

        if( true == $boolExcludeFromScreeningDetails ) {
            $strSubSql = ' AND st.screen_type_id NOT IN ( ' . sqlintImplode( CScreenType::$c_arrintExcludeFromScreeningDetails ) . ' ) ';
        }

		$strSql = 'SELECT
						st.*,
						sdp.screening_data_provider_type_id,
						stypes.parent_screen_type_id,
						sdp.on_maintenance
					FROM
						screening_transactions st
						LEFT JOIN screening_data_providers sdp ON sdp.id = st.screening_data_provider_id
						JOIN screen_types stypes ON stypes.id = st.screen_type_id
					WHERE
						st.screening_id = ' . ( int ) $intScreeningId . '
						AND st.screening_applicant_id IN( ' . implode( ',', $arrintScreeningApplicantIds ) . ' )
						AND st.screening_status_type_id NOT IN( ' . implode( ',', $arrintCancelledStatusTypeIds ) . ' )
						AND st.cid = ' . ( int ) $intCid . $strSubSql;

		return parent::fetchScreeningTransactions( $strSql, $objDatabase );
	}

	public static function fetchScreeningTransactionsByScreenTypeIdsByScreeningIdByScreeningApplicantIdByCid( $arrintScreenTypeIds, $intScreeningId, $intScreeningApplicantId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintScreenTypeIds ) ) return;

		$strSql = 'SELECT
						st.*,
						sdp.screening_data_provider_type_id,
						stypes.parent_screen_type_id,
						CASE
		                WHEN stypes.is_evaluation_required IS TRUE THEN 1
		                ELSE 0
		                END AS  is_evaluation_required
					FROM
						screening_transactions st
						LEFT JOIN screening_data_providers sdp ON sdp.id = st.screening_data_provider_id
						JOIN screen_types stypes ON stypes.id = st.screen_type_id
					WHERE
						st.screen_type_id IN( ' . implode( ',', $arrintScreenTypeIds ) . ' )
						AND st.screening_id = ' . ( int ) $intScreeningId . '
						AND st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND st.cid = ' . ( int ) $intCid;

		return parent::fetchScreeningTransactions( $strSql, $objDatabase );
	}

	public static function fetchCompletedScreeningTransactionsByScreenTypeIdsByScreeningIdByScreeningApplicantIdByCid( $arrintScreenTypeIds, $intScreeningId, $intScreeningApplicantId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintScreenTypeIds ) ) return;

		$arrintScreeningStatusTypeIds = array( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED, CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED, CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR );

		$strSql = 'SELECT
						st.*,
						sdp.screening_data_provider_type_id
					FROM
						screening_transactions st
						LEFT JOIN screening_data_providers sdp ON sdp.id = st.screening_data_provider_id
					WHERE
						st.screen_type_id IN( ' . implode( ',', $arrintScreenTypeIds ) . ' )
						AND st.screening_status_type_id IN ( ' . implode( ',', $arrintScreeningStatusTypeIds ) . ' )
						AND st.screening_id = ' . ( int ) $intScreeningId . '
						AND st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND st.cid = ' . ( int ) $intCid . '
					ORDER BY
						id DESC
					LIMIT 1';

		return parent::fetchScreeningTransactions( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningTransactionsByScreenTypeIdsByScreeningIdByScreeningApplicantIdByCid( $arrintScreenTypeIds, $intScreeningId, $intScreeningApplicantId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintScreenTypeIds ) ) return;

		$arrintCancelledStatusTypeIds = array( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED, CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED );

		$strSql = 'SELECT
						st.*,
						sdp.screening_data_provider_type_id
					FROM
						screening_transactions st
						LEFT JOIN screening_data_providers sdp ON sdp.id = st.screening_data_provider_id
					WHERE
						st.screen_type_id IN( ' . implode( ',', $arrintScreenTypeIds ) . ' )
						AND st.screening_id = ' . ( int ) $intScreeningId . '
						AND st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND st.screening_status_type_id NOT IN( ' . implode( ',', $arrintCancelledStatusTypeIds ) . ' )
						AND st.cid = ' . ( int ) $intCid;

		return parent::fetchScreeningTransactions( $strSql, $objDatabase );
	}

	public static function fetchScreeningTransactionByIdByScreeningIdByScreeningApplicantIdByCid( $intScreeningTransactionId, $intScreeningId, $intScreeningApplicantId, $intCid, $objDatabase, $arrintScreenTypeIds = NULL ) {

		$strSubSql = '';
		if( true == valArr( $arrintScreenTypeIds ) ) {
			$strSubSql = ' AND st.screen_type_id IN( ' . implode( ',', $arrintScreenTypeIds ) . ' )';
		}

		$strSql = 'SELECT
						st.*,
						sdp.screening_data_provider_type_id,
						stypes.parent_screen_type_id,
						state_code as property_state_code
					FROM
						screening_transactions st
						LEFT JOIN screening_data_providers sdp ON sdp.id = st.screening_data_provider_id
						JOIN screen_types stypes ON stypes.id = st.screen_type_id
						LEFT JOIN screening_property_accounts spa  on spa.property_id = st.property_id and spa.cid = st.cid AND spa.is_published = 1
					WHERE
						st.id = ' . ( int ) $intScreeningTransactionId . '
						AND st.screening_id = ' . ( int ) $intScreeningId . '
						AND st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND st.cid = ' . ( int ) $intCid . $strSubSql;

		return parent::fetchScreeningTransaction( $strSql, $objDatabase );
	}

	public static function updateExistingScreeningTransactionsStatus( $intCurrenUserId, $intScreenTypeId, $intScreeningStatusTypeId, $intScreeningId, $intScreeningApplicantId, $objDatabase ) {
		$strSql = 'UPDATE
						screening_transactions
					SET
						screening_status_type_id = ' . ( int ) $intScreeningStatusTypeId . ',
						updated_by = ' . ( int ) $intCurrenUserId . ',
						updated_on = NOW()
					WHERE
						screening_id = ' . ( int ) $intScreeningId . '
						AND screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND screen_type_id = ' . ( int ) $intScreenTypeId;

		return executeSql( $strSql, $objDatabase );
	}

	public static function fetchScreeningTransactionDataByTransactionIdByTransactionReviewQueueTypeIdByCid( $intScreeningTransactionId, $intTransactionReviewQueueTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT t.id,
	                      t.screen_type_id,
	                      st.name as screen_type_name,
	                      t.screening_status_type_id,
	                      t.search_type,
	                      sst.name as screening_status
	                FROM
						  screening_transactions t
					      INNER JOIN screen_types st on t.screen_type_id = st.id
					      INNER JOIN screening_status_types sst on sst.id = t.screening_status_type_id
					      INNER JOIN transaction_reviews tr ON tr.screening_transaction_id = t.id AND tr.cid = t.cid
				    WHERE
						( screening_id, screening_applicant_id ) IN ( SELECT screening_id, screening_applicant_id
														                  FROM
															                 screening_transactions
														                  WHERE
															                 id = ' . ( int ) $intScreeningTransactionId . '
						                                              ) 
						    AND t.cid = ' . ( int ) $intCid . '
						    AND screening_status_type_id NOT IN ( ' . implode( ',', CScreeningStatusType::$c_arrintCancelledStatusTypeIds ) . ' )';

		$strWhere = ' AND tr.transaction_review_queue_type_id = ' . ( int ) $intTransactionReviewQueueTypeId;

		if( $intTransactionReviewQueueTypeId == CTransactionReviewQueueType::ICORI_CO_COURT_REVIEW ) {
			$strWhere = ' AND ( tr.transaction_review_queue_type_id = ' . ( int ) $intTransactionReviewQueueTypeId . ' OR  tr.transaction_review_status_id = ' . ( int ) CTransactionReviewStatus::COMPLETE . ' ) ';
		}

		$strSql .= $strWhere;

		return fetchData( $strSql, $objDatabase );
	}

	public static  function fetchPaginatedApplicationWiseTurnAroundTimeDataByScreeningSearchFilterByPropertyIdByCid( $objPagination, $intPropertyId, $intCid, $objDatabase ) {

	}

	public static function fetchScreeningTransactionIdByScreeningIdByScreeningApplicantIdByCidByScreenTypeIdByScreeningStatusTypeId( $intScreeningId, $intScreeningApplicantId, $intCid, $intScreenTypeId, $intScreeningStatusTypeId, $objDatabase ) {

		if( false == valId( $intScreenTypeId ) || false == valId( $intScreeningId ) || false == valId( $intScreeningApplicantId ) || false == valId( $intCid ) ) return;

		$strSql = 'SELECT
						id,
						screening_status_type_id
					FROM
						screening_transactions
					WHERE
						screen_type_id = ' . ( int ) $intScreenTypeId . '
						AND screening_id = ' . ( int ) $intScreeningId . '
						AND screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND screening_status_type_id = ' . ( int ) $intScreeningStatusTypeId . '
						AND cid = ' . ( int ) $intCid;

		$arrintData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintData[0] ) ) {
			return $arrintData[0];
		}

		return NULL;
	}

	public static function fetchActiveManuallyReviewedScreeningTransactionsByScreeningIdByScreeningApplicantIdByScreenTypeIdsByTransactionReviewStatusIdByCid( $intScreeningId, $intScreeningApplicantId, $arrintScreenTypeIds, $intTransactionReviewStatusId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						st.*
					FROM
						screening_transactions st
						JOIN transaction_reviews tr ON tr.screening_transaction_id = st.id AND tr.cid = st.cid
					WHERE
						st.screening_id = ' . ( int ) $intScreeningId . '
						AND st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND st.screening_status_type_id NOT IN ( ' . sqlIntImplode( CScreeningStatusType::$c_arrintCancelledScreeningStatusTypeIds ) . ' )
						AND tr.transaction_review_status_id = ' . ( int ) $intTransactionReviewStatusId . '
						AND st.screen_type_id IN ( ' . sqlIntImplode( $arrintScreenTypeIds ) . ' ) 
						AND st.cid = ' . ( int ) $intCid;

		return parent::fetchScreeningTransactions( $strSql, $objDatabase );

	}

	public static function generateCustomScreeningScreenTypeVolumeReportByScreeningRecordSearchFilter( $objCScreeningReportSearchFilter, $objDatabase ) {

		if( false == valObj( $objCScreeningReportSearchFilter, CScreeningReportSearchFilter::class ) ) {
			return NULL;
		}

		$strWhere = '';
		if( false == $objCScreeningReportSearchFilter->getIsPropertySearchFilter() ) {
			if( true == valArr( $objCScreeningReportSearchFilter->getCids() ) ) {
				$strWhere   .= ' st.cid IN( ' . sqlIntImplode( $objCScreeningReportSearchFilter->getCids() ) . ') ';
			} else {
				return NULL;
			}
		} elseif( true == valId( $objCScreeningReportSearchFilter->getCid() ) ) {
			$strWhere .= ' st.cid = ' . ( int ) $objCScreeningReportSearchFilter->getCid();
		} else {
			return NULL;
		}

		if( true == $objCScreeningReportSearchFilter->getIsPropertySearchFilter() && true == valArr( $objCScreeningReportSearchFilter->getPropertyIds() ) ) {
			$strWhere .= ' AND st.property_id IN (' . sqlIntImplode( $objCScreeningReportSearchFilter->getPropertyIds() ) . ') ';
		}

		if( true == valStr( $objCScreeningReportSearchFilter->getFromDate() ) && true == valStr( $objCScreeningReportSearchFilter->getToDate() ) ) {
			$strWhere .= ' AND  st.created_on::DATE BETWEEN DATE(\'' . $objCScreeningReportSearchFilter->getFromDate() . '\') AND DATE(\'' . $objCScreeningReportSearchFilter->getToDate() . '\')';
		}

		if( true == valArr( $objCScreeningReportSearchFilter->getScreenTypeIds() ) ) {
			$strWhere .= ' AND st.screen_type_id IN (' . sqlIntImplode( $objCScreeningReportSearchFilter->getScreenTypeIds() ) . ') ';
		}

		if( false == $objCScreeningReportSearchFilter->getIsPropertySearchFilter() ) {
			$strField = '';
			$strGroupBy = 'st.cid, st.screen_type_id';
			$strOrderBy = 'st.cid, st.screen_type_id';
		} else {
			$strField = 'st.property_id,';
			$strGroupBy = 'st.cid, st.property_id, st.screen_type_id';
			$strOrderBy = 'st.cid, st.property_id, st.screen_type_id';
		}

		$strSql = 'SELECT 
						st.cid, ' . $strField . ' st.screen_type_id,
					    sum(transaction_amount) AS revenue,
					    COUNT(st.id) as total_transactions,
					    COUNT( CASE WHEN screening_status_type_id IN( ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . ',' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED . ' ) AND screening_recommendation_type_id = ' . CScreeningRecommendationType::PASS . ' THEN TRUE END ) AS total_pass, 
					    COUNT( CASE WHEN screening_status_type_id IN( ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . ',' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED . ' ) AND screening_recommendation_type_id = ' . CScreeningRecommendationType::PASSWITHCONDITIONS . ' THEN TRUE END ) AS total_conditional, 
					    COUNT( CASE WHEN screening_status_type_id IN( ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . ',' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED . ' ) AND screening_recommendation_type_id = ' . CScreeningRecommendationType::FAIL . ' THEN TRUE END ) AS total_fail,
					    COUNT( CASE WHEN screening_status_type_id IN( ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN . ', ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . ',' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED . ' , ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW . ' ) 
					           AND ( screening_recommendation_type_id = ' . CScreeningRecommendationType::PENDING_UNKNOWN . ' OR screening_recommendation_type_id IS NULL )  THEN TRUE END ) AS total_inprogress,
					    COUNT( CASE WHEN screening_status_type_id = ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR . ' THEN TRUE END ) AS total_error
					FROM 
						screening_transactions st
					WHERE 
						' . $strWhere . '
						AND st.screening_status_type_id != ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED . '
					GROUP BY
						' . $strGroupBy . '
					ORDER BY
						' . $strOrderBy . ';';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningScreenTypeTurnAroundTimeResultsByScreeningRecordSearchFilter( $objCScreeningReportSearchFilter, $objDatabase ) {

		if( false == valObj( $objCScreeningReportSearchFilter, CScreeningReportSearchFilter::class ) ) {
			return NULL;
		}

		$strWhere = $strGroupBy = $strFields = $strOrderBy = $strSelect = '';
		if( true == valId( $objCScreeningReportSearchFilter->getCid() ) ) {
			$strWhere .= ' st.cid = ' . ( int ) $objCScreeningReportSearchFilter->getCid();
		} else {
			return NULL;
		}

		if( true == valArr( $objCScreeningReportSearchFilter->getPropertyIds() ) ) {
			$strWhere .= ' AND st.property_id IN (' . sqlIntImplode( $objCScreeningReportSearchFilter->getPropertyIds() ) . ') ';
		}

		if( true == valStr( $objCScreeningReportSearchFilter->getFromDate() ) && true == valStr( $objCScreeningReportSearchFilter->getToDate() ) ) {
			$strWhere .= ' AND  st.created_on::DATE BETWEEN DATE(\'' . $objCScreeningReportSearchFilter->getFromDate() . '\') AND DATE(\'' . $objCScreeningReportSearchFilter->getToDate() . '\')';
		}

		if( true == valArr( $objCScreeningReportSearchFilter->getScreenTypeIds() ) ) {
			$strWhere .= ' AND st.screen_type_id IN (' . sqlIntImplode( $objCScreeningReportSearchFilter->getScreenTypeIds() ) . ') ';
			$strSelect = ' st.screen_type_id, ';
            $strGroupBy = ', st.screen_type_id ';
            $strOrderBy = ', st.screen_type_id ';
		}

        $strWhere .= ' AND st.screen_type_id NOT IN (' . sqlIntImplode( CScreenType::$c_arrintExcludeFromReportScreenTypesIds ) . ' ) ';

		$strSql = 'SELECT 
						st.cid,  st.property_id, ' . $strSelect . '
					    COUNT(st.id) as total_transactions,
                        COUNT( CASE WHEN EXTRACT( EPOCH FROM (  screening_completed_on - created_on ) ) < 3600 THEN TRUE END ) AS completed_less_than_one_hr,
                        COUNT( CASE WHEN EXTRACT( EPOCH FROM (  screening_completed_on - created_on ) ) > 3600 AND 
                                  EXTRACT( EPOCH FROM (  screening_completed_on - created_on ) ) < 7200 THEN TRUE END ) AS completed_less_than_two_hr,
                        COUNT( CASE WHEN EXTRACT( EPOCH FROM (  screening_completed_on - created_on ) ) > 7200 AND 
                                  EXTRACT( EPOCH FROM (  screening_completed_on - created_on ) ) < 10800 THEN TRUE END ) AS completed_less_than_three_hr,
                        COUNT( CASE WHEN EXTRACT( EPOCH FROM (  screening_completed_on - created_on ) ) > 10800 THEN TRUE END ) AS completed_more_than_three_hr,
					    COUNT( CASE WHEN screening_status_type_id IN( ' . sqlIntImplode( CScreeningStatusType::$c_arrintInProgressScreeningStatusTypeIds ) . '  ) THEN TRUE END ) AS total_inprogress,
					    COUNT( CASE WHEN screening_status_type_id = ' . ( int ) CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR . ' THEN TRUE END ) AS total_error,
					    AVG( screening_requested_on - created_on) AS average_requested_on,
                        AVG(screening_response_completed_on - screening_requested_on) AS average_response_completed_on,
                        AVG( ( CASE WHEN original_screening_completed_on IS NOT NULL THEN original_screening_completed_on ELSE screening_completed_on END ) - created_on) AS average_completed_on
					FROM 
						screening_transactions st
					WHERE 
						' . $strWhere . '
						AND st.screening_status_type_id NOT IN ( ' . sqlIntImplode( CScreeningStatusType::$c_arrintCancelledScreeningStatusTypeIds ) . ' )
					GROUP BY
						st.cid, st.property_id ' . $strGroupBy . '
					ORDER BY
						st.cid, st.property_id ' . $strOrderBy . ';';

		return fetchData( $strSql, $objDatabase );
	}

	public static function generateCustomScreeningReviewReportByScreeningRecordSearchFilter( $objCScreeningReportSearchFilter, $objDatabase ) {

		if( false == valObj( $objCScreeningReportSearchFilter, CScreeningReportSearchFilter::class ) ) {
			return NULL;
		}

		$strWhere = '';
		if( true == valId( $objCScreeningReportSearchFilter->getCid() ) ) {
			$strWhere .= ' st.cid = ' . ( int ) $objCScreeningReportSearchFilter->getCid();

		} else {
			return NULL;
		}

		if( true == valArr( $objCScreeningReportSearchFilter->getPropertyIds() ) ) {
			$strWhere .= ' AND st.property_id IN (' . sqlIntImplode( $objCScreeningReportSearchFilter->getPropertyIds() ) . ') ';
		}

		if( true == valStr( $objCScreeningReportSearchFilter->getFromDate() ) && true == valStr( $objCScreeningReportSearchFilter->getToDate() ) ) {
			$strWhere .= ' AND  st.created_on::DATE BETWEEN DATE(\'' . $objCScreeningReportSearchFilter->getFromDate() . '\') AND DATE(\'' . $objCScreeningReportSearchFilter->getToDate() . '\')';
		}

		if( true == valArr( $objCScreeningReportSearchFilter->getScreenTypeIds() ) ) {
			$strWhere .= ' AND st.screen_type_id IN (' . sqlIntImplode( $objCScreeningReportSearchFilter->getScreenTypeIds() ) . ') ';
		}

		$strSql = 'SELECT 
						st.cid, st.property_id, st.screen_type_id,
					    COUNT(st.id) AS total_transactions, 
					    COUNT(tr.id) AS total_reviews,
					    SUM(tr.completed_on - tr.created_on) AS total_review_time,
					    AVG(tr.completed_on - tr.reviewed_on) AS average_review_time
					FROM 
						screening_transactions st
						LEFT JOIN transaction_reviews tr ON(st.id = tr.screening_transaction_id AND st.cid = tr.cid AND tr.transaction_review_status_id = ' . CTransactionReviewStatus::COMPLETE . ' )
					WHERE 
						' . $strWhere . '
						AND st.screening_status_type_id != ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED . '
						AND st.screen_type_id IN (' . sqlIntImplode( CScreenType::$c_arrintInHouseReviewEnabledScreenTypeIds ) . ')
					GROUP BY
						st.cid, st.property_id, st.screen_type_id
					ORDER BY
						st.cid, st.property_id, st.screen_type_id';

		return fetchData( $strSql, $objDatabase );
	}

    public static function fetchScreeningTransactionSearchTypesByScreeningIdByScreeningApplicantIdByCid( $intScreeningId, $intScreeningApplicantId, $intCid, $objDatabase ) {

            $strSql = 'SELECT
						    array_to_json( array_agg ( st.search_type ) ) AS search_types,
						    st.screen_type_id,
							sact.screening_child_transaction_request_type_id
                        FROM 
                            screening_transactions st
                            LEFT JOIN screening_applicant_child_transactions sact ON sact.child_transaction_id = st.id
                        WHERE
                            st.screening_id = ' . ( int ) $intScreeningId . '
                            AND st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
                            AND st.cid = ' . ( int ) $intCid . '                      
                            AND st.search_type IS NOT NULL
                        GROUP BY st.screen_type_id, sact.screening_child_transaction_request_type_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningTransactionsByScreeningIdByCustomApplicantIdByCid( $intScreeningId, $intCustomApplicantId, $intCid, $objDatabase ) {

		if( false == valId( $intCustomApplicantId ) || false == valId( $intScreeningId ) || false == valId( $intCid ) ) return;

		$strSql = 'SELECT
						st.id as screening_transaction_id,
						st.screening_id,
						st.screening_applicant_id,
						sa.screening_package_id
					FROM
						screening_transactions st
						JOIN screening_applicants sa ON st.screening_applicant_id = sa.id AND st.cid = sa.cid
					WHERE
						st.screening_id = ' . ( int ) $intScreeningId . '
						AND sa.custom_applicant_id = ' . ( int ) $intCustomApplicantId . '
						AND st.cid = ' . ( int ) $intCid . '
					ORDER BY
						st.id DESC
					LIMIT 1';

		$arrintData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintData[0] ) ) {
			return $arrintData[0];
		}

		return $arrintData;
	}

	public static function fetchChildScreeningTransactionsByScreenTypeIdByScreeningIdByScreeningApplicantId( $intScreenTypeId, $intScreeningId, $intScreeningApplicantId, $objDatabase ) {

	    $strSql = 'SELECT 
                          st.* 
                    FROM screening_transactions st 
                    JOIN screen_types stypes ON ( st.screen_type_id = stypes.id )
                    WHERE
                        stypes.parent_screen_type_id =' . ( int ) $intScreenTypeId . ' 
                        AND st.screening_id = ' . ( int ) $intScreeningId . '
                        AND st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId;

	    return parent::fetchScreeningTransactions( $strSql, $objDatabase );

    }

	public static function fetchCompletedAndReviewedScreeningTransactionsByScreeningIdByScreeningApplicantIdByCid( $intScreeningId, $intScreeningApplicantId, $intCid, $objDatabase, $arrintScreenTypeIds = NULL ) {
		$strSubSql = '';

		if( true == valArr( $arrintScreenTypeIds ) ) {
			$strSubSql = ' AND st.screen_type_id IN( ' . implode( ',', $arrintScreenTypeIds ) . ' )';
		}

		$strSql1 = 'SELECT
							st.*,
							sdp.screening_data_provider_type_id,
							stypes.parent_screen_type_id
						FROM
							screening_transactions st
							JOIN screening_data_providers sdp ON sdp.id = st.screening_data_provider_id
							JOIN screen_types stypes ON stypes.id = st.screen_type_id  
							LEFT JOIN transaction_reviews tr ON st.id = tr.screening_transaction_id 
						WHERE
							st.screening_id = ' . ( int ) $intScreeningId . '
							AND st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
							AND tr.id IS NULL
							AND st.cid = ' . ( int ) $intCid . $strSubSql;

		$strSql2 = 'SELECT
						st.*,
							sdp.screening_data_provider_type_id,
							stypes.parent_screen_type_id
						FROM
							screening_transactions st
							JOIN screening_data_providers sdp ON sdp.id = st.screening_data_provider_id
							JOIN screen_types stypes ON stypes.id = st.screen_type_id 
							JOIN transaction_reviews tr ON st.id = tr.screening_transaction_id AND tr.transaction_review_status_id = ' . CTransactionReviewStatus::COMPLETE . '
						WHERE
							st.screening_id = ' . ( int ) $intScreeningId . '
							AND st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
							AND st.cid = ' . ( int ) $intCid . $strSubSql;

		$strSql = $strSql1 . ' UNION ' . $strSql2;

		return parent::fetchScreeningTransactions( $strSql, $objDatabase );
	}

	public static function fetchUnPostedCompletedScreeningTransactionsCountByScreenTypeIdByPropertyIdByCid( $intScreenTypeId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
							COUNT( st.id ) AS total_count
						FROM
							screening_transactions st 
							INNER JOIN screenings s ON ( s.cid = st.cid and s.id = st.screening_id and s.screening_status_type_id =  ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . ' )
						WHERE
							st.screening_batch_id IS NULL
							AND st.screening_order_id IS NOT NULL
							AND st.property_id = ' . ( int ) $intPropertyId . '
							AND st.cid = ' . ( int ) $intCid . '
							and st.created_on >= \'07/27/2019 00:00:00\'
							AND s.application_type_id <> ' . CScreeningApplicationType::VENDOR_SCREENING . '
							AND st.screen_type_id = ' . ( int ) $intScreenTypeId;

		$arrmixData = fetchData( $strSql, $objDatabase );
		return ( true == valArrKeyExists( $arrmixData[0], 'total_count' )  ? ( int ) $arrmixData[0]['total_count'] : 0 );
	}

	public static function fetchActiveScreeningTransactionsByScreenTypeIdsByScreeningIdByCustomApplicantIdsByCid( $arrintScreenTypeIds, $intScreeningId, $arrintCustomApplicantIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomApplicantIds ) ) return;

		$strSql = 'SELECT
						st.*,
						sdp.screening_data_provider_type_id,
						stypes.parent_screen_type_id
					FROM
						screening_transactions st
						JOIN screening_applicants sa ON sa.id = st.screening_applicant_id
						LEFT JOIN screening_data_providers sdp ON sdp.id = st.screening_data_provider_id
						JOIN screen_types stypes ON stypes.id = st.screen_type_id
					WHERE
						st.screening_id = ' . ( int ) $intScreeningId . '
						AND st.screening_status_type_id NOT IN( ' . implode( ',', CScreeningStatusType::$c_arrintCancelledStatusTypeIds ) . ' )
						AND st.screen_type_id IN( ' . implode( ',', $arrintScreenTypeIds ) . ' )
						AND st.cid = ' . ( int ) $intCid . '
						AND sa.custom_applicant_id IN( ' . implode( ',', $arrintCustomApplicantIds ) . ' )';

		return parent::fetchScreeningTransactions( $strSql, $objDatabase );
	}

    public static function fetchScreeningParentScreenTypeTransactionIdByIdByScreeningIdByScreeningApplicantIdByScreenTypeIdByCid( $intScreeningId, $intScreeningApplicantId, $intScreenTypeId, $intCid, $objDatabase ) {

        $strSql = 'SELECT
						id
					FROM
						screening_transactions
					WHERE
						screening_id = ' . ( int ) $intScreeningId . '
						AND screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND screen_type_id = ' . ( int ) $intScreenTypeId . '
						AND cid = ' . ( int ) $intCid;

        $arrintData = ( array ) fetchData( $strSql, $objDatabase );

        if( true == isset( $arrintData[0] ) ) {
            return $arrintData[0];
        }
    }

	public static function fetchScreeningTransactionByScreeningIdByScreeningApplicantIdByScreenTypeIdByCid( $intScreeningId, $intScreeningApplicantId, $intScreenTypeId, $intCid, $objDatabase, $boolIncludeCompletedTransaction = false ) {

		$strSubSql = '';

		if( true == $boolIncludeCompletedTransaction ) {
			$strSubSql .= ' AND st.screening_status_type_id = ' . ( int ) CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED;
		}

		$strSql = 'SELECT
						st.*,
						sdp.screening_data_provider_type_id,
						stypes.parent_screen_type_id
					FROM
						screening_transactions st
						LEFT JOIN screening_data_providers sdp ON sdp.id = st.screening_data_provider_id
						JOIN screen_types stypes ON stypes.id = st.screen_type_id
					WHERE
						st.screening_id = ' . ( int ) $intScreeningId . '
						AND st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND st.screen_type_id = ' . ( int ) $intScreenTypeId . '
						AND st.cid = ' . ( int ) $intCid . $strSubSql;

		return parent::fetchScreeningTransaction( $strSql, $objDatabase );
	}

    public static function fetchProcessedScreeningTransactions( $objCScreeningReportSearchFilter, $objDatabase ) {

        $strCondition = ' ';
	    if( CScreeningDashboardReportTypes::SCREENING_BILLING_REPORT_YEARLY_REPORT == ( int ) $objCScreeningReportSearchFilter->getIsPropertySearchFilter() ) {

            if( true == valStr( $objCScreeningReportSearchFilter->getReportYear() ) ) {

                if( CScreeningDashboardReportTypes::SCREENING_BILLING_REPORT_PREVIOUS_YEAR == $objCScreeningReportSearchFilter->getReportYear() ) {
                    $strStartYear = date( 'Y', strtotime( '-2 year' ) );
                    $strEndYear = date( 'Y', strtotime( '-1 year' ) );
                } else {
                    $strStartYear = date( 'Y', strtotime( '-1 year' ) );
                    $strEndYear = date( 'Y' );
                }
            }

            $strInnerSelectClause   = ' WITH dates as (
                       
                                            SELECT
                                                month_id,
                                                date_part( \'year\', CURRENT_DATE ) current_year,
                                                CASE WHEN month_id = 1 THEN \'12/27/' . $strStartYear . '\'  ELSE concat( month_id - 1, \'/27/' . $strEndYear . '\' )::DATE END as start_date,
                                                CASE WHEN month_id = 1 THEN  concat( month_id, \'/26/' . $strEndYear . '\' )::DATE  ELSE concat( ( month_id ), \'/26/' . $strEndYear . '\' )::DATE END as end_date
                                           FROM
                                                generate_series( 1, 12 ) as month_id
                                       ), raw_data_sql AS ( ';

            $strOuterSelectStartClause   = ' SELECT
                                               *
                                            FROM
                                               ( ';

            $strOuterSelectEndClause     = ' ) AS final_sql
                                            WHERE
                                               rank = 1 ), previous_month_data_sql AS ( SELECT 
                                                                                            *, 
                                                                                            COALESCE ( total_amount + next_month_amount, total_amount ) as total_batched_amount,
                                                                                            COALESCE ( total_count + next_month_count, total_count ) as total_batched_transaction_count
                                                                                        FROM raw_data_sql ), change_sql AS ( SELECT 
                                                                                                                                *,
                                                                                                                                LAG ( total_batched_amount, 1 )OVER ( PARTITION BY year_record ) AS prev_amt,
                                                                                                                                LAG ( total_batched_transaction_count, 1 )OVER ( PARTITION BY year_record ) AS prev_count
                                                                                                                             FROM
                                                                                                                                previous_month_data_sql )
                                                                                SELECT
                                                                                    *,
                                                                                    ( ( ( total_batched_amount::numeric - prev_amt::numeric ) / prev_amt::numeric )  * 100  ) as change_amount,
                                                                                    ( ( ( total_batched_transaction_count::numeric - prev_count::numeric ) / prev_count::numeric ) * 100 ) as change_count
                                                                                FROM
                                                                                    change_sql';

            $strSelectClause        = ' DATE_PART( \'year\', MAX( stb.created_on::DATE ) ) AS year_record,
                                        DATE_PART( \'month\', MAX( stb.created_on::DATE ) ) AS month_record_number,
                                        DATE_PART( \'day\', MAX ( stb.created_on::DATE ) ) AS day_record_number,
                                        TO_CHAR( MAX( stb.created_on::DATE ), \'Month\') AS month_record,
                                        RANK() OVER ( PARTITION BY date_part( \'year\', MAX ( stb.created_on::DATE ) ), date_part ( \'month\', MAX ( stb.created_on::DATE ) )
                                        ORDER BY DATE_PART( \'year\', MAX ( stb.created_on::DATE ) ), DATE_PART( \'month\', MAX ( stb.created_on::DATE ) ), DATE_PART( \'day\', MAX ( stb.created_on::DATE ) ) DESC ) AS rank,
                                        COUNT(*) OVER (PARTITION BY date_part(\'year\', MAX(stb.created_on::DATE)), date_part(\'month\', MAX(stb.created_on::DATE)) ) as batch_count,
                                        LEAD (SUM(st.transaction_amount), 1) OVER ( PARTITION BY date_part(\'year\', MAX(stb.created_on::DATE)), date_part(\'month\', MAX(stb.created_on::DATE)) ) AS next_month_amount,
                                        LEAD (COUNT(st.id), 1) OVER ( PARTITION BY date_part(\'year\', MAX(stb.created_on::DATE)), date_part(\'month\', MAX(stb.created_on::DATE)) ) AS next_month_count ';
            $strJoinClause          = ' JOIN dates d ON ( stb.created_on::DATE >= d.start_date AND stb.created_on::DATE <= d.end_date ) ';
            $strWhereClasue         = ' AND d.end_date <= CURRENT_DATE ';
            $strGroupByClause       = ' DATE_PART( \'day\', stb.created_on::DATE ),
                                        DATE_PART( \'month\', stb.created_on::DATE )';
            $strOrderByClause       = ' DATE_PART( \'month\', stb.created_on::DATE )';

        } else {

            if( true == $objCScreeningReportSearchFilter->getIsCustomDate() ) {
                if( true == valStr( $objCScreeningReportSearchFilter->getFromDate() ) && true == valStr( $objCScreeningReportSearchFilter->getToDate() ) ) {
                    $strCondition .= ' AND stb.created_on::DATE BETWEEN DATE(\'' . $objCScreeningReportSearchFilter->getFromDate() . '\') AND DATE(\'' . $objCScreeningReportSearchFilter->getToDate() . '\')';
                } else {
                    return NULL;
                }
            } else if( true == valStr( $objCScreeningReportSearchFilter->getMonthYear() ) ) {
                $arrintSelectedDate = explode( '-', $objCScreeningReportSearchFilter->getMonthYear() );

                $intPreviousMonth = ( $arrintSelectedDate[1] == 1 ) ? 12 : $arrintSelectedDate[1] - 1;
                $intPreviousYear  = ( $arrintSelectedDate[1] == 1 ) ? $arrintSelectedDate[0] - 1 : $arrintSelectedDate[0];

                $strCondition .= ' AND stb.created_on::DATE BETWEEN \'' . $intPreviousYear . '/' . $intPreviousMonth . '/27\' AND \'' . $arrintSelectedDate[0] . '/' . $arrintSelectedDate[1] . '/26\' ';
            } else {
                return NULL;
            }

            if( true == valArr( $objCScreeningReportSearchFilter->getCids() ) ) {
                $strCondition   .= ' AND st.cid IN ( ' . sqlIntImplode( $objCScreeningReportSearchFilter->getCids() ) . ') ';
            } else if( true == valId( $objCScreeningReportSearchFilter->getCid() ) ) {
                $strCondition .= ' AND st.cid = ' . ( int ) $objCScreeningReportSearchFilter->getCid();
            } else {
                return NULL;
            }

            $strInnerSelectClause       = ' ';
            $strOuterSelectStartClause  = ' ';
            $strOuterSelectEndClause    = ' ';
            $strSelectClause            = ' st.cid';
            $strWhereClasue             = $strCondition;
            $strJoinClause              = ' ';
            $strGroupByClause           = ' st.cid';
            $strOrderByClause           = ' total_amount DESC, total_count DESC';
        }

        $strSql = $strInnerSelectClause . ' ' .
                  $strOuterSelectStartClause . '
                  SELECT
                      COUNT( st.id ) AS total_count,
                      SUM( st.transaction_amount ) AS total_amount,
                      ' . $strSelectClause . '
                  FROM
                      screening_transactions st
                      JOIN screening_transaction_batches stb ON stb.id = st.screening_batch_id
                      ' . $strJoinClause . '
                  WHERE
                      st.screening_batch_id IS NOT NULL
					  AND st.screening_order_id IS NOT NULL
					  ' . $strWhereClasue . '
                  GROUP BY
                      ' . $strGroupByClause . '
                  ORDER by
                      ' . $strOrderByClause . ' 
                      ' . $strOuterSelectEndClause . '';

        return ( array ) fetchData( $strSql, $objDatabase );
    }

	public static function fetchSkippedScreeningTransactionByScreeningIdByScreeningApplicantIdByScreenTypeIdByCid( $intScreeningId, $intScreeningApplicantId, $intScreenTypeId, $intCid, $objDatabase ) {

		$arrintCancelledStatusTypeIds = array( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED, CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED );

		$strSql = 'SELECT
						st.is_skipped,
						st.additional_details
					FROM
						screening_transactions st						
						JOIN screen_types stypes ON stypes.id = st.screen_type_id
					WHERE
						st.screening_id = ' . ( int ) $intScreeningId . '
						AND st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND st.screening_status_type_id IN ( ' . implode( ',', $arrintCancelledStatusTypeIds ) . ' )
						AND st.cid = ' . ( int ) $intCid . '
						AND st.screen_type_id = ' . ( int ) $intScreenTypeId . '						
						AND stypes.is_published = 1
						AND st.is_skipped = true
						LIMIT 1';

		$arrintData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintData[0] ) ) {
			return $arrintData[0];
		}
	}

}
?>