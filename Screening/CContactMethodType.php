<?php

class CContactMethodType extends CBaseContactMethodType {
	const E_MAIL         = 1;
	const PHONE          = 2;
	const CERTIFIED_MAIL = 3;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getTypeName( $intContactMethodTypeId ) {

		$strContactMethodName = '';
		switch( $intContactMethodTypeId ) {
			case SELF::E_MAIL:
				$strContactMethodName = 'Email';
				break;

			case SELF::PHONE:
				$strContactMethodName = 'Phone';
				break;

			case SELF::CERTIFIED_MAIL:
				$strContactMethodName = 'Certified Mail';
				break;

			default:
				$strContactMethodName = '';
				break;
		}

		return $strContactMethodName;
	}

}
?>