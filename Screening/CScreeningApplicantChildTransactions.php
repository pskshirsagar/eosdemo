<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningApplicantChildTransactions
 * Do not add any new functions to this class.
 */

class CScreeningApplicantChildTransactions extends CBaseScreeningApplicantChildTransactions {

	public static function fetchActiveScreeningApplicantChildTransactionsByScreeningIdByScreeningApplicantIdByScreeningTransactionId( $intScreeningId, $intScreeningApplicantId, $intScreeningTransactionId, $objDatabase ) {
		$strSql = 'SELECT
						sact.*,
						st.name as screen_type_name
					FROM
						screening_applicant_child_transactions sact
						LEFT JOIN screen_types st ON st.id = sact.screen_type_id
					WHERE
						child_transaction_id IS NULL
						AND is_active = true
						AND screening_id = ' . ( int ) $intScreeningId . '
						AND screening_transaction_id = ' . ( int ) $intScreeningTransactionId . '
						AND screening_applicant_id = ' . ( int ) $intScreeningApplicantId;

		return parent::fetchScreeningApplicantChildTransactions( $strSql, $objDatabase );
	}

	public static function archieveScreeningApplicantChildTransactionResult( $intCurrentUserId, $intScreneingTransactionId, $intScreeningId, $intScreeningApplicantId, $objDatabase, $arrintScreeningChildTransactionIds = NULL ) {
		$strUpdateSql = 'UPDATE
							screening_applicant_child_transactions
						SET
							is_active = FALSE ,
							updated_by = ' . ( int ) $intCurrentUserId . ',
							updated_on = NOW() 
						WHERE
							screening_id = ' . ( int ) $intScreeningId . '
							AND screening_transaction_id = ' . ( int ) $intScreneingTransactionId . '
							AND screening_applicant_id = ' . ( int ) $intScreeningApplicantId;

		if( true == valArr( $arrintScreeningChildTransactionIds ) ) {
			$strUpdateSql .= ' AND id IN( ' . sqlIntImplode( $arrintScreeningChildTransactionIds ) . ' )';
		}

		return executeSql( $strUpdateSql, $objDatabase );
	}

	public static function fetchCustomActiveScreeningApplicantChildTransactionDetailsByChildTransactionIdByScreeningIdByScreeningApplicantId( $intChildTransactionId, $intScreeningId, $intScreeningApplicantId, $objDatabase ) {
		$strSql = 'SELECT
						child_transaction_id,
						screening_id,
						screening_applicant_id,
						reference_record_ids,
						screen_type_id,
						screening_transaction_id as parent_transaction_id
					FROM
						screening_applicant_child_transactions
					WHERE
						child_transaction_id = ' . ( int ) $intChildTransactionId . '
						AND screening_id = ' . ( int ) $intScreeningId . '
						AND screening_applicant_id = ' . ( int ) $intScreeningApplicantId;

		return executeSql( $strSql, $objDatabase );
	}

	public static function fetchCustomActiveScreeningApplicantChildTransactionsByScreeningIdByScreeningApplicantIdByScreeningTransactionId( $intScreeningId, $intScreeningApplicantId, $intScreeningTransactionId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_applicant_child_transactions
					WHERE
						is_active = true
						AND screening_id = ' . ( int ) $intScreeningId . '
						AND screening_transaction_id = ' . ( int ) $intScreeningTransactionId . '
						AND screening_applicant_id = ' . ( int ) $intScreeningApplicantId;

		return executeSql( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningApplicantChildTransactionByScreeningIdByScreeningApplicantIdByChildTransactionId( $intScreeningId, $intScreeningApplicantId, $intChildScreeningTransactionId, $objDatabase ) {
		$strSql = 'SELECT
						sact.*
					FROM
						screening_applicant_child_transactions sact
						JOIN screening_transactions st ON sact.child_transaction_id = st.id AND st.screening_id = sact.screening_id AND st.screening_applicant_id = sact.screening_applicant_id
					WHERE
						st.screening_status_type_id NOT IN ( ' . sqlIntImplode( CScreeningStatusType::$c_arrintCancelledScreeningStatusTypeIds ) . ' )
						AND sact.screening_id = ' . ( int ) $intScreeningId . '
						AND sact.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND sact.child_transaction_id = ' . ( int ) $intChildScreeningTransactionId;

		return parent::fetchScreeningApplicantChildTransaction( $strSql, $objDatabase );

	}

	public static function fetchAllScreeningApplicantChildTransactionsByScreeningIdByScreeningApplicantIdByScreeningTransactionId( $intScreeningId, $intScreeningApplicantId, $intScreeningTransactionId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_applicant_child_transactions
					WHERE
						screening_id = ' . ( int ) $intScreeningId . '
						AND screening_transaction_id = ' . ( int ) $intScreeningTransactionId . '
						AND screening_applicant_id = ' . ( int ) $intScreeningApplicantId;

		return parent::fetchScreeningApplicantChildTransactions( $strSql, $objDatabase );
	}

}
?>