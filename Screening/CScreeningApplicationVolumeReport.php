<?php

class CScreeningApplicationVolumeReport extends CBaseScreeningApplicationVolumeReport {

	const REPORT_NAME = 'volume_report';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRevenue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalApplications() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalApplicants() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalPass() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalConditional() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalFail() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalInprogress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalError() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalPassDecision() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalConditionalDecision() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalFailDecision() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalPassOverrideDecision() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalConditionalOverrideDecision() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalFailOverrideDecision() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAverageRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAverageIncome() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>