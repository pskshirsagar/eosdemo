<?php

class CRvLeaseApplicantNewDetail extends CBaseRvLeaseApplicantNewDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRvLeaseNewDetailsId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningApplicantId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningApplicantTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRvApplicantIncome() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsTransfered() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>