<?php

class CRvTestApplicants extends CBaseRvTestApplicants {

	public static function fetchRvTestApplicants( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CRvTestApplicant::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchRvTestApplicant( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CRvTestApplicant::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTestApplicantsByCid( $intCid, $objDatabase ) {
		if( !valId( $intCid ) ) return NULL;

		$strSql = 'SELECT 
						*
					FROM 
						rv_test_applicants rta
						JOIN rv_master_test_cases rmtc on rmtc.id = rta.rv_master_test_case_id
					WHERE
						rmtc.is_published = true
						AND rmtc.cid = ' . ( int ) $intCid . '
					ORDER BY rmtc.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTestApplicantsScreeningDetailsBySSN( $arrintApplicantsSSN, $objDatabase ) {
		if( !valArr( $arrintApplicantsSSN ) ) return NULL;

		$strSql = 'SELECT 
						id,
						package_id,
						ssn,
						concat(trim(lower(name_first)),trim(lower(name_last))) as full_name
					FROM 
						rv_test_applicants
					WHERE
						ssn IN  ( ' . sqlIntImplode( $arrintApplicantsSSN ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTestApplicantsBySSN( $arrintSSN, $objDatabase ) {
		if( !valArr( $arrintSSN ) ) return NULL;

		$strSql = 'SELECT 
						*
					FROM 
						rv_test_applicants rta
						JOIN rv_master_test_cases rmtc on rmtc.id = rta.rv_master_test_case_id
					WHERE
						rta.ssn IN ( ' . sqlIntImplode( $arrintSSN ) . ' )
					ORDER BY rmtc.id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>