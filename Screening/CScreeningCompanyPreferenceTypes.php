<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCompanyPreferenceTypes
 * Do not add any new functions to this class.
 */

class CScreeningCompanyPreferenceTypes extends CBaseScreeningCompanyPreferenceTypes {

	public static function fetchScreeningCompanyPreferenceTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CScreeningCompanyPreferenceType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningCompanyPreferenceType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CScreeningCompanyPreferenceType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchActiveScreeningCompanyPreferenceTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_company_preference_types
					WHERE
						is_published = TRUE';

		return parent::fetchScreeningCompanyPreferenceTypes( $strSql, $objDatabase );
	}

}
?>