<?php

class CScreeningEventActionType {

	const ACTION_PROCESS_GET_REPORT_REQUEST       = 'processGetReportRequest';
	const ACTION_PROCESS_NO_BANK_PROVIDED_REQUEST = 'processNoBankProvidedRequest';

}

?>