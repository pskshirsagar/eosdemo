<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningReviewReports
 * Do not add any new functions to this class.
 */

class CScreeningReviewReports extends CBaseScreeningReviewReports {

	public static function fetchScreeningReviewReports( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CScreeningReviewReport::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningReviewReport( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CScreeningReviewReport::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningReviewReportByScreeningRecordSearchFilter( $objCScreeningReportSearchFilter, $objDatabase ) {

		if( false == valObj( $objCScreeningReportSearchFilter, CScreeningReportSearchFilter::class ) ) {
			return NULL;
		}

		$strWhere = '';
		if( true == valId( $objCScreeningReportSearchFilter->getCid() ) ) {
			$strWhere .= ' cid = ' . ( int ) $objCScreeningReportSearchFilter->getCid();

		} else {
			return NULL;
		}

		if( true == valArr( $objCScreeningReportSearchFilter->getPropertyIds() ) ) {
			$strWhere .= ' AND property_id IN (' . sqlIntImplode( $objCScreeningReportSearchFilter->getPropertyIds() ) . ') ';
		}

		if( true == valArr( $objCScreeningReportSearchFilter->getScreenTypeIds() ) ) {
			$strWhere .= ' AND screen_type_id IN (' . sqlIntImplode( $objCScreeningReportSearchFilter->getScreenTypeIds() ) . ') ';
		}

		if( true == valStr( $objCScreeningReportSearchFilter->getMonthYear() ) ) {
			$strWhere .= ' AND report_month = DATE_TRUNC(\'month\', TIMESTAMP \'' . ( string ) $objCScreeningReportSearchFilter->getMonthYear() . '\') ';
		}

		$strSql = 'SELECT
						*
				   FROM 
				       screening_review_reports
				   WHERE
				    ' . $strWhere . '
				    ORDER BY
				        cid, property_id, screen_type_id';
		return fetchData( $strSql, $objDatabase );
	}

}
?>