<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreenTypes
 * Do not add any new functions to this class.
 */

class CScreenTypes extends CBaseScreenTypes {

    public static function fetchScreenTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CScreenType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchScreenType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CScreenType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

	public static function fetchPublishedScreenTypes( $objDatabase ) {

		$strSql = 'SELECT
						*
				   FROM
						screen_types
				   WHERE
						is_published = 1
					ORDER BY order_num';

		return parent::fetchScreenTypes( $strSql, $objDatabase );

	}

	public static function fetchPublishedPreScreeningScreenTypes( $objDatabase ) {

		$strSql = 'SELECT
						array_agg(id) as screen_type_ids
				   	FROM
						screen_types
				   	WHERE
				   		is_use_for_prescreening = true
						AND	is_published = 1';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) {
			$arrstrData[0]['screen_type_ids'] = \Psi\CStringService::singleton()->substr( $arrstrData[0]['screen_type_ids'], 1, -1 );
		}

		return ( true == valArr( $arrstrData ) ) ? explode( ',', $arrstrData[0]['screen_type_ids'] ) : array();
	}

	public static function fetchPublishedScreeningScreenTypesByIds( $arrintScreenIds, $objDatabase ) {

		if( false == valArr( $arrintScreenIds ) ) return false;

		$strSql = 'SELECT
						*
					FROM
						screen_types
					WHERE
						is_published = 1
						AND id IN ( ' . implode( ',', $arrintScreenIds ) . ' )';

		return parent::fetchScreenTypes( $strSql, $objDatabase );
	}

	public static function fetchAllScreenTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screen_types';
		return parent::fetchScreenTypes( $strSql, $objDatabase );
	}

	public static function fetchPublishedParentScreenTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screen_types
					WHERE is_published = 1
						AND parent_screen_type_id IS NULL 
						ORDER BY order_num ASC';
		return parent::fetchScreenTypes( $strSql, $objDatabase );
	}

	public static function fetchScreenTypeNameById( $intId, $objDatabase ) {
		return self::fetchColumn( sprintf( 'SELECT name FROM screen_types WHERE id = %d', ( int ) $intId ), 'name', $objDatabase );
	}

    public static function fetchPublishedScreenTypesByExcludingScreenTypeIds( $arrintExcludeScreenTypeIds, $objDatabase ) {

        if( false == valArr( $arrintExcludeScreenTypeIds ) ) return false;

        $strSql = 'SELECT
						*
					FROM
						screen_types
					WHERE
						is_published = 1
						AND id NOT IN ( ' . implode( ',', $arrintExcludeScreenTypeIds ) . ' )';

        return parent::fetchScreenTypes( $strSql, $objDatabase );
    }

    public static function fetchScreenTypesByIds( $arrintIds, $objDatabase ) {
        if( false == valArr( $arrintIds ) ) return NULL;

        $strSql = 'SELECT
                       *
                  FROM
                       screen_types
                   WHERE
                          id IN( ' . implode( ',', $arrintIds ) . ' )';

        return parent::fetchScreenTypes( $strSql, $objDatabase );
    }

}
?>