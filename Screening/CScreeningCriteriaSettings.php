<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCriteriaSettings
 * Do not add any new functions to this class.
 */

class CScreeningCriteriaSettings extends CBaseScreeningCriteriaSettings {

	public static function fetchActiveScreeningCriteriaSettingsDataByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {
		$strSql = sprintf( 'SELECT * FROM screening_criteria_settings WHERE screening_criteria_id = %d', ( int ) $intScreeningCriteriaId );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningCriteriaSettingsByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {
		$strSql = 'SELECT
						scs.*,
						scontionsets.evaluation_order,
						scontionsets.screening_recommendation_type_id,
						scpt.screening_config_value_type_id,
						scpt.screen_type_id
					FROM
						screening_criteria_settings scs
						JOIN screening_condition_sets scontionsets ON scontionsets.id = scs.screening_condition_set_id
						JOIN screening_config_preference_types scpt ON scpt.id = scs.screening_config_preference_type_id
					WHERE
						scs.screening_criteria_id = ' . ( int ) $intScreeningCriteriaId . '
						AND scpt.is_published = 1
					ORDER BY
						scontionsets.evaluation_order';

		return parent::fetchScreeningCriteriaSettings( $strSql, $objDatabase );
	}

	public static function fetchScreeningCriteriaSettingsByScreeningPackageIdsByScreeningConfigPreferenceTypeIds( $arrintScreeningPackageIds, $arrintScreeningConfigPreferenceTypeIds, $objDatabase ) {

		if( false == valArr( $arrintScreeningPackageIds ) ) {
			return;
		}

		$strSql = 'SELECT
						scs.*,
						scs1.screening_recommendation_type_id,
						spsta.screening_package_id
					FROM
						screening_criteria_settings scs
					    JOIN  screening_condition_sets scs1 ON(scs1.id = scs.screening_condition_set_id)
					    JOIN screening_package_screen_type_associations spsta ON (spsta.screening_criteria_id = scs.screening_criteria_id )
					WHERE
						spsta.screening_package_id IN( ' . implode( ',', $arrintScreeningPackageIds ) . ' )
						AND scs.screening_config_preference_type_id IN( ' . implode( ',', $arrintScreeningConfigPreferenceTypeIds ) . ' )';

		return parent::fetchObjects( $strSql, CScreeningCriteriaSetting::class, $objDatabase, false );
	}

	public static function fetchScreeningCriteriaSettingsByScreenTypeIdByScreeningPackageId( $intScreenTypeId, $intScreeningPackageId, $objDatabase ) {
		$strSql = 'SELECT
						scs.*,
						spcs.evaluation_order,
						spcs.screening_recommendation_type_id,
						scpt.screening_config_value_type_id,
						scpt.screen_type_id
					FROM
						screening_criteria_settings scs
						JOIN screening_condition_sets spcs ON spcs.id = scs.screening_condition_set_id AND spcs.is_published = TRUE 
						JOIN screening_config_preference_types scpt ON scpt.id = scs.screening_config_preference_type_id
						JOIN screening_package_screen_type_associations spcta ON spcta.screen_type_id = scpt.screen_type_id AND scs.screening_criteria_id = spcta.screening_criteria_id
					WHERE						
						spcta.screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND spcta.screen_type_id = ' . ( int ) $intScreenTypeId . '
					ORDER BY
						spcs.evaluation_order';

		return parent::fetchScreeningCriteriaSettings( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningCriteriaSettingsByScreeningConditionSetId( $intScreeningConditionSetId, $arrintScreeningCriteriaIds, $objDatabase ) {
		if( false == valArr( $arrintScreeningCriteriaIds ) ) return;

		$strSql = 'SELECT
						*
					FROM
						screening_criteria_settings
					WHERE
						screening_criteria_id IN( ' . sqlIntImplode( $arrintScreeningCriteriaIds ) . ' )
						AND screening_condition_set_id = ' . ( int ) $intScreeningConditionSetId;

		return parent::fetchScreeningCriteriaSettings( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningCriteriaSettingsByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {

		$strSql = 'SELECT
						scs.screening_condition_set_id,
						spcta.screening_package_id,
						scs.required_min_value,
						scs.required_max_value,
		 				scs.screening_config_preference_type_id
					FROM
						screening_criteria_settings scs
						JOIN screening_config_preference_types scpt ON scpt.id = scs.screening_config_preference_type_id
						JOIN screening_package_screen_type_associations spcta ON spcta.screen_type_id = scpt.screen_type_id AND scs.screening_criteria_id = spcta.screening_criteria_id
					WHERE
						screening_package_id = ' . ( int ) $intScreeningPackageId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningCriteriaSettingsByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {

		$strSql = ' SELECT
						scs.*
					FROM
						screening_criteria_settings scs
						JOIN screening_package_screen_type_associations spcta ON ( scs.screening_criteria_id = spcta.screening_criteria_id )
					WHERE
						spcta.screening_package_id = ' . ( int ) $intScreeningPackageId . '
					ORDER BY
    					scs.id DESC';

		return parent::fetchScreeningCriteriaSettings( $strSql, $objDatabase );
	}

	public static function fetchScreeningCriteriaSettingsByScreeningPackageIdsByCid( $arrintPackageIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPackageIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						sp.id AS screening_package_id,
						scs.required_max_value::NUMERIC,
						scs.required_min_value::NUMERIC,
						scpt.screening_config_value_type_id,
						scpt.id,
						scpt.name,
						scs1.condition_name
					FROM
						screening_criteria_settings scs
						JOIN screening_package_screen_type_associations spcta ON ( scs.screening_criteria_id = spcta.screening_criteria_id )
						JOIN screening_packages sp ON ( sp.id = spcta.screening_package_id )
						JOIN screening_config_preference_types scpt ON ( scpt.id = scs.screening_config_preference_type_id )
						JOIN screening_condition_sets scs1 ON ( scs1.id = scs.screening_condition_set_id )
					WHERE
						spcta.screening_package_id IN ( ' . sqlIntImplode( $arrintPackageIds ) . ' )
						AND sp.cid = ' . ( int ) $intCid . '
						AND sp.is_active IS TRUE
						AND sp.is_published = 1
					ORDER BY 
					    scpt.sort_order';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageIdsForScreeningApplicantHavingRentalCollectionCriteriaByScreeningIdByCid( $intScreeningId, $intCid, $objDatabase ) {
		$arrintScreeningConfigPreferenceTypeIds = [ CScreeningConfigPreferenceType::RENTAL_COLLECTIONS, CScreeningConfigPreferenceType::RENTAL_COLLECTION_DOLLARS ];

		$strSql = '	SELECT
						DISTINCT(spsta.screening_package_id)
					FROM
						screening_criteria_settings scs
						JOIN screening_package_screen_type_associations spsta ON ( spsta.screening_criteria_id = scs.screening_criteria_id )
						JOIN screening_applicants sa ON ( sa.screening_package_id = spsta.screening_package_id )
					WHERE
						sa.screening_id = ' . ( int ) $intScreeningId . '
						AND sa.cid = ' . ( int ) $intCid . ' 
						AND spsta.screen_type_id = ' . ( int ) CScreenType::CREDIT . '
						AND	scs.screening_config_preference_type_id IN( ' . sqlIntImplode( $arrintScreeningConfigPreferenceTypeIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningCriteriaSettingsByScreeningCriteriaIdByScreeningConfigPreferenceTypeId( $intScreeningCriteriaId, $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_criteria_settings
					WHERE
						screening_criteria_id = ' . ( int ) $intScreeningCriteriaId . '
						AND screening_config_preference_type_id = ' . ( int ) $intScreeningConfigPreferenceTypeId;
		return parent::fetchScreeningCriteriaSettings( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningCriteriasSettingsByScreeningPackageIdByScreeningApplicantIdsByCid( $arrintScreeningPackageIds, $arrintScreeningApplicantIds, $intCid, $objDatabase ) {
		if( !valArr( $arrintScreeningPackageIds ) ) return NULL;

		$strSql = 'SELECT
					    custom_applicant_id,
                        scs.screening_config_preference_type_id
					FROM
						screening_applicants sa 
                        JOIN screening_package_screen_type_associations spsta on sa.screening_package_id = spsta.screening_package_id
                        JOIN screening_criteria_settings scs on scs.screening_criteria_id = spsta.screening_criteria_id
					WHERE
						sa.screening_package_id IN ( ' . sqlIntImplode( $arrintScreeningPackageIds ) . ' )
						AND sa.id IN ( ' . sqlIntImplode( $arrintScreeningApplicantIds ) . ')
						AND sa.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningCriteriasettingsByScreeningPackageIdByScreenTypeIdByScreeningConfigPreferenceTypeIdByCid( $intScreeningPackageId, $intScreenTypeId, $intScreeningConfigPreferenceTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						scs.id,
						scs.screening_recommendation_type_id
					FROM
						screening_condition_sets scs
						JOIN screening_criteria_settings scs1 ON ( scs1.screening_condition_set_id = scs.id )
						JOIN screening_package_screen_type_associations spsta ON ( spsta.screening_criteria_id = scs1.screening_criteria_id )
					WHERE
						scs.cid = ' . ( int ) $intCid . '
						AND scs1.screening_config_preference_type_id = ' . ( int ) $intScreeningConfigPreferenceTypeId . '
						AND spsta.screen_type_id = ' . ( int ) $intScreenTypeId . '
						AND spsta.screening_package_id = ' . ( int ) $intScreeningPackageId;

		return fetchdata( $strSql, $objDatabase );
	}

}

?>