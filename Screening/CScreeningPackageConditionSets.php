<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageConditionSets
 * Do not add any new functions to this class.
 */
class CScreeningPackageConditionSets extends CBaseScreeningPackageConditionSets {

	public static function fetchScreeningPackageSetsByScreeningPackageIdByCid( $intScreeningPackageId, $intCid, $objDatabase, $boolExcludeConditionNamePassFail = false ) {

		$strWhere = '';

		if( true == $boolExcludeConditionNamePassFail ) {
			$strWhere = ' AND first_screening_package_condition_subset_id IS NOT NULL ';
		}

		$strSql = '	SELECT
						*
					FROM
						screening_package_condition_sets
					WHERE
						screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND cid = ' . ( int ) $intCid . '
						AND is_published = 1'
						. $strWhere .
				  ' ORDER BY
						evaluation_order ASC,
						id ASC';

		return parent::fetchScreeningPackageConditionSets( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageConditionSetByIdByCid( $intScreningPackageConditionSetId, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						*
					FROM
						screening_package_condition_sets
					WHERE
						id = ' . ( int ) $intScreningPackageConditionSetId . '
						AND cid = ' . ( int ) $intCid;

		return parent::fetchScreeningPackageConditionSet( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningPackageConditionSetsByScreeningPackageIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {
		if ( true == is_null( $intScreeningPackageId ) || false == isset( $intCid ) ) return NULL;

		$strSql = 'SELECT
						id as condition_id,
						condition_name,
						screening_recommendation_type_id
					FROM
						screening_package_condition_sets
					WHERE
						screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND cid = ' . ( int ) $intCid . '
						AND is_published = 1
					ORDER BY condition_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageConditionSetsByIdsByCids( $arrintScreeningPackageConditionSetsIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScreeningPackageConditionSetsIds ) ) return;

		$strSql = 'SELECT
						*
					FROM
						screening_package_condition_sets
					WHERE
						id IN(' . implode( ',', $arrintScreeningPackageConditionSetsIds ) . ')
						AND cid = ' . ( int ) $intCid;

		return parent::fetchScreeningPackageConditionSets( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningPackageConditionSetsRecommendationsByIdsByCid( $arrintScreeningPackageConditionSetIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScreeningPackageConditionSetIds ) ) return NULL;

		$strSql = 'SELECT
						id, screening_recommendation_type_id
				   FROM
						screening_package_condition_sets
				   WHERE
						id IN ( ' . implode( ',', $arrintScreeningPackageConditionSetIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningPackageConditionSetsByScreeningPackageIdsByCid( $arrintScreeningPackageIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScreeningPackageIds ) ) return;

		$strSql = 'SELECT
						id,
						screening_recommendation_type_id,
						screening_package_id
					FROM
						screening_package_condition_sets
					WHERE
						screening_package_id IN (' . implode( ',', $arrintScreeningPackageIds ) . ')
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningPackageConditionsByPackageIdByConditionTypeIdsByCid( $intScreeningPackageId, $arrintConditionTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintConditionTypeIds ) ) return NULL;

		$strSql = ' SELECT
						sv.*,
						cs.id AS screening_package_condition_set_id,
						ac.base_charge_code_id,
						ac.apply_to_charge_code_id,
						ac.screening_package_charge_code_amount_type_id,
						ac.screening_package_condition_type_id
					FROM
						screening_package_condition_sets cs
						JOIN screening_package_condition_subset_values sv ON sv.screening_package_condition_subset_id = cs.first_screening_package_condition_subset_id OR sv.screening_package_condition_subset_id = cs.second_screening_package_condition_subset_id
						JOIN screening_package_available_conditions ac ON ac.id = sv.screening_package_available_condition_id
						JOIN screening_package_condition_types ct ON ct.id = ac.screening_package_condition_type_id
					WHERE
						cs.cid = ' . ( int ) $intCid . '
						AND cs.screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND ct.screening_value_type_id IN ( ' . implode( ',', $arrintConditionTypeIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageConditionSetsByConditionSetdsByCid( $arrintConditionSetIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintConditionSetIds ) ) return NULL;

		$strSql = '	SELECT
						spcs.cid,
						spcs.condition_name AS condition_set_name,
						sp.name AS package_name,
						array_to_string ( array_agg ( spcsv.id ), \',\' ) AS condition_subset_value_id,
						array_to_string ( array_agg ( screening_package_available_condition_name ), \',\' ) AS screening_package_available_condition_name
					FROM
						screening_package_condition_sets spcs
						JOIN screening_package_condition_subset_values spcsv ON ( spcsv.screening_package_condition_subset_id = spcs.first_screening_package_condition_subset_id OR spcsv.screening_package_condition_subset_id = spcs.second_screening_package_condition_subset_id )
						JOIN screening_package_available_conditions spac ON spac.id = spcsv.screening_package_available_condition_id
						JOIN screening_packages sp ON sp.id = spcs.screening_package_id
					WHERE
						spcs.id in ( ' . implode( ',', $arrintConditionSetIds ) . ' )
						AND spac.screening_package_condition_type_id IN ( ' . implode( ',', CScreeningPackageConditionType::$c_arrintFinancialScreeningConditionTypeIds ) . ' )
						AND spac.is_published = 1
						AND sp.is_published = 1
						AND spcs.cid = ' . ( int ) $intCid . '
					GROUP BY
						spcs.CID,
						sp.name,
						spcs.condition_name
					ORDER BY
						spcs.CID ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchConditionSetsByName( $strConditionSetname, $intConditionSetId, $intScreeningPackageId, $intCid, $objDatabase ) {
		$strWhere = '';

		if( 0 != $intConditionSetId ) {
			$strWhere = ' AND id <> ' . ( int ) $intConditionSetId;
		}

		$strSql = '	SELECT
						id
					FROM
						screening_package_condition_sets
					WHERE
						condition_name = \'' . str_replace( "'", "\'", $strConditionSetname ) . '\'
						AND cid = ' . ( int ) $intCid . '
						AND screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND is_published = 1'
						. $strWhere;

		return fetchData( $strSql, $objDatabase );
	}

}
?>