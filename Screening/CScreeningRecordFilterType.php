<?php

class CScreeningRecordFilterType extends CBaseScreeningRecordFilterType {

	const UNFILTERED 							= 1;
	const FILTERED_BY_FILTERS 					= 2;
	const FILTERED_BY_DISPOSITION 				= 3;
	const FILTERED_BY_USER						= 4;
	const FILTERED_BY_USER_MISMATCH				= 5;
	const FILTERED_BY_USER_DISPOSITION 			= 6;
	const FILTERED_BY_USER_OFFENSETYPE 			= 7;
	const FILTERED_BY_JURISDICTION				= 8;
	const FILTERED_BY_STATE_STRICT_CONVICTION	= 9;
	const FILTERED_BY_SYSTEM_CHILD				= 10;
    const FILTERED_BY_USER_DISPUTE				= 11;

	public static $c_arrintUserFilterTypeIds = array(
	    self::FILTERED_BY_USER,
	    self::FILTERED_BY_USER_MISMATCH,
	    self::FILTERED_BY_USER_DISPOSITION,
	    self::FILTERED_BY_USER_OFFENSETYPE,
        self::FILTERED_BY_USER_DISPUTE
	);

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>