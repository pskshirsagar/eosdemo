<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CSelectionModeTypes
 * Do not add any new functions to this class.
 */

class CSelectionModeTypes extends CBaseSelectionModeTypes {

	public static function fetchSelectionModeTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CSelectionModeType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSelectionModeType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CSelectionModeType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

    public static function fetchPublishedSelectionModeTypes( $objDatabase ) {

        $strSql = 'SELECT
						*
				   FROM
						selection_mode_types
				   WHERE
						is_published = TRUE';

        return parent::fetchSelectionModeTypes( $strSql, $objDatabase );

    }

}
?>