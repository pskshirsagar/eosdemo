<?php

class CScreeningPackageType extends CBaseScreeningPackageType {

	const CONVENTIONAL	= 1;
	const STUDENT		= 2;
	const AFFORDABLE	= 3;
	const CORPORATE		= 4;
	const VA_CORPORATE	= 5;
	const VA_CRIMINAL	= 6;
	const BAN_THE_BOX	= 7;
	const MILITARY	    = 8;

	public static $c_arrintVAScreeningPackageTypes = [ self::VA_CORPORATE => self::VA_CORPORATE, self::VA_CRIMINAL => self::VA_CRIMINAL ];
	public static $c_arrintNonAffordableScreeningPackageTypes = array( self::CONVENTIONAL, self::STUDENT,self::CORPORATE, self::BAN_THE_BOX, self::MILITARY );
	public static $c_arrintSkipBoxValidationPackageTypes = [ self::BAN_THE_BOX, self::MILITARY, self::AFFORDABLE ];

	public function getScreeningPackageTypeByOccupancyTypeId( $intOccupancyType ) {
		switch( $intOccupancyType ) {
			case COccupancyType::AFFORDABLE:
				$intScreeningPackageTypeId = self::AFFORDABLE;
				break;

			case COccupancyType::CONVENTIONAL:
			default:
				$intScreeningPackageTypeId = self::CONVENTIONAL;
				break;
		}

		return $intScreeningPackageTypeId;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>