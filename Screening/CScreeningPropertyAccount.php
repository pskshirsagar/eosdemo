<?php

class CScreeningPropertyAccount extends CBaseScreeningPropertyAccount {

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
           		break;

            default:
            // adding default:
        }

        return $boolIsValid;
    }

    /**
     * Other Functions
     */

    public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
    	if( true == is_null( $this->getId() ) ) {
    		return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	} else {
    		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	}
    }

    public function archivePropertyScreeningAccount( $objDatabase, $boolReturnSqlOnly = false ) {
    	$strSql = 'UPDATE screening_property_accounts SET is_published = 0 WHERE id = ' . ( int ) $this->sqlId() . ';';

    	if( true == $boolReturnSqlOnly ) {
    		return $strSql;
    	} else {
    		return $this->executeSql( $strSql, $this, $objDatabase );
    	}
    }
}
?>