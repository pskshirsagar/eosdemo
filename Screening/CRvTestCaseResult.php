<?php

class CRvTestCaseResult extends CBaseRvTestCaseResult {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRvMasterTestCaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRvTestRunBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningResultDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStatus() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}


}
?>