<?php

class CTransactionChargeType extends CBaseTransactionChargeType {

	const TOTAL_EXPERIAN_CREDIT_TRANSACTIONS                 = 1;
	const TOTAL_FRAUD_SHIELD_HIT_TRANSACTIONS                = 2;
	const TOTAL_FRAUD_SHIELD_NO_HIT_TRANSACTIONS             = 3;
	const TOTAL_EXPERIAN_COLORADO_CREDIT_TRANSACTIONS        = 4;
	const TOTAL_EXPERIAN_CREDIT_PROFILE_SUMMARY_TRANSACTIONS = 5;
	const TOTAL_EXPERIAN_CREDIT_SCORE_TRANSACTIONS           = 6;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChargeAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveFrom() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>