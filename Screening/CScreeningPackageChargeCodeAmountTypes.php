<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageChargeCodeAmountTypes
 * Do not add any new functions to this class.
 */

class CScreeningPackageChargeCodeAmountTypes extends CBaseScreeningPackageChargeCodeAmountTypes {

	public static function fetchScreeningPackageChargeCodeAmountTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningPackageChargeCodeAmountType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchScreeningPackageChargeCodeAmountType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningPackageChargeCodeAmountType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>