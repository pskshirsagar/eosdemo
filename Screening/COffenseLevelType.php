<?php

class COffenseLevelType extends CBaseOffenseLevelType {

	const FELONY 		= 1;
	const MISDEMEANOR 	= 2;
	const UNKNOWN 		= 3;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// need to add
        }

        return $boolIsValid;
    }

}
?>