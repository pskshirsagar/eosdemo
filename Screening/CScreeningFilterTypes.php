<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningFilterTypes
 * Do not add any new functions to this class.
 */

class CScreeningFilterTypes extends CBaseScreeningFilterTypes {

	public static function fetchScreeningFilterTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningFilterType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

    public static function fetchScreeningFilterType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScreeningFilterType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>