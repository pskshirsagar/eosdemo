<?php

class CScreeningMetaDataType extends CBaseScreeningMetaDataType {

	const KEYWORD 	= 1;
	const VENDOR 	= 2;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>