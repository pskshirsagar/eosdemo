<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageScreenTypeActions
 * Do not add any new functions to this class.
 */

class CScreeningPackageScreenTypeActions extends CBaseScreeningPackageScreenTypeActions {

	public static function fetchPublishedScreeningPackageScreenTypeActionsByScreeningPackageIdByScreenTypeId( $intScreeningPackageId, $intScreenTypeId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						screening_package_screen_type_actions
					WHERE
						screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND screen_type_id = ' . ( int ) $intScreenTypeId . '
						AND is_published = 1
					LIMIT 1';

		return parent::fetchScreeningPackageScreenTypeActions( $strSql, $objDatabase );
	}

}
?>