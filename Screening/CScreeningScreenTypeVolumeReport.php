<?php

class CScreeningScreenTypeVolumeReport extends CBaseScreeningScreenTypeVolumeReport {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRevenue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreenTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalTransactions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalPass() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalConditional() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalFail() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalInprogress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalError() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAverageRequestedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAverageResponseCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAverageCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>