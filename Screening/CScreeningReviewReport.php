<?php

class CScreeningReviewReport extends CBaseScreeningReviewReport {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreenTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalTransactions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalReviews() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalReviewTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAverageReviewTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>