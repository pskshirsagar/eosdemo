<?php

class CScreeningPackageChargeCodeAmountType extends CBaseScreeningPackageChargeCodeAmountType {

	const PERCENTAGE 			= 1;
	const ABSOLUTE 				= 2;
	const PERCENTAGE_RENT 		= 3;

    public static function getSymbolForChargeCodeAmountType( $intChargeCodeAmountType ) {

        $strSymbol = '';

        switch( $intChargeCodeAmountType ) {
            case self::PERCENTAGE:
                $strSymbol = '%';
                break;

			case self::PERCENTAGE_RENT:
                $strSymbol = '% of Rent';
                break;

			case self::ABSOLUTE:
                $strSymbol = '$';
            	break;

            default:
                // added default case
        }

        return $strSymbol;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>