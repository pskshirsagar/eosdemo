<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageScreenTypeCriterias
 * Do not add any new functions to this class.
 */

class CScreeningPackageScreenTypeCriterias extends CBaseScreeningPackageScreenTypeCriterias {

	public static function fetchScreeningPackageScreenTypeCriteriasByScreeningPackageIdByScreeningSearchCriteriaTypeIds( $intScreeningPackageId, $arrintScreeningSearchCriteriaTypeIds, $objDatabase ) {

		if( false == valArr( array_filter( ( array ) $arrintScreeningSearchCriteriaTypeIds ) ) ) return;

		$strSql = 'SELECT
						spstc.*,
						spsta.screen_type_id,
						spsta.screening_data_provider_id
					FROM
						screening_package_screen_type_criterias spstc
						JOIN screening_package_screen_type_associations spsta ON spsta.id = spstc.screening_package_screen_type_associations_id
					WHERE
						spstc.screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND spstc.is_published = 1
						AND spstc.screening_search_criteria_type_id IN( ' . implode( ',', $arrintScreeningSearchCriteriaTypeIds ) . ' )';

		return parent::fetchScreeningPackageScreenTypeCriterias( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeCriteriasByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {

		$strSql = 'SELECT
						spstc.*, spsta.screening_data_provider_id, spsta.screen_type_id, sdp.name as screening_data_provider_name
					FROM
						screening_package_screen_type_criterias spstc
						INNER JOIN screening_package_screen_type_associations spsta ON spsta.id = spstc.screening_package_screen_type_associations_id AND spsta.screening_package_id = spstc.screening_package_id
						INNER JOIN screening_data_providers sdp ON sdp.id = spsta.screening_data_provider_id
					WHERE
						spstc.is_published = 1
						AND	spstc.screening_package_id = ' . ( int ) $intScreeningPackageId . '
					ORDER BY spsta.screen_type_id ASC, spstc.screening_search_criteria_type_id ASC, spstc.dispatch_type DESC';

		return parent::fetchScreeningPackageScreenTypeCriterias( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeCriteriasByCrimininalSearchCriteriaId( $intCrimininalSearchCriteriaId, $objDatabase ) {
		$strSql = 'SELECT
						spstc.*, spsta.screening_data_provider_id, spsta.screen_type_id
					FROM
						screening_package_screen_type_criterias spstc
						INNER JOIN screening_package_screen_type_associations spsta ON spsta.id = spstc.screening_package_screen_type_associations_id AND spsta.screening_package_id = spstc.screening_package_id
					WHERE
						spstc.is_published = 1
						AND spstc.id = ' . ( int ) $intCrimininalSearchCriteriaId;

		return parent::fetchScreeningPackageScreenTypeCriteria( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeCriteriasByScreeningPackageScreenTypeAssociationId( $intScreeningPackageScreenTypeAssociationId, $objDatabase ) {
		$strSql = 'SELECT
						spstc.*, spsta.screening_data_provider_id
					FROM
						screening_package_screen_type_criterias spstc
						INNER JOIN screening_package_screen_type_associations spsta ON spsta.id = spstc.screening_package_screen_type_associations_id AND spsta.screening_package_id = spstc.screening_package_id
					WHERE
						spstc.is_published = 1
						AND spstc.screening_package_screen_type_associations_id = ' . ( int ) $intScreeningPackageScreenTypeAssociationId;

		return parent::fetchScreeningPackageScreenTypeCriterias( $strSql, $objDatabase );

	}

	public static function fetchScreeningPackageScreenTypeCriteriasByScreenTypeIdByCriminalSearchCriteriaTypeIdByCriminalScreenTypeDataProviderByScreeningPackageId( $intCriminalScreenTypeId, $intCriminalSearchCriteriaTypeId, $intCriminalScreenTypeDataProvider, $intScreeningPackageId, $objDatabase ) {

		$strSql = 'SELECT
						spstc.*
					FROM
						screening_package_screen_type_criterias spstc
						INNER JOIN screening_package_screen_type_associations spsta ON spsta.id = spstc.screening_package_screen_type_associations_id AND spsta.screening_package_id = spstc.screening_package_id
					WHERE
						spstc.is_published = 1
						AND spstc.screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND spstc.screening_search_criteria_type_id = ' . ( int ) $intCriminalSearchCriteriaTypeId . '
						AND spsta.screening_data_provider_id = ' . ( int ) $intCriminalScreenTypeDataProvider . '
						AND spsta.screen_type_id = ' . ( int ) $intCriminalScreenTypeId;

		return parent::fetchScreeningPackageScreenTypeCriteria( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeCriteriasByScreenTypeIdByScreeningPackageIdByScreeningSearchCriteriaTypeIds( $intScreenTypeId, $intScreeningPackageId, $objDatabase ) {

		$strSql = 'SELECT
						spstc.*
					FROM
						screening_package_screen_type_criterias spstc
						JOIN screening_package_screen_type_associations spsta ON ( spsta.id = spstc.screening_package_screen_type_associations_id AND spsta.screening_package_id = spstc.screening_package_id AND spsta.screen_type_id = ' . ( int ) $intScreenTypeId . ' )
					WHERE
						spstc.is_published = 1
						AND spstc.screening_package_id = ' . ( int ) $intScreeningPackageId;

				return parent::fetchScreeningPackageScreenTypeCriterias( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeCriteriasByScreenTypeIdByCriminalSearchCriteriaTypeIdsByCriminalScreenTypeDataProviderByScreeningPackageId( $intCriminalScreenTypeId, $arrintCriminalSearchCriteriaTypeIds, $intCriminalScreenTypeDataProvider, $intScreeningPackageId, $objDatabase ) {

		if( false == ( $arrintCriminalSearchCriteriaTypeIds = getIntValuesFromArr( $arrintCriminalSearchCriteriaTypeIds ) ) ) {
			return false;
		}

		$strSql = 'SELECT
						spstc.*
					FROM
						screening_package_screen_type_criterias spstc
						INNER JOIN screening_package_screen_type_associations spsta ON spsta.id = spstc.screening_package_screen_type_associations_id AND spsta.screening_package_id = spstc.screening_package_id
					WHERE
						spstc.is_published = 1
						AND spstc.screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND spstc.screening_search_criteria_type_id IN ( ' . implode( ',', $arrintCriminalSearchCriteriaTypeIds ) . ' )
						AND spsta.screening_data_provider_id = ' . ( int ) $intCriminalScreenTypeDataProvider . '
						AND spsta.screen_type_id = ' . ( int ) $intCriminalScreenTypeId;

		return parent::fetchScreeningPackageScreenTypeCriterias( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningPackageScreenTypeCriteriasByScreeningPackageIdByScreeningSearchCriteriaTypeIds( $intScreeningPackageId, $arrintScreeningSearchCriteriaTypeIds, $objDatabase ) {
		if( false == valArr( array_filter( ( array ) $arrintScreeningSearchCriteriaTypeIds ) ) ) return;

		$strSql = 'SELECT
						spstc.screening_search_criteria_type_id,
						spstc.criteria,
						spstc.offense_description,
						spsta.screen_type_id,
						spsta.screening_data_provider_id
					FROM
						screening_package_screen_type_criterias spstc
						JOIN screening_package_screen_type_associations spsta ON spsta.id = spstc.screening_package_screen_type_associations_id						
					WHERE
						spstc.screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND spstc.is_published = 1
						AND spstc.screening_search_criteria_type_id IN( ' . implode( ',', $arrintScreeningSearchCriteriaTypeIds ) . ' )';

		return executeSql( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeCriteriasByScreeningPackageIds( $arrintScreeningPackageIds, $objDatabase ) {

		$strSql = 'SELECT
						spstc.*, spsta.screening_data_provider_id, spsta.screen_type_id, sdp.name as screening_data_provider_name
					FROM
						screening_package_screen_type_criterias spstc
						INNER JOIN screening_package_screen_type_associations spsta ON spsta.id = spstc.screening_package_screen_type_associations_id AND spsta.screening_package_id = spstc.screening_package_id
						INNER JOIN screening_data_providers sdp ON sdp.id = spsta.screening_data_provider_id
					WHERE
						spstc.is_published = 1
						AND	spstc.screening_package_id IN ( ' . sqlIntImplode( $arrintScreeningPackageIds ) . ' )
					ORDER BY spsta.screen_type_id ASC, spstc.screening_search_criteria_type_id ASC, spstc.dispatch_type DESC';

		return parent::fetchScreeningPackageScreenTypeCriterias( $strSql, $objDatabase );
	}

}
?>