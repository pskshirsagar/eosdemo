<?php

class CScreeningTransactionDetail extends CBaseScreeningTransactionDetail {

	public static $c_arrstrScreeningTransactionDetails	= array(
		'has_credit_record'				=> NULL,
		'has_credit_score' 				=> NULL,
		'has_profile_summary'		    => NULL,
		'has_fraud_hit'					=> NULL,
		'is_otp_generated'				=> NULL,
		'is_kiq_generated'              => NULL
	);

	const HAS_CREDIT_RECORD      = 'has_credit_record';
	const HAS_CREDIT_SCORE       = 'has_credit_score';
	const HAS_PROFILE_SUMMARY    = 'has_profile_summary';
	const HAS_FRAUD_HIT          = 'has_fraud_hit';
	const IS_OTP_GENERATED       = 'is_otp_generated';
	const IS_KIQ_GENERATED       = 'is_kiq_generated';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasCreditRecord() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasCreditScore() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasProfileSummary() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasFraudHit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOtpGenerated() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsKiqGenerated() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>