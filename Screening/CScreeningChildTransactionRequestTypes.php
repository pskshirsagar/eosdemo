<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningChildTransactionRequestTypes
 * Do not add any new functions to this class.
 */

class CScreeningChildTransactionRequestTypes extends CBaseScreeningChildTransactionRequestTypes {

	public static function fetchScreeningChildTransactionRequestTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningChildTransactionRequestType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchScreeningChildTransactionRequestType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningChildTransactionRequestType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>