<?php

class CRvMasterTestCases extends CBaseRvMasterTestCases {

	public static function fetchCustomMasterTestCaseScreeningApplicantDetailsByCid( $intCid, $objDatabase ) {
		if( !valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
					    sa.screening_id,
					    sa.id as screening_applicant_id,
					    custom_applicant_id
					FROM
					    screening_applicants sa
					    JOIN rv_master_test_cases rmtc ON rmtc.screening_id = sa.screening_id and rmtc.cid = sa.cid 
					WHERE
						rmtc.is_published = true
						AND rmtc.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomMasterTestCaseResultsByCid( $intCid, $objDatabase ) {
		if( !valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						id,
					    screening_id,
					    screening_result_details
					FROM
					    rv_master_test_cases
					WHERE
						cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomMasterTestCasesByCid( $intCid, $objDatabase ) {
		if( !valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
					    rv_master_test_cases
					WHERE 
						is_published = true
						AND cid =  ' . ( int ) $intCid;

		return self::fetchRvMasterTestCases( $strSql, $objDatabase );
	}

}
?>