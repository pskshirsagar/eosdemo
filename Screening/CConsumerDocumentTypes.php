<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CConsumerDocumentTypes
 * Do not add any new functions to this class.
 */

class CConsumerDocumentTypes extends CBaseConsumerDocumentTypes {

	public static function fetchConsumerDocumentTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CConsumerDocumentType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchConsumerDocumentType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CConsumerDocumentType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>