<?php

class CScreeningWebService extends CBaseScreeningWebService {

	const EXPERIAN_CONNECT_URL		= 1;
	const EXPERIAN_CONNECT_USERNAME	= 2;
	const EXPERIAN_CONNECT_PASSWORD	= 3;
	public static $c_arrintExperianApi = array( CScreeningWebService::EXPERIAN_CONNECT_URL, CScreeningWebService::EXPERIAN_CONNECT_USERNAME, CScreeningWebService::EXPERIAN_CONNECT_PASSWORD );

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>