<?php

class CConsumerDispute extends CBaseConsumerDispute {

	const DISPUTE_NUMBER_PREFIX 				= 250026;

	public function setOldDisputeStatusTypeId( $intOldDisputeStatusTypeId ) {
		$this->m_intOldDisputeStatusTypeId = CStrings::strToIntDef( $intOldDisputeStatusTypeId, NULL, false );

	}

	public function getOldDisputeStatusTypeId() {
		return $this->m_intOldDisputeStatusTypeId;
	}

	public function valDisputeTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getDisputeTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'dispute_type', 'Dispute type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDisputeTypeReasonId() {
		$boolIsValid = true;
		if( true == is_null( $this->getDisputeTypeReasonId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'dispute_type_reason', 'Dispute reason is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDisputeStatusTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getDisputeStatusTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'dispute_status_type', 'Dispute Status Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valProviderReferenceNumber() {
		$boolIsValid = true;
		if( CDisputeStatusType::DISPUTE_STATUS_TYPE_COMPLETE == $this->getDisputeStatusTypeId() && true == is_null( $this->getProviderReferenceNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'provider_reference_number', 'Provider reference number is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDataSourceId() {
		$boolIsValid = true;
		if( CDisputeStatusType::DISPUTE_STATUS_TYPE_COMPLETE == $this->getDisputeStatusTypeId() && true == is_null( $this->getDataSourceId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'data_source_id', 'Provider is required.' ) );
		}

		return $boolIsValid;
	}

	public function valConsumerStatementRequest() {
		$boolIsValid = true;
		if( true == is_null( $this->getConsumerStatementRequest() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'statement', 'Consumer Statement is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valDisputeTypeId();
				$boolIsValid &= $this->valDisputeTypeReasonId();
				$boolIsValid &= $this->valConsumerStatementRequest();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDisputeTypeId();
				$boolIsValid &= $this->valDisputeTypeReasonId();
				$boolIsValid &= $this->valProviderReferenceNumber();
				$boolIsValid &= $this->valDataSourceId();
				$boolIsValid &= $this->valDisputeStatusTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createConsumerDispute( $objStdConsumerDisputeRequest ) {
		$boolIsValid = true;

		$this->setConsumerId( $objStdConsumerDisputeRequest->consumerId );
		$this->setDisputeTypeId( $objStdConsumerDisputeRequest->disputeInfo['dispute_type'] );
		$this->setDisputeTypeReasonId( $objStdConsumerDisputeRequest->disputeInfo['dispute_type_reason'] );
		$this->setDisputeStatusTypeId( CDisputeStatusType::DISPUTE_STATUS_TYPE_NEW );
		$this->setDisputeReferenceNumber( $objStdConsumerDisputeRequest->disputeNumber );
		$this->setAssignedTo( NULL );
		$this->setConsumerStatementRequest( $objStdConsumerDisputeRequest->disputeStatementInfo['statement'] );

		$boolIsValid	&= $this->validate( VALIDATE_INSERT );
		if( false == $boolIsValid ) return false;
		return $boolIsValid;
	}

	public function updateConsumerDispute( $arrmixConsumerInfo, $intUserId ) {
		$boolIsValid = true;

		if( true == array_key_exists( 'dispute_type', $arrmixConsumerInfo ) ) {
			$this->setDisputeTypeId( $arrmixConsumerInfo['dispute_type'] );
		}
		if( true == array_key_exists( 'dispute_type_reason', $arrmixConsumerInfo ) ) {
			$this->setDisputeTypeReasonId( $arrmixConsumerInfo['dispute_type_reason'] );
		}
		if( true == array_key_exists( 'dispute_status_type', $arrmixConsumerInfo ) ) {
			$this->setDisputeStatusTypeId( $arrmixConsumerInfo['dispute_status_type'] );
		}
		if( true == array_key_exists( 'report_id', $arrmixConsumerInfo ) ) {
			$this->setProviderReferenceNumber( $arrmixConsumerInfo['report_id'] );
		}
		if( true == array_key_exists( 'data_source_id', $arrmixConsumerInfo ) ) {
			$this->setDataSourceId( $arrmixConsumerInfo['data_source_id'] );
		}
		if( true == array_key_exists( 'user', $arrmixConsumerInfo ) ) {
			$this->setAssignedTo( $arrmixConsumerInfo['user'] );
		}
		if( true == array_key_exists( 'dispute_status_type', $arrmixConsumerInfo ) && CDisputeStatusType::DISPUTE_STATUS_TYPE_COMPLETE == $arrmixConsumerInfo['dispute_status_type'] ) {
			$this->setCompletedBy( $intUserId );
			$this->setCompletedOn( 'NOW()' );
			$this->setCompletionNotes( $arrmixConsumerInfo['note'] );
		}

		$boolIsValid	&= $this->validate( VALIDATE_UPDATE );

		if( false == $boolIsValid ) return false;

		return $boolIsValid;
	}

}
?>