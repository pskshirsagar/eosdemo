<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConfigPreferenceTypes
 * Do not add any new functions to this class.
 */

class CScreeningConfigPreferenceTypes extends CBaseScreeningConfigPreferenceTypes {

   public static function fetchScreeningConfigPreferenceTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CScreeningConfigPreferenceType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
   }

   public static function fetchScreeningConfigPreferenceType( $strSql, $objDatabase ) {
       	return parent::fetchCachedObject( $strSql, 'CScreeningConfigPreferenceType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
   }

   public static function fetchPublishedAndUsedForConditionsScreeningConfigPreferenceTypesByScreenTypeIds( $arrintScreenTypeIds, $objDatabase ) {
   		if( false == valArr( $arrintScreenTypeIds ) ) return NULL;
   		return self::fetchScreeningConfigPreferenceTypes( sprintf( 'SELECT * FROM screening_config_preference_types WHERE is_published = 1 AND is_use_for_condition = 1 AND screen_type_id IN (%s) ORDER BY sort_order', implode( ', ', $arrintScreenTypeIds ) ), $objDatabase );
   }

   public static function fetchPublishedAndUsedForConditionsScreeningConfigPreferenceTypesByScreenTypeId( $intScreenTypeId, $arrintSkipIds = array(), $objDatabase ) {
		$strWhere = 'WHERE 
						screen_type_id=' . ( int ) $intScreenTypeId . ' 
					    AND is_published = 1 
					    AND is_use_for_condition = 1';
		if( true === valArr( $arrintSkipIds ) ) {
			$strWhere .= ' AND id NOT IN (' . sqlIntImplode( $arrintSkipIds ) . ')';
		}

		$strSql = 'SELECT
	                    id,
	                    NAME,
	                    screening_config_value_type_id,
	                    description 
	                FROM
	                    screening_config_preference_types ' .
		          $strWhere . '
		          ORDER BY sort_order';

		return fetchData( $strSql, $objDatabase );
   }

	public static function fetchScreeningConfigPreferenceTypesByScreenTypeIds( $arrintScreenTypeIds, $objDatabase ) {
		if( false == valArr( $arrintScreenTypeIds ) ) return NULL;
		return self::fetchScreeningConfigPreferenceTypes( sprintf( 'SELECT * FROM screening_config_preference_types WHERE screen_type_id IN (%s)', implode( ', ', $arrintScreenTypeIds ) ), $objDatabase );
	}

    public static function fetchScreeningConfigPreferenceTypesValueTypeIdsByIds( $arrintIds, $objDatabase ) {
        if( false == valArr( $arrintIds ) ) return NULL;

        $strSql = ' SELECT
                        id,
                        screening_config_value_type_id
                    FROM
                        screening_config_preference_types
                    WHERE
                        id IN ( ' . sqlIntImplode( $arrintIds ) . ')';

        return self::fetchScreeningConfigPreferenceTypes( $strSql, $objDatabase );
    }

}
?>