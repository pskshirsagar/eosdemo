<?php

class CScreeningCumulationType extends CBaseScreeningCumulationType {

	const EVALUATE_GUARANTOR_SEPERATELY 			  = 1;
	const COMBINE_GUARANTOR_INCOME 					  = 2;
	const COMBINE_ALL_GUARANTOR_INCOME_WITH_APPLICANT = 3;
	const COMBINE_INCOME 	  						  = 4;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// added default case
        }

        return $boolIsValid;
    }

    public static function getScreeningCumulationTypeName( $intScreeningCumulationTypeId ) {

        $strScreenCumulationTypeName = '';

        switch( $intScreeningCumulationTypeId ) {
            case self::EVALUATE_GUARANTOR_SEPERATELY:
                $strScreenCumulationTypeName = __( 'Evaluate guarantors separately' );
                break;

            default:
                // default case
                break;
        }

        return $strScreenCumulationTypeName;
    }

}
?>