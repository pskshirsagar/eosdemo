<?php

class CScreeningConfiguration extends CBaseScreeningConfiguration {

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valName( $objDatabase, $boolIsSoftDelete ) {
        $boolIsValid = true;

        if( false == $boolIsSoftDelete ) {

        	$intCountScreeningConfigurations = CScreeningConfigurations::fetchCountPublishedCompanyScreeningConfigsByNameByCid( $this->getId(), $this->getName(), $this->getCid(), $objDatabase );
			if( 1 == $intCountScreeningConfigurations ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name already exists.' ) );
				return $boolIsValid;
			}
    	}

		if( true == is_null( $this->getName() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valScreeningConfigTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getScreeningConfigTypeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', 'Type is required.' ) );
        }
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL, $boolIsSoftDelete = 0 ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valName( $objDatabase, $boolIsSoftDelete );
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valScreeningConfigTypeId();
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName( $objDatabase, $boolIsSoftDelete );
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valScreeningConfigTypeId();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// added default case
        }

        return $boolIsValid;
    }

}
?>