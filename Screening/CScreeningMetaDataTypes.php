<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningMetaDataTypes
 * Do not add any new functions to this class.
 */

class CScreeningMetaDataTypes extends CBaseScreeningMetaDataTypes {

    public static function fetchScreeningMetaDataTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CScreeningMetaDataType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchScreeningMetaDataType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScreeningMetaDataType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>