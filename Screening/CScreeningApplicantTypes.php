<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningApplicantTypes
 * Do not add any new functions to this class.
 */

class CScreeningApplicantTypes extends CBaseScreeningApplicantTypes {

    public static function fetchScreeningApplicantTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CScreeningApplicantType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchScreeningApplicantType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CScreeningApplicantType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchAllScreeningApplicantTypes( $objDatabase ) {
	    return self::fetchScreeningApplicantTypes( 'SELECT * FROM screening_applicant_types WHERE is_published = 1 AND id NOT IN ( ' . implode( ',', CScreeningApplicantType::$c_arrintVAScreeningApplicantTypeIds ) . ' ) ', $objDatabase );
    }
}
?>