<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CCriminalClassificationTypes
 * Do not add any new functions to this class.
 */

class CCriminalClassificationTypes extends CBaseCriminalClassificationTypes {

	public static function fetchAllCriminalClassificationTypes( $objDatabase ) {
		return self::fetchCriminalClassificationTypes( 'SELECT * FROM criminal_classification_types WHERE is_published = 1', $objDatabase );
	}

	public static function fetchCustomCriminalClassificationTypes( $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						criminal_classification_types
					WHERE
						is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPublishedCriminalClassificationTypesByExcludingClassificationTypeIds( $objDatabase, $arrintNoSubCategoriesClassificationTypeIds = [] ) {
		if( true == valArr( $arrintNoSubCategoriesClassificationTypeIds ) ) {
			$strSubSql = ' AND id NOT IN ( ' . sqlIntImplode( $arrintNoSubCategoriesClassificationTypeIds ) . ' )';
		}

		$strSql = 'SELECT 
					 * 
   	           FROM 
   	                 criminal_classification_types
               WHERE 
                    is_published = 1' .
                    $strSubSql;

		return self::fetchCriminalClassificationTypes( $strSql, $objDatabase );
	}

}
?>