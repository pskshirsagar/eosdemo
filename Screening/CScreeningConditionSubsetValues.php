<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConditionSubsetValues
 * Do not add any new functions to this class.
 */

class CScreeningConditionSubsetValues extends CBaseScreeningConditionSubsetValues {

	public static function fetchScreeningConditionSubsetValuesByScreeningConditionSubsetIds( $arrintScreeningConditionSubsetIds, $objDatabase ) {
		if( false == valArr( $arrintScreeningConditionSubsetIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						screening_condition_subset_values
					WHERE
						screening_condition_subset_id  IN ( ' . implode( ',', $arrintScreeningConditionSubsetIds ) . ' )
					ORDER BY
						id';

		return parent::fetchScreeningConditionSubsetValues( $strSql, $objDatabase );
	}

	public static function fetchScreeningConditionSubsetValueBySubsetIdByName( $intScreeningConditionSubsetValue, $strScreeningConditionSubsetValueName, $objDatabase ) {
		$strSql = 'SELECT
						spcsv.id,
						spac.id as screening_available_condition_id
					FROM
						screening_condition_subset_values spcsv
						JOIN screening_available_conditions spac ON spac.id = spcsv.screening_available_condition_id
					WHERE
						screening_package_condition_subset_id = ' . ( int ) $intScreeningConditionSubsetValue . '
						AND spac.condition_name LIKE \'' . addslashes( $strScreeningConditionSubsetValueName ) . '\'';

		return fetchData( $strSql, $objDatabase );
	}

}
?>