<?php

class CRvLeaseApplicantSecureNewDetail extends CBaseRvLeaseApplicantSecureNewDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSeqId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCollectionCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCollectionCountLastOneMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCollectionCountLastOneYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCollectionCountLastTwoYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRentalCollectionCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRentalCollectionCountLastOneMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRentalCollectionCountLastOneYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRentalCollectionCountLastTwoYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMedicalCollectionCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMedicalCollectionCountLastOneYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMedicalCollectionCountLastTwoYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityCollectionCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityCollectionCountLastOneYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityCollectionCountLastTwoYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStudentCollectionCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStudentCollectionCountLastOneYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStudentCollectionCountLastTwoYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInquiryCountLastNintyDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInquiryCountLastOneEightyDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInquiryCountLastOneYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInquiryCountLastTwoYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantIncome() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCollectionAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRentalCollectionAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMedicalCollectionAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityCollectionAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStudentCollectionAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDebtToIncome() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDelinquentAccountPercentage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalMonthlyDebt() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonthlyDebtAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCreditScore() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRvIndex() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHadBankruptcy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHadForeclosure() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningApplicantTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setCustomCreditScore( $strCreditScore, $objDatabase ) {
		$strSql = 'SELECT encode ( \'' . $strCreditScore . '\', \'base64\' )';
		$this->set( 'm_strCreditScore', fetchData( $strSql, $objDatabase )['0']['encode'] );
	}

}
?>