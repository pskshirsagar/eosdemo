<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningListTypes
 * Do not add any new functions to this class.
 */

class CScreeningListTypes extends CBaseScreeningListTypes {

	public static function fetchScreeningListTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningListType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningListType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningListType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>