<?php

class CScreeningApplicantChildTransaction extends CBaseScreeningApplicantChildTransaction {

	protected $m_intSearchCost;

	protected $m_strScreenTypeName;

	public function setSearchCost( $intSearchCost ) {
		$this->m_intSearchCost = $intSearchCost;
	}

	public function setScreenTypeName( $strScreenTypeName ) {
		$this->m_strScreenTypeName = $strScreenTypeName;
	}

	public function getScreenTypeName() {
		return $this->m_strScreenTypeName;
	}

	public function getSearchCost() {
		return $this->m_intSearchCost;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrmixValues['screen_type_name'] ) ) $this->setScreenTypeName( $arrmixValues['screen_type_name'] );

		return;
	}

}
?>