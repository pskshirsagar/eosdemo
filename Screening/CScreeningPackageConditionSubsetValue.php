<?php

class CScreeningPackageConditionSubsetValue extends CBaseScreeningPackageConditionSubsetValue {

	protected $m_intScreeningPackageConditionSetId;

	public function getScreeningPackageConditionSetId() {
		return $this->m_intScreeningPackageConditionSetId;
	}

	public function setScreeningPackageConditionSetId( $intScreeningPackageConditionSetId ) {
		$this->m_intScreeningPackageConditionSetId = ( int ) $intScreeningPackageConditionSetId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['screening_package_condition_set_id'] ) ) $this->setScreeningPackageConditionSetId( $arrmixValues['screening_package_condition_set_id'] );
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>