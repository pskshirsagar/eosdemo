<?php

class CConsumerDoc extends CBaseConsumerDoc {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConsumerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConsumerDisputeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConsumerDocumentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDocumentPath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function encryptUploadedFile( $objDocumentManager, $strFolderName = PATH_MOUNTS_CONSUMER_DOCS, $intCid, $strTempFileName, $boolEncrypt = true ) {

		$strUploadPath = CDocumentManagerUtils::getTemporaryPath( PATH_MOUNTS_CONSUMER_DOCS, $intCid );

		if( false == is_dir( $strUploadPath ) ) {
			if( false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Failed to create directory path.' ) );
				return false;
			}
		}

		if( false == move_uploaded_file( $strTempFileName, $strUploadPath . $this->getDescription() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' No file was uploaded.' ) );
			return false;
		}

		$strFileContent = file_get_contents( $strUploadPath . $this->getDescription() );

		if( false == $objDocumentManager->putDocument( CDocumentCriteria::forPut( -1, $strFolderName, $this->getDocumentPath() . $this->getDescription(), $strFileContent, $boolEncrypt ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to encrypt document' ) );
			return false;
		}

		return true;
	}

	public function encryptUploadedObject( $objObjectStorageGateway, $strFolderName = PATH_MOUNTS_CONSUMER_DOCS, $intCid, $strTempFileName, $boolEncrypt = true ) {

		$strUploadPath = $objObjectStorageGateway->calcTemporaryPath( PATH_MOUNTS_CONSUMER_DOCS, $intCid );

		if( false == is_dir( $strUploadPath ) ) {
			if( false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Failed to create directory path.' ) );
				return false;
			}
		}

		if( false == move_uploaded_file( $strTempFileName, $strUploadPath . $this->getDescription() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' No file was uploaded.' ) );
			return false;
		}

		$strFileContent = file_get_contents( $strUploadPath . $this->getDescription() );
		CFileIo::deleteFile( $strUploadPath . $this->getDescription() );

		$objObjectStorageGatewayResponse = $objObjectStorageGateway->putObject( [
			'cid'       => $intCid,
			'objectId'  => -1,
			'container' => $strFolderName,
			'key'       => $this->getDocumentPath() . $this->getDescription(),
			'data'      => $strFileContent,
			'noEncrypt' => !$boolEncrypt
		] );

		if( true == $objObjectStorageGatewayResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to encrypt document' ) );
			return false;
		}

		return true;
	}

	public function deleteFile( $strFolderPath = NULL ) {
		if( false == is_null( $this->getDocumentPath() ) && false == is_null( $this->getDescription() ) ) {

			$arrstrFileExtensionsToDelete = array( '.tif', '.png', '.doc', '.docx', '.jpg', '.jpeg', '.bmp', '.pdf','BMP', 'GIF', 'JPE', 'ICO', 'TXT', 'TIFF', 'PSD', 'PDF' );

			$arrstrFileNameExtension = explode( '.', $this->getDescription() );

			foreach( $arrstrFileExtensionsToDelete as $strFileExtension ) {
				if( file_exists( $this->getDocumentPath() . $this->getDescription() . $arrstrFileNameExtension[0] . $strFileExtension ) ) {
					@unlink( $this->getFullFilePath( $strFolderPath ) . $arrstrFileNameExtension[0] . $strFileExtension );
				}
			}
		}
	}

	public function createConsumerDoc( $objStdConsumerDisputeRequest ) {

		$boolIsValid 		= true;
		$strPath 			= 'proof/' . date( 'Y', time() ) . '/' . date( 'm', time() ) . '/' . date( 'd', time() ) . '/';
		$this->setConsumerId( $objStdConsumerDisputeRequest->consumerId );
		$this->setConsumerDisputeId( $objStdConsumerDisputeRequest->consumerDisputeId );
		$this->setConsumerDocumentTypeId( CConsumerDocumentType::CONSUMER_DOCUMENT_PROOF );
		$this->setDocumentPath( $strPath );
		$this->setDescription( $objStdConsumerDisputeRequest->fileName );
		$this->setIsPublished( true );
		$this->setCreatedOn( 'NOW()' );

		$boolIsValid	&= $this->validate( VALIDATE_INSERT );
		if( false == $boolIsValid ) return false;
		return $boolIsValid;

	}

}
?>