<?php

class CResidentDelinquentEvent extends CBaseResidentDelinquentEvent {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNameFirst() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNameLast() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNameMiddle() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmailAddress() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTaxNumberLastFour() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTaxNumberEncrypted() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBirthDateYear() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBirthDateEncrypted() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDriverLicense() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDriverLicenseState() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAddressLine() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCity() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valState() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valZipcode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeliquentEventTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeliquentEventStatusTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeliquentEventDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDelinquentAmount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDelinquentAmountOriginal() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDelinquentEventNotes() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLastPaidOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>