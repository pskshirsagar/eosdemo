<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningSearchCriteriaTypes
 * Do not add any new functions to this class.
 */

class CScreeningSearchCriteriaTypes extends CBaseScreeningSearchCriteriaTypes {

	public static function fetchScreeningSearchCriteriaTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningSearchCriteriaType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningSearchCriteriaType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningSearchCriteriaType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllScreeningSearchCriteriaTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM screening_search_criteria_types WHERE is_published = true';
		return self::fetchScreeningSearchCriteriaTypes( $strSql, $objDatabase );
	}

}
?>