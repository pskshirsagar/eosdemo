<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CRvScreeningLeaseDetails
 * Do not add any new functions to this class.
 */

class CRvScreeningLeaseDetails extends CBaseRvScreeningLeaseDetails {

    public static function fetchNonTransferedRvScreeningLeaseDetails( $objDatabase ) {

        $strSql = 'SELECT
						*
					FROM
						rv_screening_lease_details rsld
					WHERE
						rsld.is_transfered = false';

        return parent::fetchRvScreeningLeaseDetails( $strSql, $objDatabase );
    }

}
?>