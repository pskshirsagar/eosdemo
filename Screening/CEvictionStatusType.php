<?php

class CEvictionStatusType extends CBaseEvictionStatusType {

	const EVICTION_STATUS_FILED		 			= 1;
	const EVICTION_STATUS_DISMISSED  			= 2;
	const EVICTION_STATUS_JUDGEMENT  			= 3;
	const EVICTION_STATUS_JUDGEMENT_ZERO_DOLLAR = 4;
	const EVICTION_STATUS_OTHER		 			= 5;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>