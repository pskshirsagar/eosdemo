<?php

class CReconciliationStatusType extends CBaseReconciliationStatusType {

    const UNPROCESSED     	= 1;
    const DISPUTED 			= 2;
    const APPROVED 			= 3;
    const PAID 				= 4;

    public static $c_arrintRestrictDeleteScreeningInvoiceStatusIds = array( self::APPROVED, self::PAID );

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getReconciliationStatusTypeName( $intReconciliationStatusTypeId ) {
        $strReconciliationTypeName = '';

        switch( $intReconciliationStatusTypeId ) {
            case self::UNPROCESSED:
                $strReconciliationTypeName = __( 'Unprocessed' );
                break;

            case self::DISPUTED:
                $strReconciliationTypeName = __( 'Disputed' );
                break;

            case self::APPROVED:
                $strReconciliationTypeName = __( 'Approved' );
                break;

            case self::PAID:
                $strReconciliationTypeName = __( 'Paid' );
                break;

            default:
                // default case
                break;
        }

        return $strReconciliationTypeName;
    }

}
?>