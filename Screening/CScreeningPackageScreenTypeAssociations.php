<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageScreenTypeAssociations
 * Do not add any new functions to this class.
 */

class CScreeningPackageScreenTypeAssociations extends CBaseScreeningPackageScreenTypeAssociations {

	public static function fetchPublishedScreeningPackageAssociatedScreenTypesByScreeningPackageId( $intScreeningPackageId, $objDatabase, $boolIsPreScreeningEnabled = false, $intPreScreeningSetId = 0 ) {
		$strSql = 'SELECT
						*
				   FROM
						screening_package_screen_type_associations
				   WHERE
						screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND is_published = 1';

		if( true == $boolIsPreScreeningEnabled ) $strSql .= ' AND prescreening_set_id = ' . ( int ) $intPreScreeningSetId;

		return parent::fetchScreeningPackageScreenTypeAssociations( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageAssociatedScreenTypesByScreeningPackageIdByPreScreeningSetId( $intScreeningPackageId, $objDatabase, $intPreScreeningSetId = 0 ) {
		$strSql = 'SELECT
						*
				   FROM
						screening_package_screen_type_associations
				   WHERE
						screening_package_id = ' . ( int ) $intScreeningPackageId .
		            ' AND prescreening_set_id = ' . ( int ) $intPreScreeningSetId;

		return parent::fetchScreeningPackageScreenTypeAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPackageScreenTypeAssociationsByScreeningDataProviderId( $intScreeningDataProviderId, $objDatabase ) {
		$strSql = 'SELECT
						spsta.*, sdp.screening_data_provider_type_id
					FROM
						screening_package_screen_type_associations
					WHERE
						screening_data_provider_id = ' . ( int ) $intScreeningDataProviderId . '
						AND is_published = 1';

		return parent::fetchScreeningPackageScreenTypeAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPackageAssociatedScreenTypesByScreeningPackageIds( $arrintScreeningPackageIds, $objDatabase ) {

		if( false == valArr( $arrintScreeningPackageIds ) ) return NULL;

		$strSql = 'SELECT
						spsta.*, sdp.screening_data_provider_type_id, sp.screening_package_type_id, sdp.name as screening_data_provider_name
				   FROM
						screening_package_screen_type_associations spsta
						JOIN screening_packages sp ON sp.id = spsta.screening_package_id
				   		LEFT JOIN screening_data_providers sdp on spsta.screening_data_provider_id = sdp.id
				   WHERE
						spsta.screening_package_id IN ( ' . implode( ',', $arrintScreeningPackageIds ) . ' )
						AND spsta.is_published = 1
						ORDER BY spsta.screen_type_id';

		return parent::fetchScreeningPackageScreenTypeAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPackageAssociatedScreenTypesWithScreenNameByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {
		$strSql = '	SELECT
						spsta.*,
						st.name as screen_name
					FROM
						screening_package_screen_type_associations spsta
						INNER JOIN screen_types st ON ( st.id = spsta.screen_type_id )
					WHERE
						screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND spsta.is_published = 1
					ORDER BY
						st.id';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRemainingTransactionsCountByScreeningIdByScreeningApplicantIdByScreeningPackageIdByCid( $intScreeningId, $intScreeningApplicantId, $intScreeningPackageId, $intCid, $objDatabase ) {

        if( false == valId( $intScreeningId ) || false == valId( $intScreeningApplicantId ) || false == valId( $intCid ) || false == valId( $intScreeningPackageId ) ) return false;

        $strSql = '	Select (	(
					 				Select count(id) as app_screen_types
									from screening_package_screen_type_associations
									where screening_package_id = ' . ( int ) $intScreeningPackageId . '
									AND screen_type_id NOT IN( ' . sqlIntImplode( array_merge( CScreenType::$c_arrintManualCriminalSearchScreenTypeIds, CScreenType::$c_arrintPreciseIdChildScreenTypeIds ) ) . ' )
								)
								-
								(
								  Select count(id) as ttl_transactions
								  from screening_transactions
								  where
								        screening_id = ' . ( int ) $intScreeningId . ' 
                                        AND screening_applicant_id = ' . ( int ) $intScreeningApplicantId . ' 
                                        AND cid = ' . ( int ) $intCid . '
                                        AND 
                                            CASE 
                                                WHEN screening_id = ' . ( int ) $intScreeningId . ' AND screening_applicant_id = ' . ( int ) $intScreeningApplicantId . ' AND cid = ' . ( int ) $intCid . ' AND is_skipped = true 
                                                    THEN
                                                        screen_type_id NOT IN( ' . sqlIntImplode( array_merge( CScreenType::$c_arrintManualCriminalSearchScreenTypeIds, CScreenType::$c_arrintPreciseIdChildScreenTypeIds ) ) . ' )
                                                    ELSE 
                                                        screening_status_type_id NOT IN( ' . implode( ',', array( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED, CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED ) ) . ' )
                                                        AND screen_type_id NOT IN( ' . sqlIntImplode( array_merge( CScreenType::$c_arrintManualCriminalSearchScreenTypeIds, CScreenType::$c_arrintPreciseIdChildScreenTypeIds ) ) . ' )
                                            END
								)
					) as diff';

		$arrstrData 					= fetchData( $strSql, $objDatabase );
		$intRemainingTransactionsCount 	= ( int ) ( true == valArr( $arrstrData ) ) ? $arrstrData[0][diff] : '-1';

		return $intRemainingTransactionsCount;
	}

	public static function fetchNextPreScreeningSetIdByScreeningPackageIdByScreeningApplicantIdByScreeningIdByCid( $intScreeningPackageId, $intApplicantId, $intScreeningId, $intCid, $objDatabase ) {

		$strSql = '	SELECT prescreening_set_id
					FROM screening_package_screen_type_associations
					WHERE screening_package_id = ' . ( int ) $intScreeningPackageId . ' AND
					  	  ( screen_type_id NOT IN (
													SELECT
														screen_type_id
													FROM
														screening_transactions
													WHERE
													cid = ' . ( int ) $intCid . ' AND
													CASE 
                                                        WHEN screening_id = ' . ( int ) $intScreeningId . ' AND screening_applicant_id = ' . ( int ) $intApplicantId . 'AND cid = ' . ( int ) $intCid . ' ( is_skipped = true OR screening_order_id = \'PRECISE_ID\')
                                                        THEN
                                                            screening_applicant_id = ' . ( int ) $intApplicantId . '
                                                            AND screening_id = ' . ( int ) $intScreeningId . '
                                                            AND cid = ' . ( int ) $intCid . '
                                                        ELSE 
                                                            screening_applicant_id = ' . ( int ) $intApplicantId . '
                                                            AND screening_id = ' . ( int ) $intScreeningId . '
                                                            AND cid = ' . ( int ) $intCid . '
															AND screening_completed_on is NOT NULL
															AND screening_status_type_id NOT IN( ' . implode( ',', array( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED, CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED ) ) . ' )
														END
												) AND screen_type_id NOT IN( ' . sqlIntImplode( array_merge( CScreenType::$c_arrintManualCriminalSearchScreenTypeIds, CScreenType::$c_arrintPreciseIdChildScreenTypeIds ) ) . ' ) )
					ORDER BY prescreening_set_id ASC
					LIMIT 1';

		$arrstrData 				= fetchData( $strSql, $objDatabase );
		$intNextPreScreeningSetId 	= ( int ) ( true == valArr( $arrstrData ) && true == array_key_exists( 'prescreening_set_id', $arrstrData[0] ) ) ? $arrstrData[0][prescreening_set_id] : 1;

		return $intNextPreScreeningSetId;
	}

	public static function fetchScreeningPackageScreenTypeAssociationByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {
		return self::fetchScreeningPackageScreenTypeAssociation( sprintf( 'SELECT * FROM screening_package_screen_type_associations WHERE screening_package_id = %d', ( int ) $intScreeningPackageId ), $objDatabase );
	}

	public static function fetchPublishedScreeningPackageScreenTypeAssociationsByScreeningDataProviderIdByScreeningPackageId( $intScreeningDataProviderId, $intScreeningPackageId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_package_screen_type_associations
					WHERE
						screening_data_provider_id = ' . ( int ) $intScreeningDataProviderId . '
						AND screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND is_published = 1';

		return parent::fetchScreeningPackageScreenTypeAssociation( $strSql, $objDatabase );
	}

	public static function fetchCountScreeningPackageScreenTypeAssociationByScreenTypeIdsByCid( $arrintScreenTypeIds, $intCid, $objDatabase ) {

		$strAdditionalWhereClause = ( true == valArr( $arrintScreenTypeIds ) ) ? ' AND  screen_type_id NOT IN( ' . implode( ',', array_keys( $arrintScreenTypeIds ) ) . ' )' : '';

		$strSql = 'WHERE
						screening_package_id IN( SELECT id FROM screening_packages WHERE is_published = 1 AND screening_package_type_id NOT IN (' . implode( ',', CScreeningPackageType::$c_arrintVAScreeningPackageTypes ) . ') AND cid = ' . ( int ) $intCid . ' )' . $strAdditionalWhereClause;

		return self::fetchScreeningPackageScreenTypeAssociationCount( $strSql, $objDatabase );
	}

	public static function fetchCountScreeningPackageScreenTypeAssociationByScreenTypeIdByScreeningPackageId( $intScreenTypeId, $intScreeningPackageId, $objDatabase ) {
		$strSql = 'WHERE
						screening_package_id  = ' . ( int ) $intScreeningPackageId . '
						AND screen_type_id = ' . ( int ) $intScreenTypeId;

		return self::fetchScreeningPackageScreenTypeAssociationCount( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeAssociationByIdByScreeningPackageId( $intScreeningPackageScreenTypeAssociationId, $intScreeningPackageId, $objDatabase ) {
		$strSql = 'SELECT * FROM screening_package_screen_type_associations WHERE id = ' . ( int ) $intScreeningPackageScreenTypeAssociationId . ' AND screening_package_id = ' . ( int ) $intScreeningPackageId;
		return self::fetchScreeningPackageScreenTypeAssociation( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageScreenTypeAssociationsByScreenTypeIdsByScreeningPackageId( $arrintScreenTypeIds, $intScreeningPackageId, $objDatabase ) {
		$strSql = 'SELECT 
						*
					FROM
						screening_package_screen_type_associations
					WHERE
						screen_type_id IN( ' . implode( ',', $arrintScreenTypeIds ) . ' )
						AND screening_package_id = ' . ( int ) $intScreeningPackageId;

		return parent::fetchScreeningPackageScreenTypeAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPackageAssociatedScreenTypesByScreeningPackageIdByScreenTypeId( $intScreeningPackageId, $intScreenTypeId, $objDatabase, $boolIsPreScreeningEnabled = false, $intPreScreeningSetId = 0 ) {
		$strSql = 'SELECT
						*
				   FROM
						screening_package_screen_type_associations
				   WHERE
						screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND screen_type_id = ' . ( int ) $intScreenTypeId . '
						AND is_published = 1';

		if( true == $boolIsPreScreeningEnabled ) $strSql .= ' AND prescreening_set_id = ' . ( int ) $intPreScreeningSetId;

		return parent::fetchScreeningPackageScreenTypeAssociation( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningPackageScreenTypeAssociationByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_package_screen_type_associations
					WHERE
						screening_criteria_id = ' . ( int ) $intScreeningCriteriaId;
		$arrmixResult = executeSql( $strSql, $objDatabase );

		return ( false == getArrayElementByKey( 'failed', $arrmixResult ) ) ? getArrayElementByKey( 'data', $arrmixResult ) : [];
	}

	public static function fetchScreeningPackageScreenTypeAssociationsByScreeningPackageIds( $arrintScreeningPackageId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_package_screen_type_associations
					WHERE
					    is_published = 1
						AND screening_package_id IN( ' . sqlIntImplode( $arrintScreeningPackageId ) . ' )';

		return parent::fetchScreeningPackageScreenTypeAssociations( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningCriteriaIdByScreenTypeIdByScreeningPackageId( $intScreenTypeId, $intScreeningPackageId, $objDatabase ) {
		$strSql = 'SELECT
						screening_criteria_id
					FROM
						screening_package_screen_type_associations
					WHERE						
						screen_type_id = ' . ( int ) $intScreenTypeId . '
						AND screening_package_id = ' . ( int ) $intScreeningPackageId;

		return self::fetchColumn( $strSql, 'screening_criteria_id', $objDatabase );
	}

	public static function fetchActiveScreeningPackageScreenTypeAssociationByScreeningPackageIdByScreeningCriteriaId( $intScreeningPackageId, $intScreeningCriteriaId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_package_screen_type_associations
					WHERE
						screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND screening_criteria_id = ' . ( int ) $intScreeningCriteriaId;

		$arrmixResult = executeSql( $strSql, $objDatabase );

		return ( false == getArrayElementByKey( 'failed', $arrmixResult ) ) ? getArrayElementByKey( 'data', $arrmixResult ) : [];
	}

	public static function fetchPublishedScreeningPackageScreenTypeAssociationsCountByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {
		$strSql = 'SELECT
               COUNT ( spsta.id ) AS package_count
            FROM
               screening_package_screen_type_associations spsta
                JOIN screening_packages sp ON spsta.screening_package_id = sp.id
            WHERE
               spsta.is_published = 1
                 AND sp.is_published = 1
               AND spsta.screening_criteria_id = ' . ( int ) $intScreeningCriteriaId;

		$arrmixResult = executeSql( $strSql, $objDatabase );
		$arrmixResult = ( false == getArrayElementByKey( 'failed', $arrmixResult ) ) ? getArrayElementByKey( 'data', $arrmixResult ) : [];
		return $arrmixResult[0]['package_count'];
	}

}
?>