<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningStatusTypes
 * Do not add any new functions to this class.
 */

class CScreeningStatusTypes extends CBaseScreeningStatusTypes {

	public static function fetchScreeningStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CScreeningStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

    public static function fetchScreeningStatusType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CScreeningStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchActiveScreeningStatusTypes( $objDatabase ) {

    	$strSql = 'SELECT
    					*
    			   FROM
    					screening_status_types
    			   WHERE
    					is_published = 1';

    	return self::fetchScreeningStatusTypes( $strSql, $objDatabase );
    }

}
?>