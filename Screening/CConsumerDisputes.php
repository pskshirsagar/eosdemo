<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CConsumerDisputes
 * Do not add any new functions to this class.
 */

class CConsumerDisputes extends CBaseConsumerDisputes {

	public static function fetchConsumerDisputeByConsumerId( $intConsumerId, $objDatabase ) {
		$strSql = 'SELECT * FROM consumer_disputes WHERE consumer_id = ' . ( int ) $intConsumerId . ' ORDER BY id LIMIT 1';

		return self::fetchConsumerDispute( $strSql, $objDatabase );
	}

	public static function fetchConsumerDisputeByDisputeNumber( $strDisputeNumber, $objDatabase ) {
		$strSql = 'SELECT * FROM consumer_disputes WHERE dispute_reference_number=' . $strDisputeNumber;

		return self::fetchConsumerDispute( $strSql, $objDatabase );
	}

	public static function fetchConsumerDisputeByReferenceNumberBySsn( $mixRefrenceNumber, $intSSN, $objDatabase ) {

		$strSql = 'SELECT
						cd.dispute_status_type_id,
						cd.dispute_reference_number
				   	FROM
						consumer_disputes cd
				   		JOIN consumers c ON c.id = cd.consumer_id
				   	WHERE
						cd.dispute_reference_number =' . ( int ) $mixRefrenceNumber . '
				   		AND c.last_four_ssn = \'' . $intSSN . '\' ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedConsumerDisputeDetailsByDisputeSearchFilterByDisputeStatusTypeId( $objStdDisputeRecordRequest, $objDatabase ) {

		$intOffset	= ( 0 < $objStdDisputeRecordRequest->pageNo ) ? $objStdDisputeRecordRequest->pageSize * ( $objStdDisputeRecordRequest->pageNo - 1 ) : 0;
		$intLimit	= ( int ) $objStdDisputeRecordRequest->pageSize;

		$arrstrCondition = self::buildWhereCondition( $objStdDisputeRecordRequest );

		$strSql = 'SELECT
					    *
					FROM
					    (
					      SELECT
								DISTINCT ON (' . $arrstrCondition['distinctId'] . ')' . $arrstrCondition['distinctId'] . ' consumer_dispute_id,
								cd.dispute_type_id,
					            c.first_name || \' \' || c.last_name AS consumer_name,
								cd.dispute_status_type_id,
					            cd.completed_by,
								cd.completed_on,
					           	cd.dispute_reference_number,
					            c.screening_id,
								c.screening_applicant_id,
					            c.property_name,
								cd.consumer_id,
								c.reference_number,
								c.email_address,
								cd.assigned_to,
								cd.updated_on ' . $arrstrCondition['from'] . '
							FROM
							    consumer_disputes cd
								JOIN consumers c ON c.id = cd.consumer_id
								' . $arrstrCondition['join'] . '
							WHERE' .
								  $arrstrCondition['where'] . '
						) cd_subquery ' . $arrstrCondition['where_clause'] . '

					ORDER BY
						' . $arrstrCondition['order'] . '
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchConsumerDisputeDetailsCountByDisputeSearchFilter( $objStdDisputeRecordRequest, $objDatabase ) {

		$arrstrCondition = self::buildWhereCondition( $objStdDisputeRecordRequest );

		$strSql = 'SELECT
					    COUNT( DISTINCT ' . $arrstrCondition['distinctId'] . ' )
					FROM
					   consumer_disputes cd
					   JOIN consumers c ON c.id = cd.consumer_id
						' . $arrstrCondition['join'] . '
					WHERE' .
						  $arrstrCondition['where'];

		$arrintConsumerDisputesCount = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintConsumerDisputesCount ) ) ? $arrintConsumerDisputesCount[0]['count'] : 0;
	}

	public static function fetchCustomConsumerDisputeDetailsByFollowupActionTypeId( $intFollowupActionTypeId, $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    (
					      SELECT
					          DISTINCT ON ( cd.id ) cd.id consumer_dispute_id,
					          cd.dispute_type_id,
					          c.first_name ||  \' \' || c.last_name AS consumer_name,
					          cd.dispute_status_type_id,
					          cd.completed_by,
					          cd.completed_on,
					          cd.dispute_reference_number,
					          c.screening_id,
					          c.screening_applicant_id,
					          c.property_name,
					          cd.consumer_id,
					          c.reference_number,
					          c.email_address,
					          cd.created_on,
					          cd.dispute_status_type_id,
							  dfr.action_days,
							  CASE
                                    WHEN  to_char(cd.created_on, \'HH24:MI:SS\') <= \'17:00:00\'  THEN date_part(\'day\', age(CURRENT_DATE::date, cd.created_on::date) ) 
                                    WHEN  to_char(cd.created_on, \'HH24:MI:SS\') > \'17:00:00\' THEN date_part(\'day\', age(CURRENT_DATE::date, ( cd.created_on::date + 1 ) ) ) 
							END AS date_part
					      FROM
					          consumer_disputes cd
					          JOIN consumers c ON c.id = cd.consumer_id
					          JOIN dispute_followup_rules dfr ON dfr.dispute_status_type_id = cd.dispute_status_type_id AND dfr.followup_action_type_id = ' . ( int ) $intFollowupActionTypeId . '
					      WHERE
					           CASE
                                    WHEN  date( cd.created_on) < date( CURRENT_DATE - dfr.action_days ) THEN date( cd.created_on) < date( CURRENT_DATE - dfr.action_days )
                                    WHEN  date( cd.created_on) = date( CURRENT_DATE - dfr.action_days ) THEN to_char(cd.created_on, \'HH24:MI:SS\') <= \'17:00:00\' 
                                END    
					            AND cd.dispute_status_type_id NOT IN(' . implode( ', ', CDisputeStatusType::$c_arrintNotIncludeDisputeStatusTypeIds ) . ') 
					            AND cd.dispute_type_id<>' . CDisputeType::OTHER . '
					    ) cd_subquery
					ORDER BY
					    cd_subquery.consumer_dispute_id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public function buildWhereCondition( $objStdDisputeRecordRequest ) {
		$strOrder = 'cd_subquery.updated_on DESC ';
		$strDistinctId 	= 'cd.id';
		if( CDisputeSubMenuType::MY_RECENT_DISPUTE != $objStdDisputeRecordRequest->selectedSubTabId ) {
			$strWhere 		= ' cd.dispute_status_type_id =' . $objStdDisputeRecordRequest->disputeStatusTypeId . ' AND';
		}

		$strFrom = '';
		$strWhereClause = '';

		if( true == valStr( $objStdDisputeRecordRequest->searchData ) ) {
			$strWhere = ' ((c.first_name  ILIKE \'%' . $objStdDisputeRecordRequest->searchData . '%\') OR ( c.last_name  ILIKE \'%' . $objStdDisputeRecordRequest->searchData . '%\') OR ( c.screening_id=' . ( int ) $objStdDisputeRecordRequest->searchData . ' ) OR (cd.dispute_reference_number=' . ( int ) $objStdDisputeRecordRequest->searchData . ') ) AND';
		}

		if( CDisputeSubMenuType::FOLLOWUP_HIGHLIGHT == $objStdDisputeRecordRequest->selectedSubTabId ) {
			$strWhere 	.= ' CASE
                                    WHEN  date( cd.created_on) = date( CURRENT_DATE - dfr.action_days ) 
                                    THEN  date( cd.created_on) = date( CURRENT_DATE -dfr.action_days )
                                          AND to_char(cd.created_on, \'HH24:MI:SS\') <= \'17:00:00\' 
                                    END AND ';
			$strJoin 	.= ' JOIN dispute_followup_rules dfr ON dfr.dispute_status_type_id = cd.dispute_status_type_id AND dfr.followup_action_type_id =' . CFollowupActionType::DISPUTE_HIGHLIGHT;
		}

		if( CDisputeSubMenuType::FOLLOWUP_IMMEDIATE == $objStdDisputeRecordRequest->selectedSubTabId ) {
			$strWhere 	.= ' CASE
                                WHEN  date( cd.created_on) < date( CURRENT_DATE - dfr.action_days ) THEN date( cd.created_on) < date( CURRENT_DATE -dfr.action_days )
                                WHEN  date( cd.created_on) = date( CURRENT_DATE - dfr.action_days ) THEN to_char(cd.created_on, \'HH24:MI:SS\') <= \'17:00:00\' 
                            END AND ';
			$strJoin 	.= ' JOIN dispute_followup_rules dfr ON dfr.dispute_status_type_id = cd.dispute_status_type_id AND dfr.followup_action_type_id =' . CFollowupActionType::DISPUTE_EMAIL;
		}

		if( CDisputeSubMenuType::REQUEST_INFO == $objStdDisputeRecordRequest->selectedSubTabId ) {
			$strWhere .= ' cd.dispute_type_id=' . CDisputeType::OTHER;
		} elseif( true == in_array( $objStdDisputeRecordRequest->selectedSubTabId, array( CDisputeSubMenuType::MY_RECENT_DISPUTE, CDisputeSubMenuType::MY_DISPUTE, CDisputeSubMenuType::RECEIVED_MAILS ) ) ) {
			$strWhere .= ' cd.dispute_type_id IN(' . implode( ', ', CDisputeType::$c_arrintIncludeDisputeTypeIds ) . ')';
		} else {
			$strWhere .= ' cd.dispute_type_id<>' . CDisputeType::OTHER;
		}

		if( true == is_numeric( $objStdDisputeRecordRequest->selectedUserId ) ) {
			$strDistinctId 	= 'dl.consumer_id';
			$strWhere 		.= ' AND dl.assigned_to =' . $objStdDisputeRecordRequest->selectedUserId;
			$strJoin 		= ' LEFT JOIN dispute_logs dl ON dl.consumer_id = cd.consumer_id';
		}

		if( CDisputeSubMenuType::RECEIVED_MAILS == $objStdDisputeRecordRequest->selectedSubTabId ) {
			$strDistinctId 	= 'dl.consumer_id';
			$strWhere 		.= ' AND cd.dispute_status_type_id IN(' . implode( ', ', CDisputeStatusType::$c_arrintIncludeDisputeStatusTypeIdsForRecievedMails ) . ') AND dl.log_type_id =' . ClogType::OUTBOUND_NOTE . '
							 AND dl.created_on >=   ( ( CURRENT_DATE) - INTERVAL \'30 days\' ) ';
			$strJoin 		= ' LEFT JOIN dispute_logs dl ON dl.consumer_id = cd.consumer_id';
		}
		if( CDisputeSubMenuType::MY_DISPUTE == $objStdDisputeRecordRequest->selectedSubTabId ) {
			$strWhere 		.= ' AND cd.assigned_to =' . $objStdDisputeRecordRequest->userId;

		}

		if( CDisputeSubMenuType::MY_RECENT_DISPUTE == $objStdDisputeRecordRequest->selectedSubTabId ) {
			$strWhere 	.= ' AND dl.updated_by = ' . $objStdDisputeRecordRequest->userId;

			switch( $objStdDisputeRecordRequest->selectedRecentDisputeSubTab ) {
				case 'today':
					$strWhere .= ' AND DATE ( dl.created_on ) >= CURRENT_DATE';
					break;

				case 'yesterday':
					$strWhere .= ' AND dl.created_on > TIMESTAMP \'yesterday\' AND DATE(dl.created_on) < CURRENT_DATE
								   AND dl.consumer_id NOT IN (
                                                   SELECT
                                                        dl.consumer_id
                                                   FROM
                                                       dispute_logs dl
                                                   WHERE
                                                       DATE ( dl.created_on ) = CURRENT_DATE )';
					break;

				case 'month':
					$strWhere .= ' AND dl.created_on BETWEEN ( ( CURRENT_DATE - 1 ) - INTERVAL \'30 days\' )
                            		AND ( CURRENT_DATE - 1 )
									AND dl.consumer_id NOT IN (
                                                 SELECT
                                                        dl.consumer_id
                                                   FROM
                                                       dispute_logs dl
                                                   WHERE
                                                       DATE ( dl.created_on ) = CURRENT_DATE OR dl.created_on > TIMESTAMP \'yesterday\' AND DATE ( dl.created_on ) < CURRENT_DATE )';
					break;

				default:
			}

			$strDistinctId 	= 'dl.consumer_id';

			$strJoin 	= ' LEFT JOIN dispute_logs dl ON dl.consumer_id = cd.consumer_id';
			$strFrom 	= ' , dl.created_on AS created_on, dl.id';
			$strWhereClause 	= '';
			$strOrder = ' cd_subquery.created_on DESC ';
		}

		$arrstrCondition = array( 'distinctId' => $strDistinctId, 'where' => $strWhere, 'join' => $strJoin, 'order' => $strOrder, 'from' => $strFrom, 'where_clause' => $strWhereClause );

		return $arrstrCondition;
	}

	public static function fetchConsumerDisputeFollowupCount( $intDisputeSubMenuTypeId, $objDatabase ) {
		if( CFollowupActionType::DISPUTE_HIGHLIGHT == $intDisputeSubMenuTypeId ) {
			$strCondition = ' CASE
                WHEN date( cd.created_on) = date( CURRENT_DATE - dfr.action_days ) 
                THEN date( cd.created_on) = date( CURRENT_DATE -dfr.action_days )
                     AND to_char(cd.created_on, \'HH24:MI:SS\') <= \'17:00:00\'
	            END  AND dfr.followup_action_type_id = ' . CFollowupActionType::DISPUTE_HIGHLIGHT . ' AND';
		} elseif( CFollowupActionType::DISPUTE_EMAIL == $intDisputeSubMenuTypeId ) {
			$strCondition = ' CASE
                                WHEN  date( cd.created_on) < date( CURRENT_DATE - dfr.action_days ) THEN date( cd.created_on) < date( CURRENT_DATE -dfr.action_days )
                                WHEN  date( cd.created_on) = date( CURRENT_DATE - dfr.action_days ) THEN to_char(cd.created_on, \'HH24:MI:SS\') <= \'17:00:00\' 
                             END AND dfr.followup_action_type_id = ' . CFollowupActionType::DISPUTE_EMAIL . ' AND';
		} else {
			$strCondition = ' CASE
                                WHEN date( cd.created_on) = date( CURRENT_DATE - dfr.action_days ) 
                                THEN date( cd.created_on) = date( CURRENT_DATE -dfr.action_days )
                                     AND to_char(cd.created_on, \'HH24:MI:SS\') <= \'17:00:00\'
	                            END  AND dfr.followup_action_type_id = ' . CFollowupActionType::DISPUTE_HIGHLIGHT . ' OR
							 CASE
                                WHEN  date( cd.created_on) < date( CURRENT_DATE - dfr.action_days ) THEN date( cd.created_on) < date( CURRENT_DATE -dfr.action_days )
                                WHEN  date( cd.created_on) = date( CURRENT_DATE - dfr.action_days ) THEN to_char(cd.created_on, \'HH24:MI:SS\') <= \'17:00:00\' 
                             END AND dfr.followup_action_type_id = ' . CFollowupActionType::DISPUTE_EMAIL . ' AND';
		}

		$strSql = '
					     SELECT
					         COUNT ( DISTINCT cd.id ) AS total_records

					      FROM
					          dispute_status_types dst
					          LEFT JOIN consumer_disputes cd ON dst.id = cd.dispute_status_type_id AND cd.dispute_type_id<>' . CDisputeType::OTHER . '
					          JOIN dispute_followup_rules dfr ON dfr.dispute_status_type_id = dst.id
					      WHERE ' . $strCondition . '
					           dst.id NOT IN(' . implode( ', ', CDisputeStatusType::$c_arrintNotIncludeDisputeStatusTypeIds ) . ') AND dst.id <> ' . CDisputeStatusType::DISPUTE_STATUS_TYPE_COMPLETE;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchConsumerDisputeByCompletedStatusByCompletedOn( $strStartDate, $strEndDate, $objDatabase ) {

		if( true == is_null( $strStartDate ) || true == is_null( $strEndDate ) ) {
			return NULL;
		}

		$strSql = ' SELECT 
					    completed_by, COUNT(id) AS total_records
					FROM 
					      consumer_disputes
					WHERE 
					     dispute_status_type_id = ' . CDisputeStatusType::DISPUTE_STATUS_TYPE_COMPLETE . '
					    AND DATE_TRUNC(\'day\', completed_on) BETWEEN DATE(\'' . $strStartDate . '\') AND DATE(\'' . $strEndDate . '\')
					GROUP BY completed_by';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActionableConsumerDisputeByUpdatedOn( $strStartDate, $strEndDate, $objDatabase ) {

		if( true == is_null( $strStartDate ) || true == is_null( $strEndDate ) ) {
			return NULL;
		}

		$strSql = ' SELECT 
					      assigned_to, COUNT(id) AS total_records
					FROM 
					      consumer_disputes
					WHERE 
						dispute_status_type_id NOT IN(' . implode( ', ', CDisputeStatusType::$c_arrintNotIncludeDisputeStatusTypeIds ) . ') 
					    AND dispute_status_type_id <> ' . CDisputeStatusType::DISPUTE_STATUS_TYPE_COMPLETE . '
					    AND DATE_TRUNC(\'day\', updated_on) BETWEEN DATE(\'' . $strStartDate . '\') AND DATE(\'' . $strEndDate . '\')
					GROUP By assigned_to';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFCRARequirementsFailedConsumerDisputeByUpdatedOn( $strStartDate, $strEndDate, $objDatabase ) {

		if( true == is_null( $strStartDate ) || true == is_null( $strEndDate ) ) {
			return NULL;
		}

		$strSql = ' SELECT  
					    cd.assigned_to, COUNT(DISTINCT cd.id) AS total_records
					FROM dispute_status_types dst 
					    LEFT JOIN dispute_followup_rules dfr ON dfr.dispute_status_type_id = dst.id AND dfr.followup_action_type_id IN(' . CFollowupActionType::DISPUTE_HIGHLIGHT . ', ' . CFollowupActionType::DISPUTE_EMAIL . ' )
					    LEFT JOIN consumer_disputes cd ON dst.id = cd.dispute_status_type_id AND cd.dispute_type_id <> ' . CDisputeType::OTHER . ' 
					    AND CURRENT_DATE >= ( cd.created_on + ( dfr.action_days || \' days \' )::INTERVAL ) 
					WHERE dst.id NOT IN(' . implode( ', ', CDisputeStatusType::$c_arrintNotIncludeDisputeStatusTypeIds ) . ')
						AND dst.id <> ' . CDisputeStatusType::DISPUTE_STATUS_TYPE_COMPLETE . '
						AND DATE_TRUNC(\'day\', cd.updated_on) BETWEEN DATE(\'' . $strStartDate . '\') AND DATE(\'' . $strEndDate . '\')
					    AND cd.id IS NOT NULL
					GROUP BY cd.assigned_to';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAvgCompletionTimeForConsumerDisputeByCompletedOn( $strStartDate, $strEndDate, $objDatabase ) {

		if( true == is_null( $strStartDate ) || true == is_null( $strEndDate ) ) {
			return NULL;
		}

		$strSql = ' SELECT completed_by,
					    ((EXTRACT( DAYS FROM  total_time))*24 + (EXTRACT( HOUR FROM  total_time))) AS hours,
					    EXTRACT( MINUTE FROM  total_time) AS minutes
					
					FROM (
							SELECT 
							    completed_by, AVG(completed_on - created_on) AS total_time
							FROM 
							      consumer_disputes
							WHERE 
								dispute_status_type_id = ' . CDisputeStatusType::DISPUTE_STATUS_TYPE_COMPLETE . '
								AND DATE_TRUNC(\'day\', completed_on) BETWEEN DATE(\'' . $strStartDate . '\') AND DATE(\'' . $strEndDate . '\')
							GROUP By completed_by
						) AS temp_consumer_disputes';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompletedDisputesByScreeningApplicantId( $intScreeningApplicantId, $objDatabase ) {

		if( false == valId( $intScreeningApplicantId ) ) return NULL;

		$strSql = 'SELECT
						cd.id,
						cd.dispute_type_id
					FROM
						consumer_disputes cd
						JOIN consumers c ON c.id = cd.consumer_id
					WHERE
						c.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND cd.dispute_status_type_id = ' . CDisputeStatusType::DISPUTE_STATUS_TYPE_COMPLETE;

		$arrmixCompletedDisputes = executeSql( $strSql, $objDatabase );

		return $arrmixCompletedDisputes['data'];
	}

}
?>