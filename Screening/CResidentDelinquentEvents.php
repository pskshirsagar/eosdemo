<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CResidentDelinquentEvents
 * Do not add any new functions to this class.
 */

class CResidentDelinquentEvents extends CBaseResidentDelinquentEvents {

	public static function fetchResidentDelinquentEventsByPropertyIdByLeaseIdsByCustomerIdsByDeliquentEventTypeIdsByCid( $intPropertyId, $arrintLeaseIds, $arrintCustomerIds, $arrintDeliquentEventTypeIds = NULL, $intCid, $objDatabase ) {
		$arrintLeaseIds    = array_filter( $arrintLeaseIds );
		$arrintCustomerIds = array_filter( $arrintCustomerIds );
		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintCustomerIds ) ) {
			return NULL;
		}

		$strCondition = ( true == valArr( $arrintDeliquentEventTypeIds ) ) ? ' AND delinquent_event_type_id IN ( ' . implode( ',', $arrintDeliquentEventTypeIds ) . ' ) ' : '';

		$strSql = 'SELECT
						*
					FROM
						resident_delinquent_events
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND delinquent_event_status_type_id = 1' .
		          $strCondition;

		$arrmixTempResidentDelinquentEvents = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixTempResidentDelinquentEvents ) ) {
			return NULL;
		}

		$arrmixResidentDelinquentEvents = [];

		foreach( $arrmixTempResidentDelinquentEvents as $arrmixData ) {
			$strEventKey                                    = $arrmixData['cid'] . '_' . $arrmixData['property_id'] . '_' . $arrmixData['lease_id'] . '_' . $arrmixData['customer_id'];
			$arrmixResidentDelinquentEvents[$strEventKey][] = $arrmixData;
		}

		return $arrmixResidentDelinquentEvents;
	}

}

?>