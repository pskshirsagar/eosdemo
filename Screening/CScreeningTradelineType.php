<?php

class CScreeningTradelineType extends CBaseScreeningTradelineType {

	const DELINQUENT 			= 1;
	const DELINQUENT_STUDENT 	= 2;
	const DELINQUENT_MEDICAL 	= 3;
	const DELINQUENT_UTILITY	= 10;
	const COLLECTION 			= 4;
	const COLLECTION_RENTAL 	= 5;
	const COLLECTION_UTILITY 	= 6;
	const COLLECTION_MEDICAL 	= 9;
    const COLLECTION_STUDENT    = 11;
	const FORECLOSURE 			= 7;
	const UNKNOWN 				= 8;
    const CHARGE_OFF            = 12;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>