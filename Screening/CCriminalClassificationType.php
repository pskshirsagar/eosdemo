<?php

class CCriminalClassificationType extends CBaseCriminalClassificationType {

	const PROPERTY 			= 1;
	const ANIMAL 			= 2;
	const FRAUD 			= 3;
	const TECHNOLOGY 		= 4;
	const FAMILY 			= 5;
	const GOVERNMENT 		= 6;
	const OPEN 				= 7;
	const GAMBLING 			= 8;
	const WEAPON 			= 9;
	const ORGANIZED 		= 10;
	const DRUG				= 11;
	const SEX 				= 12;
	const ALCOHOL 			= 13;
	const VIOLENCE	 		= 14;
	const TRAFFIC 			= 15;
	const UNCLASSIFIED 		= 16;
	const UNKNOWN 			= 17;
	const ALL				= 18;
	const THEFT_BY_CHECK 	= 19;
	const DRUG_POSSESSION 	= 20;
	const SEX_OFFENDER_REGISTRY	= 21;

	public static $c_arrintNoKeywordsClassificationTypeIds = array(
		self::ALL,
		self::UNKNOWN,
		self::SEX_OFFENDER_REGISTRY
	);

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// need to add
        }

        return $boolIsValid;
    }

}
?>