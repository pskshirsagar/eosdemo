<?php

class CScreeningAvailableCondition extends CBaseScreeningAvailableCondition {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningPackageAvailableConditionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningPackageConditionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningPackageChargeCodeAmountTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseChargeCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyToChargeCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChargeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConditionName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningIntegrationConditionCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
