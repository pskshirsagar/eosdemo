<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConfigPreferences
 * Do not add any new functions to this class.
 */

class CScreeningConfigPreferences extends CBaseScreeningConfigPreferences {

	public static function fetchScreeningConfigPreferencesByScreeningConfigurationIdByScreeningConfigPreferenceTypeId( $intScreeningConfigurationId, $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		return self::fetchScreeningConfigPreferences( sprintf( 'SELECT * FROM %s WHERE screening_configuration_id = %d AND screening_config_preference_type_id = %d ORDER BY id DESC', 'screening_config_preferences', $intScreeningConfigurationId, $intScreeningConfigPreferenceTypeId ), $objDatabase );
	}

}
?>