<?php

class CScreeningCriteriaSetting extends CBaseScreeningCriteriaSetting {

	protected $m_intConditionEvaluationOrder;
	protected $m_intScreeningRecommendationTypeId;
	protected $m_intScreeningConfigValueTypeId;
	protected $m_intScreenTypeId;
	protected $m_intScreeningPackageId;

	public function getScreeningConfigValueTypeId() {
		return $this->m_intScreeningConfigValueTypeId;
	}

	public function setScreeningConfigValueTypeId( $intScreeningConfigValueTypeId ) {
		$this->m_intScreeningConfigValueTypeId = $intScreeningConfigValueTypeId;
	}

	public function getConditionEvaluationOrder() {
		return $this->m_intConditionEvaluationOrder;
	}

	public function setConditionEvaluationOrder( $intConditionEvaluationOrder ) {
		$this->m_intConditionEvaluationOrder = $intConditionEvaluationOrder;
	}

	public function getTimelineFilterYear() {
		return $this->m_intTimelineFilterYear;
	}

	public function setTimelineFilterYear( $intTimelineFilterYear ) {
		$this->m_intTimelineFilterYear = $intTimelineFilterYear;
	}

	public function getScreeningRecommendationTypeId() {
		return $this->m_intScreeningRecommendationTypeId;
	}

	public function setScreeningRecommendationTypeId( $intScreeningRecommendationTypeId ) {
		$this->m_intScreeningRecommendationTypeId = $intScreeningRecommendationTypeId;
	}

	public function getScreenTypeId() {
		return $this->m_intScreenTypeId;
	}

	public function setScreenTypeId( $intScreenTypeId ) {
		$this->m_intScreenTypeId = $intScreenTypeId;
	}

	public function getScreeningPackageId() {
		return $this->m_intScreeningPackageId;
	}

	public function setScreeningPackageId( $intScreeningPackageId ) {
		$this->m_intScreeningPackageId = $intScreeningPackageId;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['evaluation_order'] ) ) 							$this->setConditionEvaluationOrder( $arrValues['evaluation_order'] );
		if( true == isset( $arrValues['screening_recommendation_type_id'] ) ) 			$this->setScreeningRecommendationTypeId( $arrValues['screening_recommendation_type_id'] );
		if( true == isset( $arrValues['filter_value'] ) ) 								$this->setTimelineFilterYear( $arrValues['filter_value'] );
		if( true == isset( $arrValues['screening_config_value_type_id'] ) ) 			$this->setScreeningConfigValueTypeId( $arrValues['screening_config_value_type_id'] );
		if( true == isset( $arrValues['screen_type_id'] ) ) 							$this->setScreenTypeId( $arrValues['screen_type_id'] );
		if( true == isset( $arrValues['screening_package_id'] ) )                       $this->setScreeningPackageId( $arrValues['screening_package_id'] );

		return;
	}

}
?>