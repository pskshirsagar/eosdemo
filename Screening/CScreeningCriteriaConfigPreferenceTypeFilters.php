<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCriteriaConfigPreferenceTypeFilters
 * Do not add any new functions to this class.
 */

class CScreeningCriteriaConfigPreferenceTypeFilters extends CBaseScreeningCriteriaConfigPreferenceTypeFilters {

	public static function fetchActiveScreeningCriteriaConfigPreferenceTypeFiltersByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {
		$strSql = sprintf( 'SELECT * FROM screening_criteria_config_preference_type_filters WHERE is_active = true AND screening_criteria_id = %d', ( int ) $intScreeningCriteriaId );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningCriteriaConfigPreferenceTypeFiltersByScreeningCriteriaIdByScreenTypeId( $intScreeningCriteriaId, $intScreenTypeId, $objDatabase ) {
		$strSql = 'SELECT
						sccptf.*,
						sf.filter_value AS screening_config_preference_filter_value,
						sf.screening_filter_type_id,
						sf.id as screening_filter_id
					FROM
						screening_criteria_config_preference_type_filters sccptf
						LEFT JOIN screening_filters sf ON sf.id = sccptf.screening_filter_id
						JOIN screening_config_preference_types scpt ON ( scpt.id = sccptf.screening_config_preference_type_id AND scpt.screen_type_id = ' . ( int ) $intScreenTypeId . ')
					WHERE
						sccptf.screening_criteria_id = ' . ( int ) $intScreeningCriteriaId . '						
						AND sccptf.is_active = true';

		return parent::fetchScreeningCriteriaConfigPreferenceTypeFilters( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningCriteriaConfigPreferenceTypeFiltersByScreeningPackageIdByScreenTypeIds( $intScreeningPackageId, $arrintScreenTypeIds, $objDatabase ) {
		$strSql = 'SELECT
						sccptf.*,
						sf.filter_value AS screening_config_preference_filter_value,
						sf.screening_filter_type_id,
						sf.id as screening_filter_id
					FROM
						screening_criteria_config_preference_type_filters sccptf
						LEFT JOIN screening_filters sf ON sf.id = sccptf.screening_filter_id
						JOIN screening_config_preference_types scpt ON scpt.id = sccptf.screening_config_preference_type_id
						JOIN screening_package_screen_type_associations spcta ON spcta.screening_criteria_id = sccptf.screening_criteria_id
					WHERE
						spcta.screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND spcta.screen_type_id IN( ' . sqlIntImplode( $arrintScreenTypeIds ) . ' )
						AND sccptf.is_active = true';

		return parent::fetchScreeningCriteriaConfigPreferenceTypeFilters( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningCriteriaConfigPreferenceTypeFiltersByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {

		$strSql = 'SELECT
					    sccptf.*,
						sf.screening_filter_type_id
					FROM
					    screening_criteria_config_preference_type_filters sccptf
						JOIN screening_filters sf ON ( sf.id = sccptf.screening_filter_id )
						JOIN screening_package_screen_type_associations spcta ON ( spcta.screening_criteria_id = sccptf.screening_criteria_id )
					WHERE
					    spcta.screening_package_id = ' . ( int ) $intScreeningPackageId . '
					    AND sccptf.is_active IS TRUE';

		return self::fetchScreeningCriteriaConfigPreferenceTypeFilters( $strSql, $objDatabase );
	}

}

?>