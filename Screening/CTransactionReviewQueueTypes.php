<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CTransactionReviewQueueTypes
 * Do not add any new functions to this class.
 */

class CTransactionReviewQueueTypes extends CBaseTransactionReviewQueueTypes {

	public static function fetchTransactionReviewQueueTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CTransactionReviewQueueType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTransactionReviewQueueType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CTransactionReviewQueueType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>