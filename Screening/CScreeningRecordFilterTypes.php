<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningRecordFilterTypes
 * Do not add any new functions to this class.
 */

class CScreeningRecordFilterTypes extends CBaseScreeningRecordFilterTypes {

    public static function fetchScreeningRecordFilterTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CScreeningRecordFilterType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchScreeningRecordFilterType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScreeningRecordFilterType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchActiveScreeningRecordFilterTypes( $objDatabase ) {
        $strSql = 'select * from screening_record_filter_types where is_published=1';
        return self::fetchScreeningRecordFilterTypes( $strSql, $objDatabase );
    }
}
?>