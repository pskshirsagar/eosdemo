<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCriterias
 * Do not add any new functions to this class.
 */

class CScreeningCriterias extends CBaseScreeningCriterias {

	public static $c_arrmixTestCaseScreeningCriteriaIds = [
		129881,129476,129474,129470,129475,129477,129738,129840,129847,129836,129809,129882,129812,129473,129733,130596,127999,129848,129469,128942,129825,129742,130481,129821,129810,129740,129468,129736,129883,129823,129838,129844,129739
	];

	public static function fetchTotalCountScreeningCriteriasByScreenTypeIdByCid( $intScreenTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count(id)
					FROM
						screening_criterias
					WHERE
						screen_type_id = ' . ( int ) $intScreenTypeId . '
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningCriteriasByScreenTypeIdByCid( $intScreenTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sc.*,
						COUNT ( sp.id ) AS package_count,
						array_agg( sp.name ) AS package_list
					FROM
						screening_criterias sc
						LEFT JOIN screening_package_screen_type_associations spsta ON spsta.screening_criteria_id = sc.id AND spsta.screen_type_id = sc.screen_type_id AND spsta.is_published = 1
						LEFT JOIN screening_packages sp ON spsta.screening_package_id = sp.id AND sp.is_published = 1 AND sp.is_active = TRUE  
					WHERE
						sc.is_published = true
						AND sc.screen_type_id = ' . ( int ) $intScreenTypeId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.id NOT IN ( ' . sqlIntImplode( self::$c_arrmixTestCaseScreeningCriteriaIds ) . ' )
					GROUP BY sc.id
					ORDER BY sc.updated_on DESC';

			$arrmixResult = executeSql( $strSql, $objDatabase );
			return ( false == getArrayElementByKey( 'failed', $arrmixResult ) ) ? getArrayElementByKey( 'data', $arrmixResult ) : [];
	}

	public static function fetchPublishedScreeningCriteriasWithSettingsByScreenTypeIdByCid( $intScreenTypeId, $intCid, $objDatabase ) {

		if( $intScreenTypeId == CScreenType::CRIMINAL ) {
			$strJoinSql = ' JOIN criminal_criteria_settings scs ON scs.screening_criteria_id = sc.id ';
		} else {
			$strJoinSql = ' JOIN screening_criteria_settings scs ON scs.screening_criteria_id = sc.id ';
		}

		$strSql = ' SELECT
					      sc.*
					  FROM
					      screening_criterias sc
					      ' . $strJoinSql . '
					  WHERE
					      sc.is_published = TRUE
					      AND sc.screen_type_id = ' . ( int ) $intScreenTypeId . '
						  AND sc.cid = ' . ( int ) $intCid . '
					  GROUP BY
					      sc.id
					  ORDER BY
					      sc.updated_on DESC';

		$arrmixResult = executeSql( $strSql, $objDatabase );

		return ( false == getArrayElementByKey( 'failed', $arrmixResult ) ) ? getArrayElementByKey( 'data', $arrmixResult ) : [];
	}

	public static function fetchPublishedScreeningCriteriasWithSettingsByScreenTypeIdsByCid( $arrmixScreenTypes, $intCid, $objDatabase ) {

		if( false == valArr( $arrmixScreenTypes ) ) return NULL;

		$arrmixCriminalResult = [];
		$arrmixAllResult      = [];

		if( true == in_array( CScreentype::CRIMINAL, $arrmixScreenTypes ) ) {

			$strSql = ' SELECT
					      sc.*
					  FROM
					      screening_criterias sc
					      JOIN criminal_criteria_settings scs ON scs.screening_criteria_id = sc.id 
					  WHERE
					      sc.is_published = TRUE
					      AND sc.screen_type_id = ' . ( int ) CScreentype::CRIMINAL . '
						  AND sc.cid = ' . ( int ) $intCid . '
					  GROUP BY
					      sc.id
					  ORDER BY
					      sc.updated_on DESC';

			$arrmixResult           = executeSql( $strSql, $objDatabase );
			$arrmixCriminalResult   = ( false == getArrayElementByKey( 'failed', $arrmixResult ) ) ? getArrayElementByKey( 'data', $arrmixResult ) : [];
			$arrmixCriminalResult   = rekeyArray( 'id', $arrmixCriminalResult );
		}

		if( ( $intCriminalKey = array_search( CScreenType::CRIMINAL, $arrmixScreenTypes ) ) !== false ) {
			unset( $arrmixScreenTypes[$intCriminalKey] );
		}

		if( false == valArr( $arrmixScreenTypes ) ) {
			return $arrmixCriminalResult;
		}

		$strSql = ' SELECT
				      sc.*
				  FROM
				      screening_criterias sc
				      JOIN screening_criteria_settings scs ON scs.screening_criteria_id = sc.id 
				  WHERE
				      sc.is_published = TRUE
				      AND screen_type_id IN ( ' . implode( ',', $arrmixScreenTypes ) . ')
					  AND sc.cid = ' . ( int ) $intCid . '
				  GROUP BY
				      sc.id
				  ORDER BY
				      sc.updated_on DESC';

			$arrmixResult = executeSql( $strSql, $objDatabase );

		$arrmixAllResult    = ( false == getArrayElementByKey( 'failed', $arrmixResult ) ) ? getArrayElementByKey( 'data', $arrmixResult ) : [];
		$arrmixAllResult    = rekeyArray( 'id', $arrmixAllResult );

		return array_merge( $arrmixCriminalResult, $arrmixAllResult );
	}

	public static function fetchCountScreeningCriteriaByCriteriaNameByScreenTypeIdByCid( $strCriteriaName, $intScreenTypeId,  $intCid, $objDatabase ) {

		$strCriteriaName = trim( $strCriteriaName );

		$strSql = 'SELECT 
						count( id ) AS record_count 
					FROM 
						screening_criterias 
					WHERE 
						cid=' . ( int ) $intCid . '
					AND screen_type_id=' . ( int ) $intScreenTypeId . '
					AND is_published = true
					AND lower(criteria_name) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strCriteriaName ) ) . '\'';
		$arrmixResult = executeSql( $strSql, $objDatabase );
		$arrmixResult = ( false == getArrayElementByKey( 'failed', $arrmixResult ) ) ? getArrayElementByKey( 'data', $arrmixResult ) : [];
		return $arrmixResult[0]['record_count'];
	}

	public static function fetchCustomScreeningCriteriaAssociatedPackagesDataByScreeningCriteriaIdByScreenTypeIdByCid( $intScreeningCriteriaId, $intScreenTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						COUNT ( sp.id ) AS package_count,
						array_agg( sp.name ) AS package_list
					FROM
						screening_criterias sc
						JOIN screening_package_screen_type_associations spsta ON spsta.screening_criteria_id = sc.id AND spsta.screen_type_id = sc.screen_type_id AND spsta.is_published = 1
						JOIN screening_packages sp ON spsta.screening_package_id = sp.id AND sp.is_published = 1
					WHERE
						sc.is_published = true
						AND sc.screen_type_id = ' . ( int ) $intScreenTypeId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.id = ' . ( int ) $intScreeningCriteriaId . '
					GROUP BY sc.id
					ORDER BY sc.updated_on DESC';

		$arrmixResult = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrmixResult ) ) ? current( $arrmixResult ) : [];
	}

    public static function fetchPublishedMidleaseTransferRenewalScreeningCriteriasWithSettingsByScreenTypeIdsByCid( $arrmixScreenTypes, $intCid, $objDatabase ) {

        if( false == valArr( $arrmixScreenTypes ) ) return NULL;
        $strSql = ' ( SELECT
				      sc.*,
				       CASE
				            WHEN ( ( ' . CScreeningPackageLeaseType::MID_LEASE_MODIFICATION . ' = ANY( scons.screening_package_lease_type_ids ) OR ' . CScreeningPackageLeaseType::RENEWAL . ' = ANY( scons.screening_package_lease_type_ids ) OR
							 ' . CScreeningPackageLeaseType::TRANSFER . ' = ANY( scons.screening_package_lease_type_ids ) OR scons.screening_package_lease_type_ids IS NULL )) 
							THEN 1
							ELSE 0
						END AS is_only_for_midlease_renewal_transfer
				  FROM
				      screening_criterias sc
				      JOIN screening_criteria_condition_sets sccs ON sccs.screening_criteria_id = sc.id AND sccs.is_active = TRUE 
				      JOIN screening_condition_sets scons ON scons.id = sccs.screening_condition_set_id
                      JOIN screening_condition_subsets sconsub ON ( sconsub.id = scons.first_screening_condition_subset_id OR sconsub.id = scons.second_screening_condition_subset_id ) 
                      JOIN screening_condition_subset_values sconsubval ON ( sconsubval.screening_condition_subset_id = sconsub.id ) 
				      LEFT JOIN screening_available_conditions sac ON ( sac.id = sconsubval.screening_available_condition_id AND sac.cid = sc.cid AND sac.screening_package_condition_type_id IN ( ' . sqlIntImplode( CScreeningPackageConditionType::$c_arrintMidLeaseTransferRenewalScreeningConditionTypeIds ) . ' )  )
				  WHERE
				      sc.is_published = TRUE';
        $strSql .= ' AND screen_type_id IN ( ' . sqlIntImplode( $arrmixScreenTypes ) . ')
					  AND sc.cid = ' . ( int ) $intCid . '
				  GROUP BY
				      sc.id,
				      scons.screening_package_lease_type_ids
				  ORDER BY
				      sc.updated_on DESC ) ';
        $strSql .= ' UNION ( SELECT
    					      sc.*,
    					        1 AS is_only_for_midlease_renewal_transfer
	    				  FROM
		    			      screening_criterias sc
			    		      JOIN screening_criteria_condition_sets sccs ON sccs.screening_criteria_id = sc.id AND sccs.is_active = TRUE 
				    	  WHERE
					          sc.is_published = TRUE ';
        $strSql .= ' AND sc.screen_type_id IN ( ' . implode( ',', $arrmixScreenTypes ) . ')
    					  AND sc.cid = ' . ( int ) $intCid . '
					  GROUP BY
					      sc.id
					      HAVING count( sccs.id ) = 2 );';
		// The second sql fetches only criteria having fail and pass condition set.
        $arrmixResult = executeSql( $strSql, $objDatabase );
	    $arrmixAllResults    = ( false == getArrayElementByKey( 'failed', $arrmixResult ) ) ? getArrayElementByKey( 'data', $arrmixResult ) : [];
	    $arrmixFinalResult = array();
	    if( valArr( $arrmixAllResults ) ) {
		    foreach( $arrmixAllResults as $arrmixAllResult ) {
			    if( array_key_exists( $arrmixAllResult['id'], $arrmixFinalResult ) ) {
				    if( true == $arrmixFinalResult[$arrmixAllResult['is_only_for_midlease_renewal_transfer']] ) {
					    $arrmixFinalResult[$arrmixAllResult['id']] = $arrmixAllResult;
				    }
			    } else {
				    $arrmixFinalResult[$arrmixAllResult['id']] = $arrmixAllResult;
			    }
		    }
	    }

        return $arrmixFinalResult;
    }

    public static function fetchScreeningCriteriaByScreenTypeIdByCid( $intScreenTypeId, $intCid, $objDatabase ) {

        $strSql = 'SELECT
						sc.*
					FROM
						screening_criterias sc
					WHERE
						sc.screen_type_id = ' . ( int ) $intScreenTypeId . '
						AND sc.cid = ' . ( int ) $intCid;

        return parent::fetchScreeningCriteria( $strSql, $objDatabase );
    }

}
?>