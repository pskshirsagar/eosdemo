<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConditionSubsets
 * Do not add any new functions to this class.
 */

class CScreeningConditionSubsets extends CBaseScreeningConditionSubsets {

	public static function fetchScreeningConditionSubsetsWithScreeningConditionSubsetValuesByIds( $arrintScreeningConditionSubsetIds, $objDatabase ) {
		if( false == valArr( $arrintScreeningConditionSubsetIds ) ) return NULL;

		$strSql = '	SELECT
						scss.*, 
						scsv.screening_available_condition_id, 
						scsv.default_value
					FROM
						screening_condition_subsets scss
						JOIN screening_condition_subset_values scsv ON ( scss.id = scsv.screening_condition_subset_id )
					WHERE
						scss.id IN ( ' . implode( ',', $arrintScreeningConditionSubsetIds ) . ' )
					ORDER BY scsv.id';

		return parent::fetchObjects( $strSql, 'CScreeningConditionSubset', $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchScreeningConditionSubsetsByIds( $arrintScreeningConditionSubsetIds, $objDatabase ) {
		if( false == valArr( $arrintScreeningConditionSubsetIds ) ) return NULL;

		$strSql = '	SELECT
						scss.*
					FROM
						screening_condition_subsets scss
					WHERE
						scss.id IN ( ' . implode( ',', $arrintScreeningConditionSubsetIds ) . ' )
					ORDER BY scss.id';

		return parent::fetchScreeningConditionSubsets( $strSql, $objDatabase );
	}
}
?>