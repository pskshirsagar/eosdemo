<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CRvReportCriteriaRangeTypes
 * Do not add any new functions to this class.
 */

class CRvReportCriteriaRangeTypes extends CBaseRvReportCriteriaRangeTypes {

	public static function fetchRvReportCriteriaRangeTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CRvReportCriteriaRangeType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchRvReportCriteriaRangeType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CRvReportCriteriaRangeType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchRvReportCriteriaRangeTypeIds( $objDatabase ) {

		$strSql = 'SELECT
						distinct scpt.id , scpt.name
					FROM
						rv_report_criteria_range_types rrcrt
					JOIN
						screening_config_preference_types scpt ON scpt.id = rrcrt.screening_config_preference_type_id
					WHERE
						scpt.is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}
}
?>