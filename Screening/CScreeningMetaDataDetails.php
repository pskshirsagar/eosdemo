<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningMetaDataDetails
 * Do not add any new functions to this class.
 */

class CScreeningMetaDataDetails extends CBaseScreeningMetaDataDetails {

    public static function fetchScreeningMetaDataDetails( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CScreeningMetaDataDetail', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchScreeningMetaDataDetail( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScreeningMetaDataDetail', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchCustomScreeningMetaDataDetails( $objDatabase ) {

    	$strSql = 'SELECT
    					name,
    					screening_tradeline_type_id,
    					screening_meta_data_type_id
    				FROM
    					screening_meta_data_details';

    	return self::fetchScreeningMetaDataDetails( $strSql, $objDatabase );
    }
}
?>