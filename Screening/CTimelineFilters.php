<?php

class CTimelineFilters extends CBaseTimelineFilters {

    public static function fetchTimelineFilter( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CTimelineFilter', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>