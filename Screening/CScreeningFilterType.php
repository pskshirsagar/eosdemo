<?php

class CScreeningFilterType extends CBaseScreeningFilterType {

	const TIMELINE_FILTER	= 1;
	const STATUS_FILTER		= 2;
	const TRADELINE_FILTER	= 3;
	const TIME_PERIOD_FILTER= 4;

	public static $c_arrintScreeningFilterTypes = array(
		self::TIMELINE_FILTER	=> 'TIMELINE_FILTER',
		self::STATUS_FILTER		=> 'STATUS_FILTER',
		self::TRADELINE_FILTER	=> 'TRADELINE_FILTER',
		self::TIME_PERIOD_FILTER=> 'TIME_PERIOD_FILTER'
    );

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>