<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageLeaseTypes
 * Do not add any new functions to this class.
 */

class CScreeningPackageLeaseTypes extends CBaseScreeningPackageLeaseTypes {

	public static function fetchScreeningPackageLeaseTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CScreeningPackageLeaseType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningPackageLeaseType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CScreeningPackageLeaseType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchActiveScreeningPackageLeaseTypes( $objDatabase ) {
		$strSql = 'SELECT
						id,
						name
					FROM
						screening_package_lease_types
					WHERE
						is_active = true
					ORDER BY
						id ASC';

		return fetchData( $strSql, $objDatabase );
	}

}
?>