<?php

class CScreeningCreditAlertType extends CBaseScreeningCreditAlertType {

	const ALERT_FACTA_ACTIVE_DUTY 				= 1;
	const ALERT_FACTA_FRAUD			 			= 2;
	const ALERT_FACTA_CONSUMER_STMT				= 3;
	const ALERT_CREDIT_ERROR					= 4;
	const ALERT_OTHER_UNKNOWN					= 5;

	public static $c_arrintFactaAlertTypes 		= array( self::ALERT_FACTA_ACTIVE_DUTY, self::ALERT_FACTA_FRAUD, self::ALERT_FACTA_CONSUMER_STMT );

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>