<?php

class CScreeningConditionSet extends CBaseScreeningConditionSet {

	const INTEGRATION_SCREENING_CONDITION_SET_NAME = 'Screening Integration Condition Set';

	const PASS_CONDITION_EVALUATION_ORDER_ID		= 999;
	const FAIL_CONDITION_EVALUATION_ORDER_ID		= -1;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningRecommendationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFirstScreeningConditionSubsetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSecondScreeningConditionSubsetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningPackageOperandTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConditionName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEvaluationOrder() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAlwaysApply() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

    public function getMaxEvaluationId( $objDatabase ) {
        $strSql = 'SELECT
						max( evaluation_order ) max_evaluation_order
					FROM
						screening_condition_sets
					WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND screening_recommendation_type_id = ' . ( int ) CScreeningRecommendationType::PASSWITHCONDITIONS . '
						AND is_published = true';

        $arrmixResult = executeSql( $strSql, $objDatabase );

        if( 1 == getArrayElementByKey( 'Failed', $arrmixResult ) ) {
            return false;
        } else {
            $arrmixData = getArrayElementByKey( 'data', $arrmixResult );
            return $arrmixData[0]['max_evaluation_order'];
        }
    }

}
?>
