<?php

class CTransactionReviewDocument extends CBaseTransactionReviewDocument {

	protected $m_strUpdatedByUserName;

	public function getUpdatedByUserName() {
		return $this->m_strUpdatedByUserName;
	}

	public function setUpdatedByUserName( $strUpdatedByUserName ) {
		$this->m_strUpdatedByUserName = $strUpdatedByUserName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningApplicantId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionReviewId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDocumentPath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function deleteFile( $strFolderPath = NULL ) {
		if( false == is_null( $this->getDocumentPath() ) && false == is_null( $this->getFileName() ) ) {

			$arrstrFileExtensionsToDelete = array( '.tif', '.png', '.doc', '.docx', '.jpg', '.jpeg', '.bmp', '.pdf','BMP', 'GIF', 'JPE', 'ICO', 'TXT', 'TIFF', 'PSD', 'PDF' );

			$arrstrFileNameExtension = explode( '.', $this->getFileName() );

			foreach( $arrstrFileExtensionsToDelete as $strFileExtension ) {
				if( file_exists( $this->getDocumentPath() . $this->getFileName() . $arrstrFileNameExtension[0] . $strFileExtension ) ) {
					@unlink( $this->getFullFilePath( $strFolderPath ) . $arrstrFileNameExtension[0] . $strFileExtension );
				}
			}
		}
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrmixValues['updated_by_user_name'] ) ) $this->setUpdatedByUserName( $arrmixValues['updated_by_user_name'] );
		return;
	}

}
?>