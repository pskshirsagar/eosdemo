<?php

class CLogType extends CBaseLogType {

	const STATUS_CHANGE 	= 1;
	const CONTACT_NOTES 	= 2;
	const EMAIL_SENT 		= 3;
	const PHONE_CALL 		= 4;
	const VALUE_UPDATED 	= 5;
	const ASSIGNED_TO 		= 6;
	const OUTBOUND_NOTE 	= 7;

	public static $c_arrintDisputeNoteTypeIds = array(
		self::CONTACT_NOTES,
		self::EMAIL_SENT,
		self::OUTBOUND_NOTE
	);

	public static $c_arrintDisputeUpdateTypeIds = array(
		self::VALUE_UPDATED,
		self::ASSIGNED_TO,
		self::STATUS_CHANGE,
		self::CONTACT_NOTES,
		self::EMAIL_SENT,
		self::OUTBOUND_NOTE
	);

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>