<?php

class CRvScreeningApplicantDetail extends CBaseRvScreeningApplicantDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningApplicantId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerIncome() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRvScreeningLeaseDetailsId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsTransfered() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>