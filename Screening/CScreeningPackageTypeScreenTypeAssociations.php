<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageTypeScreenTypeAssociations
 * Do not add any new functions to this class.
 */

class CScreeningPackageTypeScreenTypeAssociations extends CBaseScreeningPackageTypeScreenTypeAssociations {

	public static function fetchPublishedScreeningPackageTypeScreenTypeAssociationsWithSelectionModeTypesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    sptsta.*,
					    st.name as name,
					    st.is_use_for_prescreening,
					    sar.selection_mode_type_id,
					    sar.screening_data_provider_id
					FROM
					    screening_package_type_screen_type_associations sptsta
					    INNER JOIN screen_Types st ON st.id = sptsta.screen_type_id
					    JOIN screening_account_rates sar ON sar.screen_type_id = st.id 
					    JOIN screening_accounts sa ON sa.id = sar.screening_account_id AND sa.cid = sar.cid AND sa.is_active = true
					WHERE
					    sptsta.is_published = 1
					    AND sar.selection_mode_type_id IS NOT NULL
					    AND sar.cid = ' . ( int ) $intCid . '
					    AND sptsta.screening_package_type_id NOT IN ( ' . implode( ',', CScreeningPackageType::$c_arrintVAScreeningPackageTypes ) . ')
					ORDER BY
					    sptsta.order_num';

		return self::fetchScreeningPackageTypeScreenTypeAssociations( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageTypeScreenTypeAssociationsByScreeningPackageTypeId( $intScreeningPackageTypeId, $objDatabase ) {

		$strSql = 'SELECT
					    sptsta.*,
					    st.name
					FROM
					    screening_package_type_screen_type_associations sptsta
					    INNER JOIN screen_types st ON st.id = sptsta.screen_type_id
					WHERE
				        sptsta.screening_package_type_id = ' . ( int ) $intScreeningPackageTypeId . '
					ORDER BY
					    sptsta.order_num';

		return self::fetchScreeningPackageTypeScreenTypeAssociations( $strSql, $objDatabase );
	}

	public static function fetchScreeningCriteriaIdByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {

		$strSql = 'SELECT
					    screening_criteria_id
					FROM
					    screening_package_screen_type_associations
					WHERE
				        screening_package_id = ' . ( int ) $intScreeningPackageId;

		return self::fetchColumn( $strSql, 'screening_criteria_id', $objDatabase );
	}

}
?>