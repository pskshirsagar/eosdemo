<?php

class CScreeningPackageOperandType extends CBaseScreeningPackageOperandType {

	const SCREENING_PACKAGE_OPERAND_TYPE_AND                     = 1;
	const SCREENING_PACKAGE_OPERAND_TYPE_OR                      = 2;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public static function getScreeningPackageOperandTypeName( $intScreeningPackageOperandTypeId ) {

		$strScreeningPackageOperandTypeName = '';

		switch( $intScreeningPackageOperandTypeId ) {
			case self::SCREENING_PACKAGE_OPERAND_TYPE_AND:
				$strScreeningPackageOperandTypeName = __( 'AND' );
				break;

			case self::SCREENING_PACKAGE_OPERAND_TYPE_OR:
				$strScreeningPackageOperandTypeName = __( 'OR' );
				break;

			default:
				// default case
				break;
		}

		return $strScreeningPackageOperandTypeName;
	}

}
?>