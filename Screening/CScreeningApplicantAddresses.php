<?php

class CScreeningApplicantAddresses extends CBaseScreeningApplicantAddresses {

	public static function fetchScreeningApplicantAddressByScreeningApplicantIdByCid( $intScreeningApplicantId, $intCid, $objDatabase ) {

		if( false == valId( $intScreeningApplicantId ) || false == valId( $intCid ) ) return;

		$strSql = ' SELECT
						*
					FROM
						screening_applicant_addresses
					WHERE
						screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND cid = ' . ( int ) $intCid;

		return parent::fetchScreeningApplicantAddress( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicantsPreviousAddressesByScreeningApplicantIdsByCid( $arrintScreeningApplicantIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScreeningApplicantIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						screening_applicant_addresses
					WHERE
						screening_applicant_id IN ( ' . sqlIntImplode( $arrintScreeningApplicantIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND address_type_id = ' . CScreeningApplicantAddressType::SCREENING_APPLICANT_PREVIOUS_ADDRESS;

		return self::fetchScreeningApplicantAddresses( $strSql, $objDatabase );
	}

}
?>