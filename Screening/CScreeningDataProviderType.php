<?php

class CScreeningDataProviderType extends CBaseScreeningDataProviderType {

    const TAZWORK               = 1;
    const RESIDENTVERIFY        = 2;
    const EXPERIAN              = 3;
    const EQUIFAX               = 4;
    const NBD                   = 5;
    const TAZWORK_TEST          = 6;
    const RESIDENT_VERIFY_TEST  = 7;
    const EXPERIAN_TEST         = 8;
    const EQUIFAX_TEST          = 9;
    const NBD_TEST              = 10;
    const COCOURT               = 11;
    const RENT_BUREAU           = 12;
    const RENT_BUREAU_TEST      = 13;
    const COCOURT_TEST          = 14;
    const PRECISE_ID            = 15;
    const PRECISE_ID_TEST       = 16;
    const RV_TEST_CASES         = 17;
	const VERIFICATION_OF_INCOME_TEST = 18;
	const VERIFICATION_OF_INCOME = 19;

	public static $c_arrintCreditDataProviderTypeIds = array(
		self::EXPERIAN,
		self::EQUIFAX,
		self::EQUIFAX_TEST,
		self::EXPERIAN_TEST
	);

	public static $c_arrintLiveDataProviderTypeIds = array(
		self::TAZWORK,
		self::RESIDENTVERIFY,
		self::EXPERIAN,
		self::EQUIFAX,
		self::NBD,
		self::COCOURT,
		self::RENT_BUREAU,
		self::PRECISE_ID,
	);

    // This array will hold data provider type ids whose
    // screening requests are not going to submit to respective data providers
    // in spite of having screening transaction exists
    // Specific cases : no ssn. list will increase in future
    public static $c_arrintExceptionalDataProviderTypeIds = array(
        self::TAZWORK,
        self::TAZWORK_TEST
    );

	public static $c_arrintExperianDataProviderTypeIds = array(
		self::EXPERIAN,
		self::EXPERIAN_TEST
	);

	public static $c_arrintRestrictManualReviewTypeIds = array(
		self::COCOURT_TEST,
		self::COCOURT
	);

    public static $c_arrintReconciliationDataProviderTypeIds = [ self::EXPERIAN ];


    // This two constant denote the data provider type whether of type live or test data provider...
    const TEST_DATA_PROVIDER_TYPE_ID    = 1;
    const LIVE_DATA_PROVIDER_TYPE_ID    = 0;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
                break;

            default:
            $boolIsValid = false;
        }

        return $boolIsValid;
    }

    public static function getScreeningDataProviderTypeName( $intScreeningDataProviderTypeId ) {

        $strScreeningDataProviderTypeName = '';

        switch( $intScreeningDataProviderTypeId ) {
            case self::TAZWORK:
                $strScreeningDataProviderTypeName = __( 'Tazwork' );
                break;

            case self::EXPERIAN:
                $strScreeningDataProviderTypeName = __( 'Experian' );
                break;

            case self::EQUIFAX:
                $strScreeningDataProviderTypeName = __( 'Equifax' );
                break;

            case self::RENT_BUREAU:
                $strScreeningDataProviderTypeName = __( 'Rent Bureau' );
                break;

            case self::PRECISE_ID:
                $strScreeningDataProviderTypeName = __( 'Precise ID' );
                break;

            default:
                // default case
                break;
        }

        return $strScreeningDataProviderTypeName;
    }

}
?>