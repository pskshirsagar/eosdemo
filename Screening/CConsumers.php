<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CConsumers
 * Do not add any new functions to this class.
 */

class CConsumers extends CBaseConsumers {

	public static function fetchConsumerByLastFourSsnForExcludeScreeningApplicantId( $strLastFourSsn, $intScreeningApplicantId,  $objDatabase ) {
		$strSql = ' SELECT
                        cd.dispute_type_id,
                        c.screening_applicant_id,
                        c.last_four_ssn
                    FROM
                        consumers c
                        JOIN consumer_disputes cd ON c.id = cd.consumer_id AND cd.dispute_type_id IN ( ' . CDisputeType::CRIMINAL . ' , ' . CDisputeType::EVICTION . ' )
                    WHERE
                        c.last_four_ssn = \'' . $strLastFourSsn . '\'
                        AND c.screening_applicant_id <> ' . ( int ) $intScreeningApplicantId . '
					GROUP BY 
						cd.dispute_type_id,
						c.screening_applicant_id,
						c.id
					ORDER BY 
						c.id DESC';

		$arrintScreeningApplicantIds = fetchData( $strSql, $objDatabase );

		return $arrintScreeningApplicantIds;

	}

	public static function fetchDisputedConsumerByScreeningApplicant( $objScreeningApplicant, $objDatabase ) {
		$strSql = ' SELECT 
                        cd.dispute_type_id,
                        c.screening_applicant_id
                    FROM 
                        consumers c
                        JOIN consumer_disputes cd ON c.id = cd.consumer_id AND cd.dispute_type_id IN ( ' . CDisputeType::CRIMINAL . ' , ' . CDisputeType::EVICTION . ' )
                    WHERE
                        lower( c.first_name ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $objScreeningApplicant->getNameFirst() ) ) . '\'
                        AND lower( c.last_name ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $objScreeningApplicant->getNameLast() ) ) . '\'
                        AND c.date_of_birth_encrypted = \'' . $objScreeningApplicant->getBirthDateEncrypted() . '\'
                        AND c.screening_applicant_id <> ' . ( int ) $objScreeningApplicant->getId() . '   
						
					GROUP BY 
						cd.dispute_type_id,
						c.screening_applicant_id,
						c.id
					ORDER BY 
						c.id DESC';
		$arrintScreeningApplicantIds = fetchData( $strSql, $objDatabase );

		return $arrintScreeningApplicantIds;
	}

}
?>
