<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageAvailableConditions
 * Do not add any new functions to this class.
 */

class CScreeningPackageAvailableConditions extends CBaseScreeningPackageAvailableConditions {

	public static function fetchPublishedScreeningPackageAvailableConditionsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningPackageAvailableConditions( sprintf( 'SELECT * FROM %s WHERE cid = %d AND is_published = 1 ', 'screening_package_available_conditions', $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageAvailableConditionsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return false;

		$strSql = '	SELECT
						*
					FROM
						screening_package_available_conditions
					WHERE
						id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND cid = ' . ( int ) $intCid;
		return self::fetchScreeningPackageAvailableConditions( $strSql, $objDatabase );
    }

    public static function fetchScreeningPackageAvailableConditionsByCids( $arrintCids, $objDatabase ) {
    	$strSql = 'SELECT
					    *
					FROM
					    screening_package_available_conditions
					WHERE
					    cid IN ( ' . implode( ',', $arrintCids ) . ' )
					AND is_published = 1';

		return self::fetchScreeningPackageAvailableConditions( $strSql, $objDatabase );
    }

    public static function fetchPublishedScreeningPackageAvailableConditionsIdsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return false;

		 $strSql = 'SELECT
						DISTINCT spac.id, spac.screening_package_available_condition_name
					FROM
					    screening_package_available_conditions AS spac
					    INNER JOIN screening_package_condition_subset_values spcsubval ON spcsubval.screening_package_available_condition_id = spac.id
					    INNER JOIN screening_package_condition_subsets spcsub ON spcsubval.screening_package_condition_subset_id = spcsub.id
					    INNER JOIN screening_package_condition_sets spcs ON ( spcs.first_screening_package_condition_subset_id = spcsub.id OR spcs.second_screening_package_condition_subset_id = spcsub.id )
					    INNER JOIN screening_packages sp ON sp.id = spcs.screening_package_id
					WHERE
						spac.id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND spcs.is_published = 1
						AND sp.is_published = 1
						AND spac.cid = ' . ( int ) $intCid;
		return fetchData( $strSql, $objDatabase );
    }

}
?>