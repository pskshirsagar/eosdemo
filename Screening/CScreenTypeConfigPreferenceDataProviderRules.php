<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreenTypeConfigPreferenceDataProviderRules
 * Do not add any new functions to this class.
 */

class CScreenTypeConfigPreferenceDataProviderRules extends CBaseScreenTypeConfigPreferenceDataProviderRules {

	public static function fetchActiveScreenTypeConfigPreferenceTypeDataProviderRulesByScreenTypeIdByScreeningDataProviderTypeIdByScreeningConfigPreferenceTypeId( $intScreenTypeId, $intScreeningDataProviderTypeId, $intScreeningConfigPreferenceTypeId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						screen_type_config_preference_data_provider_rules
					WHERE
						screen_type_id = ' . ( int ) $intScreenTypeId . '
						AND screening_data_provider_type_id = ' . ( int ) $intScreeningDataProviderTypeId . '
						AND screening_config_preference_type_id = ' . ( int ) $intScreeningConfigPreferenceTypeId . '
						AND is_active = 1';

		return parent::fetchScreenTypeConfigPreferenceDataProviderRule( $strSql, $objDatabase );
	}

}
?>