<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningTransactionConditions
 * Do not add any new functions to this class.
 */

class CScreeningTransactionConditions extends CBaseScreeningTransactionConditions {

	public static function archieveScreeningTransactionConditions( $intCurrentUserId, $arrintScreeningConfigPreferenceTypeIds, $intScreeningTransactionId, $objDatabase ) {
		if( false == valArr( $arrintScreeningConfigPreferenceTypeIds ) ) return false;
		$strUpdateSql = '';

		$strUpdateSql .= 'UPDATE
             screening_transaction_conditions
           SET
             is_active = FALSE ,
             updated_by = ' . ( int ) $intCurrentUserId . ',
             updated_on = NOW() 
           WHERE
             screening_transaction_id = ' . ( int ) $intScreeningTransactionId . '
             AND screening_config_preference_type_id IN( ' . sqlIntImplode( $arrintScreeningConfigPreferenceTypeIds ) . ' )
             AND is_active  IS TRUE;';

		return executeSql( $strUpdateSql, $objDatabase );
	}

	public static function fetchActiveScreeningTransactionConditionsByScreeningTransactionId( $intScreeningTransactionId, $objDatabase ) {

		$strSql = 'SELECT
            *
          FROM
            screening_transaction_conditions
          WHERE
            screening_transaction_id = ' . ( int ) $intScreeningTransactionId . '
            AND is_active IS TRUE';

		return parent::fetchScreeningTransactionConditions( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningConditionSetIdsInEvaluationOrderByScreeningTransactionIds( $arrintScreeningTransactionIds, $objDatabase ) {
		if( false == valArr( $arrintScreeningTransactionIds ) ) return false;

		$strSql = 'SELECT
                        screening_transaction_id,
                        id as condition_set_id
					FROM
                        (
                        SELECT
                            stc.screening_transaction_id,
                            scs.is_always_apply,
                            stc.screening_condition_set_id as id
                        FROM
                            screening_transaction_conditions stc
                            JOIN screening_condition_sets scs ON scs.id = stc.screening_condition_set_id
                        WHERE
                            screening_transaction_id IN ( ' . sqlIntImplode( $arrintScreeningTransactionIds ) . ' )
                            AND is_active IS TRUE
                        ) AS always_apply_conditions
					WHERE
                        is_always_apply = TRUE
					UNION	
						SELECT
							screening_transaction_id,
							condition_set_id
						FROM (
								SELECT
			                        stc.screening_transaction_id,
			                        scs.id as condition_set_id                            
			                      FROM
			                        screening_transaction_conditions stc
			                        JOIN screening_condition_sets scs ON scs.id = stc.screening_condition_set_id
			                      WHERE
			                        screening_transaction_id IN ( ' . sqlIntImplode( $arrintScreeningTransactionIds ) . ' )
			                        AND is_active IS TRUE
			                        AND scs.is_always_apply = FALSE
			                      ORDER BY
			                        scs.evaluation_order
			                        OFFSET 0 LIMIT 1
						) as worst_condition_set';

		$arrmixFetchedData = fetchData( $strSql, $objDatabase );

		return $arrmixFetchedData;
	}

}
?>