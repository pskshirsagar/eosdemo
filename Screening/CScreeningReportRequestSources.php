<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningReportRequestSources
 * Do not add any new functions to this class.
 */

class CScreeningReportRequestSources extends CBaseScreeningReportRequestSources {

	public static function fetchScreeningReportRequestSources( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CScreeningReportRequestSource::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningReportRequestSource( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CScreeningReportRequestSource::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedScreeningReportRequestSources( $objDatabase ) {
		$strSql = 'SELECT
		               id,
		               name
		            FROM
		               screening_report_request_sources
		            WHERE
		               is_published = TRUE';

        return fetchData( $strSql, $objDatabase );
	}

}
?>