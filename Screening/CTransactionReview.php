<?php

class CTransactionReview extends CBaseTransactionReview {

    protected $m_intScreeningId;
    protected $m_intScreeningApplicantId;
    protected $m_intPreviousReviewStatusId;

    const ASSIGN_NEXT_TEST_CLIENT       = 1;
    const ASSIGN_NEXT_PRIORITY_CLIENT   = 2;

	public static $c_arrintManualReviewPriorityClientIds = [ 8742, 3395, 14108, 3639, 2603, 2342, 13938, 11189, 12859, 4583, 14034, 13695, 13531, 13761, 3482, 15573, 13576, 14624, 16431 ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionReviewStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionReviewPriorityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function valCompletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReviewedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTransactionReviewQueueTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setScreeningId( $intScreeningId ) {
	    $this->m_intScreeningId = $intScreeningId;
	}

	public function getScreeningId() {
	    return $this->m_intScreeningId;
	}

	public function setScreeningApplicantId( $intScreeningApplicantId ) {
	    $this->m_intScreeningApplicantId = $intScreeningApplicantId;
	}

	public function getScreeningApplicantId() {
	    return $this->m_intScreeningApplicantId;
	}

	public function setPreviousReviewStatusId( $intStatusId ) {
	    $this->m_intPreviousReviewStatusId = $intStatusId;
	}

	public function getPreviousReviewStatusId() {
	    return $this->m_intPreviousReviewStatusId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
	    parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

	    if( true == isset( $arrmixValues['screening_id'] ) ) $this->setScreeningId( $arrmixValues['screening_id'] );
	    if( true == isset( $arrmixValues['screening_applicant_id'] ) ) $this->setScreeningApplicantId( $arrmixValues['screening_applicant_id'] );
	    if( true == isset( $arrmixValues['previous_review_status_id'] ) ) $this->setPreviousReviewStatusId( $arrmixValues['previous_review_status_id'] );

	}

	public function createTransactionReviewLog( $intCurrentUserId, $objDatabase ) {

	    $strReviewNotes = 'Record with screening transaction id ' . $this->getScreeningTransactionId() . ' marked as ' . CTransactionReviewStatus::getTransactionReviewStatusName( $this->getTransactionReviewStatusId() );
	    $boolIsSuccess = true;

	    $objTransactionreviewLog = new CTransactionReviewLog();
	    $objTransactionreviewLog->setTransactionReviewId( $this->getId() );
	    $objTransactionreviewLog->setTransactionReviewLogTypeId( CTransactionReviewLogType::SYSTEM ); // set this later to Email or Call by discussing with Santosh or Derek or Sachin
	    $objTransactionreviewLog->setOldStatusId( $this->getPreviousReviewStatusId() );
	    $objTransactionreviewLog->setNewStatusId( $this->getTransactionReviewStatusId() );
	    $objTransactionreviewLog->setReviewNotes( $strReviewNotes );
	    $objTransactionreviewLog->setIsArchived( '1' );
	    $objTransactionreviewLog->setUpdatedBy( $intCurrentUserId );
	    $objTransactionreviewLog->setCreatedBy( $intCurrentUserId );

	    if( false == $objTransactionreviewLog->insert( $intCurrentUserId, $objDatabase ) ) {
	        trigger_error( 'Failed to update logs record of transaction review id [' . $this->getId() . ']', E_USER_WARNING );
	        $boolIsSuccess = false;
	        $objDatabase->rollback();
	    }

	    return $boolIsSuccess;
	}

	public function createTransactionReviewLogs( $intCurrentUserId, $intOldTransactionReviewStatusId, $objDatabase ) {

		$strReviewNotes = 'Record marked as ' . CTransactionReviewStatus::getTransactionReviewStatusName( $this->getTransactionReviewStatusId() );

		$boolIsSuccess = true;

		$objTransactionreviewLog = new CTransactionReviewLog();

		$objTransactionreviewLog->setTransactionReviewId( $this->getId() );
		$objTransactionreviewLog->setTransactionReviewLogTypeId( CTransactionReviewLogType::SYSTEM ); // set this later to Email or Call by discussing with Santosh or Derek or Sachin
		$objTransactionreviewLog->setOldStatusId( $intOldTransactionReviewStatusId );
		$objTransactionreviewLog->setNewStatusId( $this->getTransactionReviewStatusId() );
		$objTransactionreviewLog->setReviewNotes( $strReviewNotes );
		$objTransactionreviewLog->setIsArchived( '1' );
		$objTransactionreviewLog->setUpdatedBy( $intCurrentUserId );
		$objTransactionreviewLog->setCreatedBy( $intCurrentUserId );

		if( false == $objTransactionreviewLog->insert( $intCurrentUserId, $objDatabase ) ) {
			trigger_error( 'Failed to update logs record of transaction review id [' . $this->getId() . ']', E_USER_WARNING );
			$boolIsSuccess = false;
			$objDatabase->rollback();
		}

		return $boolIsSuccess;
	}

	public function addTransactionReviewLog( $strReviewNotes, $intCurrentUserId, $objDatabase, $intOldTransactionReviewStatusId ) {

		$objTransactionreviewLog = new CTransactionReviewLog();
		$objTransactionreviewLog->setTransactionReviewId( $this->getId() );
		$objTransactionreviewLog->setTransactionReviewLogTypeId( CTransactionReviewLogType::SYSTEM ); // set this later to Email or Call by discussing with Santosh or Derek or Sachin
		$objTransactionreviewLog->setOldStatusId( $intOldTransactionReviewStatusId );
		$objTransactionreviewLog->setNewStatusId( $this->getTransactionReviewStatusId() );
		$objTransactionreviewLog->setReviewNotes( $strReviewNotes );
		$objTransactionreviewLog->setIsArchived( '1' );
		$objTransactionreviewLog->setUpdatedBy( $intCurrentUserId );
		$objTransactionreviewLog->setCreatedBy( $intCurrentUserId );

		if( false == $objTransactionreviewLog->insert( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

}
?>