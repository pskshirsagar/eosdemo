<?php

class CScreenTypeConfigPreferenceDataProviderRule extends CBaseScreenTypeConfigPreferenceDataProviderRule {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreenTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningConfigPreferenceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningDataProviderTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningSearchActionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>