<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDataSources
 * Do not add any new functions to this class.
 */

class CDataSources extends CBaseDataSources {

	public static function fetchPublishedDataSources( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						data_sources
					WHERE
						is_published = true
					ORDER BY id';
		return self::fetchDataSources( $strSql, $objDatabase );
	}

}
?>