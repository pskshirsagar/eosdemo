<?php

class CScreeningAccount extends CBaseScreeningAccount {

	protected $m_strScreeningRateTypeName;

    /**
     * Get Functions
     */

    public function getScreeningRateTypeName() {
    	return $this->m_strScreeningRateTypeName;
    }

    /**
     * Set Functions
     */

    public function setScreeningRateTypeName( $strScreeningRateTypeName ) {
    	$this->m_strScreeningRateTypeName = $strScreeningRateTypeName;
    }

    public function valName() {
        $boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Account Name Is Required.' ) );
        }

        return $boolIsValid;
    }

    public function valScreenRateTypeId() {
        $boolIsValid = true;

        if( false == $this->getScreenRateTypeId() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'screen_rate_type_id', 'Account Pricing Type Is Required.' ) );
        }

        return $boolIsValid;
    }

    public function valPackageRate() {
        $boolIsValid = true;

        if( true == is_null( $this->getPackageRate() ) || 0 >= $this->getPackageRate() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'package_rate', 'Package Rate Is Required.' ) );
        }

        return $boolIsValid;
    }

	public function valCertificationDate() {
		$boolIsValid = true;
		$strCertificationDate = $this->getCertificationDate();

		if( false == is_null( $strCertificationDate ) ) {
			$strCertificationDateValidCode = CValidation::checkDate( $strCertificationDate );

			if( 1 !== $strCertificationDateValidCode ) {
				$boolIsValid = false;
				$this->setCertificationDate( NULL );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'certification_date', 'Certification ' . \Psi\CStringService::singleton()->strtolower( $strCertificationDateValidCode ) ) );
			} else {
				if( strtotime( date( 'm/d/Y' ) ) < strtotime( $strCertificationDate ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'certification_date', 'Certification Date Cannot Be Of Future.' ) );
					$boolIsValid = false;
				}
			}

		}

		return $boolIsValid;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName();
            	$boolIsValid &= $this->valScreenRateTypeId();
            	$boolIsValid &= $this->valCertificationDate();
         	   	break;

            case VALIDATE_DELETE:
           		break;

            default:
            	// added default
        }

        return $boolIsValid;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['screening_rate_type_name'] ) )
    		$this->setScreeningRateTypeName( $arrmixValues['screening_rate_type_name'] );
    	return;
    }

}
?>