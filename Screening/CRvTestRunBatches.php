<?php

class CRvTestRunBatches extends CBaseRvTestRunBatches {

	public static function fetchCustomRvTestRunBatchesByCid( $intCid, $objDatabase ) {
		if( !valId( $intCid ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						rv_test_run_batches
					WHERE
						cid = ' . ( int ) $intCid . '
					ORDER BY id DESC	
					LIMIT 100';

		return fetchData( $strSql, $objDatabase );
	}

}
?>