<?php

class CDelinquentEventReason extends CBaseDelinquentEventReason {

	const APPLIED_USING_FALSE_IDENTIFICATION			= 1;
	const APPLIED_UNDER_AN_ALIAS						= 2;
	const PROVIDED_FALSE_OR_INCORRECT_DATE_OF_BIRTH		= 3;
	const PROVIDED_FALSE_PAY_STUBS						= 4;
	const APPLIED_WITH_FALSE_MONTHLY_INCOME_AMOUNT		= 5;
	const PROVIDED_FALSE_EMPLOYEMENT_DOCUMENTATION		= 6;
	const PROVIDED_FALSE_EMPLOYER						= 7;
	const PAID_USING_FRAUDULENT_METHOD					= 8;
	const CONTRACTUAL_AGREEMENT							= 9;
	const MOVED_OUT_WITH_BALANCE						= 10;
	const EVICTION 										= 11;
	const SKIPS											= 12;
	const LEASE_VIOLATION                               = 13;
	const NON_RENEWAL                                   = 14;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getDelinquentEventReasonName( $intDelinquentEventReasonId ) {

		$strDelinquentEventReasonName = '';

		switch( $intDelinquentEventReasonId ) {
			case self::APPLIED_USING_FALSE_IDENTIFICATION:
				$strDelinquentEventReasonName = __( 'Applied using false identification' );
				break;

			case self::APPLIED_UNDER_AN_ALIAS:
				$strDelinquentEventReasonName = __( 'Applied under an alias' );
				break;

			case self::PROVIDED_FALSE_OR_INCORRECT_DATE_OF_BIRTH:
				$strDelinquentEventReasonName = __( 'Provided false or incorrect date of birth' );
				break;

			case self::PROVIDED_FALSE_PAY_STUBS:
				$strDelinquentEventReasonName = __( 'Provided false pay stubs' );
				break;

			case self::APPLIED_WITH_FALSE_MONTHLY_INCOME_AMOUNT:
				$strDelinquentEventReasonName = __( 'Applied with false monthly income amount' );
				break;

			case self::PROVIDED_FALSE_EMPLOYEMENT_DOCUMENTATION:
				$strDelinquentEventReasonName = __( 'Provided false employment documentation' );
				break;

			case self::PROVIDED_FALSE_EMPLOYER:
				$strDelinquentEventReasonName = __( 'Provided false employer' );
				break;

			case self::PAID_USING_FRAUDULENT_METHOD:
				$strDelinquentEventReasonName = __( 'Paid using fraudulent method' );
				break;

			case self::CONTRACTUAL_AGREEMENT:
				$strDelinquentEventReasonName = __( 'Contractual Agreement' );
				break;

            case self::MOVED_OUT_WITH_BALANCE:
                $strDelinquentEventReasonName = 'Moved Out With Balance';
                break;

            case self::EVICTION:
                $strDelinquentEventReasonName = 'Moved Out - Evicted';
                break;

            case self::SKIPS:
                $strDelinquentEventReasonName = 'Moved out - Skipped';
                break;

			case self::LEASE_VIOLATION:
				$strDelinquentEventReasonName = __( 'Lease Violation' );
				break;

			case self::NON_RENEWAL:
				$strDelinquentEventReasonName = __( 'Non Renewal' );
				break;

			default:
				NULL;
		}

		return $strDelinquentEventReasonName;
	}

}
?>