<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CConsumerDocs
 * Do not add any new functions to this class.
 */

class CConsumerDocs extends CBaseConsumerDocs {

	public static function fetchConsumerDocsByDisputeLogId( $intDisputeLogId, $objDatabase ) {
		return self::fetchConsumerDocs( sprintf( 'SELECT * FROM consumer_docs WHERE dispute_log_id = %d', ( int ) $intDisputeLogId ), $objDatabase );
	}

}
?>