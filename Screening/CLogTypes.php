<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CLogTypes
 * Do not add any new functions to this class.
 */

class CLogTypes extends CBaseLogTypes {

	public static function fetchLogTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CLogType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchLogType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CLogType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>