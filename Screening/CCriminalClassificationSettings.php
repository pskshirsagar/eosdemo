<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CCriminalClassificationSettings
 * Do not add any new functions to this class.
 */

class CCriminalClassificationSettings extends CBaseCriminalClassificationSettings {

	public static function fetchActiveCriminalClassificationSettingsByScreeningPackageIdByCid( $intPackageId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ccs.*,
						spcs1.screening_recommendation_type_id as felony_screening_recommendation_type_id,
						spcs2.screening_recommendation_type_id as misdemeanor_screening_recommendation_type_id
					FROM
						criminal_classification_settings ccs
						LEFT JOIN screening_package_condition_sets spcs1 ON spcs1.id = ccs.felony_screening_package_condition_set_id AND ccs.cid = spcs1.cid
						LEFT JOIN screening_package_condition_sets spcs2 ON spcs2.id = ccs.misdemeanor_screening_package_condition_set_id AND ccs.cid = spcs2.cid
					WHERE
						ccs.screening_package_id = ' . ( int ) $intPackageId . '
						AND ccs.cid = ' . ( int ) $intCid . '
						AND ccs.is_active = 1';

		return self::fetchCriminalClassificationSettings( $strSql, $objDatabase );
	}

	public static function fetchCriminalClassificationSettingsByScreeningPackageIdByCid( $intPackageId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						criminal_classification_settings
					WHERE
						screening_package_id = ' . ( int ) $intPackageId . '
						AND cid = ' . ( int ) $intCid . '
						AND is_active = 1';

		return self::fetchCriminalClassificationSettings( $strSql, $objDatabase );
	}

	public static function archiveCriminalClassificationSettings( $intPackageId, $intUserId, $objDatabase ) {

		$strSql 		= '';
		$boolIsSuccess 	= true;

		$strSql = 'UPDATE
				       criminal_classification_settings
				   SET
				       is_active = 0,
				       updated_by = ' . ( int ) $intUserId . ',
				       updated_on = NOW ( )
				   WHERE
				       screening_package_id = ' . ( int ) $intPackageId;

		if( false == valStr( $strSql ) )  return;

		$objDatabase->begin();

		fetchData( $strSql, $objDatabase );

		$objDatabase->commit();

		return $boolIsSuccess;
	}

}
?>