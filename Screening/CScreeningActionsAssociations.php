<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningActionsAssociations
 * Do not add any new functions to this class.
 */

class CScreeningActionsAssociations extends CBaseScreeningActionsAssociations {

	public static function fetchPublishedScreeningActionsAssociationsByScreeningSearchActionId( $intScreeningSearchActionId, $intScreeningDataProviderTypeId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						screening_actions_associations
					WHERE
						screening_search_action_id = ' . ( int ) $intScreeningSearchActionId . '
						AND screening_data_provider_type_id	 = ' . ( int ) $intScreeningDataProviderTypeId . '
						AND is_active = 1';

		return parent::fetchScreeningActionsAssociations( $strSql, $objDatabase );

	}
}
?>