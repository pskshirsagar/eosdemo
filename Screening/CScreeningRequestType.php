<?php

class CScreeningRequestType extends CBaseScreeningRequestType {

	const UNUSED 						= 1;
	const HOLD							= 2;
	const CHECK_STATUS					= 3;
	const RESUBMIT						= 4; // create new entry in screening_applicants table altogether
	const COMPLETE						= 5;
	const VIEW_REPORT					= 6;
	const OPEN							= 7;
	const CANCELLED						= 8;
    const SCREENING_REQUEST_TYPE_VIEW   = 9;
    const EVALUATE_RESULT 				= 10; // it will evaluate screening result against screening package criterias in case of check status or re processing
    const RE_PROCESSING					= 11; // it will process only unprocessed transactions ( won't create new applicant record )
    const INTERNAL_PROCESSING			= 12; // it will check screen type and according to that it will calculate, RTI, add NO SSN flag setting after evaluation

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>