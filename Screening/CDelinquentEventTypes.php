<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDelinquentEventTypes
 * Do not add any new functions to this class.
 */

class CDelinquentEventTypes extends CBaseDelinquentEventTypes {

	public static function fetchDelinquentEventTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDelinquentEventType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDelinquentEventType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDelinquentEventType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedDelinquentEventTypes( $objDatabase ) {

		$strSql = 'SELECT
    					id,
    					name,
    					description
    				FROM
    					delinquent_event_types
    				WHERE 
    				    is_published = 1
    				ORDER BY
    					order_num';

		return parent::fetchDelinquentEventTypes( $strSql, $objDatabase );
	}

	public static function fetchPublishedDelinquentEventTypesByIds( $arrintDelinquentEventTypeIds, $objDatabase ) {

		if( false == valArr( $arrintDelinquentEventTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
    					id,
    					name,
    					description
    				FROM
    					delinquent_event_types
    				WHERE
    					id IN ( ' . implode( ',', $arrintDelinquentEventTypeIds ) . ' )
    					and is_published = 1
    				ORDER BY
    					order_num';

		return parent::fetchDelinquentEventTypes( $strSql, $objDatabase );

	}

}

?>