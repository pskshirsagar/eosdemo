<?php

class CDisputeLog extends CBaseDisputeLog {

	public function valNotes() {
		$boolIsValid = true;
		if( true == is_null( $this->getNotes() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'assigned_to', 'Note is required.' ) );
		}

		return $boolIsValid;
	}

	public function valAssignedTo() {
		$boolIsValid = true;

		if( true == is_null( $this->getAssignedTo() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'assigned_to', 'Assigned to is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valAssignedTo();
				$boolIsValid &= $this->valNotes();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function validateDisputeLog( $strConsumerInfo, $strKey, $objConsumer, $objConsumerDispute, $intUserId, $intAssignedTo, $objLatestDisputeLog ) {
		$boolIsValid = true;

		$this->setConsumerId( $objConsumer->getId() );
		$this->setConsumerDisputeId( $objConsumerDispute->getId() );
		$strNewData = ( strlen( $strConsumerInfo ) > 500 ) ? \Psi\CStringService::singleton()->substr( $strConsumerInfo, 0, 500 ): $strConsumerInfo;
		$this->setNewData( $strNewData );

		$this->setReferenceNumber( $objConsumer->getReferenceNumber() );
		$this->setAssignedBy( $intUserId );
		if( true == isset( $intAssignedTo ) ) {
			$this->setAssignedTo( $intAssignedTo );
		} else {
			$this->setAssignedTo( $objLatestDisputeLog->getAssignedTo() );
		}
		switch( $strKey ) {
			case 'user':
				$this->setLogTypeId( CLogType::ASSIGNED_TO );
				$this->setOldData( $objConsumerDispute->getAssignedTo() );
				$this->setAssignedTo( $strConsumerInfo );
				$this->setNotes( 'Dispute Assigned To ' );
				break;

			case 'dispute_type':
				$this->setLogTypeId( CLogType::VALUE_UPDATED );
				$this->setOldData( $objConsumerDispute->getDisputeTypeId() );

				$this->setNotes( 'Dispute Type updated to ' . CDisputeType::getDisputeTypeText( $strConsumerInfo ) );
				break;

			case 'dispute_type_reason':
				$this->setLogTypeId( CLogType::VALUE_UPDATED );
				$this->setOldData( $objConsumerDispute->getDisputeTypeReasonId() );
				$this->setNotes( 'Dispute Reason updated to ' . CDisputeTypeReason::getDisputeTypeReasonText( $strConsumerInfo ) );
				break;

			case 'dispute_status_type':
				$this->setLogTypeId( CLogType::STATUS_CHANGE );
				$this->setOldData( $objConsumerDispute->getDisputeStatusTypeId() );
				$this->setNotes( 'Dispute Status Type updated to ' . CDisputeStatusType::getDisputeStatusTypeText( $strConsumerInfo ) );
				break;

			case 'report_id':
				$this->setLogTypeId( CLogType::VALUE_UPDATED );
				$this->setOldData( $objConsumerDispute->getProviderReferenceNumber() );
				$this->setNotes( 'Provider Reference Number updated to ' . $strConsumerInfo );
				break;

			case 'firstname':
				$this->setLogTypeId( CLogType::VALUE_UPDATED );
				$this->setOldData( $objConsumer->getFirstName() );
				$this->setNotes( 'Consumer First Name  updated to ' . $strConsumerInfo );
				break;

			case 'lastname':
				$this->setLogTypeId( CLogType::VALUE_UPDATED );
				$this->setOldData( $objConsumer->getLastName() );
				$this->setNotes( 'Consumer Last Name  updated to ' . $strConsumerInfo );
				break;

			case 'email':
				$this->setLogTypeId( CLogType::VALUE_UPDATED );
				$this->setOldData( $objConsumer->getEmailAddress() );
				$this->setNotes( 'Consumer Email Address  updated to ' . $strConsumerInfo );
				break;

			case 'phone':
				$this->setLogTypeId( CLogType::VALUE_UPDATED );
				$this->setOldData( $objConsumer->getPhoneNumber() );
				$this->setNotes( 'Consumer Phone Number updated to ' . $strConsumerInfo );
				break;

			case 'address_line':
				$this->setLogTypeId( CLogType::VALUE_UPDATED );
				$this->setOldData( $objConsumer->getAddressLine() );
				$this->setNotes( 'Consumer Address Line updated to ' . $strConsumerInfo );
				break;

			case 'city':
				$this->setLogTypeId( CLogType::VALUE_UPDATED );
				$this->setOldData( $objConsumer->getCity() );
				$this->setNotes( 'Consumer City updated to ' . $strConsumerInfo );
				break;

			case 'state':
				$this->setLogTypeId( CLogType::VALUE_UPDATED );
				$this->setOldData( $objConsumer->getState() );
				$this->setNotes( 'Consumer State updated to ' . $strConsumerInfo );
				break;

			case 'zipcode':
				$this->setLogTypeId( CLogType::VALUE_UPDATED );
				$this->setOldData( $objConsumer->getZipcode() );
				$this->setNotes( 'Consumer Zipcode updated to ' . $strConsumerInfo );
				break;

			case 'note':
				$this->setLogTypeId( CLogType::CONTACT_NOTES );
				$this->setNotes( \Psi\CStringService::singleton()->htmlspecialchars( $strConsumerInfo ) );
				break;

			case 'contact_method_type_id':
				$this->setLogTypeId( CLogType::VALUE_UPDATED );
				$this->setOldData( $objConsumer->getAddressLine() );
				$this->setNotes( 'Preferred Method of Contact updated to ' . CContactMethodType::getTypeName( $strConsumerInfo ) );
				break;

			case 'data_source_id':
				$this->setLogTypeId( CLogType::VALUE_UPDATED );
				$this->setOldData( $objConsumerDispute->getDataSourceId() );
				$this->setNotes( 'Provider updated to ' . CDataSource::getDataSourceName( $strConsumerInfo ) );
				break;

			default:
				$boolIsValid	&= $this->validate( VALIDATE_INSERT );
				break;
		}

		return $boolIsValid;
	}

}
?>