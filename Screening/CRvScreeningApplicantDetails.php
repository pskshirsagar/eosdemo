<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CRvScreeningApplicantDetails
 * Do not add any new functions to this class.
 */

class CRvScreeningApplicantDetails extends CBaseRvScreeningApplicantDetails {

    public static function fetchNonTransferedRvScreeningApplicantDetails( $objDatabase ) {

        $strSql = 'SELECT
						*
					FROM
						rv_screening_applicant_details rsad
					WHERE
						rsad.is_transfered = false';

        return parent::fetchRvScreeningApplicantDetails( $strSql, $objDatabase );
    }

}
?>