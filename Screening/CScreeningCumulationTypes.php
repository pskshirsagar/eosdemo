<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCumulationTypes
 * Do not add any new functions to this class.
 */

class CScreeningCumulationTypes extends CBaseScreeningCumulationTypes {

    public static function fetchScreeningCumulationTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CScreeningCumulationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchScreeningCumulationType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CScreeningCumulationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchAllCumulationTypes( $objDatabase ) {
    	return self::fetchScreeningCumulationTypes( 'SELECT * FROM screening_cumulation_types', $objDatabase );
    }

    public static function fetchAllPublishedCumulationTypes( $objDatabase ) {
    	return self::fetchScreeningCumulationTypes( 'SELECT * FROM screening_cumulation_types WHERE is_published = 1', $objDatabase );
    }
}
?>