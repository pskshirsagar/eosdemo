<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningTradelineTypes
 * Do not add any new functions to this class.
 */

class CScreeningTradelineTypes extends CBaseScreeningTradelineTypes {

    public static function fetchScreeningTradelineTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CScreeningTradelineType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchScreeningTradelineType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScreeningTradelineType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>