<?php

class CScreeningTransactionBatch extends CBaseScreeningTransactionBatch {

    public function __construct() {
        parent::__construct();

        return;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	public function fetchTotalScreeningAmountAndCount( $objDatabase ) {
		$arrintCountTotalScreeningTransactions = CScreeningTransactions::fetchTotalScreeningTransactionsByScreeningBatchIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		return [
			'total_count' => $arrintCountTotalScreeningTransactions['total_count'] ?? 0,
			'total_amount' => $arrintCountTotalScreeningTransactions['total_amount'] ?? 0,
		];
	}

}
?>