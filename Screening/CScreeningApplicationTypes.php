<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningApplicationTypes
 * Do not add any new functions to this class.
 */

class CScreeningApplicationTypes extends CBaseScreeningApplicationTypes {

	public static function fetchScreeningApplicationTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningApplicationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningApplicationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningApplicationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>