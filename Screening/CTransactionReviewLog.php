<?php

class CTransactionReviewLog extends CBaseTransactionReviewLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionReviewId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionReviewLogTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsArchived() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewNotes() {
		$boolIsValid = true;

		if( true == is_null( $this->getReviewNotes() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cost', 'Review notes are required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valReviewNotes();
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>