<?php

class CScreeningApplicantType extends CBaseScreeningApplicantType {

	const LEASE 				  = 1;
	const NONLEASE 				  = 2;
	const GUARANTOR 			  = 3;
	const STUDENT 				  = 4;
	const AFFORDABLE   			  = 5;
	const CORPORATE   			  = 6;
	const VENDOR_ACCESS_BUSINESS  = 7;
	const VENDOR_ACCESS_CRIMINAL  = 8;
	const CORPORATE_VERIFICATION  = 9;

	public static $c_arrintExcludeScreeningApplicantTypeIds = array(
		self::STUDENT,
		self::AFFORDABLE
	);

	public static $c_arrintVAScreeningApplicantTypeIds = array(
	 self::VENDOR_ACCESS_BUSINESS,
	 self::VENDOR_ACCESS_CRIMINAL
	);

	public static $c_arrintVendorApplicantTypeIds = array(
		self::VENDOR_ACCESS_BUSINESS,
		self::VENDOR_ACCESS_CRIMINAL
	);

    public function validate( $strAction ) {

        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public static function getScreeningApplicantTypeName( $intScreeningApplicantTypeId ) {

        $strScreeningApplicantTypeName = '';

        switch( $intScreeningApplicantTypeId ) {
            case self::LEASE:
                $strScreeningApplicantTypeName = 'Lease Signer';
                break;

            case self::NONLEASE:
                $strScreeningApplicantTypeName = 'Non Lease Signer';
                break;

            case self::GUARANTOR:
                $strScreeningApplicantTypeName = 'Guarantor';
                break;

            default:
                // default case
                break;
        }

        return $strScreeningApplicantTypeName;
    }

}
?>