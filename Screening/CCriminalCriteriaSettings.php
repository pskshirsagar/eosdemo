<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CCriminalCriteriaSettings
 * Do not add any new functions to this class.
 */

class CCriminalCriteriaSettings extends CBaseCriminalCriteriaSettings {

	public static function fetchActiveCriminalCriteriaSettingsByScreeningCriteriaIdByCid( $intScreeningCriteriaId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						criminal_criteria_settings
					WHERE
						screening_criteria_id = ' . ( int ) $intScreeningCriteriaId . '
						AND cid = ' . ( int ) $intCid . '
						AND is_active = 1';

		return self::fetchCriminalCriteriaSettings( $strSql, $objDatabase );
	}

	public static function fetchActiveCriminalCriteriaSettingsForScreeningByScreeningCriteriaIdByCid( $intScreeningCriteriaId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ccs.*,
						scs1.screening_recommendation_type_id as felony_screening_recommendation_type_id,
						scs2.screening_recommendation_type_id as misdemeanor_screening_recommendation_type_id
					FROM
						criminal_criteria_settings ccs
						LEFT JOIN screening_condition_sets scs1 ON scs1.id = ccs.felony_screening_package_condition_set_id AND ccs.cid = scs1.cid
						LEFT JOIN screening_condition_sets scs2 ON scs2.id = ccs.misdemeanor_screening_package_condition_set_id AND ccs.cid = scs2.cid
					WHERE
						ccs.screening_criteria_id = ' . ( int ) $intScreeningCriteriaId . '
						AND ccs.cid = ' . ( int ) $intCid;

		return self::fetchCriminalCriteriaSettings( $strSql, $objDatabase );
	}

	public static function fetchActiveCriminalCriteriaSettingsByScreeningPackageSettings( $intScreeningPackageId, $objDatabase ) {
		$strSql = 'SELECT
						ccs.*,
						scs1.screening_recommendation_type_id as felony_screening_recommendation_type_id,
						scs2.screening_recommendation_type_id as misdemeanor_screening_recommendation_type_id
					FROM
						criminal_criteria_settings ccs
						JOIN screening_package_screen_type_associations spcta ON spcta.screening_criteria_id = ccs.screening_criteria_id AND spcta.screen_type_id = ' . CScreenType::CRIMINAL . '
						LEFT JOIN screening_condition_sets scs1 ON scs1.id = ccs.felony_screening_package_condition_set_id
						LEFT JOIN screening_condition_sets scs2 ON scs2.id = ccs.misdemeanor_screening_package_condition_set_id
					WHERE
						spcta.screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND spcta.is_published = 1
						AND ccs.is_active = 1';

		return parent::fetchCriminalCriteriaSettings( $strSql, $objDatabase );
	}

	public static function fetchActiveCriminalCriteriaSettingsByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {
		$strSql = 'SELECT
               *
            FROM
               criminal_criteria_settings
            WHERE
               screening_criteria_id = ' . ( int ) $intScreeningCriteriaId . '
               AND is_active = 1';

		return parent::fetchCriminalCriteriaSettings( $strSql, $objDatabase );
	}

	public static function fetchActiveCriminalCriteriaSettingsByScreeningCriteriaIdsByScreeningConditionSetId( $arrintScreeningCriteriaIds, $intScreeningConditionSetId, $objDatabase ) {
		if( false == valArr( $arrintScreeningCriteriaIds ) ) return;

		$strSql = 'SELECT
						*
					FROM
						criminal_criteria_settings
					WHERE
						screening_criteria_id IN( ' . sqlIntImplode( $arrintScreeningCriteriaIds ) . ' )
						AND ( felony_screening_package_condition_set_id = ' . ( int ) $intScreeningConditionSetId . ' OR misdemeanor_screening_package_condition_set_id = ' . ( int ) $intScreeningConditionSetId . ')';

		return parent::fetchCriminalCriteriaSettings( $strSql, $objDatabase );
	}

	public static function fetchSubcategoryUsedCriteriaSettingsByCriminalClassificationTypeIdBySubtypeIdsByCid( $intCriminalClassificationTypeId, $arrintSubTypeIds, $intCid, $objDatabase ) {
		$strSql = '
					SELECT 
						ccs.client_criminal_classification_subtype_id
					FROM
						criminal_criteria_settings ccs 
						JOIN screening_criterias sc ON ( ccs.screening_criteria_id = sc.id AND ccs.cid = sc.cid AND sc.is_published = TRUE ) 
					WHERE
						ccs.criminal_classification_type_id = ' . ( int ) $intCriminalClassificationTypeId . '
						AND ccs.cid = ' . ( int ) $intCid . '
						AND ccs.is_active = 1
						AND ccs.client_criminal_classification_subtype_id IN ( ' . sqlIntImplode( $arrintSubTypeIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveCriminalCriteriaSettingsByScreeningPackageIdsByCid( $arrintPackageIds, $intCid, $objDatabase ) {

		if( false == ( $arrintPackageIds = getIntValuesFromArr( $arrintPackageIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						spcta.screening_package_id,
						spcta.screen_type_id,
						ccs.felony_screening_package_condition_set_id,
						ccs.misdemeanor_screening_package_condition_set_id,
						cst.type_name,
						cst.id as classification_type_id,
						sf1.description as felony_description,
                        sf2.description as misdemeanor_description,
						scs1.screening_recommendation_type_id as felony_screening_recommendation_type_id,
						scs2.screening_recommendation_type_id as misdemeanor_screening_recommendation_type_id,
						scs1.condition_name as felony_condition_name,
						scs2.condition_name as misdemeanor_condition_name,
						sf1.order_num as felony_order_num,
                        sf2.order_num as misdemeanor_order_num
					FROM
						criminal_criteria_settings ccs
						JOIN screening_package_screen_type_associations spcta ON ( ccs.screening_criteria_id = spcta.screening_criteria_id AND spcta.screen_type_id IN( ' . CScreenType::CRIMINAL_CATEGORIZATION . ', ' . CScreenType::CRIMINAL . ' ) )
						JOIN screening_packages sp ON ( sp.id = spcta.screening_package_id )
						JOIN criminal_classification_types cst ON ( ccs.criminal_classification_type_id = cst.id )
						LEFT JOIN screening_filters sf1 ON ( sf1.screening_filter_type_id  = ' . CScreeningFilterType::TIMELINE_FILTER . ' AND ccs.felony_years IS NOT NULL AND ccs.felony_years = sf1.filter_value::NUMERIC )
                        LEFT JOIN screening_filters sf2 ON ( sf2.screening_filter_type_id  = ' . CScreeningFilterType::TIMELINE_FILTER . ' AND ccs.misdemeanor_years IS NOT NULL AND ccs.misdemeanor_years = sf2.filter_value::NUMERIC )
						LEFT JOIN screening_condition_sets scs1 ON ( scs1.id = ccs.felony_screening_package_condition_set_id AND ccs.cid = scs1.cid )
						LEFT JOIN screening_condition_sets scs2 ON ( scs2.id = ccs.misdemeanor_screening_package_condition_set_id AND ccs.cid = scs2.cid )
					WHERE
						spcta.screening_package_id IN ( ' . sqlIntImplode( $arrintPackageIds ) . ' )
						AND sp.cid = ' . ( int ) $intCid . '
						AND sp.is_active IS TRUE
						AND sp.is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveCriminalCriteriaSettingsForScreeningByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {

		$strSql = 'SELECT
						ccs.*,
						scs1.screening_recommendation_type_id as felony_screening_recommendation_type_id,
						scs2.screening_recommendation_type_id as misdemeanor_screening_recommendation_type_id
					FROM
						criminal_criteria_settings ccs
						LEFT JOIN screening_condition_sets scs1 ON scs1.id = ccs.felony_screening_package_condition_set_id
						LEFT JOIN screening_condition_sets scs2 ON scs2.id = ccs.misdemeanor_screening_package_condition_set_id
					WHERE
						ccs.screening_criteria_id = ' . ( int ) $intScreeningCriteriaId;

		return self::fetchCriminalCriteriaSettings( $strSql, $objDatabase );
	}

}
?>