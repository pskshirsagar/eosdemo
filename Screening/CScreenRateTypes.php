<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreenRateTypes
 * Do not add any new functions to this class.
 */

class CScreenRateTypes extends CBaseScreenRateTypes {

    public static function fetchScreenRateTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CScreenRateType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchScreenRateType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CScreenRateType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchPublishedScreenRateTypes( $objDatabase ) {

    	$strSql = 'SELECT
    					*
    			   FROM
    					screen_rate_types
    			   WHERE
    					is_published = 1
    				';

    	return parent::fetchScreenRateTypes( $strSql, $objDatabase );
    }

}
?>