<?php

class CScreeningCompanyPreference extends CBaseScreeningCompanyPreference {

	protected $m_intDataTypeId;

	public function setDataTypeId( $intDataTypeId ) {
		$this->m_intDataTypeId = $intDataTypeId;
	}

	public function getDataTypeId() {
		return $this->m_intDataTypeId;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningCompanyPreferenceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		$this->setDataTypeId( getArrayElementByKey( $this->getScreeningCompanyPreferenceTypeId(), CScreeningCompanyPreferenceType::$c_arrmixPreferenceDataTypeAssociations ) );

		return;
	}

}
?>