<?php

class CScreeningCriteriaConfigPreferenceTypeFilter extends CBaseScreeningCriteriaConfigPreferenceTypeFilter {

	protected $m_intScreeningConfigPreferenceFilterValue;
	protected $m_intScreeningFilterTypeId;
	protected $m_intScreeningFilterId;
	protected $m_strExcludeFrom;
	protected $m_strExcludeTo;

	public function setScreeningFilterId( $intScreeningFilterId ) {
		$this->m_intScreeningFilterId = $intScreeningFilterId;
	}

	public function getScreeningFilterId() {
		return $this->m_intScreeningFilterId;
	}

	public function setScreeningConfigPreferenceFilterValue( $intScreeningConfigPreferenceFilterValue ) {
		$this->m_intScreeningConfigPreferenceFilterValue = $intScreeningConfigPreferenceFilterValue;
	}

	public function getScreeningConfigPreferenceFilterValue() {
		return $this->m_intScreeningConfigPreferenceFilterValue;
	}

	public function setScreeningFilterTypeId( $intScreeningFilterTypeId ) {
		$this->m_intScreeningFilterTypeId = $intScreeningFilterTypeId;
	}

	public function getScreeningFilterTypeId() {
		return $this->m_intScreeningFilterTypeId;
	}

	public function setExcludeFrom( $strExcludeFrom ) {
		$this->m_strExcludeFrom = $strExcludeFrom;
	}

	public function getExcludeFrom() {
		return $this->m_strExcludeFrom;
	}

	public function setExcludeTo( $strExcludeTo ) {
		$this->m_strExcludeTo = $strExcludeTo;
	}

	public function getExcludeTo() {
		return $this->m_strExcludeTo;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );
		if( isset( $arrValues['screening_config_preference_filter_value'] ) ) $this->setScreeningConfigPreferenceFilterValue( $arrValues['screening_config_preference_filter_value'] );
		if( isset( $arrValues['screening_filter_type_id'] ) ) $this->setScreeningFilterTypeId( $arrValues['screening_filter_type_id'] );
		if( isset( $arrValues['screening_filter_id'] ) ) $this->setScreeningFilterId( $arrValues['screening_filter_id'] );
		if( isset( $arrValues['exclude_from'] ) ) $this->setExcludeFrom( $arrValues['exclude_from'] );
		if( isset( $arrValues['exclude_to'] ) ) $this->setExcludeTo( $arrValues['exclude_to'] );
		return;
	}

}
?>