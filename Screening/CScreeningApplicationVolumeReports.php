<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningApplicationVolumeReports
 * Do not add any new functions to this class.
 */

class CScreeningApplicationVolumeReports extends CBaseScreeningApplicationVolumeReports {

	public static function fetchScreeningApplicationVolumeReports( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CScreeningApplicationVolumeReport::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningApplicationVolumeReport( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CScreeningApplicationVolumeReport::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningApplicationVolumeReportByScreeningRecordSearchFilter( $objCScreeningReportSearchFilter, $objDatabase ) {

		if( false == valObj( $objCScreeningReportSearchFilter, CScreeningReportSearchFilter::class ) ) {
			return NULL;
		}

		$strWhere = '';
		if( false == $objCScreeningReportSearchFilter->getIsPropertySearchFilter() ) {
			if( true == valArr( $objCScreeningReportSearchFilter->getCids() ) ) {
				$strWhere   .= ' cid IN( ' . sqlIntImplode( $objCScreeningReportSearchFilter->getCids() ) . ') ';
			} else {
				return NULL;
			}
		} else if( true == valId( $objCScreeningReportSearchFilter->getCid() ) ) {
			$strWhere .= ' cid = ' . ( int ) $objCScreeningReportSearchFilter->getCid();
		} else {
			return NULL;
		}

		if( true == $objCScreeningReportSearchFilter->getIsPropertySearchFilter() && true == valArr( $objCScreeningReportSearchFilter->getPropertyIds() ) ) {
			$strWhere .= ' AND property_id IN (' . sqlIntImplode( $objCScreeningReportSearchFilter->getPropertyIds() ) . ') ';
		}

		if( true == valStr( $objCScreeningReportSearchFilter->getMonthYear() ) ) {
			$strWhere .= ' AND report_month = DATE_TRUNC(\'month\', TIMESTAMP \'' . ( string ) $objCScreeningReportSearchFilter->getMonthYear() . '\') ';
		}

		if( false == $objCScreeningReportSearchFilter->getIsPropertySearchFilter() ) {
			$strFields = 'cid, report_month, sum(total_applications) AS total_applications, sum(revenue) AS revenue, sum(total_applicants) AS total_applicants, sum(total_pass) AS total_pass, sum(total_conditional) AS total_conditional, sum(total_fail) AS total_fail, sum(total_inprogress) AS total_inprogress, sum(total_error) AS total_error, sum(total_fail_override_decision) AS total_fail_override_decision';
			$strGroupBy = 'GROUP BY cid, report_month';
			$strOrderBy = 'cid';
		} else {
			$strFields = '*';
			$strGroupBy = '';
			$strOrderBy = 'cid, property_id';
		}

		$strSql = 'SELECT
						' . $strFields . '
				   FROM 
				       screening_application_volume_reports
				   WHERE 
				        ' . $strWhere . '
				        ' . $strGroupBy . '
				   ORDER BY
				        ' . $strOrderBy . ';';
		return fetchData( $strSql, $objDatabase );
	}

}
?>