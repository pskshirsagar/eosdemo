<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageConditionValueTypes
 * Do not add any new functions to this class.
 */

class CScreeningPackageConditionValueTypes extends CBaseScreeningPackageConditionValueTypes {

	public static function fetchScreeningPackageConditionValueTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningPackageConditionValueType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

    public static function fetchScreeningPackageConditionValueType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScreeningPackageConditionValueType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>