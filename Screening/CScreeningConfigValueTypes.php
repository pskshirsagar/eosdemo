<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConfigValueTypes
 * Do not add any new functions to this class.
 */

class CScreeningConfigValueTypes extends CBaseScreeningConfigValueTypes {

    public static function fetchScreeningConfigValueTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CScreeningConfigValueType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchScreeningConfigValueType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScreeningConfigValueType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>