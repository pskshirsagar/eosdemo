<?php

class CScreeningStatusType extends CBaseScreeningStatusType {

	const APPLICANT_RECORD_STATUS_OPEN 					= 1;
	const APPLICANT_RECORD_STATUS_COMPLETED 			= 2;
	const APPLICANT_RECORD_STATUS_CANCELLED 			= 3;
	const APPLICANT_RECORD_STATUS_ERROR 				= 4;
	const APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED 	= 5;
	const APPLICANT_RECORD_STATUS_MANUALLY_REVIEWED 	= 6;
	const APPLICANT_RECORD_STATUS_DEDUPLICATED			= 7;

	const APPLICANT_RECORD_STATUS_MANUAL_REVIEW			= 8;
	const APPLICANT_RECORD_STATUS_DISPATCH_HOLD			= 9;

	const APPLICANT_RECORD_STATUS_ADMIN_HOLD			= 10;
	const APPLICANT_RECORD_STATUS_ID_VERIFICATION_PENDING = 11;


	public static $c_arrintCancelledStatusTypeIds = array(
	    self::APPLICANT_RECORD_STATUS_CANCELLED,
	    self::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED
	);

    public static $c_arrintCancelledScreeningStatusTypeIds = array( self::APPLICANT_RECORD_STATUS_CANCELLED, self::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED );

    public static $c_arrintScreeningComplatedStatusIds = array(
    	self::APPLICANT_RECORD_STATUS_COMPLETED,
    	self::APPLICANT_RECORD_STATUS_MANUAL_REVIEW
    );

    public static $c_arrintScreeningCancelledStatusIds = array(
    	self::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED,
    	self::APPLICANT_RECORD_STATUS_CANCELLED
    );

    public static $c_arrintInProgressScreeningStatusTypeIds = [
        self::APPLICANT_RECORD_STATUS_OPEN,
        self::APPLICANT_RECORD_STATUS_MANUAL_REVIEW,
        self::APPLICANT_RECORD_STATUS_ID_VERIFICATION_PENDING
    ];

    public static function getScreeningStatusText( $intScreeningStatusTypeId ) {

    	$strScreeningStatus = '';

    	switch( $intScreeningStatusTypeId ) {
    		case self::APPLICANT_RECORD_STATUS_OPEN:
    			$strScreeningStatus = 'In Progress';
    			break;

    		case self::APPLICANT_RECORD_STATUS_COMPLETED:
    			$strScreeningStatus = 'Complete';
    			break;

    		case self::APPLICANT_RECORD_STATUS_ERROR:
    			$strScreeningStatus = 'Error';
    			break;

    		case self::APPLICANT_RECORD_STATUS_MANUAL_REVIEW:
    			$strScreeningStatus = 'Pending Review';
    			break;

			case self::APPLICANT_RECORD_STATUS_CANCELLED:
    			$strScreeningStatus = 'Cancelled';
    			break;

    		default:
    			$strScreeningStatus = '';
    			break;
    	}

    	return $strScreeningStatus;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>