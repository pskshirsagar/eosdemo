<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CCriminalClassificationKeywords
 * Do not add any new functions to this class.
 */

class CCriminalClassificationKeywords extends CBaseCriminalClassificationKeywords {

   public static function fetchAllPublishedCriminalClassificationKeywordsByClassificationTypeId( $intCriminalClassificationTypeId, $objDatabase ) {

   	$strSql = 'SELECT 
					 * 
   	           FROM 
   	                 criminal_classification_keywords 
               WHERE 
   	                 classification_type_id = ' . ( int ) $intCriminalClassificationTypeId . '
   	                 AND is_published = 1';

   	return self::fetchCriminalClassificationKeywords( $strSql, $objDatabase );
   }

}
?>