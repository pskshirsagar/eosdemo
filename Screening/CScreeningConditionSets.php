<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningConditionSets
 * Do not add any new functions to this class.
 */

class CScreeningConditionSets extends CBaseScreeningConditionSets {

	public static function fetchPublishedScreeningConditionSetsByCid( $intCid, $objDatabase, $boolIsIgnoreFailConditionSet = false ) {
		$strWhereCondition = '';

		if( true == $boolIsIgnoreFailConditionSet ) {
			// ignore FAIL condition set
			$strWhereCondition = ' AND screening_recommendation_type_id <> ' . CScreeningRecommendationType::FAIL;
		}

		$strSql = '	SELECT
						*
					FROM
						screening_condition_sets
					WHERE
						cid = ' . ( int ) $intCid .
		          $strWhereCondition . '
						AND is_published = true
					ORDER By
						evaluation_order DESC';

		return parent::fetchScreeningConditionSets( $strSql, $objDatabase );
	}

	public static function fetchScreeningConditionSetByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningConditionSet( sprintf( 'SELECT * FROM screening_condition_sets WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPublishedScreeningConditionSetsByCidWithEvaluationOrderOnly( $intCid, $objDatabase, $boolIsMaxEvaluationOrder = false, $intScreeningConditionSetId = NULL ) {

		$strSelect  = ' id, evaluation_order ';
		$strOrderBy = ' ORDER BY evaluation_order DESC ';

		if( true == $boolIsMaxEvaluationOrder ) {
			$strSelect = ' MAX( evaluation_order ) as evaluation_order';
			$strOrderBy = '';
		}

		$strWhere = ( 0 != ( int ) $intScreeningConditionSetId ) ? ' AND id <> ' . ( int ) $intScreeningConditionSetId . ' ' : '';
		$strSql = 'SELECT 
						' . $strSelect . '
					FROM
						screening_condition_sets
					WHERE
						cid = ' . ( int ) $intCid . '
						AND screening_recommendation_type_id NOT IN ( ' . CScreeningRecommendationType::PASS . ', ' . CScreeningRecommendationType::FAIL . ' )
						AND is_published = true 
						AND evaluation_order IS NOT NULL ' .
		          $strWhere .
		          $strOrderBy;

		$arrmixResultData = fetchData( $strSql, $objDatabase );

		if( true == $boolIsMaxEvaluationOrder ) {
			return getArrayElementByKey( 'evaluation_order', getArrayElementByKey( 0, $arrmixResultData ) );
		}

		$arrintScreeningConditionSetIds = [];

		foreach( $arrmixResultData as $arrmixScreeningConditionSet ) {
			$arrintScreeningConditionSetIds[$arrmixScreeningConditionSet['id']] = $arrmixScreeningConditionSet['evaluation_order'];
		}

		return $arrintScreeningConditionSetIds;
	}

	public static function fetchConditionSetsByConditionNameByCid( $strConditionSetName, $intConditionSetId, $intCid, $objDatabase ) {
		$strWhere = '';

		if( 0 != $intConditionSetId ) {
			$strWhere = ' AND id <> ' . ( int ) $intConditionSetId;
		}

		$strSql = '	SELECT
						id
					FROM
						screening_condition_sets
					WHERE
						condition_name = \'' . str_replace( "'", "\'", $strConditionSetName ) . '\'
						AND cid = ' . ( int ) $intCid . '
						AND is_published = true'
		          . $strWhere;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningConditionTemplatesByCid( $intCid, $objDatabase, $boolIsRequireOrderBy = false ) {

		// $strOrderBy = ( true == $boolIsRequireOrderBy ) ? ' coalesce( scs.evaluation_order, 0) DESC' : ' scs.evaluation_order DESC';

		$strSql = 'SELECT 
						array_to_string( array_agg( DISTINCT sac_first.condition_name ), \', \') as first_condition_set_details,
						array_to_string( array_agg( DISTINCT sac_second.condition_name ), \', \') as second_condition_set_details,
						string_agg(distinct sc.criteria_name, \', \') as criteria_name,
						scs.id,
						scs.condition_name,
						scs.screening_recommendation_type_id,
						scs.screening_package_operand_type_id,
						spot.name as screening_package_operand_type,
						coalesce( scs.evaluation_order, -0.5 ) evaluation_order,
						scsub.required_items as first_subset_required_item,
						scsub_second.required_items as second_subset_required_item
					FROM
						screening_condition_sets scs
						LEFT JOIN screening_criteria_condition_sets sccs ON sccs.screening_condition_set_id = scs.id
						LEFT JOIN screening_criterias sc ON sc.id = sccs.screening_criteria_id AND sc.is_published = true
						LEFT JOIN screening_condition_subsets scsub ON scsub.id = scs.first_screening_condition_subset_id
						LEFT JOIN screening_condition_subsets scsub_second ON scsub_second.id = scs.second_screening_condition_subset_id
						LEFT JOIN screening_condition_subset_values scsv_first ON scsv_first.screening_condition_subset_id = scs.first_screening_condition_subset_id
						LEFT JOIN screening_condition_subset_values scsv_second ON scsv_second.screening_condition_subset_id = scs.second_screening_condition_subset_id
						LEFT JOIN screening_available_conditions sac_first ON sac_first.id = scsv_first.screening_available_condition_id AND sac_first.cid = scs.cid 
						LEFT JOIN screening_available_conditions sac_second ON sac_second.id = scsv_second.screening_available_condition_id AND sac_second.cid = scs.cid
						LEFT JOIN screening_package_operand_types spot ON spot.id = scs.screening_package_operand_type_id
					WHERE
						scs.is_published = true
						AND scs.cid = ' . ( int ) $intCid . '
					GROUP BY 
						scs.id,
						scsub.required_items,
						scsub_second.required_items,
						screening_package_operand_type
					ORDER BY
						coalesce( scs.evaluation_order, -0.5 ) DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllActiveScreeningCriteriaConditionSetByScreeningCriteriaIdByCid( $intScreeningCriteriaId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						scs.*
					FROM
						screening_condition_sets scs
						JOIN screening_criteria_condition_sets sccs ON sccs.screening_condition_set_id = scs.id
					WHERE												
						sccs.screening_criteria_id = ' . ( int ) $intScreeningCriteriaId . '
						AND scs.cid = ' . ( int ) $intCid;

		return parent::fetchScreeningConditionSets( $strSql, $objDatabase );
	}

	public static function fetchAllActiveScreeningConditionSetsByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_condition_sets
					WHERE
						is_published = TRUE
						AND cid = ' . ( int ) $intCid;

		return parent::fetchScreeningConditionSets( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningConditionSetsByScreenTypeIdByScreeningPackageId( $intScreenTypeId, $intScreeningPackageId, $objDatabase ) {
		if ( true == is_null( $intScreeningPackageId ) ) return NULL;

		$strSql = 'SELECT
						scs.id as condition_id,
						scs.condition_name,
						scs.screening_recommendation_type_id
					FROM
						screening_condition_sets scs
						JOIN screening_criteria_condition_sets sccs ON sccs.screening_condition_set_id = scs.id
						JOIN screening_package_screen_type_associations spcta ON spcta.screening_criteria_id = sccs.screening_criteria_id AND spcta.screen_type_id = ' . ( int ) $intScreenTypeId . '
					WHERE
						spcta.screening_package_id = ' . ( int ) $intScreeningPackageId . '
					ORDER BY
						scs.evaluation_order DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningConditionSetIdByFirstConditionSubsetId( $intFirstConditionSubsetId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						scs.id
					FROM
						screening_condition_sets scs
					WHERE
						first_screening_condition_subset_id = ' . ( int ) $intFirstConditionSubsetId . '
						AND scs.cid = ' . ( int ) $intCid . '
					ORDER BY scs.id desc
						';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllScreeningConditionSetsByScreeningIdScreeningPackageIdsByCid( $intScreeningId, $arrintScreeningPackageIds, $intCid, $objDatabase, $arrintScreeningConditionSetIds = NULL ) {
		if( false == valArr( $arrintScreeningPackageIds ) ) return NULL;

		$strConditions = '';

		if( true == valArr( $arrintScreeningConditionSetIds ) ) {
			$strConditions .= ' AND scs.id IN( ' . implode( ',', $arrintScreeningConditionSetIds ) . ' )';
		}

		$strSql = 'SELECT
						distinct( scs.id ) as condition_set_id,
						scs.is_published,
						scs.condition_name as condition_set_name,
						scs.first_screening_condition_subset_id as condition_subset_id_one,
						scs.second_screening_condition_subset_id as condition_subset_id_two,
						scsub.required_items,
						scsub.id as condition_subset_id,
						spcsubval.id as condition_subset_value_id,
						spcsubval.default_value,
						sac.condition_name as available_condition_name,
						sac.screening_package_condition_type_id,
						sac.id as screening_available_condition_id,
						scs.screening_package_operand_type_id
					FROM
						screening_condition_sets scs
						INNER JOIN screening_condition_subsets scsub ON
						(
						scs.first_screening_condition_subset_id = scsub.id OR
						scs.second_screening_condition_subset_id = scsub.id
						)
						INNER JOIN screening_condition_subset_values spcsubval ON scsub.id = spcsubval.screening_condition_subset_id
						INNER JOIN screening_available_conditions sac ON sac.id = spcsubval.screening_available_condition_id AND sac.cid = scs.cid
						INNER JOIN screening_criteria_condition_sets sccs ON(sccs.screening_condition_set_id = scs.id)
						INNER JOIN screening_package_screen_type_associations spsta ON(spsta.screening_criteria_id = sccs.screening_criteria_id )
					WHERE
		 				spsta.screening_package_id IN ( ' . implode( ',', $arrintScreeningPackageIds ) . ' )
						AND scs.cid = ' . ( int ) $intCid . ' 
						AND sccs.is_active = true '

		          . ' ' . $strConditions;

		$strSql .= ' UNION ';

		$strSql .= 'SELECT
						distinct( scs.id ) as condition_set_id,
						scs.is_published,
						scs.condition_name as condition_set_name,
						scs.first_screening_condition_subset_id as condition_subset_id_one,
						scs.second_screening_condition_subset_id as condition_subset_id_two,
						scsub.required_items,
						scsub.id as condition_subset_id,
						spcsubval.id as condition_subset_value_id,
						spcsubval.default_value,
						sac.condition_name as available_condition_name,
						sac.screening_package_condition_type_id,
						sac.id as screening_available_condition_id,
						scs.screening_package_operand_type_id
					FROM
						screening_condition_sets scs
						INNER JOIN screening_condition_subsets scsub ON
						(
						scs.first_screening_condition_subset_id = scsub.id OR
						scs.second_screening_condition_subset_id = scsub.id
						)
						INNER JOIN screening_condition_subset_values spcsubval ON scsub.id = spcsubval.screening_condition_subset_id
						INNER JOIN screening_available_conditions sac ON sac.id = spcsubval.screening_available_condition_id AND sac.cid = scs.cid					
						INNER JOIN screening_conditions sc ON( sc.screening_package_condition_set_id = scs.id AND sc.cid = scs.cid )
						INNER JOIN screening_applicants sa ON ( sa.screening_id = sc.screening_id AND sa.cid = sc.cid )                        
					WHERE
		 				sa.screening_package_id IN ( ' . implode( ',', $arrintScreeningPackageIds ) . ' )
						AND scs.cid = ' . ( int ) $intCid . '
						AND sc.screening_id = ' . ( int ) $intScreeningId . ' ' . $strConditions . ' order by condition_subset_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublishedGreaterScreeningConditionSetsByEvaluationOrderIdByCid( $intOldEvaluationOrderId, $intNewEvaluationOrderId, $intCid, $objDatabase ) {

		$strCondition = '';

		if( false == is_null( $intOldEvaluationOrderId ) && false == is_null( $intNewEvaluationOrderId ) ) {
			$strCondition = ' AND evaluation_order <= ' . ( int ) $intNewEvaluationOrderId;
		}

		$strSql = 'SELECT 
						id, evaluation_order
					FROM
						screening_condition_sets
					WHERE
						cid = ' . ( int ) $intCid . '
						AND screening_recommendation_type_id NOT IN ( ' . CScreeningRecommendationType::PASS . ', ' . CScreeningRecommendationType::FAIL . ' )
						AND is_published = true 
						AND evaluation_order IS NOT NULL AND evaluation_order >= ' . ( int ) $intOldEvaluationOrderId . ' ' . $strCondition . '
					ORDER BY 
						evaluation_order DESC';
		$arrmixResultData = fetchData( $strSql, $objDatabase );

		$arrintScreeningConditionSetIds = [];

		foreach( $arrmixResultData as $arrmixScreeningConditionSet ) {
			$arrintScreeningConditionSetIds[$arrmixScreeningConditionSet['id']] = $arrmixScreeningConditionSet['evaluation_order'];
		}

		return $arrintScreeningConditionSetIds;
	}

	public static function fetchCustomScreeningConditionSetsByCidByScreeningRecommendationTypeIds( $intCid, $arrintScreeningRecommendationTypeIds, $objDatabase ) {
		if( false == isset( $intCid ) ) return NULL;

		$strSql = 'SELECT
						id as condition_id,
						condition_name,
						screening_recommendation_type_id
					FROM
						screening_condition_sets
					WHERE
						cid = ' . ( int ) $intCid . '
						AND screening_recommendation_type_id IN (' . implode( ',', $arrintScreeningRecommendationTypeIds ) . ')
						AND is_published = true
					ORDER BY evaluation_order DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllActiveCriminalCriteriaScreeningConditionSetsByScreeningPackageIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						scs.*
					FROM
						screening_condition_sets scs
						JOIN screening_criteria_condition_sets sccs ON sccs.screening_condition_set_id = scs.id AND sccs.is_active = true
						JOIN screening_package_screen_type_associations spcta ON spcta.screening_criteria_id = sccs.screening_criteria_id AND spcta.screen_type_id IN( ' . sqlIntImplode( CScreenType::$c_arrintCriminalScreenTypeIds ) . ' )
					WHERE
						spcta.screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND scs.is_published = TRUE
						AND scs.cid = ' . ( int ) $intCid;

		return parent::fetchScreeningConditionSets( $strSql, $objDatabase );
	}

	public static function fetchScreeningConditionSetsByScreeningRecommendationTypeIdsByCid( $arrintScreeningRecommendationTypeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintScreeningRecommendationTypeIds ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						*
					FROM
						screening_condition_sets
					WHERE
						cid = ' . ( int ) $intCid . '
						AND screening_recommendation_type_id IN ( ' . sqlIntImplode( $arrintScreeningRecommendationTypeIds ) . ' )
						AND is_published IS TRUE 
		            ORDER BY
						evaluation_order ASC,
						id ASC';

		return parent::fetchScreeningConditionSets( $strSql, $objDatabase );
	}

	public static function fetchScreeningConditionSetsByScreeningPackageIdsByCid( $arrintScreeningPackageIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						scs.*
					FROM
						screening_condition_sets scs
						JOIN screening_criteria_condition_sets sccs ON sccs.screening_condition_set_id = scs.id AND sccs.is_active = true
						JOIN screening_package_screen_type_associations spcta ON spcta.screening_criteria_id = sccs.screening_criteria_id 
					WHERE
						spcta.screening_package_id IN(' . implode( ',', $arrintScreeningPackageIds ) . ' )
						AND scs.cid = ' . ( int ) $intCid;

		return parent::fetchScreeningConditionSets( $strSql, $objDatabase );
	}

    public static function fetchActivePassAndVerifyIdentityConditionSetsByCid( $intClientId, $objDatabase ) {
        $strSql = 'with condition_templates as(
	                                            SELECT
                                                      scs.*
                                                FROM
                                                     screening_condition_sets scs
                                                     JOIN screening_condition_subset_values scsv ON scsv.screening_condition_subset_id = scs.first_screening_condition_subset_id            
                                                WHERE
                                                    scs.cid = ' . ( int ) $intClientId . '
                                                    AND scs.is_published = true
                                                GROUP BY 
                                                    scs.id
                                                HAVING count( scsv.id ) = 1
											)

					SELECT
						*
					FROM
						screening_condition_sets
					WHERE
						cid = ' . ( int ) $intClientId . '
						AND is_published = true
                        AND screening_recommendation_type_id = ' . ( int ) CScreeningRecommendationType::PASS . '
                        
					UNION					

					SELECT
						scs.*
                    FROM
 	                    screening_condition_sets scs
                        JOIN condition_templates ct on ct.id = scs.id AND ct.cid = scs.cid
						JOIN screening_condition_subset_values scsv ON scsv.screening_condition_subset_id = scs.first_screening_condition_subset_id
                        JOIN screening_available_conditions sac ON sac.id = scsv.screening_available_condition_id AND sac.cid = scs.cid
                    WHERE
 	                    sac.screening_package_condition_type_id = ' . ( int ) CScreeningPackageConditionType::VERIFY_IDENTITY;

        return parent::fetchScreeningConditionSets( $strSql, $objDatabase );
    }

    public static function fetchActiveScreeningConditionSetsByCidByScreeningRecommendationTypeIds( $intCid, $arrintScreeningRecommendationTypeIds, $objDatabase ) {
        if( false == isset( $intCid ) ) return NULL;

        $strSql = 'SELECT
						*
					FROM
						screening_condition_sets
					WHERE
						cid = ' . ( int ) $intCid . '
						AND screening_recommendation_type_id IN (' . implode( ',', $arrintScreeningRecommendationTypeIds ) . ')
						AND is_published = true
					ORDER BY evaluation_order DESC';

        return parent::fetchScreeningConditionSets( $strSql, $objDatabase );
    }

}
?>