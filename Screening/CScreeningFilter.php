<?php

class CScreeningFilter extends CBaseScreeningFilter {

	const TYPE_USE_ALL = 12;
	const EXCLUDE_TIME_PERIOD = 31;

	protected $m_intScreeningConfigPreferenceTypeId;
	protected $m_intScreeningConfigPreferenceTypeFilterId;

    public function setScreeningConfigPreferenceTypeId( $intScreeningConfigPreferenceTypeId ) {
    	$this->m_intScreeningConfigPreferenceTypeId = $intScreeningConfigPreferenceTypeId;
    }

    public function getScreeningConfigPreferenceTypeId() {
    	return $this->m_intScreeningConfigPreferenceTypeId;
    }

    public function setScreeningConfigPreferenceTypeFilterId( $intScreeningConfigPreferenceTypeFilterId ) {
    	$this->m_intScreeningConfigPreferenceTypeFilterId = $intScreeningConfigPreferenceTypeFilterId;
    }

    public function getScreeningConfigPreferenceTypeFilterId() {
    	return $this->m_intScreeningConfigPreferenceTypeFilterId;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

    	if( isset( $arrValues['screening_config_preference_type_id'] ) ) $this->setScreeningConfigPreferenceTypeId( $arrValues['screening_config_preference_type_id'] );
    	if( isset( $arrValues['screening_config_preference_type_filter_id'] ) ) $this->setScreeningConfigPreferenceTypeFilterId( $arrValues['screening_config_preference_type_filter_id'] );

    	return;
    }
}
?>