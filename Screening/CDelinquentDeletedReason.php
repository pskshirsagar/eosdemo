<?php

class CDelinquentDeletedReason extends CBaseDelinquentDeletedReason {


	const DISPUTED 					    = 1;// from CA deleted record
	const SYSTEM_UPDATE_REVISAL 		= 2;// when revise the payment
	const SYSTEM_UPDATE_CANCEL 			= 3; // When reverse the FMO
    const INVALID                       = 4; // System Type
    const OTHER                         = 5; // from CA deleted record

	public static $c_arrstrDNRDeletedNotes = [
		CLeaseStatusType::PAST => 'The FMO is reversed',
		CLeaseStatusType::NOTICE => 'The Notice is reversed',
		CLeaseStatusType::CURRENT => 'The Termination is reversed',
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

    public static function getDelinquentEventReasonName( $intDelinquentDeleteReasonId ) {

        $strDelinquentDeleteReasonName = '';

        switch( $intDelinquentDeleteReasonId ) {
            case self::DISPUTED:
                $strDelinquentDeleteReasonName = 'Disputed';
                break;

            case self::SYSTEM_UPDATE_REVISAL:
                $strDelinquentDeleteReasonName = 'System Update Revisal';
                break;

            case self::SYSTEM_UPDATE_CANCEL:
                $strDelinquentDeleteReasonName = 'System Update Cancel';
                break;

            case self::INVALID:
                $strDelinquentDeleteReasonName = 'Invalid';
                break;

            case self::OTHER:
                $strDelinquentDeleteReasonName = 'Other';
                break;

            default:
                NULL;
        }

        return $strDelinquentDeleteReasonName;
    }

}
?>