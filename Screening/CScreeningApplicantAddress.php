<?php

class CScreeningApplicantAddress extends CBaseScreeningApplicantAddress {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningApplicantId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddressTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddressLine() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valState() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valZipcode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>