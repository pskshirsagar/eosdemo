<?php

class CScreeningConfigApplicantTazworksPackage extends CBaseScreeningConfigApplicantTazworksPackage {

    public function valName() {
        return true;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valName();
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName();
            	break;

            case VALIDATE_DELETE:
           		break;

            default:
            	// added default
        }

        return $boolIsValid;
    }

    public function archiveApplicantTazWorkConfig( $objDatabase, $boolReturnSqlOnly = false ) {
    	$strSql = 'UPDATE screening_config_applicant_tazworks_packages SET is_published = 0 WHERE id = ' . ( int ) $this->sqlId() . ';';

    	if( true == $boolReturnSqlOnly ) {
    		return $strSql;
    	} else {
    		return $this->executeSql( $strSql, $this, $objDatabase );
    	}

    }
}
?>