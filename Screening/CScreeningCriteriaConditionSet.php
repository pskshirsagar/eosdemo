<?php

class CScreeningCriteriaConditionSet extends CBaseScreeningCriteriaConditionSet {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function deactivateScreeningCriteriaCondtionSetsByScreeningCriteriaId( $intScreeningCriteriaId, $intCurrentUserId, $objDatabase ) {
		$strSql = 'UPDATE
						screening_criteria_condition_sets
					SET
						is_active = false,
						updated_by = ' . ( int ) $intCurrentUserId . ',
						updated_on = NOW()
					WHERE
						screening_criteria_id = ' . ( int ) $intScreeningCriteriaId;
		display( $strSql );

		//return executeSql( $strSql, $objDatabase );
	}

	public  function activateScreeningCriteriaCondtionSetsByConditionSetIdsByScreeningCriteriaId( $strScreeningConditionSetIds, $intScreeningCriteriaId, $intCurrentUserId, $objDatabase ) {
		$strSql = 'UPDATE
						screening_criteria_condition_sets
					SET
						is_active = false,
						updated_by = ' . ( int ) $intCurrentUserId . ',
						updated_on = NOW()
					WHERE
						screening_condition_set_id in ( ' . $strScreeningConditionSetIds . ' )
						screening_criteria_id = ' . ( int ) $intScreeningCriteriaId;
		display( $strSql );
		//return executeSql( $strSql, $objDatabase );
	}
}
?>