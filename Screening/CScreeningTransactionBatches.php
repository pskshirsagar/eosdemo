<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningTransactionBatches
 * Do not add any new functions to this class.
 */

class CScreeningTransactionBatches extends CBaseScreeningTransactionBatches {

	public static function fetchScreeningTransactionBatchesByTransactionIds( $arrintTransactionIds, $objScreeningDatabase ) {
		if( false == valIntArr( $arrintTransactionIds ) ) {
			return NULL;
		}

		$strSql = '
			SELECT
				*
			FROM
				screening_transaction_batches
			WHERE
				transaction_id IN ( ' . implode( ',', $arrintTransactionIds ) . ' );
		';

		return parent::fetchScreeningTransactionBatches( $strSql, $objScreeningDatabase );
	}

}
?>