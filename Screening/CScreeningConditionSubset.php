<?php

class CScreeningConditionSubset extends CBaseScreeningConditionSubset {

	protected $m_intDefaultValue;
	protected $m_intScreeningConditionTypeId;
	protected $m_intScreeningAvailableConditionId;
	protected $m_intScreeningChargeCodeAmountTypeId;

	public function getDefaultValue() {
		return $this->m_intDefaultValue;
	}

	public function getScreeningConditionTypeId() {
		return $this->m_intScreeningConditionTypeId;
	}

	public function getScreeningAvailableConditionId() {
		return $this->m_intScreeningAvailableConditionId;
	}

	public function getScreeningChargeCodeAmountTypeId() {
		return $this->m_intScreeningChargeCodeAmountTypeId;
	}

	public function setDefaultValue( $intDefaultValue ) {
		$this->m_intDefaultValue = $intDefaultValue;
	}

	public function setScreeningConditionTypeId( $intScreeningConditionTypeId ) {
		$this->m_intScreeningConditionTypeId = $intScreeningConditionTypeId;
	}

	public function setScreeningAvailableConditionId( $intScreeningAvailableConditionId ) {
		$this->m_intScreeningAvailableConditionId = $intScreeningAvailableConditionId;
	}

	public function setScreeningChargeCodeAmountTypeId( $intScreeningChargeCodeAmountTypeId ) {
		$this->m_intScreeningChargeCodeAmountTypeId = $intScreeningChargeCodeAmountTypeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['default_value'] ) )   $this->setDefaultValue( $arrmixValues['default_value'] );
		if( true == isset( $arrmixValues['screening_condition_type_id'] ) ) $this->setScreeningConditionTypeId( $arrmixValues['screening_condition_type_id'] );
		if( true == isset( $arrmixValues['screening_available_condition_id'] ) ) $this->setScreeningAvailableConditionId( $arrmixValues['screening_available_condition_id'] );
		if( true == isset( $arrmixValues['screening_charge_code_amount_type_id'] ) ) $this->setScreeningChargeCodeAmountTypeId( $arrmixValues['screening_charge_code_amount_type_id'] );

		return;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>