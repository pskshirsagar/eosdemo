<?php

class CScreeningPackageScreenTypeAssociation extends CBaseScreeningPackageScreenTypeAssociation {

	protected $m_intScreeningDataProviderTypeId;
	protected $m_strScreeningDataProviderName;
	protected $m_intScreeningPackageTypeId;

    public function getScreeningDataProviderTypeId() {
    	return $this->m_intScreeningDataProviderTypeId;
    }

    public function getScreeningDataProviderName() {
    	return $this->m_strScreeningDataProviderName;
    }

	public function getScreeningPackageTypeId() {
		return $this->m_intScreeningPackageTypeId;
	}

    public function setScreeningDataProviderTypeId( $intScreeningDataProviderTypeId ) {
    	$this->m_intScreeningDataProviderTypeId = CStrings::strToIntDef( $intScreeningDataProviderTypeId, NULL, false );
    }

    public function setScreeningDataProviderName( $strScreeningDataProviderName ) {
    	$this->m_strScreeningDataProviderName = $strScreeningDataProviderName;
    }

	public function setScreeningPackageTypeId( $intScreeningPackageTypeId ) {
		$this->m_intScreeningPackageTypeId = CStrings::strToIntDef( $intScreeningPackageTypeId, NULL, false );
	}

    public function valScreenTypeId( $objDatabase ) {
        $boolIsValid = true;

        if( true == is_null( $this->getScreenTypeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'screen_type_id', 'Screen type is required.' ) );
        } else {
        	$objScreenType = CScreenTypes::fetchScreenTypeById( $this->getScreenTypeId(), $objDatabase );

        	if( false == valObj( $objScreenType, 'CScreenType' ) ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'screen_type_id', 'Screen type should be valid.' ) );
        	}
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valScreenTypeId( $objDatabase );
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

    	parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

    	if( isset( $arrmixValues['screening_data_provider_type_id'] ) ) $this->setScreeningDataProviderTypeId( $arrmixValues['screening_data_provider_type_id'] );
    	if( isset( $arrmixValues['screening_data_provider_name'] ) )    $this->setScreeningDataProviderName( $arrmixValues['screening_data_provider_name'] );
	    if( isset( $arrmixValues['screening_package_type_id'] ) )       $this->setScreeningPackageTypeId( $arrmixValues['screening_package_type_id'] );

    	return;
    }

	public function copyManualCriminalSearchCriterias( $intOldScreeningPackageScreenTypeAssociationId ) {
		$strInsertSql = 'INSERT
							INTO
								screening_package_screen_type_criterias( screening_package_id, screening_package_screen_type_associations_id, screening_search_criteria_type_id, criteria, dispatch_type, offense_description, is_published, updated_by, updated_on, created_by, created_on )
						 SELECT
						 	   ' . $this->getScreeningPackageId() . ',
						 	   ' . $this->getId() . ',
						 	    screening_search_criteria_type_id,
								criteria,
								dispatch_type,
								offense_description,
								is_published,
								updated_by,
								NOW(),
								created_by,
								NOW()
							FROM
								screening_package_screen_type_criterias
							WHERE
								is_published = 1
								AND screening_package_screen_type_associations_id = ' . ( int ) $intOldScreeningPackageScreenTypeAssociationId . ';';
		return $strInsertSql;
	}

}
?>