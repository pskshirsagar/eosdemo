<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreenings
 * Do not add any new functions to this class.
 */

class CScreenings extends CBaseScreenings {

	public static function fetchScreeningByIdByCid( $intScreeningId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM screenings WHERE id = ' . ( int ) $intScreeningId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY id DESC limit 1';
		return parent::fetchScreening( $strSql, $objDatabase );
	}

	public static function fetchScreeningByCustomApplicationIdByCid( $intCustomApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM screenings WHERE custom_application_id = ' . ( int ) $intCustomApplicationId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY id DESC limit 1';
		return parent::fetchScreening( $strSql, $objDatabase );
	}

	public static function fetchScreeningsByCustomApplicationIdsByCid( $arrintCustomApplicationIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM screenings WHERE custom_application_id IN ( ' . implode( ',', $arrintCustomApplicationIds ) . ') AND cid = ' . ( int ) $intCid;
		return parent::fetchScreenings( $strSql, $objDatabase );
	}

	public static function fetchScreeningCids( $objDatabase ) {
		$strSql = 'SELECT cid FROM screenings GROUP BY cid';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningByIdCid( $intScreeningId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screenings
					WHERE
						id = ' . ( int ) $intScreeningId . '
						AND cid = ' . ( int ) $intCid;
		return parent::fetchScreening( $strSql, $objDatabase );
	}

	public static function fetchScreeningByIdByApplicationTypeIdByCid( $intScreeningId, $intApplicationTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM screenings WHERE id = ' . ( int ) $intScreeningId . ' AND application_type_id = ' . ( int ) $intApplicationTypeId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY id DESC limit 1';
		return parent::fetchScreening( $strSql, $objDatabase );
	}

	public static function fetchScreeningByCustomApplicationIdByApplicationTypeIdByCid( $intCustomApplicationId, $intApplicationTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM screenings WHERE custom_application_id = ' . ( int ) $intCustomApplicationId . ' AND application_type_id = ' . ( int ) $intApplicationTypeId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY id DESC limit 1';
		return parent::fetchScreening( $strSql, $objDatabase );
	}

	public static function fetchScreeningById( $intScreeningId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screenings
					WHERE
						id = ' . ( int ) $intScreeningId;

		return parent::fetchScreening( $strSql, $objDatabase );
	}

	public static function generateCustomScreeningApplicationVolumeReportByScreeningRecordSearchFilter( $objCScreeningReportSearchFilter, $objDatabase ) {

		if( false == valObj( $objCScreeningReportSearchFilter, CScreeningReportSearchFilter::class ) ) {
			return NULL;
		}

		$strWhere = $strSTWhere = '';
		if( false == $objCScreeningReportSearchFilter->getIsPropertySearchFilter() ) {
			if( true == valArr( $objCScreeningReportSearchFilter->getCids() ) ) {
				$strWhere   .= ' s.cid IN( ' . sqlIntImplode( $objCScreeningReportSearchFilter->getCids() ) . ') ';
				$strSTWhere .= ' st.cid IN( ' . sqlIntImplode( $objCScreeningReportSearchFilter->getCids() ) . ') ';
			} else {
				return NULL;
			}
		} else if( true == valId( $objCScreeningReportSearchFilter->getCid() ) ) {
			$strWhere .= ' s.cid = ' . ( int ) $objCScreeningReportSearchFilter->getCid();
			$strSTWhere .= ' st.cid = ' . ( int ) $objCScreeningReportSearchFilter->getCid();
		} else {
			return NULL;
		}

		if( true == $objCScreeningReportSearchFilter->getIsPropertySearchFilter() && true == valArr( $objCScreeningReportSearchFilter->getPropertyIds() ) ) {
			$strWhere .= ' AND s.property_id IN (' . sqlIntImplode( $objCScreeningReportSearchFilter->getPropertyIds() ) . ') ';
			$strSTWhere .= ' AND st.property_id IN (' . sqlIntImplode( $objCScreeningReportSearchFilter->getPropertyIds() ) . ') ';

		}

		if( false == $objCScreeningReportSearchFilter->getIsPropertySearchFilter() ) {
			$strJoin = 'st.cid = s.cid';
			$strGroupBy = 's.cid, st.revenue, st.total_applicants';
			$strSTGroupBy = 'st.cid';
			$strField = $strSTField = '';
		} else {
			$strField = 's.property_id,';
			$strSTField = 'st.property_id,';
			$strJoin = 'st.cid = s.cid AND st.property_id = s.property_id';
			$strGroupBy = 's.cid, s.property_id, st.revenue, st.total_applicants';
			$strSTGroupBy = 'st.cid, st.property_id';
		}

		if( true == valStr( $objCScreeningReportSearchFilter->getFromDate() ) && true == valStr( $objCScreeningReportSearchFilter->getToDate() ) ) {
			$strWhere .= ' AND  s.created_on::DATE BETWEEN DATE(\'' . $objCScreeningReportSearchFilter->getFromDate() . '\') AND DATE(\'' . $objCScreeningReportSearchFilter->getToDate() . '\')';
			$strSTWhere .= ' AND st.created_on::DATE BETWEEN DATE(\'' . $objCScreeningReportSearchFilter->getFromDate() . '\') AND DATE(\'' . $objCScreeningReportSearchFilter->getToDate() . '\')';
		}

		$strSql = 'WITH st AS (
						SELECT
						      st.cid, ' . $strSTField . '
						      SUM ( st.transaction_amount ) AS revenue,
						      COUNT ( DISTINCT ( st.screening_applicant_id ) ) AS total_applicants
						FROM
                            screening_transactions st
						WHERE
 	                        ' . $strSTWhere . '
				        GROUP BY ' . $strSTGroupBy . '
                    )
                    SELECT
				         s.cid, ' . $strField . '
				         st.revenue AS revenue,
				         COUNT( DISTINCT ( s.id )  ) AS total_applications,
				         st.total_applicants, 
				         COUNT( CASE WHEN s.screening_recommendation_type_id = ' . CScreeningRecommendationType::PASS . ' THEN TRUE END ) AS total_pass,
				         COUNT( CASE WHEN s.screening_recommendation_type_id = ' . CScreeningRecommendationType::PASSWITHCONDITIONS . ' THEN TRUE END ) AS total_conditional,
				         COUNT( CASE WHEN s.screening_recommendation_type_id = ' . CScreeningRecommendationType::FAIL . ' THEN TRUE END ) AS total_fail,
				         COUNT( CASE WHEN s.screening_recommendation_type_id = ' . CScreeningRecommendationType::PENDING_UNKNOWN . ' THEN TRUE END ) AS total_inprogress,
				         COUNT( CASE WHEN s.screening_status_type_id = ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR . ' AND s.screening_recommendation_type_id = 5 THEN TRUE END ) AS total_error,
				         COUNT( CASE WHEN s.screening_recommendation_type_id = ' . CScreeningRecommendationType::PASS . ' AND s.screening_decision_type_id = ' . CScreeningDecisionType::APPROVE . ' THEN TRUE END ) AS total_pass_decision,
				         COUNT( CASE WHEN s.screening_recommendation_type_id = ' . CScreeningRecommendationType::PASSWITHCONDITIONS . ' AND s.screening_decision_type_id = ' . CScreeningDecisionType::APPROVE_WITH_CONDITIONS . ' THEN TRUE END ) AS total_conditional_decision,
				         COUNT( CASE WHEN s.screening_recommendation_type_id = ' . CScreeningRecommendationType::FAIL . ' AND s.screening_decision_type_id = ' . CScreeningDecisionType::DENIED . ' THEN TRUE END ) AS total_fail_decision,
				         COUNT( CASE WHEN s.screening_recommendation_type_id = ' . CScreeningRecommendationType::PASS . ' AND s.screening_decision_type_id IS NOT NULL AND s.screening_decision_type_id <> ' . CScreeningDecisionType::APPROVE . ' THEN TRUE END ) AS total_pass_override_decision,
				         COUNT( CASE WHEN s.screening_recommendation_type_id = ' . CScreeningRecommendationType::PASSWITHCONDITIONS . ' AND s.screening_decision_type_id IS NOT NULL AND s.screening_decision_type_id <> ' . CScreeningDecisionType::APPROVE_WITH_CONDITIONS . ' THEN TRUE END ) AS total_conditional_override_decision,
				         COUNT( CASE WHEN s.screening_recommendation_type_id = ' . CScreeningRecommendationType::FAIL . ' AND s.screening_decision_type_id IS NOT NULL AND s.screening_decision_type_id <> ' . CScreeningDecisionType::DENIED . ' THEN TRUE END ) AS total_fail_override_decision,
				         ROUND( AVG(s.rent ), 3 ) AS average_rent,
				         ROUND( AVG(s.total_household_income ), 3 ) AS average_income
                    FROM 
                        screenings s
                        JOIN st ON ( ' . $strJoin . ' )
                    WHERE
                         ' . $strWhere . ' 
                    GROUP BY 
                        ' . $strGroupBy . ';';

		return fetchData( $strSql, $objDatabase );
	}

    public static function fetchCustomScreeningTurnAroundTimeReportByScreeningRecordSearchFilter( $objCScreeningReportSearchFilter, $objDatabase ) {

        if( false == valObj( $objCScreeningReportSearchFilter, CScreeningReportSearchFilter::class ) ) {
            return NULL;
        }

        $strWhere = '';
        if( true == valId( $objCScreeningReportSearchFilter->getCid() ) ) {
            $strWhere .= ' s.cid = ' . ( int ) $objCScreeningReportSearchFilter->getCid();
        } else {
            return NULL;
        }

        if( true == valArr( $objCScreeningReportSearchFilter->getPropertyIds() ) ) {
            $strWhere .= ' AND s.property_id IN (' . sqlIntImplode( $objCScreeningReportSearchFilter->getPropertyIds() ) . ') ';
        }

        if( true == valStr( $objCScreeningReportSearchFilter->getFromDate() ) && true == valStr( $objCScreeningReportSearchFilter->getToDate() ) ) {
            $strWhere .= ' AND  s.created_on::DATE BETWEEN DATE(\'' . $objCScreeningReportSearchFilter->getFromDate() . '\') AND DATE(\'' . $objCScreeningReportSearchFilter->getToDate() . '\')';
        }

        $strSql = 'SELECT 
						s.cid, s.property_id,
					    COUNT(s.id) as total_transactions,
                        COUNT( CASE WHEN EXTRACT( EPOCH FROM (  screening_completed_on - created_on ) ) < 3600 THEN TRUE END ) AS completed_less_than_one_hr,
                        COUNT( CASE WHEN EXTRACT( EPOCH FROM (  screening_completed_on - created_on ) )  > 3600 AND 
                                   EXTRACT( EPOCH FROM (  screening_completed_on - created_on ) ) < 7200  THEN TRUE END ) AS completed_less_than_two_hr,
                        COUNT( CASE WHEN EXTRACT( EPOCH FROM (  screening_completed_on - created_on ) ) > 7200 AND 
                                  EXTRACT( EPOCH FROM (  screening_completed_on - created_on ) ) < 10800 THEN TRUE END ) AS completed_less_than_three_hr,
                        COUNT( CASE WHEN EXTRACT( EPOCH FROM (  screening_completed_on - created_on ) ) > 10800 THEN TRUE END ) AS completed_more_than_three_hr,
					    COUNT( CASE WHEN screening_status_type_id IN( ' . sqlIntImplode( CScreeningStatusType::$c_arrintInProgressScreeningStatusTypeIds ) . '  ) THEN TRUE END ) AS total_inprogress,
					    COUNT( CASE WHEN screening_status_type_id = ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR . ' THEN TRUE END ) AS total_error,
                        AVG( screening_completed_on - created_on) AS average_completed_on
					FROM 
						screenings s
					WHERE 
						' . $strWhere . '
					GROUP BY
						s.cid, s.property_id
					ORDER BY
						s.cid, s.property_id;';

        return fetchData( $strSql, $objDatabase );
    }

}
?>