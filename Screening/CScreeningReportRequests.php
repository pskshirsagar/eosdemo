<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningReportRequests
 * Do not add any new functions to this class.
 */

class CScreeningReportRequests extends CBaseScreeningReportRequests {

	public static function fetchScreeningApplicantReportRequestCountByCidByScreeningIdByScreeningApplicantIdByStatusId( $intCid, $intScreeningId, $intScreeningApplicantId, $objDatabase ) {
		if( false == valId( $intScreeningId ) || false == valId( $intScreeningApplicantId ) ) return NULL;

		$strSql = 'SELECT
						count(id) as total_report_requests
					FROM screening_report_requests
					WHERE
						cid = ' . ( int ) $intCid . '
						AND screening_id = ' . ( int ) $intScreeningId . ' 
						AND screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
						AND screening_report_request_status_id IN ( ' . sqlIntImplode( [ CScreeningReportRequestStatus::REPORT_READY, CScreeningReportRequestStatus::INPROGRESS ] ) . ' )';

		return self::fetchColumn( $strSql, 'total_report_requests', $objDatabase );
	}

    public static function fetchScreeningApplicantReportRequestByScreeningIdByScreeningApplicantId( $intScreeningId, $intScreeningApplicantId, $objDatabase ) {
        if( false == valId( $intScreeningId ) || false == valId( $intScreeningApplicantId ) ) {
            return NULL;
        }

		$strSql = 'SELECT
						*
					FROM
						screening_report_requests
					WHERE
						screening_id = ' . ( int ) $intScreeningId . '
						AND screening_applicant_id = ' . ( int ) $intScreeningApplicantId;

		return parent::fetchScreeningReportRequest( $strSql, $objDatabase );
	}

    public static function fetchScreeningApplicantReportRequestCountByCidByScreeningIdByScreeningApplicantId( $intCid, $intScreeningId, $intScreeningApplicantId, $objDatabase ) {

        if( false == valId( $intScreeningId ) || false == valId( $intScreeningApplicantId ) ) return NULL;

		$strSql = 'SELECT
						count(id) as total_report_requests
					FROM 
						screening_report_requests
					WHERE
						cid = ' . ( int ) $intCid . '
						AND screening_id = ' . ( int ) $intScreeningId . '
						AND screening_applicant_id = ' . ( int ) $intScreeningApplicantId;

		return self::fetchColumn( $strSql, 'total_report_requests', $objDatabase );
	}

	public static function fetchScreeningReportRequestStatuses( $objDatabase ) {
			$strSql = 'SELECT
							id,
							name
						FROM
							screening_report_request_statuses
						WHERE
							is_published = TRUE
						ORDER BY 
							order_num';
			return CScreeningReportRequestStatuses::fetchScreeningReportRequestStatuses( $strSql, $objDatabase );
	}

	public static function fetchPaginatedScreeningReportRequestDetailsByScreeningReportRequestSearchFilterByScreeningReportRequestStatusTypeId( $objStdScreeningReportRequestRecordRequest, $objDatabase ) {

		$strSort = ' srr.updated_on DESC ';
		if( '' != $objStdScreeningReportRequestRecordRequest->sortDataBy ) {
			$strSort = ' sa.' . $objStdScreeningReportRequestRecordRequest->sortDataBy . ' ' . $objStdScreeningReportRequestRecordRequest->sortDataOrder;
		}

		$intOffset	= ( 0 < $objStdScreeningReportRequestRecordRequest->pageNo ) ? $objStdScreeningReportRequestRecordRequest->pageSize * ( $objStdScreeningReportRequestRecordRequest->pageNo - 1 ) : 0;
		$intLimit	= ( int ) $objStdScreeningReportRequestRecordRequest->pageSize;

		$strWhereCondition = ( '' != $objStdScreeningReportRequestRecordRequest->searchData ) ? 'AND (sa.name_first ILIKE \'%' . addslashes ( $objStdScreeningReportRequestRecordRequest->searchData ) . '%\' OR sa.name_last ILIKE \'%' . addslashes ( $objStdScreeningReportRequestRecordRequest->searchData ) . '%\' OR sa.email_address ILIKE \'%' . addslashes ( $objStdScreeningReportRequestRecordRequest->searchData ) . '%\')' :'';

        if( CScreeningReportRequestStatus::INPROGRESS == $objStdScreeningReportRequestRecordRequest->screeningReportRequestStatusTypeId ) {
        	$strWhereCondition .= ' and sa.status_type_id != ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED;
        }

        $strSql = 'SELECT
						srr.id,
						srr.screening_id,
						srr.screening_applicant_id,
						srr.screening_report_request_source_id,
						sa.name_first,
						sa.name_last,
						sa.email_address,
						sa.custom_applicant_id,
						sa.cid,
						sa.status_type_id,
						s.custom_application_id,
						s.property_id,
						sa.state as applicant_state_code,
						spa.state_code as property_state_code,
						sa.screening_completed_on,
						srr.alternate_email_address,
						CASE
							WHEN s.created_on < NOW() - \'' . CScreening::SCREENING_RETENTION_PERIOD . ' years\'::interval THEN
								1
							ELSE
							    0
						END AS is_out_of_screening_retention_period
					FROM
						screening_report_requests srr
						JOIN screening_applicants sa ON ( sa.id = srr.screening_applicant_id )
						JOIN screenings s ON ( s.id = srr.screening_id )
						JOIN screening_property_accounts spa on spa.property_id = s.property_id and spa.is_published = 1
					WHERE
						srr.screening_report_request_status_id = ' . ( int ) $objStdScreeningReportRequestRecordRequest->screeningReportRequestStatusTypeId . '
                        ' . $strWhereCondition . '
					ORDER BY ' . $strSort . '
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

        return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningReportRequestsByStatus( $arrintScreeningReportRequestStatusIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScreeningReportRequestStatusIds ) ) return NULL;

		$strSql = 'SELECT
						srr.*,
						s.property_id,
						s.custom_application_id,
						sa.custom_applicant_id,
						sa.name_first,
						sa.name_last,
						COALESCE(srr.alternate_email_address,sa.email_address) as email_address,
						tax_number_last_four,
						birth_date_year,
						s.cid,
						sa.state as applicant_state_code,
						spa.state_code as property_state_code
					FROM 
						screening_report_requests srr
						JOIN screenings s on s.id = srr.screening_id and s.cid = srr.cid
						JOIN screening_applicants sa on sa.id = srr.screening_applicant_id and sa.cid = srr.cid and sa.screening_id = srr.screening_id  
						JOIN screening_property_accounts spa on spa.property_id = s.property_id and spa.is_published = 1
					WHERE
						screening_report_request_status_id IN ( ' . sqlIntImplode( $arrintScreeningReportRequestStatusIds ) . ' )
						AND srr.cid = ' . ( int ) $intCid . '
					ORDER BY s.cid';

        return fetchData( $strSql, $objDatabase );
    }

	public static function fetchScreeningReportRequestDetailsCountByScreeningReportRequestSearchFilter( $objStdScreeningReportRequestRecordRequest, $objDatabase ) {

		$strWhereCondition = ( '' != $objStdScreeningReportRequestRecordRequest->searchData ) ? 'AND (sa.name_first ILIKE \'%' . addslashes ( $objStdScreeningReportRequestRecordRequest->searchData ) . '%\' OR sa.name_last ILIKE \'%' . addslashes ( $objStdScreeningReportRequestRecordRequest->searchData ) . '%\' OR sa.email_address ILIKE \'%' . addslashes ( $objStdScreeningReportRequestRecordRequest->searchData ) . '%\')' :'';

		if( CScreeningReportRequestStatus::INPROGRESS == $objStdScreeningReportRequestRecordRequest->screeningReportRequestStatusTypeId ) {
			$strWhereCondition .= ' and sa.status_type_id != ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED;
		}

        $strSql = 'SELECT
					    COUNT( srr.id )
					FROM
					    screening_report_requests srr
						JOIN screening_applicants sa ON ( sa.id = srr.screening_applicant_id )
						JOIN screening_report_request_sources srrs ON ( srrs.id = srr.screening_report_request_source_id )
						JOIN screenings s ON ( s.id = srr.screening_id )
						JOIN screening_property_accounts spa on spa.property_id = s.property_id and spa.is_published = 1
					WHERE
						srr.screening_report_request_status_id = ' . ( int ) $objStdScreeningReportRequestRecordRequest->screeningReportRequestStatusTypeId . '
						' . $strWhereCondition;

		$arrintScreeningReportRequestsCount = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintScreeningReportRequestsCount ) ) ? $arrintScreeningReportRequestsCount[0]['count'] : 0;
	}

	public static function updateScreeningReportRequestStatus( $intId, $intScreeningReportRequestStatusId, $intCurrentUserId, $objDatabase ) {

		$strSetCondition = '';
		if( CScreeningReportRequestStatus::REPORT_SENT == $intScreeningReportRequestStatusId ) {
			$strSetCondition = ' , report_sent_by = ' . ( int ) $intCurrentUserId . ', report_sent_on = NOW() ';
		} else {
			$strSetCondition = ' , report_sent_by = NULL, report_sent_on = NULL ';
		}

		$strSql = 'UPDATE
		                screening_report_requests
		            SET
		              screening_report_request_status_id = ' . ( int ) $intScreeningReportRequestStatusId . ',
		              updated_by = ' . ( int ) $intCurrentUserId . ',
		              updated_on = NOW() 
					  ' . $strSetCondition . '
		            WHERE
		              id = ' . ( int ) $intId;

		return executeSql( $strSql, $objDatabase );
	}

	public static function updateScreeningReportRequestStatusByScreeningIdByScreeningApplicantIdsByCid( $intScreeningId, $arrintScreeningApplicantIds, $intCid, $intScreeningReportRequestStatusId, $intCurrentUserId, $objDatabase, $boolIsCheckedOnlyInProgress = false ) {

		if( false == valId( $intScreeningId ) || false == valId( $intCid ) || false == valArr( $arrintScreeningApplicantIds ) || false == valId( $intScreeningReportRequestStatusId ) ) {
			return NULL;
		}

        $arrintScreeningReportRequestStatusIds = ( true == $boolIsCheckedOnlyInProgress ) ? [ CScreeningReportRequestStatus::INPROGRESS ] : [ CScreeningReportRequestStatus::REPORT_READY ];

		$strSql = 'UPDATE
		                screening_report_requests
		            SET
		              screening_report_request_status_id = ' . ( int ) $intScreeningReportRequestStatusId . ',
		              updated_by = ' . ( int ) $intCurrentUserId . ',
		              updated_on = NOW(),
					  report_sent_by = NULL,
					  report_sent_on = NULL,
					  system_email_ids = NULL
		            WHERE
		                cid = ' . ( int ) $intCid . '
						AND screening_id = ' . ( int ) $intScreeningId . '
						AND screening_report_request_status_id IN  (' . sqlIntImplode( $arrintScreeningReportRequestStatusIds ) . ')' . '
						AND screening_applicant_id IN  (' . sqlIntImplode( $arrintScreeningApplicantIds ) . ')';

		return executeSql( $strSql, $objDatabase );
	}

    public static function fetchScreeningReportRequestByCidByScreeningIdByScreeningApplicantId( $intCid, $intScreeningId, $intScreeningApplicantId, $objDatabase ) {
        if( false == valId( $intCid ) || false == valId( $intScreeningId ) || false == valId( $intScreeningApplicantId ) ) return NULL;

        $strSql = 'SELECT
						srr.*,
						s.property_id,
						s.custom_application_id,
						sa.custom_applicant_id,
						sa.name_first,
						sa.name_last,
						sa.email_address,
						sa.status_type_id,
						tax_number_last_four,
						birth_date_year,
						s.cid
					FROM
						screening_report_requests srr
						JOIN screenings s on s.id = srr.screening_id and s.cid = srr.cid
						JOIN screening_applicants sa on sa.id = srr.screening_applicant_id and sa.cid = srr.cid and sa.screening_id = srr.screening_id
					WHERE
						srr.cid = ' . ( int ) $intCid . '
						AND srr.screening_id = ' . ( int ) $intScreeningId . '
						AND srr.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
					ORDER BY s.cid';

        return executeSql( $strSql, $objDatabase );
    }

    public static function fetchScreeningReportRequestsByStatusHavingSystemEmails( $arrintScreeningReportRequestStatusIds, $objDatabase ) {
        if( false == valArr( $arrintScreeningReportRequestStatusIds ) ) return NULL;

        $strSql = 'SELECT
						srr.id,
						srr.created_on,
						srr.system_email_ids,
						s.custom_application_id,
						srr.cid
					FROM
						screening_report_requests srr
					Left JOIN screenings s ON s.id = srr.screening_id 
					WHERE
						srr.screening_report_request_status_id IN ( ' . sqlIntImplode( $arrintScreeningReportRequestStatusIds ) . ' )
						AND srr.system_email_ids IS NOT NULL';

        return executeSql( $strSql, $objDatabase );
    }

    public static function updateResendScreeningReportRequestStatus( $intId, $intScreeningReportRequestStatusId, $intCurrentUserId, $objDatabase ) {

        $strSql = 'UPDATE
		                screening_report_requests
		            SET
		              screening_report_request_status_id = ' . ( int ) $intScreeningReportRequestStatusId . ',
		              report_sent_by = NULL, 
		              report_sent_on = NULL, 
		              system_email_ids = NULL,
		              updated_by = ' . ( int ) $intCurrentUserId . ',
		              updated_on = NOW()
		            WHERE
		              id = ' . ( int ) $intId;

        return executeSql( $strSql, $objDatabase );
    }

	public static function fetchScreeningApplicantReportRequestByScreeningApplicantIdByCid( $intScreeningApplicantId, $intCId, $objDatabase ) {
		if( false == valId( $intCId ) || false == valId( $intScreeningApplicantId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						screening_report_requests
					WHERE
						cid = ' . ( int ) $intCId . '
						AND screening_applicant_id = ' . ( int ) $intScreeningApplicantId;

		return parent::fetchScreeningReportRequest( $strSql, $objDatabase );
	}

}
?>