<?php

class CScreeningPackageApplicant extends CBaseScreeningPackageApplicant {

	protected $m_intOccupancyTypeId;

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->m_intOccupancyTypeId = $intOccupancyTypeId;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrmixValues['occupancy_type_id'] );
		return;
	}

}
?>