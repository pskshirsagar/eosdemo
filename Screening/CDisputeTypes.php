<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDisputeTypes
 * Do not add any new functions to this class.
 */

class CDisputeTypes extends CBaseDisputeTypes {

	public static function fetchDisputeTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDisputeType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDisputeType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDisputeType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedDisputeTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						dispute_types
					WHERE
						is_published = true
					ORDER BY id
					';
		return parent::fetchDisputeTypes( $strSql, $objDatabase );
	}

	public static function fetchPublishedDisputeTypesByIds( $arrintDisputeTypeIds, $objDatabase ) {
		if( false == valArr( $arrintDisputeTypeIds ) ) return;
		$strSql = 'SELECT
						*
					FROM
						dispute_types
					WHERE
						is_published = true 
						AND id IN ( ' . sqlIntImplode( $arrintDisputeTypeIds ) . ' )
					';
		return self::fetchDisputeTypes( $strSql, $objDatabase );
	}

}
?>