<?php

class CScreeningConfigValueType extends CBaseScreeningConfigValueType {

	const FLAG 					 = 1;
	const VALUE_RANGE_ABOVE 	 = 2;
	const FRACTION_RANGE_ABOVE 	 = 3;
	const PERCENTAGE_RANGE_ABOVE = 4;
	const VALUE_RANGE_BELOW 	 = 5;
	const FRACTION_RANGE_BELOW 	 = 6;
	const PERCENTAGE_RANGE_BELOW = 7;
	const DATE_ABOVE 			 = 8;

	public static $c_arrintScreeningConfigValueTypeAssociations = array(
		self::FLAG                   => 'FLAG',
		self::VALUE_RANGE_ABOVE      => 'VALUE_RANGE_ABOVE',
		self::FRACTION_RANGE_ABOVE   => 'FRACTION_RANGE_ABOVE',
		self::PERCENTAGE_RANGE_ABOVE => 'PERCENTAGE_RANGE_ABOVE',
		self::VALUE_RANGE_BELOW      => 'VALUE_RANGE_BELOW',
		self::FRACTION_RANGE_BELOW   => 'FRACTION_RANGE_BELOW',
		self::PERCENTAGE_RANGE_BELOW => 'PERCENTAGE_RANGE_BELOW',
		self::DATE_ABOVE             => 'DATE_ABOVE'
    );

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>