<?php

class CCriminalClassificationKeyword extends CBaseCriminalClassificationKeyword {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKeywordTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClassificationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKeyword() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSortOrder() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHighThreshold() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLowThreshold() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>