<?php

class CScreeningInvoice extends CBaseScreeningInvoice {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreenTypeId() {
		$boolIsValid = true;

        if( false == is_numeric( $this->getScreenTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Screen Type is required. ', NULL ) );
        }

        return $boolIsValid;
	}

	public function valScreeningDataProviderTypeId() {
		$boolIsValid = true;
        if( false == is_numeric( $this->getScreeningDataProviderTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Screening Data Provider Type is required. ', NULL ) );
        }

        return $boolIsValid;
	}

	public function valTotalInvoiceAmount() {
		$boolIsValid = true;
        if( true == is_null( $this->getTotalInvoiceAmount() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Total Invoice Amount is required.', NULL ) );
        }
		return $boolIsValid;
	}

	public function valTotalTransactionAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInvoiceDueAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInvoiceDate() {
		$boolIsValid = true;
        if( true == is_null( $this->getInvoiceDate() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Invoice date is required.', NULL ) );
        }
		return $boolIsValid;
	}

	public function valInvoiceDueDate() {
		$boolIsValid = true;
        if( true == is_null( $this->getInvoiceDueDate() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Invoice Due Date is required.', NULL ) );
        }
		return $boolIsValid;
	}

    public function valFileName() {
        $boolIsValid = true;

        if( true == is_null( $this->getFileName() ) || 0 == strlen( $this->getFileName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Select Screening Invoice File.', NULL ) );
        }
        return $boolIsValid;
    }

	public function valReconciliationStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReconciledBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReconciledOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function valScreeningInvoiceExist( $strFileName, $objScreeningDatabase ) {

        $boolIsValid = true;

        // Check for the active invoice number
        $intCountScreeningInvoice = \Psi\Eos\Screening\CScreeningInvoices::createService()->fetchActiveScreeningInvoiceCountByCarrierInvoiceNumber( $strFileName, $objScreeningDatabase );

        if( 0 < $intCountScreeningInvoice ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Screening invoice is already exist. ', NULL ) );
        }

        return $boolIsValid;
    }

	public function validate( $strAction, $strFileName, $objScreeningDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

            case 'validate_create_screening_invoice':
                $boolIsValid &= $this->valScreenTypeId();
                $boolIsValid &= $this->valScreeningDataProviderTypeId();
                $boolIsValid &= $this->valInvoiceDate();
                $boolIsValid &= $this->valInvoiceDueDate();
                $boolIsValid &= $this->valTotalInvoiceAmount();
                $boolIsValid &= $this->valFileName();
                $boolIsValid &= $this->valScreeningInvoiceExist( $strFileName, $objScreeningDatabase );
                break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>