<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CPrescreeningSets
 * Do not add any new functions to this class.
 */

class CPrescreeningSets extends CBasePrescreeningSets {

    public static function fetchPrescreeningSets( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CPrescreeningSet', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchPrescreeningSet( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CPrescreeningSet', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchAllPrescreeningSets( $objDatabase ) {
   		return self::fetchPrescreeningSets( 'SELECT * FROM prescreening_sets', $objDatabase );
   	}

}
?>