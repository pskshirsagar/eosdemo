<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningErrorTypes
 * Do not add any new functions to this class.
 */

class CScreeningErrorTypes extends CBaseScreeningErrorTypes {

	public static function fetchScreeningErrorTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningErrorType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningErrorType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningErrorType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>