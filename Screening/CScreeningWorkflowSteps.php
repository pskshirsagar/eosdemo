<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningWorkflowSteps
 * Do not add any new functions to this class.
 */

class CScreeningWorkflowSteps extends CBaseScreeningWorkflowSteps {

	public static function fetchScreeningWorkflowSteps( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CScreeningWorkflowStep::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningWorkflowStep( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CScreeningWorkflowStep::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchNextScreeningWorkFlowScreenTypeIdByScreeningIdByScreeningApplicantIdByCid( $intScreeningId, $intScreeningApplicantId, $intCid, $objDatabase ) {

		if( false == valId( $intScreeningId ) || false == valId( $intScreeningApplicantId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						sws.* 
					FROM
						screening_transactions st
						JOIN screening_workflow_steps sws ON( st.screen_type_id = sws.screen_type_id)
					WHERE 
						st.cid = ' . ( int ) $intCid . '
						AND st.screening_id = ' . ( int ) $intScreeningId . '
						AND st.screening_applicant_id = ' . ( int ) $intScreeningApplicantId . '
					ORDER BY sws.id DESC LIMIT 1';

		return parent::fetchScreeningWorkflowStep( $strSql, $objDatabase );
	}

}
?>