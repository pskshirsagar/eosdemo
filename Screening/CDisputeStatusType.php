<?php

class CDisputeStatusType extends CBaseDisputeStatusType {

	const DISPUTE_STATUS_TYPE_NEW 								= 1;
	const DISPUTE_STATUS_TYPE_UNDER_REVIEW 						= 2;
	const DISPUTE_STATUS_TYPE_ACKNOWLEDGE 						= 3;
	const DISPUTE_STATUS_TYPE_AWAITING_CONSUMER_RESPONSE 		= 4;
	const DISPUTE_STATUS_TYPE_AWAITING_PROVIDER_RESPONSE 		= 5;
	const DISPUTE_STATUS_TYPE_COMPLETE 							= 6;
	const DISPUTE_STATUS_TYPE_CANCELLED 						= 7;
	const DISPUTE_STATUS_TYPE_ON_HOLD 							= 8;
	const DISPUTE_STATUS_TYPE_ARCHIVED 							= 9;

	public static $c_arrintDisputeStatusTypeIds = array(
			self::DISPUTE_STATUS_TYPE_CANCELLED
	);

	public static $c_arrintNotIncludeDisputeStatusTypeIds = array(
		self::DISPUTE_STATUS_TYPE_CANCELLED,
		self::DISPUTE_STATUS_TYPE_ON_HOLD,
		self::DISPUTE_STATUS_TYPE_ARCHIVED
	);

	public static $c_arrintIncludeDisputeStatusTypeIds = array(
		self::DISPUTE_STATUS_TYPE_ACKNOWLEDGE,
		self::DISPUTE_STATUS_TYPE_AWAITING_CONSUMER_RESPONSE,
		self::DISPUTE_STATUS_TYPE_ON_HOLD
	);

	public static $c_arrintIncludeDisputeStatusTypeIdsForRecievedMails = array(
		self::DISPUTE_STATUS_TYPE_CANCELLED,
		self::DISPUTE_STATUS_TYPE_COMPLETE,
		self::DISPUTE_STATUS_TYPE_ARCHIVED,
		self::DISPUTE_STATUS_TYPE_NEW,
		self::DISPUTE_STATUS_TYPE_AWAITING_PROVIDER_RESPONSE
	);

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getDisputeStatusTypeText( $intDisputeStatusTypeId ) {
		$strDisputeStatusTypeText = '';

		switch( $intDisputeStatusTypeId ) {
			case self::DISPUTE_STATUS_TYPE_NEW:
				$strDisputeStatusTypeText = 'New';
				break;

			case self::DISPUTE_STATUS_TYPE_UNDER_REVIEW:
				$strDisputeStatusTypeText = 'Under Review';
				break;

			case self::DISPUTE_STATUS_TYPE_ACKNOWLEDGE:
				$strDisputeStatusTypeText = 'Acknowledged';
				break;

			case self::DISPUTE_STATUS_TYPE_AWAITING_CONSUMER_RESPONSE:
				$strDisputeStatusTypeText = 'Awaiting Consumer Response';
				break;

			case self::DISPUTE_STATUS_TYPE_AWAITING_PROVIDER_RESPONSE:
				$strDisputeStatusTypeText = 'Awaiting Provider Response';
				break;

			case self::DISPUTE_STATUS_TYPE_COMPLETE:
				$strDisputeStatusTypeText = 'Completed';
				break;

			case self::DISPUTE_STATUS_TYPE_CANCELLED:
				$strDisputeStatusTypeText = 'Cancelled';
				break;

			case self::DISPUTE_STATUS_TYPE_ON_HOLD:
				$strDisputeStatusTypeText = 'On Hold';
				break;

			case self::DISPUTE_STATUS_TYPE_ARCHIVED:
				$strDisputeStatusTypeText = 'Archived';
				break;

			default:
				// added default case
		}

		return $strDisputeStatusTypeText;
	}

}
?>