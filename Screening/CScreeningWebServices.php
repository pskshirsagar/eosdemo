<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningWebServices
 * Do not add any new functions to this class.
 */

class CScreeningWebServices extends CBaseScreeningWebServices {

	public static function fetchSceeningWebServiceDetailsByIds( $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;
		$strSql = 'SELECT
						*
					FROM
						screening_web_services
					WHERE
						id IN ( ' . implode( ',', $arrintIds ) . ' ) ';
		return self::fetchScreeningWebServices( $strSql, $objDatabase );
	}
}
?>