<?php

class CScreenType extends CBaseScreenType {

    const CREDIT                        = 1;
    const CRIMINAL                      = 2;
    const EVICTION                      = 3;
    const MANUAL_CRIMINAL               = 4;
    const STUDENT_STATUS_VERIFICATION = 5;
    const EMPLOYMENT_VERIFICATION     = 6;
    const CRIMINAL_CATEGORIZATION     = 7;
    const INCOME                      = 8;
    const BUSINESS_CREDIT             = 9;
    const BUSINESS_PREMIER_PROFILE    = 10;
    const DO_NOT_RENT                 = 11;
    const STATE_CRIMINAL_SEARCH       = 12;
    const COUNTY_CRIMINAL_SEARCH      = 13;
    const RENTAL_HISTORY              = 14;
	const FRAUD_SHIELD                = 15;
	const PASSPORT_VISA_VERIFICATION  = 16;
	const CORPORATE_VERIFICATION      = 17;
	const ATTRIBUTE_BASED             = 18;

	const PRECISE_ID                  = 19;
	const PRECISE_ID_AO_SCORE         = 20;
	const PRECISE_ID_OTP              = 21;
	const PRECISE_ID_KIQ              = 22;
	const VERIFICATION_OF_INCOME      = 23;

    public static $c_arrintNotInUseScreenTypeIds = array(
        self::MANUAL_CRIMINAL,
        self::STUDENT_STATUS_VERIFICATION,
        self::EMPLOYMENT_VERIFICATION,
        self::CRIMINAL_CATEGORIZATION
    );

    public static $c_arrintRVScreenTypeIds = array(
        self::MANUAL_CRIMINAL,
        self::STUDENT_STATUS_VERIFICATION,
        self::EMPLOYMENT_VERIFICATION,
        self::CRIMINAL_CATEGORIZATION,
        self::INCOME,
        self::DO_NOT_RENT
    );

    public static $c_arrintRVScreenTypesOrderIdsAssociations = array(

        self::MANUAL_CRIMINAL               => 'MANUALCRIM',
        self::STUDENT_STATUS_VERIFICATION   => 'STUDENTVERIFY',
        self::EMPLOYMENT_VERIFICATION       => 'EMPVERIFY',
        self::CRIMINAL_CATEGORIZATION       => 'CRIMCAT',
        self::INCOME                        => 'INCOME',
        self::DO_NOT_RENT                   => 'DONOTRENT',
        self::FRAUD_SHIELD                  => 'FACSPLUS',
	    self::PASSPORT_VISA_VERIFICATION    => 'PASSPORT',
	    self::CORPORATE_VERIFICATION        => 'CORPORATE',
        self::PRECISE_ID                    => 'PRECISEID'
    );

    // These are the screen type which we do not consider in consolidated result calculation
    // Hence, if screening for any the defined screen type is completed then we are not going to re evaluate the result.
    public static $c_arrintNotConsolidatedScreenTypeIds = array(
        self::CRIMINAL,
        self::EVICTION,
        self::CRIMINAL_CATEGORIZATION,
        self::BUSINESS_CREDIT,
        self::BUSINESS_PREMIER_PROFILE,
        self::DO_NOT_RENT,
        self::MANUAL_CRIMINAL,
        self::EMPLOYMENT_VERIFICATION,
        self::STUDENT_STATUS_VERIFICATION,
        self::STATE_CRIMINAL_SEARCH,
        self::COUNTY_CRIMINAL_SEARCH,
        self::RENTAL_HISTORY,
	    self::PRECISE_ID
    );

    public static $c_arrintManualCriminalSearchScreenTypeIds = array(
        self::STATE_CRIMINAL_SEARCH,
        self::COUNTY_CRIMINAL_SEARCH
    );

    public static $c_arrintCriminalScreenTypeIds = array(
        self::CRIMINAL,
        self::STATE_CRIMINAL_SEARCH,
        self::COUNTY_CRIMINAL_SEARCH
    );

    // This array will hold screen type ids whose screening request is not going to submit
    // to the data provider in specific case
    // Cases : no ssn. case list may increase in future..
    public static $c_arrintExceptionalScreenTypeIds = array(
        self::CREDIT,
        self::EVICTION
    );

    public static $c_arrintAllowZeroRateScreenTypeIds = array(
    	self::INCOME,
    	self::CRIMINAL_CATEGORIZATION,
    	self::STATE_CRIMINAL_SEARCH,
    	self::COUNTY_CRIMINAL_SEARCH,
        self::FRAUD_SHIELD,
	    self::PASSPORT_VISA_VERIFICATION,
	    self::CORPORATE_VERIFICATION
    );

    public static $c_arrintScreenTypeChildScreenTypeIds = array(
    	CScreenType::CREDIT     => array( CScreenType::FRAUD_SHIELD ),
	    CScreenType::CRIMINAL   => array( CScreenType::CRIMINAL_CATEGORIZATION ),
        CScreenType::PRECISE_ID   => array( CScreenType::PRECISE_ID_AO_SCORE, CScreenType::PRECISE_ID_OTP, CScreenType::PRECISE_ID_KIQ )
    );

	public static $c_arrintCriteriaNotRequiredScreenTypes = array(
		self::PASSPORT_VISA_VERIFICATION,
		self::FRAUD_SHIELD,
		self::CRIMINAL_CATEGORIZATION,
		self::MANUAL_CRIMINAL
	);

	public static $c_arrintParentChildScreenTypes = [
		self::CRIMINAL => [ self::STATE_CRIMINAL_SEARCH, self::COUNTY_CRIMINAL_SEARCH, self::CRIMINAL, self::CRIMINAL_CATEGORIZATION ],
		self::CREDIT   => [ self::CREDIT, self::FRAUD_SHIELD ]
	];

	public static $c_arrintChildParentScreenTypes = [
		self::STATE_CRIMINAL_SEARCH   => self::CRIMINAL,
		self::COUNTY_CRIMINAL_SEARCH  => self::CRIMINAL,
		self::CRIMINAL_CATEGORIZATION => self::CRIMINAL,
		self::FRAUD_SHIELD              => self::CREDIT,
	];

	public static $c_arrintInHouseReviewEnabledScreenTypeIds = [
		self::CRIMINAL,
		self::STATE_CRIMINAL_SEARCH,
		self::COUNTY_CRIMINAL_SEARCH,
		self::CORPORATE_VERIFICATION,
		self::PASSPORT_VISA_VERIFICATION
	];

	public static $c_arrintChildScreenTypeIds = [
		self::CRIMINAL,
		self::STATE_CRIMINAL_SEARCH,
		self::COUNTY_CRIMINAL_SEARCH,
		self::FRAUD_SHIELD
	];

	public static $c_arrintOverrideMustPassCreditIncomeSetting = [ CScreenType::CREDIT ];

	public static $c_arrintMandatoryBirthDateValidationScreenTypes = [ self::CREDIT, Self::CRIMINAL, self::EVICTION ];

	public static $c_arrintChildScreenTypesIds = [ self::CRIMINAL_CATEGORIZATION, self::FRAUD_SHIELD, self::PRECISE_ID_AO_SCORE, self::PRECISE_ID_OTP, self::PRECISE_ID_KIQ ];

    public static $c_arrintExcludeFromReportScreenTypesIds = [ self::CRIMINAL_CATEGORIZATION, self::MANUAL_CRIMINAL ];

    public static $c_arrintExcludeFromScreeningDetails = [ self::CRIMINAL_CATEGORIZATION ];

	public static $c_arrintBusinessCorporateScreenTypeIds = [
		self::BUSINESS_CREDIT,
		self::BUSINESS_PREMIER_PROFILE
	];

	public static $c_arrintSkipManualReviewScreenTypeIds = [
		self::CREDIT,
		self::BUSINESS_CREDIT,
		self::BUSINESS_PREMIER_PROFILE,
		self::VERIFICATION_OF_INCOME
	];

	public static $c_arrintDocumentVerificationScreenTypesIds = [ self::CORPORATE_VERIFICATION, self::PASSPORT_VISA_VERIFICATION ];

	public static $c_arrintPreciseIdScreenTypeIds = [ self::PRECISE_ID, self::PRECISE_ID_AO_SCORE, self::PRECISE_ID_OTP, self::PRECISE_ID_KIQ ];

	public static $c_arrintPreciseIdScreenTypeIdsReviewResult = [ self::PRECISE_ID_OTP, self::PRECISE_ID_KIQ ];

	public static $c_arrintPreciseIdChildScreenTypeIds = [ self::PRECISE_ID_AO_SCORE, self::PRECISE_ID_OTP, self::PRECISE_ID_KIQ ];

    public static $c_arrintSkipScreenTypeIds = [ self::STATE_CRIMINAL_SEARCH => [ CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN, CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR ],
        self::COUNTY_CRIMINAL_SEARCH => [ CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN, CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR ],
        self::CREDIT => [ CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW, CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR ],
        self::PRECISE_ID => [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ID_VERIFICATION_PENDING, CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN, CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR, CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW ],
        self::INCOME => [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR ],
	    self::CRIMINAL => [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR ],
        self::EVICTION => [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR ],
        self::RENTAL_HISTORY => [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR ],
        self::PASSPORT_VISA_VERIFICATION => [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR ],
        self::DO_NOT_RENT => [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR ],
	    self::ATTRIBUTE_BASED => [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR ],
		self::VERIFICATION_OF_INCOME => [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR, CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW, CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN ]
	];


    public static $c_arrintConcurrentScreenType = [ self::PRECISE_ID_AO_SCORE, self::PRECISE_ID_OTP, self::PRECISE_ID_KIQ, self::CREDIT ];

    public static $c_arrintReconciliationScreenTypes = [ self::CREDIT ];

	public static $c_arrintMandatoryEmailValidationScreenTypes = [ Self::PRECISE_ID, self::VERIFICATION_OF_INCOME ];

	public static $c_arrintReSendEmailScreenTypeIds = [
		self::PRECISE_ID => [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ID_VERIFICATION_PENDING ],
		self::VERIFICATION_OF_INCOME => [ CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW ]
	];

	public static $c_arrintReInitiateEmailScreenTypeIds = [
		self::VERIFICATION_OF_INCOME => [ CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED, CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED, CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED ]
	];

	public static $c_arrintEnqueueScreeningRequestScreenTypeIds = [
		self::VERIFICATION_OF_INCOME
	];

	public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
                break;

            default:
                $boolIsValid = true;
                break;
        }

        return $boolIsValid;
    }

    public static function getScreenTypeName( $intScreenTypeId ) {

        $strScreenTypeName = '';

        switch( $intScreenTypeId ) {
            case self::CREDIT:
                $strScreenTypeName = __( 'Credit' );
                break;

            case self::CRIMINAL:
                $strScreenTypeName = __( 'Criminal' );
                break;

            case self::STATE_CRIMINAL_SEARCH:
                $strScreenTypeName = __( 'State - Manual Criminal' );
                break;

            case self::COUNTY_CRIMINAL_SEARCH:
                $strScreenTypeName = __( 'County Criminal' );
                break;

            case self::EVICTION:
                $strScreenTypeName = __( 'Evictions, Filings and Public Records' );
                break;

	        case self::INCOME:
		        $strScreenTypeName = __( 'Income' );
		        break;

	        case self::CRIMINAL_CATEGORIZATION:
		        $strScreenTypeName = __( 'Criminal Categorization' );
		        break;

	        case self::DO_NOT_RENT:
		        $strScreenTypeName = __( 'Rental History Watchlist' );
		        break;

	        case self::RENTAL_HISTORY:
		        $strScreenTypeName = __( 'Rental History' );
		        break;

	        case self::PASSPORT_VISA_VERIFICATION:
		        $strScreenTypeName = __( 'Passport Visa verification' );
		        break;

	        case self::CORPORATE_VERIFICATION:
		        $strScreenTypeName = __( 'Corporate Verification' );
		        break;

            case self::BUSINESS_PREMIER_PROFILE:
                $strScreenTypeName = __( 'Business Premier Profile' );
                break;

            case self::BUSINESS_CREDIT:
                $strScreenTypeName = __( 'Business Credit' );
                break;

            case self::FRAUD_SHIELD:
		        $strScreenTypeName = __( 'Fraud Shield' );
		        break;

            case self::PRECISE_ID:
                $strScreenTypeName = __( 'Precise ID' );
                break;

            case self::PRECISE_ID_AO_SCORE:
                $strScreenTypeName = __( 'AO Score' );
                break;

            case self::PRECISE_ID_OTP:
                $strScreenTypeName = __( 'Phone Verification' );
                break;

            case self::PRECISE_ID_KIQ:
                $strScreenTypeName = __( 'Knowledge IQ' );
                break;

	        case self::ATTRIBUTE_BASED:
		        $strScreenTypeName = __( 'Attribute Based' );
		        break;

	        case self::VERIFICATION_OF_INCOME:
		        $strScreenTypeName = __( 'Verification Of Income' );
		        break;

            default:
            	// default case
                break;
        }

        return $strScreenTypeName;
    }

}
?>