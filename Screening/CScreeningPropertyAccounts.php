<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPropertyAccounts
 * Do not add any new functions to this class.
 */

class CScreeningPropertyAccounts extends CBaseScreeningPropertyAccounts {

	public static function fetchPublishedScreeningPropertyAccounts( $objDatabase ) {

		$strSql = 'SELECT * FROM screening_property_accounts WHERE is_published = 1';

		return parent::fetchScreeningPropertyAccounts( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPropertyAccountsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM screening_property_accounts WHERE cid = ' . ( int ) $intCid . '  AND is_published = 1';

		return parent::fetchScreeningPropertyAccounts( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPropertyAccountsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT * FROM screening_property_accounts WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND cid = ' . ( int ) $intCid . '  AND is_published = 1';

		return parent::fetchScreeningPropertyAccounts( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPropertyAccountByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		if( true == empty( $intPropertyId ) ) return NULL;
		$strSql = 'SELECT * FROM screening_property_accounts WHERE property_id =' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid . '  AND is_published = 1';
		return parent::fetchScreeningPropertyAccount( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningPropertyAccountsByCid( $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
			            DISTINCT ( s.property_id ),
                        spa.property_name,
                        spa.zipcode
                    FROM
                        screenings s
                        JOIN screening_property_accounts AS spa ON ( s.cid = spa.cid AND spa.property_id = s.property_id AND spa.property_name IS NOT NULL )
                    WHERE
                        s.cid = ' . ( int ) $intCid . '
					ORDER BY spa.property_name';

		return self::fetchScreeningPropertyAccounts( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPropertyAccountsByScreeningAccountIdByCid( $intScreeningAccountId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intScreeningAccountId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM 
						screening_property_accounts
					WHERE 
						cid = ' . ( int ) $intCid . '
						AND screening_account_id =' . ( int ) $intScreeningAccountId . '
						AND is_published = 1';

		return parent::fetchScreeningPropertyAccounts( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPropertyAccountsByPropertyAccountIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						spa.*
					FROM 
						screening_property_accounts spa 
						LEFT JOIN screening_accounts sa ON(sa.id = spa.screening_account_id and sa.cid = spa.cid)
					WHERE 
						spa.cid = ' . ( int ) $intCid . '
						AND spa.property_id IN(' . sqlIntImplode( $arrintPropertyIds ) . ')
						AND sa.is_active = true
						AND spa.is_published = 1';

		return parent::fetchScreeningPropertyAccounts( $strSql, $objDatabase );
	}

}
?>