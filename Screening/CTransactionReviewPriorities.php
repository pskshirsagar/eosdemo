<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CTransactionReviewPriorities
 * Do not add any new functions to this class.
 */

class CTransactionReviewPriorities extends CBaseTransactionReviewPriorities {

	public static function fetchTransactionReviewPriority( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTransactionReviewPriority', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>