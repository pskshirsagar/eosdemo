<?php

class CScreeningDataRequestType {

	const REVIEW_RESULT 		= 1;
	const CLIENT_ADMIN 			= 2;
	const SCREENING_REPORT 		= 3;
	const MANUAL_REVIEW			= 4;
	const EVALUATION			= 5;
	const ADVERSE_ACTION_LETTER = 6;

}
?>