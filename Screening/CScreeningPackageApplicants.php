<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageApplicants
 * Do not add any new functions to this class.
 */

class CScreeningPackageApplicants extends CBaseScreeningPackageApplicants {

	public static function fetchScreeningPackageApplicantsByScreeningPackageIds( $arrintScreeningPackageIds, $objDatabase ) {
		if( false == valArr( $arrintScreeningPackageIds ) ) return NULL;

		$strSql = 'SELECT
						spa.*,
						CASE sp.screening_package_type_id 
						WHEN ' . CScreeningPackageType::AFFORDABLE . ' THEN ' . COccupancyType::AFFORDABLE . ' 
						WHEN ' . CScreeningPackageType::MILITARY . ' THEN ' . COccupancyType::MILITARY . ' 
						ELSE ' . COccupancyType::CONVENTIONAL . ' 
						END as occupancy_type_id  
					FROM 
						screening_package_applicants spa
						INNER JOIN screening_packages sp ON ( sp.id = spa.screening_package_id) 
					WHERE
						spa.screening_package_id IN(' . implode( ',', $arrintScreeningPackageIds ) . ')
						AND spa.is_published = 1';

		return self::fetchScreeningPackageApplicants( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPackageApplicantsByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {
		if( false == valId( $intScreeningPackageId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM 
						screening_package_applicants
					WHERE
						is_published = 1
						AND screening_package_id =' . ( int ) $intScreeningPackageId;

		return self::fetchScreeningPackageApplicants( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPackageApplicantByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {
		if( false == valId( $intScreeningPackageId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM 
						screening_package_applicants
					WHERE
						is_published = 1
						AND screening_package_id =' . ( int ) $intScreeningPackageId;

		return self::fetchScreeningPackageApplicant( $strSql, $objDatabase );
	}

}
?>