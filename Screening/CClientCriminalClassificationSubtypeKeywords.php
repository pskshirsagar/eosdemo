<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CClientCriminalClassificationSubtypeKeywords
 * Do not add any new functions to this class.
 */

class CClientCriminalClassificationSubtypeKeywords extends CBaseClientCriminalClassificationSubtypeKeywords {

	public static function fetchAllPublishedClientCriminalClassificationSubtypeKeywordsByCidByClassificationTypeId( $intCriminalClassificationTypeId, $intClientId, $objDatabase ) {
		$strSql = ' SELECT 
                          csk.*
					FROM
					     client_criminal_classification_subtype_keywords csk
					     JOIN criminal_classification_keywords cck  ON ( csk.criminal_classification_keyword_id =  cck.id )
					WHERE
						 csk.is_published = 1
						 AND cck.classification_type_id = ' . ( int ) $intCriminalClassificationTypeId . '
					     AND csk.cid = ' . ( int ) $intClientId;

		return parent::fetchClientCriminalClassificationSubtypeKeywords( $strSql, $objDatabase );
	}

	public static function fetchAllPublishedClientCriminalClassificationSubtypeKeywordsByCid( $intClientId, $objDatabase ) {
		$strSql = ' SELECT 
                          *
					FROM
					     client_criminal_classification_subtype_keywords
					WHERE
						 is_published = 1
					     AND cid = ' . ( int ) $intClientId;

		return parent::fetchClientCriminalClassificationSubtypeKeywords( $strSql, $objDatabase );
	}

	public static function fetchAllPublishedClientCriminalClassificationSubtypeKeywordsByScreeningCriteriaIdByCid( $intScreeningCriteriaId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
                          cccsk.*
					FROM
					     client_criminal_classification_subtype_keywords cccsk
					     JOIN criminal_criteria_settings ccs ON ccs.client_criminal_classification_subtype_id = cccsk.client_criminal_classification_subtype_id
					WHERE
					     cccsk.is_published = 1
					     AND cccsk.cid = ' . ( int ) $intCid . '
				         AND ccs.screening_criteria_id = ' . ( int ) $intScreeningCriteriaId;

		return parent::fetchClientCriminalClassificationSubtypeKeywords( $strSql, $objDatabase );
	}

	public static function fetchAllPublishedClientCriminalClassificationSubtypeKeywordsByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {

		$strSql = ' SELECT
                          cccsk.*
					FROM
					     client_criminal_classification_subtype_keywords cccsk
					     JOIN criminal_criteria_settings ccs ON ccs.client_criminal_classification_subtype_id = cccsk.client_criminal_classification_subtype_id
					WHERE
					     cccsk.is_published = 1	
				         AND ccs.screening_criteria_id = ' . ( int ) $intScreeningCriteriaId;

		return parent::fetchClientCriminalClassificationSubtypeKeywords( $strSql, $objDatabase );
	}

}
?>