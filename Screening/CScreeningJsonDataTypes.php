<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningJsonDataTypes
 * Do not add any new functions to this class.
 */

class CScreeningJsonDataTypes extends CBaseScreeningJsonDataTypes {

	public static function fetchScreeningJsonDataTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CScreeningJsonDataType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningJsonDataType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CScreeningJsonDataType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>