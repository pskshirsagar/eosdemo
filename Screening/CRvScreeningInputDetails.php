<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CRvScreeningInputDetails
 * Do not add any new functions to this class.
 */

class CRvScreeningInputDetails extends CBaseRvScreeningInputDetails {

    public static function fetchNonExecutedRvScreeningInputDetailsByCids( $arrintCids, $objDatabase ) {

        $strSql = 'SELECT
						*
					FROM
						rv_screening_input_details rsid
					WHERE
						rsid.is_executed = false
						AND cid IN ( ' . implode( ',', $arrintCids ) . ' )';

        return parent::fetchRvScreeningInputDetails( $strSql, $objDatabase );
    }

}
?>