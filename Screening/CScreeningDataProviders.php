<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningDataProviders
 * Do not add any new functions to this class.
 */

class CScreeningDataProviders extends CBaseScreeningDataProviders {

	public static function fetchPublishedScreeningDataProviders( $objDatabase ) {
		$strSql = 'SELECT * FROM screening_data_providers WHERE is_published = 1 ORDER BY id';
		return parent::fetchScreeningDataProviders( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningDataProvidersByScreeningDataProviderProductTypeIdByTestTypeId( $objDatabase, $intScreeningDataProviderProductTypeId, $intDataProviderTestTypeId = NULL ) {

		$strAndCondition = ( false == is_null( $intDataProviderTestTypeId ) ) ? ' sdpt.is_test_type =  ' . ( int ) $intDataProviderTestTypeId . ' AND ' : '';

		$strSql = 'SELECT
						sdp.*,
						sdp.screening_data_provider_type_id,
						sdpt.is_test_type
					FROM
						screening_data_providers sdp
						JOIN screening_data_provider_types sdpt ON ( sdpt.id = sdp.screening_data_provider_type_id )
					WHERE
						' . $strAndCondition . '
						sdp.is_published = 1
						AND sdp.screening_data_provider_product_type_id =  ' . ( int ) $intScreeningDataProviderProductTypeId . '
						order by sdp.id';

		return parent::fetchScreeningDataProviders( $strSql, $objDatabase );
	}

	public static function fetchAllScreeningDataProviders( $objDatabase ) {
		$strSql = 'SELECT
						sdp.*,
						sdpt.is_test_type
					FROM
						screening_data_providers sdp
					JOIN screening_data_provider_types sdpt ON ( sdpt.id = sdp.screening_data_provider_type_id )
					 order by sdp.id';

		return parent::fetchScreeningDataProviders( $strSql, $objDatabase );
	}

	public static function fetchLatestScreeningDataProviderByScreeningDataProviderTypeIdByScreeningDataProviderProductTypeId( $intScreeningDataProviderTypeId, $intScreeningDataProviderProductTypeId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						screening_data_providers
					WHERE
						screening_data_provider_type_id = ' . ( int ) $intScreeningDataProviderTypeId . ' 
						AND screening_data_provider_product_type_id = ' . ( int ) $intScreeningDataProviderProductTypeId . ' 
						ORDER BY ID DESC LIMIT 1 ';

		return parent::fetchScreeningDataProvider( $strSql, $objDatabase );
	}

	public static function fetchScreeningDataProviderByScreenTypeIdByScreeningDataProviderProductTypeId( $intScreenTypeId, $intScreeningDataProviderProductTypeId, $objDatabase, $intIsTestClient ) {
		$strSql = 'SELECT
						sdp.id,
						sdp.screening_data_provider_type_id,
						sdp.name
					FROM
						screening_data_providers sdp
						JOIN screening_data_provider_types sdpt ON ( sdpt.id = sdp.screening_data_provider_type_id )
					WHERE
						sdpt.is_test_type = ' . ( int ) $intIsTestClient . '
						AND sdp.screen_type_id = ' . ( int ) $intScreenTypeId . '
						AND sdp.screening_data_provider_product_type_id = ' . ( int ) $intScreeningDataProviderProductTypeId . '
						AND sdp.is_published=1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningDataProviderByScreeningDataProviderProductTypeIdByScreenTypeIds( $intScreeningDataProviderProductTypeId, $arrintScreenTypeIds, $objDatabase, $intIsTestClient ) {
		if( false == valArr( $arrintScreenTypeIds ) ) return;
		$strSql = 'SELECT
						sdp.id,
						sdp.name,
						sdp.screen_type_id
					FROM
						screening_data_providers sdp
					JOIN screening_data_provider_types sdpt ON ( sdpt.id = sdp.screening_data_provider_type_id )
					WHERE
						sdpt.is_test_type = ' . ( int ) $intIsTestClient . '
						AND sdp.screen_type_id IN( ' . implode( ',', $arrintScreenTypeIds ) . '  )
						AND sdp.screening_data_provider_product_type_id = ' . ( int ) $intScreeningDataProviderProductTypeId . '
						AND sdp.is_published=1';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningDataProviderByScreenTypeIdByScreeningDataProviderTypeIdByScreeningDataProviderProductTypeId( $intScreenTypeId, $intScreeningDataProviderTypeId, $intScreeningDataProviderProductTypeId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_data_providers
					WHERE
						screen_type_id = ' . ( int ) $intScreenTypeId . '
						AND screening_data_provider_type_id = ' . ( int ) $intScreeningDataProviderTypeId . '
						AND screening_data_provider_product_type_id = ' . ( int ) $intScreeningDataProviderProductTypeId . '
						AND is_published = 1';

		return parent::fetchScreeningDataProvider( $strSql, $objDatabase );
	}

}
?>