<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningScreenTypeVolumeReports
 * Do not add any new functions to this class.
 */

class CScreeningScreenTypeVolumeReports extends CBaseScreeningScreenTypeVolumeReports {

	public static function fetchScreeningScreenTypeVolumeReports( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CScreeningScreenTypeVolumeReport::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningScreenTypeVolumeReport( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CScreeningScreenTypeVolumeReport::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningScreenTypeVolumeReportByScreeningRecordSearchFilter( $objCScreeningReportSearchFilter, $objDatabase, $strReportName = '' ) {

		if( false == valObj( $objCScreeningReportSearchFilter, CScreeningReportSearchFilter::class ) ) {
			return NULL;
		}

		$strWhere = '';
		if( CScreeningApplicationVolumeReport::REPORT_NAME == $strReportName && false == $objCScreeningReportSearchFilter->getIsPropertySearchFilter() ) {
			if( true == valArr( $objCScreeningReportSearchFilter->getCids() ) ) {
				$strWhere .= ' cid IN( ' . sqlIntImplode( $objCScreeningReportSearchFilter->getCids() ) . ') ';
			} else {
				return NULL;
			}
		}else if( true == valId( $objCScreeningReportSearchFilter->getCid() ) ) {
			$strWhere .= ' cid = ' . ( int ) $objCScreeningReportSearchFilter->getCid();

		} else {
			return NULL;
		}

		if( true == $objCScreeningReportSearchFilter->getIsPropertySearchFilter() && true == valArr( $objCScreeningReportSearchFilter->getPropertyIds() ) ) {
			$strWhere .= ' AND property_id IN (' . sqlIntImplode( $objCScreeningReportSearchFilter->getPropertyIds() ) . ') ';
		}

		if( true == valArr( $objCScreeningReportSearchFilter->getScreenTypeIds() ) ) {
			$strWhere .= ' AND screen_type_id IN (' . sqlIntImplode( $objCScreeningReportSearchFilter->getScreenTypeIds() ) . ') ';
		}

		if( true == valStr( $objCScreeningReportSearchFilter->getMonthYear() ) ) {
			$strWhere .= ' AND report_month = DATE_TRUNC(\'month\', TIMESTAMP \'' . ( string ) $objCScreeningReportSearchFilter->getMonthYear() . '\') ';
		}

		if( CScreeningApplicationVolumeReport::REPORT_NAME == $strReportName && false == $objCScreeningReportSearchFilter->getIsPropertySearchFilter() ) {
			$strFields = 'cid, report_month, screen_type_id, sum(total_transactions) AS total_transactions, sum(revenue) AS revenue, sum(total_pass) AS total_pass, sum(total_conditional) AS total_conditional, sum(total_fail) AS total_fail, sum(total_inprogress) AS total_inprogress, sum(total_error) AS total_error';
			$strGroupBy = 'GROUP BY cid, report_month, screen_type_id';
			$strOrderBy = 'cid, screen_type_id';
		} else {
			$strFields = '*';
			$strGroupBy = '';
			$strOrderBy = 'cid, property_id, screen_type_id';
		}

		$strSql = 'SELECT
						' . $strFields . '
				   FROM 
				       screening_screen_type_volume_reports
				   WHERE
				        ' . $strWhere . '
				        ' . $strGroupBy . '
				   ORDER BY
				       ' . $strOrderBy . ';';
		return fetchData( $strSql, $objDatabase );
	}

}
?>