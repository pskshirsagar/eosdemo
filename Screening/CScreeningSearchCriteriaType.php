<?php

class CScreeningSearchCriteriaType extends CBaseScreeningSearchCriteriaType {

	const ALWAYS 			= 1;
	const WHEN_REPORTED 	= 2;
	const PREVIOUS_ADDRESS 	= 3;
	const RV_RECOMMENDATION = 4;
	const CURRENT_ADDRESS 	= 5;
	const PROPERTY_ADDRESS 	= 6;

	public static $c_arrintScreeningSearchCriteriaTypeIds = array(
		self::ALWAYS,
		self::WHEN_REPORTED,
		self::PREVIOUS_ADDRESS,
		self::RV_RECOMMENDATION,
		self::CURRENT_ADDRESS
	);

	public static $c_arrintParentScreenTypeIds = array(
		self::WHEN_REPORTED => array( CScreenType::CRIMINAL ),
		self::PREVIOUS_ADDRESS => array( CScreenType::CREDIT )
	);

	public static $c_arrintScreenTypeSearchTypeIds = array(
		CScreenType::CRIMINAL => self::WHEN_REPORTED,
		CScreenType::CREDIT   => self::PREVIOUS_ADDRESS
	);

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequiredScreenTypes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>