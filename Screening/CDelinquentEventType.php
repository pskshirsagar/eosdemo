<?php

class CDelinquentEventType extends CBaseDelinquentEventType {

	const MOVED_OUT_WITH_EVICTION 						= 1;
	const FRAUDULENT_ACTIVITY_IDENTIFICATION			= 2;
	const FRAUDULENT_ACTIVITY_DATE_OF_BIRTH				= 3;
	const FRAUDULENT_ACTIVITY_PROOF_OF_INCOME			= 4;
	const FRAUDULENT_ACTIVITY_PROOF_OF_EMPLOYMENT		= 5;
	const FRAUDULENT_ACTIVITY_PAYMENT					= 6;
	const MOVED_OUT_WITH_BALANCE						= 7;
	const MOVED_OUT_WITH_SKIPS							= 8;
	const CONTRACTUAL_AGREEMENT							= 9;
	const LEASE_VIOLATION								= 10;
	const NON_RENEWAL									= 11;

    // These events gets generated through residents tab
    public static $c_arrintSystemDelinquentEventTypeIds = array(
        self::MOVED_OUT_WITH_EVICTION,
        self::MOVED_OUT_WITH_BALANCE,
        self::MOVED_OUT_WITH_SKIPS
    );

    // These events are added through lead tab
    public static $c_arrintManualDelinquentEventTypeIds = array(
        self::FRAUDULENT_ACTIVITY_IDENTIFICATION,
        self::FRAUDULENT_ACTIVITY_DATE_OF_BIRTH,
        self::FRAUDULENT_ACTIVITY_PROOF_OF_INCOME,
        self::FRAUDULENT_ACTIVITY_PROOF_OF_EMPLOYMENT,
        self::FRAUDULENT_ACTIVITY_PAYMENT,
        self::CONTRACTUAL_AGREEMENT
    );

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>