<?php

class CScreeningPackageConditionSet extends CBaseScreeningPackageConditionSet {

    public function valConditionName() {
        $boolIsValid = true;
    	if( true == is_null( $this->getConditionName() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'conditionset_name', 'Condition Name is required.' ) );
        }
		return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valConditionName();
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public static function updateEvalutionOrder( $intEvaluationOrder, $intScreeningPackageConditionSetId, $objDatabase ) {
        $strSql = ' UPDATE
                        screening_package_condition_sets
                    SET
                        evaluation_order =  ' . ( int ) $intEvaluationOrder . '
                    WHERE
                        id = ' . ( int ) $intScreeningPackageConditionSetId;

        $arrstrData = fetchData( $strSql, $objDatabase );

        return ( true == valArr( $arrstrData ) ) ? true : false;
    }

    public static function getNextEvaluationOrderId( $intScreeningPackageId, $objDatabase ) {
        $strSql = ' SELECT
                        max(evaluation_order) + 1 as evaluation_order_id
                    FROM
                        screening_package_condition_sets
                    WHERE
                        screening_package_id = ' . ( int ) $intScreeningPackageId . ' AND
                        screening_recommendation_type_id != ' . ( int ) CScreeningRecommendationType::PASS;

        $arrstrData = fetchData( $strSql, $objDatabase );

        $intEvaluationOrder = ( true == valArr( $arrstrData ) && true == array_key_exists( 'evaluation_order_id', $arrstrData[0] ) ) ? $arrstrData[0]['evaluation_order_id'] : 1;

        return $intEvaluationOrder;
    }

}
?>