<?php

class CDisputeType extends CBaseDisputeType {
	const CREDIT 	= 1;
	const CRIMINAL  = 2;
	const EVICTION  = 3;
	const OTHER 	= 4;
	const DNR 	    = 5;
	const RENT_BUREAU = 6;
	const EXPERIAN_CONSUMER_DISPUTE_VERIFICATION_FORM = 7;
	const INTERNAL_CRIMINAL = 8;
	const INTERNAL_EVICTION = 9;
	const NO_PII = 10;
	const MAIL_WITH_PII = 11;

	public static $c_arrintIncludeDisputeTypeIds = array(
		self::CREDIT,
		self::CRIMINAL,
		self::EVICTION,
		self::OTHER,
		self::DNR,
		self::RENT_BUREAU,
		self::EXPERIAN_CONSUMER_DISPUTE_VERIFICATION_FORM,
		self::INTERNAL_CRIMINAL,
		self::INTERNAL_EVICTION,
		self::NO_PII,
		self::MAIL_WITH_PII
	);

	public static $c_arrintExternalDisputeTypeIds = array(
		self::CREDIT,
		self::CRIMINAL,
		self::EVICTION,
		self::OTHER,
		self::DNR,
		self::RENT_BUREAU
	);

	public static $c_arrintInternalDisputeTypeIds = array(
		self::EXPERIAN_CONSUMER_DISPUTE_VERIFICATION_FORM,
		self::INTERNAL_CRIMINAL,
		self::INTERNAL_EVICTION,
		self::NO_PII,
		self::MAIL_WITH_PII
	);

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getDisputeTypeText( $intDisputeTypeId ) {
		$strDisputeTypeText = '';

		switch( $intDisputeTypeId ) {
			case self::CREDIT:
				$strDisputeTypeText = 'Credit';
				break;

			case self::CRIMINAL:
				$strDisputeTypeText = 'Criminal';
				break;

			case self::EVICTION:
				$strDisputeTypeText = 'Eviction';
				break;

			case self::OTHER:
				$strDisputeTypeText = 'Other';
				break;

			case self::DNR:
				$strDisputeTypeText = 'Rental History Watchlist';
				break;

			case self::RENT_BUREAU:
				$strDisputeTypeText = 'Rent Bureau';
				break;

			case self::EXPERIAN_CONSUMER_DISPUTE_VERIFICATION_FORM:
				$strDisputeTypeText = 'Experian Consumer Dispute Verification Form';
				break;

			case self::INTERNAL_CRIMINAL:
				$strDisputeTypeText = 'Internal Criminal';
				break;

			case self::INTERNAL_EVICTION:
				$strDisputeTypeText = 'Internal Eviction';
				break;

			case self::NO_PII:
				$strDisputeTypeText = 'No PII';
				break;

			case self::MAIL_WITH_PII:
				$strDisputeTypeText = 'Mail With PII';
				break;

			default:
				// added default case
		}

		return $strDisputeTypeText;
	}

}
?>