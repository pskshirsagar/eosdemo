<?php

class CScreeningApplicant extends CBaseScreeningApplicant {

	protected $m_intMonthlyRent;
    protected $m_intCustomApplicationId;
	protected $m_intPropertyId;
    protected $m_intScreeningCumulationTypeId;
    protected $m_intExistingScreeningApplicantId;
	protected $m_intScreeningPackageTypeId;
	protected $m_intDisputedApplicantId;

    protected $m_strScreeningApplicantType;
    protected $m_strScreeningPackageExternalName;
	protected $m_strPropertyName;
	protected $m_strPreviousAddressLine;
	protected $m_strPreviousCity;
	protected $m_strPreviousState;
	protected $m_strPreviousZipcode;

    protected $m_arrobjScreeningTransactions;
	protected $m_arrobjTestDataProviders;

    protected $m_boolIsUseForPreScreening;
    protected $m_boolIsAllMustPassCreditAndIncome;

    protected $m_boolIsManualCriminalSearchEnabled;

    protected $m_boolIsUseForRvIndexScore;

    protected $m_strTaxNumber;

    /*
     * Set functions.
     */

    private function setTaxNumber( $strTaxNumber ) {
    	$this->m_strTaxNumber = $strTaxNumber;
    }

    public function getTaxNumber() {
    	return $this->m_strTaxNumber;
    }

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

    public function setIsManualCriminalSearchEnabled( $boolIsManualCriminalSearchEnabled ) {
    	$this->m_boolIsManualCriminalSearchEnabled = $boolIsManualCriminalSearchEnabled;
    }

    public function setIsUseForRvIndexScore( $boolIsUseForRvIndexScore ) {
    	$this->m_boolIsUseForRvIndexScore = $boolIsUseForRvIndexScore;
    }

    public function setDisputedApplicantId( $objDatabase ) {
	    $arrintScreeningApplicantDisputedIds = $this->getDisputedScreeningApplicantIds( $objDatabase );
	    $this->m_intDisputedApplicantId = $arrintScreeningApplicantDisputedIds[CScreenType::EVICTION]['id'];

    }

	public function setPreviousAddressLine( $strPreviousAddressLine ) {
		$this->m_strPreviousAddressLine = $strPreviousAddressLine;
	}

	public function setPreviousCity( $strPreviousCity ) {
		$this->m_strPreviousCity = $strPreviousCity;
	}

	public function setPreviousState( $strPreviousState ) {
		$this->m_strPreviousState = $strPreviousState;
	}

	public function setPreviousZipcode( $strPreviousZipcode ) {
		$this->m_strPreviousZipcode = $strPreviousZipcode;
	}

    /*
     * ends here
     */

    /*
     * Get function
     */

    public function getIsManualCriminalSearchEnabled() {
    	return $this->m_boolIsManualCriminalSearchEnabled;
    }

    public function getBirthDateDecrypted() {
    	return base64_decode( $this->m_strBirthDateEncrypted );
    }

    public function getIsUseForRvIndexScore() {
    	return $this->m_boolIsUseForRvIndexScore;
    }

    public function getDisputedApplicantId() {
    	return $this->m_intDisputedApplicantId;
    }

    /*
     * ends here
     */

    // New functions

	public function createScreeningTransactions( $intCurrentUserId, $intPropertyId, $arrobjScreeningAccountRates, $objDatabase, $intPreScreeningSetId = CPrescreeningSet::SET1 ) {

		$objScreeningPackage = CScreeningPackages::fetchPublishedScreeningPackageByIdByCid( $this->getScreeningPackageId(), $this->getCid(), $objDatabase );

		if( false == valObj( $objScreeningPackage, 'CScreeningPackage' ) ) {
			trigger_error( 'Failed to load screening package for screening applicant id : ' . $this->getId() . ' cid : ' . $this->getCid(), E_USER_WARNING );
			return false;
		}

		$boolIsPreScreeningEnabled = CScreeningPackages::isPackagePreScreeningEnabled( $this->getScreeningPackageId(), $objDatabase );

		$arrobjScreeningPackageScreenTypes = CScreeningPackageScreenTypeAssociations::fetchPublishedScreeningPackageAssociatedScreenTypesByScreeningPackageId( $this->getScreeningPackageId(), $objDatabase, $boolIsPreScreeningEnabled, $intPreScreeningSetId );
		$arrobjScreeningPackageScreenTypes = rekeyObjects( 'ScreenTypeId', $arrobjScreeningPackageScreenTypes );

		if( false == valArr( $arrobjScreeningPackageScreenTypes ) ) {
			trigger_error( 'Failed to load screening package screen type for screening package id : ' . $this->getScreeningPackageId() . ' cid : ' . $this->getCid(), E_USER_WARNING );
			return false;
		}

		$arrobjExistingScreeningTransactions = $this->fetchAllScreeningApplicantTransactions( $objDatabase );

		$this->setScreeningTransactions( $arrobjExistingScreeningTransactions );

		$arrobjScreeningPackageScreenTypes   = $this->getAdditionalScreenTypeIds( $intCurrentUserId, $arrobjExistingScreeningTransactions, $arrobjScreeningPackageScreenTypes, $objDatabase );

		if( 'production' != CONFIG_ENVIRONMENT ) {
			$this->m_arrobjTestDataProviders = CScreeningDataProviders::fetchPublishedScreeningDataProvidersByScreeningDataProviderProductTypeIdByTestTypeId( $objDatabase, CScreeningDataProviderProductType::RESIDENT_VERIFY, 1 );
		}

		if( true == valArr( $arrobjScreeningPackageScreenTypes ) ) {
			foreach( $arrobjScreeningPackageScreenTypes as $objScreeningPackageScreenType ) {
				if( true == in_array( $objScreeningPackageScreenType->getScreenTypeId(), CScreenType::$c_arrintManualCriminalSearchScreenTypeIds ) ) {
					$intCriminalSearchPreScreeningSetId = $objScreeningPackageScreenType->getPrescreeningSetId();
					continue;
				}
				$this->insertScreeningTransaction( $intCurrentUserId, $intPropertyId, $objScreeningPackageScreenType, $arrobjScreeningAccountRates, $objDatabase );
			}
		}

		if( false == $boolIsPreScreeningEnabled ) {
			$this->createDependantScreeningTransactions( $intCurrentUserId, $intPropertyId, $arrobjScreeningAccountRates, array( CScreeningSearchCriteriaType::ALWAYS, CScreeningSearchCriteriaType::CURRENT_ADDRESS, CScreeningSearchCriteriaType::PROPERTY_ADDRESS ), NULL, $objDatabase );
		}

		$this->setScreeningTransactions( $this->fetchActiveScreeningApplicantTransactions( $objDatabase ) );
		return;
	}

	public function createCorporateVerificationTransaction( $intCurrentUserId, $arrobjScreeningAccountRates, $objDatabase, $boolCanReUseApplicant = true ) {

		$arrobjScreeningTransaction = $this->fetchActiveScreeningApplicantTransactions( $objDatabase );

		if( true == valArr( $arrobjScreeningTransaction ) ) {
			$objScreeningTransaction = current( $arrobjScreeningTransaction );

			if( true == $boolCanReUseApplicant ) {
				$this->setScreeningTransactions( $arrobjScreeningTransaction );
				return true;
			} else {
				CScreeningTransactions::updateExistingScreeningTransactionsStatus( $intCurrentUserId, $objScreeningTransaction->getScreenTypeId(), CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED, $this->getScreeningId(), $this->getId(), $objDatabase );
			}
		}

		$objScreeningDataProvider = CScreeningDataProviders::fetchScreeningDataProviderByScreenTypeIdByScreeningDataProviderTypeIdByScreeningDataProviderProductTypeId( CScreenType::CORPORATE_VERIFICATION, CScreeningDataProviderType::RESIDENT_VERIFY_TEST, CScreeningDataProviderProductType::RESIDENT_VERIFY, $objDatabase );

		$objScreeningPackageScreenType = new CScreeningPackageScreenTypeAssociation();
		$objScreeningPackageScreenType->setScreenTypeId( CScreenType::CORPORATE_VERIFICATION );
		$objScreeningPackageScreenType->setScreeningDataProviderId( $objScreeningDataProvider->getId() );

		$objScreeningTransaction = $this->insertScreeningTransaction( $intCurrentUserId, 0, $objScreeningPackageScreenType, $arrobjScreeningAccountRates, $objDatabase );

		$this->setScreeningTransactions( $this->fetchActiveScreeningApplicantTransactions( $objDatabase ) );

		$this->updateScreeningApplicantResult( $intCurrentUserId, $this->getScreeningTransactions(), $objDatabase );
	}

	public function insertScreeningTransaction( $intCurrentUserId, $intPropertyId, $objScreeningPackageScreenType, $arrobjScreeningAccountRates, $objDatabase, $intScreeningStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN, $strScreeningSearch = NULL ) {

		$objScreeningAccountRate = ( true == in_array( $objScreeningPackageScreenType->getScreenTypeId(), CScreenType::$c_arrintPreciseIdChildScreenTypeIds ) ) ? getArrayElementByKey( CScreenType::PRECISE_ID, $arrobjScreeningAccountRates ) : getArrayElementByKey( $objScreeningPackageScreenType->getScreenTypeId(), $arrobjScreeningAccountRates );

        $objScreeningDataProviderRate = NULL;

         if( CScreenType::CREDIT == $objScreeningPackageScreenType->getScreenTypeId() ) {
           $objScreeningDataProviderRate = $this->getScreeningAccountRateByDataProviderId( $objScreeningPackageScreenType->getScreeningDataProviderId(), $this->getCid(), $intPropertyId, $objDatabase );
         }

        $objScreeningAccountRate = ( CScreenType::CREDIT == $objScreeningPackageScreenType->getScreenTypeId() && true == valObj( $objScreeningDataProviderRate, 'CScreeningAccountRate' ) ) ? $objScreeningDataProviderRate : $objScreeningAccountRate;

		if( false == valObj( $objScreeningAccountRate, 'CScreeningAccountRate' ) ) return false;

		// IF package rate is Selected then returning;
        if( false == in_array( $objScreeningPackageScreenType->getScreenTypeId(), CScreenType::$c_arrintPreciseIdChildScreenTypeIds ) ) {
            if( ( CScreenRateType::PACKAGE == $objScreeningAccountRate->getScreenRateTypeId() ) || ( false == valObj( $objScreeningPackageScreenType, 'CScreeningPackageScreenTypeAssociation' ) && false == valObj( $objScreeningPackageScreenType, 'CScreeningPackageScreenTypeCriteria' ) ) ) return false;
        }

        if( CScreenType::PRECISE_ID == $objScreeningPackageScreenType->getScreenTypeId() ) {
            $intScreeningStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_ID_VERIFICATION_PENDING;
        }

		$objScreeningTransaction = new CScreeningTransaction();

		// Override the data provider id to test in case of screening performed in lower environment with live data provider_ids
		$intDataProviderId = $objScreeningPackageScreenType->getScreeningDataProviderId();
		if( 'production' != CONFIG_ENVIRONMENT ) {
			if( false == valArr( $this->m_arrobjTestDataProviders ) ) {
				$this->m_arrobjTestDataProviders = CScreeningDataProviders::fetchPublishedScreeningDataProvidersByScreeningDataProviderProductTypeIdByTestTypeId( $objDatabase, CScreeningDataProviderProductType::RESIDENT_VERIFY, 1 );
			}
			if( false == array_key_exists( $intDataProviderId, $this->m_arrobjTestDataProviders ) ) {
				$intDataProviderId = CScreeningUtils::getRVTestCaseDataProviderByScreenTypeId( $this->m_arrobjTestDataProviders, $objScreeningPackageScreenType->getScreenTypeId() );
			}
		}

		$objScreeningTransaction->setCid( $this->getCid() );
		$objScreeningTransaction->setScreeningId( $this->getScreeningId() );
		$objScreeningTransaction->setScreeningApplicantId( $this->getId() );
		$objScreeningTransaction->setPropertyId( $intPropertyId );
		$objScreeningTransaction->setScreeningBatchId( NULL );
		$objScreeningTransaction->setScreenTypeId( $objScreeningPackageScreenType->getScreenTypeId() );
		$objScreeningTransaction->setScreeningStatusTypeId( $intScreeningStatusTypeId );
		$objScreeningTransaction->setScreeningDataProviderId( $intDataProviderId );
		$objScreeningTransaction->setCompanyScreeningAccountId( $objScreeningAccountRate->getScreeningAccountId() );
		$objScreeningTransaction->setScreeningRecommendationTypeId( CScreeningRecommendationType::PENDING_UNKNOWN );

		$objScreeningTransaction->setSearchType( $strScreeningSearch );

		$objScreeningTransaction->setTransactionAmount( ( true == in_array( $objScreeningPackageScreenType->getScreenTypeId(), CScreenType::$c_arrintPreciseIdChildScreenTypeIds ) ) ? 0 : $objScreeningTransaction->getCost( $objScreeningAccountRate, $objDatabase ) );

		if( false == $objScreeningTransaction->insert( $intCurrentUserId, $objDatabase ) ) {
			trigger_error( 'Failed to create screening transaction for screening applicant id : ' . $this->getId() . ' cid : ' . $this->getCid(), E_USER_ERROR );
			return false;
		}

		return $objScreeningTransaction;
	}

    // getBaseRete

    public function getBaseRate( $strState, $strCounty, $intScreenTypeId, $intScreeningDataProviderId, $objDatabase ) {

    	$objScreeningProviderBaseRate = CScreeningProviderBaseRates::fetchScreeningProviderBaseRatesBySearchTypeByScreeningDataProviderId( $strState, $strCounty, $intScreenTypeId, $intScreeningDataProviderId, $objDatabase );

    	if( false == valObj( $objScreeningProviderBaseRate, 'CScreeningProviderBaseRate' ) ) return;

    	return $objScreeningProviderBaseRate->getCost();
    }

    public function determineAndCreateRemainingScreeningTransactions( $intCurrentUserId, $intPropertyId, $arrobjScreeningAccountRates, $objDatabase ) {

    	if( false == valArr( $arrobjScreeningAccountRates ) ) {
    		trigger_error( 'Failed to load screening account rates for cid :' . $this->getCid(), E_USER_WARNING );
    		return false;
    	}

        $intPreScreeningSetId = ( int ) \Psi\Eos\Screening\CScreeningPackageScreenTypeAssociations::createService()->fetchNextPreScreeningSetIdByScreeningPackageIdByScreeningApplicantIdByScreeningIdByCid( $this->getScreeningPackageId(), $this->getId(), $this->getScreeningId(), $this->getCid(), $objDatabase );

    	$boolIsPreScreeningEnabled = CScreeningPackages::isPackagePreScreeningEnabled( $this->getScreeningPackageId(), $objDatabase );

    	$arrobjScreeningPackageScreenTypes = CScreeningPackageScreenTypeAssociations::fetchScreeningPackageAssociatedScreenTypesByScreeningPackageIdByPreScreeningSetId( $this->getScreeningPackageId(), $objDatabase, $intPreScreeningSetId );
    	$arrobjScreeningPackageScreenTypes = rekeyObjects( 'ScreenTypeId', $arrobjScreeningPackageScreenTypes );

    	if( false == valArr( $arrobjScreeningPackageScreenTypes ) ) {
    		trigger_error( 'Failed to load screening package screen types for screening applicant id : ' . $this->getId() . ' screening package id : ' . $this->getScreeningPackageId() . ' pre screening set id : ' . ( int ) $intPreScreeningSetId, E_USER_WARNING );
    		return false;
    	}

    	$arrintScreenTypeIds = array_keys( $arrobjScreeningPackageScreenTypes );

    	$arrobjExistingScreeningTransactions = $this->fetchAllScreeningApplicantTransactionsByScreenTypeIds( $arrintScreenTypeIds, $objDatabase );

    	$arrobjScreeningPackageScreenTypes = $this->getAdditionalScreenTypeIds( $intCurrentUserId, $arrobjExistingScreeningTransactions, $arrobjScreeningPackageScreenTypes, $objDatabase );

    	if( false == valArr( $arrobjScreeningPackageScreenTypes ) ) {
		    $arrobjScreeningTransactions = $this->fetchActiveScreeningApplicantTransactions( $objDatabase );
		    $this->setParentChildTransactions( $arrobjScreeningTransactions );
    		return $this;
    	}

    	foreach( $arrobjScreeningPackageScreenTypes as $objScreeningPackageScreenType ) {
		    if( true == in_array( $objScreeningPackageScreenType->getScreenTypeId(), CScreenType::$c_arrintManualCriminalSearchScreenTypeIds ) ) {
			    $intCriminalSearchPreScreeningSetId = $objScreeningPackageScreenType->getPrescreeningSetId();
			    continue;
		    }

	    	$this->insertScreeningTransaction( $intCurrentUserId, $intPropertyId, $objScreeningPackageScreenType, $arrobjScreeningAccountRates, $objDatabase );
    	}
	    if( $intPreScreeningSetId == $intCriminalSearchPreScreeningSetId ) {
		    $this->createDependantScreeningTransactions( $intCurrentUserId, $intPropertyId, $arrobjScreeningAccountRates, [ CScreeningSearchCriteriaType::ALWAYS, CScreeningSearchCriteriaType::CURRENT_ADDRESS, CScreeningSearchCriteriaType::PROPERTY_ADDRESS ], NULL, $objDatabase );
	    }
	    $arrobjScreeningTransactions = $this->fetchActiveScreeningApplicantTransactions( $objDatabase );
	    $this->setParentChildTransactions( $arrobjScreeningTransactions );

		return $this;
    }

    /*
     * Below function will gets called to create all types of manual criminal search transactions
     * this function gets called using its screening search criteria type id. it will be one of( always, when reported, previous addresses, etc ).
     */

	public function createDependantScreeningTransactions( $intCurrentUserId, $intPropertyId, $arrobjScreeningAccountRates, $arrintScreeningSearchCriteriaTypeIds, $objScreeningTransaction, $objDatabase ) {

		$objScreeningPropertyAccount               = CScreeningPropertyAccounts::fetchPublishedScreeningPropertyAccountByPropertyIdByCid( $intPropertyId, $this->getCid(), $objDatabase );
		$arrobjScreeningPackageScreenTypeCriterias = CScreeningPackageScreenTypeCriterias::fetchScreeningPackageScreenTypeCriteriasByScreeningPackageIdByScreeningSearchCriteriaTypeIds( $this->getScreeningPackageId(), $arrintScreeningSearchCriteriaTypeIds, $objDatabase );

		$arrobjExistingManualCriminalTransactions = $this->fetchAllScreeningApplicantTransactionsByScreenTypeIds( CScreenType::$c_arrintManualCriminalSearchScreenTypeIds, $objDatabase );

		$arrobjExistingManualCriminalTransactions = rekeyObjects( 'SearchType', $arrobjExistingManualCriminalTransactions );

		$arrobjExistingManualCriminalTransactions = rekeyObjects( 'ScreenTypeId', $arrobjExistingManualCriminalTransactions, true );

		if( false == valArr( $arrobjScreeningPackageScreenTypeCriterias ) ) {
			return false;
		}

		$arrobjScreeningTransactions = array();

		foreach( $arrobjScreeningPackageScreenTypeCriterias as $objScreeningPackageScreenTypeCriteria ) {
			$arrobjManualCriminalTransactions = getArrayElementByKey( $objScreeningPackageScreenTypeCriteria->getScreenTypeId(), $arrobjExistingManualCriminalTransactions );

			$arrmixTransactionSearchTypes = $objScreeningPackageScreenTypeCriteria->getSearchTypes( $intCurrentUserId, $objScreeningPackageScreenTypeCriteria->getScreenTypeId(), $objScreeningPropertyAccount, $objDatabase, $objScreeningTransaction, $arrobjManualCriminalTransactions, $this );
			$arrmixSearchTypes = getArrayElementByKey( $objScreeningPackageScreenTypeCriteria->getScreenTypeId(), $arrmixTransactionSearchTypes );
			$arrmixSearchTypes = $this->getAdditionalManualCriminalSearchTypes( $intCurrentUserId, $objScreeningPackageScreenTypeCriteria, $arrobjManualCriminalTransactions, $arrmixSearchTypes, $objDatabase );

			if( false == valArr( $arrmixSearchTypes ) && CScreeningSearchCriteriaType::ALWAYS != $objScreeningPackageScreenTypeCriteria->getScreeningSearchCriteriaTypeId() ) {
				$arrobjScreeningTransactions = array_merge( $arrobjScreeningTransactions, $this->getScreeningTransactionsBySearchTypes( $arrobjManualCriminalTransactions, $arrmixSearchTypes ) );
				continue;
			}

			foreach( $arrmixSearchTypes as $strSearchTypeName ) {
                $arrstrStates = explode( '~', $strSearchTypeName );
                $strSearchState = ( 2 == \Psi\Libraries\UtilFunctions\count( $arrstrStates ) ) ? $arrstrStates[1] : $strSearchTypeName;
				if( false == valStr( $strSearchTypeName ) || ( true == in_array( $strSearchState, CScreeningUtils::SKIP_STATE_COUNTY_SEARCH_RULE_FOR_STATES ) && true == $this->getIsNonUsApplicantAddress() ) ) continue;

				$intScreeningStatusTypeId = ( true == $objScreeningPackageScreenTypeCriteria->getDispatchType() ) ? CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN : CScreeningStatusType::APPLICANT_RECORD_STATUS_DISPATCH_HOLD;

				$arrobjScreeningTransactions[] = $this->insertScreeningTransaction( $intCurrentUserId, $intPropertyId, $objScreeningPackageScreenTypeCriteria, $arrobjScreeningAccountRates, $objDatabase, $intScreeningStatusTypeId, $strSearchTypeName );
			}
		}

		return $arrobjScreeningTransactions;
	}

	public function isRequiredReprocessing( $objDatabase ) {

		$boolIsRequiredReprocessing = true;

		$arrobjRekeyedScreeningTransactions = rekeyObjects( 'ScreenTypeId', $this->getScreeningTransactions() );

		if( false == valArr( $arrobjRekeyedScreeningTransactions ) ) return false;

		$boolIsRequiredReprocessing = ( true == array_key_exists( CScreenType::CRIMINAL, $arrobjRekeyedScreeningTransactions ) && false == getArrayElementByKey( CScreenType::CRIMINAL, $arrobjRekeyedScreeningTransactions )->isManualReviewRequired( $objDatabase ) ) ? true : false;

		return $boolIsRequiredReprocessing;
	}

	public function setNewScreeningTransactions( $arrobjScreeningTransactions ) {

		if( false == valArr( $arrobjScreeningTransactions ) ) return;

		$arrobjScreeningTransactions 		 = rekeyObjects( 'id', $arrobjScreeningTransactions );
		$arrobjExistingScreeningTransactions = rekeyObjects( 'id', $this->getScreeningTransactions() );

		$arrobjDiffScreeningTransactions = array_diff_key( $arrobjScreeningTransactions, $arrobjExistingScreeningTransactions );

		if( false == valArr( $arrobjDiffScreeningTransactions ) ) return;

		$arrobjScreeningApplicantTransactions = array_merge( $arrobjExistingScreeningTransactions, $arrobjDiffScreeningTransactions );

		$this->setScreeningTransactions( $arrobjScreeningApplicantTransactions );

		return;
	}

    // ends here

    public function setScreeningCumulationTypeId( $intScreeningCumulationTypeId ) {
    	$this->m_intScreeningCumulationTypeId = $intScreeningCumulationTypeId;
    }

    public function getScreeningCumulationTypeId() {
    	return $this->m_intScreeningCumulationTypeId;
    }

    public function setIsUseForPreScreening( $boolIsUseForPreScreening ) {
		$this->m_boolIsUseForPreScreening = $boolIsUseForPreScreening;
    }

    public function getIsUseForPreScreening() {
    	return $this->m_boolIsUseForPreScreening;
    }

	public function setScreeningPackageTypeId( $intScreeningPackageTypeId ) {
		$this->m_intScreeningPackageTypeId = $intScreeningPackageTypeId;
	}

	public function getScreeningPackageTypeId() {
		return $this->m_intScreeningPackageTypeId;
	}

    public function setIsAllMustPassCreditAndIncome( $boolIsAllMustPassCreditAndIncome ) {
    	$this->m_boolIsAllMustPassCreditAndIncome = CStrings::strToBool( $boolIsAllMustPassCreditAndIncome );
    }

    public function getIsAllMustPassCreditAndIncome() {
    	return $this->m_boolIsAllMustPassCreditAndIncome;
    }

    public function setScreeningTransactions( $arrobjScreeningTransactions ) {
    	$this->m_arrobjScreeningTransactions = $arrobjScreeningTransactions;
    }

    public function getScreeningTransactions() {
    	return $this->m_arrobjScreeningTransactions;
    }

    public function setMonthlyRent( $intMonthlyRent ) {
        $this->m_intMonthlyRent = $intMonthlyRent;
    }

    public function getMonthlyRent() {
        return $this->m_intMonthlyRent;
    }

    public function setCustomApplicationId( $intCustomApplicationId ) {
        $this->m_intCustomApplicationId = $intCustomApplicationId;
    }

    public function getCustomApplicationId() {
        return $this->m_intCustomApplicationId;
    }

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

    public function setScreeningPackageExternalName( $strScreeningPackageExternalName ) {
        $this->m_strScreeningPackageExternalName = $strScreeningPackageExternalName;
    }

    public function getScreeningPackageExternalName() {
        return $this->m_strScreeningPackageExternalName;
    }

    public function setScreeningApplicantType( $strScreeningApplicantType ) {
        $this->m_strScreeningApplicantType = $strScreeningApplicantType;
    }

    public function getScreeningApplicantType() {
        return $this->m_strScreeningApplicantType;
    }

    public function setExistingScreeningApplicantId( $intExistingScreeningApplicantId ) {
    	$this->m_intExistingScreeningApplicantId = $intExistingScreeningApplicantId;
    }

    public function getExistingScreeningApplicantId() {
    	return $this->m_intExistingScreeningApplicantId;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

        parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

        if( isset( $arrmixValues['custom_application_id'] ) ) $this->setCustomApplicationId( $arrmixValues['custom_application_id'] );
	    if( isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
        if( isset( $arrmixValues['screening_package_external_name'] ) ) $this->setScreeningPackageExternalName( $arrmixValues['screening_package_external_name'] );
        if( isset( $arrmixValues['monthly_rent'] ) ) $this->setMonthlyRent( $arrmixValues['monthly_rent'] );
        if( isset( $arrmixValues['screening_applicant_type'] ) ) $this->setScreeningApplicantType( $arrmixValues['screening_applicant_type'] );
        if( isset( $arrmixValues['screening_cumulation_type_id'] ) ) $this->setScreeningCumulationTypeId( $arrmixValues['screening_cumulation_type_id'] );
        if( isset( $arrmixValues['is_use_for_prescreening'] ) ) $this->setIsUseForPreScreening( $arrmixValues['is_use_for_prescreening'] );
		if( isset( $arrmixValues['is_all_must_pass_credit_and_income'] ) ) $this->setIsAllMustPassCreditAndIncome( $arrmixValues['is_all_must_pass_credit_and_income'] );
        if( isset( $arrmixValues['is_use_for_rv_index_score'] ) ) $this->setIsUseForRvIndexScore( $arrmixValues['is_use_for_rv_index_score'] );
	    if( isset( $arrmixValues['screening_package_type_id'] ) ) $this->setScreeningPackageTypeId( $arrmixValues['screening_package_type_id'] );
	    if( isset( $arrmixValues['previous_address_line'] ) ) $this->setPreviousAddressLine( $arrmixValues['previous_address_line'] );
	    if( isset( $arrmixValues['previous_city'] ) ) $this->setPreviousCity( $arrmixValues['previous_city'] );
	    if( isset( $arrmixValues['previous_state'] ) ) $this->setPreviousState( $arrmixValues['previous_state'] );
	    if( isset( $arrmixValues['previous_zipcode'] ) ) $this->setPreviousZipcode( $arrmixValues['previous_zipcode'] );

	    if( valStr( $arrmixValues['tax_number_encrypted'] ) ) {
		    $this->setTaxNumber( preg_replace( '/[^a-z0-9]/i', '', ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $arrmixValues['tax_number_encrypted'], CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] ) ) );
	    }
        return;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
                break;

            default:
                // need to add
        }

        return $boolIsValid;
    }

    public function getScreeningApplicantResults( $objScreeningDatabase, $boolExcludeFromScreeningDetails = false ) {
        $arrobjScreeningTransactions = CScreeningTransactions::fetchActiveScreeningTransactionsByScreeningIdByScreeningApplicantIdByCid( $this->getScreeningId(), $this->getId(), $this->getCid(), $objScreeningDatabase, $boolExcludeFromScreeningDetails );
        return $arrobjScreeningTransactions;
    }

	public function getSkippedScreeningApplicantTransaction( $objScreeningDatabase, $intScreenTypeId ) {
		$arrobjScreeningTransactions = CScreeningTransactions::fetchSkippedScreeningTransactionByScreeningIdByScreeningApplicantIdByScreenTypeIdByCid( $this->getScreeningId(), $this->getId(), $intScreenTypeId, $this->getCid(), $objScreeningDatabase );
		return $arrobjScreeningTransactions;
	}

    public function updateScreeningTransactionStatus( $intCompanyUserId, $intApplicantScreeningStatus, $arrobjScreeningTransactions, $objScreeningDatabase ) {

        if( false == valArr( $arrobjScreeningTransactions ) ) return;

        foreach( $arrobjScreeningTransactions as $objScreeningTransaction ) {

            $objScreeningTransaction->setScreeningStatusTypeId( $intApplicantScreeningStatus );

            if( false == $objScreeningTransaction->update( $intCompanyUserId, $objScreeningDatabase ) ) {
                trigger_error( 'Failed to update screening status for screening id [' . $this->getScreeningId() . '] for client id [' . $this->getCid() . ']', E_USER_WARNING );
                $objScreeningDatabase->rollback();
            }
        }

        return;
    }

    public function determineAndUpdateScreeningTransactionCompletionStatus( $intCurrentUserId, $objScreeningTransaction, $objScreeningDatabase, $boolIsScreeningVersionNew = true ) {

        if( false == valObj( $objScreeningTransaction, 'CScreeningTransaction' ) ) return;

        $intScreeningStatusTypeId = NULL;

        if( ( CScreenType::INCOME != $objScreeningTransaction->getScreenTypeId() && CScreenType::DO_NOT_RENT != $objScreeningTransaction->getScreenTypeId() ) && ( true == is_null( $objScreeningTransaction->getScreeningOrderId() ) || CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR == $objScreeningTransaction->getScreeningStatusTypeId() ) ) {
            $intScreeningStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR;
        } elseif( true == is_null( $objScreeningTransaction->getScreeningResponseCompletedOn() ) ) {
            $intScreeningStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN;
        } elseif( true == is_null( $objScreeningTransaction->getScreeningPackageConditionSetId() ) && true == $boolIsScreeningVersionNew ) {
            $intScreeningStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN;
        } else {
            $intScreeningStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED;
        }

        if( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED == $intScreeningStatusTypeId ) {
            if( true == is_null( $objScreeningTransaction->setScreeningCompletedOn() ) ) {
                $objScreeningTransaction->setScreeningCompletedOn( 'NOW()' );
            } else {
                $objScreeningTransaction->setScreeningRecommendationUpdatedOn( 'NOW()' );
            }
        }

        $objScreeningTransaction->setScreeningStatusTypeId( $intScreeningStatusTypeId );

        if( false == $objScreeningTransaction->update( $intCurrentUserId, $objScreeningDatabase ) ) {
        	trigger_error( 'Failed to update screening transaction status for screening transaction id : ' . $objScreeningTransaction->getId() . ' and cid : ' . $objScreeningTransaction->getCid(), E_USER_WARNING );
        	return;
        }

        return $objScreeningTransaction;
    }

    public function getAdditionalScreenTypeIds( $intCurrentUserId, $arrobjCurrentScreeningApplicantTransactions, $arrobjNewScreeningPackageScreenTypes, $objScreeningDatabase ) {
        if( false == valArr( $arrobjCurrentScreeningApplicantTransactions ) ) return $arrobjNewScreeningPackageScreenTypes;

        $arrobjScreeningPackageScreenTypeActions = CScreeningCriteriaScreenTypeActions::fetchPublishedScreeningCriteriaScreenTypeActionsByScreeningPackageId( $this->getScreeningPackageId(), $objScreeningDatabase );
        $arrobjScreeningPackageScreenTypeActions = rekeyObjects( 'ScreenTypeId', $arrobjScreeningPackageScreenTypeActions );

        foreach( $arrobjCurrentScreeningApplicantTransactions as $objScreeningApplicantTransaction ) {
        	if( true == array_key_exists( CScreenType::PASSPORT_VISA_VERIFICATION, $arrobjNewScreeningPackageScreenTypes ) && CScreenType::PASSPORT_VISA_VERIFICATION == $objScreeningApplicantTransaction->getScreenTypeId() ) {
		        if( false == in_array( $objScreeningApplicantTransaction->getScreeningStatusTypeId(), CScreeningStatusType::$c_arrintCancelledScreeningStatusTypeIds ) ) {
		        	unset( $arrobjNewScreeningPackageScreenTypes[$objScreeningApplicantTransaction->getScreenTypeId()] );
		        }

		        $objScreeningApplicantTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN );
		        if( false == $objScreeningApplicantTransaction->update( $intCurrentUserId, $objScreeningDatabase ) ) {
			        trigger_error( 'Failed to update screening transaction status for screening id : ' . $objScreeningApplicantTransaction->getScreeningId() . ' for screening applicant id : ' . $objScreeningApplicantTransaction->getScreeningApplicantId(), E_USER_WARNING );
			        break;
		        }
		        continue;
	        }
            if( true == in_array( $objScreeningApplicantTransaction->getScreenTypeId(), CScreenType::$c_arrintPreciseIdChildScreenTypeIds ) ) continue;
            // This code change determines that if this is child transaction then keep it as it is
            // Don't update anything upon resubmit
            if( false == is_null( $objScreeningApplicantTransaction->getParentScreenTypeId() ) && true == in_array( $objScreeningApplicantTransaction->getScreenTypeId(), CScreenType::$c_arrintChildScreenTypesIds ) && true == array_key_exists( $objScreeningApplicantTransaction->getScreenTypeId(), $arrobjNewScreeningPackageScreenTypes ) ) {
                unset( $arrobjNewScreeningPackageScreenTypes[$objScreeningApplicantTransaction->getScreenTypeId()] );
                continue;
            }

        	$boolIsReuseTransaction = $this->determineReuseTransaction( $arrobjScreeningPackageScreenTypeActions, $objScreeningApplicantTransaction, $objScreeningDatabase );

            if( true == $boolIsReuseTransaction && true == $objScreeningApplicantTransaction->getIsSkipped() ) {
	            $objScreeningApplicantTransaction->setIsSkipped( false );
            }

        	if( true == $boolIsReuseTransaction && false == in_array( $objScreeningApplicantTransaction->getScreenTypeId(), CScreenType::$c_arrintManualCriminalSearchScreenTypeIds ) && true == array_key_exists( $objScreeningApplicantTransaction->getScreenTypeId(), $arrobjNewScreeningPackageScreenTypes ) && $objScreeningApplicantTransaction->getScreeningDataProviderId() == $arrobjNewScreeningPackageScreenTypes[$objScreeningApplicantTransaction->getScreenTypeId()]->getScreeningDataProviderId() ) {

		        if( false == $objScreeningApplicantTransaction->isForceResubmit() ) {
			        // remove this screen type from new screen type array.
			        // $arrobjNewScreeningPackageScreenTypes
			        unset( $arrobjNewScreeningPackageScreenTypes[$objScreeningApplicantTransaction->getScreenTypeId()] );

			        if( ( false == is_null( $objScreeningApplicantTransaction->getScreeningResponseCompletedOn() ) && false == $objScreeningApplicantTransaction->isManualReviewRequired( $objScreeningDatabase ) ) ||
			         true == in_array( $objScreeningApplicantTransaction->getScreeningStatusTypeId(), CScreeningStatusType::$c_arrintCancelledStatusTypeIds ) ) {

                        if( CScreenType::CRIMINAL == $objScreeningApplicantTransaction->getScreenTypeId() && true == $objScreeningApplicantTransaction->checkIsTransactionReviewCompleted( $objScreeningDatabase ) ) {
                            $intScreeningTransactionStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED;
                        } elseif( CScreenType::CRIMINAL == $objScreeningApplicantTransaction->getScreenTypeId() && false == $objScreeningApplicantTransaction->checkIsTransactionReviewCompleted( $objScreeningDatabase ) && true == $objScreeningApplicantTransaction->isManualReviewRequired( $objScreeningDatabase ) ) {
                            $intScreeningTransactionStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW;
                        } elseif( CScreenType::VERIFICATION_OF_INCOME == $objScreeningApplicantTransaction->getScreenTypeId() && CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED == $objScreeningApplicantTransaction->getScreeningStatusTypeId() ) {
	                        // in case of resubmit don't open the VOI transaction
	                        $intScreeningTransactionStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED;
                        } else {
                        	// use getResponseCompletedOn when screeningcompleted on is null as original screening completed on date..
                            $intScreeningTransactionStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN;
                            $objScreeningApplicantTransaction->setOriginalScreeningCompletedOn( ( true == is_null( $objScreeningApplicantTransaction->getScreeningCompletedOn() ) ) ? $objScreeningApplicantTransaction->getScreeningResponseCompletedOn() : $objScreeningApplicantTransaction->getScreeningCompletedOn() );
                            $objScreeningApplicantTransaction->setScreeningRecommendationTypeId( NULL );
                            $objScreeningApplicantTransaction->setScreeningPackageConditionSetId( NULL );
                            $objScreeningApplicantTransaction->setScreeningCompletedOn( NULL );
                            $objScreeningApplicantTransaction->setScreeningRecommendationUpdatedOn( 'NOW()' );
                        }

                        $objScreeningApplicantTransaction->setScreeningStatusTypeId( $intScreeningTransactionStatusTypeId );

                        if( false == $objScreeningApplicantTransaction->update( $intCurrentUserId, $objScreeningDatabase ) ) {
                            trigger_error( 'Failed to update screening applicant transaction to re use for screening transaction id : ' . $objScreeningApplicantTransaction->getId() . ' and cid : ' . $objScreeningApplicantTransaction->getCid(), E_USER_WARNING );
                            $boolIsSuccess = false;
                            break;
                        }

                        if( true == $this->getIsUseForPreScreening() && true == array_search( $objScreeningApplicantTransaction->getScreenTypeId(), CScreenType::$c_arrintChildParentScreenTypes ) ) {
					        $objScreeningApplicantTransaction->updateChildScreeningTransactionStatus( $intCurrentUserId, $objScreeningDatabase, true );
				        }

			        }
		        } else {
			        $objScreeningApplicantTransaction->setTransactionAmount( '0' );
			        $objScreeningApplicantTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );

			        if( false == $objScreeningApplicantTransaction->update( $intCurrentUserId, $objScreeningDatabase ) ) {
				        trigger_error( 'Failed to update screening transaction status for screening id : ' . $objScreeningApplicantTransaction->getScreeningId() . ' for screening applicant id : ' . $objScreeningApplicantTransaction->getScreeningApplicantId(), E_USER_WARNING );
				        break;
			        }
		        }
        	} elseif( false == $objScreeningApplicantTransaction->determineTransactionStatusAction( $this->getScreeningPackageId(), $objScreeningDatabase ) ) {
        		if( false == is_null( $objScreeningApplicantTransaction->getScreeningOrderId() ) ) {
        			$objScreeningApplicantTransaction->updateScreeningTransactionStatus( $intCurrentUserId, CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED, $objScreeningDatabase );
        		} else {
        			$objScreeningApplicantTransaction->updateScreeningTransactionStatus( $intCurrentUserId, CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED, $objScreeningDatabase );
        		}

		        $objScreeningApplicantTransaction->updateChildScreeningTransactionStatus( $intCurrentUserId, $objScreeningDatabase );
        	}
        }

        return $arrobjNewScreeningPackageScreenTypes;
    }

    public function getNewScreeningPackageDifferentScreenTypeIds( $arrobjOldScreeningApplicantTransactions, $arrintNewScreeningPackageScreenTypeIds ) {

        $arrintNewDifferentScreenTypeIds = array();

        if( false == valArr( $arrobjOldScreeningApplicantTransactions ) ) return $arrintNewDifferentScreenTypeIds;

        $arrobjRekeyedOldScreeningApplicantTransactions = rekeyObjects( 'ScreenTypeId', $arrobjOldScreeningApplicantTransactions );
        $arrintOldScreenTypeIds = array_keys( $arrobjRekeyedOldScreeningApplicantTransactions );

        if( false == valArr( $arrintNewScreeningPackageScreenTypeIds ) ) return $arrintNewDifferentScreenTypeIds;

        $arrintNewDifferentScreenTypeIds = array_diff( $arrintNewScreeningPackageScreenTypeIds, $arrintOldScreenTypeIds );

        return $arrintNewDifferentScreenTypeIds;
    }

    public function evaluateResult( $arrmixScreeningApplicantResultValues, $objScreeningDatabase, $intScreenTypeId = 0 ) {

        if( false == valArr( $arrmixScreeningApplicantResultValues ) ) return array();

        $arrmixScreeningApplicantResultValues = rekeyArray( 'screening_config_preference_type_id', $arrmixScreeningApplicantResultValues );

        $arrintScreeningConfigPreferenceTypeIds = array_filter( array_keys( $arrmixScreeningApplicantResultValues ) );

        $arrobjScreeningPackageSettings         = CScreeningPackageSettings::fetchScreeningPackageSettingsByScreeningConfigPreferenceTypeIdsByScreeningPackageId( $arrintScreeningConfigPreferenceTypeIds, $this->getScreeningPackageId(), $objScreeningDatabase );
        $arrobjRekeyedScreeningPackageSettings  = rekeyObjects( 'ScreeningConfigPreferenceTypeId', $arrobjScreeningPackageSettings, true );

        // if( false == valArr( $arrobjScreeningPackageSettings ) ) return $arrmixScreeningApplicantResultValues;

        $arrobjScreeningPackageConditionSets = CScreeningPackageConditionSets::fetchScreeningPackageConditionSetsByScreeningPackageIdByCid( $this->getScreeningPackageId(), $this->getCid(), $objScreeningDatabase );
        $arrobjScreeningPackageConditionSets = rekeyObjects( 'ScreeningRecommendationTypeId', $arrobjScreeningPackageConditionSets );

        if( false == valArr( $arrobjScreeningPackageSettings ) ) {
            // sharnekar --------
            $boolIsPassOnEviction = true;

            if( CScreenType::EVICTION == $intScreenTypeId ) {
                $intTotalEvictionCounts = isset( $arrmixScreeningApplicantResultValues[CScreeningConfigPreferenceType::TOTAL_EVICTION_RECORDS]['value'] ) ? $arrmixScreeningApplicantResultValues[CScreeningConfigPreferenceType::TOTAL_EVICTION_RECORDS]['value'] : 0;

                foreach( $arrmixScreeningApplicantResultValues as $intScreeningConfigPreferenceTypeId => $arrmixScreeningApplicantResultValue ) {
                    if( 0 < $intTotalEvictionCounts ) {
                        $arrmixScreeningApplicantResultValues[$intScreeningConfigPreferenceTypeId]['screening_package_condition_set_id'] = $arrobjScreeningPackageConditionSets[CScreeningRecommendationType::FAIL]->getId();
                    } else {
                       $arrmixScreeningApplicantResultValues[$intScreeningConfigPreferenceTypeId]['screening_package_condition_set_id'] = $arrobjScreeningPackageConditionSets[CScreeningRecommendationType::PASS]->getId();
                    }
                }
            }

            if( CScreenType::INCOME == $intScreenTypeId ) {
                foreach( $arrmixScreeningApplicantResultValues as $intScreeningConfigPreferenceTypeId => $arrmixScreeningApplicantResultValue ) {
                    $arrmixScreeningApplicantResultValues[$intScreeningConfigPreferenceTypeId]['screening_package_condition_set_id'] = $arrobjScreeningPackageConditionSets[CScreeningRecommendationType::PASS]->getId();
                }
            }
			return $arrmixScreeningApplicantResultValues;
		}

        $arrmixScreeningEvaluationResult = array();

        if( true == array_key_exists( CScreeningConfigPreferenceType::NO_CREDIT_RECORD, $arrmixScreeningApplicantResultValues ) && 1 == $arrmixScreeningApplicantResultValues[CScreeningConfigPreferenceType::NO_CREDIT_RECORD]['value'] ) {
            if( true == array_key_exists( CScreeningConfigPreferenceType::NO_CREDIT_RECORD, $arrobjRekeyedScreeningPackageSettings ) ) {
                $objNoCreditRecordPackageSetting = current( $arrobjRekeyedScreeningPackageSettings[CScreeningConfigPreferenceType::NO_CREDIT_RECORD] );
                $arrmixScreeningApplicantResultValues[CScreeningConfigPreferenceType::NO_CREDIT_RECORD]['screening_package_condition_set_id'] = $objNoCreditRecordPackageSetting->getScreeningPackageConditionSetId();
            } else {
                $arrmixScreeningApplicantResultValues[CScreeningConfigPreferenceType::NO_CREDIT_RECORD]['screening_package_condition_set_id'] = $arrobjScreeningPackageConditionSets[CScreeningRecommendationType::FAIL]->getId();
            }
            return $arrmixScreeningApplicantResultValues;
        }

        if( true == array_key_exists( CScreeningConfigPreferenceType::NO_DO_NOT_RENT_RECORD, $arrmixScreeningApplicantResultValues ) && 1 == $arrmixScreeningApplicantResultValues[CScreeningConfigPreferenceType::NO_DO_NOT_RENT_RECORD]['value'] ) {
        	if( true == array_key_exists( CScreeningConfigPreferenceType::NO_DO_NOT_RENT_RECORD, $arrobjRekeyedScreeningPackageSettings ) ) {
        		$objNoDoNotRentRecordPackageSetting = current( $arrobjRekeyedScreeningPackageSettings[CScreeningConfigPreferenceType::NO_DO_NOT_RENT_RECORD] );
        		$arrmixScreeningApplicantResultValues[CScreeningConfigPreferenceType::NO_DO_NOT_RENT_RECORD]['screening_package_condition_set_id'] = $objNoDoNotRentRecordPackageSetting->getScreeningPackageConditionSetId();
        	} else {
        		$arrmixScreeningApplicantResultValues[CScreeningConfigPreferenceType::NO_DO_NOT_RENT_RECORD]['screening_package_condition_set_id'] = $arrobjScreeningPackageConditionSets[CScreeningRecommendationType::FAIL]->getId();
        	}
        	return $arrmixScreeningApplicantResultValues;
        }

        foreach( $arrmixScreeningApplicantResultValues as $intScreeningConfigPreferenceTypeId => $arrmixScreeningApplicantResultValue ) {

            if( false == array_key_exists( $arrmixScreeningApplicantResultValue['screening_config_preference_type_id'], $arrobjRekeyedScreeningPackageSettings ) ) continue;

            $arrobjScreeningConfigPreferenceSettings = $arrobjRekeyedScreeningPackageSettings[$arrmixScreeningApplicantResultValue['screening_config_preference_type_id']];

            // rekey array  by evaluation order
            $arrobjScreeningConfigPreferenceSettings = rekeyObjects( 'ConditionEvaluationOrder', $arrobjScreeningConfigPreferenceSettings );

            if( false == valArr( $arrobjScreeningConfigPreferenceSettings ) ) continue;

       		foreach( $arrobjScreeningConfigPreferenceSettings as $objScreeningConfigPreferenceSetting ) {

       				$boolIsEvaluated = false;

	            	// if result value is null then by default recommendation should be "Pass"
	            	if( false == is_null( $arrmixScreeningApplicantResultValue['value'] ) ) {

		                switch( $objScreeningConfigPreferenceSetting->getScreeningConfigValueTypeId() ) {
		                    case CScreeningConfigValueType::FLAG:
		                    	if( true == $arrmixScreeningApplicantResultValue['value'] ) {
		                    		$boolIsEvaluated = true;
		                    	}
								break;

		                    case CScreeningConfigValueType::DATE_ABOVE:
		                    	if( CScreeningRecommendationType::PASS == $objScreeningConfigPreferenceSetting->getScreeningRecommendationTypeId() && false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMinValue() ) && $arrmixScreeningApplicantResultValue['value'] >= $this->convertYearsIntoMonths( $objScreeningConfigPreferenceSetting->getRequiredMinValue() ) ) {
		                    		$boolIsEvaluated = true;
								} elseif( CScreeningRecommendationType::FAIL == $objScreeningConfigPreferenceSetting->getScreeningRecommendationTypeId() && false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) && $arrmixScreeningApplicantResultValue['value'] <= $this->convertYearsIntoMonths( $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) ) {
		                    		$boolIsEvaluated = true;
		                    	} elseif( false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) && false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMinValue() ) && $arrmixScreeningApplicantResultValue['value'] >= $this->convertYearsIntoMonths( $objScreeningConfigPreferenceSetting->getRequiredMinValue() ) && false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) && $arrmixScreeningApplicantResultValue['value'] <= $this->convertYearsIntoMonths( $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) ) {
		                    		$boolIsEvaluated = true;
		                    	}
		                    	break;

		                    case CScreeningConfigValueType::FRACTION_RANGE_ABOVE:
		                        $intValue = ( CScreeningConfigPreferenceType::RV_INDEX_SCORE == $arrmixScreeningApplicantResultValue['screening_config_preference_type_id'] ) ? $arrmixScreeningApplicantResultValue['value_unfiltered'] : $arrmixScreeningApplicantResultValue['value'];
		                    	if( CScreeningRecommendationType::PASS == $objScreeningConfigPreferenceSetting->getScreeningRecommendationTypeId() && false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMinValue() ) && false == is_null( $intValue ) && $intValue >= $objScreeningConfigPreferenceSetting->getRequiredMinValue() ) {
		                    		$boolIsEvaluated = true;
		                    	} elseif( CScreeningRecommendationType::FAIL == $objScreeningConfigPreferenceSetting->getScreeningRecommendationTypeId() && false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) && false == is_null( $intValue ) && $intValue <= $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) {
		                    		$boolIsEvaluated = true;
		                    	} elseif( false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMinValue() ) && $intValue >= $objScreeningConfigPreferenceSetting->getRequiredMinValue() && false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) && $intValue <= $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) {
		                    		$boolIsEvaluated = true;
		                    	}
		                    	break;

		                    case CScreeningConfigValueType::VALUE_RANGE_ABOVE:
	                    		if( CScreeningRecommendationType::PASS == $objScreeningConfigPreferenceSetting->getScreeningRecommendationTypeId() && false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMinValue() ) && $arrmixScreeningApplicantResultValue['value'] >= $objScreeningConfigPreferenceSetting->getRequiredMinValue() ) {
	                    			$boolIsEvaluated = true;
	                    		} elseif( CScreeningRecommendationType::FAIL == $objScreeningConfigPreferenceSetting->getScreeningRecommendationTypeId() && false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) && $arrmixScreeningApplicantResultValue['value'] <= $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) {
	                    			$boolIsEvaluated = true;
	                    		} elseif( false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMinValue() ) && $arrmixScreeningApplicantResultValue['value'] >= $objScreeningConfigPreferenceSetting->getRequiredMinValue() && false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) && $arrmixScreeningApplicantResultValue['value'] <= $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) {
	                    			$boolIsEvaluated = true;
	                    		}
	                    		break;

		                    case CScreeningConfigValueType::VALUE_RANGE_BELOW:
		                    case CScreeningConfigValueType::PERCENTAGE_RANGE_BELOW:
                    			if( CScreeningRecommendationType::PASS == $objScreeningConfigPreferenceSetting->getScreeningRecommendationTypeId() && false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) && $arrmixScreeningApplicantResultValue['value'] <= $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) {
                    				$boolIsEvaluated = true;
                    			} elseif( CScreeningRecommendationType::FAIL == $objScreeningConfigPreferenceSetting->getScreeningRecommendationTypeId() && false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMinValue() ) && $arrmixScreeningApplicantResultValue['value'] >= $objScreeningConfigPreferenceSetting->getRequiredMinValue() ) {
                    				$boolIsEvaluated = true;
                    			} elseif( false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMinValue() ) && $arrmixScreeningApplicantResultValue['value'] >= $objScreeningConfigPreferenceSetting->getRequiredMinValue() && false == is_null( $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) && $arrmixScreeningApplicantResultValue['value'] <= $objScreeningConfigPreferenceSetting->getRequiredMaxValue() ) {
                    				$boolIsEvaluated = true;

                    			}
                    			break;

		                    default:
		                        // need to add comment
		                        break;
		                }
                	}

                	if( true == $boolIsEvaluated ) {
                		$arrmixScreeningApplicantResultValues[$intScreeningConfigPreferenceTypeId]['screening_package_condition_set_id'] = $objScreeningConfigPreferenceSetting->getScreeningPackageConditionSetId();
                		break;
                	}

					if( false == isset( $arrmixScreeningApplicantResultValues[$intScreeningConfigPreferenceTypeId]['screening_package_condition_set_id'] ) ) {
	                	$arrmixScreeningApplicantResultValues[$intScreeningConfigPreferenceTypeId]['screening_package_condition_set_id'] = $arrobjScreeningPackageConditionSets[CScreeningRecommendationType::PASS]->getId();
	                }
            }
        }

        return $arrmixScreeningApplicantResultValues;
    }

    public function convertYearsIntoMonths( $intYear ) {
        if( false == is_numeric( $intYear ) ) return;
        return $intYear * 12;
    }

    public function setScreeningTransactionsRecommendations( $arrintScreeningRecommendation, $objScreeningApplicantTransaction ) {

    	$objScreeningApplicantTransaction->setScreeningRecommendationTypeId( $arrintScreeningRecommendation['screening_recommendation_type_id'] );
    	$objScreeningApplicantTransaction->setScreeningPackageConditionSetId( $arrintScreeningRecommendation['screening_condition_set_id'] );

    	return $objScreeningApplicantTransaction;
    }

    public function determineTransactionRecommendation( $intCurrentUserId, $arrmixScreeningApplicantResultValues, $objScreeningApplicantTransaction, $objScreeningDatabase, $boolIsSetOriginalValue = false ) {

    	if( false == valObj( $objScreeningApplicantTransaction, 'CScreeningTransaction' ) ) return;

    	$arrintScreeningRecommendation = NULL;
    	$arrintScreeningRecommendation['screening_recommendation_type_id']  = NULL;
    	$arrintScreeningRecommendation['screening_condition_set_id']        = NULL;

    	switch( $objScreeningApplicantTransaction->getScreeningStatusTypeId() ) {
    		case CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR:
    			$arrintScreeningRecommendation['screening_recommendation_type_id']  = CScreeningRecommendationType::ERROR;
    			break;

    		case CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN:
    			$arrintScreeningRecommendation['screening_recommendation_type_id']  = CScreeningRecommendationType::PENDING_UNKNOWN;
    			break;

    		case CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED:
    			$arrintScreeningRecommendation['screening_recommendation_type_id']  = CScreeningRecommendationType::PENDING_UNKNOWN;

    			if( false == valArr( $arrmixScreeningApplicantResultValues ) ) return $arrintScreeningRecommendation;

    			$objScreeningApplicantRecommendation = NULL;

    			$arrmixRekeyedScreeningApplicantResultValues = rekeyArray( 'screening_package_condition_set_id', $arrmixScreeningApplicantResultValues );

    			$arrintScreeningPackageConditionSetIds       = array_filter( array_keys( $arrmixRekeyedScreeningApplicantResultValues ) );

    			$arrobjScreeningPackageConditionSets         = CScreeningPackageConditionSets::fetchScreeningPackageConditionSetsByIdsByCids( $arrintScreeningPackageConditionSetIds, $this->getCid(), $objScreeningDatabase );
    			$arrobjScreeningApplicantResults             = rekeyObjects( 'ScreeningRecommendationTypeId', $arrobjScreeningPackageConditionSets, true );

    			if( false == valArr( $arrobjScreeningApplicantResults ) ) return $arrintScreeningRecommendation;

    			if( true == array_key_exists( CScreeningRecommendationType::FAIL, $arrobjScreeningApplicantResults ) ) {
    				$objScreeningApplicantRecommendation = current( $arrobjScreeningApplicantResults[CScreeningRecommendationType::FAIL] );
    			} elseif( true == array_key_exists( CScreeningRecommendationType::PASSWITHCONDITIONS, $arrobjScreeningApplicantResults ) ) {
    				$objScreeningApplicantRecommendation = current( $arrobjScreeningApplicantResults[CScreeningRecommendationType::PASSWITHCONDITIONS] );
    			} elseif( true == array_key_exists( CScreeningRecommendationType::PASS, $arrobjScreeningApplicantResults ) ) {
    				$objScreeningApplicantRecommendation = current( $arrobjScreeningApplicantResults[CScreeningRecommendationType::PASS] );
    			}

    			$arrintScreeningRecommendation['screening_recommendation_type_id']  = ( true == valObj( $objScreeningApplicantRecommendation, 'CScreeningPackageConditionSet' ) ) ? $objScreeningApplicantRecommendation->getScreeningRecommendationTypeId() : NULL;
    			$arrintScreeningRecommendation['screening_condition_set_id']        = ( true == valObj( $objScreeningApplicantRecommendation, 'CScreeningPackageConditionSet' ) ) ? $objScreeningApplicantRecommendation->getId() : NULL;
    			break;

    		default:
    			// need to add
    			break;
    	}

    	if( false == valArr( $arrintScreeningRecommendation ) ) return;

    	if( true == $boolIsSetOriginalValue ) $objScreeningApplicantTransaction->setOriginalScreeningRecommendationTypeId( $objScreeningApplicantTransaction->getScreeningRecommendationTypeId() );

    	$objScreeningApplicantTransaction->setScreeningRecommendationTypeId( $arrintScreeningRecommendation['screening_recommendation_type_id'] );
    	$objScreeningApplicantTransaction->setScreeningPackageConditionSetId( $arrintScreeningRecommendation['screening_condition_set_id'] );

    	$objScreeningDatabase->begin();

    	if( false == $objScreeningApplicantTransaction->update( $intCurrentUserId, $objScreeningDatabase ) ) {
    		trigger_error( 'Failed to update screening applicant status for screening transaction id : ' . $objScreeningApplicantTransaction->getId() . ' and cid : ' . $objScreeningApplicantTransaction->getCid(), E_USER_WARNING );
    		$objScreeningDatabase->rollback();
    		return false;
    	}

    	$objScreeningDatabase->commit();
    }

    public function updateScreeningEvaluationResult( $intCurrentUserId, $arrobjScreeningApplicantResultValues, $objScreeningDatabase ) {

        if( false == valArr( $arrobjScreeningApplicantResultValues ) ) return;

        $boolIsSuccess = true;

        $objScreeningDatabase->begin();

        foreach( $arrobjScreeningApplicantResultValues as $objScreeningApplicantResultValue ) {
            if( false == $objScreeningApplicantResultValue->update( $intCurrentUserId, $objScreeningDatabase ) ) {
                $objScreeningDatabase->rollback();
                $boolIsSuccess = false;
                break;
            }
        }

        $objScreeningDatabase->commit();
        return $boolIsSuccess;
    }

    public function updateScreeningApplicantStatus( $intCurrentUserId, $intScreeningApplicantStatusTypeId, $objScreeningDatabase ) {

        $this->setStatusTypeId( $intScreeningApplicantStatusTypeId );

        $objScreeningDatabase->begin();

            if( false == $this->update( $intCurrentUserId, $objScreeningDatabase ) ) {
                trigger_error( 'Failed to update screening applicant status for screening applicant id : ' . $this->getId() . ' and cid : ' . $this->getCid(), E_USER_WARNING );
                $objScreeningDatabase->rollback();
                return false;
            }

        $objScreeningDatabase->commit();

        return;
    }

    public function determineScreeningApplicantStatus( $intCurrentUserId, $arrobjScreeningTransactions, $objScreeningDatabase, $boolIsResultOnly = false ) {

        if( false == valArr( $arrobjScreeningTransactions ) ) return NULL;

        $arrobjRekeyedScreeningTransactions = rekeyObjects( 'ScreeningStatusTypeId', $arrobjScreeningTransactions );

        $intScreeningApplicantStatusTypeId  = NULL;

        if( true == array_key_exists( CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR, $arrobjRekeyedScreeningTransactions ) ) {
            $intScreeningApplicantStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR;
        } elseif( true == array_key_exists( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN, $arrobjRekeyedScreeningTransactions ) ) {
            $intScreeningApplicantStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN;
        } else {
            $intScreeningApplicantStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED;
        }

        if( true == $boolIsResultOnly ) return $intScreeningApplicantStatusTypeId;

        $this->updateScreeningApplicantStatus( $intCurrentUserId, $intScreeningApplicantStatusTypeId, $objScreeningDatabase );

        return $intScreeningApplicantStatusTypeId;
    }

    public function determineScreeningApplicantRecommendation( $intCurrentUserId, $arrobjScreeningTransactions, $objScreeningDatabase, $boolIsSetOriginalValue = false ) {

        $intScreeningApplicantRecommendationId = CScreeningRecommendationType::PENDING_UNKNOWN;

        if( false == valArr( $arrobjScreeningTransactions ) ) return $intScreeningApplicantRecommendationId;

        $arrobjRekeyedScreeningTransaction = rekeyObjects( 'ScreeningRecommendationTypeId', $arrobjScreeningTransactions );

        // Evaluating applicant result

        switch( $this->getStatusTypeId() ) {
            case CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR:
                $intScreeningApplicantRecommendationId = CScreeningRecommendationType::ERROR;
                break;

            case CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN:
                $intScreeningApplicantRecommendationId = CScreeningRecommendationType::PENDING_UNKNOWN;
                break;

            case CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED:
                if( true == array_key_exists( CScreeningRecommendationType::ERROR, $arrobjRekeyedScreeningTransaction ) ) {
                    $intScreeningApplicantRecommendationId = CScreeningRecommendationType::ERROR;
                } elseif( true == array_key_exists( CScreeningRecommendationType::PENDING_UNKNOWN, $arrobjRekeyedScreeningTransaction ) ) {
                    $intScreeningApplicantRecommendationId = CScreeningRecommendationType::PENDING_UNKNOWN;
                } elseif( true == array_key_exists( CScreeningRecommendationType::FAIL, $arrobjRekeyedScreeningTransaction ) ) {
                    $intScreeningApplicantRecommendationId = CScreeningRecommendationType::FAIL;
                } elseif( true == array_key_exists( CScreeningRecommendationType::PASSWITHCONDITIONS, $arrobjRekeyedScreeningTransaction ) ) {
                    $intScreeningApplicantRecommendationId = CScreeningRecommendationType::PASSWITHCONDITIONS;
                } else {
                    $intScreeningApplicantRecommendationId = CScreeningRecommendationType::PASS;
                }
                break;

            default:
                $intScreeningApplicantRecommendationId = CScreeningRecommendationType::PENDING_UNKNOWN;
        }

        if( true == $boolIsSetOriginalValue ) $this->setOriginalScreeningRecommendationTypeId( $this->getScreeningRecommendationTypeId() );

        $this->setScreeningRecommendationTypeId( $intScreeningApplicantRecommendationId );

        $objScreeningDatabase->begin();

        if( false == $this->update( $intCurrentUserId, $objScreeningDatabase ) ) {
        	trigger_error( 'Failed to update screening applicant recommendation for screening applicant id : ' . $this->getId() . ' and cid : ' . $this->getCid(), E_USER_WARNING );
        	$objScreeningDatabase->rollback();
        	return false;
        }

        $objScreeningDatabase->commit();

        return $intScreeningApplicantRecommendationId;
    }

    public function updateScreeningApplicantResult( $intCurrentUserId, $arrobjScreeningTransactions, $objScreeningDatabase, $boolIsSetOriginalValue = false ) {

    	if( false == valArr( $arrobjScreeningTransactions ) ) return NULL;

    	$arrobjRekeyedScreeningTransactions = rekeyObjects( 'ScreeningStatusTypeId', $arrobjScreeningTransactions );

    	$intScreeningApplicantStatusTypeId  = NULL;
    	$intScreeningRecommendationTypeId	= NULL;

    	if( true == array_key_exists( CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR, $arrobjRekeyedScreeningTransactions ) ) {
    		$intScreeningApplicantStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR;
    		$intScreeningRecommendationTypeId  = CScreeningRecommendationType::ERROR;
    	} elseif( true == array_key_exists( CScreeningStatusType::APPLICANT_RECORD_STATUS_ID_VERIFICATION_PENDING, $arrobjRekeyedScreeningTransactions ) ) {
		    $intScreeningApplicantStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_ID_VERIFICATION_PENDING;
            $intScreeningRecommendationTypeId  = CScreeningRecommendationType::PENDING_UNKNOWN;
	    } elseif( true == array_key_exists( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN, $arrobjRekeyedScreeningTransactions ) ) {
    		$intScreeningApplicantStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN;
    		$intScreeningRecommendationTypeId  = CScreeningRecommendationType::PENDING_UNKNOWN;
    	} elseif( true == array_key_exists( CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW, $arrobjRekeyedScreeningTransactions ) ) {
    		$intScreeningApplicantStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW;
    		$intScreeningRecommendationTypeId  = CScreeningRecommendationType::PENDING_UNKNOWN;
    	} else {
    		$intScreeningApplicantStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED;

    		$arrobjRekeyedScreeningApplicantTransactions = rekeyObjects( 'ScreeningRecommendationTypeId', $arrobjScreeningTransactions );

    		if( true == array_key_exists( CScreeningRecommendationType::FAIL, $arrobjRekeyedScreeningApplicantTransactions ) ) {
    			$intScreeningRecommendationTypeId = CScreeningRecommendationType::FAIL;
    		} elseif( true == array_key_exists( CScreeningRecommendationType::PASSWITHCONDITIONS, $arrobjRekeyedScreeningApplicantTransactions ) ) {
    			$intScreeningRecommendationTypeId = CScreeningRecommendationType::PASSWITHCONDITIONS;
    		} else {
    			$intScreeningRecommendationTypeId = CScreeningRecommendationType::PASS;
    		}
    	}

	    // if screening applicant status is cancelled then keep the the cancel // don't update
	    if( $this->getStatusTypeId() == CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED ) {
		    $intScreeningApplicantStatusTypeId = CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED;
	    }

	    if( true == $boolIsSetOriginalValue ) $this->setOriginalScreeningRecommendationTypeId( $this->getScreeningRecommendationTypeId() );

    	if( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED == $intScreeningApplicantStatusTypeId ) $this->setScreeningCompletedOn( 'NOW()' );

    	$this->setStatusTypeId( $intScreeningApplicantStatusTypeId );
		$this->setScreeningRecommendationTypeId( $intScreeningRecommendationTypeId );

		if( false == $this->update( $intCurrentUserId, $objScreeningDatabase ) ) {
			trigger_error( 'Failed to update screening applicant result for screening_id : ' . $this->getScreeningId() . ' And cid = ' . $this->getCid(), E_USER_WARNING );
		}

		return $this;
    }

    public function determineApplicantScreeningStatus( $intCurrentUserId, $arrobjScreeningTransactions, $objScreeningDatabase ) {

        if( false == valArr( $arrobjScreeningTransactions ) ) return;

        $boolIsScreeningCompleted = true;

        foreach( $arrobjScreeningTransactions as $objScreeningTransaction ) {
            if( false == is_null( $objScreeningTransaction->getScreeningCompletedOn() ) ) continue;
            $boolIsScreeningCompleted = false;
            break;
        }

        if( true == $boolIsScreeningCompleted ) $this->updateApplicantScreeningStatus( $intCurrentUserId, $objScreeningDatabase );

        return $boolIsScreeningCompleted;
    }

    public function updateApplicantScreeningStatus( $intCurrentUserId, $objScreeningDatabase ) {

        $this->setScreeningCompletedOn( 'NOW()' );

        $objScreeningDatabase->begin();

        if( false == $this->update( $intCurrentUserId, $objScreeningDatabase ) ) {
            trigger_error( 'Failed to update applicant screening status for screening applicant id : ' . $this->getId() . ' and cid : ' . $this->getCid(), E_USER_WARNING );
            $objScreeningDatabase->rollback();
            return false;
        }

        $objScreeningDatabase->commit();

        return;
    }

    public function updateScreeningApplicantTransactionStatus( $intCurrentUserId, $arrobjScreeningApplicantTransactions, $intScreeningStatusTypeId, $objScreeningDatabase ) {

        if( false == valArr( $arrobjScreeningApplicantTransactions ) ) return;

        foreach( $arrobjScreeningApplicantTransactions as $objScreeningApplicantTransaction ) {
            $objScreeningApplicantTransaction->updateScreeningTransactionStatus( $intCurrentUserId, $intScreeningStatusTypeId, $objScreeningDatabase );
        }

        return;
    }

    public function filterScreeningApplicantResult( $arrmixScreeningResult, $objScreeningTransaction, $objScreeningDatabase ) {

        if( false == valArr( $arrmixScreeningResult ) ) return;

        $arrmixScreenTypeResult = ( true == array_key_exists( 'response', $arrmixScreeningResult ) && true == array_key_exists( 'result', $arrmixScreeningResult['response'] ) ) ? $arrmixScreeningResult['response']['result'] : array();

        $arrmixScreeningApplicantResult = ( true == array_key_exists( 'screeningApplicantResults', $arrmixScreenTypeResult ) ) ? $arrmixScreenTypeResult['screeningApplicantResults'][$objScreeningTransaction->getScreenTypeId()] : array();

        if( false == valArr( $arrmixScreeningApplicantResult ) ) return;

        $arrmixScreeningApplicantResultValues = ( true == array_key_exists( 'CScreeningApplicantResultValues', $arrmixScreeningApplicantResult ) ) ? $arrmixScreeningApplicantResult['CScreeningApplicantResultValues'] :array();

        if( false == valArr( $arrmixScreeningApplicantResultValues ) ) return;

	    $objStdRequredFilterData 						= $this->buildScreeningFilterData( CScreenType::DO_NOT_RENT );
	    $objStdRequredFilterData->applicantScreenedDate = $this->getCreatedOn();

	    $arrmixDoNotRentFiltrationResult = array();

	    $objResidentVerifyScreeningFilter = new CResidentVerifyScreeningFilter();

	    $arrmixDoNotRentFilterResult        	= $objResidentVerifyScreeningFilter->applyDoNotRentFilters( $objStdRequredFilterData, $arrmixScreeningApplicantResultValues, $arrmixScreeningApplicantResult['DelinquentResidentEvents'], $objScreeningDatabase );
	    $arrmixFilteredDoNotRentResultValues 	= ( true == valArr( $arrmixDoNotRentFilterResult ) && true == array_key_exists( 'result_values', $arrmixDoNotRentFilterResult ) ) ? $arrmixDoNotRentFilterResult['result_values'] : array();

	    $arrmixDoNotRentFiltrationResult['screeningApplicantResults'] = $arrmixFilteredDoNotRentResultValues;

	    return $arrmixDoNotRentFiltrationResult;
    }

   public function buildScreeningFilterData( $intScreenTypeId ) {

        $objStdRequiredFilterData                       = new stdClass();
        $objStdRequiredFilterData->screeningPackageId   = $this->getScreeningPackageId();
        $objStdRequiredFilterData->Cid                  = $this->getCid();
        $objStdRequiredFilterData->screenTypeId         = $intScreenTypeId;

        return $objStdRequiredFilterData;
   }

   public function updateScreeningApplicantData( $intCurrentUserId, $arrmixScreeningApplicantDetails, $objScreeningDatabase ) {

        if( false == valArr( $arrmixScreeningApplicantDetails ) ) return;

        foreach( $arrmixScreeningApplicantDetails as $arrmixScreeningApplicantDetail ) {

            if( false == valArr( $arrmixScreeningApplicantDetail ) ) continue;

            $objScreeningDatabase->begin();

            foreach( $arrmixScreeningApplicantDetail as $objScreeningApplicantData ) {

                if( false == $objScreeningApplicantData->update( $intCurrentUserId, $objScreeningDatabase ) ) {
                    $objScreeningDatabase->rollback();
                    break 2;
                }
            }

            $objScreeningDatabase->commit();
        }

        return;
   }

   public function calculateScreeningApplicantRVIndexScore( $objScreening, $arrmixScreeningApplicantResultValues ) {

        if( false == valArr( $arrmixScreeningApplicantResultValues ) || false == valObj( $objScreening, 'CScreening' ) ) return NULL;

        $objScreeningUtil = new CScreeningUtils();

        $fltPsiIndexCreditScore = $objScreeningUtil->calculateApplicantRVIndexScore( $this, $objScreening, $arrmixScreeningApplicantResultValues );

        return $fltPsiIndexCreditScore;
   }

   public function calculateApplicantsRentToIncomeRatio( $intApplicationRent, $objScreeningDatabase ) {

        $arrobjScreeningApplicants = CScreeningApplicants::fetchActiveScreeningApplicantsByScreeningIdByCid( $this->getScreeningId(), $this->getCid(), $objScreeningDatabase );

        if( false == valArr( $arrobjScreeningApplicants ) ) return false;

        $objScreeningPackage = CScreeningPackages::fetchScreeningPackageByIdByCid( $this->getScreeningPackageId(), $this->getCid(), $objScreeningDatabase );

        if( false == valObj( $objScreeningPackage, 'CScreeningPackage' ) ) return false;

        $intTotalApplicantsIncome = 0;
        $intRentToIncomeRatio   = 0;
        $intApplicantsIncome    = 0;
        $arrmixApplicantIncomeRatio = array();

        $objCurrentScreeningApplicant = ( true == array_key_exists( $this->getId(), $arrobjScreeningApplicants ) ) ? $arrobjScreeningApplicants[$this->getId()] : NULL;

        if( false == valObj( $objCurrentScreeningApplicant, 'CScreeningApplicant' ) ) return false;

        $intApplicantsIncome = $objCurrentScreeningApplicant->getIncome();

        foreach( $arrobjScreeningApplicants as $objScreeningApplicant ) {

            switch( $objScreeningPackage->getScreeningCumulationTypeId() ) {
                case CScreeningCumulationType::EVALUATE_GUARANTOR_SEPERATELY:
                    if( true == in_array( $objScreeningApplicant->getScreeningApplicantTypeId(), array( CScreeningApplicantType::GUARANTOR, CScreeningApplicantType::CORPORATE ) ) ) {
                        if( $objCurrentScreeningApplicant->getId() != $objScreeningApplicant->getId() ) continue 2;
                        $intTotalApplicantsIncome = $objScreeningApplicant->getIncome();
                        break 2;
                    } else {
                        $intTotalApplicantsIncome += $objScreeningApplicant->getIncome();
                    }
                    break;

                case CScreeningCumulationType::COMBINE_GUARANTOR_INCOME:
                    if( true == in_array( $objCurrentScreeningApplicant->getScreeningApplicantTypeId(), array( CScreeningApplicantType::GUARANTOR, CScreeningApplicantType::CORPORATE ) ) ) {
                        if( false == in_array( $objScreeningApplicant->getScreeningApplicantTypeId(), array( CScreeningApplicantType::GUARANTOR, CScreeningApplicantType::CORPORATE ) ) ) continue 2;
                        $intTotalApplicantsIncome += $objScreeningApplicant->getIncome();
                    } else {
                        $intTotalApplicantsIncome = $objScreeningApplicant->getIncome();
                    }
                    break;

                case CScreeningCumulationType::COMBINE_ALL_GUARANTOR_INCOME_WITH_APPLICANT:
                    $intTotalApplicantsIncome += $objScreeningApplicant->getIncome();
                    break;

                default:
                    // need to add comments.
            }
        }

        $intApplicantsHouseHoldRatio = ( 0 != $intTotalApplicantsIncome && 0 != $intApplicationRent ) ? $intTotalApplicantsIncome / $intApplicationRent : 0;

        return $intApplicantsHouseHoldRatio;
   }

   public function determineReUseScreeningApplicant( $objStdScreeningPreferenceSettings, $objStdScreeningApplicant ) {

   		$intDefaultReUseApplicantDays = 30;

        $intReUseApplicantDays = ( true == valObj( $objStdScreeningPreferenceSettings, 'stdClass' ) && 0 < $objStdScreeningPreferenceSettings->dedupeSettingDays ) ? $objStdScreeningPreferenceSettings->dedupeSettingDays : $intDefaultReUseApplicantDays;

        $boolReUseApplicant = true;

        $objStdApplicantBasicInfo = $objStdScreeningApplicant->applicantInfo;
        $objStdApplicantAddress	  = $objStdScreeningApplicant->applicantAddress;

      	// if applicant screened with days as per the setting
		if( strtotime( $this->getCreatedOn() ) < strtotime( '-' . $intReUseApplicantDays . 'days' ) ) {
			$boolReUseApplicant = false;
		}

	   $strDecryptedSsn = $objStdApplicantBasicInfo->ssn;
		if( false == is_null( $objStdApplicantBasicInfo->ssn ) ) {
			$strDecryptedSsn = ( preg_replace( '/[^a-z0-9]/i', '', ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $objStdApplicantBasicInfo->ssn, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] ) ) );
		}

		// If applicant ssn has changed
		if( true == $boolReUseApplicant && 0 != strcmp( $this->getTaxNumber(), $strDecryptedSsn ) ) {
			$boolReUseApplicant = false;
		}

		// If applicant name has changed
		if( true == $boolReUseApplicant && ( 0 != strcmp( strtolower( $this->getNameFirst() ), strtolower( $objStdApplicantBasicInfo->nameFirst ) ) || 0 != strcmp( strtolower( $this->getNameLast() ), strtolower( $objStdApplicantBasicInfo->nameLast ) ) ) ) {
			$boolReUseApplicant = false;
		}

		// If applicant address has changed
		// consider zip code also
		if( true == $boolReUseApplicant && ( 0 != strcmp( trim( $this->getAddressLine() ), trim( $objStdApplicantAddress->addressLine ) ) || $this->getZipcode() != $objStdApplicantAddress->postalCode ) ) {
			$boolReUseApplicant = false;
		}

	   // If applicant dob has changed
	   if( true == $boolReUseApplicant && 0 != strcmp( $this->getBirthDateDecrypted(), $objStdApplicantBasicInfo->birthDate ) ) {
		   $boolReUseApplicant = false;
	   }

        return $boolReUseApplicant;
   }

   public function isScreeningWithDifferentPackage( $objStdScreeningApplicant ) {
        return ( $this->getScreeningPackageId() != $objStdScreeningApplicant->packageId ) ? true : false;
   }

    public function fetchRemainingTransactionsCount( $objScreeningDatabase ) {

        if( false == valId( $this->getScreeningId() ) || false == valId( $this->getId() ) || false == valId( $this->getCid() ) || false == valId( $this->getScreeningPackageId() ) ) return false;

        $intRemainingTransactionsCount = ( int ) \Psi\Eos\Screening\CScreeningPackageScreenTypeAssociations::createService()->fetchRemainingTransactionsCountByScreeningIdByScreeningApplicantIdByScreeningPackageIdByCid( $this->getScreeningId(), $this->getId(), $this->getScreeningPackageId(), $this->getCid(), $objScreeningDatabase );

        return $intRemainingTransactionsCount;
    }

    /*
     * fetch functions
     */

    public function fetchActiveScreeningApplicantTransactions( $objDatabase ) {
    	$arrobjScreeningTransactions = CScreeningTransactions::fetchActiveScreeningTransactionsByScreeningIdByScreeningApplicantIdByCid( $this->getScreeningId(), $this->getId(), $this->getCid(), $objDatabase );
    	return $arrobjScreeningTransactions;
    }

    public function fetchAllScreeningApplicantTransactions( $objDatabase ) {
       	$arrobjScreeningTransactions = CScreeningTransactions::fetchScreeningTransactionsByScreeningIdByScreeningApplicantIdByCid( $this->getScreeningId(), $this->getId(), $this->getCid(), $objDatabase );
       	return $arrobjScreeningTransactions;
    }

    public function fetchAllScreeningApplicantTransactionsByScreenTypeIds( $arrintScreenTypeIds, $objDatabase ) {
    	if( false == valArr( $arrintScreenTypeIds ) ) return;
    	$arrobjScreeningTransactions = CScreeningTransactions::fetchScreeningTransactionsByScreenTypeIdsByScreeningIdByScreeningApplicantIdByCid( $arrintScreenTypeIds, $this->getScreeningid(), $this->getId(), $this->getCid(), $objDatabase );
    	return $arrobjScreeningTransactions;
    }

	public function fetchCustomScreeningPackageScreenTypeCriteriasBySearchTypeId( $intScreeningSearchCriteriaTypeId, $objDatabase ) {
	  return CScreeningPackageScreenTypeCriterias::fetchCustomScreeningPackageScreenTypeCriteriasByScreeningPackageIdByScreeningSearchCriteriaTypeIds( $this->getScreeningPackageId(), array( $intScreeningSearchCriteriaTypeId ), $objDatabase );
	}

    public function isManualCriminalEnabled( $objDatabase ) {
    	$strWhere = 'WHERE screening_package_id = ' . ( int ) $this->getScreeningPackageId() . ' AND is_published = 1';
    	$intRowCount = CScreeningPackageScreenTypeCriterias::fetchScreeningPackageScreenTypeCriteriaCount( $strWhere, $objDatabase );
    	return ( 0 < $intRowCount ) ? true : false;
	}

    /*
     * Other functions
     */

    public function getAdditionalManualCriminalSearchTypes( $intCurrentUserId, $objScreeningPackageScreenType, $arrobjCurrentScreeningApplicantTransactions, $arrmixSearchTypes, $objDatabase ) {

        if( false == valArr( $arrobjCurrentScreeningApplicantTransactions ) ) return $arrmixSearchTypes;

        $arrobjCurrentScreeningApplicantTransactions = rekeyObjects( 'SearchType', $arrobjCurrentScreeningApplicantTransactions );
        unset( $arrobjCurrentScreeningApplicantTransactions[NULL] );

        if( false == valArr( $arrobjCurrentScreeningApplicantTransactions ) ) return $arrmixSearchTypes;

        $boolIsValidScreeningPackageObject = ( true == valObj( $objScreeningPackageScreenType, 'CScreeningPackageScreenTypeAssociation' ) || true == valObj( $objScreeningPackageScreenType, 'CScreeningPackageScreenTypeCriteria' ) ) ? true : false;

        foreach( $arrobjCurrentScreeningApplicantTransactions as $objScreeningApplicantTransaction ) {
            if( true == valArr( $arrmixSearchTypes ) && true == $boolIsValidScreeningPackageObject && true == in_array( $objScreeningApplicantTransaction->getSearchType(), $arrmixSearchTypes ) && $objScreeningApplicantTransaction->getScreeningDataProviderId() == $objScreeningPackageScreenType->getScreeningDataProviderId() ) {
                // remove this search type from new search type array.
                $intSearchTypeArrayIndex = array_search( $objScreeningApplicantTransaction->getSearchType(), $arrmixSearchTypes );
                unset( $arrmixSearchTypes[$intSearchTypeArrayIndex] );

                if( true == $objScreeningApplicantTransaction->isManualCriminalSearchExist( $this->getScreeningPackageId(), $objDatabase ) &&
                    CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED == $objScreeningApplicantTransaction->getScreeningStatusTypeId() ) {
                	continue;
                }

                if( ( false == $objScreeningApplicantTransaction->isManualReviewRequired( $objDatabase ) || true == in_array( $objScreeningApplicantTransaction->getScreeningStatusTypeId(), CScreeningStatusType::$c_arrintScreeningCancelledStatusIds ) ) && ( false == valObj( $objScreeningApplicantTransaction->getAdditionalDetails(), 'stdClass' ) || ( true == valObj( $objScreeningApplicantTransaction->getAdditionalDetails(), 'stdClass' ) && false == property_exists( $objScreeningApplicantTransaction->getAdditionalDetails(), 'is_skipped_transaction' ) ) ) ) {
                    if( false == is_null( $objScreeningApplicantTransaction->getScreeningResponseCompletedOn() ) ) {

                        $objScreeningApplicantTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN );

                        // use getResponseCompletedOn when screeningcompleted on is null as original screening completed on date..
                        $objScreeningApplicantTransaction->setOriginalScreeningCompletedOn( ( true == is_null( $objScreeningApplicantTransaction->getScreeningCompletedOn() ) ) ? $objScreeningApplicantTransaction->getScreeningResponseCompletedOn() : $objScreeningApplicantTransaction->getScreeningCompletedOn() );
                        $objScreeningApplicantTransaction->setScreeningRecommendationTypeId( CScreeningRecommendationType::PENDING_UNKNOWN );
                        $objScreeningApplicantTransaction->setScreeningPackageConditionSetId( NULL );
                        $objScreeningApplicantTransaction->setScreeningCompletedOn( NULL );
                        $objScreeningApplicantTransaction->setScreeningRecommendationUpdatedOn( 'NOW()' );

                        if( false == $objScreeningApplicantTransaction->update( $intCurrentUserId, $objDatabase ) ) {
                            trigger_error( 'Failed to update screening applicant transaction to re use for screening transaction id : ' . $objScreeningApplicantTransaction->getId() . ' and cid : ' . $objScreeningApplicantTransaction->getCid(), E_USER_WARNING );
                            $boolIsSuccess = false;
                            break;
                        }
                    } else {
	                    if( false == $objScreeningApplicantTransaction->getIsSkipped() ) {
		                    $objScreeningApplicantTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN );

		                    if( false == $objScreeningApplicantTransaction->update( $intCurrentUserId, $objDatabase ) ) {
			                    trigger_error( 'Failed to update screening applicant transaction to re use', E_USER_WARNING );
			                    $boolIsSuccess = false;
			                    break;
		                    }
	                    }
                    }
                }
            } elseif( false == $objScreeningApplicantTransaction->isManualCriminalSearchExist( $this->getScreeningPackageId(), $objDatabase ) ) {
                if( false == is_null( $objScreeningApplicantTransaction->getScreeningOrderId() ) ) {
                    $objScreeningApplicantTransaction->updateScreeningTransactionStatus( $intCurrentUserId, CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED, $objDatabase );
                } else {
                    $objScreeningApplicantTransaction->updateScreeningTransactionStatus( $intCurrentUserId, CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED, $objDatabase );
                }
            }
        }

        return $arrmixSearchTypes;
    }

    public function isManualCriminalSearchEnabled( $objDatabase ) {
    	if( true == is_null( $this->getScreeningPackageId() ) ) return false;

		$strWhere = 'WHERE screening_package_id = ' . $this->getScreeningPackageId();
		return ( 0 < CScreeningPackageScreenTypeCriterias::fetchScreeningPackageScreenTypeCriteriaCount( $strWhere, $objDatabase ) ) ? true : false;
    }

    // This function will validate the required data and only then return true
    // or always return false;

    public function determineAndAutoUpdateScreeningTransaction( $intCurrentUserId, $objScreeningApplicantTransaction, $objDatabase ) {
		if( false == valObj( $objScreeningApplicantTransaction, 'CScreeningTransaction' ) ) {
			trigger_error( 'Failed to process request for screening applicant id : ' . $this->getId() . 'cid : ' . $this->getCid(), E_USER_ERROR );
			return false;
		}

		if( false == is_null( $this->getTaxNumberLastFour() ) && true == CScreeningUtils::checkResidentVerifyValidAge( $this->getBirthDateDecrypted() ) ) return $objScreeningApplicantTransaction;

		if( false == CScreeningUtils::checkResidentVerifyValidAge( $this->getBirthDateDecrypted() ) ) {
			$intScreeningConfigPreferenceTypeId = getArrayElementByKey( $objScreeningApplicantTransaction->getScreenTypeId(), CScreeningConfigPreferenceType::$c_arrintMinorConfigMappedWithScreenType );
			$objScreenTypeDataProviderRule = CScreenTypeConfigPreferenceDataProviderRules::fetchActiveScreenTypeConfigPreferenceTypeDataProviderRulesByScreenTypeIdByScreeningDataProviderTypeIdByScreeningConfigPreferenceTypeId( $objScreeningApplicantTransaction->getScreenTypeId(), $objScreeningApplicantTransaction->getScreeningDataProviderTypeId(), $intScreeningConfigPreferenceTypeId, $objDatabase );

			if( true == valObj( $objScreenTypeDataProviderRule, 'CScreenTypeConfigPreferenceDataProviderRule' ) && CScreeningSearchAction::SKIP == $objScreenTypeDataProviderRule->getScreeningSearchActionId() ) {
				$objScreeningApplicantTransaction->autoProcessScreeningTransaction( $intCurrentUserId, $this->getScreeningPackageId(), $intScreeningConfigPreferenceTypeId, $objDatabase );
			}

		} elseif( true == is_null( $this->getTaxNumberLastFour() ) ) {
			$intScreeningConfigPreferenceTypeId = getArrayElementByKey( $objScreeningApplicantTransaction->getScreenTypeId(), CScreeningConfigPreferenceType::$c_arrintNoSsnConfigMappedWithScreenType );
			$objScreenTypeDataProviderRule = CScreenTypeConfigPreferenceDataProviderRules::fetchActiveScreenTypeConfigPreferenceTypeDataProviderRulesByScreenTypeIdByScreeningDataProviderTypeIdByScreeningConfigPreferenceTypeId( $objScreeningApplicantTransaction->getScreenTypeId(), $objScreeningApplicantTransaction->getScreeningDataProviderTypeId(), $intScreeningConfigPreferenceTypeId, $objDatabase );

			if( true == valObj( $objScreenTypeDataProviderRule, 'CScreenTypeConfigPreferenceDataProviderRule' ) && CScreeningSearchAction::SKIP == $objScreenTypeDataProviderRule->getScreeningSearchActionId() ) {
				$objScreeningApplicantTransaction->autoProcessScreeningTransaction( $intCurrentUserId, $this->getScreeningPackageId(), $intScreeningConfigPreferenceTypeId, $objDatabase );
			} else {
				// loading overriding rules
				$objScreeningPackageScreenTypeAction = CScreeningCriteriaScreenTypeActions::fetchPublishedScreeningCriteriaScreenTypeActionsByScreeningPackageIdByScreenTypeIdByScreeningConfigPreferenceTypeId( $this->getScreeningPackageId(), $objScreeningApplicantTransaction->getScreenTypeId(), $intScreeningConfigPreferenceTypeId, $objDatabase );
				// Below line is going handle following scenarios
				// 1. allow process screening for all other experian requests like : business credit, business premier, rent burau
				// 2. if experian credit value set to process : then submit the request to data provider else skip
				if( false == valObj( $objScreeningPackageScreenTypeAction, 'CScreeningCriteriaScreenTypeAction' ) || CScreeningSearchAction::PROCESS == $objScreeningPackageScreenTypeAction->getScreeningSearchActionId() ) return $objScreeningApplicantTransaction;

				$objScreeningApplicantTransaction->autoProcessScreeningTransaction( $intCurrentUserId, $this->getScreeningPackageId(), $intScreeningConfigPreferenceTypeId, $objDatabase );
			}
		}

		return $objScreeningApplicantTransaction;
    }

    private function determineReuseTransaction( $arrobjScreeningPackageScreenTypeActions, $objScreeningApplicantTransaction, $objDatabase ) {
		if( false == $this->hasScreenTypeDataProviderRule( $objScreeningApplicantTransaction->getScreenTypeId(), $objScreeningApplicantTransaction->getScreeningDataProviderTypeId(), $objDatabase ) ) return true;

    	$objScreeningPackageScreenTypeAction = getArrayElementByKey( $objScreeningApplicantTransaction->getScreenTypeId(), $arrobjScreeningPackageScreenTypeActions );

    	$boolIsReuseTransactions = true;

    	if( CScreeningTransaction::PROCESSING_STATUS_SKIPPED == $objScreeningApplicantTransaction->getScreeningOrderId() ) {
    		if( false == CScreeningUtils::checkResidentVerifyValidAge( $this->getBirthDateDecrypted() ) ) {
    			$boolIsReuseTransactions = true;
    		} else {
    			if( true == valObj( $objScreeningPackageScreenTypeAction, 'CScreeningCriteriaScreenTypeAction' ) ) {
    				$boolIsReuseTransactions = ( CScreeningSearchAction::SKIP == $objScreeningPackageScreenTypeAction->getScreeningSearchActionId() ) ? true : false;
    			} elseif( true == is_null( $this->getTaxNumberLastFour() ) && false == valObj( $objScreeningPackageScreenTypeAction, 'CScreeningCriteriaScreenTypeAction' ) ) {
    				$boolIsReuseTransactions = true;
    			} else {
    				$boolIsReuseTransactions = false;
    			}
    		}
    	} else {

    		if( false == CScreeningUtils::checkResidentVerifyValidAge( $this->getBirthDateDecrypted() ) ) {
    			$boolIsReuseTransactions = false;
    		} elseif( true == valObj( $objScreeningPackageScreenTypeAction, 'CScreeningCriteriaScreenTypeAction' ) ) {
    			$boolIsReuseTransactions = ( true == is_null( $this->getTaxNumberLastFour() ) && CScreeningSearchAction::SKIP == $objScreeningPackageScreenTypeAction->getScreeningSearchActionId() ) ? false : true;
    		} else {
    			$boolIsReuseTransactions = true;
    		}
    	}

    	return $boolIsReuseTransactions;
    }

    private function hasScreenTypeDataProviderRule( $intScreenTypeId, $intScreeningDataProviderTypeId, $objDatabase ) {
		$strWhere = ' WHERE screen_type_id = ' . ( int ) $intScreenTypeId . ' AND screening_data_provider_type_id = ' . ( int ) $intScreeningDataProviderTypeId;
		$intRecordsCount = CScreenTypeConfigPreferenceDataProviderRules::fetchScreenTypeConfigPreferenceDataProviderRuleCount( $strWhere, $objDatabase );
		return ( 0 < $intRecordsCount ) ? true : false;
    }

	public function getScreeningTransactionsBySearchTypes( $arrobjManualCriminalTransactions,  $arrmixTransactionSearchTypes ) {

		$arrobjManualCriminalTransactions = rekeyObjects( 'SearchType', $arrobjManualCriminalTransactions );
		$arrobjScreeningTransactions = array();

		if( true == valArr( $arrobjManualCriminalTransactions ) && true == valArr( $arrmixTransactionSearchTypes ) ) {

			foreach( $arrmixTransactionSearchTypes as $arrmixTransactionSearchType ) {
				$arrobjScreeningTransactions[] = ( true == valObj( $arrobjManualCriminalTransactions[$arrmixTransactionSearchType], 'CScreeningTransaction' ) ) ?  $arrobjManualCriminalTransactions[$arrmixTransactionSearchType] : NULL;
			}
		}

		return $arrobjScreeningTransactions;
	}

    public function createVendorScreeningTransactions( $objStdServiceData, $intCurrentUserId, $intPropertyId, $arrobjScreeningAccountRates, $objDatabase ) {
	    $objExistingScreeningApplicant = CScreeningApplicants::fetchActiveVendorScreeningApplicantByCustomApplicantId( $this->getCustomApplicantId(), $objDatabase );
	    $objExistingScreeningTransaction = NULL;

	    if( false == valObj( $objExistingScreeningApplicant, 'CScreeningApplicant' ) ) {
		    $objExistingScreeningTransaction = $this->getExistingVendorScreeningTransaction( $this, $objDatabase );
	    } else {
		    $objExistingScreeningTransaction = $this->getExistingVendorScreeningTransaction( $objExistingScreeningApplicant, $objDatabase );
	    }

	    switch( $objStdServiceData->preferenceSettings->screeningRequestSourceId ) {
		    case CVendorAccessScreeningLibrary::SCREENING_REQUEST_SOURCE_ID_RUN:
			    if( true == $objStdServiceData->preferenceSettings->isRunNewScreening || false == valObj( $objExistingScreeningTransaction, 'CScreeningTransaction' ) ) {
				    $this->createScreeningTransactions( $intCurrentUserId, $intPropertyId, $arrobjScreeningAccountRates, $objDatabase );
			    } else {
				    $this->createClonedTransactions( $objExistingScreeningTransaction, $intPropertyId, $arrobjScreeningAccountRates, $intCurrentUserId, $objDatabase );
			    }
			    break;

		    case CVendorAccessScreeningLibrary::SCREENING_REQUEST_SOURCE_ID_SHARE:
		    case CVendorAccessScreeningLibrary::SCREENING_REQUEST_SOURCE_ID_PROCESS_COMPLIANCE:
		    case CVendorAccessScreeningLibrary::SCREENING_REQUEST_SOURCE_ID_PACKAGE_UPDATE:
			    $objScreeningTransaction = current( ( array ) CScreeningTransactions::fetchScreeningTransactionsByScreeningIdByScreeningApplicantIdByCid( $this->getScreeningId(), $this->getId(), $this->getCid(), $objDatabase ) );
			    if( true == valObj( $objScreeningTransaction, 'CScreeningTransaction' ) ) {
					$this->updateScreeningTransaction( $objScreeningTransaction, $intCurrentUserId, $objDatabase );
			    } else {
		           $this->createClonedTransactions( $objExistingScreeningTransaction, $intPropertyId, $arrobjScreeningAccountRates, $intCurrentUserId, $objDatabase );
	            }
	            break;

		    default:
	    }

    	$this->setScreeningTransactions( $this->fetchActiveScreeningApplicantTransactions( $objDatabase ) );

    	return;
    }

    public function getExistingVendorScreeningTransaction( $objExistingScreeningApplicant, $objDatabase ) {
    	$arrintVendorScreenTypeId    = array( CScreeningApplicantType::VENDOR_ACCESS_CRIMINAL => CScreenType::CRIMINAL, CScreeningApplicantType::VENDOR_ACCESS_BUSINESS => CScreenType::BUSINESS_PREMIER_PROFILE );
    	$arrobjScreeningTransactions = CScreeningTransactions::fetchCompletedScreeningTransactionsByScreenTypeIdsByScreeningIdByScreeningApplicantIdByCid( array( $arrintVendorScreenTypeId[$objExistingScreeningApplicant->getScreeningApplicantTypeId()] ), $objExistingScreeningApplicant->getScreeningId(), $objExistingScreeningApplicant->getId(), $objExistingScreeningApplicant->getCid(), $objDatabase );
    	return ( true == valArr( $arrobjScreeningTransactions ) ) ? current( $arrobjScreeningTransactions ) : NULL;
    }

	public function setParentChildTransactions( $arrobjScreeningTransactions ) {
		$arrobjScreeningApplicantTransaction = array();

		$arrobjRekeyedScreenngTransactions  = rekeyObjects( 'ParentScreenTypeId', $arrobjScreeningTransactions, true );
		unset( $arrobjRekeyedScreenngTransactions[NULL] );

		foreach( $arrobjScreeningTransactions as $objScreeningTransaction ) {
			if( true == array_key_exists( $objScreeningTransaction->getScreenTypeId(), $arrobjRekeyedScreenngTransactions ) ) {
				$arrobjChildTransactions = array();
				foreach( getArrayElementByKey( $objScreeningTransaction->getScreenTypeId(), $arrobjRekeyedScreenngTransactions ) as $objChildTransaction ) {
					if( false == in_array( $objChildTransaction->getScreenTypeId(), CScreenType::$c_arrintManualCriminalSearchScreenTypeIds ) ) {
						$arrobjChildTransactions[] = $objChildTransaction;
					}
				}

				$objScreeningTransaction->setChildTransactions( $arrobjChildTransactions );
				$arrobjScreeningApplicantTransaction[] = $objScreeningTransaction;
			} elseif( true == is_null( $objScreeningTransaction->getParentScreenTypeId() ) || true == in_array( $objScreeningTransaction->getScreenTypeId(), CScreenType::$c_arrintManualCriminalSearchScreenTypeIds ) || CScreenType::PASSPORT_VISA_VERIFICATION == $objScreeningTransaction->getScreenTypeId() ) {
				$arrobjScreeningApplicantTransaction[] = $objScreeningTransaction;
			}
		}

		$this->setScreeningTransactions( $arrobjScreeningApplicantTransaction );
	}

	public function reopenScreeningApplicant( $intUserId, $objDatabase ) {
		if( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED != $this->getStatusTypeId() ) {
			$this->setStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN );
		}

		$this->setScreeningRecommendationTypeId( CScreeningRecommendationType::PENDING_UNKNOWN );

		if( false == $this->update( $intUserId, $objDatabase ) ) {
			trigger_error( 'Failed to reopen screening applicant for screening applicant id : ' . $this->getId() . ' and cid : ' . $this->getCid(), E_USER_WARNING );
			return false;
		}
		return true;
	}

	/*
	 * 1.New Process Screening
	 *  - If send_california_report_request flag is set then only create a new request
	 * 2.Resubmit within 30 days
	 *  - If request already exists then update the request status as per screening recommendation
	 *  - If request does not exist and if send_california_report_request flag is set then only create new request
	 * 3.Resubmit after 30 days
	 *  - If request exists for old screening_applicant then archive(Deleted Failed) the old request and create a new request with new screening_applicant_id
	 */

	public function createScreeningReportRequest( $boolSendCaliforniaConsumerReport, $intCurrentUserId, $intScreeningReportRequestStatusTypeId, $intSourceType, $objExistingScreeningApplicant, $objDatabase ) {
		$intScreeningReportRequestCount = $this->checkScreeningReportRequestExist( $objDatabase, false );

        if( true == valObj( $objExistingScreeningApplicant, 'CScreeningApplicant' ) && CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED == $objExistingScreeningApplicant->getStatusTypeId() ) {
            // if the resubmit is done after 30 days the existing screening applicant's screening report request should be deleted failed
            // Then create new screening report request with new screening applicant id
            $objExistingScreeningReportRequest = CScreeningReportRequests::fetchScreeningApplicantReportRequestByScreeningIdByScreeningApplicantId( $this->getScreeningId(), $objExistingScreeningApplicant->getId(), $objDatabase );

            if( true == valObj( $objExistingScreeningReportRequest, 'CScreeningReportRequest' ) ) {
            	if( true == in_array( $objExistingScreeningReportRequest->getScreeningReportRequestStatusId(), CScreeningReportRequestStatus::$c_arrintUnderProcessScreeningApplicantReportRequestStatuses ) ) {
		            $objExistingScreeningReportRequest->setScreeningReportRequestStatusId( CScreeningReportRequestStatus::DELETED_FAILED );
		            $objExistingScreeningReportRequest->update( $intCurrentUserId, $objDatabase );
	            }
	            return $this->insertScreeningReportRequests( $intCurrentUserId, $intScreeningReportRequestStatusTypeId, $objDatabase, $intSourceType );
            }
        } elseif( true == $boolSendCaliforniaConsumerReport && 0 == $intScreeningReportRequestCount ) {
            // if record is not exist then insert record into the table
            return $this->insertScreeningReportRequests( $intCurrentUserId, $intScreeningReportRequestStatusTypeId, $objDatabase, $intSourceType );
        }

        return true;
    }

	/**
	 * @param      $objDatabase
	 * @param bool $boolExcludeDeletedRequest ( check exist records in deleted records or not )
	 * @return bool
	 * @Description : Check if records already exist or not
	 */
	public function checkScreeningReportRequestExist( $objDatabase ) {
	    // if record is already exist then return true.

        $intScreeningReportRequestCount = CScreeningReportRequests::fetchScreeningApplicantReportRequestCountByCidByScreeningIdByScreeningApplicantId( $this->getCid(), $this->getScreeningId(), $this->getId(), $objDatabase );

        return $intScreeningReportRequestCount;
	}

	public function insertScreeningReportRequests( $intCurrentUserId, $intScreeningReportRequestStatusTypeId, $objDatabase, $intSourceType = CScreeningReportRequestSource::PROSPECT_PORTAL, $strAlternateEmailAddress = '' ) {
		$objScreeningReportRequest = new CScreeningReportRequest();
		$objScreeningReportRequest->setCid( $this->getCid() );
		$objScreeningReportRequest->setScreeningId( $this->getScreeningId() );
		$objScreeningReportRequest->setScreeningApplicantId( $this->getId() );
		$objScreeningReportRequest->setScreeningReportRequestSourceId( $intSourceType );
		$objScreeningReportRequest->setScreeningReportRequestStatusId( $intScreeningReportRequestStatusTypeId );
		$objScreeningReportRequest->setAlternateEmailAddress( $strAlternateEmailAddress );

		if( false == $objScreeningReportRequest->insert( $intCurrentUserId, $objDatabase ) ) {
			trigger_error( 'Failed to load insert record into the screening_report_requests for cid [' . $this->getCid() . '] and screening id [' . $this->getScreeningId() . '] and cid [' . $this->getId() . ']', E_USER_WARNING );
			return false;
		}

		return true;
	}

	public function createChildTransactions( $intCurrentUserId, $intParentScreeningTransactionId, $intPropertyId, $arrobjScreeningAccountRates, $objScreeningDatabase, $intParentScreenTypeId ) {
		$arrobjScreeningApplicantChildTransactions = CScreeningApplicantChildTransactions::fetchActiveScreeningApplicantChildTransactionsByScreeningIdByScreeningApplicantIdByScreeningTransactionId( $this->getScreeningId(), $this->getId(), $intParentScreeningTransactionId, $objScreeningDatabase );

		if( false == valArr( $arrobjScreeningApplicantChildTransactions ) ) return false;

		$arrobjRekeydScreeningApplicantChildTransactions = rekeyObjects( 'SearchType', $arrobjScreeningApplicantChildTransactions );
		$arrmixManualCriminalSearches = array_keys( $arrobjRekeydScreeningApplicantChildTransactions );

		$arrobjExistingManualCriminalTransactions = $this->fetchAllScreeningApplicantTransactionsByScreenTypeIds( CScreenType::$c_arrintManualCriminalSearchScreenTypeIds, $objScreeningDatabase );

		$objScreeningPackageScreenType = new CScreeningPackageScreenTypeAssociation();

		$arrobjChildTransactions = [];

		foreach( $arrobjScreeningApplicantChildTransactions as $objScreeningApplicantChildTransaction ) {
				$objScreeningPackageScreenType->setScreenTypeId( $objScreeningApplicantChildTransaction->getScreenTypeId() );
				$objScreeningPackageScreenType->setScreeningDataProviderId( $objScreeningApplicantChildTransaction->getScreeningDataProviderId() );

				$arrmixCriminalSearch = $this->getAdditionalManualCriminalSearchTypes( $intCurrentUserId, $objScreeningPackageScreenType, $arrobjExistingManualCriminalTransactions, array( $objScreeningApplicantChildTransaction->getSearchType() ), $objScreeningDatabase );

				if( true == valArr( $arrmixCriminalSearch ) ) {
				    $arrstrStates = explode( '~', $objScreeningApplicantChildTransaction->getSearchType() );
                    $strSearchState = ( 2 == \Psi\Libraries\UtilFunctions\count( $arrstrStates ) ) ? $arrstrStates[1] : $objScreeningApplicantChildTransaction->getSearchType();
                    if( CScreenType::CRIMINAL != $intParentScreenTypeId && CScreeningChildTransactionRequestType::SYSTEM == $objScreeningApplicantChildTransaction->getScreeningChildTransactionRequestTypeId() && true == in_array( $strSearchState, CScreeningUtils::SKIP_STATE_COUNTY_SEARCH_RULE_FOR_STATES ) && true == $this->getIsNonUsApplicantAddress() && true == in_array( $this->m_strState, CScreeningUtils::SKIP_STATE_COUNTY_SEARCH_RULE_FOR_STATES ) ) continue;

                    $objScreeningTransaction = $this->insertScreeningTransaction( $intCurrentUserId, $intPropertyId, $objScreeningPackageScreenType, $arrobjScreeningAccountRates, $objScreeningDatabase, CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN, $objScreeningApplicantChildTransaction->getSearchType() );

					$objScreeningApplicantChildTransaction->setChildTransactionId( $objScreeningTransaction->getId() );

					$objScreeningApplicantChildTransaction->update( $intCurrentUserId, $objScreeningDatabase );

					$arrobjChildTransactions[] = $objScreeningTransaction;
				}
		}

		return $arrobjChildTransactions;
	}

	public function setChildParentTransactions( $arrobjEvalScreeningTransactions, $objScreeningDatabase ) {
		if( false == valArr( $arrobjEvalScreeningTransactions ) ) return;

		$arrobjParentNationalCriminalTransactions = CScreeningTransactions::fetchActiveScreeningTransactionsByScreenTypeIdsByScreeningIdByScreeningApplicantIdByCid( array( CScreenType::CRIMINAL ), $this->getScreeningId(), $this->getId(), $this->getCid(), $objScreeningDatabase );

		// This is vary rare scenario..but if it fails to fetch the data then it will return

		if( false == valArr( $arrobjParentNationalCriminalTransactions ) ) return $arrobjEvalScreeningTransactions;

		$this->setScreeningTransactions( $arrobjParentNationalCriminalTransactions );

		foreach( $arrobjEvalScreeningTransactions as $objEvalScreeningTransactions ) {
			$objEvalScreeningTransactions->setParentScreeningTransaction( $this, $objScreeningDatabase );
		}

		return $arrobjEvalScreeningTransactions;
	}

	public function getManualCriminalSearchCriterias( $intScreenTypeId, $objScreeningDatabase ) {
		$arrmixResult = [];

		$intSearchCriteriaTypeId = getArrayElementByKey( $intScreenTypeId, CScreeningSearchCriteriaType::$c_arrintScreenTypeSearchTypeIds );

		$strWhere = 'WHERE screening_package_id = ' . ( int ) $this->getScreeningPackageId() . ' AND screening_search_criteria_type_id = ' . ( int ) $intSearchCriteriaTypeId . ' AND is_published = 1';
		$intSearchCount = CScreeningPackageScreenTypeCriterias::fetchScreeningPackageScreenTypeCriteriaCount( $strWhere, $objScreeningDatabase );

		if( 0 == $intSearchCount ) return $arrmixResult;

		$arrmixScreeningPackageScreenTypeCriterias = $this->fetchCustomScreeningPackageScreenTypeCriteriasBySearchTypeId( $intSearchCriteriaTypeId, $objScreeningDatabase );
		$arrmixScreeningPackageScreenTypeCriterias = ( true == valArr( $arrmixScreeningPackageScreenTypeCriterias ) && true == array_key_exists( 'data', $arrmixScreeningPackageScreenTypeCriterias ) ) ? getArrayElementByKey( 'data', $arrmixScreeningPackageScreenTypeCriterias ) : array();

		$arrmixScreeningCriminalDescriptions    = getArrayFieldValuesByFieldName( 'offense_description', $arrmixScreeningPackageScreenTypeCriterias );

		if( false == valArr( $arrmixScreeningPackageScreenTypeCriterias ) || false == valArr( array_filter( $arrmixScreeningPackageScreenTypeCriterias ) ) ) return $arrmixResult;

		foreach( $arrmixScreeningPackageScreenTypeCriterias as $arrmixScreeningPackageScreenTypeCriteria ) {
			$arrmixResult[$arrmixScreeningPackageScreenTypeCriteria['screen_type_id']] = json_decode( $arrmixScreeningPackageScreenTypeCriteria['criteria'] );
		}

		if( true == in_array( 't', $arrmixScreeningCriminalDescriptions ) ) {
			$arrmixCriminalOffenseDescriptions = CScreeningListItems::fetchActiveScreeningApplicantListItemsByScreeningListTypeId( CScreeningListType::OFFENSE_DESCRIPTION, $objScreeningDatabase );
		}

		return array( $arrmixResult, $arrmixCriminalOffenseDescriptions );
	}

	public function setChildScreeningTransactions( $arrobjScreeningTransactions, $objDatabase ) {
		$arrobjRekeyedScreeningTransactions = rekeyObjects( 'ScreenTypeId', $arrobjScreeningTransactions );

		$arrintChildScreenTypeIds = array_intersect_key( CScreenType::$c_arrintScreenTypeChildScreenTypeIds, $arrobjRekeyedScreeningTransactions );

		if( false == valArr( $arrintChildScreenTypeIds ) ) return $arrobjScreeningTransactions;

		$arrintAllChildScreenTypeIds = [];

		foreach( $arrintChildScreenTypeIds as $intParentScreenTypeId => $arrintChildScreenTypeId ) {
			if( false == valArr( $arrintAllChildScreenTypeIds ) ) {
				$arrintAllChildScreenTypeIds = $arrintChildScreenTypeId;
			} else {
				$arrintAllChildScreenTypeIds = array_merge( $arrintAllChildScreenTypeIds, $arrintChildScreenTypeId );
			}
		}

		$arrobjChildScreeningTransactions = CScreeningTransactions::fetchScreeningTransactionsByScreenTypeIdsByScreeningIdByScreeningApplicantIdByCid( $arrintAllChildScreenTypeIds, $this->getScreeningId(), $this->getId(), $this->getCid(), $objDatabase );

		if( false == valArr( $arrobjChildScreeningTransactions ) ) return $arrobjScreeningTransactions;

		$arrobjRekeyedChildScreeningTransactions = rekeyObjects( 'ParentScreenTypeId', $arrobjChildScreeningTransactions, true, true );

		$arrobjParentScreeningTransactions = [];
		foreach( $arrobjScreeningTransactions as $objScreeningTransaction ) {
			$objScreeningTransaction->setChildTransactions( getArrayElementByKey( $objScreeningTransaction->getScreenTypeId(), $arrobjRekeyedChildScreeningTransactions ) );
			if( false == in_array( $objScreeningTransaction->getScreenTypeId(), $arrintAllChildScreenTypeIds ) ) {
				$arrobjParentScreeningTransactions[] = $objScreeningTransaction;
			}
		}

		return $arrobjParentScreeningTransactions;
	}

	/**
	 * @param      $intApplicationId
	 * @param      $intScreeningReportRequestStatusTypeId
	 * @param      $intCurrentUserId
	 * @param bool $boolIsCheckedOnlyInProgress
	 * @param      $objScreeningDatabase
	 * @return bool
	 * @Description : After Screening it is required to check if any request is in progress only then status should be report ready
	 *                While reopen the request not required to check is record in progress or not
	 */
	public function updateScreeningApplicantScreeningReportRequestStatus( $intApplicationId, $intScreeningReportRequestStatusTypeId, $intCurrentUserId, $objScreeningDatabase ) {
    	if( false == valId( $intScreeningReportRequestStatusTypeId ) || false == valId( $intCurrentUserId ) ) {
			return false;
		}

		$objScreeningReportRequest = CScreeningReportRequests::fetchScreeningApplicantReportRequestByScreeningIdByScreeningApplicantId( $this->getScreeningId(), $this->getId(), $objScreeningDatabase );

		if( false == valObj( $objScreeningReportRequest, 'CScreeningReportRequest' ) ) {
			return false;
		}

		if( CScreeningReportRequestStatus::INPROGRESS == $intScreeningReportRequestStatusTypeId ) {
			$objScreeningReportRequest->setReportSentBy( NULL );
			$objScreeningReportRequest->setReportSentOn( NULL );
		}

		$objScreeningReportRequest->setScreeningReportRequestStatusId( $intScreeningReportRequestStatusTypeId );

		if( false == $objScreeningReportRequest->update( $intCurrentUserId, $objScreeningDatabase ) ) {
			trigger_error( 'Failed to update screening report requests status for application_id[ ' . ( int ) $intApplicationId . '] and applicant id [ ' . $this->getCustomApplicantId() . ' ]', E_USER_WARNING );
			return false;
		}

		return true;
	}

    public function getCaliforniaReportRequest( $objScreeningDatabase ) {
        return CScreeningReportRequests::fetchScreeningApplicantReportRequestByScreeningIdByScreeningApplicantId( $this->getScreeningId(), $this->getId(), $objScreeningDatabase );
    }

    public function sendCaliforniaConsumerReportEmail( $intApplicationId, $objStdServiceData, $objDatabase ) {
	    // verify is request is inserted in screening_report_request table or not
        if( false == CScreeningReportRequests::fetchScreeningApplicantReportRequestCountByCidByScreeningIdByScreeningApplicantIdByStatusId( $this->getCid(), $this->getScreeningId(), $this->getId(), $objDatabase ) ) {
            return;
        }

        // if request is inserted then send the mail to support team
        $objResidentVerifyEmail = CResidentVerifyEmailLibraryFactory::createEmailLibrary( CResidentVerifyEmailTriggers::SEND_SUPPORT_EMAIL );
        $objResidentVerifyEmail->sendCaliforniaConsumerReport( $this, $intApplicationId, $objStdServiceData, $objDatabase );
    }

    public function getAllScreeningApplicantTransactionResult( $objDatabase ) {
        $arrobjScreeningTransactions = CScreeningTransactions::fetchActiveScreeningTransactionsByScreeningIdByScreeningApplicantIdByCid( $this->getScreeningId(), $this->getId(), $this->getCid(), $objDatabase );
        return $arrobjScreeningTransactions;
    }

	public function getDisputedScreeningApplicantIds( $objDatabase ) {
		$objScreeningApplicant = CScreeningApplicants::fetchScreeningApplicantByIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( false == valObj( $objScreeningApplicant, 'CScreeningApplicant' ) ) return false;
		// if ssn is empty then check last name,first name,dob
		if( '' == $objScreeningApplicant->getTaxNumberEncrypted() ) {
			$arrintDisputedScreeningApplicantIds    = CConsumers::fetchDisputedConsumerByScreeningApplicant( $objScreeningApplicant, $objDatabase );
			return ( true == valArr( $arrintDisputedScreeningApplicantIds ) ) ? rekeyArray( 'dispute_type_id', $arrintDisputedScreeningApplicantIds ) : $arrintDisputedScreeningApplicantIds;
		} else {

			// for valid ssn fetch complete ssn
			$arrstrDisputedScreeningApplicantDetails           = CConsumers::fetchConsumerByLastFourSsnForExcludeScreeningApplicantId( $objScreeningApplicant->getTaxNumberLastFour(), $objScreeningApplicant->getId(), $objDatabase );

			if( true == valArr( $arrstrDisputedScreeningApplicantDetails ) ) {
				// for valid ssn match existing applicant ssn with previously disputed applicant's ssn
				$arrintDisputedScreeningApplicantIds    = array_keys( rekeyArray( 'screening_applicant_id', $arrstrDisputedScreeningApplicantDetails ) );
				$arrstrDisputedScreeningApplicantsSsn   = CScreeningApplicants::fetchCustomScreeningApplicantInformationByIds( $arrintDisputedScreeningApplicantIds, $objDatabase );
				$arrstrDisputedScreeningApplicantsSsn   = rekeyArray( 'tax_number', $arrstrDisputedScreeningApplicantsSsn, false, true );

				if( true == array_key_exists( $objScreeningApplicant->getTaxNumber(), $arrstrDisputedScreeningApplicantsSsn ) ) {
					$arrstrDisputedScreeningApplicantDetails = $arrstrDisputedScreeningApplicantsSsn[$objScreeningApplicant->getTaxNumber()];
					 return ( true == valArr( $arrintDisputedScreeningApplicantIds ) ) ? rekeyArray( 'dispute_type_id', $arrstrDisputedScreeningApplicantDetails ): $arrstrDisputedScreeningApplicantDetails;
				}

			} else {
				// if unable to fetcht the record by ssn then, check for name first name last
				$arrintDisputedScreeningApplicantIds  = CConsumers::fetchDisputedConsumerByScreeningApplicant( $objScreeningApplicant, $objDatabase );
				return ( true == valArr( $arrintDisputedScreeningApplicantIds ) ) ? rekeyArray( 'dispute_type_id', $arrintDisputedScreeningApplicantIds ) : $arrintDisputedScreeningApplicantIds;
			}
		}
	}

	public function getCompletedDisputes( $objDatabase ) {
		$arrmixCompletedDisputes    = CConsumerDisputes::fetchCompletedDisputesByScreeningApplicantId( $this->getId(), $objDatabase );
		$arrmixCompletedDisputes = ( true == valArr( $arrmixCompletedDisputes ) ) ? rekeyArray( 'dispute_type_id', $arrmixCompletedDisputes ) : $arrmixCompletedDisputes;
		return $arrmixCompletedDisputes;
	}

    public function evaluatePreciseIdTransactions( $intCurrentUserId, $objScreeningDatabase ) {
        $arrobjScreeningTransactions = CScreeningTransactions::fetchScreeningTransactionsByScreenTypeIdsByScreeningIdByScreeningApplicantIdByCid( CScreenType::$c_arrintPreciseIdScreenTypeIds, $this->getScreeningId(), $this->getId(), $this->getCid(), $objScreeningDatabase );
        $arrobjScreeningTransactions = rekeyObjects( 'ScreenTypeId', $arrobjScreeningTransactions );

	   if( false == valArr( $arrobjScreeningTransactions ) ) {
	       return false;
       }

	   $objPreciseIdScreeningTransaction = getArrayElementByKey( CScreenType::PRECISE_ID, $arrobjScreeningTransactions );
	   unset( $arrobjScreeningTransactions[CScreenType::PRECISE_ID] );

       krsort( $arrobjScreeningTransactions );

       $intErrorCount   = 0;
       $intPassCount    = 0;
       $intFailCount    = 0;
       $intPendingCount = 0;
       $intPendingReviewCount = 0;
	   $intCancelledCount = 0;
	   $intCompletedCancelledCount = 0;

       foreach( $arrobjScreeningTransactions as $objScreeningTransaction ) {
           switch( $objScreeningTransaction->getScreeningStatusTypeId() ) {
               case CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR:
                   $intErrorCount++;
                   break;

               case CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW:
                   $intPendingReviewCount++;
                   break;

               case CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN:
                   $intPendingCount++;
                   break;

	           case CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED:
		           $intCancelledCount++;
		           break;

	           case CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED:
		           $intCompletedCancelledCount++;
		           break;

               case CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED:
                   switch( $objScreeningTransaction->getScreeningRecommendationTypeId() ) {
                       case CScreeningRecommendationType::FAIL:
                           $intFailCount++;
                           break;

                       case CScreeningRecommendationType::PASS:
                           $intPassCount++;
                           break;

                       default:
                           // handle this case
                   }
                   break;

               default:
                   // handle this case
           }
       }

        // Fetch associated screening_criteria_id with screen type
        $intScreeningCriteriaId = CScreeningPackageScreenTypeAssociations::fetchActiveScreeningCriteriaIdByScreenTypeIdByScreeningPackageId( CScreenType::PRECISE_ID, $this->getScreeningPackageId(), $objScreeningDatabase );
        $arrobjPreciseIdCriteriaConditionSets = CScreeningConditionSets::fetchAllActiveScreeningCriteriaConditionSetByScreeningCriteriaIdByCid( $intScreeningCriteriaId, $this->getCid(), $objScreeningDatabase );
        $arrobjPreciseIdCriteriaConditionSets = rekeyObjects( 'ScreeningRecommendationTypeId', $arrobjPreciseIdCriteriaConditionSets );

        $intPreciseIdRecommendationTypeId   = NULL;
        $intScreeningTransactionStatus      = NULL;
        $intScreeningConditionSetId         = NULL;
        $boolSetScreeningCompletedOn        = false;

       if( 0 < $intErrorCount && 0 == $intFailCount ) {
           $intPreciseIdRecommendationTypeId = CScreeningRecommendationType::ERROR;
           $intScreeningTransactionStatus = CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR;
       } else if( 0 < $intPendingCount ) {
           $intPreciseIdRecommendationTypeId = CScreeningRecommendationType::PENDING_UNKNOWN;
           $intScreeningTransactionStatus = CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN;
       } else if( 0 < $intPendingReviewCount ) {
           $intPreciseIdRecommendationTypeId = CScreeningRecommendationType::PENDING_UNKNOWN;
           $intScreeningTransactionStatus = CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW;
       } else if( 0 < $intPassCount ) {
           $intPreciseIdRecommendationTypeId = CScreeningRecommendationType::PASS;
           $intScreeningTransactionStatus = CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED;
           $intScreeningConditionSetId = getArrayElementByKey( $intPreciseIdRecommendationTypeId, $arrobjPreciseIdCriteriaConditionSets )->getId();
           $boolSetScreeningCompletedOn = true;
       } else if( 0 < $intFailCount ) {
           $intPreciseIdRecommendationTypeId = CScreeningRecommendationType::PASSWITHCONDITIONS;
           $intScreeningTransactionStatus = CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED;
           $intScreeningConditionSetId = getArrayElementByKey( $intPreciseIdRecommendationTypeId, $arrobjPreciseIdCriteriaConditionSets )->getId();

	       $arrmixScreeningConditionDetails[CScreeningConfigPreferenceType::ID_VERIFICATION] = $intScreeningConditionSetId;
	       $objPreciseIdScreeningTransaction->createScreeningTransactionConditions( $arrmixScreeningConditionDetails, $intCurrentUserId, $objScreeningDatabase );

	       $boolSetScreeningCompletedOn = true;
       } else if( 0 < $intCancelledCount ) {
	       $intPreciseIdRecommendationTypeId = CScreeningRecommendationType::PENDING_UNKNOWN;
	       $intScreeningTransactionStatus = CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED;
       } else if( 0 < $intCompletedCancelledCount ) {
	       $intPreciseIdRecommendationTypeId = CScreeningRecommendationType::PENDING_UNKNOWN;
	       $intScreeningTransactionStatus = CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED_CANCELLED;
       } else {
           $intPreciseIdRecommendationTypeId = CScreeningRecommendationType::PENDING_UNKNOWN;
           $intScreeningTransactionStatus = CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN;
       }
        if( false == is_null( $objPreciseIdScreeningTransaction->getScreeningCompletedOn() ) ) {
            $objPreciseIdScreeningTransaction->setScreeningRecommendationUpdatedOn( 'NOW()' );
        }

        $objPreciseIdScreeningTransaction->setScreeningRecommendationTypeId( $intPreciseIdRecommendationTypeId );
        $objPreciseIdScreeningTransaction->setScreeningStatusTypeId( $intScreeningTransactionStatus );
        $objPreciseIdScreeningTransaction->setScreeningPackageConditionSetId( $intScreeningConditionSetId );
        $objPreciseIdScreeningTransaction->setScreeningCompletedOn( ( true == $boolSetScreeningCompletedOn ) ? 'NOW()' : NULL );

        if( false == $objPreciseIdScreeningTransaction->update( $intCurrentUserId, $objScreeningDatabase ) ) {
            trigger_error( 'Failed to update the precise id status/recommendation for screening transaction id : ' . $objPreciseIdScreeningTransaction->getId() . ' and cid : ' . $objPreciseIdScreeningTransaction->getCid(), E_USER_WARNING );
            return false;
        }

        return $objPreciseIdScreeningTransaction;
    }

    public function processPreciseIdTransactions( $intCurrentUserId, $objStdServiceData, $objScreeningDatabase ) {
	    $arrobjScreeningTransactions = CScreeningTransactions::fetchScreeningTransactionsByScreenTypeIdsByScreeningIdByScreeningApplicantIdByCid( CScreenType::$c_arrintPreciseIdScreenTypeIds, $this->getScreeningId(), $this->getId(), $this->getCid(), $objScreeningDatabase );
        $arrobjScreeningTransactions = rekeyObjects( 'ScreenTypeId', $arrobjScreeningTransactions );

        krsort( $arrobjScreeningTransactions );

        $objScreeningTransaction = current( $arrobjScreeningTransactions );

        if( false == valObj( $objScreeningTransaction, 'CScreeningTransaction' ) ) {
            trigger_error( 'Failed to load screening transaction for screening_id id : ' . $this->getScreeningId() . ' and cid : ' . $this->getCid(), E_USER_WARNING );
            return false;
        }

        switch( $objScreeningTransaction->getScreenTypeId() ) {
            case CScreenType::PRECISE_ID:
            case CScreenType::PRECISE_ID_AO_SCORE:
                if( false == is_null( $objStdServiceData->application->applicants->strUnFreezePin ) ) {
	                $arrmixAdditionalDetails        = $objScreeningTransaction->getAdditionalDetails();
                    $arrmixAdditionalDetails->freezekeypin  = $objStdServiceData->application->applicants->strUnFreezePin;
                    $objScreeningTransaction->setAdditionalDetails( $arrmixAdditionalDetails );
                }

                $objProcessor = $objScreeningTransaction->getProcessor( $intCurrentUserId, $this, $objScreeningDatabase );
                $objScreeningTransaction = $objProcessor->process();
                break;

            case CScreenType::PRECISE_ID_OTP:
                if( true == is_null( $objStdServiceData->application->applicants->code ) || true == empty( $objStdServiceData->application->applicants->code ) ) return $objScreeningTransaction;
	            $arrmixAdditionalDetails        = $objScreeningTransaction->getAdditionalDetails();
                $arrmixAdditionalDetails->code  = $objStdServiceData->application->applicants->code;
                $objScreeningTransaction->setAdditionalDetails( $arrmixAdditionalDetails );
                $objProcessor = $objScreeningTransaction->getProcessor( $intCurrentUserId, $this, $objScreeningDatabase );
                $objScreeningTransaction = $objProcessor->processScreeningTransaction();
                break;

            case CScreenType::PRECISE_ID_KIQ:
                if( true == is_null( $objStdServiceData->application->applicants->arrintAnswers ) || true == empty( $objStdServiceData->application->applicants->arrintAnswers ) ) return $objScreeningTransaction;
	            $arrmixAdditionalDetails = $arrobjScreeningTransactions[CScreenType::PRECISE_ID_AO_SCORE]->getAdditionalDetails();
                $arrmixAdditionalDetails->arrintAnswers = $objStdServiceData->application->applicants->arrintAnswers;
                $objScreeningTransaction->setAdditionalDetails( $arrmixAdditionalDetails );
                $objProcessor = $objScreeningTransaction->getProcessor( $intCurrentUserId, $this, $objScreeningDatabase );
                $objScreeningTransaction = $objProcessor->processScreeningTransaction();
                break;

            default:
                // handle this...
                break;
        }

        $this->evaluatePreciseIdTransactions( $intCurrentUserId, $objScreeningDatabase );

        $this->setScreeningTransactions( $this->fetchActiveScreeningApplicantTransactions( $objScreeningDatabase ) );

        return $objScreeningTransaction;
    }

    public function updateScreeningTransaction( $objScreeningTransaction, $intCurrentUserId, $objDatabase ) {

		$objScreeningTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN );
		$objScreeningTransaction->setScreeningRecommendationTypeId( NULL );
		$objScreeningTransaction->setScreeningPackageConditionSetId( NULL );
		$objScreeningTransaction->setUpdatedBy( $intCurrentUserId );
		$objScreeningTransaction->setScreeningCompletedOn( NULL );
		$objScreeningTransaction->setUpdatedOn( 'NOW()' );
		$objScreeningTransaction->setOriginalScreeningCompletedOn( ( true == is_null( $objScreeningTransaction->getOriginalScreeningCompletedOn() ) ) ? $objScreeningTransaction->getScreeningCompletedOn() : $objScreeningTransaction->getOriginalScreeningCompletedOn() );
		if( false == $objScreeningTransaction->update( $intCurrentUserId, $objDatabase ) ) {
			trigger_error( 'Failed to update screening transaction for screening id : ' . $this->getScreeningId() . ' and screening applicant id : ' . $this->getId(), E_USER_ERROR );
			return false;
		}

		return true;

	}

	public function createClonedTransactions( $objScreeningTransaction, $intPropertyId, $arrobjScreeningAccountRates, $intCurrentUserId, $objDatabase ) {
		$objClonedScreeningTransaction = clone $objScreeningTransaction;
		$objClonedScreeningTransaction->setId( NULL );
		$objClonedScreeningTransaction->setScreeningId( $this->getScreeningId() );
		$objClonedScreeningTransaction->setScreeningApplicantId( $this->getId() );
		$objClonedScreeningTransaction->setCid( $this->getCid() );
		$objClonedScreeningTransaction->setTransactionAmount( 0 );
		$objClonedScreeningTransaction->setPropertyId( $intPropertyId );

		$objClonedScreeningTransaction->setScreeningStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN );
		$objClonedScreeningTransaction->setScreeningRecommendationTypeId( NULL );
		$objClonedScreeningTransaction->setScreeningPackageConditionSetId( NULL );
		$objClonedScreeningTransaction->setCreatedBy( $intCurrentUserId );
		$objClonedScreeningTransaction->setUpdatedBy( $intCurrentUserId );
		$objClonedScreeningTransaction->setScreeningCompletedOn( NULL );
        $objClonedScreeningTransaction->setScreeningRecommendationUpdatedOn( NULL );

		$objClonedScreeningTransaction->setUpdatedOn( 'NOW()' );
		$objClonedScreeningTransaction->setCreatedOn( 'NOW()' );
		if( true == in_array( $objScreeningTransaction->getScreeningStatusTypeId(), [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR, CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN ] ) ) {
			$objClonedScreeningTransaction->setOriginalScreeningCompletedOn( ( true == is_null( $objScreeningTransaction->getOriginalScreeningCompletedOn() ) ) ? 'NOW()' : $objScreeningTransaction->getOriginalScreeningCompletedOn() );
			$objClonedScreeningTransaction->setScreeningRecommendationTypeId( $objScreeningTransaction->getScreeningRecommendationTypeId() );
		} else {
			$objClonedScreeningTransaction->setOriginalScreeningCompletedOn( ( true == is_null( $objScreeningTransaction->getOriginalScreeningCompletedOn() ) ) ? $objScreeningTransaction->getScreeningCompletedOn() : $objScreeningTransaction->getOriginalScreeningCompletedOn() );
		}

		if( false == $objClonedScreeningTransaction->insert( $intCurrentUserId, $objDatabase ) ) {
			trigger_error( 'Failed to insert screening transaction for screening id : ' . $this->getScreeningId() . ' and screening applicant id : ' . $this->getId(), E_USER_ERROR );
			return false;
		}

		if( CScreenType::CRIMINAL == $objClonedScreeningTransaction->getScreenTypeId() ) {
			// Add current_address criteria type id in the list..
			$this->createDependantScreeningTransactions( $intCurrentUserId, $intPropertyId, $arrobjScreeningAccountRates, [ CScreeningSearchCriteriaType::ALWAYS, CScreeningSearchCriteriaType::CURRENT_ADDRESS, CScreeningSearchCriteriaType::PROPERTY_ADDRESS ], $objClonedScreeningTransaction, $objDatabase );
		}

		return true;
	}

	public function fetchOrCreateScreeningApplicantAddress( $objDatabase ) {
		$objScreeningApplicantAddress = CScreeningApplicantAddresses::fetchScreeningApplicantAddressByScreeningApplicantIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		if( false == valObj( $objScreeningApplicantAddress, 'CScreeningApplicantAddress' ) ) {
			$objScreeningApplicantAddress = new CScreeningApplicantAddress();
		}
		return $objScreeningApplicantAddress;
	}

	public function cancelLatestInCompleteTransaction( $intUserId, $arrintScreenTypeIds, $objDatabase ) {

		$strSql = 'UPDATE 
						screening_transactions 
					SET 
						screening_status_type_id = ' . \CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED . ',
						screening_recommendation_type_id = ' . \CScreeningRecommendationType::PENDING_UNKNOWN . ',
						updated_on = NOW(),
						updated_by = ' . ( int ) $intUserId . '
					WHERE
						id IN ( 
							SELECT 
								id
							FROM
								screening_transactions
							WHERE
								screen_type_id IN( ' . sqlIntImplode( $arrintScreenTypeIds ) . ' )
								AND screening_id = ' . $this->getScreeningId() . '
								AND screening_applicant_id = ' . $this->getId() . '
								AND cid = ' . $this->getCid() . '
								AND screening_status_type_id = ' . \CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN . '
							ORDER BY created_on DESC
							LIMIT 1
						)';

		$arrmixResultData    = executeSql( $strSql, $objDatabase );

		if( true == $arrmixResultData['failed'] ) {
			return false;
		}

		return true;
	}

	private function getScreeningAccountRateByDataProviderId( $intScreeningDataProviderId, $intCid, $intPropertyId, $objDatabase ) {
	    $objScreeningAccountRate  = \Psi\Eos\Screening\CScreeningAccountRates::createService()->fetchPublishedScreeningAccountRateByPropertyIdByCidByScreeningDataProviderId( $intPropertyId, $intCid, $intScreeningDataProviderId, $objDatabase );
		return $objScreeningAccountRate;
    }

	public function fetchScreeningApplicantResult( $objScreeningDatabase ) {
		$objScreeningResponse = new stdClass();

		$objScreeningResponse->screeningStatusTypeId         = $this->getStatusTypeId();
		$objScreeningResponse->screeningRecommendationTypeId = $this->getScreeningRecommendationTypeId();
		$objScreeningResponse->customApplicantId             = $this->getCustomApplicantId();

		$objScreeningResponse->screenTypeResult = new stdClass();

		$arrobjScreeningTransactions = $this->fetchActiveScreeningApplicantTransactions( $objScreeningDatabase );

		$arrmixScreeningApplicantTransactionsResult = [];

		foreach( $arrobjScreeningTransactions as $objScreeningTransactions ) {
			if( true == in_array( $objScreeningTransactions->getScreenTypeId(), CScreenType::$c_arrintManualCriminalSearchScreenTypeIds ) ) {
				$arrmixScreeningApplicantTransactionsResult[$objScreeningTransactions->getScreenTypeId()][$objScreeningTransactions->getSearchType()]['ScreeningRecommendationTypeId']  = $objScreeningTransactions->getScreeningRecommendationTypeId();
				$arrmixScreeningApplicantTransactionsResult[$objScreeningTransactions->getScreenTypeId()][$objScreeningTransactions->getSearchType()]['ScreeningStatusTypeId']          = $objScreeningTransactions->getScreeningStatusTypeId();
			} else {
				$arrmixScreeningApplicantTransactionsResult[$objScreeningTransactions->getScreenTypeId()]['ScreeningRecommendationTypeId']  = $objScreeningTransactions->getScreeningRecommendationTypeId();
				$arrmixScreeningApplicantTransactionsResult[$objScreeningTransactions->getScreenTypeId()]['ScreeningStatusTypeId']          = $objScreeningTransactions->getScreeningStatusTypeId();
			}
		}

		$objScreeningResponse->screenTypeResult = $arrmixScreeningApplicantTransactionsResult;

		return $objScreeningResponse;
	}

	public function createScreeningLog( $intCurrentUserId, $intScreeningLogTypeId, $strStepCompletionLogTime, $intPropertyId, $objScreeningDatabase ) {
		$objScreeningLog = new CScreeningLog();
		$objScreeningLog->setCid( $this->getCid() );
		$objScreeningLog->setScreeningId( $this->getScreeningId() );
		$objScreeningLog->setScreeningApplicantId( $this->getId() );
		$objScreeningLog->setScreeningLogTypeId( $intScreeningLogTypeId );
		$objScreeningLog->setPropertyId( $intPropertyId );
		$objScreeningLog->setLogDescription( CScreeningLogType::getScreeningLogTypeText( $intScreeningLogTypeId ) );
		$objScreeningLog->setIsPublished( true );
		$objScreeningLog->setUpdatedOn( $strStepCompletionLogTime );
		$objScreeningLog->setCreatedOn( $strStepCompletionLogTime );

		if( false == $objScreeningLog->insert( $intCurrentUserId, $objScreeningDatabase ) ) {
			return false;
		}
		return true;
	}

	public function enqueuePreScreeningMessage( $intCurrentUserId, $intClusterId, $strAction, $strStepCompletionLogTime, $intScreeningLogTypeId, $intApplicationId, $objScreeningDatabase ) {
		$objScreeningPackage = \Psi\Eos\Screening\CScreeningPackages::createService()->fetchScreeningPackageByIdByCid( $this->getScreeningPackageId(), $this->getCid(), $objScreeningDatabase );
		try {
			$arrmixScreeningData                                        = [];
			$arrmixScreeningData['cid']                                 = $this->getCid();
			$arrmixScreeningData['company_user_id']                     = $intCurrentUserId;
			$arrmixScreeningData['application_id']                      = $intApplicationId;
			$arrmixScreeningData['cluster_id']                          = $intClusterId;
			$arrmixScreeningData['screening_applicant_id']              = $this->getId();
			$arrmixScreeningData['custom_applicant_id']                 = $this->getCustomApplicantId();
			$arrmixScreeningData['screening_recommendation_type_id']    = $this->getScreeningRecommendationTypeId();
			$arrmixScreeningData['completion_time']                     = $strStepCompletionLogTime;
			$arrmixScreeningData['screening_log_type_id']               = $intScreeningLogTypeId;
			$arrmixScreeningData['action']                              = $strAction;
			$arrmixScreeningData['screening_package_type_id']           = $objScreeningPackage->getScreeningPackageTypeId();
			$arrmixScreeningData['restrict_recommendation_type_ids']    = ( true == valArr( $objScreeningPackage->getStopProcessingOnRecommendationIds() ) ) ? implode( ',', $objScreeningPackage->getStopProcessingOnRecommendationIds() ) : NULL;

			$objResidentVerifyScreeningMessageSenderLibrary = new CResidentVerifyScreeningMessageSenderLibrary();

			if( false === $objResidentVerifyScreeningMessageSenderLibrary->configureProcessPreScreeningApplicants( $arrmixScreeningData ) ) {
				trigger_error( 'Failed to send pre screening message to queue for application id [' . $intApplicationId . '] and cid [' . $this->getCid() . ']', E_USER_WARNING );
				return false;
			}
			$objResidentVerifyScreeningMessageSenderLibrary->sendMessage();

		} catch( Exception $objException ) {
			trigger_error( $objException->getMessage() . 'Failed to send pre screening message to queue for application id [' . $intApplicationId . '] and cid [' . $this->getCid() . ']', E_USER_WARNING );
			return false;
		}
	}

	public function getActiveScreeningLogsByScreeningLogType( $intScreeningLogTypeId, $objScreeningDatabase ) {
		return \Psi\Eos\Screening\CScreeningLogs::createService()->fetchActiveExistingScreeningLogsByCidByScreeningIdByScreeningApplicantIdByScreeningLogTypeId( $this->getCid(), $this->getScreeningId(), $this->getId(), $intScreeningLogTypeId, $objScreeningDatabase );
	}

}
?>