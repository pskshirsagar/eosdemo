<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDelinquentEventStatusTypes
 * Do not add any new functions to this class.
 */

class CDelinquentEventStatusTypes extends CBaseDelinquentEventStatusTypes {

	public static function fetchDelinquentEventStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDelinquentEventStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDelinquentEventStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDelinquentEventStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedDelinquentEventStatusTypes( $objDatabase ) {

		$strSql = 'SELECT
    					id,
    					name,
    					description
    				FROM
    					delinquent_event_status_types
    				WHERE 
    				    is_published = 1
    				ORDER BY
    					order_num';

		return parent::fetchDelinquentEventStatusTypes( $strSql, $objDatabase );
	}

}

?>