<?php

class CScreeningDataProvider extends CBaseScreeningDataProvider {

	const VA_STATE							   = 98;
	const VA_COUNTY							   = 99;
	const VA_NATIONAL						   = 97;
	const DEMO_VA_STATE						   = 95;
	const DEMO_VA_COUNTY					   = 96;
	const DEMO_VA_NATIONAL					   = 94;

	const TAZ_WORK_CRIMINAL_TEST               = 2;
	const RV_CRIMCAT_TEST                      = 7;
	const TAZ_NATIONAL                         = 10;
	const RV_CRIMINALCAT                       = 14;
	const EXPERIAN_BUSINESS_PREM_PROFILE_TEST  = 20;
	const EXPERIAN_BUSINESS_PREMIER            = 37;

	const CRIMINAL_REVIEW_PRODUCT_CODE         = 'manual_review';

     /**
      * Get Functions
      */

    public function getUsername() {
	    return CEncryption::decryptText( $this->m_strUsernameEncrypted, CONFIG_KEY_LOGIN_USERNAME );
    }

    public function getPassword() {
	   return CEncryption::decryptText( $this->m_strPasswordEncrypted, CONFIG_KEY_LOGIN_PASSWORD );
    }

     /**
      * Set Functions
      */

    public function setUsername( $strUsername ) {
	    $this->setUsernameEncrypted( CEncryption::encryptText( trim( $strUsername ), CONFIG_KEY_LOGIN_USERNAME ) );
    }

    public function setPassword( $strPassword ) {
	    $this->setPasswordEncrypted( CEncryption::encryptText( trim( $strPassword ), CONFIG_KEY_LOGIN_PASSWORD ) );
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['username'] ) ) 				$this->setUsername( $arrmixValues['username'] );
    	if( true == isset( $arrmixValues['password'] ) ) 				$this->setPassword( $arrmixValues['password'] );

    	return;
    }

     /**
      * Validation Functions
      */

    public function valScreenTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getScreenTypeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Screen Type is required.' ) );
        }

        return $boolIsValid;
    }

	public function valScreeningDataProviderProductTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getScreeningDataProviderProductTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'screening_data_provider_product_type_id', 'Screening Data Provider Product Type is required.' ) );
		}

		return $boolIsValid;
	}

    public function valScreeningDataProviderTypeId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getScreeningDataProviderTypeId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Screening Data Provider Type is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valName( $objScreeningDatabase ) {
    	$boolIsValid = true;

    	if( true == is_null( $this->getName() ) ) {

    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
    	}

    	if( true == $boolIsValid && false == preg_match( '/^[A-Za-z][A-Za-z0-9]*(?:-[A-Za-z0-9]+)*$/', $this->getName() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name should be valid.' ) );
    	}

    	if( true == $boolIsValid && 3 > strlen( $this->getName() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name must be at least 3 characters.' ) );
    	}

    	if( true == $boolIsValid ) {

    		$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );

    		$intCount = CScreeningDataProviders::fetchScreeningDataProviderCount( ' WHERE lower( name ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) . '\'' . $strSqlCondition, $objScreeningDatabase );

    		if( 0 < $intCount ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Screening Data Provider already Exists.' ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function valUsername() {
    	$boolIsValid = true;

    	if( false == valStr( $this->getUsername() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', ' Username is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valPassword() {
    	$boolIsValid = true;

    	if( false == valStr( $this->getPassword() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', ' Password is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valRequestUrl() {

    	$boolIsValid = true;

    	if( true == is_null( $this->m_strRequestUrl ) || 0 == strlen( $this->m_strRequestUrl ) ) {
    		$boolIsValid = false;
   			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request_url', 'Request Url is required.' ) );

    	} elseif( false == CValidation::checkUrl( $this->m_strRequestUrl ) ) {
    		$boolIsValid = false;
   			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request_url', 'Request Url is not correct.' ) );
   		}

    	return $boolIsValid;
    }

    public function valRetrievalUrl() {
    	$boolIsValid = true;

    	if( true == is_null( $this->m_strResponseUrl ) || 0 == strlen( trim( $this->m_strResponseUrl ) ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'response_url', 'Response Url is required.' ) );
    		return  $boolIsValid;

    	} elseif( false == CValidation::checkUrl( $this->m_strResponseUrl ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'response_url', 'Response Url is not correct.' ) );
    	}

    	return $boolIsValid;
    }

    public function valScreeningDataProviderAssociation( $objScreeningDatabase ) {
    	$boolIsValid = true;

    	$arrobjScreeningPackageScreenTypeAssociations 	= CScreeningPackageScreenTypeAssociations::fetchPublishedScreeningPackageScreenTypeAssociationsByScreeningDataProviderId( $this->getId(), $objScreeningDatabase );

		if( true == valArr( $arrobjScreeningPackageScreenTypeAssociations ) ) {
			$arrintScreeningPackageIds = array();

			foreach( $arrobjScreeningPackageScreenTypeAssociations as $objScreeningPackageScreenTypeAssociation ) {
				$arrintScreeningPackageIds[] = $objScreeningPackageScreenTypeAssociation->getScreeningPackageId();
			}

			$arrobjScreeningPackages = CScreeningPackages::fetchPublishedScreeningPackagesByIds( $arrintScreeningPackageIds, $objScreeningDatabase );

			if( true == valArr( $arrobjScreeningPackages ) ) {
				$boolIsValid = false;
    	    	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Data Provider name [ ' . $this->getName() . ' ] cannot be deleted, it is currently associated to a package.' ) );
			}
		}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objScreeningDatabase ) {

        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
           	case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valScreenTypeId();
            	$boolIsValid &= $this->valScreeningDataProviderTypeId();
	            $boolIsValid &= $this->valScreeningDataProviderProductTypeId();
            	$boolIsValid &= $this->valName( $objScreeningDatabase );
            	$boolIsValid &= $this->valUsername();
            	$boolIsValid &= $this->valPassword();
            	$boolIsValid &= $this->valRequestUrl();
            	$boolIsValid &= $this->valRetrievalUrl();
	            $boolIsValid &= $this->valVendorCode();
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valScreeningDataProviderAssociation( $objScreeningDatabase );
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	public function valVendorCode() {
		$boolIsValid = true;

		if( true == in_array( $this->m_intScreeningDataProviderTypeId, CScreeningDataProviderType::$c_arrintRestrictManualReviewTypeIds ) ) {
			if( true == is_null( $this->m_strVendorCode ) || 0 == strlen( trim( $this->m_strVendorCode ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_code', 'Vendor code "manual_review=1;" is required for Co-Court data provider type.' ) );
				$boolIsValid = false;
			} else {
				if( 'manual_review=1;' != $this->m_strVendorCode ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_code', 'Vendor code should be "manual_review=1;" for co-court data provider type.' ) );
					$boolIsValid = false;
				}
			}
		}

		return $boolIsValid;
	}

}
?>