<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningAccountRates
 * Do not add any new functions to this class.
 */

class CScreeningAccountRates extends CBaseScreeningAccountRates {

	public static function fetchPublishedScreeningAccountRatesByScreeningAccountIdByCid( $intScreeningAccountId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sar.*,
						st.name as screen_type_name
				   FROM
						screening_account_rates sar
						JOIN screen_types st ON ( sar.screen_type_id = st.id )
				   WHERE
						sar.cid = ' . ( int ) $intCid . '
						AND sar.screening_account_id = ' . ( int ) $intScreeningAccountId . ' ORDER BY sar.screen_type_id ';

		return parent::fetchScreeningAccountRates( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningAccountRatesByCid( $intCid, $objDatabase, $boolAllFieldValues = false ) {

		$strSelectClause = ( true == $boolAllFieldValues ) ? ' sar.*, sa.screen_rate_type_id' : ' sar.id, sar.screen_type_id ';

		$strSql = ' SELECT ' .
						$strSelectClause .
				  ' FROM
						screening_account_rates sar
				  		JOIN screening_accounts sa ON ( sar.screening_account_id = sa.id )
					WHERE
						sa.is_active = TRUE
						AND sa.cid = ' . ( int ) $intCid;

		return parent::fetchScreeningAccountRates( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningAccountRatesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolAllFieldValues = false ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSelectClause = ( true == $boolAllFieldValues ) ? ' sar.*, sa.screen_rate_type_id' : ' sar.id, sar.screen_type_id ';

		$strSql = ' SELECT ' .
		          $strSelectClause .
		          ' FROM
						screening_account_rates sar
				  		JOIN screening_accounts sa ON ( sar.screening_account_id = sa.id AND sa.cid = sar.cid )
				  		JOIN screening_property_accounts spa ON(spa.screening_account_id = sa.id AND sa.cid = spa.cid )
					WHERE
						sa.is_active = TRUE
						AND spa.is_published = 1
						AND spa.property_id = ' . ( int ) $intPropertyId . ' AND sa.cid = ' . ( int ) $intCid;

		return parent::fetchScreeningAccountRates( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningAccountRatesCountByScreenTypeIdByCid( $intScreenTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count( sar.id )
					FROM
						screening_account_rates sar
						JOIN screening_accounts sa ON ( sar.screening_account_id = sa.id )
					WHERE
						sa.is_active = TRUE
						AND sar.screen_type_id = ' . ( int ) $intScreenTypeId . '
						AND sa.cid = ' . ( int ) $intCid;

		$arrintCount = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrintCount[0]['count'] ) ) ? ( int ) $arrintCount[0]['count'] : 0;
	}

	public static function fetchPublishedScreeningAccountRatesByScreenTypeIdsByCid( $arrintScreenTypeIds, $intCid, $objDatabase, $boolAllFieldValues = false, $intScreenRateTypeId = NULL ) {

		if( false == valArr( $arrintScreenTypeIds ) ) return;

		$strWhere = '';
		if( false == is_null( $intScreenRateTypeId ) ) {
			$strWhere = ' AND sa.screen_rate_type_id = ' . ( int ) $intScreenRateTypeId;
		}

		$strSelectClause = ( true == $boolAllFieldValues ) ? ' sar.*, sa.screen_rate_type_id, st.name as screen_type_name' : ' sar.id, sar.screen_type_id ';

		$strSql = 'SELECT ' .
					$strSelectClause .
				  ' FROM
						screening_account_rates sar
				  		JOIN screening_accounts sa ON ( sar.screening_account_id = sa.id )
				  		JOIN screen_types st ON st.id = sar.screen_type_id
					WHERE
						sa.is_active = TRUE
						' . $strWhere . '
						AND sa.cid = ' . ( int ) $intCid . '
						AND sar.screen_type_id IN( ' . implode( ',', $arrintScreenTypeIds ) . ' )';

		return parent::fetchScreeningAccountRates( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningAccountRatesByScreeningAccountIdsByScreenTypeIdsByCid( $arrintScreeningAccountIds, $arrintScreeningScreenTypeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintScreeningAccountIds ) || false == valArr( $arrintScreeningScreenTypeIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						sar.*
					FROM
						screening_account_rates sar
						JOIN screening_accounts sa ON ( sar.screening_account_id = sa.id and sar.cid = sa.cid )
					WHERE
						sa.is_active = TRUE
						AND sar.screening_account_id IN(' . sqlIntImplode( $arrintScreeningAccountIds ) . ' )
						AND sar.screen_type_id IN(' . sqlIntImplode( $arrintScreeningScreenTypeIds ) . ')
						AND sa.cid = ' . ( int ) $intCid;

		return parent::fetchScreeningAccountRates( $strSql, $objDatabase );
	}

	public static function fetchScreeningAccountRatesForCorporateScreenTypes( $objDatabase ) {

		$strSql = 'SELECT
					    DISTINCT sar.*
					FROM
					    screening_accounts sa
					    JOIN screening_account_rates sar ON( sa.id = sar.screening_account_id and sa.cid = sar.cid )
					WHERE
					    sa.is_active = TRUE
					    AND sa.screen_rate_type_id = ' . CScreenRateType::COMPONENT . '
					    AND sar.screen_type_id IN( ' . sqlIntImplode( CScreenType::$c_arrintBusinessCorporateScreenTypeIds ) . ' )';

		return parent::fetchScreeningAccountRates( $strSql, $objDatabase );
	}

}
?>