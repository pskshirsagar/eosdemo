<?php

class CScreeningPackageConditionValueType extends CBaseScreeningPackageConditionValueType {

    const DOCUMENT   = 1;
    const GUARANTOR  = 2;
    const AMOUNT     = 3;

    public static function getScreeningPackageConditionValueTypeByConditionTypeId( $intConditionTypeId ) {
        switch( $intConditionTypeId ) {
            case CTransmissionConditionType::VERIFY_IDENTITY:
            case CTransmissionConditionType::REQUIRE_CERTIFIED_FUNDS:
	        case CTransmissionCOnditionType::CUSTOM_SCREENING_CONDITION:
	        case CTransmissionConditionType::APRROVED_WITH_QUALIFYING_ROOMMATE:
                $intScreningPackageConditionValueType = self::DOCUMENT;
                break;

            case CTransmissionConditionType::INCREASE_DEPOSIT:
            case CTransmissionConditionType::INCREASE_RENT:
            case CTransmissionConditionType::OTHER_CHARGES:
            case CTransmissionConditionType::REPLACE_DEPOSIT:
                $intScreningPackageConditionValueType = self::AMOUNT;
                break;

            case CTransmissionConditionType::REQUIRE_GUARANTOR:
                $intScreningPackageConditionValueType = self::GUARANTOR;
                break;

            default:
                // added default case
                $intScreningPackageConditionValueType = 0;

        }

        return $intScreningPackageConditionValueType;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>