<?php

class CRvMasterTestCase extends CBaseRvMasterTestCase {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningResultDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getRvTestCaseResultByTestCaseId( $intTestCaseId,  $objResidentVerifyTestCaseService ) {
		$arrmixScreeningTestCases = $objResidentVerifyTestCaseService->getRvTestCaseResultByMasterTestCaseIdByTestCaseId( $this->getId(), $intTestCaseId );

		// Update the screening result details and master screening result details with json decoded value
		$arrmixScreeningTestCases['screening_result_details']        = json_decode( $arrmixScreeningTestCases['screening_result_details'], true );
		$arrmixScreeningTestCases['master_screening_result_details'] = json_decode( $arrmixScreeningTestCases['master_screening_result_details'], true );

		// Assigned the final data to member variable
		return $arrmixScreeningTestCases;
	}

}
?>