<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCriteriaConditionSets
 * Do not add any new functions to this class.
 */

class CScreeningCriteriaConditionSets extends CBaseScreeningCriteriaConditionSets {

	public static function fetchActiveScreeningConditionTemplatesByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_criteria_condition_sets
						
					WHERE
						is_active = true
						AND screening_criteria_id = ' . ( int ) $intScreeningCriteriaId;

		$arrmixResult = executeSql( $strSql, $objDatabase );

		return ( false == getArrayElementByKey( 'failed', $arrmixResult ) ) ? getArrayElementByKey( 'data', $arrmixResult ) : [];
	}

	public static function fetchActiveScreeningCriteriaConditionSetsByScreeningConditionSetId( $intScreeningConditionSetId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_criteria_condition_sets
					WHERE
						screening_condition_set_id = ' . ( int ) $intScreeningConditionSetId . '
						AND is_active = TRUE';

		return parent::fetchScreeningCriteriaConditionSets( $strSql, $objDatabase );

	}

	public static function fetchActiveScreeningConditionSetsByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						screening_criteria_condition_sets
						
					WHERE
						is_active IS TRUE
						AND screening_criteria_id = ' . ( int ) $intScreeningCriteriaId;

		return parent::fetchScreeningCriteriaConditionSets( $strSql, $objDatabase );
	}

	public function fetchScreeningCriteriaConditionSetCountByScreeningConditionTemplateIdByCid( $intScreeningConditionTemplateId, $intCid, $objScreeningDatabase ) {
		if( false == valId( $intScreeningConditionTemplateId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						count(sccs.id) 
					FROM 
						screening_criteria_condition_sets sccs
						JOIN screening_criterias sc on sc.id = sccs.screening_criteria_id
					WHERE 
						sccs.screening_condition_set_id = ' . ( int ) $intScreeningConditionTemplateId . ' 
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.is_published = TRUE';

		$arrmixResult = executeSql( $strSql, $objScreeningDatabase );
		return $arrmixResult['data'][0]['count'];
	}

}
?>