<?php

class CScreeningPackageAvailableCondition extends CBaseScreeningPackageAvailableCondition {

    public function valScreeningPackageChargeCodeAmountTypeId() {
        $boolIsValid 		= true;
        $strConditionType	= NULL;

        if( true == in_array( $this->getScreeningPackageConditionTypeId(), array( CScreeningPackageConditionType::INCREASE_SECURITY_DEPOSIT ) ) ) $strConditionType = 'Increase Security Deposit';
        if( true == in_array( $this->getScreeningPackageConditionTypeId(), array( CScreeningPackageConditionType::INCREASE_RENT ) ) ) $strConditionType = 'Increase Rent';
        if( true == in_array( $this->getScreeningPackageConditionTypeId(), array( CScreeningPackageConditionType::OTHER_CHARGES ) ) ) $strConditionType = 'Other Charges';

        if( true == is_null( $this->getScreeningPackageChargeCodeAmountTypeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Charge Code Amount Type is required for ' . $strConditionType . '.' ) );
        }

        return $boolIsValid;
    }

    public function valScreeningPackageAvailableConditionName() {

    	$boolIsValid 		= true;
        $strConditionType	= NULL;

        if( true == in_array( $this->getScreeningPackageConditionTypeId(), array( CScreeningPackageConditionType::INCREASE_SECURITY_DEPOSIT ) ) ) $strConditionType = 'Increase Security Deposit';
        if( true == in_array( $this->getScreeningPackageConditionTypeId(), array( CScreeningPackageConditionType::INCREASE_RENT ) ) ) $strConditionType = 'Increase Rent';
        if( true == in_array( $this->getScreeningPackageConditionTypeId(), array( CScreeningPackageConditionType::OTHER_CHARGES ) ) ) $strConditionType = 'Other Charges';

        if( true == is_null( $this->getScreeningPackageAvailableConditionName() ) ) {
          	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Available Condition Custom Name is required for ' . $strConditionType . '.' ) );
        }

        return $boolIsValid;
    }

    public function valBaseChargeCodeId() {
        $boolIsValid 		= true;
        $strConditionType	= NULL;

        if( true == in_array( $this->getScreeningPackageConditionTypeId(), array( CScreeningPackageConditionType::INCREASE_SECURITY_DEPOSIT ) ) ) $strConditionType = 'Increase Security Deposit';
        if( true == in_array( $this->getScreeningPackageConditionTypeId(), array( CScreeningPackageConditionType::INCREASE_RENT ) ) ) $strConditionType = 'Increase Rent';
        if( true == in_array( $this->getScreeningPackageConditionTypeId(), array( CScreeningPackageConditionType::OTHER_CHARGES ) ) ) $strConditionType = 'Other Charges';

        if( true == is_null( $this->getBaseChargeCodeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Base Charge Code is required for ' . $strConditionType . '.' ) );
        }
        return $boolIsValid;
    }

    public function valApplyToChargeCodeId() {
        $boolIsValid = true;
        $strConditionType	= NULL;

        if( true == in_array( $this->getScreeningPackageConditionTypeId(), array( CScreeningPackageConditionType::INCREASE_SECURITY_DEPOSIT ) ) ) $strConditionType = 'Increase Security Deposit';
        if( true == in_array( $this->getScreeningPackageConditionTypeId(), array( CScreeningPackageConditionType::OTHER_CHARGES ) ) ) $strConditionType = 'Other Charges';

        if( true == is_null( $this->getApplyToChargeCodeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Apply Charge Code is required for ' . $strConditionType . '.' ) );
        }
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:

        		if( false == $boolIsValid ) {
        			return $boolIsValid;
        		}

        		$boolIsValid &= $this->valScreeningPackageAvailableConditionName();
        		$boolIsValid &= $this->valBaseChargeCodeId();
        		$boolIsValid &= $this->valApplyToChargeCodeId();
        		$boolIsValid &= $this->valScreeningPackageChargeCodeAmountTypeId();
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function unPublishScreeningPackageAvailableConditions( $arrintScreeningPackageAvailableConditionTypeIds, $intCid, $intUserId, $objScreeningDatabase ) {

        if( false == valArr( $arrintScreeningPackageAvailableConditionTypeIds ) ) return NULL;

        $boolIsValid    = false;

        $strSql = ' UPDATE
                       screening_package_available_conditions
                    SET
                       is_published = 0,
                       updated_by = ' . ( int ) $intUserId . ',
                       updated_on = NOW()
                    WHERE
                       cid = ' . ( int ) $intCid . '
                       AND screening_package_condition_type_id IN ( ' . implode( ',', array_keys( $arrintScreeningPackageAvailableConditionTypeIds ) ) . ' )';

        $boolIsValid = fetchData( $strSql, $objScreeningDatabase );
        if( false == $boolIsValid ) $objScreeningDatabase->rollback();

        return $boolIsValid;
    }

}
?>