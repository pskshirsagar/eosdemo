<?php

class CRvTestCaseResults extends CBaseRvTestCaseResults {

	public static function fetchTestCaseScreeningTransactionDetailsByScreeningIds( $arrintScreeningIds, $objDatabase ) {
		if( !valArr( $arrintScreeningIds ) ) return NULL;

		$strSql = 'SELECT
						sa.name_first,
						sa.name_last,
						st.screen_type_id,
						sa.custom_applicant_id,
						st.screening_id,
						st.screening_applicant_id,
						stypes.name as screen_type_name,
						s.screening_recommendation_type_id,
						sa.screening_recommendation_type_id as applicant_recommendation_type_id,
						st.screening_recommendation_type_id as transaction_recommendation_type_id,
						srt.name as group_recommendation,
						srt1.name as applicant_recommendation,
						srt2.name as transaction_recommendation,
						st.screening_package_condition_set_id,
						sp.name as screening_package_name,
						sp.id as screening_package_id
					FROM
						screenings s
						JOIN screening_applicants sa ON sa.screening_id = s.id and s.cid = sa.cid
						JOIN screening_transactions st ON st.screening_id = sa.screening_id and st.screening_applicant_id = sa.id and st.cid = sa.cid
						JOIN screening_recommendation_types srt ON srt.id = s.screening_recommendation_type_id
						JOIN screening_recommendation_types srt1 ON srt1.id = sa.screening_recommendation_type_id
						JOIN screening_recommendation_types srt2 ON srt2.id = st.screening_recommendation_type_id
						JOIN screen_types stypes on stypes.id = st.screen_type_id
						JOIN screening_packages sp on sp.id = sa.screening_package_id and sp.cid = sp.cid
					WHERE 
						s.id IN (' . sqlIntImplode( $arrintScreeningIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomRvTestCasesCountByBatchId( $intBatchNo, $objDatabase ) {
		if( !valId( $intBatchNo ) ) return NULL;

		$strSql = 'SELECT
						count(id) 
					FROM 
						rv_test_case_results	
					WHERE 
						rv_test_run_batch_id = ' . ( int ) $intBatchNo;

		$arrmixResults = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrmixResults ) ) ? $arrmixResults[0]['count'] : 0;
	}

	public static function fetchPaginatedRvTestCasesByScreeningSearchFilter( $objScreeningSearchFilter, $objDatabase ) {
		if( false == valObj( $objScreeningSearchFilter, 'CScreeningSearchFilter' ) ) {
			return NULL;
		}

		$intPageNo   = $objScreeningSearchFilter->getPagination()->getPageNo();
		$intPageSize = $objScreeningSearchFilter->getPagination()->getPageSize();

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'SELECT
						rtcs.*,
						rmtc.id as master_test_case_id,
						rmtc.name as master_test_case_name,
						rmtc.description as master_test_case_description,
						rmtc.screening_id as master_test_case_screening_id,
						rmtc.screening_result_details as master_screening_result_details ,
						CASE
		                    WHEN rtcs.status IS TRUE THEN 1
		                    ELSE 0
		                END AS  overall_test_result
					FROM
						rv_test_case_results rtcs
						JOIN rv_master_test_cases rmtc ON rmtc.id = rtcs.rv_master_test_case_id  
					WHERE
						rtcs.rv_test_run_batch_id = 	' . ( int ) $objScreeningSearchFilter->getBatchId() . '
					ORDER BY
                        rtcs.id	
					OFFSET ' . ( int ) $intOffset . ' 
					LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomRvTestCaseResultByMasterTestCaseIdByTestCaseId( $intMasterTestCaseId, $intTestCaseId,  $objDatabase ) {
		if( !valId( $intMasterTestCaseId ) ) return NULL;

		$strSql = 'SELECT
						rtcs.*,
						rmtc.id as master_test_case_id,
						rmtc.cid,
						rmtc.name as master_test_case_name,
						rmtc.screening_id as master_test_case_screening_id,
						rmtc.screening_result_details as master_screening_result_details,
						CASE
		                    WHEN rtcs.status IS TRUE THEN 1
		                    ELSE 0
		                END AS  overall_test_result
					FROM
						rv_test_case_results rtcs
						JOIN rv_master_test_cases rmtc ON rmtc.id = rtcs.rv_master_test_case_id  
					WHERE
						rtcs.rv_master_test_case_id = 	' . ( int ) $intMasterTestCaseId . ' 
						AND rtcs.id = 	' . ( int ) $intTestCaseId . ' 
					LIMIT 1';

		$arrmixResult = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrmixResult ) ) ? $arrmixResult[0]: NULL;
	}

	public static function fetchCustomChildTestCaseScreeningApplicantDetailsByScreeningId( $arrintScreeningIds,  $objDatabase ) {
		if( !valArr( $arrintScreeningIds ) ) return NULL;

		$strSql = 'SELECT
					    sa.screening_id,
					    sa.id as screening_applicant_id,
					    custom_applicant_id
					FROM
					    screening_applicants sa
					WHERE
						sa.screening_id IN ( ' . sqlIntImplode( $arrintScreeningIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPassedRvTestCasesCountByBatchId( $intBatchId, $objDatabase ) {
		$strSql = 'SELECT
						count( id ) as total_passed_count
					FROM
						rv_test_case_results
					WHERE
						status = true
						AND rv_test_run_batch_id = 	' . ( int ) $intBatchId;

		$arrmixResults = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrmixResults ) ) ? $arrmixResults[0]['total_passed_count'] : 0;
	}

}
?>