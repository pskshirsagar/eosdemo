<?php

class CScreeningGlobalSetting extends CBaseScreeningGlobalSetting {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningGlobalSettingTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>