<?php

class CScreeningRequestTypes extends CBaseScreeningRequestTypes {

    public static function fetchScreeningRequestTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CScreeningRequestType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchScreeningRequestType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScreeningRequestType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>