<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningApplicants
 * Do not add any new functions to this class.
 */

class CScreeningApplicants extends CBaseScreeningApplicants {

	public static function fetchActiveScreeningApplicantByScreeningIdByCustomApplicantIdByCid( $intScreeningId, $intApplicantId, $intCid, $objDatabase ) {

		$arrintCancelledStatusTypeIds 	= array( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );

		$strSql = 'SELECT
						  *
				   FROM
						screening_applicants
				   WHERE
						screening_id = ' . ( int ) $intScreeningId . '
						AND custom_applicant_id = ' . ( int ) $intApplicantId . '
						AND cid = ' . ( int ) $intCid . '
						AND status_type_id NOT IN( ' . implode( ',', $arrintCancelledStatusTypeIds ) . ' ) ';

		return parent::fetchScreeningApplicant( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningApplicantsByScreeningIdByCustomApplicantIdsByCid( $intScreeningId, $arrintApplicantIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicantIds ) || true == is_null( $intScreeningId ) ) return NULL;

		$arrintCancelledStatusTypeIds 	= array( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );

		$strSql = 'SELECT
						sa.*,
						s.rent as monthly_rent
				   FROM
						screening_applicants sa
						JOIN screenings s ON s.id = sa.screening_id AND s.cid = sa.cid
				   WHERE
						sa.screening_id = ' . ( int ) $intScreeningId . '
						AND sa.custom_applicant_id IN( ' . implode( ',', $arrintApplicantIds ) . ' )
						AND sa.cid = ' . ( int ) $intCid . '
						AND sa.status_type_id NOT IN( ' . implode( ',', $arrintCancelledStatusTypeIds ) . ' ) ';

		return parent::fetchScreeningApplicants( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningApplicantsByScreeningIdByCid( $intScreeningId,  $intCid, $objDatabase ) {

		if( true == is_null( $intScreeningId ) ) return NULL;

		$arrintCancelledStatusTypeIds 	= array( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );

		$strSql = 'SELECT
						  sa.*,
						  s.rent as monthly_rent,
						  sp.screening_cumulation_type_id,
						  sp.is_use_for_prescreening,
						  sp.screening_package_type_id,
						  sp.is_all_must_pass_credit_and_income,
						  sp.is_use_for_rv_index_score,
						  saa.address_line as previous_address_line,
						  saa.city as previous_city,
						  saa.state as previous_state,
						  saa.zipcode as previous_zipcode
				   FROM
						screening_applicants sa
						JOIN screenings s ON s.id = sa.screening_id AND s.cid = sa.cid
						LEFT JOIN screening_packages sp ON sp.id = sa.screening_package_id
						LEFT JOIN screening_applicant_addresses saa ON saa.screening_applicant_id = sa.id AND saa.cid = sa.cid
				   WHERE
						sa.screening_id = ' . ( int ) $intScreeningId . '
						AND sa.cid = ' . ( int ) $intCid . '
						AND sa.status_type_id NOT IN( ' . implode( ',', $arrintCancelledStatusTypeIds ) . ' )
				   ORDER BY sa.screening_applicant_type_id, sa.custom_applicant_id';

		return parent::fetchScreeningApplicants( $strSql, $objDatabase );
	}

	public static function fetchPaginatedScreeningApplicantsDetailsByScreeningSearchFilter( $objScreeningSearchFilter, $arrintTestClients, $boolIsAllDataProviderSelected, $objDatabase ) {

		if( false == valObj( $objScreeningSearchFilter, 'CScreeningSearchFilter' ) ) return NULL;

		$intPageNo    = $objScreeningSearchFilter->getPagination()->getPageNo();
		$intPageSize  = $objScreeningSearchFilter->getPagination()->getPageSize();

		$intOffset    = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit     = ( int ) $intPageSize;

		$strWhere     = self::buildWhereClauseByScreeningSearchFilter( $objScreeningSearchFilter, $arrintTestClients, $boolIsAllDataProviderSelected );
		$strJoin      = self::buildJoinClauseByScreeningSearchFilter( $objScreeningSearchFilter, $boolIsAllDataProviderSelected );

		$strSql = 'SELECT
					    *
					FROM
					    (
					      SELECT
								DISTINCT ON ( sa.id ) sa.id screening_applicant_id,
								sa.custom_applicant_id AS applicant_id,
					            sa.name_first || \' \' || sa.name_last AS applicant_name,
					            sa.screening_completed_on AS screening_applicant_completed_date,
					            s.created_on AS screen_date,
					            s.id AS screening_id,
					            s.custom_application_id AS application_id,
					            s.screening_completed_on AS screening_completed_date,
					            s.cid,
					            s.property_id,
					            s.screening_recommendation_type_id AS group_recommendation_id,
					            s.decision_on AS decision_date,
					            s.screening_configuration_id,
					            sat.name AS screening_applicant_type,
					            srt.name AS group_recommendation,
					            srt1.name AS applicant_recommendation,
					            st.screening_data_provider_id,
								sa.status_type_id
							FROM
							    screening_applicants sa
							    JOIN screenings s ON ( s.id = sa.screening_id AND s.cid = sa.cid )
            					JOIN screening_applicant_types sat ON sat.id = sa.screening_applicant_type_id
            					JOIN screening_recommendation_types srt ON srt.id = s.screening_recommendation_type_id
								JOIN screening_recommendation_types srt1 ON srt1.id = sa.screening_recommendation_type_id
								' . $strJoin . '
								' . $strWhere . '
						) sa_subquery
					ORDER BY
						sa_subquery.screen_date ASC
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicantsDetailsCountByScreeningSearchFilter( $objScreeningSearchFilter, $arrintTestClients, $boolIsAllDataProviderSelected, $objDatabase ) {

		if( false == valObj( $objScreeningSearchFilter, 'CScreeningSearchFilter' ) ) return NULL;

		$strWhere 	= self::buildWhereClauseByScreeningSearchFilter( $objScreeningSearchFilter, $arrintTestClients, $boolIsAllDataProviderSelected );
		$strJoin 	= self::buildJoinClauseByScreeningSearchFilter( $objScreeningSearchFilter, $boolIsAllDataProviderSelected );

		$strSql = 'SELECT
					    COUNT( DISTINCT sa.id )
					FROM
					    screenings s
					    JOIN screening_applicants sa ON s.id = sa.screening_id
					    JOIN screening_recommendation_types srt ON srt.id = s.screening_recommendation_type_id
						' . $strJoin . '
						' . $strWhere;

		$arrintScreeningApplicantsCount = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintScreeningApplicantsCount ) ) ? $arrintScreeningApplicantsCount[0]['count'] : 0;
	}

	public static function buildWhereClauseByScreeningSearchFilter( $objScreeningSearchFilter, $arrintTestClients, $boolIsAllDataProviderSelected ) {

		$arrstrWheres 	                = array();
		$strWhere 		                = '';
		$boolIsIncludeOtherFilters 	= true;

		if( false == is_null( $objScreeningSearchFilter->getCid() ) ) {
			$arrstrWheres[] = 's.cid = ' . ( int ) $objScreeningSearchFilter->getCid();
		}

		if( false == is_null( $objScreeningSearchFilter->getPropertyIds() ) && true == valArr( array_filter( $objScreeningSearchFilter->getPropertyIds() ) ) ) {
			$arrstrWheres[] = 's.property_id IN( ' . implode( ',', $objScreeningSearchFilter->getPropertyIds() ) . ') ';
		}

		if( false == is_null( $objScreeningSearchFilter->getQuickSearch() ) && true == valStr( trim( $objScreeningSearchFilter->getQuickSearch() ) ) ) {
			$strQuickSearchFilterType = \Psi\CStringService::singleton()->strtolower( \Psi\CStringService::singleton()->substr( trim( $objScreeningSearchFilter->getQuickSearch() ), 0, 2 ) );

			$strQuickSearchFilterValue = trim( \Psi\CStringService::singleton()->substr( trim( $objScreeningSearchFilter->getQuickSearch() ), 2 ) );

			if( '' != $strQuickSearchFilterValue && ( true == in_array( $strQuickSearchFilterType, array( 'n:', 'a:', 's:', 'o:' ) ) ) ) {
				switch( $strQuickSearchFilterType ) {
					case 'n:':
						$arrstrApplicantNameWheres = array();
						$arrstrApplicantNameItems = array_filter( explode( ' ', trim( $strQuickSearchFilterValue ) ) );
						$arrstrWheres[] = self::buildWhereClauseByApplicantNameItems( $arrstrApplicantNameItems );
						break;

					case 'a:':
						$arrstrWheres[] = 's.custom_application_id = ' . ( int ) $strQuickSearchFilterValue;
						break;

					case 's:':
						$arrstrWheres[] = 's.id = ' . ( int ) $strQuickSearchFilterValue;
						break;

					case 'o:':
						$arrstrWheres[] = 'lower( st.screening_order_id ) LIKE lower( \'%' . addslashes( $strQuickSearchFilterValue ) . '%\' )';
						break;

					default:
						// add comment
						break;
				}
			} else {

				$strQuickSearchFilterValue = trim( $objScreeningSearchFilter->getQuickSearch() );
				$arrstrApplicantNameWheres = array();
				$strNameWhereClause = $strApplicationIdWhereClause = $strScreeningIdWhereClause = $strOrderIdWhereClause = '';

				$arrstrApplicantNameItems = array_filter( explode( ' ', trim( $strQuickSearchFilterValue ) ) );

				$strNameWhereClause = self::buildWhereClauseByApplicantNameItems( $arrstrApplicantNameItems );
				$strOrderIdWhereClause = 'lower( st.screening_order_id ) LIKE lower( \'%' . addslashes( $strQuickSearchFilterValue ) . '%\' )';

				if( 0 != ( int ) $strQuickSearchFilterValue ) {
					$strApplicationIdWhereClause = 's.custom_application_id = ' . ( int ) $strQuickSearchFilterValue;
					$strScreeningIdWhereClause = 's.id = ' . ( int ) $strQuickSearchFilterValue;
					$arrstrWheres[] = '( ( ' . $strNameWhereClause . ' ) OR ( ' . $strApplicationIdWhereClause . ' ) OR ( ' . $strScreeningIdWhereClause . ' ) OR ( ' . $strOrderIdWhereClause . ' ) )';
				} else {
					$arrstrWheres[] = '( ( ' . $strNameWhereClause . ' ) OR ( ' . $strOrderIdWhereClause . ' ) )';
				}

			}

			$boolIsIncludeOtherFilters = false;
		}

		if( false == is_null( $objScreeningSearchFilter->getScreeningId() ) && true == valStr( trim( $objScreeningSearchFilter->getScreeningId() ) ) && true == $boolIsIncludeOtherFilters ) {
			$arrstrWheres[] = 's.id = ' . ( int ) $objScreeningSearchFilter->getScreeningId();
			$boolIsIncludeOtherFilters = false;
		}

		if( true == valStr( $objScreeningSearchFilter->getScreeningOrderId() ) && true == $boolIsIncludeOtherFilters ) {
			$arrstrWheres[] = 'lower( st.screening_order_id ) LIKE lower( \'%' . addslashes( $objScreeningSearchFilter->getScreeningOrderId() ) . '%\' )';
			$boolIsIncludeOtherFilters = false;
		}
		if( false == is_null( $objScreeningSearchFilter->getScreeningStatusTypeIds() ) && true == valArr( array_filter( $objScreeningSearchFilter->getScreeningStatusTypeIds() ) ) && true == $boolIsIncludeOtherFilters ) {
			$arrstrWheres[] = 's.screening_status_type_id IN ( ' . implode( ',', $objScreeningSearchFilter->getScreeningStatusTypeIds() ) . ' ) ';
		}

		if( false == is_null( $objScreeningSearchFilter->getScreeningApplicantStatusTypeIds() ) && true == valArr( array_filter( $objScreeningSearchFilter->getScreeningApplicantStatusTypeIds() ) ) && true == $boolIsIncludeOtherFilters ) {
			$arrstrWheres[] = 'sa.status_type_id IN ( ' . implode( ',', $objScreeningSearchFilter->getScreeningApplicantStatusTypeIds() ) . ' ) ';
		}

		if( false == is_null( $objScreeningSearchFilter->getScreeningRecommendationTypeIds() ) && true == valArr( array_filter( $objScreeningSearchFilter->getScreeningRecommendationTypeIds() ) ) && true == $boolIsIncludeOtherFilters ) {
			$arrstrWheres[] = 's.screening_recommendation_type_id IN ( ' . implode( ',', $objScreeningSearchFilter->getScreeningRecommendationTypeIds() ) . ' ) ';
		}

		if( false == is_null( $objScreeningSearchFilter->getScreeningApplicantRecommendationTypeIds() ) && true == valArr( array_filter( $objScreeningSearchFilter->getScreeningApplicantRecommendationTypeIds() ) ) && true == $boolIsIncludeOtherFilters ) {
			$arrstrWheres[] = 'sa.screening_recommendation_type_id IN ( ' . implode( ',', $objScreeningSearchFilter->getScreeningApplicantRecommendationTypeIds() ) . ' ) ';
		}

		if( true == $boolIsIncludeOtherFilters ) {

			if( CScreeningSearchFilter::CLIENTS_LIVE == $objScreeningSearchFilter->getClientEnvironmentId() ) {
				$arrstrWheres[] = 's.cid NOT IN (' . implode( ',', $arrintTestClients ) . ' ) ';
			} elseif( CScreeningSearchFilter::CLIENTS_TEST == $objScreeningSearchFilter->getClientEnvironmentId() ) {
				$arrstrWheres[] = 's.cid IN (' . implode( ',', $arrintTestClients ) . ' ) ';
			}

			if( false == is_null( $objScreeningSearchFilter->getScreeningDataProviderTypeIds() ) && true == valArr( array_filter( $objScreeningSearchFilter->getScreeningDataProviderTypeIds() ) ) && false == $boolIsAllDataProviderSelected ) {
				$arrstrWheres[] = 'sdp.screening_data_provider_type_id IN ( ' . implode( ',', $objScreeningSearchFilter->getScreeningDataProviderTypeIds() ) . ' ) ';
			}
		}

		if( false == is_null( $objScreeningSearchFilter->getFromDate() ) && true == valStr( trim( $objScreeningSearchFilter->getFromDate() ) ) && true == $boolIsIncludeOtherFilters ) {
			$arrstrWheres[] = 'to_char(s.created_on, \'YYYY-MM-DD HH:MI:SS\') > \'' . date( 'Y-m-d H:i:s', strtotime( $objScreeningSearchFilter->getFromDate() ) ) . '\'';
		}

		if( false == is_null( $objScreeningSearchFilter->getToDate() ) && true == valStr( trim( $objScreeningSearchFilter->getToDate() ) ) && true == $boolIsIncludeOtherFilters ) {
			$arrstrWheres[] = 'to_char(s.created_on, \'YYYY-MM-DD HH:MI:SS\') < \'' . date( 'Y-m-d 59:59:59', strtotime( $objScreeningSearchFilter->getToDate() ) ) . '\'';
		}

		if( true == valArr( $arrstrWheres ) ) {
			$strWhere = 'WHERE ' . implode( ' AND ', $arrstrWheres );
		}

		return $strWhere;
	}

	public static function buildJoinClauseByScreeningSearchFilter( $objScreeningSearchFilter, $boolIsAllDataProviderSelected ) {
		$arrstrJoins 	= array();
		$strJoin 		= '';

		if( false == is_null( $objScreeningSearchFilter->getScreeningOrderId() ) || ( false == is_null( $objScreeningSearchFilter->getScreeningDataProviderTypeIds() ) && true == valArr( array_filter( $objScreeningSearchFilter->getScreeningDataProviderTypeIds() ) ) && false == $boolIsAllDataProviderSelected ) ) {
			$arrstrJoins[] = 'screening_transactions st ON st.screening_applicant_id = sa.id';
		}

		if( false == is_null( $objScreeningSearchFilter->getScreeningDataProviderTypeIds() ) && true == valArr( array_filter( $objScreeningSearchFilter->getScreeningDataProviderTypeIds() ) ) && false == $boolIsAllDataProviderSelected ) {
			$arrstrJoins[] = 'screening_data_providers sdp ON sdp.id = st.screening_data_provider_id';
		}

		if( true == valArr( $arrstrJoins ) ) {
			$strJoin = ' JOIN ' . implode( ' JOIN ', $arrstrJoins );
		}

		return $strJoin;

	}

	public static function buildWhereClauseByApplicantNameItems( $arrstrApplicantNameItems ) {

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrApplicantNameItems ) ) {
			$strNameWhereClause = '( lower( sa.name_first ) LIKE lower( \'%' . addslashes( $arrstrApplicantNameItems[0] ) . '%\' ) OR lower( sa.name_last ) LIKE lower( \'%' . addslashes( $arrstrApplicantNameItems[0] ) . '%\' ) OR lower( sa.name_middle ) LIKE lower( \'%' . addslashes( $arrstrApplicantNameItems[0] ) . '%\' ) )';
		} elseif( 2 == \Psi\Libraries\UtilFunctions\count( $arrstrApplicantNameItems ) ) {
			$strNameWhereClause = '( lower( sa.name_first ) = lower( \'' . addslashes( $arrstrApplicantNameItems[0] ) . '\' ) AND lower( sa.name_last ) = lower( \'' . addslashes( $arrstrApplicantNameItems[1] ) . '\' )' .
			 ' OR lower( sa.name_first ) = lower( \'' . addslashes( $arrstrApplicantNameItems[1] ) . '\' ) AND lower( sa.name_last ) = lower( \'' . addslashes( $arrstrApplicantNameItems[0] ) . '\' ) )';
		} else {
			foreach( $arrstrApplicantNameItems as $strApplicantNameItem ) {
				$arrstrApplicantNameWheres[] = '( lower( sa.name_first ) LIKE lower( \'%' . addslashes( $strApplicantNameItem ) . '%\' ) OR lower( sa.name_last ) LIKE lower( \'%' . addslashes( $strApplicantNameItem ) . '%\' ) OR lower( sa.name_middle ) LIKE lower( \'%' . addslashes( $strApplicantNameItem ) . '%\' ) )';
			}
			$strNameWhereClause = implode( ' OR ', $arrstrApplicantNameWheres );
		}
		return $strNameWhereClause;
	}

	public static function fetchActiveScreeningApplicantsByCustomApplicantIdsByStatusTypeIdsByCid( $arrintApplicantIds, $arrintStatusTypeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicantIds ) || false == valArr( $arrintStatusTypeIds ) ) return NULL;

		$strSql = 'SELECT
						  *
				   FROM
						screening_applicants
				   WHERE
						custom_applicant_id IN( ' . implode( ',', $arrintApplicantIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND status_type_id IN( ' . implode( ',', $arrintStatusTypeIds ) . ' ) ';

		return parent::fetchScreeningApplicants( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicantWithScreeningApplicantTypeByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sa.*,
						sat.name screening_applicant_type
					FROM
						screening_applicants sa
						JOIN screening_applicant_types sat ON sa.screening_applicant_type_id = sat.id
					WHERE
						sa.id = ' . ( int ) $intId . '
						AND sa.cid = ' . ( int ) $intCid;

		return parent::fetchScreeningApplicant( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicantsRecommendationByCid( $intCid, $objDatabase ) {

		$strSql = ' SELECT
						custom_applicant_id,
						screening_recommendation_type_id
					FROM
					(
						SELECT
							sa.custom_applicant_id,
							sa.screening_recommendation_type_id,
							row_number ( ) OVER ( PARTITION BY sa.cid, sa.custom_applicant_id ORDER BY sa.id DESC ) AS row_number
						FROM
							screenings s
							JOIN screening_applicants sa ON ( sa.cid = s.cid AND sa.screening_id = s.id )
						WHERE
							s.cid  = ' . ( int ) $intCid . '
					) subquery_screening_applications
					WHERE
						row_number = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLatestScreeningApplicantByScreeningIdByCustomApplicantIdsByCid( $intScreeningId, $arrintCustomApplicantIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomApplicantIds ) ) return;

		$strSql = 'SELECT
						  *
				   FROM(

						SELECT
								*,
								MAX(id) OVER ( PARTITION BY custom_applicant_id ) as max_screening_applicant_id
						FROM
							screening_applicants
						WHERE
							screening_id = ' . ( int ) $intScreeningId . '
							AND cid = ' . ( int ) $intCid . '
							AND custom_applicant_id IN( ' . implode( ',', $arrintCustomApplicantIds ) . ' )
					   ORDER BY id DESC
					) as sub
				WHERE
					id = max_screening_applicant_id';

		return parent::fetchScreeningApplicants( $strSql, $objDatabase );
	}

	public static function fetchExistingScreeningApplicantByOtherDetailsByCid( $objScreeningApplicant, $objScreeningTransaction, $intDeduplicateDays, $intCid, $objDatabase ) {

		$arrintStatusTypeIds 	= array( CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED );

		$strOrder = 'ORDER BY sa.id DESC limit 1 ';

		if( false == is_null( $objScreeningApplicant->getTaxNumberLastFour() ) ) {
			$arrstrWheres[] = ' sa.tax_number_encrypted = ' . "'" . $objScreeningApplicant->getTaxNumberEncrypted() . "'";
		} else {
			$arrstrWheres[] = ( false == is_null( $objScreeningApplicant->getNameFirst() ) && true == valStr( trim( $objScreeningApplicant->getNameFirst() ) ) ) ? '( lower( sa.name_first ) LIKE lower( \'%' . addslashes( $objScreeningApplicant->getNameFirst() ) . '%\' ) ) ' : '';
		}

		if( 0 < $intDeduplicateDays ) {
			$arrstrWheres[] = '( date_trunc( \'day\', sa.created_on ) ) > ( date_trunc( \'day\', NOW() ) - INTERVAL \' ' . ( int ) $intDeduplicateDays . ' days\' )';
		}

		if( true == valArr( $arrstrWheres ) ) {
			$strWhere = implode( ' AND ', $arrstrWheres );
		}

		$strSql = 'SELECT
						  sa.*
				   FROM
						screening_applicants sa INNER JOIN screening_transactions st
				     ON st.screening_applicant_id = sa.id and st.cid = sa.cid
				   WHERE
						sa.cid = ' . ( int ) $intCid . '
						AND sa.screening_applicant_type_id  = ' . ( int ) $objScreeningApplicant->getScreeningApplicantTypeId() . '
						AND sa.status_type_id  in  ( ' . implode( ',', $arrintStatusTypeIds ) . ' )
						AND st.screen_type_id = ' . $objScreeningTransaction->getScreenTypeId() . '
						AND st.screening_status_type_id = ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . '
						AND st.transaction_amount <> 0
						AND ' . $strWhere . $strOrder;
		return parent::fetchScreeningApplicant( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicantByIdByScreeningIdByCid( $intScreeningApplicantId, $intScreeningId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sa.*,
						s.rent as monthly_rent,
					  	sp.screening_cumulation_type_id,
					  	sp.is_use_for_prescreening,
					  	sp.is_all_must_pass_credit_and_income
					FROM
						screening_applicants sa
						JOIN screenings s ON s.id = sa.screening_id AND s.cid = sa.cid
						LEFT JOIN screening_packages sp ON sp.id = sa.screening_package_id
					WHERE
						sa.id = ' . ( int ) $intScreeningApplicantId . '
						AND sa.cid = ' . ( int ) $intCid . '
						AND sa.screening_id = ' . ( int ) $intScreeningId . 'ORDER BY ID DESC LIMIT 1';

		return parent::fetchScreeningApplicant( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningApplicantByScreeningIdByCustomApplicantIdsByCid( $intScreeningId, $arrintCustomApplicantIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomApplicantIds ) ) return;

		$arrintCancelledStatusTypeIds 	= array( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );

		$strSql = 'SELECT
						  *
				   FROM
						screening_applicants
				   WHERE
						screening_id = ' . ( int ) $intScreeningId . '
						AND custom_applicant_id IN( ' . implode( ',', $arrintCustomApplicantIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND status_type_id NOT IN( ' . implode( ',', $arrintCancelledStatusTypeIds ) . ' ) ';

		return parent::fetchScreeningApplicants( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningApplicantsAndManualReviewDetailsByScreeningTransactionIdByCid( $intScreeningTransactionId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( sa.id ) sa.id screening_applicant_id,
						sa.custom_applicant_id AS applicant_id,
					    coalesce( sa.name_first, \'\') || \' \' || coalesce(sa.name_middle,\'\') || \' \' || coalesce( sa.name_last, \'\') AS applicant_name,
					    sa.address_line || \' \' || sa.city || \' \' || sa.state || \' \' || sa.zipcode AS applicant_current_address,
					    sa.address_line AS applicant_current_address_line,
					    sa.city AS applicant_current_address_city,
					    sa.state AS applicant_current_address_state,
					    sa.zipcode AS applicant_current_address_zipcode,
						sa.birth_date_encrypted AS applicant_birth_date,
						sa.screening_completed_on AS complete_date,
					    sa.created_on AS screen_date,
					    s.id AS screening_id,
					    s.custom_application_id AS application_id,
					    s.cid,
					    s.property_id,
					    s.screening_recommendation_type_id AS group_recommendation_id,
					    s.decision_on AS decision_date,
					    s.screening_configuration_id,
					    s.transmission_id,
						s.application_type_id,
					    sat.name AS screening_applicant_type,
					    srt.name AS group_recommendation,
					    srt1.name AS applicant_recommendation,
					    st.screening_data_provider_id,
					    st.screen_type_id,
					    st.search_type,
					    st.screening_recommendation_type_id AS transaction_recommendation_type_id,
					    st.original_screening_recommendation_type_id,
						tr.id AS transaction_review_id,
						tr.screening_transaction_id,
						tr.transaction_review_status_id AS transaction_review_status,
						sp.name AS screening_package_name,
						sp.id AS screening_package_id,
						sp.is_used_for_filtering_pending_charges,
						st.screening_status_type_id,
						s.screening_decision_type_id,
						sac.client_name,
						spa.property_name,
						sa.details,
						CASE
							WHEN s.created_on < NOW() - \'' . CScreening::SCREENING_RETENTION_PERIOD . ' years\'::interval THEN
								1
							ELSE
							    0
						END AS is_out_of_screening_retention_period
					FROM
					   screening_applicants sa
					   JOIN screenings s ON s.id = sa.screening_id
            		   JOIN screening_applicant_types sat ON sat.id = sa.screening_applicant_type_id
					   JOIN screening_transactions st ON st.screening_applicant_id = sa.id
				 	   JOIN transaction_reviews tr ON tr.screening_transaction_id =  st.id
            		   LEFT JOIN screening_recommendation_types srt ON srt.id = s.screening_recommendation_type_id
					   LEFT JOIN screening_recommendation_types srt1 ON srt1.id = sa.screening_recommendation_type_id
					   LEFT JOIN screening_packages sp ON sp.id = sa.screening_package_id
					   LEFT JOIN screening_accounts sac ON sac.cid = sa.cid
					   LEFT JOIN screening_property_accounts spa ON ( spa.cid = sa.cid AND spa.property_id = s.property_id AND s.application_type_id = ' . CScreeningApplicationType::TENANT . ' AND spa.is_published = 1 )
					WHERE
						st.id = ' . ( int ) $intScreeningTransactionId . '
						AND st.cid = ' . ( int ) $intCid . '
						AND sac.is_active = true';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchQuickSearchUnderReviewRecordByTransactionReviewQueueTypeId( $arrstrFilteredExplodedSearch, $intTransactionReviewQueueTypeId, $objDatabase ) {
		if( false == valArr( $arrstrFilteredExplodedSearch ) && false == valId( $intTransactionReviewQueueTypeId ) ) return NULL;

		if( false == is_numeric( $arrstrFilteredExplodedSearch[0] ) ) {
			if( true == valStr( $arrstrFilteredExplodedSearch[1] && true == valStr( $arrstrFilteredExplodedSearch[2] ) && true == valStr( $arrstrFilteredExplodedSearch[3] ) ) ) {
				$strQuickSearchWhereSqls = ' AND ( lower( sa.name_last ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[3] ) . '%\' ) AND lower( sa.name_middle ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[2] ) . '%\' )  
				 AND lower( sa.name_first ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[1] ) . '%\' ) )';
			} elseif( true == valStr( $arrstrFilteredExplodedSearch[1] ) && true == valStr( $arrstrFilteredExplodedSearch[2] ) ) {
				$strQuickSearchWhereSqls = ' AND ( ( lower( sa.name_last ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[1] ) . '%\' ) AND lower( sa.name_first ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[2] ) . '%\' ) ) OR 
				( lower( sa.name_last ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[2] ) . '%\' ) AND lower( sa.name_first ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[1] ) . '%\' ) ) OR 
				( lower( sa.name_middle ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[2] ) . '%\' ) AND lower( sa.name_first ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[1] ) . '%\' ) ) OR 
				( lower( sa.name_last ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[2] ) . '%\' ) AND lower( sa.name_middle ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[1] ) . '%\' ) ) )';
			} else {
				foreach( $arrstrFilteredExplodedSearch as $strQuickSearchItem ) {
					if( true == valStr( $strQuickSearchItem ) ) {
						$strQuickSearchWhereSqls = ' AND ( lower( sa.name_last ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) OR lower( sa.name_middle ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) OR lower( sa.name_first ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) )';
					}
				}
			}
		}

		if( 1 == \Psi\Libraries\UtilFunctions\count( trim( $arrstrFilteredExplodedSearch[0] ) ) && is_numeric( trim( $arrstrFilteredExplodedSearch[0] ) ) )
			$strQuickSearchWhereSqls = ' AND ( to_char( s.custom_application_id, \'99999999\' ) LIKE \'%' . trim( $arrstrFilteredExplodedSearch[0] ) . '%\' OR to_char( s.cid, \'99999999\' ) LIKE \'%' . trim( $arrstrFilteredExplodedSearch[0] ) . '%\' )';

		$strSql = 'SELECT
					    *
					FROM
						(
							SELECT
								sa.id as screening_applicant_id,
								sa.custom_applicant_id AS applicant_id,
								coalesce( sa.name_first, \'\') || \' \' || coalesce(sa.name_middle,\'\') || \' \' || coalesce( sa.name_last, \'\') AS applicant_name,
								sa.address_line || \' \' || sa.city || \' \' || sa.state || \' \' || sa.zipcode AS applicant_current_address,
								sa.birth_date_encrypted AS applicant_birth_date,
								sa.updated_on AS complete_date,
								sa.created_on AS screen_date,
								s.id AS screening_id,
								s.custom_application_id AS application_id,
								s.cid,
								s.property_id,
								s.screening_recommendation_type_id AS group_recommendation_id,
								s.decision_on AS decision_date,
								s.screening_configuration_id,
								sat.name AS screening_applicant_type,
								srt.name AS group_recommendation,
								st.screening_data_provider_id,
								tr.id AS transaction_review_id,
								tr.transaction_review_status_id AS transaction_review_status,
								trs.name as transaction_review_status_name,
								st.id as screening_transaction_id,
								st.search_type,
								st.screen_type_id,
								sac.client_name,
								spa.property_name
							FROM
								screening_applicants sa
								JOIN screenings s ON s.id = sa.screening_id
								JOIN screening_applicant_types sat ON sat.id = sa.screening_applicant_type_id
								JOIN screening_transactions st ON st.screening_applicant_id = sa.id AND sa.status_type_id NOT IN ( ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED . ' ) AND st.screening_status_type_id NOT IN ( ' . implode( ',', CScreeningStatusType::$c_arrintScreeningCancelledStatusIds ) . ' )
				 				JOIN transaction_reviews tr ON tr.screening_transaction_id =  st.id
								JOIN transaction_review_statuses trs ON trs.id = tr.transaction_review_status_id
								LEFT JOIN screening_recommendation_types srt ON srt.id = s.screening_recommendation_type_id
								LEFT JOIN screening_accounts sac ON( sac.cid = sa.cid AND sac.is_active = TRUE AND sac.id = st.company_screening_account_id )
								LEFT JOIN screening_property_accounts spa ON ( spa.cid = sa.cid AND spa.property_id = s.property_id AND s.application_type_id = ' . CScreeningApplicationType::TENANT . ' AND spa.is_published = 1 )
							WHERE
								tr.transaction_review_queue_type_id = ' . ( int ) $intTransactionReviewQueueTypeId . ' 
								' . $strQuickSearchWhereSqls . ' ) sa_subquery 
							ORDER BY 
								sa_subquery.transaction_review_status ASC 
								LIMIT 10';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomReviewScreeningApplicantsCountByTransactionReviewStatusIdsByTransactionReviewQueueTypeId( $arrintTransactionReviewStatusIds, $intTransactionReviewQueueTypeId = CTransactionReviewQueueType::CRIMINAL_REVIEW, $objDatabase ) {

		$strSql = 'SELECT
					     COUNT( sa.id ), tr.transaction_review_status_id
					FROM
					    screening_applicants sa
					    JOIN screenings s ON s.id = sa.screening_id
            			JOIN screening_applicant_types sat ON sat.id = sa.screening_applicant_type_id
					    JOIN screening_transactions st ON st.screening_applicant_id = sa.id
				 		JOIN transaction_reviews tr ON tr.screening_transaction_id =  st.id
            			LEFT JOIN screening_recommendation_types srt ON srt.id = s.screening_recommendation_type_id
					WHERE
						tr.transaction_review_status_id IN (  ' . implode( ',',  $arrintTransactionReviewStatusIds ) . ')
						AND st.screening_status_type_id NOT IN ( ' . implode( ',', CScreeningStatusType::$c_arrintScreeningCancelledStatusIds ) . ' )
						AND sa.status_type_id NOT IN ( ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED . ' )
						AND tr.transaction_review_queue_type_id = ' . ( int ) $intTransactionReviewQueueTypeId . '  
					GROUP BY tr.transaction_review_status_id';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomScreeningApplicantsByTransactionReviewStatusIdByTransactionReviewIdByTransactionReviewQueueTypeId( $intPageNo, $intPageSize, $intTransactionReviewStatusId = CTransactionReviewStatus::REVIEW_STATUS_NEW, $intTransactionReviewId = NULL, $intTransactionReviewQueueTypeId = CTransactionReviewQueueType::CRIMINAL_REVIEW, $objDatabase ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		$strSql = 'SELECT * FROM (
							SELECT
								sa.id screening_applicant_id,
								sa.custom_applicant_id AS applicant_id,
								coalesce( sa.name_first, \'\') || \' \' || coalesce(sa.name_middle,\'\') || \' \' || coalesce( sa.name_last, \'\') AS applicant_name,
								sa.address_line || \' \' || sa.city || \' \' || sa.state || \' \' || sa.zipcode AS applicant_current_address,
								sa.birth_date_encrypted AS applicant_birth_date,
								sa.updated_on AS complete_date,
								sa.created_on AS screen_date,
								s.id AS screening_id,
								s.custom_application_id AS application_id,
								s.cid,
								s.property_id,
								s.screening_recommendation_type_id AS group_recommendation_id,
								s.decision_on AS decision_date,
								s.screening_configuration_id,
								sat.name AS screening_applicant_type,
								srt.name AS group_recommendation,
								srt1.name AS applicant_recommendation,
								st.screening_response_completed_on,
								st.screening_data_provider_id,
								tr.id AS transaction_review_id,
								tr.screening_transaction_id,
								tr.transaction_review_status_id AS transaction_review_status,
								st.search_type,
								st.screen_type_id,
								tr.created_on AS transaction_review_date,
								tr.updated_by AS transaction_review_agent_id,
								sac.client_name,
								spa.property_name
							FROM
								screening_applicants sa
								JOIN screenings s ON s.id = sa.screening_id
								JOIN screening_applicant_types sat ON sat.id = sa.screening_applicant_type_id
            					JOIN screening_transactions st ON st.screening_applicant_id = sa.id
				 				JOIN transaction_reviews tr ON tr.screening_transaction_id =  st.id
								LEFT JOIN screening_recommendation_types srt ON srt.id = s.screening_recommendation_type_id
								LEFT JOIN screening_recommendation_types srt1 ON srt1.id = sa.screening_recommendation_type_id
								LEFT JOIN screening_accounts sac ON( sac.cid = sa.cid AND sac.is_active = TRUE AND sac.id = st.company_screening_account_id )
								LEFT JOIN screening_property_accounts spa ON ( spa.cid = sa.cid AND spa.property_id = s.property_id AND s.application_type_id = ' . CScreeningApplicationType::TENANT . ' AND spa.is_published = 1 )
							WHERE
								tr.transaction_review_status_id = ' . ( int ) $intTransactionReviewStatusId . '
								AND st.screening_status_type_id NOT IN ( ' . implode( ',', CScreeningStatusType::$c_arrintScreeningCancelledStatusIds ) . ' )
								AND sa.status_type_id NOT IN ( ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED . ' )								 
								AND tr.transaction_review_queue_type_id = ' . ( int ) $intTransactionReviewQueueTypeId;

		if( true == isset( $intTransactionReviewId ) ) {
			$strSql .= ' AND tr.id = ' . ( int ) $intTransactionReviewId;
		}

		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		}

		$strSql .= ' ) as subquery ';

		$strSql .= 'ORDER BY
							subquery.screen_date DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveVendorScreeningApplicantByCustomApplicantIdByScreeningId( $intCustomApplicantId, $intScreeningId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_applicants
					WHERE
						screening_id = ' . ( int ) $intScreeningId . '
						AND custom_applicant_id = ' . ( int ) $intCustomApplicantId . '
						AND screening_applicant_type_id IN( ' . implode( ',', CScreeningApplicantType::$c_arrintVendorApplicantTypeIds ) . ' )
						AND status_type_id = ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . '
					ORDER BY id
						LIMIT 1';

		return parent::fetchScreeningApplicant( $strSql, $objDatabase );
	}

	public static function fetchQuickSearchScreeningApplicants( $arrstrFilteredExplodedSearch, $objDatabase ) {
		if( false == valArr( $arrstrFilteredExplodedSearch ) ) return NULL;

		$strWhere = '';
		if( false == is_numeric( $arrstrFilteredExplodedSearch[0] ) ) {
			if( !filter_var( $arrstrFilteredExplodedSearch[0], FILTER_VALIDATE_EMAIL ) === false ) {
				$strWhere = ' ( lower( sa.email_address ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[0] ) . '%\' ) ) ';
			} elseif( true == valStr( $arrstrFilteredExplodedSearch[1] && true == valStr( $arrstrFilteredExplodedSearch[2] ) ) ) {
				$strWhere = ' ( ( lower( sa.name_last ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[1] ) . '%\' ) AND lower( sa.name_first ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[2] ) . '%\' ) ) OR 
				( lower( sa.name_last ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[2] ) . '%\' ) AND lower( sa.name_first ) LIKE lower( \'%' . addslashes( $arrstrFilteredExplodedSearch[1] ) . '%\' ) ) )';
			} else {
				foreach( $arrstrFilteredExplodedSearch as $strQuickSearchItem ) {
					if( true == valStr( $strQuickSearchItem ) ) {
						$strWhere = ' ( lower( sa.name_last ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) OR lower( sa.name_first ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) )';
					}
				}
			}
		}

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrFilteredExplodedSearch ) && is_numeric( $arrstrFilteredExplodedSearch[0] ) )
			$strWhere = ' sa.tax_number_last_four = ' . ( int ) $arrstrFilteredExplodedSearch[0] . ' :: text ';

		if( true == valStr( $strWhere ) ) {
			$strWhere .= ' and sa.status_type_id != ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED;
		} else {
			$strWhere .= ' sa.status_type_id != ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED;
		}

		$strSql = 'SELECT * FROM screening_applicants sa WHERE' . $strWhere . ' ORDER BY sa.id DESC LIMIT 10';

		return self::fetchScreeningApplicants( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicantDetailsByIdByCid( $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
                       sa.*,
                       s.property_id
            FROM
                       screening_applicants sa
                       LEFT JOIN screenings s ON s.id = sa.screening_id
            WHERE
                       sa.id = ' . ( int ) $intId . '
                       AND sa.cid =' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningApplicantsByScreeningIdByCustomApplicantIdsByCid( $intScreeningId, $arrintCustomApplicantIds, $intCid, $objScreeningDatabase ) {
		if( true == is_null( $intScreeningId ) || false == valArr( $arrintCustomApplicantIds ) ) return NULL;

		$strSql = 'SELECT
						*
				   FROM
						screening_applicants 
				   WHERE
						screening_id = ' . ( int ) $intScreeningId . '
						AND cid = ' . ( int ) $intCid . '
						AND custom_applicant_id IN( ' . implode( ',', $arrintCustomApplicantIds ) . ' ) ORDER BY ID DESC';

		return parent::fetchScreeningApplicants( $strSql, $objScreeningDatabase );
	}

	public static function fetchScreeningApplicantsByScreeningIdByCustomApplicantIdsByStatusTypeIdsByCid( $intScreeningId, $arrintApplicantIds, $arrintStatusTypeIds, $intCid, $objScreeningDatabase ) {
		if( NULL == $intScreeningId || false == valArr( $arrintApplicantIds ) || false == valArr( $arrintStatusTypeIds ) ) return NULL;

		$strSql = 'SELECT
						  *
				   FROM
						screening_applicants
				   WHERE
						custom_applicant_id IN( ' . implode( ',', $arrintApplicantIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND screening_id = ' . ( int ) $intScreeningId . '
						AND status_type_id IN( ' . implode( ',', $arrintStatusTypeIds ) . ' ) ';

		return parent::fetchScreeningApplicants( $strSql, $objScreeningDatabase );
	}

	public static function fetchCustomScreeningApplicantInformationByIds( $arrintDisputedScreeningApplicantIds, $objDatabase ) {

		$strSql = 'SELECT
						sa.tax_number_encrypted,
						sa.id,
						cd.dispute_type_id
					FROM
						screening_applicants sa
						JOIN consumers c ON c.screening_applicant_id = sa.id 
						JOIN consumer_disputes cd ON cd.consumer_id = c.id
					WHERE
						sa.id IN ( ' . implode( ',', $arrintDisputedScreeningApplicantIds ) . ' )
						ORDER BY ID';

		$arrmixScreeningApplicantDetails = fetchData( $strSql, $objDatabase );
		if( false == valArr( $arrmixScreeningApplicantDetails ) ) {
			return NULL;
		}

		foreach( $arrmixScreeningApplicantDetails as $intKey => $arrmixScreeningApplicantDetail ) {
			if( valStr( $arrmixScreeningApplicantDetails[$intKey]['tax_number_encrypted'] ) ) {
				$arrmixScreeningApplicantDetails[$intKey]['tax_number'] = preg_replace( '/[^a-z0-9]/i', '', ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $arrmixScreeningApplicantDetails[$intKey]['tax_number_encrypted'], CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] ) );
			}
		}

		return $arrmixScreeningApplicantDetails;
	}

	public static function activateOrDeactivateScreeningApplicants( $intCurrentUserID, $intStatusTypeId, $intScreeningId, $arrintScreeningApplicantIds, $intCid, $objDatabase ) {

		$strSqlUpdate = 'UPDATE
						screening_applicants
					SET
						status_type_id	= ' . ( int ) $intStatusTypeId . ',
						updated_by = ' . ( int ) $intCurrentUserID . ',
						updated_on = NOW()
		          WHERE
						screening_id = ' . ( int ) $intScreeningId . '
						AND custom_applicant_id IN ( ' . implode( ',', $arrintScreeningApplicantIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		$arrmixResult = fetchData( $strSqlUpdate, $objDatabase );
		return $arrmixResult[0];
	}

	public static function fetchScreeningApplicantsByIdsByScreeningIdByCid( $arrintScreeningApplicantIds, $intScreeningId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintScreeningApplicantIds ) ) return NULL;

		$strSql = 'SELECT
							sa.*,
							saa.address_line as previous_address_line,
							saa.city as previous_city,
							saa.state as previous_state,
							saa.zipcode as previous_zipcode
				   FROM
						screening_applicants sa
						LEFT JOIN screening_applicant_addresses saa ON saa.screening_applicant_id = sa.id AND saa.cid = sa.cid
				   WHERE
						sa.screening_id = ' . ( int ) $intScreeningId . '
						AND sa.id IN ( ' . sqlIntImplode( $arrintScreeningApplicantIds ) . ' )
						AND sa.cid = ' . ( int ) $intCid;

		return parent::fetchObjects( $strSql, 'CScreeningApplicant', $objDatabase, 'id' );
	}

	public static function fetchScreeningApplicantDetailsByIdByScreeningId( $intId, $intScreeningId, $objDatabase ) {

		$strSql = 'SELECT
                        sa.*,
                        s.custom_application_id
					FROM
                        screening_applicants sa
                        LEFT JOIN screenings s ON s.id = sa.screening_id
					WHERE
                        sa.id = ' . ( int ) $intId . '
                        AND sa.screening_id = ' . ( int ) $intScreeningId;

		return parent::fetchScreeningApplicant( $strSql, $objDatabase );
	}

    public static function fetchScreeningApplicantsByScreeningRecordSearchFilter( $objCScreeningReportSearchFilter, $objDatabase ) {

        if( false == valObj( $objCScreeningReportSearchFilter, CScreeningReportSearchFilter::class ) ) {
            return NULL;
        }

        $strWhere = '';
        $strJoin = '';

        if( true == valId( $objCScreeningReportSearchFilter->getCid() ) ) {
            $strWhere .= ' sa.cid = ' . ( int ) $objCScreeningReportSearchFilter->getCid();
        } else {
            return NULL;
        }

        if( true == $objCScreeningReportSearchFilter->getIsPropertySearchFilter() && true == valArr( $objCScreeningReportSearchFilter->getPropertyIds() ) ) {
            $strWhere .= ' AND s.property_id IN (' . sqlIntImplode( $objCScreeningReportSearchFilter->getPropertyIds() ) . ') ';
        } else {
            return NULL;
        }

        if( true == valArr( $objCScreeningReportSearchFilter->getScreenTypeIds() ) ) {
            $strWhere   .= ' AND spsta.screen_type_id IN( ' . sqlIntImplode( $objCScreeningReportSearchFilter->getScreenTypeIds() ) . ') ';
            $strJoin   .= ' LEFT JOIN screening_package_screen_type_associations spsta ON spsta.screening_package_id = sp.id LEFT JOIN screening_transactions st ON st.screening_applicant_id = sa.id AND st.screen_type_id = spsta.screen_type_id ';
        } else {
            return NULL;
        }

        if( true == valStr( $objCScreeningReportSearchFilter->getFromDate() ) && true == valStr( $objCScreeningReportSearchFilter->getToDate() ) ) {
            $strWhere .= ' AND  sa.created_on::DATE BETWEEN DATE(\'' . $objCScreeningReportSearchFilter->getFromDate() . '\') AND DATE(\'' . $objCScreeningReportSearchFilter->getToDate() . '\') AND sa.screening_completed_on IS NOT NULL';
        } else if( true == valStr( $objCScreeningReportSearchFilter->getMonthYear() ) ) {
            $strWhere .= ' AND DATE_TRUNC(\'month\', sa.created_on) = DATE_TRUNC(\'month\', TIMESTAMP \'' . ( string ) $objCScreeningReportSearchFilter->getMonthYear() . '\') AND sa.screening_completed_on IS NOT NULL';
        } else {
            return NULL;
        }

        $strFields = 'st.screen_type_id,sa.id, sa.cid, sa.name_first, sa.name_middle, sa.name_last, sa.screening_completed_on, sa.created_on, st.screening_recommendation_type_id, sp.name AS package_name, s.created_on, s.property_id, s.screening_decision_type_id, s.screening_recommendation_type_id AS group_recommendation';
        if( false == $objCScreeningReportSearchFilter->getIsPropertySearchFilter() ) {
            $strOrderBy = 'sa.id DESC, sa.cid';
        } else {
            $strOrderBy = 'sa.id DESC, sa.cid, s.property_id';
        }

        $arrintFailedConditionalRecommendationTypeIds = [ CScreeningRecommendationType::PASSWITHCONDITIONS, CScreeningRecommendationType::FAIL ];
        $strWhere .= ' AND st.screening_recommendation_type_id IN( ' . sqlIntImplode( $arrintFailedConditionalRecommendationTypeIds ) . ')  AND sa.status_type_id = ' . ( int ) CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED;

        $strSql = 'SELECT
                      ' . $strFields . '
                   FROM
                       screening_applicants sa
                       LEFT JOIN screenings s ON s.id = sa.screening_id
                       LEFT JOIN screening_packages sp ON sp.id = sa.screening_package_id
                       ' . $strJoin . '
                   WHERE
                        ' . $strWhere . '
                   GROUP BY st.screen_type_id,sa.id, st.screening_recommendation_type_id,sp.name,s.created_on,s.property_id,s.screening_decision_type_id,s.screening_recommendation_type_id
                   ORDER BY
                                    ' . $strOrderBy;

        return fetchData( $strSql, $objDatabase );
    }

	public static function fetchActiveVendorScreeningApplicantByCustomApplicantId( $intCustomApplicantId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_applicants
					WHERE
						custom_applicant_id = ' . ( int ) $intCustomApplicantId . '
						AND screening_applicant_type_id IN( ' . implode( ',', CScreeningApplicantType::$c_arrintVendorApplicantTypeIds ) . ' )
						AND status_type_id IN (' . CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR . ',' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . ')
					ORDER BY id DESC
						LIMIT 1';
				return parent::fetchScreeningApplicant( $strSql, $objDatabase );
	}

    public static function fetchActiveScreeningApplicantsByApplicantTypeIdByScreeningIdByCid( $intScreeningApplicantTypeId, $intScreeningId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_applicants
					WHERE
						screening_applicant_type_id = ' . ( int ) $intScreeningApplicantTypeId . '
						AND screening_id = ' . ( int ) $intScreeningId . ' 
						AND cid = ' . ( int ) $intCid . '
						AND status_type_id <>  ' . ( int ) CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED;

	    return fetchData( $strSql, $objDatabase );
    }

	public function fetchActiveScreeningApplicantIdByScreeningIdByCustomApplicantIdByCid( $intScreeningId, $intApplicantId, $intCid, $objDatabase ) {
		$arrintCancelledStatusTypeIds 	= array( CScreeningStatusType::APPLICANT_RECORD_STATUS_CANCELLED );

		$strSql = 'SELECT
						  id
				   FROM
						screening_applicants
				   WHERE
						screening_id = ' . ( int ) $intScreeningId . '
						AND custom_applicant_id = ' . ( int ) $intApplicantId . '
						AND cid = ' . ( int ) $intCid . '
						AND status_type_id NOT IN( ' . implode( ',', $arrintCancelledStatusTypeIds ) . ' ) ';

		return parent::fetchScreeningApplicant( $strSql, $objDatabase );
	}
}
?>