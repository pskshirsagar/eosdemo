<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCreditAlertTypes
 * Do not add any new functions to this class.
 */

class CScreeningCreditAlertTypes extends CBaseScreeningCreditAlertTypes {

    public static function fetchScreeningCreditAlertTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CScreeningCreditAlertType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchScreeningCreditAlertType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScreeningCreditAlertType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>