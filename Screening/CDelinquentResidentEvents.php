<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDelinquentResidentEvents
 * Do not add any new functions to this class.
 */

class CDelinquentResidentEvents extends CBaseDelinquentResidentEvents {

	public static function fetchDelinquentResidentEventsByPropertyIdByLeaseIdsByCustomerIdsByDeliquentEventTypeIdsByCid( $intPropertyId, $arrintLeaseIds, $arrintCustomerIds, $arrintDeliquentEventTypeIds = NULL, $intCid, $boolFetchObjectOnly = false, $objDatabase ) {
		$arrintLeaseIds    = array_filter( $arrintLeaseIds );
		$arrintCustomerIds = array_filter( $arrintCustomerIds );

		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintCustomerIds ) ) {
			return NULL;
		}

		$strCondition = ( true == valArr( $arrintDeliquentEventTypeIds ) ) ? ' AND dre.delinquent_event_type_id IN ( ' . implode( ',', $arrintDeliquentEventTypeIds ) . ' ) ' : '';

		$strSql = 'SELECT
						dre.*,
						dr.customer_id
					FROM
						delinquent_resident_events dre 
						INNER JOIN delinquent_residents dr 
						ON ( dre.delinquent_resident_id = dr.id AND dre.cid = dr.cid ) 
					WHERE
						dre.cid = ' . ( int ) $intCid . '
						AND dre.property_id = ' . ( int ) $intPropertyId . '
						AND dre.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND dr.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND dre.deleted_on IS NULL ' .
		          $strCondition;

		if( true == $boolFetchObjectOnly ) {
			return self::fetchDelinquentResidentEvents( $strSql, $objDatabase );
		} else {
			$arrmixTempResidentDelinquentEvents = fetchData( $strSql, $objDatabase );

			if( false == valArr( $arrmixTempResidentDelinquentEvents ) ) {
				return NULL;
			}

			$arrmixResidentDelinquentEvents = [];

			foreach( $arrmixTempResidentDelinquentEvents as $arrmixData ) {
				$strEventKey                                    = $arrmixData['cid'] . '_' . $arrmixData['property_id'] . '_' . $arrmixData['lease_id'] . '_' . $arrmixData['customer_id'];
				$arrmixResidentDelinquentEvents[$strEventKey][] = $arrmixData;
			}

			return $arrmixResidentDelinquentEvents;
		}
	}

	public static function fetchDelinquentResidentEventsByFirstNameByLastNameByBirthDateEncryptedBySsnEncryptedByCid( $strFirstName, $strLastName, $strBirthDateEncrypted = '', $strSsnEncrypted = '', $intIsActive = 0, $intCid, $objDatabase ) {

        if( false == valStr( $strFirstName ) || false == valStr( $strLastName ) || false == valStr( $strBirthDateEncrypted ) || false == valStr( $strSsnEncrypted ) ) return array();

		$strWhere = '';

		if( '' === trim( $strSsnEncrypted ) ) {
			$strWhere .= ' AND dr.tax_number_encrypted IS NULL';
		} else {
			$strWhere .= ' AND dr.tax_number_encrypted LIKE \'' . $strSsnEncrypted . '\'';
		}

		if( '' === trim( $strBirthDateEncrypted ) ) {
			$strWhere .= ' AND dr.birth_date_encrypted IS NULL';
		} else {
			$strWhere .= ' AND dr.birth_date_encrypted LIKE \'' . $strBirthDateEncrypted . '\'';
		}

		switch( $intIsActive ) {
			case 0:
				// Nothing
				break;

			case 1:
				$strWhere .= ' AND dre.deleted_on IS NULL';
				break;

			case 2:
				$strWhere .= ' AND dre.deleted_on IS NOT NULL';
				break;

			default:
				// Nothing
				break;
		}

		$strSql = 'SELECT
						dre.*,
						dr.name_first,
						dr.name_last,
						dr.name_middle,
						dr.birth_date_encrypted,
						dr.birth_date_year,
						dr.tax_number_last_four,
						dr.email_address,
						dr.address_line,
						dr.city,
						dr.state,
						dr.phone_number
						FROM
					delinquent_resident_events dre
					LEFT JOIN delinquent_residents dr ON dre.delinquent_resident_id = dr.id
					WHERE
						dr.cid = ' . ( int ) $intCid . ' 
						AND dr.name_first like \'' . pg_escape_string( $strFirstName ) . '\' 
						AND dr.name_last like \'' . pg_escape_string( $strLastName ) . '\' 
						' . $strWhere . '
					ORDER BY dre.property_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveDelinquentResidentEventsByDelinquentResidentIdByCid( $intDelinquentResidentId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
					    *
 					FROM
						delinquent_resident_events
 					WHERE
					    delinquent_resident_id = ' . ( int ) $intDelinquentResidentId . '
					    AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL';

		return parent::fetchDelinquentResidentEvents( $strSql, $objDatabase );
 	}

	public static function fetchActiveDelinquentResidentEventsByDelinquentResidentIdByPropertyIdByCid( $intDelinquentResidentId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						dre.*,
						dr.name_first,
						dr.name_last,
						dr.name_middle,
						dr.birth_date_encrypted,
						dr.birth_date_year,
						dr.tax_number_last_four,
						dr.email_address,
						dr.address_line,
						dr.city,
						dr.state,
						dr.phone_number
					FROM
						delinquent_resident_events dre
						JOIN delinquent_residents dr ON ( dre.delinquent_resident_id = dr.id AND dre.cid = dr.cid )
					WHERE
						dre.delinquent_resident_id = ' . ( int ) $intDelinquentResidentId . '
					    AND dre.property_id = ' . ( int ) $intPropertyId . '
					    AND dre.cid = ' . ( int ) $intCid . '
						AND dre.deleted_on IS NULL
						ORDER BY dre.id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllDelinquentResidentEventsByDelinquentResidentIdByPropertyIdByCid( $intDelinquentResidentId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intDelinquentResidentId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						dre.*,
						dr.name_first,
						dr.name_last,
						dr.name_middle,
						dr.birth_date_encrypted,
						dr.birth_date_year,
						dr.tax_number_last_four,
						dr.email_address,
						dr.address_line,
						dr.city,
						dr.state,
						dr.phone_number,
						dr.driver_license,
						dr.driver_license_state,
						dr.customer_id
					FROM
						delinquent_resident_events dre
						JOIN delinquent_residents dr ON ( dre.delinquent_resident_id = dr.id AND dre.cid = dr.cid )
					WHERE
						dre.delinquent_resident_id = ' . ( int ) $intDelinquentResidentId . '
					    AND dre.property_id = ' . ( int ) $intPropertyId . '
					    AND dre.cid = ' . ( int ) $intCid . '
						ORDER BY dre.id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function archieveDelinquentResidentEventRecordsByIdsByCidWithDeletedInfo( $intCurrentUserId, $arrintDelinquentResidentEventIds, $intCid, $intDeletedReasonId, $strDeletedNote, $objDatabase ) {

		if( false == valArr( $arrintDelinquentResidentEventIds ) ) {
			return;
		}

		$strUpdateSql = 'UPDATE
							delinquent_resident_events
						SET
							is_active = FALSE, 
							deleted_notes = \'' . $strDeletedNote . '\',
							deleted_reason_id = ' . ( int ) $intDeletedReasonId . ',
							delinquent_event_status_type_id = ' . CDelinquentEventStatusType::IN_ACTIVE . ',
							updated_by = ' . ( int ) $intCurrentUserId . ',
							updated_on = NOW(),
							deleted_on = NOW()  
						WHERE
							cid = ' . ( int ) $intCid . '
							AND id IN ( ' . implode( ',', $arrintDelinquentResidentEventIds ) . ' ) ';
		return executeSql( $strUpdateSql, $objDatabase );
	}

	public static function bulkUpdateDelinquentResidentEventsIsDnrStatusBYCid( $intCurrentUserId, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) {
			return false;
		}

		$strUpdateSql = 'UPDATE
							delinquent_resident_events
						SET
							is_dnr_record = TRUE,
							updated_by = ' . ( int ) $intCurrentUserId . ',
							updated_on = NOW()
						WHERE
							cid = ' . ( int ) $intCid . '
							AND is_active = TRUE
                            AND is_dnr_record = FALSE';

		return executeSql( $strUpdateSql, $objDatabase );
	}

}

?>