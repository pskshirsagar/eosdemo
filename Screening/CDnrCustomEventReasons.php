<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDnrCustomEventReasons
 * Do not add any new functions to this class.
 */

class CDnrCustomEventReasons extends CBaseDnrCustomEventReasons {

	public static function fetchDnrCustomEventReasonsByDelinquentEventTypeIdsByCid( $arrintDelinquentEventTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintDelinquentEventTypeId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						dnr_custom_event_reasons
					WHERE
						cid = ' . ( int ) $intCid . '
						AND delinquent_event_type_id IN ( ' . sqlIntImplode( $arrintDelinquentEventTypeId ) . ' )
						AND is_published = TRUE';

		return self::fetchDnrCustomEventReasons( $strSql, $objDatabase );
	}

}
?>