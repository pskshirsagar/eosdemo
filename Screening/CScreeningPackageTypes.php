<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageTypes
 * Do not add any new functions to this class.
 */

class CScreeningPackageTypes extends CBaseScreeningPackageTypes {

    public static function fetchScreeningPackageTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CScreeningPackageType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchScreeningPackageType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScreeningPackageType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchAllScreeningPackageTypes( $objDatabase ) {
    	return self::fetchScreeningPackageTypes( 'SELECT * FROM screening_package_types', $objDatabase );
    }

    public static function fetchPublishedScreeningPackageTypes( $objDatabase, $boolAllFieldValues = false ) {

    	$strSelectClause = ( true == $boolAllFieldValues ) ? ' spt.* ' : ' spt.id, spt.name ';

    	$strSql = 'SELECT' .
    				$strSelectClause .
    			  'FROM
    			  	screening_package_types spt
    			  WHERE
    			  	is_published = 1
    			  	AND spt.id NOT IN ( ' . implode( ',', CScreeningPackageType::$c_arrintVAScreeningPackageTypes ) . ')';

    	return self::fetchScreeningPackageTypes( $strSql, $objDatabase );
    }

}
?>