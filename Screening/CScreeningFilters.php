<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningFilters
 * Do not add any new functions to this class.
 */

class CScreeningFilters extends CBaseScreeningFilters {

	public static function fetchActiveScreeningFilters( $objDatabase ) {
		$strSql = 'SELECT
						sf.*
					FROM
						screening_filters sf
					WHERE
						sf.is_active = 1';
		return fetchData( $strSql, $objDatabase );
	}

}
?>