<?php

class CScreeningErrorType extends CBaseScreeningErrorType {

	const INVALID_USER_INPUT                = 1;
	const INVALID_INPUT_DATA_FORMAT         = 2;
	const CLIENT_SETUP_ISSUE                = 3;
	const INVALID_CLIENT_USER_AUTH          = 4;
	const DATA_PROVIDER_AUTH                = 5;
	const DATA_PROVIDER_UNAVAILABLE         = 6;
	const DATA_PROVIDER_ERROR               = 7;
	const RV_SECURE_UNAVAILABLE             = 8;
	const SECURE_UNHANDLED_EXCEPTION	    = 9;
	const RV_UNHANDLED_EXCEPTION            = 10;
	const DATA_PROVIDER_SPECIAL_HANDLING    = 11;
	const UNKNOWN                           = 12;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSeverityLevel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>