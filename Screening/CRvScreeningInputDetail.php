<?php

class CRvScreeningInputDetail extends CBaseRvScreeningInputDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsExecuted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>