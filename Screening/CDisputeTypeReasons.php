<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CDisputeTypeReasons
 * Do not add any new functions to this class.
 */

class CDisputeTypeReasons extends CBaseDisputeTypeReasons {

	public static function fetchDisputeTypeReasons( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDisputeTypeReason', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDisputeTypeReason( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDisputeTypeReason', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedDisputeTypeReasonsByDisputeTypeId( $intDisputeTypeId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						dispute_type_reasons
					WHERE
						is_published = true
						AND dispute_type_id=' . ( int ) $intDisputeTypeId . '
						ORDER BY order_number';

		return self::fetchDisputeTypeReasons( $strSql, $objDatabase );
	}

}
?>