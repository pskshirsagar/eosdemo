<?php

class CScreeningPackageConditionType extends CBaseScreeningPackageConditionType {

    const VERIFY_IDENTITY                   = 1;
    const INCOME_VERIFICATION               = 2;
    const INCREASE_SECURITY_DEPOSIT         = 3;
    const INCREASE_RENT                     = 4;
    const ADD_GUARANTOR                     = 5;
    const CUSTOM_CONDITION                  = 9;
    const OTHER_CHARGES                     = 10;
    const REPLACE_SECURITY_DEPOSIT     	    = 11;
    const APRROVED_WITH_QUALIFYING_ROOMMATE = 12;
	const VERIFY_RENTAL_COLLECTION_PAYMENT  = 13;
    const UPFRONT_RENT                      = 14;

    protected $m_intCid;
    protected $m_intChargeTypeId;
    protected $m_intBaseChargeCodeId;
    protected $m_intApplyChargeCodeId;
    protected $m_intScreeningPackageConditionTypeId;
    protected $m_intScreeningAvailableConditionId;
	protected $m_intScreeningPackageAvailableConditionId;
    protected $m_intScreeningPackageChargeCodeAmountTypeId;
    protected $m_intScreeningIntegrationConditionCodeId;

    protected $m_strScreeningAvailableConditionName;
	protected $m_strScreeningPackageAvailableConditionName;

	protected $m_boolIsRateVaries;

    public static $c_arrintFinancialScreeningConditionTypeIds = array( CScreeningPackageConditionType::INCREASE_SECURITY_DEPOSIT, CScreeningPackageConditionType::REPLACE_SECURITY_DEPOSIT, CScreeningPackageConditionType::INCREASE_RENT, CScreeningPackageConditionType::OTHER_CHARGES, self::VERIFY_RENTAL_COLLECTION_PAYMENT );

    public static $c_arrintDocumentTypeScreeningConditionIds = array( self::VERIFY_IDENTITY, self::INCOME_VERIFICATION, self::CUSTOM_CONDITION );

    public static $c_arrintNonMidLeaseTransferRenewalScreeningConditionTypeIds = [ self::REPLACE_SECURITY_DEPOSIT, self::INCREASE_RENT, self::OTHER_CHARGES, self::CUSTOM_CONDITION, self::VERIFY_IDENTITY, self::INCOME_VERIFICATION, self::VERIFY_RENTAL_COLLECTION_PAYMENT ];

    public static $c_arrintMidLeaseTransferRenewalScreeningConditionTypeIds = [ self::INCREASE_SECURITY_DEPOSIT, self::ADD_GUARANTOR, self::APRROVED_WITH_QUALIFYING_ROOMMATE, self::CUSTOM_CONDITION ];

    public static function getScreeningPackageConditionTypeName( $intScreeningPackageConditionTypeId ) {
        $strScreeningPackageConditionTypeName = '';

        switch( $intScreeningPackageConditionTypeId ) {
            case self::VERIFY_IDENTITY:
                $strScreeningPackageConditionTypeName = 'Verify Identity';
                break;

            case self::INCOME_VERIFICATION:
                $strScreeningPackageConditionTypeName = 'Require Income';
                break;

            case self::INCREASE_SECURITY_DEPOSIT:
                $strScreeningPackageConditionTypeName = 'Increase Security Deposit';
                break;

            case self::INCREASE_RENT:
                $strScreeningPackageConditionTypeName = 'Increase Rent';
                break;

            case self::ADD_GUARANTOR:
                $strScreeningPackageConditionTypeName = 'Add Guarantor';
                break;

            case self::CUSTOM_CONDITION:
                $strScreeningPackageConditionTypeName = 'Custom Condition';
                break;

            case self::OTHER_CHARGES:
                $strScreeningPackageConditionTypeName = 'Other Charges';
                break;

          	case self::REPLACE_SECURITY_DEPOSIT:
            	$strScreeningPackageConditionTypeName = 'Replace Security Deposit';
           		break;

	        case self::APRROVED_WITH_QUALIFYING_ROOMMATE:
		        $strScreeningPackageConditionTypeName = 'Approved With Qualifying Roommate';
		        break;

	        case self::VERIFY_RENTAL_COLLECTION_PAYMENT:
		        $strScreeningPackageConditionTypeName = 'Rental Collection Payment Proof';
		        break;

            case self::UPFRONT_RENT:
                $strScreeningPackageConditionTypeName = 'Upfront rent';
                break;

            default:
                // added default case
        }

        return $strScreeningPackageConditionTypeName;
    }

    public function getScreeningAvailableConditionId() {
        return $this->m_intScreeningAvailableConditionId;
    }

	public function getScreeningPackageAvailableConditionId() {
		return $this->m_intScreeningPackageAvailableConditionId;
	}

	public function getCid() {
        return $this->m_intCid;
    }

    public function getScreeningPackageConditionTypeId() {
        return $this->m_intScreeningPackageConditionTypeId;
    }

    public function getScreeningPackageChargeCodeAmountTypeId() {
        return $this->m_intScreeningPackageChargeCodeAmountTypeId;
    }

    public function getBaseChargeCodeId() {
        return $this->m_intBaseChargeCodeId;
    }

    public function getApplyChargeCodeId() {
        return $this->m_intApplyChargeCodeId;
    }

    public function getChargeTypeId() {
        return $this->m_intChargeTypeId;
    }

    public function getScreeningAvailableConditionName() {
        return $this->m_strScreeningAvailableConditionName;
    }

    public function getScreeningPackageAvailableConditionName() {
		return $this->m_strScreeningPackageAvailableConditionName;
	}

	public function getIsRateVaries() {
		return $this->m_boolIsRateVaries;
	}

	public function getScreeningIntegrationConditionCodeId() {
		return $this->m_intScreeningIntegrationConditionCodeId;
	}

    public function setScreeningAvailableConditionId( $intScreeningAvailableConditionId ) {
        $this->m_intScreeningAvailableConditionId = ( int ) $intScreeningAvailableConditionId;
    }

    public function setCid( $intCid ) {
        $this->m_intCid = ( int ) $intCid;
    }

    public function setScreeningPackageConditionTypeId( $intScreeningPackageConditionTypeId ) {
        $this->m_intScreeningPackageConditionTypeId = ( int ) $intScreeningPackageConditionTypeId;
    }

    public function setScreeningPackageChargeCodeAmountTypeId( $intScreeningPackageChargeCodeAmountTypeId ) {
        $this->m_intScreeningPackageChargeCodeAmountTypeId = ( int ) $intScreeningPackageChargeCodeAmountTypeId;
    }

    public function setBaseChargeCodeId( $intBaseChargeCodeId ) {
        $this->m_intBaseChargeCodeId = ( int ) $intBaseChargeCodeId;
    }

    public function setApplyChargeCodeId( $intApplyChargeCodeId ) {
        $this->m_intApplyChargeCodeId = ( int ) $intApplyChargeCodeId;
    }

    public function setChargeTypeId( $intChargeTypeId ) {
        $this->m_intChargeTypeId = ( int ) $intChargeTypeId;
    }

    public function setScreeningAvailableConditionName( $strScreeningAvailableConditionName ) {
        $this->m_strScreeningAvailableConditionName = $strScreeningAvailableConditionName;
    }

    public function setScreeningPackageAvailableConditionName( $strScreeningPackageAvailableConditionName ) {
		$this->m_strScreeningPackageAvailableConditionName = $strScreeningPackageAvailableConditionName;
	}

	public function setScreeningPackageAvailableConditionId( $intScreeningPackageAvailableConditionId ) {
		$this->m_intScreeningPackageAvailableConditionId = ( int ) $intScreeningPackageAvailableConditionId;
	}

	public function setIsRateVaries( $boolIsRateVaries ) {
		$this->m_boolIsRateVaries = ( int ) $boolIsRateVaries;
	}

	public function setScreeningIntegrationConditionCodeId( $intScreeningIntegrationConditionCodeId ) {
		$this->m_intScreeningIntegrationConditionCodeId = $intScreeningIntegrationConditionCodeId;
	}

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

        parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['cid'] ) ) 											$this->setCid( $arrmixValues['cid'] );
        if( true == isset( $arrmixValues['charge_type_id'] ) ) 									$this->setChargeTypeId( $arrmixValues['charge_type_id'] );
        if( true == isset( $arrmixValues['base_charge_code_id'] ) ) 							$this->setBaseChargeCodeId( $arrmixValues['base_charge_code_id'] );
        if( true == isset( $arrmixValues['apply_to_charge_code_id'] ) ) 						$this->setApplyChargeCodeId( $arrmixValues['apply_to_charge_code_id'] );
        if( true == isset( $arrmixValues['screening_package_condition_type_id'] ) ) 			$this->setScreeningPackageConditionTypeId( $arrmixValues['screening_package_condition_type_id'] );
        if( true == isset( $arrmixValues['screening_available_condition_id'] ) ) 				$this->setScreeningAvailableConditionId( $arrmixValues['screening_available_condition_id'] );
	    if( true == isset( $arrmixValues['screening_package_available_condition_id'] ) ) 		$this->setScreeningPackageAvailableConditionId( $arrmixValues['screening_package_available_condition_id'] );
        if( true == isset( $arrmixValues['screening_package_charge_code_amount_type_id'] ) ) 	$this->setScreeningPackageChargeCodeAmountTypeId( $arrmixValues['screening_package_charge_code_amount_type_id'] );
        if( true == isset( $arrmixValues['screening_available_condition_name'] ) )				$this->setScreeningAvailableConditionName( $arrmixValues['screening_available_condition_name'] );
	    if( true == isset( $arrmixValues['screening_package_available_condition_name'] ) ) 		$this->setScreeningPackageAvailableConditionName( $arrmixValues['screening_package_available_condition_name'] );
	    if( true == isset( $arrmixValues['is_rate_varies'] ) ) 		                            $this->setIsRateVaries( $arrmixValues['is_rate_varies'] );
	    if( true == isset( $arrmixValues['screening_integration_condition_code_id'] ) )			$this->setScreeningIntegrationConditionCodeId( $arrmixValues['screening_integration_condition_code_id'] );

        return;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
                break;

            default:
            $boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>