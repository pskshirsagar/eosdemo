<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CTransactionReviewLogs
 * Do not add any new functions to this class.
 */

class CTransactionReviewLogs extends CBaseTransactionReviewLogs {

	public static function fetchTransactionReviewLogsDetailsByTransactionReviewId( $intTransactionReviewId, $objDatabase ) {
		$strSql = 'SELECT * FROM transaction_review_logs trl WHERE transaction_review_id = ' . ( int ) $intTransactionReviewId . ' ORDER BY trl.id DESC';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTransactionReviewLogsDetailsByTransactionReviewIdByOldStatusByNewStatus( $intTransactionReviewId, $intOldStatusId, $intNewStatusId, $objDatabase ) {
		$strSql = 'SELECT 
					    EXTRACT(MINUTE FROM (current_timestamp - created_on)) as diff,
					    created_by
					FROM 
						transaction_review_logs  
					WHERE 
						transaction_review_id = ' . ( int ) $intTransactionReviewId . ' 
						AND old_status_id =  ' . ( int ) $intOldStatusId . '
						AND new_status_id =  ' . ( int ) $intNewStatusId;

		return fetchData( $strSql, $objDatabase );
	}

}
?>