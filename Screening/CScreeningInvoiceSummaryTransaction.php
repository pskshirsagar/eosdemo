<?php

class CScreeningInvoiceSummaryTransaction extends CBaseScreeningInvoiceSummaryTransaction {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreenTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningInvoiceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionChargeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalTransactionCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalInvoiceAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalTransactionAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReconciliationStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>