<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CEvictionStatusTypes
 * Do not add any new functions to this class.
 */

class CEvictionStatusTypes extends CBaseEvictionStatusTypes {

    public static function fetchEvictionStatusTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CEvictionStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchEvictionStatusType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CEvictionStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchActiveEvictionStatusTypes( $objDatabase ) {

    	$strSql = 'SELECT
    					*
    				FROM
    					eviction_status_types
    				WHERE
    					is_active = 1';

    	return self::fetchEvictionStatusTypes( $strSql, $objDatabase );

    }

}
?>