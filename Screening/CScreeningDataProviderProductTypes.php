<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningDataProviderProductTypes
 * Do not add any new functions to this class.
 */

class CScreeningDataProviderProductTypes extends CBaseScreeningDataProviderProductTypes {

	public static function fetchScreeningDataProviderProductTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CScreeningDataProviderProductType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningDataProviderProductType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CScreeningDataProviderProductType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedScreeningDataProviderProductTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_data_provider_product_types
					WHERE
						is_published = True';
		return self::fetchScreeningDataProviderProductTypes( $strSql, $objDatabase );
	}

}
?>