<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningCriteriaScreenTypeActions
 * Do not add any new functions to this class.
 */

class CScreeningCriteriaScreenTypeActions extends CBaseScreeningCriteriaScreenTypeActions {

	public static function fetchPublishedScreeningCriteriaScreenTypeActionsDataByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {
		$strSql = 'SELECT
					    *
					FROM
					    screening_criteria_screen_type_actions
					WHERE
					    is_published = true
					    AND screening_criteria_id =' . ( int ) $intScreeningCriteriaId;

		$arrmixResult = executeSql( $strSql, $objDatabase );
		$arrmixResult = ( false == getArrayElementByKey( 'failed', $arrmixResult ) ) ? getArrayElementByKey( 'data', $arrmixResult ) : [];
		return $arrmixResult;
	}

	public static function fetchScreeningCriteriaScreenTypeActionByScreeningCriteriaId( $intScreeningCriteriaId, $objDatabase ) {
		$strSql = 'SELECT
					    *
					FROM
					    screening_criteria_screen_type_actions
					WHERE
					     screening_criteria_id =' . ( int ) $intScreeningCriteriaId;

		return parent::fetchScreeningCriteriaScreenTypeAction( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningCriteriaScreenTypeActionsByScreeningPackageIdByScreenTypeIdByScreeningConfigPreferenceTypeId( $intScreeningPackageId, $intScreenTypeId, $intScreeningConfigPreferenceTypeId, $objDatabase ) {
		$strSql = 'SELECT
						spcta.*
					FROM
						screening_criteria_screen_type_actions spcta
						JOIN screening_package_screen_type_associations spsta ON spsta.screening_criteria_id = spcta.screening_criteria_id AND spsta.screen_type_id = ' . ( int ) $intScreenTypeId . '
					WHERE
						spsta.is_published = 1
						AND spsta.screening_package_id = ' . ( int ) $intScreeningPackageId . '
						AND spcta.screening_config_preference_type_id = ' . ( int ) $intScreeningConfigPreferenceTypeId . '
						AND spcta.screen_type_id = ' . ( int ) $intScreenTypeId;

		return parent::fetchScreeningCriteriaScreenTypeAction( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningCriteriaScreenTypeActionsByScreeningPackageId( $intScreeningPackageId, $objDatabase ) {
		$strSql = 'SELECT
						spcta.*
					FROM
						screening_criteria_screen_type_actions spcta
						JOIN screening_package_screen_type_associations spsta ON spsta.screening_criteria_id = spcta.screening_criteria_id
					WHERE
						spsta.is_published = 1
						AND spsta.screening_package_id = ' . ( int ) $intScreeningPackageId;

		return parent::fetchScreeningCriteriaScreenTypeActions( $strSql, $objDatabase );
	}

}
?>