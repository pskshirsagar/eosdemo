<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageOperandTypes
 * Do not add any new functions to this class.
 */

class CScreeningPackageOperandTypes extends CBaseScreeningPackageOperandTypes {

	public static function fetchScreeningPackageOperandTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningPackageOperandType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

    public static function fetchScreeningPackageOperandType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScreeningPackageOperandType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

	public static function fetchPublishedScreeningPackageOperandTypes( $objDatabase ) {

		$strSql = 'SELECT
						*
				   FROM
						screening_package_operand_types
				   WHERE
						is_published = 1';

		return parent::fetchScreeningPackageOperandTypes( $strSql, $objDatabase );

	}

}
?>