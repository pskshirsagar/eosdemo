<?php

class CScreeningPackageTypeScreenTypeAssociation extends CBaseScreeningPackageTypeScreenTypeAssociation {

    protected $m_strName;
    protected $m_boolIsUseForPreScreening;
    protected $m_intSelectionModeTypeId;
    protected $m_intScreeningDataProviderId;

    public function getName() {
        return $this->m_strName;
    }

    public function setName( $strScreenTypeDisplayName ) {
        $this->m_strName = $strScreenTypeDisplayName;
    }

    public function getIsUseForPreScreening() {
        return $this->m_boolIsUseForPreScreening;
    }

    public function setIsUseForPreScreening( $boolIsUseForPreScreening ) {
        $this->m_boolIsUseForPreScreening = $boolIsUseForPreScreening;
    }

    public function getSelectionModeTypeId() {
        return $this->m_intSelectionModeTypeId;
    }

    public function setSelectionModeTypeId( $intSelectionModeTypeId ) {
        $this->m_intSelectionModeTypeId = $intSelectionModeTypeId;
    }

    public function getScreeningDataProviderId() {
        return $this->m_intScreeningDataProviderId;
    }

    public function setScreeningDataProviderId( $intScreeningDataProviderId ) {
        $this->m_intScreeningDataProviderId = $intScreeningDataProviderId;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
                break;

            default:
            $boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

        parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['name'] ) ) $this->setName( $arrmixValues['name'] );
        if( true == isset( $arrmixValues['is_use_for_prescreening'] ) ) $this->setIsUseForPreScreening( CStrings::strToBool( $arrmixValues['is_use_for_prescreening'] ) );
        if( true == isset( $arrmixValues['selection_mode_type_id'] ) ) $this->setSelectionModeTypeId( $arrmixValues['selection_mode_type_id'] );
        if( true == isset( $arrmixValues['screening_data_provider_id'] ) ) $this->setScreeningDataProviderId( $arrmixValues['screening_data_provider_id'] );
        return;
    }

}
?>