<?php

class CDelinquentResident extends CBaseDelinquentResident {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

    public function valNameFirst() {
        $boolIsValid = true;
        if( true == is_null( $this->getNameFirst() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First Name is missing.' ) );
        }

        return $boolIsValid;
    }

    public function valNameLast() {
        $boolIsValid = true;
        if( true == is_null( $this->getNameLast() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last Name is missing.' ) );
        }
        return $boolIsValid;
    }

    public function valTaxNumberEncrypted() {
        $boolIsValid = true;
        if( true == is_null( $this->getTaxNumberEncrypted() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_encrypted', 'SSN is missing.' ) );
        }
        return $boolIsValid;
    }

    public function valBirthDateEncrypted() {
        $boolIsValid = true;
        if( true == is_null( $this->getBirthDateEncrypted() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date_encrypted', 'Birth date is missing.' ) );
        }
        return $boolIsValid;
    }

    public function validatefetchCriateria() {
        $boolIsValid = true;
        $boolIsValid &= $this->valNameFirst();
        $boolIsValid &= $this->valNameLast();
        $boolIsValid &= $this->valTaxNumberEncrypted();
        $boolIsValid &= $this->valBirthDateEncrypted();
        return $boolIsValid;
    }

}
?>