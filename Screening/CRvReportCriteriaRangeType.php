<?php

class CRvReportCriteriaRangeType extends CBaseRvReportCriteriaRangeType {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningConfigPreferenceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>