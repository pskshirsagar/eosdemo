<?php
if( !defined( 'PATH_PHP_EOS' ) ) define( 'PATH_PHP_EOS', str_replace( '\\', '/', dirname( __FILE__ ) . '/' ) );

define( 'PATH_EOS_ADMIN',         				PATH_PHP_EOS . 'Admin/' );
define( 'PATH_EOS_CHATS',	 					PATH_PHP_EOS . 'Chats/' );
define( 'PATH_EOS_CONNECT',	 					PATH_PHP_EOS . 'Connect/' );
define( 'PATH_EOS_DEFINES',       				PATH_PHP_EOS . 'Defines/' );
define( 'PATH_EOS_DEPLOY',       				PATH_PHP_EOS . 'Deploy/' );
define( 'PATH_EOS_EMAIL',	 					PATH_PHP_EOS . 'Email/' );
define( 'PATH_EOS_FREESWITCH',	 				PATH_PHP_EOS . 'Freeswitch/' );
define( 'PATH_EOS_INSURANCE',	 				PATH_PHP_EOS . 'Insurance/' );
define( 'PATH_EOS_LOGS',	 					PATH_PHP_EOS . 'Logs/' );
define( 'PATH_EOS_MARKETING',	 				PATH_PHP_EOS . 'Marketing/' );
define( 'PATH_EOS_PAYMENT',        				PATH_PHP_EOS . 'Payment/' );
define( 'PATH_EOS_SHARED',	 					PATH_PHP_EOS . 'Shared/' );
define( 'PATH_EOS_SMS',	 						PATH_PHP_EOS . 'Sms/' );
define( 'PATH_EOS_UTILITIES',	 				PATH_PHP_EOS . 'Utilities/' );
define( 'PATH_EOS_VOIP',	 					PATH_PHP_EOS . 'Voip/' );
define( 'PATH_EOS_VENDOR',	 					PATH_PHP_EOS . 'Vendor/' );
define( 'PATH_EOS_ENTRATA', 					PATH_PHP_EOS . 'Entrata/' );
define( 'PATH_EOS_CUSTOM_TEMPLATE', 			PATH_PHP_EOS . 'CustomTemplate/' );
define( 'PATH_EOS_TEST_CASE', 					PATH_PHP_EOS . 'TestCase/' );
?>