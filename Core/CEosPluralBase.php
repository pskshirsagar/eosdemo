<?php

defined( 'CONFIG_CACHE_ENABLED' ) || define( 'CONFIG_CACHE_ENABLED', false );

class CEosPluralBase {

	use \Psi\Libraries\Container\TContainerized;

	const DEFAULT_CACHE_LIFETIME = 14400;

	public static function fetchObjects( $strSql, $mixClassName, CDatabase $objDatabase, $boolIsReturnKeyedArray = true ) {
		$arrobjTargetObjects	= NULL;
		$objTargetObject		= NULL;

		$objDataset = $objDatabase->getReplicaDatabase()->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		$boolIsCallable = valArr( $mixClassName ) && is_callable( $mixClassName );

		$fltStartTime	= microtime( true );
		$intStartMemory = memory_get_usage();

		if( 0 < $objDataset->getRecordCount() ) {
			$arrobjTargetObjects = [];

			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();

				if( $boolIsCallable ) {
					$objTargetObject = $mixClassName[0]::{$mixClassName[1]}( $arrmixValues );
				} else {
					$objTargetObject = new $mixClassName();
				}
				$objTargetObject->setInitialized( false );
				$objTargetObject->setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );
				$objTargetObject->setDatabase( $objDatabase );

				if( true == $objTargetObject->getAllowDifferentialUpdate() ) {
					$objTargetObject->setSerializedOriginalValues( serialize( $arrmixValues ) );
				}

				if( false !== $boolIsReturnKeyedArray && false == is_null( $objTargetObject->getId() ) ) {
					$arrobjTargetObjects[$objTargetObject->getId()] = $objTargetObject;
				} else {
					$arrobjTargetObjects[] = $objTargetObject;
				}

				$objDataset->next();
			}
		}

		if( true == checkDisplayDbInfoProcess() ) {
			CDisplayDebugInfo::getObject()->storeDebugInfo( [ 'memory_usage' => round( ( ( memory_get_usage() - $intStartMemory ) / 1024 ) / 1024, 3 ), 'fetch_objects_time' => ( microtime( true ) - $fltStartTime ) ] );
		}

		$objDataset->cleanup();
		return $arrobjTargetObjects;
	}

	public static function fetchObject( $strSql, $strClassName, CDatabase $objDatabase ) {
		$objTargetObject		= NULL;
		$arrobjTargetObjects	= self::fetchObjects( $strSql, $strClassName, $objDatabase );

		if( true == valArr( $arrobjTargetObjects ) ) {
			if( 1 < \Psi\Libraries\UtilFunctions\count( $arrobjTargetObjects ) ) {
				trigger_error( 'Expecting a single record when multiple records returned. Sql = ' . $strSql, E_USER_WARNING );
				return NULL;
			}

			$objTargetObject = array_shift( $arrobjTargetObjects );
		}

		return $objTargetObject;
	}

	public static function fetchColumn( $strSql, $strColumnName, CDatabase $objDatabase ) {
		$mixResult	= NULL;

		$objDataset = $objDatabase->getReplicaDatabase()->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		$intStartMemory = memory_get_usage();

		if( 0 < $objDataset->getRecordCount() ) {
			$arrValues = $objDataset->fetchArray();
			if( array_key_exists( $strColumnName, $arrValues ) ) {
				$mixResult = $arrValues[$strColumnName];
			}
		}

		if( true == checkDisplayDbInfoProcess() ) {
			storeDbInfo( [ 'memory_usage' => round( ( ( memory_get_usage() - $intStartMemory ) / 1024 ) / 1024, 3 ) ] );
		}

		$objDataset->cleanup();
		return $mixResult;
	}

	public static function fetchRowCount( $strWhere = NULL, $strFrom, CDatabase $objDatabase ) {
		$intCount	= 0;
		$objDataset = $objDatabase->getReplicaDatabase()->createDataset();
		$strSql		= sprintf( 'SELECT count( 1 ) AS count FROM %s %s', $strFrom, $strWhere );

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		$intStartMemory = memory_get_usage();

		if( 0 < $objDataset->getRecordCount() ) {
			$arrValues	= $objDataset->fetchArray();
			$intCount	= ( int ) $arrValues['count'];
		}

		if( true == checkDisplayDbInfoProcess() ) {
			storeDbInfo( [ 'memory_usage' => round( ( ( memory_get_usage() - $intStartMemory ) / 1024 ) / 1024, 3 ) ] );
		}

		$objDataset->cleanup();
		return $intCount;
	}

	public static function fetchCachedObjects( $strSql, $strClassName, CDatabase $objDatabase, $strCacheName, $intSpecificLifetime = false, $boolIsAppendDatabaseId = true, $boolIsReturnKeyedArray = true, $boolAddTag = false ) {
		$strArrayType = ( true == $boolIsReturnKeyedArray ? 'keyed' : 'simple' );

		if( true == $boolIsAppendDatabaseId ) {
			$strKey = 'arr_' . $strClassName . '_' . $objDatabase->getId() . '_' . sha1( $strSql ) . '_' . $strArrayType;
			$strParentKey = 'Eos' . $strClassName . '_' . $objDatabase->getId();
		} else {
			$strKey = 'arr_' . $strClassName . '_' . sha1( $strSql ) . '_' . $strArrayType;
			$strParentKey = 'Eos' . $strClassName;
		}

		$strLocaleCode = CLocaleContainer::createService()->getLocaleCode();
		if( false == empty( $strLocaleCode ) && CLocale::DEFAULT_LOCALE != $strLocaleCode ) {
			$strKey .= '_' . $strLocaleCode;
		}

		$fltStartTime = microtime( true );

		if( false == CONFIG_CACHE_ENABLED || false === ( $objData = CCache::fetchHashObject( $strParentKey, $strKey, $strCacheName ) ) ) {
			$objData = self::fetchObjects( $strSql, $strClassName, $objDatabase, $boolIsReturnKeyedArray );

			if( true == CONFIG_CACHE_ENABLED && false == empty( $objData ) ) {
				if( NULL === $intSpecificLifetime || false === $intSpecificLifetime ) $intSpecificLifetime = self::DEFAULT_CACHE_LIFETIME;
				CCache::storeHashObject( $strParentKey, $strKey, $objData, $intSpecificLifetime, $strCacheName );
			}
		} else {
			if( true == checkDisplayDbInfoProcess() ) {
				$intLastOffset = CDisplayDebugInfo::getObject()->storeDebugInfo( [
					'start_time' => $fltStartTime,
				] );

				CDisplayDebugInfo::getObject()->storeDebugInfo( [
					'query' => CDisplayDebugInfo::getObject()->getDbInfo()[$intLastOffset]['query'] . "\r\n" . preg_replace( '/([^\;])$/', '$1;', trim( $strSql ) )
				], $intLastOffset );

				CDisplayDebugInfo::getObject()->logBacktrace();
			}
		}

		return $objData;
	}

	public static function fetchCachedObject( $strSql, $strClassName, CDatabase $objDatabase, $strCacheName, $intSpecificLifetime = false, $boolIsAppendDatabaseId = true, $boolAddTag = false ) {
		if( true == $boolIsAppendDatabaseId ) {
			$strKey = 'arr_' . $strClassName . '_' . $objDatabase->getId() . '_' . $strSql;
			$strParentKey = 'Eos' . $strClassName . '_' . $objDatabase->getId();
		} else {
			$strKey = 'arr_' . $strClassName . '_' . $strSql;
			$strParentKey = 'Eos' . $strClassName;
		}

		$strLocaleCode = CLocaleContainer::createService()->getLocaleCode();
		if( false == empty( $strLocaleCode ) && CLocale::DEFAULT_LOCALE != $strLocaleCode ) {
			$strKey .= '_' . $strLocaleCode;
		}

		if( false == CONFIG_CACHE_ENABLED || false === ( $objData = CCache::fetchHashObject( $strParentKey, $strKey, $strCacheName ) ) ) {
			$objData = self::fetchObject( $strSql, $strClassName, $objDatabase );

			if( true == CONFIG_CACHE_ENABLED && false == empty( $objData ) ) {
				if( NULL === $intSpecificLifetime || false === $intSpecificLifetime ) $intSpecificLifetime = self::DEFAULT_CACHE_LIFETIME;
				CCache::storeHashObject( $strParentKey, $strKey, $objData, $intSpecificLifetime, $strCacheName );
			}
		}

		return $objData;
	}

	public static function bulkInsert( $arrobjTargetObjects, $intCurrentUserId, $objDatabase, $boolIsReturnSqlOnly = false, $boolIsReturnIds = false ) {
		if( false == valArr( $arrobjTargetObjects ) ) {
			return false;
		}

		$intCount			= 0;
		$strInsertFields	= '';
		$strInsertValues	= '';
		$boolIsReturnIds	= false;

		foreach( $arrobjTargetObjects as $objTargetObject ) {
			$objTargetObject->setDatabase( $objDatabase );
			$strSql = $objTargetObject->insert( $intCurrentUserId, $objDatabase, true );

			if( false == $strSql ) {
				return false;
			}

			if( 0 == $intCount ) {
				$intEndPos = \Psi\CStringService::singleton()->strpos( $strSql, 'VALUES' );
				$strInsertFields = \Psi\CStringService::singleton()->substr( $strSql, 0, $intEndPos ) . 'VALUES ';
				$intInsertFieldsLength = strlen( $strInsertFields ) - 1;// Do not include space after VALUES for calculating the length.

				if( false != \Psi\CStringService::singleton()->strstr( $strSql, 'RETURNING id;' ) ) {
					$boolIsReturnIds = true;
				}
			}

			if( false != valStr( $strInsertValues ) ) {
				$strInsertValues .= ', ' . trim( \Psi\CStringService::singleton()->substr( $strSql, $intInsertFieldsLength, \Psi\CStringService::singleton()->strrpos( $strSql, ')' ) - ( $intInsertFieldsLength - 1 ) ) );
			} else {
				$strInsertValues = trim( \Psi\CStringService::singleton()->substr( $strSql, $intInsertFieldsLength, \Psi\CStringService::singleton()->strrpos( $strSql, ')' ) - ( $intInsertFieldsLength - 1 ) ) );
			}

			$intCount++;
		}

		$strSql = $strInsertFields . $strInsertValues;

		if( false != $boolIsReturnIds ) {
			$strSql .= ' RETURNING id;';
		} else {
			$strSql .= ';';
		}

		if( true == $boolIsReturnSqlOnly ) {
			return $strSql;
		}

		$objDataset = $objDatabase->getMasterDatabase()->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return false;
		} else {
			if( true == $boolIsReturnIds ) {
				$arrobjTargetObjects = array_values( $arrobjTargetObjects );

				if( 0 < $objDataset->getRecordCount() ) {
					$intIndex = 0;

					while( false == $objDataset->eof() ) {
						$arrmixValues = $objDataset->fetchArray();
						if ( true == isset ( $arrmixValues['id'] ) ) $arrobjTargetObjects[$intIndex++]->setId( ( double ) $arrmixValues['id'] );
						$objDataset->next();
					}
				}
			}

			$objDataset->cleanup();
			return true;
		}
	}

	public static function bulkDelete( $arrobjTargetObjects, $objDatabase, $boolIsReturnSqlOnly = false ) {
		if( false == valArr( $arrobjTargetObjects ) ) {
			return false;
		}

		$strTableName			= \Psi\CStringService::singleton()->substr( \Psi\Libraries\UtilInflector\CInflector::createService()->underscore( get_called_class() ), 2 );
		$strWhereClause			= '';
		$strUsingClauseWithCid 	= '';
		$strSql 				= 'DELETE FROM public.' . $strTableName;

		foreach( $arrobjTargetObjects as $objTargetObject ) {
			$objTargetObject->setDatabase( $objDatabase );
			if( true == method_exists( $objTargetObject, 'getCid' ) && false == is_null( $objTargetObject->getCid() ) ) {
				$strUsingClauseWithCid .= '( ' . ( double ) $objTargetObject->sqlId() . ', ' . ( int ) $objTargetObject->sqlCid() . ' ), ';
			} else {
				$strWhereClause .= ( double ) $objTargetObject->sqlId() . ', ';
			}
		}

		if( true == valStr( $strUsingClauseWithCid ) ) {
			$strSql .= ' USING ( VALUES ' . trim( $strUsingClauseWithCid, ', ' ) . ' ) AS composite_values ( composite_id, composite_cid )';
		}

		$strSql .= ' WHERE ';

		if( true == valStr( $strUsingClauseWithCid ) ) $strSql .= '( id = composite_values.composite_id AND cid = composite_values.composite_cid ) ';

		if( true == valStr( $strWhereClause ) ) {
			if( true == valStr( $strUsingClauseWithCid ) ) $strSql .= ' OR ';

			$strSql .= ' id IN ( ' . trim( $strWhereClause, ', ' ) . ' ) ';
		}

		$strSql .= ';';

		if( true == $boolIsReturnSqlOnly ) {
			return $strSql;
		}

		$objDataset = $objDatabase->getMasterDatabase()->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return false;
		} else {
			$objDataset->cleanup();
			return true;
		}
	}

	public static function bulkUpdate( $arrobjTargetObjects, $arrmixFieldsToUpdate, $intCurrentUserId, $objDatabase, $boolIsReturnSqlOnly = false ) {
		// Not sure if returning false is good idea? as validating is not failure as such in this case! as while calling we need check if its array if we are validating return from here it may mislead.
		if( false == valArr( $arrobjTargetObjects ) ) {
			return false;
		}

		$strSql = '';
		// Determine table name
		$strTableName = \Psi\CStringService::singleton()->substr( \Psi\Libraries\UtilInflector\CInflector::createService()->underscore( get_called_class() ), 2 );

		// Get available fields in table using first record
		$arrobjTargetObject = reset( $arrobjTargetObjects );

		// Check if Singluar Base class has toArray() function. Newly generated classes will have it.
		if( false == method_exists( get_class( $arrobjTargetObject ), 'toArray' ) ) {
			trigger_error( get_class( $arrobjTargetObject ) . '->toArray()' . ' is required to use this bulkUpdate() function. Regenarate base class files with updated PgDictionary tool.', E_USER_WARNING );
		}

		$arrstrAvailableFields = array_keys( $arrobjTargetObject->toArray() );

		// @TODO: Check if all fields in $arrmixFieldsToUpdate exists in table

		// Check if updated_by is present in table and not passed in input parameter $arrmixFieldsToUpdate
		if( true == in_array( 'updated_by', $arrstrAvailableFields ) && false == in_array( 'updated_by', $arrmixFieldsToUpdate ) ) {
			array_push( $arrmixFieldsToUpdate, 'updated_by' );
		}

		// Check if updated_on is present in table and not passed in input parameter $arrmixFieldsToUpdate
		if( true == in_array( 'updated_on', $arrstrAvailableFields ) && false == in_array( 'updated_on', $arrmixFieldsToUpdate ) ) {
			array_push( $arrmixFieldsToUpdate, 'updated_on' );
		}

		// We should not update the created_by and/or created_on fields
		if( true == in_array( 'created_by', $arrstrAvailableFields ) || true == in_array( 'created_on', $arrstrAvailableFields ) ) {
			$arrstrAvailableFields = array_diff( $arrstrAvailableFields, [ 'created_by', 'created_on' ] );
		}

		// Sanitize required fields
		$arrmixFieldsToUpdate = array_intersect( $arrmixFieldsToUpdate, $arrstrAvailableFields );

		// Prepare UPDATE statement for each object
		foreach( $arrobjTargetObjects as $arrobjTargetObject ) {
			$arrstrFieldValues = $arrobjTargetObject->toArray();
			$arrobjTargetObject->setDatabase( $objDatabase );

			// if details field is present in table and is modified then that always should be considered to update. #i18n
			if( true == in_array( 'details', $arrstrAvailableFields ) && false == in_array( 'details', $arrmixFieldsToUpdate )
				&& true == valArr( $arrobjTargetObject->getChangedColumns() ) && true == array_key_exists( 'Details', $arrobjTargetObject->getChangedColumns() ) ) {
				array_push( $arrmixFieldsToUpdate, 'details' );
			}

			// Prepare the UPDATE sql
			$strSql .= 'UPDATE ' . $strTableName . ' SET ';

			// Loop on each field
			foreach( $arrmixFieldsToUpdate as $strFieldToUpdate ) {
				// Force updated_by as $intCurrentUserId
				if( 'updated_by' == $strFieldToUpdate ) {
					$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
					continue;
				}

				// Force updated_on as NOW()
				if( 'updated_on' == $strFieldToUpdate ) {
					$strSql .= ' updated_on = NOW(), ';
					continue;
				}

				$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strFieldToUpdate ) ) );
				$strSql .= ' ' . $strFieldToUpdate . ' = ' . ( true == is_null( $arrstrFieldValues[$strFieldToUpdate] ) ? 'NULL' : $arrobjTargetObject->$strSqlFunctionName() ) . ', ';
			}

			// Trim the ending comma
			$strSql = trim( $strSql, ', ' );

			$strSql .= ' WHERE id = ' . $arrstrFieldValues['id'];
			if( true == isset( $arrstrFieldValues['cid'] ) ) {
				$strSql .= ' AND cid = ' . $arrstrFieldValues['cid'];
			}

			$strSql .= ';' . PHP_EOL;
		}

		// Return SQL or execute it
		if( true == $boolIsReturnSqlOnly ) {
			return $strSql;
		}

		$objDataset = $objDatabase->getMasterDatabase()->createDataset();
		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return false;
		} else {
			$objDataset->cleanup();
			return true;
		}
	}

}