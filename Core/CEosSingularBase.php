<?php

/**
 *
 * @method clonePostalAddresses()
 * @see TEosPostalAddresses::clonePostalAddresses()
 *
 * @method cloneUDF()
 * @see \TEosUserDefinedFields::cloneUDF()
 */
class CEosSingularBase {

	protected $m_arrobjErrorMsgs;
	protected $m_arrobjSuccessMsgs;

	protected $m_arrstrOriginalValues;
	protected $m_arrstrChangedColumns;

	protected $m_strSerializedOriginalValues;

	protected $m_boolAllowDifferentialUpdate;
	protected $m_boolInitialized;

	protected $m_arrmixCustomVariables;

	/** @var CDatabase */
	protected $m_objDatabase;

	public function __construct() {
		$this->m_arrobjErrorMsgs       = [];
		$this->m_arrobjSuccessMsgs     = [];
		$this->m_arrmixCustomVariables = [];
		$this->m_arrstrChangedColumns	= array();

		$this->m_boolAllowDifferentialUpdate 	= false;
		$this->m_boolInitialized 				= true;

		return;
	}

	/**
	 * MAGIC FUNCTIONS
	 */

	public function __clone() {

		if( true == isset( $this->m_jsonDetails ) ) {
			$this->m_jsonDetails = unserialize( serialize( $this->m_jsonDetails ), [ 'allowed_classes' => [ 'stdClass' ] ] );
		}

		if( true == method_exists( $this, 'cloneUDF' ) ) {
			$this->cloneUDF();
		}

		if( true == method_exists( $this, 'clonePostalAddress' ) ) {
			$this->clonePostalAddresses();
		}
	}

	public function __sleep() {

		$arrstrFilterVars = array();
		$arrstrObjectVars = get_object_vars( $this );

		if( true == valArr( $arrstrObjectVars ) ) {
			foreach( $arrstrObjectVars as $strVarName => $strVarValue ) {
				if( false == is_null( $strVarValue ) ) {
					$arrstrFilterVars[] = $strVarName;
				}
			}
		}

		return $arrstrFilterVars;
	}

	/**
	 * ADD FUNCTIONS
	 */

	public function addErrorMsg( $objErrorMsg ) {
		if( ( true == valObj( $objErrorMsg, CErrorMsg::class ) || true == valObj( $objErrorMsg, \Psi\Libraries\UtilErrorMsg\CErrorMsg::class ) ) && true == in_array( $objErrorMsg->getType(), [ ERROR_TYPE_GENERAL, ERROR_TYPE_DATABASE ] ) ) {
            $this->throwException( $objErrorMsg );
        }
		$this->m_arrobjErrorMsgs[] = $objErrorMsg;
	}

	public function addErrorMsgs( $arrobjErrorMsgs ) {
		if( true == valArr( $arrobjErrorMsgs ) ) {
			foreach( $arrobjErrorMsgs as $objErrorMsg ) {
			    $this->addErrorMsg( $objErrorMsg );
            }
		}
	}

	public function addSuccessMsg( $objSuccessMsg ) {
		$this->m_arrobjSuccessMsgs[] = $objSuccessMsg;
	}

	/**
	 * GET FUNCTIONS
	 */

	public function getErrorMsgs() {
		return $this->m_arrobjErrorMsgs;
	}

	public function getSuccessMsgs() {
		return $this->m_arrobjSuccessMsgs;
	}

	public function getOriginalValueByFieldName( $strFieldName ) {
		return ( true == isset ( $this->m_arrstrOriginalValues[$strFieldName] ) ) ? $this->m_arrstrOriginalValues[$strFieldName] : NULL;
	}

	public function getSerializedOriginalValues() {
		return $this->m_strSerializedOriginalValues;
	}

	public function getAllowDifferentialUpdate() {
		return $this->m_boolAllowDifferentialUpdate;
	}

	public function getCustomVariableByKey( $strKey ) {
		return ( true == array_key_exists( $strKey, $this->m_arrmixCustomVariables ) ) ? $this->m_arrmixCustomVariables[$strKey] : NULL;
	}

	public function getDatabase() {
		return $this->m_objDatabase;
	}

	public function getChangedColumns() {
		return $this->m_arrstrChangedColumns;
	}

	public function isObjectModified() {
		return \valArr( $this->m_arrstrChangedColumns );
	}

	/**
	 * SET FUNCTIONS
	 */

	public function setOriginalValues( $arrstrOriginalValues ) {
		$this->m_arrstrOriginalValues = $arrstrOriginalValues;
	}

	public function setSerializedOriginalValues( $strOriginalValues ) {
		$this->m_strSerializedOriginalValues = $strOriginalValues;
	}

	public function setAllowDifferentialUpdate( $boolAllowDifferentialUpdate ) {
		$this->m_boolAllowDifferentialUpdate = $boolAllowDifferentialUpdate;
	}

	public function setDatabase( $objDatabase ) {
		$this->m_objDatabase = $objDatabase;
	}

	public function setInitialized( $boolInitialized ) {
		$this->m_boolInitialized = ( bool ) $boolInitialized;
	}

	public function addChangedColumn( $strColumnName ) {
		// Stripping data type from member variable of Base class.
		$strColumnName = preg_replace( '/^[a-z_]*([A-Za-z0-9]*).*$/', '$1', $strColumnName );
		$this->m_arrstrChangedColumns[$strColumnName] = true;
	}

	public function resetChangedColumns() {
		$this->m_arrstrChangedColumns = array();
	}

	final protected function set( $strName, $mixValue, $boolForce = false ) {
		if( $boolForce || $mixValue !== ( $this->$strName ?? NULL ) ) {
			$this->$strName = $mixValue;
			if( true == $this->m_boolInitialized ) {
				$this->addChangedColumn( $strName );
			}
		}
	}

	/**
	 * OTHER FUNCTIONS
	 */

	public function unSerializeAndSetOriginalValues() {
		$this->m_arrstrOriginalValues = unserialize( $this->m_strSerializedOriginalValues );
	}

	public function reSerializeAndSetOriginalValues( $arrstrOriginalValues ) {
		if( true == valArr( $arrstrOriginalValues ) ) {
			foreach( $arrstrOriginalValues as $strFieldName => $strValue ) {
				$this->m_arrstrOriginalValues[$strFieldName] = CStrings::reverseSqlFormat( $strValue );
			}
		}

		$this->m_strSerializedOriginalValues = serialize( $this->m_arrstrOriginalValues );
		unset( $this->m_arrstrOriginalValues );
	}

	public function applyRequestForm( $arrmixRequestForm, $arrmixFormFields = NULL ) {
		if( true == valArr( $arrmixFormFields ) ) {
			if( false == $this->m_boolAllowDifferentialUpdate ) {
				$arrmixRequestForm = mergeIntersectArray( $arrmixFormFields, $arrmixRequestForm );
			} else {
				if( false == valArr( $arrmixRequestForm ) || false == valArr( $arrmixFormFields ) ) {
					$arrmixRequestForm = [];
				}
				$arrmixRequestForm = array_intersect_key( $arrmixRequestForm, $arrmixFormFields );
			}
		}

		$this->setValues( $arrmixRequestForm, $boolStripSlashes = false );
		return;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function executeSql( $strSql, $objBaseTarget, $objDatabase ) {
		$strType = NULL;

		if( true == \Psi\CStringService::singleton()->stristr( \Psi\CStringService::singleton()->strtolower( $strSql ), 'insert' ) ) {
			$strType = 'insert';
		} else {
			if( true == \Psi\CStringService::singleton()->stristr( \Psi\CStringService::singleton()->strtolower( $strSql ), 'update' ) ) {
				$strType = 'update';
			} else {
				if( true == \Psi\CStringService::singleton()->stristr( \Psi\CStringService::singleton()->strtolower( $strSql ), 'delete' ) ) {
					$strType = 'delete';
				}
			}
		}

		switch( $strType ) {
			case 'insert':
			case 'update':
			case 'delete':
				$objDatabase->logWriteQueriesCount();
				break;

			default:
				// For other SQL operation, system will not check permissions.
				break;
		}

		$objDataset = $objDatabase->getMasterDatabase()->createDataset();

		if( false == $objDataset->execute( $strSql, true ) ) {
			$objBaseTarget->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to ' . $strType . ' ' . get_class( $objBaseTarget ) . ' record. The following error was reported.' ) );
			$objBaseTarget->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;

		} else {
			// On insert, we need to catch the id and assign it to the variable.
			if( 0 < $objDataset->getRecordCount() ) {
				while( false == $objDataset->eof() ) {
					$arrmixValues = $objDataset->fetchArray();
					if( true == isset ( $arrmixValues['id'] ) ) {
						$objBaseTarget->setId( ( double ) $arrmixValues['id'] );
					}
					$objDataset->next();
				}
			}
		}

		if( true == $this->isStoredProc( $strSql ) ) {
			if( 0 < $objDataset->getRecordCount() ) {
				$objBaseTarget->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to ' . $strType . ' ' . get_class( $objBaseTarget ) . ' record. The following error was reported.' ) );
				while( false == $objDataset->eof() ) {
					$arrmixValues = $objDataset->fetchArray();
					$objBaseTarget->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
					$objDataset->next();
				}

				$objDataset->cleanup();

				return false;
			}
		}

		$objDataset->cleanup();

		return true;
	}

	public function isStoredProc( $strSql ) {
		return ( true == in_array( \Psi\CStringService::singleton()->substr( \Psi\CStringService::singleton()->strtolower( $strSql ), 0, 6 ), [ 'insert', 'update', 'delete' ] ) ) ? false : true;
	}

	public function refreshField( $strFieldName, $strTableName, $objDatabase ) {
		if( true == method_exists( $this, 'getCid' ) ) {
			$arrstrData = fetchData( sprintf( 'SELECT %s FROM %s WHERE id = %d AND cid = %d', $strFieldName, $strTableName, $this->getId(), $this->getCid() ), $objDatabase );
		} else {
			$arrstrData = fetchData( sprintf( 'SELECT %s FROM %s WHERE id = %d', $strFieldName, $strTableName, $this->getId() ), $objDatabase );
		}

		$strMethodName = 'set' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strFieldName ) ) );

		if( true == isset( $arrstrData[0][$strFieldName] ) && true == method_exists( $this, $strMethodName ) ) {
			$this->{$strMethodName}( $arrstrData[0][$strFieldName] );
		}
	}

	public function fetchNextId( $objDatabase ) {

		$intArgumentCount = func_num_args();

		if( 2 == $intArgumentCount ) {
			$strSequenceName = func_get_arg( 0 );
			$objDatabase     = func_get_arg( 1 );
		} else {
			$strSequenceName = $this->getSequenceName();
		}

		$objDataset = $objDatabase->getMasterDatabase()->createDataset();

		$strSql = "SELECT nextval('" . $strSequenceName . "'::text) AS id";

		if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to get next ' . $strSequenceName . ' value. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		$arrmixValues = $objDataset->fetchArray();
		$intId        = $arrmixValues['id'];

		$objDataset->cleanup();

		$this->setId( $intId );

		return $intId;
	}

	public function getArrayFromObject() {
		$arrmixResult = [];

		$objObject = get_object_vars( $this );

		foreach( $objObject as $strKey => $mixValue ) {
			if( 'object' == gettype( $mixValue ) ) {
				$arrmixResult[get_class( $mixValue )] = $mixValue->getArrayFromObject();
			} else {
				$arrmixResult[$strKey] = $mixValue;
			}
		}

		return $arrmixResult;
	}

	public function downCast( $strClassName ) {

		$arrstrParentClasses = class_parents( $strClassName );

		if( false == in_array( get_class( $this ), $arrstrParentClasses ) ) {
			throw new InvalidArgumentException( 'argument 1 must be an object from ' . $strClassName . '\' inheritance hierarchy' );

			return false;
		}

		$objObject = new $strClassName;

		foreach( $this as $strVarName => $strValue ) {
			$strSetFunction = 'set' . ( preg_replace( '/m_[^A-Z]+/', '', $strVarName ) );

			if( false == method_exists( $objObject, $strSetFunction ) ) {
				continue;
			}

			$objObject->$strSetFunction( $strValue );
		}

		return $objObject;
	}

	/**
	 * If you are making any changes in assignSmartyConstants function then
	 * please make sure the same changes would be applied to assignTemplateConstants function also.
	 */

	public static function assignSmartyConstants( $objSmarty ) {

		$objReflectionClass        = new ReflectionClass( get_called_class() );
		$strConstantPrefix         = \Psi\CStringService::singleton()->strtoupper( preg_replace( '/([a-z\d])([A-Z])/', '$1_$2', preg_replace( '/^C/', '', get_called_class() ) ) );
		$arrstrConstantPrefix      = explode( '_', $strConstantPrefix );
		$strNumricalConstantPrefix = end( $arrstrConstantPrefix ) . '_';
		$arrstrClassConstants      = $objReflectionClass->getConstants();
		$boolIsNewConstant         = false;

		foreach( $arrstrClassConstants as $strConstantName => $mixConstantValue ) {
			if( \Psi\CStringService::singleton()->substr( $strConstantName, 0, strlen( $strConstantPrefix ) ) !== $strConstantPrefix ) {

				$boolIsNewConstant = true;
				if( \Psi\CStringService::singleton()->substr( $strConstantName, 0, strlen( $strNumricalConstantPrefix ) ) == $strNumricalConstantPrefix ) {

					$strConstantName = \Psi\CStringService::singleton()->substr( $strConstantName, strlen( $strNumricalConstantPrefix ) );
				}
			} else {

				$boolIsNewConstant = false;
			}

			if( true == $boolIsNewConstant ) {

				$strConstantName = $strConstantPrefix . '_' . $strConstantName;
			}

			$objSmarty->assign( $strConstantName, $mixConstantValue );
		}

	}

	public static function assignTemplateConstants( $arrmixTemplateParameters ) {

		$objReflectionClass        = new ReflectionClass( get_called_class() );
		$strConstantPrefix         = \Psi\CStringService::singleton()->strtoupper( preg_replace( '/([a-z\d])([A-Z])/', '$1_$2', preg_replace( '/^C/', '', get_called_class() ) ) );
		$arrstrConstantPrefix      = explode( '_', $strConstantPrefix );
		$strNumricalConstantPrefix = end( $arrstrConstantPrefix ) . '_';
		$arrstrClassConstants      = $objReflectionClass->getConstants();
		$boolIsNewConstant         = false;

		foreach( $arrstrClassConstants as $strConstantName => $mixConstantValue ) {
			if( \Psi\CStringService::singleton()->substr( $strConstantName, 0, strlen( $strConstantPrefix ) ) !== $strConstantPrefix ) {

				$boolIsNewConstant = true;
				if( \Psi\CStringService::singleton()->substr( $strConstantName, 0, strlen( $strNumricalConstantPrefix ) ) == $strNumricalConstantPrefix ) {

					$strConstantName = \Psi\CStringService::singleton()->substr( $strConstantName, strlen( $strNumricalConstantPrefix ) );
				}
			} else {

				$boolIsNewConstant = false;
			}

			if( true == $boolIsNewConstant ) {
				$strConstantName = $strConstantPrefix . '_' . $strConstantName;
			}

			$arrmixTemplateParameters[$strConstantName] = $mixConstantValue;
		}

		return $arrmixTemplateParameters;
	}

	/**
	 * Returns the constants declared in Custom Singular classes
	 * @return string[]
	 */

	public static function getConstants() {
		$objReflectionClass = new ReflectionClass( get_called_class() );

		return $objReflectionClass->getConstants();
	}

	/**
	 * Function to get type name from type id. Like CListType::NO_SHOW will return 'No Show'
	 * Note : This will not work if there are two constants with different names but same value.
	 * @param integer $intTypeId
	 * @deprecated This is NOT i18n compliant. Please discontinue use and refactor EOS classes to use TEosTypes trait
	 */

	public static function getTypeNameByTypeId( $intTypeId ) {

		$arrmixClassConstants = self::getConstants();

		// Flip the array for fast returning by key
		$arrmixClassConstants = array_flip( $arrmixClassConstants );

		$strConstantName = $arrmixClassConstants[$intTypeId];

		$strConstantName = \Psi\Libraries\UtilInflector\CInflector::createService()->humanize( \Psi\CStringService::singleton()->strtolower( $strConstantName ), 'all' );

		return $strConstantName;
	}

	/**
	 * Function to get type names from type ids. Like
	 * array( CListType::NO_SHOW, CListType::LEAD_CANCELLATION, CListType::LEAD_DENIAL_REASONS ) will return
	 * array( [3] => No Show, [6] => Lead Cancellation, [7] => Lead Denial Reasons )
	 * Note : This will not work if there are two constants with different names but same value.
	 * @param array $intTypeIds
	 * @deprecated This is NOT i18n compliant. Please discontinue use and refactor EOS classes to use TEosTypes trait
	 */

	public static function getTypeNamesByTypeIds( $arrintTypeIds ) {

		if( false == valArr( $arrintTypeIds ) ) {
			return NULL;
		}

		$arrmixTypeNames = [];

		$arrmixClassConstants = self::getConstants();

		// Flip the array for fast returning by key
		$arrmixClassConstants = array_flip( $arrmixClassConstants );

		foreach( $arrintTypeIds as $intTypeId ) {
			$strConstantName             = $arrmixClassConstants[$intTypeId];
			$arrmixTypeNames[$intTypeId] = \Psi\Libraries\UtilInflector\CInflector::createService()->humanize( \Psi\CStringService::singleton()->strtolower( $strConstantName ), 'all' );
		}

		return $arrmixTypeNames;
	}

	public function getSequenceName() {
		return static::TABLE_NAME . '_id_seq';
	}

    protected function throwException( $objErrorMsg ) {
        $objContainer = \Psi\Libraries\Container\CDependencyContainer::getInstance()->getContainer();
        if( true == isset( $objContainer['eos_throw_exceptions'] ) && $objContainer['eos_throw_exceptions'] ) {
            throw new Exception( $objErrorMsg->getMessage() );
        }
    }
}