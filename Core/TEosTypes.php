<?php
trait TEosTypes {

	protected $m_arrobjTypes;

	/**
	 * Fetch type objects for table that matches $this class (or overridden class name)
	 * @param CDatabase $objDatabase
	 * @param string $strClassName | Optional overriding class name
	 */
	private function getOrFetchTypes( CDatabase $objDatabase, $strClassName = NULL ) {
		if( valArr( $this->m_arrobjTypes ) )
			return $this->m_arrobjTypes;

		$strClassName = preg_replace( '/^(C.+?)s$/', '$1', $strClassName ?? get_class( $this ) );
		$strSql = sprintf( 'SELECT * FROM %s', $strClassName::TABLE_NAME );
		$this->m_arrobjTypes = CEosPluralBase::fetchObjects( $strSql, $strClassName, $objDatabase );
		return $this->m_arrobjTypes;
	}

	/**
	 * Get type name from type id. E.g.
	 *  CListType::NO_SHOW will return 'No Show' (for en_US locale)
	 * @param integer $intTypeId
	 */
	public function getTypeByTypeId( $intTypeId, CDatabase $objDatabase ) {
		$arrobjTypes = $this->getOrFetchTypes( $objDatabase );
		return $arrobjTypes[$intTypeId];
	}

	/**
	 * Function to get type names from type ids. E.g.
	 *  [ CListType::NO_SHOW, CListType::LEAD_CANCELLATION, CListType::LEAD_DENIAL_REASONS ) ] would return
	 *  [ [3] => No Show, [6] => Lead Cancellation, [7] => Lead Denial Reasons ] (for en_US locale)
	 * @param array $arrintTypeIds
	 */
	public function getTypesByTypeIds( $arrintTypeIds, CDatabase $objDatabase ) {
		$arrobjTypes = $this->getOrFetchTypes( $objDatabase );
		foreach( $arrintTypeIds as $intTypeId ) {
			$arrobjTypesFiltered[$intTypeId] = $arrobjTypes[$intTypeId];
		}
		return $arrobjTypesFiltered;
	}

}
?>