<?php

class CMasterPolicyCoverage extends CBaseMasterPolicyCoverage {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMasterPolicyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsuranceExportBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMasterPolicyBillingBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsuranceExportTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMasterPolicyCoverageId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMasterPolicyRecordTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProviderPremiumAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalPremiumAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdminFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPolicyFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseTax() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStampingTax() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOtherTax() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $objClientDatabase = NULL ) {

		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $objClientDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $objClientDatabase );
		}
	}

}
?>