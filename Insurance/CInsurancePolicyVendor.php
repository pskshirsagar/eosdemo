<?php

class CInsurancePolicyVendor extends CBaseInsurancePolicyVendor {

	const RX_CARD_VENDOR = 1;

    public function validate( $strAction ) {
    	$boolIsValid = true;

    	switch( $strAction ) {
    		case VALIDATE_INSERT:
    		case VALIDATE_UPDATE:
    		case VALIDATE_DELETE:
    			break;

    		default:
    			$boolIsValid = false;
    	}

    	return $boolIsValid;
    }
}
?>