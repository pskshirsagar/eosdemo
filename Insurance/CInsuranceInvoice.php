<?php

class CInsuranceInvoice extends CBaseInsuranceInvoice {

	protected $m_strInvoiceFileName;

	public function valInsuranceCarrierId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getInsuranceCarrierId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Insurance Carrier is required. ', NULL ) );
		}

		return $boolIsValid;
	}

	public function valInvoiceDate() {

		$boolIsValid = true;

		if( true == is_null( $this->getInvoiceDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Invoice date is required.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valInvoiceDueDate() {

		$boolIsValid = true;

		if( true == is_null( $this->getInvoiceDueDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Invoice due date is required.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valInvoiceDueAmount() {

		$boolIsValid = true;

		if( true == is_null( $this->getInvoiceDueAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Invoice due Amount is required.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valCarrierInvoiceNumber() {

		$boolIsValid = true;

		if( true == is_null( $this->getCarrierInvoiceNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Carrier invoice number is required. ', NULL ) );
		}
		return $boolIsValid;
	}

	public function valInsuranceInvoiceExist( $strCarrierInvoiceNumber, $objInsuranceDatabase ) {

		$boolIsValid = true;

		// Check for the active invoice number
		$intCountInsuranceInvoice = \Psi\Eos\Insurance\CInsuranceInvoices::createService()->fetchActiveInsuranceInvoiceCountByCarrierInvoiceNumber( $strCarrierInvoiceNumber, $objInsuranceDatabase );

		if( 0 < $intCountInsuranceInvoice ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Carrier invoice number is already exist. ', NULL ) );
		}

		return $boolIsValid;
	}

	public function valInvoiceFileName() {
		$boolIsValid = true;

		if( true == is_null( $this->getInvoiceFileName() ) || 0 == strlen( $this->getInvoiceFileName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Select Insurance Invoice File.', NULL ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $strCarrierInvoiceNumber = NULL, $objInsuranceDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_create_invoice':
				$boolIsValid &= $this->valInsuranceCarrierId();
				$boolIsValid &= $this->valInvoiceDate();
				$boolIsValid &= $this->valInvoiceDueDate();
				$boolIsValid &= $this->valInvoiceDueAmount();
				$boolIsValid &= $this->valCarrierInvoiceNumber();
				$boolIsValid &= $this->valInvoiceFileName();
				$boolIsValid &= $this->valInsuranceInvoiceExist( $strCarrierInvoiceNumber, $objInsuranceDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setInvoiceFileName( $strInvoiceFileName ) {
		$this->m_strInvoiceFileName = CStrings::strTrimDef( $strInvoiceFileName, NULL, NULL, true );
	}

	public function getInvoiceFileName() {
		return $this->m_strInvoiceFileName;
	}

}
?>