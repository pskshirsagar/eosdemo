<?php

class CInsuranceCachedRate extends CBaseInsuranceCachedRate {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsuranceCarrierId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsurancePolicyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPersonalInsuranceCarrierLimitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeductibleInsuranceCarrierLimitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLiabilityInsuranceCarrierLimitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPreferred() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPremiumAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>