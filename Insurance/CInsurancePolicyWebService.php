<?php
class CInsurancePolicyWebService extends CBaseInsurancePolicyWebService {

	const KEMPER_CONNECT			= 1;
	const QBE_CONNECT				= 2;
	const EXPERIAN_CONNECT_URL		= 3;
	const EXPERIAN_CONNECT_USERNAME	= 4;
	const EXPERIAN_CONNECT_PASSWORD	= 5;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
           	default:
            	$boolIsValid = true;
				break;
        }

        return $boolIsValid;
    }
}
?>