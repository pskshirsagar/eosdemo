<?php

class CInsuranceLimitType extends CBaseInsuranceLimitType {

	const LIABILITY            = 1;
	const PERSONAL             = 2;
	const DEDUCTIBLE           = 3;
	const MEDICALPAYMENT       = 4;
	const HURRICANE_DEDUCTIBLE = 5;

	public static $c_arrintInsuranceLimitTypeIds = [ self::LIABILITY, self::PERSONAL, self::DEDUCTIBLE ];

}

?>