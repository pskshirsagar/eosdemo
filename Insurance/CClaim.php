<?php

class CClaim extends CBaseClaim {

	const COMPANY_PHONE_NUMBER  = '801-375-5522';
	const COMPANY_EMAIL_ADDRESS = 'residentinsure@entrata.com';
	const COMPANY_ADDRESS       = '4205 Chapel Ridge Rd Lehi, UT 84043';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsuranceCarrierId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPolicyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCarrierPolicyNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactName() {
		$boolIsValid = true;
		if( true == is_null( $this->getContactName() ) || 0 == \Psi\CStringService::singleton()->strlen( trim( $this->getContactName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_name', __( ' Contact Name is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valContactPhoneNumber() {
		$boolIsValid = true;
		if( true == is_null( $this->getContactPhoneNumber() ) || 0 == \Psi\CStringService::singleton()->strlen( trim( $this->getContactPhoneNumber() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_phone_number', __( ' Phone Number is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valContactEmailAddress() {
		$boolIsValid = true;
		if( true == is_null( $this->getContactEmailAddress() ) || 0 == \Psi\CStringService::singleton()->strlen( trim( $this->getContactEmailAddress() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_email_address', __( ' Email is required.' ) ) );
		}

		if( true == $boolIsValid ) {
			if( false == CValidation::validateEmailAddresses( trim( $this->getContactEmailAddress() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_email_address', __( 'Please enter valid Email' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valIsUploadFail() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_claim_details':
				$boolIsValid &= $this->valContactName();
				$boolIsValid &= $this->valContactPhoneNumber();
				$boolIsValid &= $this->valContactEmailAddress();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>