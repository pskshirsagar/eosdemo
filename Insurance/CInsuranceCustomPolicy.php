<?php

class CInsuranceCustomPolicy extends CBaseInsuranceCustomPolicy {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createInsuranceCustomPolicy( $arrmixUninsuredResident ) {

		$objInsuranceCustomPolicy = new CInsuranceCustomPolicy();

		$objInsuranceCustomPolicy->setCid( $arrmixUninsuredResident['cid'] );
		$objInsuranceCustomPolicy->setPropertyId( $arrmixUninsuredResident['property_id'] );
		$objInsuranceCustomPolicy->setCustomerId( $arrmixUninsuredResident['customer_id'] );
		$objInsuranceCustomPolicy->setLeaseId( $arrmixUninsuredResident['lease_id'] );
		$objInsuranceCustomPolicy->setPolicyCarrierId( CInsuranceCarrier::SSIS );
		$objInsuranceCustomPolicy->setPolicyStatusTypeId( CInsurancePolicyStatusType::ACTIVE );

		return $objInsuranceCustomPolicy;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $objClientDatabase = NULL ) {

		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $objClientDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $objClientDatabase );
		}
	}
}
?>