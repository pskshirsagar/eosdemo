<?php

class CInsurancePolicyMigration extends CBaseInsurancePolicyMigration {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSourceCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDestinationCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSourcePropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDestinationPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPolicyNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsMigrated() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>