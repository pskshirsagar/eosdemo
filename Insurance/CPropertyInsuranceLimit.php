<?php

class CPropertyInsuranceLimit extends CBasePropertyInsuranceLimit {

	const PROPERTY_INSURANCE_LIABILITY_LIMIT_ONE_HUNDRED_THOUSAND  = 7;

	protected $m_fltInsuranceAmount;
	protected $m_strName;

    /**
     * Get Functions
     *
     */

    public function getInsuranceAmount() {
    	return $this->m_fltInsuranceAmount;
    }

    public function getName() {
    	return $this->m_strName;
    }

    /**
     * set Functions
     *
     */

    public function setName( $strName ) {
    	$this->m_strName = $strName;
    }

    public function setInsuranceAmount( $fltInsuranceAmount ) {
    	$this->m_fltInsuranceAmount = $fltInsuranceAmount;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['amount'] ) ) 	$this->setInsuranceAmount( $arrmixValues['amount'] );
    	if( true == isset( $arrmixValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['name'] ) : $arrmixValues['name'] );
    }

    public function valMinLiabilityInsuranceCarrierLimitId( $objInsurancePortalDatabase ) {
        $boolIsValid = true;

        $intMinimumLiabilityAmount = NULL;
        $intDefaultLiabilityAmount = NULL;

        if( true == is_null( $this->getMinLiabilityInsuranceCarrierLimitId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_liability_insurance_carrier_limit_id', 'Minimum liability is required. ' ) );

        } elseif( false == is_null( $this->getMinLiabilityInsuranceCarrierLimitId() ) && false == is_null( $this->getDefaultLiabilityInsuranceCarrierLimitId() ) ) {

        	$intDefaultLiabilityAmount   = NULL;
        	$arrobjInsuranceCarrierLimit = \Psi\Eos\Insurance\CInsuranceCarrierLimits::createService()->fetchInsuranceCarrierLimitsByInsuranceCarrierLimitIds( array( $this->getMinLiabilityInsuranceCarrierLimitId(), $this->getDefaultLiabilityInsuranceCarrierLimitId() ), $objInsurancePortalDatabase );

        	$intMinimumLiabilityAmount = intval( $arrobjInsuranceCarrierLimit[$this->getMinLiabilityInsuranceCarrierLimitId()]->getInsuranceAmount() );
        	$intDefaultLiabilityAmount = intval( $arrobjInsuranceCarrierLimit[$this->getDefaultLiabilityInsuranceCarrierLimitId()]->getInsuranceAmount() );

        	if( $intDefaultLiabilityAmount < $intMinimumLiabilityAmount ) {
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_liability_insurance_carrier_limit_id', 'Default liability should be greater than or equal to Minimum liability. ' ) );
        		$boolIsValid = false;
        	}
        }
        return array( $boolIsValid, $intMinimumLiabilityAmount );
    }

    public function valDefaultLiabilityInsuranceCarrierLimitId() {
        $boolIsValid = true;

        if( true == is_null( $this->getDefaultLiabilityInsuranceCarrierLimitId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_liability_insurance_carrier_limit_id', 'Default liability is required. ' ) );
        }

        return $boolIsValid;
    }

 	public function valPersonalInsuranceCarrierLimitId( $strProductName = NULL ) {
        $boolIsValid = true;

        if( true == is_null( $this->getPersonalInsuranceCarrierLimitId() ) ) {
        	$boolIsValid = false;

        	if( NULL == $strProductName ) {
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'personal_insurance_carrier_limit_id', ' Default personal is required. ' ) );
        	} else {
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'personal_insurance_carrier_limit_id', ' Please select ' . $strProductName . ' personal amount. ' ) );
        	}
        }

        return $boolIsValid;
    }

    public function valDeductibleInsuranceCarrierLimitId( $strProductName = NULL ) {
        $boolIsValid = true;

        if( true == is_null( $this->getDeductibleInsuranceCarrierLimitId() ) ) {
        	$boolIsValid = false;

	        if( NULL == $strProductName ) {
		        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deductible_insurance_carrier_limit_id', 'Default deductible is required. ' ) );
	        } else {
		        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deductible_insurance_carrier_limit_id', ' Please select ' . $strProductName . ' deductible amount. ' ) );
	        }
        }

        return $boolIsValid;
    }

	public function valDefaultMedicalPaymentLimitId() {
		$boolIsValid = true;
		if( true == is_null( $this->getDefaultMedicalPaymentLimitId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_medical_payment_limit_id', 'Medical Payment is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valClientId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getCid() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please select clients. ' ) );
    	}

    	return $boolIsValid;
    }

    public function valRatePropertyId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getPropertyId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please select property. ' ) );
    	}

    	return $boolIsValid;
    }

	public function valLiabilityInsuranceCarrierLimitId( $strProductName = NULL ) {
    	$boolIsValid = true;

    	if( true == is_null( $this->getDefaultLiabilityInsuranceCarrierLimitId() ) ) {
    		$boolIsValid = false;

    		if( NULL == $strProductName ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, ' liability amount is required. ' ) );
    		} else {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, ' Please select ' . $strProductName . ' liability amount. ' ) );
    		}
    	}

    	return $boolIsValid;
    }

	public function valPremiumRate( $strProductName = NULL ) {
    	$boolIsValid = true;

		if( true == is_null( $this->getPremiumRate() ) || false == is_numeric( $this->getPremiumRate() ) || ( 0 == $this->getPremiumRate() && 'ID Theft' != $strProductName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Please enter correct ' . $strProductName . ' premium rate. ' ) );
    	}

    	return $boolIsValid;
    }

    public function valEarnedPremium( $strProductName = NULL, $fltForcedPlacedFee ) {
    	$boolIsValid = true;

    	if( false == is_numeric( $this->getInvoicePremium() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Please enter correct ' . $strProductName . ' processing fee. ' ) );
    	}

    	return $boolIsValid;
    }

    public function valIDTheftPremiumRate( $boolIsIncludeResidentSecure ) {
    	$boolIsValid = true;
    	$fltPremiumRate = $this->getPremiumRate();

   		if( true == $boolIsIncludeResidentSecure && ( true == is_null( $fltPremiumRate ) || false == is_numeric( $fltPremiumRate ) || 6 <= \Psi\CStringService::singleton()->strlen( ( string ) $fltPremiumRate ) ) ) {
	   		$boolIsValid = false;
	   		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'premium_rate', 'Please enter correct client fee amount. ' ) );
	   	}

    	return $boolIsValid;
    }

    public function validate( $strAction, $boolIsIncludeResidentSecure = false, $fltForcedPlacedFee = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
 			case VALIDATE_UPDATE:
	            $boolIsValid &= $this->valDefaultLiabilityInsuranceCarrierLimitId();
	            $boolIsValid &= $this->valPersonalInsuranceCarrierLimitId();
	            $boolIsValid &= $this->valDeductibleInsuranceCarrierLimitId();
	            $boolIsValid &= $this->valDefaultMedicalPaymentLimitId();
	            break;

            case VALIDATE_DELETE:
	            break;

            case 'VALIDATE_FORCED_PLACED_PROPERTY_RATE_INSERT':
            case 'VALIDATE_FORCED_PLACED_PROPERTY_RATE_UPDATE':
                $boolIsValid &= $this->valEarnedPremium( 'Forced Placed', $fltForcedPlacedFee );
            	$boolIsValid &= $this->valLiabilityInsuranceCarrierLimitId( 'Forced Placed' );
	            $boolIsValid &= $this->valPersonalInsuranceCarrierLimitId( 'Forced Placed' );
	            $boolIsValid &= $this->valDeductibleInsuranceCarrierLimitId( 'Forced Placed' );
            	break;

	        case 'VALIDATE_BLANKET_PROPERTY_RATE_INSERT':
	        case 'VALIDATE_BLANKET_PROPERTY_RATE_UPDATE':
		        $boolIsValid &= $this->valEarnedPremium( 'Blanket', $fltForcedPlacedFee );
		        $boolIsValid &= $this->valLiabilityInsuranceCarrierLimitId( 'Blanket program' );
	            $boolIsValid &= $this->valPersonalInsuranceCarrierLimitId( 'Blanket program' );
	            $boolIsValid &= $this->valDeductibleInsuranceCarrierLimitId( 'Blanket program' );
		        break;

	        case 'VALIDATE_BLANKET_ENHANCED_PROPERTY_RATE_INSERT':
	        case 'VALIDATE_BLANKET_ENHANCED_PROPERTY_RATE_UPDATE':
	            $boolIsValid &= $this->valEarnedPremium( 'Blanket Enhanced', $fltForcedPlacedFee );
		        $boolIsValid &= $this->valLiabilityInsuranceCarrierLimitId( 'Blanket Enhanced' );
		        $boolIsValid &= $this->valPersonalInsuranceCarrierLimitId( 'Blanket Enhanced' );
		        $boolIsValid &= $this->valDeductibleInsuranceCarrierLimitId( 'Blanket Enhanced' );
		        break;

            case 'VALIDATE_ID_THEFT_PROPERTY_RATE_INSERT':
            case 'VALIDATE_ID_THEFT_PROPERTY_RATE_UPDATE':
            	$boolIsValid &= $this->valPremiumRate( 'ID Theft' );
            	break;

            case 'VALIDATE_CLIENT_AND_PROPERTY_INSERT':
                $boolIsValid &= $this->valClientId();
                $boolIsValid &= $this->valRatePropertyId();
                break;

			case 'validate_id_theft':
				$boolIsValid &= $this->valIDTheftPremiumRate( $boolIsIncludeResidentSecure );
                break;

	        default:
	           	// default case
	        	$boolIsValid = true;
	           	break;
        }

        return $boolIsValid;
    }

    public function createPropertyInsuranceLimitObject( $arrmixInputParameter, $objInsuranceDatabase = NULL ) {

    	if( true == is_null( $arrmixInputParameter['id'] ) || '' == $arrmixInputParameter['id'] ) {
    		$objPropertyInsuranceLimit = new CPropertyInsuranceLimit();
    	} else {
    		$objPropertyInsuranceLimit = \Psi\Eos\Insurance\CPropertyInsuranceLimits::createService()->fetchPropertyInsuranceLimitById( $arrmixInputParameter['id'], $objInsuranceDatabase );
    	}

		$objPropertyInsuranceLimit->setPremiumRate( $arrmixInputParameter['premium_rate'] );
		$objPropertyInsuranceLimit->setPersonalInsuranceCarrierLimitId( $arrmixInputParameter['personal_insurance_carrier_limit_ld'] );
    	$objPropertyInsuranceLimit->setDefaultLiabilityInsuranceCarrierLimitId( $arrmixInputParameter['default_liability_insurance_carrier_limitId'] );
    	$objPropertyInsuranceLimit->setInsurancePolicyTypeId( $arrmixInputParameter['policy_type'] );
    	$objPropertyInsuranceLimit->setDeductibleInsuranceCarrierLimitId( $arrmixInputParameter['deductible_insurance_carrier_limit_id'] );
    	$objPropertyInsuranceLimit->setIsActive( $arrmixInputParameter['is_active'] );
    	$objPropertyInsuranceLimit->setCid( $arrmixInputParameter['cid'] );
    	$objPropertyInsuranceLimit->setPropertyId( $arrmixInputParameter['property_id'] );
    	$objPropertyInsuranceLimit->setInvoicePremium( $arrmixInputParameter['invoice_premium'] );

    	return $objPropertyInsuranceLimit;
    }

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolIsReturnSql = false ) {
    	if( true == is_null( $this->getId() ) ) {
    		return $this->insert( $intCurrentUserId, $objDatabase, $boolIsReturnSql );
    	} else {
    		return $this->update( $intCurrentUserId, $objDatabase, $boolIsReturnSql );
    	}
    }

}
?>