<?php

class CInsuranceInvoiceStatusType extends CBaseInsuranceInvoiceStatusType {

	const UNPROCESSED   = 1;
	const DISPUTED      = 2;
	const APPROVED      = 3;
	const PAID          = 4;

	public static $c_arrintInsuranceInvoiceStatusTypes = [ self::UNPROCESSED => 'Un Processed', self::DISPUTED => 'Disputed', self::APPROVED => 'Approved', self::PAID => 'Paid' ];
	public static $c_arrintInsuranceInvoiceStatusTypesNotAllowedToDelete = array( self::APPROVED, self::PAID );

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>