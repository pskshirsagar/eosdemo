<?php

class CMasterPolicy extends CBaseMasterPolicy {

	public static $c_arrstrMasterPolicyPreferenceKeys = [ 'REQUIRE_FORCE_PLACED_POLICIES', 'ENFORCEMENT_STRATEGY', 'MASTER_POLICY_AGREEMENT', 'EXCLUDE_RENEWALS', 'FORCE_PLACED_FEE', 'PRORATE_BY_DAY', 'POLICY_AR_CHARGE_CODE', 'GRACE_PERIOD_DAYS', 'ALLOW_GRACE_PERIOD_DAYS', 'POLICY_POSTPONE_LIMIT_DAYS', 'POLICY_ABOUT_TO_END_DAYS', 'SEND_FORCE_PLACED_EMAILS', 'REQUIRE_BLANKET_POLICIES', 'EXCLUDE_RENEWALS_UNTIL' ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPolicyCarrierId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsurancePolicyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPolicyStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLiabilityCarrierLimitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPersonalCarrierLimitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeductibleCarrierLimitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPolicyNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createMasterPolicy( $arrmixUninsuredResident ) {

		$objMasterPolicy = new CMasterPolicy();

		$objMasterPolicy->setCid( $arrmixUninsuredResident['cid'] );
		$objMasterPolicy->setPropertyId( $arrmixUninsuredResident['property_id'] );
		$objMasterPolicy->setCustomerId( $arrmixUninsuredResident['customer_id'] );
		$objMasterPolicy->setLeaseId( $arrmixUninsuredResident['lease_id'] );
		$objMasterPolicy->setPolicyCarrierId( CInsuranceCarrier::SSIS );
		$objMasterPolicy->setPolicyStatusTypeId( CInsurancePolicyStatusType::ACTIVE );

		return $objMasterPolicy;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $objClientDatabase = NULL ) {

		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $objClientDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $objClientDatabase );
		}
	}

}
?>