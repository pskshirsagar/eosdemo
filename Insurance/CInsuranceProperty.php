<?php

class CInsuranceProperty extends CBaseInsuranceProperty {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallPhoneNumberId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAutoSprinklerCreditId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyConstructionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWindstormConstructionCreditId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBcegCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPpcCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCountyCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCountyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLongitude() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLatitude() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConstructionYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProfessionallyManagedYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsProfessionallyManaged() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsGatedCommunity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>