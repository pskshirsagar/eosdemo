<?php

class CInsuranceInsuredType extends CBaseInsuranceInsuredType {

	const PRIMARY_INSURED    = 1;
	const ADDITIONAL_INSURED = 2;
	const SPOUSE_INSURED     = 4;

	public static $c_arrintAdditionalInsuredTypeIds = [ self::ADDITIONAL_INSURED, self::SPOUSE_INSURED ];
}
?>