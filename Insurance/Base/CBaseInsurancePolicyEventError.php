<?php

class CBaseInsurancePolicyEventError extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_policy_event_errors';

	protected $m_intId;
	protected $m_intEventId;
	protected $m_strErrorCode;
	protected $m_strErrorText;
	protected $m_intErrorCount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intErrorCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['event_id'] ) && $boolDirectSet ) $this->set( 'm_intEventId', trim( $arrValues['event_id'] ) ); elseif( isset( $arrValues['event_id'] ) ) $this->setEventId( $arrValues['event_id'] );
		if( isset( $arrValues['error_code'] ) && $boolDirectSet ) $this->set( 'm_strErrorCode', trim( stripcslashes( $arrValues['error_code'] ) ) ); elseif( isset( $arrValues['error_code'] ) ) $this->setErrorCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['error_code'] ) : $arrValues['error_code'] );
		if( isset( $arrValues['error_text'] ) && $boolDirectSet ) $this->set( 'm_strErrorText', trim( stripcslashes( $arrValues['error_text'] ) ) ); elseif( isset( $arrValues['error_text'] ) ) $this->setErrorText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['error_text'] ) : $arrValues['error_text'] );
		if( isset( $arrValues['error_count'] ) && $boolDirectSet ) $this->set( 'm_intErrorCount', trim( $arrValues['error_count'] ) ); elseif( isset( $arrValues['error_count'] ) ) $this->setErrorCount( $arrValues['error_count'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEventId( $intEventId ) {
		$this->set( 'm_intEventId', CStrings::strToIntDef( $intEventId, NULL, false ) );
	}

	public function getEventId() {
		return $this->m_intEventId;
	}

	public function sqlEventId() {
		return ( true == isset( $this->m_intEventId ) ) ? ( string ) $this->m_intEventId : 'NULL';
	}

	public function setErrorCode( $strErrorCode ) {
		$this->set( 'm_strErrorCode', CStrings::strTrimDef( $strErrorCode, 200, NULL, true ) );
	}

	public function getErrorCode() {
		return $this->m_strErrorCode;
	}

	public function sqlErrorCode() {
		return ( true == isset( $this->m_strErrorCode ) ) ? '\'' . addslashes( $this->m_strErrorCode ) . '\'' : 'NULL';
	}

	public function setErrorText( $strErrorText ) {
		$this->set( 'm_strErrorText', CStrings::strTrimDef( $strErrorText, -1, NULL, true ) );
	}

	public function getErrorText() {
		return $this->m_strErrorText;
	}

	public function sqlErrorText() {
		return ( true == isset( $this->m_strErrorText ) ) ? '\'' . addslashes( $this->m_strErrorText ) . '\'' : 'NULL';
	}

	public function setErrorCount( $intErrorCount ) {
		$this->set( 'm_intErrorCount', CStrings::strToIntDef( $intErrorCount, NULL, false ) );
	}

	public function getErrorCount() {
		return $this->m_intErrorCount;
	}

	public function sqlErrorCount() {
		return ( true == isset( $this->m_intErrorCount ) ) ? ( string ) $this->m_intErrorCount : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, event_id, error_code, error_text, error_count, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEventId() . ', ' .
 						$this->sqlErrorCode() . ', ' .
 						$this->sqlErrorText() . ', ' .
 						$this->sqlErrorCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_id = ' . $this->sqlEventId() . ','; } elseif( true == array_key_exists( 'EventId', $this->getChangedColumns() ) ) { $strSql .= ' event_id = ' . $this->sqlEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_code = ' . $this->sqlErrorCode() . ','; } elseif( true == array_key_exists( 'ErrorCode', $this->getChangedColumns() ) ) { $strSql .= ' error_code = ' . $this->sqlErrorCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_text = ' . $this->sqlErrorText() . ','; } elseif( true == array_key_exists( 'ErrorText', $this->getChangedColumns() ) ) { $strSql .= ' error_text = ' . $this->sqlErrorText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_count = ' . $this->sqlErrorCount() . ','; } elseif( true == array_key_exists( 'ErrorCount', $this->getChangedColumns() ) ) { $strSql .= ' error_count = ' . $this->sqlErrorCount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'event_id' => $this->getEventId(),
			'error_code' => $this->getErrorCode(),
			'error_text' => $this->getErrorText(),
			'error_count' => $this->getErrorCount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>