<?php

class CBaseInsurancePropertyProduct extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_property_products';

	protected $m_intId;
	protected $m_intInsurancePropertyId;
	protected $m_intInsuranceCarrierId;
	protected $m_intInsurancePolicyTypeId;
	protected $m_intInsuranceCarrierClassificationTypeId;
	protected $m_intMinimumBillingFrequencyId;
	protected $m_intDefaultLiabilityInsuranceCarrierLimitId;
	protected $m_intPersonalInsuranceCarrierLimitId;
	protected $m_intDeductibleInsuranceCarrierLimitId;
	protected $m_intMinLiabilityInsuranceCarrierLimitId;
	protected $m_intMaxLiabilityInsuranceCarrierLimitId;
	protected $m_intMinDeductibleInsuranceCarrierLimitId;
	protected $m_intDefaultMedicalPaymentLimitId;
	protected $m_intEnhancementEndorsementId;
	protected $m_strSubDomain;
	protected $m_boolIsAllowedLiabilityOnly;
	protected $m_boolIsGeneric;
	protected $m_boolIsActive;
	protected $m_boolAutoEnabled;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intInsuranceProductId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsAllowedLiabilityOnly = false;
		$this->m_boolIsGeneric = false;
		$this->m_boolIsActive = true;
		$this->m_boolAutoEnabled = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_property_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePropertyId', trim( $arrValues['insurance_property_id'] ) ); elseif( isset( $arrValues['insurance_property_id'] ) ) $this->setInsurancePropertyId( $arrValues['insurance_property_id'] );
		if( isset( $arrValues['insurance_carrier_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierId', trim( $arrValues['insurance_carrier_id'] ) ); elseif( isset( $arrValues['insurance_carrier_id'] ) ) $this->setInsuranceCarrierId( $arrValues['insurance_carrier_id'] );
		if( isset( $arrValues['insurance_policy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyTypeId', trim( $arrValues['insurance_policy_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_type_id'] ) ) $this->setInsurancePolicyTypeId( $arrValues['insurance_policy_type_id'] );
		if( isset( $arrValues['insurance_carrier_classification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierClassificationTypeId', trim( $arrValues['insurance_carrier_classification_type_id'] ) ); elseif( isset( $arrValues['insurance_carrier_classification_type_id'] ) ) $this->setInsuranceCarrierClassificationTypeId( $arrValues['insurance_carrier_classification_type_id'] );
		if( isset( $arrValues['minimum_billing_frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intMinimumBillingFrequencyId', trim( $arrValues['minimum_billing_frequency_id'] ) ); elseif( isset( $arrValues['minimum_billing_frequency_id'] ) ) $this->setMinimumBillingFrequencyId( $arrValues['minimum_billing_frequency_id'] );
		if( isset( $arrValues['default_liability_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultLiabilityInsuranceCarrierLimitId', trim( $arrValues['default_liability_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['default_liability_insurance_carrier_limit_id'] ) ) $this->setDefaultLiabilityInsuranceCarrierLimitId( $arrValues['default_liability_insurance_carrier_limit_id'] );
		if( isset( $arrValues['personal_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intPersonalInsuranceCarrierLimitId', trim( $arrValues['personal_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['personal_insurance_carrier_limit_id'] ) ) $this->setPersonalInsuranceCarrierLimitId( $arrValues['personal_insurance_carrier_limit_id'] );
		if( isset( $arrValues['deductible_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intDeductibleInsuranceCarrierLimitId', trim( $arrValues['deductible_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['deductible_insurance_carrier_limit_id'] ) ) $this->setDeductibleInsuranceCarrierLimitId( $arrValues['deductible_insurance_carrier_limit_id'] );
		if( isset( $arrValues['min_liability_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intMinLiabilityInsuranceCarrierLimitId', trim( $arrValues['min_liability_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['min_liability_insurance_carrier_limit_id'] ) ) $this->setMinLiabilityInsuranceCarrierLimitId( $arrValues['min_liability_insurance_carrier_limit_id'] );
		if( isset( $arrValues['max_liability_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intMaxLiabilityInsuranceCarrierLimitId', trim( $arrValues['max_liability_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['max_liability_insurance_carrier_limit_id'] ) ) $this->setMaxLiabilityInsuranceCarrierLimitId( $arrValues['max_liability_insurance_carrier_limit_id'] );
		if( isset( $arrValues['min_deductible_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intMinDeductibleInsuranceCarrierLimitId', trim( $arrValues['min_deductible_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['min_deductible_insurance_carrier_limit_id'] ) ) $this->setMinDeductibleInsuranceCarrierLimitId( $arrValues['min_deductible_insurance_carrier_limit_id'] );
		if( isset( $arrValues['default_medical_payment_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultMedicalPaymentLimitId', trim( $arrValues['default_medical_payment_limit_id'] ) ); elseif( isset( $arrValues['default_medical_payment_limit_id'] ) ) $this->setDefaultMedicalPaymentLimitId( $arrValues['default_medical_payment_limit_id'] );
		if( isset( $arrValues['enhancement_endorsement_id'] ) && $boolDirectSet ) $this->set( 'm_intEnhancementEndorsementId', trim( $arrValues['enhancement_endorsement_id'] ) ); elseif( isset( $arrValues['enhancement_endorsement_id'] ) ) $this->setEnhancementEndorsementId( $arrValues['enhancement_endorsement_id'] );
		if( isset( $arrValues['sub_domain'] ) && $boolDirectSet ) $this->set( 'm_strSubDomain', trim( $arrValues['sub_domain'] ) ); elseif( isset( $arrValues['sub_domain'] ) ) $this->setSubDomain( $arrValues['sub_domain'] );
		if( isset( $arrValues['is_allowed_liability_only'] ) && $boolDirectSet ) $this->set( 'm_boolIsAllowedLiabilityOnly', trim( stripcslashes( $arrValues['is_allowed_liability_only'] ) ) ); elseif( isset( $arrValues['is_allowed_liability_only'] ) ) $this->setIsAllowedLiabilityOnly( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_allowed_liability_only'] ) : $arrValues['is_allowed_liability_only'] );
		if( isset( $arrValues['is_generic'] ) && $boolDirectSet ) $this->set( 'm_boolIsGeneric', trim( stripcslashes( $arrValues['is_generic'] ) ) ); elseif( isset( $arrValues['is_generic'] ) ) $this->setIsGeneric( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_generic'] ) : $arrValues['is_generic'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['auto_enabled'] ) && $boolDirectSet ) $this->set( 'm_boolAutoEnabled', trim( stripcslashes( $arrValues['auto_enabled'] ) ) ); elseif( isset( $arrValues['auto_enabled'] ) ) $this->setAutoEnabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auto_enabled'] ) : $arrValues['auto_enabled'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['insurance_product_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceProductId', trim( $arrValues['insurance_product_id'] ) ); elseif( isset( $arrValues['insurance_product_id'] ) ) $this->setInsuranceProductId( $arrValues['insurance_product_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsurancePropertyId( $intInsurancePropertyId ) {
		$this->set( 'm_intInsurancePropertyId', CStrings::strToIntDef( $intInsurancePropertyId, NULL, false ) );
	}

	public function getInsurancePropertyId() {
		return $this->m_intInsurancePropertyId;
	}

	public function sqlInsurancePropertyId() {
		return ( true == isset( $this->m_intInsurancePropertyId ) ) ? ( string ) $this->m_intInsurancePropertyId : 'NULL';
	}

	public function setInsuranceCarrierId( $intInsuranceCarrierId ) {
		$this->set( 'm_intInsuranceCarrierId', CStrings::strToIntDef( $intInsuranceCarrierId, NULL, false ) );
	}

	public function getInsuranceCarrierId() {
		return $this->m_intInsuranceCarrierId;
	}

	public function sqlInsuranceCarrierId() {
		return ( true == isset( $this->m_intInsuranceCarrierId ) ) ? ( string ) $this->m_intInsuranceCarrierId : 'NULL';
	}

	public function setInsurancePolicyTypeId( $intInsurancePolicyTypeId ) {
		$this->set( 'm_intInsurancePolicyTypeId', CStrings::strToIntDef( $intInsurancePolicyTypeId, NULL, false ) );
	}

	public function getInsurancePolicyTypeId() {
		return $this->m_intInsurancePolicyTypeId;
	}

	public function sqlInsurancePolicyTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyTypeId ) ) ? ( string ) $this->m_intInsurancePolicyTypeId : 'NULL';
	}

	public function setInsuranceCarrierClassificationTypeId( $intInsuranceCarrierClassificationTypeId ) {
		$this->set( 'm_intInsuranceCarrierClassificationTypeId', CStrings::strToIntDef( $intInsuranceCarrierClassificationTypeId, NULL, false ) );
	}

	public function getInsuranceCarrierClassificationTypeId() {
		return $this->m_intInsuranceCarrierClassificationTypeId;
	}

	public function sqlInsuranceCarrierClassificationTypeId() {
		return ( true == isset( $this->m_intInsuranceCarrierClassificationTypeId ) ) ? ( string ) $this->m_intInsuranceCarrierClassificationTypeId : 'NULL';
	}

	public function setMinimumBillingFrequencyId( $intMinimumBillingFrequencyId ) {
		$this->set( 'm_intMinimumBillingFrequencyId', CStrings::strToIntDef( $intMinimumBillingFrequencyId, NULL, false ) );
	}

	public function getMinimumBillingFrequencyId() {
		return $this->m_intMinimumBillingFrequencyId;
	}

	public function sqlMinimumBillingFrequencyId() {
		return ( true == isset( $this->m_intMinimumBillingFrequencyId ) ) ? ( string ) $this->m_intMinimumBillingFrequencyId : 'NULL';
	}

	public function setDefaultLiabilityInsuranceCarrierLimitId( $intDefaultLiabilityInsuranceCarrierLimitId ) {
		$this->set( 'm_intDefaultLiabilityInsuranceCarrierLimitId', CStrings::strToIntDef( $intDefaultLiabilityInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getDefaultLiabilityInsuranceCarrierLimitId() {
		return $this->m_intDefaultLiabilityInsuranceCarrierLimitId;
	}

	public function sqlDefaultLiabilityInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intDefaultLiabilityInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intDefaultLiabilityInsuranceCarrierLimitId : 'NULL';
	}

	public function setPersonalInsuranceCarrierLimitId( $intPersonalInsuranceCarrierLimitId ) {
		$this->set( 'm_intPersonalInsuranceCarrierLimitId', CStrings::strToIntDef( $intPersonalInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getPersonalInsuranceCarrierLimitId() {
		return $this->m_intPersonalInsuranceCarrierLimitId;
	}

	public function sqlPersonalInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intPersonalInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intPersonalInsuranceCarrierLimitId : 'NULL';
	}

	public function setDeductibleInsuranceCarrierLimitId( $intDeductibleInsuranceCarrierLimitId ) {
		$this->set( 'm_intDeductibleInsuranceCarrierLimitId', CStrings::strToIntDef( $intDeductibleInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getDeductibleInsuranceCarrierLimitId() {
		return $this->m_intDeductibleInsuranceCarrierLimitId;
	}

	public function sqlDeductibleInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intDeductibleInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intDeductibleInsuranceCarrierLimitId : 'NULL';
	}

	public function setMinLiabilityInsuranceCarrierLimitId( $intMinLiabilityInsuranceCarrierLimitId ) {
		$this->set( 'm_intMinLiabilityInsuranceCarrierLimitId', CStrings::strToIntDef( $intMinLiabilityInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getMinLiabilityInsuranceCarrierLimitId() {
		return $this->m_intMinLiabilityInsuranceCarrierLimitId;
	}

	public function sqlMinLiabilityInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intMinLiabilityInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intMinLiabilityInsuranceCarrierLimitId : 'NULL';
	}

	public function setMaxLiabilityInsuranceCarrierLimitId( $intMaxLiabilityInsuranceCarrierLimitId ) {
		$this->set( 'm_intMaxLiabilityInsuranceCarrierLimitId', CStrings::strToIntDef( $intMaxLiabilityInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getMaxLiabilityInsuranceCarrierLimitId() {
		return $this->m_intMaxLiabilityInsuranceCarrierLimitId;
	}

	public function sqlMaxLiabilityInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intMaxLiabilityInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intMaxLiabilityInsuranceCarrierLimitId : 'NULL';
	}

	public function setMinDeductibleInsuranceCarrierLimitId( $intMinDeductibleInsuranceCarrierLimitId ) {
		$this->set( 'm_intMinDeductibleInsuranceCarrierLimitId', CStrings::strToIntDef( $intMinDeductibleInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getMinDeductibleInsuranceCarrierLimitId() {
		return $this->m_intMinDeductibleInsuranceCarrierLimitId;
	}

	public function sqlMinDeductibleInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intMinDeductibleInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intMinDeductibleInsuranceCarrierLimitId : 'NULL';
	}

	public function setDefaultMedicalPaymentLimitId( $intDefaultMedicalPaymentLimitId ) {
		$this->set( 'm_intDefaultMedicalPaymentLimitId', CStrings::strToIntDef( $intDefaultMedicalPaymentLimitId, NULL, false ) );
	}

	public function getDefaultMedicalPaymentLimitId() {
		return $this->m_intDefaultMedicalPaymentLimitId;
	}

	public function sqlDefaultMedicalPaymentLimitId() {
		return ( true == isset( $this->m_intDefaultMedicalPaymentLimitId ) ) ? ( string ) $this->m_intDefaultMedicalPaymentLimitId : 'NULL';
	}

	public function setEnhancementEndorsementId( $intEnhancementEndorsementId ) {
		$this->set( 'm_intEnhancementEndorsementId', CStrings::strToIntDef( $intEnhancementEndorsementId, NULL, false ) );
	}

	public function getEnhancementEndorsementId() {
		return $this->m_intEnhancementEndorsementId;
	}

	public function sqlEnhancementEndorsementId() {
		return ( true == isset( $this->m_intEnhancementEndorsementId ) ) ? ( string ) $this->m_intEnhancementEndorsementId : 'NULL';
	}

	public function setSubDomain( $strSubDomain ) {
		$this->set( 'm_strSubDomain', CStrings::strTrimDef( $strSubDomain, 240, NULL, true ) );
	}

	public function getSubDomain() {
		return $this->m_strSubDomain;
	}

	public function sqlSubDomain() {
		return ( true == isset( $this->m_strSubDomain ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSubDomain ) : '\'' . addslashes( $this->m_strSubDomain ) . '\'' ) : 'NULL';
	}

	public function setIsAllowedLiabilityOnly( $boolIsAllowedLiabilityOnly ) {
		$this->set( 'm_boolIsAllowedLiabilityOnly', CStrings::strToBool( $boolIsAllowedLiabilityOnly ) );
	}

	public function getIsAllowedLiabilityOnly() {
		return $this->m_boolIsAllowedLiabilityOnly;
	}

	public function sqlIsAllowedLiabilityOnly() {
		return ( true == isset( $this->m_boolIsAllowedLiabilityOnly ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAllowedLiabilityOnly ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsGeneric( $boolIsGeneric ) {
		$this->set( 'm_boolIsGeneric', CStrings::strToBool( $boolIsGeneric ) );
	}

	public function getIsGeneric() {
		return $this->m_boolIsGeneric;
	}

	public function sqlIsGeneric() {
		return ( true == isset( $this->m_boolIsGeneric ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsGeneric ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAutoEnabled( $boolAutoEnabled ) {
		$this->set( 'm_boolAutoEnabled', CStrings::strToBool( $boolAutoEnabled ) );
	}

	public function getAutoEnabled() {
		return $this->m_boolAutoEnabled;
	}

	public function sqlAutoEnabled() {
		return ( true == isset( $this->m_boolAutoEnabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolAutoEnabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setInsuranceProductId( $intInsuranceProductId ) {
		$this->set( 'm_intInsuranceProductId', CStrings::strToIntDef( $intInsuranceProductId, NULL, false ) );
	}

	public function getInsuranceProductId() {
		return $this->m_intInsuranceProductId;
	}

	public function sqlInsuranceProductId() {
		return ( true == isset( $this->m_intInsuranceProductId ) ) ? ( string ) $this->m_intInsuranceProductId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, insurance_property_id, insurance_carrier_id, insurance_policy_type_id, insurance_carrier_classification_type_id, minimum_billing_frequency_id, default_liability_insurance_carrier_limit_id, personal_insurance_carrier_limit_id, deductible_insurance_carrier_limit_id, min_liability_insurance_carrier_limit_id, max_liability_insurance_carrier_limit_id, min_deductible_insurance_carrier_limit_id, default_medical_payment_limit_id, enhancement_endorsement_id, sub_domain, is_allowed_liability_only, is_generic, is_active, auto_enabled, updated_by, updated_on, created_by, created_on, insurance_product_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlInsurancePropertyId() . ', ' .
						$this->sqlInsuranceCarrierId() . ', ' .
						$this->sqlInsurancePolicyTypeId() . ', ' .
						$this->sqlInsuranceCarrierClassificationTypeId() . ', ' .
						$this->sqlMinimumBillingFrequencyId() . ', ' .
						$this->sqlDefaultLiabilityInsuranceCarrierLimitId() . ', ' .
						$this->sqlPersonalInsuranceCarrierLimitId() . ', ' .
						$this->sqlDeductibleInsuranceCarrierLimitId() . ', ' .
						$this->sqlMinLiabilityInsuranceCarrierLimitId() . ', ' .
						$this->sqlMaxLiabilityInsuranceCarrierLimitId() . ', ' .
						$this->sqlMinDeductibleInsuranceCarrierLimitId() . ', ' .
						$this->sqlDefaultMedicalPaymentLimitId() . ', ' .
						$this->sqlEnhancementEndorsementId() . ', ' .
						$this->sqlSubDomain() . ', ' .
						$this->sqlIsAllowedLiabilityOnly() . ', ' .
						$this->sqlIsGeneric() . ', ' .
						$this->sqlIsActive() . ', ' .
						$this->sqlAutoEnabled() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlInsuranceProductId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_property_id = ' . $this->sqlInsurancePropertyId(). ',' ; } elseif( true == array_key_exists( 'InsurancePropertyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_property_id = ' . $this->sqlInsurancePropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId(). ',' ; } elseif( true == array_key_exists( 'InsuranceCarrierId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_classification_type_id = ' . $this->sqlInsuranceCarrierClassificationTypeId(). ',' ; } elseif( true == array_key_exists( 'InsuranceCarrierClassificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_classification_type_id = ' . $this->sqlInsuranceCarrierClassificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minimum_billing_frequency_id = ' . $this->sqlMinimumBillingFrequencyId(). ',' ; } elseif( true == array_key_exists( 'MinimumBillingFrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' minimum_billing_frequency_id = ' . $this->sqlMinimumBillingFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_liability_insurance_carrier_limit_id = ' . $this->sqlDefaultLiabilityInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'DefaultLiabilityInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' default_liability_insurance_carrier_limit_id = ' . $this->sqlDefaultLiabilityInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' personal_insurance_carrier_limit_id = ' . $this->sqlPersonalInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'PersonalInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' personal_insurance_carrier_limit_id = ' . $this->sqlPersonalInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deductible_insurance_carrier_limit_id = ' . $this->sqlDeductibleInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'DeductibleInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' deductible_insurance_carrier_limit_id = ' . $this->sqlDeductibleInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_liability_insurance_carrier_limit_id = ' . $this->sqlMinLiabilityInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'MinLiabilityInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' min_liability_insurance_carrier_limit_id = ' . $this->sqlMinLiabilityInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_liability_insurance_carrier_limit_id = ' . $this->sqlMaxLiabilityInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'MaxLiabilityInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' max_liability_insurance_carrier_limit_id = ' . $this->sqlMaxLiabilityInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_deductible_insurance_carrier_limit_id = ' . $this->sqlMinDeductibleInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'MinDeductibleInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' min_deductible_insurance_carrier_limit_id = ' . $this->sqlMinDeductibleInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_medical_payment_limit_id = ' . $this->sqlDefaultMedicalPaymentLimitId(). ',' ; } elseif( true == array_key_exists( 'DefaultMedicalPaymentLimitId', $this->getChangedColumns() ) ) { $strSql .= ' default_medical_payment_limit_id = ' . $this->sqlDefaultMedicalPaymentLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enhancement_endorsement_id = ' . $this->sqlEnhancementEndorsementId(). ',' ; } elseif( true == array_key_exists( 'EnhancementEndorsementId', $this->getChangedColumns() ) ) { $strSql .= ' enhancement_endorsement_id = ' . $this->sqlEnhancementEndorsementId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sub_domain = ' . $this->sqlSubDomain(). ',' ; } elseif( true == array_key_exists( 'SubDomain', $this->getChangedColumns() ) ) { $strSql .= ' sub_domain = ' . $this->sqlSubDomain() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_allowed_liability_only = ' . $this->sqlIsAllowedLiabilityOnly(). ',' ; } elseif( true == array_key_exists( 'IsAllowedLiabilityOnly', $this->getChangedColumns() ) ) { $strSql .= ' is_allowed_liability_only = ' . $this->sqlIsAllowedLiabilityOnly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_generic = ' . $this->sqlIsGeneric(). ',' ; } elseif( true == array_key_exists( 'IsGeneric', $this->getChangedColumns() ) ) { $strSql .= ' is_generic = ' . $this->sqlIsGeneric() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_enabled = ' . $this->sqlAutoEnabled(). ',' ; } elseif( true == array_key_exists( 'AutoEnabled', $this->getChangedColumns() ) ) { $strSql .= ' auto_enabled = ' . $this->sqlAutoEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_product_id = ' . $this->sqlInsuranceProductId(). ',' ; } elseif( true == array_key_exists( 'InsuranceProductId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_product_id = ' . $this->sqlInsuranceProductId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_property_id' => $this->getInsurancePropertyId(),
			'insurance_carrier_id' => $this->getInsuranceCarrierId(),
			'insurance_policy_type_id' => $this->getInsurancePolicyTypeId(),
			'insurance_carrier_classification_type_id' => $this->getInsuranceCarrierClassificationTypeId(),
			'minimum_billing_frequency_id' => $this->getMinimumBillingFrequencyId(),
			'default_liability_insurance_carrier_limit_id' => $this->getDefaultLiabilityInsuranceCarrierLimitId(),
			'personal_insurance_carrier_limit_id' => $this->getPersonalInsuranceCarrierLimitId(),
			'deductible_insurance_carrier_limit_id' => $this->getDeductibleInsuranceCarrierLimitId(),
			'min_liability_insurance_carrier_limit_id' => $this->getMinLiabilityInsuranceCarrierLimitId(),
			'max_liability_insurance_carrier_limit_id' => $this->getMaxLiabilityInsuranceCarrierLimitId(),
			'min_deductible_insurance_carrier_limit_id' => $this->getMinDeductibleInsuranceCarrierLimitId(),
			'default_medical_payment_limit_id' => $this->getDefaultMedicalPaymentLimitId(),
			'enhancement_endorsement_id' => $this->getEnhancementEndorsementId(),
			'sub_domain' => $this->getSubDomain(),
			'is_allowed_liability_only' => $this->getIsAllowedLiabilityOnly(),
			'is_generic' => $this->getIsGeneric(),
			'is_active' => $this->getIsActive(),
			'auto_enabled' => $this->getAutoEnabled(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'insurance_product_id' => $this->getInsuranceProductId()
		);
	}

}
?>