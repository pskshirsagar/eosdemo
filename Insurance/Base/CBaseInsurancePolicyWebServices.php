<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyWebServices
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyWebServices extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyWebService[]
	 */
	public static function fetchInsurancePolicyWebServices( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsurancePolicyWebService', $objDatabase );
	}

	/**
	 * @return CInsurancePolicyWebService
	 */
	public static function fetchInsurancePolicyWebService( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsurancePolicyWebService', $objDatabase );
	}

	public static function fetchInsurancePolicyWebServiceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_web_services', $objDatabase );
	}

	public static function fetchInsurancePolicyWebServiceById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyWebService( sprintf( 'SELECT * FROM insurance_policy_web_services WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>