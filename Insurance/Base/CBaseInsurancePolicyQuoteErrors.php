<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyQuoteErrors
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyQuoteErrors extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyQuoteError[]
	 */
	public static function fetchInsurancePolicyQuoteErrors( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsurancePolicyQuoteError', $objDatabase );
	}

	/**
	 * @return CInsurancePolicyQuoteError
	 */
	public static function fetchInsurancePolicyQuoteError( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsurancePolicyQuoteError', $objDatabase );
	}

	public static function fetchInsurancePolicyQuoteErrorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_quote_errors', $objDatabase );
	}

	public static function fetchInsurancePolicyQuoteErrorById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyQuoteError( sprintf( 'SELECT * FROM insurance_policy_quote_errors WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsurancePolicyQuoteErrorsByQuoteId( $intQuoteId, $objDatabase ) {
		return self::fetchInsurancePolicyQuoteErrors( sprintf( 'SELECT * FROM insurance_policy_quote_errors WHERE quote_id = %d', ( int ) $intQuoteId ), $objDatabase );
	}

}
?>