<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyQuoteHistories
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyQuoteHistories extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyQuoteHistory[]
	 */
	public static function fetchInsurancePolicyQuoteHistories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsurancePolicyQuoteHistory::class, $objDatabase );
	}

	/**
	 * @return CInsurancePolicyQuoteHistory
	 */
	public static function fetchInsurancePolicyQuoteHistory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsurancePolicyQuoteHistory::class, $objDatabase );
	}

	public static function fetchInsurancePolicyQuoteHistoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_quote_histories', $objDatabase );
	}

	public static function fetchInsurancePolicyQuoteHistoryById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyQuoteHistory( sprintf( 'SELECT * FROM insurance_policy_quote_histories WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchInsurancePolicyQuoteHistoriesByInsurancePolicyQuoteId( $intInsurancePolicyQuoteId, $objDatabase ) {
		return self::fetchInsurancePolicyQuoteHistories( sprintf( 'SELECT * FROM insurance_policy_quote_histories WHERE insurance_policy_quote_id = %d', $intInsurancePolicyQuoteId ), $objDatabase );
	}

	public static function fetchInsurancePolicyQuoteHistoriesByLeadSourceTypeId( $intLeadSourceTypeId, $objDatabase ) {
		return self::fetchInsurancePolicyQuoteHistories( sprintf( 'SELECT * FROM insurance_policy_quote_histories WHERE lead_source_type_id = %d', $intLeadSourceTypeId ), $objDatabase );
	}

}
?>