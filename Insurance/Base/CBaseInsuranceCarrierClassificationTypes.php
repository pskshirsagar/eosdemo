<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceCarrierClassificationTypes
 * Do not add any new functions to this class.
 */

class CBaseInsuranceCarrierClassificationTypes extends CEosPluralBase {

	/**
	 * @return CInsuranceCarrierClassificationType[]
	 */
	public static function fetchInsuranceCarrierClassificationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceCarrierClassificationType', $objDatabase );
	}

	/**
	 * @return CInsuranceCarrierClassificationType
	 */
	public static function fetchInsuranceCarrierClassificationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceCarrierClassificationType', $objDatabase );
	}

	public static function fetchInsuranceCarrierClassificationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_carrier_classification_types', $objDatabase );
	}

	public static function fetchInsuranceCarrierClassificationTypeById( $intId, $objDatabase ) {
		return self::fetchInsuranceCarrierClassificationType( sprintf( 'SELECT * FROM insurance_carrier_classification_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierClassificationTypesByInsuranceCarrierId( $intInsuranceCarrierId, $objDatabase ) {
		return self::fetchInsuranceCarrierClassificationTypes( sprintf( 'SELECT * FROM insurance_carrier_classification_types WHERE insurance_carrier_id = %d', ( int ) $intInsuranceCarrierId ), $objDatabase );
	}

}
?>