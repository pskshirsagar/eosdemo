<?php

class CBaseInsuranceCarrierLimit extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_carrier_limits';

	protected $m_intId;
	protected $m_intInsuranceCarrierId;
	protected $m_intInsurancePolicyTypeId;
	protected $m_intInsuranceAmountTypeId;
	protected $m_intInsuranceLimitTypeId;
	protected $m_fltBaseRate;
	protected $m_fltPremiumBaseMultiplier;
	protected $m_strStateCode;
	protected $m_strWindowStartDate;
	protected $m_strWindowEndDate;
	protected $m_intIsPublished;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_carrier_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierId', trim( $arrValues['insurance_carrier_id'] ) ); elseif( isset( $arrValues['insurance_carrier_id'] ) ) $this->setInsuranceCarrierId( $arrValues['insurance_carrier_id'] );
		if( isset( $arrValues['insurance_policy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyTypeId', trim( $arrValues['insurance_policy_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_type_id'] ) ) $this->setInsurancePolicyTypeId( $arrValues['insurance_policy_type_id'] );
		if( isset( $arrValues['insurance_amount_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceAmountTypeId', trim( $arrValues['insurance_amount_type_id'] ) ); elseif( isset( $arrValues['insurance_amount_type_id'] ) ) $this->setInsuranceAmountTypeId( $arrValues['insurance_amount_type_id'] );
		if( isset( $arrValues['insurance_limit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceLimitTypeId', trim( $arrValues['insurance_limit_type_id'] ) ); elseif( isset( $arrValues['insurance_limit_type_id'] ) ) $this->setInsuranceLimitTypeId( $arrValues['insurance_limit_type_id'] );
		if( isset( $arrValues['base_rate'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRate', trim( $arrValues['base_rate'] ) ); elseif( isset( $arrValues['base_rate'] ) ) $this->setBaseRate( $arrValues['base_rate'] );
		if( isset( $arrValues['premium_base_multiplier'] ) && $boolDirectSet ) $this->set( 'm_fltPremiumBaseMultiplier', trim( $arrValues['premium_base_multiplier'] ) ); elseif( isset( $arrValues['premium_base_multiplier'] ) ) $this->setPremiumBaseMultiplier( $arrValues['premium_base_multiplier'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['window_start_date'] ) && $boolDirectSet ) $this->set( 'm_strWindowStartDate', trim( $arrValues['window_start_date'] ) ); elseif( isset( $arrValues['window_start_date'] ) ) $this->setWindowStartDate( $arrValues['window_start_date'] );
		if( isset( $arrValues['window_end_date'] ) && $boolDirectSet ) $this->set( 'm_strWindowEndDate', trim( $arrValues['window_end_date'] ) ); elseif( isset( $arrValues['window_end_date'] ) ) $this->setWindowEndDate( $arrValues['window_end_date'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsuranceCarrierId( $intInsuranceCarrierId ) {
		$this->set( 'm_intInsuranceCarrierId', CStrings::strToIntDef( $intInsuranceCarrierId, NULL, false ) );
	}

	public function getInsuranceCarrierId() {
		return $this->m_intInsuranceCarrierId;
	}

	public function sqlInsuranceCarrierId() {
		return ( true == isset( $this->m_intInsuranceCarrierId ) ) ? ( string ) $this->m_intInsuranceCarrierId : 'NULL';
	}

	public function setInsurancePolicyTypeId( $intInsurancePolicyTypeId ) {
		$this->set( 'm_intInsurancePolicyTypeId', CStrings::strToIntDef( $intInsurancePolicyTypeId, NULL, false ) );
	}

	public function getInsurancePolicyTypeId() {
		return $this->m_intInsurancePolicyTypeId;
	}

	public function sqlInsurancePolicyTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyTypeId ) ) ? ( string ) $this->m_intInsurancePolicyTypeId : 'NULL';
	}

	public function setInsuranceAmountTypeId( $intInsuranceAmountTypeId ) {
		$this->set( 'm_intInsuranceAmountTypeId', CStrings::strToIntDef( $intInsuranceAmountTypeId, NULL, false ) );
	}

	public function getInsuranceAmountTypeId() {
		return $this->m_intInsuranceAmountTypeId;
	}

	public function sqlInsuranceAmountTypeId() {
		return ( true == isset( $this->m_intInsuranceAmountTypeId ) ) ? ( string ) $this->m_intInsuranceAmountTypeId : 'NULL';
	}

	public function setInsuranceLimitTypeId( $intInsuranceLimitTypeId ) {
		$this->set( 'm_intInsuranceLimitTypeId', CStrings::strToIntDef( $intInsuranceLimitTypeId, NULL, false ) );
	}

	public function getInsuranceLimitTypeId() {
		return $this->m_intInsuranceLimitTypeId;
	}

	public function sqlInsuranceLimitTypeId() {
		return ( true == isset( $this->m_intInsuranceLimitTypeId ) ) ? ( string ) $this->m_intInsuranceLimitTypeId : 'NULL';
	}

	public function setBaseRate( $fltBaseRate ) {
		$this->set( 'm_fltBaseRate', CStrings::strToFloatDef( $fltBaseRate, NULL, false, 2 ) );
	}

	public function getBaseRate() {
		return $this->m_fltBaseRate;
	}

	public function sqlBaseRate() {
		return ( true == isset( $this->m_fltBaseRate ) ) ? ( string ) $this->m_fltBaseRate : 'NULL';
	}

	public function setPremiumBaseMultiplier( $fltPremiumBaseMultiplier ) {
		$this->set( 'm_fltPremiumBaseMultiplier', CStrings::strToFloatDef( $fltPremiumBaseMultiplier, NULL, false, 4 ) );
	}

	public function getPremiumBaseMultiplier() {
		return $this->m_fltPremiumBaseMultiplier;
	}

	public function sqlPremiumBaseMultiplier() {
		return ( true == isset( $this->m_fltPremiumBaseMultiplier ) ) ? ( string ) $this->m_fltPremiumBaseMultiplier : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 10, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setWindowStartDate( $strWindowStartDate ) {
		$this->set( 'm_strWindowStartDate', CStrings::strTrimDef( $strWindowStartDate, -1, NULL, true ) );
	}

	public function getWindowStartDate() {
		return $this->m_strWindowStartDate;
	}

	public function sqlWindowStartDate() {
		return ( true == isset( $this->m_strWindowStartDate ) ) ? '\'' . $this->m_strWindowStartDate . '\'' : 'NULL';
	}

	public function setWindowEndDate( $strWindowEndDate ) {
		$this->set( 'm_strWindowEndDate', CStrings::strTrimDef( $strWindowEndDate, -1, NULL, true ) );
	}

	public function getWindowEndDate() {
		return $this->m_strWindowEndDate;
	}

	public function sqlWindowEndDate() {
		return ( true == isset( $this->m_strWindowEndDate ) ) ? '\'' . $this->m_strWindowEndDate . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, insurance_carrier_id, insurance_policy_type_id, insurance_amount_type_id, insurance_limit_type_id, base_rate, premium_base_multiplier, state_code, window_start_date, window_end_date, is_published, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlInsuranceCarrierId() . ', ' .
 						$this->sqlInsurancePolicyTypeId() . ', ' .
 						$this->sqlInsuranceAmountTypeId() . ', ' .
 						$this->sqlInsuranceLimitTypeId() . ', ' .
 						$this->sqlBaseRate() . ', ' .
 						$this->sqlPremiumBaseMultiplier() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlWindowStartDate() . ', ' .
 						$this->sqlWindowEndDate() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; } elseif( true == array_key_exists( 'InsuranceCarrierId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId() . ','; } elseif( true == array_key_exists( 'InsurancePolicyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_amount_type_id = ' . $this->sqlInsuranceAmountTypeId() . ','; } elseif( true == array_key_exists( 'InsuranceAmountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_amount_type_id = ' . $this->sqlInsuranceAmountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_limit_type_id = ' . $this->sqlInsuranceLimitTypeId() . ','; } elseif( true == array_key_exists( 'InsuranceLimitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_limit_type_id = ' . $this->sqlInsuranceLimitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rate = ' . $this->sqlBaseRate() . ','; } elseif( true == array_key_exists( 'BaseRate', $this->getChangedColumns() ) ) { $strSql .= ' base_rate = ' . $this->sqlBaseRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' premium_base_multiplier = ' . $this->sqlPremiumBaseMultiplier() . ','; } elseif( true == array_key_exists( 'PremiumBaseMultiplier', $this->getChangedColumns() ) ) { $strSql .= ' premium_base_multiplier = ' . $this->sqlPremiumBaseMultiplier() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' window_start_date = ' . $this->sqlWindowStartDate() . ','; } elseif( true == array_key_exists( 'WindowStartDate', $this->getChangedColumns() ) ) { $strSql .= ' window_start_date = ' . $this->sqlWindowStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' window_end_date = ' . $this->sqlWindowEndDate() . ','; } elseif( true == array_key_exists( 'WindowEndDate', $this->getChangedColumns() ) ) { $strSql .= ' window_end_date = ' . $this->sqlWindowEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_carrier_id' => $this->getInsuranceCarrierId(),
			'insurance_policy_type_id' => $this->getInsurancePolicyTypeId(),
			'insurance_amount_type_id' => $this->getInsuranceAmountTypeId(),
			'insurance_limit_type_id' => $this->getInsuranceLimitTypeId(),
			'base_rate' => $this->getBaseRate(),
			'premium_base_multiplier' => $this->getPremiumBaseMultiplier(),
			'state_code' => $this->getStateCode(),
			'window_start_date' => $this->getWindowStartDate(),
			'window_end_date' => $this->getWindowEndDate(),
			'is_published' => $this->getIsPublished(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>