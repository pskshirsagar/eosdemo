<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CBuildingCodeGradings
 * Do not add any new functions to this class.
 */

class CBaseBuildingCodeGradings extends CEosPluralBase {

	/**
	 * @return CBuildingCodeGrading[]
	 */
	public static function fetchBuildingCodeGradings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBuildingCodeGrading::class, $objDatabase );
	}

	/**
	 * @return CBuildingCodeGrading
	 */
	public static function fetchBuildingCodeGrading( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBuildingCodeGrading::class, $objDatabase );
	}

	public static function fetchBuildingCodeGradingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'building_code_gradings', $objDatabase );
	}

	public static function fetchBuildingCodeGradingById( $intId, $objDatabase ) {
		return self::fetchBuildingCodeGrading( sprintf( 'SELECT * FROM building_code_gradings WHERE id = %d', $intId ), $objDatabase );
	}

}
?>