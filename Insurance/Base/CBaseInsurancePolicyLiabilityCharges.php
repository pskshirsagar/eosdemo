<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyLiabilityCharges
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyLiabilityCharges extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyLiabilityCharge[]
	 */
	public static function fetchInsurancePolicyLiabilityCharges( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsurancePolicyLiabilityCharge::class, $objDatabase );
	}

	/**
	 * @return CInsurancePolicyLiabilityCharge
	 */
	public static function fetchInsurancePolicyLiabilityCharge( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsurancePolicyLiabilityCharge::class, $objDatabase );
	}

	public static function fetchInsurancePolicyLiabilityChargeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_liability_charges', $objDatabase );
	}

	public static function fetchInsurancePolicyLiabilityChargeById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyLiabilityCharge( sprintf( 'SELECT * FROM insurance_policy_liability_charges WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsurancePolicyLiabilityChargesByInsurancePolicyId( $intInsurancePolicyId, $objDatabase ) {
		return self::fetchInsurancePolicyLiabilityCharges( sprintf( 'SELECT * FROM insurance_policy_liability_charges WHERE insurance_policy_id = %d', ( int ) $intInsurancePolicyId ), $objDatabase );
	}

	public static function fetchInsurancePolicyLiabilityChargesByLiabilityChargeTransactionId( $intLiabilityChargeTransactionId, $objDatabase ) {
		return self::fetchInsurancePolicyLiabilityCharges( sprintf( 'SELECT * FROM insurance_policy_liability_charges WHERE liability_charge_transaction_id = %d', ( int ) $intLiabilityChargeTransactionId ), $objDatabase );
	}

	public static function fetchInsurancePolicyLiabilityChargesByPremiumTransactionId( $intPremiumTransactionId, $objDatabase ) {
		return self::fetchInsurancePolicyLiabilityCharges( sprintf( 'SELECT * FROM insurance_policy_liability_charges WHERE premium_transaction_id = %d', ( int ) $intPremiumTransactionId ), $objDatabase );
	}

	public static function fetchInsurancePolicyLiabilityChargesByInsurancePolicyLiabilityChargeId( $intInsurancePolicyLiabilityChargeId, $objDatabase ) {
		return self::fetchInsurancePolicyLiabilityCharges( sprintf( 'SELECT * FROM insurance_policy_liability_charges WHERE insurance_policy_liability_charge_id = %d', ( int ) $intInsurancePolicyLiabilityChargeId ), $objDatabase );
	}

	public static function fetchInsurancePolicyLiabilityChargesByInsuranceChargeTypeId( $intInsuranceChargeTypeId, $objDatabase ) {
		return self::fetchInsurancePolicyLiabilityCharges( sprintf( 'SELECT * FROM insurance_policy_liability_charges WHERE insurance_charge_type_id = %d', ( int ) $intInsuranceChargeTypeId ), $objDatabase );
	}

}
?>