<?php

class CBaseInsurancePolicyLiabilityCharge extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_policy_liability_charges';

	protected $m_intId;
	protected $m_intInsurancePolicyId;
	protected $m_intLiabilityChargeTransactionId;
	protected $m_fltLiabilityPremium;
	protected $m_strReturnedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intPremiumTransactionId;
	protected $m_strCoverageStartDate;
	protected $m_strCoverageEndDate;
	protected $m_fltChargeAmount;
	protected $m_strSettledOn;
	protected $m_intInsurancePolicyLiabilityChargeId;
	protected $m_intInsuranceChargeTypeId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyId', trim( $arrValues['insurance_policy_id'] ) ); elseif( isset( $arrValues['insurance_policy_id'] ) ) $this->setInsurancePolicyId( $arrValues['insurance_policy_id'] );
		if( isset( $arrValues['liability_charge_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intLiabilityChargeTransactionId', trim( $arrValues['liability_charge_transaction_id'] ) ); elseif( isset( $arrValues['liability_charge_transaction_id'] ) ) $this->setLiabilityChargeTransactionId( $arrValues['liability_charge_transaction_id'] );
		if( isset( $arrValues['liability_premium'] ) && $boolDirectSet ) $this->set( 'm_fltLiabilityPremium', trim( $arrValues['liability_premium'] ) ); elseif( isset( $arrValues['liability_premium'] ) ) $this->setLiabilityPremium( $arrValues['liability_premium'] );
		if( isset( $arrValues['returned_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnedOn', trim( $arrValues['returned_on'] ) ); elseif( isset( $arrValues['returned_on'] ) ) $this->setReturnedOn( $arrValues['returned_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['premium_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intPremiumTransactionId', trim( $arrValues['premium_transaction_id'] ) ); elseif( isset( $arrValues['premium_transaction_id'] ) ) $this->setPremiumTransactionId( $arrValues['premium_transaction_id'] );
		if( isset( $arrValues['coverage_start_date'] ) && $boolDirectSet ) $this->set( 'm_strCoverageStartDate', trim( $arrValues['coverage_start_date'] ) ); elseif( isset( $arrValues['coverage_start_date'] ) ) $this->setCoverageStartDate( $arrValues['coverage_start_date'] );
		if( isset( $arrValues['coverage_end_date'] ) && $boolDirectSet ) $this->set( 'm_strCoverageEndDate', trim( $arrValues['coverage_end_date'] ) ); elseif( isset( $arrValues['coverage_end_date'] ) ) $this->setCoverageEndDate( $arrValues['coverage_end_date'] );
		if( isset( $arrValues['charge_amount'] ) && $boolDirectSet ) $this->set( 'm_fltChargeAmount', trim( $arrValues['charge_amount'] ) ); elseif( isset( $arrValues['charge_amount'] ) ) $this->setChargeAmount( $arrValues['charge_amount'] );
		if( isset( $arrValues['settled_on'] ) && $boolDirectSet ) $this->set( 'm_strSettledOn', trim( $arrValues['settled_on'] ) ); elseif( isset( $arrValues['settled_on'] ) ) $this->setSettledOn( $arrValues['settled_on'] );
		if( isset( $arrValues['insurance_policy_liability_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyLiabilityChargeId', trim( $arrValues['insurance_policy_liability_charge_id'] ) ); elseif( isset( $arrValues['insurance_policy_liability_charge_id'] ) ) $this->setInsurancePolicyLiabilityChargeId( $arrValues['insurance_policy_liability_charge_id'] );
		if( isset( $arrValues['insurance_charge_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceChargeTypeId', trim( $arrValues['insurance_charge_type_id'] ) ); elseif( isset( $arrValues['insurance_charge_type_id'] ) ) $this->setInsuranceChargeTypeId( $arrValues['insurance_charge_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsurancePolicyId( $intInsurancePolicyId ) {
		$this->set( 'm_intInsurancePolicyId', CStrings::strToIntDef( $intInsurancePolicyId, NULL, false ) );
	}

	public function getInsurancePolicyId() {
		return $this->m_intInsurancePolicyId;
	}

	public function sqlInsurancePolicyId() {
		return ( true == isset( $this->m_intInsurancePolicyId ) ) ? ( string ) $this->m_intInsurancePolicyId : 'NULL';
	}

	public function setLiabilityChargeTransactionId( $intLiabilityChargeTransactionId ) {
		$this->set( 'm_intLiabilityChargeTransactionId', CStrings::strToIntDef( $intLiabilityChargeTransactionId, NULL, false ) );
	}

	public function getLiabilityChargeTransactionId() {
		return $this->m_intLiabilityChargeTransactionId;
	}

	public function sqlLiabilityChargeTransactionId() {
		return ( true == isset( $this->m_intLiabilityChargeTransactionId ) ) ? ( string ) $this->m_intLiabilityChargeTransactionId : 'NULL';
	}

	public function setLiabilityPremium( $fltLiabilityPremium ) {
		$this->set( 'm_fltLiabilityPremium', CStrings::strToFloatDef( $fltLiabilityPremium, NULL, false, 2 ) );
	}

	public function getLiabilityPremium() {
		return $this->m_fltLiabilityPremium;
	}

	public function sqlLiabilityPremium() {
		return ( true == isset( $this->m_fltLiabilityPremium ) ) ? ( string ) $this->m_fltLiabilityPremium : 'NULL';
	}

	public function setReturnedOn( $strReturnedOn ) {
		$this->set( 'm_strReturnedOn', CStrings::strTrimDef( $strReturnedOn, -1, NULL, true ) );
	}

	public function getReturnedOn() {
		return $this->m_strReturnedOn;
	}

	public function sqlReturnedOn() {
		return ( true == isset( $this->m_strReturnedOn ) ) ? '\'' . $this->m_strReturnedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPremiumTransactionId( $intPremiumTransactionId ) {
		$this->set( 'm_intPremiumTransactionId', CStrings::strToIntDef( $intPremiumTransactionId, NULL, false ) );
	}

	public function getPremiumTransactionId() {
		return $this->m_intPremiumTransactionId;
	}

	public function sqlPremiumTransactionId() {
		return ( true == isset( $this->m_intPremiumTransactionId ) ) ? ( string ) $this->m_intPremiumTransactionId : 'NULL';
	}

	public function setCoverageStartDate( $strCoverageStartDate ) {
		$this->set( 'm_strCoverageStartDate', CStrings::strTrimDef( $strCoverageStartDate, -1, NULL, true ) );
	}

	public function getCoverageStartDate() {
		return $this->m_strCoverageStartDate;
	}

	public function sqlCoverageStartDate() {
		return ( true == isset( $this->m_strCoverageStartDate ) ) ? '\'' . $this->m_strCoverageStartDate . '\'' : 'NULL';
	}

	public function setCoverageEndDate( $strCoverageEndDate ) {
		$this->set( 'm_strCoverageEndDate', CStrings::strTrimDef( $strCoverageEndDate, -1, NULL, true ) );
	}

	public function getCoverageEndDate() {
		return $this->m_strCoverageEndDate;
	}

	public function sqlCoverageEndDate() {
		return ( true == isset( $this->m_strCoverageEndDate ) ) ? '\'' . $this->m_strCoverageEndDate . '\'' : 'NULL';
	}

	public function setChargeAmount( $fltChargeAmount ) {
		$this->set( 'm_fltChargeAmount', CStrings::strToFloatDef( $fltChargeAmount, NULL, false, 2 ) );
	}

	public function getChargeAmount() {
		return $this->m_fltChargeAmount;
	}

	public function sqlChargeAmount() {
		return ( true == isset( $this->m_fltChargeAmount ) ) ? ( string ) $this->m_fltChargeAmount : 'NULL';
	}

	public function setSettledOn( $strSettledOn ) {
		$this->set( 'm_strSettledOn', CStrings::strTrimDef( $strSettledOn, -1, NULL, true ) );
	}

	public function getSettledOn() {
		return $this->m_strSettledOn;
	}

	public function sqlSettledOn() {
		return ( true == isset( $this->m_strSettledOn ) ) ? '\'' . $this->m_strSettledOn . '\'' : 'NULL';
	}

	public function setInsurancePolicyLiabilityChargeId( $intInsurancePolicyLiabilityChargeId ) {
		$this->set( 'm_intInsurancePolicyLiabilityChargeId', CStrings::strToIntDef( $intInsurancePolicyLiabilityChargeId, NULL, false ) );
	}

	public function getInsurancePolicyLiabilityChargeId() {
		return $this->m_intInsurancePolicyLiabilityChargeId;
	}

	public function sqlInsurancePolicyLiabilityChargeId() {
		return ( true == isset( $this->m_intInsurancePolicyLiabilityChargeId ) ) ? ( string ) $this->m_intInsurancePolicyLiabilityChargeId : 'NULL';
	}

	public function setInsuranceChargeTypeId( $intInsuranceChargeTypeId ) {
		$this->set( 'm_intInsuranceChargeTypeId', CStrings::strToIntDef( $intInsuranceChargeTypeId, NULL, false ) );
	}

	public function getInsuranceChargeTypeId() {
		return $this->m_intInsuranceChargeTypeId;
	}

	public function sqlInsuranceChargeTypeId() {
		return ( true == isset( $this->m_intInsuranceChargeTypeId ) ) ? ( string ) $this->m_intInsuranceChargeTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, insurance_policy_id, liability_charge_transaction_id, liability_premium, returned_on, updated_by, updated_on, created_by, created_on, premium_transaction_id, coverage_start_date, coverage_end_date, charge_amount, settled_on, insurance_policy_liability_charge_id, insurance_charge_type_id )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlInsurancePolicyId() . ', ' .
 						$this->sqlLiabilityChargeTransactionId() . ', ' .
 						$this->sqlLiabilityPremium() . ', ' .
 						$this->sqlReturnedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlPremiumTransactionId() . ', ' .
 						$this->sqlCoverageStartDate() . ', ' .
 						$this->sqlCoverageEndDate() . ', ' .
 						$this->sqlChargeAmount() . ', ' .
 						$this->sqlSettledOn() . ', ' .
 						$this->sqlInsurancePolicyLiabilityChargeId() . ', ' .
 						$this->sqlInsuranceChargeTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; } elseif( true == array_key_exists( 'InsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' liability_charge_transaction_id = ' . $this->sqlLiabilityChargeTransactionId() . ','; } elseif( true == array_key_exists( 'LiabilityChargeTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' liability_charge_transaction_id = ' . $this->sqlLiabilityChargeTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' liability_premium = ' . $this->sqlLiabilityPremium() . ','; } elseif( true == array_key_exists( 'LiabilityPremium', $this->getChangedColumns() ) ) { $strSql .= ' liability_premium = ' . $this->sqlLiabilityPremium() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; } elseif( true == array_key_exists( 'ReturnedOn', $this->getChangedColumns() ) ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' premium_transaction_id = ' . $this->sqlPremiumTransactionId() . ','; } elseif( true == array_key_exists( 'PremiumTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' premium_transaction_id = ' . $this->sqlPremiumTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' coverage_start_date = ' . $this->sqlCoverageStartDate() . ','; } elseif( true == array_key_exists( 'CoverageStartDate', $this->getChangedColumns() ) ) { $strSql .= ' coverage_start_date = ' . $this->sqlCoverageStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' coverage_end_date = ' . $this->sqlCoverageEndDate() . ','; } elseif( true == array_key_exists( 'CoverageEndDate', $this->getChangedColumns() ) ) { $strSql .= ' coverage_end_date = ' . $this->sqlCoverageEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_amount = ' . $this->sqlChargeAmount() . ','; } elseif( true == array_key_exists( 'ChargeAmount', $this->getChangedColumns() ) ) { $strSql .= ' charge_amount = ' . $this->sqlChargeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' settled_on = ' . $this->sqlSettledOn() . ','; } elseif( true == array_key_exists( 'SettledOn', $this->getChangedColumns() ) ) { $strSql .= ' settled_on = ' . $this->sqlSettledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_liability_charge_id = ' . $this->sqlInsurancePolicyLiabilityChargeId() . ','; } elseif( true == array_key_exists( 'InsurancePolicyLiabilityChargeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_liability_charge_id = ' . $this->sqlInsurancePolicyLiabilityChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_charge_type_id = ' . $this->sqlInsuranceChargeTypeId() . ','; } elseif( true == array_key_exists( 'InsuranceChargeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_charge_type_id = ' . $this->sqlInsuranceChargeTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_policy_id' => $this->getInsurancePolicyId(),
			'liability_charge_transaction_id' => $this->getLiabilityChargeTransactionId(),
			'liability_premium' => $this->getLiabilityPremium(),
			'returned_on' => $this->getReturnedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'premium_transaction_id' => $this->getPremiumTransactionId(),
			'coverage_start_date' => $this->getCoverageStartDate(),
			'coverage_end_date' => $this->getCoverageEndDate(),
			'charge_amount' => $this->getChargeAmount(),
			'settled_on' => $this->getSettledOn(),
			'insurance_policy_liability_charge_id' => $this->getInsurancePolicyLiabilityChargeId(),
			'insurance_charge_type_id' => $this->getInsuranceChargeTypeId()
		);
	}

}
?>