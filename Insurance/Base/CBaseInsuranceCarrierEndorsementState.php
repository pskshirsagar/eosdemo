<?php

class CBaseInsuranceCarrierEndorsementState extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_carrier_endorsement_states';

	protected $m_intId;
	protected $m_intInsuranceCarrierId;
	protected $m_intInsurancePolicyTypeId;
	protected $m_intInsuranceCarrierEndorsementTypeId;
	protected $m_strStateCode;
	protected $m_fltBaseRate;
	protected $m_intIsMandatory;
	protected $m_intIsAvailable;
	protected $m_intIsVisible;

	public function __construct() {
		parent::__construct();

		$this->m_intIsMandatory = '0';
		$this->m_intIsAvailable = '1';
		$this->m_intIsVisible = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_carrier_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierId', trim( $arrValues['insurance_carrier_id'] ) ); elseif( isset( $arrValues['insurance_carrier_id'] ) ) $this->setInsuranceCarrierId( $arrValues['insurance_carrier_id'] );
		if( isset( $arrValues['insurance_policy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyTypeId', trim( $arrValues['insurance_policy_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_type_id'] ) ) $this->setInsurancePolicyTypeId( $arrValues['insurance_policy_type_id'] );
		if( isset( $arrValues['insurance_carrier_endorsement_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierEndorsementTypeId', trim( $arrValues['insurance_carrier_endorsement_type_id'] ) ); elseif( isset( $arrValues['insurance_carrier_endorsement_type_id'] ) ) $this->setInsuranceCarrierEndorsementTypeId( $arrValues['insurance_carrier_endorsement_type_id'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['base_rate'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRate', trim( $arrValues['base_rate'] ) ); elseif( isset( $arrValues['base_rate'] ) ) $this->setBaseRate( $arrValues['base_rate'] );
		if( isset( $arrValues['is_mandatory'] ) && $boolDirectSet ) $this->set( 'm_intIsMandatory', trim( $arrValues['is_mandatory'] ) ); elseif( isset( $arrValues['is_mandatory'] ) ) $this->setIsMandatory( $arrValues['is_mandatory'] );
		if( isset( $arrValues['is_available'] ) && $boolDirectSet ) $this->set( 'm_intIsAvailable', trim( $arrValues['is_available'] ) ); elseif( isset( $arrValues['is_available'] ) ) $this->setIsAvailable( $arrValues['is_available'] );
		if( isset( $arrValues['is_visible'] ) && $boolDirectSet ) $this->set( 'm_intIsVisible', trim( $arrValues['is_visible'] ) ); elseif( isset( $arrValues['is_visible'] ) ) $this->setIsVisible( $arrValues['is_visible'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsuranceCarrierId( $intInsuranceCarrierId ) {
		$this->set( 'm_intInsuranceCarrierId', CStrings::strToIntDef( $intInsuranceCarrierId, NULL, false ) );
	}

	public function getInsuranceCarrierId() {
		return $this->m_intInsuranceCarrierId;
	}

	public function sqlInsuranceCarrierId() {
		return ( true == isset( $this->m_intInsuranceCarrierId ) ) ? ( string ) $this->m_intInsuranceCarrierId : 'NULL';
	}

	public function setInsurancePolicyTypeId( $intInsurancePolicyTypeId ) {
		$this->set( 'm_intInsurancePolicyTypeId', CStrings::strToIntDef( $intInsurancePolicyTypeId, NULL, false ) );
	}

	public function getInsurancePolicyTypeId() {
		return $this->m_intInsurancePolicyTypeId;
	}

	public function sqlInsurancePolicyTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyTypeId ) ) ? ( string ) $this->m_intInsurancePolicyTypeId : 'NULL';
	}

	public function setInsuranceCarrierEndorsementTypeId( $intInsuranceCarrierEndorsementTypeId ) {
		$this->set( 'm_intInsuranceCarrierEndorsementTypeId', CStrings::strToIntDef( $intInsuranceCarrierEndorsementTypeId, NULL, false ) );
	}

	public function getInsuranceCarrierEndorsementTypeId() {
		return $this->m_intInsuranceCarrierEndorsementTypeId;
	}

	public function sqlInsuranceCarrierEndorsementTypeId() {
		return ( true == isset( $this->m_intInsuranceCarrierEndorsementTypeId ) ) ? ( string ) $this->m_intInsuranceCarrierEndorsementTypeId : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 10, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setBaseRate( $fltBaseRate ) {
		$this->set( 'm_fltBaseRate', CStrings::strToFloatDef( $fltBaseRate, NULL, false, 2 ) );
	}

	public function getBaseRate() {
		return $this->m_fltBaseRate;
	}

	public function sqlBaseRate() {
		return ( true == isset( $this->m_fltBaseRate ) ) ? ( string ) $this->m_fltBaseRate : 'NULL';
	}

	public function setIsMandatory( $intIsMandatory ) {
		$this->set( 'm_intIsMandatory', CStrings::strToIntDef( $intIsMandatory, NULL, false ) );
	}

	public function getIsMandatory() {
		return $this->m_intIsMandatory;
	}

	public function sqlIsMandatory() {
		return ( true == isset( $this->m_intIsMandatory ) ) ? ( string ) $this->m_intIsMandatory : '0';
	}

	public function setIsAvailable( $intIsAvailable ) {
		$this->set( 'm_intIsAvailable', CStrings::strToIntDef( $intIsAvailable, NULL, false ) );
	}

	public function getIsAvailable() {
		return $this->m_intIsAvailable;
	}

	public function sqlIsAvailable() {
		return ( true == isset( $this->m_intIsAvailable ) ) ? ( string ) $this->m_intIsAvailable : '1';
	}

	public function setIsVisible( $intIsVisible ) {
		$this->set( 'm_intIsVisible', CStrings::strToIntDef( $intIsVisible, NULL, false ) );
	}

	public function getIsVisible() {
		return $this->m_intIsVisible;
	}

	public function sqlIsVisible() {
		return ( true == isset( $this->m_intIsVisible ) ) ? ( string ) $this->m_intIsVisible : '1';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_carrier_id' => $this->getInsuranceCarrierId(),
			'insurance_policy_type_id' => $this->getInsurancePolicyTypeId(),
			'insurance_carrier_endorsement_type_id' => $this->getInsuranceCarrierEndorsementTypeId(),
			'state_code' => $this->getStateCode(),
			'base_rate' => $this->getBaseRate(),
			'is_mandatory' => $this->getIsMandatory(),
			'is_available' => $this->getIsAvailable(),
			'is_visible' => $this->getIsVisible()
		);
	}

}
?>