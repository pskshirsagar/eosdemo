<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceInvoiceDetails
 * Do not add any new functions to this class.
 */

class CBaseInsuranceInvoiceDetails extends CEosPluralBase {

	/**
	 * @return CInsuranceInvoiceDetail[]
	 */
	public static function fetchInsuranceInvoiceDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceInvoiceDetail::class, $objDatabase );
	}

	/**
	 * @return CInsuranceInvoiceDetail
	 */
	public static function fetchInsuranceInvoiceDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceInvoiceDetail::class, $objDatabase );
	}

	public static function fetchInsuranceInvoiceDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_invoice_details', $objDatabase );
	}

	public static function fetchInsuranceInvoiceDetailById( $intId, $objDatabase ) {
		return self::fetchInsuranceInvoiceDetail( sprintf( 'SELECT * FROM insurance_invoice_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceInvoiceDetailsByInsurancePolicyId( $intInsurancePolicyId, $objDatabase ) {
		return self::fetchInsuranceInvoiceDetails( sprintf( 'SELECT * FROM insurance_invoice_details WHERE insurance_policy_id = %d', ( int ) $intInsurancePolicyId ), $objDatabase );
	}

	public static function fetchInsuranceInvoiceDetailsByInsuranceChargeId( $intInsuranceChargeId, $objDatabase ) {
		return self::fetchInsuranceInvoiceDetails( sprintf( 'SELECT * FROM insurance_invoice_details WHERE insurance_charge_id = %d', ( int ) $intInsuranceChargeId ), $objDatabase );
	}

	public static function fetchInsuranceInvoiceDetailsByInsuranceInvoiceId( $intInsuranceInvoiceId, $objDatabase ) {
		return self::fetchInsuranceInvoiceDetails( sprintf( 'SELECT * FROM insurance_invoice_details WHERE insurance_invoice_id = %d', ( int ) $intInsuranceInvoiceId ), $objDatabase );
	}

	public static function fetchInsuranceInvoiceDetailsByInsuranceInvoiceDetailResultTypeId( $intInsuranceInvoiceDetailResultTypeId, $objDatabase ) {
		return self::fetchInsuranceInvoiceDetails( sprintf( 'SELECT * FROM insurance_invoice_details WHERE insurance_invoice_detail_result_type_id = %d', ( int ) $intInsuranceInvoiceDetailResultTypeId ), $objDatabase );
	}

	public static function fetchInsuranceInvoiceDetailsByInitialInsuranceInvoiceDetailResultTypeId( $intInitialInsuranceInvoiceDetailResultTypeId, $objDatabase ) {
		return self::fetchInsuranceInvoiceDetails( sprintf( 'SELECT * FROM insurance_invoice_details WHERE initial_insurance_invoice_detail_result_type_id = %d', ( int ) $intInitialInsuranceInvoiceDetailResultTypeId ), $objDatabase );
	}

}
?>