<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceCustomPolicyExports
 * Do not add any new functions to this class.
 */

class CBaseInsuranceCustomPolicyExports extends CEosPluralBase {

	/**
	 * @return CInsuranceCustomPolicyExport[]
	 */
	public static function fetchInsuranceCustomPolicyExports( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceCustomPolicyExport::class, $objDatabase );
	}

	/**
	 * @return CInsuranceCustomPolicyExport
	 */
	public static function fetchInsuranceCustomPolicyExport( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceCustomPolicyExport::class, $objDatabase );
	}

	public static function fetchInsuranceCustomPolicyExportCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_custom_policy_exports', $objDatabase );
	}

	public static function fetchInsuranceCustomPolicyExportById( $intId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicyExport( sprintf( 'SELECT * FROM insurance_custom_policy_exports WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPolicyExportsByInsuranceCustomPolicyId( $intInsuranceCustomPolicyId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicyExports( sprintf( 'SELECT * FROM insurance_custom_policy_exports WHERE insurance_custom_policy_id = %d', ( int ) $intInsuranceCustomPolicyId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPolicyExportsByInsuranceExportBatchId( $intInsuranceExportBatchId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicyExports( sprintf( 'SELECT * FROM insurance_custom_policy_exports WHERE insurance_export_batch_id = %d', ( int ) $intInsuranceExportBatchId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPolicyExportsByInsuranceBatchId( $intInsuranceBatchId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicyExports( sprintf( 'SELECT * FROM insurance_custom_policy_exports WHERE insurance_batch_id = %d', ( int ) $intInsuranceBatchId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPolicyExportsByInsuranceExportTypeId( $intInsuranceExportTypeId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicyExports( sprintf( 'SELECT * FROM insurance_custom_policy_exports WHERE insurance_export_type_id = %d', ( int ) $intInsuranceExportTypeId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPolicyExportsByInsuranceCustomPolicyExportId( $intInsuranceCustomPolicyExportId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicyExports( sprintf( 'SELECT * FROM insurance_custom_policy_exports WHERE insurance_custom_policy_export_id = %d', ( int ) $intInsuranceCustomPolicyExportId ), $objDatabase );
	}

}
?>