<?php

class CBaseInsurancePolicyQuote extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.insurance_policy_quotes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intEntityId;
	protected $m_intInsurancePolicyId;
	protected $m_intInsurancePropertyId;
	protected $m_strEmailAddress;
	protected $m_intLeadSourceTypeId;
	protected $m_intCompanyQuoteNumber;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strAwsSentOn;
	protected $m_intLeaseId;
	protected $m_intOriginalLeadSourceTypeId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCustomerId;
	protected $m_strLeaseStartDate;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['entity_id'] ) && $boolDirectSet ) $this->set( 'm_intEntityId', trim( $arrValues['entity_id'] ) ); elseif( isset( $arrValues['entity_id'] ) ) $this->setEntityId( $arrValues['entity_id'] );
		if( isset( $arrValues['insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyId', trim( $arrValues['insurance_policy_id'] ) ); elseif( isset( $arrValues['insurance_policy_id'] ) ) $this->setInsurancePolicyId( $arrValues['insurance_policy_id'] );
		if( isset( $arrValues['insurance_property_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePropertyId', trim( $arrValues['insurance_property_id'] ) ); elseif( isset( $arrValues['insurance_property_id'] ) ) $this->setInsurancePropertyId( $arrValues['insurance_property_id'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['lead_source_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeadSourceTypeId', trim( $arrValues['lead_source_type_id'] ) ); elseif( isset( $arrValues['lead_source_type_id'] ) ) $this->setLeadSourceTypeId( $arrValues['lead_source_type_id'] );
		if( isset( $arrValues['company_quote_number'] ) && $boolDirectSet ) $this->set( 'm_intCompanyQuoteNumber', trim( $arrValues['company_quote_number'] ) ); elseif( isset( $arrValues['company_quote_number'] ) ) $this->setCompanyQuoteNumber( $arrValues['company_quote_number'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['aws_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strAwsSentOn', trim( $arrValues['aws_sent_on'] ) ); elseif( isset( $arrValues['aws_sent_on'] ) ) $this->setAwsSentOn( $arrValues['aws_sent_on'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['original_lead_source_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalLeadSourceTypeId', trim( $arrValues['original_lead_source_type_id'] ) ); elseif( isset( $arrValues['original_lead_source_type_id'] ) ) $this->setOriginalLeadSourceTypeId( $arrValues['original_lead_source_type_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartDate', trim( $arrValues['lease_start_date'] ) ); elseif( isset( $arrValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrValues['lease_start_date'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setEntityId( $intEntityId ) {
		$this->set( 'm_intEntityId', CStrings::strToIntDef( $intEntityId, NULL, false ) );
	}

	public function getEntityId() {
		return $this->m_intEntityId;
	}

	public function sqlEntityId() {
		return ( true == isset( $this->m_intEntityId ) ) ? ( string ) $this->m_intEntityId : 'NULL';
	}

	public function setInsurancePolicyId( $intInsurancePolicyId ) {
		$this->set( 'm_intInsurancePolicyId', CStrings::strToIntDef( $intInsurancePolicyId, NULL, false ) );
	}

	public function getInsurancePolicyId() {
		return $this->m_intInsurancePolicyId;
	}

	public function sqlInsurancePolicyId() {
		return ( true == isset( $this->m_intInsurancePolicyId ) ) ? ( string ) $this->m_intInsurancePolicyId : 'NULL';
	}

	public function setInsurancePropertyId( $intInsurancePropertyId ) {
		$this->set( 'm_intInsurancePropertyId', CStrings::strToIntDef( $intInsurancePropertyId, NULL, false ) );
	}

	public function getInsurancePropertyId() {
		return $this->m_intInsurancePropertyId;
	}

	public function sqlInsurancePropertyId() {
		return ( true == isset( $this->m_intInsurancePropertyId ) ) ? ( string ) $this->m_intInsurancePropertyId : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setLeadSourceTypeId( $intLeadSourceTypeId ) {
		$this->set( 'm_intLeadSourceTypeId', CStrings::strToIntDef( $intLeadSourceTypeId, NULL, false ) );
	}

	public function getLeadSourceTypeId() {
		return $this->m_intLeadSourceTypeId;
	}

	public function sqlLeadSourceTypeId() {
		return ( true == isset( $this->m_intLeadSourceTypeId ) ) ? ( string ) $this->m_intLeadSourceTypeId : 'NULL';
	}

	public function setCompanyQuoteNumber( $intCompanyQuoteNumber ) {
		$this->set( 'm_intCompanyQuoteNumber', CStrings::strToIntDef( $intCompanyQuoteNumber, NULL, false ) );
	}

	public function getCompanyQuoteNumber() {
		return $this->m_intCompanyQuoteNumber;
	}

	public function sqlCompanyQuoteNumber() {
		return ( true == isset( $this->m_intCompanyQuoteNumber ) ) ? ( string ) $this->m_intCompanyQuoteNumber : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAwsSentOn( $strAwsSentOn ) {
		$this->set( 'm_strAwsSentOn', CStrings::strTrimDef( $strAwsSentOn, -1, NULL, true ) );
	}

	public function getAwsSentOn() {
		return $this->m_strAwsSentOn;
	}

	public function sqlAwsSentOn() {
		return ( true == isset( $this->m_strAwsSentOn ) ) ? '\'' . $this->m_strAwsSentOn . '\'' : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setOriginalLeadSourceTypeId( $intOriginalLeadSourceTypeId ) {
		$this->set( 'm_intOriginalLeadSourceTypeId', CStrings::strToIntDef( $intOriginalLeadSourceTypeId, NULL, false ) );
	}

	public function getOriginalLeadSourceTypeId() {
		return $this->m_intOriginalLeadSourceTypeId;
	}

	public function sqlOriginalLeadSourceTypeId() {
		return ( true == isset( $this->m_intOriginalLeadSourceTypeId ) ) ? ( string ) $this->m_intOriginalLeadSourceTypeId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->set( 'm_strLeaseStartDate', CStrings::strTrimDef( $strLeaseStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function sqlLeaseStartDate() {
		return ( true == isset( $this->m_strLeaseStartDate ) ) ? '\'' . $this->m_strLeaseStartDate . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, entity_id, insurance_policy_id, insurance_property_id, email_address, lead_source_type_id, company_quote_number, updated_by, updated_on, created_by, created_on, aws_sent_on, lease_id, original_lead_source_type_id, details, customer_id, lease_start_date )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlEntityId() . ', ' .
						$this->sqlInsurancePolicyId() . ', ' .
						$this->sqlInsurancePropertyId() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlLeadSourceTypeId() . ', ' .
						$this->sqlCompanyQuoteNumber() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlAwsSentOn() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlOriginalLeadSourceTypeId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlLeaseStartDate() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entity_id = ' . $this->sqlEntityId(). ',' ; } elseif( true == array_key_exists( 'EntityId', $this->getChangedColumns() ) ) { $strSql .= ' entity_id = ' . $this->sqlEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_property_id = ' . $this->sqlInsurancePropertyId(). ',' ; } elseif( true == array_key_exists( 'InsurancePropertyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_property_id = ' . $this->sqlInsurancePropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_source_type_id = ' . $this->sqlLeadSourceTypeId(). ',' ; } elseif( true == array_key_exists( 'LeadSourceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lead_source_type_id = ' . $this->sqlLeadSourceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_quote_number = ' . $this->sqlCompanyQuoteNumber(). ',' ; } elseif( true == array_key_exists( 'CompanyQuoteNumber', $this->getChangedColumns() ) ) { $strSql .= ' company_quote_number = ' . $this->sqlCompanyQuoteNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' aws_sent_on = ' . $this->sqlAwsSentOn(). ',' ; } elseif( true == array_key_exists( 'AwsSentOn', $this->getChangedColumns() ) ) { $strSql .= ' aws_sent_on = ' . $this->sqlAwsSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_lead_source_type_id = ' . $this->sqlOriginalLeadSourceTypeId(). ',' ; } elseif( true == array_key_exists( 'OriginalLeadSourceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' original_lead_source_type_id = ' . $this->sqlOriginalLeadSourceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate(). ',' ; } elseif( true == array_key_exists( 'LeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'entity_id' => $this->getEntityId(),
			'insurance_policy_id' => $this->getInsurancePolicyId(),
			'insurance_property_id' => $this->getInsurancePropertyId(),
			'email_address' => $this->getEmailAddress(),
			'lead_source_type_id' => $this->getLeadSourceTypeId(),
			'company_quote_number' => $this->getCompanyQuoteNumber(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'aws_sent_on' => $this->getAwsSentOn(),
			'lease_id' => $this->getLeaseId(),
			'original_lead_source_type_id' => $this->getOriginalLeadSourceTypeId(),
			'details' => $this->getDetails(),
			'customer_id' => $this->getCustomerId(),
			'lease_start_date' => $this->getLeaseStartDate()
		);
	}

}
?>