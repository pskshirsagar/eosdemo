<?php

class CBaseInsuranceProductLimit extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_product_limits';

	protected $m_intId;
	protected $m_intInsuranceCarrierId;
	protected $m_intInsurancePolicyTypeId;
	protected $m_intInsuranceLimitTypeId;
	protected $m_intInsuranceAmountTypeId;
	protected $m_intIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_carrier_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierId', trim( $arrValues['insurance_carrier_id'] ) ); elseif( isset( $arrValues['insurance_carrier_id'] ) ) $this->setInsuranceCarrierId( $arrValues['insurance_carrier_id'] );
		if( isset( $arrValues['insurance_policy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyTypeId', trim( $arrValues['insurance_policy_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_type_id'] ) ) $this->setInsurancePolicyTypeId( $arrValues['insurance_policy_type_id'] );
		if( isset( $arrValues['insurance_limit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceLimitTypeId', trim( $arrValues['insurance_limit_type_id'] ) ); elseif( isset( $arrValues['insurance_limit_type_id'] ) ) $this->setInsuranceLimitTypeId( $arrValues['insurance_limit_type_id'] );
		if( isset( $arrValues['insurance_amount_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceAmountTypeId', trim( $arrValues['insurance_amount_type_id'] ) ); elseif( isset( $arrValues['insurance_amount_type_id'] ) ) $this->setInsuranceAmountTypeId( $arrValues['insurance_amount_type_id'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsuranceCarrierId( $intInsuranceCarrierId ) {
		$this->set( 'm_intInsuranceCarrierId', CStrings::strToIntDef( $intInsuranceCarrierId, NULL, false ) );
	}

	public function getInsuranceCarrierId() {
		return $this->m_intInsuranceCarrierId;
	}

	public function sqlInsuranceCarrierId() {
		return ( true == isset( $this->m_intInsuranceCarrierId ) ) ? ( string ) $this->m_intInsuranceCarrierId : 'NULL';
	}

	public function setInsurancePolicyTypeId( $intInsurancePolicyTypeId ) {
		$this->set( 'm_intInsurancePolicyTypeId', CStrings::strToIntDef( $intInsurancePolicyTypeId, NULL, false ) );
	}

	public function getInsurancePolicyTypeId() {
		return $this->m_intInsurancePolicyTypeId;
	}

	public function sqlInsurancePolicyTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyTypeId ) ) ? ( string ) $this->m_intInsurancePolicyTypeId : 'NULL';
	}

	public function setInsuranceLimitTypeId( $intInsuranceLimitTypeId ) {
		$this->set( 'm_intInsuranceLimitTypeId', CStrings::strToIntDef( $intInsuranceLimitTypeId, NULL, false ) );
	}

	public function getInsuranceLimitTypeId() {
		return $this->m_intInsuranceLimitTypeId;
	}

	public function sqlInsuranceLimitTypeId() {
		return ( true == isset( $this->m_intInsuranceLimitTypeId ) ) ? ( string ) $this->m_intInsuranceLimitTypeId : 'NULL';
	}

	public function setInsuranceAmountTypeId( $intInsuranceAmountTypeId ) {
		$this->set( 'm_intInsuranceAmountTypeId', CStrings::strToIntDef( $intInsuranceAmountTypeId, NULL, false ) );
	}

	public function getInsuranceAmountTypeId() {
		return $this->m_intInsuranceAmountTypeId;
	}

	public function sqlInsuranceAmountTypeId() {
		return ( true == isset( $this->m_intInsuranceAmountTypeId ) ) ? ( string ) $this->m_intInsuranceAmountTypeId : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, insurance_carrier_id, insurance_policy_type_id, insurance_limit_type_id, insurance_amount_type_id, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlInsuranceCarrierId() . ', ' .
 						$this->sqlInsurancePolicyTypeId() . ', ' .
 						$this->sqlInsuranceLimitTypeId() . ', ' .
 						$this->sqlInsuranceAmountTypeId() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; } elseif( true == array_key_exists( 'InsuranceCarrierId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId() . ','; } elseif( true == array_key_exists( 'InsurancePolicyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_limit_type_id = ' . $this->sqlInsuranceLimitTypeId() . ','; } elseif( true == array_key_exists( 'InsuranceLimitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_limit_type_id = ' . $this->sqlInsuranceLimitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_amount_type_id = ' . $this->sqlInsuranceAmountTypeId() . ','; } elseif( true == array_key_exists( 'InsuranceAmountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_amount_type_id = ' . $this->sqlInsuranceAmountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_carrier_id' => $this->getInsuranceCarrierId(),
			'insurance_policy_type_id' => $this->getInsurancePolicyTypeId(),
			'insurance_limit_type_id' => $this->getInsuranceLimitTypeId(),
			'insurance_amount_type_id' => $this->getInsuranceAmountTypeId(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>