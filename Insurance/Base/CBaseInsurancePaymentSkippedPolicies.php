<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePaymentSkippedPolicies
 * Do not add any new functions to this class.
 */

class CBaseInsurancePaymentSkippedPolicies extends CEosPluralBase {

	/**
	 * @return CInsurancePaymentSkippedPolicy[]
	 */
	public static function fetchInsurancePaymentSkippedPolicies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsurancePaymentSkippedPolicy::class, $objDatabase );
	}

	/**
	 * @return CInsurancePaymentSkippedPolicy
	 */
	public static function fetchInsurancePaymentSkippedPolicy( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsurancePaymentSkippedPolicy::class, $objDatabase );
	}

	public static function fetchInsurancePaymentSkippedPolicyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_payment_skipped_policies', $objDatabase );
	}

	public static function fetchInsurancePaymentSkippedPolicyById( $intId, $objDatabase ) {
		return self::fetchInsurancePaymentSkippedPolicy( sprintf( 'SELECT * FROM insurance_payment_skipped_policies WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchInsurancePaymentSkippedPoliciesByInsurancePolicyId( $intInsurancePolicyId, $objDatabase ) {
		return self::fetchInsurancePaymentSkippedPolicies( sprintf( 'SELECT * FROM insurance_payment_skipped_policies WHERE insurance_policy_id = %d', $intInsurancePolicyId ), $objDatabase );
	}

	public static function fetchInsurancePaymentSkippedPoliciesByPaymentReferenceId( $intPaymentReferenceId, $objDatabase ) {
		return self::fetchInsurancePaymentSkippedPolicies( sprintf( 'SELECT * FROM insurance_payment_skipped_policies WHERE payment_reference_id = %d', $intPaymentReferenceId ), $objDatabase );
	}

}
?>