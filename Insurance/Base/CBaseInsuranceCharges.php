<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceCharges
 * Do not add any new functions to this class.
 */

class CBaseInsuranceCharges extends CEosPluralBase {

	/**
	 * @return CInsuranceCharge[]
	 */
	public static function fetchInsuranceCharges( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceCharge::class, $objDatabase );
	}

	/**
	 * @return CInsuranceCharge
	 */
	public static function fetchInsuranceCharge( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceCharge::class, $objDatabase );
	}

	public static function fetchInsuranceChargeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_charges', $objDatabase );
	}

	public static function fetchInsuranceChargeById( $intId, $objDatabase ) {
		return self::fetchInsuranceCharge( sprintf( 'SELECT * FROM insurance_charges WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceChargesByEntityId( $intEntityId, $objDatabase ) {
		return self::fetchInsuranceCharges( sprintf( 'SELECT * FROM insurance_charges WHERE entity_id = %d', ( int ) $intEntityId ), $objDatabase );
	}

	public static function fetchInsuranceChargesByInsurancePolicyId( $intInsurancePolicyId, $objDatabase ) {
		return self::fetchInsuranceCharges( sprintf( 'SELECT * FROM insurance_charges WHERE insurance_policy_id = %d', ( int ) $intInsurancePolicyId ), $objDatabase );
	}

	public static function fetchInsuranceChargesByPremiumTransactionId( $intPremiumTransactionId, $objDatabase ) {
		return self::fetchInsuranceCharges( sprintf( 'SELECT * FROM insurance_charges WHERE premium_transaction_id = %d', ( int ) $intPremiumTransactionId ), $objDatabase );
	}

	public static function fetchInsuranceChargesByTaxTransactionId( $intTaxTransactionId, $objDatabase ) {
		return self::fetchInsuranceCharges( sprintf( 'SELECT * FROM insurance_charges WHERE tax_transaction_id = %d', ( int ) $intTaxTransactionId ), $objDatabase );
	}

	public static function fetchInsuranceChargesByInsurancePolicyLiabilityChargeId( $intInsurancePolicyLiabilityChargeId, $objDatabase ) {
		return self::fetchInsuranceCharges( sprintf( 'SELECT * FROM insurance_charges WHERE insurance_policy_liability_charge_id = %d', ( int ) $intInsurancePolicyLiabilityChargeId ), $objDatabase );
	}

	public static function fetchInsuranceChargesByInsuranceChargeTypeId( $intInsuranceChargeTypeId, $objDatabase ) {
		return self::fetchInsuranceCharges( sprintf( 'SELECT * FROM insurance_charges WHERE insurance_charge_type_id = %d', ( int ) $intInsuranceChargeTypeId ), $objDatabase );
	}

	public static function fetchInsuranceChargesByInsurancePolicyCoverageDetailId( $intInsurancePolicyCoverageDetailId, $objDatabase ) {
		return self::fetchInsuranceCharges( sprintf( 'SELECT * FROM insurance_charges WHERE insurance_policy_coverage_detail_id = %d', ( int ) $intInsurancePolicyCoverageDetailId ), $objDatabase );
	}

}
?>