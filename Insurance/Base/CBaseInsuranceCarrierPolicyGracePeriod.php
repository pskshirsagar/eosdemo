<?php

class CBaseInsuranceCarrierPolicyGracePeriod extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_carrier_policy_grace_periods';

	protected $m_intId;
	protected $m_intInsuranceCarrierId;
	protected $m_strStateCode;
	protected $m_strGracePeriodDays;
	protected $m_strPostalCode;
	protected $m_strNotes;
	protected $m_boolIsActive;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_carrier_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierId', trim( $arrValues['insurance_carrier_id'] ) ); elseif( isset( $arrValues['insurance_carrier_id'] ) ) $this->setInsuranceCarrierId( $arrValues['insurance_carrier_id'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( $arrValues['state_code'] ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( $arrValues['state_code'] );
		if( isset( $arrValues['grace_period_days'] ) && $boolDirectSet ) $this->set( 'm_strGracePeriodDays', trim( $arrValues['grace_period_days'] ) ); elseif( isset( $arrValues['grace_period_days'] ) ) $this->setGracePeriodDays( $arrValues['grace_period_days'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( $arrValues['postal_code'] ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( $arrValues['postal_code'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsuranceCarrierId( $intInsuranceCarrierId ) {
		$this->set( 'm_intInsuranceCarrierId', CStrings::strToIntDef( $intInsuranceCarrierId, NULL, false ) );
	}

	public function getInsuranceCarrierId() {
		return $this->m_intInsuranceCarrierId;
	}

	public function sqlInsuranceCarrierId() {
		return ( true == isset( $this->m_intInsuranceCarrierId ) ) ? ( string ) $this->m_intInsuranceCarrierId : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 5, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStateCode ) : '\'' . addslashes( $this->m_strStateCode ) . '\'' ) : 'NULL';
	}

	public function setGracePeriodDays( $strGracePeriodDays ) {
		$this->set( 'm_strGracePeriodDays', CStrings::strTrimDef( $strGracePeriodDays, 5, NULL, true ) );
	}

	public function getGracePeriodDays() {
		return $this->m_strGracePeriodDays;
	}

	public function sqlGracePeriodDays() {
		return ( true == isset( $this->m_strGracePeriodDays ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGracePeriodDays ) : '\'' . addslashes( $this->m_strGracePeriodDays ) . '\'' ) : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 5, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostalCode ) : '\'' . addslashes( $this->m_strPostalCode ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, insurance_carrier_id, state_code, grace_period_days, postal_code, notes, is_active, start_date, end_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlInsuranceCarrierId() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlGracePeriodDays() . ', ' .
						$this->sqlPostalCode() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlIsActive() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId(). ',' ; } elseif( true == array_key_exists( 'InsuranceCarrierId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' grace_period_days = ' . $this->sqlGracePeriodDays(). ',' ; } elseif( true == array_key_exists( 'GracePeriodDays', $this->getChangedColumns() ) ) { $strSql .= ' grace_period_days = ' . $this->sqlGracePeriodDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode(). ',' ; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_carrier_id' => $this->getInsuranceCarrierId(),
			'state_code' => $this->getStateCode(),
			'grace_period_days' => $this->getGracePeriodDays(),
			'postal_code' => $this->getPostalCode(),
			'notes' => $this->getNotes(),
			'is_active' => $this->getIsActive(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>