<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceLeads
 * Do not add any new functions to this class.
 */

class CBaseInsuranceLeads extends CEosPluralBase {

	/**
	 * @return CInsuranceLead[]
	 */
	public static function fetchInsuranceLeads( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceLead::class, $objDatabase );
	}

	/**
	 * @return CInsuranceLead
	 */
	public static function fetchInsuranceLead( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceLead::class, $objDatabase );
	}

	public static function fetchInsuranceLeadCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_leads', $objDatabase );
	}

	public static function fetchInsuranceLeadById( $intId, $objDatabase ) {
		return self::fetchInsuranceLead( sprintf( 'SELECT * FROM insurance_leads WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchInsuranceLeadsByCid( $intCid, $objDatabase ) {
		return self::fetchInsuranceLeads( sprintf( 'SELECT * FROM insurance_leads WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchInsuranceLeadsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchInsuranceLeads( sprintf( 'SELECT * FROM insurance_leads WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchInsuranceLeadsByLeaseId( $intLeaseId, $objDatabase ) {
		return self::fetchInsuranceLeads( sprintf( 'SELECT * FROM insurance_leads WHERE lease_id = %d', $intLeaseId ), $objDatabase );
	}

	public static function fetchInsuranceLeadsByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchInsuranceLeads( sprintf( 'SELECT * FROM insurance_leads WHERE customer_id = %d', $intCustomerId ), $objDatabase );
	}

	public static function fetchInsuranceLeadsByCustomerTypeId( $intCustomerTypeId, $objDatabase ) {
		return self::fetchInsuranceLeads( sprintf( 'SELECT * FROM insurance_leads WHERE customer_type_id = %d', $intCustomerTypeId ), $objDatabase );
	}

	public static function fetchInsuranceLeadsByInsurancePolicyQuoteId( $intInsurancePolicyQuoteId, $objDatabase ) {
		return self::fetchInsuranceLeads( sprintf( 'SELECT * FROM insurance_leads WHERE insurance_policy_quote_id = %d', $intInsurancePolicyQuoteId ), $objDatabase );
	}

	public static function fetchInsuranceLeadsByInsurancePolicyTypeId( $intInsurancePolicyTypeId, $objDatabase ) {
		return self::fetchInsuranceLeads( sprintf( 'SELECT * FROM insurance_leads WHERE insurance_policy_type_id = %d', $intInsurancePolicyTypeId ), $objDatabase );
	}

}
?>